#Getting Started#

To get started with the CCM8 repository, you need to start off by creating a clone of this remote repository on your local machine. Then you'll want to set up a local testing branch where you can safely make changes. Then you're ready to go.

##Cloning the remote repo locally##
Probably the easiest way to clone CCM8 to your local machine is through Visual Studio 2015:

1. Open Visual Studio 2015;
2. Ensure that the source control provider for VS2015 is the Microsoft Git Provider via Tools -> Options -> Source Control;
3. Open the Team Explorer window;
4. Under Local Git Repositories, select Clone;
5. The URL of the repo can be found at the top of this page and should look something like `https://username@bitbucket.org/galleriarts/ccm8.git`;
6. The file path is the folder on your local machine that you want the repo to live, e.g. C:\Dev\CCM8\;
7. Click the clone button.

This initial clone will take a little while, because you're downloading all the code as well as the commit information about all the branches in the remote repo. Once the clone is complete, you have a local repo that is distinct from the remote repo that you can start to make changes in.

## Setting up a safe, local test branch
You'll notice that your local repository has, among others, a master branch. The remote master branch (shown in your local repo as origin/master (origin is the name of the remote repo)) is the branch that contains all of our completed and committed code changes and should never be committed or pushed to directly. 
Your local master branch is tracking this remote master, which means that if you accidently make any code changes on it then you're one click away from pushing these changes up to the remote master by mistake.

To avoid any accidental mistakes, its best to create a new branch that you can use as a copy of the remote master that you can keep up-to-date, delete, copy and re-create as often as you like. In order to do this, follow these steps:

1. Open the Team Explorer window;
2. Click the Branches button to open the Branches window;
3. Expand the remotes/origin node and right-click on the master branch;
4. Select New Local Branch From (this will create a new local branch that branches from the current state of the remote master branch);
5. At the top of the Branches screen, you can now choose the name of this branch and set up some options for it;
6. I suggest calling it something different to master, e.g. local-master;
7. Leave Checkout branch ticked, which just means that you will switch to this new branch once created in your local repo;
8. **Un-tick Track remote branch** which will disconnect this new branch from the remote master, making it a safe place to work;
9. Click Create Branch.

You will now have two branches in your local repo related to the remote repo:

1. master;
2. local-master.

The key difference is that master is tracking the remote master (which means that you can easily push unwanted changes up to the remote master) and local-master is *not* tracking the remote master (which means that any accidental pushes would go into a new branch, leaving the remote master unaffected).

Finally, you can delete the original master branch by right-clicking on it and selecting Delete. Branches are cheap and quick to create, so there's no problem deleting this branch and creating as many "local-master" type branches as you like, in additional to more formal topic branches.

# Notes on POST services
The url for discovering Jira projects is:

https://galleria-rts.atlassian.net/rest/bitbucket/1.0/repositories

The url for the POST services is:

https://galleria-rts.atlassian.net/rest/bitbucket/1.0/repository/PROJECT-ID/sync

where project id can be found be discovering the projects.
