﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26156 : N.Foster
//  Swapped to use framework dal classes
// V8-28215 : A.Probyn
//  Updated FileName to use Constants.AppDataFolderName for app data folder.
#endregion
#endregion

using System;
using System.IO;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Dal.Sync.VistaDb
{
    /// <summary>
    /// Factory class for this dal
    /// </summary>
    public class DalFactory : Galleria.Framework.Dal.VistaDb.DalFactoryBase
    {
        #region Properties
        /// <summary>
        /// Returns the database type name
        /// </summary>
        protected override String DatabaseTypeName
        {
            get { return "Customer Centric Merchandising Synchronization"; }
        }

        /// <summary>
        /// Returns the default database file name
        /// </summary>
        protected override String DefaultFileName
        {
            get
            {
                return Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    Constants.AppDataFolderName,
                    "synchronization.syncdb");
            }
        }
        #endregion

        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion
    }
}
