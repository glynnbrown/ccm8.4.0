﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26156 : N.Foster
//  Swapped to use framework dal classes
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Sync.VistaDb.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.VistaDb;
using Galleria.Framework.DataStructures;
using VistaDB;
using VistaDB.Diagnostic;
using VistaDB.Provider;

namespace Galleria.Ccm.Dal.Sync.VistaDb.Implementation
{
    public class SyncSourceDal : Galleria.Framework.Dal.VistaDb.DalBase, ISyncSourceDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private SyncSourceDto GetDataTransferObject(VistaDBDataReader dr)
        {
            return new SyncSourceDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.SyncSourceId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.SyncSourceRowVersion])),
                ServerName = (String)GetValue(dr[FieldNames.SyncSourceServerName]),
                SiteName = (String)GetValue(dr[FieldNames.SyncSourceSiteName]),
                PortNumber = (Int32)GetValue(dr[FieldNames.SyncSourcePortNumber])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public SyncSourceDto FetchById(int id)
        {
            SyncSourceDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncSourceFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncSourceId,
                        VistaDBType.Int,
                        id);

                    // execute
                    using (VistaDBDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns all sync source information from the database
        /// </summary>
        /// <returns>An enumerable list of sync target</returns>
        public IEnumerable<SyncSourceDto> FetchAll()
        {
            List<SyncSourceDto> dtoList = new List<SyncSourceDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.SyncSourceFetchAll))
            {
                using (VistaDBDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Insterts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(SyncSourceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncSourceInsert))
                {
                    // id
                    VistaDBParameter idParameter = CreateParameter(command,
                        FieldNames.SyncSourceId,
                        VistaDBType.Int);

                    // row version
                    VistaDBParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.SyncSourceRowVersion,
                        VistaDBType.Timestamp);

                    // server name
                    CreateParameter(command,
                        FieldNames.SyncSourceServerName,
                        VistaDBType.NVarChar,
                        dto.ServerName);

                    // site name
                    CreateParameter(command,
                        FieldNames.SyncSourceSiteName,
                        VistaDBType.NVarChar,
                        dto.SiteName);

                    // port number
                    CreateParameter(command,
                        FieldNames.SyncSourcePortNumber,
                        VistaDBType.Int,
                        dto.PortNumber);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(SyncSourceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncSourceUpdate))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncSourceId,
                        VistaDBType.Int,
                        dto.Id);

                    // row version
                    VistaDBParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.SyncSourceRowVersion,
                        VistaDBType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);

                    // server name
                    CreateParameter(command,
                        FieldNames.SyncSourceServerName,
                        VistaDBType.NVarChar,
                        dto.ServerName);

                    // site name
                    CreateParameter(command,
                        FieldNames.SyncSourceSiteName,
                        VistaDBType.NVarChar,
                        dto.SiteName);

                    // port number
                    CreateParameter(command,
                        FieldNames.SyncSourcePortNumber,
                        VistaDBType.Int,
                        dto.PortNumber);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncSourceDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncSourceId,
                        VistaDBType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}