﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26156 : N.Foster
//  Swapped to use framework dal classes
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Sync.VistaDb.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.VistaDb;
using VistaDB;
using VistaDB.Diagnostic;
using VistaDB.Provider;

namespace Galleria.Ccm.Dal.Sync.VistaDb.Implementation
{
    public class SyncTargetDal : Galleria.Framework.Dal.VistaDb.DalBase, ISyncTargetDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private SyncTargetDto GetDataTransferObject(VistaDBDataReader dr)
        {
            return new SyncTargetDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.SyncTargetId]),
                TargetType = (Byte)GetValue(dr[FieldNames.SyncTargetType]),
                ConnectionString = (String)GetValue(dr[FieldNames.SyncTargetConnectionString]),
                Status = (Byte)GetValue(dr[FieldNames.SyncTargetStatus]),
                Frequency = (Byte)GetValue(dr[FieldNames.SyncTargetFrequency]),
                LastSync = (DateTime?)GetValue(dr[FieldNames.SyncTargetLastSync]),
                Error = (Byte)GetValue(dr[FieldNames.SyncTargetError]),
                IsActive = (Boolean)GetValue(dr[FieldNames.SyncTargetIsActive]),
                EntityId = (Int32)GetValue(dr[FieldNames.SyncTargetEntityId]),
                RetryCount = (Int32)GetValue(dr[FieldNames.SyncTargetRetryCount]),
                FailedCount = (Int32)GetValue(dr[FieldNames.SyncTargetFailedCount]),
                LastRun = (DateTime?)GetValue(dr[FieldNames.SyncTargetLastRun])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public SyncTargetDto FetchById(int id)
        {
            SyncTargetDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncTargetId,
                        VistaDBType.Int,
                        id);

                    // execute
                    using (VistaDBDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns all sync target information from the database
        /// </summary>
        /// <returns>An enumerable list of sync target</returns>
        public IEnumerable<SyncTargetDto> FetchAll()
        {
            List<SyncTargetDto> dtoList = new List<SyncTargetDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetFetchAll))
            {
                using (VistaDBDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Insterts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(SyncTargetDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetInsert))
                {
                    // id
                    VistaDBParameter idParameter = CreateParameter(command,
                        FieldNames.SyncTargetId,
                        VistaDBType.Int);

                    // type
                    CreateParameter(command,
                        FieldNames.SyncTargetType,
                        VistaDBType.TinyInt,
                        dto.TargetType);

                    // connection string
                    CreateParameter(command,
                        FieldNames.SyncTargetConnectionString,
                        VistaDBType.NVarChar,
                        dto.ConnectionString);

                    // status
                    CreateParameter(command,
                        FieldNames.SyncTargetStatus,
                        VistaDBType.TinyInt,
                        dto.Status);

                    // frequency
                    CreateParameter(command,
                        FieldNames.SyncTargetFrequency,
                        VistaDBType.TinyInt,
                        dto.Frequency);

                    // last sync
                    CreateParameter(command,
                        FieldNames.SyncTargetLastSync,
                        VistaDBType.SmallInt,
                        dto.LastSync);

                    // error
                    CreateParameter(command,
                        FieldNames.SyncTargetError,
                        VistaDBType.TinyInt,
                        dto.Error);

                    // is active
                    CreateParameter(command,
                        FieldNames.SyncTargetIsActive,
                        VistaDBType.Bit,
                        dto.IsActive);

                    // entity id
                    CreateParameter(command,
                        FieldNames.SyncTargetEntityId,
                        VistaDBType.Int,
                        dto.EntityId);

                    // retry count
                    CreateParameter(command,
                        FieldNames.SyncTargetRetryCount,
                        VistaDBType.Int,
                        dto.RetryCount);

                    // failed count
                    CreateParameter(command,
                        FieldNames.SyncTargetFailedCount,
                        VistaDBType.Int,
                        dto.FailedCount);

                    // last run
                    CreateParameter(command,
                        FieldNames.SyncTargetLastRun,
                        VistaDBType.DateTime,
                        dto.LastRun);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(SyncTargetDto dto, SyncTargetIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new SyncTargetIsSetDto();
            }

            //Construct SetProperties String
            String setProperties = GenerateSetPropertiesString(isSetDto);

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetUpdate))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncTargetId,
                        VistaDBType.Int,
                        dto.Id);

                    // type
                    CreateParameter(command,
                        FieldNames.SyncTargetType,
                        VistaDBType.TinyInt,
                        dto.TargetType);

                    // connection string
                    CreateParameter(command,
                        FieldNames.SyncTargetConnectionString,
                        VistaDBType.NVarChar,
                        dto.ConnectionString);

                    // status
                    CreateParameter(command,
                        FieldNames.SyncTargetStatus,
                        VistaDBType.TinyInt,
                        dto.Status);

                    // frequency
                    CreateParameter(command,
                        FieldNames.SyncTargetFrequency,
                        VistaDBType.TinyInt,
                        dto.Frequency);

                    // last sync
                    CreateParameter(command,
                        FieldNames.SyncTargetLastSync,
                        VistaDBType.DateTime,
                        dto.LastSync);

                    // error
                    CreateParameter(command,
                        FieldNames.SyncTargetError,
                        VistaDBType.TinyInt,
                        dto.Error);

                    // is active
                    CreateParameter(command,
                        FieldNames.SyncTargetIsActive,
                        VistaDBType.Bit,
                        dto.IsActive);

                    // entity id
                    CreateParameter(command,
                        FieldNames.SyncTargetEntityId,
                        VistaDBType.Int,
                        dto.EntityId);

                    // retry count
                    CreateParameter(command,
                        FieldNames.SyncTargetRetryCount,
                        VistaDBType.Int,
                        dto.RetryCount);

                    // failed count
                    CreateParameter(command,
                        FieldNames.SyncTargetFailedCount,
                        VistaDBType.Int,
                        dto.FailedCount);

                    // last run
                    CreateParameter(command,
                        FieldNames.SyncTargetLastRun,
                        VistaDBType.DateTime,
                        dto.LastRun);

                    // set properties
                    CreateParameter(command,
                        FieldNames.SyncTargetSetProperties,
                        VistaDBType.NVarChar,
                        setProperties);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        private string GenerateSetPropertiesString(SyncTargetIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            // Target Type
            if (isSetDto.IsTargetTypeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetType);
            }

            // Connection String
            if (isSetDto.IsConnectionStringSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetConnectionString);
            }

            // Status
            if (isSetDto.IsStatusSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetStatus);
            }

            // Frequency
            if (isSetDto.IsFrequencySet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetFrequency);
            }

            // Last Sync
            if (isSetDto.IsLastSyncSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetLastSync);
            }

            // Error
            if (isSetDto.IsErrorSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetError);
            }

            // Is Active
            if (isSetDto.IsIsActiveSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetIsActive);
            }

            // Entity Id
            if (isSetDto.IsEntityIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetEntityId);
            }

            // Retry Count
            if (isSetDto.IsRetryCountSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetRetryCount);
            }

            // Failed Count
            if (isSetDto.IsFailedCountSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetFailedCount);
            }

            // Last Run
            if (isSetDto.IsLastRunSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.SyncTargetLastRun);
            }

            return setProperties;
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncTargetId,
                        VistaDBType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}