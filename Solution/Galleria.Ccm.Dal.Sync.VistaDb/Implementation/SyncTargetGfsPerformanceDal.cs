﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26156 : N.Foster
//  Swapped to use framework dal classes
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Sync.VistaDb.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.VistaDb;
using Galleria.Framework.DataStructures;
using VistaDB;
using VistaDB.Diagnostic;
using VistaDB.Provider;

namespace Galleria.Ccm.Dal.Sync.VistaDb.Implementation
{
    public class SyncTargetGfsPerformanceDal : Galleria.Framework.Dal.VistaDb.DalBase, ISyncTargetGfsPerformanceDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private SyncTargetGfsPerformanceDto GetDataTransferObject(VistaDBDataReader dr)
        {
            return new SyncTargetGfsPerformanceDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.SyncTargetGfsPerformanceId]),
                SyncTargetId = (Int32)GetValue(dr[FieldNames.SyncTargetGfsPerformanceSyncTargetId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.SyncTargetGfsPerformanceRowVersion])),
                EntityName = (String)GetValue(dr[FieldNames.SyncTargetGfsPerformanceEntityName]),
                SourceId = (Int32)GetValue(dr[FieldNames.SyncTargetGfsPerformanceSourceId]),
                SourceName = (String)GetValue(dr[FieldNames.SyncTargetGfsPerformanceSourceName]),
                SourceType = (Byte)GetValue(dr[FieldNames.SyncTargetGfsPerformanceSourceType]),
                SourceSyncPeriod = (Byte?)GetValue(dr[FieldNames.SyncTargetGfsPerformanceSourceSyncPeriod])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public SyncTargetGfsPerformanceDto FetchById(int id)
        {
            SyncTargetGfsPerformanceDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetGfsPerformanceFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceId,
                        VistaDBType.Int,
                        id);

                    // execute
                    using (VistaDBDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns sync target related performance sources
        /// </summary>
        /// <returns>An enumerable list of sync target gfs performance sources</returns>
        public IEnumerable<SyncTargetGfsPerformanceDto> FetchBySyncTargetId(Int32 syncTargetId)
        {
            List<SyncTargetGfsPerformanceDto> dtoList = new List<SyncTargetGfsPerformanceDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetGfsPerformanceFetchBySyncTargetId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSyncTargetId,
                        VistaDBType.Int,
                        syncTargetId);

                    using (VistaDBDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Insterts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(SyncTargetGfsPerformanceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetGfsPerformanceInsert))
                {
                    // id
                    VistaDBParameter idParameter = CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceId,
                        VistaDBType.Int);

                    //  sync target id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSyncTargetId,
                        VistaDBType.Int,
                        dto.SyncTargetId);

                    // row version
                    VistaDBParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceRowVersion,
                        VistaDBType.Timestamp);

                    // entity name
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceEntityName,
                        VistaDBType.NVarChar,
                        dto.EntityName);

                    //  source id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceId,
                        VistaDBType.Int,
                        dto.SourceId);

                    // source name
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceName,
                        VistaDBType.NVarChar,
                        dto.SourceName);

                    //  source type
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceType,
                        VistaDBType.TinyInt,
                        dto.SourceType);

                    // source sync period
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceSyncPeriod,
                        VistaDBType.TinyInt,
                        dto.SourceSyncPeriod);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(SyncTargetGfsPerformanceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetGfsPerformanceUpdate))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceId,
                        VistaDBType.Int,
                        dto.Id);

                    // row version
                    VistaDBParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceRowVersion,
                        VistaDBType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);

                    //  sync target id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSyncTargetId,
                        VistaDBType.Int,
                        dto.SyncTargetId);

                    // entity name
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceEntityName,
                        VistaDBType.NVarChar,
                        dto.EntityName);

                    //  source id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceId,
                        VistaDBType.Int,
                        dto.SourceId);

                    // source name
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceName,
                        VistaDBType.NVarChar,
                        dto.SourceName);

                    //  source type
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceType,
                        VistaDBType.TinyInt,
                        dto.SourceType);

                    // source sync period
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSourceSyncPeriod,
                        VistaDBType.TinyInt,
                        dto.SourceSyncPeriod);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetGfsPerformanceDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceId,
                        VistaDBType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteBySyncTargetId(Int32 syncTargetId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SyncTargetGfsPerformanceDeleteBySyncTargetId))
                {
                    // sync target id
                    CreateParameter(command,
                        FieldNames.SyncTargetGfsPerformanceSyncTargetId,
                        VistaDBType.Int,
                        syncTargetId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (VistaDBSQLException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}