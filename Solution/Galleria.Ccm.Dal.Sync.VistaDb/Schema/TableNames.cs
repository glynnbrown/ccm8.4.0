﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Sync.VistaDb.Schema
{
    /// <summary>
    /// Defines table names within the database schema
    /// </summary>
    internal class TableNames
    {
        public const string SyncSource = "SyncSource";
        public const string SyncTarget = "SyncTarget";
        public const string SyncTargetGfsPerformance = "SyncTargetGfsPerformance";
        public const string SchemaVersion = "SchemaVersion";
    }
}