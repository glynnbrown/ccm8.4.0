﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Sync.VistaDb.Schema
{
    /// <summary>
    /// Defines field names within the database schema
    /// </summary>
    internal class FieldNames
    {
        #region Sync Source

        public const string SyncSourceId = "SyncSource_Id";
        public const string SyncSourceRowVersion = "SyncSource_RowVersion";
        public const string SyncSourceServerName = "SyncSource_ServerName";
        public const string SyncSourceSiteName = "SyncSource_SiteName";
        public const string SyncSourcePortNumber = "SyncSource_PortNumber";

        #endregion

        #region Sync Target

        public const string SyncTargetId = "SyncTarget_Id";
        public const string SyncTargetType = "SyncTarget_Type";
        public const string SyncTargetConnectionString = "SyncTarget_ConnectionString";
        public const string SyncTargetStatus = "SyncTarget_Status";
        public const string SyncTargetFrequency = "SyncTarget_Frequency";
        public const string SyncTargetLastSync = "SyncTarget_LastSync";
        public const string SyncTargetError = "SyncTarget_Error";
        public const string SyncTargetIsActive = "SyncTarget_IsActive";
        public const string SyncTargetEntityId = "SyncTarget_Entity_Id";
        public const string SyncTargetRetryCount = "SyncTarget_RetryCount";
        public const string SyncTargetFailedCount = "SyncTarget_FailedCount";
        public const string SyncTargetLastRun = "SyncTarget_LastRun";
        public const string SyncTargetSetProperties = "SyncTarget_SetProperties";

        #endregion

        #region Sync Target Gfs Performance

        public const string SyncTargetGfsPerformanceId = "SyncTargetGfsPerformance_Id";
        public const string SyncTargetGfsPerformanceSyncTargetId = "SyncTarget_Id";
        public const string SyncTargetGfsPerformanceRowVersion = "SyncTargetGfsPerformance_RowVersion";
        public const string SyncTargetGfsPerformanceEntityName = "SyncTargetGfsPerformance_EntityName";
        public const string SyncTargetGfsPerformanceSourceId = "SyncTargetGfsPerformance_SourceId";
        public const string SyncTargetGfsPerformanceSourceName = "SyncTargetGfsPerformance_SourceName";
        public const string SyncTargetGfsPerformanceSourceType = "SyncTargetGfsPerformance_SourceType";
        public const string SyncTargetGfsPerformanceSourceSyncPeriod = "SyncTargetGfsPerformance_SourceSyncPeriod";

        #endregion

        #region SchemaVersion

        public const string SchemaVersion_Id = "SchemaVersion_Id";
        public const string SchemaVersion_Type = "SchemaVersion_Type";
        public const string SchemaVersion_MajorVersion = "SchemaVersion_MajorVersion";
        public const string SchemaVersion_MinorVersion = "SchemaVersion_MinorVersion";

        #endregion

    }
}