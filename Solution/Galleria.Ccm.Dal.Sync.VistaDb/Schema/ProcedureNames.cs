﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Sync.VistaDb.Schema
{
    internal class ProcedureNames
    {
        #region Sync Source

        public const string SyncSourceFetchById = "SyncSource_FetchById";
        public const string SyncSourceFetchAll = "SyncSource_FetchAll";
        public const string SyncSourceInsert = "SyncSource_Insert";
        public const string SyncSourceUpdate = "SyncSource_Update";
        public const string SyncSourceDeleteById = "SyncSource_DeleteById";

        #endregion

        #region Sync Target

        public const string SyncTargetFetchById = "SyncTarget_FetchById";
        public const string SyncTargetFetchAll = "SyncTarget_FetchAll";
        public const string SyncTargetInsert = "SyncTarget_Insert";
        public const string SyncTargetUpdate = "SyncTarget_Update";
        public const string SyncTargetDeleteById = "SyncTarget_DeleteById";

        #endregion

        #region Sync Target GFS Performance

        public const string SyncTargetGfsPerformanceFetchById = "SyncTargetGfsPerformance_FetchById";
        public const string SyncTargetGfsPerformanceFetchBySyncTargetId = "SyncTargetGfsPerformance_FetchBySyncTargetId";
        public const string SyncTargetGfsPerformanceInsert = "SyncTargetGfsPerformance_Insert";
        public const string SyncTargetGfsPerformanceUpdate = "SyncTargetGfsPerformance_Update";
        public const string SyncTargetGfsPerformanceDeleteById = "SyncTargetGfsPerformance_DeleteById";
        public const string SyncTargetGfsPerformanceDeleteBySyncTargetId = "SyncTargetGfsPerformance_DeleteBySyncTargetId";

        #endregion
    }
}