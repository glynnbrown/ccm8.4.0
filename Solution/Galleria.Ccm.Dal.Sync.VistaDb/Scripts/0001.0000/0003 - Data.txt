﻿-- Title: CCM SYNC v1.0 Scripts
-- Description: Builds the default tables for the CCM Sync database
-- Product: CCM v8.0
-- Company: Galleria RTS Ltd
-- Copyright: Copyright © Galleria RTS Ltd 2014

-- Version History: (CCM 8.0)

---	V8-25556 : D.Pleasance
	--	Created