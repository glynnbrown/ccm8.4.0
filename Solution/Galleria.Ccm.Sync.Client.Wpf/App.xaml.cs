﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26159 : L.Ineson
//  Added gfs service types registration
#endregion
#endregion

using System;
using System.Drawing;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Globalization;
using System.Configuration;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Sync.Client.Wpf.Common;
using Galleria.Ccm.Sync.Client.Wpf.Startup;
using Galleria.Ccm.Sync.Client.Wpf.Resources;
using Galleria.Ccm.Sync.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Sync.Client.Wpf.Resources.Language;
using Desaware.MachineLicense40;
using Gibraltar.Agent;
using System.Windows.Markup;
using Csla;
using Galleria.Ccm.Helpers;
using System.Linq;
using System.Management;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.Sync.Client.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        #region Constants
        private const int _secondsToWaitForProcessShutdown = 5;
        private const string _sessionIdName = "Session ID";
        private const Int32 _pollTimerInterval = 250; //The interval in milliseconds for polling the process state
        private const Int32 _balloonTipTimeout = 500; //The balloon tip timeout

        private const string _contextMenuItemPreferences = "Preferences"; //Preferences key used to determine which context menu item was clicked
        private const string _contextMenuItemExit = "Exit"; //Exit key used to determine which context menu item was clicked
        #endregion

        #region Fields
        private MainPageOrganiser _mainWindow; //Main page organiser
        private NotifyIcon _notifyIcon; //System tray notify icon        
        private DispatcherTimer _dispatcherTimer; //Timer used to poll the service for its status
        private SyncStatus _lastRecordedStatus; //Records the last status
        private Boolean _unitTesting; //Indicates if we are loading the application for unit testing purposes
        private SecurityViewModel _securityViewModel; //The security view model
        private SecurityWindow _securityWindow; //The security window if required
        private SecurityDialogViewModel _securityDialogViewModel; //The security view model
        private SecurityDialog _securityDialog; //The security dialog if required
        private DomainLicense _securityLicense; //The security license methods 
        private CEIPViewModel _CEIPViewModel; //The customer experience improvment program view model
        private CEIPWindow _CEIPWindow; //The customer experience improvment program window if required
        private DemoDialog _demoDialog; //The Demo dialog if required
        private Boolean _showDialog; //Has the demo dialog already been displayed
        private static LicenseState _licenseState; //holds the state of the license
        #endregion

        #region Properties

        /// <summary>
        /// The Application LicenseState
        /// </summary>
        public static LicenseState LicenseState
        {
            get
            {
                if (_licenseState == null)
                {
                    _licenseState = new LicenseState();
                }
                return _licenseState;
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public App() : this(false) { }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="unitTesting">Indicates if we are loading this app for unit testing purposes</param>
        public App(bool unitTesting)
        {
            _unitTesting = unitTesting;
            this.Startup += new StartupEventHandler(App_Startup);
            this.Exit += new ExitEventHandler(App_Exit);
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// The main Application Startup code
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void App_Startup(object sender, StartupEventArgs e)
        {
            //Unsubscribe from the start-up event
            this.Startup -= App_Startup;

            //Override the current culture info
            // This determines the numberformats/currency etc that will be used by the app.
            // The following line ensures that it will be picked up from the pc ControlPanel-> Region and Language -> Format setting.
            FrameworkElement.LanguageProperty.OverrideMetadata(
            typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            //If we are not unit testing then pull back the display language
            if (!_unitTesting)
            {
                //Initialise the notify icon
                this.CreateNotifyIcon();

                //Stop this shutting down as each dialog closes
                this.ShutdownMode = ShutdownMode.OnExplicitShutdown;

                //register gfs service types
                FoundationServiceClient.RegisterGFSServiceClientTypes();

                //CEIP gilbraltar integration
                _CEIPViewModel = new CEIPViewModel();

                if (_CEIPViewModel.CEIPWindowRequired())
                {
                    ShowCEIPWindow();
                }

                //Check the current license
                _securityLicense = new DomainLicense();
                _securityViewModel = new SecurityViewModel(_securityLicense);
                _securityDialogViewModel = new SecurityDialogViewModel(_securityLicense);

                //Initalize the license component
                _securityLicense.LicenseInitalize();

                if (_securityLicense.DemoVersion())
                {
                    //Display The demo results of the License
                    ShowDemoDialog();
                }
                else
                {
                    if (_securityViewModel.SecurityWindowRequired())
                    {
                        //Application is not licensed Show Registration Window
                        ShowSecurityWindow();
                    }
                    else
                    {
                        if (_securityDialogViewModel.SecurityDialogWindowRequired())
                        {
                            //Display The results of the License
                            ShowSecurityDialog();
                        }
                    }
                }

                //Get the culture to use for the UI
                SettingsList userSettings = SettingsList.Instance;
                userSettings.LoadSettingsList();

                //set the display language
                //This is the language that the application will be displayed in.
                String cultureCode = userSettings.DisplayLanguage;
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureCode);
                
                //Check that an instance is not already running
                Process thisProc = Process.GetCurrentProcess();
                String thisProcOwner = GetProcessOwner(thisProc.Id);
                for (int i = 0; i <= _secondsToWaitForProcessShutdown; i++)
                {
                    //get all processes with the same name
                    Process[] matchingProcesses = Process.GetProcessesByName(thisProc.ProcessName);

                    //if there is more than one check if any has the same owner
                    if (matchingProcesses.Length > 1)
                    {
                        if (matchingProcesses.Where(p => GetProcessOwner(p.Id) == thisProcOwner).Count() > 1)
                        {
                            if (i < _secondsToWaitForProcessShutdown)
                            {
                                Thread.Sleep(1000);
                            }
                            else
                            {
                                System.Windows.MessageBox.Show(Galleria.Ccm.Sync.Client.Wpf.Resources.Language.Message.Application_AlreadyRunning);
                                App.Current.Shutdown();
                                return;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            //Initialize and start the logging session
            Log.Initializing += new Log.InitializingEventHandler(Log_Initializing);
            Log.StartSession();

            //Attach to the event to globally hand exceptions before they hit the os
            this.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

            //Load the application
            if (!_unitTesting)
            {
                LoadApplication();
                if (!App.LicenseState.IsLoaded)
                {
                    App.LicenseState.Refresh(_securityLicense);
                }
            }
        }

        /// <summary>
        /// Called when the application is closing down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void App_Exit(object sender, ExitEventArgs e)
        {
            //Unsubscribe from the exit event
            this.Exit -= App_Exit;

            //Remove the notify icon
            _notifyIcon.Visible = false;
            _notifyIcon.Dispose();

            //End the logging session
            Log.EndSession();
        }

        /// <summary>
        /// Forces to close the application when ShutdownMode is set to ExplicitShutdown
        /// </summary>
        private void ApplicationForceExit()
        {
            this.Startup -= App_Startup;
            this.Exit -= App_Exit;

            System.Environment.Exit(0);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns the ownername of the given process
        /// </summary>
        /// <param name="processId"></param>
        /// <returns></returns>
        public String GetProcessOwner(Int32 processId)
        {
            String query = "Select * From Win32_Process Where ProcessID = " + processId;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();

            foreach (ManagementObject obj in processList)
            {
                String[] argList = new String[] { String.Empty, String.Empty };
                Int32 returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                if (returnVal == 0)
                {
                    // return DOMAIN\user
                    return argList[1] + "\\" + argList[0];
                }
            }

            return "NO OWNER";
        }

        /// <summary>
        /// Go forth and load the main application window
        /// </summary>
        private void LoadApplication()
        {
            //Create the main page organiser window
            _mainWindow = new MainPageOrganiser();
            _mainWindow.StateChanged += new EventHandler(MainWindow_StateChanged);

            //Set the shutdown to when the main window closes.
            this.ShutdownMode = System.Windows.ShutdownMode.OnLastWindowClose;

            //Set notify icon as ready
            this.CreateNotifyIconReady();

            //Create the dispatcher timer
            this.CreateDispatcherTimer();


        }

        /// <summary>
        /// Initialize the dispatcher timer
        /// </summary>
        private void CreateDispatcherTimer()
        {
            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Interval = TimeSpan.FromMilliseconds(_pollTimerInterval);
            _dispatcherTimer.Tick += new EventHandler(DispatchTimer_Tick);
            _dispatcherTimer.Start();
        }

        /// <summary>
        /// Initialize the notify icon
        /// </summary>
        private void CreateNotifyIcon()
        {
            //Initialize the notify icon
            _notifyIcon = new NotifyIcon();
            //Make it visible
            _notifyIcon.Visible = true;
            //Set notify status                    
            this.SetNotifyStatus("ServiceUnknown.ico",
                                    Galleria.Ccm.Sync.Client.Wpf.Resources.Language.Message.ServiceStatus_Starting);
        }

        /// <summary>
        /// Ready the notify icon and set the context menu
        /// </summary>
        private void CreateNotifyIconReady()
        {
            //Create the context menu used for the notify icon
            ContextMenuStrip contextMenu = new ContextMenuStrip();

            //Preferences: create a context menu item
            ToolStripMenuItem menuItemPreferences = new ToolStripMenuItem();
            menuItemPreferences.Name = _contextMenuItemPreferences;
            menuItemPreferences.Text = Galleria.Ccm.Sync.Client.Wpf.Resources.Language.Message.SyncMaintenance_Service_Preferences;
            contextMenu.Items.Add(menuItemPreferences);

            //Add separator
            contextMenu.Items.Add(new ToolStripSeparator());

            //Exit: create a context menu item
            ToolStripMenuItem menuItemExit = new ToolStripMenuItem();
            menuItemExit.Name = _contextMenuItemExit;
            menuItemExit.Text = Galleria.Ccm.Sync.Client.Wpf.Resources.Language.Message.SyncMaintenance_Service_Exit;
            contextMenu.Items.Add(menuItemExit);

            //Create the context menu item clicked event handler
            contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContextMenu_ItemClicked);

            //Set the context menu as the notify context menu
            _notifyIcon.ContextMenuStrip = contextMenu;

            //Create the notify double click event handler
            _notifyIcon.MouseDoubleClick += new MouseEventHandler(NotifyIcon_MouseDoubleClick);
        }

        /// <summary>
        /// Sets the notify icon balloon status
        /// </summary>
        /// <param name="tipTitle">The header text</param>
        /// <param name="tipText">The ballon tooltip text</param>
        /// <param name="tipIcon">The tool tip icon</param>
        private void SetNotifyBalloonStatus(string tipTitle, string tipText, ToolTipIcon tipIcon)
        {
            //Set the balloon parameters
            _notifyIcon.BalloonTipTitle = tipTitle;
            _notifyIcon.BalloonTipText = tipText;
            _notifyIcon.BalloonTipIcon = tipIcon;
            //Show the balloon tip
            _notifyIcon.ShowBalloonTip(_balloonTipTimeout);
        }

        /// <summary>
        /// Show the main page organiser
        /// </summary>
        private void MainWindow_Show()
        {
            this.MainWindow.WindowState = WindowState.Normal;
            this.MainWindow.Show();
        }

        /// <summary>
        /// Sets the notify icon
        /// </summary>
        /// <param name="iconName">The icon name</param>
        /// /// <param name="tooltipText">The tooltip text to display</param>
        private void SetNotifyStatus(string iconName, string tooltipText)
        {
            //Set the notify icon
            _notifyIcon.Icon = new Icon(
                Assembly.GetExecutingAssembly().GetManifestResourceStream(
                String.Format(@"Galleria.Ccm.Sync.Client.Wpf.Resources.Icons.{0}", iconName)));

            //Show balloon status                    
            this.SetNotifyBalloonStatus(Galleria.Ccm.Sync.Client.Wpf.Resources.Language.Message.SystemTray_Title,
                                        tooltipText,
                                        ToolTipIcon.Info);

            //Set the tooltip text
            _notifyIcon.Text = tooltipText;
        }
        #endregion

        #region Events
        /// <summary>
        /// Called when the state of the window changes
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void MainWindow_StateChanged(object sender, EventArgs e)
        {
            if (this.MainWindow.WindowState == WindowState.Minimized)
            {
                this.MainWindow.ShowInTaskbar = false;
            }
            else if (this.MainWindow.WindowState == WindowState.Normal)
            {
                this.MainWindow.ShowInTaskbar = true;
            }
        }

        /// <summary>
        /// Called when the notify icon is double clicked
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void NotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //Show screen
            MainWindow_Show();
        }

        /// <summary>
        /// Event raised by the dispatch timer
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void DispatchTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                //Get the current status
                SyncStatus status = SyncStatus.GetCurrentStatus();

                //Only set the notify status if it has changed
                if (_lastRecordedStatus == null || _lastRecordedStatus.ServiceStatus != status.ServiceStatus)
                {
                    //Record the last status
                    _lastRecordedStatus = status;

                    //Set the notify status
                    this.SetNotifyStatus(
                        String.Format("Service{0}.ico", status.ServiceStatus.ToString()),
                        status.ServiceStatusDescription);

                    //Refresh the target list so the user can see any changes
                    _mainWindow.ViewModel.RefreshSyncTargetsCommand.Execute();
                }
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }
        }

        /// <summary>
        /// Called when an item in the context menu is clicked
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void ContextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case _contextMenuItemPreferences:
                    //Show screen
                    MainWindow_Show();
                    break;

                case _contextMenuItemExit:
                    //Exit
                    this.Shutdown();
                    break;
            }
        }
        #endregion

        #region Security Checking
        /// <summary>
        /// Show the Demo Dialog window
        /// </summary>
        private void ShowDemoDialog()
        {

            if (_showDialog == true)
            {
                //Reset the variable so demo dialog screen is not shown again
                _showDialog = false;

                //If it is a demo version then cheeck it is valid
                if (_securityLicense.IsServerConnection())
                {
                    if (_securityLicense.DemoValid() == false)
                    {
                        _securityLicense.ValidationStatus = ValidationStatus.DemoExpired;
                    }
                }

                if (_securityLicense.ValidationStatus == ValidationStatus.DemoExpired)
                {
                    ShowSecurityDialog();
                }
                else
                {
                    //Hook up to Security completed event and show Security window
                    _demoDialog = new DemoDialog(_securityLicense.DemoDaysRemaining());
                    _demoDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    bool? _dialogResult = _demoDialog.ShowDialog();

                    if (_dialogResult == false)
                    {
                        //Application is demo Show Registration Window
                        ShowSecurityWindow();
                    }
                }
            }
        }

        /// <summary>
        /// Show the Security window
        /// </summary>
        private void ShowSecurityDialog()
        {
            //Hook up to Security completed event and show Security window
            _securityDialog = new SecurityDialog(_securityDialogViewModel);
            _securityDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            bool? _dialogResult = _securityDialog.ShowDialog();

            if (_dialogResult == false)
            {
                if (_securityDialogViewModel.IsReturnButtonPressed == true)
                {
                    //Application is not licensed Show Registration Window
                    ShowSecurityWindow();
                }
                else
                {
                    ApplicationForceExit();
                    return;
                }
            }
            else
            {
                if (_securityDialogViewModel.IsValidationError == true)
                {
                    //Reset the install type to none to show the validation error
                    _securityLicense.securityInstallType = InstallType.None;

                    //Application has a validation error show dialog
                    ShowSecurityDialog();
                }
                else
                {
                    if (_securityLicense.DemoVersion() == true)
                    {
                        ShowDemoDialog();
                    }
                }
            }
        }

        /// <summary>
        /// Show the Security window
        /// </summary>
        private void ShowSecurityWindow()
        {
            //Hook up to Security completed event and show Security window
            _securityWindow = new SecurityWindow(_securityViewModel);
            _securityWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _securityViewModel.LoadRegistrationDetails();
            bool? _dialogResult = _securityWindow.ShowDialog();

            if (_dialogResult == true)
            {
                //Close Security window
                CloseSecurityWindow();

                //Display The results of the License
                ShowSecurityDialog();
            }
            else
            {
                ApplicationForceExit();
                return;
            }
        }

        /// <summary>
        /// Security completed, close window
        /// </summary>
        private void CloseSecurityWindow()
        {
            _securityWindow.Close();
        }
        #endregion

        #region CEIP Checking
        /// <summary>
        /// Show the Customer Experiance Improvment program window
        /// </summary>
        private void ShowCEIPWindow()
        {
            _CEIPWindow = new CEIPWindow(_CEIPViewModel);
            _CEIPWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _CEIPWindow.ShowDialog();
        }
        #endregion

        #region Gibraltar Logging
        /// <summary>
        /// Called when the log is initialized
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void Log_Initializing(object sender, LogInitializingEventArgs e)
        {
            // configure the application to log to the service
            e.Configuration.Server.UseGibraltarService = true;
            e.Configuration.Server.CustomerName = "galleria";
            e.Configuration.Server.SendAllApplications = true;
            e.Configuration.Server.PurgeSentSessions = true;
            e.Configuration.Server.UseSsl = true;
            e.Configuration.Properties[_sessionIdName] = Guid.NewGuid().ToString();
            e.Configuration.SessionFile.EnableFilePruning = true; // ISO-13436
            e.Configuration.SessionFile.MaxLocalDiskUsage = 200; // ISO-13436
            e.Configuration.Publisher.EnableAnonymousMode = false; // GFS-14882
#if !DEBUG
            // now determine if we are running within the IDE.
            // normally, you shouldn't change the behaviour of
            // an application depending on whether a debugger
            // is attached or not, however, we want to be able
            // to still use the logging facilities in debug
            // builds when they are distrubuted to people such
            // as qa and consultancy. Therefore we have used
            // this method instead of conditional compilation
            if (!Debugger.IsAttached) // GFS-14882
            {
                // set auto-sending of debug information to what the user has selected
                e.Configuration.Server.AutoSendSessions = _CEIPViewModel.IsParticipate();
            }
#endif
        }
        #endregion Gibraltar Logging

        #region Exception handling
        /// <summary>
        /// Handler to catch global exceptions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            //Show the exception has been handled
            e.Handled = true;

            //Log the exception
            Log.ReportException(e.Exception, "Unhandled Exception", false, true);

            //Close 
            System.Windows.Application.Current.Shutdown();
        }
        #endregion
    }
}