﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

namespace Galleria.Ccm.Sync.Client.Wpf.Common
{
    /// <summary>
    /// Enum to indicate different tabs of the ui
    /// </summary>
    public enum UIScreen
    {
        SyncSource,
        SyncTargets
    }

    /// <summary>
    /// Enum to indicate different dialog screens
    /// </summary>
    public enum DialogScreen
    {
        SyncMaintenance
    }
}