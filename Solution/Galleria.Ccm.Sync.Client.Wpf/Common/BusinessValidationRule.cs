﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Data;

namespace Galleria.Ccm.Sync.Client.Wpf.Common
{
    /// <summary>
    /// Validation rule for use when binding to model objects
    /// </summary>
    public class BusinessValidationRule : ValidationRule
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public BusinessValidationRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Fields
        //private ErrorMessage _errorMessage; // stores the current error message
        private UIScreen? _sourceScreen; // stores the source screen
        private DialogScreen? _sourceDialog; // stores the source dialog
        #endregion

        #region Properties
        /// <summary>
        /// The source screen
        /// </summary>
        public UIScreen? SourceScreen
        {
            get { return _sourceScreen; }
            set { _sourceScreen = value; }
        }

        /// <summary>
        /// The source dialog
        /// </summary>
        public DialogScreen? SourceDialog
        {
            get { return _sourceDialog; }
            set { _sourceDialog = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            // clear the current error message if we have one
            //if (_errorMessage != null)
            //{
            //    App.ViewState.ErrorService.RemoveError(_errorMessage);
            //    _errorMessage = null;
            //}

            // use the binding expression to locate the binding source
            // and property, then using the IDataErrorInfo interface
            // query the source to determine if any business rules
            // have been broken
            BindingExpression expression = value as BindingExpression;
            if (expression != null)
            {
                IDataErrorInfo sourceItem = expression.DataItem as IDataErrorInfo;
                if (sourceItem != null)
                {
                    String sourceProperty = expression.ParentBinding.Path.Path;
                    String errorContent = sourceItem[sourceProperty];
                    if (!String.IsNullOrEmpty(errorContent))
                    {
                        //get the language resource identifier
                        Type sourceType = expression.DataItem.GetType();

                        PropertyInfo friendlyNameField = sourceType.BaseType.GetProperty("FriendlyName", (BindingFlags.Static | BindingFlags.Public | BindingFlags.GetProperty));
                        object objectVal = null;
                        if (friendlyNameField != null)
                        {
                            objectVal = friendlyNameField.GetValue(expression.DataItem, null);
                        }
                        String propertyMessageHeader;
                        if (objectVal == null)
                        {
                            propertyMessageHeader = sourceType.Name;
                        }
                        else
                        {
                            propertyMessageHeader = (String)objectVal;
                        }

                        // generate our validation result
                        validationResult = new ValidationResult(false, errorContent);
                    }
                }
            }

            // and return the validation result
            return validationResult;
        }
        #endregion
    }
}