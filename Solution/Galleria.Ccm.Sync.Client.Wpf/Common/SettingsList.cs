﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-28215 : A.Probyn
//  Updated FileName to use Constants.AppDataFolderName for app data folder.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Diagnostics;

namespace Galleria.Ccm.Sync.Client.Wpf.Common
{
    /// <summary>
    /// An object that contains the information that is saved to/from the application's
    /// user-specific xml config file. This stores details of display language, central
    /// file backup location as well as the list of databases the user has connected to
    /// in previous sessions.
    /// </summary>
    [Serializable()]
    public class SettingsList //!Singleton pattern used
    {
        #region Fields
        //Lazy instantation of the SettingsList singleton
        private static readonly Lazy<SettingsList> lazySettings = new Lazy<SettingsList>(() => new SettingsList());
        private String _fileName = "CcmSyncUsers.xml"; // the default settings file name
        private String _displayLanguage = "en-gb"; // the user's language preference
        private Boolean _displayCEIPWindow = true; // The user selects the display behaviour of the customer enhancment improvment program
        private Boolean _sendCEIPInformation = true; // The user can choose if they want to send the gibraltar information back to Galleria
        private Boolean _unitTesting = false; // indicates if we are loading the application for unit testing purposes
        #endregion

        #region Properties

        /// <summary>
        /// Instance of Settings List (Singleton)
        /// </summary>
        public static SettingsList Instance
        {
            get
            {
                return lazySettings.Value;
            }
        }

        /// <summary>
        /// Returns the full name and path of the user file
        /// </summary>
        public String Filename
        {
            get
            {
                if (_unitTesting)
                {
                    return _fileName;
                }
                else
                {
                    return Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                        Constants.AppDataFolderName, _fileName);
                }
            }
        }

        /// <summary>
        /// The users language preference
        /// </summary>
        public String DisplayLanguage
        {
            get { return _displayLanguage; }
            set
            {
                _displayLanguage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The user selects the display behaviour of the customer enhancment improvment program
        /// </summary>
        public bool DisplayCEIPWindow
        {
            get { return _displayCEIPWindow; }
            set { _displayCEIPWindow = value; }
        }

        /// <summary>
        /// The user can choose if they want to send the gibraltar information back to Galleria
        /// </summary>
        public bool SendCEIPInformation
        {
            get { return _sendCEIPInformation; }
            set { _sendCEIPInformation = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Private constructor used with Lazy instantiation of SettingsList
        /// </summary>
        public SettingsList() { }

        /// <summary>
        /// Testing constructor
        /// </summary>
        public SettingsList(Boolean IsUnitTesting, String fileName)
        {
            _unitTesting = IsUnitTesting;
            _fileName = fileName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the current users settings list
        /// </summary>
        public void LoadSettingsList()
        {
            SettingsList settingsList;
            if (_unitTesting)
            {
                settingsList = new SettingsList(true, _fileName);
            }
            else
            {
                settingsList = new SettingsList();
            }
            try
            {
                Stream stream = File.Open(this.Filename, FileMode.Open);

                XmlSerializer x = new XmlSerializer(settingsList.GetType());
                settingsList = (SettingsList)x.Deserialize(stream);

                stream.Close();
            }
            catch
            {
                //user files doesnt exist
                settingsList.SaveSettings();
            }

            _displayLanguage = settingsList._displayLanguage;

            _displayCEIPWindow = settingsList._displayCEIPWindow;
            _sendCEIPInformation = settingsList._sendCEIPInformation;
        }

        /// <summary>
        /// Saves the current users settings list
        /// </summary>
        public void SaveSettings()
        {
            try
            {
                // ensure that the database folder exists
                Directory.CreateDirectory(Path.GetDirectoryName(this.Filename));

                Stream stream = File.Open(this.Filename, FileMode.Create);
                XmlSerializer x = new XmlSerializer(this.GetType());
                x.Serialize(stream, this);

                stream.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Resets the active current connection list
        /// </summary>
        public void ResetDefault()
        {
            this.DisplayCEIPWindow = true;
            this.SendCEIPInformation = true;

            this.SaveSettings();
        }

        #endregion

        #region Event Handlers

        private void OnPropertyChanged()
        {
            this.SaveSettings();
        }

        #endregion
    }
}