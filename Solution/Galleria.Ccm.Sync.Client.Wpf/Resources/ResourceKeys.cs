﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Sync.Client.Wpf.Resources
{
    /// <summary>
    /// Holds the resource keys for all resources 
    /// defined in the application resource dictionaries.
    /// </summary>
    public static class ResourceKeys
    {

        /// Used for image-text headers on window tab pages
        /// </summary>
        [Obsolete("Use TabPageHeader control instead")]
        public static readonly String StyTabPageHeader = "StyTabPageHeader";
        public static readonly String TmpTabPageHeader = "TmpTabPageHeader";

        /// <summary>
        /// Section content control used in window tab pages
        /// </summary>
        [Obsolete("Use ContentSectionHeader control instead")]
        public static readonly String StyTabPageSection = "StyTabPageSection";
        public static readonly String TmpTabPageSection = "TmpTabPageSection";

        /// <summary>
        /// ContentControl style - border and size definition for maintenance page link items displayed in the root window content.
        /// </summary>
        public static readonly String Main_StyPrimaryWindowMaintenanceItem = "Main_StyPrimaryWindowMaintenanceItem";

    }
}
