﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Reflection;
using System.Windows.Media;

namespace Galleria.Ccm.Sync.Client.Wpf.Resources
{
    /// <summary>
    /// Holds sttaic references to all image sources required for the solution.
    /// </summary>
    /// <remarks>Need to lazy load all the main resources</remarks>
    [SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Justification = "NamingConvention ok here.")]
    public static class ImageResources
    {
        #region Helper Methods
        /// <summary>
        /// Helper method to create a frozen bitmap image source
        /// This prevents issue where static sources can memory leak
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        private static BitmapImage CreateAsFrozen(String imagePath)
        {
            //Unit test requirement as the pack header is not registered until after the WPF application is loaded.
            //This code should never execute when the main app is run.
            if (!UriParser.IsKnownScheme("pack"))
            {
                System.Windows.Application app = new System.Windows.Application();
            }

            BitmapImage src = new BitmapImage(new Uri(imagePath, UriKind.RelativeOrAbsolute));
            src.Freeze();
            return src;
        }
        #endregion

        #region Icon Constant image Paths
        // The following commented out code works just fine when executing the client directly, but doesn't work
        // for the NUnit tests, because it causes an implicit call to Assembly.GetEntryAssembly(), which isn't
        // this assembly for the NUnit tests.
        //private const String _iconFolderPath = "pack://application:,,,/Resources/Icons/{0}";
        private static readonly String _iconFolderPath =
            String.Format(CultureInfo.InvariantCulture,
            "pack://application:,,,/{0};component/Resources/Icons/{{0}}",
            Assembly.GetExecutingAssembly().GetName().Name);

        private static readonly ImageSource _application_Icon =
            CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _iconFolderPath, "CCMIcon.ico"));


        #endregion

        #region Images Constant Image Paths
        // The following commented out code works just fine when executing the client directly, but doesn't work
        // for the NUnit tests, because it causes an implicit call to Assembly.GetEntryAssembly(), which isn't
        // this assembly for the NUnit tests.
        //private const String _imagesFolderPath = "pack://application:,,,/Resources/Images/{0}";
        private static readonly String _imagesFolderPath =
            String.Format(CultureInfo.InvariantCulture,
            "pack://application:,,,/{0};component/Resources/Images/{{0}}",
            Assembly.GetExecutingAssembly().GetName().Name);

        //Application
        private static readonly ImageSource _application_Splash = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "CCM-Splashscreen.png"));

        //Generic
        private static readonly ImageSource _generic_Browse_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Generic-Browse-16.png"));
        private static readonly ImageSource _generic_Open_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Generic-Open-16.png"));
        private static readonly ImageSource _generic_World_32 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Generic-World-32.png"));
        private static readonly ImageSource _generic_Options_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Generic-Options-16.png"));
        private static readonly ImageSource _generic_Options_32 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Generic-Options-32.png"));
        private static readonly ImageSource _generic_Exit_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Generic-Exit-16.png"));

        //Dialogs
        private static readonly ImageSource _dialog_Error_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Dialog-Error-24.png"));
        private static readonly ImageSource _dialog_Question_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Dialog-Question-24.png"));
        private static readonly ImageSource _dialog_Warning_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Dialog-Warning-24.png"));
        private static readonly ImageSource _dialog_Information_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Dialog-Info-24.png"));
        private static readonly ImageSource _dialog_SideBar = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Dialog-SideBar.png"));
        private static readonly ImageSource _dialog_TopBar = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Dialog-TopBar.png"));

        //Sync tool
        private static readonly ImageSource _sync_Database_32 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Sync-DB-32.png"));
        private static readonly ImageSource _sync_Add_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Sync-Add-16.png"));
        private static readonly ImageSource _sync_Remove_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Sync-Remove-16.png"));
        private static readonly ImageSource _sync_Now_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Sync-Now-16.png"));
        private static readonly ImageSource _sync_Start_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Sync-Start-16.png"));
        private static readonly ImageSource _sync_Stop_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Sync-Stop-16.png"));
        private static readonly ImageSource _sync_Reset_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Sync-Reset-16.png"));
        private static readonly ImageSource _saAboutScreen = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "CCM-AboutScreen.png"));
        private static readonly ImageSource _backstageCorner = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "BackstageCorner.png"));

        //Sync Maintenance Performance Source Config
        private static readonly ImageSource _syncPerformanceSourceDetails_datasheet_32 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Datasheet-32-Time.png"));

        #endregion

        #region Images

        //Application
        public static ImageSource Application_Icon { get { return _application_Icon; } }
        public static ImageSource Application_Splash { get { return _application_Splash; } }

        //Generic
        public static ImageSource Generic_Browse_16 { get { return _generic_Browse_16; } }
        public static ImageSource Generic_Open_16 { get { return _generic_Open_16; } }
        public static ImageSource Generic_World_32 { get { return _generic_World_32; } }
        public static ImageSource Generic_Options_16 { get { return _generic_Options_16; } }
        public static ImageSource Generic_Options_32 { get { return _generic_Options_32; } }
        public static ImageSource Generic_Exit { get { return _generic_Exit_16; } }

        //Dialogs
        public static ImageSource Dialog_Error_24 { get { return _dialog_Error_24; } }
        public static ImageSource Dialog_Question_24 { get { return _dialog_Question_24; } }
        public static ImageSource Dialog_Warning_24 { get { return _dialog_Warning_24; } }
        public static ImageSource Dialog_Information_24 { get { return _dialog_Information_24; } }
        public static ImageSource Dialog_SideBar { get { return _dialog_SideBar; } }
        public static ImageSource Dialog_TopBar { get { return _dialog_TopBar; } }

        //Sync tool
        public static ImageSource Sync_DB_32 { get { return _sync_Database_32; } }
        public static ImageSource Sync_Add_16 { get { return _sync_Add_16; } }
        public static ImageSource Sync_Remove_16 { get { return _sync_Remove_16; } }
        public static ImageSource Sync_Now_16 { get { return _sync_Now_16; } }
        public static ImageSource Sync_Start_16 { get { return _sync_Start_16; } }
        public static ImageSource Sync_Stop_16 { get { return _sync_Stop_16; } }
        public static ImageSource Sync_Reset_16 { get { return _sync_Reset_16; } }
        public static ImageSource SAAboutScreen { get { return _saAboutScreen; } }
        public static ImageSource BackstageCorner { get { return _backstageCorner; } }

        //Sync Maintenance Performance Source Config
        public static ImageSource SyncMaintenancePerformanceSourceConfig_PerformanceSource_32 { get { return _syncPerformanceSourceDetails_datasheet_32; } }

        public static ImageSource SyncMaintenancePerformanceSourceConfig_AddPerformanceSource { get { return _sync_Add_16; } }
        public static ImageSource SyncMaintenancePerformanceSourceConfig_RemoveSyncTargetPerformanceSource { get { return _sync_Remove_16; } }

        #endregion
    }
}
