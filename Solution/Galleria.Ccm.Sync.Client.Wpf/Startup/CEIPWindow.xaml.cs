﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Sync.Client.Wpf.Startup
{
    /// <summary>
    /// Interaction logic for CEIP.xaml
    /// </summary>
    public partial class CEIPWindow : ExtendedRibbonWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CEIPViewModel), typeof(CEIPWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the attached viewmodel
        /// </summary>
        public CEIPViewModel ViewModel
        {
            get { return (CEIPViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CEIPWindow senderControl = (CEIPWindow)obj;

            if (e.OldValue != null)
            {
                CEIPViewModel oldModel = (CEIPViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                CEIPViewModel newModel = (CEIPViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CEIPWindow(CEIPViewModel viewModel)
        {
            InitializeComponent();

            //Set the viewModel
            this.ViewModel = viewModel;
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            IDisposable disposableViewModel = this.ViewModel as IDisposable;
            this.ViewModel = null;

            if (disposableViewModel != null)
            {
                disposableViewModel.Dispose();
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the retry click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SaveUserSettings();
            this.DialogResult = true;
        }

        #endregion
    }
}
