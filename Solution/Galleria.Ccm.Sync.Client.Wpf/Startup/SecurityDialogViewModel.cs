﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media;
using Csla;
using Desaware.MachineLicense40;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Sync.Client.Wpf.Resources.Language;
using Galleria.Ccm.Sync.Client.Wpf.Resources;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Sync.Client.Wpf.Startup
{
    public class SecurityDialogViewModel : ViewModelAttachedControlObject<SecurityDialog> //Common.ViewModelBase<SecurityDialog>
    {
        public event EventHandler CloseWindow;

        #region Fields
        private DomainLicense _currentSecurity; //The current security settings
        private Boolean _isProgressBar;
        private Boolean _isDialogImage;
        private Boolean _isContinueButtonVisible;
        private Boolean _isReturnButtonVisible;
        private Boolean _isRetryButtonVisible;
        private Boolean _isCancelButtonVisible;
        private Boolean _isCancelReturnButtonVisible;
        private Boolean _isReturnButtonPressed;
        private Boolean _isValidationError;
        private BackgroundWorker _validateLicense;
        private ImageSource _dialogImage = ImageResources.Dialog_Error_24;
        private Boolean _isRelicenseVisible;
        #endregion

        #region Binding PropertyPaths

        public static PropertyPath securityDialogMessageProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.SecurityDialogMsg);
        public static PropertyPath securityDialogHeaderProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.SecurityDialogHeader);
        public static PropertyPath IsDialogImageProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsDialogImage);
        public static PropertyPath IsProgressBarProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsProgressBar);
        public static PropertyPath LoadDialogImageProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.LoadDialogImage);
        public static PropertyPath IsContinueButtonVisibleProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsContinueButtonVisible);
        public static PropertyPath IsReturnButtonVisibleProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsReturnButtonVisible);
        public static PropertyPath IsRetryButtonVisibleProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsRetryButtonVisible);
        public static PropertyPath IsCancelButtonVisibleProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsCancelButtonVisible);
        public static PropertyPath IsCancelReturnButtonVisibleProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsCancelReturnButtonVisible);
        public static PropertyPath IsRelicenseVisibleProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.IsRelicenseVisible);
        public static PropertyPath OpenLicenseScreenCommandProperty = WpfHelper.GetPropertyPath<SecurityDialogViewModel>(p => p.OpenLicenseScreenCommand);

        #endregion

        #region Properties
        /// <summary>
        /// The Security Dialog Description to the user
        /// </summary>
        public String SecurityDialogMsg
        {
            get { return _currentSecurity.securityDialogMessage; }
            set
            {
                _currentSecurity.securityDialogMessage = (value != String.Empty) ? value : null;
                OnPropertyChanged(securityDialogMessageProperty);
            }
        }

        /// <summary>
        /// The Security Dialog Header title
        /// </summary>
        public String SecurityDialogHeader
        {
            get { return _currentSecurity.securityDialogHeader; }
            set
            {
                _currentSecurity.securityDialogHeader = (value != String.Empty) ? value : null;
                OnPropertyChanged(securityDialogHeaderProperty);
            }
        }

        /// <summary>
        /// Sets the visibility of the Dialog Progress Bar
        /// </summary>
        public Boolean IsProgressBar
        {
            get { return _isProgressBar; }
            set
            {
                _isProgressBar = value;
                OnPropertyChanged(IsProgressBarProperty);
            }
        }

        /// <summary>
        /// Sets the visibility of the Dialog Image
        /// </summary>
        public Boolean IsDialogImage
        {
            get { return _isDialogImage; }
            set
            {
                _isDialogImage = value;
                OnPropertyChanged(IsDialogImageProperty);
            }
        }

        /// <summary>
        /// Sets the visibility of the Continue (Ok) Button
        /// </summary>
        public Boolean IsContinueButtonVisible
        {
            get { return _isContinueButtonVisible; }
            set
            {
                _isContinueButtonVisible = value;
                OnPropertyChanged(IsContinueButtonVisibleProperty);
            }
        }

        /// <summary>
        /// Sets the visibility of the return (Ok) button
        /// </summary>
        public Boolean IsReturnButtonVisible
        {
            get { return _isReturnButtonVisible; }
            set
            {
                _isReturnButtonVisible = value;
                OnPropertyChanged(IsReturnButtonVisibleProperty);
            }
        }

        /// <summary>
        /// Sets the visibility of the retry button
        /// </summary>
        public Boolean IsRetryButtonVisible
        {
            get { return _isRetryButtonVisible; }
            set
            {
                _isRetryButtonVisible = value;
                OnPropertyChanged(IsRetryButtonVisibleProperty);
            }
        }

        /// <summary>
        /// Sets the visibility of the cancel button
        /// </summary>
        public Boolean IsCancelButtonVisible
        {
            get { return _isCancelButtonVisible; }
            set
            {
                _isCancelButtonVisible = value;
                OnPropertyChanged(IsCancelButtonVisibleProperty);
            }
        }

        /// <summary>
        /// Sets the visibility of the cancel return button
        /// </summary>
        public Boolean IsCancelReturnButtonVisible
        {
            get { return _isCancelReturnButtonVisible; }
            set
            {
                _isCancelReturnButtonVisible = value;
                OnPropertyChanged(IsCancelReturnButtonVisibleProperty);
            }
        }

        /// <summary>
        /// Sets the Button Press State
        /// </summary>
        public Boolean IsReturnButtonPressed
        {
            get { return _isReturnButtonPressed; }
            set
            {
                _isReturnButtonPressed = value;
            }
        }

        /// <summary>
        /// Sets if there is a validation error on install
        /// </summary>
        public Boolean IsValidationError
        {
            get { return _isValidationError; }
            set
            {
                _isValidationError = value;
            }
        }

        /// <summary>
        /// Change the dialog image type
        /// </summary>
        public ImageSource LoadDialogImage
        {
            get { return _dialogImage; }
            set
            {
                _dialogImage = value;
                OnPropertyChanged(LoadDialogImageProperty);
            }
        }

        /// <summary>
        /// Gets/Sets if the relicense visible hyperlink should be available to the users
        /// </summary>
        public Boolean IsRelicenseVisible
        {
            get { return _isRelicenseVisible; }
            set
            {
                _isRelicenseVisible = value;
                OnPropertyChanged(IsRelicenseVisibleProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public SecurityDialogViewModel(DomainLicense License)
        {
            _currentSecurity = License;
        }

        #endregion

        #region Commands

        #region OpenLicenseScreenCommand

        private RelayCommand _openLicenseScreenCommand;

        /// <summary>
        /// Shows the licensing screen
        /// </summary>
        public RelayCommand OpenLicenseScreenCommand
        {
            get
            {
                if (_openLicenseScreenCommand == null)
                {
                    _openLicenseScreenCommand = new RelayCommand(
                        p => OpenLicenseScreen_Executed())
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openLicenseScreenCommand);
                }
                return _openLicenseScreenCommand;
            }
        }

        private void OpenLicenseScreen_Executed()
        {
            //Create and populate license
            DomainLicense securityLicense = new DomainLicense();
            securityLicense.LicenseInitalize();

            SecurityViewModel securityViewModel = new SecurityViewModel(securityLicense);
            SecurityDialogViewModel securityDialogViewModel = new SecurityDialogViewModel(securityLicense);
            SecurityWindow securityWindow = new SecurityWindow(securityViewModel);
            securityWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            securityViewModel.LoadRegistrationDetails();

            bool? _dialogResult = securityWindow.ShowDialog();

            if (_dialogResult == true)
            {
                _currentSecurity = securityLicense;
                LoadDialog();
                securityLicense.LicenseInstall();
                securityLicense.LicenseUpdate();
                App.LicenseState.Refresh(securityLicense);
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Validates if security dialog window required
        /// </summary>
        /// <returns>True if security dialog window required, else false</returns>
        public Boolean SecurityDialogWindowRequired()
        {
            Boolean returnVal = false;

            returnVal = !_currentSecurity.IsValidLicense();

            return returnVal;
        }

        /// <summary>
        /// Load all the information to be displayed on the security dialog window
        /// </summary>
        public void SecurityDialogWindowStartUp()
        {
            //Show the default Dialog title and description
            SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_Header);
            SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_Description);

            //Show the progress wheel and hide the image
            IsProgressBar = true;
            IsDialogImage = false;

            //Reset the return button to false
            IsReturnButtonPressed = false;

            //Reset the button visiability
            ResetDialogButtons();
        }


        /// <summary>
        /// This will look at the Validation Result and display the appropiate dialog box.
        /// </summary>
        /// <returns></returns>
        private void LicenseValidationResults()
        {
            //Reset The Validation Property
            IsValidationError = false;
            IsRelicenseVisible = false;

            //Swap the progress bar for the image
            IsProgressBar = false;
            IsDialogImage = true;

            // Set the header to warn the user there is a problem
            SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_ProblemHeader);

            switch (_currentSecurity.ValidationStatus)
            {
                case ValidationStatus.DemoExpired:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Warning_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_ExpiredHeader);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_DemoExpiredVerify);
                    break;

                case ValidationStatus.DemoNoInternetDate:
                    IsCancelButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_DemoNoInternetDate);
                    break;

                case ValidationStatus.IncorrectSystem:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_IncorrectSystem);
                    break;

                case ValidationStatus.InstallKeyDisabled:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_InstallKeyDisabled);
                    break;

                case ValidationStatus.InvalidCertificate:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_InvalidCertificate_Validate);
                    break;

                case ValidationStatus.MissingOrIncorrectApplication:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_MissingOrIncorrectApplication);
                    break;

                case ValidationStatus.SignatureVerificationError:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_SignatureVerificationError);
                    break;

                case ValidationStatus.SubscriptionExpired:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Warning_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_ExpiredHeader1);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_SubscriptionExpired);
                    break;

                case ValidationStatus.Success:
                case ValidationStatus.TempSuccess:
                    if (_currentSecurity.LicenseDaysRemaining() < 10)
                    {
                    IsContinueButtonVisible = true;
                    IsRelicenseVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Information_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_WarningHeader);

                    String Msg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_SubscriptionWarning);
                    Int32 daysRemaining = _currentSecurity.LicenseDaysRemaining();
                    SecurityDialogMsg = Msg.Replace("[days]", daysRemaining.ToString());
                    }
                    else
                    {
                        IsContinueButtonVisible = true;
                        IsRelicenseVisible = false;

                        LoadDialogImage = ImageResources.Dialog_Information_24;
                        SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.Application_Title_Ccm);
                        SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_NoError);
                    }
                    break;
            }
        }

        /// <summary>
        /// This will look at the Installation Result and display the appropiate dialog box.
        /// </summary>
        /// <returns></returns>
        private void LicenseInstallationResults()
        {
            //Reset The Validation Property
            IsValidationError = false;

            //Swap the progress bar for the image
            IsProgressBar = false;
            IsDialogImage = true;

            // Set the header to warn the user there is a problem
            SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_ProblemHeader);

            switch (_currentSecurity.InstallErrorResults)
            {
                case InstallErrorResults.CertSaveError:
                    IsCancelButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_CertSaveError);
                    break;

                case InstallErrorResults.CodeReuseBlocked:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Warning_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_CodeReuseBlocked);
                    break;

                case InstallErrorResults.CodeReuseWarning:
                    IsContinueButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Information_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.Application_Title);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_CodeReuseWarning);

                    if (_currentSecurity.ValidationStatus == ValidationStatus.InstallKeyDisabled || _currentSecurity.ValidationStatus == ValidationStatus.SubscriptionExpired)
                    {
                        IsValidationError = true;
                    }

                    break;

                case InstallErrorResults.DemoExpired:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_ProblemHeader1);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_DemoExpired);
                    break;

                case InstallErrorResults.DemoInstallOnRegisteredSystem:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.Application_Title);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_DemoOnRegisteredSystem);
                    break;

                case InstallErrorResults.DemoInstallOnPrevRegisteredSystem:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Warning_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_DemoOnPrevRegisteredSystem);
                    break;

                case InstallErrorResults.InstallKeyDisabled:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Warning_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_InstallKeyDisabled);
                    break;

                case InstallErrorResults.FatalError:
                case InstallErrorResults.InvalidCertificate:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_InvalidCertificate);
                    break;

                case InstallErrorResults.InvalidCode:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Warning_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_InvalidCode);
                    break;

                case InstallErrorResults.NoError:
                    IsContinueButtonVisible = true;
                    IsRelicenseVisible = false;
                    LoadDialogImage = ImageResources.Dialog_Information_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.Application_Title);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_NoError);
                    break;

                case InstallErrorResults.NoLicenseServers:
                case InstallErrorResults.DeferServerError:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.Application_Title);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_NoLicenseServers);
                    break;

                case InstallErrorResults.NoSuchApplication:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.Application_Title);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_NoSuchApplication);
                    break;

                case InstallErrorResults.ServerError:
                    IsRetryButtonVisible = true;
                    IsCancelReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Error_24;
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_ServerError);
                    break;

                case InstallErrorResults.SubscriptionExpired:
                    IsReturnButtonVisible = true;
                    LoadDialogImage = ImageResources.Dialog_Warning_24;
                    SecurityDialogHeader = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_ExpiredHeader1);
                    SecurityDialogMsg = String.Format(CultureInfo.CurrentCulture, Message.SecurityDialog_SubscriptionExpired);
                    break;

            }
        }

        /// <summary>
        /// Set the dialog buttons to invisible
        /// </summary>
        /// <returns></returns>
        private void ResetDialogButtons()
        {
            IsRetryButtonVisible = false;
            IsReturnButtonVisible = false;
            IsCancelButtonVisible = false;
            IsCancelReturnButtonVisible = false;
            IsContinueButtonVisible = false;
        }

        public void LoadDialog()
        {
            bool validateLicense = true;

            if (_currentSecurity.GetSecurityInstallType() == InstallType.Demo)
            {
                if (_currentSecurity.DemoVersion() == true && _currentSecurity.ValidationStatus != ValidationStatus.DemoExpired)
                {
                    validateLicense = false;
                }
            }

            if (validateLicense == true)
            {
                SecurityDialogWindowStartUp();
                ValidateLicense();
            }
            else
            {
                //Client already has demo license so carry on to next screen
                OnCloseWindow();
            }
        }

        private void ValidateLicense()
        {
            _validateLicense = new BackgroundWorker();
            _validateLicense.DoWork += new DoWorkEventHandler(ValidateLicense_DoWork);
            _validateLicense.RunWorkerAsync();
        }

        /// <summary>
        /// Validates the license on the user machine is valid or the installation operation is valid
        /// </summary>
        /// <returns></returns>
        void ValidateLicense_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (_currentSecurity.GetSecurityInstallType())
            {
                case InstallType.Demo:

                    if (_currentSecurity.DemoVersion() == false)
                    {
                        _currentSecurity.InstallErrorResults = _currentSecurity.DemoInstall();
                        LicenseInstallationResults();
                    }
                    else
                    {
                        if (_currentSecurity.ValidationStatus == ValidationStatus.DemoExpired)
                        {
                            LicenseValidationResults();
                        }
                    }

                    break;

                case InstallType.Full:

                    if (_currentSecurity.LicenseVerifyInstallationCode())
                    {
                        _currentSecurity.InstallErrorResults = _currentSecurity.LicenseInstall();
                    }

                    LicenseInstallationResults();
                    break;

                case InstallType.None:
                    LicenseValidationResults();
                    break;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Method for event to fire the closing of the window
        /// </summary>
        public void OnCloseWindow()
        {
            if (this.CloseWindow != null)
            {
                this.CloseWindow(this, EventArgs.Empty);
            }
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Clears up on dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }
}
