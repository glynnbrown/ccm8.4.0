﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Galleria.Ccm.Sync.Client.Wpf.Common;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Sync.Client.Wpf.Startup
{
    public enum ParticipateCEIP
    {
        Yes,
        No
    }

    public class CEIPViewModel : ViewModelAttachedControlObject<CEIPWindow>
    {
        #region Fields

        private SettingsList _currentSettings = SettingsList.Instance;

        private bool _isShowCEIPWindowNextTime;
        private ParticipateCEIP _participate = ParticipateCEIP.Yes;

        #endregion

        #region Binding PropertyPaths

        public static PropertyPath IsShowCEIPWindowNextTimeProperty = WpfHelper.GetPropertyPath<CEIPViewModel>(p => p.IsShowCEIPWindowNextTime);
        public static PropertyPath ParticipateProperty = WpfHelper.GetPropertyPath<CEIPViewModel>(p => p.Participate);

        #endregion

        #region properties

        /// <summary>
        /// Returns/Sets the user's choice for showing CEIP window next time.
        /// </summary>
        public bool IsShowCEIPWindowNextTime
        {
            get { return _isShowCEIPWindowNextTime; }
            set
            {
                _isShowCEIPWindowNextTime = value;
                OnPropertyChanged(IsShowCEIPWindowNextTimeProperty);
            }
        }

        /// <summary>
        /// Returns/Sets the user's choice for sending gibraltar information to Galleria.
        /// </summary>
        public ParticipateCEIP Participate
        {
            get { return _participate; }
            set
            {
                _participate = value;
                OnPropertyChanged(ParticipateProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public CEIPViewModel()
            : this(false)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="unitTesting">Indicates if we are loading this app for unit testing purposes</param>
        public CEIPViewModel(bool unitTesting)
        {
            if (!unitTesting)
            {

            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates if CEIP window required
        /// </summary>
        /// <returns>True if CEIP window required, else false</returns>
        public Boolean CEIPWindowRequired()
        {
            bool showCEIPWindow = false;

            _currentSettings.LoadSettingsList();

            showCEIPWindow = _currentSettings.DisplayCEIPWindow;

            // Load the CEIP window settings for gilbraltar
            CEIPWindowLoadSettings();

            return showCEIPWindow;
        }

        /// <summary>
        /// Validates if security window required
        /// </summary>
        /// <returns></returns>
        public void CEIPWindowLoadSettings()
        {
            if (_currentSettings.SendCEIPInformation == true)
            {
                Participate = ParticipateCEIP.Yes;
            }
            else
            {
                Participate = ParticipateCEIP.No;
            }
        }

        /// <summary>
        /// Save the user settings to the ISOUsers.xml file
        /// </summary>
        /// <returns></returns>
        public void SaveUserSettings()
        {
            _currentSettings.SendCEIPInformation = IsParticipate();

            _currentSettings.DisplayCEIPWindow = !IsShowCEIPWindowNextTime;

            _currentSettings.SaveSettings();
        }

        /// <summary>
        /// Save the user settings to the ISOUsers.xml file
        /// </summary>
        /// <returns></returns>
        public bool IsParticipate()
        {
            bool result = false;

            if (Participate == ParticipateCEIP.Yes)
            {
                result = true;
            }

            return result;
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Clears up on dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }
}
