﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Sync.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Input;

namespace Galleria.Ccm.Sync.Client.Wpf.Startup
{
    /// <summary>
    /// Interaction logic for SecurityWindow.xaml
    /// </summary>
    public partial class SecurityWindow : ExtendedRibbonWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SecurityViewModel), typeof(SecurityWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the attached viewmodel
        /// </summary>
        public SecurityViewModel ViewModel
        {
            get { return (SecurityViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SecurityWindow senderControl = (SecurityWindow)obj;

            if (e.OldValue != null)
            {
                SecurityViewModel oldModel = (SecurityViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                SecurityViewModel newModel = (SecurityViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public SecurityWindow(SecurityViewModel viewModel)
        {
            InitializeComponent();

            this.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(SecurityWindow_PreviewKeyDown);

            //Set the viewModel
            this.ViewModel = viewModel;
        }

        void SecurityWindow_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.V && Keyboard.IsKeyDown(Key.LeftCtrl))
            {

            }
            else if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                e.Handled = true;
            }
        }

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            IDisposable disposableViewModel = this.ViewModel as IDisposable;
            this.ViewModel = null;

            if (disposableViewModel != null)
            {
                disposableViewModel.Dispose();
            }
        }

        #endregion
    }
}