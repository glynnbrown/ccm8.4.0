﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Sync.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for SyncMaintenancePerformanceSourceConfig.xaml
    /// </summary>
    public partial class SyncMaintenancePerformanceSourceConfig : ExtendedRibbonWindow
    {
        #region Constants
        const String RemoveSyncTargetPerformanceSourceCommandKey = "RemoveSyncTargetPerformanceSourceCommand";
        #endregion

        #region View Model property
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SyncMaintenancePerformanceSourceConfigViewModel), typeof(SyncMaintenancePerformanceSourceConfig),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        /// <summary>
        /// Get/Set view model property
        /// </summary>
        public SyncMaintenancePerformanceSourceConfigViewModel ViewModel
        {
            get { return (SyncMaintenancePerformanceSourceConfigViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Event to handle view model property changes
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private static void ViewModel_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SyncMaintenancePerformanceSourceConfig senderControl = (SyncMaintenancePerformanceSourceConfig)obj;

            if (e.OldValue != null)
            {
                SyncMaintenancePerformanceSourceConfigViewModel oldModel = (SyncMaintenancePerformanceSourceConfigViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(RemoveSyncTargetPerformanceSourceCommandKey);
                oldModel.CloseWindow -= senderControl.ViewModel_CloseWindow;
            }

            if (e.NewValue != null)
            {
                SyncMaintenancePerformanceSourceConfigViewModel newModel = (SyncMaintenancePerformanceSourceConfigViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(RemoveSyncTargetPerformanceSourceCommandKey, newModel.RemoveSyncTargetPerformanceSourceCommand);
                newModel.CloseWindow += senderControl.ViewModel_CloseWindow;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        ///  Creates a new instance of this type
        /// </summary>

        public SyncMaintenancePerformanceSourceConfig(SyncTarget currentSyncTarget,
            GFSModel.PerformanceSourceList performanceSourcesPerEntity)
        {
            InitializeComponent();

            this.ViewModel = new SyncMaintenancePerformanceSourceConfigViewModel(currentSyncTarget, performanceSourcesPerEntity);
        }
        #endregion

        #region Events

        /// <summary>
        /// Subscribe to method to close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ViewModel_CloseWindow(object sender, EventArgs e)
        {
            //Close the window
            this.Close();
        }

        /// <summary>
        /// Applies changes to the view model and closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            //Close the window
            this.Close();
        }
        #endregion

        #region Window Close
        /// <summary>
        /// Clears down the control safely
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }
        #endregion
    }
}