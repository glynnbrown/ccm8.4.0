﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Sync.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MainPageOrganiser.xaml
    /// </summary>
    public partial class MainPageOrganiser : ExtendedRibbonWindow
    {
        #region View Model Property
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(MainPageOrganiser),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        /// <summary>
        /// Get/Set view model property
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Event to handle view model property changes
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private static void ViewModel_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainPageOrganiser senderControl = (MainPageOrganiser)obj;

            if (e.OldValue != null)
            {
                MainPageViewModel oldModel = (MainPageViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MainPageViewModel newModel = (MainPageViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        ///  Creates a new instance of this type
        /// </summary>
        /// <param name="Boolean">Is the product licenced</param>
        /// <param name="NotifyIcon">The notify Icon</param>
        public MainPageOrganiser()
        {
            InitializeComponent();
            this.ViewModel = new MainPageViewModel();
        }
        #endregion

        #region Events
        /// <summary>
        /// Called when the window state is changed
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void ExtendedRibbonWindow_StateChanged(object sender, EventArgs e)
        {
            this.ViewModel.RefreshSyncTargetsCommand.Execute();
        }

        /// <summary>
        /// Called when a row has been double clicked
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.ViewSyncTargetCommand.Execute();
        }

        /// <summary>
        /// Called when the web service address link is pressed
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void cmdOpenWebServiceAddress_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            String urlString = this.ViewModel.SyncSourceAddress;

            System.Diagnostics.Process.Start(urlString);
        }
        #endregion

        #region Window Close
        /// <summary>
        /// Prevent the main window from being closed from the command window
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            this.WindowState = WindowState.Minimized;

            base.OnClosing(e);
            e.Cancel = true;
        }
        #endregion

    }
}