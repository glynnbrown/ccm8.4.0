﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// CCM-26486 : J.Pickup
//  Gracefully handles exception when GetAllSyncSources() creates exception in constructor.
// CCM-26510 : J.Pickup
//  Handles the user running the application as another user and the associated permission error.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.ServiceProcess;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Csla;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Sync.Client.Wpf.Resources;
using Galleria.Ccm.Sync.Client.Wpf.Resources.Language;
using Galleria.Ccm.Sync.Client.Wpf.Startup;

namespace Galleria.Ccm.Sync.Client.Wpf.PrimaryWindow
{
    public class MainPageViewModel : ViewModelAttachedControlObject<MainPageOrganiser>
    {
        #region Constants
        private const String _serviceName = "CCM Sync Service"; // the name of the CCM sync service
        private const Int32 _serviceWaitTimeoutSecs = 30; // the CCM sync service status wait timeout
        #endregion

        #region Fields
        //Application
        private LicenseState _state;

        //Service
        private String _serviceServerName; // name of the CCM sync service server
        private String _serviceStatus; // service status
        private String _serviceStatusLastRecorded; // service status that was last recorded
        private Boolean _isServiceRunning = false; // is the service running
        private Boolean _isServiceStopped = false; // is the service stopped
        private Int32 _defaultPortNumber = 80; // 80 Otion to set a default port number for the GFS web service
        private String _defaultServerName = string.Empty; // "GFSServices" Option to set a default server name for the GFS web service
        private String _defaultSiteName = string.Empty; // "GFSService" Option to set a default site name for the GFS web service

        //Source
        private SyncSourceList _syncSources; // holds the sync sources
        private SyncSource _syncSourceSelected; // selected sync source        
        private Boolean _isSyncSourceConnected = false; // do we have a connection to the sync source
        private String _syncSourceAddress; // sync source web service address
        private String _syncSourceResponse; // sync source connection response

        //Target
        private SyncTargetList _syncTargets; // holds the sync targets
        private SyncTargetRowViewModel _syncTargetSelected; //selected sync Target
        private SyncTarget _syncTargetCurrent; //current sync Target
        private BulkObservableCollection<SyncTargetRowViewModel> _syncTargetRows = new BulkObservableCollection<SyncTargetRowViewModel>(); // Holds the sync targets with additional information including entity name  

        //Busy
        private Boolean _isbusyProgress = false; // show or hide the busy progress
        private String _busyProgressDescription; // what is our busy progress description
        private BackgroundWorker _backgroundWorker; // background worker

        // User
        private Boolean _windowsAdministratorUser = false; // determine if the user has windows administrator privileges
        private String _buildVersion = String.Empty;
        #endregion

        #region Binding Property Paths
        public static PropertyPath IsBusyProgressProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.IsBusyProgress);
        public static PropertyPath BusyProgressDescriptionProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.BusyProgressDescription);

        public static PropertyPath ServiceStatusProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ServiceStatus);
        public static PropertyPath ServiceStatusLastRecordedProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ServiceStatusLastRecorded);
        public static PropertyPath IsServiceRunningProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.IsServiceRunning);
        public static PropertyPath IsServiceStoppedProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.IsServiceStopped);

        public static PropertyPath SyncSourceSelectedProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncSourceSelected);
        public static PropertyPath SyncSourceAddressProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncSourceAddress);
        public static PropertyPath SyncSourceResponseProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncSourceResponse);
        public static PropertyPath IsSyncSourceConnectedProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.IsSyncSourceConnected);

        public static PropertyPath SyncTargetsProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncTargets);
        public static PropertyPath SyncTargetSelectedProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncTargetSelected);
        public static PropertyPath SyncTargetCurrentProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncTargetCurrent);
        public static PropertyPath SyncTargetRowsProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncTargetRows);
        public static readonly PropertyPath BuildVersionProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.BuildVersion);
        public static readonly PropertyPath StateProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.State);

        public static PropertyPath StartServiceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.StartServiceCommand);
        public static PropertyPath StopServiceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.StopServiceCommand);
        public static PropertyPath EstablishWebServiceAddressCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.EstablishWebServiceAddressCommand);
        public static PropertyPath RefreshSyncTargetsCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.RefreshSyncTargetsCommand);
        public static PropertyPath AddSyncTargetCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.AddSyncTargetCommand);
        public static PropertyPath RemoveSyncTargetCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.RemoveSyncTargetCommand);
        public static PropertyPath ResetSyncTargetCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ResetSyncTargetCommand);
        public static PropertyPath ViewSyncTargetCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ViewSyncTargetCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.CloseCommand);
        public static PropertyPath ResetLastSyncCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ResetLastSyncCommand);
        public static readonly PropertyPath OpenLicenseScreenCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.OpenLicenseScreenCommand);
        #endregion

        #region Properties

        #region Sync tool

        /// <summary>
        /// Gets/Sets the visibility of busy progress
        /// </summary>        
        public Boolean IsBusyProgress
        {
            get { return _isbusyProgress; }
            set
            {
                _isbusyProgress = value;
                OnPropertyChanged(IsBusyProgressProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the busy progress description
        /// </summary>
        public String BusyProgressDescription
        {
            get { return _busyProgressDescription; }
            set
            {
                _busyProgressDescription = value;
                OnPropertyChanged(BusyProgressDescriptionProperty);
            }
        }

        #endregion

        #region Sync Source
        /// <summary>
        /// Gets/Sets the sync source address
        /// </summary>
        public String SyncSourceResponse
        {
            get { return _syncSourceResponse; }
            set
            {
                _syncSourceResponse = value;
                OnPropertyChanged(SyncSourceResponseProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the sync source address
        /// </summary>
        public String SyncSourceAddress
        {
            get { return _syncSourceAddress; }
            set
            {
                _syncSourceAddress = value;
                OnPropertyChanged(SyncSourceAddressProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the sync source
        /// </summary>
        public SyncSource SyncSourceSelected
        {
            get { return _syncSourceSelected; }
            private set
            {
                SyncSource oldModel = _syncSourceSelected;
                _syncSourceSelected = value;

                OnSelectedSyncSourceChanged(oldModel, value);
                OnPropertyChanged(SyncSourceSelectedProperty);
            }
        }

        /// <summary>
        /// Selected sync source property changed
        /// </summary>
        void _syncSourceSelected_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets/Sets the sync source connection
        /// </summary>        
        public Boolean IsSyncSourceConnected
        {
            get { return _isSyncSourceConnected; }
            set
            {
                _isSyncSourceConnected = value;
                OnPropertyChanged(IsSyncSourceConnectedProperty);
            }
        }
        #endregion

        #region Sync Target
        /// <summary>
        /// Gets/Sets the selected sync target
        /// </summary>
        public SyncTargetRowViewModel SyncTargetSelected
        {
            get { return _syncTargetSelected; }
            set
            {
                _syncTargetSelected = value;
                OnPropertyChanged(SyncTargetSelectedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the sync targets
        /// </summary>
        public SyncTargetList SyncTargets
        {
            get { return _syncTargets; }
            private set
            {
                _syncTargets = value;
                OnPropertyChanged(SyncTargetsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the current sync target
        /// </summary>
        public SyncTarget SyncTargetCurrent
        {
            get { return _syncTargetCurrent; }
            set
            {
                _syncTargetCurrent = value;
                OnPropertyChanged(SyncTargetCurrentProperty);
            }
        }

        /// <summary>
        /// Gets the collection of sync target row records
        /// </summary>
        public BulkObservableCollection<SyncTargetRowViewModel> SyncTargetRows
        {
            get { return _syncTargetRows; }
        }
        #endregion

        #region Service
        /// <summary>
        /// Gets/Sets the service status
        /// </summary>
        public String ServiceStatus
        {
            get { return _serviceStatus; }
            set
            {
                _serviceStatus = value;
                OnPropertyChanged(ServiceStatusProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the service status last recorded
        /// </summary>
        public String ServiceStatusLastRecorded
        {
            get { return _serviceStatusLastRecorded; }
            set
            {
                _serviceStatusLastRecorded = value;
                OnPropertyChanged(ServiceStatusLastRecordedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the service running state
        /// </summary>        
        public Boolean IsServiceRunning
        {
            get { return _isServiceRunning; }
            set
            {
                _isServiceRunning = value;
                OnPropertyChanged(IsServiceRunningProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the service stopped state
        /// </summary>        
        public Boolean IsServiceStopped
        {
            get { return _isServiceStopped; }
            set
            {
                _isServiceStopped = value;
                OnPropertyChanged(IsServiceStoppedProperty);
            }
        }
        #endregion

        /// <summary>
        /// The version of the current build
        /// </summary>
        public String BuildVersion
        {
            get { return _buildVersion; }
            private set
            {
                _buildVersion = value;
                OnPropertyChanged(BuildVersionProperty);
            }
        }

        /// <summary>
        /// Returns the license state object
        /// </summary>
        public LicenseState State
        {
            get { return _state; }
        }

        #endregion

        #region Events
        /// <summary>
        /// Delclaration of CloseWindow event
        /// </summary>
        public event EventHandler CloseWindow;
        /// <summary>
        /// Method for event to fire
        /// </summary>
        public void OnCloseWindow()
        {
            if (this.CloseWindow != null)
            {
                this.CloseWindow(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public MainPageViewModel()
        {
            // Use the current machine name as the service server name
            _serviceServerName = System.Environment.MachineName;

            //Do we have a any sync sources
            try
            {
                this._syncSources = SyncSourceList.GetAllSyncSources();
            }
            catch (Exception ex)
            {
                String exceptionMessage = ex.Message;

                //SA-26502 : If is a FileFormatException then output friendly message about 'permissions' as is likely being run
                //under a user that does not have access to the sync db directory when creating file.
                if (!(ex.GetBaseException() is System.IO.FileFormatException))
                {
                    exceptionMessage = Message.SyncMainPageViewModel_DataPortalException_FriendlyPermissions;
                }

                Galleria.Framework.Controls.Wpf.ModalMessage dialog =
                        new Galleria.Framework.Controls.Wpf.ModalMessage()
                        {
                            Title = Message.Application_Title_Ccm,
                            Header = Message.SyncMainPageViewModel_ErrorHeader,
                            Description = exceptionMessage,
                            ButtonCount = 1,
                            Button1Content = Message.Generic_Ok,
                            DefaultButton = Galleria.Framework.Controls.Wpf.ModalMessageButton.Button1,
                            CancelButton = Galleria.Framework.Controls.Wpf.ModalMessageButton.Button1,
                            MessageIcon = Galleria.Ccm.Sync.Client.Wpf.Resources.ImageResources.Dialog_Error_24
                        };

                dialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                dialog.Icon = ImageResources.Application_Icon;

                dialog.ShowDialog();

                //If exception occcurs we need to call for a shutdown
                System.Environment.Exit(1);
            }

            if (this._syncSources.Count == 0)
            {
                //Create a new sync source
                this.SyncSourceSelected = SyncSource.NewSyncSource();
                this.SyncSourceSelected.ServerName = _defaultServerName;
                this.SyncSourceSelected.SiteName = _defaultSiteName;
                this.SyncSourceSelected.PortNumber = _defaultPortNumber;
            }
            else
            {
                //Set the first sync source as the current sync source
                this.SyncSourceSelected = Model.SyncSource.GetSyncSourceById(this._syncSources.FirstOrDefault().Id);

                //Establish sync source connection
                if (this.EstablishWebServiceAddressCommand.CanExecute())
                {
                    this.EstablishWebServiceAddressCommand.Execute();
                }
            }

            //Populate the Sync Source Address
            SyncSourceAddressChanged();

            //windows administrator user
            _windowsAdministratorUser = UserHasWindowsAdministratorRolePrivileges();

            this.BuildVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            _state = App.LicenseState;
        }

        #endregion

        #region Commands

        #region Start Service
        /// <summary>
        /// Start service command field
        /// </summary>
        private RelayCommand _startServiceCommand;

        /// <summary>
        /// Returns the start service
        /// </summary>
        public RelayCommand StartServiceCommand
        {
            get
            {
                if (_startServiceCommand == null)
                {
                    _startServiceCommand =
                        new RelayCommand(p => StartService_Executed(), p => StartService_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Service_Start,
                            FriendlyDescription = Message.SyncMaintenance_Service_Start_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Service_Start_Disabled,
                            Icon = ImageResources.Sync_Start_16
                        };
                    this.ViewModelCommands.Add(_startServiceCommand);
                }
                return _startServiceCommand;
            }
        }

        /// <summary>
        /// Called when determining if the start service command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private Boolean StartService_CanExecute()
        {
            return _windowsAdministratorUser;
        }

        /// <summary>
        /// Called when the start service command is executed
        /// </summary>
        private void StartService_Executed()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(serviceStartWorker_DoWork);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(serviceStartWorker_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync();
        }
        #endregion

        #region Stop Service
        /// <summary>
        /// Stop service command field
        /// </summary>
        private RelayCommand _stopServiceCommand;

        /// <summary>
        /// Returns the stop service
        /// </summary>
        public RelayCommand StopServiceCommand
        {
            get
            {
                if (_stopServiceCommand == null)
                {
                    _stopServiceCommand =
                        new RelayCommand(p => StopService_Executed(), p => StopService_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Service_Stop,
                            FriendlyDescription = Message.SyncMaintenance_Service_Stop_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Service_Stop_Disabled,
                            Icon = ImageResources.Sync_Stop_16
                        };
                    this.ViewModelCommands.Add(_stopServiceCommand);
                }
                return _stopServiceCommand;
            }
        }

        /// <summary>
        /// Called when determining if the stop service command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private Boolean StopService_CanExecute()
        {
            return _windowsAdministratorUser;
        }

        /// <summary>
        /// Called when the stop service command is executed
        /// </summary>
        private void StopService_Executed()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(serviceStopWorker_DoWork);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(serviceStopWorker_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync();
        }
        #endregion

        #region Establish Web Service Address
        /// <summary>
        /// Establish web service address command field
        /// </summary>
        private RelayCommand _establishWebServiceAddressCommand;

        /// <summary>
        /// Returns the established web serice address
        /// </summary>
        public RelayCommand EstablishWebServiceAddressCommand
        {
            get
            {
                if (_establishWebServiceAddressCommand == null)
                {
                    _establishWebServiceAddressCommand =
                        new RelayCommand(p => EstablishWebServiceAddress_Executed(), p => EstablishWebServiceAddress_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Source_Address_Establish,
                            FriendlyDescription = Message.SyncMaintenance_Source_Address_Establish_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Source_Address_Establish_Disabled
                        };
                    this.ViewModelCommands.Add(_establishWebServiceAddressCommand);
                }
                return _establishWebServiceAddressCommand;
            }
        }

        /// <summary>
        /// Called when determining if the established web service command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private Boolean EstablishWebServiceAddress_CanExecute()
        {
            //user must have entered a sync source server name
            if (String.IsNullOrEmpty(this.SyncSourceSelected.ServerName))
            {
                _establishWebServiceAddressCommand.DisabledReason = Message.SyncMaintenance_Source_Address_Establish_Disabled_Server;
                return false;
            }

            //user must enter a sync source port number that is not equal to zero
            if (Int32.Equals(this.SyncSourceSelected.PortNumber, 0))
            {
                _establishWebServiceAddressCommand.DisabledReason = Message.SyncMaintenance_Source_Address_Establish_Disabled_Port;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called when the established web service command is executed
        /// </summary>
        private void EstablishWebServiceAddress_Executed()
        {
            //Perform the web service connection check and save the sync source
            if (this.AttachedControl != null)
            {
                _backgroundWorker = new BackgroundWorker();
                _backgroundWorker.DoWork += new DoWorkEventHandler(establishWebServiceWorker_DoWork);
                _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(establishWebServiceWorker_RunWorkerCompleted);
                _backgroundWorker.RunWorkerAsync();
            }
            else
            {
                SaveSyncSourceWebService(EstablishWebServiceConnection());
            }
        }
        #endregion

        #region Refresh Sync Targets
        /// <summary>
        /// The refresh sync targets command field
        /// </summary>
        private RelayCommand _refreshSyncTargetsCommand;

        /// <summary>
        /// Returns the refresh sync targets command
        /// </summary>
        public RelayCommand RefreshSyncTargetsCommand
        {
            get
            {
                if (_refreshSyncTargetsCommand == null)
                {
                    _refreshSyncTargetsCommand =
                        new RelayCommand(p => RefreshSyncTargets_Executed(), p => RefreshSyncTargets_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Target_Refresh,
                            FriendlyDescription = Message.SyncMaintenance_Target_Refresh_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Target_Refresh_Disabled,
                        };
                    this.ViewModelCommands.Add(_refreshSyncTargetsCommand);
                }
                return _refreshSyncTargetsCommand;
            }
        }

        /// <summary>
        /// Called when determining if the refresh sync targets command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private Boolean RefreshSyncTargets_CanExecute()
        {
            return true;
        }

        /// <summary>
        /// Called when the refresh sync targets command is executed
        /// </summary>
        private void RefreshSyncTargets_Executed()
        {
            try
            {
                //Take copy of selected sync target
                SyncTargetRowViewModel oldSelectedSyncTarget = this.SyncTargetSelected;

                this.SyncTargets = SyncTargetList.GetAllSyncTargets(true);

                // Loop through each sync target and populate the grid
                foreach (SyncTargetRowViewModel syncTargetRow in this._syncTargetRows)
                {
                    syncTargetRow.SyncTarget.PropertyChanged -= SelectedSyncTarget_PropertyChanged;
                    syncTargetRow.Dispose();
                }

                // Clear all rows from the collection before re-populating
                this._syncTargetRows.Clear();

                // Loop through each sync target and populate the grid
                foreach (SyncTarget row in this.SyncTargets)
                {
                    SyncTargetRowViewModel rowData = new SyncTargetRowViewModel(row);
                    rowData.SyncTarget.PropertyChanged += new PropertyChangedEventHandler(SelectedSyncTarget_PropertyChanged);
                    this._syncTargetRows.Add(rowData);
                }

                if (oldSelectedSyncTarget != null)
                {
                    //Try and reselect sync target
                    this.SyncTargetSelected = this.SyncTargetRows.FirstOrDefault(p => p.SyncTarget.ConnectionString == oldSelectedSyncTarget.SyncTarget.ConnectionString);
                }
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }
        }

        /// <summary>
        /// Called when the sync target property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SelectedSyncTarget_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == SyncTarget.IsActiveProperty.Name)
            {
                try
                {
                    //Cast as sync target
                    SyncTarget syncTargetObject = (SyncTarget)sender;
                    //Set the current sync target
                    this.SyncTargetCurrent = SyncTarget.GetSyncTargetById(syncTargetObject.Id);
                    //Set the current sync target active flag
                    this.SyncTargetCurrent.IsActive = syncTargetObject.IsActive;
                    //Set that the sync target owned by the UI
                    this.SyncTargetCurrent.UISource = true;
                    //Save the changes
                    this.SyncTargetCurrent = this.SyncTargetCurrent.Save();
                    // refresh the sync targets
                    this.RefreshSyncTargetsCommand.Execute();
                }
                catch (DataPortalException ex)
                {
                    //Do nothing, should re-poll and update

                    //log to gfs and Windows event log
                    GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
                }
            }
        }

        #endregion

        #region Add Sync Target
        /// <summary>
        /// The add sync target command field
        /// </summary>
        private RelayCommand _addSyncTargetCommand;

        /// <summary>
        /// Adds a new sync target and opens the edit window
        /// </summary>
        public RelayCommand AddSyncTargetCommand
        {
            get
            {
                if (_addSyncTargetCommand == null)
                {
                    _addSyncTargetCommand =
                        new RelayCommand(p => AddSyncTarget_Executed(), p => AddSyncTarget_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Target_Add,
                            FriendlyDescription = Message.SyncMaintenance_Target_Add_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Target_Add_Disabled,
                            Icon = ImageResources.Sync_Add_16
                        };
                    this.ViewModelCommands.Add(_addSyncTargetCommand);
                }
                return _addSyncTargetCommand;
            }
        }

        /// <summary>
        /// Called when determining if an add sync target command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private Boolean AddSyncTarget_CanExecute()
        {
            return true;
        }

        /// <summary>
        /// Called when the add sync target command is executed
        /// </summary>
        private void AddSyncTarget_Executed()
        {
            //If view model is attached allowing show dialog method
            if (this.AttachedControl != null)
            {
                //Show dialog
                SyncMaintenanceWindow window = new SyncMaintenanceWindow();
                window.Owner = App.Current.MainWindow;
                window.ShowDialog();

                // refresh the sync targets
                this.RefreshSyncTargetsCommand.Execute();
            }
        }
        #endregion

        #region Remove Sync Target
        /// <summary>
        /// The remove sync target command field
        /// </summary>
        private RelayCommand _removeSyncTargetCommand;

        /// <summary>
        /// Remove sync target from the global sync targets list
        /// </summary>
        public RelayCommand RemoveSyncTargetCommand
        {
            get
            {
                if (_removeSyncTargetCommand == null)
                {
                    _removeSyncTargetCommand =
                        new RelayCommand(p => RemoveSyncTarget_Executed(), p => RemoveSyncTarget_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Target_Remove,
                            FriendlyDescription = Message.SyncMaintenance_Target_Remove_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Target_Remove_Disabled,
                            Icon = ImageResources.Sync_Remove_16
                        };
                    this.ViewModelCommands.Add(_removeSyncTargetCommand);
                }
                return _removeSyncTargetCommand;
            }
        }

        /// <summary>
        /// Called when determining if a remove sync target command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private bool RemoveSyncTarget_CanExecute()
        {
            bool returnVal = true;

            if (returnVal)
            {
                //Sync target must be selected
                returnVal = (this.SyncTargetSelected != null);
                if (!returnVal)
                {
                    this.RemoveSyncTargetCommand.DisabledReason = Message.SyncMaintenance_Target_Remove_Disabled;
                }
            }

            if (returnVal)
            {
                //Sync target cannot be running
                returnVal = this.SyncTargetSelected.SyncTarget.Status != SyncTargetStatus.Running;
                if (!returnVal)
                {
                    this.RemoveSyncTargetCommand.DisabledReason = String.Format(Message.SyncMaintenance_Target_Remove_DisabledRunning, SyncTargetStatus.Running);
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Called when the remove sync target command is executed
        /// </summary>
        private void RemoveSyncTarget_Executed()
        {
            Boolean deleteConfirmed = true;
            String deleteFriendlyName = String.Empty;
            String serverName;
            String databaseName;
            String[] connectionDetails;
            String[] connectDetails;

            try
            {
                //Set the current sync target
                this.SyncTargetCurrent = SyncTarget.GetSyncTargetById(this.SyncTargetSelected.SyncTarget.Id);

                switch (this.SyncTargetCurrent.TargetType)
                {
                    //case SyncTargetType.VistaDb:
                    //    //Set the delete item name
                    //    deleteFriendlyName = this.SyncTargetCurrent.ConnectionString.Replace("File=", String.Empty);
                    //    break;
                    case SyncTargetType.Mssql:
                        //Main Connection String
                        connectionDetails = this.SyncTargetCurrent.ConnectionString.ToString().Split((";").ToCharArray());

                        //Server String
                        connectDetails = connectionDetails[0].ToString().Split(("=").ToCharArray());
                        serverName = connectDetails[1];

                        //Database String
                        connectDetails = connectionDetails[1].ToString().Split(("=").ToCharArray());
                        databaseName = connectDetails[1];

                        //Set the delete item name
                        deleteFriendlyName = serverName + " : " + databaseName;
                        break;
                }

                if (this.AttachedControl != null)
                {
                    //Confirm delete with user
                    Framework.Controls.Wpf.ModalMessage win = new Framework.Controls.Wpf.ModalMessage();
                    win.Title = Message.Application_Title + " - " + Message.Application_Title_Ccm;
                    win.Header = deleteFriendlyName;
                    win.Description = Message.Generic_Delete_Confirmation;
                    win.ButtonCount = 2;
                    win.Button1Content = Message.Generic_Delete;
                    win.Button2Content = Message.Generic_Cancel;
                    win.MessageIcon = ImageResources.Dialog_Warning_24;
                    win.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    win.Owner = this.AttachedControl != null ? this.AttachedControl : Application.Current.MainWindow;
                    win.ShowDialog();

                    //set the result
                    deleteConfirmed = (win.Result == Framework.Controls.Wpf.ModalMessageResult.Button1);
                }

                //confirm with user
                if (deleteConfirmed)
                {
                    //Delete the current sync target
                    this.SyncTargetCurrent.Delete();

                    //Save the changes
                    this.SyncTargetCurrent.Save();

                    // refresh the sync targets
                    this.RefreshSyncTargetsCommand.Execute();
                }
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }
        }
        #endregion

        #region Reset Sync Target
        /// <summary>
        /// The reset sync target command field
        /// </summary>
        private RelayCommand _resetSyncTargetCommand;

        /// <summary>
        /// Reset sync target from the global sync targets list
        /// </summary>
        public RelayCommand ResetSyncTargetCommand
        {
            get
            {
                if (_resetSyncTargetCommand == null)
                {
                    _resetSyncTargetCommand =
                        new RelayCommand(p => ResetSyncTarget_Executed(), p => ResetSyncTarget_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Target_Reset,
                            FriendlyDescription = Message.SyncMaintenance_Target_Reset_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Target_Reset_Disabled,
                            Icon = ImageResources.Sync_Now_16,
                        };
                    this.ViewModelCommands.Add(_resetSyncTargetCommand);
                }
                return _resetSyncTargetCommand;
            }
        }

        /// <summary>
        /// Called when determining if a reset sync target command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private bool ResetSyncTarget_CanExecute()
        {
            bool returnVal = true;

            if (returnVal)
            {
                //Sync target must be selected
                returnVal = (this.SyncTargetSelected != null);
                if (!returnVal)
                {
                    this.ResetSyncTargetCommand.DisabledReason = Message.SyncMaintenance_Target_Reset_Disabled;
                }
            }

            if (returnVal)
            {
                //Sync target cannot be running
                returnVal = this.SyncTargetSelected.SyncTarget.Status != SyncTargetStatus.Running;
                if (!returnVal)
                {
                    this.ResetSyncTargetCommand.DisabledReason = String.Format(Message.SyncMaintenance_Target_Reset_DisabledRunning, SyncTargetStatus.Running);
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Called when the reset sync target command is executed
        /// </summary>
        private void ResetSyncTarget_Executed()
        {
            Boolean resetConfirmed = true;
            String resetFriendlyName = String.Empty;
            String serverName;
            String databaseName;
            String[] connectionDetails;
            String[] connectDetails;

            try
            {
                //Set the current sync target
                this.SyncTargetCurrent = SyncTarget.GetSyncTargetById(this.SyncTargetSelected.SyncTarget.Id);

                switch (this.SyncTargetCurrent.TargetType)
                {
                    case SyncTargetType.Mssql:
                        //Main Connection String
                        connectionDetails = this.SyncTargetCurrent.ConnectionString.ToString().Split((";").ToCharArray());

                        //Server String
                        connectDetails = connectionDetails[0].ToString().Split(("=").ToCharArray());
                        serverName = connectDetails[1];

                        //Database String
                        connectDetails = connectionDetails[1].ToString().Split(("=").ToCharArray());
                        databaseName = connectDetails[1];

                        //Set the delete item name
                        resetFriendlyName = serverName + " : " + databaseName;
                        break;
                }

                if (this.AttachedControl != null)
                {
                    //Confirm delete with user
                    Framework.Controls.Wpf.ModalMessage win = new Framework.Controls.Wpf.ModalMessage();
                    win.Title = Message.Application_Title + " - " + Message.Application_Title_Ccm;
                    win.Header = resetFriendlyName;
                    win.Description = Message.Generic_SyncNow_Confirmation;
                    win.ButtonCount = 2;
                    win.Button1Content = Message.Generic_SyncNow;
                    win.Button2Content = Message.Generic_Cancel;
                    win.MessageIcon = ImageResources.Dialog_Information_24;
                    win.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    win.Owner = this.AttachedControl != null ? this.AttachedControl : Application.Current.MainWindow;
                    win.ShowDialog();

                    //set the result
                    resetConfirmed = (win.Result == Framework.Controls.Wpf.ModalMessageResult.Button1);
                }

                //confirm with user
                if (resetConfirmed)
                {
                    //Set the last run to null
                    this.SyncTargetCurrent.LastRun = null;
                    //Set the retry count to zero
                    this.SyncTargetCurrent.RetryCount = 0;
                    //Set the failed count to zero
                    this.SyncTargetCurrent.FailedCount = 0;
                    //Set the status to be Queued
                    this.SyncTargetCurrent.Status = SyncTargetStatus.Queued;
                    //Set the error to be none
                    this.SyncTargetCurrent.Error = SyncTargetError.None;
                    //Save the changes
                    this.SyncTargetCurrent = this.SyncTargetCurrent.Save();

                    // refresh the sync targets
                    this.RefreshSyncTargetsCommand.Execute();
                }
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }
        }
        #endregion

        #region Reset Last Sync
        /// <summary>
        /// The reset last sync command field
        /// </summary>
        private RelayCommand _resetLastSyncCommand;

        /// <summary>
        /// Reset sync target from the global sync targets list
        /// </summary>
        public RelayCommand ResetLastSyncCommand
        {
            get
            {
                if (_resetLastSyncCommand == null)
                {
                    _resetLastSyncCommand =
                        new RelayCommand(p => ResetLastSync_Executed(), p => ResetLastSync_CanExecute())
                        {
                            FriendlyName = Message.SyncMaintenance_Target_ResetLastSync,
                            FriendlyDescription = Message.SyncMaintenance_Target_Reset_Tooltip,
                            DisabledReason = Message.SyncMaintenance_Target_Reset_Disabled,
                            Icon = ImageResources.Sync_Reset_16,
                        };
                    this.ViewModelCommands.Add(_resetLastSyncCommand);
                }
                return _resetLastSyncCommand;
            }
        }

        /// <summary>
        /// Called when determining if a reset sync target command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private bool ResetLastSync_CanExecute()
        {
            bool returnVal = true;

            if (returnVal)
            {
                //Sync target must be selected
                returnVal = (this.SyncTargetSelected != null);
                if (!returnVal)
                {
                    this.ResetLastSyncCommand.DisabledReason = Message.SyncMaintenance_Target_Reset_Disabled;
                }
            }

            if (returnVal)
            {
                //Sync target cannot be running
                returnVal = this.SyncTargetSelected.SyncTarget.Status != SyncTargetStatus.Running;
                if (!returnVal)
                {
                    this.ResetLastSyncCommand.DisabledReason = String.Format(Message.SyncMaintenance_Target_Reset_DisabledRunning, SyncTargetStatus.Running);
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Called when the reset sync target command is executed
        /// </summary>
        private void ResetLastSync_Executed()
        {
            Boolean resetConfirmed = true;
            String resetFriendlyName = String.Empty;
            String serverName;
            String databaseName;
            String[] connectionDetails;
            String[] connectDetails;

            try
            {
                //Set the current sync target
                this.SyncTargetCurrent = SyncTarget.GetSyncTargetById(this.SyncTargetSelected.SyncTarget.Id);

                switch (this.SyncTargetCurrent.TargetType)
                {
                    case SyncTargetType.Mssql:
                        //Main Connection String
                        connectionDetails = this.SyncTargetCurrent.ConnectionString.ToString().Split((";").ToCharArray());

                        //Server String
                        connectDetails = connectionDetails[0].ToString().Split(("=").ToCharArray());
                        serverName = connectDetails[1];

                        //Database String
                        connectDetails = connectionDetails[1].ToString().Split(("=").ToCharArray());
                        databaseName = connectDetails[1];

                        //Set the delete item name
                        resetFriendlyName = serverName + " : " + databaseName;
                        break;
                }

                if (this.AttachedControl != null)
                {
                    //Confirm delete with user
                    Framework.Controls.Wpf.ModalMessage win = new Framework.Controls.Wpf.ModalMessage();
                    win.Title = Message.Application_Title + " - " + Message.Application_Title_Ccm;
                    win.Header = resetFriendlyName;
                    win.Description = Message.SyncMaintenance_Target_ResetLastSync_Description;
                    win.ButtonCount = 2;
                    win.Button1Content = Message.Generic_Ok;
                    win.Button2Content = Message.Generic_Cancel;
                    win.MessageIcon = ImageResources.Dialog_Information_24;
                    win.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    win.Owner = this.AttachedControl != null ? this.AttachedControl : Application.Current.MainWindow;
                    win.ShowDialog();

                    //set the result
                    resetConfirmed = (win.Result == Framework.Controls.Wpf.ModalMessageResult.Button1);
                }

                if (resetConfirmed)
                {
                    // reset last sync date
                    this.SyncTargetCurrent.LastSync = null;
                    //Set the last run to null
                    this.SyncTargetCurrent.LastRun = null;
                    //Set the retry count to zero
                    this.SyncTargetCurrent.RetryCount = 0;
                    //Set the failed count to zero
                    this.SyncTargetCurrent.FailedCount = 0;
                    //Set the status to be Queued
                    this.SyncTargetCurrent.Status = SyncTargetStatus.Queued;
                    //Set the error to be none
                    this.SyncTargetCurrent.Error = SyncTargetError.None;

                    this.SyncTargetCurrent = this.SyncTargetCurrent.Save();
                    this.RefreshSyncTargetsCommand.Execute();
                }
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }
        }
        #endregion

        #region View Sync Target
        /// <summary>
        /// The view sync target command field
        /// </summary>
        private RelayCommand _viewSyncTargetCommand;

        /// <summary>
        /// Opens the sync target in the edit window
        /// </summary>
        public RelayCommand ViewSyncTargetCommand
        {
            get
            {
                if (_viewSyncTargetCommand == null)
                {
                    _viewSyncTargetCommand =
                        new RelayCommand(p => ViewSyncTarget_Executed(), p => ViewSyncTarget_CanExecute())
                        {

                        };
                    this.ViewModelCommands.Add(_viewSyncTargetCommand);
                }
                return _viewSyncTargetCommand;
            }
        }

        /// <summary>
        /// Called when determining if a view sync target command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private bool ViewSyncTarget_CanExecute()
        {
            //Sync target must be selected
            return this.SyncTargetSelected != null ? true : false;
        }

        /// <summary>
        /// Called when the view sync target command is executed
        /// </summary>
        private void ViewSyncTarget_Executed()
        {
            //Show dialog
            SyncMaintenanceWindow window = new SyncMaintenanceWindow(this.SyncTargetSelected.SyncTarget.Id);
            window.Owner = App.Current.MainWindow;
            window.ShowDialog();

            // refresh the target list
            this.RefreshSyncTargetsCommand.Execute();
        }
        #endregion

        #region Close
        /// <summary>
        /// The close command field
        /// </summary>
        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the sync tool
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand =
                        new RelayCommand(p => Close_Executed(), p => Close_CanExecute(), attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_Close,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.F4
                        };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        /// <summary>
        /// Called when determining if the close command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private Boolean Close_CanExecute()
        {
            return true;
        }

        /// <summary>
        /// Called when the close command is executed
        /// </summary>
        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }
        #endregion

        #region OpenLicenseScreenCommand

        private RelayCommand _openLicenseScreenCommand;

        /// <summary>
        /// Shows the licensing screen
        /// </summary>
        public RelayCommand OpenLicenseScreenCommand
        {
            get
            {
                if (_openLicenseScreenCommand == null)
                {
                    _openLicenseScreenCommand = new RelayCommand(
                        p => OpenLicenseScreen_Executed())
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    base.ViewModelCommands.Add(_openLicenseScreenCommand);
                }
                return _openLicenseScreenCommand;
            }
        }

        private void OpenLicenseScreen_Executed()
        {
            //Create and populate license
            DomainLicense securityLicense = new DomainLicense();
            securityLicense.LicenseInitalize();

            SecurityViewModel securityViewModel = new SecurityViewModel(securityLicense);
            SecurityDialogViewModel securityDialogViewModel = new SecurityDialogViewModel(securityLicense);
            SecurityWindow securityWindow = new SecurityWindow(securityViewModel);
            securityWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            securityViewModel.LoadRegistrationDetails();

            bool? _dialogResult = securityWindow.ShowDialog();

            if (_dialogResult == true)
            {
                securityLicense.LicenseInstall();
                securityLicense.LicenseUpdate();
                App.LicenseState.Refresh(securityLicense);
            }
        }

        #endregion

        #endregion

        #region Methods

        #region Service Start and Stop
        /// <summary>
        /// Carries out the work of starting the service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void serviceStartWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Start service
            IsBusyProgress = true;
            BusyProgressDescription = Message.SystemTray_StartService;
            //Reset the current and last recorded status
            ServiceStatus = string.Empty;
            ServiceStatusLastRecorded = string.Empty;

            _backgroundWorker = (BackgroundWorker)sender;
            ServiceController controller = new ServiceController();

            try
            {
                e.Result = false;
                controller.MachineName = _serviceServerName;
                controller.ServiceName = _serviceName;


                // Start the service
                if (controller.Status == ServiceControllerStatus.Stopped)
                {
                    try
                    {
                        controller.Start();
                    }
                    catch (Exception ex)
                    {
                        _serviceStatus = String.Format(Message.SyncService_Response_Start_Error, controller.ServiceName, _serviceServerName, controller.Status.ToString()) + "\n" + "\n" + ex.Message;
                    }
                }

                controller.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, _serviceWaitTimeoutSecs));

                if (controller.Status == ServiceControllerStatus.Running)
                {
                    //Started
                    e.Result = true;
                }
                else
                {
                    _serviceStatus = String.Format(Message.SyncService_Response_StartUnable_Error, _serviceServerName) + "\n" + "\n" + controller.Status.ToString();
                }
            }
            catch (System.InvalidOperationException ex)
            {
                _serviceStatus = String.Format(Message.SyncService_Response_StartUnable_Error, _serviceServerName) + "\n" + "\n" + ex.Message;
            }
            controller.Close();
        }

        /// <summary>
        /// Handles the service start worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void serviceStartWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= serviceStartWorker_DoWork;
            worker.RunWorkerCompleted -= serviceStartWorker_RunWorkerCompleted;
            IsBusyProgress = false;
        }

        /// <summary>
        /// Carries out the work of stopping the service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void serviceStopWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Stop service
            IsBusyProgress = true;
            BusyProgressDescription = Message.SystemTray_StopService;
            //Reset the current and last recorded status
            ServiceStatus = string.Empty;
            ServiceStatusLastRecorded = string.Empty;

            _backgroundWorker = (BackgroundWorker)sender;
            ServiceController controller = new ServiceController();

            try
            {
                e.Result = false;
                controller.MachineName = _serviceServerName;
                controller.ServiceName = _serviceName;

                // Stop the service
                if (controller.Status == ServiceControllerStatus.Running)
                {
                    try
                    {
                        controller.Stop();
                    }
                    catch (Exception ex)
                    {
                        _serviceStatus = String.Format(Message.SyncService_Response_Stop_Error, controller.ServiceName, _serviceServerName, controller.Status.ToString()) + "\n" + "\n" + ex.Message;
                    }
                }

                controller.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, _serviceWaitTimeoutSecs));

                if (controller.Status == ServiceControllerStatus.Stopped)
                {
                    //Stopped
                    e.Result = true;
                }
                else
                {
                    _serviceStatus = String.Format(Message.SyncService_Response_StopUnable_Error, _serviceServerName) + "\n" + "\n" + controller.Status.ToString();
                }
            }
            catch (System.InvalidOperationException ex)
            {
                _serviceStatus = String.Format(Message.SyncService_Response_StopUnable_Error, _serviceServerName) + "\n" + "\n" + ex.Message;
            }
            controller.Close();
        }

        /// <summary>
        /// Handles the service stop worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void serviceStopWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= serviceStopWorker_DoWork;
            worker.RunWorkerCompleted -= serviceStopWorker_RunWorkerCompleted;
            IsBusyProgress = false;
        }
        #endregion

        #region Establish Web Service
        /// <summary>
        /// Carries out the work of establishing the web service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void establishWebServiceWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Init
            e.Result = false;
            //Show progress
            IsBusyProgress = true;
            BusyProgressDescription = Message.SyncMaintenance_Source_Address_Establish_Connection;
            //Set background worker    
            _backgroundWorker = (BackgroundWorker)sender;
            //Establish web service connection
            e.Result = EstablishWebServiceConnection();
        }

        /// <summary>
        /// Handles the establishing web service worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void establishWebServiceWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= establishWebServiceWorker_DoWork;
            worker.RunWorkerCompleted -= establishWebServiceWorker_RunWorkerCompleted;

            //Save the sync source web service
            SaveSyncSourceWebService((bool)e.Result);

            //Complete busy progress
            IsBusyProgress = false;
        }

        /// <summary>
        /// Attempts to establish the web service connection
        /// </summary>
        private Boolean EstablishWebServiceConnection()
        {
            Boolean returnVal = false;

            try
            {
                //Fetch the available entities
                GFSModel.EntityList availableEntities = GFSModel.EntityList.FetchAll(GetFoundationServicesEndpoint());

                if (availableEntities.Count == 0)
                {
                    //Set a sync source response
                    _syncSourceResponse = Message.SyncMaintenance_Source_Response_Fail;
                }
                else
                {
                    //Set a sync source response
                    _syncSourceResponse = Message.SyncMaintenance_Source_Response_Success;
                    //Connected
                    returnVal = true;
                }
            }
            catch (System.InvalidOperationException ex)
            {
                //Set a sync source response
                _syncSourceResponse = Message.SyncMaintenance_Source_Response_Fail + "\n" + "\n" + ex.Message;
            }
            catch (Exception ex)
            {
                //Set a sync source response
                _syncSourceResponse = Message.SyncMaintenance_Source_Response_Fail + "\n" + "\n" + ex.Message;
            }

            return returnVal;
        }

        /// <summary>
        /// Save the sync source web service
        /// </summary>
        /// <param name="result">Connection result of the sync source</param>
        private void SaveSyncSourceWebService(bool result)
        {
            if (result)
            {
                // save sync source
                this.SyncSourceSelected = this.SyncSourceSelected.Save();

                // refresh the sync targets
                this.RefreshSyncTargetsCommand.Execute();

                //Sync source is connected
                IsSyncSourceConnected = true;
            }
            else
            {
                //Sync source has failed to connect
                IsSyncSourceConnected = false;
            }

            //Complete the response 
            SyncSourceResponse = _syncSourceResponse;
        }
        #endregion

        #region Sync Maintenance
        /// <summary>
        /// On selected sync source changing
        /// </summary>
        private void OnSelectedSyncSourceChanged(SyncSource oldModel, SyncSource newModel)
        {
            if (oldModel != null)
            {
                //Unsubscribe from old collection changed
                oldModel.PropertyChanged -= SelectedSyncSource_PropertyChanged;
            }

            if (newModel != null)
            {
                //Resubscribe to new
                newModel.PropertyChanged += new PropertyChangedEventHandler(SelectedSyncSource_PropertyChanged);
            }
        }

        /// <summary>
        /// Builds up the sync source address 
        /// </summary>
        public void SyncSourceAddressChanged()
        {
            String _serverName;
            String _siteName;
            String _portNumber;
            String _currentSyncSourceAddress;

            if (String.IsNullOrEmpty(this.SyncSourceSelected.ServerName))
            {
                _serverName = "<" + Message.SyncMaintenance_Source_ServerName + ">";
            }
            else
            {
                _serverName = this.SyncSourceSelected.ServerName;
            }

            if (String.IsNullOrEmpty(this.SyncSourceSelected.PortNumber.ToString()) || (Int32.Equals(this.SyncSourceSelected.PortNumber, 0)))
            {
                _portNumber = "<" + Message.SyncMaintenance_Source_PortNumber + ">";
            }
            else
            {
                _portNumber = this.SyncSourceSelected.PortNumber.ToString();
            }

            if (String.IsNullOrEmpty(this.SyncSourceSelected.SiteName))
            {
                _siteName = String.Empty;
            }
            else
            {
                _siteName = this.SyncSourceSelected.SiteName;
            }

            //Set the changed sync source address
            _currentSyncSourceAddress = String.Format("http://{0}:{1}/{2}", _serverName, _portNumber, _siteName);

            //If the sync source address is different reset connection
            if (String.Compare(_syncSourceAddress, _currentSyncSourceAddress) != 0)
            {
                if (!String.IsNullOrEmpty(_syncSourceAddress))
                {
                    IsSyncSourceConnected = false;
                    SyncSourceResponse = null;
                }
                SyncSourceAddress = _currentSyncSourceAddress;
            }
        }
        #endregion

        #region User Has Windows Administrator Role Privileges

        /// <summary>
        /// Determines if the current user is a member of windows administrator role
        /// </summary>
        /// <returns></returns>
        private Boolean UserHasWindowsAdministratorRolePrivileges()
        {
            var identity = WindowsIdentity.GetCurrent();

            if (identity != null)
            {
                var principal = new WindowsPrincipal(identity);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region GetFoundationServicesEndpoint
        /// <summary>
        /// Sets the global Foundation Services Endpoint
        /// </summary>
        /// <returns></returns>
        private String GetFoundationServicesEndpoint()
        {
            if (String.IsNullOrEmpty(this.SyncSourceSelected.SiteName))
            {
                return String.Format("http://{0}:{1}/Services", this.SyncSourceSelected.ServerName, this.SyncSourceSelected.PortNumber);
            }
            else
            {
                return String.Format("http://{0}:{1}/{2}/Services", this.SyncSourceSelected.ServerName, this.SyncSourceSelected.PortNumber, this.SyncSourceSelected.SiteName);
            }
        }
        #endregion

        #endregion

        #region Event Handlers
        /// <summary>
        /// Handles the selected sync source property changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedSyncSource_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SyncSourceAddressChanged();
        }
        #endregion

        #region Disposing
        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        /// <param name="disposing">Indicates if being called from dispose</param>
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    foreach (SyncTargetRowViewModel row in this._syncTargetRows)
                    {
                        row.Dispose();
                    }
                    this._syncTargetRows.Clear();
                    if (this._syncSourceSelected != null)
                    {
                        this._syncSourceSelected.PropertyChanged -= SelectedSyncSource_PropertyChanged;
                        this._syncSourceSelected = null;
                    }
                    this._syncSources = null;
                    this._syncTargets = null;
                    this._syncTargetSelected = null;
                    this._syncTargetCurrent = null;
                    this._state = null;
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }

    public class SyncTargetRowViewModel : Framework.ViewModel.ViewModelObject
    {
        #region Field Names
        private SyncTarget _syncTarget;
        #endregion

        #region Binding Property Paths
        public static PropertyPath SyncTargetProperty = WpfHelper.GetPropertyPath<SyncTargetRowViewModel>(p => p.SyncTarget);
        #endregion

        #region Properties
        /// <summary>
        /// Gets the parent location space product group
        /// </summary>
        public SyncTarget SyncTarget
        {
            get { return _syncTarget; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="SyncTarget"></param>
        /// <param name="AvailableEntities"></param>
        public SyncTargetRowViewModel(SyncTarget syncTarget)
        {
            //Load properties
            this._syncTarget = syncTarget;
        }
        #endregion

        #region Disposing
        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        /// <param name="disposing">Indicates if being called from dispose</param>
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }

    public class CharacterReplaceWrapConverter : IValueConverter
    {
        #region Convert
        /// <summary>
        /// Convert the connection string
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            String connectionString = (String)value;
            String character = parameter as string;

            if (String.IsNullOrEmpty(character))
            {
                character = ";";
            }

            return connectionString.Replace(character, "\n");
        }
        #endregion

        #region Convert Back
        /// <summary>
        /// Not implemented
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}