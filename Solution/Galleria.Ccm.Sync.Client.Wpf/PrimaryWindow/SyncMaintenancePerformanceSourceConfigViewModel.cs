﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Sync.Client.Wpf.Resources;
using Galleria.Ccm.Sync.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Sync.Client.Wpf.PrimaryWindow
{
    public class SyncMaintenancePerformanceSourceConfigViewModel : ViewModelAttachedControlObject<SyncMaintenancePerformanceSourceConfig>
    {
        #region Constants
        private const Byte _defaultSyncPeriod = 12; //in weeks
        #endregion

        #region Fields
        private SyncTarget _syncTarget;
        private GFSModel.PerformanceSourceList _availablePerformanceSources;
        private GFSModel.PerformanceSource _selectedPerformanceSource;
        private Byte? _selectedSyncPeriod = _defaultSyncPeriod; //in weeks

        private SyncTargetGfsPerformance _selectedSyncTargetPerformanceSource;
        private ObservableCollection<SyncTargetGfsPerformance> _syncTargetPerformanceSources = new ObservableCollection<SyncTargetGfsPerformance>();

        #endregion

        #region Property Path
        //properties
        public static readonly PropertyPath AvailablePerformanceSourcesProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.AvailablePerformanceSources);
        public static readonly PropertyPath SelectedPerformanceSourceProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.SelectedPerformanceSource);
        public static readonly PropertyPath SelectedSyncPeriodProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.SelectedSyncPeriod);
        public static readonly PropertyPath SyncTargetProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.SyncTarget);

        public static readonly PropertyPath SelectedSyncTargetPerformanceSourceProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.SelectedSyncTargetPerformanceSource);
        public static readonly PropertyPath SyncTargetPerformanceSourcesProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.SyncTargetPerformanceSources);

        //commands
        public static readonly PropertyPath AddPerformanceSourceCommandProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.AddPerformanceSourceCommand);
        public static readonly PropertyPath RemoveSyncTargetPerformanceSourceCommandProperty = WpfHelper.GetPropertyPath<SyncMaintenancePerformanceSourceConfigViewModel>(p => p.RemoveSyncTargetPerformanceSourceCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Collection of Performance Sources for all GFS Entities 
        /// </summary>
        public GFSModel.PerformanceSourceList AvailablePerformanceSources
        {
            get { return _availablePerformanceSources; }
            set
            {
                _availablePerformanceSources = value;
                OnPropertyChanged(AvailablePerformanceSourcesProperty);
            }
        }

        public GFSModel.PerformanceSource SelectedPerformanceSource
        {
            get { return _selectedPerformanceSource; }
            set
            {
                _selectedPerformanceSource = value;
                OnPropertyChanged(SelectedPerformanceSourceProperty);

                OnSelectedPerformanceSourceChanged();
            }
        }

        private void OnSelectedPerformanceSourceChanged()
        {
            GFSModel.PerformanceSourceType type = this.SelectedPerformanceSource.Type;
            //sync period is not applicable to simple performance source
            if (this.SelectedPerformanceSource != null && type == GFSModel.PerformanceSourceType.Simple)
            {
                this.SelectedSyncPeriod = null;
            }
            else if (this.SelectedPerformanceSource != null && type == GFSModel.PerformanceSourceType.Advanced)
            {
                this.SelectedSyncPeriod = _defaultSyncPeriod;
            }
        }

        public Byte? SelectedSyncPeriod
        {
            get { return _selectedSyncPeriod; }
            set
            {
                _selectedSyncPeriod = value;
                OnPropertyChanged(SelectedSyncPeriodProperty);
            }
        }

        public SyncTargetGfsPerformance SelectedSyncTargetPerformanceSource
        {
            get { return _selectedSyncTargetPerformanceSource; }
            set
            {
                _selectedSyncTargetPerformanceSource = value;
                OnPropertyChanged(SelectedSyncTargetPerformanceSourceProperty);
            }
        }

        public ObservableCollection<SyncTargetGfsPerformance> SyncTargetPerformanceSources
        {
            get { return _syncTargetPerformanceSources; }
            set
            {
                _syncTargetPerformanceSources = value;
                OnPropertyChanged(SyncTargetPerformanceSourcesProperty);
            }
        }

        public SyncTarget SyncTarget
        {
            get { return _syncTarget; }
        }

        #endregion

        #region Constructors

        public SyncMaintenancePerformanceSourceConfigViewModel(SyncTarget currentSyncTarget,
            GFSModel.PerformanceSourceList performanceSourcesPerEntity)
        {
            _syncTarget = currentSyncTarget;

           // existing performance sources to sync against this sync target
            if (_syncTarget != null)
            {
                foreach (SyncTargetGfsPerformance performance in _syncTarget.SyncTargetGfsPerformances)
                {
                    _syncTargetPerformanceSources.Add(performance);
                }
            }

            // available performance sources for this entity
            this.AvailablePerformanceSources = performanceSourcesPerEntity;
            if (this.AvailablePerformanceSources.Any())
            {
                this.SelectedPerformanceSource = this.AvailablePerformanceSources.First();               
            }
        }

        #endregion

        #region Commands

        #region Add Performance Source

        private RelayCommand _addPerformanceSourceCommand;

        /// <summary>
        /// Adds any selected peformance source to the selected sources grid
        /// </summary>
        public RelayCommand AddPerformanceSourceCommand
        {
            get
            {
                if (_addPerformanceSourceCommand == null)
                {
                    _addPerformanceSourceCommand = new RelayCommand(
                        p => AddPerformanceSource_Executed(),
                        p => AddPerformanceSource_CanExecute())
                    {
                        FriendlyDescription = Message.SyncMaintenancePerformanceSourceConfig_AddSelectedPerformanceSources,
                        SmallIcon = Galleria.Ccm.Sync.Client.Wpf.Resources.ImageResources.SyncMaintenancePerformanceSourceConfig_AddPerformanceSource
                    };
                    base.ViewModelCommands.Add(_addPerformanceSourceCommand);
                }
                return _addPerformanceSourceCommand;
            }
        }


        private bool AddPerformanceSource_CanExecute()
        {
            if (this.SyncTarget.Status == SyncTargetStatus.Running)
            {
                _addPerformanceSourceCommand.DisabledReason = String.Format(Message.SyncMaintenance_Target_Invalid_Running, SyncTargetStatus.Running);
                return false;
            }

            if (this.SelectedPerformanceSource == null)
            {
                _addPerformanceSourceCommand.DisabledReason = Message.SyncMaintenancePerformanceSourceConfig_AddSelectedPerformanceSources_DisabledReason;
                return false;
            }

            GFSModel.PerformanceSourceType type = this.SelectedPerformanceSource.Type;
            if (type == GFSModel.PerformanceSourceType.Advanced)
            {
                //ensure that sync period does not exceed 2 years
                Byte syncPeriod = 0;
                if (this.AttachedControl != null)
                {
                    if (!Byte.TryParse(this.AttachedControl.xSelectedSyncPeriod.Text, out syncPeriod)
                        || (syncPeriod < 1 || syncPeriod > 104))
                    {
                        _addPerformanceSourceCommand.DisabledReason
                            = Message.SyncMaintenancePerformanceSourceConfig_RemoveSelectedPerformanceSources_DisabledReasonInvalidSyncPeriod;
                        return false;
                    }
                }
            }

            return true;
        }

        private void AddPerformanceSource_Executed()
        {
            IEnumerable<SyncTargetGfsPerformance> performanceSources = this.SyncTargetPerformanceSources.Where(p => p.EntityName == this.SelectedPerformanceSource.EntityName && p.SourceName == this.SelectedPerformanceSource.Name
                && p.SourceSyncPeriod == this.SelectedSyncPeriod);
            //only add if the same performance source with the same sync period wasn't added yet
            if (!performanceSources.Any())
            {
                SyncTargetGfsPerformance performanceSource = SyncTargetGfsPerformance.NewSyncTargetGfsPerformance(_syncTarget.Id, this.SelectedPerformanceSource);
                performanceSource.SourceSyncPeriod = this.SelectedSyncPeriod;
                this.SyncTargetPerformanceSources.Add(performanceSource);
            }
            else
            {
                //this performance source configuration alredy exists
                ModalMessage errorMessage = new ModalMessage();
                errorMessage.Header = Message.SyncMaintenancePerformanceSourceConfig_AddPerformanceSource_ConfigExists_Header;
                //Performance Source {0} with Sync Period value {1} has already been added. 
                errorMessage.Description = String.Format(Message.SyncMaintenancePerformanceSourceConfig_AddPerformanceSource_ConfigExists,
                    this.SelectedPerformanceSource.EntityName + " - " + this.SelectedPerformanceSource.Name, this.SelectedSyncPeriod);
                errorMessage.ButtonCount = 1;
                errorMessage.DefaultButton = ModalMessageButton.Button1;
                errorMessage.MessageIcon = Galleria.Ccm.Sync.Client.Wpf.Resources.ImageResources.Dialog_Information_24;
                errorMessage.Button1Content = Message.Generic_Ok;
                errorMessage.Owner = this.AttachedControl;
                errorMessage.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                errorMessage.ShowDialog();
            }
        }

        #endregion

        #region Remove Sync Target Performance Source

        private RelayCommand _removeSyncTargetPerformanceSourceCommand;

        /// <summary>
        /// Remove any selected available columns from the current view column list
        /// </summary>
        public RelayCommand RemoveSyncTargetPerformanceSourceCommand
        {
            get
            {
                if (_removeSyncTargetPerformanceSourceCommand == null)
                {
                    _removeSyncTargetPerformanceSourceCommand = new RelayCommand(
                        p => RemoveSelectedPerformanceSources_Executed(),
                        p => RemoveSelectedPerformanceSources_CanExecute())
                    {
                        FriendlyName = Message.SelectedSyncTargetPerformanceSource_Remove,
                        FriendlyDescription = Message.SyncMaintenancePerformanceSourceConfig_RemoveSelectedPerformanceSources,
                        SmallIcon = Galleria.Ccm.Sync.Client.Wpf.Resources.ImageResources.SyncMaintenancePerformanceSourceConfig_RemoveSyncTargetPerformanceSource                        
                    };
                    base.ViewModelCommands.Add(_removeSyncTargetPerformanceSourceCommand);
                }
                return _removeSyncTargetPerformanceSourceCommand;
            }
        }

        private bool RemoveSelectedPerformanceSources_CanExecute()
        {
            if (this.SelectedSyncTargetPerformanceSource == null)
            {
                _removeSyncTargetPerformanceSourceCommand.DisabledReason = Message.SyncMaintenancePerformanceSourceConfig_RemoveSelectedPerformanceSources_DisabledReason;
                return false;
            }

            if (this.SyncTarget.Status == SyncTargetStatus.Running)
            {
                _removeSyncTargetPerformanceSourceCommand.DisabledReason = String.Format(Message.SyncMaintenance_Target_Invalid_Running, SyncTargetStatus.Running);
                return false;
            }

            return true;
        }

        private void RemoveSelectedPerformanceSources_Executed()
        {
            SyncTargetGfsPerformance performanceToDelete = this.SelectedSyncTargetPerformanceSource;

            this.SyncTargetPerformanceSources.Remove(performanceToDelete);
        }

        #endregion

        #region OK

        private RelayCommand _okCommand;

        /// <summary>
        /// Adds any selected peformance source to the selected sources grid
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => Ok_Executed(),
                        p => Ok_CanExecute())
                    {
                        FriendlyDescription = Message.SyncMaintenancePerformanceSourceConfig_AddSelectedPerformanceSources
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }


        private bool Ok_CanExecute()
        {
            if (this.SyncTarget.Status == SyncTargetStatus.Running)
            {
                _okCommand.DisabledReason = String.Format(Message.SyncMaintenance_Target_Invalid_Running, SyncTargetStatus.Running);
                return false;
            }

            foreach (SyncTargetGfsPerformance source in this.SyncTargetPerformanceSources)
            {
                if (!source.IsValid)
                { return false; }
            }
            return true;
        }

        private void Ok_Executed()
        {
            try
            {
                foreach (SyncTargetGfsPerformance peformanceToRemove in this.SyncTarget.SyncTargetGfsPerformances.Except(this.SyncTargetPerformanceSources.AsEnumerable<SyncTargetGfsPerformance>()).ToList())
                {
                    _syncTarget.SyncTargetGfsPerformances.Remove(peformanceToRemove);
                }

                foreach (SyncTargetGfsPerformance peformanceToAdd in this.SyncTargetPerformanceSources)
                {
                    if (!_syncTarget.SyncTargetGfsPerformances.Contains(peformanceToAdd))
                    {
                        _syncTarget.SyncTargetGfsPerformances.Add(peformanceToAdd);
                    }
                }
            }
            catch (Exception e)
            {

            }

            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = true;
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the new performance source operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Cancel_CanExecute()
        {
            return true;
        }

        private void Cancel_Executed()
        {
            //close the window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Events
        /// <summary>
        /// Delclaration of CloseWindow event
        /// </summary>
        public event EventHandler CloseWindow;
        /// <summary>
        /// Method for event to fire
        /// </summary>
        public void OnCloseWindow()
        {
            if (this.CloseWindow != null)
            {
                this.CloseWindow(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Disposing
        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        /// <param name="disposing">Indicates if being called from dispose</param>
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedSyncTargetPerformanceSource = null;
                    _syncTargetPerformanceSources = null;
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }

    /// <summary>
    /// Binding validation rule
    /// </summary>
    public class SyncPeriodValidationRule : ValidationRule
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public SyncPeriodValidationRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            BindingExpression expression = value as BindingExpression;
            if (expression != null)
            {
                SyncTargetGfsPerformance sourceItem = expression.DataItem as SyncTargetGfsPerformance;
                if (sourceItem != null)
                {
                    //ensure that sync period does not exceed 2 years
                    if (sourceItem.SourceSyncPeriod.HasValue)
                    {
                        if (sourceItem.SourceSyncPeriod.Value < 1 || sourceItem.SourceSyncPeriod.Value > 104)
                        {
                            validationResult = new ValidationResult(false, Message.SyncMaintenancePerformanceSourceConfig_RemoveSelectedPerformanceSources_DisabledReasonInvalidSyncPeriod);
                        }
                        else
                        {
                            validationResult = new ValidationResult(true, String.Empty);
                        }
                    }
                }
            }

            // and return the validation result
            return validationResult;
        }
        #endregion

    }
}