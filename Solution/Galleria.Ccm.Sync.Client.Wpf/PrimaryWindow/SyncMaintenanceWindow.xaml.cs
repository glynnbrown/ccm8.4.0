﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Collections.ObjectModel;
using Galleria.Framework.Helpers;


namespace Galleria.Ccm.Sync.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for SyncMaintenanceWindow.xaml
    /// </summary>
    public partial class SyncMaintenanceWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model property
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SyncMaintenanceViewModel), typeof(SyncMaintenanceWindow),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        /// <summary>
        /// Get/Set view model property
        /// </summary>
        public SyncMaintenanceViewModel ViewModel
        {
            get { return (SyncMaintenanceViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Event to handle view model property changes
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private static void ViewModel_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SyncMaintenanceWindow senderControl = (SyncMaintenanceWindow)obj;

            if (e.OldValue != null)
            {
                SyncMaintenanceViewModel oldModel = (SyncMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.CloseWindow -= senderControl.ViewModel_CloseWindow;
            }

            if (e.NewValue != null)
            {
                SyncMaintenanceViewModel newModel = (SyncMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.CloseWindow += senderControl.ViewModel_CloseWindow;
            }
        }
        #endregion

        #endregion

        #region Constructor
        /// <summary>
        ///  Creates a new instance of this type
        /// </summary>
        /// <param name="AvailableEntities"></param>
        public SyncMaintenanceWindow()
        {
            InitializeComponent();

            this.ViewModel = new SyncMaintenanceViewModel();          

            //set the variable to show that the ui has created this object, not the service
            this.ViewModel.CurrentSyncTarget.UISource = true;
        }

        /// <summary>
        /// Creates a new instance of this type for a given sync target
        /// </summary>
        /// <param name="id"></param>
        /// <param name="AvailableEntities"></param>
        public SyncMaintenanceWindow(int id)
        {
            InitializeComponent();

            this.ViewModel = new SyncMaintenanceViewModel(id);

            //set the variable to show that the ui has created this object, not the service
            this.ViewModel.CurrentSyncTarget.UISource = true;
        }
        #endregion

        #region Events
        /// <summary>
        /// Subscribe to method to close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ViewModel_CloseWindow(object sender, EventArgs e)
        {
            //Close the window
            this.Close();
        }

        /// <summary>
        /// Applies changes to the view model and closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;

            //Close the window
            this.Close();
        }
        #endregion

        #region Window Close
        /// <summary>
        /// Clears down the control safely
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Set view model to null to cause edit mode to be canceled whenever the window does close
            this.ViewModel = null;
        }
        #endregion
    }
}