﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26505 : L.Ineson
//  Commented out performance source related code
//  as it is not required for v1.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Sync.Client.Wpf.Resources.Language;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Sync.Client.Wpf.PrimaryWindow
{
    public enum SaveAction
    {
        SaveAndClose,
        SaveAndNew,
    }

    public class SyncMaintenanceViewModel : ViewModelAttachedControlObject<SyncMaintenanceWindow>
    {
        #region Fields
        //Performance Sources
        //private GFSModel.PerformanceSourceList _availablePerformanceSources; // holds a list of the available performance sources from the sync source
        private static ReadOnlyCollection<Byte> _defaultSyncPeriods = new ReadOnlyCollection<Byte>(new List<Byte> { 12, 26, 52 }); //in weeks

        //Sync Target
        private SyncTargetList _syncTargets; // holds the sync targets
        private SyncTarget _currentSyncTarget; // holds the current sync target
        private string _saveInvalidReason; // holds the invalid reason for save command
        private string _connectionString; // holds the current connection string
        private Boolean _isCustomPerformanceSetup = false;

        //Database Setup
        private String _serverName; // The current server name (sql)
        private String _databaseName; // The current database name (sql)
        private String _fileLocation; // The current file location (vistadb)
        private String _fileName; // The current file name  (vistadb)
        private String _fullFileName; // The current full file name (vistadb)
        private String _defaultFileLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Behavioral Cluster Planning"); // default file location

        //Busy
        private Boolean _isbusyProgress = false; // show or hide the busy progress
        private String _busyProgressDescription; // what is our busy progress description
        private BackgroundWorker _backgroundWorker; // background worker

        //Save
        private String _saveResponse; // contains the save response
        private Boolean _isSaved = true; // has the target been saved
        private SaveAction _currentSaveAction = SaveAction.SaveAndClose;
        #endregion

        #region BindingPropertyPaths
        //Sync Target
        public static PropertyPath SyncTargetsProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SyncTargets);
        public static PropertyPath CurrentSyncTargetsProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.CurrentSyncTarget);
        public static PropertyPath IsCustomPerformanceSetupProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.IsCustomPerformanceSetup);
        
        //Database Setup
        public static PropertyPath ServerNameProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.ServerName);
        public static PropertyPath DatabaseNameProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.DatabaseName);
        public static PropertyPath FileLocationProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.FileLocation);
        public static PropertyPath FileNameProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.FileName);
        public static PropertyPath FullFileNameProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.FullFileName);

        //Busy
        public static PropertyPath IsBusyProgressProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.IsBusyProgress);
        public static PropertyPath BusyProgressDescriptionProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.BusyProgressDescription);

        //Save
        public static PropertyPath IsSavedProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.IsSaved);
        public static PropertyPath SaveResponseProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.SaveResponse);
        public static readonly PropertyPath CurrentSaveActionProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.CurrentSaveAction);

        //commands
        //public static readonly PropertyPath ConfigurePerformanceSourcesSyncCommandProperty = WpfHelper.GetPropertyPath<SyncMaintenanceViewModel>(p => p.ConfigurePerformanceSourcesSyncCommand);

        #endregion

        #region Properties
        /// <summary>
        /// Returns the current save action
        /// </summary>
        public SaveAction CurrentSaveAction
        {
            get { return _currentSaveAction; }
            private set
            {
                _currentSaveAction = value;
                OnPropertyChanged(CurrentSaveActionProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the save response
        /// </summary>
        public String SaveResponse
        {
            get { return _saveResponse; }
            set
            {
                _saveResponse = value;
                OnPropertyChanged(SaveResponseProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the is saved
        /// </summary>        
        public Boolean IsSaved
        {
            get { return _isSaved; }
            set
            {
                _isSaved = value;
                OnPropertyChanged(IsSavedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the visibility of busy progress
        /// </summary>        
        public Boolean IsBusyProgress
        {
            get { return _isbusyProgress; }
            set
            {
                _isbusyProgress = value;
                OnPropertyChanged(IsBusyProgressProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the busy progress description
        /// </summary>
        public String BusyProgressDescription
        {
            get { return _busyProgressDescription; }
            set
            {
                _busyProgressDescription = value;
                OnPropertyChanged(BusyProgressDescriptionProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the sync targets
        /// </summary>
        public SyncTargetList SyncTargets
        {
            get { return _syncTargets; }
            private set
            {
                _syncTargets = value;
                OnPropertyChanged(SyncTargetsProperty);
            }
        }

        /// <summary>
        /// The current target type
        /// </summary>
        private SyncTargetType CurrentTargetType
        {
            get { return _currentSyncTarget.TargetType; }
        }

        /// <summary>
        /// Returns the item to be used for pre-add editing
        /// </summary>
        public SyncTarget CurrentSyncTarget
        {
            get { return _currentSyncTarget; }
            set
            {
                _currentSyncTarget = value;
                OnPropertyChanged(CurrentSyncTargetsProperty);

                SyncTargetLoadDetails();
                UpdateIsCustomPerformanceSetupValue();
            }
        }

        /// <summary>
        /// Gets/Sets the sql server name
        /// </summary>
        public String ServerName
        {
            get { return _serverName; }
            set
            {
                _serverName = value;
                OnPropertyChanged(ServerNameProperty);

                SyncTargetChanged();
            }
        }

        /// <summary>
        /// Gets/Sets the sql database name
        /// </summary>
        public String DatabaseName
        {
            get { return _databaseName; }
            set
            {
                _databaseName = value;
                OnPropertyChanged(DatabaseNameProperty);

                SyncTargetChanged();
            }
        }

        /// <summary>
        /// Gets/Sets the complete local file path \ name
        /// </summary>
        public String FullFileName
        {
            get { return _fullFileName; }
            set
            {
                _fullFileName = value;
                OnPropertyChanged(FullFileNameProperty);

                SyncTargetChanged();
            }
        }

        /// <summary>
        /// Gets/Sets the local database location
        /// </summary>
        public String FileLocation
        {
            get { return _fileLocation; }
            set
            {
                _fileLocation = value;
                OnPropertyChanged(FileLocationProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the local fileName
        /// </summary>
        public String FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                OnPropertyChanged(FileNameProperty);
            }
        }

        ///// <summary>
        ///// Gets/Sets the collection of available PerformanceSources per Entity
        ///// </summary>
        //public GFSModel.PerformanceSourceList AvailablePerformanceSources
        //{
        //    get { return _availablePerformanceSources; }
        //    set
        //    {
        //        _availablePerformanceSources = value;
        //    }
        //}

        /// <summary>
        /// States if sync target performance source selection has been customised
        /// </summary>
        public Boolean IsCustomPerformanceSetup
        {
            get
            {
                return _isCustomPerformanceSetup;
            }
            private set
            {
                _isCustomPerformanceSetup = value;
                OnPropertyChanged(IsCustomPerformanceSetupProperty);
            }
        }

        #endregion

        #region Constructor

        public SyncMaintenanceViewModel()
            : this(SyncTarget.NewSyncTarget(), true)
        { }

        public SyncMaintenanceViewModel(Int32 syncTargetId)
            : this(SyncTarget.GetSyncTargetById(syncTargetId), false)
        { }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        public SyncMaintenanceViewModel(SyncTarget syncTarget, Boolean newAction)
        {
            this.CurrentSyncTarget = syncTarget;

            //Fetch available performance sources
            //_availablePerformanceSources = GFSModel.PerformanceSourceList.FetchAllFromGFS();

            //if (newAction)
            //{
                ////populate sync target with the default list of performance sources to sync
                //foreach (GFSModel.PerformanceSource performanceSource in _availablePerformanceSources)
                //{
                //    if (performanceSource.Type == GFSModel.PerformanceSourceType.Advanced)
                //    {
                //        foreach (Byte syncPeriod in _defaultSyncPeriods)
                //        {
                //            SyncTargetGfsPerformance advancedPerformanceSource
                //                = SyncTargetGfsPerformance.NewSyncTargetGfsPerformance(this.CurrentSyncTarget.Id, performanceSource);
                //            advancedPerformanceSource.SourceSyncPeriod = syncPeriod;
                //            this.CurrentSyncTarget.SyncTargetGfsPerformances.Add(advancedPerformanceSource);
                //        }
                //    }
                //    else
                //    {
                //        SyncTargetGfsPerformance simplePerformanceSource
                //            = SyncTargetGfsPerformance.NewSyncTargetGfsPerformance(this.CurrentSyncTarget.Id, performanceSource);
                //        this.CurrentSyncTarget.SyncTargetGfsPerformances.Add(simplePerformanceSource);
                //    }
                //}
            //}
        }
        #endregion

        #region Commands

        #region Save
        /// <summary>
        /// The save command field
        /// </summary>
        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the sync target list
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        /// <summary>
        /// Called when determining if the save command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private bool Save_CanExecute()
        {
            if (_currentSyncTarget == null)
            {
                return false;
            }

            switch (CurrentSyncTarget.TargetType)
            {
                case SyncTargetType.Mssql:
                    if (String.IsNullOrEmpty(ServerName) && String.IsNullOrEmpty(DatabaseName))
                    {
                        _saveInvalidReason = Message.SyncMaintenance_Target_Invalid_SQL;
                        return false;
                    }
                    break;
            }

            if (this.CurrentSyncTarget.Status == SyncTargetStatus.Running)
            {
                _saveInvalidReason = String.Format(Message.SyncMaintenance_Target_Invalid_Running, SyncTargetStatus.Running);
                return false;
            }

            if (!(this.CurrentSyncTarget.IsDirty && this.CurrentSyncTarget.IsValid))
            {
                _saveInvalidReason = Message.SyncMaintenance_Target_Invalid_NoChanges;
            }

            return true;
        }

        /// <summary>
        /// Called when the save command is executed
        /// </summary>
        private void Save_Executed()
        {
            if (this.Save_CanExecute())
            {
                //Perform the save
                if (this.AttachedControl != null)
                {
                    _backgroundWorker = new BackgroundWorker();
                    _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
                    _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                    _backgroundWorker.RunWorkerAsync();
                }
                else
                {
                    SaveTarget(SaveTargetValidation());
                }
            }
        }
        #endregion

        #region Save And Close
        /// <summary>
        /// The save and close command field
        /// </summary>
        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current sync target and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(p => SaveAndClose_Executed(), p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        /// <summary>
        /// Called when determining if the save and close command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private bool SaveAndClose_CanExecute()
        {
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAndCloseCommand.DisabledReason = _saveInvalidReason;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called when the save and close command is executed
        /// </summary>
        private void SaveAndClose_Executed()
        {
            //Save and close
            this.CurrentSaveAction = SaveAction.SaveAndClose;
            this.SaveCommand.Execute();
        }
        #endregion

        #region Save And New
        /// <summary>
        /// The save and new command field
        /// </summary>
        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Saves the current sync target and loads a new one
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => SaveAndNew_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew
                    };
                    this.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        /// <summary>
        /// Called when determining if the save and new command can be executed
        /// </summary>
        /// <returns>True if the command can be executed</returns>
        private bool SaveAndNew_CanExecute()
        {
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAndCloseCommand.DisabledReason = _saveInvalidReason;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called when the save and new command is executed
        /// </summary>
        private void SaveAndNew_Executed()
        {
            //Save and new
            this.CurrentSaveAction = SaveAction.SaveAndNew;
            this.SaveCommand.Execute();
        }
        #endregion

        #region Browse For Local Database
        /// <summary>
        /// The browse for local database command field
        /// </summary>
        private RelayCommand _browseForLocalDatabaseCommand;

        /// <summary>
        /// Shows the open file dialog to select a local database file
        /// </summary>
        public RelayCommand BrowseForLocalDatabaseCommand
        {
            get
            {
                if (_browseForLocalDatabaseCommand == null)
                {
                    _browseForLocalDatabaseCommand = new RelayCommand(
                        p => BrowseForLocalDatabase_Executed())
                    {
                        FriendlyName = Message.Generic_Browse
                    };
                    base.ViewModelCommands.Add(_browseForLocalDatabaseCommand);
                }
                return _browseForLocalDatabaseCommand;
            }
        }

        /// <summary>
        /// Called when the browse for local database command is executed
        /// </summary>
        private void BrowseForLocalDatabase_Executed()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.InitialDirectory = _defaultFileLocation;
            dialog.CheckFileExists = true;
            dialog.CheckPathExists = true;
            dialog.Multiselect = false;

            if (dialog.ShowDialog() == true)
            {
                FullFileName = dialog.FileName;
                FileLocation = Path.GetDirectoryName(dialog.FileName);
                FileName = Path.GetFileNameWithoutExtension(dialog.FileName);
            }
        }
        #endregion

        #region Configure Performance Sources Sync Details
        ///// <summary>
        ///// The Configure Performance Sources Sync command field
        ///// </summary>
        //private RelayCommand _configurePerformanceSourcesSyncCommand;

        ///// <summary>
        /////  The Configure Performance Sources Sync command 
        ///// </summary>
        //public RelayCommand ConfigurePerformanceSourcesSyncCommand
        //{
        //    get
        //    {
        //        if (_configurePerformanceSourcesSyncCommand == null)
        //        {
        //            _configurePerformanceSourcesSyncCommand = new RelayCommand(
        //                p => ConfigurePerformanceSourcesSync_Executed(),
        //                p => ConfigurePerformanceSourcesSync_CanExecute())
        //            {
        //                FriendlyName = Message.Generic_Configure
        //            };
        //            this.ViewModelCommands.Add(_configurePerformanceSourcesSyncCommand);
        //        }
        //        return _configurePerformanceSourcesSyncCommand;
        //    }
        //}

        ///// <summary>
        ///// Called when determining if the save and new command can be executed
        ///// </summary>
        ///// <returns>True if the command can be executed</returns>
        //private bool ConfigurePerformanceSourcesSync_CanExecute()
        //{
        //    if (this.AvailablePerformanceSources.Count() == 0)
        //    {
        //        _configurePerformanceSourcesSyncCommand.DisabledReason = Message.SyncMaintenanceViewModel_ConfigurePerformanceSourcesSyncCommand_NoSources;
        //        return false;
        //    }

        //    return true;
        //    //return false;
        //}

        ///// <summary>
        ///// Called when the save and new command is executed
        ///// </summary>
        //private void ConfigurePerformanceSourcesSync_Executed()
        //{
        //    //Show dialog
        //    SyncMaintenancePerformanceSourceConfig window = new SyncMaintenancePerformanceSourceConfig(
        //        this.CurrentSyncTarget, this.AvailablePerformanceSources);
        //    window.Owner = this.AttachedControl;

        //    //retrieve modified sync target
        //    if (window.ShowDialog() == true && window.ViewModel != null)
        //    {
        //        this.CurrentSyncTarget = window.ViewModel.SyncTarget;
        //    }
        //}
        #endregion

        #endregion

        #region Events
        /// <summary>
        /// Delclaration of CloseWindow event
        /// </summary>
        public event EventHandler CloseWindow;
        /// <summary>
        /// Method for event to fire
        /// </summary>
        public void OnCloseWindow()
        {
            if (this.CloseWindow != null)
            {
                this.CloseWindow(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Carries out the work of validating and performing the target save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Show progress
            IsBusyProgress = true;
            BusyProgressDescription = Message.SyncMaintenance_Target_Database_Connection;
            //Set back ground worker
            _backgroundWorker = (BackgroundWorker)sender;
            //Validate save
            e.Result = SaveTargetValidation();
        }

        /// <summary>
        /// Handles the worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;

            //Save target
            SaveTarget((bool)e.Result);

            //Complete busy progress
            IsBusyProgress = false;
        }

        /// <summary>
        /// Validates the save by check if the record is unique and has a valid connection
        /// </summary>
        private Boolean SaveTargetValidation()
        {
            try
            {
                Boolean returnVal = false;
                Boolean isUniqueSyncTarget = true;
                String uniqueFriendlyName = String.Empty;

                //Make sure we apply the latest connection string changes
                SyncTargetChanged();
                //Populate list of sync targets
                this.SyncTargets = SyncTargetList.GetAllSyncTargets(true);
                //Use connection string as the unique check
                Predicate<String> isUniqueCheck =
                    (s) =>
                    {
                        Int32 id = this.CurrentSyncTarget.Id;
                        IEnumerable<String> takenValues = this.SyncTargets.Where(p => p.Id != id).Select(p => p.ConnectionString);
                        return !takenValues.Contains(s);
                    };
                isUniqueSyncTarget = isUniqueCheck(this.CurrentSyncTarget.ConnectionString);

                //Is it unquie
                if (isUniqueSyncTarget)
                {
                    _saveResponse = Message.SyncMaintenance_Target_Connection_Error;

                    switch (this.CurrentSyncTarget.TargetType)
                    {
                        case SyncTargetType.Mssql:
                            // create a new config element for the dal
                            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement();
                            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", ServerName));
                            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", DatabaseName));

                            // now create a new instance of the MSSql dal to check if ISO db
                            using (IDalFactory dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig))
                            {
                                String databaseType = dalFactory.GetDatabaseType();
                                returnVal = (databaseType == Constants.CcmDatabaseType);
                            };
                            break;
                    }
                }
                else
                {
                    _saveResponse = Message.SyncMaintenance_Target_Unique_Warning;
                }

                return returnVal;
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());

                return false;
            }
        }

        /// <summary>
        /// Save the Sync Target
        /// </summary>
        /// <param name="result">Has the validation passed or failed</param>
        private void SaveTarget(bool result)
        {
            if (result)
            {
                //Set the active flag to true if it's new
                if (this.CurrentSyncTarget.IsNew)
                {
                    this.CurrentSyncTarget.IsActive = true;
                }
                //Save sync target
                this.CurrentSyncTarget = this.CurrentSyncTarget.Save();

                //Perform save action
                switch (this.CurrentSaveAction)
                {
                    case SaveAction.SaveAndClose:
                        //close the attached window
                        OnCloseWindow();
                        break;

                    case SaveAction.SaveAndNew:
                        //Reset local connection string properties
                        ServerName = String.Empty;
                        DatabaseName = String.Empty;
                        FullFileName = String.Empty;
                        FileLocation = String.Empty;
                        FileName = String.Empty;

                        //Create new target
                        this.CurrentSyncTarget = SyncTarget.NewSyncTarget();
                        //set the variable to show that the ui has created this object, not the service
                        this.CurrentSyncTarget.UISource = true;
                        break;
                }
            }
            else
            {
                SaveResponse = _saveResponse;
                IsSaved = false;
            }
        }

        /// <summary>
        /// Sets the sync Target details for current type if not set and is available
        /// </summary>
        private void SyncTargetLoadDetails()
        {
            string connectionString;
            string[] connectionDetails;
            string[] connectDetails;

            //Set connection details if available
            if (!String.IsNullOrEmpty(CurrentSyncTarget.ConnectionString))
            {
                connectionString = CurrentSyncTarget.ConnectionString;

                switch (CurrentSyncTarget.TargetType)
                {
                    case SyncTargetType.Mssql:
                        //Main Connection String
                        connectionDetails = connectionString.ToString().Split((";").ToCharArray());

                        //Server String
                        connectDetails = connectionDetails[0].ToString().Split(("=").ToCharArray());
                        ServerName = connectDetails[1];

                        //Database String
                        connectDetails = connectionDetails[1].ToString().Split(("=").ToCharArray());
                        DatabaseName = connectDetails[1];
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the changed sync Target connection string
        /// </summary>
        private void SyncTargetChanged()
        {
            SaveResponse = String.Empty;
            IsSaved = true;

            Boolean canCompare = false;

            switch (CurrentSyncTarget.TargetType)
            {
                case SyncTargetType.Mssql:
                    _connectionString = String.Format(
                        "Server={0};Database={1};Integrated Security=True",
                        ServerName,
                        DatabaseName);

                    canCompare = !String.IsNullOrEmpty(ServerName) && !String.IsNullOrEmpty(DatabaseName);
                    break;
            }

            if (canCompare)
            {
                if (String.Compare(CurrentSyncTarget.ConnectionString, _connectionString) != 0)
                {
                    CurrentSyncTarget.ConnectionString = _connectionString;
                }
            }
        }

        /// <summary>
        /// Updates the value of IsCustomPerformanceSetup property depending on number of sync target performances 
        /// </summary>
        private void UpdateIsCustomPerformanceSetupValue()
        {
            if (_currentSyncTarget == null || (_currentSyncTarget != null && !_currentSyncTarget.SyncTargetGfsPerformances.Any()))
            {
                this.IsCustomPerformanceSetup = false;
            }
            else
            {
                this.IsCustomPerformanceSetup = true;
            }
        }
        #endregion

        #region Disposing
        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        /// <param name="disposing">Indicates if being called from dispose</param>
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _syncTargets = null;
                    _currentSyncTarget = null;
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }
}