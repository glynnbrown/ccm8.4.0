﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup/A.Kuszyk
//      Initial version.
// V8-24971 : L.Hodson
//  Changes to PlanogramPosition fields
// V8-24972 : L.Hodson
//  Added product image fields for display, tray,  pointofpurchase, alternate and case
// V8-25916 : N.Foster
//  Added PackageRowVersion, PackageDateDeleted. Removed PackageCreatedBy, PackageLastModifiedBy, PackageDescription
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added Planogram, PlanogramFixtureItem, PlanogramAssemblyComponent, PlanogramFixtureAssembly, PlanogramFixtureComponent, PlanogramComponent 
//  MetaData properties.
// V8-25881 : A.Probyn
//  Replaced PackagePlanogramCount with PackageMetaPlanogramCount
// V8-26041 : A.Kuszyk
// Added additional Product fields to PlanogramProduct.
// V8-26147 : L.Ineson
//  Added CustomAttributeData fields.
// V8-26269 : A.Kuszyk
//  Added PlanogramPerformance, PlanogramPerformanceData and PlanogramPerformanceMetric.
// V8-26426 : A.Kuszyk
//  Added PlanogramAssortment related fields.
// V8-26091 : L.Ineson
//  Added additional FaceThickness properties
//  Removed FaceTopOffset and CanMultiMerchX,Y,Z
// V8-26573 : L.Luong
//  Added PlanogramValidationTemplate and related fields
// V8-26709 : L.Ineson
//  Removed old  planogram unit of measure property.
// V8-26706 : L.Luong
//  Added PlanogramConsumerDecisionTree and related fields
// V8-26836 : L.Luong
//  Added PlanogramEventLog fields
// V8-26799 : A.Kuszyk
//  Removed PlanogramAssortmentProduct LocationCode property.
// V8-26954 : A.Silva 
//      Added ResultType field name to ValidationTemplateGroup and Metric.
// V8-26891 : L.Ineson
//  Added PlanogramBlocking
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
// V8-27058 : A.Probyn
//  Added new meta data properties
//  Added PackageDateMetadataCalculated
// V8-26812 : A.Silva
//  Added ValidationType field name to ValidationTemplateGroup and Metric.
// V8-27237 : A.Silva
//      Added PackageDateValidationDataCalculated.
// V8-27154 : L.Luong
//  Added Planogram ProductPlacement X,Y,Z
// V8-27404 : L.Luong
//   Changed LevelType to EntryType and added Content to PlanogramEventLog
// V8-27153 : A.Silva
//      Added PlanogramRenumberingStrategy fields.
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber field to PlanogramFixtureComponent.
//      Added ComponentSequenceNumber field to PlanogramAssemblyComponent.
//      Added PositionSequenceNumber field to PlanogramPosition.
// V8-27476 : L.Luong
//  Added BlockPlacementXType and BlockPlacementYType to PlanogramBlocking
// V8-27605 : Added PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser to PlanogramAssemblyComponentMetaTotalComponentCollisions.
// V8-27647 : L.Luong
//  Added PlanogramInventory
// V8-27759 : A.Kuszyk
//  Added AffectedType and AffectedId to PlanogramEventLog fields.
// V8-27411 : M.Pettit
//  Added Planogram.UserName property
// V8-24779 : D.Pleasance
//  Added Planogram.SourcePath
// V8-28059 : A.Kuszyk
//  Added WorkpackageSource, TaskSource and EventId fields to PlanogramEventLog.
// V8-27783 : M.Brumby
//	Added Planogram.UniqueContentReference
#endregion
#region Version History: CCM801
// V8-27636 : L.Ineson
//  Added PlanogramProduct MetaTotalUnits
#endregion
#region Version History: CCM802

// V8-28987 : I.George
// Added LocationCode, LocationName, ClusterSchemeName and ClusterName properties
// V8-28996 : A.Silva
//      Added PlanogramSequence, PlanogramSequenceGroup and PlanogramSequenceGroupProduct.
// V8-28766 : J.Pickup
//      Added PlanogramPosistionIsManuallyPlaced.
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
// V8-29023 : M.Pettit
//  Added Merchandisable Depth
// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-29028 : L.Ineson
//  Added position squeeze properties
// V8-28840 : L.Luong 
//  Added Package EntityId property
// V8-29054 : M.Pettit
//  Added Position meta validation fields
// V8-28811 : L.Luong
//  Added PlanogramProduct, PlanogramPerformanceData, PlanogramFixureComponent, PlanogramAssemblyComponent meta data fields
// V8-29232 : A.Silva
//      Added PlanogramProductSequenceNumber.
// V8-30763 :I.George
//  Added ConsumerDecisionTreeNode MetaData Fields

#endregion
#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added NotchNumber property
// V8-28491 : L.Luong
//  Added PlanogramProductMetaIsInMasterData
// V8-29590 : A.Probyn
//  Added PlanogramEventLog_Score, 
//  Added Planogram_MetaNoOfErrors, Planogram_MetaNoOfWarnings, Planogram_MetaTotalErrorScore , Planogram_MetaHighestErrorScore 
// V8-29682 : A.Probyn
//  Added PlanogramPerformanceMetric_AggregationType 
#endregion
#region Version History: CCM810
// V8-29778 : L.Ineson
//  Added more metadata
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents
#endregion
#region Version History: CCM820
// V8-29998 : I.George
//	Added PlanogramPerformanceData_MetaP1Rank to PlanogramPerformanceData_MetaP20Rank fields
// V8-30773 : J.Pickup
//	InventoryMetricType added.
// V8-30762 : I.George
//  Added ConsumerDecisionTreeNode MetaData property
// V8-30763 :I.George
//	Added MetaCDTNode field to PlanogramProduct 
// V8-30765 :I.George
//	Added PlanogramBlockingGroup metaData fields
// V8-30705 : A.Probyn
//  Added PlanogramAssortmentProductDelistedDueToAssortmentRule
//  Added new Assortment rule related meta data fields to Planogram, PlanogramProduct
// V8-30992 : A.Probyn
//  Added PlanogramPerformanceDataMinimumInventoryUnits
// V8-30737 : M.Shelley
//  Added new metadata fields for selected CDT nodes
// V8-31164 : D.Pleasance
//  Added PlanogramPositionSequenceNumber \ PlanogramPositionSequenceColour
// V8-30737 : M.Shelley
//  Added Meta performance fields P1-P20 and MetaP1Percentage-MetaP20Percentage to PlanogramBlockingGroup
#endregion
#region Version History: CCM830
// V8-31550 : A.Probyn
//  Added new PlanogramAssortmentProductBuddy, PlanogramAssortmentLocationBuddy fields
// V8-31551 : A.Probyn
//  Added new PlanogramAssortmentInventoryRule fields
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31819 : A.Silva
//  Added new PlanogramComparison, PlanogramComparisonField, PlanogramComparisonResult, PlanogramComparisonItem, PlanogramComparisonFieldValue fields.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts to PlanogramComparison.
// V8-31947 : A.Silva
//  Added MetaComparisonStatus to PlanogramProduct and PlanogramPosition.
// V8-31963 : A.Silva
//  Added Display field to PlanogramComparisonField.
// V8-32005 : A.Silva
//  Added Name, Description and DataOrderType to PlanogramComparison.
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroup.
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
// V8-32396 : A.Probyn
//  Added PlanogramMetaNumberOfDelistFamilyRulesBroken, PlanogramProductMetaIsDelistFamilyRuleBroken
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added MetaIsBuddied
//  Added MetaCountOfProductsBuddied
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName & MetaSequenceGroupName.
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
#endregion
#region Version History: CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Dal.Pog.Schema
{
    internal partial class FieldNames
    {
        #region SchemaVersion

        public const String SchemaVersionMajorVersion = "MajorVersion";
        public const String SchemaVersionMinorVersion = "MinorVersion";

        #endregion

        #region CustomAttributeData

        public const String CustomAttributeDataId = "Id";
        public const String CustomAttributeDataParentId = "ParentId";
        public const String CustomAttributeDataParentType = "ParentType";

        public const String CustomAttributeDataText1 = "Text1";
        public const String CustomAttributeDataText2 = "Text2";
        public const String CustomAttributeDataText3 = "Text3";
        public const String CustomAttributeDataText4 = "Text4";
        public const String CustomAttributeDataText5 = "Text5";
        public const String CustomAttributeDataText6 = "Text6";
        public const String CustomAttributeDataText7 = "Text7";
        public const String CustomAttributeDataText8 = "Text8";
        public const String CustomAttributeDataText9 = "Text9";
        public const String CustomAttributeDataText10 = "Text10";
        public const String CustomAttributeDataText11 = "Text11";
        public const String CustomAttributeDataText12 = "Text12";
        public const String CustomAttributeDataText13 = "Text13";
        public const String CustomAttributeDataText14 = "Text14";
        public const String CustomAttributeDataText15 = "Text15";
        public const String CustomAttributeDataText16 = "Text16";
        public const String CustomAttributeDataText17 = "Text17";
        public const String CustomAttributeDataText18 = "Text18";
        public const String CustomAttributeDataText19 = "Text19";
        public const String CustomAttributeDataText20 = "Text20";
        public const String CustomAttributeDataText21 = "Text21";
        public const String CustomAttributeDataText22 = "Text22";
        public const String CustomAttributeDataText23 = "Text23";
        public const String CustomAttributeDataText24 = "Text24";
        public const String CustomAttributeDataText25 = "Text25";
        public const String CustomAttributeDataText26 = "Text26";
        public const String CustomAttributeDataText27 = "Text27";
        public const String CustomAttributeDataText28 = "Text28";
        public const String CustomAttributeDataText29 = "Text29";
        public const String CustomAttributeDataText30 = "Text30";
        public const String CustomAttributeDataText31 = "Text31";
        public const String CustomAttributeDataText32 = "Text32";
        public const String CustomAttributeDataText33 = "Text33";
        public const String CustomAttributeDataText34 = "Text34";
        public const String CustomAttributeDataText35 = "Text35";
        public const String CustomAttributeDataText36 = "Text36";
        public const String CustomAttributeDataText37 = "Text37";
        public const String CustomAttributeDataText38 = "Text38";
        public const String CustomAttributeDataText39 = "Text39";
        public const String CustomAttributeDataText40 = "Text40";
        public const String CustomAttributeDataText41 = "Text41";
        public const String CustomAttributeDataText42 = "Text42";
        public const String CustomAttributeDataText43 = "Text43";
        public const String CustomAttributeDataText44 = "Text44";
        public const String CustomAttributeDataText45 = "Text45";
        public const String CustomAttributeDataText46 = "Text46";
        public const String CustomAttributeDataText47 = "Text47";
        public const String CustomAttributeDataText48 = "Text48";
        public const String CustomAttributeDataText49 = "Text49";
        public const String CustomAttributeDataText50= "Text50";

        public const String CustomAttributeDataValue1 = "Value1";
        public const String CustomAttributeDataValue2 = "Value2";
        public const String CustomAttributeDataValue3 = "Value3";
        public const String CustomAttributeDataValue4 = "Value4";
        public const String CustomAttributeDataValue5 = "Value5";
        public const String CustomAttributeDataValue6 = "Value6";
        public const String CustomAttributeDataValue7 = "Value7";
        public const String CustomAttributeDataValue8 = "Value8";
        public const String CustomAttributeDataValue9 = "Value9";
        public const String CustomAttributeDataValue10 = "Value10";
        public const String CustomAttributeDataValue11 = "Value11";
        public const String CustomAttributeDataValue12 = "Value12";
        public const String CustomAttributeDataValue13 = "Value13";
        public const String CustomAttributeDataValue14 = "Value14";
        public const String CustomAttributeDataValue15 = "Value15";
        public const String CustomAttributeDataValue16 = "Value16";
        public const String CustomAttributeDataValue17 = "Value17";
        public const String CustomAttributeDataValue18 = "Value18";
        public const String CustomAttributeDataValue19 = "Value19";
        public const String CustomAttributeDataValue20 = "Value20";
        public const String CustomAttributeDataValue21 = "Value21";
        public const String CustomAttributeDataValue22 = "Value22";
        public const String CustomAttributeDataValue23 = "Value23";
        public const String CustomAttributeDataValue24 = "Value24";
        public const String CustomAttributeDataValue25 = "Value25";
        public const String CustomAttributeDataValue26 = "Value26";
        public const String CustomAttributeDataValue27 = "Value27";
        public const String CustomAttributeDataValue28 = "Value28";
        public const String CustomAttributeDataValue29 = "Value29";
        public const String CustomAttributeDataValue30 = "Value30";
        public const String CustomAttributeDataValue31 = "Value31";
        public const String CustomAttributeDataValue32 = "Value32";
        public const String CustomAttributeDataValue33 = "Value33";
        public const String CustomAttributeDataValue34 = "Value34";
        public const String CustomAttributeDataValue35 = "Value35";
        public const String CustomAttributeDataValue36 = "Value36";
        public const String CustomAttributeDataValue37 = "Value37";
        public const String CustomAttributeDataValue38 = "Value38";
        public const String CustomAttributeDataValue39 = "Value39";
        public const String CustomAttributeDataValue40 = "Value40";
        public const String CustomAttributeDataValue41 = "Value41";
        public const String CustomAttributeDataValue42 = "Value42";
        public const String CustomAttributeDataValue43 = "Value43";
        public const String CustomAttributeDataValue44 = "Value44";
        public const String CustomAttributeDataValue45 = "Value45";
        public const String CustomAttributeDataValue46 = "Value46";
        public const String CustomAttributeDataValue47 = "Value47";
        public const String CustomAttributeDataValue48 = "Value48";
        public const String CustomAttributeDataValue49 = "Value49";
        public const String CustomAttributeDataValue50 = "Value50";

        public const String CustomAttributeDataFlag1 = "Flag1";
        public const String CustomAttributeDataFlag2 = "Flag2";
        public const String CustomAttributeDataFlag3 = "Flag3";
        public const String CustomAttributeDataFlag4 = "Flag4";
        public const String CustomAttributeDataFlag5 = "Flag5";
        public const String CustomAttributeDataFlag6 = "Flag6";
        public const String CustomAttributeDataFlag7 = "Flag7";
        public const String CustomAttributeDataFlag8 = "Flag8";
        public const String CustomAttributeDataFlag9 = "Flag9";
        public const String CustomAttributeDataFlag10 = "Flag10";

        public const String CustomAttributeDataDate1 = "Date1";
        public const String CustomAttributeDataDate2 = "Date2";
        public const String CustomAttributeDataDate3 = "Date3";
        public const String CustomAttributeDataDate4 = "Date4";
        public const String CustomAttributeDataDate5 = "Date5";
        public const String CustomAttributeDataDate6 = "Date6";
        public const String CustomAttributeDataDate7 = "Date7";
        public const String CustomAttributeDataDate8 = "Date8";
        public const String CustomAttributeDataDate9 = "Date9";
        public const String CustomAttributeDataDate10 = "Date10";

        public const String CustomAttributeDataNote1 = "Note1";
        public const String CustomAttributeDataNote2 = "Note2";
        public const String CustomAttributeDataNote3 = "Note3";
        public const String CustomAttributeDataNote4 = "Note4";
        public const String CustomAttributeDataNote5 = "Note5";

        #endregion

        #region Package

        public const String PackageId = "Id";
        public const String PackageRowVersion = "RowVersion";
        public const String PackageEntityId = "EntityId";
        public const String PackageName = "Name";
        public const String PackageDateCreated = "DateCreated";
        public const String PackageDateLastModified = "DateLastModified";
        public const String PackageDateDeleted = "DateDeleted";
        public const String PackageMetaPlanogramCount = "MetaPlanogramCount";
        public const String PackageDateMetadataCalculated = "DateMetadataCalculated";
        public const String PackageDateValidationDataCalculated = "DateValidationDataCalculated";

        #endregion

        #region Planogram

        public const String PlanogramId = "Id";
        public const String PlanogramPackageId = "PackageId";
        public const String PlanogramUniqueContentReference = "UniqueContentReference";
        public const String PlanogramName = "Name";
        public const String PlanogramHeight = "Height";
        public const String PlanogramWidth = "Width";
        public const String PlanogramDepth = "Depth";
        public const String PlanogramUserName = "UserName";
        public const String PlanogramLengthUnitsOfMeasure = "LengthUnitsOfMeasure";
        public const String PlanogramAreaUnitsOfMeasure = "AreaUnitsOfMeasure";
        public const String PlanogramVolumeUnitsOfMeasure = "VolumeUnitsOfMeasure";
        public const String PlanogramWeightUnitsOfMeasure = "WeightUnitsOfMeasure";
        public const String PlanogramAngleUnitsOfMeasure = "AngleUnitsOfMeasure";
        public const String PlanogramCurrencyUnitsOfMeasure = "CurrencyUnitsOfMeasure";
        public const String PlanogramProductPlacementX = "ProductPlacementX";
        public const String PlanogramProductPlacementY = "ProductPlacementY";
        public const String PlanogramProductPlacementZ = "ProductPlacementZ";
        public const String PlanogramCategoryCode = "CategoryCode";
        public const String PlanogramCategoryName = "CategoryName";
        public const String PlanogramSourcePath = "SourcePath";
        public const String PlanogramMetaBayCount = "MetaBayCount";
        public const String PlanogramMetaUniqueProductCount = "MetaUniqueProductCount";
        public const String PlanogramMetaComponentCount = "MetaComponentCount";
        public const String PlanogramMetaTotalMerchandisableLinearSpace = "MetaTotalMerchandisableLinearSpace";
        public const String PlanogramMetaTotalMerchandisableAreaSpace = "MetaTotalMerchandisableAreaSpace";
        public const String PlanogramMetaTotalMerchandisableVolumetricSpace = "MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramMetaTotalLinearWhiteSpace = "MetaTotalLinearWhiteSpace";
        public const String PlanogramMetaTotalAreaWhiteSpace = "MetaTotalAreaWhiteSpace";
        public const String PlanogramMetaTotalVolumetricWhiteSpace = "MetaTotalVolumetricWhiteSpace";
        public const String PlanogramMetaProductsPlaced = "MetaProductsPlaced";
        public const String PlanogramMetaProductsUnplaced = "MetaProductsUnplaced";
        public const String PlanogramMetaNewProducts = "MetaNewProducts";
        public const String PlanogramMetaChangesFromPreviousCount = "MetaChangesFromPreviousCount";
        public const String PlanogramMetaChangeFromPreviousStarRating = "MetaChangeFromPreviousStarRating";
        public const String PlanogramMetaBlocksDropped = "MetaBlocksDropped";
        public const String PlanogramMetaNotAchievedInventory = "MetaNotAchievedInventory";
        public const String PlanogramMetaTotalFacings = "MetaTotalFacings";
        public const String PlanogramMetaAverageFacings = "MetaAverageFacings";
        public const String PlanogramMetaTotalUnits = "MetaTotalUnits";
        public const String PlanogramMetaAverageUnits = "MetaAverageUnits";
        public const String PlanogramMetaMinDos = "MetaMinDos";
        public const String PlanogramMetaMaxDos = "MetaMaxDos";
        public const String PlanogramMetaAverageDos = "MetaAverageDos";
        public const String PlanogramMetaMinCases = "MetaMinCases";
        public const String PlanogramMetaAverageCases = "MetaAverageCases";
        public const String PlanogramMetaMaxCases = "MetaMaxCases";
        public const String PlanogramMetaSpaceToUnitsIndex = "MetaSpaceToUnitsIndex";
        public const String PlanogramMetaTotalComponentCollisions = "MetaTotalComponentCollisions";
        public const String PlanogramMetaTotalComponentsOverMerchandisedDepth = "MetaTotalComponentsOverMerchandisedDepth";
        public const String PlanogramMetaTotalComponentsOverMerchandisedHeight =  "MetaTotalComponentsOverMerchandisedHeight";
        public const String PlanogramMetaTotalComponentsOverMerchandisedWidth = "MetaTotalComponentsOverMerchandisedWidth";
        public const String PlanogramMetaHasComponentsOutsideOfFixtureArea = "MetaHasComponentsOutsideOfFixtureArea";
        public const String PlanogramMetaTotalPositionCollisions = "MetaTotalPositionCollisions";
        public const String PlanogramMetaTotalFrontFacings = "MetaTotalFrontFacings";
        public const String PlanogramMetaAverageFrontFacings = "MetaAverageFrontFacings";
        public const String PlanogramMetaNoOfErrors = "MetaNoOfErrors";
        public const String PlanogramMetaNoOfWarnings = "MetaNoOfWarnings";
        public const String PlanogramMetaTotalErrorScore = "MetaTotalErrorScore";
        public const String PlanogramMetaHighestErrorScore = "MetaHighestErrorScore";
        public const String PlanogramLocationCode = "LocationCode";
        public const String PlanogramLocationName = "LocationName";
        public const String PlanogramClusterSchemeName = "ClusterSchemeName";
        public const String PlanogramClusterName = "ClusterName";
        public const String PlanogramStatus = "Status";
        public const String PlanogramDateWip = "DateWip";
        public const String PlanogramDateApproved = "DateApproved";
        public const String PlanogramDateArchived = "DateArchived";
        public const String PlanogramMetaCountOfProductNotAchievedCases = "MetaCountOfProductNotAchievedCases";
        public const String PlanogramMetaCountOfProductNotAchievedDOS = "MetaCountOfProductNotAchievedDOS";
        public const String PlanogramMetaCountOfProductOverShelfLifePercent = "MetaCountOfProductOverShelfLifePercent";
        public const String PlanogramMetaCountOfProductNotAchievedDeliveries = "MetaCountOfProductNotAchievedDeliveries";
        public const String PlanogramMetaPercentOfProductNotAchievedCases = "MetaPercentOfProductNotAchievedCases";
        public const String PlanogramMetaPercentOfProductNotAchievedDOS = "MetaPercentOfProductNotAchievedDOS";
        public const String PlanogramMetaPercentOfProductOverShelfLifePercent = "MetaPercentOfProductOverShelfLifePercent";
        public const String PlanogramMetaPercentOfProductNotAchievedDeliveries = "MetaPercentOfProductNotAchievedDeliveries";
        public const String PlanogramMetaCountOfPositionsOutsideOfBlockSpace = "MetaCountOfPositionsOutsideOfBlockSpace";
        public const String PlanogramMetaPercentOfPositionsOutsideOfBlockSpace = "MetaPercentOfPositionsOutsideOfBlockSpace";
        public const String PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment = "MetaPercentOfPlacedProductsRecommendedInAssortment";
        public const String PlanogramMetaCountOfPlacedProductsRecommendedInAssortment = "MetaCountOfPlacedProductsRecommendedInAssortment";
        public const String PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment = "MetaCountOfPlacedProductsNotRecommendedInAssortment";
        public const String PlanogramMetaCountOfRecommendedProductsInAssortment = "MetaCountOfRecommendedProductsInAssortment";
        public const String PlanogramPlanogramType = "PlanogramType";
        public const String PlanogramMetaPercentOfProductNotAchievedInventory = "MetaPercentOfProductNotAchievedInventory";
        public const String PlanogramMetaNoOfInformationEvents = "MetaNoOfInformationEvents";
        public const String PlanogramMetaNoOfDebugEvents = "MetaNoOfDebugEvents";
        public const String PlanogramHighlightSequenceStrategy = "HighlightSequenceStrategy";
        public const String PlanogramMetaNumberOfAssortmentRulesBroken = "MetaNumberOfAssortmentRulesBroken";
        public const String PlanogramMetaNumberOfProductRulesBroken = "MetaNumberOfProductRulesBroken";
        public const String PlanogramMetaNumberOfFamilyRulesBroken = "MetaNumberOfFamilyRulesBroken";
        public const String PlanogramMetaNumberOfInheritanceRulesBroken = "MetaNumberOfInheritanceRulesBroken";
        public const String PlanogramMetaNumberOfLocalProductRulesBroken = "MetaNumberOfLocalProductRulesBroken";
        public const String PlanogramMetaNumberOfDistributionRulesBroken = "MetaNumberOfDistributionRulesBroken";
        public const String PlanogramMetaNumberOfCoreRulesBroken = "MetaNumberOfCoreRulesBroken";
        public const String PlanogramMetaPercentageOfAssortmentRulesBroken = "MetaPercentageOfAssortmentRulesBroken";
        public const String PlanogramMetaPercentageOfProductRulesBroken = "MetaPercentageOfProductRulesBroken";
        public const String PlanogramMetaPercentageOfFamilyRulesBroken = "MetaPercentageOfFamilyRulesBroken";
        public const String PlanogramMetaPercentageOfInheritanceRulesBroken = "MetaPercentageOfInheritanceRulesBroken";
        public const String PlanogramMetaPercentageOfLocalProductRulesBroken = "MetaPercentageOfLocalProductRulesBroken";
        public const String PlanogramMetaPercentageOfDistributionRulesBroken = "MetaPercentageOfDistributionRulesBroken";
        public const String PlanogramMetaPercentageOfCoreRulesBroken = "MetaPercentageOfCoreRulesBroken";
        public const String PlanogramMetaNumberOfDelistProductRulesBroken = "MetaNumberOfDelistProductRulesBroken";
        public const String PlanogramMetaNumberOfForceProductRulesBroken = "MetaNumberOfForceProductRulesBroken";
        public const String PlanogramMetaNumberOfPreserveProductRulesBroken = "MetaNumberOfPreserveProductRulesBroken";
        public const String PlanogramMetaNumberOfMinimumHurdleProductRulesBroken = "MetaNumberOfMinimumHurdleProductRulesBroken";
        public const String PlanogramMetaNumberOfMaximumProductFamilyRulesBroken = "MetaNumberOfMaximumProductFamilyRulesBroken";
        public const String PlanogramMetaNumberOfMinimumProductFamilyRulesBroken = "MetaNumberOfMinimumProductFamilyRulesBroken";
        public const String PlanogramMetaNumberOfDependencyFamilyRulesBroken = "MetaNumberOfDependencyFamilyRulesBroken";
        public const String PlanogramMetaNumberOfDelistFamilyRulesBroken = "MetaNumberOfDelistFamilyRulesBroken";
        public const String PlanogramMetaCountOfProductsBuddied = "MetaCountOfProductsBuddied";
        #endregion

        #region PlanogramAnnotation

        public const String PlanogramAnnotationId = "Id";
        public const String PlanogramAnnotationPlanogramId = "PlanogramId";
        public const String PlanogramAnnotationPlanogramFixtureItemId = "PlanogramFixtureItemId";
        public const String PlanogramAnnotationPlanogramFixtureAssemblyId = "PlanogramFixtureAssemblyId";
        public const String PlanogramAnnotationPlanogramFixtureComponentId = "PlanogramFixtureComponentId";
        public const String PlanogramAnnotationPlanogramAssemblyComponentId = "PlanogramAssemblyComponentId";
        public const String PlanogramAnnotationPlanogramSubComponentId = "PlanogramSubComponentId";
        public const String PlanogramAnnotationPlanogramPositionId = "PlanogramPositionId";
        public const String PlanogramAnnotationAnnotationType = "AnnotationType";
        public const String PlanogramAnnotationText = "Text";
        public const String PlanogramAnnotationX = "X";
        public const String PlanogramAnnotationY = "Y";
        public const String PlanogramAnnotationZ = "Z";
        public const String PlanogramAnnotationHeight = "Height";
        public const String PlanogramAnnotationWidth = "Width";
        public const String PlanogramAnnotationDepth = "Depth";
        public const String PlanogramAnnotationBorderColour = "BorderColour";
        public const String PlanogramAnnotationBorderThickness = "BorderThickness";
        public const String PlanogramAnnotationBackgroundColour = "BackgroundColour";
        public const String PlanogramAnnotationFontColour = "FontColour";
        public const String PlanogramAnnotationFontSize = "FontSize";
        public const String PlanogramAnnotationFontName = "FontName";
        public const String PlanogramAnnotationCanReduceFontToFit = "CanReduceFontToFit";

        #endregion

        #region PlanogramAssemblyComponent

        public const String PlanogramAssemblyComponentId = "Id";
        public const String PlanogramAssemblyComponentPlanogramAssemblyId = "PlanogramAssemblyId";
        public const String PlanogramAssemblyComponentPlanogramComponentId = "PlanogramComponentId";
        public const String PlanogramAssemblyComponentComponentSequenceNumber = "ComponentSequenceNumber";
        public const String PlanogramAssemblyComponentX = "X";
        public const String PlanogramAssemblyComponentY = "Y";
        public const String PlanogramAssemblyComponentZ = "Z";
        public const String PlanogramAssemblyComponentSlope = "Slope";
        public const String PlanogramAssemblyComponentAngle = "Angle";
        public const String PlanogramAssemblyComponentRoll = "Roll";
        public const String PlanogramAssemblyComponentMetaComponentCount = "MetaComponentCount";
        public const String PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace = "MetaTotalMerchandisableLinearSpace";
        public const String PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace = "MetaTotalMerchandisableAreaSpace";
        public const String PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace = "MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramAssemblyComponentMetaTotalLinearWhiteSpace = "MetaTotalLinearWhiteSpace";
        public const String PlanogramAssemblyComponentMetaTotalAreaWhiteSpace = "MetaTotalAreaWhiteSpace";
        public const String PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace = "MetaTotalVolumetricWhiteSpace";
        public const String PlanogramAssemblyComponentMetaProductsPlaced = "MetaProductsPlaced";
        public const String PlanogramAssemblyComponentMetaNewProducts = "MetaNewProducts";
        public const String PlanogramAssemblyComponentMetaChangesFromPreviousCount = "MetaChangesFromPreviousCount";
        public const String PlanogramAssemblyComponentMetaChangeFromPreviousStarRating = "MetaChangeFromPreviousStarRating";
        public const String PlanogramAssemblyComponentMetaBlocksDropped = "MetaBlocksDropped";
        public const String PlanogramAssemblyComponentMetaNotAchievedInventory = "MetaNotAchievedInventory";
        public const String PlanogramAssemblyComponentMetaTotalFacings = "MetaTotalFacings";
        public const String PlanogramAssemblyComponentMetaAverageFacings = "MetaAverageFacings";
        public const String PlanogramAssemblyComponentMetaTotalUnits = "MetaTotalUnits";
        public const String PlanogramAssemblyComponentMetaAverageUnits = "MetaAverageUnits";
        public const String PlanogramAssemblyComponentMetaMinDos = "MetaMinDos";
        public const String PlanogramAssemblyComponentMetaMaxDos = "MetaMaxDos";
        public const String PlanogramAssemblyComponentMetaAverageDos = "MetaAverageDos";
        public const String PlanogramAssemblyComponentMetaMinCases = "MetaMinCases";
        public const String PlanogramAssemblyComponentMetaAverageCases = "MetaAverageCases";
        public const String PlanogramAssemblyComponentMetaMaxCases = "MetaMaxCases";
        public const String PlanogramAssemblyComponentMetaSpaceToUnitsIndex = "MetaSpaceToUnitsIndex";
        public const String PlanogramAssemblyComponentMetaTotalFrontFacings = "MetaTotalFrontFacings";
        public const String PlanogramAssemblyComponentMetaAverageFrontFacings = "MetaAverageFrontFacings";
        public const String PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser = "MetaIsComponentSlopeWithNoRiser";
        public const String PlanogramAssemblyComponentMetaIsOverMerchandisedDepth = "MetaIsOverMerchandisedDepth";
        public const String PlanogramAssemblyComponentMetaIsOverMerchandisedHeight = "MetaIsOverMerchandisedHeight";
        public const String PlanogramAssemblyComponentMetaIsOverMerchandisedWidth = "MetaIsOverMerchandisedWidth";
        public const String PlanogramAssemblyComponentMetaTotalPositionCollisions = "MetaTotalPositionCollisions";
        public const String PlanogramAssemblyComponentMetaTotalComponentCollisions = "MetaTotalComponentCollisions";
        public const String PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled = "MetaPercentageLinearSpaceFilled";
        public const String PlanogramAssemblyComponentNotchNumber = "NotchNumber";
        public const String PlanogramAssemblyComponentMetaWorldX = "MetaWorldX";
        public const String PlanogramAssemblyComponentMetaWorldY = "MetaWorldY";
        public const String PlanogramAssemblyComponentMetaWorldZ = "MetaWorldZ";
        public const String PlanogramAssemblyComponentMetaWorldAngle = "MetaWorldAngle";
        public const String PlanogramAssemblyComponentMetaWorldSlope = "MetaWorldSlope";
        public const String PlanogramAssemblyComponentMetaWorldRoll = "MetaWorldRoll";
        public const String PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea = "MetaIsOutsideOfFixtureArea";

        #endregion

        #region PlanogramAssembly

        public const String PlanogramAssemblyId = "Id";
        public const String PlanogramAssemblyPlanogramId = "PlanogramId";
        public const String PlanogramAssemblyName = "Name";
        public const String PlanogramAssemblyHeight = "Height";
        public const String PlanogramAssemblyWidth = "Width";
        public const String PlanogramAssemblyDepth = "Depth";
        public const String PlanogramAssemblyNumberOfComponents = "NumberOfComponents";
        public const String PlanogramAssemblyTotalComponentCost = "TotalComponentCost";
        public const String PlanogramAssemblyExtendedData = "ExtendedData";

        #endregion

        #region PlanogramAssortment

        public const String PlanogramAssortmentId = "Id";
        public const String PlanogramAssortmentPlanogramId = "PlanogramId";
        public const String PlanogramAssortmentName = "Name";
        public const String PlanogramAssortmentExtendedData = "ExtendedData";

        #endregion

        #region PlanogramAssortmentInventoryRule

        public const String PlanogramAssortmentInventoryRuleId = "Id";
        public const String PlanogramAssortmentInventoryRuleProductGtin = "ProductGtin";
        public const String PlanogramAssortmentInventoryRuleCasePack = "CasePack";
        public const String PlanogramAssortmentInventoryRuleDaysOfSupply = "DaysOfSupply";
        public const String PlanogramAssortmentInventoryRuleShelfLife = "ShelfLife";
        public const String PlanogramAssortmentInventoryRuleReplenishmentDays = "ReplenishmentDays";
        public const String PlanogramAssortmentInventoryRuleWasteHurdleUnits = "WasteHurdleUnits";
        public const String PlanogramAssortmentInventoryRuleWasteHurdleCasePack = "WasteHurdleCasePack";
        public const String PlanogramAssortmentInventoryRuleMinUnits = "MinUnits";
        public const String PlanogramAssortmentInventoryRuleMinFacings = "MinFacings";
        public const String PlanogramAssortmentInventoryRulePlanogramAssortmentId = "PlanogramAssortmentId";
        
        #endregion

        #region PlanogramAssortmentLocalProduct

        public const String PlanogramAssortmentLocalProductId = "Id";
        public const String PlanogramAssortmentLocalProductPlanogramAssortmentId = "PlanogramAssortmentId";
        public const String PlanogramAssortmentLocalProductProductGtin = "ProductGtin";
        public const String PlanogramAssortmentLocalProductLocationCode = "LocationCode";
        public const String PlanogramAssortmentLocalProductExtendedData = "ExtendedData";

        #endregion

        #region PlanogramAssortmentLocationBuddy

        public const String PlanogramAssortmentLocationBuddyId = "Id";
        public const String PlanogramAssortmentLocationBuddyLocationCode = "LocationCode";
        public const String PlanogramAssortmentLocationBuddyTreatmentType = "TreatmentType";
        public const String PlanogramAssortmentLocationBuddyS1LocationCode = "S1LocationCode";
        public const String PlanogramAssortmentLocationBuddyS1Percentage = "S1Percentage";
        public const String PlanogramAssortmentLocationBuddyS2LocationCode = "S2LocationCode";
        public const String PlanogramAssortmentLocationBuddyS2Percentage = "S2Percentage";
        public const String PlanogramAssortmentLocationBuddyS3LocationCode = "S3LocationCode";
        public const String PlanogramAssortmentLocationBuddyS3Percentage = "S3Percentage";
        public const String PlanogramAssortmentLocationBuddyS4LocationCode = "S4LocationCode";
        public const String PlanogramAssortmentLocationBuddyS4Percentage = "S4Percentage";
        public const String PlanogramAssortmentLocationBuddyS5LocationCode = "S5LocationCode";
        public const String PlanogramAssortmentLocationBuddyS5Percentage = "S5Percentage";
        public const String PlanogramAssortmentLocationBuddyPlanogramAssortmentId = "PlanogramAssortmentId";
        
        #endregion

        #region PlanogramAssortmentProduct

        public const String PlanogramAssortmentProductId = "Id";
        public const String PlanogramAssortmentProductPlanogramAssortmentId = "PlanogramAssortmentId";
        public const String PlanogramAssortmentProductGTIN = "Gtin";
        public const String PlanogramAssortmentProductName = "Name";
        public const String PlanogramAssortmentProductIsRanged = "IsRanged";
        public const String PlanogramAssortmentProductRank = "Rank";
        public const String PlanogramAssortmentProductSegmentation = "Segmentation";
        public const String PlanogramAssortmentProductFacings = "Facings";
        public const String PlanogramAssortmentProductUnits = "Units";
        public const String PlanogramAssortmentProductProductTreatmentType = "ProductTreatmentType";
        public const String PlanogramAssortmentProductProductLocalizationType = "ProductLocalizationType";
        public const String PlanogramAssortmentProductComments = "Comments";
        public const String PlanogramAssortmentProductExactListFacings = "ExactListFacings";
        public const String PlanogramAssortmentProductExactListUnits = "ExactListUnits";
        public const String PlanogramAssortmentProductPreserveListFacings = "PreserveListFacings";
        public const String PlanogramAssortmentProductPreserveListUnits = "PreserveListUnits";
        public const String PlanogramAssortmentProductMaxListFacings = "MaxListFacings";
        public const String PlanogramAssortmentProductMaxListUnits = "MaxListUnits";
        public const String PlanogramAssortmentProductMinListFacings = "MinListFacings";
        public const String PlanogramAssortmentProductMinListUnits = "MinListUnits";
        public const String PlanogramAssortmentProductFamilyRuleName = "FamilyRuleName";
        public const String PlanogramAssortmentProductFamilyRuleType = "FamilyRuleType";
        public const String PlanogramAssortmentProductFamilyRuleValue = "FamilyRuleValue";
        public const String PlanogramAssortmentProductIsPrimaryRegionalProduct = "IsPrimaryRegionalProduct";
        public const String PlanogramAssortmentProductExtendedData = "ExtendedData";
        public const String PlanogramAssortmentProductDelistedDueToAssortmentRule = "DelistedDueToAssortmentRule";
        public const String PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram = "MetaIsProductPlacedOnPlanogram";


        #endregion

        #region PlanogramAssortmentProductBuddy

        public const String PlanogramAssortmentProductBuddyId = "Id";
        public const String PlanogramAssortmentProductBuddyProductGtin = "ProductGtin";
        public const String PlanogramAssortmentProductBuddySourceType = "SourceType";
        public const String PlanogramAssortmentProductBuddyTreatmentType = "TreatmentType";
        public const String PlanogramAssortmentProductBuddyTreatmentTypePercentage = "TreatmentTypePercentage";
        public const String PlanogramAssortmentProductBuddyProductAttributeType = "ProductAttributeType";
        public const String PlanogramAssortmentProductBuddyS1ProductGtin = "S1ProductGtin";
        public const String PlanogramAssortmentProductBuddyS1Percentage = "S1Percentage";
        public const String PlanogramAssortmentProductBuddyS2ProductGtin = "S2ProductGtin";
        public const String PlanogramAssortmentProductBuddyS2Percentage = "S2Percentage";
        public const String PlanogramAssortmentProductBuddyS3ProductGtin = "S3ProductGtin";
        public const String PlanogramAssortmentProductBuddyS3Percentage = "S3Percentage";
        public const String PlanogramAssortmentProductBuddyS4ProductGtin = "S4ProductGtin";
        public const String PlanogramAssortmentProductBuddyS4Percentage = "S4Percentage";
        public const String PlanogramAssortmentProductBuddyS5ProductGtin = "S5ProductGtin";
        public const String PlanogramAssortmentProductBuddyS5Percentage = "S5Percentage";
        public const String PlanogramAssortmentProductBuddyPlanogramAssortmentId = "PlanogramAssortmentId";

        #endregion

        #region PlanogramAssortmentRegion

        public const String PlanogramAssortmentRegionId = "Id";
        public const String PlanogramAssortmentRegionPlanogramAssortmentId = "PlanogramAssortmentId";
        public const String PlanogramAssortmentRegionName = "Name";
        public const String PlanogramAssortmentRegionExtendedData = "ExtendedData";

        #endregion

        #region PlanogramAssortmentRegionLocation

        public const String PlanogramAssortmentRegionLocationId = "Id";
        public const String PlanogramAssortmentRegionLocationPlanogramAssortmentRegionId = "PlanogramAssortmentRegionId";
        public const String PlanogramAssortmentRegionLocationLocationCode = "LocationCode";
        public const String PlanogramAssortmentRegionLocationExtendedData = "ExtendedData";

        #endregion

        #region PlanogramAssortmentRegionProduct

        public const String PlanogramAssortmentRegionProductId = "Id";
        public const String PlanogramAssortmentRegionProductPlanogramAssortmentRegionId = "PlanogramAssortmentRegionId";
        public const String PlanogramAssortmentRegionProductPrimaryProductGtin = "PrimaryProductGtin";
        public const String PlanogramAssortmentRegionProductRegionalProductGtin = "RegionalProductGtin";
        public const String PlanogramAssortmentRegionProductExtendedData = "ExtendedData";

        #endregion

        #region PlanogramBlockingDivider
        public const String PlanogramBlockingDividerId = "Id";
        public const String PlanogramBlockingDividerPlanogramBlockingId = "PlanogramBlockingId";
        public const String PlanogramBlockingDividerType = "Type";
        public const String PlanogramBlockingDividerLevel = "Level";
        public const String PlanogramBlockingDividerX = "X";
        public const String PlanogramBlockingDividerY = "Y";
        public const String PlanogramBlockingDividerLength = "Length";
        public const String PlanogramBlockingDividerIsSnapped = "IsSnapped";
        public const String PlanogramBlockingDividerIsLimited = "IsLimited";
        public const String PlanogramBlockingDividerLimitedPercentage = "LimitedPercentage";
        #endregion

        #region PlanogramBlocking
        public const String PlanogramBlockingId = "Id";
        public const String PlanogramBlockingPlanogramId = "PlanogramId";
        public const String PlanogramBlockingType = "Type";
        #endregion

        #region PlanogramBlockingGroup
        public const String PlanogramBlockingGroupId = "Id";
        public const String PlanogramBlockingGroupPlanogramBlockingId = "PlanogramBlockingId";
        public const String PlanogramBlockingGroupName = "Name";
        public const String PlanogramBlockingGroupColour = "Colour";
        public const String PlanogramBlockingGroupFillPatternType = "FillPatternType";
        public const String PlanogramBlockingGroupCanCompromiseSequence = "CanCompromiseSequence";
        public const String PlanogramBlockingGroupIsRestrictedByComponentType = "IsRestrictedByComponentType";
        public const String PlanogramBlockingGroupCanOptimise = "CanOptimise";
        public const String PlanogramBlockingGroupCanMerge = "CanMerge";
        public const String PlanogramBlockingGroupProductAssignType = "ProductAssignType";
        public const String PlanogramBlockingGroupIsLimited = "IsLimited";
        public const String PlanogramBlockingGroupLimitedPercentage = "LimitedPercentage";
        public const String PlanogramBlockingGroupBlockPlacementXType = "BlockPlacementXType";
        public const String PlanogramBlockingGroupBlockPlacementYType = "BlockPlacementYType";
        public const String PlanogramBlockingGroupBlockPlacementPrimaryType = "BlockPlacementPrimaryType";
        public const String PlanogramBlockingGroupBlockPlacementSecondaryType = "BlockPlacementSecondaryType";
        public const String PlanogramBlockingGroupBlockPlacementTertiaryType = "BlockPlacementTertiaryType";
        public const String PlanogramBlockingGroupTotalSpacePercentage = "TotalSpacePercentage";
        //metaData
        public const String PlanogramBlockingGroupMetaCountOfProducts = "PlanogramBlockingGroup_MetaCountOfProducts";
        public const String PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram = "MetaCountOfProductRecommendedOnPlanogram";
        public const String PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram = "MetaCountOfProductPlacedOnPlanogram";
        public const String PlanogramBlockingGroupMetaOriginalPercentAllocated = "MetaOriginalPercentAllocated";
        public const String PlanogramBlockingGroupMetaPerformancePercentAllocated = "MetaPerformancePercentAllocated";
        public const String PlanogramBlockingGroupMetaFinalPercentAllocated = "MetaFinalPercentAllocated";
        public const String PlanogramBlockingGroupMetaLinearProductPlacementPercent = "MetaLinearProductPlacmentPercent";
        public const String PlanogramBlockingGroupMetaAreaProductPlacementPercent = "MetaAreaProductPlacmentPercent";
        public const String PlanogramBlockingGroupMetaVolumetricProductPlacementPercent = "MetaVolumetricProductPlacmentPercent";

        public const String PlanogramBlockingGroupP1 = "PlanogramBlockingGroupP1";
        public const String PlanogramBlockingGroupP2 = "PlanogramBlockingGroupP2";
        public const String PlanogramBlockingGroupP3 = "PlanogramBlockingGroupP3";
        public const String PlanogramBlockingGroupP4 = "PlanogramBlockingGroupP4";
        public const String PlanogramBlockingGroupP5 = "PlanogramBlockingGroupP5";
        public const String PlanogramBlockingGroupP6 = "PlanogramBlockingGroupP6";
        public const String PlanogramBlockingGroupP7 = "PlanogramBlockingGroupP7";
        public const String PlanogramBlockingGroupP8 = "PlanogramBlockingGroupP8";
        public const String PlanogramBlockingGroupP9 = "PlanogramBlockingGroupP9";
        public const String PlanogramBlockingGroupP10 = "PlanogramBlockingGroupP10";
        public const String PlanogramBlockingGroupP11 = "PlanogramBlockingGroupP11";
        public const String PlanogramBlockingGroupP12 = "PlanogramBlockingGroupP12";
        public const String PlanogramBlockingGroupP13 = "PlanogramBlockingGroupP13";
        public const String PlanogramBlockingGroupP14 = "PlanogramBlockingGroupP14";
        public const String PlanogramBlockingGroupP15 = "PlanogramBlockingGroupP15";
        public const String PlanogramBlockingGroupP16 = "PlanogramBlockingGroupP16";
        public const String PlanogramBlockingGroupP17 = "PlanogramBlockingGroupP17";
        public const String PlanogramBlockingGroupP18 = "PlanogramBlockingGroupP18";
        public const String PlanogramBlockingGroupP19 = "PlanogramBlockingGroupP19";
        public const String PlanogramBlockingGroupP20 = "PlanogramBlockingGroupP20";

        public const String PlanogramBlockingGroupMetaP1Percentage = "PlanogramBlockingGroupMetaP1Percentage";
        public const String PlanogramBlockingGroupMetaP2Percentage = "PlanogramBlockingGroupMetaP2Percentage";
        public const String PlanogramBlockingGroupMetaP3Percentage = "PlanogramBlockingGroupMetaP3Percentage";
        public const String PlanogramBlockingGroupMetaP4Percentage = "PlanogramBlockingGroupMetaP4Percentage";
        public const String PlanogramBlockingGroupMetaP5Percentage = "PlanogramBlockingGroupMetaP5Percentage";
        public const String PlanogramBlockingGroupMetaP6Percentage = "PlanogramBlockingGroupMetaP6Percentage";
        public const String PlanogramBlockingGroupMetaP7Percentage = "PlanogramBlockingGroupMetaP7Percentage";
        public const String PlanogramBlockingGroupMetaP8Percentage = "PlanogramBlockingGroupMetaP8Percentage";
        public const String PlanogramBlockingGroupMetaP9Percentage = "PlanogramBlockingGroupMetaP9Percentage";
        public const String PlanogramBlockingGroupMetaP10Percentage = "PlanogramBlockingGroupMetaP10Percentage";
        public const String PlanogramBlockingGroupMetaP11Percentage = "PlanogramBlockingGroupMetaP11Percentage";
        public const String PlanogramBlockingGroupMetaP12Percentage = "PlanogramBlockingGroupMetaP12Percentage";
        public const String PlanogramBlockingGroupMetaP13Percentage = "PlanogramBlockingGroupMetaP13Percentage";
        public const String PlanogramBlockingGroupMetaP14Percentage = "PlanogramBlockingGroupMetaP14Percentage";
        public const String PlanogramBlockingGroupMetaP15Percentage = "PlanogramBlockingGroupMetaP15Percentage";
        public const String PlanogramBlockingGroupMetaP16Percentage = "PlanogramBlockingGroupMetaP16Percentage";
        public const String PlanogramBlockingGroupMetaP17Percentage = "PlanogramBlockingGroupMetaP17Percentage";
        public const String PlanogramBlockingGroupMetaP18Percentage = "PlanogramBlockingGroupMetaP18Percentage";
        public const String PlanogramBlockingGroupMetaP19Percentage = "PlanogramBlockingGroupMetaP19Percentage";
        public const String PlanogramBlockingGroupMetaP20Percentage = "PlanogramBlockingGroupMetaP20Percentage";

        #endregion

        #region PlanogramBlockingGroupField
        public const String PlanogramBlockingGroupFieldId = "Id";
        public const String PlanogramBlockingGroupFieldPlanogramBlockingGroupId = "PlanogramBlockingGroupId";
        public const String PlanogramBlockingGroupFieldName = "Name";
        public const String PlanogramBlockingGroupFieldFieldType = "FieldType";
        public const String PlanogramBlockingGroupFieldOperandType = "OperandType";
        public const String PlanogramBlockingGroupFieldTextValue = "TextValue";
        public const String PlanogramBlockingGroupFieldFlagValue = "FlagValue";
        public const String PlanogramBlockingGroupFieldNumericValue = "NumericValue";
        public const String PlanogramBlockingGroupFieldDateTimeValue = "DateTimeValue";
        #endregion

        #region PlanogramBlockingGroupProduct
        public const String PlanogramBlockingGroupProductId = "Id";
        public const String PlanogramBlockingGroupProductPlanogramBlockingGroupId = "PlanogramBlockingGroupId";
        public const String PlanogramBlockingGroupProductGtin = "Gtin";
        #endregion

        #region PlanogramBlockingLocation
        public const String PlanogramBlockingLocationId = "Id";
        public const String PlanogramBlockingLocationPlanogramBlockingId = "PlanogramBlockingId";
        public const String PlanogramBlockingLocationPlanogramBlockingGroupId = "PlanogramBlockingGroupId";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerTopId = "PlanogramBlockingDividerTopId";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerBottomId = "PlanogramBlockingDividerBottomId";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerLeftId = "PlanogramBlockingDividerLeftId";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerRightId = "PlanogramBlockingDividerRightId";
        public const String PlanogramBlockingLocationSpacePercentage = "PlanogramBlockingLocationSpacePercentage";
        #endregion

        #region PlanogramComparison 

        public const String PlanogramComparisonId = "Id";
        public const String PlanogramComparisonPlanogramId = "PlanogramId";
        public const String PlanogramComparisonName = "Name";
        public const String PlanogramComparisonDescription = "Description";
        public const String PlanogramComparisonDateLastCompared = "DateLastCompared";
        public const String PlanogramComparisonIgnoreNonPlacedProducts = "IgnoreNonPlacedProducts";
        public const String PlanogramComparisonDataOrderType = "DataOrderType";
        public const String PlanogramComparisonExtendedData = "ExtendedData";

        #endregion

        #region PlanogramComparisonField

        public const String PlanogramComparisonFieldId = "Id";
        public const String PlanogramComparisonFieldPlanogramComparisonId = "PlanogramComparisonId";
        public const String PlanogramComparisonFieldItemType = "ItemType";
        public const String PlanogramComparisonFieldDisplayName = "DisplayName";
        public const String PlanogramComparisonFieldNumber = "Number";
        public const String PlanogramComparisonFieldFieldPlaceholder = "FieldPlaceholder";
        public const String PlanogramComparisonFieldDisplay = "Display";
        public const String PlanogramComparisonFieldExtendedData = "ExtendedData";

        #endregion

        #region PlanogramComparisonResult

        public const String PlanogramComparisonResultId = "Id";
        public const String PlanogramComparisonResultPlanogramComparisonId = "PlanogramComparisonId";
        public const String PlanogramComparisonResultPlanogramName = "PlanogramName";
        public const String PlanogramComparisonResultStatus = "Status";
        public const String PlanogramComparisonResultExtendedData = "ExtendedData";

        #endregion

        #region PlanogramComparisonItem

        public const String PlanogramComparisonItemId = "Id";
        public const String PlanogramComparisonItemPlanogramComparisonResultId = "PlanogramComparisonResultId";
        public const String PlanogramComparisonItemItemId = "ItemId";
        public const String PlanogramComparisonItemItemType = "ItemType";
        public const String PlanogramComparisonItemStatus = "Status";
        public const String PlanogramComparisonItemExtendedData = "ExtendedData";

        #endregion

        #region PlanogramComparisonFieldValue

        public const String PlanogramComparisonFieldValueId = "Id";
        public const String PlanogramComparisonFieldValuePlanogramComparisonItemId = "PlanogramComparisonItemId";
        public const String PlanogramComparisonFieldValueFieldPlaceholder = "FieldValueFieldPlaceholder";
        public const String PlanogramComparisonFieldValueValue = "Value";
        public const String PlanogramComparisonFieldValueExtendedData = "ExtendedData";

        #endregion

        #region PlanogramComponent

        public const String PlanogramComponentId = "Id";
        public const String PlanogramComponentPlanogramId = "PlanogramId";
        public const String PlanogramComponentMesh3DId = "Mesh3DId";
        public const String PlanogramComponentImageIdFront = "ImageIdFront";
        public const String PlanogramComponentImageIdBack = "ImageIdBack";
        public const String PlanogramComponentImageIdTop = "ImageIdTop";
        public const String PlanogramComponentImageIdBottom = "ImageIdBottom";
        public const String PlanogramComponentImageIdLeft = "ImageIdLeft";
        public const String PlanogramComponentImageIdRight = "ImageIdRight";
        public const String PlanogramComponentName = "Name";
        public const String PlanogramComponentHeight = "Height";
        public const String PlanogramComponentWidth = "Width";
        public const String PlanogramComponentDepth = "Depth";
        public const String PlanogramComponentMetaNumberOfSubComponents = "MetaNumberOfSubComponents";
        public const String PlanogramComponentMetaNumberOfMerchandisedSubComponents = "MetaNumberOfMerchandisedSubComponents";
        public const String PlanogramComponentIsMoveable = "IsMoveable";
        public const String PlanogramComponentIsDisplayOnly = "IsDisplayOnly";
        public const String PlanogramComponentCanAttachShelfEdgeLabel = "CanAttachShelfEdgeLabel";
        public const String PlanogramComponentRetailerReferenceCode = "RetailerReferenceCode";
        public const String PlanogramComponentBarCode = "BarCode";
        public const String PlanogramComponentManufacturer = "Manufacturer";
        public const String PlanogramComponentManufacturerPartName = "ManufacturerPartName";
        public const String PlanogramComponentManufacturerPartNumber = "ManufacturerPartNumber";
        public const String PlanogramComponentSupplierName = "SupplierName";
        public const String PlanogramComponentSupplierPartNumber = "SupplierPartNumber";
        public const String PlanogramComponentSupplierCostPrice = "SupplierCostPrice";
        public const String PlanogramComponentSupplierDiscount = "SupplierDiscount";
        public const String PlanogramComponentSupplierLeadTime = "SupplierLeadTime";
        public const String PlanogramComponentMinPurchaseQty = "MinPurchaseQty";
        public const String PlanogramComponentWeightLimit = "WeightLimit";
        public const String PlanogramComponentWeight = "Weight";
        public const String PlanogramComponentVolume = "Volume";
        public const String PlanogramComponentDiameter = "Diameter";
        public const String PlanogramComponentCapacity = "Capacity";
        public const String PlanogramComponentComponentType = "ComponentType";
        public const String PlanogramComponentIsCarPark = "IsCarPark";
        public const String PlanogramComponentIsMerchandisedTopDown = "IsMerchandisedTopDown";

        #endregion

        #region PlanogramConsumerDecisionTree

        public const String PlanogramConsumerDecisionTreeId = "Id";
        public const String PlanogramConsumerDecisionTreePlanogramId = "PlanogramId";
        public const String PlanogramConsumerDecisionTreeName = "Name";
        public const String PlanogramConsumerDecisionTreeExtendedData = "ExtendedData";

        #endregion

        #region PlanogramConsumerDecisionTreeLevel

        public const String PlanogramConsumerDecisionTreeLevelId = "Id";
        public const String PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId = "PlanogramConsumerDecisionTreeId";
        public const String PlanogramConsumerDecisionTreeLevelName = "Name";
        public const String PlanogramConsumerDecisionTreeLevelParentLevelId = "ParentLevelId";
        public const String PlanogramConsumerDecisionTreeLevelExtendedData = "ExtendedData";

        #endregion

        #region PlanogramConsumerDecisionTreeNode

        public const String PlanogramConsumerDecisionTreeNodeId = "Id";
        public const String PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId = "PlanogramConsumerDecisionTreeId";
        public const String PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId = "PlanogramConsumerDecisionTreeLevelId";
        public const String PlanogramConsumerDecisionTreeNodeName = "Name";
        public const String PlanogramConsumerDecisionTreeNodeParentNodeId = "ParentNodeId";
        public const String PlanogramConsumerDecisionTreeNodeExtendedData = "ExtendedData";
        public const String PlanogramConsumerDecisionTreeNodeMetaCountOfProducts = "MetaCountOfProducts";
        public const String PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment = "MetaCountOfProductRecommendedInAssortment";
        public const String PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram = "CountOfProductPlacedOnPlanogram";
        public const String PlanogramConsumerDecisionTreeNodeMetaAggregatedPerformanceOfProduct = "MetaAggregatedPerformanceOfProduct";
        public const String PlanogramConsumerDecisionTreeNodeMetaPercentOfPerformance = "MetaPercentOfPerformance";
        public const String PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram = "MetaLinearSpaceAllocatedToProductsOnPlanogram";
        public const String PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram = "MetaVolumetricSpaceAllocatedToProductsOnPlanogram";
        public const String PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram = "MetaAreaSpaceAllocatedToProductsOnPlanogram";

        public const String PlanogramConsumerDecisionTreeNodeP1 = "PlanogramConsumerDecisionTreeNodeP1";
        public const String PlanogramConsumerDecisionTreeNodeP2 = "PlanogramConsumerDecisionTreeNodeP2";
        public const String PlanogramConsumerDecisionTreeNodeP3 = "PlanogramConsumerDecisionTreeNodeP3";
        public const String PlanogramConsumerDecisionTreeNodeP4 = "PlanogramConsumerDecisionTreeNodeP4";
        public const String PlanogramConsumerDecisionTreeNodeP5 = "PlanogramConsumerDecisionTreeNodeP5";
        public const String PlanogramConsumerDecisionTreeNodeP6 = "PlanogramConsumerDecisionTreeNodeP6";
        public const String PlanogramConsumerDecisionTreeNodeP7 = "PlanogramConsumerDecisionTreeNodeP7";
        public const String PlanogramConsumerDecisionTreeNodeP8 = "PlanogramConsumerDecisionTreeNodeP8";
        public const String PlanogramConsumerDecisionTreeNodeP9 = "PlanogramConsumerDecisionTreeNodeP9";
        public const String PlanogramConsumerDecisionTreeNodeP10 = "PlanogramConsumerDecisionTreeNodeP10";
        public const String PlanogramConsumerDecisionTreeNodeP11 = "PlanogramConsumerDecisionTreeNodeP11";
        public const String PlanogramConsumerDecisionTreeNodeP12 = "PlanogramConsumerDecisionTreeNodeP12";
        public const String PlanogramConsumerDecisionTreeNodeP13 = "PlanogramConsumerDecisionTreeNodeP13";
        public const String PlanogramConsumerDecisionTreeNodeP14 = "PlanogramConsumerDecisionTreeNodeP14";
        public const String PlanogramConsumerDecisionTreeNodeP15 = "PlanogramConsumerDecisionTreeNodeP15";
        public const String PlanogramConsumerDecisionTreeNodeP16 = "PlanogramConsumerDecisionTreeNodeP16";
        public const String PlanogramConsumerDecisionTreeNodeP17 = "PlanogramConsumerDecisionTreeNodeP17";
        public const String PlanogramConsumerDecisionTreeNodeP18 = "PlanogramConsumerDecisionTreeNodeP18";
        public const String PlanogramConsumerDecisionTreeNodeP19 = "PlanogramConsumerDecisionTreeNodeP19";
        public const String PlanogramConsumerDecisionTreeNodeP20 = "PlanogramConsumerDecisionTreeNodeP20";

        public const String PlanogramConsumerDecisionTreeNodeMetaP1Percentage = "PlanogramConsumerDecisionTreeNodeMetaP1Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP2Percentage = "PlanogramConsumerDecisionTreeNodeMetaP2Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP3Percentage = "PlanogramConsumerDecisionTreeNodeMetaP3Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP4Percentage = "PlanogramConsumerDecisionTreeNodeMetaP4Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP5Percentage = "PlanogramConsumerDecisionTreeNodeMetaP5Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP6Percentage = "PlanogramConsumerDecisionTreeNodeMetaP6Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP7Percentage = "PlanogramConsumerDecisionTreeNodeMetaP7Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP8Percentage = "PlanogramConsumerDecisionTreeNodeMetaP8Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP9Percentage = "PlanogramConsumerDecisionTreeNodeMetaP9Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP10Percentage = "PlanogramConsumerDecisionTreeNodeMetaP10Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP11Percentage = "PlanogramConsumerDecisionTreeNodeMetaP11Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP12Percentage = "PlanogramConsumerDecisionTreeNodeMetaP12Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP13Percentage = "PlanogramConsumerDecisionTreeNodeMetaP13Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP14Percentage = "PlanogramConsumerDecisionTreeNodeMetaP14Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP15Percentage = "PlanogramConsumerDecisionTreeNodeMetaP15Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP16Percentage = "PlanogramConsumerDecisionTreeNodeMetaP16Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP17Percentage = "PlanogramConsumerDecisionTreeNodeMetaP17Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP18Percentage = "PlanogramConsumerDecisionTreeNodeMetaP18Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP19Percentage = "PlanogramConsumerDecisionTreeNodeMetaP19Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP20Percentage = "PlanogramConsumerDecisionTreeNodeMetaP20Percentage";

        #endregion

        #region PlanogramConsumerDecisionTreeNodeProduct

        public const String PlanogramConsumerDecisionTreeNodeProductId = "Id";
        public const String PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId = "PlanogramConsumerDecisionTreeNodeId";
        public const String PlanogramConsumerDecisionTreeNodeProductPlanogramProductId = "PlanogramProductId";
        public const String PlanogramConsumerDecisionTreeNodeProductExtendedData = "ExtendedData";

        #endregion

        #region PlanogramEventLog

        public const String PlanogramEventLogId = "Id";
        public const String PlanogramEventLogPlanogramId = "PlanogramId";
        public const String PlanogramEventLogEventType = "EventType";
        public const String PlanogramEventLogEntryType = "EntryType";
        public const String PlanogramEventLogAffectedType = "AffectedType";
        public const String PlanogramEventLogAffectedId = "AffectedId";
        public const String PlanogramEventLogWorkpackageSource = "WorkpackageSource";
        public const String PlanogramEventLogTaskSource = "TaskSource";
        public const String PlanogramEventLogEventId = "EventId";
        public const String PlanogramEventLogDateTime = "DateTime";
        public const String PlanogramEventLogDescription= "Description";
        public const String PlanogramEventLogContent = "Content";
        public const String PlanogramEventLogExtendedData = "ExtendedData";
        public const String PlanogramEventLogScore = "Score";

        #endregion

        #region PlanogramFixtureAssembly

        public const String PlanogramFixtureAssemblyId = "Id";
        public const String PlanogramFixtureAssemblyPlanogramFixtureId = "PlanogramFixtureId";
        public const String PlanogramFixtureAssemblyPlanogramAssemblyId = "PlanogramAssemblyId";
        public const String PlanogramFixtureAssemblyX = "X";
        public const String PlanogramFixtureAssemblyY = "Y";
        public const String PlanogramFixtureAssemblyZ = "Z";
        public const String PlanogramFixtureAssemblySlope = "Slope";
        public const String PlanogramFixtureAssemblyAngle = "Angle";
        public const String PlanogramFixtureAssemblyRoll = "Roll"; 
        public const String PlanogramFixtureAssemblyMetaComponentCount = "MetaComponentCount";
        public const String PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace = "MetaTotalMerchandisableLinearSpace";
        public const String PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace = "MetaTotalMerchandisableAreaSpace";
        public const String PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace = "MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace = "MetaTotalLinearWhiteSpace";
        public const String PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace = "MetaTotalAreaWhiteSpace";
        public const String PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace = "MetaTotalVolumetricWhiteSpace";
        public const String PlanogramFixtureAssemblyMetaProductsPlaced = "MetaProductsPlaced";
        public const String PlanogramFixtureAssemblyMetaNewProducts = "MetaNewProducts";
        public const String PlanogramFixtureAssemblyMetaChangesFromPreviousCount = "MetaChangesFromPreviousCount";
        public const String PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating = "MetaChangeFromPreviousStarRating";
        public const String PlanogramFixtureAssemblyMetaBlocksDropped = "MetaBlocksDropped";
        public const String PlanogramFixtureAssemblyMetaNotAchievedInventory = "MetaNotAchievedInventory";
        public const String PlanogramFixtureAssemblyMetaTotalFacings = "MetaTotalFacings";
        public const String PlanogramFixtureAssemblyMetaAverageFacings = "MetaAverageFacings";
        public const String PlanogramFixtureAssemblyMetaTotalUnits = "MetaTotalUnits";
        public const String PlanogramFixtureAssemblyMetaAverageUnits = "MetaAverageUnits";
        public const String PlanogramFixtureAssemblyMetaMinDos = "MetaMinDos";
        public const String PlanogramFixtureAssemblyMetaMaxDos = "MetaMaxDos";
        public const String PlanogramFixtureAssemblyMetaAverageDos = "MetaAverageDos";
        public const String PlanogramFixtureAssemblyMetaMinCases = "MetaMinCases";
        public const String PlanogramFixtureAssemblyMetaAverageCases = "MetaAverageCases";
        public const String PlanogramFixtureAssemblyMetaMaxCases = "MetaMaxCases";
        public const String PlanogramFixtureAssemblyMetaSpaceToUnitsIndex = "MetaSpaceToUnitsIndex";
        public const String PlanogramFixtureAssemblyMetaTotalFrontFacings = "MetaTotalFrontFacings";
        public const String PlanogramFixtureAssemblyMetaAverageFrontFacings = "MetaAverageFrontFacings";

        #endregion

        #region PlanogramFixtureComponent

        public const String PlanogramFixtureComponentId = "Id";
        public const String PlanogramFixtureComponentPlanogramFixtureId = "PlanogramFixtureId";
        public const String PlanogramFixtureComponentPlanogramComponentId = "PlanogramComponentId";
        public const String PlanogramFixtureComponentComponentSequenceNumber = "ComponentSequenceNumber";
        public const String PlanogramFixtureComponentX = "X";
        public const String PlanogramFixtureComponentY = "Y";
        public const String PlanogramFixtureComponentZ = "Z";
        public const String PlanogramFixtureComponentSlope = "Slope";
        public const String PlanogramFixtureComponentAngle = "Angle";
        public const String PlanogramFixtureComponentRoll = "Roll";
        public const String PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace = "MetaTotalMerchandisableLinearSpace";
        public const String PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace = "MetaTotalMerchandisableAreaSpace";
        public const String PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace = "MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramFixtureComponentMetaTotalLinearWhiteSpace = "MetaTotalLinearWhiteSpace";
        public const String PlanogramFixtureComponentMetaTotalAreaWhiteSpace = "MetaTotalAreaWhiteSpace";
        public const String PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace = "MetaTotalVolumetricWhiteSpace";
        public const String PlanogramFixtureComponentMetaProductsPlaced = "MetaProductsPlaced";
        public const String PlanogramFixtureComponentMetaNewProducts = "MetaNewProducts";
        public const String PlanogramFixtureComponentMetaChangesFromPreviousCount = "MetaChangesFromPreviousCount";
        public const String PlanogramFixtureComponentMetaChangeFromPreviousStarRating = "MetaChangeFromPreviousStarRating";
        public const String PlanogramFixtureComponentMetaBlocksDropped = "MetaBlocksDropped";
        public const String PlanogramFixtureComponentMetaNotAchievedInventory = "MetaNotAchievedInventory";
        public const String PlanogramFixtureComponentMetaTotalFacings = "MetaTotalFacings";
        public const String PlanogramFixtureComponentMetaAverageFacings = "MetaAverageFacings";
        public const String PlanogramFixtureComponentMetaTotalUnits = "MetaTotalUnits";
        public const String PlanogramFixtureComponentMetaAverageUnits = "MetaAverageUnits";
        public const String PlanogramFixtureComponentMetaMinDos = "MetaMinDos";
        public const String PlanogramFixtureComponentMetaMaxDos = "MetaMaxDos";
        public const String PlanogramFixtureComponentMetaAverageDos = "MetaAverageDos";
        public const String PlanogramFixtureComponentMetaMinCases = "MetaMinCases";
        public const String PlanogramFixtureComponentMetaAverageCases = "MetaAverageCases";
        public const String PlanogramFixtureComponentMetaMaxCases = "MetaMaxCases";
        public const String PlanogramFixtureComponentMetaSpaceToUnitsIndex = "MetaSpaceToUnitsIndex";
        public const String PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser = "MetaIsComponentSlopeWithNoRiser";
        public const String PlanogramFixtureComponentMetaIsOverMerchandisedDepth = "MetaIsOverMerchandisedDepth";
        public const String PlanogramFixtureComponentMetaIsOverMerchandisedHeight = "MetaIsOverMerchandisedHeight";
        public const String PlanogramFixtureComponentMetaIsOverMerchandisedWidth = "MetaIsOverMerchandisedWidth";
        public const String PlanogramFixtureComponentMetaTotalComponentCollisions = "MetaTotalComponentCollisions";
        public const String PlanogramFixtureComponentMetaTotalPositionCollisions = "MetaTotalPositionCollisions";
        public const String PlanogramFixtureComponentMetaTotalFrontFacings = "MetaTotalFrontFacings";
        public const String PlanogramFixtureComponentMetaAverageFrontFacings = "MetaAverageFrontFacings";
        public const String PlanogramFixtureComponentMetaPercentageLinearSpaceFilled = "MetaPercentageLinearSpaceFilled";
        public const String PlanogramFixtureComponentNotchNumber = "NotchNumber";
        public const String PlanogramFixtureComponentMetaWorldX = "MetaWorldX";
        public const String PlanogramFixtureComponentMetaWorldY = "MetaWorldY";
        public const String PlanogramFixtureComponentMetaWorldZ = "MetaWorldZ";
        public const String PlanogramFixtureComponentMetaWorldAngle = "MetaWorldAngle";
        public const String PlanogramFixtureComponentMetaWorldSlope = "MetaWorldSlope";
        public const String PlanogramFixtureComponentMetaWorldRoll = "MetaWorldRoll";
        public const String PlanogramFixtureComponentMetaIsOutsideOfFixtureArea = "MetaIsOutsideOfFixtureArea";

        #endregion

        #region PlanogramFixture

        public const String PlanogramFixtureId = "Id";
        public const String PlanogramFixturePlanogramId = "PlanogramId";
        public const String PlanogramFixtureName = "Name";
        public const String PlanogramFixtureHeight = "Height";
        public const String PlanogramFixtureWidth = "Width";
        public const String PlanogramFixtureDepth = "Depth";
        public const String PlanogramFixtureNumberOfAssemblies = "NumberOfAssemblies";
        public const String PlanogramFixtureMetaNumberOfMerchandisedSubComponents = "MetaNumberOfMerchandisedSubComponents";
        public const String PlanogramFixtureMetaTotalFixtureCost = "MetaTotalFixtureCost";
        public const String PlanogramFixtureUniqueProductCount = "UniqueProductCount";

        #endregion

        #region PlanogramFixtureItem

        public const String PlanogramFixtureItemId = "Id";
        public const String PlanogramFixtureItemPlanogramId = "PlanogramId";
        public const String PlanogramFixtureItemPlanogramFixtureId = "PlanogramFixtureId";
        public const String PlanogramFixtureItemX = "X";
        public const String PlanogramFixtureItemY = "Y";
        public const String PlanogramFixtureItemZ = "Z";
        public const String PlanogramFixtureItemSlope = "Slope";
        public const String PlanogramFixtureItemAngle = "Angle";
        public const String PlanogramFixtureItemRoll = "Roll";
        public const String PlanogramFixtureItemBaySequenceNumber = "BaySequenceNumber";
        public const String PlanogramFixtureItemMetaComponentCount = "MetaComponentCount";
        public const String PlanogramFixtureItemMetaTotalMerchandisableLinearSpace = "MetaTotalMerchandisableLinearSpace";
        public const String PlanogramFixtureItemMetaTotalMerchandisableAreaSpace = "MetaTotalMerchandisableAreaSpace";
        public const String PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace = "MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramFixtureItemMetaTotalLinearWhiteSpace = "MetaTotalLinearWhiteSpace";
        public const String PlanogramFixtureItemMetaTotalAreaWhiteSpace = "MetaTotalAreaWhiteSpace";
        public const String PlanogramFixtureItemMetaTotalVolumetricWhiteSpace = "MetaTotalVolumetricWhiteSpace";
        public const String PlanogramFixtureItemMetaProductsPlaced = "MetaProductsPlaced";
        public const String PlanogramFixtureItemMetaNewProducts = "MetaNewProducts";
        public const String PlanogramFixtureItemMetaChangesFromPreviousCount = "MetaChangesFromPreviousCount";
        public const String PlanogramFixtureItemMetaChangeFromPreviousStarRating = "MetaChangeFromPreviousStarRating";
        public const String PlanogramFixtureItemMetaBlocksDropped = "MetaBlocksDropped";
        public const String PlanogramFixtureItemMetaNotAchievedInventory = "MetaNotAchievedInventory";
        public const String PlanogramFixtureItemMetaTotalFacings = "MetaTotalFacings";
        public const String PlanogramFixtureItemMetaAverageFacings = "MetaAverageFacings";
        public const String PlanogramFixtureItemMetaTotalUnits = "MetaTotalUnits";
        public const String PlanogramFixtureItemMetaAverageUnits = "MetaAverageUnits";
        public const String PlanogramFixtureItemMetaMinDos = "MetaMinDos";
        public const String PlanogramFixtureItemMetaMaxDos = "MetaMaxDos";
        public const String PlanogramFixtureItemMetaAverageDos = "MetaAverageDos";
        public const String PlanogramFixtureItemMetaMinCases = "MetaMinCases";
        public const String PlanogramFixtureItemMetaAverageCases = "MetaAverageCases";
        public const String PlanogramFixtureItemMetaMaxCases = "MetaMaxCases";
        public const String PlanogramFixtureItemMetaSpaceToUnitsIndex = "MetaSpaceToUnitsIndex";
        public const String PlanogramFixtureItemMetaTotalComponentCollisions = "MetaTotalComponentCollisions";
        public const String PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth = "MetaTotalComponentsOverMerchandisedDepth";
        public const String PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight = "MetaTotalComponentsOverMerchandisedHeight";
        public const String PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth = "MetaTotalComponentsOverMerchandisedWidth";
        public const String PlanogramFixtureItemMetaTotalPositionCollisions = "MetaTotalPositionCollisions";
        public const String PlanogramFixtureItemMetaTotalFrontFacings = "MetaTotalFrontFacings";
        public const String PlanogramFixtureItemMetaAverageFrontFacings = "MetaAverageFrontFacings";

        #endregion

        #region PlanogramImage

        public const String PlanogramImageId = "Id";
        public const String PlanogramImagePlanogramId = "PlanogramId";
        public const String PlanogramImageFileName = "FileName";
        public const String PlanogramImageDescription = "Description";
        public const String PlanogramImageImageData = "ImageData";

        #endregion

        #region PlanogramInventory

        public const String PlanogramInventoryId = "Id";
        public const String PlanogramInventoryPlanogramId = "PlanogramId";
        public const String PlanogramInventoryDaysOfPerformance = "DaysOfPerformance";
        public const String PlanogramInventoryMinCasePacks = "MinCasePacks";
        public const String PlanogramInventoryMinDos = "MinDos";
        public const String PlanogramInventoryMinShelfLife = "MinShelfLife";
        public const String PlanogramInventoryMinDeliveries = "MinDeliveries";
        public const String PlanogramInventoryIsCasePacksValidated = "IsCasePacksValidated";
        public const String PlanogramInventoryIsDosValidated = "IsDosValidated";
        public const String PlanogramInventoryIsShelfLifeValidated = "IsShelfLifeValidated";
        public const String PlanogramInventoryIsDeliveriesValidated = "IsDeliveriesValidated";
        public const String PlanogramInventoryInventoryMetricType = "InventoryMetricType";

        #endregion

        #region PlanogramMetadataImage

        public const String PlanogramMetadataImageId = "Id";
        public const String PlanogramMetadataImagePlanogramId = "PlanogramId";
        public const String PlanogramMetadataImageFileName = "FileName";
        public const String PlanogramMetadataImageDescription = "Description";
        public const String PlanogramMetadataImageImageData = "ImageData";

        #endregion

        #region PlanogramPerformance
        public const String PlanogramPerformanceId = "Id";
        public const String PlanogramPerformancePlanogramId = "PlanogramId";
        public const String PlanogramPerformanceName = "Name";
        public const String PlanogramPerformanceDescription = "Description";
        #endregion

        #region PlanogramPerformanceData
        public const String PlanogramPerformanceDataId = "Id";
        public const String PlanogramPerformanceDataPlanogramPerformanceId = "PlanogramPerformanceId";
        public const String PlanogramPerformanceDataPlanogramProductId = "PlanogramProductId";
        public const String PlanogramPerformanceDataP1 = "P1";
        public const String PlanogramPerformanceDataP2 = "P2";
        public const String PlanogramPerformanceDataP3 = "P3";
        public const String PlanogramPerformanceDataP4 = "P4";
        public const String PlanogramPerformanceDataP5 = "P5";
        public const String PlanogramPerformanceDataP6 = "P6";
        public const String PlanogramPerformanceDataP7 = "P7";
        public const String PlanogramPerformanceDataP8 = "P8";
        public const String PlanogramPerformanceDataP9 = "P9";
        public const String PlanogramPerformanceDataP10 = "P10";
        public const String PlanogramPerformanceDataP11 = "P11";
        public const String PlanogramPerformanceDataP12 = "P12";
        public const String PlanogramPerformanceDataP13 = "P13";
        public const String PlanogramPerformanceDataP14 = "P14";
        public const String PlanogramPerformanceDataP15 = "P15";
        public const String PlanogramPerformanceDataP16 = "P16";
        public const String PlanogramPerformanceDataP17 = "P17";
        public const String PlanogramPerformanceDataP18 = "P18";
        public const String PlanogramPerformanceDataP19 = "P19";
        public const String PlanogramPerformanceDataP20 = "P20";
        public const String PlanogramPerformanceDataUnitsSoldPerDay = "UnitsSoldPerDay";
        public const String PlanogramPerformanceDataAchievedCasePacks = "AchievedCasePacks";
        public const String PlanogramPerformanceDataAchievedDos = "AchievedDos";
        public const String PlanogramPerformanceDataAchievedShelfLife = "AchievedShelfLife";
        public const String PlanogramPerformanceDataAchievedDeliveries = "AchievedDeliveries";
        public const String PlanogramPerformanceDataMinimumInventoryUnits = "MinimumInventoryUnits";
        public const String PlanogramPerformanceDataMetaP1Percentage = "MetaP1Percentage";
        public const String PlanogramPerformanceDataMetaP2Percentage = "MetaP2Percentage";
        public const String PlanogramPerformanceDataMetaP3Percentage = "MetaP3Percentage";
        public const String PlanogramPerformanceDataMetaP4Percentage = "MetaP4Percentage";
        public const String PlanogramPerformanceDataMetaP5Percentage = "MetaP5Percentage";
        public const String PlanogramPerformanceDataMetaP6Percentage = "MetaP6Percentage";
        public const String PlanogramPerformanceDataMetaP7Percentage = "MetaP7Percentage";
        public const String PlanogramPerformanceDataMetaP8Percentage = "MetaP8Percentage";
        public const String PlanogramPerformanceDataMetaP9Percentage = "MetaP9Percentage";
        public const String PlanogramPerformanceDataMetaP10Percentage = "MetaP10Percentage";
        public const String PlanogramPerformanceDataMetaP11Percentage = "MetaP11Percentage";
        public const String PlanogramPerformanceDataMetaP12Percentage = "MetaP12Percentage";
        public const String PlanogramPerformanceDataMetaP13Percentage = "MetaP13Percentage";
        public const String PlanogramPerformanceDataMetaP14Percentage = "MetaP14Percentage";
        public const String PlanogramPerformanceDataMetaP15Percentage = "MetaP15Percentage";
        public const String PlanogramPerformanceDataMetaP16Percentage = "MetaP16Percentage";
        public const String PlanogramPerformanceDataMetaP17Percentage = "MetaP17Percentage";
        public const String PlanogramPerformanceDataMetaP18Percentage = "MetaP18Percentage";
        public const String PlanogramPerformanceDataMetaP19Percentage = "MetaP19Percentage";
        public const String PlanogramPerformanceDataMetaP20Percentage = "MetaP20Percentage";
        public const String PlanogramPerformanceDataMetaP1Rank = "MetaP1Rank";
        public const String PlanogramPerformanceDataMetaP2Rank = "MetaP2Rank";
        public const String PlanogramPerformanceDataMetaP3Rank = "MetaP3Rank";
        public const String PlanogramPerformanceDataMetaP4Rank = "MetaP4Rank";
        public const String PlanogramPerformanceDataMetaP5Rank = "MetaP5Rank";
        public const String PlanogramPerformanceDataMetaP6Rank = "MetaP6Rank";
        public const String PlanogramPerformanceDataMetaP7Rank = "MetaP7Rank";
        public const String PlanogramPerformanceDataMetaP8Rank = "MetaP8Rank";
        public const String PlanogramPerformanceDataMetaP9Rank = "MetaP9Rank";
        public const String PlanogramPerformanceDataMetaP10Rank = "MetaP10Rank";
        public const String PlanogramPerformanceDataMetaP11Rank = "MetaP11Rank";
        public const String PlanogramPerformanceDataMetaP12Rank = "MetaP12Rank";
        public const String PlanogramPerformanceDataMetaP13Rank = "MetaP13Rank";
        public const String PlanogramPerformanceDataMetaP14Rank = "MetaP14Rank";
        public const String PlanogramPerformanceDataMetaP15Rank = "MetaP15Rank";
        public const String PlanogramPerformanceDataMetaP16Rank = "MetaP16Rank";
        public const String PlanogramPerformanceDataMetaP17Rank = "MetaP17Rank";
        public const String PlanogramPerformanceDataMetaP18Rank = "MetaP18Rank";
        public const String PlanogramPerformanceDataMetaP19Rank = "MetaP19Rank";
        public const String PlanogramPerformanceDataMetaP20Rank = "MetaP20Rank";
        public const String PlanogramPerformanceDataCP1 = "CP1";
        public const String PlanogramPerformanceDataCP2 = "CP2";
        public const String PlanogramPerformanceDataCP3 = "CP3";
        public const String PlanogramPerformanceDataCP4 = "CP4";
        public const String PlanogramPerformanceDataCP5 = "CP5";
        public const String PlanogramPerformanceDataCP6 = "CP6";
        public const String PlanogramPerformanceDataCP7 = "CP7";
        public const String PlanogramPerformanceDataCP8 = "CP8";
        public const String PlanogramPerformanceDataCP9 = "CP9";
        public const String PlanogramPerformanceDataCP10 = "CP10";
        public const String PlanogramPerformanceDataMetaCP1Percentage = "MetaCP1Percentage";
        public const String PlanogramPerformanceDataMetaCP2Percentage = "MetaCP2Percentage";
        public const String PlanogramPerformanceDataMetaCP3Percentage = "MetaCP3Percentage";
        public const String PlanogramPerformanceDataMetaCP4Percentage = "MetaCP4Percentage";
        public const String PlanogramPerformanceDataMetaCP5Percentage = "MetaCP5Percentage";
        public const String PlanogramPerformanceDataMetaCP6Percentage = "MetaCP6Percentage";
        public const String PlanogramPerformanceDataMetaCP7Percentage = "MetaCP7Percentage";
        public const String PlanogramPerformanceDataMetaCP8Percentage = "MetaCP8Percentage";
        public const String PlanogramPerformanceDataMetaCP9Percentage = "MetaCP9Percentage";
        public const String PlanogramPerformanceDataMetaCP10Percentage = "MetaCP10Percentage";
        public const String PlanogramPerformanceDataMetaCP1Rank = "MetaCP1Rank";
        public const String PlanogramPerformanceDataMetaCP2Rank = "MetaCP2Rank";
        public const String PlanogramPerformanceDataMetaCP3Rank = "MetaCP3Rank";
        public const String PlanogramPerformanceDataMetaCP4Rank = "MetaCP4Rank";
        public const String PlanogramPerformanceDataMetaCP5Rank = "MetaCP5Rank";
        public const String PlanogramPerformanceDataMetaCP6Rank = "MetaCP6Rank";
        public const String PlanogramPerformanceDataMetaCP7Rank = "MetaCP7Rank";
        public const String PlanogramPerformanceDataMetaCP8Rank = "MetaCP8Rank";
        public const String PlanogramPerformanceDataMetaCP9Rank = "MetaCP9Rank";
        public const String PlanogramPerformanceDataMetaCP10Rank = "MetaCP10Rank";
        #endregion

        #region PlanogramPerformanceMetric
        public const String PlanogramPerformanceMetricId = "Id";
        public const String PlanogramPerformanceMetricPlanogramPerformanceId = "PlanogramPerformanceId";
        public const String PlanogramPerformanceMetricName = "Name";
        public const String PlanogramPerformanceMetricDescription = "Description";
        public const String PlanogramPerformanceMetricDirection = "Direction";
        public const String PlanogramPerformanceMetricSpecialType = "SpecialType";
        public const String PlanogramPerformanceMetricMetricType = "MetricType";
        public const String PlanogramPerformanceMetricMetricId = "MetricId";
        public const String PlanogramPerformanceMetricAggregationType = "AggregationType";
        #endregion

        #region PlanogramPosition

        public const String PlanogramPositionId = "Id";
        public const String PlanogramPositionPlanogramId = "PlanogramId";
        public const String PlanogramPositionPlanogramFixtureItemId = "PlanogramFixtureItemId";
        public const String PlanogramPositionPlanogramFixtureAssemblyId = "PlanogramFixtureAssemblyId";
        public const String PlanogramPositionPlanogramAssemblyComponentId = "PlanogramAssemblyComponentId";
        public const String PlanogramPositionPlanogramFixtureComponentId = "PlanogramFixtureComponentId";
        public const String PlanogramPositionPlanogramSubComponentId = "PlanogramSubComponentId";
        public const String PlanogramPositionPlanogramProductId = "PlanogramProductId";
        public const String PlanogramPositionPositionSequenceNumber = "PositionSequenceNumber";
        public const String PlanogramPositionX = "X";
        public const String PlanogramPositionY = "Y";
        public const String PlanogramPositionZ = "Z";
        public const String PlanogramPositionSlope = "Slope";
        public const String PlanogramPositionAngle = "Angle";
        public const String PlanogramPositionRoll = "Roll";
        public const String PlanogramPositionFacingsHigh = "FacingsHigh";
        public const String PlanogramPositionFacingsWide = "FacingsWide";
        public const String PlanogramPositionFacingsDeep = "FacingsDeep";
        public const String PlanogramPositionOrientationType = "OrientationType";
        public const String PlanogramPositionMerchandisingStyle = "MerchandisingStyle";
        public const String PlanogramPositionFacingsXHigh = "FacingsXHigh";
        public const String PlanogramPositionFacingsXWide = "FacingsXWide";
        public const String PlanogramPositionFacingsXDeep = "FacingsXDeep";
        public const String PlanogramPositionMerchandisingStyleX = "MerchandisingStyleX";
        public const String PlanogramPositionOrientationTypeX = "OrientationTypeX";
        public const String PlanogramPositionIsXPlacedLeft = "IsXPlacedLeft";
        public const String PlanogramPositionFacingsYHigh = "FacingsYHigh";
        public const String PlanogramPositionFacingsYWide = "FacingsYWide";
        public const String PlanogramPositionFacingsYDeep = "FacingsYDeep";
        public const String PlanogramPositionMerchandisingStyleY = "MerchandisingStyleY";
        public const String PlanogramPositionOrientationTypeY = "OrientationTypeY";
        public const String PlanogramPositionIsYPlacedBottom = "IsYPlacedBottom";
        public const String PlanogramPositionFacingsZHigh = "FacingsZHigh";
        public const String PlanogramPositionFacingsZWide = "FacingsZWide";
        public const String PlanogramPositionFacingsZDeep = "FacingsZDeep";
        public const String PlanogramPositionMerchandisingStyleZ = "MerchandisingStyleZ";
        public const String PlanogramPositionOrientationTypeZ = "OrientationTypeZ";
        public const String PlanogramPositionIsZPlacedFront = "IsZPlacedFront";
        public const String PlanogramPositionSequence = "Sequence";
        public const String PlanogramPositionSequenceX = "SequenceX";
        public const String PlanogramPositionSequenceY = "SequenceY";
        public const String PlanogramPositionSequenceZ = "SequenceZ";
        public const String PlanogramPositionUnitsHigh = "UnitsHigh";
        public const String PlanogramPositionUnitsWide = "UnitsWide";
        public const String PlanogramPositionUnitsDeep = "UnitsDeep";
        public const String PlanogramPositionTotalUnits = "TotalUnits";
        public const String PlanogramPositionMetaAchievedCases = "MetaAchievedCases";
        public const String PlanogramPositionMetaIsPositionCollisions = "MetaIsPositionCollisions";
        public const String PlanogramPositionMetaFrontFacingsWide = "MetaFrontFacingsWide";
        public const String PlanogramPositionIsManuallyPlaced = "IsManuallyPlaced";
        public const String PlanogramPositionHorizontalSqueeze = "HorizontalSqueeze";
        public const String PlanogramPositionVerticalSqueeze = "VerticalSqueeze";
        public const String PlanogramPositionDepthSqueeze = "DepthSqueeze";
        public const String PlanogramPositionHorizontalSqueezeX = "HorizontalSqueezeX";
        public const String PlanogramPositionVerticalSqueezeX = "VerticalSqueezeX";
        public const String PlanogramPositionDepthSqueezeX = "DepthSqueezeX";
        public const String PlanogramPositionHorizontalSqueezeY = "HorizontalSqueezeY";
        public const String PlanogramPositionVerticalSqueezeY = "VerticalSqueezeY";
        public const String PlanogramPositionDepthSqueezeY = "DepthSqueezeY";
        public const String PlanogramPositionHorizontalSqueezeZ = "HorizontalSqueezeZ";
        public const String PlanogramPositionVerticalSqueezeZ = "VerticalSqueezeZ";
        public const String PlanogramPositionDepthSqueezeZ = "DepthSqueezeZ";
        public const String PlanogramPositionMetaIsUnderMinDeep = "MetaIsUnderMinDeep";
        public const String PlanogramPositionMetaIsOverMaxDeep = "MetaIsOverMaxDeep";
        public const String PlanogramPositionMetaIsOverMaxRightCap = "MetaIsOverMaxRightCap";
        public const String PlanogramPositionMetaIsOverMaxStack = "MetaIsOverMaxStack";
        public const String PlanogramPositionMetaIsOverMaxTopCap = "MetaIsOverMaxTopCap";
        public const String PlanogramPositionMetaIsInvalidMerchandisingStyle = "MetaIsInvalidMerchandisingStyle";
        public const String PlanogramPositionMetaIsHangingTray = "MetaIsHangingTray";
        public const String PlanogramPositionMetaIsPegOverfilled = "MetaIsPegOverfilled";
        public const String PlanogramPositionMetaIsOutsideMerchandisingSpace = "MetaIsOutsideMerchandisingSpace";
        public const String PlanogramPositionMetaIsOutsideOfBlockSpace = "MetaIsOutsideOfBlockSpace";
        public const String PlanogramPositionMetaHeight = "MetaHeight";
        public const String PlanogramPositionMetaWidth = "MetaWidth";
        public const String PlanogramPositionMetaDepth = "MetaDepth";
        public const String PlanogramPositionMetaWorldX = "MetaWorldX";
        public const String PlanogramPositionMetaWorldY = "MetaWorldY";
        public const String PlanogramPositionMetaWorldZ = "MetaWorldZ";
        public const String PlanogramPositionMetaTotalLinearSpace = "MetaTotalLinearSpace";
        public const String PlanogramPositionMetaTotalAreaSpace = "MetaTotalAreaSpace";
        public const String PlanogramPositionMetaTotalVolumetricSpace = "MetaTotalVolumetricSpace";
        public const String PlanogramPositionMetaPlanogramLinearSpacePercentage = "MetaPlanogramLinearSpacePercentage";
        public const String PlanogramPositionMetaPlanogramAreaSpacePercentage = "MetaPlanogramAreaSpacePercentage";
        public const String PlanogramPositionMetaPlanogramVolumetricSpacePercentage = "MetaPlanogramVolumetricSpacePercentage";
        public const String PlanogramPositionSequenceNumber = "SequenceNumber";
        public const String PlanogramPositionSequenceColour = "SequenceColour";
        public const String PlanogramPositionMetaComparisonStatus = "MetaComparisonStatus";
        public const String PlanogramPositionMetaSequenceSubGroupName = "MetaSequenceSubGroupName";
        public const String PlanogramPositionMetaSequenceGroupName = "MetaSequenceGroupName";
        public const String PlanogramPositionMetaPegRowNumber = "MetaPegRowNumber";
        public const String PlanogramPositionMetaPegColumnNumber = "MetaPegColumnNumber";

        #endregion

        #region PlanogramProduct

        public const String PlanogramProductId = "Id";
        public const String PlanogramProductPlanogramId = "PlanogramId";
        public const String PlanogramProductGtin = "Gtin";
        public const String PlanogramProductName = "Name";
        public const String PlanogramProductBrand = "Brand";
        public const String PlanogramProductHeight = "Height";
        public const String PlanogramProductWidth = "Width";
        public const String PlanogramProductDepth = "Depth";
        public const String PlanogramProductDisplayHeight = "DisplayHeight";
        public const String PlanogramProductDisplayWidth = "DisplayWidth";
        public const String PlanogramProductDisplayDepth = "DisplayDepth";
        public const String PlanogramProductAlternateHeight = "AlternateHeight";
        public const String PlanogramProductAlternateWidth = "AlternateWidth";
        public const String PlanogramProductAlternateDepth = "AlternateDepth";
        public const String PlanogramProductPointOfPurchaseHeight = "PointOfPurchaseHeight";
        public const String PlanogramProductPointOfPurchaseWidth = "PointOfPurchaseWidth";
        public const String PlanogramProductPointOfPurchaseDepth = "PointOfPurchaseDepth";
        public const String PlanogramProductNumberOfPegHoles = "NumberOfPegHoles";
        public const String PlanogramProductPegX = "PegX";
        public const String PlanogramProductPegX2 = "PegX2";
        public const String PlanogramProductPegX3 = "PegX3";
        public const String PlanogramProductPegY = "PegY";
        public const String PlanogramProductPegY2 = "PegY2";
        public const String PlanogramProductPegY3 = "PegY3";
        public const String PlanogramProductPegProngOffset = "PegProngOffset";
        public const String PlanogramProductPegProngOffsetX = "PegProngOffsetX";
        public const String PlanogramProductPegProngOffsetY = "PegProngOffsetY";
        public const String PlanogramProductPegDepth = "PegDepth";
        public const String PlanogramProductSqueezeHeight = "SqueezeHeight";
        public const String PlanogramProductSqueezeWidth = "SqueezeWidth";
        public const String PlanogramProductSqueezeDepth = "SqueezeDepth";
        public const String PlanogramProductNestingHeight = "NestingHeight";
        public const String PlanogramProductNestingWidth = "NestingWidth";
        public const String PlanogramProductNestingDepth = "NestingDepth";
        public const String PlanogramProductCasePackUnits = "CasePackUnits";
        public const String PlanogramProductCaseHigh = "CaseHigh";
        public const String PlanogramProductCaseWide = "CaseWide";
        public const String PlanogramProductCaseDeep = "CaseDeep";
        public const String PlanogramProductCaseHeight = "CaseHeight";
        public const String PlanogramProductCaseWidth = "CaseWidth";
        public const String PlanogramProductCaseDepth = "CaseDepth";
        public const String PlanogramProductMaxStack = "MaxStack";
        public const String PlanogramProductMaxTopCap = "MaxTopCap";
        public const String PlanogramProductMaxRightCap = "MaxRightCap";
        public const String PlanogramProductMinDeep = "MinDeep";
        public const String PlanogramProductMaxDeep = "MaxDeep";
        public const String PlanogramProductMaxNestingHigh = "MaxNestingHigh";
        public const String PlanogramProductMaxNestingDeep = "MaxNestingDeep";
        public const String PlanogramProductTrayPackUnits = "TrayPackUnits";
        public const String PlanogramProductTrayHigh = "TrayHigh";
        public const String PlanogramProductTrayWide = "TrayWide";
        public const String PlanogramProductTrayDeep = "TrayDeep";
        public const String PlanogramProductTrayHeight = "TrayHeight";
        public const String PlanogramProductTrayWidth = "TrayWidth";
        public const String PlanogramProductTrayDepth = "TrayDepth";
        public const String PlanogramProductTrayThickHeight = "TrayThickHeight";
        public const String PlanogramProductTrayThickWidth = "TrayThickWidth";
        public const String PlanogramProductTrayThickDepth = "TrayThickDepth";
        public const String PlanogramProductFrontOverhang = "FrontOverhang";
        public const String PlanogramProductFingerSpaceAbove = "FingerSpaceAbove";
        public const String PlanogramProductFingerSpaceToTheSide = "FingerSpaceToTheSide";
        public const String PlanogramProductStatusType = "StatusType";
        public const String PlanogramProductOrientationType = "OrientationType";
        public const String PlanogramProductMerchandisingStyle = "MerchandisingStyle";
        public const String PlanogramProductIsFrontOnly = "IsFrontOnly";
        public const String PlanogramProductIsTrayProduct = "IsTrayProduct";
        public const String PlanogramProductIsPlaceHolderProduct = "IsPlaceHolderProduct";
        public const String PlanogramProductIsActive = "IsActive";
        public const String PlanogramProductCanBreakTrayUp = "CanBreakTrayUp";
        public const String PlanogramProductCanBreakTrayDown = "CanBreakTrayDown";
        public const String PlanogramProductCanBreakTrayBack = "CanBreakTrayBack";
        public const String PlanogramProductCanBreakTrayTop = "CanBreakTrayTop";
        public const String PlanogramProductForceMiddleCap = "ForceMiddleCap";
        public const String PlanogramProductForceBottomCap = "ForceBottomCap";
        public const String PlanogramProductPlanogramImageIdFront = "PlanogramImageIdFront";
        public const String PlanogramProductPlanogramImageIdBack = "PlanogramImageIdBack";
        public const String PlanogramProductPlanogramImageIdTop = "PlanogramImageIdTop";
        public const String PlanogramProductPlanogramImageIdBottom = "PlanogramImageIdBottom";
        public const String PlanogramProductPlanogramImageIdLeft = "PlanogramImageIdLeft";
        public const String PlanogramProductPlanogramImageIdRight = "PlanogramImageIdRight";
        public const String PlanogramProductPlanogramImageIdDisplayFront = "PlanogramImageIdDisplayFront";
        public const String PlanogramProductPlanogramImageIdDisplayBack = "PlanogramImageIdDisplayBack";
        public const String PlanogramProductPlanogramImageIdDisplayTop = "PlanogramImageIdDisplayTop";
        public const String PlanogramProductPlanogramImageIdDisplayBottom = "PlanogramImageIdDisplayBottom";
        public const String PlanogramProductPlanogramImageIdDisplayLeft = "PlanogramImageIdDisplayLeft";
        public const String PlanogramProductPlanogramImageIdDisplayRight = "PlanogramImageIdDisplayRight";
        public const String PlanogramProductPlanogramImageIdTrayFront = "PlanogramImageIdTrayFront";
        public const String PlanogramProductPlanogramImageIdTrayBack = "PlanogramImageIdTrayBack";
        public const String PlanogramProductPlanogramImageIdTrayTop = "PlanogramImageIdTrayTop";
        public const String PlanogramProductPlanogramImageIdTrayBottom = "PlanogramImageIdTrayBottom";
        public const String PlanogramProductPlanogramImageIdTrayLeft = "PlanogramImageIdTrayLeft";
        public const String PlanogramProductPlanogramImageIdTrayRight = "PlanogramImageIdTrayRight";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseFront = "PlanogramImageIdPointOfPurchaseFront";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseBack = "PlanogramImageIdPointOfPurchaseBack";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseTop = "PlanogramImageIdPointOfPurchaseTop";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseBottom = "PlanogramImageIdPointOfPurchaseBottom";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseLeft = "PlanogramImageIdPointOfPurchaseLeft";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseRight = "PlanogramImageIdPointOfPurchaseRight";
        public const String PlanogramProductPlanogramImageIdAlternateFront = "PlanogramImageIdAlternateFront";
        public const String PlanogramProductPlanogramImageIdAlternateBack = "PlanogramImageIdAlternateBack";
        public const String PlanogramProductPlanogramImageIdAlternateTop = "PlanogramImageIdAlternateTop";
        public const String PlanogramProductPlanogramImageIdAlternateBottom = "PlanogramImageIdAlternateBottom";
        public const String PlanogramProductPlanogramImageIdAlternateLeft = "PlanogramImageIdAlternateLeft";
        public const String PlanogramProductPlanogramImageIdAlternateRight = "PlanogramImageIdAlternateRight";
        public const String PlanogramProductPlanogramImageIdCaseFront = "PlanogramImageIdCaseFront";
        public const String PlanogramProductPlanogramImageIdCaseBack = "PlanogramImageIdCaseBack";
        public const String PlanogramProductPlanogramImageIdCaseTop = "PlanogramImageIdCaseTop";
        public const String PlanogramProductPlanogramImageIdCaseBottom = "PlanogramImageIdCaseBottom";
        public const String PlanogramProductPlanogramImageIdCaseLeft = "PlanogramImageIdCaseLeft";
        public const String PlanogramProductPlanogramImageIdCaseRight = "PlanogramImageIdCaseRight";
        public const String PlanogramProductShapeType = "ShapeType";
        public const String PlanogramProductFillPatternType = "FillPatternType";
        public const String PlanogramProductFillColour = "FillColour";
        public const String PlanogramProductShape = "Shape";
        public const String PlanogramProductPointOfPurchaseDescription = "PointOfPurchaseDescription"; 
        public const String PlanogramProductShortDescription = "ShortDescription"; 
        public const String PlanogramProductSubcategory = "Subcategory"; 
        public const String PlanogramProductCustomerStatus = "CustomerStatus"; 
        public const String PlanogramProductColour = "Colour"; 
        public const String PlanogramProductFlavour = "Flavour"; 
        public const String PlanogramProductPackagingShape = "PackagingShape"; 
        public const String PlanogramProductPackagingType = "PackagingType"; 
        public const String PlanogramProductCountryOfOrigin = "CountryOfOrigin"; 
        public const String PlanogramProductCountryOfProcessing = "CountryOfProcessing"; 
        public const String PlanogramProductShelfLife = "ShelfLife"; 
        public const String PlanogramProductDeliveryFrequencyDays = "DeliveryFrequencyDays"; 
        public const String PlanogramProductDeliveryMethod = "DeliveryMethod"; 
        public const String PlanogramProductVendorCode = "VendorCode"; 
        public const String PlanogramProductVendor = "Vendor"; 
        public const String PlanogramProductManufacturerCode = "ManufacturerCode"; 
        public const String PlanogramProductManufacturer = "Manufacturer"; 
        public const String PlanogramProductSize = "Size"; 
        public const String PlanogramProductUnitOfMeasure = "UnitOfMeasure"; 
        public const String PlanogramProductDateIntroduced = "DateIntroduced"; 
        public const String PlanogramProductDateDiscontinued = "DateDiscontinued"; 
        public const String PlanogramProductDateEffective = "DateEffective"; 
        public const String PlanogramProductHealth = "Health"; 
        public const String PlanogramProductCorporateCode = "CorporateCode"; 
        public const String PlanogramProductBarcode = "Barcode"; 
        public const String PlanogramProductSellPrice = "SellPrice"; 
        public const String PlanogramProductSellPackCount = "SellPackCount"; 
        public const String PlanogramProductSellPackDescription = "SellPackDescription"; 
        public const String PlanogramProductRecommendedRetailPrice = "RecommendedRetailPrice"; 
        public const String PlanogramProductManufacturerRecommendedRetailPrice = "ManufacturerRecommendedRetailPrice"; 
        public const String PlanogramProductCostPrice = "CostPrice"; 
        public const String PlanogramProductCaseCost = "CaseCost"; 
        public const String PlanogramProductTaxRate = "TaxRate"; 
        public const String PlanogramProductConsumerInformation = "ConsumerInformation"; 
        public const String PlanogramProductTexture = "Texture"; 
        public const String PlanogramProductStyleNumber = "StyleNumber"; 
        public const String PlanogramProductPattern = "Pattern"; 
        public const String PlanogramProductModel = "Model"; 
        public const String PlanogramProductGarmentType = "GarmentType"; 
        public const String PlanogramProductIsPrivateLabel = "IsPrivateLabel"; 
        public const String PlanogramProductIsNewProduct = "IsNewProduct";
        public const String PlanogramProductFinancialGroupCode = "FinancialGroupCode";
        public const String PlanogramProductFinancialGroupName = "FinacialGroupName";
        public const String PlanogramProductMetaNotAchievedInventory = "MetaNotAchievedInventory";
        public const String PlanogramProductMetaTotalUnits = "MetaTotalUnits";
        public const String PlanogramProductBlockingColour = "BlockingColour";
        public const String PlanogramProductMetaPlanogramLinearSpacePercentage = "MetaPlanogramLinearSpacePercentage";
        public const String PlanogramProductMetaPlanogramAreaSpacePercentage = "MetaPlanogramAreaSpacePercentage";
        public const String PlanogramProductMetaPlanogramVolumetricSpacePercentage = "MetaPlanogramVolumetricSpacePercentage";
        public const String PlanogramProductSequenceNumber = "SequenceNumber";
        public const String PlanogramProductMetaPositionCount = "MetaPositionCount";
        public const String PlanogramProductMetaTotalFacings = "MetaTotalFacings";
        public const String PlanogramProductMetaTotalLinearSpace = "MetaTotalLinearSpace";
        public const String PlanogramProductMetaTotalAreaSpace = "MetaTotalAreaSpace";
        public const String PlanogramProductMetaTotalVolumetricSpace = "MetaTotalVolumetricSpace";
        public const String PlanogramProductMetaIsInMasterData = "MetaIsInMasterData";
        public const String PlanogramProductMetaNotAchievedCases ="MetaNotAchievedCases";
        public const String PlanogramProductMetaNotAchievedDOS ="MetaNotAchievedDOS";
        public const String PlanogramProductMetaIsOverShelfLifePercent = "MetaIsOverShelfLifePercent";
        public const String PlanogramProductMetaNotAchievedDeliveries = "MetaNotAchievedDeliveries";
        public const String PlanogramProductMetaTotalMainFacings = "MetaTotalMainFacings";
        public const String PlanogramProductMetaTotalXFacings = "MetaTotalXFacings";
        public const String PlanogramProductMetaTotalYFacings = "MetaTotalYFacings";
        public const String PlanogramProductMetaTotalZFacings = "MetaTotalZFacings";
        public const String PlanogramProductMetaIsRangedInAssortment = "MetaIsRangedInAssortment";
        public const String PlanogramProductColourGroupValue = "ColourGroupValue";
        public const String PlanogramProductMetaCDTNode = "MetaCDTNode";
        public const String PlanogramProductMetaIsProductRuleBroken = "MetaIsProductRuleBroken";
        public const String PlanogramProductMetaIsFamilyRuleBroken = "MetaIsFamilyRuleBroken";
        public const String PlanogramProductMetaIsInheritanceRuleBroken = "MetaIsInheritanceRuleBroken";
        public const String PlanogramProductMetaIsLocalProductRuleBroken = "MetaIsLocalProductRuleBroken";
        public const String PlanogramProductMetaIsDistributionRuleBroken = "MetaIsDistributionRuleBroken";
        public const String PlanogramProductMetaIsCoreRuleBroken = "MetaIsCoreRuleBroken";
        public const String PlanogramProductMetaIsDelistProductRuleBroken = "MetaIsDelistProductRuleBroken";
        public const String PlanogramProductMetaIsForceProductRuleBroken = "MetaIsForceProductRuleBroken";
        public const String PlanogramProductMetaIsPreserveProductRuleBroken = "MetaIsPreserveProductRuleBroken";
        public const String PlanogramProductMetaIsMinimumHurdleProductRuleBroken = "MetaIsMinimumHurdleProductRuleBroken";
        public const String PlanogramProductMetaIsMaximumProductFamilyRuleBroken = "MetaIsMaximumProductFamilyRuleBroken";
        public const String PlanogramProductMetaIsMinimumProductFamilyRuleBroken = "MetaIsMinimumProductFamilyRuleBroken";
        public const String PlanogramProductMetaIsDependencyFamilyRuleBroken = "MetaIsDependencyFamilyRuleBroken";
        public const String PlanogramProductMetaIsDelistFamilyRuleBroken = "MetaIsDelistFamilyRuleBroken";
        public const String PlanogramProductMetaComparisonStatus = "MetaComparisonStatus";
        public const String PlanogramProductMetaIsBuddied = "MetaIsBuddied";

        #endregion

        #region PlanogramSubComponent

        public const String PlanogramSubComponentId = "Id";
        public const String PlanogramSubComponentPlanogramComponentId = "PlanogramComponentId";
        public const String PlanogramSubComponentMesh3DId = "Mesh3DId";
        public const String PlanogramSubComponentImageIdFront = "ImageIdFront";
        public const String PlanogramSubComponentImageIdBack = "ImageIdBack";
        public const String PlanogramSubComponentImageIdTop = "ImageIdTop";
        public const String PlanogramSubComponentImageIdBottom = "ImageIdBottom";
        public const String PlanogramSubComponentImageIdLeft = "ImageIdLeft";
        public const String PlanogramSubComponentImageIdRight = "ImageIdRight";
        public const String PlanogramSubComponentName = "Name";
        public const String PlanogramSubComponentHeight = "Height";
        public const String PlanogramSubComponentWidth = "Width";
        public const String PlanogramSubComponentDepth = "Depth";
        public const String PlanogramSubComponentX = "X";
        public const String PlanogramSubComponentY = "Y";
        public const String PlanogramSubComponentZ = "Z";
        public const String PlanogramSubComponentSlope = "Slope";
        public const String PlanogramSubComponentAngle = "Angle";
        public const String PlanogramSubComponentRoll = "Roll";
        public const String PlanogramSubComponentShapeType = "ShapeType";
        public const String PlanogramSubComponentMerchandisableHeight = "MerchandisableHeight";
        public const String PlanogramSubComponentMerchandisableDepth = "MerchandisableDepth";
        public const String PlanogramSubComponentIsVisible = "IsVisible";
        public const String PlanogramSubComponentHasCollisionDetection = "HasCollisionDetection";
        public const String PlanogramSubComponentFillPatternTypeFront = "FillPatternTypeFront";
        public const String PlanogramSubComponentFillPatternTypeBack = "FillPatternTypeBack";
        public const String PlanogramSubComponentFillPatternTypeTop = "FillPatternTypeTop";
        public const String PlanogramSubComponentFillPatternTypeBottom = "FillPatternTypeBottom";
        public const String PlanogramSubComponentFillPatternTypeLeft = "FillPatternTypeLeft";
        public const String PlanogramSubComponentFillPatternTypeRight = "FillPatternTypeRight";
        public const String PlanogramSubComponentFillColourFront = "FillColourFront";
        public const String PlanogramSubComponentFillColourBack = "FillColourBack";
        public const String PlanogramSubComponentFillColourTop = "FillColourTop";
        public const String PlanogramSubComponentFillColourBottom = "FillColourBottom";
        public const String PlanogramSubComponentFillColourLeft = "FillColourLeft";
        public const String PlanogramSubComponentFillColourRight = "FillColourRight";
        public const String PlanogramSubComponentLineColour = "LineColour";
        public const String PlanogramSubComponentTransparencyPercentFront = "TransparencyPercentFront";
        public const String PlanogramSubComponentTransparencyPercentBack = "TransparencyPercentBack";
        public const String PlanogramSubComponentTransparencyPercentTop = "TransparencyPercentTop";
        public const String PlanogramSubComponentTransparencyPercentBottom = "TransparencyPercentBottom";
        public const String PlanogramSubComponentTransparencyPercentLeft = "TransparencyPercentLeft";
        public const String PlanogramSubComponentTransparencyPercentRight = "TransparencyPercentRight";
        public const String PlanogramSubComponentFaceThicknessFront = "FaceThicknessFront";
        public const String PlanogramSubComponentFaceThicknessBack = "FaceThicknessBack";
        public const String PlanogramSubComponentFaceThicknessTop = "FaceThicknessTop";
        public const String PlanogramSubComponentFaceThicknessBottom = "FaceThicknessBottom";
        public const String PlanogramSubComponentFaceThicknessLeft = "FaceThicknessLeft";
        public const String PlanogramSubComponentFaceThicknessRight = "FaceThicknessRight";
        public const String PlanogramSubComponentRiserHeight = "RiserHeight";
        public const String PlanogramSubComponentRiserThickness = "RiserThickness";
        public const String PlanogramSubComponentIsRiserPlacedOnFront = "IsRiserPlacedOnFront";
        public const String PlanogramSubComponentIsRiserPlacedOnBack = "IsRiserPlacedOnBack";
        public const String PlanogramSubComponentIsRiserPlacedOnLeft = "IsRiserPlacedOnLeft";
        public const String PlanogramSubComponentIsRiserPlacedOnRight = "IsRiserPlacedOnRight";
        public const String PlanogramSubComponentRiserFillPatternType = "RiserFillPatternType";
        public const String PlanogramSubComponentRiserColour = "RiserColour";
        public const String PlanogramSubComponentRiserTransparencyPercent = "RiserTransparencyPercent";
        public const String PlanogramSubComponentNotchStartX = "NotchStartX";
        public const String PlanogramSubComponentNotchSpacingX = "NotchSpacingX";
        public const String PlanogramSubComponentNotchStartY = "NotchStartY";
        public const String PlanogramSubComponentNotchSpacingY = "NotchSpacingY";
        public const String PlanogramSubComponentNotchHeight = "NotchHeight";
        public const String PlanogramSubComponentNotchWidth = "NotchWidth";
        public const String PlanogramSubComponentIsNotchPlacedOnFront = "IsNotchPlacedOnFront";
        public const String PlanogramSubComponentIsNotchPlacedOnBack = "IsNotchPlacedOnBack";
        public const String PlanogramSubComponentIsNotchPlacedOnLeft = "IsNotchPlacedOnLeft";
        public const String PlanogramSubComponentIsNotchPlacedOnRight = "IsNotchPlacedOnRight";
        public const String PlanogramSubComponentNotchStyleType = "NotchStyleType";
        public const String PlanogramSubComponentDividerObstructionHeight = "DividerObstructionHeight";
        public const String PlanogramSubComponentDividerObstructionWidth = "DividerObstructionWidth";
        public const String PlanogramSubComponentDividerObstructionDepth = "DividerObstructionDepth";
        public const String PlanogramSubComponentDividerObstructionStartX = "DividerObstructionStartX";
        public const String PlanogramSubComponentDividerObstructionSpacingX = "DividerObstructionSpacingX";
        public const String PlanogramSubComponentDividerObstructionStartY = "DividerObstructionStartY";
        public const String PlanogramSubComponentDividerObstructionSpacingY = "DividerObstructionSpacingY";
        public const String PlanogramSubComponentDividerObstructionStartZ = "DividerObstructionStartZ";
        public const String PlanogramSubComponentDividerObstructionSpacingZ = "DividerObstructionSpacingZ";
        public const String PlanogramSubComponentIsDividerObstructionAtStart = "IsDividerObstructionAtStart";
        public const String PlanogramSubComponentIsDividerObstructionAtEnd = "IsDividerObstructionAtEnd";
        public const String PlanogramSubComponentIsDividerObstructionByFacing = "IsDividerObstructionByFacing";
        public const String PlanogramSubComponentDividerObstructionFillColour = "DividerObstructionFillColour";
        public const String PlanogramSubComponentDividerObstructionFillPattern = "DividerObstructionFillPattern";
        public const String PlanogramSubComponentMerchConstraintRow1StartX = "MerchConstraintRow1StartX";
        public const String PlanogramSubComponentMerchConstraintRow1SpacingX = "MerchConstraintRow1SpacingX";
        public const String PlanogramSubComponentMerchConstraintRow1StartY = "MerchConstraintRow1StartY";
        public const String PlanogramSubComponentMerchConstraintRow1SpacingY = "MerchConstraintRow1SpacingY";
        public const String PlanogramSubComponentMerchConstraintRow1Height = "MerchConstraintRow1Height";
        public const String PlanogramSubComponentMerchConstraintRow1Width = "MerchConstraintRow1Width";
        public const String PlanogramSubComponentMerchConstraintRow2StartX = "MerchConstraintRow2StartX";
        public const String PlanogramSubComponentMerchConstraintRow2SpacingX = "MerchConstraintRow2SpacingX";
        public const String PlanogramSubComponentMerchConstraintRow2StartY = "MerchConstraintRow2StartY";
        public const String PlanogramSubComponentMerchConstraintRow2SpacingY = "MerchConstraintRow2SpacingY";
        public const String PlanogramSubComponentMerchConstraintRow2Height = "MerchConstraintRow2Height";
        public const String PlanogramSubComponentMerchConstraintRow2Width = "MerchConstraintRow2Width";
        public const String PlanogramSubComponentLineThickness = "LineThickness";
        public const String PlanogramSubComponentMerchandisingType = "MerchandisingType";
        public const String PlanogramSubComponentCombineType = "CombineType";
        public const String PlanogramSubComponentIsProductOverlapAllowed = "IsProductOverlapAllowed";
        public const String PlanogramSubComponentIsProductSqueezeAllowed = "IsProductSqueezeAllowed";
        public const String PlanogramSubComponentMerchandisingStrategyX = "MerchandisingStrategyX";
        public const String PlanogramSubComponentMerchandisingStrategyY = "MerchandisingStrategyY";
        public const String PlanogramSubComponentMerchandisingStrategyZ = "MerchandisingStrategyZ";
        public const String PlanogramSubComponentLeftOverhang = "LeftOverhang";
        public const String PlanogramSubComponentRightOverhang = "RightOverhang";
        public const String PlanogramSubComponentFrontOverhang = "FrontOverhang";
        public const String PlanogramSubComponentBackOverhang = "BackOverhang";
        public const String PlanogramSubComponentTopOverhang = "TopOverhang";
        public const String PlanogramSubComponentBottomOverhang = "BottomOverhang";

        #endregion

        #region PlanogramValidationTemplate

        public const String PlanogramValidationTemplateId = "Id";
        public const String PlanogramValidationTemplatePlanogramId = "PlanogramId";
        public const String PlanogramValidationTemplateName = "Name";
        public const String PlanogramValidationTemplateExtendedData = "ExtendedData";

        #endregion

        #region PlanogramValidationTemplateGroup

        public const String PlanogramValidationTemplateGroupId = "Id";
        public const String PlanogramValidationTemplateGroupPlanogramValidationTemplateId = "PlanogramValidationTemplateId";
        public const String PlanogramValidationTemplateGroupName = "Name";
        public const String PlanogramValidationTemplateGroupThreshold1 = "Threshold1";
        public const String PlanogramValidationTemplateGroupThreshold2 = "Threshold2";
        public const String PlanogramValidationTemplateGroupResultType = "ResultType";
        public const String PlanogramValidationTemplateGroupValidationType = "ValidationType";
        public const String PlanogramValidationTemplateGroupExtendedData = "ExtendedData";

        #endregion

        #region PlanogramValidationTemplateGroupMetric

        public const String PlanogramValidationTemplateGroupMetricId = "Id";
        public const String PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId = "PlanogramValidationTemplateGroupId";
        public const String PlanogramValidationTemplateGroupMetricField = "Field";
        public const String PlanogramValidationTemplateGroupMetricThreshold1 = "Threshold1";
        public const String PlanogramValidationTemplateGroupMetricThreshold2 = "Threshold2";
        public const String PlanogramValidationTemplateGroupMetricScore1 = "Score1";
        public const String PlanogramValidationTemplateGroupMetricScore2 = "Score2";
        public const String PlanogramValidationTemplateGroupMetricScore3 = "Score3";
        public const String PlanogramValidationTemplateGroupMetricAggregationType = "AggregationType";
        public const String PlanogramValidationTemplateGroupMetricResultType = "ResultType";
        public const String PlanogramValidationTemplateGroupMetricValidationType = "ValidationType";
        public const String PlanogramValidationTemplateGroupMetricCriteria = "Criteria";
        public const String PlanogramValidationTemplateGroupMetricExtendedData = "ExtendedData";

        #endregion

        #region PlanogramRenumberingStrategy

        public const String PlanogramRenumberingStrategyId = "Id";
        public const String PlanogramRenumberingStrategyPlanogramId = "PlanogramId";
        public const String PlanogramRenumberingStrategyName = "Name";
        public const String PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority = "BayComponentXRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority = "BayComponentYRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority = "BayComponentZRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyPositionXRenumberStrategyPriority = "PositionXRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyPositionYRenumberStrategyPriority = "PositionYRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyPositionZRenumberStrategyPriority = "PositionZRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyBayComponentXRenumberStrategy = "BayComponentXRenumberStrategy";
        public const String PlanogramRenumberingStrategyBayComponentYRenumberStrategy = "BayComponentYRenumberStrategy";
        public const String PlanogramRenumberingStrategyBayComponentZRenumberStrategy = "BayComponentZRenumberStrategy";
        public const String PlanogramRenumberingStrategyPositionXRenumberStrategy = "PositionXRenumberStrategy";
        public const String PlanogramRenumberingStrategyPositionYRenumberStrategy = "PositionYRenumberStrategy";
        public const String PlanogramRenumberingStrategyPositionZRenumberStrategy = "PositionZRenumberStrategy";
        public const String PlanogramRenumberingStrategyRestartComponentRenumberingPerBay = "RestartComponentRenumberingPerBay";
        public const String PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents = "IgnoreNonMerchandisingComponents";
        public const String PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent = "RestartPositionRenumberingPerComponent";
        public const String PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent = "UniqueNumberMultiPositionProductsPerComponent";
        public const String PlanogramRenumberingStrategyExceptAdjacentPositions = "ExceptAdjacentPositions";
        public const String PlanogramRenumberingStrategyIsEnabled = "IsEnabled";

        #endregion

        #region PlanogramSequence

        public const String PlanogramSequenceId = "Id";
        public const String PlanogramSequencePlanogramId = "PlanogramId";

        #endregion

        #region PlanogramSequenceGroup

        public const String PlanogramSequenceGroupId = "Id";
        public const String PlanogramSequenceGroupPlanogramSequenceId = "PlanogramSequenceId";
        public const String PlanogramSequenceGroupName = "Name";
        public const String PlanogramSequenceGroupColour = "Colour";

        #endregion

        #region PlanogramSequenceGroupProduct

        public const String PlanogramSequenceGroupProductId = "Id";
        public const String PlanogramSequenceGroupProductPlanogramSequenceGroupId = "PlanogramSequenceGroupId";
        public const String PlanogramSequenceGroupProductGtin = "Gtin";
        public const String PlanogramSequenceGroupProductSequenceNumber = "SequenceNumber";
        public const String PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId = "PlanogramSequenceGroupSubGroupId";

        #endregion

        #region PlanogramSequenceGroupSubGroup

        public const String PlanogramSequenceGroupSubGroupId = "Id";
        public const String PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId = "PlanogramSequenceGroupId";
        public const String PlanogramSequenceGroupSubGroupName = "Name";
        public const String PlanogramSequenceGroupSubGroupAlignment = "Alignment";

        #endregion

        #region ProductAttributeComparisonResul

        public const String ProductAttributeComparisonResultId = "Id";
        public const String ProductAttributeComparisonResultPlanogramId = "PlanogramId";
        public const String ProductAttributeComparisonResultProductGtin = "ProductGtin";
        public const String ProductAttributeComparisonResultComparedProductAttribute = "ComparedProductAttribute";
        public const String ProductAttributeComparisonResultMasterDataValue = "MasterDataValue";
        public const String ProductAttributeComparisonResultProductValue = "ProductValue";

        #endregion
    }
}