﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-24290 : N.Foster
//  Created
// V8-24658 : K.Pickup
//  Fleshed out.
// V8-26156 : N.Foster
//  Converted to use framework classes
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Framework.Planograms.Dal.Pog
{
    public class DalFactory : Galleria.Framework.Dal.Gbf.DalFactoryBase<DalCache>
    {
        #region Properties
        /// <summary>
        /// Returns the database type name
        /// </summary>
        public override String DatabaseTypeName
        {
            get { return "Galleria Planogram File"; }
        }
        #endregion

        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion
    }
}
