﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24658 : K.Pickup
//  Added DalContext and DalCache properties.
#endregion
#endregion

using System;

using Galleria.Framework.Dal;

namespace Galleria.Framework.Planograms.Dal.Pog
{
    public class DalBase : Galleria.Framework.Dal.DalBase
    {
        #region Properties
        /// <summary>
        /// The context for this dal
        /// </summary>
        public DalContext DalContext
        {
            get { return (DalContext)((IDal)this).DalContext; }
        }

        /// <summary>
        /// The DalCache for this dal
        /// </summary>
        internal DalCache DalCache
        {
            get { return DalContext.DalCache; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when disposing of this instance
        /// </summary>
        public override void Dispose()
        {
            // Nothing to do
        }
        #endregion
    }
}
