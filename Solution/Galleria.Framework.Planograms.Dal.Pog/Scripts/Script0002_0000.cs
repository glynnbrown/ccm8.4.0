﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM801
//V8-27636 L:Ineson
//  Initial version
//  Added PlanogramProduct.MetaTotalUnits property
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0002_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region PlanogramProduct

            // MetaTotalUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMetaTotalUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMetaTotalUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region PlanogramProduct

            // MetaTotalUnits
            if (file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMetaTotalUnits))
            {
                file.RemoveItemProperty((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMetaTotalUnits);
            }

            #endregion
        }

        #endregion

    }
}
