﻿using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0010_0000 : IScript
    {
        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Item Properties

            #region PlanogramPerformanceData

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP1,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP2,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP3,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP4,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP5,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP6,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP7,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP8,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP9,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP10,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP1Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP2Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP3Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP4Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP5Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP6Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP7Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP8Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP9Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP10Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP1Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP2Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP3Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP4Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP5Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP6Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP7Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP8Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP9Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP10Rank,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);
            #endregion

            #endregion Add Item Properties
        }

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Remove Item Properties

            #region PlanogramPerformanceData

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP1);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP2);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP3);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP4);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP5);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP6);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP7);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP8);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP9);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataCP10);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP1Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP2Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP3Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP4Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP5Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP6Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP7Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP8Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP9Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP10Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP1Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP2Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP3Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP4Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP5Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP6Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP7Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP8Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP9Rank);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaCP10Rank);

            #endregion

            #endregion Remove Item Properties
        }
    }
}
