﻿using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0012_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Sections

            // Note: Order is important here.  The sections will be written to the file in the order in which they are
            // added.  As a general rule of thumb, therefore, the sections should be added in descending order by
            // anticipated size.  This is because if a section has to be expanded to accomodate new data, all sections
            // following it will have to be moved down, and moving significant number of bytes around in a file takes
            // time.  In one test involving around 5000 images (a reasonable number of images for a Maxima planogram)
            // of 32 KB each (no idea what size the images will be in real life) moving the images section up from
            // last in the file reduced saving time for a small change (adding a new fixture item) from around 2.5 
            // seconds to 267 milliseconds.

            file.AddSectionIfMissing(SectionType.ProductAttributeComparisonResults);

            #endregion

            #region Add Item Properties

            #region ProductAttributeComparisonResults

            //  ProductAttributeComparisonResultId
            file.AddItemPropertyIfMissing(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  ProductAttributeComparisonResultPlanogramId
            file.AddItemPropertyIfMissing(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultPlanogramId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  ProductAttributeComparisonResultProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  ProductAttributeComparisonResultComparedProductAttribute
            file.AddItemPropertyIfMissing(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultComparedProductAttribute,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  ProductAttributeComparisonResultMasterDataValue
            file.AddItemPropertyIfMissing(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultMasterDataValue,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  ProductAttributeComparisonResultProductValue 
            file.AddItemPropertyIfMissing(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultProductValue,
                GalleriaBinaryFile.ItemPropertyType.String);


            #endregion

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {

            #region ProductAttributeComparisonResults

            //  ProductAttributeComparisonResultId
            file.RemoveItemPropertyIfPresent(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultId);

            //  ProductAttributeComparisonResultPlanogramId
            file.RemoveItemPropertyIfPresent(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultPlanogramId);

            //  ProductAttributeComparisonResultProductGtin
            file.RemoveItemPropertyIfPresent(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultProductGtin);

            //  ProductAttributeComparisonResultComparedProductAttribute
            file.RemoveItemPropertyIfPresent(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultComparedProductAttribute);

            //  ProductAttributeComparisonResultMasterDataValue
            file.RemoveItemPropertyIfPresent(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultMasterDataValue);

            //  ProductAttributeComparisonResultProductValue 
            file.RemoveItemPropertyIfPresent(
                SectionType.ProductAttributeComparisonResults,
                FieldNames.ProductAttributeComparisonResultProductValue);

            #endregion

            #region Remove Sections

            file.RemoveSection((UInt16)SectionType.ProductAttributeComparisonResults);

            #endregion
        }

        #endregion
    }
}
