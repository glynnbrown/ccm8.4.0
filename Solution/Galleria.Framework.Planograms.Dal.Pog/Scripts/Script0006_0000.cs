﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811

// V8-30350 : L.Ineson
//  Initial version.
// V8-30164 : A.Silva
//  Added Changes to Property Types for PlanogramMetaAverageFacings, PlanogramMetaAverageUnits, PlanogramMetaSpaceToUnitsIndex.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0006_0000 : IScript
    {
        #region Updgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Changes to Property Types

            #region Planogram

            file.SetItemPropertyTypeIfPresent(SectionType.Planograms,
                                              FieldNames.PlanogramMetaAverageFacings,
                                              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                                              ConvertToSingle);
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms,
                                              FieldNames.PlanogramMetaAverageUnits,
                                              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                                              ConvertToSingle);
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms,
                                              FieldNames.PlanogramMetaSpaceToUnitsIndex,
                                              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                                              ConvertToSingle);

            #endregion

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Changes to Property Types

            #region Planogram

            file.SetItemPropertyTypeIfPresent(SectionType.Planograms,
                                              FieldNames.PlanogramMetaAverageFacings,
                                              GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                                              ConvertToInt16);
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms,
                                              FieldNames.PlanogramMetaAverageUnits,
                                              GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                                              ConvertToInt16);
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms,
                                              FieldNames.PlanogramMetaSpaceToUnitsIndex,
                                              GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                                              ConvertToInt16);

            #endregion

            #endregion
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Simple comversion to <c>Single</c> for the given <paramref name="value"/>.
        /// </summary>
        /// <param name="value">The value to be converted.</param>
        /// <returns>An object containing the <paramref name="value"/> converted to <c>Single</c>.</returns>
        private static Object ConvertToSingle(Object value)
        {
            return Convert.ToSingle(value);
        }

        /// <summary>
        ///     Simple comversion to <c>Int16</c> for the given <paramref name="value"/>.
        /// </summary>
        /// <param name="value">The value to be converted.</param>
        /// <returns>An object containing the <paramref name="value"/> converted to <c>Int16</c>.</returns>
        private static Object ConvertToInt16(Object value)
        {
            return Convert.ToInt16(value);
        }

        #endregion
    }
}