﻿using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0011_0000 : IScript
    {
        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Item Properties

            #region Note Properties

            file.AddItemPropertyIfMissing(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote1,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);

            file.AddItemPropertyIfMissing(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote2,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);

            file.AddItemPropertyIfMissing(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote3,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);


            file.AddItemPropertyIfMissing(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote4,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);

            file.AddItemPropertyIfMissing(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote5,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);

            #endregion

            #endregion Add Item Properties
        }

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Remove Item Properties

            #region Note Properties

            file.RemoveItemPropertyIfPresent(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote1);

            file.RemoveItemPropertyIfPresent(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote2);

            file.RemoveItemPropertyIfPresent(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote3);


            file.RemoveItemPropertyIfPresent(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote4);

            file.RemoveItemPropertyIfPresent(
                    SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataNote5);

            #endregion

            #endregion Remove Item Properties
        }
    }
}
