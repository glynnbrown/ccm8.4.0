﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.2.0
// V8-29998 : I.George
//	Added PlanogramPerformanceData_MetaP1Rank to PlanogramPerformanceData_MetaP20Rank fields
// V8-30773 : J.Pickup
//	Added PlanogramInventory_InventoryMetricType
// V8-30762 : I.George
//  Added PlanogramAssortmentProduct MetaData property
// V8-30763 :I.George
//  Added ConsumerDecisionTreeNode MetaData Fields
// V8-30763 :I.George
//	Added MetaCDTNode field to PlanogramProduct 
// V8-30765 :I.George
//	Added PlanogramBlockingGroup metaData Properties
// V8-30705 : A.Probyn
//  Added DelistedDueToAssortmentRule to PlanogramAssortmentProduct
//	Added Planogram and PlanogramProduct to include new assortment rules related fields
// V8-30193 : L.Ineson
//  Added PlanogramRenumberingStrategy.IsEnabled property
// V8-30992 : A.Probyn
//	Added PlanogramPerformanceData_MinimumInventoryUnits
// V8-30873 : L.Ineson
//  Removed PlanogramComponent_IsCarPark
// V8-30737 : M.Shelley
//  Added Consumer Decision Tree performance details
// V8-31131 : A.Probyn
//  Removed Planogram_MetaSpaceToUnitsIndex
// V8-30737 : M.Shelley
//	Change previously defined meta percent field type from Int32? to Single?

// V8-31164 : D.Pleasance
//  Added Sequence Number \ Sequence Colour to Planogram Position
//  Removed Sequence Number \ Blocking Colour from Planogram Product
#endregion

#endregion

using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;
using Galleria.Framework.Planograms.Model;
using System;
using Galleria.Framework.Planograms.Dal.Pog.Implementation.Items;
using System.Linq;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0007_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Item Properties
            
            #region Planogram

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramHighlightSequenceStrategy,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfAssortmentRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfFamilyRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfInheritanceRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfLocalProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfDistributionRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfCoreRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentageOfAssortmentRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentageOfProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentageOfFamilyRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentageOfInheritanceRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentageOfLocalProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentageOfDistributionRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentageOfCoreRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfDelistProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfForceProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfPreserveProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfMinimumHurdleProductRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfMaximumProductFamilyRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfMinimumProductFamilyRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfDependencyFamilyRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);  

            #endregion

            #region PlanogramProduct

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductColourGroupValue,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaCDTNode,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsProductRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsFamilyRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsInheritanceRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsLocalProductRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsDistributionRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsCoreRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsDelistProductRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsForceProductRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsPreserveProductRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsMinimumHurdleProductRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsMaximumProductFamilyRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsMinimumProductFamilyRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsDependencyFamilyRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);

            #endregion

            #region PlanogramPerformanceData

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramPerformanceDatas,
               FieldNames.PlanogramPerformanceDataMetaP1Rank,
               GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP2Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP3Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP4Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP5Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP6Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP7Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP8Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP9Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP10Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP11Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP12Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP13Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP14Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP15Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP16Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP17Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP18Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP19Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP20Rank,
              GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMinimumInventoryUnits,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            #endregion

            #region PlanogramInventory

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramInventory,
                FieldNames.PlanogramInventoryInventoryMetricType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                (Byte)0);

            #endregion

            #region PlanogramAssortmentProduct
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProducts,
                FieldNames.PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);
            
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProducts,
                FieldNames.PlanogramAssortmentProductDelistedDueToAssortmentRule,
                GalleriaBinaryFile.ItemPropertyType.Boolean,
                false);

            #endregion

            #region PlanogramConsumerDecisionTreeNode

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramConsumerDecisionTreeNodes,
               FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProducts,
               GalleriaBinaryFile.ItemPropertyType.NullableInt32,
               null);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramConsumerDecisionTreeNodes,
               FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment,
               GalleriaBinaryFile.ItemPropertyType.NullableInt32,
               null);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramConsumerDecisionTreeNodes,
               FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram,
               GalleriaBinaryFile.ItemPropertyType.NullableInt32,
               null);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramConsumerDecisionTreeNodes,
               FieldNames.PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle,
               null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP1,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP2,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP3,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP4,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP5,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP6,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP7,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP8,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP9,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP10,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP11,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP12,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP13,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP14,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP15,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP16,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP17,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP18,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP19,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP20,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Percentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle,
              null);

            #endregion

            #region PlanogramBlockingGroup

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaOriginalPercentAllocated,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaPerformancePercentAllocated,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaFinalPercentAllocated,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaLinearProductPlacementPercent,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaAreaProductPlacementPercent,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaVolumetricProductPlacementPercent,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaCountOfProducts,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP1,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP2,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP3,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP4,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP5,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP6,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP7,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP8,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP9,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP10,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP11,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP12,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP13,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP14,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP15,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP16,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP17,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP18,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP19,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupP20,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP1Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP2Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP3Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP4Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP5Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP6Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP7Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP8Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP9Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP10Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP11Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP12Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP13Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP14Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP15Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP16Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP17Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP18Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP19Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaP20Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            #endregion

            #region PlanogramRenumberingStrategy

            file.AddItemPropertyIfMissing(
             SectionType.PlanogramRenumberingStrategy,
             FieldNames.PlanogramRenumberingStrategyIsEnabled,
             GalleriaBinaryFile.ItemPropertyType.Boolean,
             true);

            #endregion

            #region PlanogramPosition

            // SequenceNumber
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionSequenceNumber,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);
            // SequenceColour
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionSequenceColour,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            #endregion

            #endregion

            #region Update Sequence Number \ Sequence Colour

            if (file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSequenceNumber))
            {
                var products = file.GetItems((UInt16)SectionType.PlanogramProducts).ToDictionary<Galleria.Framework.IO.GalleriaBinaryFile.IEditableItem, Object>(
                                                                                        item => item.GetItemPropertyValue(FieldNames.PlanogramProductId));

                var positions = file.GetItems((UInt16)SectionType.PlanogramPositions);
                foreach (var position in positions)
                {
                    var product = products[position.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramProductId)];

                    if (product != null)
                    {
                        position.SetItemPropertyValue(FieldNames.PlanogramPositionSequenceNumber, product.GetItemPropertyValue(FieldNames.PlanogramProductSequenceNumber));
                        position.SetItemPropertyValue(FieldNames.PlanogramPositionSequenceColour, product.GetItemPropertyValue(FieldNames.PlanogramProductBlockingColour));
                    }
                }
            }
            #endregion

            #region Remove Item Properties

            #region PlanogramComponent

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramComponents,
               FieldNames.PlanogramComponentIsCarPark);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaAggregatedPerformanceOfProduct);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaPercentOfPerformance);

            #endregion

            #region PlanogramProduct

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramProducts,
               FieldNames.PlanogramProductBlockingColour);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramProducts,
               FieldNames.PlanogramProductSequenceNumber);
            
            #endregion

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Add Item Properties

            #region PlanogramComponent

            file.AddItemPropertyIfMissing(
             SectionType.PlanogramComponents,
             FieldNames.PlanogramComponentIsCarPark,
             GalleriaBinaryFile.ItemPropertyType.Boolean,
             false);

            #endregion

            #region PlanogramProduct

            file.AddItemPropertyIfMissing(
             SectionType.PlanogramProducts,
             FieldNames.PlanogramProductSequenceNumber,
             GalleriaBinaryFile.ItemPropertyType.NullableInt32,
             null);

            file.AddItemPropertyIfMissing(
             SectionType.PlanogramProducts,
             FieldNames.PlanogramProductBlockingColour,
             GalleriaBinaryFile.ItemPropertyType.NullableInt32,
             null);

            #endregion

            #endregion

            #region Update Sequence Number \ Blocking Colour

            if (file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionSequenceNumber))
            {
                Dictionary<Object, GalleriaBinaryFile.IEditableItem> positionsLookup = new Dictionary<Object, GalleriaBinaryFile.IEditableItem>();

                foreach (var position in file.GetItems((UInt16)SectionType.PlanogramPositions))
                {
                    Object planogramPositionPlanogramProductId = position.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramProductId);

                    if (!positionsLookup.ContainsKey(planogramPositionPlanogramProductId))
                    {
                        positionsLookup.Add(planogramPositionPlanogramProductId, position);
                    }
                }

                var products = file.GetItems((UInt16)SectionType.PlanogramProducts);
                foreach (var product in products)
                {
                    var position = positionsLookup[product.GetItemPropertyValue(FieldNames.PlanogramProductId)];

                    if (position != null)
                    {
                        product.SetItemPropertyValue(FieldNames.PlanogramProductSequenceNumber, product.GetItemPropertyValue(FieldNames.PlanogramPositionSequenceNumber));
                        product.SetItemPropertyValue(FieldNames.PlanogramProductBlockingColour, product.GetItemPropertyValue(FieldNames.PlanogramPositionSequenceColour));
                    }
                }                
            }
            #endregion

            #region Remove Item Properties

            #region Planogram

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramHighlightSequenceStrategy);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaSpaceToUnitsIndex);

            #endregion

            #region PlanogramProduct

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductColourGroupValue);
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaCDTNode);

            #endregion

            #region PlanogramPerformanceData

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramPerformanceDatas,
               FieldNames.PlanogramPerformanceDataMetaP1Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP2Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP3Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP4Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP5Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP6Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP7Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP8Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP9Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP10Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP11Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP12Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP13Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP14Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP15Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP16Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP17Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP18Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP19Rank);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPerformanceDatas,
              FieldNames.PlanogramPerformanceDataMetaP20Rank);
            #endregion

            #region PlanogramInventory

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramInventory,
                FieldNames.PlanogramInventoryInventoryMetricType);

            #endregion

            #region PlanogramAssortmentProduct
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramAssortmentProducts,
                FieldNames.PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram);
            #endregion

            #region PlanogramConsumerDecisionTreeNode

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProducts);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaAggregatedPerformanceOfProduct);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaPercentOfPerformance);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramConsumerDecisionTreeNodes,
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP1);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP2);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP3);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP4);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP5);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP6);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP7);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP8);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP9);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP10);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP11);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP12);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP13);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP14);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP15);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP16);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP17);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP18);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP19);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeP20);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramConsumerDecisionTreeNodes,
              FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Percentage);

            #endregion

            #region PlanogramBlockingGroup

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaCountOfProducts);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaOriginalPercentAllocated);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaPerformancePercentAllocated);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaFinalPercentAllocated);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaLinearProductPlacementPercent);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaAreaProductPlacementPercent);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupMetaVolumetricProductPlacementPercent);

            #endregion

            #region PlanogramRenumberingStrategy

            file.RemoveItemPropertyIfPresent(
             SectionType.PlanogramRenumberingStrategy,
             FieldNames.PlanogramRenumberingStrategyIsEnabled);

            #endregion

            #region PlanogramPosition

            file.RemoveItemPropertyIfPresent(
             SectionType.PlanogramPositions,
             FieldNames.PlanogramPositionSequenceNumber);

            file.RemoveItemPropertyIfPresent(
             SectionType.PlanogramPositions,
             FieldNames.PlanogramPositionSequenceColour);

            #endregion

            #endregion
        }

        #endregion
    }
}
