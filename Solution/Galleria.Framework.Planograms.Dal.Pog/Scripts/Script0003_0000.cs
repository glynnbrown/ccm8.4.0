﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-28987 : I.George
//      Initial version
// V8-28996 : A.Silva
//      Added PlanogramSequence, PlanogramSequenceGroups and PlanogramSequenceGroupProducts.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-28766 : J.Pickup
//      Added PlanogramPosistion IsManuallyPlaced property.
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
// V8-29023 : M.Pettit
//  Added Merchandisable Depth
// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-29028 : L.Ineson
// Added position squeeze properties.
// V8-28840 : L.Luong
//  Added EntityId to Package
// V8-29054 : M.Pettit
//  Added Position validation meta fields
// V8-28811 : L.Luong
//  Added Meta fields for PlanogramProduct, PlanogramPerformanceData, PlanogramFixtureComponent, PlanogramAssemblyComponent
// V8-29232 : A.Silva
//      Added PlanogramProductSequenceNumber.
// V8-29023 : M.Pettit
//  Added default value for Merchandisable Depth
#endregion
#region Version History: CCM810
//V8-29908 : M.Pettit
//  Non-nullable properties should all have default values
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0003_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Remove Item Properties

            #region PlanogramBlockingGroup
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupProductAssignType);
            #endregion

            #endregion

            #region Remove Sections

            file.RemoveSectionIfPresent(SectionType.PlanogramBlockingGroupFields);
            file.RemoveSectionIfPresent(SectionType.PlanogramBlockingGroupProducts);

            #endregion

            #region Add Sections

            // Note: Order is important here.  The sections will be written to the file in the order in which they are
            // added.  As a general rule of thumb, therefore, the sections should be added in descending order by
            // anticipated size.  This is because if a section has to be expanded to accomodate new data, all sections
            // following it will have to be moved down, and moving significant number of bytes around in a file takes
            // time.  In one test involving around 5000 images (a reasonable number of images for a Maxima planogram)
            // of 31 KB each (no idea what size the images will be in real life) moving the images section up from
            // last in the file reduced saving time for a small change (adding a new fixture item) from around 2.5 
            // seconds to 167 milliseconds.

            file.AddSectionIfMissing(SectionType.PlanogramSequence);
            file.AddSectionIfMissing(SectionType.PlanogramSequenceGroups);
            file.AddSectionIfMissing(SectionType.PlanogramSequenceGroupProducts);

            #endregion

            #region Add Item Properties

            #region Package

            // EntityId 
            file.AddItemPropertyIfMissing(
                SectionType.Package,
                FieldNames.PackageEntityId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            #endregion

            #region Planogram

            //Location Code
            if (!file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramLocationCode))
            {
                file.AddItemProperty(
                    (UInt16) SectionType.Planograms,
                    FieldNames.PlanogramLocationCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Location Name
            if (!file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramLocationName))
            {
                file.AddItemProperty(
                    (UInt16) SectionType.Planograms,
                    FieldNames.PlanogramLocationName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Cluster Scheme Name
            if (!file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramClusterSchemeName))
            {
                file.AddItemProperty(
                    (UInt16) SectionType.Planograms,
                    FieldNames.PlanogramClusterSchemeName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }


            //Cluster Name
            if (!file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramClusterName))
            {
                file.AddItemProperty(
                    (UInt16) SectionType.Planograms,
                    FieldNames.PlanogramClusterName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramSequence

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequence, 
                FieldNames.PlanogramSequenceId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequence, 
                FieldNames.PlanogramSequencePlanogramId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);

            #endregion

            #region PlanogramSequenceGroups

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroups,
                FieldNames.PlanogramSequenceGroupId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroups,
                FieldNames.PlanogramSequenceGroupPlanogramSequenceId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroups,
                FieldNames.PlanogramSequenceGroupName,
                GalleriaBinaryFile.ItemPropertyType.String);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroups,
                FieldNames.PlanogramSequenceGroupColour,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);

            #endregion

            #region PlanogramSequenceGroupProducts

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupProducts,
                FieldNames.PlanogramSequenceGroupProductId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupProducts,
                FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupProducts,
                FieldNames.PlanogramSequenceGroupProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String);
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupProducts,
                FieldNames.PlanogramSequenceGroupProductSequenceNumber,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                (Int32)0);

            #endregion

            #region PlanogramPosition

            //IsManuallyPlaced
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionIsManuallyPlaced,
                GalleriaBinaryFile.ItemPropertyType.Boolean,
                false);

            //HorizontalSqueeze
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionHorizontalSqueeze,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //VerticalSqueeze
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionVerticalSqueeze,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //DepthSqueeze
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionDepthSqueeze,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //HorizontalSqueezeX
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionHorizontalSqueezeX,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //VerticalSqueezeX
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionVerticalSqueezeX,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //DepthSqueezeX
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionDepthSqueezeX,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //HorizontalSqueezeY
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionHorizontalSqueezeY,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //VerticalSqueezeY
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionVerticalSqueezeY,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //DepthSqueezeY
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionDepthSqueezeY,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //HorizontalSqueezeZ
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionHorizontalSqueezeZ,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //VerticalSqueezeZ
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionVerticalSqueezeZ,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            //DepthSqueezeZ
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionDepthSqueezeZ,
                GalleriaBinaryFile.ItemPropertyType.Single, 1F);

            // PlanogramPositionMetaIsUnderMinDeep
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaIsUnderMinDeep,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsOverMaxDeep
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsOverMaxDeep,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsOverMaxRightCap
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsOverMaxRightCap,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsOverMaxStack
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsOverMaxStack,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsOverMaxTopCap
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsOverMaxTopCap,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsInvalidMerchandisingStyle
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsInvalidMerchandisingStyle,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsHangingTray
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsHangingTray,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsPegOverfilled
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsPegOverfilled,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            // PlanogramPositionMetaIsOutsideMerchandisingSpace
            file.AddItemPropertyIfMissing(
                    SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsOutsideMerchandisingSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean, null);

            #endregion

			#region PlanogramBlockingGroup

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupBlockPlacementPrimaryType,
                GalleriaBinaryFile.ItemPropertyType.Byte, (Byte)0);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupBlockPlacementSecondaryType,
                GalleriaBinaryFile.ItemPropertyType.Byte, (Byte)0);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupBlockPlacementTertiaryType,
                GalleriaBinaryFile.ItemPropertyType.Byte, (Byte)0);

            #endregion

            #region PlanogramSubComponent

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSubComponents,
                FieldNames.PlanogramSubComponentMerchandisableDepth,
                GalleriaBinaryFile.ItemPropertyType.Single, 0F);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSubComponents,
                FieldNames.PlanogramSubComponentIsProductSqueezeAllowed,
                GalleriaBinaryFile.ItemPropertyType.Boolean, true);

            #endregion

            #region PlanogramProduct
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductBlockingColour,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPlanogramLinearSpacePercentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPlanogramAreaSpacePercentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPlanogramVolumetricSpacePercentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductSequenceNumber,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            #endregion

            #region PlanogramPerformanceData

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP1Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP2Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP3Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP4Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP5Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP6Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP7Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP8Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP9Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP10Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP11Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP12Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP13Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP14Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP15Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP16Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP17Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP18Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP19Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP20Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            #endregion

            #region PlanogramFixtureComponent

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramFixtureComponents,
                FieldNames.PlanogramFixtureComponentMetaPercentageLinearSpaceFilled,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            #endregion

            #region PlanogramAssemblyComponent

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            #endregion

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Add Item Properties

            #region PlanogramBlockingGroup
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups, 
                FieldNames.PlanogramBlockingGroupProductAssignType,
                GalleriaBinaryFile.ItemPropertyType.Byte);
            #endregion

            #endregion

            #region Add Sections

            file.AddSectionIfMissing(SectionType.PlanogramBlockingGroupFields);
            file.AddSectionIfMissing(SectionType.PlanogramBlockingGroupProducts);

            #endregion

            #region Remove Item Properties

            #region Package

            // EntityId
            file.RemoveItemPropertyIfPresent(SectionType.Package, FieldNames.PackageEntityId);

            #endregion

            #region Planogram

            // Location Code
            if (file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramLocationCode))
                file.RemoveItemProperty((UInt16) SectionType.Planograms, FieldNames.PlanogramLocationCode);

            // Location Name
            if (file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramLocationName))
                file.RemoveItemProperty((UInt16) SectionType.Planograms, FieldNames.PlanogramLocationName);

            // Cluster Scheme Name
            if (file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramClusterSchemeName))
                file.RemoveItemProperty((UInt16) SectionType.Planograms, FieldNames.PlanogramClusterSchemeName);

            // Cluster Name
            if (file.ItemPropertyExists((UInt16) SectionType.Planograms, FieldNames.PlanogramClusterName))
                file.RemoveItemProperty((UInt16) SectionType.Planograms, FieldNames.PlanogramClusterName);

            #endregion

            #region PlanogramPosition

            // IsManuallyPlaced
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionIsManuallyPlaced);

            //HorizontalSqueeze
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionHorizontalSqueeze);
            
            //VerticalSqueeze
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionVerticalSqueeze);

            //DepthSqueeze
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionDepthSqueeze);

            //HorizontalSqueezeX
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionHorizontalSqueezeX);

            //VerticalSqueezeX
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionVerticalSqueezeX);

            //DepthSqueezeX
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionDepthSqueezeX);

            //HorizontalSqueezeY
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionHorizontalSqueezeY);

            //VerticalSqueezeY
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionVerticalSqueezeY);

            //DepthSqueezeY
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionDepthSqueezeY);

            //HorizontalSqueezeZ
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionHorizontalSqueezeZ);

            //VerticalSqueezeZ
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionVerticalSqueezeZ);

            //DepthSqueezeZ
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionDepthSqueezeZ);

            //MetaIsUnderMinDeep
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsUnderMinDeep);

            //MetaIsOverMaxDeep
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsOverMaxDeep);

            //MetaIsOverMaxRightCap
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsOverMaxRightCap);

            //MetaIsOverMaxStack
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsOverMaxStack);

            // MetaIsOverMaxTopCap
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsOverMaxTopCap);

            //MetaIsInvalidMerchandisingStyle
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsInvalidMerchandisingStyle);

            // PlanogramPositionMetaIsHangingTray
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsHangingTray);

            // PlanogramPositionMetaIsPegOverfilled
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsPegOverfilled);

            // PlanogramPositionMetaIsOutsideMerchandisingSpace
            file.RemoveItemPropertyIfPresent(SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsOutsideMerchandisingSpace);
            #endregion

			#region PlanogramBlockingGroup

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupBlockPlacementPrimaryType);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupBlockPlacementSecondaryType);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupBlockPlacementTertiaryType);

            #endregion

            #region PlanogramSubComponent

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSubComponents,
                FieldNames.PlanogramSubComponentMerchandisableDepth);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSubComponents,
                FieldNames.PlanogramSubComponentIsProductSqueezeAllowed);

            #endregion

            #region PlanogramProduct
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductBlockingColour);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPlanogramLinearSpacePercentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPlanogramAreaSpacePercentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPlanogramVolumetricSpacePercentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductSequenceNumber);

            #endregion

            #region PlanogramPerformanceData

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP1Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP2Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP3Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP4Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP5Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP6Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP7Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP8Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP9Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP10Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP11Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP12Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP13Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP14Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP15Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP16Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP17Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP18Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP19Percentage);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPerformanceDatas,
                FieldNames.PlanogramPerformanceDataMetaP20Percentage);

            #endregion
            
            #region PlanogramFixtureComponent

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramFixtureComponents,
                FieldNames.PlanogramFixtureComponentMetaPercentageLinearSpaceFilled);

            #endregion

            #region PlanogramAssemblyComponent

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled);

            #endregion

            #endregion

            #region Remove Sections

            file.RemoveSectionIfPresent(SectionType.PlanogramSequenceGroupProducts);
            file.RemoveSectionIfPresent(SectionType.PlanogramSequenceGroups);
            file.RemoveSectionIfPresent(SectionType.PlanogramSequence);

            #endregion
        }

        #endregion
    }

    #region InternalExtensions

    internal static class Extensions
    {
        /// <summary>
        ///     Adds the given <paramref name="sectionType" /> to the <paramref name="file" /> if it does not exist.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> to add the <see cref="SectionType" /> if it is missing.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> to add to the <see cref="GalleriaBinaryFile" />.</param>
        /// <param name="encrypted">Whether to enable Aes 256 encryption on the <see cref="SectionType" />.</param>
        internal static void AddSectionIfMissing(this GalleriaBinaryFile file, SectionType sectionType,
            Boolean encrypted = false)
        {
            UInt16 type = (UInt16) sectionType;
            if (!file.SectionExists(type)) file.CreateSection(type);
            if (encrypted) file.EnableAes256Encryption(type);
        }

        /// <summary>
        ///     Adds a given <paramref name="fieldName" /> to the <see cref="SectionType" /> in the <paramref name="file" />.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> that will be updated.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> that the item property belongs to.</param>
        /// <param name="fieldName">The name of the item property.</param>
        /// <param name="itemPropertyType">The type of data held in the item property.</param>
        /// <param name="defaultValue">[OPTIONAL] The default value for the item property when it needs to be added.</param>
        internal static void AddItemPropertyIfMissing(this GalleriaBinaryFile file, SectionType sectionType,
            String fieldName,
            GalleriaBinaryFile.ItemPropertyType itemPropertyType, Object defaultValue = null)
        {
            UInt16 type = (UInt16) sectionType;
            if (!file.ItemPropertyExists(type, fieldName))
                file.AddItemProperty(type, fieldName, itemPropertyType, defaultValue);
        }

        /// <summary>
        ///     Adds the given <paramref name="sectionType" /> to the <paramref name="file" /> if it does not exist.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> to add the <see cref="SectionType" /> if it is missing.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> to add to the <see cref="GalleriaBinaryFile" />.</param>
        internal static void RemoveSectionIfPresent(this GalleriaBinaryFile file, SectionType sectionType)
        {
            UInt16 type = (UInt16) sectionType;
            if (file.SectionExists(type)) file.RemoveSection(type);
        }

        /// <summary>
        ///     Removes a given <paramref name="fieldName" /> to the <see cref="SectionType" /> in the <paramref name="file" />.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> that will be updated.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> that the item property belongs to.</param>
        /// <param name="fieldName">The name of the item property.</param>
        internal static void RemoveItemPropertyIfPresent(this GalleriaBinaryFile file, SectionType sectionType,
            String fieldName)
        {
            UInt16 type = (UInt16) sectionType;
            if (file.ItemPropertyExists(type, fieldName))
                file.RemoveItemProperty(type, fieldName);
        }

        /// <summary>
        ///     Removes a given <paramref name="fieldName" /> to the <see cref="SectionType" /> in the <paramref name="file" />.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> that will be updated.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> that the item property belongs to.</param>
        /// <param name="fieldName">The name of the item property.</param>
        internal static void SetItemPropertyTypeIfPresent(this GalleriaBinaryFile file, SectionType sectionType,
            String fieldName, GalleriaBinaryFile.ItemPropertyType propertyType , Func<Object,Object> converter)
        {
            UInt16 type = (UInt16)sectionType;
            if (file.ItemPropertyExists(type, fieldName))
                file.SetItemPropertyType(type, fieldName, propertyType, converter);
        }
    }

    #endregion
}