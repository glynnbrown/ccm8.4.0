﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30011 : L.Ineson
//  Added position level linear, area and volumetric metadata.
// V8-30036 : L.Ineson
//  Added PlanogramMetaPercentOfProductNotAchievedInventory
// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0005_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Remove Item Properties

            #region PlanogramAssemblyComponent
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentMetaComponentCount);
            #endregion

            #endregion

            #region Add Item Properties

            #region Planogram

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductNotAchievedCases,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductNotAchievedDOS,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductOverShelfLifePercent,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductNotAchievedDeliveries,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedCases,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedDOS,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductOverShelfLifePercent,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedDeliveries,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);


            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfPositionsOutsideOfBlockSpace,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfPositionsOutsideOfBlockSpace,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfPlacedProductsRecommendedInAssortment,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
               SectionType.Planograms,
               FieldNames.PlanogramMetaCountOfRecommendedProductsInAssortment,
               GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramPlanogramType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                (Byte)0);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedInventory,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNoOfInformationEvents,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNoOfDebugEvents,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            #endregion

            #region PlanogramProduct

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaNotAchievedCases,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaNotAchievedDOS,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsOverShelfLifePercent,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaNotAchievedDeliveries,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalMainFacings,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalXFacings,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramProducts,
               FieldNames.PlanogramProductMetaTotalYFacings,
               GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalZFacings,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsRangedInAssortment,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            #endregion

            #region PlanogramPosition

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaIsOutsideOfBlockSpace,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaHeight,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramPositions,
               FieldNames.PlanogramPositionMetaWidth,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramPositions,
               FieldNames.PlanogramPositionMetaDepth,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramPositions,
               FieldNames.PlanogramPositionMetaWorldX,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramPositions,
               FieldNames.PlanogramPositionMetaWorldY,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramPositions,
               FieldNames.PlanogramPositionMetaWorldZ,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaTotalLinearSpace,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaTotalAreaSpace,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaTotalVolumetricSpace,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaPlanogramLinearSpacePercentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaPlanogramAreaSpacePercentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaPlanogramVolumetricSpacePercentage,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            #endregion

            #region PlanogramFixtureComponent


            file.AddItemPropertyIfMissing(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldX,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldY,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldZ,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldAngle,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldSlope,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldRoll,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            #endregion

            #region PlanogramAssemblyComponent

            file.AddItemPropertyIfMissing(
              SectionType.PlanogramAssemblyComponents,
              FieldNames.PlanogramAssemblyComponentMetaWorldX,
              GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldY,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldZ,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldAngle,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldSlope,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldRoll,
               GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            #endregion

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Add Item Properties

            #region PlanogramAssemblyComponent
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentMetaComponentCount,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);
            #endregion
            #endregion

            #region Remove Item Properties

            #region Planogram

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductNotAchievedCases);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductNotAchievedDOS);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductOverShelfLifePercent);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductNotAchievedDeliveries);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedCases);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedDOS);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductOverShelfLifePercent);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedDeliveries);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfPositionsOutsideOfBlockSpace);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfPositionsOutsideOfBlockSpace);

            file.RemoveItemPropertyIfPresent(
               SectionType.Planograms,
               FieldNames.PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfPlacedProductsRecommendedInAssortment);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfRecommendedProductsInAssortment);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramPlanogramType);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaPercentOfProductNotAchievedInventory);

            #endregion

            #region PlanogramProduct

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaNotAchievedCases);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaNotAchievedDOS);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsOverShelfLifePercent);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaNotAchievedDeliveries);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalMainFacings);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalXFacings);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramProducts,
               FieldNames.PlanogramProductMetaTotalYFacings);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalZFacings);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsRangedInAssortment);

            #endregion

            #region PlanogramPosition

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaIsOutsideOfBlockSpace);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaHeight);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaWidth);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaDepth);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaWorldX);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaWorldY);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaWorldZ);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaTotalLinearSpace);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaTotalAreaSpace);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaTotalVolumetricSpace);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaPlanogramLinearSpacePercentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaPlanogramAreaSpacePercentage);

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramPositions,
              FieldNames.PlanogramPositionMetaPlanogramVolumetricSpacePercentage);

            #endregion

            #region PlanogramFixtureComponent

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldX);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldY);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldZ);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldAngle);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldSlope);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramFixtureComponents,
               FieldNames.PlanogramFixtureComponentMetaWorldRoll);

            #endregion

            #region PlanogramAssemblyComponent

            file.RemoveItemPropertyIfPresent(
              SectionType.PlanogramAssemblyComponents,
              FieldNames.PlanogramAssemblyComponentMetaWorldX);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldY);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldZ);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldAngle);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldSlope);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramAssemblyComponents,
               FieldNames.PlanogramAssemblyComponentMetaWorldRoll);

            #endregion

            #endregion
        }

        #endregion
    }
}
