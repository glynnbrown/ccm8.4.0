﻿// Copyright © Galleria RTS Ltd 2016


using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0009_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Item properties

            #region PlanogramSubComponent

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSubComponents,
                FieldNames.PlanogramSubComponentDividerObstructionFillColour,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                -65536);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramSubComponents,
               FieldNames.PlanogramSubComponentDividerObstructionFillPattern,
               GalleriaBinaryFile.ItemPropertyType.Byte,
               (Byte)0);

            #endregion

            #region Planogram Position

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaPegRowNumber,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramPositions,
               FieldNames.PlanogramPositionMetaPegColumnNumber,
               GalleriaBinaryFile.ItemPropertyType.NullableInt32,
               null);
            #endregion

            #endregion
        }
        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Item Properties

            #region PlanogramSubComponent

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSubComponents,
                FieldNames.PlanogramSubComponentDividerObstructionFillColour);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramSubComponents,
               FieldNames.PlanogramSubComponentDividerObstructionFillPattern);

            #endregion

            #region PlanogramPosition

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaPegRowNumber);

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramPositions,
               FieldNames.PlanogramPositionMetaPegColumnNumber);

            #endregion

            #endregion

        }
        #endregion
    }
}
