﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added NotchNumber property
// V8-29506 : M.Shelley
//  Add new metadata properties to PlanogramProduct for reporting:
//      MetaPositionCount, MetaTotalFacings, MetaTotalLinearSpace, MetaTotalAreaSpace, MetaTotalVolumetricSpace
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
// V8-28491 : L.Luong
//  Added MetaIsInMetaData in PlanogramProduct 
// V8-29590 : A.Probyn
//	Extended for new PlanogramEventLog_Score property
//	Extended for new Added Planogram_MetaNoOfErrors, Planogram_MetaNoOfWarnings, Planogram_MetaTotalErrorScore, Planogram_MetaHighestErrorScore
// V8-29596 : L.Luong
//  Added Criteria to PlanogramValidationTemplateGroupMetric
// V8-28242 : M.Shelley
//  Added Status, DateWIP, DateApproved, DateArchived 
// V8-29682 : A.Probyn
//  Added PerformanceSourceMetric_AggregationType
#endregion
#region Version History: CCM810
//V8-29908 : M.Pettit
//  Added default values for non-nullable properties PlanogramStatus, PlanogramEventLogScore
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0004_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Remove Item Properties
            #endregion

            #region Remove Sections
            #endregion

            #region Add Sections
            #endregion

            #region Add Item Properties

            #region Planogram

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNoOfErrors,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNoOfWarnings,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaTotalErrorScore,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaHighestErrorScore,
                GalleriaBinaryFile.ItemPropertyType.NullableByte);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramStatus,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                (Byte)0);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramDateWip,
                GalleriaBinaryFile.ItemPropertyType.NullableDateTime);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramDateApproved,
                GalleriaBinaryFile.ItemPropertyType.NullableDateTime);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramDateArchived,
                GalleriaBinaryFile.ItemPropertyType.NullableDateTime);

            #endregion

            #region PlanogramFixtureComponent

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramFixtureComponents,
                FieldNames.PlanogramFixtureComponentNotchNumber,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            #endregion

            #region PlanogramAssemblyComponent

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentNotchNumber,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            #endregion

            #region PlanogramProduct

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPositionCount,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalFacings,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalLinearSpace,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalAreaSpace,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalVolumetricSpace,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsInMasterData,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            #endregion

            #region PlanogramPerformanceMetric

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPerformanceMetrics,
                FieldNames.PlanogramPerformanceMetricAggregationType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                (Byte)0);

            #endregion

            #region PlanogramEventLog

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramEventLogs,
                FieldNames.PlanogramEventLogScore,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                (Byte)0);

            #endregion

            #region PlanogramValidationTemplateGroupMetric

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramValidationTemplateGroupMetrics,
                FieldNames.PlanogramValidationTemplateGroupMetricCriteria,
                GalleriaBinaryFile.ItemPropertyType.String);

            #endregion

            #endregion

            #region Change Properties Type

            #region Planogram

            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });

            #endregion

            #region PlanogramFixtureItem

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });

            #endregion

            #region PlanogramFixtureComponent

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });

            #endregion

            #region PlanogramFixtureAssembly

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });

            #endregion

            #region PlanogramAssemblyComponent

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableSingle, (value) => { return Convert.ToSingle(value); });

            #endregion

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Remove Item Properties

            #region Planogram

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramStatus);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramDateWip);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramDateApproved);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramDateArchived);

            #endregion

            #region PlanogramFixtureComponent

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramFixtureComponents,
                FieldNames.PlanogramFixtureComponentNotchNumber);

            #endregion

            #region PlanogramAssemblyComponent

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentNotchNumber);

            #endregion

            #region PlanogramProduct

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaPositionCount);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalFacings);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalLinearSpace);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalAreaSpace);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaTotalVolumetricSpace);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsInMasterData);

            #endregion

            #region PlanogramValidationTemplateGroupMetric

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramValidationTemplateGroupMetrics,
                FieldNames.PlanogramValidationTemplateGroupMetricCriteria);

            #endregion

            #endregion

            #region Add Item Properties
            #endregion

            #region Change Properties Type

            #region Planogram

            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.Planograms, FieldNames.PlanogramMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });

            #endregion

            #region PlanogramFixtureItem

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });

            #endregion

            #region PlanogramFixtureComponent

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });

            #endregion

            #region PlanogramFixtureAssembly

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });

            #endregion

            #region PlanogramAssemblyComponent

            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMinDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMaxDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageDos, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMinCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMaxCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });
            file.SetItemPropertyTypeIfPresent(SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageCases, GalleriaBinaryFile.ItemPropertyType.NullableInt16, (value) => { return Convert.ToInt16(value); });

            #endregion

            #endregion
        }

        #endregion
    }
}
