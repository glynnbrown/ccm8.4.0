﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.3.0
// V8-31550 : A.Probyn
//  Created
// V8-31531 : A.Heathcote 
//  Added the upgrade and downgrade scripts for IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31819 : A.Silva
//  Added PlanogramComparison, PlanogramComparisonField, PlanogramComparisonResult, PlanogramComparisonItem,
//      PlanogramComparisonFieldValue to upgrade and downgrade scritps.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts to PlanogramComparison.
// V8-31947 : A.Silva
//  Added MetaComparisonStatus to PlanogramProduct and PlanogramPosition.
// V8-31963 : A.Silva
//  Added Display field to PlanogramComparisonField.
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
// V8-32396 : A.Probyn
//  Added PlanogramProductMetaIsDelistFamilyRuleBroken & PlanogramMetaNumberOfDelistFamilyRulesBroken
// V8-32521 : J.Pickup
//	Added Planogram_MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added PlanogramProduct_MetaIsBuddied
//  Added Planogram_MetaCountOfProductsBuddied	
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName.
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
#endregion

#endregion

using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0008_0000 : IScript
    {
        #region Upgrade

        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Sections

            // Note: Order is important here.  The sections will be written to the file in the order in which they are
            // added.  As a general rule of thumb, therefore, the sections should be added in descending order by
            // anticipated size.  This is because if a section has to be expanded to accomodate new data, all sections
            // following it will have to be moved down, and moving significant number of bytes around in a file takes
            // time.  In one test involving around 5000 images (a reasonable number of images for a Maxima planogram)
            // of 32 KB each (no idea what size the images will be in real life) moving the images section up from
            // last in the file reduced saving time for a small change (adding a new fixture item) from around 2.5 
            // seconds to 267 milliseconds.

            file.AddSectionIfMissing(SectionType.PlanogramAssortmentLocationBuddys);
            file.AddSectionIfMissing(SectionType.PlanogramAssortmentProductBuddys);
            file.AddSectionIfMissing(SectionType.PlanogramAssortmentInventoryRules);
            file.AddSectionIfMissing(SectionType.PlanogramComparisons);
            file.AddSectionIfMissing(SectionType.PlanogramComparisonFields);
            file.AddSectionIfMissing(SectionType.PlanogramComparisonResults);
            file.AddSectionIfMissing(SectionType.PlanogramComparisonItems);
            file.AddSectionIfMissing(SectionType.PlanogramComparisonFieldValues);
            file.AddSectionIfMissing(SectionType.PlanogramSequenceGroupSubGroups);

            #endregion

            #region Add Item Properties

            #region Planogram
            
            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaNumberOfDelistFamilyRulesBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaHasComponentsOutsideOfFixtureArea,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductsBuddied,
                GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                null);
            #endregion

            #region PlanogramAssortmentLocationBuddy

            // PlanogramAssortmentLocationBuddyId 
            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssortmentLocationBuddys,
               FieldNames.PlanogramAssortmentLocationBuddyId,
               GalleriaBinaryFile.ItemPropertyType.Int32,
               null);

            //PlanogramAssortmentLocationBuddyLocationCode
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyLocationCode,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentLocationBuddyTreatmentType
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyTreatmentType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                null);
            
            //PlanogramAssortmentLocationBuddyS1LocationCode
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS1LocationCode,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentLocationBuddyS1Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS1Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentLocationBuddyS2LocationCode
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS2LocationCode,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentLocationBuddyS2Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS2Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //	PlanogramAssortmentLocationBuddyS3LocationCode
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS3LocationCode,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentLocationBuddyS3Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS3Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentLocationBuddyS4LocationCode
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS4LocationCode,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentLocationBuddyS4Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS4Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentLocationBuddyS5LocationCode
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS5LocationCode,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentLocationBuddyS5Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyS5Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentLocationBuddys,
                FieldNames.PlanogramAssortmentLocationBuddyPlanogramAssortmentId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                null);

            #endregion

            #region PlanogramAssortmentProductBuddy

            // PlanogramAssortmentProductBuddyId 
            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssortmentProductBuddys,
               FieldNames.PlanogramAssortmentProductBuddyId,
               GalleriaBinaryFile.ItemPropertyType.Int32,
               null);

            //PlanogramAssortmentProductBuddyProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentProductBuddyTreatmentType
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyTreatmentType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                null);

            //PlanogramAssortmentProductBuddyTreatmentTypePercentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyTreatmentTypePercentage,
                GalleriaBinaryFile.ItemPropertyType.Single,
                null);

            //PlanogramAssortmentProductBuddySourceType
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddySourceType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                null);

            //PlanogramAssortmentProductBuddyProductAttributeType
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyProductAttributeType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                null);

            //PlanogramAssortmentProductBuddyS1ProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS1ProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentProductBuddyS1Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS1Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentProductBuddyS2ProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS2ProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentProductBuddyS2Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS2Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //	PlanogramAssortmentProductBuddyS3ProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS3ProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentProductBuddyS3Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS3Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentProductBuddyS4ProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS4ProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentProductBuddyS4Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS4Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentProductBuddyS5ProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS5ProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentProductBuddyS5Percentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyS5Percentage,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            //PlanogramAssortmentId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentProductBuddys,
                FieldNames.PlanogramAssortmentProductBuddyPlanogramAssortmentId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                null);

            #endregion

            #region PlanogramAssortmentInventoryRule

            // PlanogramAssortmentInventoryRuleId 
            file.AddItemPropertyIfMissing(
               SectionType.PlanogramAssortmentInventoryRules,
               FieldNames.PlanogramAssortmentInventoryRuleId,
               GalleriaBinaryFile.ItemPropertyType.Int32,
               null);

            //PlanogramAssortmentInventoryRuleProductGtin
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleProductGtin,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            //PlanogramAssortmentInventoryRuleCasePack
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleCasePack,
                GalleriaBinaryFile.ItemPropertyType.Single,
                0);

            //PlanogramAssortmentInventoryRuleDaysOfSupply
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleDaysOfSupply,
                GalleriaBinaryFile.ItemPropertyType.Single,
                0);

            //PlanogramAssortmentInventoryRuleShelfLife
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleShelfLife,
                GalleriaBinaryFile.ItemPropertyType.Single,
                0);

            //PlanogramAssortmentInventoryRuleReplenishmentDays
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleReplenishmentDays,
                GalleriaBinaryFile.ItemPropertyType.Single,
                0);

            //PlanogramAssortmentInventoryRuleWasteHurdleUnits
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleUnits,
                GalleriaBinaryFile.ItemPropertyType.Single,
                0);

            //PlanogramAssortmentInventoryRuleWasteHurdleCasePack
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleCasePack,
                GalleriaBinaryFile.ItemPropertyType.Single,
                null);

            //PlanogramAssortmentInventoryRuleMinUnits
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleMinUnits,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                0);

            //PlanogramAssortmentInventoryRuleMinFacings
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRuleMinFacings,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                0);

            //PlanogramAssortmentId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssortmentInventoryRules,
                FieldNames.PlanogramAssortmentInventoryRulePlanogramAssortmentId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                null);

            #endregion

            #region PlanogramProduct

            #region Remove IsTrayProduct

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramProducts,
               FieldNames.PlanogramProductIsTrayProduct);
            #endregion

            #region PegProngOffset / PegProngOffsetX / PegProngOffsetY

            //  V8-31564 - Support Peg Prong Offset
            //
            // Previous versions of the POG file used the PegProngOffset property
            // but this has now been renamed to PegProngOffsetX and the PegProngOffsetY
            // property also added

            // ToDo: Handle upgrade

            // map PlanogramProductPegProngOffset values to the new PlanogramProductPegProngOffsetX property

            // PlanogramProductPegProngOffsetX
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductPegProngOffsetX,
                GalleriaBinaryFile.ItemPropertyType.Single,
                0F);

            // PlanogramProductPegProngOffsetY
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductPegProngOffsetY,
                GalleriaBinaryFile.ItemPropertyType.Single,
                0F);

            Dictionary<Object, Galleria.Framework.IO.GalleriaBinaryFile.IEditableItem> products;
            if (file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegProngOffset))
            {
                products = file.GetItems((UInt16)SectionType.PlanogramProducts)
                    .ToDictionary<Galleria.Framework.IO.GalleriaBinaryFile.IEditableItem, Object>(
                           item => item.GetItemPropertyValue(FieldNames.PlanogramProductId));

                var positions = file.GetItems((UInt16)SectionType.PlanogramPositions);
                foreach (var position in positions)
                {
                    var product = products[position.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramProductId)];

                    if (product != null)
                    {
                        var oldOffsetValue = product.GetItemPropertyValue(FieldNames.PlanogramProductPegProngOffset);
                        product.SetItemPropertyValue(FieldNames.PlanogramProductPegProngOffsetX, oldOffsetValue);
                    }
                }
            }

            // Debug code to check if the PegProngOffsetX has been copied over
            //if (file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegProngOffsetX))
            //{
            //    var products2 = file.GetItems((UInt16)SectionType.PlanogramProducts).ToDictionary<Galleria.Framework.IO.GalleriaBinaryFile.IEditableItem, Object>(
            //                                                                            item => item.GetItemPropertyValue(FieldNames.PlanogramProductId));

            //    var positions = file.GetItems((UInt16)SectionType.PlanogramPositions);
            //    foreach (var position in positions)
            //    {
            //        var product = products2[position.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramProductId)];

            //        if (product != null)
            //        {
            //            var newOffsetValue = product.GetItemPropertyValue(FieldNames.PlanogramProductPegProngOffsetX);
            //        }
            //    }
            //}

            // And remove the old PlanogramProductPegProngOffset field
            //file.RemoveItemPropertyIfPresent(SectionType.PlanogramProducts, FieldNames.PlanogramProductPegProngOffset);

            #endregion

            #region MetaComparisonStatus

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaComparisonStatus,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            #endregion

            #endregion

            #region PlanogramPosition

            #region MetaComparisonStatus

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaComparisonStatus,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            #endregion

            #region MetaSequenceSubGroupName

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaSequenceSubGroupName,
                GalleriaBinaryFile.ItemPropertyType.String);

            #endregion

            #region MetaSequenceGroupName

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaSequenceGroupName,
                GalleriaBinaryFile.ItemPropertyType.String);

            #endregion

            #endregion

            #region PlanogramComparisons

            //  PlanogramComparisonId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonPlanogramId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonPlanogramId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonName
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonName,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  PlanogramComparisonDescription
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonDescription,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  PlanogramComparisonDateLastCompared
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonDateLastCompared,
                GalleriaBinaryFile.ItemPropertyType.NullableDateTime);

            //  PlanogramComparisonIgnoreNonPlacedProducts
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonIgnoreNonPlacedProducts,
                GalleriaBinaryFile.ItemPropertyType.Boolean);

            //  PlanogramComparisonDataOrderType
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonDataOrderType,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            #endregion

            #region PlanogramComparisonFields

            //  PlanogramComparisonId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonFieldPlanogramComparisonId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldPlanogramComparisonId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonFieldItemType
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldItemType,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            //  PlanogramComparisonFieldDisplayName
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldDisplayName,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  PlanogramComparisonFieldNumber
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldNumber,
                GalleriaBinaryFile.ItemPropertyType.Int16);

            //  PlanogramComparisonFieldFieldPlaceholder
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldFieldPlaceholder,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  PlanogramComparisonFieldDisplay
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldDisplay,
                GalleriaBinaryFile.ItemPropertyType.Boolean);

            #endregion

            #region PlanogramComparisonResults

            //  PlanogramComparisonResultsId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonResultPlanogramComparisonId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultPlanogramComparisonId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonResultPlanogramName
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultPlanogramName,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  PlanogramComparisonResultStatus
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultStatus,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            #endregion

            #region PlanogramComparisonItems

            //  PlanogramComparisonResultsId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonItemPlanogramComparisonResultId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemPlanogramComparisonResultId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonItemItemId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemItemId,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  PlanogramComparisonItemItemType
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemItemType,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            //  PlanogramComparisonItemStatus
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemStatus,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            #endregion

            #region PlanogramComparisonFieldValues

            //  PlanogramComparisonFieldValueId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValueId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonFieldValuePlanogramComparisonItemId
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValuePlanogramComparisonItemId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            //  PlanogramComparisonFieldValueFieldPlaceholder
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValueFieldPlaceholder,
                GalleriaBinaryFile.ItemPropertyType.String);

            //  PlanogramComparisonFieldValueValue
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValueValue,
                GalleriaBinaryFile.ItemPropertyType.String);

            #endregion

            #region PlanogramProduct

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMaxNestingDeep);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMaxNestingHigh);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsDelistFamilyRuleBroken,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsBuddied,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                null);

            #endregion

            #region PlanogramSequenceGroupProduct

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupProducts,
                FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32);

            #endregion

            #region PlanogramSequenceGroupSubGroup

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId,
                GalleriaBinaryFile.ItemPropertyType.Int32);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupName,
                GalleriaBinaryFile.ItemPropertyType.String);

            file.AddItemPropertyIfMissing(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupAlignment,
                GalleriaBinaryFile.ItemPropertyType.Byte);

            #endregion

            #region PlanogramFixtureComponent

            //  PlanogramFixtureComponentMetaIsOutsideOfFixtureArea
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramFixtureComponents,
                FieldNames.PlanogramFixtureComponentMetaIsOutsideOfFixtureArea,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            #endregion

            #region PlanogramAssemblyComponent

            //  PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea,
                GalleriaBinaryFile.ItemPropertyType.NullableBoolean);

            #endregion

            #region PlanogramBlockingGroup

            #region Remove CanOptimise

            file.RemoveItemPropertyIfPresent(
               SectionType.PlanogramBlockingGroups,
               FieldNames.PlanogramBlockingGroupCanOptimise);
            
            #endregion

            //  PlanogramBlockingGroupIsLimited
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupIsLimited,
                GalleriaBinaryFile.ItemPropertyType.Boolean);

            //  PlanogramBlockingGroupLimitedPercentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupLimitedPercentage,
                GalleriaBinaryFile.ItemPropertyType.Single);

            #endregion

            #region PlanogramBlockingDivider
            
            //  PlanogramBlockingDividerIsLimited
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingDividers,
                FieldNames.PlanogramBlockingDividerIsLimited,
                GalleriaBinaryFile.ItemPropertyType.Boolean);

            //  PlanogramBlockingDividerLimitedPercentage
            file.AddItemPropertyIfMissing(
                SectionType.PlanogramBlockingDividers,
                FieldNames.PlanogramBlockingDividerLimitedPercentage,
                GalleriaBinaryFile.ItemPropertyType.Single);

            #endregion

            #endregion
        }

        #endregion

        #region Downgrade

        public void Downgrade(GalleriaBinaryFile file)
        {
            #region Planogram

            //  Planogram Meta Has Components Outside Of Fixture Area
            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaHasComponentsOutsideOfFixtureArea);

            file.RemoveItemPropertyIfPresent(
                SectionType.Planograms,
                FieldNames.PlanogramMetaCountOfProductsBuddied);

            #endregion
            
            #region PlanogramProduct

            #region Add IsTrayProduct

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramProducts,
               FieldNames.PlanogramProductIsTrayProduct,
               GalleriaBinaryFile.ItemPropertyType.Boolean);

            #endregion

            //  V8-31564 - Support Peg Prong Offset
            //
            // Previous versions of the POG file used the PegProngOffset property
            // but this has now been renamed to PegProngOffsetX and the PegProngOffsetY
            // property also added

            // ToDo: Handle downgrade

            // First add back the PlanogramProductPegProngOffset property
            file.AddItemPropertyIfMissing(
               SectionType.PlanogramProducts,
               FieldNames.PlanogramProductPegProngOffset,
               GalleriaBinaryFile.ItemPropertyType.Single);

            // Map values back from PlanogramProductPegProngOffsetX to the PlanogramProductPegProngOffset field

            // PlanogramProductPegProngOffsetX
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductPegProngOffsetX);

            // PlanogramProductPegProngOffsetY
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductPegProngOffsetY);

            // PlanogramProductMetaIsBuddied
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaIsBuddied);

            #region MetaComparisonStatus

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramProducts,
                FieldNames.PlanogramProductMetaComparisonStatus);

            #endregion

            #endregion

            #region PlanogramPosition

            #region MetaComparisonStatus

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaComparisonStatus);

            #endregion

            #region MetaSequenceSubGroupName

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaSequenceSubGroupName);

            #endregion

            #region MetaSequenceGroupName

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramPositions,
                FieldNames.PlanogramPositionMetaSequenceGroupName);

            #endregion

            #endregion

            #region PlanogramComparisonFieldValues

            //  PlanogramComparisonFieldValueId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValueId);

            //  PlanogramComparisonFieldValuePlanogramComparisonItemId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValuePlanogramComparisonItemId);

            //  PlanogramComparisonFieldValueFieldPlaceholder
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValueFieldPlaceholder);

            //  PlanogramComparisonFieldValueValue
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFieldValues,
                FieldNames.PlanogramComparisonFieldValueValue);

            file.RemoveSection((UInt16)SectionType.PlanogramComparisonFieldValues);

            #endregion

            #region PlanogramComparisonItems

            //  PlanogramComparisonResultsId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemId);

            //  PlanogramComparisonItemPlanogramComparisonResultId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemPlanogramComparisonResultId);

            //  PlanogramComparisonItemItemId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemItemId);

            //  PlanogramComparisonItemItemType
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemItemType);

            //  PlanogramComparisonItemStatus
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonItems,
                FieldNames.PlanogramComparisonItemStatus);

            file.RemoveSection((UInt16)SectionType.PlanogramComparisonItems);

            #endregion

            #region PlanogramComparisonResults

            //  PlanogramComparisonResultsId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultId);

            //  PlanogramComparisonResultPlanogramComparisonId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultPlanogramComparisonId);

            //  PlanogramComparisonResultPlanogramName
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultPlanogramName);

            //  PlanogramComparisonResultStatus
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonResults,
                FieldNames.PlanogramComparisonResultStatus);

            file.RemoveSection((UInt16)SectionType.PlanogramComparisonResults);

            #endregion

            #region PlanogramComparisonFields

            //  PlanogramComparisonId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldId);

            //  PlanogramComparisonFieldPlanogramComparisonId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldPlanogramComparisonId);

            //  PlanogramComparisonFieldItemType
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldItemType);

            //  PlanogramComparisonFieldDisplayName
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldDisplayName);

            //  PlanogramComparisonFieldNumber
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldNumber);

            //  PlanogramComparisonFieldFieldPlaceholder
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldFieldPlaceholder);

            //  PlanogramComparisonFieldDisplay
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisonFields,
                FieldNames.PlanogramComparisonFieldDisplay);

            file.RemoveSection((UInt16)SectionType.PlanogramComparisonFields);

            #endregion

            #region PlanogramComparisons

            //  PlanogramComparisonId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonId);

            //  PlanogramComparisonPlanogramId
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonPlanogramId);

            //  PlanogramComparisonName
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonName);

            //  PlanogramComparisonDescription
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonDescription);

            //  PlanogramComparisonDateLastCompared
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonDateLastCompared);

            //  PlanogramComparisonIgnoreNonPlacedProducts
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonIgnoreNonPlacedProducts);

            //  PlanogramComparisonDataOrderType
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramComparisons,
                FieldNames.PlanogramComparisonDataOrderType);

            file.RemoveSection((UInt16)SectionType.PlanogramComparisons);

            #endregion

            #region PlanogramProducts

            file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxNestingDeep,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);

            file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxNestingHigh,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);

            #endregion

            #region PlanogramSequenceGroupProduct

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSequenceGroupProducts,
                FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId);

            #endregion

            #region PlanogramSequenceGroupSubGroup

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupId);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupName);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramSequenceGroupSubGroups,
                FieldNames.PlanogramSequenceGroupSubGroupAlignment);

            #endregion

            #region PlanogramFixtureComponent

            //  PlanogramFixtureComponentMetaIsOutsideOfFixtureArea
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramFixtureComponents,
                FieldNames.PlanogramFixtureComponentMetaIsOutsideOfFixtureArea);

            #endregion

            #region PlanogramAssemblyComponent

            //  PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramAssemblyComponents,
                FieldNames.PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea);

            #endregion

            #region PlanogramBlockingGroup

            #region Add CanOptimise

            file.AddItemPropertyIfMissing(
               SectionType.PlanogramBlockingGroups,
               FieldNames.PlanogramBlockingGroupCanOptimise,
               GalleriaBinaryFile.ItemPropertyType.Boolean);

            #endregion

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupIsLimited);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingGroups,
                FieldNames.PlanogramBlockingGroupLimitedPercentage);

            #endregion

            #region PlanogramBlockingDivider
            
            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingDividers,
                FieldNames.PlanogramBlockingDividerIsLimited);

            file.RemoveItemPropertyIfPresent(
                SectionType.PlanogramBlockingDividers,
                FieldNames.PlanogramBlockingDividerLimitedPercentage);

            #endregion
        }

        #endregion
    }
}