﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup/A.Kuszyk
//      Initial version.
// V8-24971 : L.Hodson
//  Changes to PlangoramPosition structure
// V8-24972 : L.Hodson
//  Added product image fields for display, tray,  pointofpurchase, alternate and case
// V8-25916 : N.Foster
//  Added Package RowVersion, DateDeleted. Removed Package CreatedBy, LastModifiedBy, Description
// V8-25949 : N.Foster
//  Changed package id to a string, added a check to upgrade file so old plans are not broken
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties to Planogram, PlanogramFixtureItem, PlanogramAssemblyComponent, PlanogramFixtureAssembly, PlanogramFixtureComponent 
// V8-25787 : N.Foster
//  Added PlanogramComponent.IsCarPark
// V8-25881 : A.Probyn
//  Replaced PackagePlanogramCount with PackageMetaPlanogramCount
// V8-26041 : A.Kuszyk
//  Added additional Product fields to PlanogramProduct.
// V8-26269 : A.Kuszyk
//  Added PlanogramPerformance, PlanogramPerformanceData and PlanogramPerformanceMetric.
// V8-26426 : A.Kuszyk
//  Added PlanogramAssortment related sections.
// V8-26091 : L.Ineson
//  Added additional FaceThickness properties
//  Removed FaceTopOffset and CanMultiMerchX,Y,Z
// V8-26573 : L.Luong
//  Added PlanogramValidationTemplate and relative properties
// V8-26709 : L.Ineson
//  Removed old planogram unit of measure property.
// V8-26706 : L.Luong
//  Added PlanogramConsumerDecisionTree and relative properties
// V8-26836 : L.Luong
//  Added PlanogramEventLog properties
// V8-26799 : A.Kuszyk
//  Removed PlanogramAssortmentProduct LocationCode property.
// V8-26954 : A.Silva 
//  Added ResultType property to PlanogramValidationTemplateGroup and Metric.
// V8-26891 : L.Ineson
//  Added Blocking
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
// V8-26812 : A.Silva
//  Added ValidationType property to PlanogramValidationTemplateGroup and Metric.    
// V8-27058 : A.Probyn
//  Updated MetaData properties for Planogram related structure.
// CCM-27222 : L.Ineson
//      Added PlanogramBlockingGroupField.OperandType
// V8-27237 : A.Silva   ~ Added Package.DateValidationDataCalculated property.
// V8-27154 : L.Luong
//  Added Planogram ProductPlacement X,Y,Z
// V8-27058 : A.Probyn
//  Added PlanogramMetaDataImages section and fields
// V8-27404 : L.Luong
//   Changed LevelType to EntryType and added Content to PlangramEventLog
// V8-27153 : A.Silva
//      Added PlanogramRenumberingStrategy and related properties.
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber to PlanogramFixtureComponent.
//      Added ComponentSequenceNumber to PlanogramAssemblyComponent.
//      Added PositionSequenceNumber to PlanogramPosition.
// V8-27476 : L.Luong
//  Added BlockPlacementXType and BlockPlacementYType to PlanogramBlocking
// V8-27558 : L.Ineson
//  Removed IsBay
// V8-27605 : A.Silva
//      Added missing metadata fields to PlanogramAssemblyComponent.
// V8-27606 : L.Ineson
//  Added FinancialGroupCode and FinancialGroupName
// V8-27647 : L.Luong
//  Added PlanogramInventory
// V8-27759 : A.Kuszyk
//  Added AffectedType and AffectedId to PlanogramEventLog.
// V8-26777 : L.Ineson
//  Removed Product CanMerch properties
// V8-27823 : L.Ineson
//  PlanogramPosition facings properties are now Int16
// V8-27411 : M.Pettit
//  Added UserName property
// V8-24779 : D.Pleasance
//  Added Planogram.SourcePath
// V8-28059 : A.Kuszyk
//  Added PlanogramEventLog WorkpackageSource, TaskSource and EventId.
// V8-27783 : M.Brumby
//	Added Planogram.UniqueContentReference
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Scripts
{
    public class Script0001_0000 : IScript
    {
        #region Upgrade
        /// <summary>
        /// Performs the upgrade of a planogram file
        /// </summary>
        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Sections

            // Note: Order is important here.  The sections will be written to the file in the order in which they are
            // added.  As a general rule of thumb, therefore, the sections should be added in descending order by
            // anticipated size.  This is because if a section has to be expanded to accomodate new data, all sections
            // following it will have to be moved down, and moving significant number of bytes around in a file takes
            // time.  In one test involving around 5000 images (a reasonable number of images for a Maxima planogram)
            // of 31 KB each (no idea what size the images will be in real life) moving the images section up from
            // last in the file reduced saving time for a small change (adding a new fixture item) from around 2.5 
            // seconds to 167 milliseconds.

            // The Schema Version section is very small, but we also don't anticipate it ever needing to change in 
            // size once written out the first time, so it might as well top the file.
            if (!file.SectionExists((UInt16)SectionType.SchemaVersion))
            {
                file.CreateSection((UInt16)SectionType.SchemaVersion);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramImages))
            {
                file.CreateSection((UInt16)SectionType.PlanogramImages, GalleriaBinaryFile.ItemPropertyType.Int32);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramMetadataImages))
            {
                file.CreateSection((UInt16)SectionType.PlanogramMetadataImages, GalleriaBinaryFile.ItemPropertyType.Int32);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramMetadataImages);
            if (!file.SectionExists((UInt16)SectionType.PlanogramEventLogs))
            {
                file.CreateSection((UInt16)SectionType.PlanogramEventLogs);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramEventLogs);
            if (!file.SectionExists((UInt16)SectionType.CustomAttributeData))
            {
                file.CreateSection((UInt16)SectionType.CustomAttributeData);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramProducts))
            {
                file.CreateSection((UInt16)SectionType.PlanogramProducts);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramPerformanceDatas))
            {
                file.CreateSection((UInt16)SectionType.PlanogramPerformanceDatas);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramPerformanceMetrics))
            {
                file.CreateSection((UInt16)SectionType.PlanogramPerformanceMetrics);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramPerformances))
            {
                file.CreateSection((UInt16)SectionType.PlanogramPerformances);
            }
            if (!file.SectionExists((UInt16) SectionType.PlanogramRenumberingStrategy))
            {
                file.CreateSection((UInt16)SectionType.PlanogramRenumberingStrategy);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics))
            {
                file.CreateSection((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics);
            if (!file.SectionExists((UInt16)SectionType.PlanogramValidationTemplateGroups))
            {
                file.CreateSection((UInt16)SectionType.PlanogramValidationTemplateGroups);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramValidationTemplateGroups);
            if (!file.SectionExists((UInt16)SectionType.PlanogramValidationTemplates))
            {
                file.CreateSection((UInt16)SectionType.PlanogramValidationTemplates);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramValidationTemplates);
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssortmentProducts))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssortmentProducts);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramAssortmentProducts);
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssortmentLocalProducts))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssortmentLocalProducts);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramAssortmentLocalProducts);
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssortmentRegionLocations))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssortmentRegionLocations);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramAssortmentRegionLocations);
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssortmentRegions))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssortmentRegions);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramAssortmentRegions);
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssortmentRegionProducts))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssortmentRegionProducts);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramAssortmentRegionProducts);
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssortments))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssortments);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramAssortments);
            if (!file.SectionExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts))
            {
                file.CreateSection((UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts);
            if (!file.SectionExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes))
            {
                file.CreateSection((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes);
            if (!file.SectionExists((UInt16)SectionType.PlanogramConsumerDecisionTreeLevels))
            {
                file.CreateSection((UInt16)SectionType.PlanogramConsumerDecisionTreeLevels);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramConsumerDecisionTreeLevels);
            if (!file.SectionExists((UInt16)SectionType.PlanogramConsumerDecisionTrees))
            {
                file.CreateSection((UInt16)SectionType.PlanogramConsumerDecisionTrees);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramConsumerDecisionTrees);
            if (!file.SectionExists((UInt16)SectionType.PlanogramPositions))
            {
                file.CreateSection((UInt16)SectionType.PlanogramPositions);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramSubComponents))
            {
                file.CreateSection((UInt16)SectionType.PlanogramSubComponents);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramComponents))
            {
                file.CreateSection((UInt16)SectionType.PlanogramComponents);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramFixtureAssemblies))
            {
                file.CreateSection((UInt16)SectionType.PlanogramFixtureAssemblies);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramFixtures))
            {
                file.CreateSection((UInt16)SectionType.PlanogramFixtures);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramFixtureItems))
            {
                file.CreateSection((UInt16)SectionType.PlanogramFixtureItems);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramBlockingLocations))
            {
                file.CreateSection((UInt16)SectionType.PlanogramBlockingLocations);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramBlockingLocations);
            if (!file.SectionExists((UInt16)SectionType.PlanogramBlockingDividers))
            {
                file.CreateSection((UInt16)SectionType.PlanogramBlockingDividers);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramBlockingDividers);
            if (!file.SectionExists((UInt16)SectionType.PlanogramBlockingGroupFields))
            {
                file.CreateSection((UInt16)SectionType.PlanogramBlockingGroupFields);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramBlockingGroupFields);
            if (!file.SectionExists((UInt16)SectionType.PlanogramBlockingGroups))
            {
                file.CreateSection((UInt16)SectionType.PlanogramBlockingGroups);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramBlockingGroups);
            if (!file.SectionExists((UInt16)SectionType.PlanogramBlocking))
            {
                file.CreateSection((UInt16)SectionType.PlanogramBlocking);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramBlocking);
            if (!file.SectionExists((UInt16)SectionType.PlanogramBlockingGroupProducts))
            {
                file.CreateSection((UInt16)SectionType.PlanogramBlockingGroupProducts);
            }
            file.EnableAes256Encryption((UInt16)SectionType.PlanogramBlockingGroupProducts);
            if (!file.SectionExists((UInt16)SectionType.Planograms))
            {
                file.CreateSection((UInt16)SectionType.Planograms);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramAnnotations))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAnnotations);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramFixtureComponents))
            {
                file.CreateSection((UInt16)SectionType.PlanogramFixtureComponents);
            }
            if (!file.SectionExists((UInt16)SectionType.Package))
            {
                file.CreateSection((UInt16)SectionType.Package);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssemblies))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssemblies);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramAssemblyComponents))
            {
                file.CreateSection((UInt16)SectionType.PlanogramAssemblyComponents);
            }
            if (!file.SectionExists((UInt16)SectionType.PlanogramInventory))
            {
                file.CreateSection((UInt16)SectionType.PlanogramInventory);
            }

            #endregion

            #region Add Item Properties

            #region Schema Version

            // MajorVersion
            if (!file.ItemPropertyExists((UInt16)SectionType.SchemaVersion, FieldNames.SchemaVersionMajorVersion))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.SchemaVersion,
                    FieldNames.SchemaVersionMajorVersion,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MinorVersion
            if (!file.ItemPropertyExists((UInt16)SectionType.SchemaVersion, FieldNames.SchemaVersionMinorVersion))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.SchemaVersion,
                    FieldNames.SchemaVersionMinorVersion,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            #endregion

            #region Package

            // Id
            if ((file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageId)) &&
                (file.GetItemPropertyType((UInt16)SectionType.Package, FieldNames.PackageId) != GalleriaBinaryFile.ItemPropertyType.String))
            {
                file.RemoveItemProperty((UInt16)SectionType.Package, FieldNames.PackageId);
            }
            if(!file.ItemPropertyExists((UInt16)SectionType.Package,FieldNames.PackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageId,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // RowVersion
            if (!file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageRowVersion))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageRowVersion,
                    GalleriaBinaryFile.ItemPropertyType.Int64,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // DateCreated
            if (!file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageDateCreated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageDateCreated,
                    GalleriaBinaryFile.ItemPropertyType.DateTime,
                    null);
            }

            // DateLastModified
            if (!file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageDateLastModified))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageDateLastModified,
                    GalleriaBinaryFile.ItemPropertyType.DateTime,
                    null);
            }

            // DateDeleted
            if (!file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageDateDeleted))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageDateDeleted,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            // MetaPlanogramCount
            if (!file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageMetaPlanogramCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageMetaPlanogramCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }


            // DateMetadataCalculated
            if (!file.ItemPropertyExists((UInt16)SectionType.Package, FieldNames.PackageDateMetadataCalculated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageDateMetadataCalculated,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            // DateValidationDataCalculated
            if (!file.ItemPropertyExists((UInt16) SectionType.Package, FieldNames.PackageDateValidationDataCalculated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Package,
                    FieldNames.PackageDateValidationDataCalculated,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            #endregion

            #region Planograms

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms, 
                    FieldNames.PlanogramId, 
                    GalleriaBinaryFile.ItemPropertyType.Int32, 
                    null);
            }

            // PackageId
            if ((file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramPackageId)) &&
                (file.GetItemPropertyType((UInt16)SectionType.Planograms, FieldNames.PlanogramPackageId) != GalleriaBinaryFile.ItemPropertyType.String))
            {
                file.RemoveItemProperty((UInt16)SectionType.Planograms, FieldNames.PlanogramPackageId);
            }
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramPackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramPackageId,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramUniqueContentReference))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramUniqueContentReference,
                    GalleriaBinaryFile.ItemPropertyType.String, //this is wrong but passes nunit, should be GUID but doesn't exist
                    Guid.Empty.ToString());
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // UserName
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramUserName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramUserName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // LengthUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramLengthUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramLengthUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // AreaUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramAreaUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramAreaUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // VolumeUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramVolumeUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramVolumeUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // WeightUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramWeightUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramWeightUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // AngleUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramAngleUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramAngleUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // CurrencyUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramCurrencyUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramCurrencyUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // ProductPlacementX
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramProductPlacementX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramProductPlacementX,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // ProductPlacementY
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramProductPlacementY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramProductPlacementY,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // ProductPlacementZ
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramProductPlacementZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramProductPlacementZ,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // Category Code
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramCategoryCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramCategoryCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Category Name
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramCategoryName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramCategoryName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Source Path
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramSourcePath))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramSourcePath,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //MetaBayCount
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaBayCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaBayCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaUniqueProductCount
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaUniqueProductCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaUniqueProductCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaComponentCount
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaComponentCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaComponentCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalMerchandisableLinearSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalMerchandisableLinearSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalMerchandisableLinearSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableAreaSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalMerchandisableAreaSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalMerchandisableAreaSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableVolumetricSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalMerchandisableVolumetricSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalMerchandisableVolumetricSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalLinearWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalLinearWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalLinearWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalAreaWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalAreaWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalAreaWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalVolumetricWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalVolumetricWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalVolumetricWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaProductsPlaced
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaProductsPlaced))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaProductsPlaced,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaProductsUnplaced
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaProductsUnplaced))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaProductsUnplaced,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaNewProducts
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaNewProducts))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaNewProducts,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangesFromPreviousCount
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaChangesFromPreviousCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaChangesFromPreviousCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangeFromPreviousStarRating
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaChangeFromPreviousStarRating))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaChangeFromPreviousStarRating,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaBlocksDropped
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaBlocksDropped))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaBlocksDropped,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaNotAchievedInventory
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaNotAchievedInventory))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaNotAchievedInventory,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaAverageFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaAverageFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaTotalUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaAverageUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaAverageUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaMinDos
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaMinDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaMinDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxDos
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaMaxDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaMaxDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageDos
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaAverageDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaAverageDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMinCases
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaMinCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaMinCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageCases
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaAverageCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaAverageCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxCases
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaMaxCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaMaxCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaSpaceToUnitsIndex
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaSpaceToUnitsIndex))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaSpaceToUnitsIndex,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaTotalComponentCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalComponentCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalComponentCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaTotalComponentsOverMerchandisedDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalComponentsOverMerchandisedDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalComponentsOverMerchandisedDepth,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaTotalComponentsOverMerchandisedHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalComponentsOverMerchandisedHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalComponentsOverMerchandisedHeight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaTotalComponentsOverMerchandisedWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalComponentsOverMerchandisedWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalComponentsOverMerchandisedWidth,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaTotalPositionCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalPositionCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalPositionCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaAverageFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaAverageFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.Planograms, FieldNames.PlanogramMetaTotalFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Planograms,
                    FieldNames.PlanogramMetaTotalFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            #endregion

            #region PlanogramFixtureItems

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramFixtureId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemPlanogramFixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemPlanogramFixtureId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // BaySequenceNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemBaySequenceNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemBaySequenceNumber,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }
            
            //MetaComponentCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaComponentCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaComponentCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalMerchandisableLinearSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableLinearSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalMerchandisableLinearSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableAreaSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableAreaSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalMerchandisableAreaSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableVolumetricSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalLinearWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalLinearWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalLinearWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalAreaWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalAreaWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalAreaWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalVolumetricWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalVolumetricWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalVolumetricWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaProductsPlaced
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaProductsPlaced))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaProductsPlaced,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaNewProducts
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaNewProducts))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaNewProducts,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangesFromPreviousCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaChangesFromPreviousCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaChangesFromPreviousCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangeFromPreviousStarRating
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaChangeFromPreviousStarRating))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaChangeFromPreviousStarRating,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaBlocksDropped
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaBlocksDropped))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaBlocksDropped,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaNotAchievedInventory
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaNotAchievedInventory))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaNotAchievedInventory,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaAverageFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaTotalUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaAverageUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaMinDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMinDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaMinDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMaxDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaMaxDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaAverageDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMinCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMinCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaMinCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaAverageCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaMaxCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaMaxCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaSpaceToUnitsIndex
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaSpaceToUnitsIndex))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaSpaceToUnitsIndex,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }
            
            //MetaTotalComponentCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalComponentCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalComponentCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalComponentsOverMerchandisedDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalComponentsOverMerchandisedHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalComponentsOverMerchandisedWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalPositionCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalPositionCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalPositionCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaAverageFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaAverageFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureItems, FieldNames.PlanogramFixtureItemMetaTotalFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    FieldNames.PlanogramFixtureItemMetaTotalFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            #endregion

            #region PlanogramFixtures

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixturePlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixturePlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NumberOfAssemblies
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureNumberOfAssemblies))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureNumberOfAssemblies,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // NumberOfMerchandisedSubComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureMetaNumberOfMerchandisedSubComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureMetaNumberOfMerchandisedSubComponents,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // TotalFixtureCost
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureMetaTotalFixtureCost))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureMetaTotalFixtureCost,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // UniqueProductCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtures, FieldNames.PlanogramFixtureUniqueProductCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtures,
                    FieldNames.PlanogramFixtureUniqueProductCount,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            #endregion

            #region PlanogramFixtureAssemblies

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramFixtureId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyPlanogramFixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyPlanogramFixtureId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramAssemblyId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyPlanogramAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyPlanogramAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblySlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblySlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }
            
            //MetaComponentCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaComponentCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaComponentCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalMerchandisableLinearSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableAreaSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableVolumetricSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalLinearWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalAreaWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalVolumetricWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaProductsPlaced
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaProductsPlaced))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaProductsPlaced,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaNewProducts
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaNewProducts))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaNewProducts,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangesFromPreviousCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaChangesFromPreviousCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaChangesFromPreviousCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangeFromPreviousStarRating
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaBlocksDropped
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaBlocksDropped))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaBlocksDropped,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaNotAchievedInventory
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaNotAchievedInventory))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaNotAchievedInventory,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaAverageFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaTotalUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaAverageUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaMinDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMinDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaMinDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMaxDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaMaxDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaAverageDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMinCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMinCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaMinCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaAverageCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaMaxCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaMaxCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaSpaceToUnitsIndex
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaSpaceToUnitsIndex))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaSpaceToUnitsIndex,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }
            
            //MetaAverageFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaAverageFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaAverageFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureAssemblies, FieldNames.PlanogramFixtureAssemblyMetaTotalFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    FieldNames.PlanogramFixtureAssemblyMetaTotalFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }


            #endregion

            #region PlanogramFixtureComponents

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramFixtureId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentPlanogramFixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentPlanogramFixtureId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentPlanogramComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentPlanogramComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ComponentSequenceNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentComponentSequenceNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentComponentSequenceNumber,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }
            
            //MetaTotalMerchandisableLinearSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableAreaSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableVolumetricSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalLinearWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalLinearWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalLinearWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalAreaWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalAreaWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalAreaWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalVolumetricWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaProductsPlaced
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaProductsPlaced))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaProductsPlaced,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaNewProducts
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaNewProducts))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaNewProducts,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangesFromPreviousCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaChangesFromPreviousCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaChangesFromPreviousCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangeFromPreviousStarRating
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaChangeFromPreviousStarRating))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaChangeFromPreviousStarRating,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaBlocksDropped
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaBlocksDropped))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaBlocksDropped,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaNotAchievedInventory
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaNotAchievedInventory))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaNotAchievedInventory,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaAverageFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaTotalUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaAverageUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaMinDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMinDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaMinDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMaxDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaMaxDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaAverageDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMinCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMinCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaMinCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaAverageCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaMaxCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaMaxCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaSpaceToUnitsIndex
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaSpaceToUnitsIndex))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaSpaceToUnitsIndex,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }
                        
            //MetaIsComponentSlopeWithNoRiser
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }
            
            //MetaIsOverMerchandisedDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedDepth,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }
            
            //MetaIsOverMerchandisedHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedHeight,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }
            
            //MetaIsOverMerchandisedWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedWidth,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }
            
            //MetaTotalComponentCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalComponentCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalComponentCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaTotalPositionCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalPositionCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalPositionCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaAverageFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaAverageFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramFixtureComponents, FieldNames.PlanogramFixtureComponentMetaTotalFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    FieldNames.PlanogramFixtureComponentMetaTotalFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }


            #endregion

            #region PlanogramAssemblies

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyName))            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NumberOfComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyNumberOfComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyNumberOfComponents,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // TotalComponentCost
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblies, FieldNames.PlanogramAssemblyTotalComponentCost))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblies,
                    FieldNames.PlanogramAssemblyTotalComponentCost,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region PlanogramAssemblyComponents

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramAssemblyId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentPlanogramAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentPlanogramAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentPlanogramComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentPlanogramComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ComponentSequenceNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentComponentSequenceNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentComponentSequenceNumber,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //MetaComponentCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaComponentCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaComponentCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalMerchandisableLinearSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableAreaSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalMerchandisableVolumetricSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalLinearWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalLinearWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalLinearWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalAreaWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalAreaWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalAreaWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalVolumetricWhiteSpace
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaProductsPlaced
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaProductsPlaced))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaProductsPlaced,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }
            
            //MetaNewProducts
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaNewProducts))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaNewProducts,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangesFromPreviousCount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaChangesFromPreviousCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaChangesFromPreviousCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaChangeFromPreviousStarRating
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaChangeFromPreviousStarRating))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaChangeFromPreviousStarRating,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaBlocksDropped
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaBlocksDropped))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaBlocksDropped,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaNotAchievedInventory
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaNotAchievedInventory))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaNotAchievedInventory,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaAverageFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaTotalUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaAverageUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaMinDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMinDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaMinDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMaxDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaMaxDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaAverageDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMinCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMinCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaMinCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaAverageCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaAverageCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaMaxCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaMaxCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaMaxCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaSpaceToUnitsIndex
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaSpaceToUnitsIndex))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaSpaceToUnitsIndex,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            //MetaIsComponentSlopeWithNoRiser
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }

            //MetaIsOverMerchandisedDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedDepth,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }

            //MetaIsOverMerchandisedHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedHeight,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }

            //MetaIsOverMerchandisedWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedWidth,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }

            //MetaTotalComponentCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalComponentCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalComponentCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaTotalPositionCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalPositionCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalPositionCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //MetaAverageFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaAverageFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaAverageFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //MetaTotalFrontFacings
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssemblyComponents, FieldNames.PlanogramAssemblyComponentMetaTotalFrontFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    FieldNames.PlanogramAssemblyComponentMetaTotalFrontFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            #endregion

            #region PlanogramRenumberingStrategy

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // BayComponentXRenumberStrategyPriority
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // BayComponentYRenumberStrategyPriority
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // BayComponentZRenumberStrategyPriority
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // PositionXRenumberStrategyPriority
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategyPriority))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategyPriority,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // PositionYRenumberStrategyPriority
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategyPriority))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategyPriority,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // PositionZRenumberStrategyPriority
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategyPriority))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategyPriority,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // BayComponentXRenumberStrategy
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategy,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // BayComponentYRenumberStrategy
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategy,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // BayComponentZRenumberStrategy
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategy,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // PositionXRenumberStrategy
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategy,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // PositionYRenumberStrategy
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategy,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // PositionZRenumberStrategy
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategy,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // RestartComponentRenumberingPerBay
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerBay))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerBay,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IgnoreNonMerchandisingComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // RestartPositionRenumberingPerComponent
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // UniqueNumberMultiPositionProductsPerComponent
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // ExceptAdjacentPositions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramRenumberingStrategy, FieldNames.PlanogramRenumberingStrategyExceptAdjacentPositions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramRenumberingStrategy,
                    FieldNames.PlanogramRenumberingStrategyExceptAdjacentPositions,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region PlanogramValidationTemplates

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplates, FieldNames.PlanogramValidationTemplateId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplates,
                    FieldNames.PlanogramValidationTemplateId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplates, FieldNames.PlanogramValidationTemplatePlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplates,
                    FieldNames.PlanogramValidationTemplatePlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplates, FieldNames.PlanogramValidationTemplateName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplates,
                    FieldNames.PlanogramValidationTemplateName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramValidationTemplateGroups

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroups, FieldNames.PlanogramValidationTemplateGroupId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    FieldNames.PlanogramValidationTemplateGroupId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroups, FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroups, FieldNames.PlanogramValidationTemplateGroupName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    FieldNames.PlanogramValidationTemplateGroupName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Threshold1
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroups, FieldNames.PlanogramValidationTemplateGroupThreshold1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    FieldNames.PlanogramValidationTemplateGroupThreshold1,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Threshold2
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroups, FieldNames.PlanogramValidationTemplateGroupThreshold2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    FieldNames.PlanogramValidationTemplateGroupThreshold2,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // ResultType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroups, FieldNames.PlanogramValidationTemplateGroupResultType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    FieldNames.PlanogramValidationTemplateGroupResultType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // ValidationType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroups, FieldNames.PlanogramValidationTemplateGroupValidationType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    FieldNames.PlanogramValidationTemplateGroupValidationType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            #endregion

            #region PlanogramValidationTemplateGroupMetrics

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Field
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricField))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricField,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Threshold1
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricThreshold1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricThreshold1,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Threshold2
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricThreshold2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricThreshold2,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Score1
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricScore1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricScore1,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Score2
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricScore2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricScore2,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Score3
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricScore3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricScore3,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // AggregationType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricAggregationType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricAggregationType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // ResultType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricResultType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricResultType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // ResultType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramValidationTemplateGroupMetrics, FieldNames.PlanogramValidationTemplateGroupMetricValidationType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    FieldNames.PlanogramValidationTemplateGroupMetricValidationType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            #endregion

            #region PlanogramAssortments
            
            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortments, FieldNames.PlanogramAssortmentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortments,
                    FieldNames.PlanogramAssortmentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortments, FieldNames.PlanogramAssortmentPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortments,
                    FieldNames.PlanogramAssortmentPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortments, FieldNames.PlanogramAssortmentName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortments,
                    FieldNames.PlanogramAssortmentName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramAssortmentProducts

            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramAssortmentId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductPlanogramAssortmentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductPlanogramAssortmentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Gtin
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductGTIN))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductGTIN,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Name
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // IsRanged
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductIsRanged))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductIsRanged,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // Rank
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductRank))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductRank,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // Segmentation
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductSegmentation))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductSegmentation,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Facings
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductFacings,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // Units
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductUnits,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // ProductTreatmentType
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductProductTreatmentType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductProductTreatmentType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // ProductLocalizationType
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductProductLocalizationType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductProductLocalizationType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // Comments
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductComments))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductComments,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ExactListFacings
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductExactListFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductExactListFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableByte,
                    null);
            }

            // ExactListUnits
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductExactListUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductExactListUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // PreserveListFacings
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductPreserveListFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductPreserveListFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableByte,
                    null);
            }

            // PreserveListUnits
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductPreserveListUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductPreserveListUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // MaxListFacings
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductMaxListFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductMaxListFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableByte,
                    null);
            }

            // MaxListUnits
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductMaxListUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductMaxListUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // MinListFacings
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductMinListFacings))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductMinListFacings,
                    GalleriaBinaryFile.ItemPropertyType.NullableByte,
                    null);
            }

            // MinListUnits
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductMinListUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductMinListUnits,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // FamilyRuleName
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductFamilyRuleName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductFamilyRuleName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // FamilyRuleType
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductFamilyRuleType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductFamilyRuleType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FamilyRuleValue
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductFamilyRuleValue))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductFamilyRuleValue,
                    GalleriaBinaryFile.ItemPropertyType.NullableByte,
                    null);
            }

            // IsPrimaryRegionalProduct
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentProducts, FieldNames.PlanogramAssortmentProductIsPrimaryRegionalProduct))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    FieldNames.PlanogramAssortmentProductIsPrimaryRegionalProduct,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region PlanogramAssortmentLocalProducts

            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentLocalProducts, FieldNames.PlanogramAssortmentLocalProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentLocalProducts,
                    FieldNames.PlanogramAssortmentLocalProductId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramAssortmentId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentLocalProducts, FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentLocalProducts,
                    FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // ProductGtin
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentLocalProducts, FieldNames.PlanogramAssortmentLocalProductProductGtin))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentLocalProducts,
                    FieldNames.PlanogramAssortmentLocalProductProductGtin,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // LocationCode
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentLocalProducts, FieldNames.PlanogramAssortmentLocalProductLocationCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentLocalProducts,
                    FieldNames.PlanogramAssortmentLocalProductLocationCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramAssortmentRegions

            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegions, FieldNames.PlanogramAssortmentRegionId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegions,
                    FieldNames.PlanogramAssortmentRegionId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramAssortmentId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegions, FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegions,
                    FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegions, FieldNames.PlanogramAssortmentRegionName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegions,
                    FieldNames.PlanogramAssortmentRegionName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramAssortmentRegionLocations

            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegionLocations, FieldNames.PlanogramAssortmentRegionLocationId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegionLocations,
                    FieldNames.PlanogramAssortmentRegionLocationId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramAssortmentRegionId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegionLocations, FieldNames.PlanogramAssortmentRegionLocationPlanogramAssortmentRegionId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegionLocations,
                    FieldNames.PlanogramAssortmentRegionLocationPlanogramAssortmentRegionId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // LocationCode
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegionLocations, FieldNames.PlanogramAssortmentRegionLocationLocationCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegionLocations,
                    FieldNames.PlanogramAssortmentRegionLocationLocationCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramAssortmentRegionProducts

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegionProducts, FieldNames.PlanogramAssortmentRegionProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegionProducts,
                    FieldNames.PlanogramAssortmentRegionProductId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramAssortmentRegionId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegionProducts, FieldNames.PlanogramAssortmentRegionProductPlanogramAssortmentRegionId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegionProducts,
                    FieldNames.PlanogramAssortmentRegionProductPlanogramAssortmentRegionId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PrimaryProductGtin
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegionProducts, FieldNames.PlanogramAssortmentRegionProductPrimaryProductGtin))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegionProducts,
                    FieldNames.PlanogramAssortmentRegionProductPrimaryProductGtin,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // RegionalProductGtin
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAssortmentRegionProducts, FieldNames.PlanogramAssortmentRegionProductRegionalProductGtin))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAssortmentRegionProducts,
                    FieldNames.PlanogramAssortmentRegionProductRegionalProductGtin,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramConsumerDecisionTrees

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTrees, FieldNames.PlanogramConsumerDecisionTreeId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTrees,
                    FieldNames.PlanogramConsumerDecisionTreeId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTrees, FieldNames.PlanogramConsumerDecisionTreePlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTrees,
                    FieldNames.PlanogramConsumerDecisionTreePlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTrees, FieldNames.PlanogramConsumerDecisionTreeName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTrees,
                    FieldNames.PlanogramConsumerDecisionTreeName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramConsumerDecisionTreeLevels

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeLevels, FieldNames.PlanogramConsumerDecisionTreeLevelId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeLevels,
                    FieldNames.PlanogramConsumerDecisionTreeLevelId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramConsumerDecisionTreeId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeLevels, FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeLevels,
                    FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // ParentLevelId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeLevels, FieldNames.PlanogramConsumerDecisionTreeLevelParentLevelId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeLevels,
                    FieldNames.PlanogramConsumerDecisionTreeLevelParentLevelId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeLevels, FieldNames.PlanogramConsumerDecisionTreeLevelName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeLevels,
                    FieldNames.PlanogramConsumerDecisionTreeLevelName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramConsumerDecisionTreeNodes

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes, FieldNames.PlanogramConsumerDecisionTreeNodeId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodes,
                    FieldNames.PlanogramConsumerDecisionTreeNodeId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramConsumerDecisionTreeId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes, FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodes,
                    FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramConsumerDecisionTreeLevelId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes, FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodes,
                    FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ParentNodeId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes, FieldNames.PlanogramConsumerDecisionTreeNodeParentNodeId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodes,
                    FieldNames.PlanogramConsumerDecisionTreeNodeParentNodeId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodes, FieldNames.PlanogramConsumerDecisionTreeNodeName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodes,
                    FieldNames.PlanogramConsumerDecisionTreeNodeName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramConsumerDecisionTreeNodeProducts

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts, FieldNames.PlanogramConsumerDecisionTreeNodeProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts,
                    FieldNames.PlanogramConsumerDecisionTreeNodeProductId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramConsumerDecisionTreeId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts, FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts,
                    FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // ProductGtin
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts, FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts,
                    FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramProductId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            #endregion

            #region PlanogramComponents

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Mesh3DId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentMesh3DId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentMesh3DId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentImageIdFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentImageIdFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentImageIdBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentImageIdBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentImageIdTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentImageIdTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentImageIdBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentImageIdBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentImageIdLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentImageIdLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentImageIdRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentImageIdRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MetaNumberOfSubComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentMetaNumberOfSubComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentMetaNumberOfSubComponents,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // MetaNumberOfMerchandisedSubComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentMetaNumberOfMerchandisedSubComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentMetaNumberOfMerchandisedSubComponents,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }
            
            // IsMoveable
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentIsMoveable))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentIsMoveable,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDisplayOnly
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentIsDisplayOnly))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentIsDisplayOnly,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // CanAttachShelfEdgeLabel
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentCanAttachShelfEdgeLabel))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentCanAttachShelfEdgeLabel,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // RetailerReferenceCode
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentRetailerReferenceCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentRetailerReferenceCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // BarCode
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentBarCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentBarCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Manufacturer
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentManufacturer))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentManufacturer,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ManufacturerPartName
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentManufacturerPartName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentManufacturerPartName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ManufacturerPartNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentManufacturerPartNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentManufacturerPartNumber,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // SupplierName
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentSupplierName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentSupplierName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // SupplierPartNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentSupplierPartNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentSupplierPartNumber,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // SupplierCostPrice
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentSupplierCostPrice))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentSupplierCostPrice,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // SupplierDiscount
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentSupplierDiscount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentSupplierDiscount,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // SupplierLeadTime
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentSupplierLeadTime))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentSupplierLeadTime,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // MinPurchaseQty
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentMinPurchaseQty))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentMinPurchaseQty,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // WeightLimit
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentWeightLimit))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentWeightLimit,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Weight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentWeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentWeight,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Volume
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentVolume))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentVolume,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Diameter
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentDiameter))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentDiameter,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Capacity
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentCapacity))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentCapacity,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // ComponentType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentComponentType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentComponentType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // IsCarPark
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentIsCarPark))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentIsCarPark,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    false);
            }

            // IsMerchandisedTopDown
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramComponents, FieldNames.PlanogramComponentIsMerchandisedTopDown))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramComponents,
                    FieldNames.PlanogramComponentIsMerchandisedTopDown,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    false);
            }

            #endregion

            #region PlanogramSubComponents

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentPlanogramComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentPlanogramComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Mesh3DId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMesh3DId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMesh3DId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentImageIdFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentImageIdFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentImageIdBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentImageIdBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentImageIdTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentImageIdTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentImageIdBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentImageIdBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentImageIdLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentImageIdLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentImageIdRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentImageIdRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // ShapeType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentShapeType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentShapeType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisableHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchandisableHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchandisableHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsVisible
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsVisible))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsVisible,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // HasCollisionDetection
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentHasCollisionDetection))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentHasCollisionDetection,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // FillPatternTypeFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillPatternTypeFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillPatternTypeFront,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillPatternTypeBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillPatternTypeBack,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillPatternTypeTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillPatternTypeTop,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillPatternTypeBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillPatternTypeBottom,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillPatternTypeLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillPatternTypeLeft,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillPatternTypeRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillPatternTypeRight,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillColourFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillColourFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillColourFront,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillColourBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillColourBack,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillColourTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillColourTop,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillColourBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillColourBottom,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillColourLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillColourLeft,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFillColourRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFillColourRight,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // LineColour
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentLineColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentLineColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentTransparencyPercentFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentTransparencyPercentFront,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentTransparencyPercentBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentTransparencyPercentBack,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentTransparencyPercentTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentTransparencyPercentTop,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentTransparencyPercentBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentTransparencyPercentBottom,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentTransparencyPercentLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentTransparencyPercentLeft,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentTransparencyPercentRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentTransparencyPercentRight,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FaceThicknessFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFaceThicknessFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFaceThicknessFront,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FaceThicknessBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFaceThicknessBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFaceThicknessBack,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }


            // FaceThicknessTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFaceThicknessTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFaceThicknessTop,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FaceThicknessBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFaceThicknessBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFaceThicknessBottom,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }


            // FaceThicknessLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFaceThicknessLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFaceThicknessLeft,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }


            // FaceThicknessRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFaceThicknessRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFaceThicknessRight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }


            // RiserHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentRiserHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentRiserHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // RiserThickness
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentRiserThickness))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentRiserThickness,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsRiserPlacedOnFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsRiserPlacedOnFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsRiserPlacedOnFront,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsRiserPlacedOnBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsRiserPlacedOnBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsRiserPlacedOnBack,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsRiserPlacedOnLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsRiserPlacedOnLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsRiserPlacedOnLeft,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsRiserPlacedOnRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsRiserPlacedOnRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsRiserPlacedOnRight,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // RiserFillPatternType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentRiserFillPatternType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentRiserFillPatternType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // RiserColour
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentRiserColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentRiserColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // RiserTransparencyPercent
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentRiserTransparencyPercent))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentRiserTransparencyPercent,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // NotchStartX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentNotchStartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentNotchStartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchSpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentNotchSpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentNotchSpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchStartY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentNotchStartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentNotchStartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchSpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentNotchSpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentNotchSpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentNotchHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentNotchHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentNotchWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentNotchWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsNotchPlacedOnFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsNotchPlacedOnFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsNotchPlacedOnFront,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsNotchPlacedOnBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsNotchPlacedOnBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsNotchPlacedOnBack,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsNotchPlacedOnLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsNotchPlacedOnLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsNotchPlacedOnLeft,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsNotchPlacedOnRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsNotchPlacedOnRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsNotchPlacedOnRight,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // NotchStyleType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentNotchStyleType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentNotchStyleType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // DividerObstructionHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionStartX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionStartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionStartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionSpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionSpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionSpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionStartY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionStartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionStartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionSpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionSpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionSpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionStartZ
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionStartZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionStartZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionSpacingZ
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentDividerObstructionSpacingZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentDividerObstructionSpacingZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsDividerObstructionAtStart
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsDividerObstructionAtStart))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsDividerObstructionAtStart,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDividerObstructionAtEnd
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsDividerObstructionAtEnd))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsDividerObstructionAtEnd,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDividerObstructionByFacing
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsDividerObstructionByFacing))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsDividerObstructionByFacing,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // MerchConstraintRow1StartX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow1StartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow1StartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1SpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1StartY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow1StartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow1StartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1SpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow1Height))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow1Height,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow1Width))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow1Width,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2StartX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow2StartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow2StartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2SpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2StartY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow2StartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow2StartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2SpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow2Height))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow2Height,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchConstraintRow2Width))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchConstraintRow2Width,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // LineThickness
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentLineThickness))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentLineThickness,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchandisingType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchandisingType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchandisingType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // CombineType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentCombineType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentCombineType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // IsProductOverlapAllowed
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentIsProductOverlapAllowed))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentIsProductOverlapAllowed,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // MerchandisingStrategyX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchandisingStrategyX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchandisingStrategyX,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisingStrategyY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchandisingStrategyY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchandisingStrategyY,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisingStrategyZ
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentMerchandisingStrategyZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentMerchandisingStrategyZ,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // LeftOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentLeftOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentLeftOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // RightOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentRightOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentRightOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FrontOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentFrontOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentFrontOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // BackOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentBackOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentBackOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // TopOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentTopOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentTopOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // BottomOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramSubComponents, FieldNames.PlanogramSubComponentBottomOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramSubComponents,
                    FieldNames.PlanogramSubComponentBottomOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            

            #endregion

            #region PlanogramProducts

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Gtin
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductGtin))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductGtin,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Brand
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductBrand))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductBrand,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DisplayHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDisplayHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDisplayHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DisplayWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDisplayWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDisplayWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DisplayDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDisplayDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDisplayDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // AlternateHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductAlternateHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductAlternateHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // AlternateWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductAlternateWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductAlternateWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // AlternateDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductAlternateDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductAlternateDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PointOfPurchaseHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPointOfPurchaseHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPointOfPurchaseHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PointOfPurchaseWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPointOfPurchaseWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPointOfPurchaseWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PointOfPurchaseDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPointOfPurchaseDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPointOfPurchaseDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NumberOfPegHoles
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductNumberOfPegHoles))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductNumberOfPegHoles,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // PegX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PegX2
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegX2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegX2,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PegX3
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegX3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegX3,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PegY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PegY2
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegY2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegY2,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PegY3
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegY3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegY3,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PegProngOffset
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegProngOffset))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegProngOffset,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // PegDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPegDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPegDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // SqueezeHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSqueezeHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSqueezeHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // SqueezeWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSqueezeWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSqueezeWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // SqueezeDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSqueezeDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSqueezeDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NestingHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductNestingHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductNestingHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NestingWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductNestingWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductNestingWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NestingDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductNestingDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductNestingDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // CasePackUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCasePackUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCasePackUnits,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // CaseHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCaseHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCaseHigh,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // CaseWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCaseWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCaseWide,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // CaseDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCaseDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCaseDeep,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // CaseHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCaseHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCaseHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // CaseWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCaseWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCaseWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // CaseDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCaseDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCaseDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MaxStack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMaxStack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxStack,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MaxTopCap
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMaxTopCap))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxTopCap,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MaxRightCap
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMaxRightCap))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxRightCap,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MinDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMinDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMinDeep,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MaxDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMaxDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxDeep,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MaxNestingHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMaxNestingHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxNestingHigh,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MaxNestingDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMaxNestingDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMaxNestingDeep,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // TrayPackUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayPackUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayPackUnits,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // TrayHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayHigh,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // TrayWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayWide,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // TrayDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayDeep,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // TrayHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // TrayWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // TrayDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // TrayThickHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayThickHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayThickHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // TrayThickWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayThickWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayThickWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // TrayThickDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTrayThickDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTrayThickDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FrontOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFrontOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFrontOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FingerSpaceAbove
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFingerSpaceAbove))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFingerSpaceAbove,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FingerSpaceToTheSide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFingerSpaceToTheSide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFingerSpaceToTheSide,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // StatusType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductStatusType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductStatusType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // OrientationType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductOrientationType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductOrientationType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisingStyle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMerchandisingStyle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMerchandisingStyle,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // IsFrontOnly
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductIsFrontOnly))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductIsFrontOnly,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsTrayProduct
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductIsTrayProduct))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductIsTrayProduct,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsPlaceHolderProduct
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductIsPlaceHolderProduct))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductIsPlaceHolderProduct,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsActive
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductIsActive))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductIsActive,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // CanBreakTrayUp
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCanBreakTrayUp))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCanBreakTrayUp,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // CanBreakTrayDown
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCanBreakTrayDown))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCanBreakTrayDown,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // CanBreakTrayBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCanBreakTrayBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCanBreakTrayBack,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // CanBreakTrayTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCanBreakTrayTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCanBreakTrayTop,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // ForceMiddleCap
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductForceMiddleCap))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductForceMiddleCap,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // ForceBottomCap
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductForceBottomCap))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductForceBottomCap,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // PlanogramImageIdFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }


            // PlanogramImageIdDisplayFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdDisplayFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdDisplayFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdDisplayBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdDisplayBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdDisplayBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdDisplayTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdDisplayTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdDisplayTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdDisplayBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdDisplayBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdDisplayBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdDisplayLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdDisplayLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdDisplayLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdDisplayRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdDisplayRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdDisplayRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdTrayFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdTrayFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdTrayFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdTrayBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdTrayBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdTrayBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdTrayTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdTrayTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdTrayTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdTrayBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdTrayBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdTrayBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdTrayLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdTrayLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdTrayLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdTrayRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdTrayRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdTrayRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }



            // PlanogramImageIdPointOfPurchaseFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdPointOfPurchaseBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdPointOfPurchaseTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdPointOfPurchaseBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdPointOfPurchaseLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdPointOfPurchaseRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }


            // PlanogramImageIdAlternateFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdAlternateFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdAlternateFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdAlternateBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdAlternateBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdAlternateBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdAlternateTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdAlternateTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdAlternateTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdAlternateBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdAlternateBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdAlternateBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdAlternateLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdAlternateLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdAlternateLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdAlternateRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdAlternateRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdAlternateRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }


            // PlanogramImageIdCaseFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdCaseFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdCaseFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdCaseBack
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdCaseBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdCaseBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdCaseTop
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdCaseTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdCaseTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdCaseBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdCaseBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdCaseBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdCaseLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdCaseLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdCaseLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramImageIdCaseRight
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPlanogramImageIdCaseRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPlanogramImageIdCaseRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }



            // ShapeType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductShapeType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductShapeType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFillPatternType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFillPatternType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillColour
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFillColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFillColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Shape
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductShape))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductShape,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // PointOfPurchaseDescription
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPointOfPurchaseDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPointOfPurchaseDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // ShortDescription
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductShortDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductShortDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Subcategory
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSubcategory))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSubcategory,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // CustomerStatus
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCustomerStatus))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCustomerStatus,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Colour
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductColour,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Flavour
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFlavour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFlavour,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // PackagingShape
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPackagingShape))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPackagingShape,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // PackagingType
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPackagingType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPackagingType,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // CountryOfOrigin
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCountryOfOrigin))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCountryOfOrigin,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // CountryOfProcessing
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCountryOfProcessing))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCountryOfProcessing,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // ShelfLife
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductShelfLife))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductShelfLife,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }
 
            // DeliveryFrequencyDays
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDeliveryFrequencyDays))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDeliveryFrequencyDays,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
 
            // DeliveryMethod
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDeliveryMethod))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDeliveryMethod,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // VendorCode
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductVendorCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductVendorCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Vendor
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductVendor))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductVendor,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // ManufacturerCode
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductManufacturerCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductManufacturerCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Manufacturer
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductManufacturer))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductManufacturer,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Size
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSize))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSize,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // UnitOfMeasure
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductUnitOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductUnitOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // DateIntroduced
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDateIntroduced))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDateIntroduced,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }
 
            // DateDiscontinued
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDateDiscontinued))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDateDiscontinued,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }
 
            // DateEffective
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductDateEffective))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductDateEffective,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }
 
            // Health
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductHealth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductHealth,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // CorporateCode
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCorporateCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCorporateCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Barcode
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductBarcode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductBarcode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // SellPrice
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSellPrice))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSellPrice,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
 
            // SellPackCount
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSellPackCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSellPackCount,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }
 
            // SellPackDescription
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductSellPackDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductSellPackDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // RecommendedRetailPrice
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductRecommendedRetailPrice))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductRecommendedRetailPrice,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
 
            // ManufacturerRecommendedRetailPrice
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductManufacturerRecommendedRetailPrice))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductManufacturerRecommendedRetailPrice,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
 
            // CostPrice
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCostPrice))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCostPrice,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
 
            // CaseCost
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductCaseCost))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductCaseCost,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
 
            // TaxRate
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTaxRate))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTaxRate,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
 
            // ConsumerInformation
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductConsumerInformation))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductConsumerInformation,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Texture
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductTexture))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductTexture,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // StyleNumber
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductStyleNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductStyleNumber,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }
 
            // Pattern
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductPattern))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductPattern,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // Model
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductModel))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductModel,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // GarmentType
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductGarmentType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductGarmentType,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }
 
            // IsPrivateLabel
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductIsPrivateLabel))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductIsPrivateLabel,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }
 
            // IsNewProduct
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductIsNewProduct))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductIsNewProduct,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //FinancialGroupCode
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFinancialGroupCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFinancialGroupCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //FinancialGroupName
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductFinancialGroupName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductFinancialGroupName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // MetaNotAchievedInventory
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramProducts, FieldNames.PlanogramProductMetaNotAchievedInventory))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramProducts,
                    FieldNames.PlanogramProductMetaNotAchievedInventory,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }

            #endregion

            #region PlanogramPositions

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramFixtureItemId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPlanogramFixtureItemId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPlanogramFixtureItemId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramFixtureAssemblyId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPlanogramFixtureAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPlanogramFixtureAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramAssemblyComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPlanogramAssemblyComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPlanogramAssemblyComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramFixtureComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPlanogramFixtureComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPlanogramFixtureComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramSubComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPlanogramSubComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPlanogramSubComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramProductId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPlanogramProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPlanogramProductId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //FacingsHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsHigh,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsWide,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsDeep,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // OrientationType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionOrientationType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionOrientationType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisingStyle
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionMerchandisingStyle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMerchandisingStyle,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //FacingsXHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsXHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsXHigh,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsXWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsXWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsXWide,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsXDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsXDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsXDeep,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //MerchandisingStyleX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionMerchandisingStyleX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMerchandisingStyleX,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //OrientationTypeX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionOrientationTypeX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionOrientationTypeX,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //IsXPlacedLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionIsXPlacedLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionIsXPlacedLeft,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            

            //FacingsYHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsYHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsYHigh,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsYWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsYWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsYWide,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsYDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsYDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsYDeep,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //MerchandisingStyleY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionMerchandisingStyleY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMerchandisingStyleY,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //OrientationTypeY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionOrientationTypeY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionOrientationTypeY,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //IsYPlacedBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionIsYPlacedBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionIsYPlacedBottom,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            

            //FacingsZHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsZHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsZHigh,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsZWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsZWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsZWide,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //FacingsZDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionFacingsZDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionFacingsZDeep,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //MerchandisingStyleZ
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionMerchandisingStyleZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMerchandisingStyleZ,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //OrientationTypeZ
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionOrientationTypeZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionOrientationTypeZ,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //IsZPlacedFront
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionIsZPlacedFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionIsZPlacedFront,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Sequence
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionSequence))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionSequence,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }


            //SequenceX
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionSequenceX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionSequenceX,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            //SequenceY
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionSequenceY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionSequenceY,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }


            //SequenceZ
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionSequenceZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionSequenceZ,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }


            // UnitsHigh
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionUnitsHigh))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionUnitsHigh,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // UnitsWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionUnitsWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionUnitsWide,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // UnitsDeep
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionUnitsDeep))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionUnitsDeep,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // TotalUnits
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionTotalUnits))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionTotalUnits,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }
            
            // MetaAchievedCases
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaAchievedCases))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaAchievedCases,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }
                        
            // MetaIsPositionCollisions
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaIsPositionCollisions))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaIsPositionCollisions,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }

            // MetaFrontFacingsWide
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionMetaFrontFacingsWide))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionMetaFrontFacingsWide,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }
            
            // PositionSequenceNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPositions, FieldNames.PlanogramPositionPositionSequenceNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPositions,
                    FieldNames.PlanogramPositionPositionSequenceNumber,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            #endregion

            #region PlanogramAnnotations

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramFixtureItemId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationPlanogramFixtureItemId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationPlanogramFixtureItemId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramFixtureAssemblyId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationPlanogramFixtureAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationPlanogramFixtureAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramFixtureComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationPlanogramFixtureComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationPlanogramFixtureComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramAssemblyComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationPlanogramAssemblyComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationPlanogramAssemblyComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramSubComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationPlanogramSubComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationPlanogramSubComponentId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramPositionId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationPlanogramPositionId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationPlanogramPositionId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // AnnotationType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationAnnotationType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationAnnotationType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // Text
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationText))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationText,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationX,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationY,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationZ,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // BorderColour
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationBorderColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationBorderColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // BorderThickness
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationBorderThickness))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationBorderThickness,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // BackgroundColour
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationBackgroundColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationBackgroundColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FontColour
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationFontColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationFontColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FontSize
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationFontSize))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationFontSize,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FontName
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationFontName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationFontName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // CanReduceFontToFit
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramAnnotations, FieldNames.PlanogramAnnotationCanReduceFontToFit))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramAnnotations,
                    FieldNames.PlanogramAnnotationCanReduceFontToFit,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region PlanogramImages

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramImages, FieldNames.PlanogramImageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramImages,
                    FieldNames.PlanogramImageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramImages, FieldNames.PlanogramImagePlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramImages,
                    FieldNames.PlanogramImagePlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FileName
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramImages, FieldNames.PlanogramImageFileName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramImages,
                    FieldNames.PlanogramImageFileName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Description
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramImages, FieldNames.PlanogramImageDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramImages,
                    FieldNames.PlanogramImageDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ImageData
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramImages, FieldNames.PlanogramImageImageData))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramImages,
                    FieldNames.PlanogramImageImageData,
                    GalleriaBinaryFile.ItemPropertyType.ByteArray,
                    null);
            }

            #endregion

            #region PlanogramInventory

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // DaysOfPerformance
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryDaysOfPerformance))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryDaysOfPerformance,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MinCasePacks
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryMinCasePacks))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryMinCasePacks,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MinDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryMinDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryMinDos,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MinShelfLife
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryMinShelfLife))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryMinShelfLife,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MinDeliveries
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryMinDeliveries))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryMinDeliveries,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsCasePacksValidated
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryIsCasePacksValidated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryIsCasePacksValidated,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDosValidated
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryIsDosValidated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryIsDosValidated,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsShelfLifeValidated
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryIsShelfLifeValidated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryIsShelfLifeValidated,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDeliveriesValidated
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramInventory, FieldNames.PlanogramInventoryIsDeliveriesValidated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramInventory,
                    FieldNames.PlanogramInventoryIsDeliveriesValidated,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region PlanogramMetadataImages

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramMetadataImages, FieldNames.PlanogramMetadataImageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramMetadataImages,
                    FieldNames.PlanogramMetadataImageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramMetadataImages, FieldNames.PlanogramMetadataImagePlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramMetadataImages,
                    FieldNames.PlanogramMetadataImagePlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FileName
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramMetadataImages, FieldNames.PlanogramMetadataImageFileName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramMetadataImages,
                    FieldNames.PlanogramMetadataImageFileName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Description
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramMetadataImages, FieldNames.PlanogramMetadataImageDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramMetadataImages,
                    FieldNames.PlanogramMetadataImageDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ImageData
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramMetadataImages, FieldNames.PlanogramMetadataImageImageData))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramMetadataImages,
                    FieldNames.PlanogramMetadataImageImageData,
                    GalleriaBinaryFile.ItemPropertyType.ByteArray,
                    null);
            }

            #endregion

            #region Custom Attribute Data

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //ParentId
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataParentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataParentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //ParentType
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataParentType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataParentType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            #region Text Properties

            //Text1
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText1,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text2
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText2,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text3
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText3,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text4
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText4))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText4,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text5
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText5))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText5,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text6
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText6))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText6,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text7
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText7))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText7,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text8
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText8))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText8,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text9
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText9))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText9,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text10
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText10))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText10,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }


            //Text11
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText11))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText11,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text12
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText12))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText12,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text13
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText13))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText13,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text14
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText14))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText14,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text15
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText15))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText15,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text16
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText16))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText16,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text17
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText17))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText17,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text18
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText18))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText18,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text19
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText19))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText19,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text20
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText20))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText20,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text21
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText21))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText21,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text22
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText22))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText22,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text23
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText23))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText23,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text24
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText24))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText24,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text25
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText25))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText25,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text26
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText26))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText26,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text27
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText27))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText27,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text28
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText28))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText28,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text29
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText29))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText29,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text30
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText30))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText30,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text31
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText31))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText31,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text32
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText32))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText32,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text33
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText33))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText33,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text34
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText34))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText34,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text35
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText35))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText35,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text36
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText36))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText36,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text37
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText37))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText37,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text38
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText38))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText38,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text39
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText39))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText39,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text40
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText40))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText40,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text41
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText41))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText41,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text42
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText42))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText42,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text43
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText43))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText43,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text44
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText44))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText44,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text45
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText45))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText45,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text46
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText46))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText46,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text47
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText47))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText47,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text48
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText48))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText48,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text49
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText49))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText49,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Text50
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataText50))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataText50,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }


            #endregion

            #region Value Properties

            //Value1
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue1,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value2
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue2,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value3
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue3,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value4
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue4))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue4,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value5
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue5))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue5,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value6
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue6))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue6,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value7
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue7))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue7,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value8
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue8))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue8,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value9
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue9))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue9,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value10
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue10))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue10,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }


            //Value11
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue11))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue11,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value12
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue12))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue12,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value13
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue13))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue13,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value14
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue14))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue14,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value15
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue15))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue15,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value16
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue16))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue16,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value17
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue17))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue17,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value18
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue18))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue18,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value19
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue19))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue19,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value20
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue20))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue20,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value21
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue21))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue21,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value22
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue22))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue22,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value23
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue23))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue23,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value24
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue24))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue24,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value25
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue25))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue25,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value26
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue26))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue26,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value27
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue27))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue27,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value28
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue28))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue28,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value29
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue29))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue29,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value30
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue30))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue30,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value31
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue31))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue31,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value32
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue32))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue32,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value33
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue33))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue33,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value34
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue34))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue34,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value35
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue35))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue35,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value36
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue36))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue36,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value37
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue37))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue37,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value38
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue38))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue38,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value39
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue39))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue39,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value40
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue40))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue40,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value41
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue41))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue41,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value42
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue42))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue42,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value43
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue43))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue43,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value44
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue44))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue44,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value45
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue45))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue45,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value46
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue46))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue46,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value47
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue47))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue47,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value48
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue48))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue48,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value49
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue49))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue49,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Value50
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataValue50))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataValue50,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }


            #endregion

            #region Flag Properties

            //Flag1
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag1,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag2
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag2,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag3
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag3,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag4
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag4))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag4,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag5
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag5))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag5,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag6
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag6))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag6,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag7
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag7))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag7,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag8
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag8))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag8,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag9
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag9))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag9,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //Flag10
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataFlag10))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataFlag10,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region Date Properties

            //Date1
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate1,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date2
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate2,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date3
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate3,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date4
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate4))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate4,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date5
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate5))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate5,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date6
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate6))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate6,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date7
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate7))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate7,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date8
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate8))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate8,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date9
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate9))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate9,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            //Date10
            if (!file.ItemPropertyExists((UInt16)SectionType.CustomAttributeData, FieldNames.CustomAttributeDataDate10))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.CustomAttributeData,
                    FieldNames.CustomAttributeDataDate10,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            #endregion

            #endregion

            #region PlanogramPerformances

            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformances,FieldNames.PlanogramPerformanceId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformances,
                    FieldNames.PlanogramPerformanceId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformances,FieldNames.PlanogramPerformancePlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformances,
                    FieldNames.PlanogramPerformancePlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformances,FieldNames.PlanogramPerformanceName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformances,
                    FieldNames.PlanogramPerformanceName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Description
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformances,FieldNames.PlanogramPerformanceDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformances,
                    FieldNames.PlanogramPerformanceDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramPerformanceDatas
            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramPerformanceId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataPlanogramPerformanceId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataPlanogramPerformanceId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramProductId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataPlanogramProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataPlanogramProductId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // P1
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP1,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P2
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP2,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P3
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP3,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P4
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP4))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP4,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P5
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP5))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP5,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P6
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP6))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP6,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P7
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP7))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP7,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P8
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP8))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP8,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P9
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP9))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP9,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P10
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP10))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP10,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P11
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP11))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP11,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P12
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP12))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP12,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P13
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP13))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP13,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P14
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP14))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP14,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P15
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP15))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP15,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P16
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP16))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP16,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P17
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP17))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP17,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P18
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP18))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP18,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P19
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP19))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP19,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // P20
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas,FieldNames.PlanogramPerformanceDataP20))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataP20,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // UnitsSoldPerDay
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas, FieldNames.PlanogramPerformanceDataUnitsSoldPerDay))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataUnitsSoldPerDay,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // AchieveCasePacks
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas, FieldNames.PlanogramPerformanceDataAchievedCasePacks))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataAchievedCasePacks,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // AchieveDos
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas, FieldNames.PlanogramPerformanceDataAchievedDos))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataAchievedDos,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // AchieveShelfLife
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas, FieldNames.PlanogramPerformanceDataAchievedShelfLife))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataAchievedShelfLife,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // AchieveDeliveries
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceDatas, FieldNames.PlanogramPerformanceDataAchievedDeliveries))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    FieldNames.PlanogramPerformanceDataAchievedDeliveries,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            #endregion

            #region PlanogramPerformanceMetrics

            // Id
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramPerformanceId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Description
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Direction
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricDirection))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricDirection,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // SpecialType
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricSpecialType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricSpecialType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MetricType
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricMetricType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricMetricType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MetricId
            if(!file.ItemPropertyExists((UInt16)SectionType.PlanogramPerformanceMetrics,FieldNames.PlanogramPerformanceMetricMetricId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    FieldNames.PlanogramPerformanceMetricMetricId,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            #endregion

            #region PlanogramEventLogs

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // EventType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogEventType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogEventType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // EntryType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogEntryType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogEntryType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // AffectedType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogAffectedType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogAffectedType,
                    GalleriaBinaryFile.ItemPropertyType.NullableByte,
                    null);
            }

            // AffectedId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogAffectedId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogAffectedId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // WorkpackageSource
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogWorkpackageSource))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogWorkpackageSource,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // TaskSource
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogTaskSource))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogTaskSource,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // EventId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogEventId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogEventId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // DateTime
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogDateTime))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogDateTime,
                    GalleriaBinaryFile.ItemPropertyType.DateTime,
                    null);
            }

            // Description
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Content
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramEventLogs, FieldNames.PlanogramEventLogContent))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramEventLogs,
                    FieldNames.PlanogramEventLogContent,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramBlocking

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlocking, FieldNames.PlanogramBlockingId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlocking,
                    FieldNames.PlanogramBlockingId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlocking, FieldNames.PlanogramBlockingPlanogramId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlocking,
                    FieldNames.PlanogramBlockingPlanogramId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Type
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlocking, FieldNames.PlanogramBlockingType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlocking,
                    FieldNames.PlanogramBlockingType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            #endregion

            #region PlanogramBlockingGroup

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //PlanogramBlockingId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupPlanogramBlockingId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupPlanogramBlockingId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Colour
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //FillPatternType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupFillPatternType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupFillPatternType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //CanCompromiseSequence
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupCanCompromiseSequence))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupCanCompromiseSequence,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //IsRestrictedByComponentType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupIsRestrictedByComponentType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupIsRestrictedByComponentType,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //CanOptimise
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupCanOptimise))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupCanOptimise,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //CanMerge
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupCanMerge))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupCanMerge,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            //ProductAssignType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupProductAssignType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupProductAssignType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //BlockPlacementXType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupBlockPlacementXType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupBlockPlacementXType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //BlockPlacementYType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupBlockPlacementYType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupBlockPlacementYType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //TotalSpacePercentage
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroups, FieldNames.PlanogramBlockingGroupTotalSpacePercentage))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    FieldNames.PlanogramBlockingGroupTotalSpacePercentage,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region PlanogramBlockingGroupProduct

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupProducts, FieldNames.PlanogramBlockingGroupProductId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupProducts,
                    FieldNames.PlanogramBlockingGroupProductId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //PlanogramBlockingGroupId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupProducts, FieldNames.PlanogramBlockingGroupProductPlanogramBlockingGroupId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupProducts,
                    FieldNames.PlanogramBlockingGroupProductPlanogramBlockingGroupId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //Gtin
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupProducts, FieldNames.PlanogramBlockingGroupProductGtin))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupProducts,
                    FieldNames.PlanogramBlockingGroupProductGtin,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region PlanogramBlockingGroupField

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //PlanogramBlockingGroupId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldPlanogramBlockingGroupId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldPlanogramBlockingGroupId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //Name
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //FieldType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldFieldType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldFieldType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //OperandType
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldOperandType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldOperandType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //TextValue
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldTextValue))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldTextValue,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //FlagValue
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldFlagValue))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldFlagValue,
                    GalleriaBinaryFile.ItemPropertyType.NullableBoolean,
                    null);
            }

            //NumericValue
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldNumericValue))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldNumericValue,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            //DateTimeValue
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingGroupFields, FieldNames.PlanogramBlockingGroupFieldDateTimeValue))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingGroupFields,
                    FieldNames.PlanogramBlockingGroupFieldDateTimeValue,
                    GalleriaBinaryFile.ItemPropertyType.NullableDateTime,
                    null);
            }

            #endregion

            #region PlanogramBlockingDivider

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //PlanogramBlockingId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerPlanogramBlockingId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerPlanogramBlockingId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //Type
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //Level
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerLevel))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerLevel,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //X
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Y
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Length
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerLength))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerLength,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //IsSnapped
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingDividers, FieldNames.PlanogramBlockingDividerIsSnapped))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    FieldNames.PlanogramBlockingDividerIsSnapped,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region PlanogramBlockingLocation

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // PlanogramBlockingId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationPlanogramBlockingId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationPlanogramBlockingId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramBlockingGroupId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationPlanogramBlockingGroupId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationPlanogramBlockingGroupId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramBlockingDividerTopId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerTopId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerTopId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramBlockingDividerBottomId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerBottomId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerBottomId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // PlanogramBlockingDividerLeftId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerLeftId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerLeftId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //PlanogramBlockingDividerRightId
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerRightId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerRightId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            //SpacePercentage
            if (!file.ItemPropertyExists((UInt16)SectionType.PlanogramBlockingLocations, FieldNames.PlanogramBlockingLocationSpacePercentage))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    FieldNames.PlanogramBlockingLocationSpacePercentage,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #endregion
        }
        #endregion

        #region Downgrade
        /// <summary>
        /// Performs the downgrade of a planogram file
        /// </summary>
        public void Downgrade(GalleriaBinaryFile file)
        {
            // Do nothing - this is the first version!
        }
        #endregion
    }
}
