﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Security.Cryptography;
//using System.Text;

//namespace Galleria.Framework.Planograms.Dal.Pog
//{
//    /// <summary>
//    /// The GalleriaBinaryFile class represents a binary file that contains data for consumption by a Galleria product,
//    /// e.g. a planogram.  The file's TypeIdentifier, e.g. "Galleria Planogram File", tells you what the file contains.
//    /// GalleriaBinaryFile implements IDisposable and maintains an open stream to the underlying file on the file
//    /// system that it represents until it is disposed.  Changes made to the file through this class's methods are
//    /// held in memory until Save() is called, at which point they are written to the underlying file which then
//    /// remains open for subsequent changes.
//    /// 
//    /// The document Planogram File Format and its accompanying Visio diagram explain the structure of a Galleria
//    /// Binary File in detail.  (TODO: Once the documentation is formally finished and released, refer to its exact
//    /// location.)
//    /// </summary>
//    public class GalleriaBinaryFile : IDisposable
//    {
//        #region Instrumentation

//        // TODO: Consider the use of Loupe for this.

//        private static Dictionary<String, System.Diagnostics.Stopwatch> _stopwatches =
//            new Dictionary<String, System.Diagnostics.Stopwatch>();
//        private static Dictionary<String, Int32> _counts = new Dictionary<String, Int32>();

//        [System.Diagnostics.Conditional("DEBUG")]
//        private static void StartTiming(String name)
//        {
//            System.Diagnostics.Stopwatch stopwatch = null;
//            if (!_stopwatches.TryGetValue(name, out stopwatch))
//            {
//                stopwatch = new System.Diagnostics.Stopwatch();
//                _stopwatches[name] = stopwatch;
//                _counts[name] = 0;
//            }
//            stopwatch.Start();
//        }

//        [System.Diagnostics.Conditional("DEBUG")]
//        private static void StopTiming(String name)
//        {
//            _stopwatches[name].Stop();
//            _counts[name]++;
//        }

//        [System.Diagnostics.Conditional("DEBUG")]
//        private static void WriteTimers()
//        {
//            Console.WriteLine("Galleria Binary File Times:");
//            foreach (String name in _stopwatches.Keys)
//            {
//                Console.WriteLine(String.Format(
//                    "\t{0}: {1} / {2}",
//                    name,
//                    _stopwatches[name].ElapsedMilliseconds,
//                    _counts[name]));
//            }
//        }

//        #endregion

//        #region Constants

//        /// <summary>
//        /// A version number used to manage changes to the file format over time, so that if this code must change, it
//        /// can still be programmed to handle older files.
//        /// </summary>
//        private const Int32 _formatVersion = 1;
//        /// <summary>
//        /// The section type number that represents removed sections.
//        /// </summary>
//        private const Int32 _removedSectionType = 0;

//        #endregion

//        #region Private Fields

//        /// <summary>
//        /// A read/write stream to the underlying file.
//        /// </summary>
//        private FileStream _fileStream;
//        /// <summary>
//        /// A reader for reading data from the underlying file.
//        /// </summary>
//        private ExtendedBinaryReader _fileReader;
//        /// <summary>
//        /// A writer for writing data to the underlying file.
//        /// </summary>
//        private ExtendedBinaryWriter _fileWriter;
//        /// <summary>
//        /// The file's header.
//        /// </summary>
//        private FileHeader _fileHeader;
//        /// <summary>
//        /// The file's sections.
//        /// </summary>
//        private List<Section> _sections = new List<Section>();
//        /// <summary>
//        /// The key (password) to be used for AES encryption/decryption, if any sections are encrypted in this way.
//        /// </summary>
//        private Byte[] _aes256Key;

//        #endregion

//        #region Constructor

//        /// <summary>
//        /// Creates a new GalleriaBinaryFile representing a particular file on the file system.
//        /// </summary>
//        /// <param name="file">The fully-qualified path and filename for the file to open.  If it does not exist it
//        /// will be created.</param>
//        /// <param name="typeIdentifier">The type identifier for the file.  If the underlying file already exists, this
//        /// value will be compared against the value read out of the underlying file, and if they don't match an
//        /// exception will be thrown.</param>
//        public GalleriaBinaryFile(String file, String typeIdentifier) : this(file, typeIdentifier, null) {}

//        /// <summary>
//        /// Creates a new GalleriaBinaryFile representing a particular file on the file system.
//        /// </summary>
//        /// <param name="file">The fully-qualified path and filename for the file to open.  If it does not exist it
//        /// will be created.</param>
//        /// <param name="typeIdentifier">The type identifier for the file.  If the underlying file already exists, this
//        /// value will be compared against the value read out of the underlying file, and if they don't match an
//        /// exception will be thrown.</param>
//        /// <param name="aes256Key">The key for AES encryption/decryption if any of the file's sections are/should be
//        /// encrypted in this way.</param>
//        public GalleriaBinaryFile(String file, String typeIdentifier, Byte[] aes256Key)
//        {
//            Boolean isNew = !File.Exists(file);
//            _aes256Key = aes256Key;
//            _fileStream = new FileStream(file, FileMode.OpenOrCreate);
//            if (isNew)
//            {
//                _fileHeader = new FileHeader(typeIdentifier);
//            }
//            else
//            {
//                Load(typeIdentifier);
//            }
//            _fileWriter = new ExtendedBinaryWriter(_fileStream);
//        }

//        #endregion

//        #region Nested Types

//        #region EncryptionType Enum

//        /// <summary>
//        /// The encryption type enum enumerates all possible ways of encrypting sections.
//        /// </summary>
//        private enum EncryptionType : byte
//        {
//            None = 0,
//            Aes256 = 1
//        }

//        #endregion

//        #region ItemPropertyType Enum

//        /// <summary>
//        /// The ItemPropertyType enum enumerates all of the data types that are supported as types for item properties
//        /// within a Galleria Binary File.
//        /// </summary>
//        public enum ItemPropertyType : byte
//        {
//            Deleted = 0, // TODO: Do we need this if we have to rewrite any sections that have changed anyway?
//            Boolean = 1,
//            Byte = 2,
//            //Char = 3, // TODO: Learn more about surrogate pairs before implementing
//            DateTime = 4,
//            Decimal = 5,
//            Double = 6,
//            Int32 = 7,
//            Int64 = 8,
//            SByte = 9,
//            Int16 = 10,
//            Single = 11,
//            String = 12,
//            UInt32 = 13,
//            UInt64 = 14,
//            UInt16 = 15,
//            NullableBoolean = 16,
//            NullableByte = 17,
//            //NullableChar = 18, // TODO: Learn more about surrogate pairs before implementing
//            NullableDateTime = 19,
//            NullableDecimal = 20,
//            NullableDouble = 21,
//            NullableInt32 = 22,
//            NullableInt64 = 23,
//            NullableSByte = 24,
//            NullableInt16 = 25,
//            NullableSingle = 26,
//            NullableUInt32 = 27,
//            NullableUInt64 = 28,
//            NullableUInt16 = 29,
//            BooleanArray = 30,
//            ByteArray = 31,
//            //CharArray = 32, // TODO: Learn more about surrogate pairs before implementing
//            DecimalArray = 33,
//            DoubleArray = 34,
//            Int32Array = 35,
//            Int64Array = 36,
//            SByteArray = 37,
//            Int16Array = 38,
//            SingleArray = 39,
//            UInt32Array = 40,
//            UInt64Array = 41,
//            UInt16Array = 42
//        }

//        #endregion

//        #region IItem Interface

//        /// <summary>
//        /// The IItem interface defines the contract for any items read from and written to a Galleria Binary File.
//        /// </summary>
//        public interface IItem
//        {
//            /// <summary>
//            /// Gets the value of an item property by name.
//            /// </summary>
//            /// <param name="itemPropertyName">The name of the property whose value is to be fetched.</param>
//            Object GetItemPropertyValue(String itemPropertyName);
//        }

//        #endregion

//        #region FileHeader Class

//        /// <summary>
//        /// The FileHeader class represents the header of a Galleria Planogram File, for internal use.
//        /// </summary>
//        private class FileHeader
//        {
//            #region Constructors

//            /// <summary>
//            /// Creates a new FileHeader with a given type identifier, 0 for size and 1 for format version.
//            /// </summary>
//            public FileHeader(String typeIdentifier)
//            {
//                TypeIdentifier = typeIdentifier;
//                FormatVersion = _formatVersion;
//            }

//            /// <summary>
//            /// Creates a new FileHeader by reading the expected data from an ExtendedBinaryReader.
//            /// </summary>
//            public FileHeader(ExtendedBinaryReader fileReader)
//            {
//                try
//                {
//                    // TODO: Think about whether we should do something besides let whatever exception gets thrown get
//                    // thrown if we don't find what we're looking for.
//                    Size = fileReader.ReadUInt32();
//                    TypeIdentifier = fileReader.ReadString();
//                    FormatVersion = fileReader.ReadByte();

//                    // If new elements must be added to the file header after the intitial release of the code, add code to
//                    // read them in after this line, but be sure to check against the FormatVersion property to ensure that 
//                    // the file you're reading is new enough to contain them.
//                }
//                catch (Exception e)
//                {
//                    throw new Exception("TODO");
//                }
//            }

//            #endregion

//            #region Public Properties

//            /// <summary>
//            /// The size of the file header, including the bytes for this property, as read in from the file, or 0 if
//            /// the file has never been written before.
//            /// </summary>
//            public UInt32 Size { get; set; }
//            /// <summary>
//            /// A string identifying what type of file this is, e.g. "Galleria Planogram File".
//            /// </summary>
//            public String TypeIdentifier { get; set; }
//            /// <summary>
//            /// A number identifying the format version of this file, so that the DAL reading it knows what sections
//            /// and item properties to expect.
//            /// </summary>
//            public Byte FormatVersion { get; set; }

//            #endregion

//            #region Public Methods

//            /// <summary>
//            /// Writes this file header to an ExtendedBinaryWriter.
//            /// </summary>
//            public void Write(ExtendedBinaryWriter fileWriter)
//            {
//                Int64 startPosition = fileWriter.BaseStream.Position;
//                fileWriter.Write(Size);
//                fileWriter.Write(TypeIdentifier);
//                fileWriter.Write(FormatVersion);

//                // If new elements must be added to the file header after the intitial release of the code, add them
//                // after this line.

//                // If new elements must be added to the file header after the intitial release of the code, add them
//                // before this line.

//                UInt32 size = (UInt32)Math.Max(Size, fileWriter.BaseStream.Position - startPosition);
//                if (Size != size)
//                {
//                    // Either we've never written this header before, or the header is now bigger than it was when we
//                    // first read it in because the file format has changed, so we need to write the correct value of 
//                    // Size out again.
//                    Size = size;
//                    fileWriter.BaseStream.Position = startPosition;
//                    fileWriter.Write(Size);
//                }
//                fileWriter.BaseStream.Position = startPosition + Size;
//            }

//            #endregion
//        }

//        #endregion

//        #region Section Class

//        /// <summary>
//        /// The Section class represents a section in a Galleria Planogram File, for internal use.  In order to speed
//        /// up the loading of Galleria Binary Files, the Section class always has its header resident in memory, but
//        /// its item properties and items are only loaded if required.
//        /// </summary>
//        private class Section
//        {
//            #region Constants

//            private const UInt32 _minTargetEmptySpace = 1024;
//            private const UInt32 _maxTargetEmptySpace = 1048576;

//            #endregion

//            public struct DummyItemGroupKey
//            {
//                public override Boolean Equals(Object obj)
//                {
//                    if (obj is DummyItemGroupKey)
//                    {
//                        return true;
//                    }
//                    return false;
//                }

//                public override Int32 GetHashCode()
//                {
//                    return 17;
//                }
//            }

//            #region Private Fields
//            private static readonly DummyItemGroupKey _dummyItemGroupKey = new DummyItemGroupKey();
//            private ItemProperties _itemProperties;
//            private Dictionary<Object, ItemGroup> _itemGroups = new Dictionary<Object, ItemGroup>();
//            private Boolean _headerSizeCalculated;
//            private Byte[] _aes256Key;
//            private Byte[] _aes256Salt;
//            #endregion

//            #region Constructors

//            /// <summary>
//            /// Creates a new empty Section of a given type.
//            /// </summary>
//            /// <param name="type">The Section type.</param>
//            /// <param name="aes256Key">The key (password) to be used for AES encryption, if this section should be
//            /// encrypted in this way.</param>
//            public Section(UInt16 type, ItemPropertyType itemGroupKeyType, Byte[] aes256Key)
//            {
//                Type = type;
//                ItemGroupKeyType = itemGroupKeyType;
//                _aes256Key = aes256Key;
//                using (AesManaged aes = new AesManaged())
//                {
//                    aes.GenerateIV();
//                    _aes256Salt = aes.IV;
//                }
//                _itemProperties = new ItemProperties(aes256Key, _aes256Salt, EncryptionType.None);
//                if (itemGroupKeyType == ItemPropertyType.Deleted)
//                {
//                    _itemGroups[_dummyItemGroupKey] = new ItemGroup(
//                        aes256Key, 
//                        _aes256Salt, 
//                        _itemProperties, 
//                        EncryptionType.None);
//                }
//            }

//            /// <summary>
//            /// Creates a new Section by reading the expected data from a file.
//            /// </summary>
//            /// <param name="fileReader">An ExtendedBinaryReader reading from a stream to the underlying file.</param>
//            /// <param name="startPosition">The position within the underlying stream of the beginning of the section
//            /// to be read.</param>
//            /// <param name="aes256Key">The key (password) to be used for AES encryption/decryption, if this section is/
//            /// should be encrypted in this way.</param>
//            public Section(ExtendedBinaryReader fileReader, Int64 startPosition, Byte[] aes256Key)
//            {
//                _aes256Key = aes256Key;
//                fileReader.BaseStream.Position = startPosition;
//                HeaderSize = fileReader.ReadUInt32();
//                Type = fileReader.ReadUInt16();
//                ContentSize = fileReader.ReadUInt32();
//                EncryptionType = (EncryptionType)fileReader.ReadByte();
//                switch (EncryptionType)
//                {
//                    case EncryptionType.Aes256:
//                        _aes256Salt = fileReader.ReadByteArray();
//                        break;
//                }
//                _itemProperties = new ItemProperties(
//                    fileReader,
//                    startPosition + HeaderSize,
//                    aes256Key,
//                    _aes256Salt,
//                    EncryptionType);
//                ItemGroupKeyType = (ItemPropertyType)fileReader.ReadByte();
//                if (ItemGroupKeyType != ItemPropertyType.Deleted)
//                {
//                    Byte itemGroupCount = fileReader.ReadByte();
//                    for (Byte i = 0; i < itemGroupCount; i++)
//                    {
//                        Object itemGroupKey = ReadValue(fileReader, ItemGroupKeyType);
//                        UInt32 relativeStartPosition = fileReader.ReadUInt32();
//                        _itemGroups[itemGroupKey] = new ItemGroup(
//                            fileReader,
//                            startPosition + HeaderSize + relativeStartPosition,
//                            aes256Key,
//                            _aes256Salt,
//                            _itemProperties,
//                            EncryptionType);
//                    }
//                }
//                else
//                {
//                    _itemGroups[_dummyItemGroupKey] = new ItemGroup(
//                        fileReader,
//                        startPosition + HeaderSize + fileReader.ReadUInt32(),
//                        aes256Key,
//                        _aes256Salt,
//                        _itemProperties,
//                        EncryptionType);
//                }
//                // Skip to the start of the section content.
//                fileReader.BaseStream.Position = startPosition + HeaderSize;
//                StartPosition = startPosition;
//            }

//            #endregion

//            #region Nested Types

//            #region ItemProperties Class

//            private class ItemProperties : ContentSubSection
//            {
//                private Dictionary<Byte, ItemProperty> _itemProperties = new Dictionary<Byte, ItemProperty>();

//                public ItemProperties(Byte[] aes256Key, Byte[] aesSalt, EncryptionType encryptionType) : 
//                    base(aes256Key, aesSalt, encryptionType) { }

//                public ItemProperties(
//                    ExtendedBinaryReader fileReader, 
//                    Int64 startPosition, 
//                    Byte[] aes256Key, 
//                    Byte[] aes256Salt,
//                    EncryptionType encryptionType) :
//                    base(fileReader, startPosition, aes256Key, aes256Salt, encryptionType) { }

//                public ItemProperty this[Byte id]
//                {
//                    get
//                    {
//                        Load();
//                        return _itemProperties[id];
//                    }
//                }

//                public IEnumerable<ItemProperty> GetItemProperties()
//                {
//                    Load();
//                    foreach (ItemProperty itemProperty in _itemProperties.Values)
//                    {
//                        yield return itemProperty;
//                    }
//                }

//                public Boolean ItemPropertyExists(String itemPropertyName)
//                {
//                    return GetItemPropertyByName(itemPropertyName) != null;
//                }

//                public ItemPropertyType GetItemPropertyType(String itemPropertyName)
//                {
//                    return GetItemPropertyByName(itemPropertyName).Type;
//                }

//                public Byte AddItemProperty(
//                    String itemPropertyName,
//                    ItemPropertyType itemPropertyType,
//                    Object defaultValue)
//                {
//                    Load();
//                    Byte id = 1;
//                    if (_itemProperties.Count > 0)
//                    {
//                        id = (Byte)(_itemProperties.Keys.Max() + 1);
//                    }
//                    ItemProperty itemProperty = new ItemProperty(
//                        id,
//                        itemPropertyName,
//                        itemPropertyType);
//                    _itemProperties[itemProperty.Id] = itemProperty;
//                    return itemProperty.Id;
//                }

//                public void RemoveItemProperty(Byte id)
//                {
//                    Load();
//                    _itemProperties.Remove(id);
//                }

//                public void RenameItemProperty(String itemPropertyName, String newItemPropertyName)
//                {
//                    Load();
//                    ItemProperty itemProperty = GetItemPropertyByName(itemPropertyName);
//                    itemProperty.Name = newItemPropertyName;
//                }

//                public ItemProperty GetItemPropertyByName(String itemPropertyName)
//                {
//                    // Could optimize, but probably no need just for upgrades.
//                    Load();
//                    return _itemProperties.Values.FirstOrDefault(i => i.Name == itemPropertyName);
//                }

//                public override void LoadData(ExtendedBinaryReader reader)
//                {
//                    // Load Item Properties
//                    Byte itemPropertyCount = reader.ReadByte();
//                    for (Byte i = 0; i < itemPropertyCount; i++)
//                    {
//                        ItemProperty itemProperty = new ItemProperty(
//                            reader.ReadByte(),
//                            reader.ReadString(),
//                            (ItemPropertyType)reader.ReadByte());
//                        _itemProperties[itemProperty.Id] = itemProperty;
//                    }
//                }

//                public override void WriteData(ExtendedBinaryWriter writer)
//                {
//                    writer.Write((Byte)_itemProperties.Count);
//                    foreach (ItemProperty itemProperty in _itemProperties.Values)
//                    {
//                        writer.Write(itemProperty.Id);
//                        writer.Write(itemProperty.Name);
//                        writer.Write((Byte)itemProperty.Type);
//                    }
//                }
//            }

//            private class ItemGroup : ContentSubSection
//            {
//                public List<ItemData> _items = new List<ItemData>();
//                private ItemProperties _itemProperties;

//                public ItemGroup(
//                    Byte[] aes256Key, 
//                    Byte[] aes256Salt, 
//                    ItemProperties itemProperties, 
//                    EncryptionType encryptionType) :
//                    base(aes256Key, aes256Salt, encryptionType)
//                {
//                    _itemProperties = itemProperties;
//                }

//                public ItemGroup(
//                    ExtendedBinaryReader fileReader,
//                    Int64 startPosition,
//                    Byte[] aes256Key,
//                    Byte[] aes256Salt, 
//                    ItemProperties itemProperties,
//                    EncryptionType encryptionType) :
//                    base(fileReader, startPosition, aes256Key, aes256Salt, encryptionType)
//                {
//                    _itemProperties = itemProperties;
//                }

//                public void AddItemProperty(
//                    Byte itemPropertyId,
//                    ItemPropertyType itemPropertyType,
//                    Object defaultValue)
//                {
//                    Load();
//                    if (defaultValue != GetDefaultValue(itemPropertyType))
//                    {
//                        foreach (ItemData item in _items)
//                        {
//                            item.PropertyIds.Add(itemPropertyId);
//                            item.PropertyValues.Add(defaultValue);
//                        }
//                    }
//                }

//                public void RemoveItemProperty(Byte itemPropertyId)
//                {
//                    Load();
//                    foreach (ItemData item in _items)
//                    {
//                        for (Int32 i = 0; i < item.PropertyIds.Count; i++)
//                        {
//                            if (item.PropertyIds[i] == itemPropertyId)
//                            {
//                                item.PropertyIds.RemoveAt(i);
//                                item.PropertyValues.RemoveAt(i);
//                                break;
//                            }
//                        }
//                    }
//                }

//                public IEnumerable<IItem> GetItems()
//                {
//                    Load();
//                    foreach (ItemData item in _items)
//                    {
//                        yield return new Item(_itemProperties, item);
//                    }
//                }

//                public void SetItems(IEnumerable<IItem> items)
//                {
//                    LoadRequired = false;
//                    if (_items == null)
//                    {
//                        _items = new List<ItemData>();
//                    }
//                    else
//                    {
//                        _items.Clear();
//                    }
//                    List<ItemProperty> itemProperties = new List<ItemProperty>(_itemProperties.GetItemProperties());
//                    foreach (IItem item in items)
//                    {
//                        _items.Add(new ItemData(itemProperties, item));
//                    }
//                }

//                protected override void Load()
//                {
//                    _itemProperties.ItemPropertyExists("TriggerLoad");
//                    base.Load();
//                }

//                public override void LoadData(ExtendedBinaryReader reader)
//                {
//                    // Read Items
//                    UInt32 itemCount = reader.ReadUInt32();
//                    for (UInt32 i = 0; i < itemCount; i++)
//                    {
//                        ItemData item = new ItemData();
//                        Byte itemPropertyCount = reader.ReadByte();
//                        for (Byte j = 0; j < itemPropertyCount; j++)
//                        {
//                            Byte itemPropertyId = reader.ReadByte();
//                            item.PropertyIds.Add(itemPropertyId);
//                            item.PropertyValues.Add(ReadValue(reader, _itemProperties[itemPropertyId].Type));
//                        }
//                        _items.Add(item);
//                    }
//                }

//                public override void WriteData(ExtendedBinaryWriter writer)
//                {
//                    writer.Write((UInt32)_items.Count);
//                    foreach (ItemData item in _items)
//                    {
//                        writer.Write((Byte)item.PropertyIds.Count);
//                        for (Int32 i = 0; i < item.PropertyIds.Count; i++)
//                        {
//                            writer.Write(item.PropertyIds[i]);
//                            WriteValue(
//                                writer,
//                                _itemProperties[item.PropertyIds[i]].Type,
//                                item.PropertyValues[i]);
//                        }
//                    }
//                }
//            }

//            private abstract class ContentSubSection
//            {
//                private ExtendedBinaryReader _fileReader;
//                private Int64 _startPosition;
//                private ExtendedBinaryReader _decryptedContentReader;
//                private Byte[] _aes256Key;
//                private Byte[] _aes256Salt;
//                private EncryptionType _encryptionType;
//                private Byte[] _bytesToWrite;

//                /// <summary>
//                /// Creates a new empty Section of a given type.
//                /// </summary>
//                /// <param name="type">The Section type.</param>
//                /// <param name="aes256Key">The key (password) to be used for AES encryption, if this section should be
//                /// encrypted in this way.</param>
//                public ContentSubSection(Byte[] aes256Key, Byte[] aes256Salt, EncryptionType encryptionType)
//                {
//                    _aes256Key = aes256Key;
//                    _aes256Salt = aes256Salt;
//                    _encryptionType = encryptionType;
//                }

//                /// <summary>
//                /// Creates a new Section by reading the expected data from a file.
//                /// </summary>
//                /// <param name="fileReader">An ExtendedBinaryReader reading from a stream to the underlying file.</param>
//                /// <param name="startPosition">The position within the underlying stream of the beginning of the section
//                /// to be read.</param>
//                /// <param name="aes256Key">The key (password) to be used for AES encryption/decryption, if this section is/
//                /// should be encrypted in this way.</param>
//                public ContentSubSection(
//                    ExtendedBinaryReader fileReader, 
//                    Int64 startPosition, 
//                    Byte[] aes256Key, 
//                    Byte[] aes256Salt,
//                    EncryptionType encryptionType)
//                {
//                    _aes256Key = aes256Key;
//                    _aes256Salt = aes256Salt;
//                    _encryptionType = encryptionType;
//                    _fileReader = fileReader;
//                    _startPosition = startPosition;
//                    //_itemProperties = new Dictionary<Byte, ItemProperty>();
//                    LoadRequired = true;
//                }

//                public EncryptionType EncryptionType { 
//                    get { return _encryptionType; }
//                    set {
//                        if (_encryptionType != value) {
//                            Load();

//                            _encryptionType = value;
//                        }
//                    }
//                }
//                public Boolean LoadRequired { get; protected set; }

//                protected virtual void Load()
//                {
//                    if (LoadRequired)
//                    {
//                        _fileReader.BaseStream.Position = _startPosition;
//                        _decryptedContentReader = LoadBytes(_fileReader, EncryptionType, _aes256Key, _aes256Salt);
//                        try
//                        {
//                            LoadData(GetReader(_fileReader));
//                        }
//                        finally
//                        {
//                            CloseReader();
//                        }
//                        LoadRequired = false;
//                    }
//                }

//                public abstract void LoadData(ExtendedBinaryReader reader);

//                public UInt32 GetSizeToWrite()
//                {
//                    CreateBytesToWrite();
//                    return (UInt32)_bytesToWrite.Length;
//                }

//                public void Write(ExtendedBinaryWriter fileWriter)
//                {
//                    Load();
//                    CreateBytesToWrite();
//                    fileWriter.Write(_bytesToWrite);
//                }

//                private void CreateBytesToWrite()
//                {
//                    Load();
//                    if (_bytesToWrite == null)
//                    {
//                        using (MemoryStream memoryStream = new MemoryStream())
//                        {
//                            using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(memoryStream))
//                            {
//                                WriteData(writer);
//                                writer.Flush();
//                                _bytesToWrite = GetBytes(memoryStream, EncryptionType, _aes256Key, _aes256Salt);
//                            }
//                        }
//                    }
//                }

//                public abstract void WriteData(ExtendedBinaryWriter writer);

//                private static Byte[] GetBytes(MemoryStream memoryStream, EncryptionType encryptionType, Byte[] aes256Key, Byte[] aes256Salt)
//                {
//                    StartTiming("GetContentBytes");
//                    Byte[] returnValue = null;
//                    switch (encryptionType)
//                    {
//                        case EncryptionType.Aes256:
//                            if (aes256Key == null)
//                            {
//                                throw new Exception("TODO");
//                            }
//                            StartTiming("ComputeHash");
//                            Byte[] hash = SHA256Managed.Create().ComputeHash(memoryStream.ToArray());
//                            StopTiming("ComputeHash");
//                            Byte[] encryptedBytes = null;
//                            using (AesManaged aes = new AesManaged())
//                            {
//                                aes.Padding = PaddingMode.PKCS7;
//                                ICryptoTransform encryptor = aes.CreateEncryptor(aes256Key, aes256Salt);
//                                using (MemoryStream encryptedMemoryStream = new MemoryStream())
//                                {
//                                    using (CryptoStream cryptoStream =
//                                        new CryptoStream(encryptedMemoryStream, encryptor, CryptoStreamMode.Write))
//                                    {
//                                        using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(cryptoStream))
//                                        {
//                                            StartTiming("Encrypt");
//                                            writer.WriteByteArrayAsSingleValue(hash);
//                                            memoryStream.Position = 0;
//                                            memoryStream.WriteTo(cryptoStream);
//                                            cryptoStream.FlushFinalBlock();
//                                            encryptedBytes = encryptedMemoryStream.ToArray();
//                                            StopTiming("Encrypt");
//                                        }
//                                    }
//                                }
//                            }
//                            using (MemoryStream contentBytesStream = new MemoryStream())
//                            {
//                                using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(contentBytesStream))
//                                {
//                                    StartTiming("StoreEncryptedBytes");
//                                    writer.WriteByteArrayAsSingleValue(encryptedBytes);
//                                    returnValue = contentBytesStream.ToArray();
//                                    StopTiming("StoreEncryptedBytes");
//                                }
//                            }
//                            break;
//                        default:
//                            returnValue = memoryStream.ToArray();
//                            break;
//                    }
//                    StopTiming("GetContentBytes");
//                    return returnValue;
//                }

//                private ExtendedBinaryReader GetReader(ExtendedBinaryReader fileReader)
//                {
//                    if (_decryptedContentReader != null)
//                    {
//                        return _decryptedContentReader;
//                    }
//                    return fileReader;
//                }

//                private void CloseReader()
//                {
//                    if (_decryptedContentReader != null)
//                    {
//                        _decryptedContentReader.Close();
//                    }
//                }

//                public ExtendedBinaryReader LoadBytes(ExtendedBinaryReader fileReader, EncryptionType encryptionType, Byte[] aes256Key, Byte[] aes256Salt)
//                {
//                    ExtendedBinaryReader returnValue = null;
//                    StartTiming("LoadContentBytes");
//                    if (encryptionType != EncryptionType.None)
//                    {
//                        switch (encryptionType)
//                        {
//                            case EncryptionType.Aes256:
//                                if (aes256Key == null)
//                                {
//                                    throw new Exception("TODO");
//                                }
//                                Byte[] encryptedContent = fileReader.ReadByteArray();
//                                Byte[] decryptedContent = null;
//                                Byte[] hash = null;
//                                using (AesManaged aes = new AesManaged())
//                                {
//                                    aes.Padding = PaddingMode.PKCS7;
//                                    ICryptoTransform decryptor = aes.CreateDecryptor(aes256Key, aes256Salt);
//                                    using (MemoryStream memoryStream = new MemoryStream())
//                                    {

//                                        using (CryptoStream cryptoStream =
//                                            new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Write))
//                                        {
//                                            using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(cryptoStream))
//                                            {
//                                                writer.Write(encryptedContent);
//                                                cryptoStream.FlushFinalBlock();
//                                                decryptedContent = memoryStream.ToArray();
//                                            }
//                                        }
//                                    }
//                                }
//                                Byte[] bytesRead = null;
//                                using (MemoryStream memoryStream = new MemoryStream(decryptedContent))
//                                {
//                                    using (ExtendedBinaryReader reader = new ExtendedBinaryReader(memoryStream))
//                                    {
//                                        memoryStream.Position = 0;
//                                        hash = reader.ReadByteArray();
//                                        //_contentBytesRead = reader.ReadByteArray();
//                                        bytesRead =
//                                            reader.ReadBytes((Int32)(memoryStream.Length - memoryStream.Position));
//                                    }
//                                }
//                                if (!hash.SequenceEqual(SHA256Managed.Create().ComputeHash(bytesRead)))
//                                {
//                                    throw new Exception("TODO");
//                                }
//                                returnValue = new ExtendedBinaryReader(new MemoryStream(bytesRead));
//                                break;
//                        }
//                    }
//                    StopTiming("LoadContentBytes");
//                    return returnValue;
//                }
//            }

//            #endregion

//            #region ItemProperty Class

//            /// <summary>
//            /// The ItemProperty Class represents the definition of an item property.
//            /// </summary>
//            private class ItemProperty
//            {
//                #region Constructor

//                /// <summary>
//                /// Creates a new ItemProperty.
//                /// </summary>
//                public ItemProperty(Byte id, String name, ItemPropertyType type)
//                {
//                    Id = id;
//                    Name = name;
//                    Type = type;
//                }

//                #endregion

//                #region Public Properties

//                /// <summary>
//                /// The ID of this item property.
//                /// </summary>
//                public Byte Id { get; private set; }

//                /// <summary>
//                /// The name of this item property.
//                /// </summary>
//                public String Name { get; set; }

//                /// <summary>
//                /// The type of this item property.
//                /// </summary>
//                public ItemPropertyType Type { get; private set; }

//                #endregion
//            }

//            #endregion

//            #region ItemData Class

//            /// <summary>
//            /// The ItemData class is a container for all item property values that pertain to a particular item.  It 
//            /// is used to store items that have been read from/written to the GalleriaBinaryFile.
//            /// </summary>
//            private class ItemData
//            {
//                #region Constructors

//                /// <summary>
//                /// Creates a new empty ItemData.
//                /// </summary>
//                public ItemData()
//                {
//                    PropertyIds = new List<Byte>();
//                    PropertyValues = new List<Object>();
//                }

//                /// <summary>
//                /// Creates an ItemData and fills it with the values for a given set of item properties from a 
//                /// particular IItem.
//                /// </summary>
//                public ItemData(IEnumerable<ItemProperty> itemProperties, IItem item)
//                    : this()
//                {
//                    foreach (ItemProperty itemProperty in itemProperties)
//                    {
//                        Object value = item.GetItemPropertyValue(itemProperty.Name);
//                        // To save space in the file, we don't bother storing values that are null or correspond to
//                        // the default for an item property's type.
//                        if ((value != null) &&
//                            (value != Section.GetDefaultValue(itemProperty.Type)))
//                        {
//                            PropertyIds.Add(itemProperty.Id);
//                            PropertyValues.Add(value);
//                        }
//                    }
//                }

//                #endregion

//                #region Properties

//                /// <summary>
//                /// The IDs of the item properties for which this ItemData instance holds values.
//                /// </summary>
//                public List<Byte> PropertyIds { get; private set; }
//                /// <summary>
//                /// The item property values that correspond to the IDs in PropertyIds, in the same order.
//                /// </summary>
//                public List<Object> PropertyValues { get; private set; }

//                #endregion
//            }

//            #endregion

//            #region Item Class

//            /// <summary>
//            /// The Item class implements IItem for the purpose of reading items out of a Galleria Binary File.
//            /// </summary>
//            private class Item : IItem
//            {
//                #region Private Fields

//                /// <summary>
//                /// A dictionary mapping item property values to item property names.
//                /// </summary>
//                Dictionary<String, Object> _propertyValues = new Dictionary<String, Object>();

//                #endregion

//                #region Constructor

//                /// <summary>
//                /// Creates a new Item from a set of item properties and an ItemData instance.
//                /// </summary>
//                /// <param name="itemProperties">A dictionary mapping item properties to their IDs.</param>
//                /// <param name="itemData">The data to expose through the new Item.</param>
//                public Item(ItemProperties itemProperties, ItemData itemData)
//                {
//                    for (Int32 i = 0; i < itemData.PropertyIds.Count; i++)
//                    {
//                        _propertyValues[itemProperties[itemData.PropertyIds[i]].Name] = itemData.PropertyValues[i];
//                    }
//                }

//                #endregion

//                #region IItem Members

//                public Object GetItemPropertyValue(String itemPropertyName)
//                {
//                    Object returnValue;
//                    _propertyValues.TryGetValue(itemPropertyName, out returnValue);
//                    return returnValue;
//                }

//                #endregion
//            }

//            #endregion

//            #endregion

//            #region Public Properties

//            /// <summary>
//            /// The size of the section header in the underlying file, including any extra space, or 0 if the section 
//            /// has never been written to the file.  Note that this isn't necessarily the size the section header will
//            /// be when it is next written.
//            /// </summary>
//            public UInt32 HeaderSize { get; private set; }

//            /// <summary>
//            /// The section type.
//            /// </summary>
//            public UInt16 Type { get; private set; }

//            /// <summary>
//            /// The size of the section content in the underlying file, including any extra space, or 0 if the section
//            /// has never been written to the file.  Note that this isn't necessarily the size the content will be 
//            /// when it is next written.
//            /// </summary>
//            public UInt32 ContentSize { get; private set; }

//            public Int64 NewSectionSize { get; set; }

//            /// <summary>
//            /// The type of encryption to use when writing this section's content to the file.
//            /// </summary>
//            public EncryptionType EncryptionType { get; private set; }

//            /// <summary>
//            /// The start position of this section in the underlying file, or 0 if the section has never been written
//            /// to the file.
//            /// </summary>
//            public Int64 StartPosition { get; private set; }

//            /// <summary>
//            /// The start position in the underlying file that should be used for this section the next time Write()
//            /// is called.  It is the responsibility of the object consumer (i.e. GalleriaBinaryFile.Save()) to set
//            /// this value correctly before calling Write().
//            /// </summary>
//            public Int64 NewStartPosition { get; set; }

//            /// <summary>
//            /// Whether this section has any unsaved changes.  If this is false, the GalleriaBinaryFile's Save()
//            /// method can either ignore this section altogether, if it is still in the correct location, or move its
//            /// bytes within the file without parsing it.  If this is true, the section must be written to the
//            /// correct location in the file using Write().
//            /// </summary>
//            public Boolean HasChanges { get; private set; }

//            /// <summary>
//            /// Whether this section is new, i.e. whether it has never been written to the file.
//            /// </summary>
//            public Boolean IsNew
//            {
//                get { return StartPosition == 0; }
//            }

//            public Boolean IsRemoved
//            {
//                get { return Type == _removedSectionType; }
//            }

//            public ItemPropertyType ItemGroupKeyType { get; private set; }

//            #endregion

//            #region Public Methods

//            public void SetEncryptionType(EncryptionType encryptionType)
//            {
//                if (EncryptionType != encryptionType)
//                {
//                    EncryptionType = encryptionType;
//                    _itemProperties.EncryptionType = encryptionType;
//                    foreach (ItemGroup itemGroup in _itemGroups.Values)
//                    {
//                        itemGroup.EncryptionType = encryptionType;
//                    }
//                    HasChanges = true;
//                }
//            }

//            /// <summary>
//            /// Gets the number of bytes this section will take up on disk when it is next written.
//            /// </summary>
//            public UInt32 GetNewSectionWrittenSize()
//            {
//                return
//                    GetNewHeaderSize() +
//                    GetNewContentSize();

//            }

//            public UInt32 GetTargetEmptySpaceSize()
//            {
//                return Math.Min(_maxTargetEmptySpace, Math.Max(_minTargetEmptySpace, GetNewContentSize()));
//            }

//            public void Remove()
//            {
//                Type = _removedSectionType;
//                HasChanges = true;
//            }

//            public Boolean ItemPropertyExists(String itemPropertyName)
//            {
//                return _itemProperties.ItemPropertyExists(itemPropertyName);
//            }

//            public ItemPropertyType GetItemPropertyType(String itemPropertyName)
//            {
//                return _itemProperties.GetItemPropertyType(itemPropertyName);
//            }

//            public void AddItemProperty(
//                String itemPropertyName,
//                ItemPropertyType itemPropertyType,
//                Object defaultValue)
//            {
//                Byte id = _itemProperties.AddItemProperty(itemPropertyName, itemPropertyType, defaultValue);
//                foreach (ItemGroup itemGroup in _itemGroups.Values)
//                {
//                    itemGroup.AddItemProperty(id, itemPropertyType, defaultValue);
//                }
//                HasChanges = true;
//            }

//            public void RemoveItemProperty(String itemPropertyName)
//            {
//                Byte id = _itemProperties.GetItemPropertyByName(itemPropertyName).Id;
//                foreach (ItemGroup itemGroup in _itemGroups.Values)
//                {
//                    itemGroup.RemoveItemProperty(id);
//                }
//                _itemProperties.RemoveItemProperty(id);
//                HasChanges = true;
//            }

//            public void RenameItemProperty(String itemPropertyName, String newItemPropertyName)
//            {
//                _itemProperties.RenameItemProperty(itemPropertyName, newItemPropertyName);
//                HasChanges = true;
//            }

//            public IEnumerable<IItem> GetItems()
//            {
//                if (ItemGroupKeyType != ItemPropertyType.Deleted)
//                {
//                    throw new InvalidOperationException("TODO");
//                }
//                return _itemGroups[_dummyItemGroupKey].GetItems();
//            }

//            public IEnumerable<IItem> GetItems(Object itemGroupKey)
//            {
//                if (ItemGroupKeyType == ItemPropertyType.Deleted)
//                {
//                    throw new InvalidOperationException("TODO");
//                }
//                if (_itemGroups.ContainsKey(itemGroupKey))
//                {
//                    return _itemGroups[itemGroupKey].GetItems();
//                }
//                return new List<IItem>();
//            }

//            public void SetItems(IEnumerable<IItem> items)
//            {
//                if (ItemGroupKeyType != ItemPropertyType.Deleted)
//                {
//                    throw new InvalidOperationException("TODO");
//                }
//                _itemGroups[_dummyItemGroupKey].SetItems(items);
//                HasChanges = true;
//            }

//            public void SetItems(Object itemGroupKey, IEnumerable<IItem> items)
//            {
//                if (ItemGroupKeyType == ItemPropertyType.Deleted)
//                {
//                    throw new InvalidOperationException("TODO");
//                }
//                if (!_itemGroups.ContainsKey(itemGroupKey))
//                {
//                    _itemGroups[itemGroupKey] = new ItemGroup(_aes256Key, _aes256Salt, _itemProperties, EncryptionType);
//                }
//                _itemGroups[itemGroupKey].SetItems(items);
//                HasChanges = true;
//            }

//            public void Write(ExtendedBinaryWriter fileWriter)
//            {
//                if (IsNew || HasChanges)
//                {
//                    fileWriter.BaseStream.Position = NewStartPosition;
//                    HeaderSize = GetNewHeaderSize();
//                    fileWriter.Write(HeaderSize);
//                    fileWriter.Write(Type);
//                    if (!IsRemoved)
//                    {
//                        ContentSize = (UInt32)(NewSectionSize - HeaderSize);
//                        fileWriter.Write(ContentSize);
//                        fileWriter.Write((Byte)EncryptionType);
//                        switch (EncryptionType)
//                        {
//                            case EncryptionType.Aes256:
//                                fileWriter.WriteByteArrayAsSingleValue(_aes256Salt);
//                                break;
//                        }
//                        fileWriter.Write((Byte)ItemGroupKeyType);
//                        if (ItemGroupKeyType != ItemPropertyType.Deleted)
//                        {
//                            fileWriter.Write((Byte)_itemGroups.Count);
//                            UInt32 relativeStartPosition = _itemProperties.GetSizeToWrite();
//                            foreach (Object key in _itemGroups.Keys)
//                            {
//                                WriteValue(fileWriter, ItemGroupKeyType, key);
//                                fileWriter.Write(relativeStartPosition);
//                                relativeStartPosition += _itemGroups[key].GetSizeToWrite();
//                            }
//                        }
//                        else
//                        {
//                            fileWriter.Write((UInt32)_itemProperties.GetSizeToWrite());
//                        }
//                        _itemProperties.Write(fileWriter);
//                        foreach (Object key in _itemGroups.Keys)
//                        {
//                            _itemGroups[key].Write(fileWriter);
//                        }
//                    }
//                }
//                StartPosition = NewStartPosition;
//            }

//            public static Object GetDefaultValue(ItemPropertyType itemPropertyType)
//            {
//                switch (itemPropertyType)
//                {
//                    case ItemPropertyType.Boolean:
//                        return new Boolean();
//                    case ItemPropertyType.Byte:
//                        return new Byte();
//                    //case ItemPropertyType.Char:
//                    //    return new Char();
//                    case ItemPropertyType.DateTime:
//                        return new DateTime();
//                    case ItemPropertyType.Decimal:
//                        return new Decimal();
//                    case ItemPropertyType.Double:
//                        return new Double();
//                    case ItemPropertyType.Int32:
//                        return new Int32();
//                    case ItemPropertyType.Int64:
//                        return new Int64();
//                    case ItemPropertyType.SByte:
//                        return new SByte();
//                    case ItemPropertyType.Int16:
//                        return new Int16();
//                    case ItemPropertyType.Single:
//                        return new Single();
//                    case ItemPropertyType.UInt32:
//                        return new UInt32();
//                    case ItemPropertyType.UInt64:
//                        return new UInt64();
//                    case ItemPropertyType.UInt16:
//                        return new UInt16();
//                    case ItemPropertyType.String:
//                    case ItemPropertyType.NullableBoolean:
//                    case ItemPropertyType.NullableByte:
//                    //case ItemPropertyType.NullableChar:
//                    case ItemPropertyType.NullableDateTime:
//                    case ItemPropertyType.NullableDecimal:
//                    case ItemPropertyType.NullableDouble:
//                    case ItemPropertyType.NullableInt32:
//                    case ItemPropertyType.NullableInt64:
//                    case ItemPropertyType.NullableSByte:
//                    case ItemPropertyType.NullableInt16:
//                    case ItemPropertyType.NullableSingle:
//                    case ItemPropertyType.NullableUInt32:
//                    case ItemPropertyType.NullableUInt64:
//                    case ItemPropertyType.NullableUInt16:
//                    case ItemPropertyType.BooleanArray:
//                    case ItemPropertyType.ByteArray:
//                    //case ItemPropertyType.CharArray:
//                    case ItemPropertyType.DecimalArray:
//                    case ItemPropertyType.DoubleArray:
//                    case ItemPropertyType.Int32Array:
//                    case ItemPropertyType.Int64Array:
//                    case ItemPropertyType.SByteArray:
//                    case ItemPropertyType.Int16Array:
//                    case ItemPropertyType.SingleArray:
//                    case ItemPropertyType.UInt32Array:
//                    case ItemPropertyType.UInt64Array:
//                    case ItemPropertyType.UInt16Array:
//                    default:
//                        return null;
//                }
//            }

//            #endregion

//            #region Private Methods

//            private static Object ReadValue(ExtendedBinaryReader reader, ItemPropertyType itemPropertyType)
//            {
//                switch (itemPropertyType)
//                {
//                    case ItemPropertyType.Boolean:
//                    case ItemPropertyType.NullableBoolean:
//                        return reader.ReadBoolean();
//                    case ItemPropertyType.Byte:
//                    case ItemPropertyType.NullableByte:
//                        return reader.ReadByte();
//                    //case ItemPropertyType.Char:
//                    //case ItemPropertyType.NullableChar:
//                    //    return reader.ReadChar();
//                    case ItemPropertyType.DateTime:
//                    case ItemPropertyType.NullableDateTime:
//                        return reader.ReadDateTime();
//                    case ItemPropertyType.Decimal:
//                    case ItemPropertyType.NullableDecimal:
//                        return reader.ReadDecimal();
//                    case ItemPropertyType.Double:
//                    case ItemPropertyType.NullableDouble:
//                        return reader.ReadDouble();
//                    case ItemPropertyType.Int32:
//                    case ItemPropertyType.NullableInt32:
//                        return reader.ReadInt32();
//                    case ItemPropertyType.Int64:
//                    case ItemPropertyType.NullableInt64:
//                        return reader.ReadInt64();
//                    case ItemPropertyType.SByte:
//                    case ItemPropertyType.NullableSByte:
//                        return reader.ReadSByte();
//                    case ItemPropertyType.Int16:
//                    case ItemPropertyType.NullableInt16:
//                        return reader.ReadInt16();
//                    case ItemPropertyType.Single:
//                    case ItemPropertyType.NullableSingle:
//                        return reader.ReadSingle();
//                    case ItemPropertyType.String:
//                        return reader.ReadString();
//                    case ItemPropertyType.UInt32:
//                    case ItemPropertyType.NullableUInt32:
//                        return reader.ReadUInt32();
//                    case ItemPropertyType.UInt64:
//                    case ItemPropertyType.NullableUInt64:
//                        return reader.ReadUInt64();
//                    case ItemPropertyType.UInt16:
//                    case ItemPropertyType.NullableUInt16:
//                        return reader.ReadUInt16();
//                    case ItemPropertyType.BooleanArray:
//                        return reader.ReadBooleanArray();
//                    case ItemPropertyType.ByteArray:
//                        return reader.ReadByteArray();
//                    //case ItemPropertyType.CharArray:
//                    //    return reader.ReadCharArray();
//                    case ItemPropertyType.DecimalArray:
//                        return reader.ReadDecimalArray();
//                    case ItemPropertyType.DoubleArray:
//                        return reader.ReadDoubleArray();
//                    case ItemPropertyType.Int32Array:
//                        return reader.ReadInt32Array();
//                    case ItemPropertyType.Int64Array:
//                        return reader.ReadInt64Array();
//                    case ItemPropertyType.SByteArray:
//                        return reader.ReadSByteArray();
//                    case ItemPropertyType.Int16Array:
//                        return reader.ReadInt16Array();
//                    case ItemPropertyType.SingleArray:
//                        return reader.ReadSingleArray();
//                    case ItemPropertyType.UInt32Array:
//                        return reader.ReadUInt32Array();
//                    case ItemPropertyType.UInt64Array:
//                        return reader.ReadInt64Array();
//                    case ItemPropertyType.UInt16Array:
//                        return reader.ReadUInt16Array();
//                    default:
//                        return null;
//                }
//            }

//            /// <summary>
//            /// Gets the number of bytes this section's header will take up on disk when it is next written.
//            /// </summary>
//            private UInt32 GetNewHeaderSize()
//            {
//                UInt32 returnValue = HeaderSize;
//                if ((!IsRemoved) && (IsNew || HasChanges) && (!_headerSizeCalculated))
//                {
//                    returnValue = (UInt32)CalculateHeaderSize();
//                }
//                return returnValue;
//            }

//            private UInt32 CalculateHeaderSize()
//            {
//                UInt32 returnValue = 0;
//                using (MemoryStream memoryStream = new MemoryStream())
//                {
//                    using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(memoryStream))
//                    {
//                        writer.Write(HeaderSize);
//                        writer.Write(Type);
//                        if (!IsRemoved)
//                        {
//                            writer.Write(ContentSize);
//                            writer.Write((Byte)EncryptionType);
//                            switch (EncryptionType)
//                            {
//                                case EncryptionType.Aes256:
//                                    writer.WriteByteArrayAsSingleValue(_aes256Salt);
//                                    break;
//                            }
//                            writer.Write((Byte)ItemGroupKeyType);
//                            if (ItemGroupKeyType != ItemPropertyType.Deleted)
//                            {
//                                writer.Write((Byte)_itemGroups.Count);
//                                UInt32 relativeStartPosition = _itemProperties.GetSizeToWrite();
//                                foreach (Object key in _itemGroups.Keys)
//                                {
//                                    WriteValue(writer, ItemGroupKeyType, key);
//                                    writer.Write(relativeStartPosition);
//                                    relativeStartPosition += _itemGroups[key].GetSizeToWrite();
//                                }
//                            }
//                            else
//                            {
//                                writer.Write((UInt32)_itemProperties.GetSizeToWrite());
//                            }
//                            writer.Flush();
//                            returnValue = (UInt32)memoryStream.Length;
//                        }
//                    }
//                }
//                return returnValue;
//            }

//            /// <summary>
//            /// Gets the number of bytes this section's content will take up on disk when it is next written.
//            /// </summary>
//            /// <returns></returns>
//            private UInt32 GetNewContentSize()
//            {
//                UInt32 returnValue = ContentSize;
//                if ((!IsRemoved) && (IsNew || HasChanges))
//                {
//                    returnValue = 
//                        (UInt32)(_itemProperties.GetSizeToWrite() + _itemGroups.Values.Sum(g => g.GetSizeToWrite()));
//                }
//                return returnValue;
//            }

//            public static void WriteValue(
//                ExtendedBinaryWriter writer, 
//                ItemPropertyType itemPropertyType, 
//                Object value)
//            {
//                switch (itemPropertyType)
//                {
//                    case ItemPropertyType.Boolean:
//                        writer.Write((Boolean)value);
//                        break;
//                    case ItemPropertyType.Byte:
//                        writer.Write((Byte)value);
//                        break;
//                    //case ItemPropertyType.Char:
//                    //    writer.Write((Char)value);
//                    //    break;
//                    case ItemPropertyType.DateTime:
//                        writer.Write((DateTime)value);
//                        break;
//                    case ItemPropertyType.Decimal:
//                        writer.Write((Decimal)value);
//                        break;
//                    case ItemPropertyType.Double:
//                        writer.Write((Double)value);
//                        break;
//                    case ItemPropertyType.Int32:
//                        writer.Write((Int32)value);
//                        break;
//                    case ItemPropertyType.Int64:
//                        writer.Write((Int64)value);
//                        break;
//                    case ItemPropertyType.SByte:
//                        writer.Write((SByte)value);
//                        break;
//                    case ItemPropertyType.Int16:
//                        writer.Write((Int16)value);
//                        break;
//                    case ItemPropertyType.Single:
//                        writer.Write((Single)value);
//                        break;
//                    case ItemPropertyType.String:
//                        writer.Write((String)value);
//                        break;
//                    case ItemPropertyType.UInt32:
//                        writer.Write((UInt32)value);
//                        break;
//                    case ItemPropertyType.UInt64:
//                        writer.Write((UInt64)value);
//                        break;
//                    case ItemPropertyType.UInt16:
//                        writer.Write((UInt16)value);
//                        break;
//                    case ItemPropertyType.NullableBoolean:
//                        writer.Write(((Boolean?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableByte:
//                        writer.Write(((Byte?)value).Value);
//                        break;
//                    //case ItemPropertyType.NullableChar:
//                    //    writer.Write(((Char?)value).Value);
//                    //    break;
//                    case ItemPropertyType.NullableDateTime:
//                        writer.Write(((DateTime?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableDecimal:
//                        writer.Write(((Decimal?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableDouble:
//                        writer.Write(((Double?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableInt32:
//                        writer.Write(((Int32?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableInt64:
//                        writer.Write(((Int64?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableSByte:
//                        writer.Write(((SByte?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableInt16:
//                        writer.Write(((Int16?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableSingle:
//                        writer.Write(((Single?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableUInt32:
//                        writer.Write(((UInt32?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableUInt64:
//                        writer.Write(((UInt64?)value).Value);
//                        break;
//                    case ItemPropertyType.NullableUInt16:
//                        writer.Write(((UInt16?)value).Value);
//                        break;
//                    case ItemPropertyType.BooleanArray:
//                        writer.Write((Boolean[])value);
//                        break;
//                    case ItemPropertyType.ByteArray:
//                        writer.WriteByteArrayAsSingleValue((Byte[])value);
//                        break;
//                    //case ItemPropertyType.CharArray:
//                    //    writer.WriteCharArrayAsSingleValue((Char[])value);
//                    //    break;
//                    case ItemPropertyType.DecimalArray:
//                        writer.Write((Decimal[])value);
//                        break;
//                    case ItemPropertyType.DoubleArray:
//                        writer.Write((Double[])value);
//                        break;
//                    case ItemPropertyType.Int32Array:
//                        writer.Write((Int32[])value);
//                        break;
//                    case ItemPropertyType.Int64Array:
//                        writer.Write((Int64[])value);
//                        break;
//                    case ItemPropertyType.SByteArray:
//                        writer.Write((SByte[])value);
//                        break;
//                    case ItemPropertyType.Int16Array:
//                        writer.Write((Int16[])value);
//                        break;
//                    case ItemPropertyType.SingleArray:
//                        writer.Write((Single[])value);
//                        break;
//                    case ItemPropertyType.UInt32Array:
//                        writer.Write((UInt32[])value);
//                        break;
//                    case ItemPropertyType.UInt64Array:
//                        writer.Write((UInt64[])value);
//                        break;
//                    case ItemPropertyType.UInt16Array:
//                        writer.Write((UInt16[])value);
//                        break;
//                }
//            }

//            #endregion
//        }

//        #endregion

//        #endregion

//        #region Public Properties

//        /// <summary>
//        /// The type identifier for this file.
//        /// </summary>
//        public String TypeIdentifier
//        {
//            get { return _fileHeader.TypeIdentifier; }
//        }

//        /// <summary>
//        /// The format version for this file.
//        /// </summary>
//        public Byte FormatVersion
//        {
//            get { return _fileHeader.FormatVersion; }
//            set { _fileHeader.FormatVersion = value; }
//        }

//        #endregion

//        #region Public Methods

//        #region Section-Related Methods

//        /// <summary>
//        /// Determines whether a section with a particular type exists within the file.
//        /// </summary>
//        public Boolean SectionExists(UInt16 sectionType)
//        {
//            return GetSection(sectionType, false) != null;
//        }

//        /// <summary>
//        /// Creates a new empty section with a particular type within the file.
//        /// </summary>
//        public void CreateSection(UInt16 sectionType)
//        {
//            if (SectionExists(sectionType))
//            {
//                throw new InvalidOperationException("Section already exists");
//            }
//            _sections.Add(new Section(sectionType, ItemPropertyType.Deleted, _aes256Key));
//        }

//        public void CreateSection(UInt16 sectionType, ItemPropertyType itemGroupKeyType)
//        {
//            if (SectionExists(sectionType))
//            {
//                throw new InvalidOperationException("Section already exists");
//            }
//            _sections.Add(new Section(sectionType, itemGroupKeyType, _aes256Key));
//        }

//        /// <summary>
//        /// Removes the section with a particular type from the file, if it exists.
//        /// </summary>
//        public void RemoveSection(UInt16 sectionType)
//        {
//            Section section = GetSection(sectionType, false);
//            if (section != null)
//            {
//                if (section.IsNew)
//                {
//                    // We never got around to writing this section to the file, so we can just forget it ever
//                    // happened.
//                    _sections.RemoveAll(s => s.Type == sectionType);
//                }
//                else
//                {
//                    // Just mark the section as deleted and deal with the consequences when Save() is called.
//                    section.Remove();
//                }
//            }
//        }

//        /// <summary>
//        /// Enables AES 128 encryption on a particular section.
//        /// </summary>
//        public void EnableAes128Encryption(UInt16 sectionType)
//        {
//            GetSection(sectionType, true).SetEncryptionType(EncryptionType.Aes256);
//        }

//        /// <summary>
//        /// Disables encryption on a particular section.
//        /// </summary>
//        public void DisableEncryption(UInt16 sectionType)
//        {
//            GetSection(sectionType, true).SetEncryptionType(EncryptionType.None);
//        }

//        public ItemPropertyType GetSectionItemGroupKeyType(UInt16 sectionType)
//        {
//            return GetSection(sectionType, true).ItemGroupKeyType;
//        }

//        #endregion

//        #region Item Property-Related Methods

//        /// <summary>
//        /// Determines whether an item property with a particular name exists within a given section.
//        /// </summary>
//        public Boolean ItemPropertyExists(UInt16 sectionType, String itemPropertyName)
//        {
//            if (itemPropertyName == null)
//            {
//                throw new ArgumentNullException("itemPropertyName");
//            }
//            return GetSection(sectionType, true).ItemPropertyExists(itemPropertyName);
//        }

//        /// <summary>
//        /// Gets the item property type for a particular item property within a given section.
//        /// </summary>
//        public ItemPropertyType GetItemPropertyType(UInt16 sectionType, String itemPropertyName)
//        {
//            if (itemPropertyName == null)
//            {
//                throw new ArgumentNullException("itemPropertyName");
//            }
//            return GetSection(sectionType, true).GetItemPropertyType(itemPropertyName);
//        }

//        /// <summary>
//        /// Adds an item property to a particular section.
//        /// </summary>
//        /// <param name="sectionType">The type of the section to add the item property to.</param>
//        /// <param name="itemPropertyName">The name of the item property to add.</param>
//        /// <param name="itemPropertyType">The type of the item property to add.</param>
//        /// <param name="defaultValue">The default value to use when adding this item property to existing items within
//        /// the section.  If null, the default value for the item property type will be used.  For example, when 
//        /// creating an item property of type Int32, using a defaultValue of 1 will set the property's value for all
//        /// existing items to 1, while a defaultValue of either 0 or null will result in all existing items having a
//        /// value of 0.</param>
//        public void AddItemProperty(
//            UInt16 sectionType, 
//            String itemPropertyName, 
//            ItemPropertyType itemPropertyType, 
//            Object defaultValue)
//        {
//            if (itemPropertyName == null)
//            {
//                throw new ArgumentNullException("itemPropertyName");
//            }
//            GetSection(sectionType, true).AddItemProperty(itemPropertyName, itemPropertyType, defaultValue);
//        }

//        /// <summary>
//        /// Removes the item property with a particular name from a given section, if it exists.
//        /// </summary>
//        public void RemoveItemProperty(UInt16 sectionType, String itemPropertyName)
//        {
//            if (itemPropertyName == null)
//            {
//                throw new ArgumentNullException("itemPropertyName");
//            }
//            GetSection(sectionType, true).RemoveItemProperty(itemPropertyName);
//        }

//        /// <summary>
//        /// Renames a particular item property in a given section.
//        /// </summary>
//        public void RenameItemProperty(UInt16 sectionType, String itemPropertyName, String newItemPropertyName)
//        {
//            if (itemPropertyName == null)
//            {
//                throw new ArgumentNullException("itemPropertyName");
//            }
//            if (newItemPropertyName == null)
//            {
//                throw new ArgumentNullException("newItemPropertyName");
//            }
//            GetSection(sectionType, true).RenameItemProperty(itemPropertyName, newItemPropertyName);
//        }

//        #endregion

//        #region Item-Related Methods

//        /// <summary>
//        /// Returns all items currently within a particular section.
//        /// </summary>
//        public IEnumerable<IItem> GetItems(UInt16 sectionType)
//        {
//            return GetSection(sectionType, true).GetItems();
//        }

//        public IEnumerable<IItem> GetItems(UInt16 sectionType, Object itemGroupKey)
//        {
//            return GetSection(sectionType, true).GetItems(itemGroupKey);
//        }

//        /// <summary>
//        /// Sets the items within a particular section.  This method overwrites ALL current items with ALL items passed
//        /// in.  Because of how sections are stored, the entire contents of a section must be rewritten when even the
//        /// smallest change is made, so there is no point in incurring the extra overhead that would be involved in
//        /// providing methods to edit individual items.  It is expected that the layer built on top of this one, e.g.
//        /// a DAL, will provide methods for dealing with individual items in a way that can be optimized using that
//        /// layer's knowledge of what the information being stored actually means, what properties the API user might
//        /// want to perform lookups on, etc.
//        /// </summary>
//        public void SetItems(UInt16 sectionType, IEnumerable<IItem> items)
//        {
//            GetSection(sectionType, true).SetItems(items);
//        }

//        public void SetItems(UInt16 sectionType, Object itemGroupKey, IEnumerable<IItem> items)
//        {
//            GetSection(sectionType, true).SetItems(itemGroupKey, items);
//        }

//        #endregion

//        #region Save


//        public void Save()
//        {
//            StartTiming("Save");
//            _fileStream.Position = 0;
//            _fileHeader.Write(_fileWriter);
//            MakeRoom();
//            foreach (Section section in _sections)
//            {
//                if (section.NewStartPosition > 0)
//                {
//                    section.Write(_fileWriter);
//                }
//            }
//            _fileWriter.Flush();
//            if (_sections.Count > 0)
//            {
//                Section lastSection = _sections.Last();
//                _fileStream.SetLength(lastSection.StartPosition + lastSection.HeaderSize + lastSection.ContentSize);
//            }
//            else
//            {
//                _fileStream.SetLength(_fileHeader.Size);
//            }
//            _sections.Clear();
//            _fileStream.Position = 0;
//            Load(_fileHeader.TypeIdentifier);
//            StopTiming("Save");
//        }

//        #endregion

//        #region IDisposable Members

//        public void Dispose()
//        {
//            WriteTimers();
//            // TODO: Implement properly
//            if (_fileReader != null)
//            {
//                _fileReader.Close();
//            }
//            _fileWriter.Close();
//            _fileStream.Close();
//        }

//        #endregion

//        #endregion

//        #region Private Helper Methods

//        private void Load(String typeIdentifier)
//        {
//            _fileReader = new ExtendedBinaryReader(_fileStream);
//            Int64 fileStreamLength = _fileStream.Length;
//            _fileHeader = new FileHeader(_fileReader);
//            if (_fileHeader.TypeIdentifier != typeIdentifier)
//            {
//                // TODO
//                Console.WriteLine("doh!");
//            }
//            _fileStream.Position = _fileHeader.Size;
//            while (_fileStream.Position < fileStreamLength)
//            {
//                Section section = new Section(_fileReader, _fileStream.Position, _aes256Key);
//                _sections.Add(section);
//#if DEBUG
//                Console.WriteLine(String.Format("{0}: {1} bytes", section.Type, section.HeaderSize + section.ContentSize));
//#endif
//                _fileStream.Position = section.StartPosition + section.HeaderSize + section.ContentSize;
//            }
//        }

//        private Section GetSection(UInt16 sectionType, Boolean sectionMustExist)
//        {
//            if (sectionType == _removedSectionType)
//            {
//                throw new ArgumentException(
//                    String.Format("Section type {0} is reserved for internal use.", _removedSectionType));
//            }
//            // Could be optimized, but probably doesn't need to be, as there probably won't be that many sections.
//            Section returnValue = _sections.FirstOrDefault(s => s.Type == sectionType);
//            if ((returnValue == null) && sectionMustExist)
//            {
//                throw new InvalidOperationException("Section does not exist.");
//            }
//            return returnValue;
//        }

//        private void MakeRoom()
//        {
//            StartTiming("MakeRoom");
//            Int64 savePosition = _fileStream.Position;
//            Int64 nextStartPosition = _fileHeader.Size;
//            StartTiming("MakeRoom1");
//            // First, fix any sections that we don't have to move because they haven't changed and there's enough
//            // room before their current positions to write what will need to be written.
//            Boolean[] fixedPosition = new Boolean[_sections.Count];
//            Boolean[] fixedSize = new Boolean[_sections.Count];
//            Int64 nonNegotiableBytes = _fileHeader.Size;
//            for (Int32 i = 0; i < _sections.Count; i++)
//            {
//                if (_sections[i].IsNew)
//                {
//                    break;
//                }
//                if (_sections[i].HasChanges)
//                {
//                    nonNegotiableBytes += _sections[i].GetNewSectionWrittenSize();
//                }
//                else
//                {
//                    fixedSize[i] = true;
//                    if (_sections[i].StartPosition >= nonNegotiableBytes)
//                    {
//                        fixedPosition[i] = true;
//                        nonNegotiableBytes =
//                            _sections[i].StartPosition + _sections[i].HeaderSize + _sections[i].ContentSize;
//                    }
//                    else
//                    {
//                        nonNegotiableBytes += _sections[i].HeaderSize + _sections[i].ContentSize;
//                    }
//                }
//            }
//            List<Section> sectionsToArrange = new List<Section>();
//            Int64 startPosition = _fileHeader.Size;
//            for (Int32 i = 0; i < _sections.Count; i++)
//            {
//                if (fixedPosition[i])
//                {
//                    if (sectionsToArrange.Count > 0)
//                    {
//                        ArrangeSections(sectionsToArrange, startPosition, _sections[i].StartPosition - startPosition);
//                        sectionsToArrange.Clear();
//                    }
//                    startPosition = _sections[i].StartPosition + _sections[i].HeaderSize + _sections[i].ContentSize;
//                }
//                else
//                {
//                    sectionsToArrange.Add(_sections[i]);
//                }
//            }
//            if (sectionsToArrange.Count > 0)
//            {
//                ArrangeSections(sectionsToArrange, startPosition, null);
//            }
//            StopTiming("MakeRoom1");
//            StartTiming("MakeRoom2");
//            for (Int32 i = _sections.Count - 1; i >= 0; i--)
//            {
//                // Work through sections backwards, moving any that need to be moved.  A section needs to be moved if it
//                // exists in the file already, has no changes, but is in the wrong place relative to other sections that
//                // have changed.  Sections that have changes don't need to be moved at this point.  They'll be rewritten
//                // later.  By moving through the sections in reverse order, we ensure that we don't overwrite bytes we
//                // haven't read yet.
//                Section section = _sections[i];
//                if ((!section.IsNew) && 
//                    (!section.HasChanges) && 
//                    (section.NewStartPosition > section.StartPosition))
//                {
//                    _fileStream.Flush();
//                    _fileStream.Position = section.StartPosition;
//                    Byte[] buffer = new Byte[section.HeaderSize + section.ContentSize];
//                    StartTiming("Read");
//                    _fileStream.Read(buffer, 0, buffer.Length);
//                    StopTiming("Read");
//                    _fileStream.Position = section.NewStartPosition;
//                    StartTiming("Write");
//                    _fileStream.Write(buffer, 0, buffer.Length);
//                    StopTiming("Write");
//                }
//            }
//            _fileStream.Position = savePosition;
//            StopTiming("MakeRoom2");
//            StopTiming("MakeRoom");
//        }

//        private void ArrangeSections(List<Section> sections, Int64 startPosition, Int64? size)
//        {
//            StartTiming("ArrangeSections");
//            Boolean containsExistingSections = sections.FirstOrDefault(s => s.Type != 0) != null;
//            if (containsExistingSections)
//            {
//                sections.RemoveAll(s => s.Type == _removedSectionType);
//                Int64 nextStartPosition = startPosition;
//                if (size == null)
//                {
//                    for (Int32 i = 0; i < sections.Count; i++)
//                    {
//                        sections[i].NewStartPosition = nextStartPosition;
//                        sections[i].NewSectionSize = sections[i].GetNewSectionWrittenSize();
//                        if ((i < sections.Count - 1) && (sections[i].IsNew || sections[i].HasChanges))
//                        {
//                            sections[i].NewSectionSize += sections[i].GetTargetEmptySpaceSize();
//                        }
//                        nextStartPosition += sections[i].NewSectionSize;
//                    }
//                }
//                else
//                {
//                    Int64 availableSpace = size.Value - sections.Sum(s => s.GetNewSectionWrittenSize());
//                    for (Int32 i = 0; i < sections.Count; i++)
//                    {
//                        sections[i].NewStartPosition = nextStartPosition;
//                        Int64 extraSpace = 0;
//                        if (sections[i].IsNew || sections[i].HasChanges)
//                        {
//                            extraSpace = Math.Min(availableSpace, sections[i].GetTargetEmptySpaceSize());
//                        }
//                        sections[i].NewSectionSize = sections[i].GetNewSectionWrittenSize() + extraSpace;
//                        availableSpace -= extraSpace;
//                        if (i == sections.Count - 1)
//                        {
//                            sections[i].NewSectionSize += availableSpace;
//                        }
//                        nextStartPosition += sections[i].NewSectionSize;
//                    }
//                }
//            }
//            else
//            {
//                foreach (Section section in sections)
//                {
//                    section.NewStartPosition = section.StartPosition;
//                }
//                //if (size != null)
//                //{
//                //    sections[0].NewStartPosition = startPosition;
//                //    sections[0].NewSectionSize = size.Value;
//                //}
//            }
//            StopTiming("ArrangeSections");
//        }

//        #endregion
//    }
//}
