﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//  Created
// V8-25949 : N.Foster
//  Allowed Ids to be something other than Int32
// V8-26147 : L.Ineson
//  Added CustomAttributeData
// V8-26156 : N.Foster
//  Converted to use framework classes
// V8-26269 : A.Kuszyk
//  Added PlanogramPerformance, PlanogramPerformanceData and PlanogramPerformanceMetric.
// V8-26426 : A.Kuszyk
//  Add PlanogramAssortment related sections.
// V8-26573 : L.Luong
//  Added PlanogramValidationTemplate and related sections
// V8-26706 : L.Luong
//  Added PlanogramConsumerDecisionTree and related sections
// V8-26836 : L.Luong
//  Added PlanogramEventLog section
// V8-26891 : L.Ineson
//  Added Blocking
// V8-27058 : A.Probyn
//  Extended for PlanogramMetaDataImage
// V8-27647 : L.Luong 
//  Added PlanogramInventory
#endregion

#region Version History: CCM802
// V8-28996 : A.Silva
//      Added PlanogramSequence, PlanogramSequenceGroups and PlanogramSequenceGroupProducts.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
#endregion

#region Version History: CCM830
// V8-31550 : A.Probyn
//  Added new PlanogramAssortmentLocationBuddys and PlanogramAssortmentProductBuddys to the cache
// V8-31551 : A.Probyn
//  Added new PlanogramAssortmentInventoryRules to the cache
// V8-31819 : A.Silva
//  Added new PlanogramComparison, PlanogramComparisonField, PlanogramComparisonResult, 
//      PlanogramComparisonItem and PlanogramComparisonFieldValue to the cache.
// V8-32504 : A.Kuszyk.
//  Added PLanogramSequenceGroupSubGroup to cache.
#endregion

#endregion

using System;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Implementation.Items;

namespace Galleria.Framework.Planograms.Dal.Pog
{
    /// <summary>
    /// The DalCache is used to store DTO instances in memory so that they can be fetched/inserted/updated quickly and
    /// so that DAL operations can be commited to the underlying POG file only when a DalContext transaction is 
    /// committed.
    /// </summary>
    public class DalCache : Galleria.Framework.Dal.Gbf.DalCacheBase<DalCache>
    {
        #region Fields
        private SingleItemCache<PackageDto, PackageDtoItem> _package;
        private OneLayerItemCache<PlanogramDto, PlanogramDtoItem> _planograms;
        private TwoLayerItemCache<PlanogramFixtureItemDto, PlanogramFixtureItemDtoItem> _planogramFixtureItems;
        private TwoLayerItemCache<PlanogramFixtureDto, PlanogramFixtureDtoItem> _planogramFixtures;
        private TwoLayerItemCache<PlanogramFixtureAssemblyDto, PlanogramFixtureAssemblyDtoItem> _planogramFixtureAssemblies;
        private TwoLayerItemCache<PlanogramFixtureComponentDto, PlanogramFixtureComponentDtoItem> _planogramFixtureComponents;
        private TwoLayerItemCache<PlanogramAssemblyDto, PlanogramAssemblyDtoItem> _planogramAssemblies;
        private TwoLayerItemCache<PlanogramAssemblyComponentDto, PlanogramAssemblyComponentDtoItem> _planogramAssemblyComponents;
        private TwoLayerItemCache<PlanogramComponentDto, PlanogramComponentDtoItem> _planogramComponents;
        private TwoLayerItemCache<PlanogramSubComponentDto, PlanogramSubComponentDtoItem> _planogramSubComponents;
        private TwoLayerItemCache<PlanogramProductDto, PlanogramProductDtoItem> _planogramProducts;
        private TwoLayerItemCache<PlanogramPositionDto, PlanogramPositionDtoItem> _planogramPositions;
        private TwoLayerItemCache<PlanogramAnnotationDto, PlanogramAnnotationDtoItem> _planogramAnnotations;
        private TwoLayerItemCache<PlanogramImageDto, PlanogramImageDtoItem> _planogramImages;
        private TwoLayerItemCache<PlanogramMetadataImageDto, PlanogramMetadataImageDtoItem> _planogramMetadataImages;
        private TwoLayerItemCache<CustomAttributeDataDto, CustomAttributeDataDtoItem> _customAttributeData;
        private TwoLayerItemCache<PlanogramPerformanceDataDto, PlanogramPerformanceDataDtoItem> _planogramPerformanceDatas;
        private TwoLayerItemCache<PlanogramPerformanceMetricDto, PlanogramPerformanceMetricDtoItem> _planogramPerformanceMetrics;
        private TwoLayerItemCache<PlanogramPerformanceDto, PlanogramPerformanceDtoItem> _planogramPerformances;
        private TwoLayerItemCache<PlanogramAssortmentDto, PlanogramAssortmentDtoItem> _planogramAssortments;
        private TwoLayerItemCache<PlanogramAssortmentInventoryRuleDto, PlanogramAssortmentInventoryRuleDtoItem> _planogramAssortmentInventoryRules;
        private TwoLayerItemCache<PlanogramAssortmentLocalProductDto, PlanogramAssortmentLocalProductDtoItem> _planogramAssortmentLocalProducts;
        private TwoLayerItemCache<PlanogramAssortmentLocationBuddyDto, PlanogramAssortmentLocationBuddyDtoItem> _planogramAssortmentLocationBuddys;
        private TwoLayerItemCache<PlanogramAssortmentProductDto, PlanogramAssortmentProductDtoItem> _planogramAssortmentProducts;
        private TwoLayerItemCache<PlanogramAssortmentProductBuddyDto, PlanogramAssortmentProductBuddyDtoItem> _planogramAssortmentProductBuddys;
        private TwoLayerItemCache<PlanogramAssortmentRegionDto, PlanogramAssortmentRegionDtoItem> _planogramAssortmentRegions;
        private TwoLayerItemCache<PlanogramAssortmentRegionLocationDto, PlanogramAssortmentRegionLocationDtoItem> _planogramAssortmentRegionLocations;
        private TwoLayerItemCache<PlanogramAssortmentRegionProductDto, PlanogramAssortmentRegionProductDtoItem> _planogramAssortmentRegionProducts;
        private TwoLayerItemCache<PlanogramValidationTemplateDto, PlanogramValidationTemplateDtoItem> _planogramValidationTemplates;
        private TwoLayerItemCache<PlanogramValidationTemplateGroupDto, PlanogramValidationTemplateGroupDtoItem> _planogramValidationTemplateGroups;
        private TwoLayerItemCache<PlanogramValidationTemplateGroupMetricDto, PlanogramValidationTemplateGroupMetricDtoItem> _planogramValidationTemplateGroupMetrics;
        private TwoLayerItemCache<PlanogramConsumerDecisionTreeDto, PlanogramConsumerDecisionTreeDtoItem> _planogramConsumerDecisionTrees;
        private TwoLayerItemCache<PlanogramConsumerDecisionTreeLevelDto, PlanogramConsumerDecisionTreeLevelDtoItem> _planogramConsumerDecisionTreeLevels;
        private TwoLayerItemCache<PlanogramConsumerDecisionTreeNodeDto, PlanogramConsumerDecisionTreeNodeDtoItem> _planogramConsumerDecisionTreeNodes;
        private TwoLayerItemCache<PlanogramConsumerDecisionTreeNodeProductDto, PlanogramConsumerDecisionTreeNodeProductDtoItem> _planogramConsumerDecisionTreeNodeProducts;
        private TwoLayerItemCache<PlanogramEventLogDto, PlanogramEventLogDtoItem> _planogramEventLogs;
        private TwoLayerItemCache<PlanogramBlockingDto, PlanogramBlockingDtoItem> _planogramBlocking;
        private TwoLayerItemCache<PlanogramBlockingGroupDto, PlanogramBlockingGroupDtoItem> _planogramBlockingGroups;
        private TwoLayerItemCache<PlanogramBlockingDividerDto, PlanogramBlockingDividerDtoItem> _planogramBlockingDividers;
        private TwoLayerItemCache<PlanogramBlockingLocationDto, PlanogramBlockingLocationDtoItem> _planogramBlockingLocations;
        private readonly TwoLayerItemCache<PlanogramRenumberingStrategyDto, PlanogramRenumberingStrategyDtoItem> _planogramRenumberingStrategy;
        private TwoLayerItemCache<PlanogramInventoryDto, PlanogramInventoryDtoItem> _planogramInventory;
        private readonly TwoLayerItemCache<PlanogramSequenceDto, PlanogramSequenceDtoItem> _planogramSequences;
        private readonly TwoLayerItemCache<PlanogramSequenceGroupDto, PlanogramSequenceGroupDtoItem> _planogramSequenceGroups;
        private readonly TwoLayerItemCache<PlanogramSequenceGroupProductDto, PlanogramSequenceGroupProductDtoItem> _planogramSequenceGroupProducts;
        private readonly TwoLayerItemCache<PlanogramSequenceGroupSubGroupDto, PlanogramSequenceGroupSubGroupDtoItem> _planogramSequenceGroupSubGroups;
        private readonly TwoLayerItemCache<PlanogramComparisonDto, PlanogramComparisonDtoItem> _planogramComparisons;
        private readonly TwoLayerItemCache<PlanogramComparisonFieldDto, PlanogramComparisonFieldDtoItem> _planogramComparisonFields;
        private readonly TwoLayerItemCache<PlanogramComparisonResultDto, PlanogramComparisonResultDtoItem> _planogramComparisonResults;
        private readonly TwoLayerItemCache<PlanogramComparisonItemDto, PlanogramComparisonItemDtoItem> _planogramComparisonItems;
        private readonly TwoLayerItemCache<PlanogramComparisonFieldValueDto, PlanogramComparisonFieldValueDtoItem> _planogramComparisonFieldValues;
        private TwoLayerItemCache<ProductAttributeComparisonResultDto, ProductAttributeComparisonResultDtoItem> _productAttributeComparisonResults;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCache(DalFactory dalFactory, Boolean readOnly)
            : base(dalFactory, readOnly)
        {
            _package =
                CreateSingleItemCache<PackageDto, PackageDtoItem>(
                    (UInt16)SectionType.Package,
                    delegate(PackageDto dto)
                    {
                        return new PackageDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PackageDtoItem(item);
                    });
            _planograms =
                CreateOneLayerItemCache<PlanogramDto, PlanogramDtoItem>(
                    (UInt16)SectionType.Planograms,
                    delegate(PlanogramDto dto)
                    {
                        return new PlanogramDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramDtoItem(item);
                    });

            #region Fixturing & Products
            _planogramFixtureItems =
                CreateTwoLayerItemCache<PlanogramFixtureItemDto, PlanogramFixtureItemDtoItem>(
                    (UInt16)SectionType.PlanogramFixtureItems,
                    delegate(PlanogramFixtureItemDto dto)
                    {
                        return new PlanogramFixtureItemDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramFixtureItemDtoItem(item);
                    });
            _planogramFixtures =
                CreateTwoLayerItemCache<PlanogramFixtureDto, PlanogramFixtureDtoItem>(
                    (UInt16)SectionType.PlanogramFixtures,
                    delegate(PlanogramFixtureDto dto)
                    {
                        return new PlanogramFixtureDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramFixtureDtoItem(item);
                    });
            _planogramFixtureAssemblies =
                CreateTwoLayerItemCache<PlanogramFixtureAssemblyDto, PlanogramFixtureAssemblyDtoItem>(
                    (UInt16)SectionType.PlanogramFixtureAssemblies,
                    delegate(PlanogramFixtureAssemblyDto dto)
                    {
                        return new PlanogramFixtureAssemblyDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramFixtureAssemblyDtoItem(item);
                    });
            _planogramFixtureComponents =
                CreateTwoLayerItemCache<PlanogramFixtureComponentDto, PlanogramFixtureComponentDtoItem>(
                    (UInt16)SectionType.PlanogramFixtureComponents,
                    delegate(PlanogramFixtureComponentDto dto)
                    {
                        return new PlanogramFixtureComponentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramFixtureComponentDtoItem(item);
                    });
            _planogramAssemblies =
                CreateTwoLayerItemCache<PlanogramAssemblyDto, PlanogramAssemblyDtoItem>(
                    (UInt16)SectionType.PlanogramAssemblies,
                    delegate(PlanogramAssemblyDto dto)
                    {
                        return new PlanogramAssemblyDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssemblyDtoItem(item);
                    });
            _planogramAssemblyComponents =
                CreateTwoLayerItemCache<PlanogramAssemblyComponentDto, PlanogramAssemblyComponentDtoItem>(
                    (UInt16)SectionType.PlanogramAssemblyComponents,
                    delegate(PlanogramAssemblyComponentDto dto)
                    {
                        return new PlanogramAssemblyComponentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssemblyComponentDtoItem(item);
                    });
            _planogramComponents =
                CreateTwoLayerItemCache<PlanogramComponentDto, PlanogramComponentDtoItem>(
                    (UInt16)SectionType.PlanogramComponents,
                    delegate(PlanogramComponentDto dto)
                    {
                        return new PlanogramComponentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramComponentDtoItem(item);
                    });
            _planogramSubComponents =
                CreateTwoLayerItemCache<PlanogramSubComponentDto, PlanogramSubComponentDtoItem>(
                    (UInt16)SectionType.PlanogramSubComponents,
                    delegate(PlanogramSubComponentDto dto)
                    {
                        return new PlanogramSubComponentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramSubComponentDtoItem(item);
                    });
            _planogramProducts =
                CreateTwoLayerItemCache<PlanogramProductDto, PlanogramProductDtoItem>(
                    (UInt16)SectionType.PlanogramProducts,
                    delegate(PlanogramProductDto dto)
                    {
                        return new PlanogramProductDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramProductDtoItem(item);
                    });
            _planogramPositions =
                CreateTwoLayerItemCache<PlanogramPositionDto, PlanogramPositionDtoItem>(
                    (UInt16)SectionType.PlanogramPositions,
                    delegate(PlanogramPositionDto dto)
                    {
                        return new PlanogramPositionDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramPositionDtoItem(item);
                    });
            _planogramAnnotations =
                CreateTwoLayerItemCache<PlanogramAnnotationDto, PlanogramAnnotationDtoItem>(
                    (UInt16)SectionType.PlanogramAnnotations,
                    delegate(PlanogramAnnotationDto dto)
                    {
                        return new PlanogramAnnotationDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAnnotationDtoItem(item);
                    });
            _planogramImages =
                CreateTwoLayerItemCache<PlanogramImageDto, PlanogramImageDtoItem>(
                    (UInt16)SectionType.PlanogramImages,
                    delegate(PlanogramImageDto dto)
                    {
                        return new PlanogramImageDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramImageDtoItem(item);
                    });
            _planogramMetadataImages =
                CreateTwoLayerItemCache<PlanogramMetadataImageDto, PlanogramMetadataImageDtoItem>(
                    (UInt16)SectionType.PlanogramMetadataImages,
                    delegate(PlanogramMetadataImageDto dto)
                    {
                        return new PlanogramMetadataImageDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramMetadataImageDtoItem(item);
                    });
            _customAttributeData =
                CreateTwoLayerItemCache<CustomAttributeDataDto, CustomAttributeDataDtoItem>(
                    (UInt16)SectionType.CustomAttributeData,
                    delegate(CustomAttributeDataDto dto)
                    {
                        return new CustomAttributeDataDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new CustomAttributeDataDtoItem(item);
                    });
            #endregion

            #region Performance Data
            _planogramPerformanceDatas =
                CreateTwoLayerItemCache<PlanogramPerformanceDataDto, PlanogramPerformanceDataDtoItem>(
                    (UInt16)SectionType.PlanogramPerformanceDatas,
                    delegate(PlanogramPerformanceDataDto dto)
                    {
                        return new PlanogramPerformanceDataDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramPerformanceDataDtoItem(item);
                    });
            _planogramPerformanceMetrics =
                CreateTwoLayerItemCache<PlanogramPerformanceMetricDto, PlanogramPerformanceMetricDtoItem>(
                    (UInt16)SectionType.PlanogramPerformanceMetrics,
                    delegate(PlanogramPerformanceMetricDto dto)
                    {
                        return new PlanogramPerformanceMetricDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramPerformanceMetricDtoItem(item);
                    });
            _planogramPerformances =
                CreateTwoLayerItemCache<PlanogramPerformanceDto, PlanogramPerformanceDtoItem>(
                    (UInt16)SectionType.PlanogramPerformances,
                    delegate(PlanogramPerformanceDto dto)
                    {
                        return new PlanogramPerformanceDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramPerformanceDtoItem(item);
                    });
            #endregion

            #region Assortment
            _planogramAssortments =
                CreateTwoLayerItemCache<PlanogramAssortmentDto, PlanogramAssortmentDtoItem>(
                    (UInt16)SectionType.PlanogramAssortments,
                    delegate(PlanogramAssortmentDto dto)
                    {
                        return new PlanogramAssortmentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentDtoItem(item);
                    });
            _planogramAssortmentLocalProducts =
                CreateTwoLayerItemCache<PlanogramAssortmentLocalProductDto, PlanogramAssortmentLocalProductDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentLocalProducts,
                    delegate(PlanogramAssortmentLocalProductDto dto)
                    {
                        return new PlanogramAssortmentLocalProductDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentLocalProductDtoItem(item);
                    });
            _planogramAssortmentProducts =
                CreateTwoLayerItemCache<PlanogramAssortmentProductDto, PlanogramAssortmentProductDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentProducts,
                    delegate(PlanogramAssortmentProductDto dto)
                    {
                        return new PlanogramAssortmentProductDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentProductDtoItem(item);
                    });
            _planogramAssortmentRegions =
                CreateTwoLayerItemCache<PlanogramAssortmentRegionDto, PlanogramAssortmentRegionDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentRegions,
                    delegate(PlanogramAssortmentRegionDto dto)
                    {
                        return new PlanogramAssortmentRegionDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentRegionDtoItem(item);
                    });
            _planogramAssortmentRegionLocations =
                CreateTwoLayerItemCache<PlanogramAssortmentRegionLocationDto, PlanogramAssortmentRegionLocationDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentRegionLocations,
                    delegate(PlanogramAssortmentRegionLocationDto dto)
                    {
                        return new PlanogramAssortmentRegionLocationDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentRegionLocationDtoItem(item);
                    });
            _planogramAssortmentRegionProducts =
                CreateTwoLayerItemCache<PlanogramAssortmentRegionProductDto, PlanogramAssortmentRegionProductDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentRegionProducts,
                    delegate(PlanogramAssortmentRegionProductDto dto)
                    {
                        return new PlanogramAssortmentRegionProductDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentRegionProductDtoItem(item);
                    });
            _planogramAssortmentInventoryRules =
                CreateTwoLayerItemCache<PlanogramAssortmentInventoryRuleDto, PlanogramAssortmentInventoryRuleDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentInventoryRules,
                    delegate(PlanogramAssortmentInventoryRuleDto dto)
                    {
                        return new PlanogramAssortmentInventoryRuleDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentInventoryRuleDtoItem(item);
                    });
            _planogramAssortmentLocationBuddys =
                CreateTwoLayerItemCache<PlanogramAssortmentLocationBuddyDto, PlanogramAssortmentLocationBuddyDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentLocationBuddys,
                    delegate(PlanogramAssortmentLocationBuddyDto dto)
                    {
                        return new PlanogramAssortmentLocationBuddyDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentLocationBuddyDtoItem(item);
                    });
            _planogramAssortmentProductBuddys =
                CreateTwoLayerItemCache<PlanogramAssortmentProductBuddyDto, PlanogramAssortmentProductBuddyDtoItem>(
                    (UInt16)SectionType.PlanogramAssortmentProductBuddys,
                    delegate(PlanogramAssortmentProductBuddyDto dto)
                    {
                        return new PlanogramAssortmentProductBuddyDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramAssortmentProductBuddyDtoItem(item);
                    });
            #endregion

            #region Validation Template
            _planogramValidationTemplates =
                CreateTwoLayerItemCache<PlanogramValidationTemplateDto, PlanogramValidationTemplateDtoItem>(
                    (UInt16)SectionType.PlanogramValidationTemplates,
                    delegate(PlanogramValidationTemplateDto dto)
                    {
                        return new PlanogramValidationTemplateDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramValidationTemplateDtoItem(item);
                    });
            _planogramValidationTemplateGroups =
                CreateTwoLayerItemCache<PlanogramValidationTemplateGroupDto, PlanogramValidationTemplateGroupDtoItem>(
                    (UInt16)SectionType.PlanogramValidationTemplateGroups,
                    delegate(PlanogramValidationTemplateGroupDto dto)
                    {
                        return new PlanogramValidationTemplateGroupDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramValidationTemplateGroupDtoItem(item);
                    });
            _planogramValidationTemplateGroupMetrics =
                CreateTwoLayerItemCache<PlanogramValidationTemplateGroupMetricDto, PlanogramValidationTemplateGroupMetricDtoItem>(
                    (UInt16)SectionType.PlanogramValidationTemplateGroupMetrics,
                    delegate(PlanogramValidationTemplateGroupMetricDto dto)
                    {
                        return new PlanogramValidationTemplateGroupMetricDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramValidationTemplateGroupMetricDtoItem(item);
                    });
            #endregion

            #region CDT
            _planogramConsumerDecisionTrees =
                CreateTwoLayerItemCache<PlanogramConsumerDecisionTreeDto, PlanogramConsumerDecisionTreeDtoItem>(
                    (UInt16)SectionType.PlanogramConsumerDecisionTrees,
                    delegate(PlanogramConsumerDecisionTreeDto dto)
                    {
                        return new PlanogramConsumerDecisionTreeDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramConsumerDecisionTreeDtoItem(item);
                    });
            _planogramConsumerDecisionTreeLevels =
                CreateTwoLayerItemCache<PlanogramConsumerDecisionTreeLevelDto, PlanogramConsumerDecisionTreeLevelDtoItem>(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeLevels,
                    delegate(PlanogramConsumerDecisionTreeLevelDto dto)
                    {
                        return new PlanogramConsumerDecisionTreeLevelDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramConsumerDecisionTreeLevelDtoItem(item);
                    });
            _planogramConsumerDecisionTreeNodes =
                CreateTwoLayerItemCache<PlanogramConsumerDecisionTreeNodeDto, PlanogramConsumerDecisionTreeNodeDtoItem>(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodes,
                    delegate(PlanogramConsumerDecisionTreeNodeDto dto)
                    {
                        return new PlanogramConsumerDecisionTreeNodeDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramConsumerDecisionTreeNodeDtoItem(item);
                    });
            _planogramConsumerDecisionTreeNodeProducts =
                CreateTwoLayerItemCache<PlanogramConsumerDecisionTreeNodeProductDto, PlanogramConsumerDecisionTreeNodeProductDtoItem>(
                    (UInt16)SectionType.PlanogramConsumerDecisionTreeNodeProducts,
                    delegate(PlanogramConsumerDecisionTreeNodeProductDto dto)
                    {
                        return new PlanogramConsumerDecisionTreeNodeProductDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramConsumerDecisionTreeNodeProductDtoItem(item);
                    });
            #endregion

            #region Event log
            _planogramEventLogs =
                CreateTwoLayerItemCache<PlanogramEventLogDto, PlanogramEventLogDtoItem>(
                    (UInt16)SectionType.PlanogramEventLogs,
                    delegate(PlanogramEventLogDto dto)
                    {
                        return new PlanogramEventLogDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramEventLogDtoItem(item);
                    });
            #endregion

            #region Blocking

            _planogramBlocking =
               CreateTwoLayerItemCache<PlanogramBlockingDto, PlanogramBlockingDtoItem>(
                   (UInt16)SectionType.PlanogramBlocking,
                   delegate(PlanogramBlockingDto dto)
                   {
                       return new PlanogramBlockingDtoItem(dto);
                   },
                   delegate(GalleriaBinaryFile.IItem item)
                   {
                       return new PlanogramBlockingDtoItem(item);
                   });

            _planogramBlockingGroups =
                CreateTwoLayerItemCache<PlanogramBlockingGroupDto, PlanogramBlockingGroupDtoItem>(
                    (UInt16)SectionType.PlanogramBlockingGroups,
                    delegate(PlanogramBlockingGroupDto dto)
                    {
                        return new PlanogramBlockingGroupDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramBlockingGroupDtoItem(item);
                    });

            _planogramBlockingDividers =
                CreateTwoLayerItemCache<PlanogramBlockingDividerDto, PlanogramBlockingDividerDtoItem>(
                    (UInt16)SectionType.PlanogramBlockingDividers,
                    delegate(PlanogramBlockingDividerDto dto)
                    {
                        return new PlanogramBlockingDividerDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramBlockingDividerDtoItem(item);
                    });

            _planogramBlockingLocations =
                CreateTwoLayerItemCache<PlanogramBlockingLocationDto, PlanogramBlockingLocationDtoItem>(
                    (UInt16)SectionType.PlanogramBlockingLocations,
                    delegate(PlanogramBlockingLocationDto dto)
                    {
                        return new PlanogramBlockingLocationDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramBlockingLocationDtoItem(item);
                    });

            #endregion

            #region Renumbering Strategy

            _planogramRenumberingStrategy =
                CreateTwoLayerItemCache<PlanogramRenumberingStrategyDto, PlanogramRenumberingStrategyDtoItem>(
                    (UInt16) SectionType.PlanogramRenumberingStrategy,
                    dto => new PlanogramRenumberingStrategyDtoItem(dto),
                    item => new PlanogramRenumberingStrategyDtoItem(item));

            #endregion

            #region Inventory 

            _planogramInventory =
                CreateTwoLayerItemCache<PlanogramInventoryDto, PlanogramInventoryDtoItem>(
                    (UInt16)SectionType.PlanogramInventory,
                    delegate(PlanogramInventoryDto dto)
                    {
                        return new PlanogramInventoryDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new PlanogramInventoryDtoItem(item);
                    });

            #endregion

            #region Planogram Sequence

            _planogramSequences =
                CreateTwoLayerItemCache<PlanogramSequenceDto, PlanogramSequenceDtoItem>(
                    (UInt16) SectionType.PlanogramSequence,
                    dto => new PlanogramSequenceDtoItem(dto),
                    item => new PlanogramSequenceDtoItem(item));
            _planogramSequenceGroups =
                CreateTwoLayerItemCache<PlanogramSequenceGroupDto, PlanogramSequenceGroupDtoItem>(
                    (UInt16)SectionType.PlanogramSequenceGroups,
                    dto => new PlanogramSequenceGroupDtoItem(dto),
                    item => new PlanogramSequenceGroupDtoItem(item));
            _planogramSequenceGroupProducts =
                CreateTwoLayerItemCache<PlanogramSequenceGroupProductDto, PlanogramSequenceGroupProductDtoItem>(
                    (UInt16)SectionType.PlanogramSequenceGroupProducts,
                    dto => new PlanogramSequenceGroupProductDtoItem(dto),
                    item => new PlanogramSequenceGroupProductDtoItem(item));
            _planogramSequenceGroupSubGroups =
                CreateTwoLayerItemCache<PlanogramSequenceGroupSubGroupDto, PlanogramSequenceGroupSubGroupDtoItem>(
                    (UInt16)SectionType.PlanogramSequenceGroupSubGroups,
                    dto => new PlanogramSequenceGroupSubGroupDtoItem(dto),
                    item => new PlanogramSequenceGroupSubGroupDtoItem(item));

            #endregion

            #region PlanogramComparisons

            _planogramComparisons = CreateTwoLayerItemCache<PlanogramComparisonDto, PlanogramComparisonDtoItem>(
                (UInt16) SectionType.PlanogramComparisons,
                dto => new PlanogramComparisonDtoItem(dto),
                item => new PlanogramComparisonDtoItem(item));
            _planogramComparisonFields = CreateTwoLayerItemCache<PlanogramComparisonFieldDto, PlanogramComparisonFieldDtoItem>(
                (UInt16) SectionType.PlanogramComparisonFields,
                dto => new PlanogramComparisonFieldDtoItem(dto),
                item => new PlanogramComparisonFieldDtoItem(item));
            _planogramComparisonResults = CreateTwoLayerItemCache<PlanogramComparisonResultDto, PlanogramComparisonResultDtoItem>(
                (UInt16) SectionType.PlanogramComparisonResults,
                dto => new PlanogramComparisonResultDtoItem(dto),
                item => new PlanogramComparisonResultDtoItem(item));
            _planogramComparisonItems = CreateTwoLayerItemCache<PlanogramComparisonItemDto, PlanogramComparisonItemDtoItem>(
                (UInt16) SectionType.PlanogramComparisonItems,
                dto => new PlanogramComparisonItemDtoItem(dto),
                item => new PlanogramComparisonItemDtoItem(item));
            _planogramComparisonFieldValues = CreateTwoLayerItemCache<PlanogramComparisonFieldValueDto, PlanogramComparisonFieldValueDtoItem>(
                (UInt16) SectionType.PlanogramComparisonFieldValues,
                dto => new PlanogramComparisonFieldValueDtoItem(dto),
                item => new PlanogramComparisonFieldValueDtoItem(item));

            #endregion

            #region ProductAttributeComparisonResult

            _productAttributeComparisonResults = CreateTwoLayerItemCache<ProductAttributeComparisonResultDto, ProductAttributeComparisonResultDtoItem>(
                (UInt16)SectionType.ProductAttributeComparisonResults,
                dto => new ProductAttributeComparisonResultDtoItem(dto),
                item => new ProductAttributeComparisonResultDtoItem(item));

            #endregion
        }
        #endregion

        #region Properties
        /// <summary>
        /// Returns the package cache item
        /// </summary>
        public SingleItemCache<PackageDto, PackageDtoItem> Package
        {
            get { return _package; }
        }

        /// <summary>
        /// Returns the planogram cache item
        /// </summary>
        public OneLayerItemCache<PlanogramDto, PlanogramDtoItem> Planograms
        {
            get { return _planograms; }
        }

        /// <summary>
        /// Returns the planogram fxiture items cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramFixtureItemDto, PlanogramFixtureItemDtoItem> PlanogramFixtureItems
        {
            get { return _planogramFixtureItems; }
        }

        /// <summary>
        /// Returns the planogram fixtures cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramFixtureDto, PlanogramFixtureDtoItem> PlanogramFixtures
        {
            get { return _planogramFixtures; }
        }

        /// <summary>
        /// Returns the planogram fixture assemblies cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramFixtureAssemblyDto, PlanogramFixtureAssemblyDtoItem> PlanogramFixtureAssemblies
        {
            get { return _planogramFixtureAssemblies; }
        }

        /// <summary>
        /// Returns the planogram fixture components cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramFixtureComponentDto, PlanogramFixtureComponentDtoItem> PlanogramFixtureComponents
        {
            get { return _planogramFixtureComponents; }
        }

        /// <summary>
        /// Returns the planogram assemblies cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramAssemblyDto, PlanogramAssemblyDtoItem> PlanogramAssemblies
        {
            get { return _planogramAssemblies; }
        }

        /// <summary>
        /// Returns the planogram assembly components cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramAssemblyComponentDto, PlanogramAssemblyComponentDtoItem> PlanogramAssemblyComponents
        {
            get { return _planogramAssemblyComponents; }
        }

        /// <summary>
        /// Returns the planogram components cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramComponentDto, PlanogramComponentDtoItem> PlanogramComponents
        {
            get { return _planogramComponents; }
        }

        /// <summary>
        /// Returns the planogram sub components cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramSubComponentDto, PlanogramSubComponentDtoItem> PlanogramSubComponents
        {
            get { return _planogramSubComponents; }
        }

        /// <summary>
        /// Returns the planogram products cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramProductDto, PlanogramProductDtoItem> PlanogramProducts
        {
            get { return _planogramProducts; }
        }

        /// <summary>
        /// Returns the planogram positions cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramPositionDto, PlanogramPositionDtoItem> PlanogramPositions
        {
            get { return _planogramPositions; }
        }

        /// <summary>
        /// Returns the planogram annotations cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramAnnotationDto, PlanogramAnnotationDtoItem> PlanogramAnnotations
        {
            get { return _planogramAnnotations; }
        }

        /// <summary>
        /// Returns the planogram images cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramImageDto, PlanogramImageDtoItem> PlanogramImages
        {
            get { return _planogramImages; }
        }

        /// <summary>
        /// Returns the planogram meta dataimages cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramMetadataImageDto, PlanogramMetadataImageDtoItem> PlanogramMetadataImages
        {
            get { return _planogramMetadataImages; }
        }

        /// <summary>
        /// Returns the CustomAttributeData cache item.
        /// </summary>
        public TwoLayerItemCache<CustomAttributeDataDto, CustomAttributeDataDtoItem> CustomAttributeData
        {
            get { return _customAttributeData; }
        }

        /// <summary>
        /// Returns the PlanogramPerformanceData cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramPerformanceDataDto, PlanogramPerformanceDataDtoItem> PlanogramPerformanceDatas
        {
            get { return _planogramPerformanceDatas; }
        }

        /// <summary>
        /// Returns the PlanogramPerformanceMetric cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramPerformanceMetricDto, PlanogramPerformanceMetricDtoItem> PlanogramPerformanceMetrics
        {
            get { return _planogramPerformanceMetrics; }
        }

        /// <summary>
        /// Returns the PlanogramPerformance cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramPerformanceDto, PlanogramPerformanceDtoItem> PlanogramPerformances
        {
            get { return _planogramPerformances; }
        }

        /// <summary>
        /// Returns the PlanogramAssortment cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentDto, PlanogramAssortmentDtoItem> PlanogramAssortments
        {
            get { return _planogramAssortments; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentInventoryRule cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentInventoryRuleDto, PlanogramAssortmentInventoryRuleDtoItem> PlanogramAssortmentInventoryRules
        {
            get { return _planogramAssortmentInventoryRules; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentLocalProduct cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentLocalProductDto, PlanogramAssortmentLocalProductDtoItem> PlanogramAssortmentLocalProducts
        {
            get { return _planogramAssortmentLocalProducts; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentLocationBuddy cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentLocationBuddyDto, PlanogramAssortmentLocationBuddyDtoItem> PlanogramAssortmentLocationBuddys
        {
            get { return _planogramAssortmentLocationBuddys; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentProduct cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentProductDto, PlanogramAssortmentProductDtoItem> PlanogramAssortmentProducts
        {
            get { return _planogramAssortmentProducts; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentProductBuddy cache item
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentProductBuddyDto, PlanogramAssortmentProductBuddyDtoItem> PlanogramAssortmentProductBuddys
        {
            get { return _planogramAssortmentProductBuddys; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentRegion cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentRegionDto, PlanogramAssortmentRegionDtoItem> PlanogramAssortmentRegions
        {
            get { return _planogramAssortmentRegions; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentRegionLocation cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentRegionLocationDto, PlanogramAssortmentRegionLocationDtoItem> PlanogramAssortmentRegionLocations
        {
            get { return _planogramAssortmentRegionLocations; }
        }

        /// <summary>
        /// Returns the PlanogramAssortmentRegionProduct cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramAssortmentRegionProductDto, PlanogramAssortmentRegionProductDtoItem> PlanogramAssortmentRegionProducts
        {
            get { return _planogramAssortmentRegionProducts; }
        }

        /// <summary>
        /// Returns the PlanogramValidationTemplate cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramValidationTemplateDto, PlanogramValidationTemplateDtoItem> PlanogramValidationTemplates
        {
            get { return _planogramValidationTemplates; }
        }

        /// <summary>
        /// Returns the PlanogramValidationTemplateGroup cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramValidationTemplateGroupDto, PlanogramValidationTemplateGroupDtoItem> PlanogramValidationTemplateGroups
        {
            get { return _planogramValidationTemplateGroups; }
        }

        /// <summary>
        /// Returns the PlanogramValidationTemplateGroupMetric cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramValidationTemplateGroupMetricDto, PlanogramValidationTemplateGroupMetricDtoItem> PlanogramValidationTemplateGroupMetrics
        {
            get { return _planogramValidationTemplateGroupMetrics; }
        }

        /// <summary>
        /// Returns the PlanogramConsumerDecisionTree cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramConsumerDecisionTreeDto, PlanogramConsumerDecisionTreeDtoItem> PlanogramConsumerDecisionTrees
        {
            get { return _planogramConsumerDecisionTrees; }
        }

        /// <summary>
        /// Returns the PlanogramConsumerDecisionTreeLevel cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramConsumerDecisionTreeLevelDto, PlanogramConsumerDecisionTreeLevelDtoItem> PlanogramConsumerDecisionTreeLevels
        {
            get { return _planogramConsumerDecisionTreeLevels; }
        }

        /// <summary>
        /// Returns the PlanogramConsumerDecisionTreeNode cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramConsumerDecisionTreeNodeDto, PlanogramConsumerDecisionTreeNodeDtoItem> PlanogramConsumerDecisionTreeNodes
        {
            get { return _planogramConsumerDecisionTreeNodes; }
        }

        /// <summary>
        /// Returns the PlanogramConsumerDecisionTreeNodeProduct cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramConsumerDecisionTreeNodeProductDto, PlanogramConsumerDecisionTreeNodeProductDtoItem> PlanogramConsumerDecisionTreeNodeProducts
        {
            get { return _planogramConsumerDecisionTreeNodeProducts; }
        }

        /// <summary>
        /// Returns the PlanogramEventLog cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramEventLogDto, PlanogramEventLogDtoItem> PlanogramEventLogs
        {
            get { return _planogramEventLogs; }
        }


        /// <summary>
        /// Returns the PlanogramBlocking cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramBlockingDto, PlanogramBlockingDtoItem> PlanogramBlocking
        {
            get { return _planogramBlocking; }
        }

        /// <summary>
        /// Returns the PlanogramBlockingGroups cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramBlockingGroupDto, PlanogramBlockingGroupDtoItem> PlanogramBlockingGroups
        {
            get { return _planogramBlockingGroups; }
        }

        /// <summary>
        /// Returns the PlanogramBlockingDividers cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramBlockingDividerDto, PlanogramBlockingDividerDtoItem> PlanogramBlockingDividers
        {
            get { return _planogramBlockingDividers; }
        }

        /// <summary>
        /// Returns the PlanogramBlockingLocations cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramBlockingLocationDto, PlanogramBlockingLocationDtoItem> PlanogramBlockingLocations
        {
            get { return _planogramBlockingLocations; }
        }

        public TwoLayerItemCache<PlanogramRenumberingStrategyDto, PlanogramRenumberingStrategyDtoItem>
            PlanogramRenumberingStrategy
        {
            get { return _planogramRenumberingStrategy; }
        }

        /// <summary>
        /// Returns the PlanogramInventory cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramInventoryDto, PlanogramInventoryDtoItem> PlanogramInventory
        {
            get { return _planogramInventory; }
        }

        /// <summary>
        /// Returns a Planogram Sequence cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramSequenceDto, PlanogramSequenceDtoItem> PlanogramSequences
        {
            get { return _planogramSequences; }
        }

        /// <summary>
        /// Returns a Planogram Sequence Group cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramSequenceGroupDto, PlanogramSequenceGroupDtoItem> PlanogramSequenceGroups
        {
            get { return _planogramSequenceGroups; }
        }

        /// <summary>
        /// Returns a Planogram Sequence Group Product cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramSequenceGroupProductDto, PlanogramSequenceGroupProductDtoItem> PlanogramSequencesGroupProducts
        {
            get { return _planogramSequenceGroupProducts; }
        }

        /// <summary>
        /// Returns a Planogram Sequence Group SubGroup cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramSequenceGroupSubGroupDto, PlanogramSequenceGroupSubGroupDtoItem> PlanogramSequencesGroupSubGroups
        {
            get { return _planogramSequenceGroupSubGroups; }
        }

        #region Comparison cache items

        /// <summary>
        ///     Get a Planogram Comparison cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramComparisonDto, PlanogramComparisonDtoItem> PlanogramComparisons { get { return _planogramComparisons; } }

        /// <summary>
        ///     Get a Planogram Comparison Field cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramComparisonFieldDto, PlanogramComparisonFieldDtoItem> PlanogramComparisonFields
        {
            get { return _planogramComparisonFields; }
        }

        /// <summary>
        ///     Get a Planogram Comparison Result cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramComparisonResultDto, PlanogramComparisonResultDtoItem> PlanogramComparisonResults
        {
            get { return _planogramComparisonResults; }
        }

        /// <summary>
        ///     Get a Planogram Comparison Item cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramComparisonItemDto, PlanogramComparisonItemDtoItem> PlanogramComparisonItems
        {
            get { return _planogramComparisonItems; }
        }

        /// <summary>
        ///     Get a Planogram Comparison Field Value cache item.
        /// </summary>
        public TwoLayerItemCache<PlanogramComparisonFieldValueDto, PlanogramComparisonFieldValueDtoItem> PlanogramComparisonFieldValues
        {
            get { return _planogramComparisonFieldValues; }
        }

        #endregion

        /// <summary>
        /// Returns the ProductAttributeComparisonResult cache item.
        /// </summary>
        public TwoLayerItemCache<ProductAttributeComparisonResultDto, ProductAttributeComparisonResultDtoItem> ProductAttributeComparisonResults
        {
            get { return _productAttributeComparisonResults; }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when committing this data
        /// </summary>
        protected override void OnCommit()
        {
            if (_package.Fetch() == null)
            {
                base.DeleteFile();
            }
            else
            {
                base.OnCommit();
            }
        }
        #endregion
    }
}