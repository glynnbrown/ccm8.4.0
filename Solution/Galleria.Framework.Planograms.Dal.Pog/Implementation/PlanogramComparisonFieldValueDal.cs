﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramComparisonFieldValueDal : Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramComparisonFieldValueDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramComparisonFieldValueDto"/> assigned to the given planogram item <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="object"/> identifying the parent planogram item.</param>
        public IEnumerable<PlanogramComparisonFieldValueDto> FetchByPlanogramComparisonItemId(Object id)
        {
            List<PlanogramComparisonFieldValueDto> dtos = DalCache.PlanogramComparisonFieldValues.FetchByParentId((Int32)id);
            if (dtos == null) throw new DtoDoesNotExistException();
            return dtos;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the given <paramref name="dto"/> data, assigning the next available Integer value as ID.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldValueDto"/> containing the values to insert.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Insert(PlanogramComparisonFieldValueDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramComparisonFieldValues.Insert(dto);
        }

        /// <summary>
        ///     Insert the given <paramref name="dtos"/> data, assigning the next available Integer value as ID.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonFieldValueDto"/> containing the values to insert.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Insert(IEnumerable<PlanogramComparisonFieldValueDto> dtos)
        {
            foreach (PlanogramComparisonFieldValueDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the data from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldValueDto"/> containing the values to update.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Update(PlanogramComparisonFieldValueDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComparisonFieldValues.Update(dto);
        }

        /// <summary>
        ///     Update the data from the given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> containing the values to update.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Update(IEnumerable<PlanogramComparisonFieldValueDto> dtos)
        {
            foreach (PlanogramComparisonFieldValueDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> identifying the planogram comparison.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComparisonFieldValues.Delete((Int32)id);
        }

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldValueDto"/> containing the values to delete.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Delete(PlanogramComparisonFieldValueDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonFieldValueDto"/> containing the values to delete.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Delete(IEnumerable<PlanogramComparisonFieldValueDto> dtos)
        {
            foreach (PlanogramComparisonFieldValueDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}