﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramImageDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramImageDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram images with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramImageDto> FetchByPlanogramId(Object planogramId)
        {
            return DalCache.PlanogramImages.FetchByParentId((Int32)planogramId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram image, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramImageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramImages.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramImageDto> dtos)
        {
            foreach (PlanogramImageDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram image by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramImageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramImages.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramImageDto> dtos)
        {
            foreach (PlanogramImageDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram image by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramImages.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramImageDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramImageDto> dtos)
        {
            foreach (PlanogramImageDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
