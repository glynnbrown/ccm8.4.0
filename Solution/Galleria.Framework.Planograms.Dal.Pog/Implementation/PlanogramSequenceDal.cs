﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramSequenceDal : DalBase<DalCache>, IPlanogramSequenceDal
    {
        #region Fetch

        /// <summary>
        ///     Fetches the PlanogramSequenceDto with a given planogram ID.
        /// </summary>
        /// <param name="planogramId"></param>
        /// <returns></returns>
        public PlanogramSequenceDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramSequenceDto dto = DalCache.PlanogramSequences.FetchByParentId((Int32) planogramId).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a Planogram Sequence by using the values in the provided <paramref name="dto"/>, assigning it the next available integer ID.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceDto"/> containing the values of the newly inserted record.</param>
        public void Insert(PlanogramSequenceDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            dto.Id = DalCache.PlanogramSequences.Insert(dto);
        }

        /// <summary>
        ///     Inserts a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceDto> dtos)
        {
            foreach (PlanogramSequenceDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates a Planogram Sequence by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceDto"/> containing the values to update.</param>
        public void Update(PlanogramSequenceDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequences.Update(dto);
        }

        /// <summary>
        ///     Updates a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceDto> dtos)
        {
            foreach (PlanogramSequenceDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a Planogram Sequence matching the <paramref name="id"/>, if it exists.
        /// </summary>
        /// <param name="id">The <see cref="PlanogramSequenceDto"/> containing the values to update.</param>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequences.Delete((Int32) id);
        }

        /// <summary>
        ///     Deletes a Planogram Sequence by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceDto"/> containing the values to delete.</param>
        public void Delete(PlanogramSequenceDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceDto> dtos)
        {
            foreach (PlanogramSequenceDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}
