﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27058 : A.Probyn
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramMetadataImageDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramMetadataImageDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram images with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramMetadataImageDto> FetchByPlanogramId(Object planogramId)
        {
            return DalCache.PlanogramMetadataImages.FetchByParentId((Int32)planogramId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram image, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramMetadataImageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramMetadataImages.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            foreach (PlanogramMetadataImageDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram image by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramMetadataImageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramMetadataImages.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            foreach (PlanogramMetadataImageDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion 

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram image by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramMetadataImages.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(PlanogramMetadataImageDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            foreach (PlanogramMetadataImageDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
