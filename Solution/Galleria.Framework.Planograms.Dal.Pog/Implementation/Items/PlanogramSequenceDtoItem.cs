﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramSequenceDtoItem : PlanogramSequenceDto, IChildDtoItem<PlanogramSequenceDto>
    {
        #region Constructors

        /// <summary>
        ///     Creates a new <see cref="PlanogramSequenceDtoItem"/> and populates it with values from the <paramref name="item"/>.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem"/> from which to take the values for this instance.</param>
        public PlanogramSequenceDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceId));
            PlanogramId =
                GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequencePlanogramId));
        }

        /// <summary>
        ///     Creates a new <see cref="PlanogramSequenceDtoItem"/> and populates it with values from the <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceDto"/> from which to take the values for this instance.</param>
        public PlanogramSequenceDtoItem(PlanogramSequenceDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
        }

        #endregion

        #region IChildDtoItem members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramSequenceId:
                    return Id;
                case FieldNames.PlanogramSequencePlanogramId:
                    return PlanogramId;
                default:
                    return null;
            }
        }

        /// <summary>
        ///     Create and return a new instance with identical values to this one.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramSequenceDto"/>.</returns>
        public PlanogramSequenceDto CopyDto()
        {
            return new PlanogramSequenceDto
            {
                Id = Id,
                PlanogramId = PlanogramId
            };
        }

        public Boolean AutoIncrementId
        {
            get { return true; }
        }

        public Object ParentId
        {
            get { return PlanogramId; }
        }

        #endregion
    }
}
