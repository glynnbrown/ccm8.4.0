﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-25881 : A.Probyn
//  Added Meta data properties    
// V8-27058 : A.Probyn
//  Updated MetaData properties
#endregion

#region Version History: CCM803
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramFixtureAssemblyDtoItem : PlanogramFixtureAssemblyDto, IChildDtoItem<PlanogramFixtureAssemblyDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramFixtureAssemblyDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramFixtureAssemblyDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyId));
            PlanogramFixtureId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyPlanogramFixtureId));
            PlanogramAssemblyId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyPlanogramAssemblyId));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblySlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyRoll));
            MetaComponentCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaComponentCount));
            MetaTotalMerchandisableLinearSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace));
            MetaTotalMerchandisableAreaSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace));
            MetaTotalMerchandisableVolumetricSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace));
            MetaTotalLinearWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace));
            MetaTotalAreaWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace));
            MetaTotalVolumetricWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace));
            MetaProductsPlaced = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaProductsPlaced));
            MetaNewProducts = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaNewProducts));
            MetaChangesFromPreviousCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaChangesFromPreviousCount));
            MetaChangeFromPreviousStarRating = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating));
            MetaBlocksDropped = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaBlocksDropped));
            MetaNotAchievedInventory = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaNotAchievedInventory));
            MetaTotalFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalFacings));
            MetaAverageFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaAverageFacings));
            MetaTotalUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalUnits));
            MetaAverageUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaAverageUnits));
            MetaMinDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaMinDos));
            MetaMaxDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaMaxDos));
            MetaAverageDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaAverageDos));
            MetaMinCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaMinCases));
            MetaAverageCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaAverageCases));
            MetaMaxCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaMaxCases));
            MetaSpaceToUnitsIndex = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaSpaceToUnitsIndex));
            MetaTotalFrontFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaTotalFrontFacings));
            MetaAverageFrontFacings = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureAssemblyMetaAverageFrontFacings));
        }

        /// <summary>
        /// Creates a new PlanogramFixtureAssemblyDtoItem and populates its properties with values taken from a 
        /// PlanogramFixtureAssemblyDto.
        /// </summary>
        public PlanogramFixtureAssemblyDtoItem(PlanogramFixtureAssemblyDto dto)
        {
            Id = dto.Id;
            PlanogramFixtureId = dto.PlanogramFixtureId;
            PlanogramAssemblyId = dto.PlanogramAssemblyId;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
            MetaComponentCount = dto.MetaComponentCount;
            MetaTotalMerchandisableLinearSpace = dto.MetaTotalMerchandisableLinearSpace;
            MetaTotalMerchandisableAreaSpace = dto.MetaTotalMerchandisableAreaSpace;
            MetaTotalMerchandisableVolumetricSpace = dto.MetaTotalMerchandisableVolumetricSpace;
            MetaTotalLinearWhiteSpace = dto.MetaTotalLinearWhiteSpace;
            MetaTotalAreaWhiteSpace = dto.MetaTotalAreaWhiteSpace;
            MetaTotalVolumetricWhiteSpace = dto.MetaTotalVolumetricWhiteSpace;
            MetaProductsPlaced = dto.MetaProductsPlaced;
            MetaNewProducts = dto.MetaNewProducts;
            MetaChangesFromPreviousCount = dto.MetaChangesFromPreviousCount;
            MetaChangeFromPreviousStarRating = dto.MetaChangeFromPreviousStarRating;
            MetaBlocksDropped = dto.MetaBlocksDropped;
            MetaNotAchievedInventory = dto.MetaNotAchievedInventory;
            MetaTotalFacings = dto.MetaTotalFacings;
            MetaAverageFacings = dto.MetaAverageFacings;
            MetaTotalUnits = dto.MetaTotalUnits;
            MetaAverageUnits = dto.MetaAverageUnits;
            MetaMinDos = dto.MetaMinDos;
            MetaMaxDos = dto.MetaMaxDos;
            MetaAverageDos = dto.MetaAverageDos;
            MetaMinCases = dto.MetaMinCases;
            MetaAverageCases = dto.MetaAverageCases;
            MetaMaxCases = dto.MetaMaxCases;
            MetaSpaceToUnitsIndex = dto.MetaSpaceToUnitsIndex;
            MetaTotalFrontFacings = dto.MetaTotalFrontFacings;
            MetaAverageFrontFacings = dto.MetaAverageFrontFacings;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramFixtureAssemblyDto>.Id
        {
            get { return (Int32)Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramFixtureAssemblyDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramFixtureAssemblyDto>.ParentId
        {
            get { return (Int32)PlanogramFixtureId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramFixtureAssemblyDto CopyDto()
        {
            return new PlanogramFixtureAssemblyDto()
            {
                Id = this.Id,
                PlanogramFixtureId = this.PlanogramFixtureId,
                PlanogramAssemblyId = this.PlanogramAssemblyId,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
                MetaComponentCount = this.MetaComponentCount,
                MetaTotalMerchandisableLinearSpace = this.MetaTotalMerchandisableLinearSpace,
                MetaTotalMerchandisableAreaSpace = this.MetaTotalMerchandisableAreaSpace,
                MetaTotalMerchandisableVolumetricSpace = this.MetaTotalMerchandisableVolumetricSpace,
                MetaTotalLinearWhiteSpace = this.MetaTotalLinearWhiteSpace,
                MetaTotalAreaWhiteSpace = this.MetaTotalAreaWhiteSpace,
                MetaTotalVolumetricWhiteSpace = this.MetaTotalVolumetricWhiteSpace,
                MetaProductsPlaced = this.MetaProductsPlaced,
                MetaNewProducts = this.MetaNewProducts,
                MetaChangesFromPreviousCount = this.MetaChangesFromPreviousCount,
                MetaChangeFromPreviousStarRating = this.MetaChangeFromPreviousStarRating,
                MetaBlocksDropped = this.MetaBlocksDropped,
                MetaNotAchievedInventory = this.MetaNotAchievedInventory,
                MetaTotalFacings = this.MetaTotalFacings,
                MetaAverageFacings = this.MetaAverageFacings,
                MetaTotalUnits = this.MetaTotalUnits,
                MetaAverageUnits = this.MetaAverageUnits,
                MetaMinDos = this.MetaMinDos,
                MetaMaxDos = this.MetaMaxDos,
                MetaAverageDos = this.MetaAverageDos,
                MetaMinCases = this.MetaMinCases,
                MetaAverageCases = this.MetaAverageCases,
                MetaMaxCases = this.MetaMaxCases,
                MetaSpaceToUnitsIndex = this.MetaSpaceToUnitsIndex,
                MetaTotalFrontFacings = this.MetaTotalFrontFacings,
                MetaAverageFrontFacings = this.MetaAverageFrontFacings
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramFixtureAssemblyId:
                    return Id;
                case FieldNames.PlanogramFixtureAssemblyPlanogramFixtureId:
                    return PlanogramFixtureId;
                case FieldNames.PlanogramFixtureAssemblyPlanogramAssemblyId:
                    return PlanogramAssemblyId;
                case FieldNames.PlanogramFixtureAssemblyX:
                    return X;
                case FieldNames.PlanogramFixtureAssemblyY:
                    return Y;
                case FieldNames.PlanogramFixtureAssemblyZ:
                    return Z;
                case FieldNames.PlanogramFixtureAssemblySlope:
                    return Slope;
                case FieldNames.PlanogramFixtureAssemblyAngle:
                    return Angle;
                case FieldNames.PlanogramFixtureAssemblyRoll:
                    return Roll;
                case FieldNames.PlanogramFixtureAssemblyMetaComponentCount:
                    return MetaComponentCount;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace:
                    return MetaTotalMerchandisableLinearSpace;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace:
                    return MetaTotalMerchandisableAreaSpace;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace:
                    return MetaTotalMerchandisableVolumetricSpace;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace:
                    return MetaTotalLinearWhiteSpace;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace:
                    return MetaTotalAreaWhiteSpace;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace:
                    return MetaTotalVolumetricWhiteSpace;
                case FieldNames.PlanogramFixtureAssemblyMetaProductsPlaced:
                    return MetaProductsPlaced;
                case FieldNames.PlanogramFixtureAssemblyMetaNewProducts:
                    return MetaNewProducts;
                case FieldNames.PlanogramFixtureAssemblyMetaChangesFromPreviousCount:
                    return MetaChangesFromPreviousCount;
                case FieldNames.PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating:
                    return MetaChangeFromPreviousStarRating;
                case FieldNames.PlanogramFixtureAssemblyMetaBlocksDropped:
                    return MetaBlocksDropped;
                case FieldNames.PlanogramFixtureAssemblyMetaNotAchievedInventory:
                    return MetaNotAchievedInventory;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalFacings:
                    return MetaTotalFacings;
                case FieldNames.PlanogramFixtureAssemblyMetaAverageFacings:
                    return MetaAverageFacings;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalUnits:
                    return MetaTotalUnits;
                case FieldNames.PlanogramFixtureAssemblyMetaAverageUnits:
                    return MetaAverageUnits;
                case FieldNames.PlanogramFixtureAssemblyMetaMinDos:
                    return MetaMinDos;
                case FieldNames.PlanogramFixtureAssemblyMetaMaxDos:
                    return MetaMaxDos;
                case FieldNames.PlanogramFixtureAssemblyMetaAverageDos:
                    return MetaAverageDos;
                case FieldNames.PlanogramFixtureAssemblyMetaMinCases:
                    return MetaMinCases;
                case FieldNames.PlanogramFixtureAssemblyMetaAverageCases:
                    return MetaAverageCases;
                case FieldNames.PlanogramFixtureAssemblyMetaMaxCases:
                    return MetaMaxCases;
                case FieldNames.PlanogramFixtureAssemblyMetaSpaceToUnitsIndex:
                    return MetaSpaceToUnitsIndex;
                case FieldNames.PlanogramFixtureAssemblyMetaTotalFrontFacings:
                    return MetaTotalFrontFacings;
                case FieldNames.PlanogramFixtureAssemblyMetaAverageFrontFacings:
                    return MetaAverageFrontFacings;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
