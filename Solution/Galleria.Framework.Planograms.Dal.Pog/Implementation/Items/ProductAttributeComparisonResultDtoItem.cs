﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class ProductAttributeComparisonResultDtoItem : ProductAttributeComparisonResultDto, IChildDtoItem<ProductAttributeComparisonResultDto>
    {
        #region Public Properties

        Object IChildDtoItem<ProductAttributeComparisonResultDto>.Id { get { return Id; } set { Id = value; } }

        Boolean IChildDtoItem<ProductAttributeComparisonResultDto>.AutoIncrementId { get { return true; } }

        Object IChildDtoItem<ProductAttributeComparisonResultDto>.ParentId { get { return PlanogramId; } }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize a new instance of <see cref="ProductAttributeComparisonResultDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem" /> contaning the values used to populate the new instance.</param>
        public ProductAttributeComparisonResultDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.ProductAttributeComparisonResultId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.ProductAttributeComparisonResultPlanogramId));
            ProductGtin = (String)item.GetItemPropertyValue(FieldNames.ProductAttributeComparisonResultProductGtin);
            ComparedProductAttribute = (String)item.GetItemPropertyValue(FieldNames.ProductAttributeComparisonResultComparedProductAttribute);
            MasterDataValue = (String)item.GetItemPropertyValue(FieldNames.ProductAttributeComparisonResultMasterDataValue);
            ProductValue = (String)item.GetItemPropertyValue(FieldNames.ProductAttributeComparisonResultProductValue);
        }

        /// <summary>
        ///     Initialize a new instance of <see cref="ProductAttributeComparisonResultDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">The <see cref="ProductAttributeComparisonResultDto" /> contaning the values used to populate the new instance.</param>
        public ProductAttributeComparisonResultDtoItem(ProductAttributeComparisonResultDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            ProductGtin = dto.ProductGtin;
            ComparedProductAttribute = dto.ComparedProductAttribute;
            MasterDataValue = dto.MasterDataValue;
            ProductValue = dto.ProductValue;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public ProductAttributeComparisonResultDto CopyDto()
        {
            return new ProductAttributeComparisonResultDto
            {
                PlanogramId = PlanogramId,
                ProductGtin = ProductGtin,
                ComparedProductAttribute = ComparedProductAttribute,
                MasterDataValue = MasterDataValue,
                ProductValue = ProductValue
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.ProductAttributeComparisonResultId:
                    return Id;
                case FieldNames.ProductAttributeComparisonResultPlanogramId:
                    return PlanogramId;
                case FieldNames.ProductAttributeComparisonResultProductGtin:
                    return ProductGtin;
                case FieldNames.ProductAttributeComparisonResultComparedProductAttribute:
                    return ComparedProductAttribute;
                case FieldNames.ProductAttributeComparisonResultMasterDataValue:
                    return MasterDataValue;
                case FieldNames.ProductAttributeComparisonResultProductValue:
                    return ProductValue;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}