﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26269 : A.Kuszyk
//  Created
#endregion
#region Version History: CCM803
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramPerformanceMetricDtoItem :
        PlanogramPerformanceMetricDto,
        IChildDtoItem<PlanogramPerformanceMetricDto>,
        GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramPerformanceMetricDtoItem and populates its properties with 
        /// values taken from a GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramPerformanceMetricDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricId));
            PlanogramPerformanceId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricName);
            Description = (String)item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricDescription);
            Direction = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricDirection));
            SpecialType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricSpecialType));
            MetricType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricMetricType));
            MetricId = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricMetricId));
            AggregationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceMetricAggregationType));
        }

        /// <summary>
        /// Creates a new PlanogramPerformanceMetricDtoItem and populates its properties with 
        /// values taken from a PlanogramPerformanceDataDto.
        /// </summary>
        public PlanogramPerformanceMetricDtoItem(PlanogramPerformanceMetricDto dto)
        {
            Id = dto.Id;
            PlanogramPerformanceId = dto.PlanogramPerformanceId;
            Name = dto.Name;
            Description = dto.Description;
            Direction = dto.Direction;
            SpecialType = dto.SpecialType;
            MetricType = dto.MetricType;
            MetricId = dto.MetricId;
            AggregationType = dto.AggregationType;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramPerformanceMetricDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramPerformanceMetricDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramPerformanceMetricDto>.ParentId
        {
            get { return PlanogramPerformanceId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramPerformanceMetricDto CopyDto()
        {
            return new PlanogramPerformanceMetricDto()
            {
                Id = this.Id,
                PlanogramPerformanceId = this.PlanogramPerformanceId,
                Name = this.Name,
                Description = this.Description,
                Direction = this.Direction,
                SpecialType = this.SpecialType,
                MetricType = this.MetricType,
                MetricId = this.MetricId,
                AggregationType = this.AggregationType
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramPerformanceMetricId:
                    return Id;
                case FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId:
                    return PlanogramPerformanceId;
                case FieldNames.PlanogramPerformanceMetricName:
                    return Name;
                case FieldNames.PlanogramPerformanceMetricDescription:
                    return Description;
                case FieldNames.PlanogramPerformanceMetricDirection:
                    return Direction;
                case FieldNames.PlanogramPerformanceMetricSpecialType:
                    return SpecialType;
                case FieldNames.PlanogramPerformanceMetricMetricType:
                    return MetricType;
                case FieldNames.PlanogramPerformanceMetricMetricId:
                    return MetricId;
                case FieldNames.PlanogramPerformanceMetricAggregationType:
                    return AggregationType;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
