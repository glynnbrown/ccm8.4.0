﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup
//  Initial version.
// V8-24290: A. Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25949 : N.Foster
//  Changed PackageId to String
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added Meta data properties
// V8-26709 : L.Ineson
//  Removed old unit of measure property.
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27411 : M.Pettit
//  Added Planogram.UserName property
// V8-24779 : D.Pleasance
//  Added Planogram.SourcePath
// V8-27783 : M.Brumby
//	Added Planogram.UniqueContentReference

#endregion

#region Version History: CCM802

// V8-28987 : I.George
// Added LocationCode, LocationName, ClusterSchemeName and ClusterName properties

#endregion

#region Version History: CCM803

// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-28242 : M.Shelley
//  Added Status, DateWIP, DateApproved, DateArchived 

#endregion

#region Version History: CCM810

// V8-29844 : L.Ineson
//  Added more metadata
// V--29860 : M.Shelley
//  Added PlanogramType
// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents

#endregion

#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>.

#endregion

#region Version History: CCM820
// V8-30705 : A.Probyn
//  Extended for new assortment rule related meta data properties
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
#endregion

#region Version History: CCM830
// V8-32396 : A.Probyn
//  Extended for MetaNumberOfDelistFamilyRulesBroken
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added MetaCountOfProductsBuddied
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramDtoItem : PlanogramDto, IParentDtoItem<PlanogramDto>
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramDtoItem and populates its properties with values taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramId));
            UniqueContentReference = Convert.ToString(item.GetItemPropertyValue(FieldNames.PlanogramUniqueContentReference) ?? Guid.Empty);
            PackageId = (String)item.GetItemPropertyValue(FieldNames.PlanogramPackageId);
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramDepth));
            UserName = (String)item.GetItemPropertyValue(FieldNames.PlanogramUserName);
            LengthUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramLengthUnitsOfMeasure));
            AreaUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAreaUnitsOfMeasure));
            VolumeUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramVolumeUnitsOfMeasure));
            WeightUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramWeightUnitsOfMeasure));
            AngleUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAngleUnitsOfMeasure));
            CurrencyUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramCurrencyUnitsOfMeasure));
            ProductPlacementX = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductPlacementX));
            ProductPlacementY = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductPlacementY));
            ProductPlacementZ = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductPlacementZ));
            CategoryCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramCategoryCode);
            CategoryName = (String)item.GetItemPropertyValue(FieldNames.PlanogramCategoryName);
            SourcePath = (String)item.GetItemPropertyValue(FieldNames.PlanogramSourcePath);
            MetaBayCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaBayCount));
            MetaUniqueProductCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaUniqueProductCount));
            MetaComponentCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaComponentCount));
            MetaTotalMerchandisableLinearSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalMerchandisableLinearSpace));
            MetaTotalMerchandisableAreaSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalMerchandisableAreaSpace));
            MetaTotalMerchandisableVolumetricSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalMerchandisableVolumetricSpace));
            MetaTotalLinearWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalLinearWhiteSpace));
            MetaTotalAreaWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalAreaWhiteSpace));
            MetaTotalVolumetricWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalVolumetricWhiteSpace));
            MetaProductsPlaced = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaProductsPlaced));
            MetaProductsUnplaced = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaProductsUnplaced));
            MetaNewProducts = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNewProducts));
            MetaChangesFromPreviousCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaChangesFromPreviousCount));
            MetaChangeFromPreviousStarRating = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaChangeFromPreviousStarRating));
            MetaBlocksDropped = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaBlocksDropped));
            MetaNotAchievedInventory = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNotAchievedInventory));
            MetaTotalFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalFacings));
            MetaAverageFacings = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaAverageFacings));
            MetaTotalUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalUnits));
            MetaAverageUnits = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaAverageUnits));
            MetaMinDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaMinDos));
            MetaMaxDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaMaxDos));
            MetaAverageDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaAverageDos));
            MetaMinCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaMinCases));
            MetaAverageCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaAverageCases));
            MetaMaxCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaMaxCases));
            MetaTotalComponentCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalComponentCollisions));
            MetaTotalComponentsOverMerchandisedDepth = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalComponentsOverMerchandisedDepth));
            MetaTotalComponentsOverMerchandisedHeight = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalComponentsOverMerchandisedHeight));
            MetaTotalComponentsOverMerchandisedWidth = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalComponentsOverMerchandisedWidth));
            MetaHasComponentsOutsideOfFixtureArea = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramMetaHasComponentsOutsideOfFixtureArea));
            MetaTotalPositionCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalPositionCollisions));
            MetaTotalFrontFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalFrontFacings));
            MetaAverageFrontFacings = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaAverageFrontFacings));
            LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramLocationCode);
            LocationName = (String)item.GetItemPropertyValue(FieldNames.PlanogramLocationName);
            ClusterSchemeName = (String)item.GetItemPropertyValue(FieldNames.PlanogramClusterSchemeName);
            ClusterName = (String)item.GetItemPropertyValue(FieldNames.PlanogramClusterName);
            MetaNoOfErrors = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNoOfErrors));
            MetaNoOfWarnings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNoOfWarnings));
            MetaNoOfInformationEvents = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNoOfInformationEvents));
            MetaNoOfDebugEvents = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNoOfDebugEvents));
            MetaTotalErrorScore = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaTotalErrorScore));
            MetaHighestErrorScore = GalleriaBinaryFileItemHelper.GetByteNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaHighestErrorScore));
            Status = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramStatus));
            DateWip = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PlanogramDateWip));
            DateApproved = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PlanogramDateApproved));
            DateArchived = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PlanogramDateArchived));
            MetaCountOfProductNotAchievedCases = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfProductNotAchievedCases));
            MetaCountOfProductNotAchievedDOS = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfProductNotAchievedDOS));
            MetaCountOfProductOverShelfLifePercent = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfProductOverShelfLifePercent));
            MetaCountOfProductNotAchievedDeliveries = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfProductNotAchievedDeliveries));
            MetaPercentOfProductNotAchievedCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentOfProductNotAchievedCases));
            MetaPercentOfProductNotAchievedDOS = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentOfProductNotAchievedDOS));
            MetaPercentOfProductOverShelfLifePercent = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentOfProductOverShelfLifePercent));
            MetaPercentOfProductNotAchievedDeliveries = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentOfProductNotAchievedDeliveries));
            MetaCountOfPositionsOutsideOfBlockSpace = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfPositionsOutsideOfBlockSpace));
            MetaPercentOfPositionsOutsideOfBlockSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentOfPositionsOutsideOfBlockSpace));
            MetaPercentOfPlacedProductsRecommendedInAssortment = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment));
            MetaCountOfPlacedProductsRecommendedInAssortment = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfPlacedProductsRecommendedInAssortment));
            MetaCountOfPlacedProductsNotRecommendedInAssortment = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment));
            MetaCountOfRecommendedProductsInAssortment = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfRecommendedProductsInAssortment));
            PlanogramType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPlanogramType));
            MetaPercentOfProductNotAchievedInventory = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentOfProductNotAchievedInventory));
            HighlightSequenceStrategy = (String)item.GetItemPropertyValue(FieldNames.PlanogramHighlightSequenceStrategy);

            MetaNumberOfAssortmentRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfAssortmentRulesBroken));
            MetaNumberOfProductRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfProductRulesBroken));
            MetaNumberOfFamilyRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfFamilyRulesBroken));
            MetaNumberOfInheritanceRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfInheritanceRulesBroken));
            MetaNumberOfLocalProductRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfLocalProductRulesBroken));
            MetaNumberOfDistributionRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfDistributionRulesBroken));
            MetaNumberOfCoreRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfCoreRulesBroken));
            MetaPercentageOfAssortmentRulesBroken = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentageOfAssortmentRulesBroken));
            MetaPercentageOfProductRulesBroken = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentageOfProductRulesBroken));
            MetaPercentageOfFamilyRulesBroken = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentageOfFamilyRulesBroken));
            MetaPercentageOfInheritanceRulesBroken = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentageOfInheritanceRulesBroken));
            MetaPercentageOfLocalProductRulesBroken = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentageOfLocalProductRulesBroken));
            MetaPercentageOfDistributionRulesBroken = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentageOfDistributionRulesBroken));
            MetaPercentageOfCoreRulesBroken = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaPercentageOfCoreRulesBroken));
            MetaNumberOfDelistProductRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfDelistProductRulesBroken));
            MetaNumberOfForceProductRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfForceProductRulesBroken));
            MetaNumberOfPreserveProductRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfPreserveProductRulesBroken));
            MetaNumberOfMinimumHurdleProductRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfMinimumHurdleProductRulesBroken));
            MetaNumberOfMaximumProductFamilyRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfMaximumProductFamilyRulesBroken));
            MetaNumberOfMinimumProductFamilyRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfMinimumProductFamilyRulesBroken));
            MetaNumberOfDependencyFamilyRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfDependencyFamilyRulesBroken));
            MetaNumberOfDelistFamilyRulesBroken = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaNumberOfDelistFamilyRulesBroken));
            MetaCountOfProductsBuddied = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramMetaCountOfProductsBuddied));
        }

        /// <summary>
        /// Creates a new PlanogramDtoItem and populates its properties with values taken from a PlanogramDto.
        /// </summary>
        public PlanogramDtoItem(PlanogramDto dto)
        {
            Id = dto.Id;
            UniqueContentReference = dto.UniqueContentReference;
            PackageId = dto.PackageId;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            UserName = dto.UserName;
            LengthUnitsOfMeasure = dto.LengthUnitsOfMeasure;
            AreaUnitsOfMeasure = dto.AreaUnitsOfMeasure;
            VolumeUnitsOfMeasure = dto.VolumeUnitsOfMeasure;
            WeightUnitsOfMeasure = dto.WeightUnitsOfMeasure;
            AngleUnitsOfMeasure = dto.AngleUnitsOfMeasure;
            CurrencyUnitsOfMeasure = dto.CurrencyUnitsOfMeasure;
            ProductPlacementX = dto.ProductPlacementX;
            ProductPlacementY = dto.ProductPlacementY;
            ProductPlacementZ = dto.ProductPlacementZ;
            CategoryCode = dto.CategoryCode;
            CategoryName = dto.CategoryName;
            SourcePath = dto.SourcePath;
            MetaBayCount = dto.MetaBayCount;
            MetaUniqueProductCount = dto.MetaUniqueProductCount;
            MetaComponentCount = dto.MetaComponentCount;
            MetaTotalMerchandisableLinearSpace = dto.MetaTotalMerchandisableLinearSpace;
            MetaTotalMerchandisableAreaSpace = dto.MetaTotalMerchandisableAreaSpace;
            MetaTotalMerchandisableVolumetricSpace = dto.MetaTotalMerchandisableVolumetricSpace;
            MetaTotalLinearWhiteSpace = dto.MetaTotalLinearWhiteSpace;
            MetaTotalAreaWhiteSpace = dto.MetaTotalAreaWhiteSpace;
            MetaTotalVolumetricWhiteSpace = dto.MetaTotalVolumetricWhiteSpace;
            MetaProductsPlaced = dto.MetaProductsPlaced;
            MetaProductsUnplaced = dto.MetaProductsUnplaced;
            MetaNewProducts = dto.MetaNewProducts;
            MetaChangesFromPreviousCount = dto.MetaChangesFromPreviousCount;
            MetaChangeFromPreviousStarRating = dto.MetaChangeFromPreviousStarRating;
            MetaBlocksDropped = dto.MetaBlocksDropped;
            MetaNotAchievedInventory = dto.MetaNotAchievedInventory;
            MetaTotalFacings = dto.MetaTotalFacings;
            MetaAverageFacings = dto.MetaAverageFacings;
            MetaTotalUnits = dto.MetaTotalUnits;
            MetaAverageUnits = dto.MetaAverageUnits;
            MetaMinDos = dto.MetaMinDos;
            MetaMaxDos = dto.MetaMaxDos;
            MetaAverageDos = dto.MetaAverageDos;
            MetaMinCases = dto.MetaMinCases;
            MetaAverageCases = dto.MetaAverageCases;
            MetaMaxCases = dto.MetaMaxCases;
            MetaTotalComponentCollisions = dto.MetaTotalComponentCollisions;
            MetaTotalComponentsOverMerchandisedDepth = dto.MetaTotalComponentsOverMerchandisedDepth;
            MetaTotalComponentsOverMerchandisedHeight = dto.MetaTotalComponentsOverMerchandisedHeight;
            MetaTotalComponentsOverMerchandisedWidth = dto.MetaTotalComponentsOverMerchandisedWidth;
            MetaHasComponentsOutsideOfFixtureArea = dto.MetaHasComponentsOutsideOfFixtureArea;
            MetaTotalPositionCollisions = dto.MetaTotalPositionCollisions;
            MetaTotalFrontFacings = dto.MetaTotalFrontFacings;
            MetaAverageFrontFacings = dto.MetaAverageFrontFacings;
            LocationCode = dto.LocationCode;
            LocationName = dto.LocationName;
            ClusterSchemeName = dto.ClusterSchemeName;
            ClusterName = dto.ClusterName;
            MetaNoOfErrors = dto.MetaNoOfErrors;
            MetaNoOfWarnings = dto.MetaNoOfWarnings;
            MetaNoOfInformationEvents = dto.MetaNoOfInformationEvents;
            MetaNoOfDebugEvents = dto.MetaNoOfDebugEvents;
            MetaTotalErrorScore = dto.MetaTotalErrorScore;
            MetaHighestErrorScore = dto.MetaHighestErrorScore;
            Status = dto.Status;
            DateWip = dto.DateWip;
            DateApproved = dto.DateApproved;
            DateArchived = dto.DateArchived;
            MetaCountOfProductNotAchievedCases = dto.MetaCountOfProductNotAchievedCases;
            MetaCountOfProductNotAchievedDOS = dto.MetaCountOfProductNotAchievedDOS;
            MetaCountOfProductOverShelfLifePercent = dto.MetaCountOfProductOverShelfLifePercent;
            MetaCountOfProductNotAchievedDeliveries = dto.MetaCountOfProductNotAchievedDeliveries;
            MetaPercentOfProductNotAchievedCases = dto.MetaPercentOfProductNotAchievedCases;
            MetaPercentOfProductNotAchievedDOS = dto.MetaPercentOfProductNotAchievedDOS;
            MetaPercentOfProductOverShelfLifePercent = dto.MetaPercentOfProductOverShelfLifePercent;
            MetaPercentOfProductNotAchievedDeliveries = dto.MetaPercentOfProductNotAchievedDeliveries;
            MetaCountOfPositionsOutsideOfBlockSpace = dto.MetaCountOfPositionsOutsideOfBlockSpace;
            MetaPercentOfPositionsOutsideOfBlockSpace = dto.MetaPercentOfPositionsOutsideOfBlockSpace;
            MetaPercentOfPlacedProductsRecommendedInAssortment = dto.MetaPercentOfPlacedProductsRecommendedInAssortment;
            MetaCountOfPlacedProductsRecommendedInAssortment = dto.MetaCountOfPlacedProductsRecommendedInAssortment;
            MetaCountOfPlacedProductsNotRecommendedInAssortment = dto.MetaCountOfPlacedProductsNotRecommendedInAssortment;
            MetaCountOfRecommendedProductsInAssortment = dto.MetaCountOfRecommendedProductsInAssortment;
            PlanogramType = dto.PlanogramType;
            MetaPercentOfProductNotAchievedInventory = dto.MetaPercentOfProductNotAchievedInventory;
            HighlightSequenceStrategy = dto.HighlightSequenceStrategy;
            MetaNumberOfAssortmentRulesBroken = dto.MetaNumberOfAssortmentRulesBroken;
            MetaNumberOfProductRulesBroken = dto.MetaNumberOfProductRulesBroken;
            MetaNumberOfFamilyRulesBroken = dto.MetaNumberOfFamilyRulesBroken;
            MetaNumberOfInheritanceRulesBroken = dto.MetaNumberOfInheritanceRulesBroken;
            MetaNumberOfLocalProductRulesBroken = dto.MetaNumberOfLocalProductRulesBroken;
            MetaNumberOfDistributionRulesBroken = dto.MetaNumberOfDistributionRulesBroken;
            MetaNumberOfCoreRulesBroken = dto.MetaNumberOfCoreRulesBroken;
            MetaPercentageOfAssortmentRulesBroken = dto.MetaPercentageOfAssortmentRulesBroken;
            MetaPercentageOfProductRulesBroken = dto.MetaPercentageOfProductRulesBroken;
            MetaPercentageOfFamilyRulesBroken = dto.MetaPercentageOfFamilyRulesBroken;
            MetaPercentageOfInheritanceRulesBroken = dto.MetaPercentageOfInheritanceRulesBroken;
            MetaPercentageOfLocalProductRulesBroken = dto.MetaPercentageOfLocalProductRulesBroken;
            MetaPercentageOfDistributionRulesBroken = dto.MetaPercentageOfDistributionRulesBroken;
            MetaPercentageOfCoreRulesBroken = dto.MetaPercentageOfCoreRulesBroken;
            MetaNumberOfDelistProductRulesBroken = dto.MetaNumberOfDelistProductRulesBroken;
            MetaNumberOfForceProductRulesBroken = dto.MetaNumberOfForceProductRulesBroken;
            MetaNumberOfPreserveProductRulesBroken = dto.MetaNumberOfPreserveProductRulesBroken;
            MetaNumberOfMinimumHurdleProductRulesBroken = dto.MetaNumberOfMinimumHurdleProductRulesBroken;
            MetaNumberOfMaximumProductFamilyRulesBroken = dto.MetaNumberOfMaximumProductFamilyRulesBroken;
            MetaNumberOfMinimumProductFamilyRulesBroken = dto.MetaNumberOfMinimumProductFamilyRulesBroken;
            MetaNumberOfDependencyFamilyRulesBroken = dto.MetaNumberOfDependencyFamilyRulesBroken;
            MetaNumberOfDelistFamilyRulesBroken = dto.MetaNumberOfDelistFamilyRulesBroken;
            MetaCountOfProductsBuddied = dto.MetaCountOfProductsBuddied;
        }

        #endregion

        #region Public Properties

        Object IParentDtoItem<PlanogramDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IParentDtoItem<PlanogramDto>.AutoIncrementId
        {
            get { return true; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramDto CopyDto()
        {
            return new PlanogramDto()
            {
                Id = this.Id,
                UniqueContentReference = this.UniqueContentReference,
                PackageId = this.PackageId,
                Name = this.Name,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                UserName = this.UserName,
                LengthUnitsOfMeasure = this.LengthUnitsOfMeasure,
                AreaUnitsOfMeasure = this.AreaUnitsOfMeasure,
                VolumeUnitsOfMeasure = this.VolumeUnitsOfMeasure,
                WeightUnitsOfMeasure = this.WeightUnitsOfMeasure,
                AngleUnitsOfMeasure = this.AngleUnitsOfMeasure,
                CurrencyUnitsOfMeasure = this.CurrencyUnitsOfMeasure,
                ProductPlacementX = this.ProductPlacementX,
                ProductPlacementY = this.ProductPlacementY,
                ProductPlacementZ = this.ProductPlacementZ,
                CategoryCode = this.CategoryCode,
                CategoryName = this.CategoryName,
                SourcePath = this.SourcePath,
                MetaBayCount = this.MetaBayCount,
                MetaUniqueProductCount = this.MetaUniqueProductCount,
                MetaComponentCount = this.MetaComponentCount,
                MetaTotalMerchandisableLinearSpace = this.MetaTotalMerchandisableLinearSpace,
                MetaTotalMerchandisableAreaSpace = this.MetaTotalMerchandisableAreaSpace,
                MetaTotalMerchandisableVolumetricSpace = this.MetaTotalMerchandisableVolumetricSpace,
                MetaTotalLinearWhiteSpace = this.MetaTotalLinearWhiteSpace,
                MetaTotalAreaWhiteSpace = this.MetaTotalAreaWhiteSpace,
                MetaTotalVolumetricWhiteSpace = this.MetaTotalVolumetricWhiteSpace,
                MetaProductsPlaced = this.MetaProductsPlaced,
                MetaProductsUnplaced = this.MetaProductsUnplaced,
                MetaNewProducts = this.MetaNewProducts,
                MetaChangesFromPreviousCount = this.MetaChangesFromPreviousCount,
                MetaChangeFromPreviousStarRating = this.MetaChangeFromPreviousStarRating,
                MetaBlocksDropped = this.MetaBlocksDropped,
                MetaNotAchievedInventory = this.MetaNotAchievedInventory,
                MetaTotalFacings = this.MetaTotalFacings,
                MetaAverageFacings = this.MetaAverageFacings,
                MetaTotalUnits = this.MetaTotalUnits,
                MetaAverageUnits = this.MetaAverageUnits,
                MetaMinDos = this.MetaMinDos,
                MetaMaxDos = this.MetaMaxDos,
                MetaAverageDos = this.MetaAverageDos,
                MetaMinCases = this.MetaMinCases,
                MetaAverageCases = this.MetaAverageCases,
                MetaMaxCases = this.MetaMaxCases,
                MetaTotalComponentCollisions = this.MetaTotalComponentCollisions,
                MetaTotalComponentsOverMerchandisedDepth = this.MetaTotalComponentsOverMerchandisedDepth,
                MetaTotalComponentsOverMerchandisedHeight = this.MetaTotalComponentsOverMerchandisedHeight,
                MetaTotalComponentsOverMerchandisedWidth = this.MetaTotalComponentsOverMerchandisedWidth,
                MetaHasComponentsOutsideOfFixtureArea = this.MetaHasComponentsOutsideOfFixtureArea,
                MetaTotalPositionCollisions = this.MetaTotalPositionCollisions,
                MetaTotalFrontFacings = this.MetaTotalFrontFacings,
                MetaAverageFrontFacings = this.MetaAverageFrontFacings,
                LocationCode = this.LocationCode,
                LocationName = this.LocationName,
                ClusterSchemeName = this.ClusterSchemeName,
                ClusterName = this.ClusterName,
                MetaNoOfErrors = this.MetaNoOfErrors,
                MetaNoOfWarnings = this.MetaNoOfWarnings,
                MetaNoOfInformationEvents = this.MetaNoOfInformationEvents,
                MetaNoOfDebugEvents = this.MetaNoOfDebugEvents,
                MetaTotalErrorScore = this.MetaTotalErrorScore,
                MetaHighestErrorScore = this.MetaHighestErrorScore,
                Status = this.Status,
                DateWip = this.DateWip,
                DateApproved = this.DateApproved,
                DateArchived = this.DateArchived,
                MetaCountOfProductNotAchievedCases = this.MetaCountOfProductNotAchievedCases,
                MetaCountOfProductNotAchievedDOS = this.MetaCountOfProductNotAchievedDOS,
                MetaCountOfProductOverShelfLifePercent = this.MetaCountOfProductOverShelfLifePercent,
                MetaCountOfProductNotAchievedDeliveries = this.MetaCountOfProductNotAchievedDeliveries,
                MetaPercentOfProductNotAchievedCases = this.MetaPercentOfProductNotAchievedCases,
                MetaPercentOfProductNotAchievedDOS = this.MetaPercentOfProductNotAchievedDOS,
                MetaPercentOfProductOverShelfLifePercent = this.MetaPercentOfProductOverShelfLifePercent,
                MetaPercentOfProductNotAchievedDeliveries = this.MetaPercentOfProductNotAchievedDeliveries,
                MetaCountOfPositionsOutsideOfBlockSpace = this.MetaCountOfPositionsOutsideOfBlockSpace,
                MetaPercentOfPositionsOutsideOfBlockSpace = this.MetaPercentOfPositionsOutsideOfBlockSpace,
                MetaPercentOfPlacedProductsRecommendedInAssortment = this.MetaPercentOfPlacedProductsRecommendedInAssortment,
                MetaCountOfPlacedProductsRecommendedInAssortment = this.MetaCountOfPlacedProductsRecommendedInAssortment,
                MetaCountOfPlacedProductsNotRecommendedInAssortment = this.MetaCountOfPlacedProductsNotRecommendedInAssortment,
                MetaCountOfRecommendedProductsInAssortment = this.MetaCountOfRecommendedProductsInAssortment,
                PlanogramType = this.PlanogramType,
                MetaPercentOfProductNotAchievedInventory = this.MetaPercentOfProductNotAchievedInventory,
                HighlightSequenceStrategy = this.HighlightSequenceStrategy,
                MetaNumberOfAssortmentRulesBroken = this.MetaNumberOfAssortmentRulesBroken,
                MetaNumberOfProductRulesBroken = this.MetaNumberOfProductRulesBroken,
                MetaNumberOfFamilyRulesBroken = this.MetaNumberOfFamilyRulesBroken,
                MetaNumberOfInheritanceRulesBroken = this.MetaNumberOfInheritanceRulesBroken,
                MetaNumberOfLocalProductRulesBroken = this.MetaNumberOfLocalProductRulesBroken,
                MetaNumberOfDistributionRulesBroken = this.MetaNumberOfDistributionRulesBroken,
                MetaNumberOfCoreRulesBroken = this.MetaNumberOfCoreRulesBroken,
                MetaPercentageOfAssortmentRulesBroken = this.MetaPercentageOfAssortmentRulesBroken,
                MetaPercentageOfProductRulesBroken = this.MetaPercentageOfProductRulesBroken,
                MetaPercentageOfFamilyRulesBroken = this.MetaPercentageOfFamilyRulesBroken,
                MetaPercentageOfInheritanceRulesBroken = this.MetaPercentageOfInheritanceRulesBroken,
                MetaPercentageOfLocalProductRulesBroken = this.MetaPercentageOfLocalProductRulesBroken,
                MetaPercentageOfDistributionRulesBroken = this.MetaPercentageOfDistributionRulesBroken,
                MetaPercentageOfCoreRulesBroken = this.MetaPercentageOfCoreRulesBroken,
                MetaNumberOfDelistProductRulesBroken = this.MetaNumberOfDelistProductRulesBroken,
                MetaNumberOfForceProductRulesBroken = this.MetaNumberOfForceProductRulesBroken,
                MetaNumberOfPreserveProductRulesBroken = this.MetaNumberOfPreserveProductRulesBroken,
                MetaNumberOfMinimumHurdleProductRulesBroken = this.MetaNumberOfMinimumHurdleProductRulesBroken,
                MetaNumberOfMaximumProductFamilyRulesBroken = this.MetaNumberOfMaximumProductFamilyRulesBroken,
                MetaNumberOfMinimumProductFamilyRulesBroken = this.MetaNumberOfMinimumProductFamilyRulesBroken,
                MetaNumberOfDependencyFamilyRulesBroken = this.MetaNumberOfDependencyFamilyRulesBroken,
                MetaNumberOfDelistFamilyRulesBroken = this.MetaNumberOfDelistFamilyRulesBroken,
                MetaCountOfProductsBuddied = this.MetaCountOfProductsBuddied
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramId:
                    return Id;
                case FieldNames.PlanogramUniqueContentReference:
                    return UniqueContentReference;
                case FieldNames.PlanogramPackageId:
                    return PackageId;
                case FieldNames.PlanogramName:
                    return Name;
                case FieldNames.PlanogramHeight:
                    return Height;
                case FieldNames.PlanogramWidth:
                    return Width;
                case FieldNames.PlanogramDepth:
                    return Depth;
                case FieldNames.PlanogramUserName:
                    return UserName;
                case FieldNames.PlanogramLengthUnitsOfMeasure:
                    return LengthUnitsOfMeasure;
                case FieldNames.PlanogramAreaUnitsOfMeasure:
                    return AreaUnitsOfMeasure;
                case FieldNames.PlanogramVolumeUnitsOfMeasure:
                    return VolumeUnitsOfMeasure;
                case FieldNames.PlanogramWeightUnitsOfMeasure:
                    return WeightUnitsOfMeasure;
                case FieldNames.PlanogramAngleUnitsOfMeasure:
                    return AngleUnitsOfMeasure;
                case FieldNames.PlanogramCurrencyUnitsOfMeasure:
                    return CurrencyUnitsOfMeasure;
                case FieldNames.PlanogramProductPlacementX:
                    return ProductPlacementX;
                case FieldNames.PlanogramProductPlacementY:
                    return ProductPlacementY;
                case FieldNames.PlanogramProductPlacementZ:
                    return ProductPlacementZ;
                case FieldNames.PlanogramCategoryCode:
                    return CategoryCode;
                case FieldNames.PlanogramCategoryName:
                    return CategoryName;
                case FieldNames.PlanogramSourcePath:
                    return SourcePath;
                case FieldNames.PlanogramMetaBayCount:
                    return MetaBayCount;
                case FieldNames.PlanogramMetaUniqueProductCount:
                    return MetaUniqueProductCount;
                case FieldNames.PlanogramMetaComponentCount:
                    return MetaComponentCount;
                case FieldNames.PlanogramMetaTotalMerchandisableLinearSpace:
                    return MetaTotalMerchandisableLinearSpace;
                case FieldNames.PlanogramMetaTotalMerchandisableAreaSpace:
                    return MetaTotalMerchandisableAreaSpace;
                case FieldNames.PlanogramMetaTotalMerchandisableVolumetricSpace:
                    return MetaTotalMerchandisableVolumetricSpace;
                case FieldNames.PlanogramMetaTotalLinearWhiteSpace:
                    return MetaTotalLinearWhiteSpace;
                case FieldNames.PlanogramMetaTotalAreaWhiteSpace:
                    return MetaTotalAreaWhiteSpace;
                case FieldNames.PlanogramMetaTotalVolumetricWhiteSpace:
                    return MetaTotalVolumetricWhiteSpace;
                case FieldNames.PlanogramMetaProductsPlaced:
                    return MetaProductsPlaced;
                case FieldNames.PlanogramMetaProductsUnplaced:
                    return MetaProductsUnplaced;
                case FieldNames.PlanogramMetaNewProducts:
                    return MetaNewProducts;
                case FieldNames.PlanogramMetaChangesFromPreviousCount:
                    return MetaChangesFromPreviousCount;
                case FieldNames.PlanogramMetaChangeFromPreviousStarRating:
                    return MetaChangeFromPreviousStarRating;
                case FieldNames.PlanogramMetaBlocksDropped:
                    return MetaBlocksDropped;
                case FieldNames.PlanogramMetaNotAchievedInventory:
                    return MetaNotAchievedInventory;
                case FieldNames.PlanogramMetaTotalFacings:
                    return MetaTotalFacings;
                case FieldNames.PlanogramMetaAverageFacings:
                    return MetaAverageFacings;
                case FieldNames.PlanogramMetaTotalUnits:
                    return MetaTotalUnits;
                case FieldNames.PlanogramMetaAverageUnits:
                    return MetaAverageUnits;
                case FieldNames.PlanogramMetaMinDos:
                    return MetaMinDos;
                case FieldNames.PlanogramMetaMaxDos:
                    return MetaMaxDos;
                case FieldNames.PlanogramMetaAverageDos:
                    return MetaAverageDos;
                case FieldNames.PlanogramMetaMinCases:
                    return MetaMinCases;
                case FieldNames.PlanogramMetaAverageCases:
                    return MetaAverageCases;
                case FieldNames.PlanogramMetaMaxCases:
                    return MetaMaxCases;
                case FieldNames.PlanogramMetaTotalComponentCollisions:
                    return MetaTotalComponentCollisions;
                case FieldNames.PlanogramMetaTotalComponentsOverMerchandisedDepth:
                    return MetaTotalComponentsOverMerchandisedDepth;
                case FieldNames.PlanogramMetaTotalComponentsOverMerchandisedHeight:
                    return MetaTotalComponentsOverMerchandisedHeight;
                case FieldNames.PlanogramMetaTotalComponentsOverMerchandisedWidth:
                    return MetaTotalComponentsOverMerchandisedWidth;
                case FieldNames.PlanogramMetaHasComponentsOutsideOfFixtureArea:
                    return MetaHasComponentsOutsideOfFixtureArea;
                case FieldNames.PlanogramMetaTotalPositionCollisions:
                    return MetaTotalPositionCollisions;
                case FieldNames.PlanogramMetaTotalFrontFacings:
                    return MetaTotalFrontFacings;
                case FieldNames.PlanogramMetaAverageFrontFacings:
                    return MetaAverageFrontFacings;
                case FieldNames.PlanogramLocationCode:
                    return LocationCode;
                case FieldNames.PlanogramLocationName:
                    return LocationName;
                case FieldNames.PlanogramClusterSchemeName:
                    return ClusterSchemeName;
                case FieldNames.PlanogramClusterName:
                    return ClusterName;
                case FieldNames.PlanogramMetaNoOfErrors:
                    return MetaNoOfErrors;
                case FieldNames.PlanogramMetaNoOfWarnings:
                    return MetaNoOfWarnings;
                case FieldNames.PlanogramMetaNoOfInformationEvents:
                    return MetaNoOfInformationEvents;
                case FieldNames.PlanogramMetaNoOfDebugEvents:
                    return MetaNoOfDebugEvents;
                case FieldNames.PlanogramMetaTotalErrorScore:
                    return MetaTotalErrorScore;
                case FieldNames.PlanogramMetaHighestErrorScore:
                    return MetaHighestErrorScore;
                case FieldNames.PlanogramStatus:
                    return Status;
                case FieldNames.PlanogramDateWip:
                    return DateWip;
                case FieldNames.PlanogramDateApproved:
                    return DateApproved;
                case FieldNames.PlanogramDateArchived:
                    return DateArchived;
                case FieldNames.PlanogramMetaCountOfProductNotAchievedCases:
                    return MetaCountOfProductNotAchievedCases;
                case FieldNames.PlanogramMetaCountOfProductNotAchievedDOS:
                    return MetaCountOfProductNotAchievedDOS;
                case FieldNames.PlanogramMetaCountOfProductOverShelfLifePercent:
                    return MetaCountOfProductOverShelfLifePercent;
                case FieldNames.PlanogramMetaCountOfProductNotAchievedDeliveries:
                    return MetaCountOfProductNotAchievedDeliveries;
                case FieldNames.PlanogramMetaPercentOfProductNotAchievedCases:
                    return MetaPercentOfProductNotAchievedCases;
                case FieldNames.PlanogramMetaPercentOfProductNotAchievedDOS:
                    return MetaPercentOfProductNotAchievedDOS;
                case FieldNames.PlanogramMetaPercentOfProductOverShelfLifePercent:
                    return MetaPercentOfProductOverShelfLifePercent;
                case FieldNames.PlanogramMetaPercentOfProductNotAchievedDeliveries:
                    return MetaPercentOfProductNotAchievedDeliveries;
                case FieldNames.PlanogramMetaCountOfPositionsOutsideOfBlockSpace:
                    return MetaCountOfPositionsOutsideOfBlockSpace;
                case FieldNames.PlanogramMetaPercentOfPositionsOutsideOfBlockSpace:
                    return MetaPercentOfPositionsOutsideOfBlockSpace;
                case FieldNames.PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment:
                    return MetaPercentOfPlacedProductsRecommendedInAssortment;
                case FieldNames.PlanogramMetaCountOfPlacedProductsRecommendedInAssortment:
                    return MetaCountOfPlacedProductsRecommendedInAssortment;
                case FieldNames.PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment:
                    return MetaCountOfPlacedProductsNotRecommendedInAssortment;
                case FieldNames.PlanogramMetaCountOfRecommendedProductsInAssortment:
                    return MetaCountOfRecommendedProductsInAssortment;
                case FieldNames.PlanogramPlanogramType:
                    return PlanogramType;
                case FieldNames.PlanogramMetaPercentOfProductNotAchievedInventory:
                    return MetaPercentOfProductNotAchievedInventory;
                case FieldNames.PlanogramHighlightSequenceStrategy:
                    return HighlightSequenceStrategy;
                case FieldNames.PlanogramMetaNumberOfAssortmentRulesBroken:
                    return MetaNumberOfAssortmentRulesBroken;
                case FieldNames.PlanogramMetaNumberOfProductRulesBroken:
                    return MetaNumberOfProductRulesBroken;
                case FieldNames.PlanogramMetaNumberOfFamilyRulesBroken:
                    return MetaNumberOfFamilyRulesBroken;
                case FieldNames.PlanogramMetaNumberOfInheritanceRulesBroken:
                    return MetaNumberOfInheritanceRulesBroken;
                case FieldNames.PlanogramMetaNumberOfLocalProductRulesBroken:
                    return MetaNumberOfLocalProductRulesBroken;
                case FieldNames.PlanogramMetaNumberOfDistributionRulesBroken:
                    return MetaNumberOfDistributionRulesBroken;
                case FieldNames.PlanogramMetaNumberOfCoreRulesBroken:
                    return MetaNumberOfCoreRulesBroken;
                case FieldNames.PlanogramMetaPercentageOfAssortmentRulesBroken:
                    return MetaPercentageOfAssortmentRulesBroken;
                case FieldNames.PlanogramMetaPercentageOfProductRulesBroken:
                    return MetaPercentageOfProductRulesBroken;
                case FieldNames.PlanogramMetaPercentageOfFamilyRulesBroken:
                    return MetaPercentageOfFamilyRulesBroken;
                case FieldNames.PlanogramMetaPercentageOfInheritanceRulesBroken:
                    return MetaPercentageOfInheritanceRulesBroken;
                case FieldNames.PlanogramMetaPercentageOfLocalProductRulesBroken:
                    return MetaPercentageOfLocalProductRulesBroken;
                case FieldNames.PlanogramMetaPercentageOfDistributionRulesBroken:
                    return MetaPercentageOfDistributionRulesBroken;
                case FieldNames.PlanogramMetaPercentageOfCoreRulesBroken:
                    return MetaPercentageOfCoreRulesBroken;
                case FieldNames.PlanogramMetaNumberOfDelistProductRulesBroken:
                    return MetaNumberOfDelistProductRulesBroken;
                case FieldNames.PlanogramMetaNumberOfForceProductRulesBroken:
                    return MetaNumberOfForceProductRulesBroken;
                case FieldNames.PlanogramMetaNumberOfPreserveProductRulesBroken:
                    return MetaNumberOfPreserveProductRulesBroken;
                case FieldNames.PlanogramMetaNumberOfMinimumHurdleProductRulesBroken:
                    return MetaNumberOfMinimumHurdleProductRulesBroken;
                case FieldNames.PlanogramMetaNumberOfMaximumProductFamilyRulesBroken:
                    return MetaNumberOfMaximumProductFamilyRulesBroken;
                case FieldNames.PlanogramMetaNumberOfMinimumProductFamilyRulesBroken:
                    return MetaNumberOfMinimumProductFamilyRulesBroken;
                case FieldNames.PlanogramMetaNumberOfDependencyFamilyRulesBroken:
                    return MetaNumberOfDependencyFamilyRulesBroken;
                case FieldNames.PlanogramMetaNumberOfDelistFamilyRulesBroken:
                    return MetaNumberOfDelistFamilyRulesBroken;
                case FieldNames.PlanogramMetaCountOfProductsBuddied:
                    return MetaCountOfProductsBuddied;

                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
