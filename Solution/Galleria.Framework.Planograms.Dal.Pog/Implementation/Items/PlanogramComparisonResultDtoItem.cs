﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramComparisonResultDtoItem : PlanogramComparisonResultDto, IChildDtoItem<PlanogramComparisonResultDto>
    {
        #region Public Properties

        Object IChildDtoItem<PlanogramComparisonResultDto>.Id { get { return Id; } set { Id = value; } }

        Boolean IChildDtoItem<PlanogramComparisonResultDto>.AutoIncrementId { get { return true; } }

        Object IChildDtoItem<PlanogramComparisonResultDto>.ParentId { get { return PlanogramComparisonId; } }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonResultDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonResultDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonResultId));
            PlanogramComparisonId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonResultPlanogramComparisonId));
            PlanogramName = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonResultPlanogramName);
            Status = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramComparisonResultStatus));
        }

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonResultDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonResultDto" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonResultDtoItem(PlanogramComparisonResultDto dto)
        {
            Id = dto.Id;
            PlanogramComparisonId = dto.PlanogramComparisonId;
            PlanogramName = dto.PlanogramName;
            Status = dto.Status;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramComparisonResultDto CopyDto()
        {
            return new PlanogramComparisonResultDto
                   {
                       Id = Id,
                       PlanogramComparisonId = PlanogramComparisonId,
                       PlanogramName = PlanogramName,
                       Status = Status
                   };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramComparisonResultId:
                    return Id;
                case FieldNames.PlanogramComparisonResultPlanogramComparisonId:
                    return PlanogramComparisonId;
                case FieldNames.PlanogramComparisonResultPlanogramName:
                    return PlanogramName;
                case FieldNames.PlanogramComparisonResultStatus:
                    return Status;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}