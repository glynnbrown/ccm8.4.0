﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25916 : N.Foster
//  Added RowVersion, DateDeleted. Removed CreateBy, LastModifiedBy, Description
// V8-25949 : N.Foster
//  Changed PackageId to be String
// V8-25881 : A.Probyn
//  Replaced PlanogramCount with MetaPlanogramCount
//  Added DateMetadataCalculated
// V8-27237 : A.Silva   ~ Added DateValidationCalculated.
// V8-28840 : L.Luong 
//  Added EntityId

#endregion

#region Version History: CCM802

// V8-28840 : L.Luong 
//  Added EntityId

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PackageDtoItem : PackageDto, IParentDtoItem<PackageDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PackageDtoItem and populates its properties with values taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PackageDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = (String)item.GetItemPropertyValue(FieldNames.PackageId);
            RowVersion = GalleriaBinaryFileItemHelper.GetRowVersion(item.GetItemPropertyValue(FieldNames.PackageRowVersion));
            EntityId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PackageEntityId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PackageName);
            DateCreated = GalleriaBinaryFileItemHelper.GetDateTime(item.GetItemPropertyValue(FieldNames.PackageDateCreated));
            DateLastModified = GalleriaBinaryFileItemHelper.GetDateTime(item.GetItemPropertyValue(FieldNames.PackageDateLastModified));
            DateDeleted = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PackageDateDeleted));
            MetaPlanogramCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PackageMetaPlanogramCount));
            DateMetadataCalculated = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PackageDateMetadataCalculated));
            DateValidationDataCalculated = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PackageDateValidationDataCalculated));
        }

        /// <summary>
        /// Creates a new PackageDtoItem and populates its properties with values taken from a PackageDto.
        /// </summary>
        public PackageDtoItem(PackageDto dto)
        {
            Id = dto.Id;
            RowVersion = dto.RowVersion;
            EntityId = dto.EntityId;
            Name = dto.Name;
            DateCreated = dto.DateCreated;
            DateLastModified = dto.DateLastModified;
            DateDeleted = dto.DateDeleted;
            MetaPlanogramCount = dto.MetaPlanogramCount;
            DateMetadataCalculated = dto.DateMetadataCalculated;
            DateValidationDataCalculated = dto.DateValidationDataCalculated;
        }

        #endregion

        #region Public Properties

        Object IParentDtoItem<PackageDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IParentDtoItem<PackageDto>.AutoIncrementId
        {
            get { return false; }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PackageDto CopyDto()
        {
            return new PackageDto()
            {
                Id = this.Id,
                RowVersion = this.RowVersion,
                EntityId = this.EntityId,
                Name = this.Name,
                DateCreated = this.DateCreated,
                DateLastModified = this.DateLastModified,
                DateDeleted = this.DateDeleted,
                MetaPlanogramCount = this.MetaPlanogramCount,
                DateMetadataCalculated = this.DateMetadataCalculated,
                DateValidationDataCalculated = this.DateValidationDataCalculated
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PackageId:
                    return Id;
                case FieldNames.PackageRowVersion:
                    return RowVersion;
                case FieldNames.PackageEntityId:
                    return EntityId;
                case FieldNames.PackageName:
                    return Name;
                case FieldNames.PackageDateCreated:
                    return DateCreated;
                case FieldNames.PackageDateLastModified:
                    return DateLastModified;
                case FieldNames.PackageMetaPlanogramCount:
                    return MetaPlanogramCount;
                case FieldNames.PackageDateMetadataCalculated:
                    return DateMetadataCalculated;
                case FieldNames.PackageDateValidationDataCalculated:
                    return DateValidationDataCalculated;
                default:
                    return null;
            }
        }
        #endregion

        #endregion
    }
}
