﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
// V8-25949 : N.Foster
//  Allowed id's to be something other than Int32
#endregion
#endregion

using System;
using Galleria.Framework.IO;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    /// <summary>
    /// The IChildDtoItem interface defines the contract for a wrapper around a DTO to be cached by the POG DAL in
    /// such a way that it can be easily and quickly retrieved by parent ID.
    /// </summary>
    /// <typeparam name="DTO">The DTO type being wrapped.</typeparam>
    public interface IChildDtoItem<DTO> : GalleriaBinaryFile.IItem
    {
        #region Properties

        /// <summary>
        /// The ID of the item.
        /// </summary>
        Object Id { get; set; }

        /// <summary>
        /// Indicates
        /// </summary>
        Boolean AutoIncrementId { get; }

        /// <summary>
        /// The ID of the item's parent.
        /// </summary>
        Object ParentId { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a DTO that is a copy of this item.
        /// </summary>
        DTO CopyDto();

        #endregion
    }
}
