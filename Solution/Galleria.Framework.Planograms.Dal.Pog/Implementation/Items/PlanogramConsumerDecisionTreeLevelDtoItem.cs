﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramConsumerDecisionTreeLevelDtoItem : PlanogramConsumerDecisionTreeLevelDto, IChildDtoItem<PlanogramConsumerDecisionTreeLevelDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeLevelDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramConsumerDecisionTreeLevelDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeLevelId));
            PlanogramConsumerDecisionTreeId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId));
            ParentLevelId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeLevelParentLevelId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeLevelName);
        }

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeLevelDtoItem and populates its properties with values taken from a 
        /// PlanogramConsumerDecisionTreeLevelDto.
        /// </summary>
        public PlanogramConsumerDecisionTreeLevelDtoItem(PlanogramConsumerDecisionTreeLevelDto dto)
        {
            Id = dto.Id;
            PlanogramConsumerDecisionTreeId = dto.PlanogramConsumerDecisionTreeId;
            ParentLevelId = dto.ParentLevelId;
            Name = dto.Name;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramConsumerDecisionTreeLevelDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramConsumerDecisionTreeLevelDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramConsumerDecisionTreeLevelDto>.ParentId
        {
            get { return PlanogramConsumerDecisionTreeId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramConsumerDecisionTreeLevelDto CopyDto()
        {
            return new PlanogramConsumerDecisionTreeLevelDto()
            {
                Id = this.Id,
                PlanogramConsumerDecisionTreeId = this.PlanogramConsumerDecisionTreeId,
                ParentLevelId = this.ParentLevelId,
                Name = this.Name
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramConsumerDecisionTreeLevelId:
                    return Id;
                case FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId:
                    return PlanogramConsumerDecisionTreeId;
                case FieldNames.PlanogramConsumerDecisionTreeLevelParentLevelId:
                    return ParentLevelId;
                case FieldNames.PlanogramConsumerDecisionTreeLevelName:
                    return Name;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
