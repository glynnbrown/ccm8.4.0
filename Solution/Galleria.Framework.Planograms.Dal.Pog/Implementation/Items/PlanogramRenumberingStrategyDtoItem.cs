﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-27153 : A.Silva   ~ Created.
#endregion
#region Version History: (CCM 8.2)
// V8-30193 : L.Ineson
//  Added IsEnabled property
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    /// <summary>
    ///     DTO item for <see cref="PlanogramRenumberingStrategyDto" />.
    /// </summary>
    public class PlanogramRenumberingStrategyDtoItem : PlanogramRenumberingStrategyDto,
        IChildDtoItem<PlanogramRenumberingStrategyDto>
    {
        #region Constructors

        /// <summary>
        ///     Creates a new <see cref="PlanogramRenumberingStrategyDtoItem"/> and populates it with values from a <see cref="GalleriaBinaryFile.IItem"/>.
        /// </summary>
        /// <param name="item">The item containing the values to load into the new instance.</param>
        public PlanogramRenumberingStrategyDtoItem(GalleriaBinaryFile.IItem item)
        {
            this.Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyId));
            this.PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyPlanogramId));
            this.Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyName);
            this.BayComponentXRenumberStrategyPriority = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority));
            this.BayComponentYRenumberStrategyPriority = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority));
            this.BayComponentZRenumberStrategyPriority = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority));
            this.PositionXRenumberStrategyPriority = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategyPriority));
            this.PositionYRenumberStrategyPriority = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategyPriority));
            this.PositionZRenumberStrategyPriority = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategyPriority));
            this.BayComponentXRenumberStrategy = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategy));
            this.BayComponentYRenumberStrategy = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategy));
            this.BayComponentZRenumberStrategy = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategy));
            this.PositionXRenumberStrategy = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategy));
            this.PositionYRenumberStrategy = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategy));
            this.PositionZRenumberStrategy = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategy));
            this.RestartComponentRenumberingPerBay = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerBay));
            this.IgnoreNonMerchandisingComponents = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents));
            this.RestartPositionRenumberingPerComponent = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent));
            this.UniqueNumberMultiPositionProductsPerComponent = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent));
            this.ExceptAdjacentPositions = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyExceptAdjacentPositions));
            this.IsEnabled = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramRenumberingStrategyIsEnabled));
        }

        /// <summary>
        ///     Creates a new <see cref="PlanogramRenumberingStrategyDtoItem"/> and populates it with values from a <see cref="PlanogramRenumberingStrategyDto"/>.
        /// </summary>
        /// <param name="dto">The dto containing the values to load into the new instance.</param>
        public PlanogramRenumberingStrategyDtoItem(PlanogramRenumberingStrategyDto dto)
        {
            this.Id = dto.Id;
            this.PlanogramId = dto.PlanogramId;
            this.Name = dto.Name;
            this.BayComponentXRenumberStrategyPriority = dto.BayComponentXRenumberStrategyPriority;
            this.BayComponentYRenumberStrategyPriority = dto.BayComponentYRenumberStrategyPriority;
            this.BayComponentZRenumberStrategyPriority = dto.BayComponentZRenumberStrategyPriority;
            this.PositionXRenumberStrategyPriority = dto.PositionXRenumberStrategyPriority;
            this.PositionYRenumberStrategyPriority = dto.PositionYRenumberStrategyPriority;
            this.PositionZRenumberStrategyPriority = dto.PositionZRenumberStrategyPriority;
            this.BayComponentXRenumberStrategy = dto.BayComponentXRenumberStrategy;
            this.BayComponentYRenumberStrategy = dto.BayComponentYRenumberStrategy;
            this.BayComponentZRenumberStrategy = dto.BayComponentZRenumberStrategy;
            this.PositionXRenumberStrategy = dto.PositionXRenumberStrategy;
            this.PositionYRenumberStrategy = dto.PositionYRenumberStrategy;
            this.PositionZRenumberStrategy = dto.PositionZRenumberStrategy;
            this.RestartComponentRenumberingPerBay = dto.RestartComponentRenumberingPerBay;
            this.IgnoreNonMerchandisingComponents = dto.IgnoreNonMerchandisingComponents;
            this.RestartPositionRenumberingPerComponent = dto.RestartPositionRenumberingPerComponent;
            this.UniqueNumberMultiPositionProductsPerComponent = dto.UniqueNumberMultiPositionProductsPerComponent;
            this.ExceptAdjacentPositions = dto.ExceptAdjacentPositions;
            this.IsEnabled = dto.IsEnabled;
        }

        #endregion

        #region IChildDtoItem<PlanogramRenumberingStrategyDto> Members

        Boolean IChildDtoItem<PlanogramRenumberingStrategyDto>.AutoIncrementId
        {
            get { return true; }
        }

        /// <summary>
        ///     Creates a <see cref="PlanogramRenumberingStrategyDto" /> with values from this instance.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramRenumberingStrategyDto" /> for this instance.</returns>
        public PlanogramRenumberingStrategyDto CopyDto()
        {
            return new PlanogramRenumberingStrategyDto
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Name = this.Name,
                BayComponentXRenumberStrategyPriority = this.BayComponentXRenumberStrategyPriority,
                BayComponentYRenumberStrategyPriority = this.BayComponentYRenumberStrategyPriority,
                BayComponentZRenumberStrategyPriority = this.BayComponentZRenumberStrategyPriority,
                PositionXRenumberStrategyPriority = this.PositionXRenumberStrategyPriority,
                PositionYRenumberStrategyPriority = this.PositionYRenumberStrategyPriority,
                PositionZRenumberStrategyPriority = this.PositionZRenumberStrategyPriority,
                BayComponentXRenumberStrategy = this.BayComponentXRenumberStrategy,
                BayComponentYRenumberStrategy = this.BayComponentYRenumberStrategy,
                BayComponentZRenumberStrategy = this.BayComponentZRenumberStrategy,
                PositionXRenumberStrategy = this.PositionXRenumberStrategy,
                PositionYRenumberStrategy = this.PositionYRenumberStrategy,
                PositionZRenumberStrategy = this.PositionZRenumberStrategy,
                RestartComponentRenumberingPerBay = this.RestartComponentRenumberingPerBay,
                IgnoreNonMerchandisingComponents = this.IgnoreNonMerchandisingComponents,
                RestartPositionRenumberingPerComponent = this.RestartPositionRenumberingPerComponent,
                UniqueNumberMultiPositionProductsPerComponent = this.UniqueNumberMultiPositionProductsPerComponent,
                ExceptAdjacentPositions = this.ExceptAdjacentPositions,
                IsEnabled = this.IsEnabled
            };
        }

        /// <summary>
        ///     Get the value of the property matching the given <paramref name="itemPropertyName" />.
        /// </summary>
        /// <param name="itemPropertyName">Name of the property to return a value from.</param>
        /// <returns>Returns the value of the property with <see cref="itemPropertyName" />.</returns>
        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramRenumberingStrategyId:
                    return Id;
                case FieldNames.PlanogramRenumberingStrategyPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramRenumberingStrategyName:
                    return Name;
                case FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority:
                    return BayComponentXRenumberStrategyPriority;
                case FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority:
                    return BayComponentYRenumberStrategyPriority;
                case FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority:
                    return BayComponentZRenumberStrategyPriority;
                case FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategyPriority:
                    return PositionXRenumberStrategyPriority;
                case FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategyPriority:
                    return PositionYRenumberStrategyPriority;
                case FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategyPriority:
                    return PositionZRenumberStrategyPriority;
                case FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategy:
                    return BayComponentXRenumberStrategy;
                case FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategy:
                    return BayComponentYRenumberStrategy;
                case FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategy:
                    return BayComponentZRenumberStrategy;
                case FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategy:
                    return PositionXRenumberStrategy;
                case FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategy:
                    return PositionYRenumberStrategy;
                case FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategy:
                    return PositionZRenumberStrategy;
                case FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerBay:
                    return RestartComponentRenumberingPerBay;
                case FieldNames.PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents:
                    return IgnoreNonMerchandisingComponents;
                case FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent:
                    return RestartPositionRenumberingPerComponent;
                case FieldNames.PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent:
                    return UniqueNumberMultiPositionProductsPerComponent;
                case FieldNames.PlanogramRenumberingStrategyExceptAdjacentPositions:
                    return ExceptAdjacentPositions;
                case FieldNames.PlanogramRenumberingStrategyIsEnabled:
                    return IsEnabled;
                default:
                    return null;
            }
        }

        Object IChildDtoItem<PlanogramRenumberingStrategyDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Object IChildDtoItem<PlanogramRenumberingStrategyDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion
    }
}