﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-24971 : L.Hodson
//  Changes to structure
// V8-25700 : L.Ineson
//  Null id fields no longer return as 0.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27474 : A.Silva
//      Added PositionSequenceNumber.

#endregion

#region Version History: CCM802
// V8-28766 : J.Pickup
//  IsManuallyPlaced property introduced.
// V8-29028 : L.Ineson
//  Added squeeze properties.
// V8-29054 : M.Pettit
//  Added Meta validation properties
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
// V8-30011 : L.Ineson
//  Added addition linear, area and volumetric metadata.
#endregion

#region Version History: CCM830
// V8-31947 : A.Silva
//  Added MetaComparisonStatus field.
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName & MetaSequenceGroupName.
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramPositionDtoItem : PlanogramPositionDto, IChildDtoItem<PlanogramPositionDto>
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramPositionDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramPositionDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPositionId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramId));
            PlanogramFixtureItemId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramFixtureItemId));
            PlanogramFixtureAssemblyId = item.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramFixtureAssemblyId);
            PlanogramAssemblyComponentId = item.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramAssemblyComponentId);
            PlanogramFixtureComponentId = item.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramFixtureComponentId);
            PlanogramSubComponentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramSubComponentId));
            PlanogramProductId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPositionPlanogramProductId));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionRoll));
            FacingsHigh = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsHigh));
            FacingsWide = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsWide));
            FacingsDeep = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsDeep));
            OrientationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionOrientationType));
            MerchandisingStyle = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionMerchandisingStyle));
            FacingsXHigh = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsXHigh));
            FacingsXWide = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsXWide));
            FacingsXDeep = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsXDeep));
            MerchandisingStyleX = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionMerchandisingStyleX));
            OrientationTypeX = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionOrientationTypeX));
            IsXPlacedLeft = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionIsXPlacedLeft));
            FacingsYHigh = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsYHigh));
            FacingsYWide = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsYWide));
            FacingsYDeep = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsYDeep));
            MerchandisingStyleY = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionMerchandisingStyleY));
            OrientationTypeY = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionOrientationTypeY));
            IsYPlacedBottom = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionIsYPlacedBottom));
            FacingsZHigh = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsZHigh));
            FacingsZWide = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsZWide));
            FacingsZDeep = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionFacingsZDeep));
            MerchandisingStyleZ = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionMerchandisingStyleZ));
            OrientationTypeZ = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionOrientationTypeZ));
            IsZPlacedFront = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionIsZPlacedFront));
            Sequence = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionSequence));
            SequenceX = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionSequenceX));
            SequenceY = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionSequenceY));
            SequenceZ = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionSequenceZ));
            UnitsHigh = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionUnitsHigh));
            UnitsWide = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionUnitsWide));
            UnitsDeep = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramPositionUnitsDeep));
            TotalUnits = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPositionTotalUnits));
            MetaAchievedCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaAchievedCases));
            MetaIsPositionCollisions = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsPositionCollisions));
            MetaFrontFacingsWide = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaFrontFacingsWide));
            PositionSequenceNumber = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionPositionSequenceNumber));
            IsManuallyPlaced = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionIsManuallyPlaced));
            HorizontalSqueeze = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionHorizontalSqueeze));
            VerticalSqueeze = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionVerticalSqueeze));
            DepthSqueeze = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionDepthSqueeze));
            HorizontalSqueezeX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionHorizontalSqueezeX));
            VerticalSqueezeX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionVerticalSqueezeX));
            DepthSqueezeX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionDepthSqueezeX));
            HorizontalSqueezeY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionHorizontalSqueezeY));
            VerticalSqueezeY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionVerticalSqueezeY));
            DepthSqueezeY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionDepthSqueezeY));
            HorizontalSqueezeZ = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionHorizontalSqueezeZ));
            VerticalSqueezeZ = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionVerticalSqueezeZ));
            DepthSqueezeZ = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPositionDepthSqueezeZ));
            MetaIsUnderMinDeep = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsUnderMinDeep));
            MetaIsOverMaxDeep = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsOverMaxDeep));
            MetaIsOverMaxRightCap = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsOverMaxRightCap));
            MetaIsOverMaxStack = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsOverMaxStack));
            MetaIsOverMaxTopCap = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsOverMaxTopCap));
            MetaIsInvalidMerchandisingStyle = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsInvalidMerchandisingStyle));
            MetaIsHangingTray = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsHangingTray));
            MetaIsPegOverfilled = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsPegOverfilled));
            MetaIsOutsideMerchandisingSpace = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsOutsideMerchandisingSpace));
            MetaIsOutsideOfBlockSpace = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaIsOutsideOfBlockSpace));
            MetaHeight = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaHeight));
            MetaWidth = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaWidth));
            MetaDepth = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaDepth));
            MetaWorldX = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaWorldX));
            MetaWorldY = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaWorldY));
            MetaWorldZ = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaWorldZ));
            MetaTotalLinearSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaTotalLinearSpace));
            MetaTotalAreaSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaTotalAreaSpace));
            MetaTotalVolumetricSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaTotalVolumetricSpace));
            MetaPlanogramLinearSpacePercentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaPlanogramLinearSpacePercentage));
            MetaPlanogramAreaSpacePercentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaPlanogramAreaSpacePercentage));
            MetaPlanogramVolumetricSpacePercentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaPlanogramVolumetricSpacePercentage));
            SequenceNumber = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionSequenceNumber));
            SequenceColour = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionSequenceColour));
            MetaComparisonStatus = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaComparisonStatus));
            MetaSequenceSubGroupName = (String)item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaSequenceSubGroupName);
            MetaSequenceGroupName = (String)item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaSequenceGroupName);
            MetaPegRowNumber = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaPegRowNumber));
            MetaPegColumnNumber = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPositionMetaPegColumnNumber));
        }

        /// <summary>
        /// Creates a new PlanogramPositionDtoItem and populates its properties with values taken from a 
        /// PlanogramPositionDto.
        /// </summary>
        public PlanogramPositionDtoItem(PlanogramPositionDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            PlanogramFixtureItemId = dto.PlanogramFixtureItemId;
            PlanogramFixtureAssemblyId = dto.PlanogramFixtureAssemblyId;
            PlanogramAssemblyComponentId = dto.PlanogramAssemblyComponentId;
            PlanogramFixtureComponentId = dto.PlanogramFixtureComponentId;
            PlanogramSubComponentId = dto.PlanogramSubComponentId;
            PlanogramProductId = dto.PlanogramProductId;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
            FacingsHigh = dto.FacingsHigh;
            FacingsWide = dto.FacingsWide;
            FacingsDeep = dto.FacingsDeep;
            OrientationType = dto.OrientationType;
            MerchandisingStyle = dto.MerchandisingStyle;
            FacingsXHigh = dto.FacingsXHigh;
            FacingsXWide = dto.FacingsXWide;
            FacingsXDeep = dto.FacingsXDeep;
            MerchandisingStyleX = dto.MerchandisingStyleX;
            OrientationTypeX = dto.OrientationTypeX;
            IsXPlacedLeft = dto.IsXPlacedLeft;
            FacingsYHigh = dto.FacingsYHigh;
            FacingsYWide = dto.FacingsYWide;
            FacingsYDeep = dto.FacingsYDeep;
            MerchandisingStyleY = dto.MerchandisingStyleY;
            OrientationTypeY = dto.OrientationTypeY;
            IsYPlacedBottom = dto.IsYPlacedBottom;
            FacingsZHigh = dto.FacingsZHigh;
            FacingsZWide = dto.FacingsZWide;
            FacingsZDeep = dto.FacingsZDeep;
            MerchandisingStyleZ = dto.MerchandisingStyleZ;
            OrientationTypeZ = dto.OrientationTypeZ;
            IsZPlacedFront = dto.IsZPlacedFront;
            Sequence = dto.Sequence;
            SequenceX = dto.SequenceX;
            SequenceY = dto.SequenceY;
            SequenceZ = dto.SequenceZ;
            UnitsHigh = dto.UnitsHigh;
            UnitsWide = dto.UnitsWide;
            UnitsDeep = dto.UnitsDeep;
            TotalUnits = dto.TotalUnits;
            MetaAchievedCases = dto.MetaAchievedCases;
            MetaIsPositionCollisions = dto.MetaIsPositionCollisions;
            MetaFrontFacingsWide = dto.MetaFrontFacingsWide;
            PositionSequenceNumber = dto.PositionSequenceNumber;
            IsManuallyPlaced = dto.IsManuallyPlaced;
            HorizontalSqueeze = dto.HorizontalSqueeze;
            VerticalSqueeze = dto.VerticalSqueeze;
            DepthSqueeze = dto.DepthSqueeze;
            HorizontalSqueezeX = dto.HorizontalSqueezeX;
            VerticalSqueezeX = dto.VerticalSqueezeX;
            DepthSqueezeX = dto.DepthSqueezeX;
            HorizontalSqueezeY = dto.HorizontalSqueezeY;
            VerticalSqueezeY = dto.VerticalSqueezeY;
            DepthSqueezeY = dto.DepthSqueezeY;
            HorizontalSqueezeZ = dto.HorizontalSqueezeZ;
            VerticalSqueezeZ = dto.VerticalSqueezeZ;
            DepthSqueezeZ = dto.DepthSqueezeZ;
            MetaIsUnderMinDeep = dto.MetaIsUnderMinDeep;
            MetaIsOverMaxDeep = dto.MetaIsOverMaxDeep;
            MetaIsOverMaxRightCap = dto.MetaIsOverMaxRightCap;
            MetaIsOverMaxStack = dto.MetaIsOverMaxStack;
            MetaIsOverMaxTopCap = dto.MetaIsOverMaxTopCap;
            MetaIsInvalidMerchandisingStyle = dto.MetaIsInvalidMerchandisingStyle;
            MetaIsHangingTray = dto.MetaIsHangingTray;
            MetaIsPegOverfilled = dto.MetaIsPegOverfilled;
            MetaIsOutsideMerchandisingSpace = dto.MetaIsOutsideMerchandisingSpace;
            MetaIsOutsideOfBlockSpace = dto.MetaIsOutsideOfBlockSpace;
            MetaHeight = dto.MetaHeight;
            MetaWidth = dto.MetaWidth;
            MetaDepth = dto.MetaDepth;
            MetaWorldX = dto.MetaWorldX;
            MetaWorldY = dto.MetaWorldY;
            MetaWorldZ = dto.MetaWorldZ;
            MetaTotalLinearSpace = dto.MetaTotalLinearSpace;
            MetaTotalAreaSpace = dto.MetaTotalAreaSpace;
            MetaTotalVolumetricSpace = dto.MetaTotalVolumetricSpace;
            MetaPlanogramLinearSpacePercentage = dto.MetaPlanogramLinearSpacePercentage;
            MetaPlanogramAreaSpacePercentage = dto.MetaPlanogramAreaSpacePercentage;
            MetaPlanogramVolumetricSpacePercentage = dto.MetaPlanogramVolumetricSpacePercentage;
            SequenceNumber = dto.SequenceNumber;
            SequenceColour = dto.SequenceColour;
            MetaComparisonStatus = dto.MetaComparisonStatus;
            MetaSequenceSubGroupName = dto.MetaSequenceSubGroupName;
            MetaSequenceGroupName = dto.MetaSequenceGroupName;
            MetaPegRowNumber = dto.MetaPegRowNumber;
            MetaPegColumnNumber = dto.MetaPegColumnNumber;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramPositionDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramPositionDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramPositionDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramPositionDto CopyDto()
        {
            return new PlanogramPositionDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                PlanogramFixtureItemId = this.PlanogramFixtureItemId,
                PlanogramFixtureAssemblyId = this.PlanogramFixtureAssemblyId,
                PlanogramAssemblyComponentId = this.PlanogramAssemblyComponentId,
                PlanogramFixtureComponentId = this.PlanogramFixtureComponentId,
                PlanogramSubComponentId = this.PlanogramSubComponentId,
                PlanogramProductId = this.PlanogramProductId,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
                FacingsHigh = this.FacingsHigh,
                FacingsWide = this.FacingsWide,
                FacingsDeep = this.FacingsDeep,
                OrientationType = this.OrientationType,
                MerchandisingStyle = this.MerchandisingStyle,
                FacingsXHigh = this.FacingsXHigh,
                FacingsXWide = this.FacingsXWide,
                FacingsXDeep = this.FacingsXDeep,
                MerchandisingStyleX = this.MerchandisingStyleX,
                OrientationTypeX = this.OrientationTypeX,
                IsXPlacedLeft = this.IsXPlacedLeft,
                FacingsYHigh = this.FacingsYHigh,
                FacingsYWide = this.FacingsYWide,
                FacingsYDeep = this.FacingsYDeep,
                MerchandisingStyleY = this.MerchandisingStyleY,
                OrientationTypeY = this.OrientationTypeY,
                IsYPlacedBottom = this.IsYPlacedBottom,
                FacingsZHigh = this.FacingsZHigh,
                FacingsZWide = this.FacingsZWide,
                FacingsZDeep = this.FacingsZDeep,
                MerchandisingStyleZ = this.MerchandisingStyleZ,
                OrientationTypeZ = this.OrientationTypeZ,
                IsZPlacedFront = this.IsZPlacedFront,
                Sequence = this.Sequence,
                SequenceX = this.SequenceX,
                SequenceY = this.SequenceY,
                SequenceZ = this.SequenceZ,
                UnitsHigh = this.UnitsHigh,
                UnitsWide = this.UnitsWide,
                UnitsDeep = this.UnitsDeep,
                TotalUnits = this.TotalUnits,
                MetaAchievedCases = this.MetaAchievedCases,
                MetaIsPositionCollisions = this.MetaIsPositionCollisions,
                MetaFrontFacingsWide = this.MetaFrontFacingsWide,
                PositionSequenceNumber = this.PositionSequenceNumber,
                IsManuallyPlaced = this.IsManuallyPlaced,
                HorizontalSqueeze = this.HorizontalSqueeze,
                VerticalSqueeze = this.VerticalSqueeze,
                DepthSqueeze = this.DepthSqueeze,
                HorizontalSqueezeX = this.HorizontalSqueezeX,
                VerticalSqueezeX = this.VerticalSqueezeX,
                DepthSqueezeX = this.DepthSqueezeX,
                HorizontalSqueezeY = this.HorizontalSqueezeY,
                VerticalSqueezeY = this.VerticalSqueezeY,
                DepthSqueezeY = this.DepthSqueezeY,
                HorizontalSqueezeZ = this.HorizontalSqueezeZ,
                VerticalSqueezeZ = this.VerticalSqueezeZ,
                DepthSqueezeZ = this.DepthSqueezeZ,
                MetaIsUnderMinDeep = this.MetaIsUnderMinDeep,
                MetaIsOverMaxDeep = this.MetaIsOverMaxDeep,
                MetaIsOverMaxRightCap = this.MetaIsOverMaxRightCap,
                MetaIsOverMaxStack = this.MetaIsOverMaxStack,
                MetaIsOverMaxTopCap = this.MetaIsOverMaxTopCap,
                MetaIsInvalidMerchandisingStyle = this.MetaIsInvalidMerchandisingStyle,
                MetaIsHangingTray = this.MetaIsHangingTray,
                MetaIsPegOverfilled = this.MetaIsPegOverfilled,
                MetaIsOutsideMerchandisingSpace = this.MetaIsOutsideMerchandisingSpace,
                MetaIsOutsideOfBlockSpace = this.MetaIsOutsideOfBlockSpace,
                MetaHeight = this.MetaHeight,
                MetaWidth = this.MetaWidth,
                MetaDepth = this.MetaDepth,
                MetaWorldX = this.MetaWorldX,
                MetaWorldY = this.MetaWorldY,
                MetaWorldZ = this.MetaWorldZ,
                MetaTotalLinearSpace = this.MetaTotalLinearSpace,
                MetaTotalAreaSpace = this.MetaTotalAreaSpace,
                MetaTotalVolumetricSpace = this.MetaTotalVolumetricSpace,
                MetaPlanogramLinearSpacePercentage = this.MetaPlanogramLinearSpacePercentage,
                MetaPlanogramAreaSpacePercentage = this.MetaPlanogramAreaSpacePercentage,
                MetaPlanogramVolumetricSpacePercentage = this.MetaPlanogramVolumetricSpacePercentage,
                SequenceNumber = this.SequenceNumber,
                SequenceColour = this.SequenceColour,
                MetaComparisonStatus = this.MetaComparisonStatus,
                MetaSequenceSubGroupName = this.MetaSequenceSubGroupName,
                MetaSequenceGroupName = this.MetaSequenceGroupName,
                MetaPegRowNumber = this.MetaPegRowNumber,
                MetaPegColumnNumber = this.MetaPegColumnNumber,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramPositionId:
                    return Id;
                case FieldNames.PlanogramPositionPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramPositionPlanogramFixtureItemId:
                    return PlanogramFixtureItemId;
                case FieldNames.PlanogramPositionPlanogramFixtureAssemblyId:
                    return PlanogramFixtureAssemblyId;
                case FieldNames.PlanogramPositionPlanogramAssemblyComponentId:
                    return PlanogramAssemblyComponentId;
                case FieldNames.PlanogramPositionPlanogramFixtureComponentId:
                    return PlanogramFixtureComponentId;
                case FieldNames.PlanogramPositionPlanogramSubComponentId:
                    return PlanogramSubComponentId;
                case FieldNames.PlanogramPositionPlanogramProductId:
                    return PlanogramProductId;
                case FieldNames.PlanogramPositionX:
                    return X;
                case FieldNames.PlanogramPositionY:
                    return Y;
                case FieldNames.PlanogramPositionZ:
                    return Z;
                case FieldNames.PlanogramPositionSlope:
                    return Slope;
                case FieldNames.PlanogramPositionAngle:
                    return Angle;
                case FieldNames.PlanogramPositionRoll:
                    return Roll;
                case FieldNames.PlanogramPositionFacingsHigh:
                    return FacingsHigh;
                case FieldNames.PlanogramPositionFacingsWide:
                    return FacingsWide;
                case FieldNames.PlanogramPositionFacingsDeep:
                    return FacingsDeep;
                case FieldNames.PlanogramPositionOrientationType:
                    return OrientationType;
                case FieldNames.PlanogramPositionMerchandisingStyle:
                    return MerchandisingStyle;
                case FieldNames.PlanogramPositionFacingsXHigh:
                    return FacingsXHigh;
                case FieldNames.PlanogramPositionFacingsXWide:
                    return FacingsXWide;
                case FieldNames.PlanogramPositionFacingsXDeep:
                    return FacingsXDeep;
                case FieldNames.PlanogramPositionMerchandisingStyleX:
                    return MerchandisingStyleX;
                case FieldNames.PlanogramPositionOrientationTypeX:
                    return OrientationTypeX;
                case FieldNames.PlanogramPositionIsXPlacedLeft:
                    return IsXPlacedLeft;
                case FieldNames.PlanogramPositionFacingsYHigh:
                    return FacingsYHigh;
                case FieldNames.PlanogramPositionFacingsYWide:
                    return FacingsYWide;
                case FieldNames.PlanogramPositionFacingsYDeep:
                    return FacingsYDeep;
                case FieldNames.PlanogramPositionMerchandisingStyleY:
                    return MerchandisingStyleY;
                case FieldNames.PlanogramPositionOrientationTypeY:
                    return OrientationTypeY;
                case FieldNames.PlanogramPositionIsYPlacedBottom:
                    return IsYPlacedBottom;
                case FieldNames.PlanogramPositionFacingsZHigh:
                    return FacingsZHigh;
                case FieldNames.PlanogramPositionFacingsZWide:
                    return FacingsZWide;
                case FieldNames.PlanogramPositionFacingsZDeep:
                    return FacingsZDeep;
                case FieldNames.PlanogramPositionMerchandisingStyleZ:
                    return MerchandisingStyleZ;
                case FieldNames.PlanogramPositionOrientationTypeZ:
                    return OrientationTypeZ;
                case FieldNames.PlanogramPositionIsZPlacedFront:
                    return IsZPlacedFront;
                case FieldNames.PlanogramPositionSequence:
                    return Sequence;
                case FieldNames.PlanogramPositionSequenceX:
                    return SequenceX;
                case FieldNames.PlanogramPositionSequenceY:
                    return SequenceY;
                case FieldNames.PlanogramPositionSequenceZ:
                    return SequenceZ;
                case FieldNames.PlanogramPositionUnitsHigh:
                    return UnitsHigh;
                case FieldNames.PlanogramPositionUnitsWide:
                    return UnitsWide;
                case FieldNames.PlanogramPositionUnitsDeep:
                    return UnitsDeep;
                case FieldNames.PlanogramPositionTotalUnits:
                    return TotalUnits;
                case FieldNames.PlanogramPositionMetaAchievedCases:
                    return MetaAchievedCases;
                case FieldNames.PlanogramPositionMetaIsPositionCollisions:
                    return MetaIsPositionCollisions;
                case FieldNames.PlanogramPositionMetaFrontFacingsWide:
                    return MetaFrontFacingsWide;
                case FieldNames.PlanogramPositionPositionSequenceNumber:
                    return PositionSequenceNumber;
                case FieldNames.PlanogramPositionIsManuallyPlaced:
                    return IsManuallyPlaced;
                case FieldNames.PlanogramPositionHorizontalSqueeze:
                    return HorizontalSqueeze;
                case FieldNames.PlanogramPositionVerticalSqueeze:
                    return VerticalSqueeze;
                case FieldNames.PlanogramPositionDepthSqueeze:
                    return DepthSqueeze;
                case FieldNames.PlanogramPositionHorizontalSqueezeX:
                    return HorizontalSqueezeX;
                case FieldNames.PlanogramPositionVerticalSqueezeX:
                    return VerticalSqueezeX;
                case FieldNames.PlanogramPositionDepthSqueezeX:
                    return DepthSqueezeX;
                case FieldNames.PlanogramPositionHorizontalSqueezeY:
                    return HorizontalSqueezeY;
                case FieldNames.PlanogramPositionVerticalSqueezeY:
                    return VerticalSqueezeY;
                case FieldNames.PlanogramPositionDepthSqueezeY:
                    return DepthSqueezeY;
                case FieldNames.PlanogramPositionHorizontalSqueezeZ:
                    return HorizontalSqueezeZ;
                case FieldNames.PlanogramPositionVerticalSqueezeZ:
                    return VerticalSqueezeZ;
                case FieldNames.PlanogramPositionDepthSqueezeZ:
                    return DepthSqueezeZ;
                case FieldNames.PlanogramPositionMetaIsUnderMinDeep:
                    return MetaIsUnderMinDeep;
                case FieldNames.PlanogramPositionMetaIsOverMaxDeep:
                    return MetaIsOverMaxDeep;
                case FieldNames.PlanogramPositionMetaIsOverMaxRightCap:
                    return MetaIsOverMaxRightCap;
                case FieldNames.PlanogramPositionMetaIsOverMaxStack:
                    return MetaIsOverMaxStack;
                case FieldNames.PlanogramPositionMetaIsOverMaxTopCap:
                    return MetaIsOverMaxTopCap;
                case FieldNames.PlanogramPositionMetaIsInvalidMerchandisingStyle:
                    return MetaIsInvalidMerchandisingStyle;
                case FieldNames.PlanogramPositionMetaIsHangingTray:
                    return MetaIsHangingTray;
                case FieldNames.PlanogramPositionMetaIsPegOverfilled:
                    return MetaIsPegOverfilled;
                case FieldNames.PlanogramPositionMetaIsOutsideMerchandisingSpace:
                    return MetaIsOutsideMerchandisingSpace;
                case FieldNames.PlanogramPositionMetaIsOutsideOfBlockSpace:
                    return MetaIsOutsideOfBlockSpace;
                case FieldNames.PlanogramPositionMetaHeight:
                    return MetaHeight;
                case FieldNames.PlanogramPositionMetaWidth:
                    return MetaWidth;
                case FieldNames.PlanogramPositionMetaDepth:
                    return MetaDepth;
                case FieldNames.PlanogramPositionMetaWorldX:
                    return MetaWorldX;
                case FieldNames.PlanogramPositionMetaWorldY:
                    return MetaWorldY;
                case FieldNames.PlanogramPositionMetaWorldZ:
                    return MetaWorldZ;
                case FieldNames.PlanogramPositionMetaTotalLinearSpace:
                    return MetaTotalLinearSpace;
                case FieldNames.PlanogramPositionMetaTotalAreaSpace:
                    return MetaTotalAreaSpace;
                case FieldNames.PlanogramPositionMetaTotalVolumetricSpace:
                    return MetaTotalVolumetricSpace;
                case FieldNames.PlanogramPositionMetaPlanogramLinearSpacePercentage:
                    return MetaPlanogramLinearSpacePercentage;
                case FieldNames.PlanogramPositionMetaPlanogramAreaSpacePercentage:
                    return MetaPlanogramAreaSpacePercentage;
                case FieldNames.PlanogramPositionMetaPlanogramVolumetricSpacePercentage:
                    return MetaPlanogramVolumetricSpacePercentage;
                case FieldNames.PlanogramPositionSequenceNumber:
                    return SequenceNumber;
                case FieldNames.PlanogramPositionSequenceColour:
                    return SequenceColour;
                case FieldNames.PlanogramPositionMetaComparisonStatus:
                    return MetaComparisonStatus;
                case FieldNames.PlanogramPositionMetaSequenceSubGroupName:
                    return MetaSequenceSubGroupName;
                case FieldNames.PlanogramPositionMetaSequenceGroupName:
                    return MetaSequenceGroupName;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
