﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26269 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramPerformanceDtoItem :
        PlanogramPerformanceDto,
        IChildDtoItem<PlanogramPerformanceDto>,
        GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramPerformanceDtoItem and populates its properties with 
        /// values taken from a GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramPerformanceDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPerformancePlanogramId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramPerformanceName);
            Description = (String)item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDescription);
        }

        /// <summary>
        /// Creates a new PlanogramPerformanceDtoItem and populates its properties with 
        /// values taken from a PlanogramPerformanceDataDto.
        /// </summary>
        public PlanogramPerformanceDtoItem(PlanogramPerformanceDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Name = dto.Name;
            Description = dto.Description;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramPerformanceDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramPerformanceDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramPerformanceDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramPerformanceDto CopyDto()
        {
            return new PlanogramPerformanceDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Name = this.Name,
                Description = this.Description
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramPerformanceId:
                    return Id;
                case FieldNames.PlanogramPerformancePlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramPerformanceName:
                    return Name;
                case FieldNames.PlanogramPerformanceDescription:
                    return Description;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
