﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25700 : L.Ineson
//  Null id fields no longer return as 0.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-26091 : L.Ineson
//  Added additional FaceThickness properties
//  Removed FaceTopOffset and CanMultiMerchX,Y,Z
#endregion
#region Version History: CCM802
// V8-29023 : M.Pettit
//  Added MerchandisableDepth
#endregion
#region Version History: CCM830
// V8-32401 : N.Haywood
//  Added MerchandisableDepth to GetItemPropertyValue
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramSubComponentDtoItem : PlanogramSubComponentDto, IChildDtoItem<PlanogramSubComponentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramSubComponentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramSubComponentDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentId));
            PlanogramComponentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentPlanogramComponentId));
            Mesh3DId = item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMesh3DId);
            ImageIdFront = item.GetItemPropertyValue(FieldNames.PlanogramSubComponentImageIdFront);
            ImageIdBack = item.GetItemPropertyValue(FieldNames.PlanogramSubComponentImageIdBack);
            ImageIdTop = item.GetItemPropertyValue(FieldNames.PlanogramSubComponentImageIdTop);
            ImageIdBottom = item.GetItemPropertyValue(FieldNames.PlanogramSubComponentImageIdBottom);
            ImageIdLeft = item.GetItemPropertyValue(FieldNames.PlanogramSubComponentImageIdLeft);
            ImageIdRight = item.GetItemPropertyValue(FieldNames.PlanogramSubComponentImageIdRight);
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramSubComponentName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDepth));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentRoll));
            ShapeType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentShapeType));
            MerchandisableHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchandisableHeight));
            MerchandisableDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchandisableDepth));
            IsVisible = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsVisible));
            HasCollisionDetection = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentHasCollisionDetection));
            FillPatternTypeFront = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillPatternTypeFront));
            FillPatternTypeBack = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillPatternTypeBack));
            FillPatternTypeTop = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillPatternTypeTop));
            FillPatternTypeBottom = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillPatternTypeBottom));
            FillPatternTypeLeft = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillPatternTypeLeft));
            FillPatternTypeRight = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillPatternTypeRight));
            FillColourFront = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillColourFront));
            FillColourBack = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillColourBack));
            FillColourTop = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillColourTop));
            FillColourBottom = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillColourBottom));
            FillColourLeft = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillColourLeft));
            FillColourRight = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFillColourRight));
            LineColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentLineColour));
            TransparencyPercentFront = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentTransparencyPercentFront));
            TransparencyPercentBack = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentTransparencyPercentBack));
            TransparencyPercentTop = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentTransparencyPercentTop));
            TransparencyPercentBottom = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentTransparencyPercentBottom));
            TransparencyPercentLeft = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentTransparencyPercentLeft));
            TransparencyPercentRight = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentTransparencyPercentRight));
            FaceThicknessFront = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFaceThicknessFront));
            FaceThicknessBack = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFaceThicknessBack));
            FaceThicknessTop = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFaceThicknessTop));
            FaceThicknessBottom = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFaceThicknessBottom));
            FaceThicknessLeft = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFaceThicknessLeft));
            FaceThicknessRight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFaceThicknessRight));
            RiserHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentRiserHeight));
            RiserThickness = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentRiserThickness));
            IsRiserPlacedOnFront = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsRiserPlacedOnFront));
            IsRiserPlacedOnBack = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsRiserPlacedOnBack));
            IsRiserPlacedOnLeft = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsRiserPlacedOnLeft));
            IsRiserPlacedOnRight = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsRiserPlacedOnRight));
            RiserFillPatternType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentRiserFillPatternType));
            RiserColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentRiserColour));
            RiserTransparencyPercent = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentRiserTransparencyPercent));
            NotchStartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentNotchStartX));
            NotchSpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentNotchSpacingX));
            NotchStartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentNotchStartY));
            NotchSpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentNotchSpacingY));
            NotchHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentNotchHeight));
            NotchWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentNotchWidth));
            IsNotchPlacedOnFront = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsNotchPlacedOnFront));
            IsNotchPlacedOnBack = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsNotchPlacedOnBack));
            IsNotchPlacedOnLeft = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsNotchPlacedOnLeft));
            IsNotchPlacedOnRight = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsNotchPlacedOnRight));
            NotchStyleType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentNotchStyleType));
            DividerObstructionHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionHeight));
            DividerObstructionWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionWidth));
            DividerObstructionDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionDepth));
            DividerObstructionStartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionStartX));
            DividerObstructionSpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionSpacingX));
            DividerObstructionStartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionStartY));
            DividerObstructionSpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionSpacingY));
            DividerObstructionStartZ = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionStartZ));
            DividerObstructionSpacingZ = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionSpacingZ));
            MerchConstraintRow1StartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow1StartX));
            MerchConstraintRow1SpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingX));
            MerchConstraintRow1StartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow1StartY));
            MerchConstraintRow1SpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingY));
            MerchConstraintRow1Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow1Height));
            MerchConstraintRow1Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow1Width));
            MerchConstraintRow2StartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow2StartX));
            MerchConstraintRow2SpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingX));
            MerchConstraintRow2StartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow2StartY));
            MerchConstraintRow2SpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingY));
            MerchConstraintRow2Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow2Height));
            MerchConstraintRow2Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchConstraintRow2Width));
            LineThickness = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentLineThickness));
            MerchandisingType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchandisingType));
            CombineType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentCombineType));
            IsProductOverlapAllowed = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsProductOverlapAllowed));
            IsProductSqueezeAllowed = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsProductSqueezeAllowed));
            MerchandisingStrategyX = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchandisingStrategyX));
            MerchandisingStrategyY = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchandisingStrategyY));
            MerchandisingStrategyZ = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentMerchandisingStrategyZ));
            LeftOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentLeftOverhang));
            RightOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentRightOverhang));
            FrontOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentFrontOverhang));
            BackOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentBackOverhang));
            TopOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentTopOverhang));
            BottomOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentBottomOverhang));
            IsDividerObstructionAtStart = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsDividerObstructionAtStart));
            IsDividerObstructionAtEnd = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsDividerObstructionAtEnd));
            IsDividerObstructionByFacing = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentIsDividerObstructionByFacing));
            DividerObstructionFillColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionFillColour));
            DividerObstructionFillPattern = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramSubComponentDividerObstructionFillPattern));
        }

        /// <summary>
        /// Creates a new PlanogramSubComponentDtoItem and populates its properties with values taken from a 
        /// PlanogramSubComponentDto.
        /// </summary>
        public PlanogramSubComponentDtoItem(PlanogramSubComponentDto dto)
        {
            Id = dto.Id;
            PlanogramComponentId = dto.PlanogramComponentId;
            Mesh3DId = dto.Mesh3DId;
            ImageIdFront = dto.ImageIdFront;
            ImageIdBack = dto.ImageIdBack;
            ImageIdTop = dto.ImageIdTop;
            ImageIdBottom = dto.ImageIdBottom;
            ImageIdLeft = dto.ImageIdLeft;
            ImageIdRight = dto.ImageIdRight;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
            ShapeType = dto.ShapeType;
            MerchandisableHeight = dto.MerchandisableHeight;
            MerchandisableDepth = dto.MerchandisableDepth;
            IsVisible = dto.IsVisible;
            HasCollisionDetection = dto.HasCollisionDetection;
            FillPatternTypeFront = dto.FillPatternTypeFront;
            FillPatternTypeBack = dto.FillPatternTypeBack;
            FillPatternTypeTop = dto.FillPatternTypeTop;
            FillPatternTypeBottom = dto.FillPatternTypeBottom;
            FillPatternTypeLeft = dto.FillPatternTypeLeft;
            FillPatternTypeRight = dto.FillPatternTypeRight;
            FillColourFront = dto.FillColourFront;
            FillColourBack = dto.FillColourBack;
            FillColourTop = dto.FillColourTop;
            FillColourBottom = dto.FillColourBottom;
            FillColourLeft = dto.FillColourLeft;
            FillColourRight = dto.FillColourRight;
            LineColour = dto.LineColour;
            TransparencyPercentFront = dto.TransparencyPercentFront;
            TransparencyPercentBack = dto.TransparencyPercentBack;
            TransparencyPercentTop = dto.TransparencyPercentTop;
            TransparencyPercentBottom = dto.TransparencyPercentBottom;
            TransparencyPercentLeft = dto.TransparencyPercentLeft;
            TransparencyPercentRight = dto.TransparencyPercentRight;
            FaceThicknessFront = dto.FaceThicknessFront;
            FaceThicknessBack = dto.FaceThicknessBack;
            FaceThicknessTop = dto.FaceThicknessTop;
            FaceThicknessBottom = dto.FaceThicknessBottom;
            FaceThicknessLeft = dto.FaceThicknessLeft;
            FaceThicknessRight = dto.FaceThicknessRight;
            RiserHeight = dto.RiserHeight;
            RiserThickness = dto.RiserThickness;
            IsRiserPlacedOnFront = dto.IsRiserPlacedOnFront;
            IsRiserPlacedOnBack = dto.IsRiserPlacedOnBack;
            IsRiserPlacedOnLeft = dto.IsRiserPlacedOnLeft;
            IsRiserPlacedOnRight = dto.IsRiserPlacedOnRight;
            RiserFillPatternType = dto.RiserFillPatternType;
            RiserColour = dto.RiserColour;
            RiserTransparencyPercent = dto.RiserTransparencyPercent;
            NotchStartX = dto.NotchStartX;
            NotchSpacingX = dto.NotchSpacingX;
            NotchStartY = dto.NotchStartY;
            NotchSpacingY = dto.NotchSpacingY;
            NotchHeight = dto.NotchHeight;
            NotchWidth = dto.NotchWidth;
            IsNotchPlacedOnFront = dto.IsNotchPlacedOnFront;
            IsNotchPlacedOnBack = dto.IsNotchPlacedOnBack;
            IsNotchPlacedOnLeft = dto.IsNotchPlacedOnLeft;
            IsNotchPlacedOnRight = dto.IsNotchPlacedOnRight;
            NotchStyleType = dto.NotchStyleType;
            DividerObstructionHeight = dto.DividerObstructionHeight;
            DividerObstructionWidth = dto.DividerObstructionWidth;
            DividerObstructionDepth = dto.DividerObstructionDepth;
            DividerObstructionStartX = dto.DividerObstructionStartX;
            DividerObstructionSpacingX = dto.DividerObstructionSpacingX;
            DividerObstructionStartY = dto.DividerObstructionStartY;
            DividerObstructionSpacingY = dto.DividerObstructionSpacingY;
            DividerObstructionStartZ = dto.DividerObstructionStartZ;
            DividerObstructionSpacingZ = dto.DividerObstructionSpacingZ;
            MerchConstraintRow1StartX = dto.MerchConstraintRow1StartX;
            MerchConstraintRow1SpacingX = dto.MerchConstraintRow1SpacingX;
            MerchConstraintRow1StartY = dto.MerchConstraintRow1StartY;
            MerchConstraintRow1SpacingY = dto.MerchConstraintRow1SpacingY;
            MerchConstraintRow1Height = dto.MerchConstraintRow1Height;
            MerchConstraintRow1Width = dto.MerchConstraintRow1Width;
            MerchConstraintRow2StartX = dto.MerchConstraintRow2StartX;
            MerchConstraintRow2SpacingX = dto.MerchConstraintRow2SpacingX;
            MerchConstraintRow2StartY = dto.MerchConstraintRow2StartY;
            MerchConstraintRow2SpacingY = dto.MerchConstraintRow2SpacingY;
            MerchConstraintRow2Height = dto.MerchConstraintRow2Height;
            MerchConstraintRow2Width = dto.MerchConstraintRow2Width;
            LineThickness = dto.LineThickness;
            MerchandisingType = dto.MerchandisingType;
            CombineType = dto.CombineType;
            IsProductOverlapAllowed = dto.IsProductOverlapAllowed;
            IsProductSqueezeAllowed = dto.IsProductSqueezeAllowed;
            MerchandisingStrategyX = dto.MerchandisingStrategyX;
            MerchandisingStrategyY = dto.MerchandisingStrategyY;
            MerchandisingStrategyZ = dto.MerchandisingStrategyZ;
            LeftOverhang = dto.LeftOverhang;
            RightOverhang = dto.RightOverhang;
            FrontOverhang = dto.FrontOverhang;
            BackOverhang = dto.BackOverhang;
            TopOverhang = dto.TopOverhang;
            BottomOverhang = dto.BottomOverhang;
            IsDividerObstructionAtStart = dto.IsDividerObstructionAtStart;
            IsDividerObstructionAtEnd = dto.IsDividerObstructionAtEnd;
            IsDividerObstructionByFacing = dto.IsDividerObstructionByFacing;
            DividerObstructionFillColour = dto.DividerObstructionFillColour;
            DividerObstructionFillPattern = dto.DividerObstructionFillPattern;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramSubComponentDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramSubComponentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramSubComponentDto>.ParentId
        {
            get { return PlanogramComponentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramSubComponentDto CopyDto()
        {
            return new PlanogramSubComponentDto()
            {
                Id = this.Id,
                PlanogramComponentId = this.PlanogramComponentId,
                Mesh3DId = this.Mesh3DId,
                ImageIdFront = this.ImageIdFront,
                ImageIdBack = this.ImageIdBack,
                ImageIdTop = this.ImageIdTop,
                ImageIdBottom = this.ImageIdBottom,
                ImageIdLeft = this.ImageIdLeft,
                ImageIdRight = this.ImageIdRight,
                Name = this.Name,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
                ShapeType = this.ShapeType,
                MerchandisableHeight = this.MerchandisableHeight,
                MerchandisableDepth = this.MerchandisableDepth,
                IsVisible = this.IsVisible,
                HasCollisionDetection = this.HasCollisionDetection,
                FillPatternTypeFront = this.FillPatternTypeFront,
                FillPatternTypeBack = this.FillPatternTypeBack,
                FillPatternTypeTop = this.FillPatternTypeTop,
                FillPatternTypeBottom = this.FillPatternTypeBottom,
                FillPatternTypeLeft = this.FillPatternTypeLeft,
                FillPatternTypeRight = this.FillPatternTypeRight,
                FillColourFront = this.FillColourFront,
                FillColourBack = this.FillColourBack,
                FillColourTop = this.FillColourTop,
                FillColourBottom = this.FillColourBottom,
                FillColourLeft = this.FillColourLeft,
                FillColourRight = this.FillColourRight,
                LineColour = this.LineColour,
                TransparencyPercentFront = this.TransparencyPercentFront,
                TransparencyPercentBack = this.TransparencyPercentBack,
                TransparencyPercentTop = this.TransparencyPercentTop,
                TransparencyPercentBottom = this.TransparencyPercentBottom,
                TransparencyPercentLeft = this.TransparencyPercentLeft,
                TransparencyPercentRight = this.TransparencyPercentRight,
                FaceThicknessFront = this.FaceThicknessFront,
                FaceThicknessBack = this.FaceThicknessBack,
                FaceThicknessTop = this.FaceThicknessTop,
                FaceThicknessBottom = this.FaceThicknessBottom,
                FaceThicknessLeft = this.FaceThicknessLeft,
                FaceThicknessRight = this.FaceThicknessRight,
                RiserHeight = this.RiserHeight,
                RiserThickness = this.RiserThickness,
                IsRiserPlacedOnFront = this.IsRiserPlacedOnFront,
                IsRiserPlacedOnBack = this.IsRiserPlacedOnBack,
                IsRiserPlacedOnLeft = this.IsRiserPlacedOnLeft,
                IsRiserPlacedOnRight = this.IsRiserPlacedOnRight,
                RiserFillPatternType = this.RiserFillPatternType,
                RiserColour = this.RiserColour,
                RiserTransparencyPercent = this.RiserTransparencyPercent,
                NotchStartX = this.NotchStartX,
                NotchSpacingX = this.NotchSpacingX,
                NotchStartY = this.NotchStartY,
                NotchSpacingY = this.NotchSpacingY,
                NotchHeight = this.NotchHeight,
                NotchWidth = this.NotchWidth,
                IsNotchPlacedOnFront = this.IsNotchPlacedOnFront,
                IsNotchPlacedOnBack = this.IsNotchPlacedOnBack,
                IsNotchPlacedOnLeft = this.IsNotchPlacedOnLeft,
                IsNotchPlacedOnRight = this.IsNotchPlacedOnRight,
                NotchStyleType = this.NotchStyleType,
                DividerObstructionHeight = this.DividerObstructionHeight,
                DividerObstructionWidth = this.DividerObstructionWidth,
                DividerObstructionDepth = this.DividerObstructionDepth,
                DividerObstructionStartX = this.DividerObstructionStartX,
                DividerObstructionSpacingX = this.DividerObstructionSpacingX,
                DividerObstructionStartY = this.DividerObstructionStartY,
                DividerObstructionSpacingY = this.DividerObstructionSpacingY,
                DividerObstructionStartZ = this.DividerObstructionStartZ,
                DividerObstructionSpacingZ = this.DividerObstructionSpacingZ,
                IsDividerObstructionAtStart = this.IsDividerObstructionAtStart,
                IsDividerObstructionAtEnd = this.IsDividerObstructionAtEnd,
                IsDividerObstructionByFacing = this.IsDividerObstructionByFacing,
                DividerObstructionFillColour = this.DividerObstructionFillColour,
                DividerObstructionFillPattern = this.DividerObstructionFillPattern,
                MerchConstraintRow1StartX = this.MerchConstraintRow1StartX,
                MerchConstraintRow1SpacingX = this.MerchConstraintRow1SpacingX,
                MerchConstraintRow1StartY = this.MerchConstraintRow1StartY,
                MerchConstraintRow1SpacingY = this.MerchConstraintRow1SpacingY,
                MerchConstraintRow1Height = this.MerchConstraintRow1Height,
                MerchConstraintRow1Width = this.MerchConstraintRow1Width,
                MerchConstraintRow2StartX = this.MerchConstraintRow2StartX,
                MerchConstraintRow2SpacingX = this.MerchConstraintRow2SpacingX,
                MerchConstraintRow2StartY = this.MerchConstraintRow2StartY,
                MerchConstraintRow2SpacingY = this.MerchConstraintRow2SpacingY,
                MerchConstraintRow2Height = this.MerchConstraintRow2Height,
                MerchConstraintRow2Width = this.MerchConstraintRow2Width,
                LineThickness = this.LineThickness,
                MerchandisingType = this.MerchandisingType,
                CombineType = this.CombineType,
                IsProductOverlapAllowed = this.IsProductOverlapAllowed,
                IsProductSqueezeAllowed = this.IsProductSqueezeAllowed,
                MerchandisingStrategyX = this.MerchandisingStrategyX,
                MerchandisingStrategyY = this.MerchandisingStrategyY,
                MerchandisingStrategyZ = this.MerchandisingStrategyZ,
                LeftOverhang = this.LeftOverhang,
                RightOverhang = this.RightOverhang,
                FrontOverhang = this.FrontOverhang,
                BackOverhang = this.BackOverhang,
                TopOverhang = this.TopOverhang,
                BottomOverhang = this.BottomOverhang,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {                 
                case FieldNames.PlanogramSubComponentId:        
                    return Id;
                case FieldNames.PlanogramSubComponentPlanogramComponentId: 
                    return PlanogramComponentId;
                case FieldNames.PlanogramSubComponentMesh3DId: 
                    return Mesh3DId;
                case FieldNames.PlanogramSubComponentImageIdFront: 
                    return ImageIdFront;
                case FieldNames.PlanogramSubComponentImageIdBack: 
                    return ImageIdBack;
                case FieldNames.PlanogramSubComponentImageIdTop: 
                    return ImageIdTop;
                case FieldNames.PlanogramSubComponentImageIdBottom: 
                    return ImageIdBottom;
                case FieldNames.PlanogramSubComponentImageIdLeft: 
                    return ImageIdLeft;
                case FieldNames.PlanogramSubComponentImageIdRight: 
                    return ImageIdRight;
                case FieldNames.PlanogramSubComponentName: 
                    return Name;
                case FieldNames.PlanogramSubComponentHeight: 
                    return Height;
                case FieldNames.PlanogramSubComponentWidth: 
                    return Width;
                case FieldNames.PlanogramSubComponentDepth: 
                    return Depth;
                case FieldNames.PlanogramSubComponentX: 
                    return X;
                case FieldNames.PlanogramSubComponentY: 
                    return Y;
                case FieldNames.PlanogramSubComponentZ: 
                    return Z;
                case FieldNames.PlanogramSubComponentSlope: 
                    return Slope;
                case FieldNames.PlanogramSubComponentAngle: 
                    return Angle;
                case FieldNames.PlanogramSubComponentRoll: 
                    return Roll;
                case FieldNames.PlanogramSubComponentShapeType: 
                    return ShapeType;
                case FieldNames.PlanogramSubComponentMerchandisableHeight: 
                    return MerchandisableHeight;
                case FieldNames.PlanogramSubComponentIsVisible: 
                    return IsVisible;
                case FieldNames.PlanogramSubComponentHasCollisionDetection: 
                    return HasCollisionDetection;
                case FieldNames.PlanogramSubComponentFillPatternTypeFront: 
                    return FillPatternTypeFront;
                case FieldNames.PlanogramSubComponentFillPatternTypeBack: 
                    return FillPatternTypeBack;
                case FieldNames.PlanogramSubComponentFillPatternTypeTop: 
                    return FillPatternTypeTop;
                case FieldNames.PlanogramSubComponentFillPatternTypeBottom: 
                    return FillPatternTypeBottom;
                case FieldNames.PlanogramSubComponentFillPatternTypeLeft: 
                    return FillPatternTypeLeft;
                case FieldNames.PlanogramSubComponentFillPatternTypeRight: 
                    return FillPatternTypeRight;
                case FieldNames.PlanogramSubComponentFillColourFront: 
                    return FillColourFront;
                case FieldNames.PlanogramSubComponentFillColourBack: 
                    return FillColourBack;
                case FieldNames.PlanogramSubComponentFillColourTop: 
                    return FillColourTop;
                case FieldNames.PlanogramSubComponentFillColourBottom: 
                    return FillColourBottom;
                case FieldNames.PlanogramSubComponentFillColourLeft: 
                    return FillColourLeft;
                case FieldNames.PlanogramSubComponentFillColourRight: 
                    return FillColourRight;
                case FieldNames.PlanogramSubComponentLineColour: 
                    return LineColour;
                case FieldNames.PlanogramSubComponentTransparencyPercentFront: 
                    return TransparencyPercentFront;
                case FieldNames.PlanogramSubComponentTransparencyPercentBack: 
                    return TransparencyPercentBack;
                case FieldNames.PlanogramSubComponentTransparencyPercentTop: 
                    return TransparencyPercentTop;
                case FieldNames.PlanogramSubComponentTransparencyPercentBottom: 
                    return TransparencyPercentBottom;
                case FieldNames.PlanogramSubComponentTransparencyPercentLeft: 
                    return TransparencyPercentLeft;
                case FieldNames.PlanogramSubComponentTransparencyPercentRight: 
                    return TransparencyPercentRight;
                case FieldNames.PlanogramSubComponentFaceThicknessFront: 
                    return FaceThicknessFront;
                case FieldNames.PlanogramSubComponentFaceThicknessBack:
                    return FaceThicknessBack;
                case FieldNames.PlanogramSubComponentFaceThicknessTop: 
                    return FaceThicknessTop;
                case FieldNames.PlanogramSubComponentFaceThicknessBottom:
                    return FaceThicknessBottom;
                case FieldNames.PlanogramSubComponentFaceThicknessLeft:
                    return FaceThicknessLeft;
                case FieldNames.PlanogramSubComponentFaceThicknessRight:
                    return FaceThicknessRight;
                case FieldNames.PlanogramSubComponentRiserHeight: 
                    return RiserHeight;
                case FieldNames.PlanogramSubComponentRiserThickness: 
                    return RiserThickness;
                case FieldNames.PlanogramSubComponentIsRiserPlacedOnFront: 
                    return IsRiserPlacedOnFront;
                case FieldNames.PlanogramSubComponentIsRiserPlacedOnBack: 
                    return IsRiserPlacedOnBack;
                case FieldNames.PlanogramSubComponentIsRiserPlacedOnLeft: 
                    return IsRiserPlacedOnLeft;
                case FieldNames.PlanogramSubComponentIsRiserPlacedOnRight: 
                    return IsRiserPlacedOnRight;
                case FieldNames.PlanogramSubComponentRiserFillPatternType: 
                    return RiserFillPatternType;
                case FieldNames.PlanogramSubComponentRiserColour: 
                    return RiserColour;
                case FieldNames.PlanogramSubComponentRiserTransparencyPercent: 
                    return RiserTransparencyPercent;
                case FieldNames.PlanogramSubComponentNotchStartX: 
                    return NotchStartX;
                case FieldNames.PlanogramSubComponentNotchSpacingX: 
                    return NotchSpacingX;
                case FieldNames.PlanogramSubComponentNotchStartY: 
                    return NotchStartY;
                case FieldNames.PlanogramSubComponentNotchSpacingY: 
                    return NotchSpacingY;
                case FieldNames.PlanogramSubComponentNotchHeight: 
                    return NotchHeight;
                case FieldNames.PlanogramSubComponentNotchWidth: 
                    return NotchWidth;
                case FieldNames.PlanogramSubComponentIsNotchPlacedOnFront: 
                    return IsNotchPlacedOnFront;
                case FieldNames.PlanogramSubComponentIsNotchPlacedOnBack: 
                    return IsNotchPlacedOnBack;
                case FieldNames.PlanogramSubComponentIsNotchPlacedOnLeft: 
                    return IsNotchPlacedOnLeft;
                case FieldNames.PlanogramSubComponentIsNotchPlacedOnRight: 
                    return IsNotchPlacedOnRight;
                case FieldNames.PlanogramSubComponentNotchStyleType: 
                    return NotchStyleType;
                case FieldNames.PlanogramSubComponentDividerObstructionHeight: 
                    return DividerObstructionHeight;
                case FieldNames.PlanogramSubComponentDividerObstructionWidth: 
                    return DividerObstructionWidth;
                case FieldNames.PlanogramSubComponentDividerObstructionDepth:
                    return DividerObstructionDepth;
                case FieldNames.PlanogramSubComponentDividerObstructionStartX: 
                    return DividerObstructionStartX;
                case FieldNames.PlanogramSubComponentDividerObstructionSpacingX: 
                    return DividerObstructionSpacingX;
                case FieldNames.PlanogramSubComponentDividerObstructionStartY: 
                    return DividerObstructionStartY;
                case FieldNames.PlanogramSubComponentDividerObstructionSpacingY:
                    return DividerObstructionSpacingY;
                case FieldNames.PlanogramSubComponentDividerObstructionStartZ:
                    return DividerObstructionStartZ;
                case FieldNames.PlanogramSubComponentDividerObstructionSpacingZ:
                    return DividerObstructionSpacingZ;
                case FieldNames.PlanogramSubComponentMerchConstraintRow1StartX: 
                    return MerchConstraintRow1StartX;
                case FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingX: 
                    return MerchConstraintRow1SpacingX;
                case FieldNames.PlanogramSubComponentMerchConstraintRow1StartY: 
                    return MerchConstraintRow1StartY;
                case FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingY: 
                    return MerchConstraintRow1SpacingY;
                case FieldNames.PlanogramSubComponentMerchConstraintRow1Height: 
                    return MerchConstraintRow1Height;
                case FieldNames.PlanogramSubComponentMerchConstraintRow1Width: 
                    return MerchConstraintRow1Width;
                case FieldNames.PlanogramSubComponentMerchConstraintRow2StartX: 
                    return MerchConstraintRow2StartX;
                case FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingX: 
                    return MerchConstraintRow2SpacingX;
                case FieldNames.PlanogramSubComponentMerchConstraintRow2StartY: 
                    return MerchConstraintRow2StartY;
                case FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingY: 
                    return MerchConstraintRow2SpacingY;
                case FieldNames.PlanogramSubComponentMerchConstraintRow2Height: 
                    return MerchConstraintRow2Height;
                case FieldNames.PlanogramSubComponentMerchConstraintRow2Width: 
                    return MerchConstraintRow2Width;
                case FieldNames.PlanogramSubComponentLineThickness: 
                    return LineThickness;
                case FieldNames.PlanogramSubComponentMerchandisingType: 
                    return MerchandisingType;
                case FieldNames.PlanogramSubComponentCombineType: 
                    return CombineType;
                case FieldNames.PlanogramSubComponentIsProductOverlapAllowed: 
                    return IsProductOverlapAllowed;
                case FieldNames.PlanogramSubComponentIsProductSqueezeAllowed:
                    return IsProductSqueezeAllowed;
                case FieldNames.PlanogramSubComponentMerchandisingStrategyX: 
                    return MerchandisingStrategyX;
                case FieldNames.PlanogramSubComponentMerchandisingStrategyY: 
                    return MerchandisingStrategyY;
                case FieldNames.PlanogramSubComponentMerchandisingStrategyZ: 
                    return MerchandisingStrategyZ;
                case FieldNames.PlanogramSubComponentLeftOverhang: 
                    return LeftOverhang;
                case FieldNames.PlanogramSubComponentRightOverhang: 
                    return RightOverhang;
                case FieldNames.PlanogramSubComponentFrontOverhang: 
                    return FrontOverhang;
                case FieldNames.PlanogramSubComponentBackOverhang: 
                    return BackOverhang;
                case FieldNames.PlanogramSubComponentTopOverhang: 
                    return TopOverhang;
                case FieldNames.PlanogramSubComponentBottomOverhang: 
                    return BottomOverhang;
                case FieldNames.PlanogramSubComponentIsDividerObstructionAtStart: 
                    return IsDividerObstructionAtStart;
                case FieldNames.PlanogramSubComponentIsDividerObstructionAtEnd: 
                    return IsDividerObstructionAtEnd;
                case FieldNames.PlanogramSubComponentIsDividerObstructionByFacing: 
                    return IsDividerObstructionByFacing;
                case FieldNames.PlanogramSubComponentMerchandisableDepth:
                    return MerchandisableDepth;
                case FieldNames.PlanogramSubComponentDividerObstructionFillColour:
                    return DividerObstructionFillColour;
                case FieldNames.PlanogramSubComponentDividerObstructionFillPattern:
                    return DividerObstructionFillPattern;
                default:
                    return null;
            }
        }

        #endregion

        #endregion

    }
}
