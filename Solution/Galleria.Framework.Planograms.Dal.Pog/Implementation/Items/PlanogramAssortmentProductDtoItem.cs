﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26799 : A.Kuszyk
//  Removed LocationCode property.
// V8-27058 : A.Probyn
//  Updated Gtin to be GTIN
#endregion
#region Version History: CCM820
// V8-30762 : I.George
//  Added MetaData property
// V8-30705 : A.Probyn
//  Added DelistedDueToAssortmentRule
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentProductDtoItem : PlanogramAssortmentProductDto, IChildDtoItem<PlanogramAssortmentProductDto>
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentProductDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentProductDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductId));
            PlanogramAssortmentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductPlanogramAssortmentId));
            Gtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductGTIN);
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductName);
            IsRanged = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductIsRanged));
            Rank = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductRank));
            Segmentation = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductSegmentation);
            Facings = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductFacings));
            Units = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductUnits));
            ProductTreatmentType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductProductTreatmentType));
            ProductLocalizationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductProductLocalizationType));
            Comments = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductComments);
            ExactListFacings = GalleriaBinaryFileItemHelper.GetByteNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductExactListFacings));
            ExactListUnits = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductExactListUnits));
            PreserveListFacings = GalleriaBinaryFileItemHelper.GetByteNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductPreserveListFacings));
            PreserveListUnits = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductPreserveListUnits));
            MaxListFacings = GalleriaBinaryFileItemHelper.GetByteNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductMaxListFacings));
            MaxListUnits = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductMaxListUnits));
            MinListFacings = GalleriaBinaryFileItemHelper.GetByteNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductMinListFacings));
            MinListUnits = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductMinListUnits));
            FamilyRuleName = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductFamilyRuleName);
            FamilyRuleType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductFamilyRuleType));
            FamilyRuleValue = GalleriaBinaryFileItemHelper.GetByteNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductFamilyRuleValue));
            IsPrimaryRegionalProduct = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductIsPrimaryRegionalProduct));
            MetaIsProductPlacedOnPlanogram = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram));
            DelistedDueToAssortmentRule = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductDelistedDueToAssortmentRule));
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentProductDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentProductDtoItem(PlanogramAssortmentProductDto dto)
        {
            Id = dto.Id;
            PlanogramAssortmentId = dto.PlanogramAssortmentId;
            Gtin = dto.Gtin;
            Name = dto.Name;
            IsRanged = dto.IsRanged;
            Rank = dto.Rank;
            Segmentation = dto.Segmentation;
            Facings = dto.Facings;
            Units = dto.Units;
            ProductTreatmentType = dto.ProductTreatmentType;
            ProductLocalizationType = dto.ProductLocalizationType;
            Comments = dto.Comments;
            ExactListFacings = dto.ExactListFacings;
            ExactListUnits = dto.ExactListUnits;
            PreserveListFacings = dto.PreserveListFacings;
            PreserveListUnits = dto.PreserveListUnits;
            MaxListFacings = dto.MaxListFacings;
            MaxListUnits = dto.MaxListUnits;
            MinListFacings = dto.MinListFacings;
            MinListUnits = dto.MinListUnits;
            FamilyRuleName = dto.FamilyRuleName;
            FamilyRuleType = dto.FamilyRuleType;
            FamilyRuleValue = dto.FamilyRuleValue;
            IsPrimaryRegionalProduct = dto.IsPrimaryRegionalProduct;
            MetaIsProductPlacedOnPlanogram = dto.MetaIsProductPlacedOnPlanogram;
            DelistedDueToAssortmentRule = dto.DelistedDueToAssortmentRule;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentProductDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentProductDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentProductDto>.ParentId
        {
            get { return PlanogramAssortmentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentProductDto CopyDto()
        {
            return new PlanogramAssortmentProductDto()
            {
                Id = this.Id,
                PlanogramAssortmentId = this.PlanogramAssortmentId,
                Gtin = this.Gtin,
                Name = this.Name,
                IsRanged = this.IsRanged,
                Rank = this.Rank,
                Segmentation = this.Segmentation,
                Facings = this.Facings,
                Units = this.Units,
                ProductTreatmentType = this.ProductTreatmentType,
                ProductLocalizationType = this.ProductLocalizationType,
                Comments = this.Comments,
                ExactListFacings = this.ExactListFacings,
                ExactListUnits = this.ExactListUnits,
                PreserveListFacings = this.PreserveListFacings,
                PreserveListUnits = this.PreserveListUnits,
                MaxListFacings = this.MaxListFacings,
                MaxListUnits = this.MaxListUnits,
                MinListFacings = this.MinListFacings,
                MinListUnits = this.MinListUnits,
                FamilyRuleName = this.FamilyRuleName,
                FamilyRuleType = this.FamilyRuleType,
                FamilyRuleValue = this.FamilyRuleValue,
                IsPrimaryRegionalProduct = this.IsPrimaryRegionalProduct,
                MetaIsProductPlacedOnPlanogram = this.MetaIsProductPlacedOnPlanogram,
                DelistedDueToAssortmentRule = this.DelistedDueToAssortmentRule
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentProductId:
                    return Id;
                case FieldNames.PlanogramAssortmentProductPlanogramAssortmentId:
                    return PlanogramAssortmentId;
                case FieldNames.PlanogramAssortmentProductGTIN:
                    return Gtin;
                case FieldNames.PlanogramAssortmentProductName:
                    return Name;
                case FieldNames.PlanogramAssortmentProductIsRanged:
                    return IsRanged;
                case FieldNames.PlanogramAssortmentProductRank:
                    return Rank;
                case FieldNames.PlanogramAssortmentProductSegmentation:
                    return Segmentation;
                case FieldNames.PlanogramAssortmentProductFacings:
                    return Facings;
                case FieldNames.PlanogramAssortmentProductUnits:
                    return Units;
                case FieldNames.PlanogramAssortmentProductProductTreatmentType:
                    return ProductTreatmentType;
                case FieldNames.PlanogramAssortmentProductProductLocalizationType:
                    return ProductLocalizationType;
                case FieldNames.PlanogramAssortmentProductComments:
                    return Comments;
                case FieldNames.PlanogramAssortmentProductExactListFacings:
                    return ExactListFacings;
                case FieldNames.PlanogramAssortmentProductExactListUnits:
                    return ExactListUnits;
                case FieldNames.PlanogramAssortmentProductPreserveListFacings:
                    return PreserveListFacings;
                case FieldNames.PlanogramAssortmentProductPreserveListUnits:
                    return PreserveListUnits;
                case FieldNames.PlanogramAssortmentProductMaxListFacings:
                    return MaxListFacings;
                case FieldNames.PlanogramAssortmentProductMaxListUnits:
                    return MaxListUnits;
                case FieldNames.PlanogramAssortmentProductMinListFacings:
                    return MinListFacings;
                case FieldNames.PlanogramAssortmentProductMinListUnits:
                    return MinListUnits;
                case FieldNames.PlanogramAssortmentProductFamilyRuleName:
                    return FamilyRuleName;
                case FieldNames.PlanogramAssortmentProductFamilyRuleType:
                    return FamilyRuleType;
                case FieldNames.PlanogramAssortmentProductFamilyRuleValue:
                    return FamilyRuleValue;
                case FieldNames.PlanogramAssortmentProductIsPrimaryRegionalProduct:
                    return IsPrimaryRegionalProduct;
                case FieldNames.PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram:
                    return MetaIsProductPlacedOnPlanogram;
                case FieldNames.PlanogramAssortmentProductDelistedDueToAssortmentRule:
                    return DelistedDueToAssortmentRule;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}


