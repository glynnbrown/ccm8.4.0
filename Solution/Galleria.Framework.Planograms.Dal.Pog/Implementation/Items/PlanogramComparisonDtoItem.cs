﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts.
// V8-32005 : A.Silva
//  Added Name, Description and DataOrderType.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramComparisonDtoItem : PlanogramComparisonDto, IChildDtoItem<PlanogramComparisonDto>
    {
        #region Public Properties

        Object IChildDtoItem<PlanogramComparisonDto>.Id { get { return Id; } set { Id = value; } }

        Boolean IChildDtoItem<PlanogramComparisonDto>.AutoIncrementId { get { return true; } }

        Object IChildDtoItem<PlanogramComparisonDto>.ParentId { get { return PlanogramId; } }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonPlanogramId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonName);
            Description = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonDescription);
            DateLastCompared = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PlanogramComparisonDateLastCompared));
            IgnoreNonPlacedProducts =
                GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramComparisonIgnoreNonPlacedProducts));
            DataOrderType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramComparisonDataOrderType));
        }

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonDto" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonDtoItem(PlanogramComparisonDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Name = dto.Name;
            Description = dto.Description;
            DateLastCompared = dto.DateLastCompared;
            IgnoreNonPlacedProducts = dto.IgnoreNonPlacedProducts;
            DataOrderType = dto.DataOrderType;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramComparisonDto CopyDto()
        {
            return new PlanogramComparisonDto
                   {
                       Id = Id,
                       PlanogramId = PlanogramId,
                       Name = Name,
                       Description = Description,
                       DateLastCompared = DateLastCompared,
                       IgnoreNonPlacedProducts = IgnoreNonPlacedProducts,
                       DataOrderType = DataOrderType
                   };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramComparisonId:
                    return Id;
                case FieldNames.PlanogramComparisonPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramComparisonName:
                    return Name;
                case FieldNames.PlanogramComparisonDescription:
                    return Description;
                case FieldNames.PlanogramComparisonDateLastCompared:
                    return DateLastCompared;
                case FieldNames.PlanogramComparisonIgnoreNonPlacedProducts:
                    return IgnoreNonPlacedProducts;
                case FieldNames.PlanogramComparisonDataOrderType:
                    return DataOrderType;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}