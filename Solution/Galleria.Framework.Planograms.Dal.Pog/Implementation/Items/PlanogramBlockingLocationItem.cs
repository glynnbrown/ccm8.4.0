﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramBlockingLocationDtoItem : PlanogramBlockingLocationDto, IChildDtoItem<PlanogramBlockingLocationDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramBlockingLocationDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramBlockingLocationDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationId));
            PlanogramBlockingId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationPlanogramBlockingId));
            PlanogramBlockingGroupId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationPlanogramBlockingGroupId));
            PlanogramBlockingDividerTopId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerTopId));
            PlanogramBlockingDividerBottomId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerBottomId));
            PlanogramBlockingDividerLeftId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerLeftId));
            PlanogramBlockingDividerRightId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerRightId));
            SpacePercentage = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramBlockingLocationSpacePercentage));
        }

        /// <summary>
        /// Creates a new PlanogramBlockingLocationDtoItem and populates its properties with values taken from a 
        /// PlanogramBlockingLocationDto.
        /// </summary>
        public PlanogramBlockingLocationDtoItem(PlanogramBlockingLocationDto dto)
        {
            Id = dto.Id;
            PlanogramBlockingId = dto.PlanogramBlockingId;
            PlanogramBlockingGroupId = dto.PlanogramBlockingGroupId;
            PlanogramBlockingDividerTopId = dto.PlanogramBlockingDividerTopId;
            PlanogramBlockingDividerBottomId = dto.PlanogramBlockingDividerBottomId;
            PlanogramBlockingDividerLeftId = dto.PlanogramBlockingDividerLeftId;
            PlanogramBlockingDividerRightId = dto.PlanogramBlockingDividerRightId;
            SpacePercentage = dto.SpacePercentage;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramBlockingLocationDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramBlockingLocationDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramBlockingLocationDto>.ParentId
        {
            get { return PlanogramBlockingId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramBlockingLocationDto CopyDto()
        {
            return new PlanogramBlockingLocationDto()
            {
                Id = this.Id,
                PlanogramBlockingId = this.PlanogramBlockingId,
                PlanogramBlockingGroupId = this.PlanogramBlockingGroupId,
                PlanogramBlockingDividerTopId = this.PlanogramBlockingDividerTopId,
                PlanogramBlockingDividerBottomId = this.PlanogramBlockingDividerBottomId,
                PlanogramBlockingDividerLeftId = this.PlanogramBlockingDividerLeftId,
                PlanogramBlockingDividerRightId = this.PlanogramBlockingDividerRightId,
                SpacePercentage = this.SpacePercentage
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramBlockingLocationId:
                    return Id;
                case FieldNames.PlanogramBlockingLocationPlanogramBlockingId:
                    return PlanogramBlockingId;
                case FieldNames.PlanogramBlockingLocationPlanogramBlockingGroupId:
                    return PlanogramBlockingGroupId;
                case FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerTopId:
                    return PlanogramBlockingDividerTopId;
                case FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerBottomId:
                    return PlanogramBlockingDividerBottomId;
                case FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerLeftId:
                    return PlanogramBlockingDividerLeftId;
                case FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerRightId:
                    return PlanogramBlockingDividerRightId;
                case FieldNames.PlanogramBlockingLocationSpacePercentage:
                    return SpacePercentage;

                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
