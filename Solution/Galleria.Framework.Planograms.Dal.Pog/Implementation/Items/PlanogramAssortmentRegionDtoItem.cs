﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentRegionDtoItem : PlanogramAssortmentRegionDto, IChildDtoItem<PlanogramAssortmentRegionDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentRegionDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentRegionDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionId));
            PlanogramAssortmentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionName);
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentRegionDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentRegionDtoItem(PlanogramAssortmentRegionDto dto)
        {
            Id = dto.Id;
            PlanogramAssortmentId = dto.PlanogramAssortmentId;
            Name = dto.Name;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentRegionDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentRegionDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentRegionDto>.ParentId
        {
            get { return PlanogramAssortmentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentRegionDto CopyDto()
        {
            return new PlanogramAssortmentRegionDto()
            {
                Id = this.Id,
                PlanogramAssortmentId = this.PlanogramAssortmentId,
                Name = this.Name
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentRegionId:
                    return Id;
                case FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId:
                    return PlanogramAssortmentId;
                case FieldNames.PlanogramAssortmentRegionName:
                    return Name;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
