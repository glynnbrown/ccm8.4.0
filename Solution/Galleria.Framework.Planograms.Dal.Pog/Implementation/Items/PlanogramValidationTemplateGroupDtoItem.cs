﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26954 : A.Silva ~ Added ResultType property.
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramValidationTemplateGroupDtoItem : PlanogramValidationTemplateGroupDto,
        IChildDtoItem<PlanogramValidationTemplateGroupDto>
    {
        #region Constructors

        /// <summary>
        ///     Creates a new PlanogramValidationTemplateGroupDtoItem and populates its properties with value taken from a
        ///     GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramValidationTemplateGroupDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupId));
            PlanogramValidationTemplateId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId));
            Name = (String) item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupName);
            Threshold1 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupThreshold1));
            Threshold2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupThreshold2));
            ResultType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupResultType));
            ValidationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupValidationType));
        }

        /// <summary>
        ///     Creates a new PlanogramValidationTemplateGroupDtoItem and populates its properties with values taken from a
        ///     PlanogramValidationTemplateDto.
        /// </summary>
        public PlanogramValidationTemplateGroupDtoItem(PlanogramValidationTemplateGroupDto dto)
        {
            Id = dto.Id;
            PlanogramValidationTemplateId = dto.PlanogramValidationTemplateId;
            Name = dto.Name;
            Threshold1 = dto.Threshold1;
            Threshold2 = dto.Threshold2;
            ResultType = dto.ResultType;
            ValidationType = dto.ValidationType;
        }

        #endregion

        #region Public Properties

        Boolean IChildDtoItem<PlanogramValidationTemplateGroupDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramValidationTemplateGroupDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Object IChildDtoItem<PlanogramValidationTemplateGroupDto>.ParentId
        {
            get { return PlanogramValidationTemplateId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramValidationTemplateGroupDto CopyDto()
        {
            return new PlanogramValidationTemplateGroupDto
            {
                Id = Id,
                PlanogramValidationTemplateId = PlanogramValidationTemplateId,
                Name = Name,
                Threshold1 = Threshold1,
                Threshold2 = Threshold2,
                ResultType = ResultType,
                ValidationType = ValidationType
            };
        }

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramValidationTemplateGroupId:
                    return Id;
                case FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId:
                    return PlanogramValidationTemplateId;
                case FieldNames.PlanogramValidationTemplateGroupName:
                    return Name;
                case FieldNames.PlanogramValidationTemplateGroupThreshold1:
                    return Threshold1;
                case FieldNames.PlanogramValidationTemplateGroupThreshold2:
                    return Threshold2;
                case FieldNames.PlanogramValidationTemplateGroupResultType:
                    return ResultType;
                case FieldNames.PlanogramValidationTemplateGroupValidationType:
                    return ValidationType;
                default:
                    return null;
            }
        }

        #endregion
    }
}