﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssemblyDtoItem : PlanogramAssemblyDto, IChildDtoItem<PlanogramAssemblyDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssemblyDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssemblyDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyPlanogramId));
            Name =(String)item.GetItemPropertyValue(FieldNames.PlanogramAssemblyName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyDepth));
            NumberOfComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyNumberOfComponents));
            TotalComponentCost = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyTotalComponentCost));
        }

        /// <summary>
        /// Creates a new PlanogramAssemblyDtoItem and populates its properties with values taken from a 
        /// PlanogramAssemblyDto.
        /// </summary>
        public PlanogramAssemblyDtoItem(PlanogramAssemblyDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            NumberOfComponents = dto.NumberOfComponents;
            TotalComponentCost = dto.TotalComponentCost;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssemblyDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssemblyDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssemblyDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssemblyDto CopyDto()
        {
            return new PlanogramAssemblyDto()
            {
                Id=this.Id,
                PlanogramId =this.PlanogramId ,
                Name =this.Name ,
                Height =this.Height ,
                Width =this.Width ,
                Depth =this.Depth ,
                NumberOfComponents =this.NumberOfComponents ,
                TotalComponentCost =this.TotalComponentCost
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssemblyId:
                    return Id;
                case FieldNames.PlanogramAssemblyPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramAssemblyName:
                    return Name;
                case FieldNames.PlanogramAssemblyHeight:
                    return Height;
                case FieldNames.PlanogramAssemblyWidth:
                    return Width;
                case FieldNames.PlanogramAssemblyDepth:
                    return Depth;
                case FieldNames.PlanogramAssemblyNumberOfComponents:
                    return NumberOfComponents;
                case FieldNames.PlanogramAssemblyTotalComponentCost:
                    return TotalComponentCost;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
