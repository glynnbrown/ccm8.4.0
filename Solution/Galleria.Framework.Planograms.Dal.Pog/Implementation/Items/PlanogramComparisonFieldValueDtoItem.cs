﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramComparisonFieldValueDtoItem : PlanogramComparisonFieldValueDto, IChildDtoItem<PlanogramComparisonFieldValueDto>
    {
        #region Public Properties

        Object IChildDtoItem<PlanogramComparisonFieldValueDto>.Id { get { return Id; } set { Id = value; } }

        Boolean IChildDtoItem<PlanogramComparisonFieldValueDto>.AutoIncrementId { get { return true; } }

        Object IChildDtoItem<PlanogramComparisonFieldValueDto>.ParentId { get { return PlanogramComparisonItemId; } }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonFieldValueDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonFieldValueDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldValueId));
            PlanogramComparisonItemId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldValuePlanogramComparisonItemId));
            FieldPlaceholder = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldValueFieldPlaceholder);
            Value = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldValueValue);
        }

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonFieldValueDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldValueDto" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonFieldValueDtoItem(PlanogramComparisonFieldValueDto dto)
        {
            Id = dto.Id;
            PlanogramComparisonItemId = dto.PlanogramComparisonItemId;
            FieldPlaceholder = dto.FieldPlaceholder;
            Value = dto.Value;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramComparisonFieldValueDto CopyDto()
        {
            return new PlanogramComparisonFieldValueDto
                   {
                       Id = Id,
                       PlanogramComparisonItemId = PlanogramComparisonItemId,
                       FieldPlaceholder = FieldPlaceholder,
                       Value = Value
                   };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramComparisonFieldValueId:
                    return Id;
                case FieldNames.PlanogramComparisonFieldValuePlanogramComparisonItemId:
                    return PlanogramComparisonItemId;
                case FieldNames.PlanogramComparisonFieldValueFieldPlaceholder:
                    return FieldPlaceholder;
                case FieldNames.PlanogramComparisonFieldValueValue:
                    return Value;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}