﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-24972 : L.Hodson
//  Added product image fields for display, tray,  pointofpurchase, alternate and case
// V8-25700 : L.Ineson
//  Null id fields no longer return as 0.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-26041 : A.Kuszyk
//  Added additional Product properties.
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27606 : L.Ineson
//  Added FinancialGroupCode and FinancialGroupName
// V8-26777 : L.Ineson
//  Removed CanMerch properties
#endregion
#region Version History: CCM801
// V8-27636 : L.Ineson
//  Added MetaTotalUnits
#endregion
#region Version History: CCM802
// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-28811 : L.Luong
//  Added MetaPlanogramLinearSpacePercentage
//  Added MetaPlanogramAreaSpacePercentage
//  Added MetaPlanogramVolumetricSpacePercentage
// V8-29232 : A.Silva
//      Added PlanogramProductSequenceNumber.
#endregion
#region Version History: CCM803
// V8-28491 : L.Luong
//  Added MetaIsInMasterData
#endregion
#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
#endregion
#region Version History: CCM820
// V8-30763 :I.George
//	Added MetaCDTNode field to PlanogramProduct
// V8-30705 : A.Probyn
//  Extended for new assortment rule related meta data properties
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct.
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31947 : A.Silva
//  Added MetaComparisonStatus field.
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32396 : A.Probyn
//  Extended for MetaIsDelistFamilyRuleBroken
// V8-32609 : L.Ineson
//  Removed old PegProngOffset
// V8-32819 : M.Pettit
//  Added MetaIsBuddied
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramProductDtoItem : PlanogramProductDto, IChildDtoItem<PlanogramProductDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramProductDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramProductDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramProductId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramId));
            Gtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductGtin);
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductName);
            Brand = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductBrand);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductDepth));
            DisplayHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductDisplayHeight));
            DisplayWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductDisplayWidth));
            DisplayDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductDisplayDepth));
            AlternateHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductAlternateHeight));
            AlternateWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductAlternateWidth));
            AlternateDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductAlternateDepth));
            PointOfPurchaseHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPointOfPurchaseHeight));
            PointOfPurchaseWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPointOfPurchaseWidth));
            PointOfPurchaseDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPointOfPurchaseDepth));
            NumberOfPegHoles = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductNumberOfPegHoles));
            PegX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegX));
            PegX2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegX2));
            PegX3 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegX3));
            PegY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegY));
            PegY2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegY2));
            PegY3 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegY3));
            //PegProngOffset = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegProngOffset));
            PegProngOffsetX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegProngOffsetX));
            PegProngOffsetY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegProngOffsetY));
            PegDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductPegDepth));
            SqueezeHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductSqueezeHeight));
            SqueezeWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductSqueezeWidth));
            SqueezeDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductSqueezeDepth));
            NestingHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductNestingHeight));
            NestingWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductNestingWidth));
            NestingDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductNestingDepth));
            CasePackUnits = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramProductCasePackUnits));
            CaseHigh = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductCaseHigh));
            CaseWide = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductCaseWide));
            CaseDeep = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductCaseDeep));
            CaseHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductCaseHeight));
            CaseWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductCaseWidth));
            CaseDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductCaseDepth));
            MaxStack = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductMaxStack));
            MaxTopCap = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductMaxTopCap));
            MaxRightCap = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductMaxRightCap));
            MinDeep = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductMinDeep));
            MaxDeep = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductMaxDeep));
            TrayPackUnits = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayPackUnits));
            TrayHigh = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayHigh));
            TrayWide = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayWide));
            TrayDeep = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayDeep));
            TrayHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayHeight));
            TrayWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayWidth));
            TrayDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayDepth));
            TrayThickHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayThickHeight));
            TrayThickWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayThickWidth));
            TrayThickDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductTrayThickDepth));
            FrontOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductFrontOverhang));
            FingerSpaceAbove = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductFingerSpaceAbove));
            FingerSpaceToTheSide = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramProductFingerSpaceToTheSide));
            StatusType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductStatusType));
            OrientationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductOrientationType));
            MerchandisingStyle = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductMerchandisingStyle));
            IsFrontOnly = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductIsFrontOnly));
            IsPlaceHolderProduct = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductIsPlaceHolderProduct));
            IsActive = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductIsActive));
            CanBreakTrayUp = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductCanBreakTrayUp));
            CanBreakTrayDown = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductCanBreakTrayDown));
            CanBreakTrayBack = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductCanBreakTrayBack));
            CanBreakTrayTop = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductCanBreakTrayTop));
            ForceMiddleCap = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductForceMiddleCap));
            ForceBottomCap = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductForceBottomCap));
            PlanogramImageIdFront = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdFront);
            PlanogramImageIdBack = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdBack);
            PlanogramImageIdTop = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdTop);
            PlanogramImageIdBottom = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdBottom);
            PlanogramImageIdLeft = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdLeft);
            PlanogramImageIdRight = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdRight);
            PlanogramImageIdDisplayFront = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdDisplayFront);
            PlanogramImageIdDisplayBack = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdDisplayBack);
            PlanogramImageIdDisplayTop = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdDisplayTop);
            PlanogramImageIdDisplayBottom = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdDisplayBottom);
            PlanogramImageIdDisplayLeft = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdDisplayLeft);
            PlanogramImageIdDisplayRight = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdDisplayRight);
            PlanogramImageIdTrayFront = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdTrayFront);
            PlanogramImageIdTrayBack = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdTrayBack);
            PlanogramImageIdTrayTop = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdTrayTop);
            PlanogramImageIdTrayBottom = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdTrayBottom);
            PlanogramImageIdTrayLeft = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdTrayLeft);
            PlanogramImageIdTrayRight = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdTrayRight);
            PlanogramImageIdPointOfPurchaseFront = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseFront);
            PlanogramImageIdPointOfPurchaseBack = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBack);
            PlanogramImageIdPointOfPurchaseTop = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseTop);
            PlanogramImageIdPointOfPurchaseBottom = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBottom);
            PlanogramImageIdPointOfPurchaseLeft = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseLeft);
            PlanogramImageIdPointOfPurchaseRight = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseRight);
            PlanogramImageIdAlternateFront = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdAlternateFront);
            PlanogramImageIdAlternateBack = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdAlternateBack);
            PlanogramImageIdAlternateTop = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdAlternateTop);
            PlanogramImageIdAlternateBottom = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdAlternateBottom);
            PlanogramImageIdAlternateLeft = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdAlternateLeft);
            PlanogramImageIdAlternateRight = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdAlternateRight);
            PlanogramImageIdCaseFront = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdCaseFront);
            PlanogramImageIdCaseBack = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdCaseBack);
            PlanogramImageIdCaseTop = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdCaseTop);
            PlanogramImageIdCaseBottom = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdCaseBottom);
            PlanogramImageIdCaseLeft = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdCaseLeft);
            PlanogramImageIdCaseRight = item.GetItemPropertyValue(FieldNames.PlanogramProductPlanogramImageIdCaseRight);
            ShapeType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductShapeType));
            FillPatternType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductFillPatternType));
            FillColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramProductFillColour));
            // Additional Product properties.
            Shape = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductShape);
            PointOfPurchaseDescription = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductPointOfPurchaseDescription);
            ShortDescription = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductShortDescription);
            Subcategory = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductSubcategory);
            CustomerStatus = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductCustomerStatus);
            Colour = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductColour);
            Flavour = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductFlavour);
            PackagingShape = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductPackagingShape);
            PackagingType = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductPackagingType);
            CountryOfOrigin = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductCountryOfOrigin);
            CountryOfProcessing = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductCountryOfProcessing);
            ShelfLife = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramProductShelfLife));
            DeliveryFrequencyDays = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductDeliveryFrequencyDays));
            DeliveryMethod = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductDeliveryMethod);
            VendorCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductVendorCode);
            Vendor = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductVendor);
            ManufacturerCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductManufacturerCode);
            Manufacturer = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductManufacturer);
            Size = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductSize);
            UnitOfMeasure = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductUnitOfMeasure);
            DateIntroduced = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductDateIntroduced));
            DateDiscontinued = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductDateDiscontinued));
            DateEffective = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductDateEffective));
            Health = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductHealth);
            CorporateCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductCorporateCode);
            Barcode = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductBarcode);
            SellPrice = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductSellPrice));
            SellPackCount = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductSellPackCount));
            SellPackDescription = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductSellPackDescription);
            RecommendedRetailPrice = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductRecommendedRetailPrice));
            ManufacturerRecommendedRetailPrice = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductManufacturerRecommendedRetailPrice));
            CostPrice = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductCostPrice));
            CaseCost = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductCaseCost));
            TaxRate = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductTaxRate));
            ConsumerInformation = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductConsumerInformation);
            Texture = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductTexture);
            StyleNumber = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductStyleNumber));
            Pattern = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductPattern);
            Model = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductModel);
            GarmentType = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductGarmentType);
            IsPrivateLabel = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductIsPrivateLabel));
            IsNewProduct = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductIsNewProduct));
            FinancialGroupCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductFinancialGroupCode);
            FinancialGroupName = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductFinancialGroupName);
            MetaNotAchievedInventory = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaNotAchievedInventory));
            MetaTotalUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalUnits));
            MetaPlanogramLinearSpacePercentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaPlanogramLinearSpacePercentage));
            MetaPlanogramAreaSpacePercentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaPlanogramAreaSpacePercentage));
            MetaPlanogramVolumetricSpacePercentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaPlanogramVolumetricSpacePercentage));
            MetaPositionCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaPositionCount));
            MetaTotalFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalFacings));
            MetaTotalLinearSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalLinearSpace));
            MetaTotalAreaSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalAreaSpace));
            MetaTotalVolumetricSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalVolumetricSpace));
            MetaIsInMasterData = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsInMasterData));
            MetaNotAchievedCases = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaNotAchievedCases));
            MetaNotAchievedDOS = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaNotAchievedDOS));
            MetaIsOverShelfLifePercent = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsOverShelfLifePercent));
            MetaNotAchievedDeliveries = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaNotAchievedDeliveries));
            MetaTotalMainFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalMainFacings));
            MetaTotalXFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalXFacings));
            MetaTotalYFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalYFacings));
            MetaTotalZFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaTotalZFacings));
            MetaIsRangedInAssortment = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsRangedInAssortment));
            ColourGroupValue = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductColourGroupValue);
            MetaCDTNode = (String)item.GetItemPropertyValue(FieldNames.PlanogramProductMetaCDTNode);
            MetaIsProductRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsProductRuleBroken));
            MetaIsFamilyRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsFamilyRuleBroken));
            MetaIsInheritanceRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsInheritanceRuleBroken));
            MetaIsLocalProductRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsLocalProductRuleBroken));
            MetaIsDistributionRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsDistributionRuleBroken));
            MetaIsCoreRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsCoreRuleBroken));
            MetaIsDelistProductRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsCoreRuleBroken));
            MetaIsForceProductRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsForceProductRuleBroken));
            MetaIsPreserveProductRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsPreserveProductRuleBroken));
            MetaIsMinimumHurdleProductRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsMinimumHurdleProductRuleBroken));
            MetaIsMaximumProductFamilyRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsMaximumProductFamilyRuleBroken));
            MetaIsMinimumProductFamilyRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsMinimumProductFamilyRuleBroken));
            MetaIsDependencyFamilyRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsDependencyFamilyRuleBroken));
            MetaIsDelistFamilyRuleBroken = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsDelistFamilyRuleBroken));
            MetaComparisonStatus = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaComparisonStatus));
            MetaIsBuddied = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramProductMetaIsBuddied));
        }

        /// <summary>
        /// Creates a new PlanogramProductDtoItem and populates its properties with values taken from a 
        /// PlanogramProductDto.
        /// </summary>
        public PlanogramProductDtoItem(PlanogramProductDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Gtin = dto.Gtin;
            Name = dto.Name;
            Brand = dto.Brand;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            DisplayHeight = dto.DisplayHeight;
            DisplayWidth = dto.DisplayWidth;
            DisplayDepth = dto.DisplayDepth;
            AlternateHeight = dto.AlternateHeight;
            AlternateWidth = dto.AlternateWidth;
            AlternateDepth = dto.AlternateDepth;
            PointOfPurchaseHeight = dto.PointOfPurchaseHeight;
            PointOfPurchaseWidth = dto.PointOfPurchaseWidth;
            PointOfPurchaseDepth = dto.PointOfPurchaseDepth;
            NumberOfPegHoles = dto.NumberOfPegHoles;
            PegX = dto.PegX;
            PegX2 = dto.PegX2;
            PegX3 = dto.PegX3;
            PegY = dto.PegY;
            PegY2 = dto.PegY2;
            PegY3 = dto.PegY3;
            PegProngOffsetX = dto.PegProngOffsetX;
            PegProngOffsetY = dto.PegProngOffsetY;
            PegDepth = dto.PegDepth;
            SqueezeHeight = dto.SqueezeHeight;
            SqueezeWidth = dto.SqueezeWidth;
            SqueezeDepth = dto.SqueezeDepth;
            NestingHeight = dto.NestingHeight;
            NestingWidth = dto.NestingWidth;
            NestingDepth = dto.NestingDepth;
            CasePackUnits = dto.CasePackUnits;
            CaseHigh = dto.CaseHigh;
            CaseWide = dto.CaseWide;
            CaseDeep = dto.CaseDeep;
            CaseHeight = dto.CaseHeight;
            CaseWidth = dto.CaseWidth;
            CaseDepth = dto.CaseDepth;
            MaxStack = dto.MaxStack;
            MaxTopCap = dto.MaxTopCap;
            MaxRightCap = dto.MaxRightCap;
            MinDeep = dto.MinDeep;
            MaxDeep = dto.MaxDeep;
            TrayPackUnits = dto.TrayPackUnits;
            TrayHigh = dto.TrayHigh;
            TrayWide = dto.TrayWide;
            TrayDeep = dto.TrayDeep;
            TrayHeight = dto.TrayHeight;
            TrayWidth = dto.TrayWidth;
            TrayDepth = dto.TrayDepth;
            TrayThickHeight = dto.TrayThickHeight;
            TrayThickWidth = dto.TrayThickWidth;
            TrayThickDepth = dto.TrayThickDepth;
            FrontOverhang = dto.FrontOverhang;
            FingerSpaceAbove = dto.FingerSpaceAbove;
            FingerSpaceToTheSide = dto.FingerSpaceToTheSide;
            StatusType = dto.StatusType;
            OrientationType = dto.OrientationType;
            MerchandisingStyle = dto.MerchandisingStyle;
            IsFrontOnly = dto.IsFrontOnly;
            IsTrayProduct = dto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Tray;
            IsPlaceHolderProduct = dto.IsPlaceHolderProduct;
            IsActive = dto.IsActive;
            CanBreakTrayUp = dto.CanBreakTrayUp;
            CanBreakTrayDown = dto.CanBreakTrayDown;
            CanBreakTrayBack = dto.CanBreakTrayBack;
            CanBreakTrayTop = dto.CanBreakTrayTop;
            ForceMiddleCap = dto.ForceMiddleCap;
            ForceBottomCap = dto.ForceBottomCap;
            PlanogramImageIdFront = dto.PlanogramImageIdFront;
            PlanogramImageIdBack = dto.PlanogramImageIdBack;
            PlanogramImageIdTop = dto.PlanogramImageIdTop;
            PlanogramImageIdBottom = dto.PlanogramImageIdBottom;
            PlanogramImageIdLeft = dto.PlanogramImageIdLeft;
            PlanogramImageIdRight = dto.PlanogramImageIdRight;
            PlanogramImageIdDisplayFront = dto.PlanogramImageIdDisplayFront;
            PlanogramImageIdDisplayBack = dto.PlanogramImageIdDisplayBack;
            PlanogramImageIdDisplayTop = dto.PlanogramImageIdDisplayTop;
            PlanogramImageIdDisplayBottom = dto.PlanogramImageIdDisplayBottom;
            PlanogramImageIdDisplayLeft = dto.PlanogramImageIdDisplayLeft;
            PlanogramImageIdDisplayRight = dto.PlanogramImageIdDisplayRight;
            PlanogramImageIdTrayFront = dto.PlanogramImageIdTrayFront;
            PlanogramImageIdTrayBack = dto.PlanogramImageIdTrayBack;
            PlanogramImageIdTrayTop = dto.PlanogramImageIdTrayTop;
            PlanogramImageIdTrayBottom = dto.PlanogramImageIdTrayBottom;
            PlanogramImageIdTrayLeft = dto.PlanogramImageIdTrayLeft;
            PlanogramImageIdTrayRight = dto.PlanogramImageIdTrayRight;
            PlanogramImageIdPointOfPurchaseFront = dto.PlanogramImageIdPointOfPurchaseFront;
            PlanogramImageIdPointOfPurchaseBack = dto.PlanogramImageIdPointOfPurchaseBack;
            PlanogramImageIdPointOfPurchaseTop = dto.PlanogramImageIdPointOfPurchaseTop;
            PlanogramImageIdPointOfPurchaseBottom = dto.PlanogramImageIdPointOfPurchaseBottom;
            PlanogramImageIdPointOfPurchaseLeft = dto.PlanogramImageIdPointOfPurchaseLeft;
            PlanogramImageIdPointOfPurchaseRight = dto.PlanogramImageIdPointOfPurchaseRight;
            PlanogramImageIdAlternateFront = dto.PlanogramImageIdAlternateFront;
            PlanogramImageIdAlternateBack = dto.PlanogramImageIdAlternateBack;
            PlanogramImageIdAlternateTop = dto.PlanogramImageIdAlternateTop;
            PlanogramImageIdAlternateBottom = dto.PlanogramImageIdAlternateBottom;
            PlanogramImageIdAlternateLeft = dto.PlanogramImageIdAlternateLeft;
            PlanogramImageIdAlternateRight = dto.PlanogramImageIdAlternateRight;
            PlanogramImageIdCaseFront = dto.PlanogramImageIdCaseFront;
            PlanogramImageIdCaseBack = dto.PlanogramImageIdCaseBack;
            PlanogramImageIdCaseTop = dto.PlanogramImageIdCaseTop;
            PlanogramImageIdCaseBottom = dto.PlanogramImageIdCaseBottom;
            PlanogramImageIdCaseLeft = dto.PlanogramImageIdCaseLeft;
            PlanogramImageIdCaseRight = dto.PlanogramImageIdCaseRight;
            ShapeType = dto.ShapeType;
            FillPatternType = dto.FillPatternType;
            FillColour = dto.FillColour;
            // Additional Product properties
            Shape = dto.Shape;
            PointOfPurchaseDescription = dto.PointOfPurchaseDescription;
            ShortDescription = dto.ShortDescription;
            Subcategory = dto.Subcategory;
            CustomerStatus = dto.CustomerStatus;
            Colour = dto.Colour;
            Flavour = dto.Flavour;
            PackagingShape = dto.PackagingShape;
            PackagingType = dto.PackagingType;
            CountryOfOrigin = dto.CountryOfOrigin;
            CountryOfProcessing = dto.CountryOfProcessing;
            ShelfLife = dto.ShelfLife;
            DeliveryFrequencyDays = dto.DeliveryFrequencyDays;
            DeliveryMethod = dto.DeliveryMethod;
            VendorCode = dto.VendorCode;
            Vendor = dto.Vendor;
            ManufacturerCode = dto.ManufacturerCode;
            Manufacturer = dto.Manufacturer;
            Size = dto.Size;
            UnitOfMeasure = dto.UnitOfMeasure;
            DateIntroduced = dto.DateIntroduced;
            DateDiscontinued = dto.DateDiscontinued;
            DateEffective = dto.DateEffective;
            Health = dto.Health;
            CorporateCode = dto.CorporateCode;
            Barcode = dto.Barcode;
            SellPrice = dto.SellPrice;
            SellPackCount = dto.SellPackCount;
            SellPackDescription = dto.SellPackDescription;
            RecommendedRetailPrice = dto.RecommendedRetailPrice;
            ManufacturerRecommendedRetailPrice = dto.ManufacturerRecommendedRetailPrice;
            CostPrice = dto.CostPrice;
            CaseCost = dto.CaseCost;
            TaxRate = dto.TaxRate;
            ConsumerInformation = dto.ConsumerInformation;
            Texture = dto.Texture;
            StyleNumber = dto.StyleNumber;
            Pattern = dto.Pattern;
            Model = dto.Model;
            GarmentType = dto.GarmentType;
            IsPrivateLabel = dto.IsPrivateLabel;
            IsNewProduct = dto.IsNewProduct;
            FinancialGroupCode = dto.FinancialGroupCode;
            FinancialGroupName = dto.FinancialGroupName;
            MetaNotAchievedInventory = dto.MetaNotAchievedInventory;
            MetaTotalUnits = dto.MetaTotalUnits;
            MetaPlanogramLinearSpacePercentage = dto.MetaPlanogramLinearSpacePercentage;
            MetaPlanogramAreaSpacePercentage = dto.MetaPlanogramAreaSpacePercentage;
            MetaPlanogramVolumetricSpacePercentage = dto.MetaPlanogramVolumetricSpacePercentage;
            MetaPositionCount = dto.MetaPositionCount;
            MetaTotalFacings = dto.MetaTotalFacings;
            MetaTotalLinearSpace = dto.MetaTotalLinearSpace;
            MetaTotalAreaSpace = dto.MetaTotalAreaSpace;
            MetaTotalVolumetricSpace = dto.MetaTotalVolumetricSpace;
            MetaIsInMasterData = dto.MetaIsInMasterData;
            MetaNotAchievedCases = dto.MetaNotAchievedCases;
            MetaNotAchievedDOS = dto.MetaNotAchievedDOS;
            MetaIsOverShelfLifePercent = dto.MetaIsOverShelfLifePercent;
            MetaNotAchievedDeliveries = dto.MetaNotAchievedDeliveries;
            MetaTotalMainFacings = dto.MetaTotalMainFacings;
            MetaTotalXFacings = dto.MetaTotalXFacings;
            MetaTotalYFacings = dto.MetaTotalYFacings;
            MetaTotalZFacings = dto.MetaTotalZFacings;
            MetaIsRangedInAssortment = dto.MetaIsRangedInAssortment;
            ColourGroupValue = dto.ColourGroupValue;
            MetaCDTNode = dto.MetaCDTNode;
            MetaIsProductRuleBroken = dto.MetaIsProductRuleBroken;
            MetaIsFamilyRuleBroken = dto.MetaIsFamilyRuleBroken;
            MetaIsInheritanceRuleBroken = dto.MetaIsInheritanceRuleBroken;
            MetaIsLocalProductRuleBroken = dto.MetaIsLocalProductRuleBroken;
            MetaIsDistributionRuleBroken = dto.MetaIsDistributionRuleBroken;
            MetaIsCoreRuleBroken = dto.MetaIsCoreRuleBroken;
            MetaIsDelistProductRuleBroken = dto.MetaIsDelistProductRuleBroken;
            MetaIsForceProductRuleBroken = dto.MetaIsForceProductRuleBroken;
            MetaIsPreserveProductRuleBroken = dto.MetaIsPreserveProductRuleBroken;
            MetaIsMinimumHurdleProductRuleBroken = dto.MetaIsMinimumHurdleProductRuleBroken;
            MetaIsMaximumProductFamilyRuleBroken = dto.MetaIsMaximumProductFamilyRuleBroken;
            MetaIsMinimumProductFamilyRuleBroken = dto.MetaIsMinimumProductFamilyRuleBroken;
            MetaIsDependencyFamilyRuleBroken = dto.MetaIsDependencyFamilyRuleBroken;
            MetaIsDelistFamilyRuleBroken = dto.MetaIsDelistFamilyRuleBroken;
            MetaComparisonStatus = dto.MetaComparisonStatus;
            MetaIsBuddied = dto.MetaIsBuddied;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramProductDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramProductDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramProductDto>.ParentId
        {
            get { return PlanogramId; }
        }

        public Boolean IsTrayProduct { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramProductDto CopyDto()
        {
            return new PlanogramProductDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Gtin = this.Gtin,
                Name = this.Name,
                Brand = this.Brand,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                DisplayHeight = this.DisplayHeight,
                DisplayWidth = this.DisplayWidth,
                DisplayDepth = this.DisplayDepth,
                AlternateHeight = this.AlternateHeight,
                AlternateWidth = this.AlternateWidth,
                AlternateDepth = this.AlternateDepth,
                PointOfPurchaseHeight = this.PointOfPurchaseHeight,
                PointOfPurchaseWidth = this.PointOfPurchaseWidth,
                PointOfPurchaseDepth = this.PointOfPurchaseDepth,
                NumberOfPegHoles = this.NumberOfPegHoles,
                PegX = this.PegX,
                PegX2 = this.PegX2,
                PegX3 = this.PegX3,
                PegY = this.PegY,
                PegY2 = this.PegY2,
                PegY3 = this.PegY3,
                PegProngOffsetX = this.PegProngOffsetX,
                PegProngOffsetY = this.PegProngOffsetY,
                PegDepth = this.PegDepth,
                SqueezeHeight = this.SqueezeHeight,
                SqueezeWidth = this.SqueezeWidth,
                SqueezeDepth = this.SqueezeDepth,
                NestingHeight = this.NestingHeight,
                NestingWidth = this.NestingWidth,
                NestingDepth = this.NestingDepth,
                CasePackUnits = this.CasePackUnits,
                CaseHigh = this.CaseHigh,
                CaseWide = this.CaseWide,
                CaseDeep = this.CaseDeep,
                CaseHeight = this.CaseHeight,
                CaseWidth = this.CaseWidth,
                CaseDepth = this.CaseDepth,
                MaxStack = this.MaxStack,
                MaxTopCap = this.MaxTopCap,
                MaxRightCap = this.MaxRightCap,
                MinDeep = this.MinDeep,
                MaxDeep = this.MaxDeep,
                TrayPackUnits = this.TrayPackUnits,
                TrayHigh = this.TrayHigh,
                TrayWide = this.TrayWide,
                TrayDeep = this.TrayDeep,
                TrayHeight = this.TrayHeight,
                TrayWidth = this.TrayWidth,
                TrayDepth = this.TrayDepth,
                TrayThickHeight = this.TrayThickHeight,
                TrayThickWidth = this.TrayThickWidth,
                TrayThickDepth = this.TrayThickDepth,
                FrontOverhang = this.FrontOverhang,
                FingerSpaceAbove = this.FingerSpaceAbove,
                FingerSpaceToTheSide = this.FingerSpaceToTheSide,
                StatusType = this.StatusType,
                OrientationType = this.OrientationType,
                MerchandisingStyle = this.MerchandisingStyle,
                IsFrontOnly = this.IsFrontOnly,
                IsPlaceHolderProduct = this.IsPlaceHolderProduct,
                IsActive = this.IsActive,
                CanBreakTrayUp = this.CanBreakTrayUp,
                CanBreakTrayDown = this.CanBreakTrayDown,
                CanBreakTrayBack = this.CanBreakTrayBack,
                CanBreakTrayTop = this.CanBreakTrayTop,
                ForceMiddleCap = this.ForceMiddleCap,
                ForceBottomCap = this.ForceBottomCap,
                PlanogramImageIdFront = this.PlanogramImageIdFront,
                PlanogramImageIdBack = this.PlanogramImageIdBack,
                PlanogramImageIdTop = this.PlanogramImageIdTop,
                PlanogramImageIdBottom = this.PlanogramImageIdBottom,
                PlanogramImageIdLeft = this.PlanogramImageIdLeft,
                PlanogramImageIdRight = this.PlanogramImageIdRight,
                PlanogramImageIdDisplayFront = this.PlanogramImageIdDisplayFront,
                PlanogramImageIdDisplayBack = this.PlanogramImageIdDisplayBack,
                PlanogramImageIdDisplayTop = this.PlanogramImageIdDisplayTop,
                PlanogramImageIdDisplayBottom = this.PlanogramImageIdDisplayBottom,
                PlanogramImageIdDisplayLeft = this.PlanogramImageIdDisplayLeft,
                PlanogramImageIdDisplayRight = this.PlanogramImageIdDisplayRight,
                PlanogramImageIdTrayFront = this.PlanogramImageIdTrayFront,
                PlanogramImageIdTrayBack = this.PlanogramImageIdTrayBack,
                PlanogramImageIdTrayTop = this.PlanogramImageIdTrayTop,
                PlanogramImageIdTrayBottom = this.PlanogramImageIdTrayBottom,
                PlanogramImageIdTrayLeft = this.PlanogramImageIdTrayLeft,
                PlanogramImageIdTrayRight = this.PlanogramImageIdTrayRight,
                PlanogramImageIdPointOfPurchaseFront = this.PlanogramImageIdPointOfPurchaseFront,
                PlanogramImageIdPointOfPurchaseBack = this.PlanogramImageIdPointOfPurchaseBack,
                PlanogramImageIdPointOfPurchaseTop = this.PlanogramImageIdPointOfPurchaseTop,
                PlanogramImageIdPointOfPurchaseBottom = this.PlanogramImageIdPointOfPurchaseBottom,
                PlanogramImageIdPointOfPurchaseLeft = this.PlanogramImageIdPointOfPurchaseLeft,
                PlanogramImageIdPointOfPurchaseRight = this.PlanogramImageIdPointOfPurchaseRight,
                PlanogramImageIdAlternateFront = this.PlanogramImageIdAlternateFront,
                PlanogramImageIdAlternateBack = this.PlanogramImageIdAlternateBack,
                PlanogramImageIdAlternateTop = this.PlanogramImageIdAlternateTop,
                PlanogramImageIdAlternateBottom = this.PlanogramImageIdAlternateBottom,
                PlanogramImageIdAlternateLeft = this.PlanogramImageIdAlternateLeft,
                PlanogramImageIdAlternateRight = this.PlanogramImageIdAlternateRight,
                PlanogramImageIdCaseFront = this.PlanogramImageIdCaseFront,
                PlanogramImageIdCaseBack = this.PlanogramImageIdCaseBack,
                PlanogramImageIdCaseTop = this.PlanogramImageIdCaseTop,
                PlanogramImageIdCaseBottom = this.PlanogramImageIdCaseBottom,
                PlanogramImageIdCaseLeft = this.PlanogramImageIdCaseLeft,
                PlanogramImageIdCaseRight = this.PlanogramImageIdCaseRight,
                ShapeType = this.ShapeType,
                FillPatternType = this.FillPatternType,
                FillColour = this.FillColour,
                // Additional Product properties
                Shape = this.Shape,
                PointOfPurchaseDescription = this.PointOfPurchaseDescription,
                ShortDescription = this.ShortDescription,
                Subcategory = this.Subcategory,
                CustomerStatus = this.CustomerStatus,
                Colour = this.Colour,
                Flavour = this.Flavour,
                PackagingShape = this.PackagingShape,
                PackagingType = this.PackagingType,
                CountryOfOrigin = this.CountryOfOrigin,
                CountryOfProcessing = this.CountryOfProcessing,
                ShelfLife = this.ShelfLife,
                DeliveryFrequencyDays = this.DeliveryFrequencyDays,
                DeliveryMethod = this.DeliveryMethod,
                VendorCode = this.VendorCode,
                Vendor = this.Vendor,
                ManufacturerCode = this.ManufacturerCode,
                Manufacturer = this.Manufacturer,
                Size = this.Size,
                UnitOfMeasure = this.UnitOfMeasure,
                DateIntroduced = this.DateIntroduced,
                DateDiscontinued = this.DateDiscontinued,
                DateEffective = this.DateEffective,
                Health = this.Health,
                CorporateCode = this.CorporateCode,
                Barcode = this.Barcode,
                SellPrice = this.SellPrice,
                SellPackCount = this.SellPackCount,
                SellPackDescription = this.SellPackDescription,
                RecommendedRetailPrice = this.RecommendedRetailPrice,
                ManufacturerRecommendedRetailPrice = this.ManufacturerRecommendedRetailPrice,
                CostPrice = this.CostPrice,
                CaseCost = this.CaseCost,
                TaxRate = this.TaxRate,
                ConsumerInformation = this.ConsumerInformation,
                Texture = this.Texture,
                StyleNumber = this.StyleNumber,
                Pattern = this.Pattern,
                Model = this.Model,
                GarmentType = this.GarmentType,
                IsPrivateLabel = this.IsPrivateLabel,
                IsNewProduct = this.IsNewProduct,
                FinancialGroupCode = this.FinancialGroupCode,
                FinancialGroupName = this.FinancialGroupName,
                MetaNotAchievedInventory = this.MetaNotAchievedInventory,
                MetaTotalUnits = this.MetaTotalUnits,
                MetaPlanogramLinearSpacePercentage = this.MetaPlanogramLinearSpacePercentage,
                MetaPlanogramAreaSpacePercentage = this.MetaPlanogramAreaSpacePercentage,
                MetaPlanogramVolumetricSpacePercentage = this.MetaPlanogramVolumetricSpacePercentage,
                MetaPositionCount = this.MetaPositionCount,
                MetaTotalFacings = this.MetaTotalFacings,
                MetaTotalLinearSpace = this.MetaTotalLinearSpace,
                MetaTotalAreaSpace = this.MetaTotalAreaSpace,
                MetaTotalVolumetricSpace = this.MetaTotalVolumetricSpace,
                MetaIsInMasterData = this.MetaIsInMasterData,
                MetaNotAchievedCases = this.MetaNotAchievedCases,
                MetaNotAchievedDOS = this.MetaNotAchievedDOS,
                MetaIsOverShelfLifePercent = this.MetaIsOverShelfLifePercent,
                MetaNotAchievedDeliveries = this.MetaNotAchievedDeliveries,
                MetaTotalMainFacings = this.MetaTotalMainFacings,
                MetaTotalXFacings = this.MetaTotalXFacings,
                MetaTotalYFacings = this.MetaTotalYFacings,
                MetaTotalZFacings = this.MetaTotalZFacings,
                MetaIsRangedInAssortment = this.MetaIsRangedInAssortment,
                ColourGroupValue = this.ColourGroupValue,
                MetaCDTNode = this.MetaCDTNode,
                MetaIsProductRuleBroken = this.MetaIsProductRuleBroken,
                MetaIsFamilyRuleBroken = this.MetaIsFamilyRuleBroken,
                MetaIsInheritanceRuleBroken = this.MetaIsInheritanceRuleBroken,
                MetaIsLocalProductRuleBroken = this.MetaIsLocalProductRuleBroken,
                MetaIsDistributionRuleBroken = this.MetaIsDistributionRuleBroken,
                MetaIsCoreRuleBroken = this.MetaIsCoreRuleBroken,
                MetaIsDelistProductRuleBroken = this.MetaIsDelistProductRuleBroken,
                MetaIsForceProductRuleBroken = this.MetaIsForceProductRuleBroken,
                MetaIsPreserveProductRuleBroken = this.MetaIsPreserveProductRuleBroken,
                MetaIsMinimumHurdleProductRuleBroken = this.MetaIsMinimumHurdleProductRuleBroken,
                MetaIsMaximumProductFamilyRuleBroken = this.MetaIsMaximumProductFamilyRuleBroken,
                MetaIsMinimumProductFamilyRuleBroken = this.MetaIsMinimumProductFamilyRuleBroken,
                MetaIsDependencyFamilyRuleBroken = this.MetaIsDependencyFamilyRuleBroken,
                MetaIsDelistFamilyRuleBroken = this.MetaIsDelistFamilyRuleBroken,
                MetaComparisonStatus = this.MetaComparisonStatus,
                MetaIsBuddied = this.MetaIsBuddied
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramProductId:
                    return Id;
                case FieldNames.PlanogramProductPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramProductGtin:
                    return Gtin;
                case FieldNames.PlanogramProductName:
                    return Name;
                case FieldNames.PlanogramProductBrand:
                    return Brand;
                case FieldNames.PlanogramProductHeight:
                    return Height;
                case FieldNames.PlanogramProductWidth:
                    return Width;
                case FieldNames.PlanogramProductDepth:
                    return Depth;
                case FieldNames.PlanogramProductDisplayHeight:
                    return DisplayHeight;
                case FieldNames.PlanogramProductDisplayWidth:
                    return DisplayWidth;
                case FieldNames.PlanogramProductDisplayDepth:
                    return DisplayDepth;
                case FieldNames.PlanogramProductAlternateHeight:
                    return AlternateHeight;
                case FieldNames.PlanogramProductAlternateWidth:
                    return AlternateWidth;
                case FieldNames.PlanogramProductAlternateDepth:
                    return AlternateDepth;
                case FieldNames.PlanogramProductPointOfPurchaseHeight:
                    return PointOfPurchaseHeight;
                case FieldNames.PlanogramProductPointOfPurchaseWidth:
                    return PointOfPurchaseWidth;
                case FieldNames.PlanogramProductPointOfPurchaseDepth:
                    return PointOfPurchaseDepth;
                case FieldNames.PlanogramProductNumberOfPegHoles:
                    return NumberOfPegHoles;
                case FieldNames.PlanogramProductPegX:
                    return PegX;
                case FieldNames.PlanogramProductPegX2:
                    return PegX2;
                case FieldNames.PlanogramProductPegX3:
                    return PegX3;
                case FieldNames.PlanogramProductPegY:
                    return PegY;
                case FieldNames.PlanogramProductPegY2:
                    return PegY2;
                case FieldNames.PlanogramProductPegY3:
                    return PegY3;
                case FieldNames.PlanogramProductPegProngOffsetX:
                    return PegProngOffsetX;
                case FieldNames.PlanogramProductPegProngOffsetY:
                    return PegProngOffsetY;
                case FieldNames.PlanogramProductPegDepth:
                    return PegDepth;
                case FieldNames.PlanogramProductSqueezeHeight:
                    return SqueezeHeight;
                case FieldNames.PlanogramProductSqueezeWidth:
                    return SqueezeWidth;
                case FieldNames.PlanogramProductSqueezeDepth:
                    return SqueezeDepth;
                case FieldNames.PlanogramProductNestingHeight:
                    return NestingHeight;
                case FieldNames.PlanogramProductNestingWidth:
                    return NestingWidth;
                case FieldNames.PlanogramProductNestingDepth:
                    return NestingDepth;
                case FieldNames.PlanogramProductCasePackUnits:
                    return CasePackUnits;
                case FieldNames.PlanogramProductCaseHigh:
                    return CaseHigh;
                case FieldNames.PlanogramProductCaseWide:
                    return CaseWide;
                case FieldNames.PlanogramProductCaseDeep:
                    return CaseDeep;
                case FieldNames.PlanogramProductCaseHeight:
                    return CaseHeight;
                case FieldNames.PlanogramProductCaseWidth:
                    return CaseWidth;
                case FieldNames.PlanogramProductCaseDepth:
                    return CaseDepth;
                case FieldNames.PlanogramProductMaxStack:
                    return MaxStack;
                case FieldNames.PlanogramProductMaxTopCap:
                    return MaxTopCap;
                case FieldNames.PlanogramProductMaxRightCap:
                    return MaxRightCap;
                case FieldNames.PlanogramProductMinDeep:
                    return MinDeep;
                case FieldNames.PlanogramProductMaxDeep:
                    return MaxDeep;
                case FieldNames.PlanogramProductTrayPackUnits:
                    return TrayPackUnits;
                case FieldNames.PlanogramProductTrayHigh:
                    return TrayHigh;
                case FieldNames.PlanogramProductTrayWide:
                    return TrayWide;
                case FieldNames.PlanogramProductTrayDeep:
                    return TrayDeep;
                case FieldNames.PlanogramProductTrayHeight:
                    return TrayHeight;
                case FieldNames.PlanogramProductTrayWidth:
                    return TrayWidth;
                case FieldNames.PlanogramProductTrayDepth:
                    return TrayDepth;
                case FieldNames.PlanogramProductTrayThickHeight:
                    return TrayThickHeight;
                case FieldNames.PlanogramProductTrayThickWidth:
                    return TrayThickWidth;
                case FieldNames.PlanogramProductTrayThickDepth:
                    return TrayThickDepth;
                case FieldNames.PlanogramProductFrontOverhang:
                    return FrontOverhang;
                case FieldNames.PlanogramProductFingerSpaceAbove:
                    return FingerSpaceAbove;
                case FieldNames.PlanogramProductFingerSpaceToTheSide:
                    return FingerSpaceToTheSide;
                case FieldNames.PlanogramProductStatusType:
                    return StatusType;
                case FieldNames.PlanogramProductOrientationType:
                    return OrientationType;
                case FieldNames.PlanogramProductMerchandisingStyle:
                    return MerchandisingStyle;
                case FieldNames.PlanogramProductIsFrontOnly:
                    return IsFrontOnly;
                case FieldNames.PlanogramProductIsTrayProduct:
                    return IsTrayProduct;
                case FieldNames.PlanogramProductIsPlaceHolderProduct:
                    return IsPlaceHolderProduct;
                case FieldNames.PlanogramProductIsActive:
                    return IsActive;
                 case FieldNames.PlanogramProductCanBreakTrayUp:
                    return CanBreakTrayUp;
                case FieldNames.PlanogramProductCanBreakTrayDown:
                    return CanBreakTrayDown;
                case FieldNames.PlanogramProductCanBreakTrayBack:
                    return CanBreakTrayBack;
                case FieldNames.PlanogramProductCanBreakTrayTop:
                    return CanBreakTrayTop;
                case FieldNames.PlanogramProductForceMiddleCap:
                    return ForceMiddleCap;
                case FieldNames.PlanogramProductForceBottomCap:
                    return ForceBottomCap;
                case FieldNames.PlanogramProductPlanogramImageIdFront:
                    return PlanogramImageIdFront;
                case FieldNames.PlanogramProductPlanogramImageIdBack:
                    return PlanogramImageIdBack;
                case FieldNames.PlanogramProductPlanogramImageIdTop:
                    return PlanogramImageIdTop;
                case FieldNames.PlanogramProductPlanogramImageIdBottom:
                    return PlanogramImageIdBottom;
                case FieldNames.PlanogramProductPlanogramImageIdLeft:
                    return PlanogramImageIdLeft;
                case FieldNames.PlanogramProductPlanogramImageIdRight:
                    return PlanogramImageIdRight;
                case FieldNames.PlanogramProductPlanogramImageIdDisplayFront:
                    return PlanogramImageIdDisplayFront;
                case FieldNames.PlanogramProductPlanogramImageIdDisplayBack:
                    return PlanogramImageIdDisplayBack;
                case FieldNames.PlanogramProductPlanogramImageIdDisplayTop:
                    return PlanogramImageIdDisplayTop;
                case FieldNames.PlanogramProductPlanogramImageIdDisplayBottom:
                    return PlanogramImageIdDisplayBottom;
                case FieldNames.PlanogramProductPlanogramImageIdDisplayLeft:
                    return PlanogramImageIdDisplayLeft;
                case FieldNames.PlanogramProductPlanogramImageIdDisplayRight:
                    return PlanogramImageIdDisplayRight;
                case FieldNames.PlanogramProductPlanogramImageIdTrayFront:
                    return PlanogramImageIdTrayFront;
                case FieldNames.PlanogramProductPlanogramImageIdTrayBack:
                    return PlanogramImageIdTrayBack;
                case FieldNames.PlanogramProductPlanogramImageIdTrayTop:
                    return PlanogramImageIdTrayTop;
                case FieldNames.PlanogramProductPlanogramImageIdTrayBottom:
                    return PlanogramImageIdTrayBottom;
                case FieldNames.PlanogramProductPlanogramImageIdTrayLeft:
                    return PlanogramImageIdTrayLeft;
                case FieldNames.PlanogramProductPlanogramImageIdTrayRight:
                    return PlanogramImageIdTrayRight;
                case FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseFront:
                    return PlanogramImageIdPointOfPurchaseFront;
                case FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBack:
                    return PlanogramImageIdPointOfPurchaseBack;
                case FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseTop:
                    return PlanogramImageIdPointOfPurchaseTop;
                case FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBottom:
                    return PlanogramImageIdPointOfPurchaseBottom;
                case FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseLeft:
                    return PlanogramImageIdPointOfPurchaseLeft;
                case FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseRight:
                    return PlanogramImageIdPointOfPurchaseRight;
                case FieldNames.PlanogramProductPlanogramImageIdAlternateFront:
                    return PlanogramImageIdAlternateFront;
                case FieldNames.PlanogramProductPlanogramImageIdAlternateBack:
                    return PlanogramImageIdAlternateBack;
                case FieldNames.PlanogramProductPlanogramImageIdAlternateTop:
                    return PlanogramImageIdAlternateTop;
                case FieldNames.PlanogramProductPlanogramImageIdAlternateBottom:
                    return PlanogramImageIdAlternateBottom;
                case FieldNames.PlanogramProductPlanogramImageIdAlternateLeft:
                    return PlanogramImageIdAlternateLeft;
                case FieldNames.PlanogramProductPlanogramImageIdAlternateRight:
                    return PlanogramImageIdAlternateRight;
                case FieldNames.PlanogramProductPlanogramImageIdCaseFront:
                    return PlanogramImageIdCaseFront;
                case FieldNames.PlanogramProductPlanogramImageIdCaseBack:
                    return PlanogramImageIdCaseBack;
                case FieldNames.PlanogramProductPlanogramImageIdCaseTop:
                    return PlanogramImageIdCaseTop;
                case FieldNames.PlanogramProductPlanogramImageIdCaseBottom:
                    return PlanogramImageIdCaseBottom;
                case FieldNames.PlanogramProductPlanogramImageIdCaseLeft:
                    return PlanogramImageIdCaseLeft;
                case FieldNames.PlanogramProductPlanogramImageIdCaseRight:
                    return PlanogramImageIdCaseRight;
                case FieldNames.PlanogramProductShapeType:
                    return ShapeType;
                case FieldNames.PlanogramProductFillPatternType:
                    return FillPatternType;
                case FieldNames.PlanogramProductFillColour:
                    return FillColour;
                // Additional Product properties
                case FieldNames.PlanogramProductShape:
                    return Shape;
                case FieldNames.PlanogramProductPointOfPurchaseDescription:
                    return PointOfPurchaseDescription;
                case FieldNames.PlanogramProductShortDescription:
                    return ShortDescription;
                case FieldNames.PlanogramProductSubcategory:
                    return Subcategory;
                case FieldNames.PlanogramProductCustomerStatus:
                    return CustomerStatus;
                case FieldNames.PlanogramProductColour:
                    return Colour;
                case FieldNames.PlanogramProductFlavour:
                    return Flavour;
                case FieldNames.PlanogramProductPackagingShape:
                    return PackagingShape;
                case FieldNames.PlanogramProductPackagingType:
                    return PackagingType;
                case FieldNames.PlanogramProductCountryOfOrigin:
                    return CountryOfOrigin;
                case FieldNames.PlanogramProductCountryOfProcessing:
                    return CountryOfProcessing;
                case FieldNames.PlanogramProductShelfLife:
                    return ShelfLife;
                case FieldNames.PlanogramProductDeliveryFrequencyDays:
                    return DeliveryFrequencyDays;
                case FieldNames.PlanogramProductDeliveryMethod:
                    return DeliveryMethod;
                case FieldNames.PlanogramProductVendorCode:
                    return VendorCode;
                case FieldNames.PlanogramProductVendor:
                    return Vendor;
                case FieldNames.PlanogramProductManufacturerCode:
                    return ManufacturerCode;
                case FieldNames.PlanogramProductManufacturer:
                    return Manufacturer;
                case FieldNames.PlanogramProductSize:
                    return Size;
                case FieldNames.PlanogramProductUnitOfMeasure:
                    return UnitOfMeasure;
                case FieldNames.PlanogramProductDateIntroduced:
                    return DateIntroduced;
                case FieldNames.PlanogramProductDateDiscontinued:
                    return DateDiscontinued;
                case FieldNames.PlanogramProductDateEffective:
                    return DateEffective;
                case FieldNames.PlanogramProductHealth:
                    return Health;
                case FieldNames.PlanogramProductCorporateCode:
                    return CorporateCode;
                case FieldNames.PlanogramProductBarcode:
                    return Barcode;
                case FieldNames.PlanogramProductSellPrice:
                    return SellPrice;
                case FieldNames.PlanogramProductSellPackCount:
                    return SellPackCount;
                case FieldNames.PlanogramProductSellPackDescription:
                    return SellPackDescription;
                case FieldNames.PlanogramProductRecommendedRetailPrice:
                    return RecommendedRetailPrice;
                case FieldNames.PlanogramProductManufacturerRecommendedRetailPrice:
                    return ManufacturerRecommendedRetailPrice;
                case FieldNames.PlanogramProductCostPrice:
                    return CostPrice;
                case FieldNames.PlanogramProductCaseCost:
                    return CaseCost;
                case FieldNames.PlanogramProductTaxRate:
                    return TaxRate;
                case FieldNames.PlanogramProductConsumerInformation:
                    return ConsumerInformation;
                case FieldNames.PlanogramProductTexture:
                    return Texture;
                case FieldNames.PlanogramProductStyleNumber:
                    return StyleNumber;
                case FieldNames.PlanogramProductPattern:
                    return Pattern;
                case FieldNames.PlanogramProductModel:
                    return Model;
                case FieldNames.PlanogramProductGarmentType:
                    return GarmentType;
                case FieldNames.PlanogramProductIsPrivateLabel:
                    return IsPrivateLabel;
                case FieldNames.PlanogramProductIsNewProduct:
                    return IsNewProduct;
                case FieldNames.PlanogramProductFinancialGroupCode:
                    return FinancialGroupCode;
                case FieldNames.PlanogramProductFinancialGroupName:
                    return FinancialGroupName;
                case FieldNames.PlanogramProductMetaNotAchievedInventory:
                    return MetaNotAchievedInventory;
                case FieldNames.PlanogramProductMetaTotalUnits:
                    return MetaTotalUnits;
                case FieldNames.PlanogramProductMetaPlanogramLinearSpacePercentage:
                    return MetaPlanogramLinearSpacePercentage;
                case FieldNames.PlanogramProductMetaPlanogramAreaSpacePercentage:
                    return MetaPlanogramAreaSpacePercentage;
                case FieldNames.PlanogramProductMetaPlanogramVolumetricSpacePercentage:
                    return MetaPlanogramVolumetricSpacePercentage;
                case FieldNames.PlanogramProductMetaPositionCount:
                    return MetaPositionCount;
                case FieldNames.PlanogramProductMetaTotalFacings:
                    return MetaTotalFacings;
                case FieldNames.PlanogramProductMetaTotalLinearSpace:
                    return MetaTotalLinearSpace;
                case FieldNames.PlanogramProductMetaTotalAreaSpace:
                    return MetaTotalAreaSpace;
                case FieldNames.PlanogramProductMetaTotalVolumetricSpace:
                    return MetaTotalVolumetricSpace;
                case FieldNames.PlanogramProductMetaIsInMasterData:
                    return MetaIsInMasterData;
                case FieldNames.PlanogramProductMetaNotAchievedCases:
                    return MetaNotAchievedCases;
                case FieldNames.PlanogramProductMetaNotAchievedDOS:
                    return MetaNotAchievedDOS;
                case FieldNames.PlanogramProductMetaIsOverShelfLifePercent:
                    return MetaIsOverShelfLifePercent;
                case FieldNames.PlanogramProductMetaNotAchievedDeliveries:
                    return MetaNotAchievedDeliveries;
                case FieldNames.PlanogramProductMetaTotalMainFacings:
                    return MetaTotalMainFacings;
                case FieldNames.PlanogramProductMetaTotalXFacings:
                    return MetaTotalXFacings;
                case FieldNames.PlanogramProductMetaTotalYFacings:
                    return MetaTotalYFacings;
                case FieldNames.PlanogramProductMetaTotalZFacings:
                    return MetaTotalZFacings;
                case FieldNames.PlanogramProductMetaIsRangedInAssortment:
                    return MetaIsRangedInAssortment;
                case FieldNames.PlanogramProductColourGroupValue:
                    return ColourGroupValue;
                case FieldNames.PlanogramProductMetaCDTNode:
                    return MetaCDTNode;
                case FieldNames.PlanogramProductMetaIsProductRuleBroken:
                    return MetaIsProductRuleBroken;
                case FieldNames.PlanogramProductMetaIsFamilyRuleBroken:
                    return MetaIsFamilyRuleBroken;
                case FieldNames.PlanogramProductMetaIsInheritanceRuleBroken:
                    return MetaIsInheritanceRuleBroken;
                case FieldNames.PlanogramProductMetaIsLocalProductRuleBroken:
                    return MetaIsLocalProductRuleBroken;
                case FieldNames.PlanogramProductMetaIsDistributionRuleBroken:
                    return MetaIsDistributionRuleBroken;
                case FieldNames.PlanogramProductMetaIsCoreRuleBroken:
                    return MetaIsCoreRuleBroken;
                case FieldNames.PlanogramProductMetaIsDelistProductRuleBroken:
                    return MetaIsDelistProductRuleBroken;
                case FieldNames.PlanogramProductMetaIsForceProductRuleBroken:
                    return MetaIsForceProductRuleBroken;
                case FieldNames.PlanogramProductMetaIsPreserveProductRuleBroken:
                    return MetaIsPreserveProductRuleBroken;
                case FieldNames.PlanogramProductMetaIsMinimumHurdleProductRuleBroken:
                    return MetaIsMinimumHurdleProductRuleBroken;
                case FieldNames.PlanogramProductMetaIsMaximumProductFamilyRuleBroken:
                    return MetaIsMaximumProductFamilyRuleBroken;
                case FieldNames.PlanogramProductMetaIsMinimumProductFamilyRuleBroken:
                    return MetaIsMinimumProductFamilyRuleBroken;
                case FieldNames.PlanogramProductMetaIsDependencyFamilyRuleBroken:
                    return MetaIsDependencyFamilyRuleBroken;
                case FieldNames.PlanogramProductMetaIsDelistFamilyRuleBroken:
                    return MetaIsDelistFamilyRuleBroken;
                case FieldNames.PlanogramProductMetaComparisonStatus:
                    return MetaComparisonStatus;
                case FieldNames.PlanogramProductMetaIsBuddied:
                    return MetaIsBuddied;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
