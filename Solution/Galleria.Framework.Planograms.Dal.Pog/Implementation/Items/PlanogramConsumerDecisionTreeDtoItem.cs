﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramConsumerDecisionTreeDtoItem : PlanogramConsumerDecisionTreeDto, IChildDtoItem<PlanogramConsumerDecisionTreeDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramConsumerDecisionTreeDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreePlanogramId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeName);
        }

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeDtoItem and populates its properties with values taken from a 
        /// PlanogramConsumerDecisionTreeDto.
        /// </summary>
        public PlanogramConsumerDecisionTreeDtoItem(PlanogramConsumerDecisionTreeDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Name = dto.Name;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramConsumerDecisionTreeDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramConsumerDecisionTreeDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramConsumerDecisionTreeDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramConsumerDecisionTreeDto CopyDto()
        {
            return new PlanogramConsumerDecisionTreeDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Name = this.Name
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramConsumerDecisionTreeId:
                    return Id;
                case FieldNames.PlanogramConsumerDecisionTreePlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramConsumerDecisionTreeName:
                    return Name;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
