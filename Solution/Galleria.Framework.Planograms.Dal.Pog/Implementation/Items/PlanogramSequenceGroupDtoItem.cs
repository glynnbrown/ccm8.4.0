﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramSequenceGroupDtoItem : PlanogramSequenceGroupDto, IChildDtoItem<PlanogramSequenceGroupDto>
    {
        #region Constructors

        /// <summary>
        ///     Creates a new <see cref="PlanogramSequenceGroupDtoItem"/> and populates it with values from the <paramref name="item"/>.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem"/> from which to take the values for this instance.</param>
        public PlanogramSequenceGroupDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupId));
            PlanogramSequenceId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupPlanogramSequenceId));
            Name = (String) item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupName);
            Colour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupColour));
        }

        /// <summary>
        ///     Creates a new <see cref="PlanogramSequenceGroupDtoItem"/> and populates it with values from the <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupDto"/> from which to take the values for this instance.</param>
        public PlanogramSequenceGroupDtoItem(PlanogramSequenceGroupDto dto)
        {
            Id = dto.Id;
            PlanogramSequenceId = dto.PlanogramSequenceId;
            Name = dto.Name;
            Colour = dto.Colour;
        }

        #endregion

        #region IChildDtoItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramSequenceGroupId:
                    return Id;
                case FieldNames.PlanogramSequenceGroupPlanogramSequenceId:
                    return PlanogramSequenceId;
                case FieldNames.PlanogramSequenceGroupName:
                    return Name;
                case FieldNames.PlanogramSequenceGroupColour:
                    return Colour;
                default:
                    return null;
            }
        }

        /// <summary>
        ///     Create and return a new instance with identical values to this one.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramSequenceGroupDto"/>.</returns>
        public PlanogramSequenceGroupDto CopyDto()
        {
            return new PlanogramSequenceGroupDto
            {
                Id = Id,
                PlanogramSequenceId = PlanogramSequenceId,
                Name = Name,
                Colour = Colour
            };
        }

        public Boolean AutoIncrementId
        {
            get { return true; }
        }

        public Object ParentId
        {
            get { return PlanogramSequenceId; }
        }

        #endregion
    }
}
