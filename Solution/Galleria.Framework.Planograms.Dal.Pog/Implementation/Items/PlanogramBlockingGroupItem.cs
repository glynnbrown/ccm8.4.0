﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
// V8-27476 : L.Luong
//  Added BlockPlacementXType and BlockPlacementYType
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#region Version History : CCM 820
// V8-30765 :I.George
//	Added PlanogramBlockingGroup metaData Properties
// V8-30737 : M.Shelley
//  Added Meta performance fields P1-P20 and MetaP1Percentage-MetaP20Percentage
#endregion
#region Version History : CCM 830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramBlockingGroupDtoItem : PlanogramBlockingGroupDto, IChildDtoItem<PlanogramBlockingGroupDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramBlockingGroupDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramBlockingGroupDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupId));
            PlanogramBlockingId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupPlanogramBlockingId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupName);
            Colour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupColour));
            FillPatternType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupFillPatternType));
            CanCompromiseSequence = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupCanCompromiseSequence));
            IsRestrictedByComponentType = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupIsRestrictedByComponentType));            
            CanMerge = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupCanMerge));
            IsLimited = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupIsLimited));
            LimitedPercentage = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupLimitedPercentage));

            BlockPlacementPrimaryType = GalleriaBinaryFileItemHelper.GetByte(
                item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupBlockPlacementPrimaryType));
            BlockPlacementSecondaryType = GalleriaBinaryFileItemHelper.GetByte(
                item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupBlockPlacementSecondaryType));
            BlockPlacementTertiaryType = GalleriaBinaryFileItemHelper.GetByte(
                item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupBlockPlacementTertiaryType));
            TotalSpacePercentage = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupTotalSpacePercentage));
            //metaData
            MetaCountOfProducts = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaCountOfProducts));
            MetaCountOfProductRecommendedOnPlanogram = 
                GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram));
            MetaCountOfProductPlacedOnPlanogram = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram));
            MetaOriginalPercentAllocated = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaOriginalPercentAllocated));
            MetaPerformancePercentAllocated = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaPerformancePercentAllocated));
            MetaFinalPercentAllocated = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaFinalPercentAllocated));
            MetaLinearProductPlacementPercent = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaLinearProductPlacementPercent));
            MetaAreaProductPlacementPercent = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaAreaProductPlacementPercent));
            MetaVolumetricProductPlacementPercent = 
                GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaVolumetricProductPlacementPercent));

            P1 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP1));
            P2 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP2));
            P3 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP3));
            P4 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP4));
            P5 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP5));
            P6 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP6));
            P7 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP7));
            P8 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP8));
            P9 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP9));
            P10 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP10));
            P11 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP11));
            P12 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP12));
            P13 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP13));
            P14 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP14));
            P15 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP15));
            P16 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP16));
            P17 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP17));
            P18 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP18));
            P19 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP19));
            P20 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupP20));

            MetaP1Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP1Percentage));
            MetaP2Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP2Percentage));
            MetaP3Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP3Percentage));
            MetaP4Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP4Percentage));
            MetaP5Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP5Percentage));
            MetaP6Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP6Percentage));
            MetaP7Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP7Percentage));
            MetaP8Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP8Percentage));
            MetaP9Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP9Percentage));
            MetaP10Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP10Percentage));
            MetaP11Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP11Percentage));
            MetaP12Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP12Percentage));
            MetaP13Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP13Percentage));
            MetaP14Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP14Percentage));
            MetaP15Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP15Percentage));
            MetaP16Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP16Percentage));
            MetaP17Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP17Percentage));
            MetaP18Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP18Percentage));
            MetaP19Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP19Percentage));
            MetaP20Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramBlockingGroupMetaP20Percentage));
        }

        /// <summary>
        /// Creates a new PlanogramBlockingGroupDtoItem and populates its properties with values taken from a 
        /// PlanogramBlockingGroupDto.
        /// </summary>
        public PlanogramBlockingGroupDtoItem(PlanogramBlockingGroupDto dto)
        {
            Id = dto.Id;
            PlanogramBlockingId = dto.PlanogramBlockingId;
            Name = dto.Name;
            Colour = dto.Colour;
            FillPatternType = dto.FillPatternType;
            CanCompromiseSequence = dto.CanCompromiseSequence;
            IsRestrictedByComponentType = dto.IsRestrictedByComponentType;
            CanMerge = dto.CanMerge;
            IsLimited = dto.IsLimited;
            LimitedPercentage = dto.LimitedPercentage;
            BlockPlacementPrimaryType = dto.BlockPlacementPrimaryType;
            BlockPlacementSecondaryType = dto.BlockPlacementSecondaryType;
            BlockPlacementTertiaryType = dto.BlockPlacementTertiaryType;
            TotalSpacePercentage = dto.TotalSpacePercentage;
            MetaCountOfProducts = dto.MetaCountOfProducts;
            MetaCountOfProductRecommendedOnPlanogram = dto.MetaCountOfProductRecommendedOnPlanogram;
            MetaCountOfProductPlacedOnPlanogram = dto.MetaCountOfProductPlacedOnPlanogram;
            MetaOriginalPercentAllocated = dto.MetaOriginalPercentAllocated;
            MetaPerformancePercentAllocated = dto.MetaPerformancePercentAllocated;
            MetaFinalPercentAllocated = dto.MetaFinalPercentAllocated;
            MetaLinearProductPlacementPercent = dto.MetaLinearProductPlacementPercent;
            MetaAreaProductPlacementPercent = dto.MetaAreaProductPlacementPercent;
            MetaVolumetricProductPlacementPercent = dto.MetaVolumetricProductPlacementPercent;

            P1 = dto.P1;
            P2  = dto.P2;
            P3  = dto.P3;
            P4  = dto.P4;
            P5  = dto.P5;
            P6  = dto.P6;
            P7  = dto.P7;
            P8  = dto.P8;
            P9  = dto.P9;
            P10 = dto.P10;
            P11 = dto.P11;
            P12 = dto.P12;
            P13 = dto.P13;
            P14 = dto.P14;
            P15 = dto.P15;
            P16 = dto.P16;
            P17 = dto.P17;
            P18 = dto.P18;
            P19 = dto.P19;
            P20 = dto.P20;

            MetaP1Percentage  = dto.MetaP1Percentage;
            MetaP2Percentage  = dto.MetaP2Percentage;
            MetaP3Percentage  = dto.MetaP3Percentage;
            MetaP4Percentage  = dto.MetaP4Percentage;
            MetaP5Percentage  = dto.MetaP5Percentage;
            MetaP6Percentage  = dto.MetaP6Percentage;
            MetaP7Percentage  = dto.MetaP7Percentage;
            MetaP8Percentage  = dto.MetaP8Percentage;
            MetaP9Percentage  = dto.MetaP9Percentage;
            MetaP10Percentage = dto.MetaP10Percentage;
            MetaP11Percentage = dto.MetaP11Percentage;
            MetaP12Percentage = dto.MetaP12Percentage;
            MetaP13Percentage = dto.MetaP13Percentage;
            MetaP14Percentage = dto.MetaP14Percentage;
            MetaP15Percentage = dto.MetaP15Percentage;
            MetaP16Percentage = dto.MetaP16Percentage;
            MetaP17Percentage = dto.MetaP17Percentage;
            MetaP18Percentage = dto.MetaP18Percentage;
            MetaP19Percentage = dto.MetaP19Percentage;
            MetaP20Percentage = dto.MetaP20Percentage;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramBlockingGroupDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramBlockingGroupDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramBlockingGroupDto>.ParentId
        {
            get { return PlanogramBlockingId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramBlockingGroupDto CopyDto()
        {
            return new PlanogramBlockingGroupDto()
            {
                Id = this.Id,
                PlanogramBlockingId = this.PlanogramBlockingId,
                Name = this.Name,
                Colour = this.Colour,
                FillPatternType = this.FillPatternType,
                CanCompromiseSequence = this.CanCompromiseSequence,
                IsRestrictedByComponentType = this.IsRestrictedByComponentType,
                CanMerge = this.CanMerge,
                IsLimited = this.IsLimited,
                LimitedPercentage = this.LimitedPercentage,
                BlockPlacementPrimaryType = this.BlockPlacementPrimaryType,
                BlockPlacementSecondaryType = this.BlockPlacementSecondaryType,
                BlockPlacementTertiaryType = this.BlockPlacementTertiaryType,
                TotalSpacePercentage = this.TotalSpacePercentage,
                MetaCountOfProducts = this.MetaCountOfProducts,
                MetaCountOfProductRecommendedOnPlanogram = this.MetaCountOfProductRecommendedOnPlanogram,
                MetaCountOfProductPlacedOnPlanogram = this.MetaCountOfProductPlacedOnPlanogram,
                MetaOriginalPercentAllocated = this.MetaOriginalPercentAllocated,
                MetaPerformancePercentAllocated = this.MetaPerformancePercentAllocated,
                MetaFinalPercentAllocated = this.MetaFinalPercentAllocated,
                MetaLinearProductPlacementPercent = this.MetaLinearProductPlacementPercent,
                MetaAreaProductPlacementPercent = this.MetaAreaProductPlacementPercent,
                MetaVolumetricProductPlacementPercent = this.MetaVolumetricProductPlacementPercent,

                P1  = this.P1,
                P2  = this.P2,
                P3  = this.P3,
                P4  = this.P4,
                P5  = this.P5,
                P6  = this.P6,
                P7  = this.P7,
                P8  = this.P8,
                P9  = this.P9,
                P10 = this.P10,
                P11 = this.P11,
                P12 = this.P12,
                P13 = this.P13,
                P14 = this.P14,
                P15 = this.P15,
                P16 = this.P16,
                P17 = this.P17,
                P18 = this.P18,
                P19 = this.P19,
                P20 = this.P20,
                MetaP1Percentage = this.MetaP1Percentage,
                MetaP2Percentage = this.MetaP2Percentage,
                MetaP3Percentage = this.MetaP3Percentage,
                MetaP4Percentage = this.MetaP4Percentage,
                MetaP5Percentage = this.MetaP5Percentage,
                MetaP6Percentage = this.MetaP6Percentage,
                MetaP7Percentage = this.MetaP7Percentage,
                MetaP8Percentage = this.MetaP8Percentage,
                MetaP9Percentage = this.MetaP9Percentage,
                MetaP10Percentage = this.MetaP10Percentage,
                MetaP11Percentage = this.MetaP11Percentage,
                MetaP12Percentage = this.MetaP12Percentage,
                MetaP13Percentage = this.MetaP13Percentage,
                MetaP14Percentage = this.MetaP14Percentage,
                MetaP15Percentage = this.MetaP15Percentage,
                MetaP16Percentage = this.MetaP16Percentage,
                MetaP17Percentage = this.MetaP17Percentage,
                MetaP18Percentage = this.MetaP18Percentage,
                MetaP19Percentage = this.MetaP19Percentage,
                MetaP20Percentage = this.MetaP20Percentage,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramBlockingGroupId:
                    return Id;
                case FieldNames.PlanogramBlockingGroupPlanogramBlockingId:
                    return PlanogramBlockingId;
                case FieldNames.PlanogramBlockingGroupName:
                    return Name;
                case FieldNames.PlanogramBlockingGroupColour:
                    return Colour;
                case FieldNames.PlanogramBlockingGroupFillPatternType:
                    return FillPatternType;
                case FieldNames.PlanogramBlockingGroupCanCompromiseSequence:
                    return CanCompromiseSequence;
                case FieldNames.PlanogramBlockingGroupIsRestrictedByComponentType:
                    return IsRestrictedByComponentType;
                case FieldNames.PlanogramBlockingGroupCanMerge:
                    return CanMerge;
                case FieldNames.PlanogramBlockingGroupIsLimited:
                    return IsLimited;
                case FieldNames.PlanogramBlockingGroupLimitedPercentage:
                    return LimitedPercentage;
                case FieldNames.PlanogramBlockingGroupBlockPlacementPrimaryType:
                    return BlockPlacementPrimaryType;
                case FieldNames.PlanogramBlockingGroupBlockPlacementSecondaryType:
                    return BlockPlacementSecondaryType;
                case FieldNames.PlanogramBlockingGroupBlockPlacementTertiaryType:
                    return BlockPlacementTertiaryType;
                case FieldNames.PlanogramBlockingGroupTotalSpacePercentage:
                    return TotalSpacePercentage;
                case FieldNames.PlanogramBlockingGroupMetaCountOfProducts:
                    return MetaCountOfProducts;
                case FieldNames.PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram:
                    return MetaCountOfProductRecommendedOnPlanogram;
                case FieldNames.PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram:
                    return MetaCountOfProductPlacedOnPlanogram;
                case FieldNames.PlanogramBlockingGroupMetaOriginalPercentAllocated:
                    return MetaOriginalPercentAllocated;
                case FieldNames.PlanogramBlockingGroupMetaPerformancePercentAllocated:
                    return MetaPerformancePercentAllocated;
                case FieldNames.PlanogramBlockingGroupMetaFinalPercentAllocated:
                    return MetaFinalPercentAllocated;
                case FieldNames.PlanogramBlockingGroupMetaLinearProductPlacementPercent:
                    return MetaLinearProductPlacementPercent;
                case FieldNames.PlanogramBlockingGroupMetaAreaProductPlacementPercent:
                    return MetaAreaProductPlacementPercent;
                case FieldNames.PlanogramBlockingGroupMetaVolumetricProductPlacementPercent:
                    return MetaVolumetricProductPlacementPercent;

                case FieldNames.PlanogramConsumerDecisionTreeNodeP1: return P1;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP2: return P2;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP3: return P3;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP4: return P4;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP5: return P5;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP6: return P6;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP7: return P7;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP8: return P8;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP9: return P9;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP10: return P10;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP11: return P11;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP12: return P12;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP13: return P13;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP14: return P14;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP15: return P15;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP16: return P16;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP17: return P17;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP18: return P18;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP19: return P19;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP20: return P20;

                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Percentage: return MetaP1Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Percentage: return MetaP2Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Percentage: return MetaP3Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Percentage: return MetaP4Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Percentage: return MetaP5Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Percentage: return MetaP6Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Percentage: return MetaP7Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Percentage: return MetaP8Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Percentage: return MetaP9Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Percentage: return MetaP10Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Percentage: return MetaP11Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Percentage: return MetaP12Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Percentage: return MetaP13Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Percentage: return MetaP14Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Percentage: return MetaP15Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Percentage: return MetaP16Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Percentage: return MetaP17Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Percentage: return MetaP18Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage: return MetaP19Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Percentage: return MetaP20Percentage;

                default:
                    return null;
            }
        }

        #endregion

        #endregion

    }
}


