﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26147 : L.Ineson
//  Initial version.
#endregion
#region Version History: CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class CustomAttributeDataDtoItem : CustomAttributeDataDto, IChildDtoItem<CustomAttributeDataDto>, GalleriaBinaryFile.IItem
    {
        #region Nested Classes

        internal class CustomAttributeDataDtoItemParentKey
        {
            #region Properties

            public Object ParentId { get; set; }
            public Byte ParentType { get; set; }

            #endregion

            #region Methods
            /// <summary>
            /// Returns a hash code for this object
            /// </summary>
            /// <returns>The object hash code</returns>
            public override Int32 GetHashCode()
            {
                return ParentId.GetHashCode()
                    + ParentType.GetHashCode();
            }

            /// <summary>
            /// Check to see if two keys are the ISOme
            /// </summary>
            /// <param name="obj">The object to compare against</param>
            /// <returns>true if objects are equal</returns>
            public override Boolean Equals(Object obj)
            {
                CustomAttributeDataDtoItemParentKey other = obj as CustomAttributeDataDtoItemParentKey;
                if (other != null)
                {
                    // ParentId
                    if ((other.ParentId != null) && (this.ParentId == null)) return false;
                    if ((other.ParentId == null) && (this.ParentId != null)) return false;
                    if ((other.ParentId != null) && (this.ParentId != null)) if (!other.ParentId.Equals(this.ParentId)) return false;

                    // ParentType
                    if (other.ParentType != this.ParentType) return false;
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion

        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new CustomAttributeDataDtoItem and populates its properties with values taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public CustomAttributeDataDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.CustomAttributeDataId));
            ParentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.CustomAttributeDataParentId));
            ParentType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.CustomAttributeDataParentType));

            Text1 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText1);
            Text2 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText2);
            Text3 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText3);
            Text4 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText4);
            Text5 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText5);
            Text6 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText6);
            Text7 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText7);
            Text8 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText8);
            Text9 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText9);
            Text10 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText10);
            Text11 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText11);
            Text12 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText12);
            Text13 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText13);
            Text14 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText14);
            Text15 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText15);
            Text16 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText16);
            Text17 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText17);
            Text18 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText18);
            Text19 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText19);
            Text20 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText20);
            Text21 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText21);
            Text22 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText22);
            Text23 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText23);
            Text24 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText24);
            Text25 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText25);
            Text26 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText26);
            Text27 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText27);
            Text28 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText28);
            Text29 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText29);
            Text30 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText30);
            Text31 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText31);
            Text32 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText32);
            Text33 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText33);
            Text34 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText34);
            Text35 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText35);
            Text36 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText36);
            Text37 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText37);
            Text38 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText38);
            Text39 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText39);
            Text40 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText40);
            Text41 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText41);
            Text42 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText42);
            Text43 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText43);
            Text44 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText44);
            Text45 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText45);
            Text46 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText46);
            Text47 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText47);
            Text48 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText48);
            Text49 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText49);
            Text50 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataText50);

            Value1 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue1));
            Value2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue2));
            Value3 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue3));
            Value4 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue4));
            Value5 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue5));
            Value6 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue6));
            Value7 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue7));
            Value8 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue8));
            Value9 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue9));
            Value10 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue10));
            Value11 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue11));
            Value12 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue12));
            Value13 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue13));
            Value14 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue14));
            Value15 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue15));
            Value16 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue16));
            Value17 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue17));
            Value18 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue18));
            Value19 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue19));
            Value20 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue20));
            Value21 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue21));
            Value22 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue22));
            Value23 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue23));
            Value24 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue24));
            Value25 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue25));
            Value26 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue26));
            Value27 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue27));
            Value28 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue28));
            Value29 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue29));
            Value30 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue30));
            Value31 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue31));
            Value32 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue32));
            Value33 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue33));
            Value34 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue34));
            Value35 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue35));
            Value36 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue36));
            Value37 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue37));
            Value38 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue38));
            Value39 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue39));
            Value40 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue40));
            Value41 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue41));
            Value42 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue42));
            Value43 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue43));
            Value44 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue44));
            Value45 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue45));
            Value46 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue46));
            Value47 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue47));
            Value48 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue48));
            Value49 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue49));
            Value50 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.CustomAttributeDataValue50));

            Flag1 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag1));
            Flag2 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag2));
            Flag3 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag3));
            Flag4 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag4));
            Flag5 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag5));
            Flag6 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag6));
            Flag7 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag7));
            Flag8 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag8));
            Flag9 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag9));
            Flag10 = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.CustomAttributeDataFlag10));

            Date1 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate1));
            Date2 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate2));
            Date3 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate3));
            Date4 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate4));
            Date5 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate5));
            Date6 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate6));
            Date7 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate7));
            Date8 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate8));
            Date9 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate9));
            Date10 = GalleriaBinaryFileItemHelper.GetDateTimeNullable(item.GetItemPropertyValue(FieldNames.CustomAttributeDataDate10));

            Note1 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataNote1);
            Note2 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataNote2);
            Note3 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataNote3);
            Note4 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataNote4);
            Note5 = (String)item.GetItemPropertyValue(FieldNames.CustomAttributeDataNote5);
        }

        /// <summary>
        /// Creates a new CustomAttributeDataDtoItem and populates its properties with values taken from a CustomAttributeDataDto.
        /// </summary>
        public CustomAttributeDataDtoItem(CustomAttributeDataDto dto)
        {
            Id = dto.Id;
            ParentId = dto.ParentId;
            ParentType = dto.ParentType;

            Text1 = dto.Text1;
            Text2 = dto.Text2;
            Text3 = dto.Text3;
            Text4 = dto.Text4;
            Text5 = dto.Text5;
            Text6 = dto.Text6;
            Text7 = dto.Text7;
            Text8 = dto.Text8;
            Text9 = dto.Text9;
            Text10 = dto.Text10;
            Text11 = dto.Text11;
            Text12 = dto.Text12;
            Text13 = dto.Text13;
            Text14 = dto.Text14;
            Text15 = dto.Text15;
            Text16 = dto.Text16;
            Text17 = dto.Text17;
            Text18 = dto.Text18;
            Text19 = dto.Text19;
            Text20 = dto.Text20;
            Text21 = dto.Text21;
            Text22 = dto.Text22;
            Text23 = dto.Text23;
            Text24 = dto.Text24;
            Text25 = dto.Text25;
            Text26 = dto.Text26;
            Text27 = dto.Text27;
            Text28 = dto.Text28;
            Text29 = dto.Text29;
            Text30 = dto.Text30;
            Text31 = dto.Text31;
            Text32 = dto.Text32;
            Text33 = dto.Text33;
            Text34 = dto.Text34;
            Text35 = dto.Text35;
            Text36 = dto.Text36;
            Text37 = dto.Text37;
            Text38 = dto.Text38;
            Text39 = dto.Text39;
            Text40 = dto.Text40;
            Text41 = dto.Text41;
            Text42 = dto.Text42;
            Text43 = dto.Text43;
            Text44 = dto.Text44;
            Text45 = dto.Text45;
            Text46 = dto.Text46;
            Text47 = dto.Text47;
            Text48 = dto.Text48;
            Text49 = dto.Text49;
            Text50 = dto.Text50;

            Value1 = dto.Value1;
            Value2 = dto.Value2;
            Value3 = dto.Value3;
            Value4 = dto.Value4;
            Value5 = dto.Value5;
            Value6 = dto.Value6;
            Value7 = dto.Value7;
            Value8 = dto.Value8;
            Value9 = dto.Value9;
            Value10 = dto.Value10;
            Value11 = dto.Value11;
            Value12 = dto.Value12;
            Value13 = dto.Value13;
            Value14 = dto.Value14;
            Value15 = dto.Value15;
            Value16 = dto.Value16;
            Value17 = dto.Value17;
            Value18 = dto.Value18;
            Value19 = dto.Value19;
            Value20 = dto.Value20;
            Value21 = dto.Value21;
            Value22 = dto.Value22;
            Value23 = dto.Value23;
            Value24 = dto.Value24;
            Value25 = dto.Value25;
            Value26 = dto.Value26;
            Value27 = dto.Value27;
            Value28 = dto.Value28;
            Value29 = dto.Value29;
            Value30 = dto.Value30;
            Value31 = dto.Value31;
            Value32 = dto.Value32;
            Value33 = dto.Value33;
            Value34 = dto.Value34;
            Value35 = dto.Value35;
            Value36 = dto.Value36;
            Value37 = dto.Value37;
            Value38 = dto.Value38;
            Value39 = dto.Value39;
            Value40 = dto.Value40;
            Value41 = dto.Value41;
            Value42 = dto.Value42;
            Value43 = dto.Value43;
            Value44 = dto.Value44;
            Value45 = dto.Value45;
            Value46 = dto.Value46;
            Value47 = dto.Value47;
            Value48 = dto.Value48;
            Value49 = dto.Value49;
            Value50 = dto.Value50;

            Flag1 = dto.Flag1;
            Flag2 = dto.Flag2;
            Flag3 = dto.Flag3;
            Flag4 = dto.Flag4;
            Flag5 = dto.Flag5;
            Flag6 = dto.Flag6;
            Flag7 = dto.Flag7;
            Flag8 = dto.Flag8;
            Flag9 = dto.Flag9;
            Flag10 = dto.Flag10;

            Date1 = dto.Date1;
            Date2 = dto.Date2;
            Date3 = dto.Date3;
            Date4 = dto.Date4;
            Date5 = dto.Date5;
            Date6 = dto.Date6;
            Date7 = dto.Date7;
            Date8 = dto.Date8;
            Date9 = dto.Date9;
            Date10 = dto.Date10;

            Note1 = dto.Note1;
            Note2 = dto.Note2;
            Note3 = dto.Note3;
            Note4 = dto.Note4;
            Note5 = dto.Note5;

        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<CustomAttributeDataDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<CustomAttributeDataDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<CustomAttributeDataDto>.ParentId
        {
            get
            {
                return new CustomAttributeDataDtoItemParentKey()
                    {
                        ParentId = this.ParentId,
                        ParentType = this.ParentType
                    };
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public CustomAttributeDataDto CopyDto()
        {
            return new CustomAttributeDataDto()
            {
                Id = this.Id,
                ParentId = this.ParentId,
                ParentType = this.ParentType,

                Text1 = this.Text1,
                Text2 = this.Text2,
                Text3 = this.Text3,
                Text4 = this.Text4,
                Text5 = this.Text5,
                Text6 = this.Text6,
                Text7 = this.Text7,
                Text8 = this.Text8,
                Text9 = this.Text9,
                Text10 = this.Text10,
                Text11 = this.Text11,
                Text12 = this.Text12,
                Text13 = this.Text13,
                Text14 = this.Text14,
                Text15 = this.Text15,
                Text16 = this.Text16,
                Text17 = this.Text17,
                Text18 = this.Text18,
                Text19 = this.Text19,
                Text20 = this.Text20,
                Text21 = this.Text21,
                Text22 = this.Text22,
                Text23 = this.Text23,
                Text24 = this.Text24,
                Text25 = this.Text25,
                Text26 = this.Text26,
                Text27 = this.Text27,
                Text28 = this.Text28,
                Text29 = this.Text29,
                Text30 = this.Text30,
                Text31 = this.Text31,
                Text32 = this.Text32,
                Text33 = this.Text33,
                Text34 = this.Text34,
                Text35 = this.Text35,
                Text36 = this.Text36,
                Text37 = this.Text37,
                Text38 = this.Text38,
                Text39 = this.Text39,
                Text40 = this.Text40,
                Text41 = this.Text41,
                Text42 = this.Text42,
                Text43 = this.Text43,
                Text44 = this.Text44,
                Text45 = this.Text45,
                Text46 = this.Text46,
                Text47 = this.Text47,
                Text48 = this.Text48,
                Text49 = this.Text49,
                Text50 = this.Text50,

                Value1 = this.Value1,
                Value2 = this.Value2,
                Value3 = this.Value3,
                Value4 = this.Value4,
                Value5 = this.Value5,
                Value6 = this.Value6,
                Value7 = this.Value7,
                Value8 = this.Value8,
                Value9 = this.Value9,
                Value10 = this.Value10,
                Value11 = this.Value11,
                Value12 = this.Value12,
                Value13 = this.Value13,
                Value14 = this.Value14,
                Value15 = this.Value15,
                Value16 = this.Value16,
                Value17 = this.Value17,
                Value18 = this.Value18,
                Value19 = this.Value19,
                Value20 = this.Value20,
                Value21 = this.Value21,
                Value22 = this.Value22,
                Value23 = this.Value23,
                Value24 = this.Value24,
                Value25 = this.Value25,
                Value26 = this.Value26,
                Value27 = this.Value27,
                Value28 = this.Value28,
                Value29 = this.Value29,
                Value30 = this.Value30,
                Value31 = this.Value31,
                Value32 = this.Value32,
                Value33 = this.Value33,
                Value34 = this.Value34,
                Value35 = this.Value35,
                Value36 = this.Value36,
                Value37 = this.Value37,
                Value38 = this.Value38,
                Value39 = this.Value39,
                Value40 = this.Value40,
                Value41 = this.Value41,
                Value42 = this.Value42,
                Value43 = this.Value43,
                Value44 = this.Value44,
                Value45 = this.Value45,
                Value46 = this.Value46,
                Value47 = this.Value47,
                Value48 = this.Value48,
                Value49 = this.Value49,
                Value50 = this.Value50,

                Flag1 = this.Flag1,
                Flag2 = this.Flag2,
                Flag3 = this.Flag3,
                Flag4 = this.Flag4,
                Flag5 = this.Flag5,
                Flag6 = this.Flag6,
                Flag7 = this.Flag7,
                Flag8 = this.Flag8,
                Flag9 = this.Flag9,
                Flag10 = this.Flag10,

                Date1 = this.Date1,
                Date2 = this.Date2,
                Date3 = this.Date3,
                Date4 = this.Date4,
                Date5 = this.Date5,
                Date6 = this.Date6,
                Date7 = this.Date7,
                Date8 = this.Date8,
                Date9 = this.Date9,
                Date10 = this.Date10,

                Note1 = this.Note1,
                Note2 = this.Note2,
                Note3 = this.Note3,
                Note4 = this.Note4,
                Note5 = this.Note5
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.CustomAttributeDataId: return Id; 
                case FieldNames.CustomAttributeDataParentId: return ParentId;
                case FieldNames.CustomAttributeDataParentType: return ParentType; 

                case FieldNames.CustomAttributeDataText1: return Text1;
                case FieldNames.CustomAttributeDataText2: return Text2;
                case FieldNames.CustomAttributeDataText3: return Text3;
                case FieldNames.CustomAttributeDataText4: return Text4;
                case FieldNames.CustomAttributeDataText5: return Text5;
                case FieldNames.CustomAttributeDataText6: return Text6;
                case FieldNames.CustomAttributeDataText7: return Text7;
                case FieldNames.CustomAttributeDataText8: return Text8;
                case FieldNames.CustomAttributeDataText9: return Text9;
                case FieldNames.CustomAttributeDataText10: return Text10;
                case FieldNames.CustomAttributeDataText11: return Text11;
                case FieldNames.CustomAttributeDataText12: return Text12;
                case FieldNames.CustomAttributeDataText13: return Text13;
                case FieldNames.CustomAttributeDataText14: return Text14;
                case FieldNames.CustomAttributeDataText15: return Text15;
                case FieldNames.CustomAttributeDataText16: return Text16;
                case FieldNames.CustomAttributeDataText17: return Text17;
                case FieldNames.CustomAttributeDataText18: return Text18;
                case FieldNames.CustomAttributeDataText19: return Text19;
                case FieldNames.CustomAttributeDataText20: return Text20;
                case FieldNames.CustomAttributeDataText21: return Text21;
                case FieldNames.CustomAttributeDataText22: return Text22;
                case FieldNames.CustomAttributeDataText23: return Text23;
                case FieldNames.CustomAttributeDataText24: return Text24;
                case FieldNames.CustomAttributeDataText25: return Text25;
                case FieldNames.CustomAttributeDataText26: return Text26;
                case FieldNames.CustomAttributeDataText27: return Text27;
                case FieldNames.CustomAttributeDataText28: return Text28;
                case FieldNames.CustomAttributeDataText29: return Text29;
                case FieldNames.CustomAttributeDataText30: return Text30;
                case FieldNames.CustomAttributeDataText31: return Text31;
                case FieldNames.CustomAttributeDataText32: return Text32;
                case FieldNames.CustomAttributeDataText33: return Text33;
                case FieldNames.CustomAttributeDataText34: return Text34;
                case FieldNames.CustomAttributeDataText35: return Text35;
                case FieldNames.CustomAttributeDataText36: return Text36;
                case FieldNames.CustomAttributeDataText37: return Text37;
                case FieldNames.CustomAttributeDataText38: return Text38;
                case FieldNames.CustomAttributeDataText39: return Text39;
                case FieldNames.CustomAttributeDataText40: return Text40;
                case FieldNames.CustomAttributeDataText41: return Text41;
                case FieldNames.CustomAttributeDataText42: return Text42;
                case FieldNames.CustomAttributeDataText43: return Text43;
                case FieldNames.CustomAttributeDataText44: return Text44;
                case FieldNames.CustomAttributeDataText45: return Text45;
                case FieldNames.CustomAttributeDataText46: return Text46;
                case FieldNames.CustomAttributeDataText47: return Text47;
                case FieldNames.CustomAttributeDataText48: return Text48;
                case FieldNames.CustomAttributeDataText49: return Text49;
                case FieldNames.CustomAttributeDataText50: return Text50;

                case FieldNames.CustomAttributeDataValue1: return Value1;
                case FieldNames.CustomAttributeDataValue2: return Value2;
                case FieldNames.CustomAttributeDataValue3: return Value3;
                case FieldNames.CustomAttributeDataValue4: return Value4;
                case FieldNames.CustomAttributeDataValue5: return Value5;
                case FieldNames.CustomAttributeDataValue6: return Value6;
                case FieldNames.CustomAttributeDataValue7: return Value7;
                case FieldNames.CustomAttributeDataValue8: return Value8;
                case FieldNames.CustomAttributeDataValue9: return Value9;
                case FieldNames.CustomAttributeDataValue10: return Value10;
                case FieldNames.CustomAttributeDataValue11: return Value11;
                case FieldNames.CustomAttributeDataValue12: return Value12;
                case FieldNames.CustomAttributeDataValue13: return Value13;
                case FieldNames.CustomAttributeDataValue14: return Value14;
                case FieldNames.CustomAttributeDataValue15: return Value15;
                case FieldNames.CustomAttributeDataValue16: return Value16;
                case FieldNames.CustomAttributeDataValue17: return Value17;
                case FieldNames.CustomAttributeDataValue18: return Value18;
                case FieldNames.CustomAttributeDataValue19: return Value19;
                case FieldNames.CustomAttributeDataValue20: return Value20;
                case FieldNames.CustomAttributeDataValue21: return Value21;
                case FieldNames.CustomAttributeDataValue22: return Value22;
                case FieldNames.CustomAttributeDataValue23: return Value23;
                case FieldNames.CustomAttributeDataValue24: return Value24;
                case FieldNames.CustomAttributeDataValue25: return Value25;
                case FieldNames.CustomAttributeDataValue26: return Value26;
                case FieldNames.CustomAttributeDataValue27: return Value27;
                case FieldNames.CustomAttributeDataValue28: return Value28;
                case FieldNames.CustomAttributeDataValue29: return Value29;
                case FieldNames.CustomAttributeDataValue30: return Value30;
                case FieldNames.CustomAttributeDataValue31: return Value31;
                case FieldNames.CustomAttributeDataValue32: return Value32;
                case FieldNames.CustomAttributeDataValue33: return Value33;
                case FieldNames.CustomAttributeDataValue34: return Value34;
                case FieldNames.CustomAttributeDataValue35: return Value35;
                case FieldNames.CustomAttributeDataValue36: return Value36;
                case FieldNames.CustomAttributeDataValue37: return Value37;
                case FieldNames.CustomAttributeDataValue38: return Value38;
                case FieldNames.CustomAttributeDataValue39: return Value39;
                case FieldNames.CustomAttributeDataValue40: return Value40;
                case FieldNames.CustomAttributeDataValue41: return Value41;
                case FieldNames.CustomAttributeDataValue42: return Value42;
                case FieldNames.CustomAttributeDataValue43: return Value43;
                case FieldNames.CustomAttributeDataValue44: return Value44;
                case FieldNames.CustomAttributeDataValue45: return Value45;
                case FieldNames.CustomAttributeDataValue46: return Value46;
                case FieldNames.CustomAttributeDataValue47: return Value47;
                case FieldNames.CustomAttributeDataValue48: return Value48;
                case FieldNames.CustomAttributeDataValue49: return Value49;
                case FieldNames.CustomAttributeDataValue50: return Value50;

                case FieldNames.CustomAttributeDataFlag1: return Flag1;
                case FieldNames.CustomAttributeDataFlag2: return Flag2;
                case FieldNames.CustomAttributeDataFlag3: return Flag3;
                case FieldNames.CustomAttributeDataFlag4: return Flag4;
                case FieldNames.CustomAttributeDataFlag5: return Flag5;
                case FieldNames.CustomAttributeDataFlag6: return Flag6;
                case FieldNames.CustomAttributeDataFlag7: return Flag7;
                case FieldNames.CustomAttributeDataFlag8: return Flag8;
                case FieldNames.CustomAttributeDataFlag9: return Flag9;
                case FieldNames.CustomAttributeDataFlag10: return Flag10;

                case FieldNames.CustomAttributeDataDate1: return Date1;
                case FieldNames.CustomAttributeDataDate2: return Date2;
                case FieldNames.CustomAttributeDataDate3: return Date3;
                case FieldNames.CustomAttributeDataDate4: return Date4;
                case FieldNames.CustomAttributeDataDate5: return Date5;
                case FieldNames.CustomAttributeDataDate6: return Date6;
                case FieldNames.CustomAttributeDataDate7: return Date7;
                case FieldNames.CustomAttributeDataDate8: return Date8;
                case FieldNames.CustomAttributeDataDate9: return Date9;
                case FieldNames.CustomAttributeDataDate10: return Date10;

                case FieldNames.CustomAttributeDataNote1: return Note1;
                case FieldNames.CustomAttributeDataNote2: return Note2;
                case FieldNames.CustomAttributeDataNote3: return Note3;
                case FieldNames.CustomAttributeDataNote4: return Note4;
                case FieldNames.CustomAttributeDataNote5: return Note5;

                default:
                    return null;

            }
        }

        #endregion

        #endregion
    }

}
