﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-25881 : A.Probyn
//  Added Meta data properties
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber property.

#endregion

#region Version History: CCM802

// V8-28811 : L.Luong
//  Added MetaPercentageLinearSpaceFilled

#endregion

#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added NotchNumber property
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
#endregion

#region Version History: (CCM 8.3)
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramFixtureComponentDtoItem : PlanogramFixtureComponentDto, IChildDtoItem<PlanogramFixtureComponentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramFixtureComponentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramFixtureComponentDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentId));
            PlanogramFixtureId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentPlanogramFixtureId));
            PlanogramComponentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentPlanogramComponentId));
            ComponentSequenceNumber = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentComponentSequenceNumber));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentRoll));
            MetaTotalMerchandisableLinearSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace));
            MetaTotalMerchandisableAreaSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace));
            MetaTotalMerchandisableVolumetricSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace));
            MetaTotalLinearWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalLinearWhiteSpace));
            MetaTotalAreaWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalAreaWhiteSpace));
            MetaTotalVolumetricWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace));
            MetaProductsPlaced = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaProductsPlaced));
            MetaNewProducts = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaNewProducts));
            MetaChangesFromPreviousCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaChangesFromPreviousCount));
            MetaChangeFromPreviousStarRating = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaChangeFromPreviousStarRating));
            MetaBlocksDropped = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaBlocksDropped));
            MetaNotAchievedInventory = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaNotAchievedInventory));
            MetaTotalFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalFacings));
            MetaAverageFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaAverageFacings));
            MetaTotalUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalUnits));
            MetaAverageUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaAverageUnits));
            MetaMinDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaMinDos));
            MetaMaxDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaMaxDos));
            MetaAverageDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaAverageDos));
            MetaMinCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaMinCases));
            MetaAverageCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaAverageCases));
            MetaMaxCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaMaxCases));
            MetaSpaceToUnitsIndex = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaSpaceToUnitsIndex));
            MetaIsComponentSlopeWithNoRiser = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser));
            MetaIsOverMerchandisedDepth = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedDepth));
            MetaIsOverMerchandisedHeight = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedHeight));
            MetaIsOverMerchandisedWidth = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedWidth));
            MetaTotalComponentCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalComponentCollisions));
            MetaTotalPositionCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalPositionCollisions));
            MetaTotalFrontFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaTotalFrontFacings));
            MetaAverageFrontFacings = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaAverageFrontFacings));
            MetaPercentageLinearSpaceFilled = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaPercentageLinearSpaceFilled));
            NotchNumber = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentNotchNumber));
            MetaWorldX = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaWorldX));
            MetaWorldY = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaWorldY));
            MetaWorldZ = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaWorldZ));
            MetaWorldAngle = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaWorldAngle));
            MetaWorldSlope = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaWorldSlope));
            MetaWorldRoll = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaWorldRoll));
            MetaIsOutsideOfFixtureArea = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramFixtureComponentMetaIsOutsideOfFixtureArea));

        }

        /// <summary>
        /// Creates a new PlanogramFixtureComponentDtoItem and populates its properties with values taken from a 
        /// PlanogramFixtureComponentDto.
        /// </summary>
        public PlanogramFixtureComponentDtoItem(PlanogramFixtureComponentDto dto)
        {
            Id = dto.Id;
            PlanogramFixtureId = dto.PlanogramFixtureId;
            PlanogramComponentId = dto.PlanogramComponentId;
            ComponentSequenceNumber = dto.ComponentSequenceNumber;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
            MetaTotalMerchandisableLinearSpace = dto.MetaTotalMerchandisableLinearSpace;
            MetaTotalMerchandisableAreaSpace = dto.MetaTotalMerchandisableAreaSpace;
            MetaTotalMerchandisableVolumetricSpace = dto.MetaTotalMerchandisableVolumetricSpace;
            MetaTotalLinearWhiteSpace = dto.MetaTotalLinearWhiteSpace;
            MetaTotalAreaWhiteSpace = dto.MetaTotalAreaWhiteSpace;
            MetaTotalVolumetricWhiteSpace = dto.MetaTotalVolumetricWhiteSpace;
            MetaProductsPlaced = dto.MetaProductsPlaced;
            MetaNewProducts = dto.MetaNewProducts;
            MetaChangesFromPreviousCount = dto.MetaChangesFromPreviousCount;
            MetaChangeFromPreviousStarRating = dto.MetaChangeFromPreviousStarRating;
            MetaBlocksDropped = dto.MetaBlocksDropped;
            MetaNotAchievedInventory = dto.MetaNotAchievedInventory;
            MetaTotalFacings = dto.MetaTotalFacings;
            MetaAverageFacings = dto.MetaAverageFacings;
            MetaTotalUnits = dto.MetaTotalUnits;
            MetaAverageUnits = dto.MetaAverageUnits;
            MetaMinDos = dto.MetaMinDos;
            MetaMaxDos = dto.MetaMaxDos;
            MetaAverageDos = dto.MetaAverageDos;
            MetaMinCases = dto.MetaMinCases;
            MetaAverageCases = dto.MetaAverageCases;
            MetaMaxCases = dto.MetaMaxCases;
            MetaSpaceToUnitsIndex = dto.MetaSpaceToUnitsIndex;
            MetaIsComponentSlopeWithNoRiser = dto.MetaIsComponentSlopeWithNoRiser;
            MetaIsOverMerchandisedDepth = dto.MetaIsOverMerchandisedDepth;
            MetaIsOverMerchandisedHeight = dto.MetaIsOverMerchandisedHeight;
            MetaIsOverMerchandisedWidth = dto.MetaIsOverMerchandisedWidth;
            MetaTotalComponentCollisions = dto.MetaTotalComponentCollisions;
            MetaTotalPositionCollisions = dto.MetaTotalPositionCollisions;
            MetaTotalFrontFacings = dto.MetaTotalFrontFacings;
            MetaAverageFrontFacings = dto.MetaAverageFrontFacings;
            MetaPercentageLinearSpaceFilled = dto.MetaPercentageLinearSpaceFilled;
            NotchNumber = dto.NotchNumber;
            MetaWorldX = dto.MetaWorldX;
            MetaWorldY = dto.MetaWorldY;
            MetaWorldZ = dto.MetaWorldZ;
            MetaWorldAngle = dto.MetaWorldAngle;
            MetaWorldSlope = dto.MetaWorldSlope;
            MetaWorldRoll = dto.MetaWorldRoll;
            MetaIsOutsideOfFixtureArea = dto.MetaIsOutsideOfFixtureArea;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramFixtureComponentDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramFixtureComponentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramFixtureComponentDto>.ParentId
        {
            get { return PlanogramFixtureId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramFixtureComponentDto CopyDto()
        {
            return new PlanogramFixtureComponentDto()
            {
                Id = this.Id,
                PlanogramFixtureId = this.PlanogramFixtureId,
                PlanogramComponentId = this.PlanogramComponentId,
                ComponentSequenceNumber = this.ComponentSequenceNumber,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
                MetaTotalMerchandisableLinearSpace = this.MetaTotalMerchandisableLinearSpace,
                MetaTotalMerchandisableAreaSpace = this.MetaTotalMerchandisableAreaSpace,
                MetaTotalMerchandisableVolumetricSpace = this.MetaTotalMerchandisableVolumetricSpace,
                MetaTotalLinearWhiteSpace = this.MetaTotalLinearWhiteSpace,
                MetaTotalAreaWhiteSpace = this.MetaTotalAreaWhiteSpace,
                MetaTotalVolumetricWhiteSpace = this.MetaTotalVolumetricWhiteSpace,
                MetaProductsPlaced = this.MetaProductsPlaced,
                MetaNewProducts = this.MetaNewProducts,
                MetaChangesFromPreviousCount = this.MetaChangesFromPreviousCount,
                MetaChangeFromPreviousStarRating = this.MetaChangeFromPreviousStarRating,
                MetaBlocksDropped = this.MetaBlocksDropped,
                MetaNotAchievedInventory = this.MetaNotAchievedInventory,
                MetaTotalFacings = this.MetaTotalFacings,
                MetaAverageFacings = this.MetaAverageFacings,
                MetaTotalUnits = this.MetaTotalUnits,
                MetaAverageUnits = this.MetaAverageUnits,
                MetaMinDos = this.MetaMinDos,
                MetaMaxDos = this.MetaMaxDos,
                MetaAverageDos = this.MetaAverageDos,
                MetaMinCases = this.MetaMinCases,
                MetaAverageCases = this.MetaAverageCases,
                MetaMaxCases = this.MetaMaxCases,
                MetaSpaceToUnitsIndex = this.MetaSpaceToUnitsIndex,
                MetaIsComponentSlopeWithNoRiser = this.MetaIsComponentSlopeWithNoRiser,
                MetaIsOverMerchandisedDepth = this.MetaIsOverMerchandisedDepth,
                MetaIsOverMerchandisedHeight = this.MetaIsOverMerchandisedHeight,
                MetaIsOverMerchandisedWidth = this.MetaIsOverMerchandisedWidth,
                MetaTotalComponentCollisions = this.MetaTotalComponentCollisions,
                MetaTotalPositionCollisions = this.MetaTotalPositionCollisions,
                MetaTotalFrontFacings = this.MetaTotalFrontFacings,
                MetaAverageFrontFacings = this.MetaAverageFrontFacings,
                MetaPercentageLinearSpaceFilled = this.MetaPercentageLinearSpaceFilled,
                NotchNumber = this.NotchNumber,
                MetaWorldX = this.MetaWorldX,
                MetaWorldY = this.MetaWorldY,
                MetaWorldZ = this.MetaWorldZ,
                MetaWorldAngle = this.MetaWorldAngle,
                MetaWorldSlope = this.MetaWorldSlope,
                MetaWorldRoll = this.MetaWorldRoll,
                MetaIsOutsideOfFixtureArea = this.MetaIsOutsideOfFixtureArea,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramFixtureComponentId:
                    return Id;
                case FieldNames.PlanogramFixtureComponentPlanogramFixtureId:
                    return PlanogramFixtureId;
                case FieldNames.PlanogramFixtureComponentPlanogramComponentId:
                    return PlanogramComponentId;
                case FieldNames.PlanogramFixtureComponentComponentSequenceNumber:
                    return ComponentSequenceNumber;
                case FieldNames.PlanogramFixtureComponentX:
                    return X;
                case FieldNames.PlanogramFixtureComponentY:
                    return Y;
                case FieldNames.PlanogramFixtureComponentZ:
                    return Z;
                case FieldNames.PlanogramFixtureComponentSlope:
                    return Slope;
                case FieldNames.PlanogramFixtureComponentAngle:
                    return Angle;
                case FieldNames.PlanogramFixtureComponentRoll:
                    return Roll;
                case FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace:
                    return MetaTotalMerchandisableLinearSpace;
                case FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace:
                    return MetaTotalMerchandisableAreaSpace;
                case FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace:
                    return MetaTotalMerchandisableVolumetricSpace;
                case FieldNames.PlanogramFixtureComponentMetaTotalLinearWhiteSpace:
                    return MetaTotalLinearWhiteSpace;
                case FieldNames.PlanogramFixtureComponentMetaTotalAreaWhiteSpace:
                    return MetaTotalAreaWhiteSpace;
                case FieldNames.PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace:
                    return MetaTotalVolumetricWhiteSpace;
                case FieldNames.PlanogramFixtureComponentMetaProductsPlaced:
                    return MetaProductsPlaced;
                case FieldNames.PlanogramFixtureComponentMetaNewProducts:
                    return MetaNewProducts;
                case FieldNames.PlanogramFixtureComponentMetaChangesFromPreviousCount:
                    return MetaChangesFromPreviousCount;
                case FieldNames.PlanogramFixtureComponentMetaChangeFromPreviousStarRating:
                    return MetaChangeFromPreviousStarRating;
                case FieldNames.PlanogramFixtureComponentMetaBlocksDropped:
                    return MetaBlocksDropped;
                case FieldNames.PlanogramFixtureComponentMetaNotAchievedInventory:
                    return MetaNotAchievedInventory;
                case FieldNames.PlanogramFixtureComponentMetaTotalFacings:
                    return MetaTotalFacings;
                case FieldNames.PlanogramFixtureComponentMetaAverageFacings:
                    return MetaAverageFacings;
                case FieldNames.PlanogramFixtureComponentMetaTotalUnits:
                    return MetaTotalUnits;
                case FieldNames.PlanogramFixtureComponentMetaAverageUnits:
                    return MetaAverageUnits;
                case FieldNames.PlanogramFixtureComponentMetaMinDos:
                    return MetaMinDos;
                case FieldNames.PlanogramFixtureComponentMetaMaxDos:
                    return MetaMaxDos;
                case FieldNames.PlanogramFixtureComponentMetaAverageDos:
                    return MetaAverageDos;
                case FieldNames.PlanogramFixtureComponentMetaMinCases:
                    return MetaMinCases;
                case FieldNames.PlanogramFixtureComponentMetaAverageCases:
                    return MetaAverageCases;
                case FieldNames.PlanogramFixtureComponentMetaMaxCases:
                    return MetaMaxCases;
                case FieldNames.PlanogramFixtureComponentMetaSpaceToUnitsIndex:
                    return MetaSpaceToUnitsIndex;
                case FieldNames.PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser:
                    return MetaIsComponentSlopeWithNoRiser;
                case FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedDepth:
                    return MetaIsOverMerchandisedDepth;
                case FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedHeight:
                    return MetaIsOverMerchandisedHeight;
                case FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedWidth:
                    return MetaIsOverMerchandisedWidth;
                case FieldNames.PlanogramFixtureComponentMetaTotalComponentCollisions:
                    return MetaTotalComponentCollisions;
                case FieldNames.PlanogramFixtureComponentMetaTotalPositionCollisions:
                    return MetaTotalPositionCollisions;
                case FieldNames.PlanogramFixtureComponentMetaTotalFrontFacings:
                    return MetaTotalFrontFacings;
                case FieldNames.PlanogramFixtureComponentMetaAverageFrontFacings:
                    return MetaAverageFrontFacings;
                case FieldNames.PlanogramFixtureComponentMetaPercentageLinearSpaceFilled:
                    return MetaPercentageLinearSpaceFilled;
                case FieldNames.PlanogramFixtureComponentNotchNumber:
                    return NotchNumber;
                case FieldNames.PlanogramFixtureComponentMetaWorldX:
                    return MetaWorldX;
                case FieldNames.PlanogramFixtureComponentMetaWorldY:
                    return MetaWorldY;
                case FieldNames.PlanogramFixtureComponentMetaWorldZ:
                    return MetaWorldZ;
                case FieldNames.PlanogramFixtureComponentMetaWorldAngle:
                    return MetaWorldAngle;
                case FieldNames.PlanogramFixtureComponentMetaWorldSlope:
                    return MetaWorldSlope;
                case FieldNames.PlanogramFixtureComponentMetaWorldRoll:
                    return MetaWorldRoll;
                case FieldNames.PlanogramFixtureComponentMetaIsOutsideOfFixtureArea:
                    return MetaIsOutsideOfFixtureArea;


                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
