﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26954 : A.Silva ~ Added ResultType property.
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#region Version History: CCM803

// V8-29596 : L.Luong 
//   Added Criteria

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramValidationTemplateGroupMetricDtoItem : PlanogramValidationTemplateGroupMetricDto, IChildDtoItem<PlanogramValidationTemplateGroupMetricDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramValidationTemplateGroupMetricDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramValidationTemplateGroupMetricDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricId));
            PlanogramValidationTemplateGroupId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId));
            Field = (String)item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricField);
            Threshold1 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricThreshold1));
            Threshold2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricThreshold2));
            Score1 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricScore1));
            Score2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricScore2));
            Score3 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricScore3));
            AggregationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricAggregationType));
            ResultType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricResultType));
            ValidationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricValidationType));
            Criteria = (String)item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateGroupMetricCriteria);
        }

        /// <summary>
        /// Creates a new PlanogramValidationTemplateGroupMetricDtoItem and populates its properties with values taken from a 
        /// PlanogramValidationTemplateDto.
        /// </summary>
        public PlanogramValidationTemplateGroupMetricDtoItem(PlanogramValidationTemplateGroupMetricDto dto)
        {
            Id = dto.Id;
            PlanogramValidationTemplateGroupId = dto.PlanogramValidationTemplateGroupId;
            Field = dto.Field;
            Threshold1 = dto.Threshold1;
            Threshold2 = dto.Threshold2;
            Score1 = dto.Score1;
            Score2 = dto.Score2;
            Score3 = dto.Score3;
            AggregationType = dto.AggregationType;
            ResultType = dto.ResultType;
            ValidationType = dto.ValidationType;
            Criteria = dto.Criteria;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramValidationTemplateGroupMetricDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramValidationTemplateGroupMetricDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramValidationTemplateGroupMetricDto>.ParentId
        {
            get { return PlanogramValidationTemplateGroupId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramValidationTemplateGroupMetricDto CopyDto()
        {
            return new PlanogramValidationTemplateGroupMetricDto()
            {
                Id = this.Id,
                PlanogramValidationTemplateGroupId = this.PlanogramValidationTemplateGroupId,
                Field = this.Field,
                Threshold1 = this.Threshold1,
                Threshold2 = this.Threshold2,
                Score1 = this.Score1,
                Score2 = this.Score2,
                Score3 = this.Score3,
                AggregationType = this.AggregationType,
                ResultType =  this.ResultType,
                ValidationType = this.ValidationType,
                Criteria = this.Criteria
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramValidationTemplateGroupMetricId:
                    return Id;
                case FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId:
                    return PlanogramValidationTemplateGroupId;
                case FieldNames.PlanogramValidationTemplateGroupMetricField:
                    return Field;
                case FieldNames.PlanogramValidationTemplateGroupMetricThreshold1:
                    return Threshold1;
                case FieldNames.PlanogramValidationTemplateGroupMetricThreshold2:
                    return Threshold2;
                case FieldNames.PlanogramValidationTemplateGroupMetricScore1:
                    return Score1;
                case FieldNames.PlanogramValidationTemplateGroupMetricScore2:
                    return Score2;
                case FieldNames.PlanogramValidationTemplateGroupMetricScore3:
                    return Score3;
                case FieldNames.PlanogramValidationTemplateGroupMetricAggregationType:
                    return AggregationType;
                case FieldNames.PlanogramValidationTemplateGroupMetricResultType:
                    return ResultType;
                case FieldNames.PlanogramValidationTemplateGroupMetricValidationType:
                    return ValidationType;
                case FieldNames.PlanogramValidationTemplateGroupMetricCriteria:
                    return Criteria;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
