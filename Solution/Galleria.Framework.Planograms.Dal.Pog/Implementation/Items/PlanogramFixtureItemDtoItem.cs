﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-25881 : A.Probyn
//  Added Meta data properties
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27558 : L.Ineson
//  Removed IsBay
#endregion

#region Version History: CCM803
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramFixtureItemDtoItem : PlanogramFixtureItemDto, IChildDtoItem<PlanogramFixtureItemDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramFixtureItemDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramFixtureItemDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemPlanogramId));
            PlanogramFixtureId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemPlanogramFixtureId));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemRoll));
            BaySequenceNumber = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemBaySequenceNumber));
            MetaComponentCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaComponentCount));
            MetaTotalMerchandisableLinearSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalMerchandisableLinearSpace));
            MetaTotalMerchandisableAreaSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalMerchandisableAreaSpace));
            MetaTotalMerchandisableVolumetricSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace));
            MetaTotalLinearWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalLinearWhiteSpace));
            MetaTotalAreaWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalAreaWhiteSpace));
            MetaTotalVolumetricWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalVolumetricWhiteSpace));
            MetaProductsPlaced = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaProductsPlaced));
            MetaNewProducts = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaNewProducts));
            MetaChangesFromPreviousCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaChangesFromPreviousCount));
            MetaChangeFromPreviousStarRating = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaChangeFromPreviousStarRating));
            MetaBlocksDropped = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaBlocksDropped));
            MetaNotAchievedInventory = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaNotAchievedInventory));
            MetaTotalFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalFacings));
            MetaAverageFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaAverageFacings));
            MetaTotalUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalUnits));
            MetaAverageUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaAverageUnits));
            MetaMinDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaMinDos));
            MetaMaxDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaMaxDos));
            MetaAverageDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaAverageDos));
            MetaMinCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaMinCases));
            MetaAverageCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaAverageCases));
            MetaMaxCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaMaxCases));
            MetaSpaceToUnitsIndex = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaSpaceToUnitsIndex));
            MetaTotalComponentCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalComponentCollisions));
            MetaTotalComponentsOverMerchandisedDepth = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth));
            MetaTotalComponentsOverMerchandisedHeight = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight));
            MetaTotalComponentsOverMerchandisedWidth = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth));
            MetaTotalPositionCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalPositionCollisions));
            MetaTotalFrontFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaTotalFrontFacings));
            MetaAverageFrontFacings = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramFixtureItemMetaAverageFrontFacings));

        }

        /// <summary>
        /// Creates a new PlanogramFixtureItemDtoItem and populates its properties with values taken from a 
        /// PlanogramFixtureItemDto.
        /// </summary>
        public PlanogramFixtureItemDtoItem(PlanogramFixtureItemDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            PlanogramFixtureId = dto.PlanogramFixtureId;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
            BaySequenceNumber = dto.BaySequenceNumber;
            MetaComponentCount = dto.MetaComponentCount;
            MetaTotalMerchandisableLinearSpace = dto.MetaTotalMerchandisableLinearSpace;
            MetaTotalMerchandisableAreaSpace = dto.MetaTotalMerchandisableAreaSpace;
            MetaTotalMerchandisableVolumetricSpace = dto.MetaTotalMerchandisableVolumetricSpace;
            MetaTotalLinearWhiteSpace = dto.MetaTotalLinearWhiteSpace;
            MetaTotalAreaWhiteSpace = dto.MetaTotalAreaWhiteSpace;
            MetaTotalVolumetricWhiteSpace = dto.MetaTotalVolumetricWhiteSpace;
            MetaProductsPlaced = dto.MetaProductsPlaced;
            MetaNewProducts = dto.MetaNewProducts;
            MetaChangesFromPreviousCount = dto.MetaChangesFromPreviousCount;
            MetaChangeFromPreviousStarRating = dto.MetaChangeFromPreviousStarRating;
            MetaBlocksDropped = dto.MetaBlocksDropped;
            MetaNotAchievedInventory = dto.MetaNotAchievedInventory;
            MetaTotalFacings = dto.MetaTotalFacings;
            MetaAverageFacings = dto.MetaAverageFacings;
            MetaTotalUnits = dto.MetaTotalUnits;
            MetaAverageUnits = dto.MetaAverageUnits;
            MetaMinDos = dto.MetaMinDos;
            MetaMaxDos = dto.MetaMaxDos;
            MetaAverageDos = dto.MetaAverageDos;
            MetaMinCases = dto.MetaMinCases;
            MetaAverageCases = dto.MetaAverageCases;
            MetaMaxCases = dto.MetaMaxCases;
            MetaSpaceToUnitsIndex = dto.MetaSpaceToUnitsIndex;
            MetaTotalComponentCollisions = dto.MetaTotalComponentCollisions;
            MetaTotalComponentsOverMerchandisedDepth = dto.MetaTotalComponentsOverMerchandisedDepth;
            MetaTotalComponentsOverMerchandisedHeight = dto.MetaTotalComponentsOverMerchandisedHeight;
            MetaTotalComponentsOverMerchandisedWidth = dto.MetaTotalComponentsOverMerchandisedWidth;
            MetaTotalPositionCollisions = dto.MetaTotalPositionCollisions;
            MetaTotalFrontFacings = dto.MetaTotalFrontFacings;
            MetaAverageFrontFacings = dto.MetaAverageFrontFacings;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramFixtureItemDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramFixtureItemDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramFixtureItemDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramFixtureItemDto CopyDto()
        {
            return new PlanogramFixtureItemDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                PlanogramFixtureId = this.PlanogramFixtureId,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
                BaySequenceNumber = this.BaySequenceNumber,
                MetaComponentCount = this.MetaComponentCount,
                MetaTotalMerchandisableLinearSpace = this.MetaTotalMerchandisableLinearSpace,
                MetaTotalMerchandisableAreaSpace = this.MetaTotalMerchandisableAreaSpace,
                MetaTotalMerchandisableVolumetricSpace = this.MetaTotalMerchandisableVolumetricSpace,
                MetaTotalLinearWhiteSpace = this.MetaTotalLinearWhiteSpace,
                MetaTotalAreaWhiteSpace = this.MetaTotalAreaWhiteSpace,
                MetaTotalVolumetricWhiteSpace = this.MetaTotalVolumetricWhiteSpace,
                MetaProductsPlaced = this.MetaProductsPlaced,
                MetaNewProducts = this.MetaNewProducts,
                MetaChangesFromPreviousCount = this.MetaChangesFromPreviousCount,
                MetaChangeFromPreviousStarRating = this.MetaChangeFromPreviousStarRating,
                MetaBlocksDropped = this.MetaBlocksDropped,
                MetaNotAchievedInventory = this.MetaNotAchievedInventory,
                MetaTotalFacings = this.MetaTotalFacings,
                MetaAverageFacings = this.MetaAverageFacings,
                MetaTotalUnits = this.MetaTotalUnits,
                MetaAverageUnits = this.MetaAverageUnits,
                MetaMinDos = this.MetaMinDos,
                MetaMaxDos = this.MetaMaxDos,
                MetaAverageDos = this.MetaAverageDos,
                MetaMinCases = this.MetaMinCases,
                MetaAverageCases = this.MetaAverageCases,
                MetaMaxCases = this.MetaMaxCases,
                MetaSpaceToUnitsIndex = this.MetaSpaceToUnitsIndex,
                MetaTotalComponentCollisions = this.MetaTotalComponentCollisions,
                MetaTotalComponentsOverMerchandisedDepth = this.MetaTotalComponentsOverMerchandisedDepth,
                MetaTotalComponentsOverMerchandisedHeight = this.MetaTotalComponentsOverMerchandisedHeight,
                MetaTotalComponentsOverMerchandisedWidth = this.MetaTotalComponentsOverMerchandisedWidth,
                MetaTotalPositionCollisions = this.MetaTotalPositionCollisions,
                MetaTotalFrontFacings = this.MetaTotalFrontFacings,
                MetaAverageFrontFacings = this.MetaAverageFrontFacings

            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramFixtureItemId:
                    return Id;
                case FieldNames.PlanogramFixtureItemPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramFixtureItemPlanogramFixtureId:
                    return PlanogramFixtureId;
                case FieldNames.PlanogramFixtureItemX:
                    return X;
                case FieldNames.PlanogramFixtureItemY:
                    return Y;
                case FieldNames.PlanogramFixtureItemZ:
                    return Z;
                case FieldNames.PlanogramFixtureItemSlope:
                    return Slope;
                case FieldNames.PlanogramFixtureItemAngle:
                    return Angle;
                case FieldNames.PlanogramFixtureItemRoll:
                    return Roll;
                case FieldNames.PlanogramFixtureItemBaySequenceNumber:
                    return BaySequenceNumber;
                case FieldNames.PlanogramFixtureItemMetaComponentCount:
                    return MetaComponentCount;
                case FieldNames.PlanogramFixtureItemMetaTotalMerchandisableLinearSpace:
                    return MetaTotalMerchandisableLinearSpace;
                case FieldNames.PlanogramFixtureItemMetaTotalMerchandisableAreaSpace:
                    return MetaTotalMerchandisableAreaSpace;
                case FieldNames.PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace:
                    return MetaTotalMerchandisableVolumetricSpace;
                case FieldNames.PlanogramFixtureItemMetaTotalLinearWhiteSpace:
                    return MetaTotalLinearWhiteSpace;
                case FieldNames.PlanogramFixtureItemMetaTotalAreaWhiteSpace:
                    return MetaTotalAreaWhiteSpace;
                case FieldNames.PlanogramFixtureItemMetaTotalVolumetricWhiteSpace:
                    return MetaTotalVolumetricWhiteSpace;
                case FieldNames.PlanogramFixtureItemMetaProductsPlaced:
                    return MetaProductsPlaced;
                case FieldNames.PlanogramFixtureItemMetaNewProducts:
                    return MetaNewProducts;
                case FieldNames.PlanogramFixtureItemMetaChangesFromPreviousCount:
                    return MetaChangesFromPreviousCount;
                case FieldNames.PlanogramFixtureItemMetaChangeFromPreviousStarRating:
                    return MetaChangeFromPreviousStarRating;
                case FieldNames.PlanogramFixtureItemMetaBlocksDropped:
                    return MetaBlocksDropped;
                case FieldNames.PlanogramFixtureItemMetaNotAchievedInventory:
                    return MetaNotAchievedInventory;
                case FieldNames.PlanogramFixtureItemMetaTotalFacings:
                    return MetaTotalFacings;
                case FieldNames.PlanogramFixtureItemMetaAverageFacings:
                    return MetaAverageFacings;
                case FieldNames.PlanogramFixtureItemMetaTotalUnits:
                    return MetaTotalUnits;
                case FieldNames.PlanogramFixtureItemMetaAverageUnits:
                    return MetaAverageUnits;
                case FieldNames.PlanogramFixtureItemMetaMinDos:
                    return MetaMinDos;
                case FieldNames.PlanogramFixtureItemMetaMaxDos:
                    return MetaMaxDos;
                case FieldNames.PlanogramFixtureItemMetaAverageDos:
                    return MetaAverageDos;
                case FieldNames.PlanogramFixtureItemMetaMinCases:
                    return MetaMinCases;
                case FieldNames.PlanogramFixtureItemMetaAverageCases:
                    return MetaAverageCases;
                case FieldNames.PlanogramFixtureItemMetaMaxCases:
                    return MetaMaxCases;
                case FieldNames.PlanogramFixtureItemMetaSpaceToUnitsIndex:
                    return MetaSpaceToUnitsIndex;
                case FieldNames.PlanogramFixtureItemMetaTotalComponentCollisions:
                    return MetaTotalComponentCollisions;
                case FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth:
                    return MetaTotalComponentsOverMerchandisedDepth;
                case FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight:
                    return MetaTotalComponentsOverMerchandisedHeight;
                case FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth:
                    return MetaTotalComponentsOverMerchandisedWidth;
                case FieldNames.PlanogramFixtureItemMetaTotalPositionCollisions:
                    return MetaTotalPositionCollisions;
                case FieldNames.PlanogramFixtureItemMetaTotalFrontFacings:
                    return MetaTotalFrontFacings;
                case FieldNames.PlanogramFixtureItemMetaAverageFrontFacings:
                    return MetaAverageFrontFacings;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
