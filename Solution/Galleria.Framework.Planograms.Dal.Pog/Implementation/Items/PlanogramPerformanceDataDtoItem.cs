﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26269 : A.Kuszyk
//  Created
#endregion
#region Version History: CCM802

// V8-28811 : L.Luong
//  Added Meta Data Properties MetaP1Percentage to MetaP20Percentage

#endregion
#region Version History: CCM 8.2.0
// V8-29998 : I.George
//	Added PlanogramPerformanceData_MetaP1Rank to PlanogramPerformanceData_MetaP20Rank fields
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramPerformanceDataDtoItem :
        PlanogramPerformanceDataDto, 
        IChildDtoItem<PlanogramPerformanceDataDto>, 
        GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramPerformanceDataDtoItem and populates its properties with 
        /// values taken from a GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramPerformanceDataDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataId));
            PlanogramPerformanceId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataPlanogramPerformanceId));
            PlanogramProductId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataPlanogramProductId));
            P1 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP1));
            P2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP2));
            P3 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP3));
            P4 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP4));
            P5 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP5));
            P6 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP6));
            P7 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP7));
            P8 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP8));
            P9 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP9));
            P10 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP10));
            P11 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP11));
            P12 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP12));
            P13 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP13));
            P14 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP14));
            P15 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP15));
            P16 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP16));
            P17 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP17));
            P18 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP18));
            P19 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP19));
            P20 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataP20));
            UnitsSoldPerDay = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataUnitsSoldPerDay));
            AchievedCasePacks = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataAchievedCasePacks));
            AchievedDos = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataAchievedDos));
            AchievedShelfLife = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataAchievedShelfLife));
            AchievedDeliveries = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataAchievedDeliveries));
            MinimumInventoryUnits = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMinimumInventoryUnits));
            MetaP1Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP1Percentage));
            MetaP2Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP2Percentage));
            MetaP3Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP3Percentage));
            MetaP4Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP4Percentage));
            MetaP5Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP5Percentage));
            MetaP6Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP6Percentage));
            MetaP7Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP7Percentage));
            MetaP8Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP8Percentage));
            MetaP9Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP9Percentage));
            MetaP10Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP10Percentage));
            MetaP11Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP11Percentage));
            MetaP12Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP12Percentage));
            MetaP13Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP13Percentage));
            MetaP14Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP14Percentage));
            MetaP15Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP15Percentage));
            MetaP16Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP16Percentage));
            MetaP17Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP17Percentage));
            MetaP18Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP18Percentage));
            MetaP19Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP19Percentage));
            MetaP20Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP20Percentage));
            MetaP1Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP1Rank));
            MetaP2Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP2Rank));
            MetaP3Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP3Rank));
            MetaP4Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP4Rank));
            MetaP5Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP5Rank));
            MetaP6Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP6Rank));
            MetaP7Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP7Rank));
            MetaP8Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP8Rank));
            MetaP9Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP9Rank));
            MetaP10Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP10Rank));
            MetaP11Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP11Rank));
            MetaP12Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP12Rank));
            MetaP13Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP13Rank));
            MetaP14Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP14Rank));
            MetaP15Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP15Rank));
            MetaP16Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP16Rank));
            MetaP17Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP17Rank));
            MetaP18Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP18Rank));
            MetaP19Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP19Rank));
            MetaP20Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaP20Rank));

            CP1 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP1));
            CP2 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP2));
            CP3 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP3));
            CP4 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP4));
            CP5 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP5));
            CP6 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP6));
            CP7 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP7));
            CP8 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP8));
            CP9 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP9));
            CP10 = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataCP10));
            MetaCP1Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP1Percentage));
            MetaCP2Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP2Percentage));
            MetaCP3Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP3Percentage));
            MetaCP4Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP4Percentage));
            MetaCP5Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP5Percentage));
            MetaCP6Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP6Percentage));
            MetaCP7Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP7Percentage));
            MetaCP8Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP8Percentage));
            MetaCP9Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP9Percentage));
            MetaCP10Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP10Percentage));
            MetaCP1Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP1Rank));
            MetaCP2Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP2Rank));
            MetaCP3Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP3Rank));
            MetaCP4Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP4Rank));
            MetaCP5Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP5Rank));
            MetaCP6Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP6Rank));
            MetaCP7Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP7Rank));
            MetaCP8Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP8Rank));
            MetaCP9Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP9Rank));
            MetaCP10Rank = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramPerformanceDataMetaCP10Rank));
        }

        /// <summary>
        /// Creates a new PlanogramPerformanceDataDtoItem and populates its properties with 
        /// values taken from a PlanogramPerformanceDataDto.
        /// </summary>
        public PlanogramPerformanceDataDtoItem(PlanogramPerformanceDataDto dto)
        {
            Id = dto.Id;
            PlanogramPerformanceId = dto.PlanogramPerformanceId;
            PlanogramProductId = dto.PlanogramProductId;
            P1 = dto.P1;
            P2 = dto.P2;
            P3 = dto.P3;
            P4 = dto.P4;
            P5 = dto.P5;
            P6 = dto.P6;
            P7 = dto.P7;
            P8 = dto.P8;
            P9 = dto.P9;
            P10 = dto.P10;
            P11 = dto.P11;
            P12 = dto.P12;
            P13 = dto.P13;
            P14 = dto.P14;
            P15 = dto.P15;
            P16 = dto.P16;
            P17 = dto.P17;
            P18 = dto.P18;
            P19 = dto.P19;
            P20 = dto.P20;
            UnitsSoldPerDay = dto.UnitsSoldPerDay;
            AchievedCasePacks = dto.AchievedCasePacks;
            AchievedDos = dto.AchievedDos;
            AchievedShelfLife = dto.AchievedShelfLife;
            AchievedDeliveries = dto.AchievedDeliveries;
            MinimumInventoryUnits = dto.MinimumInventoryUnits;
            MetaP1Percentage = dto.MetaP1Percentage;
            MetaP2Percentage = dto.MetaP2Percentage;
            MetaP3Percentage = dto.MetaP3Percentage;
            MetaP4Percentage = dto.MetaP4Percentage;
            MetaP5Percentage = dto.MetaP5Percentage;
            MetaP6Percentage = dto.MetaP6Percentage;
            MetaP7Percentage = dto.MetaP7Percentage;
            MetaP8Percentage = dto.MetaP8Percentage;
            MetaP9Percentage = dto.MetaP9Percentage;
            MetaP10Percentage = dto.MetaP10Percentage;
            MetaP11Percentage = dto.MetaP11Percentage;
            MetaP12Percentage = dto.MetaP12Percentage;
            MetaP13Percentage = dto.MetaP13Percentage;
            MetaP14Percentage = dto.MetaP14Percentage;
            MetaP15Percentage = dto.MetaP15Percentage;
            MetaP16Percentage = dto.MetaP16Percentage;
            MetaP17Percentage = dto.MetaP17Percentage;
            MetaP18Percentage = dto.MetaP18Percentage;
            MetaP19Percentage = dto.MetaP19Percentage;
            MetaP20Percentage = dto.MetaP20Percentage;
            MetaP1Rank = dto.MetaP1Rank;
            MetaP2Rank = dto.MetaP2Rank;
            MetaP3Rank = dto.MetaP3Rank;
            MetaP4Rank = dto.MetaP4Rank;
            MetaP5Rank = dto.MetaP5Rank;
            MetaP6Rank = dto.MetaP6Rank;
            MetaP7Rank = dto.MetaP7Rank;
            MetaP8Rank = dto.MetaP8Rank;
            MetaP9Rank = dto.MetaP9Rank;
            MetaP10Rank = dto.MetaP10Rank;
            MetaP11Rank = dto.MetaP11Rank;
            MetaP12Rank = dto.MetaP12Rank;
            MetaP13Rank = dto.MetaP13Rank;
            MetaP14Rank = dto.MetaP14Rank;
            MetaP15Rank = dto.MetaP15Rank;
            MetaP16Rank = dto.MetaP16Rank;
            MetaP17Rank = dto.MetaP17Rank;
            MetaP18Rank = dto.MetaP18Rank;
            MetaP19Rank = dto.MetaP19Rank;
            MetaP20Rank = dto.MetaP20Rank;

            CP1 = dto.CP1;
            CP2 = dto.CP2;
            CP3 = dto.CP3;
            CP4 = dto.CP4;
            CP5 = dto.CP5;
            CP6 = dto.CP6;
            CP7 = dto.CP7;
            CP8 = dto.CP8;
            CP9 = dto.CP9;
            CP10 = dto.CP10;

            MetaCP1Percentage = dto.MetaCP1Percentage;
            MetaCP2Percentage = dto.MetaCP2Percentage;
            MetaCP3Percentage = dto.MetaCP3Percentage;
            MetaCP4Percentage = dto.MetaCP4Percentage;
            MetaCP5Percentage = dto.MetaCP5Percentage;
            MetaCP6Percentage = dto.MetaCP6Percentage;
            MetaCP7Percentage = dto.MetaCP7Percentage;
            MetaCP8Percentage = dto.MetaCP8Percentage;
            MetaCP9Percentage = dto.MetaCP9Percentage;
            MetaCP10Percentage = dto.MetaCP10Percentage;

            MetaCP1Rank = dto.MetaCP1Rank;
            MetaCP2Rank = dto.MetaCP2Rank;
            MetaCP3Rank = dto.MetaCP3Rank;
            MetaCP4Rank = dto.MetaCP4Rank;
            MetaCP5Rank = dto.MetaCP5Rank;
            MetaCP6Rank = dto.MetaCP6Rank;
            MetaCP7Rank = dto.MetaCP7Rank;
            MetaCP8Rank = dto.MetaCP8Rank;
            MetaCP9Rank = dto.MetaCP9Rank;
            MetaCP10Rank = dto.MetaCP10Rank;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramPerformanceDataDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramPerformanceDataDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramPerformanceDataDto>.ParentId
        {
            get { return PlanogramPerformanceId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramPerformanceDataDto CopyDto()
        {
            return new PlanogramPerformanceDataDto()
            {
                Id = this.Id,
                PlanogramPerformanceId = this.PlanogramPerformanceId,
                PlanogramProductId = this.PlanogramProductId,
                P1 = this.P1,
                P2 = this.P2,
                P3 = this.P3,
                P4 = this.P4,
                P5 = this.P5,
                P6 = this.P6,
                P7 = this.P7,
                P8 = this.P8,
                P9 = this.P9,
                P10 = this.P10,
                P11 = this.P11,
                P12 = this.P12,
                P13 = this.P13,
                P14 = this.P14,
                P15 = this.P15,
                P16 = this.P16,
                P17 = this.P17,
                P18 = this.P18,
                P19 = this.P19,
                P20 = this.P20,
                UnitsSoldPerDay = this.UnitsSoldPerDay,
                AchievedCasePacks = this.AchievedCasePacks,
                AchievedDos = this.AchievedDos,
                AchievedShelfLife = this.AchievedShelfLife,
                AchievedDeliveries = this.AchievedDeliveries,
                MinimumInventoryUnits  = this.MinimumInventoryUnits,
                MetaP1Percentage = this.MetaP1Percentage,
                MetaP2Percentage = this.MetaP2Percentage,
                MetaP3Percentage = this.MetaP3Percentage,
                MetaP4Percentage = this.MetaP4Percentage,
                MetaP5Percentage = this.MetaP5Percentage,
                MetaP6Percentage = this.MetaP6Percentage,
                MetaP7Percentage = this.MetaP7Percentage,
                MetaP8Percentage = this.MetaP8Percentage,
                MetaP9Percentage = this.MetaP9Percentage,
                MetaP10Percentage = this.MetaP10Percentage,
                MetaP11Percentage = this.MetaP11Percentage,
                MetaP12Percentage = this.MetaP12Percentage,
                MetaP13Percentage = this.MetaP13Percentage,
                MetaP14Percentage = this.MetaP14Percentage,
                MetaP15Percentage = this.MetaP15Percentage,
                MetaP16Percentage = this.MetaP16Percentage,
                MetaP17Percentage = this.MetaP17Percentage,
                MetaP18Percentage = this.MetaP18Percentage,
                MetaP19Percentage = this.MetaP19Percentage,
                MetaP20Percentage = this.MetaP20Percentage,
                MetaP1Rank = this.MetaP1Rank,
                MetaP2Rank = this.MetaP2Rank,
                MetaP3Rank = this.MetaP3Rank,
                MetaP4Rank = this.MetaP4Rank,
                MetaP5Rank = this.MetaP5Rank,
                MetaP6Rank = this.MetaP6Rank,
                MetaP7Rank = this.MetaP7Rank,
                MetaP8Rank = this.MetaP8Rank,
                MetaP9Rank = this.MetaP9Rank,
                MetaP10Rank = this.MetaP10Rank,
                MetaP11Rank = this.MetaP11Rank,
                MetaP12Rank = this.MetaP12Rank,
                MetaP13Rank = this.MetaP13Rank,
                MetaP14Rank = this.MetaP14Rank,
                MetaP15Rank = this.MetaP15Rank,
                MetaP16Rank = this.MetaP16Rank,
                MetaP17Rank = this.MetaP17Rank,
                MetaP18Rank = this.MetaP18Rank,
                MetaP19Rank = this.MetaP19Rank,
                MetaP20Rank = this.MetaP20Rank,
                CP1 = this.CP1,
                CP2 = this.CP2,
                CP3 = this.CP3,
                CP4 = this.CP4,
                CP5 = this.CP5,
                CP6 = this.CP6,
                CP7 = this.CP7,
                CP8 = this.CP8,
                CP9 = this.CP9,
                CP10 = this.CP10,
                MetaCP1Percentage = this.MetaCP1Percentage,
                MetaCP2Percentage = this.MetaCP2Percentage,
                MetaCP3Percentage = this.MetaCP3Percentage,
                MetaCP4Percentage = this.MetaCP4Percentage,
                MetaCP5Percentage = this.MetaCP5Percentage,
                MetaCP6Percentage = this.MetaCP6Percentage,
                MetaCP7Percentage = this.MetaCP7Percentage,
                MetaCP8Percentage = this.MetaCP8Percentage,
                MetaCP9Percentage = this.MetaCP9Percentage,
                MetaCP10Percentage = this.MetaCP10Percentage,
                MetaCP1Rank = this.MetaCP1Rank,
                MetaCP2Rank = this.MetaCP2Rank,
                MetaCP3Rank = this.MetaCP3Rank,
                MetaCP4Rank = this.MetaCP4Rank,
                MetaCP5Rank = this.MetaCP5Rank,
                MetaCP6Rank = this.MetaCP6Rank,
                MetaCP7Rank = this.MetaCP7Rank,
                MetaCP8Rank = this.MetaCP8Rank,
                MetaCP9Rank = this.MetaCP9Rank,
                MetaCP10Rank = this.MetaCP10Rank
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramPerformanceDataId:
                    return Id;
                case FieldNames.PlanogramPerformanceDataPlanogramPerformanceId:
                    return PlanogramPerformanceId;
                case FieldNames.PlanogramPerformanceDataPlanogramProductId:
                    return PlanogramProductId;
                case FieldNames.PlanogramPerformanceDataP1:
                    return P1;
                case FieldNames.PlanogramPerformanceDataP2:
                    return P2;
                case FieldNames.PlanogramPerformanceDataP3:
                    return P3;
                case FieldNames.PlanogramPerformanceDataP4:
                    return P4;
                case FieldNames.PlanogramPerformanceDataP5:
                    return P5;
                case FieldNames.PlanogramPerformanceDataP6:
                    return P6;
                case FieldNames.PlanogramPerformanceDataP7:
                    return P7;
                case FieldNames.PlanogramPerformanceDataP8:
                    return P8;
                case FieldNames.PlanogramPerformanceDataP9:
                    return P9;
                case FieldNames.PlanogramPerformanceDataP10:
                    return P10;
                case FieldNames.PlanogramPerformanceDataP11:
                    return P11;
                case FieldNames.PlanogramPerformanceDataP12:
                    return P12;
                case FieldNames.PlanogramPerformanceDataP13:
                    return P13;
                case FieldNames.PlanogramPerformanceDataP14:
                    return P14;
                case FieldNames.PlanogramPerformanceDataP15:
                    return P15;
                case FieldNames.PlanogramPerformanceDataP16:
                    return P16;
                case FieldNames.PlanogramPerformanceDataP17:
                    return P17;
                case FieldNames.PlanogramPerformanceDataP18:
                    return P18;
                case FieldNames.PlanogramPerformanceDataP19:
                    return P19;
                case FieldNames.PlanogramPerformanceDataP20:
                    return P20;
                case FieldNames.PlanogramPerformanceDataUnitsSoldPerDay:
                    return UnitsSoldPerDay;
                case FieldNames.PlanogramPerformanceDataAchievedCasePacks:
                    return AchievedCasePacks;
                case FieldNames.PlanogramPerformanceDataAchievedDos:
                    return AchievedDos;
                case FieldNames.PlanogramPerformanceDataAchievedShelfLife:
                    return AchievedShelfLife;
                case FieldNames.PlanogramPerformanceDataAchievedDeliveries:
                    return AchievedDeliveries;
                case FieldNames.PlanogramPerformanceDataMinimumInventoryUnits:
                    return MinimumInventoryUnits;
                case FieldNames.PlanogramPerformanceDataMetaP1Percentage:
                    return MetaP1Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP2Percentage:
                    return MetaP2Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP3Percentage:
                    return MetaP3Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP4Percentage:
                    return MetaP4Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP5Percentage:
                    return MetaP5Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP6Percentage:
                    return MetaP6Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP7Percentage:
                    return MetaP7Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP8Percentage:
                    return MetaP8Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP9Percentage:
                    return MetaP9Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP10Percentage:
                    return MetaP10Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP11Percentage:
                    return MetaP11Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP12Percentage:
                    return MetaP12Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP13Percentage:
                    return MetaP13Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP14Percentage:
                    return MetaP14Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP15Percentage:
                    return MetaP15Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP16Percentage:
                    return MetaP16Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP17Percentage:
                    return MetaP17Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP18Percentage:
                    return MetaP18Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP19Percentage:
                    return MetaP19Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP20Percentage:
                    return MetaP20Percentage;
                case FieldNames.PlanogramPerformanceDataMetaP1Rank:
                    return MetaP1Rank;
                case FieldNames.PlanogramPerformanceDataMetaP2Rank:
                    return MetaP2Rank;
                case FieldNames.PlanogramPerformanceDataMetaP3Rank:
                    return MetaP3Rank;
                case FieldNames.PlanogramPerformanceDataMetaP4Rank:
                    return MetaP4Rank;
                case FieldNames.PlanogramPerformanceDataMetaP5Rank:
                    return MetaP5Rank;
                case FieldNames.PlanogramPerformanceDataMetaP6Rank:
                    return MetaP6Rank;
                case FieldNames.PlanogramPerformanceDataMetaP7Rank:
                    return MetaP7Rank;
                case FieldNames.PlanogramPerformanceDataMetaP8Rank:
                    return MetaP8Rank;
                case FieldNames.PlanogramPerformanceDataMetaP9Rank:
                    return MetaP9Rank;
                case FieldNames.PlanogramPerformanceDataMetaP10Rank:
                    return MetaP10Rank;
                case FieldNames.PlanogramPerformanceDataMetaP11Rank:
                    return MetaP11Rank;
                case FieldNames.PlanogramPerformanceDataMetaP12Rank:
                    return MetaP12Rank;
                case FieldNames.PlanogramPerformanceDataMetaP13Rank:
                    return MetaP13Rank;
                case FieldNames.PlanogramPerformanceDataMetaP14Rank:
                    return MetaP14Rank;
                case FieldNames.PlanogramPerformanceDataMetaP15Rank:
                    return MetaP15Rank;
                case FieldNames.PlanogramPerformanceDataMetaP16Rank:
                    return MetaP16Rank;
                case FieldNames.PlanogramPerformanceDataMetaP17Rank:
                    return MetaP17Rank;
                case FieldNames.PlanogramPerformanceDataMetaP18Rank:
                    return MetaP18Rank;
                case FieldNames.PlanogramPerformanceDataMetaP19Rank:
                    return MetaP19Rank;
                case FieldNames.PlanogramPerformanceDataMetaP20Rank:
                    return MetaP20Rank;
                case FieldNames.PlanogramPerformanceDataCP1:
                    return CP1;
                case FieldNames.PlanogramPerformanceDataCP2:
                    return CP2;
                case FieldNames.PlanogramPerformanceDataCP3:
                    return CP3;
                case FieldNames.PlanogramPerformanceDataCP4:
                    return CP4;
                case FieldNames.PlanogramPerformanceDataCP5:
                    return CP5;
                case FieldNames.PlanogramPerformanceDataCP6:
                    return CP6;
                case FieldNames.PlanogramPerformanceDataCP7:
                    return CP7;
                case FieldNames.PlanogramPerformanceDataCP8:
                    return CP8;
                case FieldNames.PlanogramPerformanceDataCP9:
                    return CP9;
                case FieldNames.PlanogramPerformanceDataCP10:
                    return CP10;
                case FieldNames.PlanogramPerformanceDataMetaCP1Percentage:
                    return MetaCP1Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP2Percentage:
                    return MetaCP2Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP3Percentage:
                    return MetaCP3Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP4Percentage:
                    return MetaCP4Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP5Percentage:
                    return MetaCP5Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP6Percentage:
                    return MetaCP6Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP7Percentage:
                    return MetaCP7Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP8Percentage:
                    return MetaCP8Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP9Percentage:
                    return MetaCP9Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP10Percentage:
                    return MetaCP10Percentage;
                case FieldNames.PlanogramPerformanceDataMetaCP1Rank:
                    return MetaCP1Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP2Rank:
                    return MetaCP2Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP3Rank:
                    return MetaCP3Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP4Rank:
                    return MetaCP4Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP5Rank:
                    return MetaCP5Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP6Rank:
                    return MetaCP6Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP7Rank:
                    return MetaCP7Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP8Rank:
                    return MetaCP8Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP9Rank:
                    return MetaCP9Rank;
                case FieldNames.PlanogramPerformanceDataMetaCP10Rank:
                    return MetaCP10Rank;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
