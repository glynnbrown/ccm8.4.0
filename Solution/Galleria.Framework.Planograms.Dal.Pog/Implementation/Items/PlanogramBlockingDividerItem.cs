﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#region Version History : CCM 830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramBlockingDividerDtoItem : PlanogramBlockingDividerDto, IChildDtoItem<PlanogramBlockingDividerDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramBlockingDividerDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramBlockingDividerDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerId));
            PlanogramBlockingId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerPlanogramBlockingId));
            Type = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerType));
            Level = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerLevel));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerY));
            Length = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerLength));
            IsSnapped = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerIsSnapped));
            IsLimited = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerIsLimited));
            LimitedPercentage = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramBlockingDividerLimitedPercentage));
        }

        /// <summary>
        /// Creates a new PlanogramBlockingDividerDtoItem and populates its properties with values taken from a 
        /// PlanogramBlockingDividerDto.
        /// </summary>
        public PlanogramBlockingDividerDtoItem(PlanogramBlockingDividerDto dto)
        {
            Id = dto.Id;
            PlanogramBlockingId = dto.PlanogramBlockingId;
            Type = dto.Type;
            Level = dto.Level;
            X = dto.X;
            Y = dto.Y;
            Length = dto.Length;
            IsSnapped = dto.IsSnapped;
            IsLimited = dto.IsLimited;
            LimitedPercentage = dto.LimitedPercentage;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramBlockingDividerDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramBlockingDividerDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramBlockingDividerDto>.ParentId
        {
            get { return PlanogramBlockingId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramBlockingDividerDto CopyDto()
        {
            return new PlanogramBlockingDividerDto()
            {
                Id = this.Id,
                PlanogramBlockingId = this.PlanogramBlockingId,
                Type = this.Type,
                Level = this.Level,
                X = this.X,
                Y = this.Y,
                Length = this.Length,
                IsSnapped = this.IsSnapped,
                IsLimited = this.IsLimited,
                LimitedPercentage = this.LimitedPercentage
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramBlockingDividerId:
                    return Id;
                case FieldNames.PlanogramBlockingDividerPlanogramBlockingId:
                    return PlanogramBlockingId;
                case FieldNames.PlanogramBlockingDividerType:
                    return Type;
                case FieldNames.PlanogramBlockingDividerLevel:
                    return Level;
                case FieldNames.PlanogramBlockingDividerX:
                    return X;
                case FieldNames.PlanogramBlockingDividerY:
                    return Y;
                case FieldNames.PlanogramBlockingDividerLength:
                    return Length;
                case FieldNames.PlanogramBlockingDividerIsSnapped:
                    return IsSnapped;
                case FieldNames.PlanogramBlockingDividerIsLimited:
                    return IsLimited;
                case FieldNames.PlanogramBlockingDividerLimitedPercentage:
                    return LimitedPercentage;

                default:
                    return null;
            }
        }

        #endregion

        #endregion

    }
}
