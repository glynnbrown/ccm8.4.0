﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
// V8-25916 : N.Foster
//  Added GetRowVersion, GetDateTimeNullable
// V8-25881 : A.Probyn
//  Added nullable Get helper methods
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    /// <summary>
    /// The FileItemHelper class provides a series of helper methods for processing values returned from POG
    /// files.
    /// </summary>
    public static class FileItemHelper
    {
        /// <summary>
        /// Returns the value passed in, or the default Boolean if value is null.
        /// </summary>
        public static Boolean GetBoolean(Object value)
        {
            Boolean returnValue = new Boolean();
            if (value != null)
            {
                returnValue = (Boolean)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default Boolean if value is null.
        /// </summary>
        public static Byte GetByte(Object value)
        {
            Byte returnValue = new Byte();
            if (value != null)
            {
                returnValue = (Byte)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or null
        /// </summary>
        public static Byte? GetByteNullable(Object value)
        {
            return value as Byte?;
        }

        /// <summary>
        /// Returns the value passed in, or the default Char if value is null.
        /// </summary>
        public static Char GetChar(Object value)
        {
            Char returnValue = new Char();
            if (value != null)
            {
                returnValue = (Char)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default DateTime if value is null.
        /// </summary>
        public static DateTime GetDateTime(Object value)
        {
            DateTime returnValue = new DateTime();
            if (value != null)
            {
                returnValue = (DateTime)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or null
        /// </summary>
        public static DateTime? GetDateTimeNullable(Object value)
        {
            return value as DateTime?;
        }

        /// <summary>
        /// Returns the value passed in, or the default Decimal if value is null.
        /// </summary>
        public static Decimal GetDecimal(Object value)
        {
            Decimal returnValue = new Decimal();
            if (value != null)
            {
                returnValue = (Decimal)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default Double if value is null.
        /// </summary>
        public static Double GetDouble(Object value)
        {
            Double returnValue = new Double();
            if (value != null)
            {
                returnValue = (Double)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default Int32 if value is null.
        /// </summary>
        public static Int32 GetInt32(Object value)
        {
            Int32 returnValue = new Int32();
            if (value != null)
            {
                returnValue = (Int32)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or null
        /// </summary>
        public static Int32? GetInt32Nullable(Object value)
        {
            return value as Int32?;
        }

        /// <summary>
        /// Returns the value passed in, or the default Int64 if value is null.
        /// </summary>
        public static Int64 GetInt64(Object value)
        {
            Int64 returnValue = new Int64();
            if (value != null)
            {
                returnValue = (Int64)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default SByte if value is null.
        /// </summary>
        public static SByte GetSByte(Object value)
        {
            SByte returnValue = new SByte();
            if (value != null)
            {
                returnValue = (SByte)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default Int16 if value is null.
        /// </summary>
        public static Int16 GetInt16(Object value)
        {
            Int16 returnValue = new Int16();
            if (value != null)
            {
                returnValue = (Int16)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or null
        /// </summary>
        public static Int16? GetInt16Nullable(Object value)
        {
            return value as Int16?;
        }

        /// <summary>
        /// Returns the value passed in, or the default Boolean if value is null.
        /// </summary>
        public static Single GetSingle(Object value)
        {
            Single returnValue = new Single();
            if (value != null)
            {
                returnValue = (Single)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or null
        /// </summary>
        public static Single? GetSingleNullable(Object value)
        {
            return value as Single?;
        }

        /// <summary>
        /// Returns the value passed in, or the default UInt32 if value is null.
        /// </summary>
        public static UInt32 GetUInt32(Object value)
        {
            UInt32 returnValue = new UInt32();
            if (value != null)
            {
                returnValue = (UInt32)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default UInt64 if value is null.
        /// </summary>
        public static UInt64 GetUInt64(Object value)
        {
            UInt64 returnValue = new UInt64();
            if (value != null)
            {
                returnValue = (UInt64)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default UInt16 if value is null.
        /// </summary>
        public static UInt16 GetUInt16(Object value)
        {
            UInt16 returnValue = new UInt16();
            if (value != null)
            {
                returnValue = (UInt16)value;
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the value passed in, or the default RowVersion if the value is null
        /// </summary>
        public static Int64 GetRowVersion(Object value)
        {
            RowVersion returnValue = new RowVersion();
            if (value != null)
            {
                returnValue = new RowVersion(value);
            }
            return returnValue;
        }
    }
}
