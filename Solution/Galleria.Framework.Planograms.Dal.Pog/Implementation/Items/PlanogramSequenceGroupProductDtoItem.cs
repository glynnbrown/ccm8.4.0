﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-28996 : A.Silva
//		Created
#endregion

#region Version History : CCM830
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroupId
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramSequenceGroupProductDtoItem : PlanogramSequenceGroupProductDto, IChildDtoItem<PlanogramSequenceGroupProductDto>
    {
        #region Constructors

        /// <summary>
        ///     Creates a new <see cref="PlanogramSequenceGroupProductDtoItem"/> and populates it with values from the <paramref name="item"/>.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem"/> from which to take the values for this instance.</param>
        public PlanogramSequenceGroupProductDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupProductId));
            PlanogramSequenceGroupId =
                GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupId));
            Gtin = (String) item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupProductGtin);
            SequenceNumber =
                GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupProductSequenceNumber));
            PlanogramSequenceGroupSubGroupId = GalleriaBinaryFileItemHelper.GetInt32Nullable(
                item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId));
        }

        /// <summary>
        ///     Creates a new <see cref="PlanogramSequenceGroupProductDtoItem"/> and populates it with values from the <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupProductDto"/> from which to take the values for this instance.</param>
        public PlanogramSequenceGroupProductDtoItem(PlanogramSequenceGroupProductDto dto)
        {
            Id = dto.Id;
            PlanogramSequenceGroupId = dto.PlanogramSequenceGroupId;
            Gtin = dto.Gtin;
            SequenceNumber = dto.SequenceNumber;
            PlanogramSequenceGroupSubGroupId = dto.PlanogramSequenceGroupSubGroupId;
        }

        #endregion

        #region IChildDtoItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramSequenceGroupProductId:
                    return Id;
                case FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupId:
                    return PlanogramSequenceGroupId;
                case FieldNames.PlanogramSequenceGroupProductGtin:
                    return Gtin;
                case FieldNames.PlanogramSequenceGroupProductSequenceNumber:
                    return SequenceNumber;
                case FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId:
                    return PlanogramSequenceGroupSubGroupId;
                default:
                    return null;
            }
        }

        /// <summary>
        ///     Create and return a new instance with identical values to this one.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramSequenceDto"/>.</returns>
        public PlanogramSequenceGroupProductDto CopyDto()
        {
            return new PlanogramSequenceGroupProductDto
            {
                Id = Id,
                PlanogramSequenceGroupId = PlanogramSequenceGroupId,
                Gtin = Gtin,
                SequenceNumber = SequenceNumber,
                PlanogramSequenceGroupSubGroupId = PlanogramSequenceGroupSubGroupId,
            };
        }

        public Boolean AutoIncrementId
        {
            get { return true; }
        }

        public Object ParentId
        {
            get { return PlanogramSequenceGroupId; }
        }

        #endregion
    }
}
