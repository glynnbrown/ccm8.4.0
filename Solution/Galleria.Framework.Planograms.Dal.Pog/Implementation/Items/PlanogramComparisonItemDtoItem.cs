﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramComparisonItemDtoItem : PlanogramComparisonItemDto, IChildDtoItem<PlanogramComparisonItemDto>
    {
        #region Public Properties

        Object IChildDtoItem<PlanogramComparisonItemDto>.Id { get { return Id; } set { Id = value; } }

        Boolean IChildDtoItem<PlanogramComparisonItemDto>.AutoIncrementId { get { return true; } }

        Object IChildDtoItem<PlanogramComparisonItemDto>.ParentId { get { return PlanogramComparisonResultId; } }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonItemDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonItemDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonItemId));
            PlanogramComparisonResultId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonItemPlanogramComparisonResultId));
            ItemId = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonItemItemId);
            ItemType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramComparisonItemItemType));
            Status = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramComparisonItemStatus));
        }

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonItemDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonItemDto" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonItemDtoItem(PlanogramComparisonItemDto dto)
        {
            Id = dto.Id;
            PlanogramComparisonResultId = dto.PlanogramComparisonResultId;
            ItemId = dto.ItemId;
            ItemType = dto.ItemType;
            Status = dto.Status;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramComparisonItemDto CopyDto()
        {
            return new PlanogramComparisonItemDto
                   {
                       Id = Id,
                       PlanogramComparisonResultId = PlanogramComparisonResultId,
                       ItemId = ItemId,
                       ItemType = ItemType,
                       Status = Status
                   };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramComparisonItemId:
                    return Id;
                case FieldNames.PlanogramComparisonItemPlanogramComparisonResultId:
                    return PlanogramComparisonResultId;
                case FieldNames.PlanogramComparisonItemItemId:
                    return ItemId;
                case FieldNames.PlanogramComparisonItemItemType:
                    return ItemType;
                case FieldNames.PlanogramComparisonItemStatus:
                    return Status;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}