﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramConsumerDecisionTreeNodeProductDtoItem : PlanogramConsumerDecisionTreeNodeProductDto, IChildDtoItem<PlanogramConsumerDecisionTreeNodeProductDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeNodeProductDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeProductDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeProductId));
            PlanogramConsumerDecisionTreeNodeId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId));
            PlanogramProductId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramProductId));
        }

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeNodeProductDtoItem and populates its properties with values taken from a 
        /// PlanogramConsumerDecisionTreeNodeProductDto.
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeProductDtoItem(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            Id = dto.Id;
            PlanogramConsumerDecisionTreeNodeId = dto.PlanogramConsumerDecisionTreeNodeId;
            PlanogramProductId = dto.PlanogramProductId;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramConsumerDecisionTreeNodeProductDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramConsumerDecisionTreeNodeProductDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramConsumerDecisionTreeNodeProductDto>.ParentId
        {
            get { return PlanogramConsumerDecisionTreeNodeId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeProductDto CopyDto()
        {
            return new PlanogramConsumerDecisionTreeNodeProductDto()
            {
                Id = this.Id,
                PlanogramConsumerDecisionTreeNodeId = this.PlanogramConsumerDecisionTreeNodeId,
                PlanogramProductId = this.PlanogramProductId
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramConsumerDecisionTreeNodeProductId:
                    return Id;
                case FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId:
                    return PlanogramConsumerDecisionTreeNodeId;
                case FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramProductId:
                    return PlanogramProductId;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
