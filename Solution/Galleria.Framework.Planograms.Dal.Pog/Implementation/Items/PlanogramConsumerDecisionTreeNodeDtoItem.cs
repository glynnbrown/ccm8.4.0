﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)
#endregion

#region Version History: CCM8.2.0
// V8-30763 :I.George
//  Added MetaData Fields
// V8-30737 : M.Shelley
//  Added new metadata fields for selected CDT nodes
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramConsumerDecisionTreeNodeDtoItem : PlanogramConsumerDecisionTreeNodeDto, IChildDtoItem<PlanogramConsumerDecisionTreeNodeDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeNodeDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeId));
            PlanogramConsumerDecisionTreeId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId));
            PlanogramConsumerDecisionTreeLevelId = 
                GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId));
            ParentNodeId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeParentNodeId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeName);
            MetaCountOfProducts =
                GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProducts));
            MetaCountOfProductRecommendedInAssortment = 
                GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment));
            MetaCountOfProductPlacedOnPlanogram = 
                GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram));
            MetaLinearSpaceAllocatedToProductsOnPlanogram = 
                GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram));
            MetaAreaSpaceAllocatedToProductsOnPlanogram = 
                GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram));
            MetaVolumetricSpaceAllocatedToProductsOnPlanogram = 
                GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram));

            P1 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP1));
            P2 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP2));
            P3 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP3));
            P4 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP4));
            P5 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP5));
            P6 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP6));
            P7 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP7));
            P8 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP8));
            P9 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP9));
            P10 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP10));
            P11 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP11));
            P12 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP12));
            P13 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP13));
            P14 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP14));
            P15 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP15));
            P16 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP16));
            P17 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP17));
            P18 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP18));
            P19 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP19));
            P20 = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeP20));

            MetaP1Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Percentage));
            MetaP2Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Percentage));
            MetaP3Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Percentage));
            MetaP4Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Percentage));
            MetaP5Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Percentage));
            MetaP6Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Percentage));
            MetaP7Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Percentage));
            MetaP8Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Percentage));
            MetaP9Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Percentage));
            MetaP10Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Percentage));
            MetaP11Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Percentage));
            MetaP12Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Percentage));
            MetaP13Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Percentage));
            MetaP14Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Percentage));
            MetaP15Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Percentage));
            MetaP16Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Percentage));
            MetaP17Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Percentage));
            MetaP18Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Percentage));
            MetaP19Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage));
            MetaP20Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Percentage));
        }     

        /// <summary>
        /// Creates a new PlanogramConsumerDecisionTreeNodeDtoItem and populates its properties with values taken from a 
        /// PlanogramConsumerDecisionTreeNodeDto.
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeDtoItem(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            Id = dto.Id;
            PlanogramConsumerDecisionTreeId = dto.PlanogramConsumerDecisionTreeId;
            PlanogramConsumerDecisionTreeLevelId = dto.PlanogramConsumerDecisionTreeLevelId;
            ParentNodeId = dto.ParentNodeId;
            Name = dto.Name;
            MetaCountOfProducts = dto.MetaCountOfProducts;
            MetaCountOfProductRecommendedInAssortment = dto.MetaCountOfProductRecommendedInAssortment;
            MetaCountOfProductPlacedOnPlanogram = dto.MetaCountOfProductPlacedOnPlanogram;
            MetaLinearSpaceAllocatedToProductsOnPlanogram = dto.MetaLinearSpaceAllocatedToProductsOnPlanogram;
            MetaAreaSpaceAllocatedToProductsOnPlanogram = dto.MetaAreaSpaceAllocatedToProductsOnPlanogram;
            MetaVolumetricSpaceAllocatedToProductsOnPlanogram = dto.MetaVolumetricSpaceAllocatedToProductsOnPlanogram;

            P1 = dto.P1;
            P2 = dto.P2;
            P3 = dto.P3;
            P4 = dto.P4;
            P5 = dto.P5;
            P6 = dto.P6;
            P7 = dto.P7;
            P8 = dto.P8;
            P9 = dto.P9;
            P10 = dto.P10;
            P11 = dto.P11;
            P12 = dto.P12;
            P13 = dto.P13;
            P14 = dto.P14;
            P15 = dto.P15;
            P16 = dto.P16;
            P17 = dto.P17;
            P18 = dto.P18;
            P19 = dto.P19;
            P20 = dto.P20;

            MetaP1Percentage = dto.MetaP1Percentage;
            MetaP2Percentage = dto.MetaP2Percentage;
            MetaP3Percentage = dto.MetaP3Percentage;
            MetaP4Percentage = dto.MetaP4Percentage;
            MetaP5Percentage = dto.MetaP5Percentage;
            MetaP6Percentage = dto.MetaP6Percentage;
            MetaP7Percentage = dto.MetaP7Percentage;
            MetaP8Percentage = dto.MetaP8Percentage;
            MetaP9Percentage = dto.MetaP9Percentage;
            MetaP10Percentage = dto.MetaP10Percentage;
            MetaP11Percentage = dto.MetaP11Percentage;
            MetaP12Percentage = dto.MetaP12Percentage;
            MetaP13Percentage = dto.MetaP13Percentage;
            MetaP14Percentage = dto.MetaP14Percentage;
            MetaP15Percentage = dto.MetaP15Percentage;
            MetaP16Percentage = dto.MetaP16Percentage;
            MetaP17Percentage = dto.MetaP17Percentage;
            MetaP18Percentage = dto.MetaP18Percentage;
            MetaP19Percentage = dto.MetaP19Percentage;
            MetaP20Percentage = dto.MetaP20Percentage;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramConsumerDecisionTreeNodeDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramConsumerDecisionTreeNodeDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramConsumerDecisionTreeNodeDto>.ParentId
        {
            get { return PlanogramConsumerDecisionTreeId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeDto CopyDto()
        {
            return new PlanogramConsumerDecisionTreeNodeDto()
            {
                Id = this.Id,
                PlanogramConsumerDecisionTreeId = this.PlanogramConsumerDecisionTreeId,
                PlanogramConsumerDecisionTreeLevelId = this.PlanogramConsumerDecisionTreeLevelId,
                ParentNodeId = this.ParentNodeId,
                Name = this.Name,
                MetaCountOfProducts = this.MetaCountOfProducts,
                MetaCountOfProductRecommendedInAssortment = this.MetaCountOfProductRecommendedInAssortment,
                MetaCountOfProductPlacedOnPlanogram = this.MetaCountOfProductPlacedOnPlanogram,
                MetaLinearSpaceAllocatedToProductsOnPlanogram = this.MetaLinearSpaceAllocatedToProductsOnPlanogram,
                MetaAreaSpaceAllocatedToProductsOnPlanogram = this.MetaAreaSpaceAllocatedToProductsOnPlanogram,
                MetaVolumetricSpaceAllocatedToProductsOnPlanogram = this.MetaVolumetricSpaceAllocatedToProductsOnPlanogram,
                P1  = this.P1,
                P2  = this.P2,
                P3  = this.P3,
                P4  = this.P4,
                P5  = this.P5,
                P6  = this.P6,
                P7  = this.P7,
                P8  = this.P8,
                P9  = this.P9,
                P10 = this.P10,
                P11 = this.P11,
                P12 = this.P12,
                P13 = this.P13,
                P14 = this.P14,
                P15 = this.P15,
                P16 = this.P16,
                P17 = this.P17,
                P18 = this.P18,
                P19 = this.P19,
                P20 = this.P20,
                MetaP1Percentage = this.MetaP1Percentage,
                MetaP2Percentage = this.MetaP2Percentage,
                MetaP3Percentage = this.MetaP3Percentage,
                MetaP4Percentage = this.MetaP4Percentage,
                MetaP5Percentage = this.MetaP5Percentage,
                MetaP6Percentage = this.MetaP6Percentage,
                MetaP7Percentage = this.MetaP7Percentage,
                MetaP8Percentage = this.MetaP8Percentage,
                MetaP9Percentage = this.MetaP9Percentage,
                MetaP10Percentage = this.MetaP10Percentage,
                MetaP11Percentage = this.MetaP11Percentage,
                MetaP12Percentage = this.MetaP12Percentage,
                MetaP13Percentage = this.MetaP13Percentage,
                MetaP14Percentage = this.MetaP14Percentage,
                MetaP15Percentage = this.MetaP15Percentage,
                MetaP16Percentage = this.MetaP16Percentage,
                MetaP17Percentage = this.MetaP17Percentage,
                MetaP18Percentage = this.MetaP18Percentage,
                MetaP19Percentage = this.MetaP19Percentage,
                MetaP20Percentage = this.MetaP20Percentage,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramConsumerDecisionTreeNodeId:
                    return Id;
                case FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId:
                    return PlanogramConsumerDecisionTreeId;
                case FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId:
                    return PlanogramConsumerDecisionTreeLevelId;
                case FieldNames.PlanogramConsumerDecisionTreeNodeParentNodeId:
                    return ParentNodeId;
                case FieldNames.PlanogramConsumerDecisionTreeNodeName:
                    return Name;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProducts:
                    return MetaCountOfProducts;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment:
                    return MetaCountOfProductRecommendedInAssortment;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram:
                    return MetaCountOfProductPlacedOnPlanogram;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram:
                    return MetaLinearSpaceAllocatedToProductsOnPlanogram;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram:
                    return MetaAreaSpaceAllocatedToProductsOnPlanogram;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram:
                    return MetaVolumetricSpaceAllocatedToProductsOnPlanogram;

                case FieldNames.PlanogramConsumerDecisionTreeNodeP1: return P1;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP2: return P2;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP3: return P3;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP4: return P4;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP5: return P5;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP6: return P6;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP7: return P7;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP8: return P8;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP9: return P9;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP10: return P10;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP11: return P11;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP12: return P12;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP13: return P13;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP14: return P14;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP15: return P15;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP16: return P16;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP17: return P17;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP18: return P18;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP19: return P19;
                case FieldNames.PlanogramConsumerDecisionTreeNodeP20: return P20;

                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Percentage: return MetaP1Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Percentage: return MetaP2Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Percentage: return MetaP3Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Percentage: return MetaP4Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Percentage: return MetaP5Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Percentage: return MetaP6Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Percentage: return MetaP7Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Percentage: return MetaP8Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Percentage: return MetaP9Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Percentage: return MetaP10Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Percentage: return MetaP11Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Percentage: return MetaP12Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Percentage: return MetaP13Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Percentage: return MetaP14Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Percentage: return MetaP15Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Percentage: return MetaP16Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Percentage: return MetaP17Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Percentage: return MetaP18Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage: return MetaP19Percentage;
                case FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Percentage: return MetaP20Percentage;

                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
