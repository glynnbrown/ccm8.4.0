﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25700 : L.Ineson
//  Null id fields no longer return as 0.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAnnotationDtoItem : PlanogramAnnotationDto, IChildDtoItem<PlanogramAnnotationDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAnnotationDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAnnotationDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationPlanogramId));
            PlanogramFixtureItemId = item.GetItemPropertyValue(FieldNames.PlanogramAnnotationPlanogramFixtureItemId);
            PlanogramFixtureAssemblyId = item.GetItemPropertyValue(FieldNames.PlanogramAnnotationPlanogramFixtureAssemblyId);
            PlanogramFixtureComponentId = item.GetItemPropertyValue(FieldNames.PlanogramAnnotationPlanogramFixtureComponentId);
            PlanogramAssemblyComponentId = item.GetItemPropertyValue(FieldNames.PlanogramAnnotationPlanogramAssemblyComponentId);
            PlanogramSubComponentId = item.GetItemPropertyValue(FieldNames.PlanogramAnnotationPlanogramSubComponentId);
            PlanogramPositionId = item.GetItemPropertyValue(FieldNames.PlanogramAnnotationPlanogramPositionId);
            AnnotationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationAnnotationType));
            Text = (String)item.GetItemPropertyValue(FieldNames.PlanogramAnnotationText);
            X = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramAnnotationX);
            Y = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramAnnotationY);
            Z = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramAnnotationZ);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationDepth));
            BorderColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationBorderColour));
            BorderThickness = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationBorderThickness));
            BackgroundColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationBackgroundColour));
            FontColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationFontColour));
            FontSize = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationFontSize));
            FontName = (String)item.GetItemPropertyValue(FieldNames.PlanogramAnnotationFontName);
            CanReduceFontToFit = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAnnotationCanReduceFontToFit));
        }

        /// <summary>
        /// Creates a new PlanogramAnnotationDtoItem and populates its properties with values taken from a 
        /// PlanogramAnnotationDto.
        public PlanogramAnnotationDtoItem(PlanogramAnnotationDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            PlanogramFixtureItemId = dto.PlanogramFixtureItemId;
            PlanogramFixtureAssemblyId = dto.PlanogramFixtureAssemblyId;
            PlanogramFixtureComponentId = dto.PlanogramFixtureComponentId;
            PlanogramAssemblyComponentId = dto.PlanogramAssemblyComponentId;
            PlanogramSubComponentId = dto.PlanogramSubComponentId;
            PlanogramPositionId = dto.PlanogramPositionId;
            AnnotationType = dto.AnnotationType;
            Text = dto.Text;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            BorderColour = dto.BorderColour;
            BorderThickness = dto.BorderThickness;
            BackgroundColour = dto.BackgroundColour;
            FontColour = dto.FontColour;
            FontSize = dto.FontSize;
            FontName = dto.FontName;
            CanReduceFontToFit = dto.CanReduceFontToFit;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAnnotationDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAnnotationDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAnnotationDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAnnotationDto CopyDto()
        {
            return new PlanogramAnnotationDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                PlanogramFixtureItemId = this.PlanogramFixtureItemId,
                PlanogramFixtureAssemblyId = this.PlanogramFixtureAssemblyId,
                PlanogramFixtureComponentId = this.PlanogramFixtureComponentId,
                PlanogramAssemblyComponentId = this.PlanogramAssemblyComponentId,
                PlanogramSubComponentId = this.PlanogramSubComponentId,
                PlanogramPositionId = this.PlanogramPositionId,
                AnnotationType = this.AnnotationType,
                Text = this.Text,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                BorderColour = this.BorderColour,
                BorderThickness = this.BorderThickness,
                BackgroundColour = this.BackgroundColour,
                FontColour = this.FontColour,
                FontSize = this.FontSize,
                FontName = this.FontName,
                CanReduceFontToFit = this.CanReduceFontToFit,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAnnotationId:
                    return Id;
                case FieldNames.PlanogramAnnotationPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramAnnotationPlanogramFixtureItemId:
                    return PlanogramFixtureItemId;
                case FieldNames.PlanogramAnnotationPlanogramFixtureAssemblyId:
                    return PlanogramFixtureAssemblyId;
                case FieldNames.PlanogramAnnotationPlanogramFixtureComponentId:
                    return PlanogramFixtureComponentId;
                case FieldNames.PlanogramAnnotationPlanogramAssemblyComponentId:
                    return PlanogramAssemblyComponentId;
                case FieldNames.PlanogramAnnotationPlanogramSubComponentId:
                    return PlanogramSubComponentId;
                case FieldNames.PlanogramAnnotationPlanogramPositionId:
                    return PlanogramPositionId;
                case FieldNames.PlanogramAnnotationAnnotationType:
                    return AnnotationType;
                case FieldNames.PlanogramAnnotationText:
                    return Text;
                case FieldNames.PlanogramAnnotationX:
                    return X;
                case FieldNames.PlanogramAnnotationY:
                    return Y;
                case FieldNames.PlanogramAnnotationZ:
                    return Z;
                case FieldNames.PlanogramAnnotationHeight:
                    return Height;
                case FieldNames.PlanogramAnnotationWidth:
                    return Width;
                case FieldNames.PlanogramAnnotationDepth:
                    return Depth;
                case FieldNames.PlanogramAnnotationBorderColour:
                    return BorderColour;
                case FieldNames.PlanogramAnnotationBorderThickness:
                    return BorderThickness;
                case FieldNames.PlanogramAnnotationBackgroundColour:
                    return BackgroundColour;
                case FieldNames.PlanogramAnnotationFontColour:
                    return FontColour;
                case FieldNames.PlanogramAnnotationFontSize:
                    return FontSize;
                case FieldNames.PlanogramAnnotationFontName:
                    return FontName;
                case FieldNames.PlanogramAnnotationCanReduceFontToFit:
                    return CanReduceFontToFit;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
