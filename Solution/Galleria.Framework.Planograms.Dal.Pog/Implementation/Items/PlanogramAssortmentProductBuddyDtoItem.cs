﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentProductBuddyDtoItem : PlanogramAssortmentProductBuddyDto, IChildDtoItem<PlanogramAssortmentProductBuddyDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentProductBuddyDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentProductBuddyDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyId));
            PlanogramAssortmentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyPlanogramAssortmentId));
            ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyProductGtin);
            SourceType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddySourceType));
            TreatmentType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyTreatmentType));
            TreatmentTypePercentage = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyTreatmentTypePercentage));
            ProductAttributeType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyProductAttributeType));
            S1ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS1ProductGtin);
            S1Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS1Percentage));
            S2ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS2ProductGtin);
            S2Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS2Percentage));
            S3ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS3ProductGtin);
            S3Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS3Percentage));
            S4ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS4ProductGtin);
            S4Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS4Percentage));
            S5ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS5ProductGtin);
            S5Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentProductBuddyS5Percentage));
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentProductBuddyDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentProductBuddyDtoItem(PlanogramAssortmentProductBuddyDto dto)
        {
            Id = dto.Id;
            ProductGtin = dto.ProductGtin;
            PlanogramAssortmentId = dto.PlanogramAssortmentId;
            SourceType = dto.SourceType;
            TreatmentType = dto.TreatmentType;
            TreatmentTypePercentage = dto.TreatmentTypePercentage;
            ProductAttributeType = dto.ProductAttributeType;
            S1ProductGtin = dto.S1ProductGtin;
            S1Percentage = dto.S1Percentage;
            S2ProductGtin = dto.S2ProductGtin;
            S2Percentage = dto.S2Percentage;
            S3ProductGtin = dto.S3ProductGtin;
            S3Percentage = dto.S3Percentage;
            S4ProductGtin = dto.S4ProductGtin;
            S4Percentage = dto.S4Percentage;
            S5ProductGtin = dto.S5ProductGtin;
            S5Percentage = dto.S5Percentage;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentProductBuddyDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentProductBuddyDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentProductBuddyDto>.ParentId
        {
            get { return PlanogramAssortmentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentProductBuddyDto CopyDto()
        {
            return new PlanogramAssortmentProductBuddyDto()
            {
                Id = this.Id,
                ProductGtin = this.ProductGtin,
                SourceType = this.SourceType,
                TreatmentType = this.TreatmentType,
                TreatmentTypePercentage = this.TreatmentTypePercentage,
                ProductAttributeType = this.ProductAttributeType,
                S1ProductGtin = this.S1ProductGtin,
                S1Percentage = this.S1Percentage,
                S2ProductGtin = this.S2ProductGtin,
                S2Percentage = this.S2Percentage,
                S3ProductGtin = this.S3ProductGtin,
                S3Percentage = this.S3Percentage,
                S4ProductGtin = this.S4ProductGtin,
                S4Percentage = this.S4Percentage,
                S5ProductGtin = this.S5ProductGtin,
                S5Percentage = this.S5Percentage,
                PlanogramAssortmentId = this.PlanogramAssortmentId

            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentProductBuddyId:
                    return Id;
                case FieldNames.PlanogramAssortmentProductBuddyProductGtin:
                    return ProductGtin;
                case FieldNames.PlanogramAssortmentProductBuddySourceType:
                    return SourceType;
                case FieldNames.PlanogramAssortmentProductBuddyTreatmentType:
                    return TreatmentType;
                case FieldNames.PlanogramAssortmentProductBuddyTreatmentTypePercentage:
                    return TreatmentTypePercentage;
                case FieldNames.PlanogramAssortmentProductBuddyProductAttributeType:
                    return ProductAttributeType;
                case FieldNames.PlanogramAssortmentProductBuddyS1ProductGtin:
                    return S1ProductGtin;
                case FieldNames.PlanogramAssortmentProductBuddyS1Percentage:
                    return S1Percentage;
                case FieldNames.PlanogramAssortmentProductBuddyS2ProductGtin:
                    return S2ProductGtin;
                case FieldNames.PlanogramAssortmentProductBuddyS2Percentage:
                    return S2Percentage;
                case FieldNames.PlanogramAssortmentProductBuddyS3ProductGtin:
                    return S3ProductGtin;
                case FieldNames.PlanogramAssortmentProductBuddyS3Percentage:
                    return S3Percentage;
                case FieldNames.PlanogramAssortmentProductBuddyS4ProductGtin:
                    return S4ProductGtin;
                case FieldNames.PlanogramAssortmentProductBuddyS4Percentage:
                    return S4Percentage;
                case FieldNames.PlanogramAssortmentProductBuddyS5ProductGtin:
                    return S5ProductGtin;
                case FieldNames.PlanogramAssortmentProductBuddyS5Percentage:
                    return S5Percentage;
                case FieldNames.PlanogramAssortmentProductBuddyPlanogramAssortmentId:
                    return PlanogramAssortmentId;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}

