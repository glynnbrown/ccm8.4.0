﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentInventoryRuleDtoItem : PlanogramAssortmentInventoryRuleDto, IChildDtoItem<PlanogramAssortmentInventoryRuleDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentInventoryRuleDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentInventoryRuleDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleId));
            PlanogramAssortmentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRulePlanogramAssortmentId));
            ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleProductGtin);
            CasePack = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleCasePack));
            DaysOfSupply  = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleDaysOfSupply));
            ShelfLife  = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleShelfLife));
            ReplenishmentDays  = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleReplenishmentDays));
            WasteHurdleUnits  = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleUnits));
            WasteHurdleCasePack = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleCasePack));
            MinUnits = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleMinUnits));
            MinFacings = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentInventoryRuleMinFacings));
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentInventoryRuleDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentInventoryRuleDtoItem(PlanogramAssortmentInventoryRuleDto dto)
        {
            Id = dto.Id;
            PlanogramAssortmentId = dto.PlanogramAssortmentId;
            ProductGtin = dto.ProductGtin;
            CasePack = dto.CasePack;
            DaysOfSupply  = dto.DaysOfSupply;
            ShelfLife  = dto.ShelfLife;
            ReplenishmentDays = dto.ReplenishmentDays;
            WasteHurdleUnits = dto.WasteHurdleUnits;
            WasteHurdleCasePack = dto.WasteHurdleCasePack;
            MinUnits = dto.MinUnits;
            MinFacings = dto.MinFacings;

        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentInventoryRuleDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentInventoryRuleDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentInventoryRuleDto>.ParentId
        {
            get { return PlanogramAssortmentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentInventoryRuleDto CopyDto()
        {
            return new PlanogramAssortmentInventoryRuleDto()
            {
                Id = this.Id,
                ProductGtin = this.ProductGtin,
                CasePack = this.CasePack,
                DaysOfSupply = this.DaysOfSupply,
                ShelfLife = this.ShelfLife,
                ReplenishmentDays = this.ReplenishmentDays,
                WasteHurdleUnits = this.WasteHurdleUnits,
                WasteHurdleCasePack = this.WasteHurdleCasePack,
                MinUnits = this.MinUnits,
                MinFacings = this.MinFacings,
                PlanogramAssortmentId = this.PlanogramAssortmentId

            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentInventoryRuleId:
                    return Id;
                case FieldNames.PlanogramAssortmentInventoryRuleProductGtin:
                    return ProductGtin;
                case FieldNames.PlanogramAssortmentInventoryRuleCasePack:
                    return CasePack;
                case FieldNames.PlanogramAssortmentInventoryRuleDaysOfSupply:
                    return DaysOfSupply;
                case FieldNames.PlanogramAssortmentInventoryRuleShelfLife:
                    return ShelfLife;
                case FieldNames.PlanogramAssortmentInventoryRuleReplenishmentDays:
                    return ReplenishmentDays;
                case FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleUnits:
                    return WasteHurdleUnits;
                case FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleCasePack:
                    return WasteHurdleCasePack;
                case FieldNames.PlanogramAssortmentInventoryRuleMinUnits:
                    return MinUnits;
                case FieldNames.PlanogramAssortmentInventoryRuleMinFacings:
                    return MinFacings;
                case FieldNames.PlanogramAssortmentInventoryRulePlanogramAssortmentId:
                    return PlanogramAssortmentId;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}

