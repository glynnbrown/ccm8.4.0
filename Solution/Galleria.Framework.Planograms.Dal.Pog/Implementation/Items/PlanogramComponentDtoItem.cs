﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25700 : L.Ineson
//  Null id fields no longer return as 0.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-25881 : A.Probyn
//  Updated so NumberOfSubComponents, NumberOfMerchandisableSubComponent, NumberOfShelfEdgeLabels are meta data
// V8-27058 : A.Probyn
//  Removed MetaNumberOfShelfEdgeLabels
#endregion
#region Version History: CCM820
// V8-30873 : L.Ineson
//  Removed IsCarPark
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramComponentDtoItem : PlanogramComponentDto, IChildDtoItem<PlanogramComponentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors
        /// <summary>
        /// Creates a new PlanogramComponentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramComponentDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComponentId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComponentPlanogramId));
            Mesh3DId = item.GetItemPropertyValue(FieldNames.PlanogramComponentMesh3DId);
            ImageIdFront = item.GetItemPropertyValue(FieldNames.PlanogramComponentImageIdFront);
            ImageIdBack = item.GetItemPropertyValue(FieldNames.PlanogramComponentImageIdBack);
            ImageIdTop = item.GetItemPropertyValue(FieldNames.PlanogramComponentImageIdTop);
            ImageIdBottom = item.GetItemPropertyValue(FieldNames.PlanogramComponentImageIdBottom);
            ImageIdLeft = item.GetItemPropertyValue(FieldNames.PlanogramComponentImageIdLeft);
            ImageIdRight = item.GetItemPropertyValue(FieldNames.PlanogramComponentImageIdRight);
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramComponentName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramComponentHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramComponentWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramComponentDepth));
            MetaNumberOfSubComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramComponentMetaNumberOfSubComponents));
            MetaNumberOfMerchandisedSubComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramComponentMetaNumberOfMerchandisedSubComponents));
            IsMoveable = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramComponentIsMoveable));
            IsDisplayOnly = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramComponentIsDisplayOnly));
            CanAttachShelfEdgeLabel = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramComponentCanAttachShelfEdgeLabel));
            RetailerReferenceCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramComponentRetailerReferenceCode);
            BarCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramComponentBarCode);
            Manufacturer = (String)item.GetItemPropertyValue(FieldNames.PlanogramComponentManufacturer);
            ManufacturerPartName=(String)item.GetItemPropertyValue(FieldNames.PlanogramComponentManufacturerPartName);
            ManufacturerPartNumber = (String)item.GetItemPropertyValue(FieldNames.PlanogramComponentManufacturerPartNumber);
            SupplierName = (String)item.GetItemPropertyValue(FieldNames.PlanogramComponentSupplierName);
            SupplierPartNumber = (String)item.GetItemPropertyValue(FieldNames.PlanogramComponentSupplierPartNumber);
            SupplierCostPrice = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramComponentSupplierCostPrice);
            SupplierDiscount = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramComponentSupplierDiscount);
            SupplierLeadTime = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramComponentSupplierLeadTime);
            MinPurchaseQty = (Int32?)item.GetItemPropertyValue(FieldNames.PlanogramComponentMinPurchaseQty);
            WeightLimit = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramComponentWeightLimit);
            Weight = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramComponentWeight);
            Volume = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramComponentVolume); ;
            Diameter = (Single?)item.GetItemPropertyValue(FieldNames.PlanogramComponentDiameter);
            Capacity = (Int16?)item.GetItemPropertyValue(FieldNames.PlanogramComponentCapacity);
            ComponentType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramComponentComponentType));
            IsMerchandisedTopDown = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramComponentIsMerchandisedTopDown));
        }

        /// <summary>
        /// Creates a new PlanogramComponentDtoItem and populates its properties with values taken from a 
        /// PlanogramComponentDto.
        /// </summary>
        public PlanogramComponentDtoItem(PlanogramComponentDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Mesh3DId = dto.Mesh3DId;
            ImageIdFront = dto.ImageIdFront;
            ImageIdBack = dto.ImageIdBack;
            ImageIdTop = dto.ImageIdTop;
            ImageIdBottom = dto.ImageIdBottom;
            ImageIdLeft = dto.ImageIdLeft;
            ImageIdRight = dto.ImageIdRight;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            MetaNumberOfSubComponents = dto.MetaNumberOfSubComponents;
            MetaNumberOfMerchandisedSubComponents = dto.MetaNumberOfMerchandisedSubComponents;
            IsMoveable = dto.IsMoveable;
            IsDisplayOnly = dto.IsDisplayOnly;
            CanAttachShelfEdgeLabel = dto.CanAttachShelfEdgeLabel;
            RetailerReferenceCode = dto.RetailerReferenceCode;
            BarCode = dto.BarCode;
            Manufacturer = dto.Manufacturer;
            ManufacturerPartName = dto.ManufacturerPartName;
            ManufacturerPartNumber = dto.ManufacturerPartNumber;
            SupplierName = dto.SupplierName;
            SupplierPartNumber = dto.SupplierPartNumber;
            SupplierCostPrice = dto.SupplierCostPrice;
            SupplierDiscount = dto.SupplierDiscount;
            SupplierLeadTime = dto.SupplierLeadTime;
            MinPurchaseQty = dto.MinPurchaseQty;
            WeightLimit = dto.WeightLimit;
            Weight = dto.Weight;
            Volume = dto.Volume;
            Diameter = dto.Diameter;
            Capacity = dto.Capacity;
            ComponentType = dto.ComponentType;
            IsMerchandisedTopDown = dto.IsMerchandisedTopDown;
        }
        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramComponentDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramComponentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramComponentDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramComponentDto CopyDto()
        {
            return new PlanogramComponentDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Mesh3DId = this.Mesh3DId,
                ImageIdFront = this.ImageIdFront,
                ImageIdBack = this.ImageIdBack,
                ImageIdTop = this.ImageIdTop,
                ImageIdBottom = this.ImageIdBottom,
                ImageIdLeft = this.ImageIdLeft,
                ImageIdRight = this.ImageIdRight,
                Name = this.Name,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                MetaNumberOfSubComponents = this.MetaNumberOfSubComponents,
                MetaNumberOfMerchandisedSubComponents = this.MetaNumberOfMerchandisedSubComponents,
                IsMoveable = this.IsMoveable,
                IsDisplayOnly = this.IsDisplayOnly,
                CanAttachShelfEdgeLabel = this.CanAttachShelfEdgeLabel,
                RetailerReferenceCode = this.RetailerReferenceCode,
                BarCode = this.BarCode,
                Manufacturer = this.Manufacturer,
                ManufacturerPartName = this.ManufacturerPartName,
                ManufacturerPartNumber = this.ManufacturerPartNumber,
                SupplierName = this.SupplierName,
                SupplierPartNumber = this.SupplierPartNumber,
                SupplierCostPrice = this.SupplierCostPrice,
                SupplierDiscount = this.SupplierDiscount,
                SupplierLeadTime = this.SupplierLeadTime,
                MinPurchaseQty = this.MinPurchaseQty,
                WeightLimit = this.WeightLimit,
                Weight = this.Weight,
                Volume = this.Volume,
                Diameter = this.Diameter,
                Capacity = this.Capacity,
                ComponentType = this.ComponentType,
                IsMerchandisedTopDown = this.IsMerchandisedTopDown
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramComponentId:
                    return Id;
                case FieldNames.PlanogramComponentPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramComponentMesh3DId:
                    return Mesh3DId;
                case FieldNames.PlanogramComponentImageIdFront:
                    return ImageIdFront;
                case FieldNames.PlanogramComponentImageIdBack:
                    return ImageIdBack;
                case FieldNames.PlanogramComponentImageIdTop:
                    return ImageIdTop;
                case FieldNames.PlanogramComponentImageIdBottom:
                    return ImageIdBottom;
                case FieldNames.PlanogramComponentImageIdLeft:
                    return ImageIdLeft;
                case FieldNames.PlanogramComponentImageIdRight:
                    return ImageIdRight;
                case FieldNames.PlanogramComponentName:
                    return Name;
                case FieldNames.PlanogramComponentHeight:
                    return Height;
                case FieldNames.PlanogramComponentWidth:
                    return Width;
                case FieldNames.PlanogramComponentDepth:
                    return Depth;
                case FieldNames.PlanogramComponentMetaNumberOfSubComponents:
                    return MetaNumberOfSubComponents;
                case FieldNames.PlanogramComponentMetaNumberOfMerchandisedSubComponents:
                    return MetaNumberOfMerchandisedSubComponents;
                case FieldNames.PlanogramComponentIsMoveable:
                    return IsMoveable;
                case FieldNames.PlanogramComponentIsDisplayOnly:
                    return IsDisplayOnly;
                case FieldNames.PlanogramComponentCanAttachShelfEdgeLabel:
                    return CanAttachShelfEdgeLabel;
                case FieldNames.PlanogramComponentRetailerReferenceCode:
                    return RetailerReferenceCode;
                case FieldNames.PlanogramComponentBarCode:
                    return BarCode;
                case FieldNames.PlanogramComponentManufacturer:
                    return Manufacturer;
                case FieldNames.PlanogramComponentManufacturerPartName:
                    return ManufacturerPartName;
                case FieldNames.PlanogramComponentManufacturerPartNumber:
                    return ManufacturerPartNumber;
                case FieldNames.PlanogramComponentSupplierName:
                    return SupplierName;
                case FieldNames.PlanogramComponentSupplierPartNumber:
                    return SupplierPartNumber;
                case FieldNames.PlanogramComponentSupplierCostPrice:
                    return SupplierCostPrice;
                case FieldNames.PlanogramComponentSupplierDiscount:
                    return SupplierDiscount;
                case FieldNames.PlanogramComponentSupplierLeadTime:
                    return SupplierLeadTime;
                case FieldNames.PlanogramComponentMinPurchaseQty:
                    return MinPurchaseQty;
                case FieldNames.PlanogramComponentWeightLimit:
                    return WeightLimit;
                case FieldNames.PlanogramComponentWeight:
                    return Weight;
                case FieldNames.PlanogramComponentVolume:
                    return Volume;
                case FieldNames.PlanogramComponentDiameter:
                    return Diameter;
                case FieldNames.PlanogramComponentCapacity:
                    return Capacity;
                case FieldNames.PlanogramComponentComponentType:
                    return ComponentType;
                case FieldNames.PlanogramComponentIsMerchandisedTopDown:
                    return IsMerchandisedTopDown;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
