﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-25881 : A.Probyn
//  Added Meta data properties
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber property.
// V8-27605 : A.Silva
//      Added missing metadata properties.

#endregion

#region Version History: CCM802

// V8-28811 : L.Luong
//  Added MetaPercentageLinearSpaceFilled

#endregion

#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added NotchNumber property
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29514 : L.Ineson
//  Removed MetaComponentCount
#endregion

#region Version History: (CCM 8.3)
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssemblyComponentDtoItem : PlanogramAssemblyComponentDto, IChildDtoItem<PlanogramAssemblyComponentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssemblyComponentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssemblyComponentDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentId));
            PlanogramAssemblyId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentPlanogramAssemblyId));
            PlanogramComponentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentPlanogramComponentId));
            ComponentSequenceNumber = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentComponentSequenceNumber));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentRoll));
            MetaTotalMerchandisableLinearSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace));
            MetaTotalMerchandisableAreaSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace));
            MetaTotalMerchandisableVolumetricSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace));
            MetaTotalLinearWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalLinearWhiteSpace));
            MetaTotalAreaWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalAreaWhiteSpace));
            MetaTotalVolumetricWhiteSpace = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace));
            MetaProductsPlaced = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaProductsPlaced));
            MetaNewProducts = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaNewProducts));
            MetaChangesFromPreviousCount = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaChangesFromPreviousCount));
            MetaChangeFromPreviousStarRating = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaChangeFromPreviousStarRating));
            MetaBlocksDropped = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaBlocksDropped));
            MetaNotAchievedInventory = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaNotAchievedInventory));
            MetaTotalFacings = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalFacings));
            MetaAverageFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaAverageFacings));
            MetaTotalUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalUnits));
            MetaAverageUnits = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaAverageUnits));
            MetaMinDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaMinDos));
            MetaMaxDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaMaxDos));
            MetaAverageDos = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaAverageDos));
            MetaMinCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaMinCases));
            MetaAverageCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaAverageCases));
            MetaMaxCases = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaMaxCases));
            MetaSpaceToUnitsIndex = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaSpaceToUnitsIndex));
            MetaTotalFrontFacings = GalleriaBinaryFileItemHelper.GetInt16Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalFrontFacings));
            MetaAverageFrontFacings = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaAverageFrontFacings));
            MetaIsComponentSlopeWithNoRiser = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser));
            MetaIsOverMerchandisedDepth = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedDepth));
            MetaIsOverMerchandisedHeight = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedHeight));
            MetaIsOverMerchandisedWidth = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedWidth));
            MetaTotalPositionCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalPositionCollisions));
            MetaTotalComponentCollisions = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaTotalComponentCollisions));
            MetaPercentageLinearSpaceFilled = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled));
            NotchNumber = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentNotchNumber));
            MetaWorldX = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaWorldX));
            MetaWorldY = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaWorldY));
            MetaWorldZ = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaWorldZ));
            MetaWorldAngle = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaWorldAngle));
            MetaWorldSlope = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaWorldSlope));
            MetaWorldRoll = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaWorldRoll));
            MetaIsOutsideOfFixtureArea = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea));
        }

        /// <summary>
        /// Creates a new PlanogramAssemblyComponentDtoItem and populates its properties with values taken from a 
        /// PlanogramAssemblyComponentDto.
        /// </summary>
        public PlanogramAssemblyComponentDtoItem(PlanogramAssemblyComponentDto dto)
        {
            Id = dto.Id;
            PlanogramAssemblyId = dto.PlanogramAssemblyId;
            PlanogramComponentId = dto.PlanogramComponentId;
            ComponentSequenceNumber = dto.ComponentSequenceNumber;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
            MetaTotalMerchandisableLinearSpace = dto.MetaTotalMerchandisableLinearSpace;
            MetaTotalMerchandisableAreaSpace = dto.MetaTotalMerchandisableAreaSpace;
            MetaTotalMerchandisableVolumetricSpace = dto.MetaTotalMerchandisableVolumetricSpace;
            MetaTotalLinearWhiteSpace = dto.MetaTotalLinearWhiteSpace;
            MetaTotalAreaWhiteSpace = dto.MetaTotalAreaWhiteSpace;
            MetaTotalVolumetricWhiteSpace = dto.MetaTotalVolumetricWhiteSpace;
            MetaProductsPlaced = dto.MetaProductsPlaced;
            MetaNewProducts = dto.MetaNewProducts;
            MetaChangesFromPreviousCount = dto.MetaChangesFromPreviousCount;
            MetaChangeFromPreviousStarRating = dto.MetaChangeFromPreviousStarRating;
            MetaBlocksDropped = dto.MetaBlocksDropped;
            MetaNotAchievedInventory = dto.MetaNotAchievedInventory;
            MetaTotalFacings = dto.MetaTotalFacings;
            MetaAverageFacings = dto.MetaAverageFacings;
            MetaTotalUnits = dto.MetaTotalUnits;
            MetaAverageUnits = dto.MetaAverageUnits;
            MetaMinDos = dto.MetaMinDos;
            MetaMaxDos = dto.MetaMaxDos;
            MetaAverageDos = dto.MetaAverageDos;
            MetaMinCases = dto.MetaMinCases;
            MetaAverageCases = dto.MetaAverageCases;
            MetaMaxCases = dto.MetaMaxCases;
            MetaSpaceToUnitsIndex = dto.MetaSpaceToUnitsIndex;
            MetaIsComponentSlopeWithNoRiser = dto.MetaIsComponentSlopeWithNoRiser;
            MetaIsOverMerchandisedDepth = dto.MetaIsOverMerchandisedDepth;
            MetaIsOverMerchandisedHeight = dto.MetaIsOverMerchandisedHeight;
            MetaIsOverMerchandisedWidth = dto.MetaIsOverMerchandisedWidth;
            MetaTotalPositionCollisions = dto.MetaTotalPositionCollisions;
            MetaTotalComponentCollisions = dto.MetaTotalComponentCollisions;
            MetaTotalFrontFacings = dto.MetaTotalFrontFacings;
            MetaAverageFrontFacings = dto.MetaAverageFrontFacings;
            MetaPercentageLinearSpaceFilled = dto.MetaPercentageLinearSpaceFilled;
            NotchNumber = dto.NotchNumber;
            MetaWorldX = dto.MetaWorldX;
            MetaWorldY = dto.MetaWorldY;
            MetaWorldZ = dto.MetaWorldZ;
            MetaWorldAngle = dto.MetaWorldAngle;
            MetaWorldSlope = dto.MetaWorldSlope;
            MetaWorldRoll = dto.MetaWorldRoll;
            MetaIsOutsideOfFixtureArea = dto.MetaIsOutsideOfFixtureArea;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssemblyComponentDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssemblyComponentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssemblyComponentDto>.ParentId
        {
            get { return PlanogramAssemblyId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssemblyComponentDto CopyDto()
        {
            return new PlanogramAssemblyComponentDto()
            {
                Id = this.Id,
                PlanogramAssemblyId = this.PlanogramAssemblyId,
                PlanogramComponentId = this.PlanogramComponentId,
                ComponentSequenceNumber = this.ComponentSequenceNumber,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
                MetaTotalMerchandisableLinearSpace = this.MetaTotalMerchandisableLinearSpace,
                MetaTotalMerchandisableAreaSpace = this.MetaTotalMerchandisableAreaSpace,
                MetaTotalMerchandisableVolumetricSpace = this.MetaTotalMerchandisableVolumetricSpace,
                MetaTotalLinearWhiteSpace = this.MetaTotalLinearWhiteSpace,
                MetaTotalAreaWhiteSpace = this.MetaTotalAreaWhiteSpace,
                MetaTotalVolumetricWhiteSpace = this.MetaTotalVolumetricWhiteSpace,
                MetaProductsPlaced = this.MetaProductsPlaced,
                MetaNewProducts = this.MetaNewProducts,
                MetaChangesFromPreviousCount = this.MetaChangesFromPreviousCount,
                MetaChangeFromPreviousStarRating = this.MetaChangeFromPreviousStarRating,
                MetaBlocksDropped = this.MetaBlocksDropped,
                MetaNotAchievedInventory = this.MetaNotAchievedInventory,
                MetaTotalFacings = this.MetaTotalFacings,
                MetaAverageFacings = this.MetaAverageFacings,
                MetaTotalUnits = this.MetaTotalUnits,
                MetaAverageUnits = this.MetaAverageUnits,
                MetaMinDos = this.MetaMinDos,
                MetaMaxDos = this.MetaMaxDos,
                MetaAverageDos = this.MetaAverageDos,
                MetaMinCases = this.MetaMinCases,
                MetaAverageCases = this.MetaAverageCases,
                MetaMaxCases = this.MetaMaxCases,
                MetaSpaceToUnitsIndex = this.MetaSpaceToUnitsIndex,
                MetaTotalFrontFacings = this.MetaTotalFrontFacings,
                MetaAverageFrontFacings = this.MetaAverageFrontFacings,
                MetaIsComponentSlopeWithNoRiser =  this.MetaIsComponentSlopeWithNoRiser,
                MetaIsOverMerchandisedDepth = this.MetaIsOverMerchandisedDepth,
                MetaIsOverMerchandisedHeight = this.MetaIsOverMerchandisedHeight,
                MetaIsOverMerchandisedWidth = this.MetaIsOverMerchandisedWidth,
                MetaTotalPositionCollisions = this.MetaTotalPositionCollisions,
                MetaTotalComponentCollisions = this.MetaTotalComponentCollisions,
                MetaPercentageLinearSpaceFilled = this.MetaPercentageLinearSpaceFilled,
                NotchNumber = this.NotchNumber,
                MetaWorldX = this.MetaWorldX,
                MetaWorldY = this.MetaWorldY,
                MetaWorldZ = this.MetaWorldZ,
                MetaWorldAngle = this.MetaWorldAngle,
                MetaWorldSlope = this.MetaWorldSlope,
                MetaWorldRoll = this.MetaWorldRoll,
                MetaIsOutsideOfFixtureArea = this.MetaIsOutsideOfFixtureArea,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssemblyComponentId :
                    return Id;
                case FieldNames.PlanogramAssemblyComponentPlanogramAssemblyId :
                    return PlanogramAssemblyId;
                case FieldNames.PlanogramAssemblyComponentPlanogramComponentId :
                    return PlanogramComponentId;
                case FieldNames.PlanogramAssemblyComponentComponentSequenceNumber:
                    return ComponentSequenceNumber;
                case FieldNames.PlanogramAssemblyComponentX:
                    return X;
                case FieldNames.PlanogramAssemblyComponentY :
                    return Y;
                case FieldNames.PlanogramAssemblyComponentZ :
                    return Z;
                case FieldNames.PlanogramAssemblyComponentSlope :
                    return Slope;
                case FieldNames.PlanogramAssemblyComponentAngle :
                    return Angle;
                case FieldNames.PlanogramAssemblyComponentRoll:
                    return Roll;
                case FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace:
                    return MetaTotalMerchandisableLinearSpace;
                case FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace:
                    return MetaTotalMerchandisableAreaSpace;
                case FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace:
                    return MetaTotalMerchandisableVolumetricSpace;
                case FieldNames.PlanogramAssemblyComponentMetaTotalLinearWhiteSpace:
                    return MetaTotalLinearWhiteSpace;
                case FieldNames.PlanogramAssemblyComponentMetaTotalAreaWhiteSpace:
                    return MetaTotalAreaWhiteSpace;
                case FieldNames.PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace:
                    return MetaTotalVolumetricWhiteSpace;
                case FieldNames.PlanogramAssemblyComponentMetaProductsPlaced:
                    return MetaProductsPlaced;
                case FieldNames.PlanogramAssemblyComponentMetaNewProducts:
                    return MetaNewProducts;
                case FieldNames.PlanogramAssemblyComponentMetaChangesFromPreviousCount:
                    return MetaChangesFromPreviousCount;
                case FieldNames.PlanogramAssemblyComponentMetaChangeFromPreviousStarRating:
                    return MetaChangeFromPreviousStarRating;
                case FieldNames.PlanogramAssemblyComponentMetaBlocksDropped:
                    return MetaBlocksDropped;
                case FieldNames.PlanogramAssemblyComponentMetaNotAchievedInventory:
                    return MetaNotAchievedInventory;
                case FieldNames.PlanogramAssemblyComponentMetaTotalFacings:
                    return MetaTotalFacings;
                case FieldNames.PlanogramAssemblyComponentMetaAverageFacings:
                    return MetaAverageFacings;
                case FieldNames.PlanogramAssemblyComponentMetaTotalUnits:
                    return MetaTotalUnits;
                case FieldNames.PlanogramAssemblyComponentMetaAverageUnits:
                    return MetaAverageUnits;
                case FieldNames.PlanogramAssemblyComponentMetaMinDos:
                    return MetaMinDos;
                case FieldNames.PlanogramAssemblyComponentMetaMaxDos:
                    return MetaMaxDos;
                case FieldNames.PlanogramAssemblyComponentMetaAverageDos:
                    return MetaAverageDos;
                case FieldNames.PlanogramAssemblyComponentMetaMinCases:
                    return MetaMinCases;
                case FieldNames.PlanogramAssemblyComponentMetaAverageCases:
                    return MetaAverageCases;
                case FieldNames.PlanogramAssemblyComponentMetaMaxCases:
                    return MetaMaxCases;
                case FieldNames.PlanogramAssemblyComponentMetaSpaceToUnitsIndex:
                    return MetaSpaceToUnitsIndex;
                case FieldNames.PlanogramAssemblyComponentMetaTotalFrontFacings:
                    return MetaTotalFrontFacings;
                case FieldNames.PlanogramAssemblyComponentMetaAverageFrontFacings:
                    return MetaAverageFrontFacings;
                case FieldNames.PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser:
                    return MetaIsComponentSlopeWithNoRiser;
                case FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedDepth:
                    return MetaIsOverMerchandisedDepth;
                case FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedHeight:
                    return MetaIsOverMerchandisedHeight;
                case FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedWidth:
                    return MetaIsOverMerchandisedWidth;
                case FieldNames.PlanogramAssemblyComponentMetaTotalPositionCollisions:
                    return MetaTotalPositionCollisions;
                case FieldNames.PlanogramAssemblyComponentMetaTotalComponentCollisions:
                    return MetaTotalComponentCollisions;
                case FieldNames.PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled:
                    return MetaPercentageLinearSpaceFilled;
                case FieldNames.PlanogramAssemblyComponentNotchNumber:
                    return NotchNumber;
                case FieldNames.PlanogramAssemblyComponentMetaWorldX:
                    return MetaWorldX;
                case FieldNames.PlanogramAssemblyComponentMetaWorldY:
                    return MetaWorldY;
                case FieldNames.PlanogramAssemblyComponentMetaWorldZ:
                    return MetaWorldZ;
                case FieldNames.PlanogramAssemblyComponentMetaWorldAngle:
                    return MetaWorldAngle;
                case FieldNames.PlanogramAssemblyComponentMetaWorldSlope:
                    return MetaWorldSlope;
                case FieldNames.PlanogramAssemblyComponentMetaWorldRoll:
                    return MetaWorldRoll;
                case FieldNames.PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea:
                    return MetaIsOutsideOfFixtureArea;

                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
