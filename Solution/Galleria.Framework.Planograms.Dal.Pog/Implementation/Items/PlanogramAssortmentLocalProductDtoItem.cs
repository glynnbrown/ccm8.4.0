﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentLocalProductDtoItem : PlanogramAssortmentLocalProductDto, IChildDtoItem<PlanogramAssortmentLocalProductDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentLocalProductDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentLocalProductDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocalProductId));
            PlanogramAssortmentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId));
            ProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocalProductProductGtin);
            LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocalProductLocationCode);
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentLocalProductDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentLocalProductDtoItem(PlanogramAssortmentLocalProductDto dto)
        {
            Id = dto.Id;
            PlanogramAssortmentId = dto.PlanogramAssortmentId;
            ProductGtin = dto.ProductGtin;
            LocationCode = dto.LocationCode;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentLocalProductDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentLocalProductDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentLocalProductDto>.ParentId
        {
            get { return PlanogramAssortmentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentLocalProductDto CopyDto()
        {
            return new PlanogramAssortmentLocalProductDto()
            {
                Id = this.Id,
                PlanogramAssortmentId = this.PlanogramAssortmentId,
                ProductGtin = this.ProductGtin,
                LocationCode = this.LocationCode
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentLocalProductId:
                    return Id;
                case FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId:
                    return PlanogramAssortmentId;
                case FieldNames.PlanogramAssortmentLocalProductProductGtin:
                    return ProductGtin;
                case FieldNames.PlanogramAssortmentLocalProductLocationCode:
                    return LocationCode;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
