﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
// V8-27058 : A.Probyn
//  Added new meta data properties

#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramFixtureDtoItem : PlanogramFixtureDto, IChildDtoItem<PlanogramFixtureDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramFixtureDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramFixtureDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixtureId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramFixturePlanogramId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramFixtureName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureDepth));
            MetaNumberOfMerchandisedSubComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramFixtureMetaNumberOfMerchandisedSubComponents));
            MetaTotalFixtureCost = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramFixtureMetaTotalFixtureCost));
            
        }

        /// <summary>
        /// Creates a new PlanogramFixtureDtoItem and populates its properties with values taken from a 
        /// PlanogramFixtureDto.
        /// </summary>
        public PlanogramFixtureDtoItem(PlanogramFixtureDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            MetaNumberOfMerchandisedSubComponents = dto.MetaNumberOfMerchandisedSubComponents;
            MetaTotalFixtureCost = dto.MetaTotalFixtureCost;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramFixtureDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramFixtureDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramFixtureDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramFixtureDto CopyDto()
        {
            return new PlanogramFixtureDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Name = this.Name,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                MetaNumberOfMerchandisedSubComponents = this.MetaNumberOfMerchandisedSubComponents,
                MetaTotalFixtureCost = this.MetaTotalFixtureCost
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramFixtureId:
                    return Id;
                case FieldNames.PlanogramFixturePlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramFixtureName:
                    return Name;
                case FieldNames.PlanogramFixtureHeight:
                    return Height;
                case FieldNames.PlanogramFixtureWidth:
                    return Width;
                case FieldNames.PlanogramFixtureDepth:
                    return Depth;
                case FieldNames.PlanogramFixtureMetaNumberOfMerchandisedSubComponents:
                    return MetaNumberOfMerchandisedSubComponents;
                case FieldNames.PlanogramFixtureMetaTotalFixtureCost:
                    return MetaTotalFixtureCost;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
