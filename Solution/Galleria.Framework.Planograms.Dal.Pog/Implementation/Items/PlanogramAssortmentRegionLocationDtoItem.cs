﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentRegionLocationDtoItem : PlanogramAssortmentRegionLocationDto, IChildDtoItem<PlanogramAssortmentRegionLocationDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentRegionLocationDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentRegionLocationDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionLocationId));
            PlanogramAssortmentRegionId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionLocationPlanogramAssortmentRegionId));
            LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionLocationLocationCode);
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentRegionLocationDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentRegionLocationDtoItem(PlanogramAssortmentRegionLocationDto dto)
        {
            Id = dto.Id;
            LocationCode = dto.LocationCode;
            PlanogramAssortmentRegionId = dto.PlanogramAssortmentRegionId;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentRegionLocationDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentRegionLocationDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentRegionLocationDto>.ParentId
        {
            get { return PlanogramAssortmentRegionId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentRegionLocationDto CopyDto()
        {
            return new PlanogramAssortmentRegionLocationDto()
            {
                Id = this.Id,
                PlanogramAssortmentRegionId = this.PlanogramAssortmentRegionId,
                LocationCode = this.LocationCode
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentRegionLocationId:
                    return Id;
                case FieldNames.PlanogramAssortmentRegionLocationPlanogramAssortmentRegionId:
                    return PlanogramAssortmentRegionId;
                case FieldNames.PlanogramAssortmentRegionLocationLocationCode:
                    return LocationCode;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
