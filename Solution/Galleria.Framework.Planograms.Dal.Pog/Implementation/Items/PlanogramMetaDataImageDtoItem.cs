﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27058 : A.Probyn
//  Initial version.
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramMetadataImageDtoItem : PlanogramMetadataImageDto, IChildDtoItem<PlanogramMetadataImageDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramMetadataImageDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramMetadataImageDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramMetadataImageId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramMetadataImagePlanogramId));
            FileName = (String)item.GetItemPropertyValue(FieldNames.PlanogramMetadataImageFileName);
            Description = (String)item.GetItemPropertyValue(FieldNames.PlanogramMetadataImageDescription);
            ImageData = (Byte[])item.GetItemPropertyValue(FieldNames.PlanogramMetadataImageImageData);
        }

        /// <summary>
        /// Creates a new PlanogramMetadataImageDtoItem and populates its properties with values taken from a 
        /// PlanogramMetaDataImageDto.
        /// </summary>
        public PlanogramMetadataImageDtoItem(PlanogramMetadataImageDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            FileName = dto.FileName;
            Description = dto.Description;
            ImageData = dto.ImageData;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramMetadataImageDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramMetadataImageDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramMetadataImageDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramMetadataImageDto CopyDto()
        {
            return new PlanogramMetadataImageDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                FileName = this.FileName,
                Description = this.Description,
                ImageData = this.ImageData
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramMetadataImageId:
                    return Id;
                case FieldNames.PlanogramMetadataImagePlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramMetadataImageFileName:
                    return FileName;
                case FieldNames.PlanogramMetadataImageDescription:
                    return Description;
                case FieldNames.PlanogramMetadataImageImageData:
                    return ImageData;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
