﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramValidationTemplateDtoItem : PlanogramValidationTemplateDto, IChildDtoItem<PlanogramValidationTemplateDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramValidationTemplateDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramValidationTemplateDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplatePlanogramId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramValidationTemplateName);
        }

        /// <summary>
        /// Creates a new PlanogramValidationTemplateDtoItem and populates its properties with values taken from a 
        /// PlanogramValidationTemplateDto.
        /// </summary>
        public PlanogramValidationTemplateDtoItem(PlanogramValidationTemplateDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Name = dto.Name;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramValidationTemplateDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramValidationTemplateDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramValidationTemplateDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramValidationTemplateDto CopyDto()
        {
            return new PlanogramValidationTemplateDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Name = this.Name
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramValidationTemplateId:
                    return Id;
                case FieldNames.PlanogramValidationTemplatePlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramValidationTemplateName:
                    return Name;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
