﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentLocationBuddyDtoItem : PlanogramAssortmentLocationBuddyDto, IChildDtoItem<PlanogramAssortmentLocationBuddyDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentLocationBuddyDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentLocationBuddyDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyId));
            PlanogramAssortmentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyPlanogramAssortmentId));
            LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyLocationCode);
            TreatmentType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyTreatmentType));
            S1LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS1LocationCode);
            S1Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS1Percentage));
            S2LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS2LocationCode);
            S2Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS2Percentage));
            S3LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS3LocationCode);
            S3Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS3Percentage));
            S4LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS4LocationCode);
            S4Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS4Percentage));
            S5LocationCode = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS5LocationCode);
            S5Percentage = GalleriaBinaryFileItemHelper.GetSingleNullable(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentLocationBuddyS5Percentage));
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentLocationBuddyDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentLocationBuddyDtoItem(PlanogramAssortmentLocationBuddyDto dto)
        {
            Id = dto.Id;
            LocationCode = dto.LocationCode;
            PlanogramAssortmentId = dto.PlanogramAssortmentId;
            TreatmentType = dto.TreatmentType;
            S1LocationCode = dto.S1LocationCode;
            S1Percentage = dto.S1Percentage;
            S2LocationCode = dto.S2LocationCode;
            S2Percentage = dto.S2Percentage;
            S3LocationCode = dto.S3LocationCode;
            S3Percentage = dto.S3Percentage;
            S4LocationCode = dto.S4LocationCode;
            S4Percentage = dto.S4Percentage;
            S5LocationCode = dto.S5LocationCode;
            S5Percentage = dto.S5Percentage;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentLocationBuddyDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentLocationBuddyDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentLocationBuddyDto>.ParentId
        {
            get { return PlanogramAssortmentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentLocationBuddyDto CopyDto()
        {
            return new PlanogramAssortmentLocationBuddyDto()
            {
                Id = this.Id,
                LocationCode = this.LocationCode,
                TreatmentType = this.TreatmentType,
                S1LocationCode = this.S1LocationCode,
                S1Percentage = this.S1Percentage,
                S2LocationCode = this.S2LocationCode,
                S2Percentage = this.S2Percentage,
                S3LocationCode = this.S3LocationCode,
                S3Percentage = this.S3Percentage,
                S4LocationCode = this.S4LocationCode,
                S4Percentage = this.S4Percentage,
                S5LocationCode = this.S5LocationCode,
                S5Percentage = this.S5Percentage,
                PlanogramAssortmentId = this.PlanogramAssortmentId

            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentLocationBuddyId:
                    return Id;
                case FieldNames.PlanogramAssortmentLocationBuddyLocationCode:
                    return LocationCode;
                case FieldNames.PlanogramAssortmentLocationBuddyTreatmentType:
                    return TreatmentType;
                case FieldNames.PlanogramAssortmentLocationBuddyS1LocationCode:
                    return S1LocationCode;
                case FieldNames.PlanogramAssortmentLocationBuddyS1Percentage:
                    return S1Percentage;
                case FieldNames.PlanogramAssortmentLocationBuddyS2LocationCode:
                    return S2LocationCode;
                case FieldNames.PlanogramAssortmentLocationBuddyS2Percentage:
                    return S2Percentage;
                case FieldNames.PlanogramAssortmentLocationBuddyS3LocationCode:
                    return S3LocationCode;
                case FieldNames.PlanogramAssortmentLocationBuddyS3Percentage:
                    return S3Percentage;
                case FieldNames.PlanogramAssortmentLocationBuddyS4LocationCode:
                    return S4LocationCode;
                case FieldNames.PlanogramAssortmentLocationBuddyS4Percentage:
                    return S4Percentage;
                case FieldNames.PlanogramAssortmentLocationBuddyS5LocationCode:
                    return S5LocationCode;
                case FieldNames.PlanogramAssortmentLocationBuddyS5Percentage:
                    return S5Percentage;
                case FieldNames.PlanogramAssortmentLocationBuddyPlanogramAssortmentId:
                    return PlanogramAssortmentId;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}

