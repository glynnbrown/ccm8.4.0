﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentDtoItem : PlanogramAssortmentDto, IChildDtoItem<PlanogramAssortmentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentPlanogramId));
            Name = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentName);
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentDtoItem(PlanogramAssortmentDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Name = dto.Name;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentDto CopyDto()
        {
            return new PlanogramAssortmentDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Name = this.Name
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentId:
                    return Id;
                case FieldNames.PlanogramAssortmentPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramAssortmentName:
                    return Name;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
