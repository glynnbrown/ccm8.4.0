﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)
// V8-27404 : L.Luong
//   Changed LevelType to EntryType and added Content
// V8-27759 : A.Kuszyk
//  Added AffectedId and AffectedType fields.
// V8-28059 : A.Kuszyk
//  Added WorkpackageSource, TaskSource and EventId fields.
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new Score property
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramEventLogDtoItem : PlanogramEventLogDto, IChildDtoItem<PlanogramEventLogDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramEventLogDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramEventLogDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramEventLogId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramEventLogPlanogramId));
            EventType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramEventLogEventType));
            EntryType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramEventLogEntryType));
            AffectedId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramEventLogAffectedId));
            AffectedType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramEventLogAffectedType));
            WorkpackageSource = (String)item.GetItemPropertyValue(FieldNames.PlanogramEventLogWorkpackageSource);
            TaskSource = (String)item.GetItemPropertyValue(FieldNames.PlanogramEventLogTaskSource);
            EventId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramEventLogEventId));
            DateTime = GalleriaBinaryFileItemHelper.GetDateTime(item.GetItemPropertyValue(FieldNames.PlanogramEventLogDateTime));
            Description = (String)item.GetItemPropertyValue(FieldNames.PlanogramEventLogDescription);
            Content = (String)item.GetItemPropertyValue(FieldNames.PlanogramEventLogContent);
            Score = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramEventLogScore));
        }

        /// <summary>
        /// Creates a new PlanogramEventLogDtoItem and populates its properties with values taken from a 
        /// PlanogramEventLogDto.
        /// </summary>
        public PlanogramEventLogDtoItem(PlanogramEventLogDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            EventType = dto.EventType;
            EntryType = dto.EntryType;
            AffectedId = dto.AffectedId;
            AffectedType = dto.AffectedType;
            WorkpackageSource = dto.WorkpackageSource;
            TaskSource = dto.TaskSource;
            EventId = dto.EventId;
            DateTime = dto.DateTime;
            Description = dto.Description;
            Content = dto.Content;
            Score = dto.Score;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramEventLogDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramEventLogDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramEventLogDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramEventLogDto CopyDto()
        {
            return new PlanogramEventLogDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                EventType = this.EventType,
                EntryType = this.EntryType,
                AffectedId = this.AffectedId,
                AffectedType = this.AffectedType,
                WorkpackageSource = this.WorkpackageSource,
                TaskSource = this.TaskSource,
                EventId = this.EventId,
                DateTime = this.DateTime,
                Description = this.Description,
                Content = this.Content,
                Score = this.Score
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramEventLogId:
                    return Id;
                case FieldNames.PlanogramEventLogPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramEventLogEventType:
                    return EventType;
                case FieldNames.PlanogramEventLogEntryType:
                    return EntryType;
                case FieldNames.PlanogramEventLogAffectedId:
                    return AffectedId;
                case FieldNames.PlanogramEventLogAffectedType:
                    return AffectedType;
                case FieldNames.PlanogramEventLogWorkpackageSource:
                    return WorkpackageSource;
                case FieldNames.PlanogramEventLogTaskSource:
                    return TaskSource;
                case FieldNames.PlanogramEventLogEventId:
                    return EventId;
                case FieldNames.PlanogramEventLogDateTime:
                    return DateTime;
                case FieldNames.PlanogramEventLogDescription:
                    return Description;
                case FieldNames.PlanogramEventLogContent:
                    return Content;
                case FieldNames.PlanogramEventLogScore:
                    return Score;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
