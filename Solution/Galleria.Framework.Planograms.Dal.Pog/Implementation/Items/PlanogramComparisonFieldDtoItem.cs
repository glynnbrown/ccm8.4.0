﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31963 : A.Silva
//  Added Display field.

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramComparisonFieldDtoItem : PlanogramComparisonFieldDto, IChildDtoItem<PlanogramComparisonFieldDto>
    {
        #region Public Properties

        Object IChildDtoItem<PlanogramComparisonFieldDto>.Id { get { return Id; } set { Id = value; } }

        Boolean IChildDtoItem<PlanogramComparisonFieldDto>.AutoIncrementId { get { return true; } }

        Object IChildDtoItem<PlanogramComparisonFieldDto>.ParentId { get { return PlanogramComparisonId; } }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonFieldDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The <see cref="GalleriaBinaryFile.IItem" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonFieldDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldId));
            PlanogramComparisonId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldPlanogramComparisonId));
            ItemType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldItemType));
            FieldPlaceholder = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldFieldPlaceholder);
            DisplayName = (String)item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldDisplayName);
            Number = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldNumber));
            Display = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramComparisonFieldDisplay));
        }

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonFieldDtoItem" /> and populates it with data contained in the
        ///     given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldDto" /> contaning the values used to populate the new instance.</param>
        public PlanogramComparisonFieldDtoItem(PlanogramComparisonFieldDto dto)
        {
            Id = dto.Id;
            PlanogramComparisonId = dto.PlanogramComparisonId;
            ItemType = dto.ItemType;
            FieldPlaceholder = dto.FieldPlaceholder;
            DisplayName = dto.DisplayName;
            Number = dto.Number;
            Display = dto.Display;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramComparisonFieldDto CopyDto()
        {
            return new PlanogramComparisonFieldDto
                   {
                       Id = Id,
                       PlanogramComparisonId = PlanogramComparisonId,
                       ItemType = ItemType,
                       FieldPlaceholder = FieldPlaceholder,
                       DisplayName = DisplayName,
                       Number = Number,
                       Display = Display
                   };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramComparisonFieldId:
                    return Id;
                case FieldNames.PlanogramComparisonFieldPlanogramComparisonId:
                    return PlanogramComparisonId;
                case FieldNames.PlanogramComparisonFieldItemType:
                    return ItemType;
                case FieldNames.PlanogramComparisonFieldFieldPlaceholder:
                    return FieldPlaceholder;
                case FieldNames.PlanogramComparisonFieldDisplayName:
                    return DisplayName;
                case FieldNames.PlanogramComparisonFieldNumber:
                    return Number;
                case FieldNames.PlanogramComparisonFieldDisplay:
                    return Display;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}