﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27647 : L.Luong 
//   Created 

#endregion
#region Version History: (CCM v8.20)
// V8-30773 : J.Pickup
//   Inventory Profile added. 

#endregion

#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramInventoryDtoItem : PlanogramInventoryDto, IChildDtoItem<PlanogramInventoryDto>,GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramInventoryDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramInventoryDtoItem (GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramInventoryId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramInventoryPlanogramId));
            DaysOfPerformance = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramInventoryDaysOfPerformance));
            MinCasePacks = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramInventoryMinCasePacks));
            MinDos = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramInventoryMinDos));
            MinShelfLife = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramInventoryMinShelfLife));
            MinDeliveries = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.PlanogramInventoryMinDeliveries));
            IsCasePacksValidated = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramInventoryIsCasePacksValidated));
            IsDosValidated = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramInventoryIsDosValidated));
            IsShelfLifeValidated = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramInventoryIsShelfLifeValidated));
            IsDeliveriesValidated = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.PlanogramInventoryIsDeliveriesValidated));
            InventoryMetricType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramInventoryInventoryMetricType));
        }

        /// <summary>
        /// Creates a new PlanogramInventoryDtoItem and populates its properties with values taken from a 
        /// PlanogramInventoryDto.
        /// </summary>
        public PlanogramInventoryDtoItem(PlanogramInventoryDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            DaysOfPerformance = dto.DaysOfPerformance;
            MinCasePacks = dto.MinCasePacks;
            MinDos = dto.MinDos;
            MinShelfLife = dto.MinShelfLife;
            MinDeliveries = dto.MinDeliveries;
            IsCasePacksValidated = dto.IsCasePacksValidated;
            IsDosValidated = dto.IsDosValidated;
            IsShelfLifeValidated = dto.IsShelfLifeValidated;
            IsDeliveriesValidated = dto.IsDeliveriesValidated;
            InventoryMetricType = dto.InventoryMetricType;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramInventoryDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramInventoryDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramInventoryDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramInventoryDto CopyDto()
        {
            return new PlanogramInventoryDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                DaysOfPerformance = this.DaysOfPerformance,
                MinCasePacks = this.MinCasePacks,
                MinDos = this.MinDos,
                MinShelfLife = this.MinShelfLife,
                MinDeliveries = this.MinDeliveries,
                IsCasePacksValidated = this.IsCasePacksValidated,
                IsDosValidated = this.IsDosValidated,
                IsShelfLifeValidated = this.IsShelfLifeValidated,
                IsDeliveriesValidated = this.IsDeliveriesValidated,
                InventoryMetricType = this.InventoryMetricType
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramInventoryId:
                    return Id;
                case FieldNames.PlanogramInventoryPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramInventoryDaysOfPerformance:
                    return DaysOfPerformance;
                case FieldNames.PlanogramInventoryMinCasePacks:
                    return MinCasePacks;
                case FieldNames.PlanogramInventoryMinDos:
                    return MinDos;
                case FieldNames.PlanogramInventoryMinShelfLife:
                    return MinShelfLife;
                case FieldNames.PlanogramInventoryMinDeliveries:
                    return MinDeliveries;
                case FieldNames.PlanogramInventoryIsCasePacksValidated:
                    return IsCasePacksValidated;
                case FieldNames.PlanogramInventoryIsDosValidated:
                    return IsDosValidated;
                case FieldNames.PlanogramInventoryIsShelfLifeValidated:
                    return IsShelfLifeValidated;
                case FieldNames.PlanogramInventoryIsDeliveriesValidated:
                    return IsDeliveriesValidated;
                case FieldNames.PlanogramInventoryInventoryMetricType:
                    return InventoryMetricType;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
