﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 830
// V8-32504 : A.Kuszyk
//	Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramSequenceGroupSubGroupDtoItem : PlanogramSequenceGroupSubGroupDto, IChildDtoItem<PlanogramSequenceGroupSubGroupDto>
    {
        #region Constructors

        public PlanogramSequenceGroupSubGroupDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupSubGroupId));
            PlanogramSequenceGroupId =GalleriaBinaryFileItemHelper.GetInt32(
                item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId));
            Name = (String) item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupSubGroupName);
            Alignment = GalleriaBinaryFileItemHelper.GetByte(
                item.GetItemPropertyValue(FieldNames.PlanogramSequenceGroupSubGroupAlignment));
        }

        public PlanogramSequenceGroupSubGroupDtoItem(PlanogramSequenceGroupSubGroupDto dto)
        {
            Id = dto.Id;
            PlanogramSequenceGroupId = dto.PlanogramSequenceGroupId;
            Name = dto.Name;
            Alignment = dto.Alignment;
        }

        #endregion

        #region IChildDtoItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramSequenceGroupSubGroupId:
                    return Id;
                case FieldNames.PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId:
                    return PlanogramSequenceGroupId;
                case FieldNames.PlanogramSequenceGroupSubGroupName:
                    return Name;
                case FieldNames.PlanogramSequenceGroupSubGroupAlignment:
                    return Alignment;
                default:
                    return null;
            }
        }

        /// <summary>
        ///     Create and return a new instance with identical values to this one.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramSequenceDto"/>.</returns>
        public PlanogramSequenceGroupSubGroupDto CopyDto()
        {
            return new PlanogramSequenceGroupSubGroupDto
            {
                Id = Id,
                PlanogramSequenceGroupId = PlanogramSequenceGroupId,
                Name = Name,
                Alignment = Alignment
            };
        }

        public Boolean AutoIncrementId
        {
            get { return true; }
        }

        public Object ParentId
        {
            get { return PlanogramSequenceGroupId; }
        }

        #endregion
    }
}
