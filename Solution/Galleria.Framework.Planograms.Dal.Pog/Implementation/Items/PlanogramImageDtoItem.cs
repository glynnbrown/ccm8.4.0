﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup/A.Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Allows Ids to be something other than Int32
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramImageDtoItem : PlanogramImageDto, IChildDtoItem<PlanogramImageDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramImageDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramImageDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramImageId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramImagePlanogramId));
            FileName = (String)item.GetItemPropertyValue(FieldNames.PlanogramImageFileName);
            Description = (String)item.GetItemPropertyValue(FieldNames.PlanogramImageDescription);
            ImageData = (Byte[])item.GetItemPropertyValue(FieldNames.PlanogramImageImageData);
        }

        /// <summary>
        /// Creates a new PlanogramImageDtoItem and populates its properties with values taken from a 
        /// PlanogramImageDto.
        /// </summary>
        public PlanogramImageDtoItem(PlanogramImageDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            FileName = dto.FileName;
            Description = dto.Description;
            ImageData= dto.ImageData;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramImageDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramImageDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramImageDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramImageDto CopyDto()
        {
            return new PlanogramImageDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                FileName = this.FileName,
                Description = this.Description,
                ImageData= this.ImageData
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramImageId:
                    return Id;
                case FieldNames.PlanogramImagePlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramImageFileName:
                    return FileName;
                case FieldNames.PlanogramImageDescription:
                    return Description;
                case FieldNames.PlanogramImageImageData:
                    return ImageData;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
