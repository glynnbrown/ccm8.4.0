﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramBlockingDtoItem : PlanogramBlockingDto, IChildDtoItem<PlanogramBlockingDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramBlockingDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramBlockingDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingId));
            PlanogramId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramBlockingPlanogramId));
            Type = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.PlanogramBlockingType));
        }

        /// <summary>
        /// Creates a new PlanogramBlockingDtoItem and populates its properties with values taken from a 
        /// PlanogramBlockingDto.
        /// </summary>
        public PlanogramBlockingDtoItem(PlanogramBlockingDto dto)
        {
            Id = dto.Id;
            PlanogramId = dto.PlanogramId;
            Type = dto.Type;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramBlockingDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramBlockingDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramBlockingDto>.ParentId
        {
            get { return PlanogramId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramBlockingDto CopyDto()
        {
            return new PlanogramBlockingDto()
            {
                Id = this.Id,
                PlanogramId = this.PlanogramId,
                Type = this.Type,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramBlockingId:
                    return Id;
                case FieldNames.PlanogramBlockingPlanogramId:
                    return PlanogramId;
                case FieldNames.PlanogramBlockingType:
                    return Type;

                default:
                    return null;
            }
        }

        #endregion

        #endregion

    }
}
