﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.IO;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.Pog.Schema;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation.Items
{
    public class PlanogramAssortmentRegionProductDtoItem : PlanogramAssortmentRegionProductDto, IChildDtoItem<PlanogramAssortmentRegionProductDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new PlanogramAssortmentRegionProductDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public PlanogramAssortmentRegionProductDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionProductId));
            PlanogramAssortmentRegionId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionProductPlanogramAssortmentRegionId));
            PrimaryProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionProductPrimaryProductGtin);
            RegionalProductGtin = (String)item.GetItemPropertyValue(FieldNames.PlanogramAssortmentRegionProductRegionalProductGtin);
        }

        /// <summary>
        /// Creates a new PlanogramAssortmentRegionProductDtoItem and populates its properties with values taken from a 
        /// PlanogramAssortmentDto.
        /// </summary>
        public PlanogramAssortmentRegionProductDtoItem(PlanogramAssortmentRegionProductDto dto)
        {
            Id = dto.Id;
            PlanogramAssortmentRegionId = dto.PlanogramAssortmentRegionId;
            PrimaryProductGtin = dto.PrimaryProductGtin;
            RegionalProductGtin = dto.RegionalProductGtin;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<PlanogramAssortmentRegionProductDto>.Id
        {
            get { return Id; }
            set { Id = value; }
        }

        Boolean IChildDtoItem<PlanogramAssortmentRegionProductDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<PlanogramAssortmentRegionProductDto>.ParentId
        {
            get { return PlanogramAssortmentRegionId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public PlanogramAssortmentRegionProductDto CopyDto()
        {
            return new PlanogramAssortmentRegionProductDto()
            {
                Id = this.Id,
                PlanogramAssortmentRegionId = this.PlanogramAssortmentRegionId,
                PrimaryProductGtin = this.PrimaryProductGtin,
                RegionalProductGtin = this.RegionalProductGtin
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.PlanogramAssortmentRegionProductId:
                    return Id;
                case FieldNames.PlanogramAssortmentRegionProductPlanogramAssortmentRegionId:
                    return PlanogramAssortmentRegionId;
                case FieldNames.PlanogramAssortmentRegionProductPrimaryProductGtin:
                    return PrimaryProductGtin;
                case FieldNames.PlanogramAssortmentRegionProductRegionalProductGtin:
                    return RegionalProductGtin;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
