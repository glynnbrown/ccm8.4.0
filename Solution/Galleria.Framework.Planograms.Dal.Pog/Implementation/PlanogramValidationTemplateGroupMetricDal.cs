﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramValidationTemplateGroupMetricDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramValidationTemplateGroupMetricDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram validation template group metrics with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramValidationTemplateGroupMetricDto> FetchByPlanogramValidationTemplateGroupId(object id)
        {
            return DalCache.PlanogramValidationTemplateGroupMetrics.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram validation template group metric, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramValidationTemplateGroupMetricDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramValidationTemplateGroupMetrics.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupMetricDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram validation template group metric by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramValidationTemplateGroupMetricDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramValidationTemplateGroupMetrics.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupMetricDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram validation template group metric by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramValidationTemplateGroupMetrics.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramValidationTemplateGroupMetricDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupMetricDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
