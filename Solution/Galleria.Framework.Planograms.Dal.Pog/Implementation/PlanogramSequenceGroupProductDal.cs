﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramSequenceGroupProductDal : DalBase<DalCache>, IPlanogramSequenceGroupProductDal
    {
        #region Fetch

        /// <summary>
        ///     Fetches the PlanogramSequenceGroupProductDtos with a given planogram sequence group ID.
        /// </summary>
        /// <param name="planogramSequenceGroupId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramSequenceGroupProductDto> FetchByPlanogramSequenceGroupId(Object planogramSequenceGroupId)
        {
            return DalCache.PlanogramSequencesGroupProducts.FetchByParentId((Int32)planogramSequenceGroupId);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a Planogram Sequence Group Product by using the values in the provided <paramref name="dto"/>, assigning it the next available integer ID.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupProductDto"/> containing the values of the newly inserted record.</param>
        public void Insert(PlanogramSequenceGroupProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            dto.Id = DalCache.PlanogramSequencesGroupProducts.Insert(dto);
        }

        /// <summary>
        ///     Inserts a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupProductDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            foreach (PlanogramSequenceGroupProductDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates a Planogram Sequence Group Product by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupProductDto"/> containing the values to update.</param>
        public void Update(PlanogramSequenceGroupProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequencesGroupProducts.Update(dto);
        }

        /// <summary>
        ///     Updates a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupProductDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            foreach (PlanogramSequenceGroupProductDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a Planogram Sequence Group Product matching the <paramref name="id"/>, if it exists.
        /// </summary>
        /// <param name="id">The <see cref="PlanogramSequenceDto"/> containing the values to update.</param>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequencesGroupProducts.Delete((Int32)id);
        }

        /// <summary>
        ///     Deletes a Planogram Sequence Group Product by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupProductDto"/> containing the values to delete.</param>
        public void Delete(PlanogramSequenceGroupProductDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupProductDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            foreach (PlanogramSequenceGroupProductDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}
