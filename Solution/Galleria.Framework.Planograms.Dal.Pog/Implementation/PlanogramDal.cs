﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planograms.  (The contract for this method as defined in the interface is to return all
        /// planograms for a specific package, but as this DAL reads data from a Galleria Planogram File, and each such
        /// file is only allowed to contain one package, we might as well just return everything.
        /// </summary>
        public IEnumerable<PlanogramDto> FetchByPackageId(Object packageId)
        {
            return DalCache.Planograms.FetchAll();
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PackageDto packageDto, PlanogramDto planogramDto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            planogramDto.Id = DalCache.Planograms.Insert(planogramDto);
        }

        /// <summary>
        /// For unit testing purposes only
        /// </summary>
        public void Insert(PlanogramDto planogramDto)
        {
            this.Insert(null, planogramDto);
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram by ID, if the planogram exists.
        /// </summary>
        public void Update(PackageDto packageDto, PlanogramDto planogramDto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.Planograms.Update(planogramDto);
        }

        /// <summary>
        /// For unit testing purposes only
        /// </summary>
        /// <param name="dto"></param>
        public void Update(PlanogramDto planogramDto)
        {
            this.Update(null, planogramDto);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.Planograms.Delete((Int32)id);
        }

        #endregion
    }
}
