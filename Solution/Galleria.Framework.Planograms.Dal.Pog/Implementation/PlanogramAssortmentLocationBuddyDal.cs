﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramAssortmentLocationBuddyDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramAssortmentLocationBuddyDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram assortment location buddy with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramAssortmentLocationBuddyDto> FetchByPlanogramAssortmentId(object id)
        {
            return DalCache.PlanogramAssortmentLocationBuddys.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram assortment location buddy, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramAssortmentLocationBuddyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramAssortmentLocationBuddys.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentLocationBuddyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram assortment location buddy by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramAssortmentLocationBuddyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentLocationBuddys.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentLocationBuddyDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram assortment location buddy by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentLocationBuddys.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentLocationBuddyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentLocationBuddyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }


        #endregion
    }
}

