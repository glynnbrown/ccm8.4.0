﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 830
// V8-32504 : A.Kuszyk
//	Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramSequenceGroupSubGroupDal : DalBase<DalCache>, IPlanogramSequenceGroupSubGroupDal
    {
        #region Fetch

        /// <summary>
        ///     Fetches the PlanogramSequenceGroupSubGroupDtos with a given planogram sequence group ID.
        /// </summary>
        /// <param name="planogramSequenceGroupId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramSequenceGroupSubGroupDto> FetchByPlanogramSequenceGroupId(Object planogramSequenceGroupId)
        {
            return DalCache.PlanogramSequencesGroupSubGroups.FetchByParentId((Int32)planogramSequenceGroupId);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a Planogram Sequence Group SubGroup by using the values in the provided <paramref name="dto"/>, assigning it the next available integer ID.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupSubGroupDto"/> containing the values of the newly inserted record.</param>
        public void Insert(PlanogramSequenceGroupSubGroupDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            dto.Id = DalCache.PlanogramSequencesGroupSubGroups.Insert(dto);
        }

        /// <summary>
        ///     Inserts a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupSubGroupDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupSubGroupDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates a Planogram Sequence Group SubGroup by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupSubGroupDto"/> containing the values to update.</param>
        public void Update(PlanogramSequenceGroupSubGroupDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequencesGroupSubGroups.Update(dto);
        }

        /// <summary>
        ///     Updates a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupSubGroupDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupSubGroupDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a Planogram Sequence Group SubGroup matching the <paramref name="id"/>, if it exists.
        /// </summary>
        /// <param name="id">The <see cref="PlanogramSequenceDto"/> containing the values to update.</param>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequencesGroupSubGroups.Delete((Int32)id);
        }

        /// <summary>
        ///     Deletes a Planogram Sequence Group SubGroup by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupSubGroupDto"/> containing the values to delete.</param>
        public void Delete(PlanogramSequenceGroupSubGroupDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupSubGroupDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupSubGroupDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}
