﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramAssortmentRegionProductDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramAssortmentRegionProductDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram assortment region locations with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramAssortmentRegionProductDto> FetchByPlanogramAssortmentRegionId(object id)
        {
            return DalCache.PlanogramAssortmentRegionProducts.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram assortment region location, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramAssortmentRegionProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramAssortmentRegionProducts.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            foreach (PlanogramAssortmentRegionProductDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram assortment region location by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramAssortmentRegionProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentRegionProducts.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            foreach (PlanogramAssortmentRegionProductDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram assortment region location by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentRegionProducts.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentRegionProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            foreach (PlanogramAssortmentRegionProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
