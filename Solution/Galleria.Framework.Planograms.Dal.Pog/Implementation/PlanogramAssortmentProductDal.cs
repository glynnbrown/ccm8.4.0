﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramAssortmentProductDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramAssortmentProductDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram assortment products with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramAssortmentProductDto> FetchByPlanogramAssortmentId(object id)
        {
            return DalCache.PlanogramAssortmentProducts.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram assortment product, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramAssortmentProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramAssortmentProducts.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            foreach (PlanogramAssortmentProductDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram assortment product by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramAssortmentProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentProducts.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            foreach (PlanogramAssortmentProductDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram assortment product by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentProducts.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            foreach (PlanogramAssortmentProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
