﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26147 : L.Ineson
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-31456 : N.Foster
//  Added method to fetch a batch of custom attributes
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Implementation.Items;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    /// <summary>
    /// Pog dal implementation of ICustomAttributeDataDal
    /// </summary>
    public class CustomAttributeDataDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, ICustomAttributeDataDal
    {
        #region Fetch

        /// <summary>
        /// Fetches the CustomAttributeDataDto item 
        /// for the given parent type and parent id.
        /// </summary>
        /// <param name="parentType"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public CustomAttributeDataDto FetchByParentTypeParentId(Byte parentType, object parentId)
        {
            CustomAttributeDataDto dto = 
                DalCache.CustomAttributeData.FetchByParentId(
                new CustomAttributeDataDtoItem.CustomAttributeDataDtoItemParentKey()
                {
                    ParentType = parentType,
                    ParentId = parentId,
                }).FirstOrDefault();

            if (dto == null)
            {
                throw new DtoDoesNotExistException();
            }

            return dto;
        }

        public IEnumerable<CustomAttributeDataDto> FetchByParentTypeParentIds(Byte parentType, IEnumerable<Object> parentIds)
        {
            List<CustomAttributeDataDto> dtos = new List<CustomAttributeDataDto>();
            foreach (Object parentId in parentIds)
            {
                dtos.AddRange(this.DalCache.CustomAttributeData.FetchByParentId(
                    new CustomAttributeDataDtoItem.CustomAttributeDataDtoItemParentKey()
                    {
                        ParentType = parentType,
                        ParentId = parentId
                    }));
            }
            return dtos;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a CustomAttributeData, assigning it the next available integer ID.
        /// </summary>
        public void Insert(CustomAttributeDataDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.CustomAttributeData.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items into the dal
        /// </summary>
        public void Insert(IEnumerable<CustomAttributeDataDto> dtos)
        {
            foreach (CustomAttributeDataDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a CustomAttributeData, if the id exists.
        /// </summary>
        public void Update(CustomAttributeDataDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.CustomAttributeData.Update(dto);
        }

        /// <summary>
        /// Updates the specified items in the dal
        /// </summary>
        public void Update(IEnumerable<CustomAttributeDataDto> dtos)
        {
            foreach (CustomAttributeDataDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Upsert

        public void Upsert(IEnumerable<CustomAttributeDataDto> dtoList, CustomAttributeDataIsSetDto isSetDto)
        {
            //Not required by this dal.
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a CustomAttributeData by ID, if it exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.CustomAttributeData.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item from the dal
        /// </summary>
        public void Delete(CustomAttributeDataDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes all the specified items from the dal
        /// </summary>
        public void Delete(IEnumerable<CustomAttributeDataDto> dtos)
        {
            foreach (CustomAttributeDataDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
