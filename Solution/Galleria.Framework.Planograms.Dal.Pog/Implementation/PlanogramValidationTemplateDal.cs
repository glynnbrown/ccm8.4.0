﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramValidationTemplateDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramValidationTemplateDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram Validation templates with a given planogram ID.
        /// </summary>
        public PlanogramValidationTemplateDto FetchByPlanogramId(object id)
        {
            PlanogramValidationTemplateDto dto = DalCache.PlanogramValidationTemplates.FetchByParentId((Int32)id).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram Validation template, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramValidationTemplateDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramValidationTemplates.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            foreach (PlanogramValidationTemplateDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram Validation template by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramValidationTemplateDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramValidationTemplates.Update(dto);
        }

        /// <summary>
        /// Updates the specified dto
        /// </summary>
        public void Update(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            foreach (PlanogramValidationTemplateDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram Validation template by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramValidationTemplates.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramValidationTemplateDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            foreach (PlanogramValidationTemplateDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
