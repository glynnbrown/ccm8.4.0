﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramSequenceGroupDal : DalBase<DalCache>, IPlanogramSequenceGroupDal
    {
        #region Fetch

        /// <summary>
        ///     Fetches the PlanogramSequenceGroupDtos with a given planogram sequence ID.
        /// </summary>
        /// <param name="planogramSequenceId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramSequenceGroupDto> FetchByPlanogramSequenceId(Object planogramSequenceId)
        {
            return DalCache.PlanogramSequenceGroups.FetchByParentId((Int32)planogramSequenceId);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a Planogram Sequence Group by using the values in the provided <paramref name="dto"/>, assigning it the next available integer ID.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupDto"/> containing the values of the newly inserted record.</param>
        public void Insert(PlanogramSequenceGroupDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            dto.Id = DalCache.PlanogramSequenceGroups.Insert(dto);
        }

        /// <summary>
        ///     Inserts a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates a Planogram Sequence Group by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupDto"/> containing the values to update.</param>
        public void Update(PlanogramSequenceGroupDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequenceGroups.Update(dto);
        }

        /// <summary>
        ///     Updates a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a Planogram Sequence Group matching the <paramref name="id"/>, if it exists.
        /// </summary>
        /// <param name="id">The <see cref="PlanogramSequenceGroupDto"/> containing the values to update.</param>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }

            DalCache.PlanogramSequenceGroups.Delete((Int32)id);
        }

        /// <summary>
        ///     Deletes a Planogram Sequence Group by using the values in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupDto"/> containing the values to delete.</param>
        public void Delete(PlanogramSequenceGroupDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes a collection of <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}
