﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
// V8-25949 : N.Foster
//  Changed LockById to return success or failure
// V8-27411 : M.Pettit
//  Package locking now requires userId and locktype
// V8-26637 : L.Ineson
//  Made sure that an empty file is not left behind when FetchById throws a DtoDoesNotExistException
// V8-27919 : L.Ineson
//  Added overload to FetchById for fetchArgument parameter
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    /// <summary>
    /// Note: The id parameter to various methods in this class is ignored, as a Galleria Planogram File may contain
    /// only one package anyway.
    /// </summary>
    public class PackageDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPackageDal
    {
        #region Lock/Unlock

        /// <summary>
        /// Locks the package.
        /// </summary>
        public Byte LockById(Object id, Object lockUserId, Byte lockType, Boolean readOnly)
        {
            if (!this.DalContext.LockDalCache(readOnly)) return 0; // Failed
            if (this.DalCache.IsReadOnly == readOnly)  return 1; // Success
            return 0; // FAILED
        }

        /// <summary>
        /// Unlocks the package.
        /// </summary>
        public Byte UnlockById(Object id, Object lockUserId, Byte lockType, Boolean readOnly)
        {
            this.DalContext.UnlockDalCache();
            return 1; // Success
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Fetches the package.
        /// </summary>
        public PackageDto FetchById(Object packageId)
        {
            return FetchById(packageId, null);
        }

        /// <summary>
        /// Fetches the package.
        /// </summary>
        /// <param name="packageId">id of the package to fetch</param>
        /// <param name="fetchArgument">optional argument (currently not used by this dal)</param>
        public PackageDto FetchById(Object packageId, Object fetchArgument)
        {
            PackageDto dto =  DalCache.Package.Fetch();
            if (dto == null)
            {
                //Call delete by id so that we don't leave an empty file.
                DeleteById(packageId);
                throw new DtoDoesNotExistException();
            }
            dto.Id = this.DalCache.Filename;
            return dto;
        }

        public IList<Object> FetchIdsExceptFor(IEnumerable<string> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchByUcrsDateLastModified(IEnumerable<String> ucrs, DateTime dateLastModified)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchDeletedByUcrs(IEnumerable<String> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a package.
        /// </summary>
        public void Insert(PackageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            if (DalCache.Package.Fetch() != null)
            {
                throw new DuplicateException();
            }
            dto.Id = this.DalCache.Filename;
            DalCache.Package.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a package.
        /// </summary>
        public void Update(PackageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            if (DalCache.Package.Fetch() != null)
            {
                dto.Id = this.DalCache.Filename;
                DalCache.Package.InsertOrUpdate(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a package.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_Transaction);
            }
            DalCache.Package.Delete();
            DalCache.Commit();
            DalContext.UnlockDalCache();
        }
        #endregion

        #region UpdatePlanogramAttributesUpdatePlanogramAttributes

        public byte UpdatePlanogramAttributes(System.Collections.Generic.IEnumerable<PlanogramAttributesDto> planAttributesDtoList)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
