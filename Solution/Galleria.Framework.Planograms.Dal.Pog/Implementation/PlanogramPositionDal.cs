﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramPositionDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramPositionDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram positions with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramPositionDto> FetchByPlanogramId(Object planogramId)
        {
            return DalCache.PlanogramPositions.FetchByParentId((Int32)planogramId);
        }

        #endregion      

        #region Insert

        /// <summary>
        /// Inserts a planogram position, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramPositionDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramPositions.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramPositionDto> dtos)
        {
            foreach (PlanogramPositionDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram position by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramPositionDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramPositions.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramPositionDto> dtos)
        {
            foreach (PlanogramPositionDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram position by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramPositions.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramPositionDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramPositionDto> dtos)
        {
            foreach (PlanogramPositionDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
