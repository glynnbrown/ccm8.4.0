﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramSubComponentDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramSubComponentDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram sub components with a given planogram component ID.
        /// </summary>
        public IEnumerable<PlanogramSubComponentDto> FetchByPlanogramComponentId(Object planogramComponentId)
        {
            return DalCache.PlanogramSubComponents.FetchByParentId((Int32)planogramComponentId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram sub component, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramSubComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramSubComponents.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram sub component by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramSubComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramSubComponents.Update(dto);
        }

        /// <summary>
        /// Updates the specified dtos
        /// </summary>
        public void Update(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram sub component by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramSubComponents.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramSubComponentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
