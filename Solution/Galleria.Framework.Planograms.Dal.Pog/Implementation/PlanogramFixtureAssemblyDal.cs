﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramFixtureAssemblyDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramFixtureAssemblyDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram fixture assemblys with a given planogram fixture ID.
        /// </summary>
        public IEnumerable<PlanogramFixtureAssemblyDto> FetchByPlanogramFixtureId(Object planogramFixtureId)
        {
            return DalCache.PlanogramFixtureAssemblies.FetchByParentId((Int32)planogramFixtureId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram fixture assembly, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramFixtureAssemblyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramFixtureAssemblies.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }


        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram fixture assembly by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramFixtureAssemblyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramFixtureAssemblies.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram fixture assembly by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramFixtureAssemblies.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramFixtureAssemblyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
