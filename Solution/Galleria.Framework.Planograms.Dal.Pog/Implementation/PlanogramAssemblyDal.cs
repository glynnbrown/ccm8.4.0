﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//      Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramAssemblyDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramAssemblyDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram assemblies with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramAssemblyDto> FetchByPlanogramId(Object planogramId)
        {
            return DalCache.PlanogramAssemblies.FetchByParentId((Int32)planogramId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram assembly, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramAssemblyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramAssemblies.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items into the dal
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            foreach (PlanogramAssemblyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram assembly by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramAssemblyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssemblies.Update(dto);
        }

        /// <summary>
        /// Updates the specified items in the dal
        /// </summary>
        public void Update(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            foreach (PlanogramAssemblyDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram assembly by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssemblies.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item from the dal
        /// </summary>
        public void Delete(PlanogramAssemblyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items from the dal
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            foreach (PlanogramAssemblyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
