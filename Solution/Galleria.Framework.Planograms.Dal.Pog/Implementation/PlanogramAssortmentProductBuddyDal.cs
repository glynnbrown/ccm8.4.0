﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramAssortmentProductBuddyDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramAssortmentProductBuddyDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram assortment location buddy with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramAssortmentProductBuddyDto> FetchByPlanogramAssortmentId(object id)
        {
            return DalCache.PlanogramAssortmentProductBuddys.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram assortment location buddy, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramAssortmentProductBuddyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramAssortmentProductBuddys.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentProductBuddyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram assortment location buddy by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramAssortmentProductBuddyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentProductBuddys.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentProductBuddyDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram assortment location buddy by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentProductBuddys.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentProductBuddyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentProductBuddyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }


        #endregion
    }
}

