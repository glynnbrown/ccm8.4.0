﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramComparisonDal : Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramComparisonDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramComparisonDto"/> assigned to the given planogram <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> identifying the parent planogram.</param>
        public PlanogramComparisonDto FetchByPlanogramId(Object id)
        {
            PlanogramComparisonDto dto = DalCache.PlanogramComparisons.FetchByParentId((Int32)id).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the given <paramref name="dto"/> data, assigning the next available Integer value as ID.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonDto"/> containing the values to insert.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Insert(PlanogramComparisonDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramComparisons.Insert(dto);
        }

        /// <summary>
        ///     Insert the given <paramref name="dtos"/> data, assigning the next available Integer value as ID.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> containing the values to insert.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Insert(IEnumerable<PlanogramComparisonDto> dtos)
        {
            foreach (PlanogramComparisonDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the data from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonDto"/> containing the values to update.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Update(PlanogramComparisonDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComparisons.Update(dto);
        }

        /// <summary>
        ///     Update the data from the given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> containing the values to update.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Update(IEnumerable<PlanogramComparisonDto> dtos)
        {
            foreach (PlanogramComparisonDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> identifying the planogram comparison.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComparisons.Delete((Int32)id);
        }

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonDto"/> containing the values to delete.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Delete(PlanogramComparisonDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> containing the values to delete.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Delete(IEnumerable<PlanogramComparisonDto> dtos)
        {
            foreach (PlanogramComparisonDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}