﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramConsumerDecisionTreeNodeProductDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramConsumerDecisionTreeNodeProductDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram consumer Decision tree node products with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> FetchByPlanogramConsumerDecisionTreeNodeId(object id)
        {
            return DalCache.PlanogramConsumerDecisionTreeNodeProducts.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram consumer Decision tree node product, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramConsumerDecisionTreeNodeProducts.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeProductDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram consumer Decision tree node product by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramConsumerDecisionTreeNodeProducts.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeProductDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram consumer Decision tree node product by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramConsumerDecisionTreeNodeProducts.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
