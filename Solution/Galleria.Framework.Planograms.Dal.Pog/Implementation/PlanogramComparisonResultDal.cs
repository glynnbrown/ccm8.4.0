﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramComparisonResultDal : Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramComparisonResultDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the collection of <see cref="PlanogramComparisonResultDto"/> assigned to the given planogram comparison <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="object"/> identifying the parent planogram comparison.</param>
        public IEnumerable<PlanogramComparisonResultDto> FetchByPlanogramComparisonId(Object id)
        {
            List<PlanogramComparisonResultDto> dtos = DalCache.PlanogramComparisonResults.FetchByParentId((Int32)id);
            if (dtos == null) throw new DtoDoesNotExistException();
            return dtos;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the given <paramref name="dto"/> data, assigning the next available Integer value as ID.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonResultDto"/> containing the values to insert.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Insert(PlanogramComparisonResultDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramComparisonResults.Insert(dto);
        }

        /// <summary>
        ///     Insert the given <paramref name="dtos"/> data, assigning the next available Integer value as ID.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonResultDto"/> containing the values to insert.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Insert(IEnumerable<PlanogramComparisonResultDto> dtos)
        {
            foreach (PlanogramComparisonResultDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the data from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonResultDto"/> containing the values to update.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Update(PlanogramComparisonResultDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComparisonResults.Update(dto);
        }

        /// <summary>
        ///     Update the data from the given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> containing the values to update.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Update(IEnumerable<PlanogramComparisonResultDto> dtos)
        {
            foreach (PlanogramComparisonResultDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> identifying the planogram comparison.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComparisonResults.Delete((Int32)id);
        }

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonResultDto"/> containing the values to delete.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Delete(PlanogramComparisonResultDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Delete the data assigned to the given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonResultDto"/> containing the values to delete.</param>
        /// <remarks>The operation will be invalid if the containing planogram does not exist.</remarks>
        public void Delete(IEnumerable<PlanogramComparisonResultDto> dtos)
        {
            foreach (PlanogramComparisonResultDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}
