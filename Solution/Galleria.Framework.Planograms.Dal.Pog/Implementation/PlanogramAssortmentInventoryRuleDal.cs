﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramAssortmentInventoryRuleDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramAssortmentInventoryRuleDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram assortment inventory rules with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramAssortmentInventoryRuleDto> FetchByPlanogramAssortmentId(object id)
        {
            return DalCache.PlanogramAssortmentInventoryRules.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram assortment inventory rule, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramAssortmentInventoryRuleDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramAssortmentInventoryRules.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            foreach (PlanogramAssortmentInventoryRuleDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram assortment inventory rule by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramAssortmentInventoryRuleDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentInventoryRules.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            foreach (PlanogramAssortmentInventoryRuleDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram assortment inventory rule by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAssortmentInventoryRules.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentInventoryRuleDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            foreach (PlanogramAssortmentInventoryRuleDto dto in dtos)
            {
                this.Delete(dto);
            }
        }


        #endregion
    }
}

