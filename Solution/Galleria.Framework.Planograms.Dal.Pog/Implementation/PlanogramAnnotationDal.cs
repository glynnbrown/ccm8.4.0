﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//      Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramAnnotationDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramAnnotationDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram annotations with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramAnnotationDto> FetchByPlanogramId(Object planogramId)
        {
            return DalCache.PlanogramAnnotations.FetchByParentId((Int32)planogramId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram annotation, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramAnnotationDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramAnnotations.Insert(dto);
        }

        /// <summary>
        /// Inserts the items into the dal
        /// </summary>
        public void Insert(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram annotation by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramAnnotationDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAnnotations.Update(dto);
        }

        /// <summary>
        /// Updates the specified items in the dal
        /// </summary>
        public void Update(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram annotation by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramAnnotations.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item from the dal
        /// </summary>
        public void Delete(PlanogramAnnotationDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items from the dal
        /// </summary>
        public void Delete(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
