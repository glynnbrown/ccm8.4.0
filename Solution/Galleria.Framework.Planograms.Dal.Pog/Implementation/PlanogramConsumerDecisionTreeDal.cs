﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong 
//   Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramConsumerDecisionTreeDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramConsumerDecisionTreeDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram consumer Decision trees with a given planogram ID.
        /// </summary>
        public PlanogramConsumerDecisionTreeDto FetchByPlanogramId(object id)
        {
            PlanogramConsumerDecisionTreeDto dto = DalCache.PlanogramConsumerDecisionTrees.FetchByParentId((Int32)id).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram consumer Decision tree, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramConsumerDecisionTreeDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramConsumerDecisionTrees.Insert(dto);
        }

        /// <summary>
        /// Insert the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram consumer Decision tree by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramConsumerDecisionTreeDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramConsumerDecisionTrees.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram consumer Decision tree by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramConsumerDecisionTrees.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramConsumerDecisionTreeDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
