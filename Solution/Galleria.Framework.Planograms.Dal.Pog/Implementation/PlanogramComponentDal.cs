﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//      Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramComponentDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramComponentDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram annotations with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramComponentDto> FetchByPlanogramId(Object planogramId)
        {
            return DalCache.PlanogramComponents.FetchByParentId((Int32)planogramId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram annotation, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramComponents.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramComponentDto> dtos)
        {
            foreach (PlanogramComponentDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram annotation by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComponents.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramComponentDto> dtos)
        {
            foreach (PlanogramComponentDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram annotation by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramComponents.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramComponentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramComponentDto> dtos)
        {
            foreach (PlanogramComponentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
