﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27647 : L.Luong 
//  Created 
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramInventoryDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramInventoryDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram inventory with a given planogram ID.
        /// </summary>
        public PlanogramInventoryDto FetchByPlanogramId(object id)
        {
            PlanogramInventoryDto dto = DalCache.PlanogramInventory.FetchByParentId((Int32)id).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram inventory, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramInventoryDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramInventory.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramInventoryDto> dtos)
        {
            foreach (PlanogramInventoryDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram inventory by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramInventoryDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramInventory.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramInventoryDto> dtos)
        {
            foreach (PlanogramInventoryDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram inventory by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramInventory.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramInventoryDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramInventoryDto> dtos)
        {
            foreach (PlanogramInventoryDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
