﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27153 : A.Silva
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramRenumberingStrategyDal : DalBase<DalCache>, IPlanogramRenumberingStrategyDal
    {
        #region Fetch

        /// <summary>
        ///     Fetches all the <see cref="PlanogramRenumberingStrategyDto"/> for a given <paramref name="planogramId"/>.
        /// </summary>
        /// <param name="planogramId">Unique identifier of the planogram to retrieve items from.</param>
        /// <returns>A new collection of <see cref="PlanogramRenumberingStrategyDto"/> instances that match the given <paramref name="planogramId"/>.</returns>
        public PlanogramRenumberingStrategyDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramRenumberingStrategyDto dto = DalCache.PlanogramRenumberingStrategy.FetchByParentId((Int32)planogramId).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a planogram with the values from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramRenumberingStrategyDto"/> containing the values to insert.</param>
        public void Insert(PlanogramRenumberingStrategyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramRenumberingStrategy.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            foreach (PlanogramRenumberingStrategyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates a planogram with the values from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramRenumberingStrategyDto"/> containing the values to update.</param>
        public void Update(PlanogramRenumberingStrategyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramRenumberingStrategy.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            foreach (PlanogramRenumberingStrategyDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the planogram renumbering strategy matching the given <paramref name="id"/> from the planogram.
        /// </summary>
        /// <param name="id">Unique id for the planogram renumbering strategy to be removed.</param>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramRenumberingStrategy.Delete((Int32) id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramRenumberingStrategyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            foreach (PlanogramRenumberingStrategyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
