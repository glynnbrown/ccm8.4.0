﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26836 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramEventLogDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramEventLogDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram event logs with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramEventLogDto> FetchByPlanogramId(object id)
        {
            return DalCache.PlanogramEventLogs.FetchByParentId((Int32)id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram event log, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramEventLogDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramEventLogs.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramEventLogDto> dtos)
        {
            foreach (PlanogramEventLogDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram event log by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramEventLogDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramEventLogs.Update(dto);
        }

        /// <summary>
        /// Updates the specified dtos
        /// </summary>
        public void Update(IEnumerable<PlanogramEventLogDto> dtos)
        {
            foreach (PlanogramEventLogDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram event log by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramEventLogs.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramEventLogDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramEventLogDto> dtos)
        {
            foreach (PlanogramEventLogDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
