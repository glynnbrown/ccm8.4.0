﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramFixtureComponentDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramFixtureComponentDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram fixture components with a given planogram component ID.
        /// </summary>
        public IEnumerable<PlanogramFixtureComponentDto> FetchByPlanogramFixtureId(Object planogramFixtureId)
        {
            return DalCache.PlanogramFixtureComponents.FetchByParentId((Int32)planogramFixtureId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram fixture component, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramFixtureComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramFixtureComponents.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram fixture component by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramFixtureComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramFixtureComponents.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram fixture component by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramFixtureComponents.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramFixtureComponentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
