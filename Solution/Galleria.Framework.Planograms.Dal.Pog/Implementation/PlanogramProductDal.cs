﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramProductDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramProductDal
    {
        #region Fetch

        /// <summary>
        /// Fetches all planogram products with a given planogram ID.
        /// </summary>
        public IEnumerable<PlanogramProductDto> FetchByPlanogramId(Object planogramId)
        {
            return DalCache.PlanogramProducts.FetchByParentId((Int32)planogramId);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a planogram product, assigning it the next available integer ID.
        /// </summary>
        public void Insert(PlanogramProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramProducts.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramProductDto> dtos)
        {
            foreach (PlanogramProductDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Finds and updates a planogram product by ID, if the planogram exists.
        /// </summary>
        public void Update(PlanogramProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramProducts.Update(dto);
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramProductDto> dtos)
        {
            foreach (PlanogramProductDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Finds and deletes a planogram product by ID, if the planogram exists.
        /// </summary>
        public void DeleteById(Object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramProducts.Delete((Int32)id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramProductDto> dtos)
        {
            foreach (PlanogramProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
