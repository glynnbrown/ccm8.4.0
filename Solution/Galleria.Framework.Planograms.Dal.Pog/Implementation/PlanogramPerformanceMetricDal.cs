﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26269 : A.Kuszyk
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog.Implementation
{
    public class PlanogramPerformanceMetricDal : Galleria.Framework.Dal.Gbf.DalBase<DalCache>, IPlanogramPerformanceMetricDal
    {
        #region Fetch

        public IEnumerable<PlanogramPerformanceMetricDto> FetchByPlanogramPerformanceId(object id)
        {
            return DalCache.PlanogramPerformanceMetrics.FetchByParentId(id);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts the specified item
        /// </summary>
        public void Insert(PlanogramPerformanceMetricDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = DalCache.PlanogramPerformanceMetrics.Insert(dto);
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the specified item
        /// </summary>
        public void Update(PlanogramPerformanceMetricDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramPerformanceMetrics.Update(dto);
        }

        /// <summary>
        /// Updates this specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.PlanogramPerformanceMetrics.Delete(id);
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramPerformanceMetricDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
