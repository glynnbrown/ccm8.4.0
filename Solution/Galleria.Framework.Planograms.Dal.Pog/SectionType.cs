﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-24658 : K.Pickup
//  Initial version.
// V8-26147 : L.Ineson
//  Added CustomAttributeData.
// V8-26156 : N.Foster
//  Converted to use framework dal
// V8-26269 : A.Kuszyk
//  Added PlanogramPerformance, PlanogramPerformanceData and PlanogramPerformanceMetric.
// V8-26426 : A.Kuszyk
//  Added PlanogramAssortment related sections.
// V8-26573 : L.Luong
//  Added PlanogramValidationTemplate and related sections
// V8-26706 : L.Luong
//  Added PlanogramConsumerDecisionTree and related sections
// V8-26836 : L.Luong
//  Added PlanogramEventLog section
// V8-26891 : L.Ineson
//  Added Blocking sections
// V8-27058 : A.Probyn
//  Added PlanogramMetaDataImages
// V8-27153 : A.Silva   ~ Added PlanogramRenumberingStrategy.
// V8-27647 : L.Luong
//  Added Planogram Inventory

#endregion

#region Version History CCM802

// V8-28996 : A.Silva
//      Added PlanogramSequence, PlanogramSequenceGroups and PlanogramSequenceGroupProducts.

#endregion

#region Version History: CCM830
// V8-31550 : A.Probyn
//  Added PlanogramAssortmentLocationBuddys, PlanogramAssortmentProductBuddys
// V8-31551 : A.Probyn
//  Added PlanogramAssortmentInventoryRules
// V8-31819 : A.Silva
//  Added PlanogramComparison, PlanogramComparisonField, PlanogramComparisonResult, PlanogramComparisonItem, PlanogramComparisonFieldValue
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroups.
#endregion

#endregion

namespace Galleria.Framework.Planograms.Dal.Pog
{
    /// <summary>
    /// The SectionType enum represents the type of a section within a Galleria Planogram File.
    /// </summary>
    internal enum SectionType : ushort
    {
        SchemaVersion = 1,
        Package = 2,
        Planograms = 3,
        PlanogramAnnotations = 4,
        PlanogramAssemblies = 5,
        PlanogramAssemblyComponents = 6,
        PlanogramComponents = 7,
        PlanogramFixtures = 8,
        PlanogramFixtureAssemblies = 9,
        PlanogramFixtureComponents = 10,
        PlanogramFixtureItems = 11,
        PlanogramImages = 12,
        PlanogramPositions = 13,
        PlanogramProducts = 14,
        PlanogramSubComponents = 15,
        CustomAttributeData = 16,
        PlanogramPerformances = 17,
        PlanogramPerformanceMetrics = 18,
        PlanogramPerformanceDatas = 19,
        PlanogramAssortments = 20,
        PlanogramAssortmentProducts = 21,
        PlanogramAssortmentLocalProducts = 22,
        PlanogramAssortmentRegions = 23,
        PlanogramAssortmentRegionLocations = 24,
        PlanogramAssortmentRegionProducts = 25,
        PlanogramValidationTemplates = 26,
        PlanogramValidationTemplateGroups = 27,
        PlanogramValidationTemplateGroupMetrics = 28,
        PlanogramConsumerDecisionTrees = 29,
        PlanogramConsumerDecisionTreeLevels = 30,
        PlanogramConsumerDecisionTreeNodes = 31,
        PlanogramConsumerDecisionTreeNodeProducts = 32,
        PlanogramEventLogs = 33,
        PlanogramBlocking = 34,
        PlanogramBlockingGroups = 35,
        PlanogramBlockingGroupProducts = 36,
        PlanogramBlockingGroupFields = 37,
        PlanogramBlockingDividers = 38,
        PlanogramBlockingLocations = 39,
        PlanogramMetadataImages = 40,
        PlanogramRenumberingStrategy = 41,
        PlanogramInventory = 42,
        PlanogramSequence = 43,
        PlanogramSequenceGroups = 44,
        PlanogramSequenceGroupProducts = 45,
        PlanogramAssortmentInventoryRules = 46,
        PlanogramAssortmentLocationBuddys = 47,
        PlanogramAssortmentProductBuddys = 48,
        PlanogramComparisons = 49,
        PlanogramComparisonFields = 50,
        PlanogramComparisonResults = 51,
        PlanogramComparisonItems = 52,
        PlanogramComparisonFieldValues = 53,
        PlanogramSequenceGroupSubGroups = 54,
        ProductAttributeComparisonResults = 55,
    }
}
