﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;

//namespace Galleria.Framework.Planograms.Dal.Pog
//{
//    // TODO: Cleanup, comment, and move

//    public class ExtendedBinaryReader : BinaryReader
//    {
//        public ExtendedBinaryReader(Stream input) : base(input) { }

//        public ExtendedBinaryReader(Stream input, Encoding encoding) : base(input, encoding) { }

//        public Boolean[] ReadBooleanArray()
//        {
//            Int32 length = ReadInt32();
//            Boolean[] returnValue = new Boolean[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadBoolean();
//            }
//            return returnValue;
//        }

//        public Byte[] ReadByteArray()
//        {
//            Int32 length = ReadInt32();
//            return ReadBytes(length);
//        }

//        public Char[] ReadCharArray()
//        {
//            Int32 length = ReadInt32();
//            return ReadChars(length);
//        }

//        public DateTime ReadDateTime()
//        {
//            return new DateTime(ReadInt64());
//        }

//        public Decimal[] ReadDecimalArray()
//        {
//            Int32 length = ReadInt32();
//            Decimal[] returnValue = new Decimal[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadDecimal();
//            }
//            return returnValue;
//        }

//        public Double[] ReadDoubleArray()
//        {
//            Int32 length = ReadInt32();
//            Double[] returnValue = new Double[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadDouble();
//            }
//            return returnValue;
//        }

//        public Int16[] ReadInt16Array()
//        {
//            Int32 length = ReadInt32();
//            Int16[] returnValue = new Int16[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadInt16();
//            }
//            return returnValue;
//        }

//        public Int32[] ReadInt32Array()
//        {
//            Int32 length = ReadInt32();
//            Int32[] returnValue = new Int32[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadInt32();
//            }
//            return returnValue;
//        }

//        public Int64[] ReadInt64Array()
//        {
//            Int32 length = ReadInt32();
//            Int64[] returnValue = new Int64[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadInt64();
//            }
//            return returnValue;
//        }

//        public SByte[] ReadSByteArray()
//        {
//            Int32 length = ReadInt32();
//            SByte[] returnValue = new SByte[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadSByte();
//            }
//            return returnValue;
//        }

//        public Single[] ReadSingleArray()
//        {
//            Int32 length = ReadInt32();
//            Single[] returnValue = new Single[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadSingle();
//            }
//            return returnValue;
//        }

//        public UInt16[] ReadUInt16Array()
//        {
//            Int32 length = ReadInt32();
//            UInt16[] returnValue = new UInt16[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadUInt16();
//            }
//            return returnValue;
//        }

//        public UInt32[] ReadUInt32Array()
//        {
//            Int32 length = ReadInt32();
//            UInt32[] returnValue = new UInt32[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadUInt32();
//            }
//            return returnValue;
//        }

//        public UInt64[] ReadUInt64Array()
//        {
//            Int32 length = ReadInt32();
//            UInt64[] returnValue = new UInt64[length];
//            for (Int32 i = 0; i < length; i++)
//            {
//                returnValue[i] = ReadUInt64();
//            }
//            return returnValue;
//        }
//    }
//}
