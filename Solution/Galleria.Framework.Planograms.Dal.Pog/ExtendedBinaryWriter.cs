﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;

//namespace Galleria.Framework.Planograms.Dal.Pog
//{
//    // TODO: Cleanup, comment, and move

//    public class ExtendedBinaryWriter : BinaryWriter
//    {
//        public ExtendedBinaryWriter(Stream output) : base(output) { }

//        public ExtendedBinaryWriter(Stream output, Encoding encoding) : base(output, encoding) { }

//        public void Write(Boolean[] value)
//        {
//            Write(value.Length);
//            foreach (Boolean item in value)
//            {
//                Write(item);
//            }
//        }

//        public void WriteByteArrayAsSingleValue(Byte[] value)
//        {
//            Write(value.Length);
//            Write(value);
//        }

//        public void WriteCharArrayAsSingleValue(Char[] value)
//        {
//            Write(value.Length);
//            Write(value);
//        }

//        public void Write(DateTime dateTime)
//        {
//            Write(dateTime.Ticks);
//        }

//        public void Write(Decimal[] value)
//        {
//            Write(value.Length);
//            foreach (Decimal item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(Double[] value)
//        {
//            Write(value.Length);
//            foreach (Double item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(Int16[] value)
//        {
//            Write(value.Length);
//            foreach (Int16 item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(Int32[] value)
//        {
//            Write(value.Length);
//            foreach (Int32 item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(Int64[] value)
//        {
//            Write(value.Length);
//            foreach (Int64 item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(SByte[] value)
//        {
//            Write(value.Length);
//            foreach (SByte item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(Single[] value)
//        {
//            Write(value.Length);
//            foreach (Single item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(UInt16[] value)
//        {
//            Write(value.Length);
//            foreach (UInt16 item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(UInt32[] value)
//        {
//            Write(value.Length);
//            foreach (UInt32 item in value)
//            {
//                Write(item);
//            }
//        }

//        public void Write(UInt64[] value)
//        {
//            Write(value.Length);
//            foreach (UInt64 item in value)
//            {
//                Write(item);
//            }
//        }
//    }
//}
