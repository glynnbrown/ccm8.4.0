﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24658 : K.Pickup
//  Finished implementing.
// V8-25916 : N.Foster
//  Changed LockById to return success or failure
#endregion
#endregion

using System;
using System.IO;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Planograms.Dal.Pog.Resources;

namespace Galleria.Framework.Planograms.Dal.Pog
{
    public class DalContext : DalContextBase
    {
        #region Fields
        private DalFactory _dalFactory; // the DalFactory that created this DalContext.
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="dalFactoryConfig">The dal factory configuration element</param>
        public DalContext(DalFactory dalFactory, DalFactoryConfigElement dalFactoryConfig)
        {
            _dalFactory = dalFactory;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The dal cache, taken from the dal factory.  If LockDalCache has never been called then an exception will be 
        /// thrown.
        /// </summary>
        internal DalCache DalCache
        {
            get
            {
                if (_dalFactory.LockedDalCache == null)
                {
                    throw new InvalidOperationException(Language.Exception_DalCacheNotLocked);
                }
                return _dalFactory.LockedDalCache;
            }
        }

        /// <summary>
        /// Whether a transaction is currently in progress.
        /// </summary>
        internal Boolean TransactionInProgress { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Begins a transaction within this context
        /// </summary>
        public override void Begin()
        {
            lock (_dalFactory.TransactionLock)
            {
                if (_dalFactory.TransactionInProgress)
                {
                    throw new InvalidOperationException(Language.Exception_ExcessTransaction);
                }
                _dalFactory.TransactionInProgress = TransactionInProgress = true;
            }
        }

        /// <summary>
        /// Commits a transaction within this context
        /// </summary>
        public override void Commit()
        {
            lock (_dalFactory.TransactionLock)
            {
                if (!TransactionInProgress)
                {
                    throw new InvalidOperationException(Language.Exception_NoTransactionForCommit);
                }
                DalCache.Commit();
                _dalFactory.TransactionInProgress = TransactionInProgress = false;
            }
        }

        /// <summary>
        /// Locks the DalCache for this context's factory.  This is just a fancy way of saying that the underlying
        /// file is ours now, so no one else is going to be able to use it.
        /// </summary>
        public Boolean LockDalCache()
        {
            Boolean successful = true;
            if (_dalFactory.LockedDalCache == null)
            {
                try
                {
                    _dalFactory.LockedDalCache = new DalCache(_dalFactory);
                }
                catch (IOException)
                {
                    successful = false;
                }
            }
            return successful;
        }

        /// <summary>
        /// Unlocks the DalCache for this context's factory.
        /// </summary>
        public void UnlockDalCache()
        {
            if (_dalFactory.LockedDalCache != null)
            {
                _dalFactory.LockedDalCache.Dispose();
            }
            _dalFactory.LockedDalCache = null;
        }

        /// <summary>
        /// Called when disposing of this context
        /// </summary>
        public override void Dispose()
        {
            if (TransactionInProgress && (_dalFactory.LockedDalCache != null))
            {
                _dalFactory.LockedDalCache.Rollback();
            }
        }
        #endregion
    }
}
