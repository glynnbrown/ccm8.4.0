﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public class FilePart : IFilePart
    {
        public String FileName { get; set; }
        public Int32 FilePartNumber { get; set; }
        public Int32 TotalNumberOfParts { get; set; }
        public Byte[] FileData { get; set; }
        public String TimelineCode { get; set; }
    }
}
