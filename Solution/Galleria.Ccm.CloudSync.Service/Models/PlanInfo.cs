﻿using System;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public class PlanInfo : IPlanInfo
    {
        public String Ucr { get; set; }
        public String Name { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}