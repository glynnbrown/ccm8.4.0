﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models.LocationGraph
{
    public class LocationNode
    {
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String AdvertisingZone { get; set; }
        public Single? AverageOpeningHours { get; set; }
        public String CarParkManagement { get; set; }
        public Int16? CarParkSpaces { get; set; }
        public String City { get; set; }
        public String Code { get; set; }
        public String Country { get; set; }
        public String County { get; set; }
        public DateTime? DateClosed { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime? DateLastRefitted { get; set; }
        public DateTime? DateOpen { get; set; }
        public String DefaultClusterAttribute { get; set; }
        public String DistributionCentre { get; set; }
        public String DivisionalManagerEmail { get; set; }
        public String DivisionalManagerName { get; set; }
        public Int32 EntityId { get; set; }
        public String FaxAreaCode { get; set; }
        public String FaxCountryCode { get; set; }
        public String FaxNumber { get; set; }
        public Boolean? HasAlcohol { get; set; }
        public Boolean? HasAtmMachines { get; set; }
        public Boolean? HasBabyChanging { get; set; }
        public Boolean? HasButcher { get; set; }
        public Boolean? HasCafe { get; set; }
        public Boolean? HasCarServiceArea { get; set; }
        public Boolean? HasClinic { get; set; }
        public Boolean? HasCustomerWc { get; set; }
        public Boolean? HasDeli { get; set; }
        public Boolean? HasDryCleaning { get; set; }
        public Boolean? HasFashion { get; set; }
        public Boolean? HasFishMonger { get; set; }
        public Boolean? HasGardenCenter { get; set; }
        public Boolean? HasGrocery { get; set; }
        public Boolean? HasHomeShoppingAvailable { get; set; }
        public Boolean? HasHotFoodToGo { get; set; }
        public Boolean? HasInStoreBakery { get; set; }
        public Boolean? HasJewellery { get; set; }
        public Boolean? HasLottery { get; set; }
        public Boolean? HasMobilePhones { get; set; }
        public Boolean? HasMovieRental { get; set; }
        public Boolean? HasNewsCube { get; set; }
        public Boolean? HasOptician { get; set; }
        public Boolean? HasOrganic { get; set; }
        public Boolean? HasPetrolForecourt { get; set; }
        public Boolean? HasPharmacy { get; set; }
        public Boolean? HasPhotocopier { get; set; }
        public Boolean? HasPhotoDepartment { get; set; }
        public Boolean? HasPizza { get; set; }
        public Boolean? HasPostOffice { get; set; }
        public Boolean? HasRecycling { get; set; }
        public Boolean? HasRotisserie { get; set; }
        public Boolean? HasSaladBar { get; set; }
        public Boolean? HasTravel { get; set; }
        public Int16 Id { get; set; }
        public Boolean? Is24Hours { get; set; }
        public Boolean? IsFreeHold { get; set; }
        public Boolean? IsMezzFitted { get; set; }
        public Boolean? IsOpenFriday { get; set; }
        public Boolean? IsOpenMonday { get; set; }
        public Boolean? IsOpenSaturday { get; set; }
        public Boolean? IsOpenSunday { get; set; }
        public Boolean? IsOpenThursday { get; set; }
        public Boolean? IsOpenTuesday { get; set; }
        public Boolean? IsOpenWednesday { get; set; }
        public Single? Latitude { get; set; }
        public String LocationAttribute { get; set; }
        public Int32 LocationGroupId { get; set; }
        public Int32 LocationTypeId { get; set; }
        public Single? Longitude { get; set; }
        public String ManagerEmail { get; set; }
        public String ManagerName { get; set; }
        public String Name { get; set; }
        public Byte? NoOfCheckouts { get; set; }
        public String OpeningHours { get; set; }
        public String PetrolForecourtType { get; set; }
        public String PostalCode { get; set; }
        public String Region { get; set; }
        public String RegionalManagerEmail { get; set; }
        public String RegionalManagerName { get; set; }
        public String Restaurant { get; set; }
        public Int32? SizeGrossFloorArea { get; set; }
        public Int32? SizeMezzSalesArea { get; set; }
        public Int32? SizeNetSalesArea { get; set; }
        public String State { get; set; }
        public String TelephoneAreaCode { get; set; }
        public String TelephoneCountryCode { get; set; }
        public String TelephoneNumber { get; set; }
        public String TvRegion { get; set; }

    }
}
