﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public interface ILogFile
    {
        Stream stream { get; set; }
    }
}
