using System;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public class WebApiCredentials : IWebApiCredentials
    {
        public WebApiCredentials(String userName, String password)
        {
            UserName = userName;
            Password = password;
        }

        public String UserName { get; }

        public String Password { get; }
    }
}