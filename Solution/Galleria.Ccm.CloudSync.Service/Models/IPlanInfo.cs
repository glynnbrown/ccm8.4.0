﻿using System;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public interface IPlanInfo
    {
        String Ucr { get; set; }
        String Name { get; set; }
        DateTime DateUpdated { get; set; }
    }
}