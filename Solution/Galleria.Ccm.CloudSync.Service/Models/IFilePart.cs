﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public interface IFilePart
    {
        String FileName { get; set; }
        Int32 FilePartNumber { get; set; }
        Int32 TotalNumberOfParts { get; set; }
        Byte[] FileData { get; set; }
        String TimelineCode { get; set; }
    }
}
