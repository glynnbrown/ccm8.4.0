using System;
using System.Collections.Specialized;
using System.Configuration;
using Galleria.Ccm.CloudSync.Service.Security;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public class SyncSettings : ISyncSettings
    {
        /// <exception cref="InvalidOperationException">Unable to retrieve App Settings.</exception>
        public SyncSettings()
        {
            NameValueCollection settings;
            if (!TryGetAppSettings(out settings)) throw new InvalidOperationException("Unable to retrieve App Settings.");
            RepositoryServer = settings.Get(AppSettingsKeys.Server);
            WebApiEndPoint = settings[AppSettingsKeys.PublicApiServiceEndPoint];
            PollingInterval = 10;
            RepositoryName = settings.Get(AppSettingsKeys.Database);
            EntityId = Int32.Parse(settings.Get(AppSettingsKeys.EntityId));
            PerformanceDataFileLocation = settings[AppSettingsKeys.PerformanceFilesLocation];
            String webApiServiceKeyValue = settings[AppSettingsKeys.ServiceKey];
            Credentials = ObtainWebApiCredentials(webApiServiceKeyValue);
            ServiceStateRepositoryPath = settings[AppSettingsKeys.ServiceStateRepositoryPath];
        }

        public Double PollingInterval { get; }

        /// <summary>
        ///     Get the server that the repository is in.
        /// </summary>
        public String RepositoryServer { get; }

        /// <summary>
        ///     Get the name of the repository.
        /// </summary>
        public String RepositoryName { get; }

        public Int32 EntityId { get; }
        public String PerformanceDataFileLocation { get; }

        public String WebApiEndPoint { get; }
        /// <inheritdoc />
        public String ServiceStateRepositoryPath { get; }

        /// <summary>
        ///     Get the credentials stored in App.Settings.
        /// </summary>
        /// <value></value>
        /// <remarks>The credentials are stored encrypted.</remarks>
        public IWebApiCredentials Credentials { get; }

        /// <summary>
        ///     Safely get the App Settings, or null if unable to.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static Boolean TryGetAppSettings(out NameValueCollection settings)
        {
            try
            {
                settings = ConfigurationManager.AppSettings;
            }
            catch (ConfigurationErrorsException)
            {
                settings = null;
            }
            return settings != null;
        }

        private static IWebApiCredentials ObtainWebApiCredentials(String webApiServiceKeyValue)
        {
            if (String.IsNullOrWhiteSpace(webApiServiceKeyValue))
                throw new ArgumentNullException(nameof(webApiServiceKeyValue), $"No value for the key was found in the app settings.");

            String plainText = CryptoService.DecryptString(webApiServiceKeyValue, AppSettingsKeys.WebApiServiceKeyPassphrase);
            if (String.IsNullOrWhiteSpace(plainText)) throw new ArgumentNullException(nameof(webApiServiceKeyValue), $"Unable to decrypt the key found in the app settings.");

            String[] pair = plainText.Split(':');
            if (pair.Length != 2) throw new ArgumentNullException(nameof(webApiServiceKeyValue), $"Unable to recognize key in app settings.");

            return new WebApiCredentials(pair[0], pair[1]);
        }
    }
}