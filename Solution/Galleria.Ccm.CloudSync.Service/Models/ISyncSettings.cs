using System;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public interface ISyncSettings
    {
        /// <summary>
        /// Get the server that the repository is in.
        /// </summary>
        String RepositoryServer { get; }

        /// <summary>
        /// Get the name of the repository.
        /// </summary>
        String RepositoryName { get; }

        Double PollingInterval { get; }

        Int32 EntityId { get; }
        String PerformanceDataFileLocation { get; }
        IWebApiCredentials Credentials { get; }
        String WebApiEndPoint { get; }

        /// <summary>
        ///     The path to the file containing the Service State Repository.
        /// </summary>
        String ServiceStateRepositoryPath { get; }
    }
}