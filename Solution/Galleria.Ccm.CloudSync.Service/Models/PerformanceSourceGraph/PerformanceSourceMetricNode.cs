﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Interfaces;

namespace Galleria.Ccm.CloudSync.Service.Models.PerformanceSourceGraph
{
    public class PerformanceSourceMetricNode : IPerformanceSourceMetric
    {
        public Int32 Id { get; set; }
        public String MetricName { get; set; }
        public String ColumnNumber { get; set; }
        public Boolean IsDuplicateColumnMapping { get; set; }
        public String AggregationType { get; set; }
        public String MetricDescription { get; set; }
        public Boolean IsCalculatedMetric { get; set; }

    }
}
