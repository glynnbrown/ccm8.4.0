﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.CloudSync.Service.Models.PerformanceSourceGraph;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public class PerformanceSourceNode : IPerformanceSource
    {
        public Int32 Id { get; set; }
        public String EntityName { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String LocationLevelName { get; set; }
        public String ProductLevelName { get; set; }
        public String TimelineLevelName { get; set; }
        public String Type { get; set; }
        public List<IPerformanceSourceMetric> PerformanceSourceMetrics { get; set; }
        public DateTime? DateLastProcessed { get; set; }
        public String DataType { get; set; }
        public String DaysPerPeriod { get; set; }
    }
}
