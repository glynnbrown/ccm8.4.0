﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public class LogFile : ILogFile
    {
        public Stream stream { get; set; }

        public LogFile(Stream stream)
        {
            this.stream = stream;
        }
    }
}