using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramInventoryNode
    {
        public Object Id { get; set; }
        public Single MinDos { get; set; }
        public Single MinDeliveries { get; set; }
        public Single MinCasePacks { get; set; }
        public Boolean IsShelfLifeValidated { get; set; }
        public Boolean IsDosValidated { get; set; }
        public Boolean IsDeliveriesValidated { get; set; }
        public Boolean IsCasePacksValidated { get; set; }
        public PlanogramInventoryMetricType InventoryMetricType { get; set; }
        public Single DaysOfPerformance { get; set; }
        public Single MinShelfLife { get; set; }
    }
}