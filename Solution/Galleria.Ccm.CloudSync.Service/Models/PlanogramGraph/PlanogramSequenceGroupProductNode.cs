using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramSequenceGroupProductNode
    {
        public Object Id { get; set; }
        public String Gtin { get; set; }
        public Int32 SequenceNumber { get; set; }
        public Object PlanogramSequenceGroupSubGroupId { get; set; }
        public PlanogramProductNode Product { get; set; }
    }
}