using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramValidationTemplateGroupNode
    {
        public Object Id { get; set; }
        public String Name { get; set; }
        public Single Threshold1 { get; set; }
        public Single Threshold2 { get; set; }
        public PlanogramValidationTemplateResultType ResultType { get; set; }
        public IEnumerable<PlanogramValidationTemplateGroupMetricNode> Metrics { get; set; }
        public PlanogramValidationType ValidationType { get; set; }
    }
}