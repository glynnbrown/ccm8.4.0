using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentInventoryRuleNode
    {
        public Object Id { get; set; }
        public String ProductGtin { get; set; }
        public Single CasePack { get; set; }
        public Single DaysOfSupply { get; set; }
        public Single ShelfLife { get; set; }
        public Single ReplenishmentDays { get; set; }
        public Single WasteHurdleUnits { get; set; }
        public Int32 MinUnits { get; set; }
        public Single WasteHurdleCasePack { get; set; }
        public Int32 MinFacings { get; set; }
    }
}