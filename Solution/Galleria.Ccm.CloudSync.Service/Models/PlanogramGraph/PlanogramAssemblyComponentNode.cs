using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssemblyComponentNode
    {
        public Object Id { get; set; }
        public Single Angle { get; set; }
        public Int16? ComponentSequenceNumber { get; set; }
        public Single? MetaAverageCases { get; set; }
        public Single? MetaAverageDos { get; set; }
        public Int16? MetaAverageFacings { get; set; }
        public Single? MetaAverageFrontFacings { get; set; }
        public Int32? MetaAverageUnits { get; set; }
        public Int32? MetaBlocksDropped { get; set; }
        public Int32? MetaChangeFromPreviousStarRating { get; set; }
        public Int32? MetaChangesFromPreviousCount { get; set; }
        public Boolean? MetaIsComponentSlopeWithNoRiser { get; set; }
        public Boolean? MetaIsOutsideOfFixtureArea { get; set; }
        public Boolean? MetaIsOverMerchandisedDepth { get; set; }
        public Boolean? MetaIsOverMerchandisedHeight { get; set; }
        public Boolean? MetaIsOverMerchandisedWidth { get; set; }
        public Single? MetaMaxCases { get; set; }
        public Single? MetaMaxDos { get; set; }
        public Single? MetaMinCases { get; set; }
        public Single? MetaMinDos { get; set; }
        public Int32? MetaNewProducts { get; set; }
        public Int32? MetaNotAchievedInventory { get; set; }
        public Single? MetaPercentageLinearSpaceFilled { get; set; }
        public Int32? MetaProductsPlaced { get; set; }
        public Int16? MetaSpaceToUnitsIndex { get; set; }
        public Single? MetaTotalAreaWhiteSpace { get; set; }
        public Int32? MetaTotalComponentCollisions { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Int16? MetaTotalFrontFacings { get; set; }
        public Single? MetaTotalLinearWhiteSpace { get; set; }
        public Single? MetaTotalMerchandisableAreaSpace { get; set; }
        public Single? MetaTotalMerchandisableLinearSpace { get; set; }
        public Single? MetaTotalMerchandisableVolumetricSpace { get; set; }
        public Int32? MetaTotalPositionCollisions { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Single? MetaTotalVolumetricWhiteSpace { get; set; }
        public Single? MetaWorldAngle { get; set; }
        public Single? MetaWorldRoll { get; set; }
        public Single? MetaWorldSlope { get; set; }
        public Single? MetaWorldX { get; set; }
        public Single? MetaWorldY { get; set; }
        public Single? MetaWorldZ { get; set; }
        public Int32? NotchNumber { get; set; }
        public Object PlanogramComponentId { get; set; }
        public Single Roll { get; set; }
        public Single Slope { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
    }
}