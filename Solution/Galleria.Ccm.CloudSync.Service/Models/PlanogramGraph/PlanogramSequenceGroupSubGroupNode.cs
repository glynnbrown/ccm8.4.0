using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramSequenceGroupSubGroupNode
    {
        public Object Id { get; set; }
        public String Name { get; set; }
        public PlanogramSequenceGroupSubGroupAlignmentType Alignment { get; set; }
    }
}