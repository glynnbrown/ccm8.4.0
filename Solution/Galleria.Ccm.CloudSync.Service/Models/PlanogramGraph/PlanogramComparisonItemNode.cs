using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramComparisonItemNode
    {
        public Object Id { get; set; }
        public PlanogramItemType ItemType { get; set; }
        public PlanogramComparisonItemStatusType Status { get; set; }
        public IList<PlanogramComparisonFieldValueNode> FieldValues { get; set; }
    }
}