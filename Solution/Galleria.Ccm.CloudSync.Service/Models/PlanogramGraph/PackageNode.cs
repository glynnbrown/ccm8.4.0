using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PackageNode
    {
        public Object Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateMetadataCalculated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime? DateValidationDataCalculated { get; set; }
        public Object EntityId { get; set; }
        public String Name { get; set; }
        public PackageType PackageType { get; set; }
        public String UserName { get; set; }
        public Boolean IsReadOnly { get; set; }
        public Int32? MetaPlanogramCount { get; set; }
        public IList<PlanogramNode> Planograms { get; set; }
    }
}