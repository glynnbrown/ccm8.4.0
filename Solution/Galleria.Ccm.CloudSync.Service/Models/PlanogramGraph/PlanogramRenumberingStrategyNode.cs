using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramRenumberingStrategyNode
    {
        public Object Id { get; set; }
        public Boolean UniqueNumberMultiPositionProductsPerComponent { get; set; }
        public Boolean RestartPositionRenumberingPerComponent { get; set; }
        public Boolean RestartComponentRenumberingPerBay { get; set; }
        public Byte PositionZRenumberStrategyPriority { get; set; }
        public PlanogramRenumberingStrategyZRenumberType PositionZRenumberStrategy { get; set; }
        public Byte PositionYRenumberStrategyPriority { get; set; }
        public PlanogramRenumberingStrategyYRenumberType PositionYRenumberStrategy { get; set; }
        public Byte PositionXRenumberStrategyPriority { get; set; }
        public PlanogramRenumberingStrategyXRenumberType PositionXRenumberStrategy { get; set; }
        public String Name { get; set; }
        public Boolean IsEnabled { get; set; }
        public Boolean IgnoreNonMerchandisingComponents { get; set; }
        public Boolean ExceptAdjacentPositions { get; set; }
        public Byte BayComponentZRenumberStrategyPriority { get; set; }
        public PlanogramRenumberingStrategyZRenumberType BayComponentZRenumberStrategy { get; set; }
        public Byte BayComponentYRenumberStrategyPriority { get; set; }
        public PlanogramRenumberingStrategyYRenumberType BayComponentYRenumberStrategy { get; set; }
        public Byte BayComponentXRenumberStrategyPriority { get; set; }
    }
}