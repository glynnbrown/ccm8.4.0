using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramBlockingGroupNode
    {
        public Object Id { get; set; }
        public Boolean BlockPercentageIsChanged { get; set; }
        public PlanogramBlockingGroupPlacementType BlockPlacementPrimaryType { get; set; }
        public PlanogramBlockingGroupPlacementType BlockPlacementSecondaryType { get; set; }
        public PlanogramBlockingGroupPlacementType BlockPlacementTertiaryType { get; set; }
        public Single BlockSpaceLimitedPercentage { get; set; }
        public Boolean CanCompromiseSequence { get; set; }
        public Boolean CanMerge { get; set; }
        public Single? CanOptimisePercentage { get; set; }
        public Int32 Colour { get; set; }
        public PlanogramBlockingFillPatternType FillPatternType { get; set; }
        public Boolean IsLimited { get; set; }
        public Boolean IsRestrictedByComponentType { get; set; }
        public Single LimitedPercentage { get; set; }
        public Single? MetaAreaProductPlacementPercent { get; set; }
        public Int32? MetaCountOfProductPlacedOnPlanogram { get; set; }
        public Int32? MetaCountOfProductRecommendedOnPlanogram { get; set; }
        public Int32? MetaCountOfProducts { get; set; }
        public Single? MetaFinalPercentAllocated { get; set; }
        public Single? MetaLinearProductPlacementPercent { get; set; }
        public Single? MetaOriginalPercentAllocated { get; set; }
        public Single? MetaP1 { get; set; }
        public Single? MetaP10 { get; set; }
        public Single? MetaP10Percentage { get; set; }
        public Single? MetaP11 { get; set; }
        public Single? MetaP11Percentage { get; set; }
        public Single? MetaP12 { get; set; }
        public Single? MetaP12Percentage { get; set; }
        public Single? MetaP13 { get; set; }
        public Single? MetaP13Percentage { get; set; }
        public Single? MetaP14 { get; set; }
        public Single? MetaP14Percentage { get; set; }
        public Single? MetaP15 { get; set; }
        public Single? MetaP15Percentage { get; set; }
        public Single? MetaP16 { get; set; }
        public Single? MetaP16Percentage { get; set; }
        public Single? MetaP17 { get; set; }
        public Single? MetaP17Percentage { get; set; }
        public Single? MetaP18 { get; set; }
        public Single? MetaP18Percentage { get; set; }
        public Single? MetaP19 { get; set; }
        public Single? MetaP19Percentage { get; set; }
        public Single? MetaP1Percentage { get; set; }
        public Single? MetaP2 { get; set; }
        public Single? MetaP20 { get; set; }
        public Single? MetaP20Percentage { get; set; }
        public Single? MetaP2Percentage { get; set; }
        public Single? MetaP3 { get; set; }
        public Single? MetaP3Percentage { get; set; }
        public Single? MetaP4 { get; set; }
        public Single? MetaP4Percentage { get; set; }
        public Single? MetaP5 { get; set; }
        public Single? MetaP5Percentage { get; set; }
        public Single? MetaP6 { get; set; }
        public Single? MetaP6Percentage { get; set; }
        public Single? MetaP7 { get; set; }
        public Single? MetaP7Percentage { get; set; }
        public Single? MetaP8 { get; set; }
        public Single? MetaP8Percentage { get; set; }
        public Single? MetaP9 { get; set; }
        public Single? MetaP9Percentage { get; set; }
        public Single? MetaPerformancePercentAllocated { get; set; }
        public Single? MetaVolumetricProductPlacementPercent { get; set; }
        public String Name { get; set; }
        public Single TotalSpacePercentage { get; set; }
    }
}