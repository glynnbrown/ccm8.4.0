using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramProductNode
    {
        public Object Id { get; set; }
        public Single AlternateDepth { get; set; }
        public Single AlternateHeight { get; set; }
        public Single AlternateWidth { get; set; }
        public String Barcode { get; set; }
        public String Brand { get; set; }
        public Boolean CanBreakTrayBack { get; set; }
        public Boolean CanBreakTrayDown { get; set; }
        public Boolean CanBreakTrayTop { get; set; }
        public Boolean CanBreakTrayUp { get; set; }
        public Single? CaseCost { get; set; }
        public Byte CaseDeep { get; set; }
        public Single CaseDepth { get; set; }
        public Single CaseHeight { get; set; }
        public Byte CaseHigh { get; set; }
        public Int16 CasePackUnits { get; set; }
        public Byte CaseWide { get; set; }
        public Single CaseWidth { get; set; }
        public String Colour { get; set; }
        public String ColourGroupValue { get; set; }
        public String ConsumerInformation { get; set; }
        public String CorporateCode { get; set; }
        public Single? CostPrice { get; set; }
        public String CountryOfOrigin { get; set; }
        public String CountryOfProcessing { get; set; }
        public CustomAttributeDataNode CustomAttributes { get; set; }
        public String CustomerStatus { get; set; }
        public DateTime? DateDiscontinued { get; set; }
        public DateTime? DateEffective { get; set; }
        public DateTime? DateIntroduced { get; set; }
        public Single? DeliveryFrequencyDays { get; set; }
        public String DeliveryMethod { get; set; }
        public Single Depth { get; set; }
        public Single DisplayDepth { get; set; }
        public Single DisplayHeight { get; set; }
        public Single DisplayWidth { get; set; }
        public Int32 FillColour { get; set; }
        public PlanogramProductFillPatternType FillPatternType { get; set; }
        public String FinancialGroupCode { get; set; }
        public String FinancialGroupName { get; set; }
        public Single FingerSpaceAbove { get; set; }
        public Single FingerSpaceToTheSide { get; set; }
        public String Flavour { get; set; }
        public Boolean ForceBottomCap { get; set; }
        public Boolean ForceMiddleCap { get; set; }
        public Single FrontOverhang { get; set; }
        public String GarmentType { get; set; }
        public String Gtin { get; set; }
        public String Health { get; set; }
        public Single Height { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean IsFrontOnly { get; set; }
        public Boolean IsNewProduct { get; set; }
        public Boolean IsPlaceHolderProduct { get; set; }
        public Boolean IsPrivateLabel { get; set; }
        public String Manufacturer { get; set; }
        public String ManufacturerCode { get; set; }
        public Single? ManufacturerRecommendedRetailPrice { get; set; }
        public Byte MaxDeep { get; set; }
        public Byte MaxRightCap { get; set; }
        public Byte MaxStack { get; set; }
        public Byte MaxTopCap { get; set; }
        public PlanogramProductMerchandisingStyle MerchandisingStyle { get; set; }
        public String MetaCDTNode { get; set; }
        public PlanogramItemComparisonStatusType MetaComparisonStatus { get; set; }
        public Boolean? MetaIsBuddied { get; set; }
        public Boolean? MetaIsCoreRuleBroken { get; set; }
        public Boolean? MetaIsDelistFamilyRuleBroken { get; set; }
        public Boolean? MetaIsDelistProductRuleBroken { get; set; }
        public Boolean? MetaIsDependencyFamilyRuleBroken { get; set; }
        public Boolean? MetaIsDistributionRuleBroken { get; set; }
        public Boolean? MetaIsFamilyRuleBroken { get; set; }
        public Boolean? MetaIsForceProductRuleBroken { get; set; }
        public Boolean? MetaIsIllegalProduct { get; set; }
        public Boolean? MetaIsInheritanceRuleBroken { get; set; }
        public Boolean? MetaIsInMasterData { get; set; }
        public Boolean? MetaIsLocalProductRuleBroken { get; set; }
        public Boolean? MetaIsMaximumProductFamilyRuleBroken { get; set; }
        public Boolean? MetaIsMinimumHurdleProductRuleBroken { get; set; }
        public Boolean? MetaIsMinimumProductFamilyRuleBroken { get; set; }
        public Boolean? MetaIsNotLegalProduct { get; set; }
        public Boolean? MetaIsOverShelfLifePercent { get; set; }
        public Boolean? MetaIsPreserveProductRuleBroken { get; set; }
        public Boolean? MetaIsProductRuleBroken { get; set; }
        public Boolean? MetaIsRangedInAssortment { get; set; }
        public Boolean? MetaNotAchievedCases { get; set; }
        public Boolean? MetaNotAchievedDeliveries { get; set; }
        public Boolean? MetaNotAchievedDOS { get; set; }
        public Boolean? MetaNotAchievedInventory { get; set; }
        public Single? MetaPlanogramAreaSpacePercentage { get; set; }
        public Single? MetaPlanogramLinearSpacePercentage { get; set; }
        public Single? MetaPlanogramVolumetricSpacePercentage { get; set; }
        public Int32? MetaPositionCount { get; set; }
        public Single? MetaTotalAreaSpace { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Single? MetaTotalLinearSpace { get; set; }
        public Int32? MetaTotalMainFacings { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Single? MetaTotalVolumetricSpace { get; set; }
        public Int32? MetaTotalXFacings { get; set; }
        public Int32? MetaTotalYFacings { get; set; }
        public Int32? MetaTotalZFacings { get; set; }
        public Byte MinDeep { get; set; }
        public String Model { get; set; }
        public String Name { get; set; }
        public Single NestingDepth { get; set; }
        public Single NestingHeight { get; set; }
        public Single NestingWidth { get; set; }
        public Byte NumberOfPegHoles { get; set; }
        public PlanogramProductOrientationType OrientationType { get; set; }
        public String PackagingShape { get; set; }
        public String PackagingType { get; set; }
        public String Pattern { get; set; }
        public Single PegDepth { get; set; }
        public Single PegProngOffsetX { get; set; }
        public Single PegProngOffsetY { get; set; }
        public Single PegX { get; set; }
        public Single PegX2 { get; set; }
        public Single PegX3 { get; set; }
        public Single PegY { get; set; }
        public Single PegY2 { get; set; }
        public Single PegY3 { get; set; }
        public Object PlanogramImageIdAlternateBack { get; set; }
        public Object PlanogramImageIdAlternateBottom { get; set; }
        public Object PlanogramImageIdAlternateFront { get; set; }
        public Object PlanogramImageIdAlternateLeft { get; set; }
        public Object PlanogramImageIdAlternateRight { get; set; }
        public Object PlanogramImageIdAlternateTop { get; set; }
        public Object PlanogramImageIdBack { get; set; }
        public Object PlanogramImageIdBottom { get; set; }
        public Object PlanogramImageIdCaseBack { get; set; }
        public Object PlanogramImageIdCaseBottom { get; set; }
        public Object PlanogramImageIdCaseFront { get; set; }
        public Object PlanogramImageIdCaseLeft { get; set; }
        public Object PlanogramImageIdCaseRight { get; set; }
        public Object PlanogramImageIdCaseTop { get; set; }
        public Object PlanogramImageIdDisplayBack { get; set; }
        public Object PlanogramImageIdDisplayBottom { get; set; }
        public Object PlanogramImageIdDisplayFront { get; set; }
        public Object PlanogramImageIdDisplayLeft { get; set; }
        public Object PlanogramImageIdDisplayRight { get; set; }
        public Object PlanogramImageIdDisplayTop { get; set; }
        public Object PlanogramImageIdFront { get; set; }
        public Object PlanogramImageIdLeft { get; set; }
        public Object PlanogramImageIdPointOfPurchaseBack { get; set; }
        public Object PlanogramImageIdPointOfPurchaseBottom { get; set; }
        public Object PlanogramImageIdPointOfPurchaseFront { get; set; }
        public Object PlanogramImageIdPointOfPurchaseLeft { get; set; }
        public Object PlanogramImageIdPointOfPurchaseRight { get; set; }
        public Object PlanogramImageIdPointOfPurchaseTop { get; set; }
        public Object PlanogramImageIdRight { get; set; }
        public Object PlanogramImageIdTop { get; set; }
        public Object PlanogramImageIdTrayBack { get; set; }
        public Object PlanogramImageIdTrayBottom { get; set; }
        public Object PlanogramImageIdTrayFront { get; set; }
        public Object PlanogramImageIdTrayLeft { get; set; }
        public Object PlanogramImageIdTrayRight { get; set; }
        public Object PlanogramImageIdTrayTop { get; set; }
        public Single PointOfPurchaseDepth { get; set; }
        public String PointOfPurchaseDescription { get; set; }
        public Single PointOfPurchaseHeight { get; set; }
        public Single PointOfPurchaseWidth { get; set; }
        public Single? RecommendedRetailPrice { get; set; }
        public Int16? SellPackCount { get; set; }
        public String SellPackDescription { get; set; }
        public Single? SellPrice { get; set; }
        public String Shape { get; set; }
        public PlanogramProductShapeType ShapeType { get; set; }
        public Int16 ShelfLife { get; set; }
        public String ShortDescription { get; set; }
        public String Size { get; set; }
        public Single SqueezeDepth { get; set; }
        public Single SqueezeDepthActual { get; set; }
        public Single SqueezeHeight { get; set; }
        public Single SqueezeHeightActual { get; set; }
        public Single SqueezeWidth { get; set; }
        public Single SqueezeWidthActual { get; set; }
        public PlanogramProductStatusType StatusType { get; set; }
        public Int16? StyleNumber { get; set; }
        public String Subcategory { get; set; }
        public Single? TaxRate { get; set; }
        public String Texture { get; set; }
        public Byte TrayDeep { get; set; }
        public Single TrayDepth { get; set; }
        public Single TrayHeight { get; set; }
        public Byte TrayHigh { get; set; }
        public Int16 TrayPackUnits { get; set; }
        public Single TrayThickDepth { get; set; }
        public Single TrayThickHeight { get; set; }
        public Single TrayThickWidth { get; set; }
        public Byte TrayWide { get; set; }
        public Single TrayWidth { get; set; }
        public String UnitOfMeasure { get; set; }
        public String Vendor { get; set; }
        public String VendorCode { get; set; }
        public Single Width { get; set; }
    }
}