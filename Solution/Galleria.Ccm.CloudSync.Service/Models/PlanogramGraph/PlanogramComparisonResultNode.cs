using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramComparisonResultNode
    {
        public Object Id { get; set; }
        public String PlanogramName { get; set; }
        public PlanogramComparisonResultStatusType Status { get; set; }
        public IList<PlanogramComparisonItemNode> ComparedItems { get; set; }
    }
}