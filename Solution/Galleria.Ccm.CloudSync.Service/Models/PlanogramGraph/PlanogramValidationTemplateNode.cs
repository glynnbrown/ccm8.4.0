using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramValidationTemplateNode
    {
        public Object Id { get; set; }
        public String Name { get; set; }
        public IEnumerable<PlanogramValidationTemplateGroupNode> Groups { get; set; }
    }
}