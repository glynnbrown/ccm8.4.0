using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramBlockingLocationNode
    {
        public Object Id { get; set; }
        public Single Height { get; set; }
        public Object PlanogramBlockingDividerBottomId { get; set; }
        public Object PlanogramBlockingDividerLeftId { get; set; }
        public Object PlanogramBlockingDividerRightId { get; set; }
        public Object PlanogramBlockingDividerTopId { get; set; }
        public Object PlanogramBlockingGroupId { get; set; }
        public Single SpacePercentage { get; set; }
        public Single Width { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
    }
}