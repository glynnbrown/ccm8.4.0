using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAnnotationNode
    {
        public Object Id { get; set; }
        public PlanogramAnnotationType AnnotationType { get; set; }
        public Int32 BackgroundColour { get; set; }
        public Int32 BorderColour { get; set; }
        public Single BorderThickness { get; set; }
        public Boolean CanReduceFontToFit { get; set; }
        public Single Depth { get; set; }
        public Int32 FontColour { get; set; }
        public String FontName { get; set; }
        public Single FontSize { get; set; }
        public Single Height { get; set; }
        public Object PlanogramAssemblyComponentId { get; set; }
        public Object PlanogramFixtureAssemblyId { get; set; }
        public Object PlanogramFixtureComponentId { get; set; }
        public Object PlanogramFixtureItemId { get; set; }
        public Object PlanogramPositionId { get; set; }
        public Object PlanogramSubComponentId { get; set; }
        public String Text { get; set; }
        public Single Width { get; set; }
        public Single? X { get; set; }
        public Single? Y { get; set; }
        public Single? Z { get; set; }
    }
}