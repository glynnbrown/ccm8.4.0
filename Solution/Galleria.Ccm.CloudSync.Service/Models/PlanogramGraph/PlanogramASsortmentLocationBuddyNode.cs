using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentLocationBuddyNode
    {
        public Object Id { get; set; }
        public String LocationCode { get; set; }
        public PlanogramAssortmentLocationBuddyTreatmentType TreatmentType { get; set; }
        public String S1LocationCode { get; set; }
        public Single? S1Percentage { get; set; }
        public String S2LocationCode { get; set; }
        public Single? S2Percentage { get; set; }
        public String S3LocationCode { get; set; }
        public Single? S3Percentage { get; set; }
        public String S4LocationCode { get; set; }
        public Single? S4Percentage { get; set; }
    }
}