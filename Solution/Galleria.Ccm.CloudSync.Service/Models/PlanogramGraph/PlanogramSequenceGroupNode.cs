using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramSequenceGroupNode
    {
        public Object Id { get; set; }
        public Int32 Colour { get; set; }
        public IList<PlanogramSequenceGroupProductNode> Products { get; set; }
        public IList<PlanogramSequenceGroupSubGroupNode> SubGroups { get; set; }
    }
}