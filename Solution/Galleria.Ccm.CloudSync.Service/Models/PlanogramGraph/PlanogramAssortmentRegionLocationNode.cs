using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentRegionLocationNode
    {
        public Object Id { get; set; }
        public String LocationCode { get; set; }
    }
}