using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramComponentNode
    {
        public Object Id { get; set; }
        public String BarCode { get; set; }
        public Boolean CanAttachShelfEdgeLabel { get; set; }
        public Int16? Capacity { get; set; }
        public PlanogramComponentType ComponentType { get; set; }
        public CustomAttributeDataNode CustomAttributes { get; set; }
        public Single Depth { get; set; }
        public Single? Diameter { get; set; }
        public Single Height { get; set; }
        public Object ImageIdBack { get; set; }
        public Object ImageIdBottom { get; set; }
        public Object ImageIdFront { get; set; }
        public Object ImageIdLeft { get; set; }
        public Object ImageIdRight { get; set; }
        public Object ImageIdTop { get; set; }
        public Boolean IsDisplayOnly { get; set; }
        public Boolean IsMerchandisable { get; set; }
        public Boolean IsMerchandisedTopDown { get; set; }
        public Boolean IsMoveable { get; set; }
        public String Manufacturer { get; set; }
        public String ManufacturerPartName { get; set; }
        public String ManufacturerPartNumber { get; set; }
        public Object Mesh3DId { get; set; }
        public Int16? MetaNumberOfMerchandisedSubComponents { get; set; }
        public Int16? MetaNumberOfSubComponents { get; set; }
        public Int32? MinPurchaseQty { get; set; }
        public String Name { get; set; }
        public String RetailerReferenceCode { get; set; }
        public IList<PlanogramSubComponentNode> SubComponents { get; set; }
        public Single? SupplierCostPrice { get; set; }
        public Single? SupplierDiscount { get; set; }
        public Single? SupplierLeadTime { get; set; }
        public String SupplierName { get; set; }
        public String SupplierPartNumber { get; set; }
        public Single? Volume { get; set; }
        public Single? Weight { get; set; }
        public Single? WeightLimit { get; set; }
        public Single Width { get; set; }
    }
}