using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramPerformanceMetricNode
    {
        public Object Id { get; set; }
        public Object Type { get; set; }
        public Object SpecialType { get; set; }
        public Object Name { get; set; }
        public Object MetricId { get; set; }
        public Object Direction { get; set; }
        public Object Description { get; set; }
        public Object AggregationType { get; set; }
    }
}