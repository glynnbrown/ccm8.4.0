using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentLocalProductNode
    {
        public Object Id { get; set; }
        public String LocationCode { get; set; }
        public String ProductGtin { get; set; }
    }
}