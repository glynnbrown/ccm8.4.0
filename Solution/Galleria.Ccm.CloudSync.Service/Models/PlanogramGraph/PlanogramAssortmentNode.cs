using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentNode
    {
        public Object Id { get; set; }
        public IList<PlanogramAssortmentInventoryRuleNode> InventoryRules { get; set; }
        public IList<PlanogramAssortmentLocalProductNode> LocalProducts { get; set; }
        public IList<PlanogramAssortmentLocationBuddyNode> LocationBuddies { get; set; }
        public String Name { get; set; }
        public IList<PlanogramAssortmentProductNode> Products { get; set; }
        public IList<PlanogramAssortmentProductBuddyNode> ProductBuddies { get; set; }
        public IList<PlanogramAssortmentRegionNode> Regions { get; set; }
    }
}