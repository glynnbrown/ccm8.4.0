using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssemblyNode
    {
        public Object Id { get; set; }
        public String Name { get; set; }
        public IList<PlanogramAssemblyComponentNode> Components { get; set; }
        public Single Depth { get; set; }
        public Single Height { get; set; }
        public Int16 NumberOfComponents { get; set; }
        public Single TotalComponentCost { get; set; }
    }
}