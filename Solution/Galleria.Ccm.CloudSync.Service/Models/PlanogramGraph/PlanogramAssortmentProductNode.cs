using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentProductNode
    {
        public Object Id { get; set; }
        public String Gtin { get; set; }
        public String Name { get; set; }
        public Boolean IsRanged { get; set; }
        public Int16 Rank { get; set; }
        public String Segmentation { get; set; }
        public Byte Facings { get; set; }
        public Int16 Units { get; set; }
        public PlanogramAssortmentProductTreatmentType ProductTreatmentType { get; set; }
        public PlanogramAssortmentProductLocalizationType ProductLocalizationType { get; set; }
        public String Comments { get; set; }
        public Byte? ExactListFacings { get; set; }
        public Int16? ExactListUnits { get; set; }
        public Byte? PreserveListFacings { get; set; }
        public Int16? PreserveListUnits { get; set; }
        public Int16? MaxListUnits { get; set; }
        public Byte? MinListFacings { get; set; }
        public Byte? MaxListFacings { get; set; }
        public Int16? MinListUnits { get; set; }
        public String FamilyRuleName { get; set; }
        public PlanogramAssortmentProductFamilyRuleType FamilyRuleType { get; set; }
        public Byte? FamilyRuleValue { get; set; }
        public Boolean IsPrimaryRegionalProduct { get; set; }
        public Boolean? MetaIsProductPlacedOnPlanogram { get; set; }
        public Boolean DelistedDueToAssortmentRule { get; set; }
        public Int16? RuleMaximumValue { get; set; }
        public PlanogramAssortmentProductRuleValueType RuleMaximumValueType { get; set; }
        public PlanogramAssortmentProductRuleType RuleType { get; set; }
        public Int16? RuleValue { get; set; }
        public PlanogramAssortmentProductRuleValueType RuleValueType { get; set; }
        public Boolean HasMaximum { get; set; }
        public String RuleSummary { get; set; }
    }
}