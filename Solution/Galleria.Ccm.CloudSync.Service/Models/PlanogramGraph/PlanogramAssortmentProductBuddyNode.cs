using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentProductBuddyNode
    {
        public Object Id { get; set; }
        public String ProductGtin { get; set; }
        public PlanogramAssortmentProductBuddySourceType SourceType { get; set; }
        public PlanogramAssortmentProductBuddyTreatmentType TreatmentType { get; set; }
        public Single TreatmentTypePercentage { get; set; }
        public PlanogramAssortmentProductBuddyProductAttributeType ProductAttributeType { get; set; }
        public Single? S1Percentage { get; set; }
        public String S1ProductGtin { get; set; }
        public Single? S2Percentage { get; set; }
        public String S2ProductGtin { get; set; }
        public Single? S3Percentage { get; set; }
        public String S3ProductGtin { get; set; }
        public String S4ProductGtin { get; set; }
        public Single? S4Percentage { get; set; }
        public Single? S5Percentage { get; set; }
        public String S5ProductGtin { get; set; }
    }
}