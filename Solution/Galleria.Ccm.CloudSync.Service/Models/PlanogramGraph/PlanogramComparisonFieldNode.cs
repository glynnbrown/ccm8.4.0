using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramComparisonFieldNode
    {
        public Object Id { get; set; }
        public PlanogramItemType ItemType { get; set; }
        public String FieldPlaceholder { get; set; }
        public String DisplayName { get; set; }
        public Int16 Number { get; set; }
        public Boolean Display { get; set; }
    }
}