using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramBlockingNode
    {
        public Object Id { get; set; }
        public IList<PlanogramBlockingDividerNode> Dividers { get; set; }
        public IList<PlanogramBlockingGroupNode> Groups { get; set; }
        public IList<PlanogramBlockingLocationNode> Locations { get; set; }
        public PlanogramBlockingType Type { get; set; }
    }
}