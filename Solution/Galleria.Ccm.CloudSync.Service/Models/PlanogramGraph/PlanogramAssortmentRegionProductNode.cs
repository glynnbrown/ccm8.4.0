using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentRegionProductNode
    {
        public Object Id { get; set; }
        public String PrimaryProductGtin { get; set; }
        public String RegionalProductGtin { get; set; }
    }
}