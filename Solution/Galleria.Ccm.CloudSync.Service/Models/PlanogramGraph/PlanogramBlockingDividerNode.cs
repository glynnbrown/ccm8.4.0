using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramBlockingDividerNode
    {
        public Object Id { get; set; }
        public Boolean IsLimited { get; set; }
        public Boolean IsSnapped { get; set; }
        public Single Length { get; set; }
        public Byte Level { get; set; }
        public Single LimitedPercentage { get; set; }
        public PlanogramBlockingDividerType Type { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
    }
}