using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramSequenceProductLinkNode
    {
        public Object AnchorSequenceGroupProductId { get; set; }
        public Object TargetSequenceGroupProductId { get; set; }
        public PlanogramSequenceProductLinkDirectionType Direction { get; set; }
    }
}