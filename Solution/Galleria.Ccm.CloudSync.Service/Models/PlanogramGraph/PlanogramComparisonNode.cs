using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramComparisonNode
    {
        public Object Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public DateTime? DateLastCompared { get; set; }
        public DataOrderType DataOrderType { get; set; }
        public Boolean IgnoreNonPlacedProducts { get; set; }
        public IList<PlanogramComparisonFieldNode> ComparisonFields { get; set; }
        public IList<PlanogramComparisonResultNode> Results { get; set; }
    }
}