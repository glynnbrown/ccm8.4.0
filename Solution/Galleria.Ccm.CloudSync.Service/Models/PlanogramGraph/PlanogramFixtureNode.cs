using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramFixtureNode
    {
        public Object Id { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public IList<PlanogramFixtureAssemblyNode> Assemblies { get; set; }
        public IList<PlanogramFixtureComponentNode> Components { get; set; }
        public Int16? MetaNumberOfMerchandisedSubComponents { get; set; }
        public Single? MetaTotalFixtureCost { get; set; }
    }
}