using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramFixtureAssemblyNode
    {
        public Object Id { get; set; }
        public Object PlanogramAssemblyId { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
        public Single Slope { get; set; }
        public Single Angle { get; set; }
        public Single Roll { get; set; }
        public Int32? MetaComponentCount { get; set; }
        public Single? MetaTotalMerchandisableLinearSpace { get; set; }
        public Single? MetaTotalMerchandisableAreaSpace { get; set; }
        public Single? MetaTotalMerchandisableVolumetricSpace { get; set; }
        public Single? MetaTotalLinearWhiteSpace { get; set; }
        public Single? MetaTotalVolumetricWhiteSpace { get; set; }
        public Int32? MetaProductsPlaced { get; set; }
        public Int32? MetaNewProducts { get; set; }
        public Int32? MetaChangesFromPreviousCount { get; set; }
        public Int32? MetaChangeFromPreviousStarRating { get; set; }
        public Int32? MetaBlocksDropped { get; set; }
        public Int32? MetaNotAchievedInventory { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Int16? MetaAverageFacings { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Int32? MetaAverageUnits { get; set; }
        public Single? MetaMinDos { get; set; }
        public Single? MetaMaxDos { get; set; }
        public Single? MetaAverageDos { get; set; }
        public Single? MetaMinCases { get; set; }
        public Single? MetaAverageCases { get; set; }
        public Single? MetaMaxCases { get; set; }
        public Int16? MetaSpaceToUnitsIndex { get; set; }
        public Int16? MetaTotalFrontFacings { get; set; }
        public Single? MetaAverageFrontFacings { get; set; }
    }
}