using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramValidationTemplateGroupMetricNode
    {
        public Object Id { get; set; }
        public String Field { get; set; }
        public Single Threshold1 { get; set; }
        public Single Threshold2 { get; set; }
        public Single Score1 { get; set; }
        public Single Score2 { get; set; }
        public Single Score3 { get; set; }
        public PlanogramValidationAggregationType AggregationType { get; set; }
        public PlanogramValidationTemplateResultType ResultType { get; set; }
        public PlanogramValidationType ValidationType { get; set; }
        public String Criteria { get; set; }
    }
}