using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramSequenceNode
    {
        public Object Id { get; set; }
        public IList<PlanogramSequenceProductLinkNode> ProductLinks { get; set; }
        public IList<PlanogramSequenceGroupNode> Groups { get; set; }
    }
}