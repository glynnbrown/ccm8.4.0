using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramComparisonFieldValueNode
    {
        public Object Id { get; set; }
        public String FieldPlaceholder { get; set; }
        public String Value { get; set; }
    }
}