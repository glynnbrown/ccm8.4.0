using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramFixtureItemNode
    {
        public Object Id { get; set; }
        public Single? MetaTotalAreaWhiteSpace { get; set; }
        public Single Angle { get; set; }
        public Int16 BaySequenceNumber { get; set; }
        public Single? MetaAverageCases { get; set; }
        public Single? MetaAverageDos { get; set; }
        public Int16? MetaAverageFacings { get; set; }
        public Int32? MetaAverageUnits { get; set; }
        public Single? MetaAverageFrontFacings { get; set; }
        public Int32? MetaBlocksDropped { get; set; }
        public Int32? MetaChangeFromPreviousStarRating { get; set; }
        public Int32? MetaChangesFromPreviousCount { get; set; }
        public Int32? MetaComponentCount { get; set; }
        public Single? MetaMaxCases { get; set; }
        public Single? MetaMaxDos { get; set; }
        public Single? MetaMinCases { get; set; }
        public Single? MetaMinDos { get; set; }
        public Int32? MetaNewProducts { get; set; }
        public Int32? MetaNotAchievedInventory { get; set; }
        public Int32? MetaProductsPlaced { get; set; }
        public Int16? MetaSpaceToUnitsIndex { get; set; }
        public Int32? MetaTotalComponentCollisions { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedDepth { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedHeight { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedWidth { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Int16? MetaTotalFrontFacings { get; set; }
        public Single? MetaTotalLinearWhiteSpace { get; set; }
        public Single? MetaTotalMerchandisableAreaSpace { get; set; }
        public Single? MetaTotalMerchandisableLinearSpace { get; set; }
        public Single? MetaTotalMerchandisableVolumetricSpace { get; set; }
        public Int32? MetaTotalPositionCollisions { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Single PercentageWidth { get; set; }
        public Single PercentageX { get; set; }
        public Object PlanogramFixtureId { get; set; }
        public Single Roll { get; set; }
        public Single Slope { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
    }
}