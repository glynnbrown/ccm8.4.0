using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramAssortmentRegionNode
    {
        public Object Id { get; set; }
        public String Name { get; set; }
        public IList<PlanogramAssortmentRegionLocationNode> Locations { get; set; }
        public IList<PlanogramAssortmentRegionProductNode> Products { get; set; }
    }
}