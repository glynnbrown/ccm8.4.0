using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramEventLogNode
    {
        public Object Id { get; set; }
        public Object AffectedId { get; set; }
        public PlanogramEventLogAffectedType? AffectedType { get; set; }
        public String Content { get; set; }
        public DateTime DateTime { get; set; }
        public String Description { get; set; }
        public PlanogramEventLogEntryType EntryType { get; set; }
        public Int32 EventId { get; set; }
        public PlanogramEventLogEventType EventType { get; set; }
        public Byte Score { get; set; }
        public String TaskSource { get; set; }
        public String WorkpackageSource { get; set; }
    }
}