using System;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramMetaDataImageNode
    {
        public Object Id { get; set; }
        public String FileName { get; set; }
        public String Description { get; set; }
        public Byte[] ImageData { get; set; }
    }
}