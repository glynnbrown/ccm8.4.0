using System;
using System.Collections.Generic;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph
{
    public class PlanogramPerformanceNode
    {
        public Object Id { get; set; }
        public IList<PlanogramPerformanceDataNode> PerformanceData { get; set; }
        public String Name { get; set; }
        public IList<PlanogramPerformanceMetricNode> Metrics { get; set; }
        public String Description { get; set; }
    }
}