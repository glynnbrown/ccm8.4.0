﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models.ProductHierarchyGraph
{
    class ProductGroupNode
    {
        public Int32 Id { get; set; }
        public Int32 ProductHierarchyId { get; set; }
        public Int32 ProductLevelId { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public Int32? ParentId { get; set; }
        public Int64 AssignedProductsCount { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        public Byte CategoryType { get; set; }
        public String CustomAttribute01 { get; set; }
        public String CustomAttribute02 { get; set; }
        public String CustomAttribute03 { get; set; }
        public String CustomAttribute04 { get; set; }
        public String CustomAttribute05 { get; set; }
        public IList<ProductGroupNode> ChildList { get; set; }
        public ProductHierarchyNode ProductHierarchyNode { get; set; }
    }
}
