﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models.ProductHierarchyGraph
{
    class ProductLevelNode
    {
        public Int32 Id { get; set; }
        public Int32 ProductHierarchyId { get; set; }
        public String Name { get; set; }
        public Byte ShapeNo { get; set; }
        public Int32 Colour { get; set; }
        public Int32? ParentLevelId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public ProductLevelNode ChildLevel { get; set; }
    }
}
