﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models.ProductHierarchyGraph
{
    class ProductHierarchyNode
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public ProductGroupNode RootGroup { get; set; }
        public ProductLevelNode RootLevel { get; set; }
    }
}
