using System;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public interface IWebApiCredentials
    {
        String UserName { get; }
        String Password { get; }
    }
}