﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramHierarchyGraph
{
    public class PlanogramHierarchyNode
    {
        public Int32 Id { get; set; }
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public PlanogramGroupNode RootGroup { get; set; }
    }
}
