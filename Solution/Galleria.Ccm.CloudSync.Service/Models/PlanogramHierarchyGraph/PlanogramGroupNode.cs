﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Models.PlanogramHierarchyGraph
{
    public class PlanogramGroupNode
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Int32? ProductGroupId { get; set; }
        public IList<PlanogramGroupNode> ChildList { get; set; }
        public Int32? ParentGroupNodeId { get; set; }
        public Guid Code { get; set; }
    }
}
