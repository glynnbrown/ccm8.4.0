﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Galleria.Ccm.CloudSync.Service.Models
{
    public interface ILogFileManager
    {
        IList<ILogFile> FetchLogFiles();
        Boolean DoesLogFileExist(String path);
        Boolean DeleteLogFiles();
    }
}
