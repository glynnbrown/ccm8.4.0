﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.CloudSync.Service.Models.LocationGraph;
using Galleria.Ccm.CloudSync.Service.Models.ProductHierarchyGraph;
using Newtonsoft.Json;

namespace Galleria.Ccm.CloudSync.Service
{
    public class LocationSerializer : ILocationSerializer
    {
        public String ToJson(List<Services.Location.Location> source)
        {
            return JsonConvert.SerializeObject(source.Select(AsGraphNode).ToList());
        }

        private static LocationNode AsGraphNode(Services.Location.Location source)
        {
            return new LocationNode
            {
                Address1 = source.Address1,
                Address2 = source.Address2,
                AdvertisingZone = source.AdvertisingZone,
                AverageOpeningHours = source.AverageOpeningHours,
                CarParkManagement = source.CarParkManagement,
                CarParkSpaces = source.CarParkSpaces,
                City = source.City,
                Code = source.Code,
                Country = source.Country,
                County = source.County,
                DateClosed = source.DateClosed,
                DateDeleted = source.DateDeleted,
                DateLastRefitted = source.DateLastRefitted,
                DateOpen = source.DateOpen,
                DefaultClusterAttribute = source.DefaultClusterAttribute,
                DistributionCentre = source.DistributionCentre,
                DivisionalManagerEmail = source.DivisionalManagerEmail,
                DivisionalManagerName = source.DivisionalManagerName,
                EntityId = source.EntityId,
                FaxAreaCode = source.FaxAreaCode,
                FaxCountryCode = source.FaxCountryCode,
                FaxNumber = source.FaxNumber,
                HasAlcohol = source.HasAlcohol,
                HasAtmMachines = source.HasAtmMachines,
                HasBabyChanging = source.HasBabyChanging,
                HasButcher = source.HasButcher,
                HasCafe = source.HasCafe,
                HasCarServiceArea = source.HasCarServiceArea,
                HasClinic = source.HasClinic,
                HasCustomerWc = source.HasCustomerWC,
                HasDeli = source.HasDeli,
                HasDryCleaning = source.HasDryCleaning,
                HasFashion = source.HasFashion,
                HasFishMonger = source.HasFishMonger,
                HasGardenCenter = source.HasGardenCenter,
                HasGrocery = source.HasGrocery,
                HasHomeShoppingAvailable = source.HasHomeShoppingAvailable,
                HasHotFoodToGo = source.HasHotFoodToGo,
                HasInStoreBakery = source.HasInStoreBakery,
                HasJewellery = source.HasJewellery,
                HasLottery = source.HasLottery,
                HasMobilePhones = source.HasMobilePhones,
                HasMovieRental = source.HasMovieRental,
                HasNewsCube = source.HasNewsCube,
                HasOptician = source.HasOptician,
                HasOrganic = source.HasOrganic,
                HasPetrolForecourt = source.HasPetrolForecourt,
                HasPharmacy = source.HasPharmacy,
                HasPhotocopier = source.HasPhotocopier,
                HasPhotoDepartment = source.HasPhotoDepartment,
                HasPizza = source.HasPizza,
                HasPostOffice = source.HasPostOffice,
                HasRecycling = source.HasRecycling,
                HasRotisserie = source.HasRotisserie,
                HasSaladBar = source.HasSaladBar,
                HasTravel = source.HasTravel,
                Id = source.Id,
                Is24Hours = source.Is24Hours,
                IsFreeHold = source.IsFreeHold,
                IsMezzFitted = source.IsMezzFitted,
                IsOpenFriday = source.IsOpenFriday,
                IsOpenMonday = source.IsOpenMonday,
                IsOpenSaturday = source.IsOpenSaturday,
                IsOpenSunday = source.IsOpenSunday,
                IsOpenThursday = source.IsOpenThursday,
                IsOpenTuesday = source.IsOpenTuesday,
                IsOpenWednesday = source.IsOpenWednesday,
                Latitude = source.Latitude,
                LocationAttribute = source.LocationAttribute,
                LocationGroupId = source.LocationGroupId,
                LocationTypeId = source.LocationTypeId,
                Longitude = source.Longitude,
                ManagerEmail = source.ManagerEmail,
                ManagerName = source.ManagerName,
                Name = source.Name,
                NoOfCheckouts = source.NoOfCheckouts,
                OpeningHours = source.OpeningHours,
                PetrolForecourtType = source.PetrolForecourtType,
                PostalCode = source.PostalCode,
                Region = source.Region,
                RegionalManagerEmail = source.RegionalManagerEmail,
                RegionalManagerName = source.RegionalManagerName,
                Restaurant = source.Restaurant,
                SizeGrossFloorArea = source.SizeGrossFloorArea,
                SizeMezzSalesArea = source.SizeMezzSalesArea,
                SizeNetSalesArea = source.SizeNetSalesArea,
                State = source.State,
                TelephoneAreaCode = source.TelephoneAreaCode,
                TelephoneCountryCode = source.TelephoneCountryCode,
                TelephoneNumber = source.TelephoneNumber,
                TvRegion = source.TvRegion
            };
        }
    }
}
