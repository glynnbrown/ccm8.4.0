﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service
{
    public interface ILocationSerializer
    {
        String ToJson(List<Services.Location.Location> source);
    }
}
