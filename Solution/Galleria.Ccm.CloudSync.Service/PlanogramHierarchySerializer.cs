﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Newtonsoft.Json;
using Galleria.Ccm.CloudSync.Service.Models.PlanogramHierarchyGraph;

namespace Galleria.Ccm.CloudSync.Service
{
    public class PlanogramHierarchySerializer : IPlanogramHierarchySerializer
    {
        public string ToJson(PlanogramHierarchy source)
        {
            return JsonConvert.SerializeObject(AsGraphNode(source));
        }

        private static PlanogramHierarchyNode AsGraphNode(PlanogramHierarchy source)
        {
            return new PlanogramHierarchyNode
            {
                Id = source.Id,
                EntityId = source.EntityId,
                Name = source.Name,
                DateCreated = source.DateCreated,
                DateLastModified = source.DateLastModified,
                RootGroup = AsGraphNode(source.RootGroup)
            };
        }

        private static PlanogramGroupNode AsGraphNode(PlanogramGroup source)
        {
            return new PlanogramGroupNode
            {
                Id = source.Id,
                Name = source.Name,
                DateCreated = source.DateCreated,
                DateLastModified = source.DateLastModified,
                ProductGroupId = source.ProductGroupId,
                ParentGroupNodeId = source.ParentGroup?.Id,
                ChildList = source.ChildList.Select(AsGraphNode).ToList(),
                Code = source.Code
            };
        }
    }
}
