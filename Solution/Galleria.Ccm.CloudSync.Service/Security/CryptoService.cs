using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service.Security
{
    internal static class CryptoService
    {
        private const Int32 KeySize = 256;
        private const Int32 Iterations = 1000;

        public static String EncryptString(String source, String key)
        {
            Byte[] salt = Generate256RandomBits();
            Byte[] iv = Generate256RandomBits();
            Byte[] plainText = Encoding.UTF8.GetBytes(source);

            using (var derivedKey = new Rfc2898DeriveBytes(key, salt, Iterations))
            {
                Byte[] keyBytes = derivedKey.GetBytes(KeySize / 8);
                using (var symmetricKey = new RijndaelManaged { BlockSize = KeySize, Mode = CipherMode.CBC, Padding = PaddingMode.PKCS7 })
                {
                    using (ICryptoTransform decryptor = symmetricKey.CreateEncryptor(keyBytes, iv))
                    using (var memoryStream = new MemoryStream())
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        Byte[] cypherBytes = salt;
                        cypherBytes = cypherBytes.Concat(iv).ToArray();
                        cypherBytes = cypherBytes.Concat(memoryStream.ToArray()).ToArray();
                        memoryStream.Close();
                        cryptoStream.Close();
                        return Convert.ToBase64String(cypherBytes);
                    }
                }
            }
        }

        public static String DecryptString(String source, String key)
        {
            Byte[] decodedSource = Convert.FromBase64String(source);
            Byte[] salt = decodedSource.Take(KeySize / 8).ToArray();
            Byte[] iv = decodedSource.Skip(KeySize/8).Take(KeySize/8).ToArray();
            Byte[] cypher = decodedSource.Skip((KeySize/8)*2).Take(source.Length - ((KeySize/8)*2)).ToArray();

            using (var derivedKey = new Rfc2898DeriveBytes(key, salt, Iterations))
            {
                Byte[] keyBytes = derivedKey.GetBytes(KeySize/8);
                using (var symmetricKey = new RijndaelManaged {BlockSize = KeySize, Mode = CipherMode.CBC, Padding = PaddingMode.PKCS7})
                {
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, iv))
                    using (var memoryStream = new MemoryStream(cypher))
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        Int32 cypherLength = cypher.Length;
                        var buffer = new Byte[cypherLength];
                        Int32 decryptedByteCount = cryptoStream.Read(buffer, 0, cypherLength);
                        memoryStream.Close();
                        cryptoStream.Close();
                        return Encoding.UTF8.GetString(buffer, 0, decryptedByteCount);
                    }
                }
            }
        }

        private static Byte[] Generate256RandomBits()
        {
            var randomBytes = new Byte[32];
            using (var random = new RNGCryptoServiceProvider())
                random.GetBytes(randomBytes);
            return randomBytes;
        }
    }
}