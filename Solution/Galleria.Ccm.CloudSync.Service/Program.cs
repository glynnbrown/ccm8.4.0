﻿using System;
using System.Collections.Specialized;
using Topshelf;
using System.Diagnostics;
using System.Configuration;
using Galleria.Ccm.CloudSync.Service.Clients;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Ccm.CloudSync.Service.Providers;
using Topshelf.Logging;
using static Galleria.Ccm.CloudSync.Service.AppSettingsKeys;

namespace Galleria.Ccm.CloudSync.Service
{
    public static class Program
    {
        static Program()
        {
            ConfigureDebugSettings();
        }

        [Conditional("DEBUG")]
        private static void ConfigureDebugSettings()
        {
            NameValueCollection settings = ConfigurationManager.AppSettings;

            #region LocalDebugSettings

            //settings.Set(
            //    PublicApiService.EndPoint, 
            //    "http://localhost:9000/");
            //settings.Set(
            //    PublicApiService.ServiceKey,
            //    "7j6LYoQaHKmyOKT6qUTHF1J85xSARAy+Ro82vvM+PYVVOpsrHWtdz/61eaDH7D97N7ixorhMWICceZGzIe4TnvT4MyOWZaSpszOYMa+wv1PVMhMwIiGxn/e0umgSBOMp");
            //settings.Set(
            //    Ccm8DataSync.Server, 
            //    @".\DevSql");
            //settings.Set(
            //    Ccm8DataSync.Database, 
            //    "DevCcmDb");
            //settings.Set(
            //    Ccm8DataSync.EntityId, 
            //    "1");
            //settings.Set(
            //    PerformanceDataSync.PerformanceFilesLocation, 
            //    @"C:\Performance");
            //settings.Set(
            //    PerformanceDataSync.StagingLocation, 
            //    @"C:\PerformanceStaging");
            //settings.Set(
            //    GfsApiService.EndPoint,
            //    @"localhost:60099");

            #endregion

            #region RemoteDebugSettings

            settings.Set(
                PublicApiServiceEndPoint, 
                //"https://dev.galleria-rts.com/");
                "http://localhost:9000/");
            settings.Set(
                ServiceKey,
                "TNxZTwSDuVQJtNfLFhktCAtKdygD1AW6WgxSkPuZkFuPLWMYsI9ysfBCTfM6gH1/AOuZFTN4DJojkLzcVberX0XF2/cvUh5D/G1VQA1OSU0hLQfiYYqyxIVf35h5LfeC");
            settings.Set(
                Server, 
                @"gal-sql-003\qasql2012");
            settings.Set(
                Database, 
                "PNS_CCM_840_B227");
            settings.Set(
                EntityId, 
                "2");
            settings.Set(
                PerformanceFilesLocation,
                @"C:\Performance");
            settings.Set(
                StagingLocation, 
                @"C:\PerformanceStaging");
            settings.Set(
                GfsApiServiceEndPoint, 
                @"QA-Web-Dev:80/PNS_GFS_230_B173");

            #endregion
        }

        public static void Main(params String[] args)
        {
            HostFactory.Run(serviceConfig =>
            {
                serviceConfig.UseLog4Net("log4net.config");
                serviceConfig.SetDisplayName("CCM Cloud Sync Service");
                serviceConfig.Service(serviceInstance => CreateNewService());
            });
        }

        /// <summary>
        ///     Create a new service with the necessary settings.
        /// </summary>
        /// <returns></returns>
        private static ServiceControl CreateNewService()
        {
            ISyncSettings syncSettings = new SyncSettings();
            LogWriter logWriter = HostLogger.Get<SyncService>();
            IWebApiClient webApiClient = new Net4WebApiClient(syncSettings.WebApiEndPoint, logWriter);
            IServiceStateRepository serviceStateRepository = new JsonServiceStateRepository(syncSettings.ServiceStateRepositoryPath);
            var service = new SyncService(syncSettings, webApiClient, new LocalSyncProvider(syncSettings, serviceStateRepository, logWriter), new RemoteSyncProvider(webApiClient, logWriter), new LogFileManager(), serviceStateRepository);
            service.SetLogWriter(logWriter);
            return service;
        }
    }
}