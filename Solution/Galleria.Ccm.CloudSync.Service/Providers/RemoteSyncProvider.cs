﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Galleria.Ccm.CloudSync.Service.Clients;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Ccm.GFSModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Content;
using Galleria.Framework.Planograms.Model;
using Newtonsoft.Json;
using Topshelf.Logging;
using static Galleria.Ccm.CloudSync.Service.Providers.RemoteSyncProviderLogMessages;
using File = System.IO.File;
using Location = Galleria.Ccm.Services.Location.Location;
using PerformanceSource = Galleria.Ccm.Services.Performance.PerformanceSource;
using ProductHierarchy = Galleria.Ccm.Services.Product.ProductHierarchy;
using Task = System.Threading.Tasks.Task;

namespace Galleria.Ccm.CloudSync.Service.Providers
{
    public class RemoteSyncProvider : IRemoteSyncProvider
    {
        private static readonly NameValueCollection Settings;
        private readonly IWebApiClient _webClient;
        private readonly LogWriter _log;

        static RemoteSyncProvider()
        {
            Settings = ConfigurationManager.AppSettings;
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="RemoteSyncProvider" />.
        /// </summary>
        /// <param name="webClient">
        ///     A <see cref="IWebApiClient" /> instance is required to allow communication with the remore
        ///     endpoint.
        /// </param>
        /// <param name="log"></param>
        /// <exception cref="ArgumentNullException">A null <paramref name="webClient" /> was provided.</exception>
        public RemoteSyncProvider(IWebApiClient webClient, LogWriter log = null)
        {
            if (webClient == null) throw new ArgumentNullException(nameof(webClient));
            _webClient = webClient;
            _log = log;
        }

        public IList<IPlanInfo> FetchPlanInfos()
        {
            _log?.Info($"{OperationStart(nameof(FetchPlanInfos))}");

            HttpResponseMessage response = _webClient.GetAsync("api/planograms/infos/").Result;
            String json = response.Content.ReadAsStringAsync().Result;
            var deserializeObject = JsonConvert.DeserializeObject<List<PlanInfo>>(json);
            if (deserializeObject == null) return new List<IPlanInfo>();
            IList<IPlanInfo> planInfos = deserializeObject.ConvertAll(input => input as IPlanInfo);
            Int32 count = planInfos.Count;
            _log?.Info($"{OperationEnd(nameof(FetchPlanInfos))} {ProcessedPlansCount(count)}");
            return planInfos;
        }

        public void AddPackage(Package package, Dictionary<Guid, Guid> planogramGroupsDictionary)
        {
            String jsonPackage = SerializeToJson(package, planogramGroupsDictionary);
            if (String.IsNullOrWhiteSpace(jsonPackage)) return;

            var requestMessage = new HttpRequestMessage(HttpMethod.Put, Api.PlanogramsAdd)
            {
                Content = new StringContent(jsonPackage, Encoding.UTF8, "application/json")
            };
            HttpResponseMessage response = null;
            DateTime start = DateTime.UtcNow;
            try
            {
                _log?.Info($"--- Starting sync for {package.Name}.");
                _log?.Info($"--- Size {Encoding.Unicode.GetByteCount(jsonPackage) / 1024} KB.");
                response = _webClient.SendAsync(requestMessage).Result;
            }
            catch (Exception ex)
            {
                _log?.Error($"An error occurred while adding {PackageName(package.Name)}. {ex.Message}");
                _log?.Error($"Stack Trace: {ex.StackTrace}");
            }
            if (response?.IsSuccessStatusCode ?? false)
            {
                _log?.Info($"--- Succesfully synced [{(DateTime.UtcNow - start).Milliseconds} ms] {PackageName(package.Name)}");
            }
            else
            {
                _log?.Error($"XXX {OperationFailed(nameof(AddPackage))} [{(DateTime.UtcNow - start).Milliseconds} ms] {PackageName(package.Name)}");
            }
        }

        /// <summary>
        ///     Helper method used to serialize a package asynchronous, and log any exceptions. Will return empty if there were any issues serializing.
        /// </summary>
        /// <param name="package"></param>
        /// <param name="planogramGroupsDictionary"></param>
        /// <returns></returns>
        private String SerializeToJson(Package package, Dictionary<Guid, Guid> planogramGroupsDictionary)
        {
            String json = String.Empty;
            try
            {
                json = new PlanogramSerializer().ToJson(package, planogramGroupsDictionary);
            }
            catch (Exception ex)
            {
                _log?.Error($"XXX An error occurred while serializing the planogram. {ex.Message}");
                _log?.Error($"{ex.StackTrace}");
            }
            return json;
        }

        public Boolean UpdatePackages(IList<Package> packages, Dictionary<Guid, Guid> planogramGroupsDictionary)
        {
            Int32 count = packages.Count;
            _log?.Info($"{OperationStart(nameof(UpdatePackages))} {PackageCount(count)}");
            DateTime start = DateTime.UtcNow;

            var failedPlans = 0;
            foreach (Package package in packages)
            {
                var request =
                    new HttpRequestMessage(HttpMethod.Post, Api.PlanogramsUpdate)
                    {
                        Content = new StringContent(new PlanogramSerializer().ToJson(package, planogramGroupsDictionary), Encoding.UTF8, "application/json")
                    };
                HttpResponseMessage response = _webClient.SendAsync(request).Result;
                if (response?.IsSuccessStatusCode ?? false) continue;

                // We did not get an OK response or response at all.. log the error before going to the next package.
                _log?.Error($"{OperationFailed(nameof(UpdatePackages))} {PackageName(package.Name)}");
                failedPlans++;
            }

            TimeSpan duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationEnd(nameof(UpdatePackages))} {OperationDuration(duration)} {FailedPlansMessage(failedPlans)}");
            return true;
        }

        public Boolean DeleteUcrs(IList<String> ucrs)
        {
            Int32 count = ucrs.Count;
            _log?.Info($"{OperationStart(nameof(DeleteUcrs))} {PackageCount(count)}");
            DateTime start = DateTime.UtcNow;

            var failedPlans = 0;
            foreach (String ucr in ucrs)
            {
                var request =
                    new HttpRequestMessage(HttpMethod.Delete, Api.PlanogramsDelete)
                    {
                        Content = new StringContent(ucr)
                    };
                HttpResponseMessage response = _webClient.SendAsync(request).Result;
                if (response?.IsSuccessStatusCode ?? false) continue;

                // We did not get an OK response or response at all.. log the error before going to the next package.
                _log?.Error($"{OperationFailed(nameof(DeleteUcrs))} {Ucr(ucr)}");
                failedPlans++;
            }

            TimeSpan duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationEnd(nameof(DeleteUcrs))} {OperationDuration(duration)} {FailedPlansMessage(failedPlans)}");
            return true;
        }

        public Boolean AddLogFiles(IList<ILogFile> logFiles)
        {
            Int32 count = logFiles.Count;
            _log?.Info($"{OperationStart(nameof(AddLogFiles))}");
            DateTime start = DateTime.UtcNow;
            var failedLogs = 0;

            foreach (ILogFile log in logFiles)
            {
                using (Stream fileStream = log.stream)
                {
                    var request =
                        new HttpRequestMessage(HttpMethod.Put, Api.LogFilesAdd)
                        {
                            Content = new StreamContent(fileStream)
                        };
                    HttpResponseMessage response = _webClient.SendAsync(request).Result;
                    if (response?.IsSuccessStatusCode ?? false) continue;
                }
                // We did not get an OK response or response at all.. log the error before going to the next package.
                _log?.Error($"{OperationFailed(nameof(AddLogFiles))}");
                failedLogs++;
            }

            TimeSpan duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationEnd(nameof(AddLogFiles))} {OperationDuration(duration)} {FailedPlansMessage(failedLogs)}");
            return true;
        }

        public Boolean UpdatePlanogramHierarchy(PlanogramHierarchy planogramHierarchy)
        {
            _log?.Info($"{OperationStart(nameof(UpdatePlanogramHierarchy))}");
            DateTime start = DateTime.UtcNow;

            var request =
                new HttpRequestMessage(HttpMethod.Put, Api.PlanogramHierarchyUpdate)
                {
                    Content = new StringContent(new PlanogramHierarchySerializer().ToJson(planogramHierarchy), Encoding.UTF8, "application/json")
                };
            HttpResponseMessage response = _webClient.SendAsync(request).Result;
            TimeSpan duration;
            if (response?.IsSuccessStatusCode ?? false)
            {
                duration = DateTime.UtcNow - start;
                _log?.Info($"{OperationEnd(nameof(UpdatePlanogramHierarchy))} {OperationDuration(duration)}");

                return true;
            }

            duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationFailed(nameof(UpdatePlanogramHierarchy))} {OperationDuration(duration)}");

            return false;
        }

        public Boolean PublishPerformanceFile()
        {
            _log?.Info($"{OperationStart(nameof(PublishPerformanceFile))}");
            DateTime start = DateTime.UtcNow;

            String performanceDataFileLocation = Settings.Get(AppSettingsKeys.PerformanceFilesLocation);

            if (!Directory.Exists(performanceDataFileLocation)) return true;

            IList<String> paths = Directory.GetFiles(performanceDataFileLocation);

            foreach (String filePath in paths) CompressSavePublish(filePath);

            TimeSpan duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationEnd(nameof(PublishPerformanceFile))} {OperationDuration(duration)}");
            return true;
        }

        public Boolean UpdateLocations(List<Location> location)
        {
            _log?.Info($"{OperationStart(nameof(UpdateLocations))}");
            DateTime start = DateTime.UtcNow;

            var request =
                new HttpRequestMessage(HttpMethod.Put, Api.LocationUpdate)
                {
                    Content = new StringContent(new LocationSerializer().ToJson(location), Encoding.UTF8, "application/json")
                };
            HttpResponseMessage response = _webClient.SendAsync(request).Result;
            TimeSpan duration;
            if (response?.IsSuccessStatusCode ?? false)
            {
                duration = DateTime.UtcNow - start;
                _log?.Info($"{OperationEnd(nameof(UpdateLocations))} {OperationDuration(duration)}");

                return true;
            }

            duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationFailed(nameof(UpdateLocations))} {OperationDuration(duration)}");

            return false;
        }

        public Boolean PublishPerformanceSources(Dictionary<PerformanceSource, List<String>> performanceSources)
        {
            _log?.Info($"{OperationStart(nameof(PublishPerformanceSources))}");
            DateTime start = DateTime.UtcNow;

            String content = new PerformanceSourceSerializer().ToJson(performanceSources);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");
            var request =
                new HttpRequestMessage(HttpMethod.Post, Api.PerformanceSourcePublish)
                {
                    Content = stringContent
                };
            HttpResponseMessage response = _webClient.SendAsync(request).Result;
            TimeSpan duration;
            if (response?.IsSuccessStatusCode ?? false)
            {
                duration = DateTime.UtcNow - start;
                _log?.Info($"{OperationEnd(nameof(PublishPerformanceSources))} {OperationDuration(duration)}");

                return true;
            }

            duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationFailed(nameof(PublishPerformanceSources))} {OperationDuration(duration)}");

            return false;
        }

        /// <summary>
        ///     Sends the PublishingStatusDetails into the cloud
        /// </summary>
        /// <param name="publishingStatusDetails"></param>
        /// <returns></returns>
        public Boolean UpdatePublishingStatusDetails(List<PublishingStatusDetails> publishingStatusDetails)
        {
            _log?.Info($"{OperationStart(nameof(UpdatePublishingStatusDetails))}");
            DateTime start = DateTime.UtcNow;

            String content = new PublishingStatusDetailsSerializer().ToJson(publishingStatusDetails);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");
            var request =
                new HttpRequestMessage(HttpMethod.Put, Api.PublishingStatusDetailsUpdate)
                {
                    Content = stringContent
                };
            HttpResponseMessage response = _webClient.SendAsync(request).Result;
            TimeSpan duration;
            if (response?.IsSuccessStatusCode ?? false)
            {
                duration = DateTime.UtcNow - start;
                _log?.Info($"{OperationEnd(nameof(UpdatePublishingStatusDetails))} {OperationDuration(duration)}");

                return true;
            }

            duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationFailed(nameof(UpdatePublishingStatusDetails))} {OperationDuration(duration)}");

            return false;
        }

        public Boolean UpdateMerchandisingHierarchy(ProductHierarchy gfsMerchHierarchy)
        {
            _log?.Info($"{OperationStart(nameof(UpdateMerchandisingHierarchy))}");
            DateTime start = DateTime.UtcNow;

            var request =
                new HttpRequestMessage(HttpMethod.Put, Api.MerchandisingHierarchyUpdate)
                {
                    Content = new StringContent(new MerchandisingHierarchySerializer().ToJson(gfsMerchHierarchy), Encoding.UTF8, "application/json")
                };
            HttpResponseMessage response = _webClient.SendAsync(request).Result;
            TimeSpan duration;
            if (response?.IsSuccessStatusCode ?? false)
            {
                duration = DateTime.UtcNow - start;
                _log?.Info($"{OperationEnd(nameof(UpdateMerchandisingHierarchy))} {OperationDuration(duration)}");

                return true;
            }

            duration = DateTime.UtcNow - start;
            _log?.Info($"{OperationFailed(nameof(UpdatePlanogramHierarchy))} {OperationDuration(duration)}");

            return false;
        }

        private void CompressSavePublish(String filePath)
        {
            List<Byte[]> streams = CompressAndSplit(filePath);
            String fileName = Path.GetFileName(filePath);

            String timelineCode = fileName?.Split('_', '.').FirstOrDefault(x => x.All(Char.IsDigit));

            Directory.CreateDirectory(Settings.Get(AppSettingsKeys.StagingLocation) + $@"\{timelineCode}\");
            Directory.CreateDirectory(Settings.Get(AppSettingsKeys.StagingLocation) + $@"\{timelineCode}\OriginalFile");
            File.Move(
                filePath,
                Settings.Get(AppSettingsKeys.StagingLocation) + $@"\{timelineCode}\OriginalFile\{fileName}");

            SplitAndMoveToStaging(streams, fileName, timelineCode);

            String[] stagingFiles = Directory.GetFiles(
                Settings.Get(AppSettingsKeys.StagingLocation) +
                $@"\{timelineCode}\Staging\");

            foreach (String stagingFile in stagingFiles)
                using (FileStream originalFileStream = File.OpenRead(stagingFile))
                {
                    var chunk = new Byte[originalFileStream.Length];

                    originalFileStream.Read(chunk, 0, (Int32) originalFileStream.Length);

                    String[] stagingFileNameParts = stagingFile.Split('.');

                    var fp = new FilePart
                    {
                        FileData = chunk,
                        FileName = Path.GetFileName(stagingFile),
                        FilePartNumber = Convert.ToInt32(stagingFileNameParts[stagingFileNameParts.Length - 2]),
                        TotalNumberOfParts = stagingFiles.Length,
                        TimelineCode = timelineCode
                    };

                    var stringContent = new StringContent(JsonConvert.SerializeObject(fp), Encoding.UTF8, "application/json");
                    var request =
                        new HttpRequestMessage(HttpMethod.Post, Api.PerformanceDataPublish)
                        {
                            Content = stringContent
                        };

                    HttpResponseMessage response = _webClient.SendAsync(request).Result;
                    if (response?.IsSuccessStatusCode != false) continue;
                    // We did not get an OK response or response at all.. log the error before going to the next package.
                    _log?.Error($"{OperationFailed(nameof(UpdatePlanogramHierarchy))}");
                    break;
                }
        }

        private static void SplitAndMoveToStaging(ICollection<Byte[]> streams, String fileName, String timelineCode)
        {
            var filePartNumber = 0;
            foreach (Byte[] stream in streams)
            {
                filePartNumber++;
                String partFileName = $"{fileName}.{filePartNumber}.{streams.Count}";

                Directory.CreateDirectory(Settings.Get(AppSettingsKeys.StagingLocation) + $@"\{timelineCode}\Staging\");

                FileStream partFile =
                    File.Create(
                        Settings.Get(AppSettingsKeys.StagingLocation) +
                        $@"\{timelineCode}\Staging\" + partFileName);

                partFile.Write(stream, 0, stream.Length);

                partFile.Close();
            }
        }

        private static List<Byte[]> CompressAndSplit(String pathString)
        {
            var returnList = new List<Byte[]>();

            using (FileStream originalFileStream = File.OpenRead(pathString))
            {
                if (Path.GetExtension(pathString) == ".gz") return returnList;
                var chunk = new Byte[originalFileStream.Length];

                originalFileStream.Read(chunk, 0, (Int32) originalFileStream.Length);

                IEnumerable<Byte[]> splitPieces = Split(chunk, 1024*1024*5);

                foreach (Byte[] splitPiece in splitPieces)
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var compressionStream = new GZipStream(memoryStream, CompressionMode.Compress))
                        {
                            compressionStream.Write(splitPiece, 0, splitPiece.Length);
                        }
                        returnList.Add(memoryStream.ToArray());
                    }
            }
            return returnList;
        }

        private static IEnumerable<Byte[]> Split(ICollection<Byte> value, Int32 bufferLength)
        {
            Int32 countOfArray = value.Count/bufferLength;
            if (value.Count%bufferLength > 0)
                countOfArray++;
            for (var i = 0; i < countOfArray; i++)
                yield return value.Skip(i*bufferLength).Take(bufferLength).ToArray();
        }

        public void PublishPublishingStatusDetails(Int32 entityId, String entityName, DateTime lastUpdated, FoundationServiceClient gfsClient)
        {
            List<String> locationCodes = LocationList.FetchByEntityId(entityId).Select(l => l.Code).ToList();

            List<List<String>> locationCodesList = ListHelper.ChunkBy(locationCodes, 10);


            foreach (List<String> list in locationCodesList)
            {
                //use _changeDate temporarily till we actually add a date last synced field
                GetPublishingStatusDetailsResponse response = gfsClient.ContentServiceClient.GetPublishingStatusDetails(
                                                                           new GetPublishingStatusDetailsRequest(
                                                                               entityName,
                                                                               ContentType.Planogram.ToString(),
                                                                               lastUpdated,
                                                                               true,
                                                                               false,
                                                                               list));

                UpdatePublishingStatusDetails(response.PublishingStatusDetails);
            }
        }

        /// <summary>
        ///     Helper class to handle splitting up of lists into smaller lists
        /// </summary>
        private static class ListHelper
        {
            /// <summary>
            ///     Splits a list into chunk sizes returns a list of lists of the requested size
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="source">list to split up</param>
            /// <param name="chunkSize">number of items in each sub list</param>
            /// <returns></returns>
            public static List<List<T>> ChunkBy<T>(IEnumerable<T> source, Int32 chunkSize)
            {
                return source
                    .Select((x, i) => new { Index = i, Value = x })
                    .GroupBy(x => x.Index / chunkSize)
                    .Select(x => x.Select(v => v.Value).ToList())
                    .ToList();
            }
        }
    }
}