using System;
using System.Collections.Generic;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Topshelf.Logging;

namespace Galleria.Ccm.CloudSync.Service.Providers
{
    public interface ILocalSyncProvider
    {
        IList<PlanogramInfo> GetPlanInfos();
        IList<Package> GetPackagesDistinct(IEnumerable<IPlanInfo> planInfos);
        IList<Package> GetUpdatedPackages(IEnumerable<IPlanInfo> planInfos);
        IList<IPlanInfo> GetDeletedUcrs(IList<IPlanInfo> planInfos);
        PlanogramHierarchy GetPlanogramHierarchy();
        Dictionary<Guid, Guid> LookupPlanogramGroupPerPlanogram();
        Services.Product.ProductHierarchy GetMerchandisingHierarchy(FoundationServiceClient gfsClient);
        IList<Services.Location.Location> FetchLocationsFromGfsWebService(FoundationServiceClient gfsClient);

        /// <summary>
        ///     Get the entity from the local provider.
        /// </summary>
        /// <returns></returns>
        Entity GetEntity();

        IList<Services.Performance.PerformanceSource> FetchPerformanceSourcesFromGfsWebService(String entityName,
            FoundationServiceClient gfsClient);

        IList<Services.Timeline.TimelineGroup> FetchLastTwelveTimelineGroupsFromGfs(String entityName,
            String performanceSourceName, FoundationServiceClient gfsClient);

        Package FetchPackageById(Object id);
        void ProcessPlansByUcr(ICollection<String> newUcrsForRemote, IDictionary<String, SyncService.PlanLookup> localPlanLookup, Action<Package, Dictionary<Guid, Guid>> processDelegate, LogWriter logWriter = null);
    }
}