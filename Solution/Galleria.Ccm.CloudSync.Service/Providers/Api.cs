using System;

namespace Galleria.Ccm.CloudSync.Service.Providers
{
    public static class Api
    {
        public const String PlanogramsAdd = "api/planograms/add";
        public const String PlanogramsUpdate = "api/planograms/update";
        public const String PlanogramsDelete = "api/planograms/delete";
        public const String LogFilesAdd = "api/logfiles/add";
        public const String PlanogramHierarchyUpdate = "api/planogramhierarchy/update";
        public const String PerformanceDataPublish = "api/performancedata/publish";
        public const String MerchandisingHierarchyUpdate = "api/producthierarchy/update";
        public const String LocationUpdate = "api/location/update";
        public const String PerformanceSourcePublish = "api/performancesource/publish";
        public const String PublishingStatusDetailsUpdate = "api/publishingstatusdetails/update";
    }
}