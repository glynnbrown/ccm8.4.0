﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Performance;

namespace Galleria.Ccm.CloudSync.Service.Providers
{
    public interface IRemoteSyncProvider
    {
        IList<IPlanInfo> FetchPlanInfos();
        void AddPackage(Package package, Dictionary<Guid,Guid> planogramGroupings);
        Boolean UpdatePackages(IList<Package> packages, Dictionary<Guid, Guid> planogramGroupings);
        Boolean DeleteUcrs(IList<String> ucrs);
        Boolean AddLogFiles(IList<ILogFile> logFiles);
        Boolean UpdatePlanogramHierarchy(PlanogramHierarchy planHierarchy);
        Boolean UpdateMerchandisingHierarchy(Services.Product.ProductHierarchy gfsMerchHierarchy);
        Boolean PublishPerformanceFile();
        Boolean UpdateLocations(List<Services.Location.Location> location);
        Boolean PublishPerformanceSources(Dictionary<PerformanceSource, List<String>> performanceSources);
        Boolean UpdatePublishingStatusDetails(List<Services.Content.PublishingStatusDetails> publishingStatusDetails);
        void PublishPublishingStatusDetails(Int32 entityId, String entityName, DateTime lastUpdated, FoundationServiceClient gfsClient);
    }
}