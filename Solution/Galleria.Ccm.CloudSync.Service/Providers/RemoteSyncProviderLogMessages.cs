using System;

namespace Galleria.Ccm.CloudSync.Service.Providers
{
    public static class RemoteSyncProviderLogMessages
    {
        public static String ProcessedPlansCount(Int32 count) => $"# of plan infos: {count};";

        public static String FailedPlansMessage(Int32 failedPlans) => failedPlans == 0 ? String.Empty : $"Failed plans: {failedPlans};";

        public static String OperationEnd(String operation) => $"Operation end: {operation};";

        public static String OperationFailed(String operation) => $"Failed: {operation};";

        public static String PackageName(String name) => $"Planogram: {name};";

        public static String PackageCount(Int32 count) => $"# of packages: {count};";

        public static String OperationStart(String operation) => $"Operation start: {operation};";

        public static String OperationDuration(TimeSpan duration) => $"Duration: {duration.Milliseconds} ms;";

        public static String Ucr(String ucr) => $"Ucr: {ucr}";
    }
}