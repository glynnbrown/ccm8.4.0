using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Location;
using Galleria.Ccm.Services.Performance;
using Galleria.Ccm.Services.Product;
using Galleria.Ccm.Services.Timeline;
using Topshelf.Logging;
using Location = Galleria.Ccm.Services.Location.Location;
using ProductHierarchy = Galleria.Ccm.Services.Product.ProductHierarchy;

namespace Galleria.Ccm.CloudSync.Service.Providers
{
    public class LocalSyncProvider : ILocalSyncProvider
    {
        private readonly LogWriter _log;
        private readonly String _ccmRepository;
        private readonly Int32 _entityId;
        private readonly IServiceStateRepository _serviceStateRepository;

        /// <summary>
        ///     Initializes a new instance of <see cref="ILocalSyncProvider"/>.
        /// </summary>
        /// <param name="syncSettings">The settings used to setup and use this instance.</param>
        /// <param name="serviceStateRepository"></param>
        /// <param name="log">[Optional] The <see cref="LogWriter"/> instance used to log events to.</param>
        public LocalSyncProvider(ISyncSettings syncSettings, IServiceStateRepository serviceStateRepository, LogWriter log = null)
        {
            _log = log;
            _serviceStateRepository = serviceStateRepository;
            _ccmRepository = $"{syncSettings.RepositoryServer}{syncSettings.RepositoryName}";
            _entityId = syncSettings.EntityId;
        }

        public IList<Package> GetPackagesDistinct(IEnumerable<IPlanInfo> planInfos)
        {
            _log?.Info($"Operation start: {nameof(GetPackagesDistinct)};");

            var ucrs = planInfos.Select(info => info.Ucr);
            IList<Object> newIds = Package.FetchIdsExceptFor(ucrs);
            var packages = new List<Package>();
            foreach (Object id in newIds)
            {
                packages.Add(Package.FetchById(id));
            }

            Int32 count = packages.Count;
            _log?.Info($"Operation end: {nameof(GetPackagesDistinct)}; # of new packages: {count};");
            return packages;
        }

        /// <summary>
        ///     Fetch all the <see cref="PlanogramInfo"/> for the given <paramref name="entityId"/>.
        /// </summary>
        /// <returns></returns>
        public IList<PlanogramInfo> GetPlanInfos()
        {
            var allPlans = new List<PlanogramInfo>();
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(_entityId);

            foreach (PlanogramGroup planogramGroup in hierarchy.EnumerateAllGroups())
            {
                allPlans.AddRange(PlanogramInfoList.FetchByPlanogramGroupId(planogramGroup.Id));
            }

            return allPlans;
        }

        public Package FetchPackageById(Object id)
        {
            return Package.FetchById(id);
        }

        public IList<Package> GetUpdatedPackages(IEnumerable<IPlanInfo> planInfos)
        {
            _log?.Info($"Operation start: {nameof(GetUpdatedPackages)};");

            var urcs = planInfos.Select(info => info.Ucr);
            var dateLastSycned = _serviceStateRepository.LastFullSyncDate;

            IList<Object> newIds = Package.FetchPlansModifiedAfter(urcs, dateLastSycned);

            var packages = new List<Package>();
            foreach (Object id in newIds)
            {
                packages.Add(Package.FetchById(id));
            }


            Int32 count = packages.Count;
            _log?.Info($"Operation end: {nameof(GetUpdatedPackages)}; # of packages to update: {count};");
            return packages;
        }

        public IList<IPlanInfo> GetDeletedUcrs(IList<IPlanInfo> planInfos)
        {
            _log?.Info($"Operation start: {nameof(GetDeletedUcrs)};");

            var urcs = planInfos.Select(info => info.Ucr).ToList();
            Int32 count = 0;
            IList<IPlanInfo> deletedInfos;

            IList<Object> deletedUcrs = Package.FetchDeletedByUcrs(urcs);

            if (deletedUcrs != null)
            {
                deletedInfos = planInfos.Where(pi => deletedUcrs.Contains(pi.Ucr)).ToList();
                count = deletedInfos.Count;
            }
            else
            {
                deletedInfos = new List<IPlanInfo>();
            }

            _log?.Info($"Operation end: {nameof(GetDeletedUcrs)}; # of packages to delete: {count};");
            return deletedInfos;
        }

        public PlanogramHierarchy GetPlanogramHierarchy()
        {
            DateTime start = DateTime.UtcNow;
            _log?.Info($"Operation start: {nameof(GetPlanogramHierarchy)};");

            try
            {
                return PlanogramHierarchy.FetchByEntityId(_entityId);
            }
            catch
            {
                _log?.Error($"An error occurred when trying to fetch the Planogram Hierarchy for entity {_entityId} from {_ccmRepository}");
                return null;
            }
            finally
            {
                _log?.Info($"{nameof(GetPlanogramHierarchy)} took {(DateTime.UtcNow - start).TotalMilliseconds:F} ms");
                _log?.Info($"Operation end: {nameof(GetPlanogramHierarchy)};");
            }
        }

        public Dictionary<Guid, Guid> LookupPlanogramGroupPerPlanogram()
        {
            DateTime start = DateTime.UtcNow;
            _log?.Info($"Operation start: {nameof(LookupPlanogramGroupPerPlanogram)};");

            try
            {
                return PlanogramHierarchy.FetchPlanogramPlanogramGroupGroupings(_entityId);
            }
            catch
            {
                _log?.Error($"An error occurred when trying to fetch the Planogram Group data for Planograms for entity {_entityId} from {_ccmRepository}");
                return null;
            }
            finally
            {
                _log?.Info($"{nameof(LookupPlanogramGroupPerPlanogram)} took {(DateTime.UtcNow - start).TotalMilliseconds:F} ms");
                _log?.Info($"Operation end: {nameof(LookupPlanogramGroupPerPlanogram)};");
            }
        }

        public ProductHierarchy GetMerchandisingHierarchy(FoundationServiceClient gfsClient)
        {
            DateTime start = DateTime.UtcNow;
            _log?.Info($"Operation start: {nameof(GetMerchandisingHierarchy)};");

            try
            {
                return gfsClient.ProductServiceClient.GetMerchandisingHierarchyByEntityId(
                                    new GetMerchandisingHierarchyByEntityIdRequest(_entityId)).MerchandisingHierarchy;
            }
            catch
            {
                _log?.Error($"An error occurred when trying to fetch the Planogram Merchandising Hierarchy data for entity {_entityId} from {_ccmRepository}");
                return null;
            }
            finally
            {
                _log?.Info($"{nameof(GetMerchandisingHierarchy)} took {(DateTime.UtcNow - start).TotalMilliseconds:F} ms");
                _log?.Info($"Operation end: {nameof(GetMerchandisingHierarchy)};");
            }
        }

        public IList<Location> FetchLocationsFromGfsWebService(FoundationServiceClient gfsClient)
        {
            _log?.Info($"Operation start: {nameof(FetchLocationsFromGfsWebService)};");

            List<Location> gfsLocations =gfsClient.LocationServiceClient.GetLocationByEntityIdIncludingDeleted(
                new GetLocationByEntityIdIncludingDeletedRequest(_entityId)).Locations;

            _log?.Info($"Operation end: {nameof(FetchLocationsFromGfsWebService)};");
            return gfsLocations;
        }

        public IList<PerformanceSource> FetchPerformanceSourcesFromGfsWebService(String entityName, FoundationServiceClient gfsClient)
        {
            _log?.Info($"Operation start: {nameof(FetchPerformanceSourcesFromGfsWebService)};");

            //context.DoWorkWithRetries(delegate ()
            //{
            List<PerformanceSource> gfsPerformanceSources = gfsClient.PerformanceServiceClient.GetPerformanceSourceByEntityName(
                new GetPerformanceSourceByEntityNameRequest(entityName)).PerformanceSources;
            //});

            _log?.Info($"Operation end: {nameof(FetchPerformanceSourcesFromGfsWebService)};");
            return gfsPerformanceSources;
        }

        public IList<TimelineGroup> FetchLastTwelveTimelineGroupsFromGfs(String entityName,
            String performanceSourceName, FoundationServiceClient gfsClient)
        {
            _log?.Info($"Operation start: {nameof(FetchLastTwelveTimelineGroupsFromGfs)};");

            //context.DoWorkWithRetries(delegate ()
            //{

            Int32 days = 12;
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            Calendar calendar = cultureInfo.Calendar;
            DateTime syncEndDate = DateTime.UtcNow;
            Int32 currWeek = calendar.GetWeekOfYear(syncEndDate, CalendarWeekRule.FirstFourDayWeek, cultureInfo.DateTimeFormat.FirstDayOfWeek);
            DateTime syncStartDate = calendar.AddWeeks(syncEndDate, -(days));

            Int32 preWeek = currWeek;

            //set the sync date to the first date of the previous week
            //as the data for the current week couldn't have possibly
            //been put into the ODS yet!
            while (preWeek == currWeek)
            {
                syncEndDate = syncEndDate.AddDays(-1);
                preWeek = calendar.GetWeekOfYear(syncEndDate, CalendarWeekRule.FirstFourDayWeek, cultureInfo.DateTimeFormat.FirstDayOfWeek);
            }

            List<TimelineGroup> gfsTimelineGroups = gfsClient.TimelineServiceClient.GetTimelineGroupByEntityNamePerformanceSourceNameDateRange(
                new GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeRequest(entityName, performanceSourceName, syncStartDate, syncEndDate)).TimelineGroups;

            _log?.Info($"Operation end: {nameof(FetchLastTwelveTimelineGroupsFromGfs)};");
            return gfsTimelineGroups;
        }

        public Entity GetEntity()
        {
            DateTime start = DateTime.UtcNow;
            _log?.Info($"Operation start: {nameof(GetEntity)};");

            try
            {
                return Entity.FetchById(_entityId);
            }
            catch
            {
                _log?.Error($"An error occurred when trying to fetch entity {_entityId} from {_ccmRepository}");
                return null;
            }
            finally
            {
                _log?.Info($"{nameof(GetEntity)} took {(DateTime.UtcNow - start).TotalMilliseconds:F} ms");
                _log?.Info($"Operation end: {nameof(GetEntity)};");
            }
        }

        public void ProcessPlansByUcr(ICollection<String> newUcrsForRemote, IDictionary<String, SyncService.PlanLookup> localPlanLookup, Action<Package, Dictionary<Guid, Guid>> processDelegate, LogWriter logWriter = null)
        {
            logWriter?.Info($"--- Found {newUcrsForRemote.Count} planograms in CCM that are not in the Cloud;");
            var errorIds = new StringBuilder();
            var successfulCount = 0;
            var errorCount = 0;
            Dictionary<Guid, Guid> planogramGroupings = LookupPlanogramGroupPerPlanogram();
            foreach (String ucr in newUcrsForRemote)
            {
                try
                {
                    Package package = FetchPackageById(localPlanLookup[ucr].Id);
                    processDelegate?.Invoke(package, planogramGroupings);
                    logWriter?.Info($"--- Synced {ucr}");
                    successfulCount++;
                }
                catch (Exception)
                {
                    errorIds.Append(errorIds.Length != 0 ? $", {ucr}" : ucr);
                    errorCount++;
                }
            }
            logWriter?.Info($"--- Processed {newUcrsForRemote.Count} planograms, {successfulCount} successfully uploaded - {errorCount} errors;");

            if (errorCount > 0) logWriter?.Error($"The following planogram IDs could not be synced: {errorIds}");
        }
    }
} 