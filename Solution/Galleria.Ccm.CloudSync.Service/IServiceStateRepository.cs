﻿using System;

namespace Galleria.Ccm.CloudSync.Service
{
    /// <summary>
    ///     Represents a repository for Service State instances.
    /// </summary>
    public interface IServiceStateRepository
    {
        /// <summary>
        ///     The last time a full sync was succesfully completed.
        /// </summary>
        DateTime LastFullSyncDate { get; set; }

        /// <summary>
        ///     The last time a full Publishing sync was succesfully completed.
        /// </summary>
        DateTime LastPublishingSyncDate { get; set; }
    }
}