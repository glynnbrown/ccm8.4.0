﻿using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.CloudSync.Service
{
    public interface IPlanogramSerializer
    {
        String ToJson(Package source, Dictionary<Guid, Guid> planogramGroupDictionary = null);
    }
}