﻿using System;

namespace Galleria.Ccm.CloudSync.Service
{
    /// <summary>
    ///     Represents a service state persisted.
    /// </summary>
    public interface IServiceState
    {
        /// <summary>
        ///     The last time that the service synced all data succesfully.
        /// </summary>
        DateTime LastFullSyncDate { get; set; }

        /// <summary>
        ///     The last time that the service synced publishing data succsfully.
        /// </summary>
        DateTime LastPublishingSyncDate { get; set; }
    }
}