﻿using Galleria.Ccm.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service
{
    public interface IPlanogramHierarchySerializer
    {
        String ToJson(PlanogramHierarchy source);
    }
}
