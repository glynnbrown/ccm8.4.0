﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service
{
    public class LogManager : ILogManager
    {
        private const String cLogFilePath = @"c:\galleria\cloudsync\logs\CloudSyncService.log";

        public IList<String> GetLogs()
        {
            
            // Need to pull from file the log files. 
            return new List<String>();
        }
    }

    public interface ILogManager
    {
        IList<String> GetLogs();
    }
}