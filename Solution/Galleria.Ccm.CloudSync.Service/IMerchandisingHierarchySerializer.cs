﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Services.Product;

namespace Galleria.Ccm.CloudSync.Service
{
    public interface IMerchandisingHierarchySerializer
    {
        String ToJson(Services.Product.ProductHierarchy source);
    }
}
