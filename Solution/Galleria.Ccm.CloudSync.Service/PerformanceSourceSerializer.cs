﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Ccm.CloudSync.Service.Models.PerformanceSourceGraph;
using Galleria.Ccm.CloudSync.Service.Providers;
using Galleria.Ccm.Services.Performance;
using Newtonsoft.Json;

namespace Galleria.Ccm.CloudSync.Service
{
    public interface IPerformanceSource
    {
        String DataType { get; set; }
        DateTime? DateLastProcessed { get; set; }
        String DaysPerPeriod { get; set; }
        String Description { get; set; }
        String EntityName { get; set; }
        Int32 Id { get; set; }
        String LocationLevelName { get; set; }
        String Name { get; set; }
        List<IPerformanceSourceMetric> PerformanceSourceMetrics { get; set; }
        String ProductLevelName { get; set; }
        String TimelineLevelName { get; set; }
        String Type { get; set; }
    }

    public interface IPerformanceSourceAddRequest
    {
        IPerformanceSource PerformanceSourceNode { get; set; }
        List<String> Timelines { get; set; }
    }

    public interface IPerformanceSourceMetric
    {
        String AggregationType { get; set; }
        String ColumnNumber { get; set; }
        Int32 Id { get; set; }
        Boolean IsCalculatedMetric { get; set; }
        Boolean IsDuplicateColumnMapping { get; set; }
        String MetricDescription { get; set; }
        String MetricName { get; set; }
    }

    public class PerformanceSourceSerializer : IPerformanceSourceSerializer
    {
        public String ToJson(Dictionary<PerformanceSource, List<String>> source)
        {
            List<PerformanceSourceAddRequest> wrappers =
                source.Select(
                    pair =>
                        new PerformanceSourceAddRequest
                        {
                            PerformanceSourceNode = AsGraphNode(pair.Key),
                            Timelines = pair.Value
                        }).ToList();

            return JsonConvert.SerializeObject(wrappers);
        }

        private static PerformanceSourceNode AsGraphNode(PerformanceSource source)
        {
            return new PerformanceSourceNode
            {
                Id = source.Id,
                EntityName = source.EntityName,
                Name = source.Name,
                Description = source.Description,
                LocationLevelName = source.LocationLevelName,
                ProductLevelName = source.ProductLevelName,
                TimelineLevelName = source.TimelineLevelName,
                Type = source.Type,
                PerformanceSourceMetrics = source.PerformanceSourceMetrics.Select(AsGraphNode).ToList<IPerformanceSourceMetric>(),
                DateLastProcessed = source.DateLastProcessed,
                DataType = source.DataType,
                DaysPerPeriod = source.DaysPerPeriod
            };
        }

        private static PerformanceSourceMetricNode AsGraphNode(PerformanceSourceMetric source)
        {
            return new PerformanceSourceMetricNode
            {
                Id = source.Id,
                MetricName = source.MetricName,
                ColumnNumber = source.ColumnNumber,
                IsDuplicateColumnMapping = source.IsDuplicateColumnMapping,
                AggregationType = source.AggregationType,
                MetricDescription = source.MetricDescription,
                IsCalculatedMetric = source.IsCalculatedMetric
            };
        }
    }
}
