﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Galleria.Ccm.CloudSync.Service
{
    /// <summary>
    ///     Represents a Repository that uses json files to store its data.
    /// </summary>
    public class JsonServiceStateRepository : IServiceStateRepository
    {
        private readonly String _jsonRepositoryPath;

        /// <summary>
        ///     Initializes a new instance of <see cref="JsonServiceStateRepository" />.
        /// </summary>
        /// <param name="jsonRepositoryPath">Full path to the file that will persist the information.</param>
        /// <remarks>The state directory and file will be created at this point if they do not exist yet.</remarks>
        public JsonServiceStateRepository(String jsonRepositoryPath)
        {
            _jsonRepositoryPath = jsonRepositoryPath;
        }

        /// <inheritdoc />
        public DateTime LastFullSyncDate
        {
            get { return GetServiceState().LastFullSyncDate; }
            set
            {
                IServiceState serviceState = GetServiceState();
                serviceState.LastFullSyncDate = value;
                WriteServiceState(serviceState);
            }
        }

        /// <inheritdoc />
        public DateTime LastPublishingSyncDate
        {
            get { return GetServiceState().LastPublishingSyncDate; }
            set
            {
                IServiceState serviceState = GetServiceState();
                serviceState.LastPublishingSyncDate = value;
                WriteServiceState(serviceState);
            }
        }

        private void WriteServiceState(IServiceState serviceState)
        {
            File.WriteAllText(_jsonRepositoryPath, JsonConvert.SerializeObject(serviceState));
        }

        private IServiceState GetServiceState()
        {
            IServiceState serviceState = File.Exists(_jsonRepositoryPath)
                                             ? JsonConvert.DeserializeObject<JsonServiceState>(File.ReadAllText(_jsonRepositoryPath))
                                             : new JsonServiceState();
            return serviceState;
        }
    }
}