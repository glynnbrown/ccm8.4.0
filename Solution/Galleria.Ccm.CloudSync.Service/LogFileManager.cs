﻿using Galleria.Ccm.CloudSync.Service.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Galleria.Ccm.CloudSync.Service
{
    public class LogFileManager : ILogFileManager
    {
        const string Path = @"c:\galleria\cloudsync\logs\";
        const String FileMask = "{0}.log";

        public Boolean DoesLogFileExist(String path)
        {
            return File.Exists(path);
        }

        private IEnumerable<String> GetSyncLogFileNames()
        {
            return Directory.EnumerateFiles(Path, String.Format(FileMask, "*"), SearchOption.TopDirectoryOnly);
        }

        private Stream GetStream(String path)
        {
            if (!DoesLogFileExist(path)) return null;

            return new FileStream(path, FileMode.Open, FileAccess.Read);
        }

        private Boolean DeleteLogFile(String path)
        {
            if (DoesLogFileExist(path))
            {
                File.Delete(path);
                return true;
            }
            return false;
        }

        public Boolean DeleteLogFiles()
        {
            IEnumerable<String> logFileNames = GetSyncLogFileNames();
            foreach (String path in logFileNames)
            {
                if (!DeleteLogFile(path)) return false;
            }

            return true;
        }

        public IList<ILogFile> FetchLogFiles()
        {
            IList<ILogFile> logs = new List<ILogFile>();

            IEnumerable<String> logFileNames = GetSyncLogFileNames();
            foreach (String path in logFileNames)
            {
                var logFile = new LogFile(GetStream(path));
                if (logFile.stream != null) logs.Add(logFile);
            }

            return logs;
        }

        public IEnumerable<ILogFile> LogFiles()
        {
            foreach(var fileName in GetSyncLogFileNames())
            {
                yield return new LogFile(new FileStream(fileName, FileMode.Open, FileAccess.Read)); 
            }
        }
     }
}