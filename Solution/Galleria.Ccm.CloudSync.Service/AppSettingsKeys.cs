﻿using System;

namespace Galleria.Ccm.CloudSync.Service
{
    /// <summary>
    ///     Provides convenient access to the keys used for different settings within the service.
    /// </summary>
    public static class AppSettingsKeys
    {
        /// <summary>
        ///     The key that stores the path to the Service State Repository file.
        /// </summary>
        public const String ServiceStateRepositoryPath = "ServiceStateRepositoryPath";

        /// <summary>
        ///     This key contains the endpoint for the Public API service.
        /// </summary>
        public const String PublicApiServiceEndPoint = "WebApiEndPoint";

        /// <summary>
        ///     This key contains the service key to authenticate in the Public API service.
        /// </summary>
        public const String ServiceKey = "WebApiServiceKey";

        /// <summary>
        ///     This key contains the endpoint for the GFS API service.
        /// </summary>
        public const String GfsApiServiceEndPoint  = "GFSWebServiceEndPoint";

        /// <summary>
        ///     This key contains the entity ID that will be used to obtain data from in CCM8.
        /// </summary>
        public const String EntityId = "EntityId";

        /// <summary>
        ///     This key contains the name of the server that hosts the CCM8 database.
        /// </summary>
        public const String Server = "ServerName";

        /// <summary>
        ///     This key contains the name of the database taht contains the CCM8 data.
        /// </summary>
        public const String Database = "DatabaseName";

        /// <summary>
        ///     This key contains the path where the performance data files to be uploaded are obtained from.
        /// </summary>
        public const String PerformanceFilesLocation = "PerformanceDataFileLocation";

        /// <summary>
        ///     This key contains the path where the performance data files will be stored while staging the sync process.
        /// </summary>
        public const String StagingLocation = "PerformanceDataStagingLocation";

        public const String WebApiServiceKeyPassphrase = "passphrase";
    }
}