﻿using Galleria.Ccm.Dal.Mssql;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CloudSync.Service
{
    public class DalConfigurator
    {

        private static void SetupAndRegisterDal(String serverName, String databaseName, String dalName, String windowsLogonName)
        {
            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(dalName);
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", serverName));
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", databaseName));
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("DefaultUser", windowsLogonName));

            // now create a new instance of the Mssql dal                
            IDalFactory dalFactory = new DalFactory(dalFactoryConfig);

            DalContainer.RegisterFactory(dalName, dalFactory);
            DalContainer.DalName = dalName;
        }

        public static void SetupAndRegisterMsqlDal(String serverName, String databaseName)
        {
            const String cMssql = "V8MsqlLDAL";
            var windowsLogonName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

           

            DalConfigurator.SetupAndRegisterDal(serverName, databaseName, cMssql, windowsLogonName);

            Ccm.Security.DomainPrincipal.Authenticate();
        }

    }
}
