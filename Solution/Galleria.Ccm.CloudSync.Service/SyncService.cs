using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Galleria.Ccm.CloudSync.Service.Clients;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Ccm.CloudSync.Service.Providers;
using Galleria.Ccm.GFSModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Content;
using Galleria.Ccm.Services.Entity;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Topshelf;
using Topshelf.Logging;
using Entity = Galleria.Ccm.Model.Entity;
using Location = Galleria.Ccm.Services.Location.Location;
using PerformanceSource = Galleria.Ccm.Services.Performance.PerformanceSource;
using ProductHierarchy = Galleria.Ccm.Services.Product.ProductHierarchy;
using Task = System.Threading.Tasks.Task;

namespace Galleria.Ccm.CloudSync.Service
{
    public class SyncService : ServiceControl
    {
        private readonly ISyncSettings _syncSettings;
        private readonly TimeSpan _timeout = TimeSpan.FromSeconds(6000);

        private FoundationServiceClient _gfsServiceClient;
        private CancellationTokenSource _mainTaskCancellationTokenSource;
        private String _webServiceAddress;
        private LogWriter _log;
        private readonly IWebApiClient _webApiClient;
        private readonly ILocalSyncProvider _localProvider;
        private readonly IRemoteSyncProvider _remoteProvider;
        private readonly ILogFileManager _logFileManager;
        private readonly IServiceStateRepository _serviceStateRepository;

        public SyncService(ISyncSettings syncSettings, IWebApiClient webApiClient, ILocalSyncProvider localSyncProvider, IRemoteSyncProvider remoteSyncProvider, ILogFileManager logFileManager, IServiceStateRepository serviceStateRepository)
        {
            _syncSettings = syncSettings;
            _serviceStateRepository = serviceStateRepository;
            _webApiClient = webApiClient;

            _log?.Info("Initializing SyncSettings from AppSettingsConstants.");
            DalConfigurator.SetupAndRegisterMsqlDal(_syncSettings.RepositoryServer, _syncSettings.RepositoryName);
            _localProvider = localSyncProvider;
            _remoteProvider = remoteSyncProvider;
            _logFileManager = logFileManager;
        }

        public void SetLogWriter(LogWriter logWriter)
        {
            _log = logWriter;
        }

        public Boolean Start(HostControl hostControl)
        {
            _webApiClient.SignInAsync(_syncSettings.Credentials).Wait();

            if (_webApiClient.IsSignedIn)
            {
                _log?.Info("Service start");
                _mainTaskCancellationTokenSource = new CancellationTokenSource();
                TaskEx.Run(() => PollWebservice(_mainTaskCancellationTokenSource.Token), _mainTaskCancellationTokenSource.Token);
            }
            else
            {
                _log?.Error("Service failed to start. Could not sign in using the Web API credenticals supplied.");
                Console.WriteLine("Service failed to start. Could not sign in using the Web API credenticals supplied.");
                Console.ReadLine();
            }

            return _webApiClient.IsSignedIn;
        }
        
        public Boolean Stop(HostControl hostControl)
        {
            _log?.Info("Service stop");
            if (!_mainTaskCancellationTokenSource.IsCancellationRequested) _mainTaskCancellationTokenSource.Cancel();
            return true;
        }

        private void PollWebservice(CancellationToken token)
        {
            _log?.Info("Polling start");
            while (true)
            {
                if (token.IsCancellationRequested) break;
                TaskEx.Delay(TimeSpan.FromSeconds(_syncSettings.PollingInterval)).Wait();
                _log?.Info("Polling for ucr list");
                var cancelSource = new CancellationTokenSource();
                CancellationToken cancellationToken = cancelSource.Token;
                Task mainTask = TaskEx.Run(() => ProcessPolling(cancellationToken), cancellationToken)
                                      .ContinueWith(
                                          LogTaskException,
                                          CancellationToken.None,
                                          TaskContinuationOptions.OnlyOnFaulted,
                                          TaskScheduler.Default);
                if (TaskEx.WhenAny(mainTask, TaskEx.Delay(_timeout, cancellationToken)).Result != mainTask)
                    _log?.Error($"Sync timeout ({_timeout}).");
                cancelSource.Cancel();
            }
            _log?.Info("Polling stop");
        }

        private void LogTaskException(Task task)
        {
            Exception baseException = task.Exception?.GetBaseException();
            _log.Error($"ERROR: A task failed to complete... {baseException?.Message ?? "[unknown exception]"}");
            _log.Error($"STACK TRACE: {baseException?.StackTrace ?? "[missing]"}");
            try
            {
                IList<ILogFile> logFiles = _logFileManager.FetchLogFiles();
                Boolean logsFetched = _remoteProvider.AddLogFiles(logFiles);
                if (logsFetched) _logFileManager.DeleteLogFiles();

            }
            finally
            {
                
            }
        }

        private void ProcessPolling(CancellationToken token)
        {
            DateTime fullSyncDate = DateTime.UtcNow;
            var timeoutTokenSource = new CancellationTokenSource();
            var timeoutCancelTokenSource = new CancellationTokenSource();
            CancellationToken timeoutToken = timeoutTokenSource.Token;
            CancellationToken timeoutCancelToken = timeoutCancelTokenSource.Token;
            TaskEx.Delay(TimeSpan.FromMinutes(120), timeoutToken).ContinueWith(
                      task =>
                      {
                          if (task.IsCompleted) { timeoutCancelTokenSource.Cancel(); }
                      }, CancellationToken.None);
            var isLogPending = true;
            try
            {
                Entity entity = _localProvider.GetEntity();
                if (entity == null || CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

                PlanogramHierarchy localHierarchy = _localProvider.GetPlanogramHierarchy();
                if (localHierarchy != null) _remoteProvider.UpdatePlanogramHierarchy(localHierarchy);
                if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

                SyncPlanograms(token, timeoutCancelToken);
                if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

                CreateWebServiceProxies();
                if (!CheckGfsConnection()) return;
                if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

                PublishLocations(entity);
                if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

                PublishPerformanceSources(entity.Name);
                if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

                PublishPerformanceData();
                if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

                if (entity.GFSId != null)
                {
                    ProductHierarchy merchHierarchy = _localProvider.GetMerchandisingHierarchy(_gfsServiceClient);
                    if (merchHierarchy != null) _remoteProvider.UpdateMerchandisingHierarchy(merchHierarchy);
                    if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;
                }

                _remoteProvider.PublishPublishingStatusDetails(entity.Id, entity.Name, _serviceStateRepository.LastPublishingSyncDate, _gfsServiceClient);
                _serviceStateRepository.LastFullSyncDate = fullSyncDate;
            }
            catch
            {
                // Whenever an exception is caught the logs are sent so there is no need to send them again.
                isLogPending = false;
            }
            finally
            {
                timeoutTokenSource.Cancel();
                if (isLogPending)
                {
                    IList<ILogFile> logFiles = _logFileManager.FetchLogFiles();
                    Boolean logsFetched = _remoteProvider.AddLogFiles(logFiles);
                    if (logsFetched) _logFileManager.DeleteLogFiles();
                }
            }
        }

        public class PlanLookup
        {
            public Object Id { get; }
            public String Ucr { get; }
            public DateTime Date { get; }

            public PlanLookup(Int32 id, Guid ucr, DateTime date)
            {
                Id = id;
                Ucr = ucr.ToString();
                Date = date;
            }

            public PlanLookup(String ucr, DateTime date)
            {
                Id = -1;
                Ucr = ucr;
                Date = date;
            }

            public override String ToString()
            {
                return $"{{ Id = {Id}, ucr = {Ucr}, date = {Date} }}";
            }

            public Boolean IsNewer(PlanLookup other) => Date >= other.Date;

            public override Boolean Equals(Object value)
            {
                var type = value as PlanLookup;
                return (type != null) && EqualityComparer<String>.Default.Equals(type.Ucr, Ucr) &&
                       EqualityComparer<DateTime>.Default.Equals(type.Date, Date);
            }

            public override Int32 GetHashCode()
            {
                var num = 0x7a2f0b42;
                num = -1521134295*num + EqualityComparer<String>.Default.GetHashCode(Ucr);
                return -1521134295*num + EqualityComparer<DateTime>.Default.GetHashCode(Date);
            }
        }

        private void SyncPlanograms(CancellationToken token, CancellationToken timeoutCancelToken)
        {
            if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;

            DateTime startTime = DateTime.UtcNow;
            _log?.Info($"Operation started {nameof(SyncPlanograms)};");

            IList<IPlanInfo> remotePlanInfos = _remoteProvider.FetchPlanInfos();
            IList<PlanogramInfo> localPlanogramInfos;
            try
            {
                if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;
                localPlanogramInfos = _localProvider.GetPlanInfos();
            }
            catch (Exception ex)
            {
                _log?.Error($"An error occurred while getting new local IDs from the database. {ex.Message}");
                _log?.Error($"Stack Trace: {ex.StackTrace}");
                _log?.Info($"Operation stopped {nameof(SyncPlanograms)} after running for {(DateTime.UtcNow - startTime).TotalMilliseconds} ms;");
                return;
            }

            var localPlanLookup = localPlanogramInfos.Select(info => new PlanLookup(info.Id, info.UniqueContentReference, info.DateLastModified)).ToLookupDictionary(arg => arg.Ucr);
            var remotePlanLookup = remotePlanInfos.Select(info => new PlanLookup(info.Ucr, info.DateUpdated)).ToLookupDictionary(arg => arg.Ucr);

            if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;
            ICollection<String> newUcrsForRemote = localPlanLookup.Keys.Where(key => !remotePlanLookup.ContainsKey(key)).ToList();
            _localProvider.ProcessPlansByUcr(newUcrsForRemote, localPlanLookup, AddPackageToRemote, _log);

            if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;
            ICollection<String> newUcrsForLocal = remotePlanLookup.Keys.Where(key => !localPlanLookup.ContainsKey(key)).ToList();
            SyncNewPlansForLocal(newUcrsForLocal, remotePlanLookup);

            if (CheckCancel(timeoutCancelToken) || CheckCancel(token)) return;
            ICollection<String> commonUcrs = localPlanLookup.Keys.Where(key => remotePlanLookup.ContainsKey(key)).ToList();
            SyncCommonPlans(commonUcrs, localPlanLookup, remotePlanLookup);

            _log?.Info($"Operation stopped {nameof(SyncPlanograms)} after running for {(DateTime.UtcNow - startTime).TotalMilliseconds} ms;");
        }

        private void SyncCommonPlans(ICollection<String> commonUcrs, IDictionary<String, PlanLookup> localPlanLookup, IDictionary<String, PlanLookup> remotePlanLookup)
        {
            var successfulToRemoteCount = 0;
            var errorToRemoteCount = 0;
            var errorToRemoteUcrs = new StringBuilder();
            Dictionary<Guid, Guid> planogramGroupings = _localProvider.LookupPlanogramGroupPerPlanogram();
            foreach (String ucr in commonUcrs)
            {
                if (localPlanLookup[ucr].IsNewer(remotePlanLookup[ucr]))
                {
                    try
                    {
                        Package package = _localProvider.FetchPackageById(localPlanLookup[ucr].Id);
                        _remoteProvider.AddPackage(package, planogramGroupings);
                        successfulToRemoteCount++;
                    }
                    catch (Exception)
                    {
                        errorToRemoteUcrs.Append(errorToRemoteUcrs.Length != 0 ? $", {ucr}" : ucr);
                        errorToRemoteCount++;
                    }
                }
                else
                {
                    //        Dictionary<Guid, Guid> planogramGroupings = _localProvider.LookupPlanogramGroupPerPlanogram(_syncSettings.EntityId).Result;
                    //    try
                    //    {
                    //        Package package = _remoteProvider.FetchPackageByUcr(ucr);
                    //        _remoteProvider.AddPackageAsync(package, planogramGroupings);
                    //        successfulToLocalCount++;
                    //    }
                    //    catch (Exception)
                    //    {
                    //        errorToLocalUcrs.Append(errorToLocalUcrs.Length != 0 ? $", {ucr}" : ucr);
                    //        errorToLocalCount++;
                    //    }
                }
            }
        }

        private void SyncNewPlansForLocal(ICollection<String> newUcrsForLocal, IDictionary<String, PlanLookup> remotePlanLookup)
        {
            //_log?.Info($"--- Found {newUcrsForLocal.Count} planograms in the Cloud that are not in CCM;");
            //var errorIds = new StringBuilder();
            //var successfulCount = 0;
            //var errorCount = 0;
            //        Dictionary<Guid, Guid> planogramGroupings = _localProvider.LookupPlanogramGroupPerPlanogram(_syncSettings.EntityId).Result;
            //foreach (String ucr in newUcrsForLocal)
            //{
            //    try
            //    {
            //        Package package = _remoteProvider.FetchPackageByUcr(ucr);
            //        _remoteProvider.AddPackageAsync(package, planogramGroupings);
            //        successfulCount++;
            //    }
            //    catch (Exception)
            //    {
            //        errorIds.Append(errorIds.Length != 0 ? $", {ucr}" : ucr);
            //        errorCount++;
            //    }
            //}
            //_log?.Info($"--- Processed {newUcrsForLocal.Count} planograms, {successfulCount} successfully uploaded - {errorCount} errors;");

            //if (errorCount > 0) _log?.Error($"The following planogram IDs could not be synced: {errorIds}");
        }

        private void AddPackageToRemote(Package package, Dictionary<Guid, Guid> planogramGroupings)
        {
            _remoteProvider.AddPackage(package, planogramGroupings);
        }

        private Boolean CheckGfsConnection()
        {
            try
            {
                _gfsServiceClient.EntityServiceClient.GetAllEntities(new GetAllEntitiesRequest());
                return true;
            }
            catch (Exception e)
            {
                _log?.Log(LoggingLevel.Error, "No Connection to GFS Web Services", e);
                return false;
            }
        }

        private Boolean PublishPerformanceData()
        {
            return _remoteProvider.PublishPerformanceFile();
        }

        private Boolean CheckCancel(CancellationToken token)
        {
            Boolean cancellationRequested = _mainTaskCancellationTokenSource.IsCancellationRequested || token.IsCancellationRequested;
            if (cancellationRequested)
                _log?.Warn($"{nameof(ProcessPolling)} cancelled...");
            return cancellationRequested;
        }

        private void PublishLocations(Entity entity)
        {
            if (entity.GFSId == null) return;
            DateTime publishingSyncDate = DateTime.UtcNow;
            IList<Location> locations = _localProvider.FetchLocationsFromGfsWebService(_gfsServiceClient);

            _remoteProvider.UpdateLocations(locations.ToList());
            _serviceStateRepository.LastPublishingSyncDate = publishingSyncDate;
        }

        private void PublishPerformanceSources(String entityName)
        {
            ILocalSyncProvider localProvider = _localProvider;
            IList<PerformanceSource> performanceSources =
                localProvider.FetchPerformanceSourcesFromGfsWebService(entityName, _gfsServiceClient);

            Dictionary<PerformanceSource, List<String>> performanceSourceTimelineDictionary =
                performanceSources.ToDictionary(
                    source => source,
                    source =>
                        localProvider.FetchLastTwelveTimelineGroupsFromGfs(entityName, source.Name, _gfsServiceClient)
                                      .Select(t => t.Code.ToString())
                                      .ToList());

            _remoteProvider.PublishPerformanceSources(performanceSourceTimelineDictionary);
        }

        private void CreateWebServiceProxies()
        {
            //if we have a dto, then attempt to create the proxies

            _webServiceAddress = $"http://{ConfigurationManager.AppSettings.Get(AppSettingsKeys.GfsApiServiceEndPoint)}/Services";

            _gfsServiceClient = new FoundationServiceClient(_webServiceAddress);

            FoundationServiceClient.RegisterGFSServiceClientTypes();
        }
    }
}