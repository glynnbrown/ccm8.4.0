using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.CloudSync.Service.Models.PlanogramGraph;
using Galleria.Framework.Planograms.Model;
using Newtonsoft.Json;

namespace Galleria.Ccm.CloudSync.Service
{
    public class PlanogramSerializer : IPlanogramSerializer
    {
        public String ToJson(Package source, Dictionary<Guid, Guid> planogramGroupDictionary = null)
        {
            if (planogramGroupDictionary == null)
            {
                planogramGroupDictionary = new Dictionary<Guid, Guid>();
            }
            return JsonConvert.SerializeObject(AsGraphNode(source, planogramGroupDictionary));
        }

        private static PackageNode AsGraphNode(Package source, Dictionary<Guid, Guid> planogramGroupDictionary)
        {
            var node = new PackageNode();
            node.Id = source.Id;
            node.DateCreated = source.DateCreated;
            node.DateLastModified = source.DateLastModified;
            node.DateMetadataCalculated = source.DateMetadataCalculated;
            node.DateDeleted = source.DateDeleted;
            node.DateValidationDataCalculated = source.DateValidationDataCalculated;
            node.EntityId = source.EntityId;
            node.Name = source.Name;
            node.PackageType = source.PackageType;
            node.UserName = source.UserName;
            node.IsReadOnly = source.IsReadOnly;
            node.MetaPlanogramCount = source.MetaPlanogramCount;
            node.Planograms = source.Planograms.Select(s => AsGraphNode(s, planogramGroupDictionary)).ToList();
            return node;
        }

        private static PlanogramNode AsGraphNode(Planogram source, Dictionary<Guid, Guid> planogramGroupDictionary)
        {
            Guid planogramGroupCode = planogramGroupDictionary.ContainsKey(source.UniqueContentReference) ? planogramGroupDictionary[source.UniqueContentReference] : Guid.Empty;

            var node = new PlanogramNode();
            node.Id = source.Id;
            node.Assemblies = source.Assemblies.Select(AsGraphNode).ToList();
            node.AngleUnitsOfMeasure = source.AngleUnitsOfMeasure;
            node.Annotations = source.Annotations.Select(AsGraphNode).ToList();
            node.AreaUnitsOfMeasure = source.AreaUnitsOfMeasure;
            node.Assortment = AsGraphNode(source.Assortment);
            node.Blocking = source.Blocking.Select(AsGraphNode);
            node.CustomAttributes = AsGraphNode(source.CustomAttributes);
            node.Comparison = AsGraphNode(source.Comparison);
            node.CategoryCode = source.CategoryCode;
            node.ClusterName = source.ClusterName;
            node.ClusterSchemeName = source.ClusterSchemeName;
            node.CategoryName = source.CategoryName;
            node.Components = source.Components.Select(AsGraphNode).ToList();
            node.CurrencyUnitsOfMeasure = source.CurrencyUnitsOfMeasure;
            node.DateApproved = source.DateApproved;
            node.DateArchived = source.DateArchived;
            node.DateWip = source.DateWip;
            node.Depth = source.Depth;
            node.EventLogs = source.EventLogs.Select(AsGraphNode).ToList();
            node.FixtureItems = source.FixtureItems.Select(AsGraphNode).ToList();
            node.Fixtures = source.Fixtures.Select(AsGraphNode).ToList();
            node.Height = source.Height;
            node.HighlightSequenceStrategy = source.HighlightSequenceStrategy;
            node.Images = source.Images.Select(AsGraphNode).ToList();
            node.Inventory = AsGraphNode(source.Inventory);
            node.LengthUnitsOfMeasure = source.LengthUnitsOfMeasure;
            node.LocationCode = source.LocationCode;
            node.LocationName = source.LocationName;
            node.MetaAverageCases = source.MetaAverageCases;
            node.MetaAverageDos = source.MetaAverageDos;
            node.MetaAverageFacings = source.MetaAverageFacings;
            node.MetaAverageFrontFacings = source.MetaAverageFrontFacings;
            node.MetaAverageUnits = source.MetaAverageUnits;
            node.MetaBayCount = source.MetaBayCount;
            node.MetaBlocksDropped = source.MetaBlocksDropped;
            node.MetaChangeFromPreviousStarRating = source.MetaChangeFromPreviousStarRating;
            node.MetaChangesFromPreviousCount = source.MetaChangesFromPreviousCount;
            node.MetaComponentCount = source.MetaComponentCount;
            node.MetaCountOfPlacedProductsNotRecommendedInAssortment = source.MetaCountOfPlacedProductsNotRecommendedInAssortment;
            node.MetaCountOfPlacedProductsRecommendedInAssortment = source.MetaCountOfPlacedProductsRecommendedInAssortment;
            node.MetaCountOfPositionsOutsideOfBlockSpace = source.MetaCountOfPositionsOutsideOfBlockSpace;
            node.MetaCountOfProductNotAchievedCases = source.MetaCountOfProductNotAchievedCases;
            node.MetaCountOfProductNotAchievedDOS = source.MetaCountOfProductNotAchievedDOS;
            node.MetaCountOfProductNotAchievedDeliveries = source.MetaCountOfProductNotAchievedDeliveries;
            node.MetaCountOfProductOverShelfLifePercent = source.MetaCountOfProductOverShelfLifePercent;
            node.MetaCountOfProductsBuddied = source.MetaCountOfProductsBuddied;
            node.MetaCountOfRecommendedProductsInAssortment = source.MetaCountOfRecommendedProductsInAssortment;
            node.MetaHasComponentsOutsideOfFixtureArea = source.MetaHasComponentsOutsideOfFixtureArea;
            node.MetaHighestErrorScore = source.MetaHighestErrorScore;
            node.MetaMaxCases = source.MetaMaxCases;
            node.MetaMaxDos = source.MetaMaxDos;
            node.MetaMinCases = source.MetaMinCases;
            node.MetaMinDos = source.MetaMinDos;
            node.MetaNewProducts = source.MetaNewProducts;
            node.MetaNoOfDebugEvents = source.MetaNoOfDebugEvents;
            node.MetaNoOfErrors = source.MetaNoOfErrors;
            node.MetaNoOfInformationEvents = source.MetaNoOfInformationEvents;
            node.MetaNoOfWarnings = source.MetaNoOfWarnings;
            node.MetaNotAchievedInventory = source.MetaNotAchievedInventory;
            node.MetaNumberOfAssortmentRulesBroken = source.MetaNumberOfAssortmentRulesBroken;
            node.MetaNumberOfCoreRulesBroken = source.MetaNumberOfCoreRulesBroken;
            node.MetaNumberOfDelistFamilyRulesBroken = source.MetaNumberOfDelistFamilyRulesBroken;
            node.MetaNumberOfDelistProductRulesBroken = source.MetaNumberOfDelistProductRulesBroken;
            node.MetaNumberOfDependencyFamilyRulesBroken = source.MetaNumberOfDependencyFamilyRulesBroken;
            node.MetaNumberOfDistributionRulesBroken = source.MetaNumberOfDistributionRulesBroken;
            node.MetaNumberOfForceProductRulesBroken = source.MetaNumberOfForceProductRulesBroken;
            node.MetaNumberOfFamilyRulesBroken = source.MetaNumberOfFamilyRulesBroken;
            node.MetaNumberOfInheritanceRulesBroken = source.MetaNumberOfInheritanceRulesBroken;
            node.MetaNumberOfLocalProductRulesBroken = source.MetaNumberOfLocalProductRulesBroken;
            node.MetaNumberOfMaximumProductFamilyRulesBroken = source.MetaNumberOfMaximumProductFamilyRulesBroken;
            node.MetaNumberOfMinimumHurdleProductRulesBroken = source.MetaNumberOfMinimumHurdleProductRulesBroken;
            node.MetaNumberOfMinimumProductFamilyRulesBroken = source.MetaNumberOfMinimumProductFamilyRulesBroken;
            node.MetaNumberOfPreserveProductRulesBroken = source.MetaNumberOfPreserveProductRulesBroken;
            node.MetaNumberOfProductRulesBroken = source.MetaNumberOfProductRulesBroken;
            node.MetaPercentOfPlacedProductsRecommendedInAssortment = source.MetaPercentOfPlacedProductsRecommendedInAssortment;
            node.MetaPercentOfPositionsOutsideOfBlockSpace = source.MetaPercentOfPositionsOutsideOfBlockSpace;
            node.MetaPercentOfProductNotAchievedCases = source.MetaPercentOfProductNotAchievedCases;
            node.MetaPercentOfProductNotAchievedDOS = source.MetaPercentOfProductNotAchievedDOS;
            node.MetaPercentOfProductNotAchievedDeliveries = source.MetaPercentOfProductNotAchievedDeliveries;
            node.MetaPercentOfProductNotAchievedInventory = source.MetaPercentOfProductNotAchievedInventory;
            node.MetaPercentOfProductOverShelfLifePercent = source.MetaPercentOfProductOverShelfLifePercent;
            node.MetaPercentageOfAssortmentRulesBroken = source.MetaPercentageOfAssortmentRulesBroken;
            node.MetaPercentageOfCoreRulesBroken = source.MetaPercentageOfCoreRulesBroken;
            node.MetaPercentageOfDistributionRulesBroken = source.MetaPercentageOfDistributionRulesBroken;
            node.MetaPercentageOfFamilyRulesBroken = source.MetaPercentageOfFamilyRulesBroken;
            node.MetaPercentageOfInheritanceRulesBroken = source.MetaPercentageOfInheritanceRulesBroken;
            node.MetaPercentageOfLocalProductRulesBroken = source.MetaPercentageOfLocalProductRulesBroken;
            node.MetaPercentageOfProductRulesBroken = source.MetaPercentageOfProductRulesBroken;
            node.MetaProductsPlaced = source.MetaProductsPlaced;
            node.MetaProductsUnplaced = source.MetaProductsUnplaced;
            node.MetaTotalAreaWhiteSpace = source.MetaTotalAreaWhiteSpace;
            node.MetaTotalComponentCollisions = source.MetaTotalComponentCollisions;
            node.MetaTotalComponentsOverMerchandisedDepth = source.MetaTotalComponentsOverMerchandisedDepth;
            node.MetaTotalComponentsOverMerchandisedHeight = source.MetaTotalComponentsOverMerchandisedHeight;
            node.MetaTotalComponentsOverMerchandisedWidth = source.MetaTotalComponentsOverMerchandisedWidth;
            node.MetaTotalErrorScore = source.MetaTotalErrorScore;
            node.MetaTotalFacings = source.MetaTotalFacings;
            node.MetaTotalFrontFacings = source.MetaTotalFrontFacings;
            node.MetaTotalLinearWhiteSpace = source.MetaTotalLinearWhiteSpace;
            node.MetaTotalMerchandisableAreaSpace = source.MetaTotalMerchandisableAreaSpace;
            node.MetaTotalMerchandisableLinearSpace = source.MetaTotalMerchandisableLinearSpace;
            node.MetaTotalMerchandisableVolumetricSpace = source.MetaTotalMerchandisableVolumetricSpace;
            node.MetaTotalPositionCollisions = source.MetaTotalPositionCollisions;
            node.MetaTotalUnits = source.MetaTotalUnits;
            node.MetaTotalVolumetricWhiteSpace = source.MetaTotalVolumetricWhiteSpace;
            node.MetaUniqueProductCount = source.MetaUniqueProductCount;
            node.Name = source.Name;
            node.Performance = AsGraphNode(source.Performance);
            node.PlanogramGroupCode = planogramGroupCode;
            node.PlanogramMetadataImages = source.PlanogramMetadataImages.Select(AsGraphNode);
            node.PlanogramType = source.PlanogramType;
            node.Positions = source.Positions.Select(AsGraphNode);
            node.ProductPlacementX = source.ProductPlacementX;
            node.ProductPlacementY = source.ProductPlacementY;
            node.ProductPlacementZ = source.ProductPlacementZ;
            node.Products = source.Products.Select(AsGraphNode);
            node.RenumberingStrategy = AsGraphNode(source.RenumberingStrategy);
            node.Sequence = AsGraphNode(source.Sequence);
            node.SourcePath = source.SourcePath;
            node.Status = source.Status;
            node.UserName = source.UserName;
            node.UniqueContentReference = source.UniqueContentReference;
            node.ValidationTemplate = AsGraphNode(source.ValidationTemplate);
            node.VolumeUnitsOfMeasure = source.VolumeUnitsOfMeasure;
            node.WeightUnitsOfMeasure = source.WeightUnitsOfMeasure;
            node.Width = source.Width;
            return node;
        }

        private static PlanogramMetaDataImageNode AsGraphNode(PlanogramMetadataImage source)
        {
            var node = new PlanogramMetaDataImageNode();
            node.Id = source.Id;
            node.FileName = source.FileName;
            node.Description = source.Description;
            node.ImageData = source.ImageData;
            return node;
        }

        private static PlanogramImageNode AsGraphNode(PlanogramImage source)
        {
            return new PlanogramImageNode {Id = source.Id, FileName = source.FileName, Description = source.Description};
        }

        private static PlanogramFixtureNode AsGraphNode(PlanogramFixture source)
        {
            return new PlanogramFixtureNode
                   {
                       Id = source.Id,
                       Name = source.Name,
                       Height = source.Height,
                       Width = source.Width,
                       Depth = source.Depth,
                       Assemblies = source.Assemblies.Select(AsGraphNode).ToList(),
                       Components = source.Components.Select(AsGraphNode).ToList(),
                       MetaNumberOfMerchandisedSubComponents = source.MetaNumberOfMerchandisedSubComponents,
                       MetaTotalFixtureCost = source.MetaTotalFixtureCost
                   };
        }

        private static PlanogramFixtureAssemblyNode AsGraphNode(PlanogramFixtureAssembly source)
        {
            return new PlanogramFixtureAssemblyNode
                   {
                       Id = source.Id,
                       PlanogramAssemblyId = source.PlanogramAssemblyId,
                       X = source.X,
                       Y = source.Y,
                       Z = source.Z,
                       Slope = source.Slope,
                       Angle = source.Angle,
                       Roll = source.Roll,
                       MetaComponentCount = source.MetaComponentCount,
                       MetaTotalMerchandisableLinearSpace = source.MetaTotalMerchandisableLinearSpace,
                       MetaTotalMerchandisableAreaSpace = source.MetaTotalMerchandisableAreaSpace,
                       MetaTotalMerchandisableVolumetricSpace = source.MetaTotalMerchandisableVolumetricSpace,
                       MetaTotalLinearWhiteSpace = source.MetaTotalLinearWhiteSpace,
                       MetaTotalVolumetricWhiteSpace = source.MetaTotalVolumetricWhiteSpace,
                       MetaProductsPlaced = source.MetaProductsPlaced,
                       MetaNewProducts = source.MetaNewProducts,
                       MetaChangesFromPreviousCount = source.MetaChangesFromPreviousCount,
                       MetaChangeFromPreviousStarRating = source.MetaChangeFromPreviousStarRating,
                       MetaBlocksDropped = source.MetaBlocksDropped,
                       MetaNotAchievedInventory = source.MetaNotAchievedInventory,
                       MetaTotalFacings = source.MetaTotalFacings,
                       MetaAverageFacings = source.MetaAverageFacings,
                       MetaTotalUnits = source.MetaTotalUnits,
                       MetaAverageUnits = source.MetaAverageUnits,
                       MetaMinDos = source.MetaMinDos,
                       MetaMaxDos = source.MetaMaxDos,
                       MetaAverageDos = source.MetaAverageDos,
                       MetaMinCases = source.MetaMinCases,
                       MetaAverageCases = source.MetaAverageCases,
                       MetaMaxCases = source.MetaMaxCases,
                       MetaSpaceToUnitsIndex = source.MetaSpaceToUnitsIndex,
                       MetaTotalFrontFacings = source.MetaTotalFrontFacings,
                       MetaAverageFrontFacings = source.MetaAverageFrontFacings
                   };
        }

        private static PlanogramFixtureComponentNode AsGraphNode(PlanogramFixtureComponent source)
        {
            return new PlanogramFixtureComponentNode
                   {
                       Id = source.Id,
                       PlanogramComponentId = source.PlanogramComponentId,
                       ComponentSequenceNumber = source.ComponentSequenceNumber,
                       X = source.X,
                       Y = source.Y,
                       Z = source.Z,
                       Slope = source.Slope,
                       Angle = source.Angle,
                       Roll = source.Roll,
                       MetaTotalMerchandisableLinearSpace = source.MetaTotalMerchandisableLinearSpace,
                       MetaTotalMerchandisableAreaSpace = source.MetaTotalMerchandisableAreaSpace,
                       MetaTotalMerchandisableVolumetricSpace = source.MetaTotalMerchandisableVolumetricSpace,
                       MetaTotalLinearWhiteSpace = source.MetaTotalLinearWhiteSpace,
                       MetaTotalAreaWhiteSpace = source.MetaTotalAreaWhiteSpace,
                       MetaTotalVolumetricWhiteSpace = source.MetaTotalVolumetricWhiteSpace,
                       MetaProductsPlaced = source.MetaProductsPlaced,
                       MetaNewProducts = source.MetaNewProducts,
                       MetaChangesFromPreviousCount = source.MetaChangesFromPreviousCount,
                       MetaChangeFromPreviousStarRating = source.MetaChangeFromPreviousStarRating,
                       MetaBlocksDropped = source.MetaBlocksDropped,
                       MetaNotAchievedInventory = source.MetaNotAchievedInventory,
                       MetaTotalFacings = source.MetaTotalFacings,
                       MetaAverageFacings = source.MetaAverageFacings,
                       MetaTotalUnits = source.MetaTotalUnits,
                       MetaAverageUnits = source.MetaAverageUnits,
                       MetaMinDos = source.MetaMinDos,
                       MetaMaxDos = source.MetaMaxDos,
                       MetaAverageDos = source.MetaAverageDos,
                       MetaMinCases = source.MetaMinCases,
                       MetaAverageCases = source.MetaAverageCases,
                       MetaMaxCases = source.MetaMaxCases,
                       MetaSpaceToUnitsIndex = source.MetaSpaceToUnitsIndex,
                       MetaIsComponentSlopeWithNoRiser = source.MetaIsComponentSlopeWithNoRiser,
                       MetaIsOverMerchandisedDepth = source.MetaIsOverMerchandisedDepth,
                       MetaIsOverMerchandisedHeight = source.MetaIsOverMerchandisedHeight,
                       MetaIsOverMerchandisedWidth = source.MetaIsOverMerchandisedWidth,
                       MetaTotalComponentCollisions = source.MetaTotalComponentCollisions,
                       MetaTotalPositionCollisions = source.MetaTotalPositionCollisions,
                       MetaTotalFrontFacings = source.MetaTotalFrontFacings,
                       MetaAverageFrontFacings = source.MetaAverageFrontFacings,
                       MetaPercentageLinearSpaceFilled = source.MetaPercentageLinearSpaceFilled,
                       MetaWorldX = source.MetaWorldX,
                       MetaWorldY = source.MetaWorldY,
                       MetaWorldZ = source.MetaWorldZ,
                       MetaWorldAngle = source.MetaWorldAngle,
                       MetaWorldSlope = source.MetaWorldSlope,
                       MetaWorldRoll = source.MetaWorldRoll,
                       MetaIsOutsideOfFixtureArea = source.MetaIsOutsideOfFixtureArea,
                       NotchNumber = source.NotchNumber
                   };
        }

        private static PlanogramFixtureItemNode AsGraphNode(PlanogramFixtureItem source)
        {
            return new PlanogramFixtureItemNode
                   {
                       Id = source.Id,
                       MetaTotalAreaWhiteSpace = source.MetaTotalAreaWhiteSpace,
                       Angle = source.Angle,
                       BaySequenceNumber = source.BaySequenceNumber,
                       MetaAverageCases = source.MetaAverageCases,
                       MetaAverageDos = source.MetaAverageDos,
                       MetaAverageFacings = source.MetaAverageFacings,
                       MetaAverageUnits = source.MetaAverageUnits,
                       MetaAverageFrontFacings = source.MetaAverageFrontFacings,
                       MetaBlocksDropped = source.MetaBlocksDropped,
                       MetaChangeFromPreviousStarRating = source.MetaChangeFromPreviousStarRating,
                       MetaChangesFromPreviousCount = source.MetaChangesFromPreviousCount,
                       MetaComponentCount = source.MetaComponentCount,
                       MetaMaxCases = source.MetaMaxCases,
                       MetaMaxDos = source.MetaMaxDos,
                       MetaMinCases = source.MetaMinCases,
                       MetaMinDos = source.MetaMinDos,
                       MetaNewProducts = source.MetaNewProducts,
                       MetaNotAchievedInventory = source.MetaNotAchievedInventory,
                       MetaProductsPlaced = source.MetaProductsPlaced,
                       MetaSpaceToUnitsIndex = source.MetaSpaceToUnitsIndex,
                       MetaTotalComponentCollisions = source.MetaTotalComponentCollisions,
                       MetaTotalComponentsOverMerchandisedDepth = source.MetaTotalComponentsOverMerchandisedDepth,
                       MetaTotalComponentsOverMerchandisedHeight = source.MetaTotalComponentsOverMerchandisedHeight,
                       MetaTotalComponentsOverMerchandisedWidth = source.MetaTotalComponentsOverMerchandisedWidth,
                       MetaTotalFacings = source.MetaTotalFacings,
                       MetaTotalFrontFacings = source.MetaTotalFrontFacings,
                       MetaTotalLinearWhiteSpace = source.MetaTotalLinearWhiteSpace,
                       MetaTotalMerchandisableAreaSpace = source.MetaTotalMerchandisableAreaSpace,
                       MetaTotalMerchandisableLinearSpace = source.MetaTotalMerchandisableLinearSpace,
                       MetaTotalMerchandisableVolumetricSpace = source.MetaTotalMerchandisableVolumetricSpace,
                       MetaTotalPositionCollisions = source.MetaTotalPositionCollisions,
                       MetaTotalUnits = source.MetaTotalUnits,
                       PercentageWidth = source.PercentageWidth,
                       PercentageX = source.PercentageX,
                       PlanogramFixtureId = source.PlanogramFixtureId,
                       Roll = source.Roll,
                       Slope = source.Slope,
                       X = source.X,
                       Y = source.Y,
                       Z = source.Z
                   };
        }

        private static PlanogramAssemblyNode AsGraphNode(PlanogramAssembly source)
        {
            return new PlanogramAssemblyNode
                   {
                       Id = source.Id,
                       Name = source.Name,
                       Components = source.Components.Select(AsGraphNode).ToList(),
                       Depth = source.Depth,
                       Height = source.Height,
                       NumberOfComponents = source.NumberOfComponents,
                       TotalComponentCost = source.TotalComponentCost
                   };
        }

        private static PlanogramAssemblyComponentNode AsGraphNode(PlanogramAssemblyComponent source)
        {
            return new PlanogramAssemblyComponentNode
                   {
                       Id = source.Id,
                       Angle = source.Angle,
                       ComponentSequenceNumber = source.ComponentSequenceNumber,
                       MetaAverageCases = source.MetaAverageCases,
                       MetaAverageDos = source.MetaAverageDos,
                       MetaAverageFacings = source.MetaAverageFacings,
                       MetaAverageFrontFacings = source.MetaAverageFrontFacings,
                       MetaAverageUnits = source.MetaAverageUnits,
                       MetaBlocksDropped = source.MetaBlocksDropped,
                       MetaChangeFromPreviousStarRating = source.MetaChangeFromPreviousStarRating,
                       MetaChangesFromPreviousCount = source.MetaChangesFromPreviousCount,
                       MetaIsComponentSlopeWithNoRiser = source.MetaIsComponentSlopeWithNoRiser,
                       MetaIsOutsideOfFixtureArea = source.MetaIsOutsideOfFixtureArea,
                       MetaIsOverMerchandisedDepth = source.MetaIsOverMerchandisedDepth,
                       MetaIsOverMerchandisedHeight = source.MetaIsOverMerchandisedHeight,
                       MetaIsOverMerchandisedWidth = source.MetaIsOverMerchandisedWidth,
                       MetaMaxCases = source.MetaMaxCases,
                       MetaMaxDos = source.MetaMaxDos,
                       MetaMinCases = source.MetaMinCases,
                       MetaMinDos = source.MetaMinDos,
                       MetaNewProducts = source.MetaNewProducts,
                       MetaNotAchievedInventory = source.MetaNotAchievedInventory,
                       MetaPercentageLinearSpaceFilled = source.MetaPercentageLinearSpaceFilled,
                       MetaProductsPlaced = source.MetaProductsPlaced,
                       MetaSpaceToUnitsIndex = source.MetaSpaceToUnitsIndex,
                       MetaTotalAreaWhiteSpace = source.MetaTotalAreaWhiteSpace,
                       MetaTotalComponentCollisions = source.MetaTotalComponentCollisions,
                       MetaTotalFacings = source.MetaTotalFacings,
                       MetaTotalFrontFacings = source.MetaTotalFrontFacings,
                       MetaTotalLinearWhiteSpace = source.MetaTotalLinearWhiteSpace,
                       MetaTotalMerchandisableAreaSpace = source.MetaTotalMerchandisableAreaSpace,
                       MetaTotalMerchandisableLinearSpace = source.MetaTotalMerchandisableLinearSpace,
                       MetaTotalMerchandisableVolumetricSpace = source.MetaTotalMerchandisableVolumetricSpace,
                       MetaTotalPositionCollisions = source.MetaTotalPositionCollisions,
                       MetaTotalUnits = source.MetaTotalUnits,
                       MetaTotalVolumetricWhiteSpace = source.MetaTotalVolumetricWhiteSpace,
                       MetaWorldAngle = source.MetaWorldAngle,
                       MetaWorldRoll = source.MetaWorldRoll,
                       MetaWorldSlope = source.MetaWorldSlope,
                       MetaWorldX = source.MetaWorldX,
                       MetaWorldY = source.MetaWorldY,
                       MetaWorldZ = source.MetaWorldZ,
                       NotchNumber = source.NotchNumber,
                       PlanogramComponentId = source.PlanogramComponentId,
                       Roll = source.Roll,
                       Slope = source.Slope,
                       X = source.X,
                       Y = source.Y,
                       Z = source.Z
                   };
        }

        private static PlanogramValidationTemplateNode AsGraphNode(PlanogramValidationTemplate source)
        {
            return new PlanogramValidationTemplateNode {Id = source.Id, Name = source.Name, Groups = source.Groups.Select(AsGraphNode)};
        }

        private static PlanogramValidationTemplateGroupNode AsGraphNode(PlanogramValidationTemplateGroup source)
        {
            return new PlanogramValidationTemplateGroupNode
                   {
                       Id = source.Id,
                       Name = source.Name,
                       Threshold1 = source.Threshold1,
                       Threshold2 = source.Threshold2,
                       ResultType = source.ResultType,
                       Metrics = source.Metrics.Select(AsGraphNode),
                       ValidationType = source.ValidationType
                   };
        }

        private static PlanogramValidationTemplateGroupMetricNode AsGraphNode(PlanogramValidationTemplateGroupMetric source)
        {
            return new PlanogramValidationTemplateGroupMetricNode
                   {
                       Id = source.Id,
                       Field = source.Field,
                       Threshold1 = source.Threshold1,
                       Threshold2 = source.Threshold2,
                       Score1 = source.Score1,
                       Score2 = source.Score2,
                       Score3 = source.Score3,
                       AggregationType = source.AggregationType,
                       ResultType = source.ResultType,
                       ValidationType = source.ValidationType,
                       Criteria = source.Criteria
                   };
        }

        private static PlanogramSequenceNode AsGraphNode(PlanogramSequence source)
        {
            return new PlanogramSequenceNode
                   {
                       Id = source.Id,
                       ProductLinks = source.ProductLinks.Select(AsGraphNode).ToList(),
                       Groups = source.Groups.Select(AsGraphNode).ToList()
                   };
        }

        private static PlanogramSequenceProductLinkNode AsGraphNode(PlanogramSequenceProductLink source)
        {
            return new PlanogramSequenceProductLinkNode
                   {
                       AnchorSequenceGroupProductId = source.AnchorSequenceGroupProductId,
                       TargetSequenceGroupProductId = source.TargetSequenceGroupProductId,
                       Direction = source.Direction
                   };
        }

        private static PlanogramSequenceGroupNode AsGraphNode(PlanogramSequenceGroup source)
        {
            return new PlanogramSequenceGroupNode
                   {
                       Id = source.Id,
                       Colour = source.Colour,
                       Products = source.Products.Select(AsGraphNode).ToList(),
                       SubGroups = source.SubGroups.Select(AsGraphNode).ToList()
                   };
        }

        private static PlanogramSequenceGroupProductNode AsGraphNode(PlanogramSequenceGroupProduct source)
        {
            // The Product property on the source type should not be copied over as it is a copy used in some processes, not a real instance, but some sort of reference.
            return new PlanogramSequenceGroupProductNode
                   {
                       Id = source.Id,
                       Gtin = source.Gtin,
                       SequenceNumber = source.SequenceNumber,
                       PlanogramSequenceGroupSubGroupId = source.PlanogramSequenceGroupSubGroupId
                   };
        }

        private static PlanogramSequenceGroupSubGroupNode AsGraphNode(PlanogramSequenceGroupSubGroup source)
        {
            return new PlanogramSequenceGroupSubGroupNode {Id = source.Id, Name = source.Name, Alignment = source.Alignment};
        }

        private static PlanogramRenumberingStrategyNode AsGraphNode(PlanogramRenumberingStrategy source)
        {
            return new PlanogramRenumberingStrategyNode
                   {
                       Id = source.Id,
                       UniqueNumberMultiPositionProductsPerComponent =
                           source.UniqueNumberMultiPositionProductsPerComponent,
                       RestartPositionRenumberingPerComponent = source.RestartPositionRenumberingPerComponent,
                       RestartComponentRenumberingPerBay = source.RestartComponentRenumberingPerBay,
                       PositionZRenumberStrategyPriority = source.PositionZRenumberStrategyPriority,
                       PositionZRenumberStrategy = source.PositionZRenumberStrategy,
                       PositionYRenumberStrategyPriority = source.PositionYRenumberStrategyPriority,
                       PositionYRenumberStrategy = source.PositionYRenumberStrategy,
                       PositionXRenumberStrategyPriority = source.PositionXRenumberStrategyPriority,
                       PositionXRenumberStrategy = source.PositionXRenumberStrategy,
                       Name = source.Name,
                       IsEnabled = source.IsEnabled,
                       IgnoreNonMerchandisingComponents = source.IgnoreNonMerchandisingComponents,
                       ExceptAdjacentPositions = source.ExceptAdjacentPositions,
                       BayComponentZRenumberStrategyPriority = source.BayComponentZRenumberStrategyPriority,
                       BayComponentZRenumberStrategy = source.BayComponentZRenumberStrategy,
                       BayComponentYRenumberStrategyPriority = source.BayComponentYRenumberStrategyPriority,
                       BayComponentYRenumberStrategy = source.BayComponentYRenumberStrategy,
                       BayComponentXRenumberStrategyPriority = source.BayComponentXRenumberStrategyPriority
                   };
        }

        private static PlanogramPerformanceNode AsGraphNode(PlanogramPerformance source)
        {
            return new PlanogramPerformanceNode
                   {
                       Id = source.Id,
                       PerformanceData = source.PerformanceData.Select(AsGraphNode).ToList(),
                       Name = source.Name,
                       Metrics = source.Metrics.Select(AsGraphNode).ToList(),
                       Description = source.Description
                   };
        }

        private static PlanogramPerformanceMetricNode AsGraphNode(PlanogramPerformanceMetric source)
        {
            return new PlanogramPerformanceMetricNode
                   {
                       Id = source.Id,
                       Type = source.Type,
                       SpecialType = source.SpecialType,
                       Name = source.Name,
                       MetricId = source.MetricId,
                       Direction = source.Direction,
                       Description = source.Description,
                       AggregationType = source.AggregationType
                   };
        }

        private static PlanogramInventoryNode AsGraphNode(PlanogramInventory source)
        {
            return new PlanogramInventoryNode
                   {
                       Id = source.Id,
                       MinDos = source.MinDos,
                       MinDeliveries = source.MinDeliveries,
                       MinCasePacks = source.MinCasePacks,
                       IsShelfLifeValidated = source.IsShelfLifeValidated,
                       IsDosValidated = source.IsDosValidated,
                       IsDeliveriesValidated = source.IsDeliveriesValidated,
                       IsCasePacksValidated = source.IsCasePacksValidated,
                       InventoryMetricType = source.InventoryMetricType,
                       DaysOfPerformance = source.DaysOfPerformance,
                       MinShelfLife = source.MinShelfLife
                   };
        }

        private static PlanogramComparisonNode AsGraphNode(PlanogramComparison source)
        {
            return new PlanogramComparisonNode
                   {
                       ComparisonFields = source.ComparisonFields.Select(AsGraphNode).ToList(),
                       DataOrderType = source.DataOrderType,
                       DateLastCompared = source.DateLastCompared,
                       Description = source.Description,
                       IgnoreNonPlacedProducts = source.IgnoreNonPlacedProducts,
                       Name = source.Name,
                       Results = source.Results.Select(AsGraphNode).ToList(),
                       Id = source.Id
                   };
        }

        private static PlanogramComparisonResultNode AsGraphNode(PlanogramComparisonResult source)
        {
            return new PlanogramComparisonResultNode
                   {
                       Id = source.Id,
                       PlanogramName = source.PlanogramName,
                       Status = source.Status,
                       ComparedItems = source.ComparedItems.Select(AsGraphNode).ToList()
                   };
        }

        private static PlanogramComparisonItemNode AsGraphNode(PlanogramComparisonItem source)
        {
            return new PlanogramComparisonItemNode
                   {
                       Id = source.Id,
                       ItemType = source.ItemType,
                       Status = source.Status,
                       FieldValues = source.FieldValues.Select(AsGraphNode).ToList()
                   };
        }

        private static PlanogramComparisonFieldValueNode AsGraphNode(PlanogramComparisonFieldValue source)
        {
            return new PlanogramComparisonFieldValueNode {Id = source.Id, FieldPlaceholder = source.FieldPlaceholder, Value = source.Value};
        }

        private static PlanogramComparisonFieldNode AsGraphNode(PlanogramComparisonField source)
        {
            return new PlanogramComparisonFieldNode
                   {
                       Id = source.Id,
                       ItemType = source.ItemType,
                       FieldPlaceholder = source.FieldPlaceholder,
                       DisplayName = source.DisplayName,
                       Number = source.Number,
                       Display = source.Display
                   };
        }

        private static CustomAttributeDataNode AsGraphNode(CustomAttributeData source)
        {
            return new CustomAttributeDataNode
                   {
                       Date1 = source.Date1,
                       Date2 = source.Date2,
                       Date3 = source.Date3,
                       Date4 = source.Date4,
                       Date5 = source.Date5,
                       Date6 = source.Date6,
                       Date7 = source.Date7,
                       Date8 = source.Date8,
                       Date9 = source.Date9,
                       Date10 = source.Date10,
                       Flag1 = source.Flag1,
                       Flag2 = source.Flag2,
                       Flag3 = source.Flag3,
                       Flag4 = source.Flag4,
                       Flag5 = source.Flag5,
                       Flag6 = source.Flag6,
                       Flag7 = source.Flag7,
                       Flag8 = source.Flag8,
                       Flag9 = source.Flag9,
                       Flag10 = source.Flag10,
                       Id = source.Id,
                       Text1 = source.Text1,
                       Text2 = source.Text2,
                       Text3 = source.Text3,
                       Text4 = source.Text4,
                       Text5 = source.Text5,
                       Text6 = source.Text6,
                       Text7 = source.Text7,
                       Text8 = source.Text8,
                       Text9 = source.Text9,
                       Text10 = source.Text10,
                       Text11 = source.Text11,
                       Text12 = source.Text12,
                       Text13 = source.Text13,
                       Text14 = source.Text14,
                       Text15 = source.Text15,
                       Text16 = source.Text16,
                       Text17 = source.Text17,
                       Text18 = source.Text18,
                       Text19 = source.Text19,
                       Text20 = source.Text20,
                       Text21 = source.Text21,
                       Text22 = source.Text22,
                       Text23 = source.Text23,
                       Text24 = source.Text24,
                       Text25 = source.Text25,
                       Text26 = source.Text26,
                       Text27 = source.Text27,
                       Text28 = source.Text28,
                       Text29 = source.Text29,
                       Text30 = source.Text30,
                       Text31 = source.Text31,
                       Text32 = source.Text32,
                       Text33 = source.Text33,
                       Text34 = source.Text34,
                       Text35 = source.Text35,
                       Text36 = source.Text36,
                       Text37 = source.Text37,
                       Text38 = source.Text38,
                       Text39 = source.Text39,
                       Text40 = source.Text40,
                       Text41 = source.Text41,
                       Text42 = source.Text42,
                       Text43 = source.Text43,
                       Text44 = source.Text44,
                       Text45 = source.Text45,
                       Text46 = source.Text46,
                       Text47 = source.Text47,
                       Text48 = source.Text48,
                       Text49 = source.Text49,
                       Text50 = source.Text50,
                       Value1 = source.Value1,
                       Value2 = source.Value2,
                       Value3 = source.Value3,
                       Value4 = source.Value4,
                       Value5 = source.Value5,
                       Value6 = source.Value6,
                       Value7 = source.Value7,
                       Value8 = source.Value8,
                       Value9 = source.Value9,
                       Value10 = source.Value10,
                       Value11 = source.Value11,
                       Value12 = source.Value12,
                       Value13 = source.Value13,
                       Value14 = source.Value14,
                       Value15 = source.Value15,
                       Value16 = source.Value16,
                       Value17 = source.Value17,
                       Value18 = source.Value18,
                       Value19 = source.Value19,
                       Value20 = source.Value20,
                       Value21 = source.Value21,
                       Value22 = source.Value22,
                       Value23 = source.Value23,
                       Value24 = source.Value24,
                       Value25 = source.Value25,
                       Value26 = source.Value26,
                       Value27 = source.Value27,
                       Value28 = source.Value28,
                       Value29 = source.Value29,
                       Value30 = source.Value30,
                       Value31 = source.Value31,
                       Value32 = source.Value32,
                       Value33 = source.Value33,
                       Value34 = source.Value34,
                       Value35 = source.Value35,
                       Value36 = source.Value36,
                       Value37 = source.Value37,
                       Value38 = source.Value38,
                       Value39 = source.Value39,
                       Value40 = source.Value40,
                       Value41 = source.Value41,
                       Value42 = source.Value42,
                       Value43 = source.Value43,
                       Value44 = source.Value44,
                       Value45 = source.Value45,
                       Value46 = source.Value46,
                       Value47 = source.Value47,
                       Value48 = source.Value48,
                       Value49 = source.Value49,
                       Value50 = source.Value50
                   };
        }

        private static PlanogramAssortmentNode AsGraphNode(PlanogramAssortment source)
        {
            return new PlanogramAssortmentNode
                   {
                       InventoryRules = source.InventoryRules.Select(AsGraphNode).ToList(),
                       Id = source.Id,
                       LocalProducts = source.LocalProducts.Select(AsGraphNode).ToList(),
                       LocationBuddies = source.LocationBuddies.Select(AsGraphNode).ToList(),
                       Name = source.Name,
                       Products = source.Products.Select(AsGraphNode).ToList(),
                       ProductBuddies = source.ProductBuddies.Select(AsGraphNode).ToList(),
                       Regions = source.Regions.Select(AsGraphNode).ToList()
                   };
        }

        private static PlanogramAssortmentProductBuddyNode AsGraphNode(PlanogramAssortmentProductBuddy source)
        {
            return new PlanogramAssortmentProductBuddyNode
                   {
                       Id = source.Id,
                       ProductGtin = source.ProductGtin,
                       SourceType = source.SourceType,
                       TreatmentType = source.TreatmentType,
                       TreatmentTypePercentage = source.TreatmentTypePercentage,
                       ProductAttributeType = source.ProductAttributeType,
                       S1Percentage = source.S1Percentage,
                       S1ProductGtin = source.S1ProductGtin,
                       S2Percentage = source.S2Percentage,
                       S2ProductGtin = source.S2ProductGtin,
                       S3Percentage = source.S3Percentage,
                       S3ProductGtin = source.S3ProductGtin,
                       S4ProductGtin = source.S4ProductGtin,
                       S4Percentage = source.S4Percentage,
                       S5Percentage = source.S5Percentage,
                       S5ProductGtin = source.S5ProductGtin
                   };
        }

        private static PlanogramAssortmentRegionNode AsGraphNode(PlanogramAssortmentRegion source)
        {
            return new PlanogramAssortmentRegionNode
                   {
                       Id = source.Id,
                       Name = source.Name,
                       Locations = source.Locations.Select(AsGraphNode).ToList(),
                       Products = source.Products.Select(AsGraphNode).ToList()
                   };
        }

        private static PlanogramAssortmentRegionProductNode AsGraphNode(PlanogramAssortmentRegionProduct source)
        {
            return new PlanogramAssortmentRegionProductNode
                   {
                       Id = source.Id,
                       PrimaryProductGtin = source.PrimaryProductGtin,
                       RegionalProductGtin = source.RegionalProductGtin
                   };
        }

        private static PlanogramAssortmentRegionLocationNode AsGraphNode(PlanogramAssortmentRegionLocation source)
        {
            return new PlanogramAssortmentRegionLocationNode {Id = source.Id, LocationCode = source.LocationCode};
        }

        private static PlanogramAssortmentLocationBuddyNode AsGraphNode(PlanogramAssortmentLocationBuddy source)
        {
            return new PlanogramAssortmentLocationBuddyNode
                   {
                       Id = source.Id,
                       LocationCode = source.LocationCode,
                       TreatmentType = source.TreatmentType,
                       S1LocationCode = source.S1LocationCode,
                       S1Percentage = source.S1Percentage,
                       S2LocationCode = source.S2LocationCode,
                       S2Percentage = source.S2Percentage,
                       S3LocationCode = source.S3LocationCode,
                       S3Percentage = source.S3Percentage,
                       S4LocationCode = source.S4LocationCode,
                       S4Percentage = source.S4Percentage
                   };
        }

        private static PlanogramAssortmentProductNode AsGraphNode(PlanogramAssortmentProduct source)
        {
            return new PlanogramAssortmentProductNode
                   {
                       Id = source.Id,
                       Gtin = source.Gtin,
                       Name = source.Name,
                       IsRanged = source.IsRanged,
                       Rank = source.Rank,
                       Segmentation = source.Segmentation,
                       Facings = source.Facings,
                       Units = source.Units,
                       ProductTreatmentType = source.ProductTreatmentType,
                       ProductLocalizationType = source.ProductLocalizationType,
                       Comments = source.Comments,
                       ExactListFacings = source.ExactListFacings,
                       ExactListUnits = source.ExactListUnits,
                       PreserveListFacings = source.PreserveListFacings,
                       PreserveListUnits = source.PreserveListUnits,
                       MaxListUnits = source.MaxListUnits,
                       MinListFacings = source.MinListFacings,
                       MaxListFacings = source.MaxListFacings,
                       MinListUnits = source.MinListUnits,
                       FamilyRuleName = source.FamilyRuleName,
                       FamilyRuleType = source.FamilyRuleType,
                       FamilyRuleValue = source.FamilyRuleValue,
                       IsPrimaryRegionalProduct = source.IsPrimaryRegionalProduct,
                       MetaIsProductPlacedOnPlanogram = source.MetaIsProductPlacedOnPlanogram,
                       DelistedDueToAssortmentRule = source.DelistedDueToAssortmentRule,
                       RuleMaximumValue = source.RuleMaximumValue,
                       RuleMaximumValueType = source.RuleMaximumValueType,
                       RuleType = source.RuleType,
                       RuleValue = source.RuleValue,
                       RuleValueType = source.RuleValueType,
                       HasMaximum = source.HasMaximum,
                       RuleSummary = source.RuleSummary
                   };
        }

        private static PlanogramAssortmentInventoryRuleNode AsGraphNode(PlanogramAssortmentInventoryRule source)
        {
            return new PlanogramAssortmentInventoryRuleNode
                   {
                       Id = source.Id,
                       ProductGtin = source.ProductGtin,
                       CasePack = source.CasePack,
                       DaysOfSupply = source.DaysOfSupply,
                       ShelfLife = source.ShelfLife,
                       ReplenishmentDays = source.ReplenishmentDays,
                       WasteHurdleUnits = source.WasteHurdleUnits,
                       MinUnits = source.MinUnits,
                       WasteHurdleCasePack = source.WasteHurdleCasePack,
                       MinFacings = source.MinFacings
                   };
        }

        private static PlanogramAssortmentLocalProductNode AsGraphNode(PlanogramAssortmentLocalProduct source)
        {
            return new PlanogramAssortmentLocalProductNode {Id = source.Id, LocationCode = source.LocationCode, ProductGtin = source.ProductGtin};
        }

        private static PlanogramComponentNode AsGraphNode(PlanogramComponent source)
        {
            return new PlanogramComponentNode
                   {
                       Id = source.Id,
                       BarCode = source.BarCode,
                       CanAttachShelfEdgeLabel = source.CanAttachShelfEdgeLabel,
                       Capacity = source.Capacity,
                       ComponentType = source.ComponentType,
                       CustomAttributes = AsGraphNode(source.CustomAttributes),
                       Depth = source.Depth,
                       Diameter = source.Diameter,
                       Height = source.Height,
                       ImageIdBack = source.ImageIdBack,
                       ImageIdBottom = source.ImageIdBottom,
                       ImageIdFront = source.ImageIdFront,
                       ImageIdLeft = source.ImageIdLeft,
                       ImageIdRight = source.ImageIdRight,
                       ImageIdTop = source.ImageIdTop,
                       IsDisplayOnly = source.IsDisplayOnly,
                       IsMerchandisable = source.IsMerchandisable,
                       IsMerchandisedTopDown = source.IsMerchandisedTopDown,
                       IsMoveable = source.IsMoveable,
                       Manufacturer = source.Manufacturer,
                       ManufacturerPartName = source.ManufacturerPartName,
                       ManufacturerPartNumber = source.ManufacturerPartNumber,
                       Mesh3DId = source.Mesh3DId,
                       MetaNumberOfMerchandisedSubComponents = source.MetaNumberOfMerchandisedSubComponents,
                       MetaNumberOfSubComponents = source.MetaNumberOfSubComponents,
                       MinPurchaseQty = source.MinPurchaseQty,
                       Name = source.Name,
                       RetailerReferenceCode = source.RetailerReferenceCode,
                       SubComponents = source.SubComponents.Select(AsGraphNode).ToList(),
                       SupplierCostPrice = source.SupplierCostPrice,
                       SupplierDiscount = source.SupplierDiscount,
                       SupplierLeadTime = source.SupplierLeadTime,
                       SupplierName = source.SupplierName,
                       SupplierPartNumber = source.SupplierPartNumber,
                       Volume = source.Volume,
                       Weight = source.Weight,
                       WeightLimit = source.WeightLimit,
                       Width = source.Width
                   };
        }

        private static PlanogramSubComponentNode AsGraphNode(PlanogramSubComponent source)
        {
            return new PlanogramSubComponentNode
                   {
                       Id = source.Id,
                       Angle = source.Angle,
                       BackOverhang = source.BackOverhang,
                       BottomOverhang = source.BottomOverhang,
                       CombineType = source.CombineType,
                       Depth = source.Depth,
                       DividerObstructionDepth = source.DividerObstructionDepth,
                       DividerObstructionFillColour = source.DividerObstructionFillColour,
                       DividerObstructionFillPattern = source.DividerObstructionFillPattern,
                       DividerObstructionHeight = source.DividerObstructionHeight,
                       DividerObstructionSpacingX = source.DividerObstructionSpacingX,
                       DividerObstructionSpacingY = source.DividerObstructionSpacingY,
                       DividerObstructionSpacingZ = source.DividerObstructionSpacingZ,
                       DividerObstructionStartX = source.DividerObstructionStartX,
                       DividerObstructionStartY = source.DividerObstructionStartY,
                       DividerObstructionStartZ = source.DividerObstructionStartZ,
                       DividerObstructionWidth = source.DividerObstructionWidth,
                       FaceThicknessBack = source.FaceThicknessBack,
                       FaceThicknessBottom = source.FaceThicknessBottom,
                       FaceThicknessFront = source.FaceThicknessFront,
                       FaceThicknessLeft = source.FaceThicknessLeft,
                       FaceThicknessRight = source.FaceThicknessRight,
                       FaceThicknessTop = source.FaceThicknessTop,
                       FillColourBack = source.FillColourBack,
                       FillColourBottom = source.FillColourBottom,
                       FillColourFront = source.FillColourFront,
                       FillColourLeft = source.FillColourLeft,
                       FillColourRight = source.FillColourRight,
                       FillColourTop = source.FillColourTop,
                       FillPatternTypeBack = source.FillPatternTypeBack,
                       FillPatternTypeBottom = source.FillPatternTypeBottom,
                       FillPatternTypeFront = source.FillPatternTypeFront,
                       FillPatternTypeLeft = source.FillPatternTypeLeft,
                       FillPatternTypeRight = source.FillPatternTypeRight,
                       FillPatternTypeTop = source.FillPatternTypeTop,
                       FrontOverhang = source.FrontOverhang,
                       HasCollisionDetection = source.HasCollisionDetection,
                       Height = source.Height,
                       ImageIdBack = source.ImageIdBack,
                       ImageIdBottom = source.ImageIdBottom,
                       ImageIdFront = source.ImageIdFront,
                       ImageIdLeft = source.ImageIdLeft,
                       ImageIdRight = source.ImageIdRight,
                       ImageIdTop = source.ImageIdTop,
                       IsDividerObstructionAtEnd = source.IsDividerObstructionAtEnd,
                       IsDividerObstructionAtStart = source.IsDividerObstructionAtStart,
                       IsDividerObstructionByFacing = source.IsDividerObstructionByFacing,
                       IsInitialized = source.IsInitialized,
                       IsNotchPlacedOnBack = source.IsNotchPlacedOnBack,
                       IsNotchPlacedOnFront = source.IsNotchPlacedOnFront,
                       IsNotchPlacedOnLeft = source.IsNotchPlacedOnLeft,
                       IsNotchPlacedOnRight = source.IsNotchPlacedOnRight,
                       IsProductOverlapAllowed = source.IsProductOverlapAllowed,
                       IsProductSqueezeAllowed = source.IsProductSqueezeAllowed,
                       IsRiserPlacedOnBack = source.IsRiserPlacedOnBack,
                       IsRiserPlacedOnFront = source.IsRiserPlacedOnFront,
                       IsRiserPlacedOnLeft = source.IsRiserPlacedOnLeft,
                       IsRiserPlacedOnRight = source.IsRiserPlacedOnRight,
                       IsVisible = source.IsVisible,
                       LeftOverhang = source.LeftOverhang,
                       LineColour = source.LineColour,
                       LineThickness = source.LineThickness,
                       MerchandisableDepth = source.MerchandisableDepth,
                       MerchandisableHeight = source.MerchandisableHeight,
                       MerchandisingStrategyX = source.MerchandisingStrategyX,
                       MerchandisingStrategyY = source.MerchandisingStrategyY,
                       MerchandisingStrategyZ = source.MerchandisingStrategyZ,
                       MerchandisingType = source.MerchandisingType,
                       MerchConstraintRow1Height = source.MerchConstraintRow1Height,
                       MerchConstraintRow1SpacingX = source.MerchConstraintRow1SpacingX,
                       MerchConstraintRow1SpacingY = source.MerchConstraintRow1SpacingY,
                       MerchConstraintRow1StartX = source.MerchConstraintRow1StartX,
                       MerchConstraintRow1StartY = source.MerchConstraintRow1StartY,
                       MerchConstraintRow1Width = source.MerchConstraintRow1Width,
                       MerchConstraintRow2Height = source.MerchConstraintRow2Height,
                       MerchConstraintRow2SpacingX = source.MerchConstraintRow2SpacingX,
                       MerchConstraintRow2SpacingY = source.MerchConstraintRow2SpacingY,
                       MerchConstraintRow2StartX = source.MerchConstraintRow2StartX,
                       MerchConstraintRow2StartY = source.MerchConstraintRow2StartY,
                       MerchConstraintRow2Width = source.MerchConstraintRow2Width,
                       Mesh3DId = source.Mesh3DId,
                       Name = source.Name,
                       NotchHeight = source.NotchHeight,
                       NotchSpacingX = source.NotchSpacingX,
                       NotchSpacingY = source.NotchSpacingY,
                       NotchStartX = source.NotchStartX,
                       NotchStartY = source.NotchStartY,
                       NotchStyleType = source.NotchStyleType,
                       NotchWidth = source.NotchWidth,
                       RightOverhang = source.RightOverhang,
                       RiserColour = source.RiserColour,
                       RiserFillPatternType = source.RiserFillPatternType,
                       RiserHeight = source.RiserHeight,
                       RiserThickness = source.RiserThickness,
                       RiserTransparencyPercent = source.RiserTransparencyPercent,
                       Roll = source.Roll,
                       ShapeType = source.ShapeType,
                       Slope = source.Slope,
                       TopOverhang = source.TopOverhang,
                       TransparencyPercentBack = source.TransparencyPercentBack,
                       TransparencyPercentBottom = source.TransparencyPercentBottom,
                       TransparencyPercentFront = source.TransparencyPercentFront,
                       TransparencyPercentLeft = source.TransparencyPercentLeft,
                       TransparencyPercentRight = source.TransparencyPercentRight,
                       TransparencyPercentTop = source.TransparencyPercentTop,
                       Width = source.Width,
                       X = source.X,
                       Y = source.Y,
                       Z = source.Z
                   };
        }

        private static PlanogramBlockingNode AsGraphNode(PlanogramBlocking source)
        {
            return new PlanogramBlockingNode
                   {
                       Id = source.Id,
                       Dividers = source.Dividers.Select(AsGraphNode).ToList(),
                       Groups = source.Groups.Select(AsGraphNode).ToList(),
                       Locations = source.Locations.Select(AsGraphNode).ToList(),
                       Type = source.Type
                   };
        }

        private static PlanogramBlockingDividerNode AsGraphNode(PlanogramBlockingDivider source)
        {
            return new PlanogramBlockingDividerNode
                   {
                       Id = source.Id,
                       IsLimited = source.IsLimited,
                       IsSnapped = source.IsSnapped,
                       Length = source.Length,
                       Level = source.Level,
                       LimitedPercentage = source.LimitedPercentage,
                       Type = source.Type,
                       X = source.X,
                       Y = source.Y
                   };
        }

        private static PlanogramBlockingGroupNode AsGraphNode(PlanogramBlockingGroup source)
        {
            return new PlanogramBlockingGroupNode
                   {
                       Id = source.Id,
                       BlockPercentageIsChanged = source.BlockPercentageIsChanged,
                       BlockPlacementPrimaryType = source.BlockPlacementPrimaryType,
                       BlockPlacementSecondaryType = source.BlockPlacementSecondaryType,
                       BlockPlacementTertiaryType = source.BlockPlacementTertiaryType,
                       BlockSpaceLimitedPercentage = source.BlockSpaceLimitedPercentage,
                       CanCompromiseSequence = source.CanCompromiseSequence,
                       CanMerge = source.CanMerge,
                       CanOptimisePercentage = source.CanOptimisePercentage,
                       Colour = source.Colour,
                       FillPatternType = source.FillPatternType,
                       IsLimited = source.IsLimited,
                       IsRestrictedByComponentType = source.IsRestrictedByComponentType,
                       LimitedPercentage = source.LimitedPercentage,
                       MetaAreaProductPlacementPercent = source.MetaAreaProductPlacementPercent,
                       MetaCountOfProductPlacedOnPlanogram = source.MetaCountOfProductPlacedOnPlanogram,
                       MetaCountOfProductRecommendedOnPlanogram = source.MetaCountOfProductRecommendedOnPlanogram,
                       MetaCountOfProducts = source.MetaCountOfProducts,
                       MetaFinalPercentAllocated = source.MetaFinalPercentAllocated,
                       MetaLinearProductPlacementPercent = source.MetaLinearProductPlacementPercent,
                       MetaOriginalPercentAllocated = source.MetaOriginalPercentAllocated,
                       MetaP1 = source.MetaP1,
                       MetaP10 = source.MetaP10,
                       MetaP10Percentage = source.MetaP10Percentage,
                       MetaP11 = source.MetaP11,
                       MetaP11Percentage = source.MetaP11Percentage,
                       MetaP12 = source.MetaP12,
                       MetaP12Percentage = source.MetaP12Percentage,
                       MetaP13 = source.MetaP13,
                       MetaP13Percentage = source.MetaP13Percentage,
                       MetaP14 = source.MetaP14,
                       MetaP14Percentage = source.MetaP14Percentage,
                       MetaP15 = source.MetaP15,
                       MetaP15Percentage = source.MetaP15Percentage,
                       MetaP16 = source.MetaP16,
                       MetaP16Percentage = source.MetaP16Percentage,
                       MetaP17 = source.MetaP17,
                       MetaP17Percentage = source.MetaP17Percentage,
                       MetaP18 = source.MetaP18,
                       MetaP18Percentage = source.MetaP18Percentage,
                       MetaP19 = source.MetaP19,
                       MetaP19Percentage = source.MetaP19Percentage,
                       MetaP1Percentage = source.MetaP1Percentage,
                       MetaP2 = source.MetaP2,
                       MetaP20 = source.MetaP20,
                       MetaP20Percentage = source.MetaP20Percentage,
                       MetaP2Percentage = source.MetaP2Percentage,
                       MetaP3 = source.MetaP3,
                       MetaP3Percentage = source.MetaP3Percentage,
                       MetaP4 = source.MetaP4,
                       MetaP4Percentage = source.MetaP4Percentage,
                       MetaP5 = source.MetaP5,
                       MetaP5Percentage = source.MetaP5Percentage,
                       MetaP6 = source.MetaP6,
                       MetaP6Percentage = source.MetaP6Percentage,
                       MetaP7 = source.MetaP7,
                       MetaP7Percentage = source.MetaP7Percentage,
                       MetaP8 = source.MetaP8,
                       MetaP8Percentage = source.MetaP8Percentage,
                       MetaP9 = source.MetaP9,
                       MetaP9Percentage = source.MetaP9Percentage,
                       MetaPerformancePercentAllocated = source.MetaPerformancePercentAllocated,
                       MetaVolumetricProductPlacementPercent = source.MetaVolumetricProductPlacementPercent,
                       Name = source.Name,
                       TotalSpacePercentage = source.TotalSpacePercentage
                   };
        }

        private static PlanogramBlockingLocationNode AsGraphNode(PlanogramBlockingLocation source)
        {
            return new PlanogramBlockingLocationNode
                   {
                       Id = source.Id,
                       Height = source.Height,
                       PlanogramBlockingDividerBottomId = source.PlanogramBlockingDividerBottomId,
                       PlanogramBlockingDividerLeftId = source.PlanogramBlockingDividerLeftId,
                       PlanogramBlockingDividerRightId = source.PlanogramBlockingDividerRightId,
                       PlanogramBlockingDividerTopId = source.PlanogramBlockingDividerTopId,
                       PlanogramBlockingGroupId = source.PlanogramBlockingGroupId,
                       SpacePercentage = source.SpacePercentage,
                       Width = source.Width,
                       X = source.X,
                       Y = source.Y
                   };
        }

        private static PlanogramProductNode AsGraphNode(PlanogramProduct source)
        {
            return new PlanogramProductNode
                   {
                       Id = source.Id,
                       AlternateDepth = source.AlternateDepth,
                       AlternateHeight = source.AlternateHeight,
                       AlternateWidth = source.AlternateWidth,
                       Barcode = source.Barcode,
                       Brand = source.Brand,
                       CanBreakTrayBack = source.CanBreakTrayBack,
                       CanBreakTrayDown = source.CanBreakTrayDown,
                       CanBreakTrayTop = source.CanBreakTrayTop,
                       CanBreakTrayUp = source.CanBreakTrayUp,
                       CaseCost = source.CaseCost,
                       CaseDeep = source.CaseDeep,
                       CaseDepth = source.CaseDepth,
                       CaseHeight = source.CaseHeight,
                       CaseHigh = source.CaseHigh,
                       CasePackUnits = source.CasePackUnits,
                       CaseWide = source.CaseWide,
                       CaseWidth = source.CaseWidth,
                       Colour = source.Colour,
                       ColourGroupValue = source.ColourGroupValue,
                       ConsumerInformation = source.ConsumerInformation,
                       CorporateCode = source.CorporateCode,
                       CostPrice = source.CostPrice,
                       CountryOfOrigin = source.CountryOfOrigin,
                       CountryOfProcessing = source.CountryOfProcessing,
                       CustomAttributes = AsGraphNode(source.CustomAttributes),
                       CustomerStatus = source.CustomerStatus,
                       DateDiscontinued = source.DateDiscontinued,
                       DateEffective = source.DateEffective,
                       DateIntroduced = source.DateIntroduced,
                       DeliveryFrequencyDays = source.DeliveryFrequencyDays,
                       DeliveryMethod = source.DeliveryMethod,
                       Depth = source.Depth,
                       DisplayDepth = source.DisplayDepth,
                       DisplayHeight = source.DisplayHeight,
                       DisplayWidth = source.DisplayWidth,
                       FillColour = source.FillColour,
                       FillPatternType = source.FillPatternType,
                       FinancialGroupCode = source.FinancialGroupCode,
                       FinancialGroupName = source.FinancialGroupName,
                       FingerSpaceAbove = source.FingerSpaceAbove,
                       FingerSpaceToTheSide = source.FingerSpaceToTheSide,
                       Flavour = source.Flavour,
                       ForceBottomCap = source.ForceBottomCap,
                       ForceMiddleCap = source.ForceMiddleCap,
                       FrontOverhang = source.FrontOverhang,
                       GarmentType = source.GarmentType,
                       Gtin = source.Gtin,
                       Health = source.Health,
                       Height = source.Height,
                       IsActive = source.IsActive,
                       IsFrontOnly = source.IsFrontOnly,
                       IsNewProduct = source.IsNewProduct,
                       IsPlaceHolderProduct = source.IsPlaceHolderProduct,
                       IsPrivateLabel = source.IsPrivateLabel,
                       Manufacturer = source.Manufacturer,
                       ManufacturerCode = source.ManufacturerCode,
                       ManufacturerRecommendedRetailPrice = source.ManufacturerRecommendedRetailPrice,
                       MaxDeep = source.MaxDeep,
                       MaxRightCap = source.MaxRightCap,
                       MaxStack = source.MaxStack,
                       MaxTopCap = source.MaxTopCap,
                       MerchandisingStyle = source.MerchandisingStyle,
                       MetaCDTNode = source.MetaCDTNode,
                       MetaComparisonStatus = source.MetaComparisonStatus,
                       MetaIsBuddied = source.MetaIsBuddied,
                       MetaIsCoreRuleBroken = source.MetaIsCoreRuleBroken,
                       MetaIsDelistFamilyRuleBroken = source.MetaIsDelistFamilyRuleBroken,
                       MetaIsDelistProductRuleBroken = source.MetaIsDelistProductRuleBroken,
                       MetaIsDependencyFamilyRuleBroken = source.MetaIsDependencyFamilyRuleBroken,
                       MetaIsDistributionRuleBroken = source.MetaIsDistributionRuleBroken,
                       MetaIsFamilyRuleBroken = source.MetaIsFamilyRuleBroken,
                       MetaIsForceProductRuleBroken = source.MetaIsForceProductRuleBroken,
                       MetaIsIllegalProduct = source.MetaIsIllegalProduct,
                       MetaIsInheritanceRuleBroken = source.MetaIsInheritanceRuleBroken,
                       MetaIsInMasterData = source.MetaIsInMasterData,
                       MetaIsLocalProductRuleBroken = source.MetaIsLocalProductRuleBroken,
                       MetaIsMaximumProductFamilyRuleBroken = source.MetaIsMaximumProductFamilyRuleBroken,
                       MetaIsMinimumHurdleProductRuleBroken = source.MetaIsMinimumHurdleProductRuleBroken,
                       MetaIsMinimumProductFamilyRuleBroken = source.MetaIsMinimumProductFamilyRuleBroken,
                       MetaIsNotLegalProduct = source.MetaIsNotLegalProduct,
                       MetaIsOverShelfLifePercent = source.MetaIsOverShelfLifePercent,
                       MetaIsPreserveProductRuleBroken = source.MetaIsPreserveProductRuleBroken,
                       MetaIsProductRuleBroken = source.MetaIsProductRuleBroken,
                       MetaIsRangedInAssortment = source.MetaIsRangedInAssortment,
                       MetaNotAchievedCases = source.MetaNotAchievedCases,
                       MetaNotAchievedDeliveries = source.MetaNotAchievedDeliveries,
                       MetaNotAchievedDOS = source.MetaNotAchievedDOS,
                       MetaNotAchievedInventory = source.MetaNotAchievedInventory,
                       MetaPlanogramAreaSpacePercentage = source.MetaPlanogramAreaSpacePercentage,
                       MetaPlanogramLinearSpacePercentage = source.MetaPlanogramLinearSpacePercentage,
                       MetaPlanogramVolumetricSpacePercentage = source.MetaPlanogramVolumetricSpacePercentage,
                       MetaPositionCount = source.MetaPositionCount,
                       MetaTotalAreaSpace = source.MetaTotalAreaSpace,
                       MetaTotalFacings = source.MetaTotalFacings,
                       MetaTotalLinearSpace = source.MetaTotalLinearSpace,
                       MetaTotalMainFacings = source.MetaTotalMainFacings,
                       MetaTotalUnits = source.MetaTotalUnits,
                       MetaTotalVolumetricSpace = source.MetaTotalVolumetricSpace,
                       MetaTotalXFacings = source.MetaTotalXFacings,
                       MetaTotalYFacings = source.MetaTotalYFacings,
                       MetaTotalZFacings = source.MetaTotalZFacings,
                       MinDeep = source.MinDeep,
                       Model = source.Model,
                       Name = source.Name,
                       NestingDepth = source.NestingDepth,
                       NestingHeight = source.NestingHeight,
                       NestingWidth = source.NestingWidth,
                       NumberOfPegHoles = source.NumberOfPegHoles,
                       OrientationType = source.OrientationType,
                       PackagingShape = source.PackagingShape,
                       PackagingType = source.PackagingType,
                       Pattern = source.Pattern,
                       PegDepth = source.PegDepth,
                       PegProngOffsetX = source.PegProngOffsetX,
                       PegProngOffsetY = source.PegProngOffsetY,
                       PegX = source.PegX,
                       PegX2 = source.PegX2,
                       PegX3 = source.PegX3,
                       PegY = source.PegY,
                       PegY2 = source.PegY2,
                       PegY3 = source.PegY3,
                       PlanogramImageIdAlternateBack = source.PlanogramImageIdAlternateBack,
                       PlanogramImageIdAlternateBottom = source.PlanogramImageIdAlternateBottom,
                       PlanogramImageIdAlternateFront = source.PlanogramImageIdAlternateFront,
                       PlanogramImageIdAlternateLeft = source.PlanogramImageIdAlternateLeft,
                       PlanogramImageIdAlternateRight = source.PlanogramImageIdAlternateRight,
                       PlanogramImageIdAlternateTop = source.PlanogramImageIdAlternateTop,
                       PlanogramImageIdBack = source.PlanogramImageIdBack,
                       PlanogramImageIdBottom = source.PlanogramImageIdBottom,
                       PlanogramImageIdCaseBack = source.PlanogramImageIdCaseBack,
                       PlanogramImageIdCaseBottom = source.PlanogramImageIdCaseBottom,
                       PlanogramImageIdCaseFront = source.PlanogramImageIdCaseFront,
                       PlanogramImageIdCaseLeft = source.PlanogramImageIdCaseLeft,
                       PlanogramImageIdCaseRight = source.PlanogramImageIdCaseRight,
                       PlanogramImageIdCaseTop = source.PlanogramImageIdCaseTop,
                       PlanogramImageIdDisplayBack = source.PlanogramImageIdDisplayBack,
                       PlanogramImageIdDisplayBottom = source.PlanogramImageIdDisplayBottom,
                       PlanogramImageIdDisplayFront = source.PlanogramImageIdDisplayFront,
                       PlanogramImageIdDisplayLeft = source.PlanogramImageIdDisplayLeft,
                       PlanogramImageIdDisplayRight = source.PlanogramImageIdDisplayRight,
                       PlanogramImageIdDisplayTop = source.PlanogramImageIdDisplayTop,
                       PlanogramImageIdFront = source.PlanogramImageIdFront,
                       PlanogramImageIdLeft = source.PlanogramImageIdLeft,
                       PlanogramImageIdPointOfPurchaseBack = source.PlanogramImageIdPointOfPurchaseBack,
                       PlanogramImageIdPointOfPurchaseBottom = source.PlanogramImageIdPointOfPurchaseBottom,
                       PlanogramImageIdPointOfPurchaseFront = source.PlanogramImageIdPointOfPurchaseFront,
                       PlanogramImageIdPointOfPurchaseLeft = source.PlanogramImageIdPointOfPurchaseLeft,
                       PlanogramImageIdPointOfPurchaseRight = source.PlanogramImageIdPointOfPurchaseRight,
                       PlanogramImageIdPointOfPurchaseTop = source.PlanogramImageIdPointOfPurchaseTop,
                       PlanogramImageIdRight = source.PlanogramImageIdRight,
                       PlanogramImageIdTop = source.PlanogramImageIdTop,
                       PlanogramImageIdTrayBack = source.PlanogramImageIdTrayBack,
                       PlanogramImageIdTrayBottom = source.PlanogramImageIdTrayBottom,
                       PlanogramImageIdTrayFront = source.PlanogramImageIdTrayFront,
                       PlanogramImageIdTrayLeft = source.PlanogramImageIdTrayLeft,
                       PlanogramImageIdTrayRight = source.PlanogramImageIdTrayRight,
                       PlanogramImageIdTrayTop = source.PlanogramImageIdTrayTop,
                       PointOfPurchaseDepth = source.PointOfPurchaseDepth,
                       PointOfPurchaseDescription = source.PointOfPurchaseDescription,
                       PointOfPurchaseHeight = source.PointOfPurchaseHeight,
                       PointOfPurchaseWidth = source.PointOfPurchaseWidth,
                       RecommendedRetailPrice = source.RecommendedRetailPrice,
                       SellPackCount = source.SellPackCount,
                       SellPackDescription = source.SellPackDescription,
                       SellPrice = source.SellPrice,
                       Shape = source.Shape,
                       ShapeType = source.ShapeType,
                       ShelfLife = source.ShelfLife,
                       ShortDescription = source.ShortDescription,
                       Size = source.Size,
                       SqueezeDepth = source.SqueezeDepth,
                       SqueezeDepthActual = source.SqueezeDepthActual,
                       SqueezeHeight = source.SqueezeHeight,
                       SqueezeHeightActual = source.SqueezeHeightActual,
                       SqueezeWidth = source.SqueezeWidth,
                       SqueezeWidthActual = source.SqueezeWidthActual,
                       StatusType = source.StatusType,
                       StyleNumber = source.StyleNumber,
                       Subcategory = source.Subcategory,
                       TaxRate = source.TaxRate,
                       Texture = source.Texture,
                       TrayDeep = source.TrayDeep,
                       TrayDepth = source.TrayDepth,
                       TrayHeight = source.TrayHeight,
                       TrayHigh = source.TrayHigh,
                       TrayPackUnits = source.TrayPackUnits,
                       TrayThickDepth = source.TrayThickDepth,
                       TrayThickHeight = source.TrayThickHeight,
                       TrayThickWidth = source.TrayThickWidth,
                       TrayWide = source.TrayWide,
                       TrayWidth = source.TrayWidth,
                       UnitOfMeasure = source.UnitOfMeasure,
                       Vendor = source.Vendor,
                       VendorCode = source.VendorCode,
                       Width = source.Width
                   };
        }

        private static PlanogramPerformanceDataNode AsGraphNode(PlanogramPerformanceData source)
        {
            return new PlanogramPerformanceDataNode
                   {
                       Id = source.Id,
                       AchievedCasePacks = source.AchievedCasePacks,
                       AchievedDeliveries = source.AchievedDeliveries,
                       AchievedDos = source.AchievedDos,
                       AchievedShelfLife = source.AchievedShelfLife,
                       CP1 = source.CP1,
                       CP10 = source.CP10,
                       CP2 = source.CP2,
                       CP3 = source.CP3,
                       CP4 = source.CP4,
                       CP5 = source.CP5,
                       CP6 = source.CP6,
                       CP7 = source.CP7,
                       CP8 = source.CP8,
                       CP9 = source.CP9,
                       MetaCP10Percentage = source.MetaCP10Percentage,
                       MetaCP10Rank = source.MetaCP10Rank,
                       MetaCP1Percentage = source.MetaCP1Percentage,
                       MetaCP1Rank = source.MetaCP1Rank,
                       MetaCP2Percentage = source.MetaCP2Percentage,
                       MetaCP2Rank = source.MetaCP2Rank,
                       MetaCP3Percentage = source.MetaCP3Percentage,
                       MetaCP3Rank = source.MetaCP3Rank,
                       MetaCP4Percentage = source.MetaCP4Percentage,
                       MetaCP4Rank = source.MetaCP4Rank,
                       MetaCP5Percentage = source.MetaCP5Percentage,
                       MetaCP5Rank = source.MetaCP5Rank,
                       MetaCP6Percentage = source.MetaCP6Percentage,
                       MetaCP6Rank = source.MetaCP6Rank,
                       MetaCP7Percentage = source.MetaCP7Percentage,
                       MetaCP7Rank = source.MetaCP7Rank,
                       MetaCP8Percentage = source.MetaCP8Percentage,
                       MetaCP8Rank = source.MetaCP8Rank,
                       MetaCP9Percentage = source.MetaCP9Percentage,
                       MetaCP9Rank = source.MetaCP9Rank,
                       MetaP10Percentage = source.MetaP10Percentage,
                       MetaP10Rank = source.MetaP10Rank,
                       MetaP11Percentage = source.MetaP11Percentage,
                       MetaP11Rank = source.MetaP11Rank,
                       MetaP12Percentage = source.MetaP12Percentage,
                       MetaP12Rank = source.MetaP12Rank,
                       MetaP13Percentage = source.MetaP13Percentage,
                       MetaP13Rank = source.MetaP13Rank,
                       MetaP14Percentage = source.MetaP14Percentage,
                       MetaP14Rank = source.MetaP14Rank,
                       MetaP15Percentage = source.MetaP15Percentage,
                       MetaP15Rank = source.MetaP15Rank,
                       MetaP16Percentage = source.MetaP16Percentage,
                       MetaP16Rank = source.MetaP16Rank,
                       MetaP17Percentage = source.MetaP17Percentage,
                       MetaP17Rank = source.MetaP17Rank,
                       MetaP18Percentage = source.MetaP18Percentage,
                       MetaP18Rank = source.MetaP18Rank,
                       MetaP19Percentage = source.MetaP19Percentage,
                       MetaP19Rank = source.MetaP19Rank,
                       MetaP1Percentage = source.MetaP1Percentage,
                       MetaP1Rank = source.MetaP1Rank,
                       MetaP20Percentage = source.MetaP20Percentage,
                       MetaP20Rank = source.MetaP20Rank,
                       MetaP2Percentage = source.MetaP2Percentage,
                       MetaP2Rank = source.MetaP2Rank,
                       MetaP3Percentage = source.MetaP3Percentage,
                       MetaP3Rank = source.MetaP3Rank,
                       MetaP4Percentage = source.MetaP4Percentage,
                       MetaP4Rank = source.MetaP4Rank,
                       MetaP5Percentage = source.MetaP5Percentage,
                       MetaP5Rank = source.MetaP5Rank,
                       MetaP6Percentage = source.MetaP6Percentage,
                       MetaP6Rank = source.MetaP6Rank,
                       MetaP7Percentage = source.MetaP7Percentage,
                       MetaP7Rank = source.MetaP7Rank,
                       MetaP8Percentage = source.MetaP8Percentage,
                       MetaP8Rank = source.MetaP8Rank,
                       MetaP9Percentage = source.MetaP9Percentage,
                       MetaP9Rank = source.MetaP9Rank,
                       MinimumInventoryUnits = source.MinimumInventoryUnits,
                       P1 = source.P1,
                       P10 = source.P10,
                       P11 = source.P11,
                       P12 = source.P12,
                       P13 = source.P13,
                       P14 = source.P14,
                       P15 = source.P15,
                       P16 = source.P16,
                       P17 = source.P17,
                       P18 = source.P18,
                       P19 = source.P19,
                       P2 = source.P2,
                       P20 = source.P20,
                       P3 = source.P3,
                       P4 = source.P4,
                       P5 = source.P5,
                       P6 = source.P6,
                       P7 = source.P7,
                       P8 = source.P8,
                       P9 = source.P9,
                       PlanogramProductId = source.PlanogramProductId,
                       UnitsSoldPerDay = source.UnitsSoldPerDay
                   };
        }

        private static PlanogramAnnotationNode AsGraphNode(PlanogramAnnotation source)
        {
            return new PlanogramAnnotationNode
                   {
                       Id = source.Id,
                       AnnotationType = source.AnnotationType,
                       BackgroundColour = source.BackgroundColour,
                       BorderColour = source.BorderColour,
                       BorderThickness = source.BorderThickness,
                       CanReduceFontToFit = source.CanReduceFontToFit,
                       Depth = source.Depth,
                       FontColour = source.FontColour,
                       FontName = source.FontName,
                       FontSize = source.FontSize,
                       Height = source.Height,
                       PlanogramAssemblyComponentId = source.PlanogramAssemblyComponentId,
                       PlanogramFixtureAssemblyId = source.PlanogramFixtureAssemblyId,
                       PlanogramFixtureComponentId = source.PlanogramFixtureComponentId,
                       PlanogramFixtureItemId = source.PlanogramFixtureItemId,
                       PlanogramPositionId = source.PlanogramPositionId,
                       PlanogramSubComponentId = source.PlanogramSubComponentId,
                       Text = source.Text,
                       Width = source.Width,
                       X = source.X,
                       Y = source.Y,
                       Z = source.Z
                   };
        }

        private static PlanogramPositionNode AsGraphNode(PlanogramPosition source)
        {
            return new PlanogramPositionNode
                   {
                       Id = source.Id,
                       Angle = source.Angle,
                       DepthSqueeze = source.DepthSqueeze,
                       DepthSqueezeX = source.DepthSqueezeX,
                       DepthSqueezeY = source.DepthSqueezeY,
                       DepthSqueezeZ = source.DepthSqueezeZ,
                       FacingsDeep = source.FacingsDeep,
                       FacingsHigh = source.FacingsHigh,
                       FacingsWide = source.FacingsWide,
                       FacingsXDeep = source.FacingsXDeep,
                       FacingsXHigh = source.FacingsXHigh,
                       FacingsXWide = source.FacingsXWide,
                       FacingsYDeep = source.FacingsYDeep,
                       FacingsYHigh = source.FacingsYHigh,
                       FacingsYWide = source.FacingsYWide,
                       FacingsZDeep = source.FacingsZDeep,
                       FacingsZHigh = source.FacingsZHigh,
                       FacingsZWide = source.FacingsZWide,
                       HorizontalSqueeze = source.HorizontalSqueeze,
                       HorizontalSqueezeX = source.HorizontalSqueezeX,
                       HorizontalSqueezeY = source.HorizontalSqueezeY,
                       HorizontalSqueezeZ = source.HorizontalSqueezeZ,
                       IsManuallyPlaced = source.IsManuallyPlaced,
                       IsXPlacedLeft = source.IsXPlacedLeft,
                       IsYPlacedBottom = source.IsYPlacedBottom,
                       IsZPlacedFront = source.IsZPlacedFront,
                       MerchandisingStyle = source.MerchandisingStyle,
                       MerchandisingStyleX = source.MerchandisingStyleX,
                       MerchandisingStyleY = source.MerchandisingStyleY,
                       MerchandisingStyleZ = source.MerchandisingStyleZ,
                       MetaAchievedCases = source.MetaAchievedCases,
                       MetaComparisonStatus = source.MetaComparisonStatus,
                       MetaDepth = source.MetaDepth,
                       MetaFrontFacingsWide = source.MetaFrontFacingsWide,
                       MetaHeight = source.MetaHeight,
                       MetaIsHangingTray = source.MetaIsHangingTray,
                       MetaIsInvalidMerchandisingStyle = source.MetaIsInvalidMerchandisingStyle,
                       MetaIsOutsideMerchandisingSpace = source.MetaIsOutsideMerchandisingSpace,
                       MetaIsOutsideOfBlockSpace = source.MetaIsOutsideOfBlockSpace,
                       MetaIsOverMaxDeep = source.MetaIsOverMaxDeep,
                       MetaIsOverMaxRightCap = source.MetaIsOverMaxRightCap,
                       MetaIsOverMaxStack = source.MetaIsOverMaxStack,
                       MetaIsOverMaxTopCap = source.MetaIsOverMaxTopCap,
                       MetaIsPegOverfilled = source.MetaIsPegOverfilled,
                       MetaIsPositionCollisions = source.MetaIsPositionCollisions,
                       MetaIsUnderMinDeep = source.MetaIsUnderMinDeep,
                       MetaPegColumnNumber = source.MetaPegColumnNumber,
                       MetaPegRowNumber = source.MetaPegRowNumber,
                       MetaPlanogramAreaSpacePercentage = source.MetaPlanogramAreaSpacePercentage,
                       MetaPlanogramLinearSpacePercentage = source.MetaPlanogramLinearSpacePercentage,
                       MetaPlanogramVolumetricSpacePercentage = source.MetaPlanogramVolumetricSpacePercentage,
                       MetaSequenceGroupName = source.MetaSequenceGroupName,
                       MetaSequenceSubGroupName = source.MetaSequenceSubGroupName,
                       MetaTotalAreaSpace = source.MetaTotalAreaSpace,
                       MetaTotalLinearSpace = source.MetaTotalLinearSpace,
                       MetaTotalVolumetricSpace = source.MetaTotalVolumetricSpace,
                       MetaWidth = source.MetaWidth,
                       MetaWorldX = source.MetaWorldX,
                       MetaWorldY = source.MetaWorldY,
                       MetaWorldZ = source.MetaWorldZ,
                       OrientationType = source.OrientationType,
                       OrientationTypeX = source.OrientationTypeX,
                       OrientationTypeY = source.OrientationTypeY,
                       OrientationTypeZ = source.OrientationTypeZ,
                       PlanogramAssemblyComponentId = source.PlanogramAssemblyComponentId,
                       PlanogramFixtureAssemblyId = source.PlanogramFixtureAssemblyId,
                       PlanogramFixtureComponentId = source.PlanogramFixtureComponentId,
                       PlanogramFixtureItemId = source.PlanogramFixtureItemId,
                       PlanogramProductId = source.PlanogramProductId,
                       PlanogramSubComponentId = source.PlanogramSubComponentId,
                       PositionSequenceNumber = source.PositionSequenceNumber,
                       Roll = source.Roll,
                       Sequence = source.Sequence,
                       SequenceColour = source.SequenceColour,
                       SequenceNumber = source.SequenceNumber,
                       SequenceX = source.SequenceX,
                       SequenceY = source.SequenceY,
                       SequenceZ = source.SequenceZ,
                       Slope = source.Slope,
                       TotalUnits = source.TotalUnits,
                       UnitsDeep = source.UnitsDeep,
                       UnitsHigh = source.UnitsHigh,
                       UnitsWide = source.UnitsWide,
                       VerticalSqueeze = source.VerticalSqueeze,
                       VerticalSqueezeX = source.VerticalSqueezeX,
                       VerticalSqueezeY = source.VerticalSqueezeY,
                       VerticalSqueezeZ = source.VerticalSqueezeZ,
                       X = source.X,
                       Y = source.Y,
                       Z = source.Z
                   };
        }

        private static PlanogramEventLogNode AsGraphNode(PlanogramEventLog source)
        {
            return new PlanogramEventLogNode
                   {
                       Id = source.Id,
                       AffectedId = source.AffectedId,
                       AffectedType = source.AffectedType,
                       Content = source.Content,
                       DateTime = source.DateTime,
                       Description = source.Description,
                       EntryType = source.EntryType,
                       EventId = source.EventId,
                       EventType = source.EventType,
                       Score = source.Score,
                       TaskSource = source.TaskSource,
                       WorkpackageSource = source.WorkpackageSource
                   };
        }
    }
}