using System;

namespace Galleria.Ccm.CloudSync.Service.Clients
{
    public class Token : IToken
    {
        private Guid _token = Guid.Empty;

        /// <summary>
        ///     Create a new instance from a json formatted token. 
        /// </summary>
        /// <param name="json">Json format version of the token.</param>
        /// <returns></returns>
        public Boolean Accept(String json)
        {
            _token = Guid.Empty;
            return Guid.TryParse(json, out _token);
        }

        /// <summary>
        /// Get the json serialization of this token.
        /// </summary>
        /// <returns></returns>
        public String ToJson()
        {
            return IsValid() ? _token.ToString() : String.Empty;
        }

        /// <summary>
        /// Get wether the token is valid or not.
        /// </summary>
        /// <returns></returns>
        public Boolean IsValid()
        {
            return _token != Guid.Empty;
        }
    }
}