using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Galleria.Ccm.CloudSync.Service.Models;
using Topshelf.Logging;

namespace Galleria.Ccm.CloudSync.Service.Clients
{
    /// <summary>
    ///     Implementation of <see cref="IWebApiClient" /> for .Net Framework 4.0.
    /// </summary>
    /// <remarks>Implements a facade for the <see cref="HttpClient" />.</remarks>
    public class Net4WebApiClient : IWebApiClient
    {
        private readonly LogWriter _log;
        private readonly IToken _token = new Token();
        private readonly String _endpoint;

        /// <summary>
        ///     Initializes a new instance of <see cref="Net4WebApiClient" /> for the given <paramref name="endpoint" /> and
        ///     optionally using the provided <paramref name="logWriter" />
        /// </summary>
        /// <param name="endpoint">The endpoint used to connect to the web service.</param>
        /// <param name="logWriter">[Optional] The <see cref="LogWriter" /> to report events to.</param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="endpoint" /> is null.
        /// </exception>
        public Net4WebApiClient(String endpoint, LogWriter logWriter)
        {
            if (String.IsNullOrWhiteSpace(endpoint)) throw new ArgumentNullException(nameof(endpoint));

            _endpoint = endpoint;
            _log = logWriter;
        }

        /// <summary>
        ///     Get whether the client is currently signed in (and thus holds a token).
        /// </summary>
        public Boolean IsSignedIn => _token.IsValid();

        public async Task SignInAsync(IWebApiCredentials credentials)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri($"{_endpoint}api/authenticate/login/"),
                Method = HttpMethod.Post,
                Content = new StringContent("Login Request for GCS"),
                Headers = {Authorization = CreateAuthorization(credentials.UserName, credentials.Password)}
            };
            HttpResponseMessage response = await SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                _log?.Error($"Operation: {nameof(SignInAsync)}; Error: Authentication failed, the provided user name and password were not recognized by the web service.");
                return;
            }
            IEnumerable<String> tokenValues;
            if (!response.Headers.TryGetValues("Token", out tokenValues))
            {
                _log?.Error($"Operation: {nameof(SignInAsync)}; Error: No token was sent back by the web service.");
                return;
            }
            if (!_token.Accept(tokenValues.FirstOrDefault()))
                _log?.Error($"Operation: {nameof(SignInAsync)}; Error: Token not recognized.");
        }


        public async Task<HttpResponseMessage> GetAsync(String requestUri)
        {
            using (HttpClient client = GetHttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(requestUri);
                    return response;
                }
                catch (HttpRequestException ex)
                {
                    _log?.Fatal($"{ex.Message} - {ex.InnerException?.Message}");
                    return new HttpResponseMessage(HttpStatusCode.GatewayTimeout);
                }
            }
        }

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            using (HttpClient client = GetHttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.SendAsync(request);
                    return response;
                }
                catch (Exception ex)
                {
                    String errorString = $"{ex.Message} - {ex.InnerException?.Message}";
                    _log?.Fatal(errorString);
                    return new HttpResponseMessage(HttpStatusCode.GatewayTimeout) {Content = new StringContent(errorString)};
                }
            }
        }

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient {BaseAddress = new Uri(_endpoint)};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Token", _token.ToJson());
            return client;
        }

        private static AuthenticationHeaderValue CreateAuthorization(String userName, String password)
        {
            String authorizationString = $"{userName}:{password}";
            return new AuthenticationHeaderValue("Basic", EncodeToBase64(authorizationString));
        }

        private static String EncodeToBase64(String text)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(text));
        }
    }
}