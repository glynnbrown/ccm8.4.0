using System;
using System.Net.Http;
using System.Threading.Tasks;
using Galleria.Ccm.CloudSync.Service.Models;

namespace Galleria.Ccm.CloudSync.Service.Clients
{
    /// <summary>
    /// Represents a facade to operate with a class that allows using Web Api to communicate with a server.
    /// </summary>
    /// <remarks>Facade Pattern.</remarks>
    public interface IWebApiClient
    {
        Task<HttpResponseMessage> GetAsync(String requestUri);
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);

        /// <summary>
        /// Get whether the client is current signed in (and thus holds a token).
        /// </summary>
        Boolean IsSignedIn { get; }

        Task SignInAsync(IWebApiCredentials credentials);
    }
}