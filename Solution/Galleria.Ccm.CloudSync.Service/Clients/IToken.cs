using System;

namespace Galleria.Ccm.CloudSync.Service.Clients
{
    public interface IToken
    {
        /// <summary>
        ///     Create a new instance from a json formatted token. 
        /// </summary>
        /// <param name="json">Json format version of the token.</param>
        /// <returns></returns>
        Boolean Accept(String json);

        /// <summary>
        /// Get the json serialization of this token.
        /// </summary>
        /// <returns></returns>
        String ToJson();

        /// <summary>
        /// Get whether the token is valid or not.
        /// </summary>
        /// <returns></returns>
        Boolean IsValid();
    }
}