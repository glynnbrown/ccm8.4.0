﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Galleria.Ccm.CloudSync.Service
{
    /// <summary>
    /// Serializes PublishingStatusDetails to a Json string
    /// </summary>
    public class PublishingStatusDetailsSerializer
    {
        /// <summary>
        /// returns the PublishingStatusDetails serialized as Json
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public String ToJson(List<Services.Content.PublishingStatusDetails> source)
        {
            return JsonConvert.SerializeObject(source);
        }
    }
}
