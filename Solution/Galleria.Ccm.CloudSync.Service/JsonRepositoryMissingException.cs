﻿using System;
using System.Runtime.Serialization;

namespace Galleria.Ccm.CloudSync.Service
{
    [Serializable]
    public class JsonRepositoryMissingException : Exception
    {
        public JsonRepositoryMissingException() : base("Could not find the Json Repository.")
        {
        }

        public JsonRepositoryMissingException(String message) : base(message)
        {
        }

        public JsonRepositoryMissingException(String message, Exception innerException) : base(message, innerException)
        {
        }

        protected JsonRepositoryMissingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}