using System;
using System.Collections.Generic;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Ccm.CloudSync.Service.Models.PerformanceSourceGraph;

namespace Galleria.Ccm.CloudSync.Service
{
    public class PerformanceSourceAddRequest : IPerformanceSourceAddRequest
    {
        public IPerformanceSource PerformanceSourceNode { get; set; }
        public List<String> Timelines { get; set; }
    }
}