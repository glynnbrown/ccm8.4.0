﻿using System;
using System.Linq;
using Galleria.Ccm.CloudSync.Service.Models.ProductHierarchyGraph;
using Newtonsoft.Json;

namespace Galleria.Ccm.CloudSync.Service
{
    public class MerchandisingHierarchySerializer : IMerchandisingHierarchySerializer
    {
        public String ToJson(Services.Product.ProductHierarchy source)
        {
            return JsonConvert.SerializeObject(AsGraphNode(source));
        }

        private static ProductHierarchyNode AsGraphNode(Services.Product.ProductHierarchy source)
        {
            return new ProductHierarchyNode
            {
                Id = source.Id,
                Name = source.Name,
                RootGroup = AsGraphNode(source.RootGroup),
                RootLevel = AsGraphNode(source.RootLevel)
            };
        }

        private static ProductGroupNode AsGraphNode(Services.Product.ProductGroup source)
        {
            return new ProductGroupNode
            {
                Id = source.Id,
                Name = source.Name,
                Code = source.Code,
                ProductLevelId = source.ProductLevelId,
                ChildList = source.Children.Select(AsGraphNode).ToList()

            };
        }

        private static ProductLevelNode AsGraphNode(Services.Product.ProductLevel source)
        {
            return new ProductLevelNode
            {
                Id = source.Id,
                Name = source.Name,
                Colour = source.Colour,
                ShapeNo = source.ShapeNo, 
                ChildLevel = source.ChildLevel != null ? AsGraphNode(source.ChildLevel) : null
            };
        }
    }
}
