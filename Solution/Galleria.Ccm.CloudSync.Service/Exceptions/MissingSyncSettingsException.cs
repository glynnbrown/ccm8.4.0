﻿using System;

namespace Galleria.Ccm.CloudSync.Service.Exceptions
{
    public class MissingSyncSettingsException : Exception {}
}