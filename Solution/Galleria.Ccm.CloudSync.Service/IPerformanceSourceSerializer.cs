﻿using System;
using System.Collections.Generic;
using Galleria.Ccm.Services.Performance;

namespace Galleria.Ccm.CloudSync.Service
{
    public interface IPerformanceSourceSerializer
    {
        String ToJson(Dictionary<PerformanceSource, List<String>> source);
    }
}
