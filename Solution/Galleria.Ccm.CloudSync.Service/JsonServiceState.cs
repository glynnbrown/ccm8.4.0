﻿using System;

namespace Galleria.Ccm.CloudSync.Service
{
    /// <summary>
    ///     Represents a service state persisted to a json file.
    /// </summary>
    public class JsonServiceState : IServiceState
    {
        /// <inheritdoc />
        public DateTime LastFullSyncDate { get; set; }

        /// <inheritdoc />
        public DateTime LastPublishingSyncDate { get; set; }
    }
}