﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25559 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class UserSystemSettingDal : MockDalBase<UserSystemSettingDto>, IUserSystemSettingDal
    {
        #region IUserSystemSettingDal Members

        public UserSystemSettingDto Fetch()
        {
            UserSystemSettingDto dto = DalCache.GetDtos<UserSystemSettingDto>().FirstOrDefault();
            
            if(dto == null)
            {
                dto = new UserSystemSettingDto();
                dto.DisplayLanguage = "en-gb";
                dto.DisplayCEIPWindow = true;
                dto.SendCEIPInformation = true;
                dto.IsDatabaseSelectionRemembered = false;
                dto.IsEntitySelectionRemembered = false;

                base.Insert(dto);    
            }

            return dto;
        }

        #endregion
    }
}
