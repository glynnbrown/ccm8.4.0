﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25608 : N.Foster
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link
//  Added Entity_Id link
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkpackageInfoDal : MockDalBase<WorkpackageInfoDto>, IWorkpackageInfoDal
    {
        #region Fetch
        /// <summary>
        /// Returns all workpackages for the specified planogram group
        /// </summary>
        public IEnumerable<WorkpackageInfoDto> FetchByEntityId(Int32 entityId)
        {
            return this.FetchByProperties<WorkpackageDto>(new String[] { "EntityId" }, new Object[] { entityId }, false);
        }

        /// <summary>
        /// Returns all workpackages for the specified planogram group
        /// </summary>
        public IEnumerable<WorkpackageInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return this.FetchByProperties<WorkpackageDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }
        #endregion
    }
}
