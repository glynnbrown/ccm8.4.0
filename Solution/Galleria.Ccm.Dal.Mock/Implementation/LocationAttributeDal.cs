﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationAttributeDal : MockDalBase<LocationAttributeDto>, ILocationAttributeDal
    {
        #region ILocationAttributeDal Members

        /// <summary>
        /// Fetches default StoreAttribute
        /// </summary>
        /// <returns></returns>
        public LocationAttributeDto FetchByEntityId(Int32 entityId)
        {
            LocationAttributeDto dto = new LocationAttributeDto();

            List<LocationDto> locationDtos = DalCache.GetDtos<LocationDto>().ToList();

            foreach (LocationDto locDto in locationDtos)
            {
                if (locDto.EntityId == entityId && locDto.DateDeleted == null)
                {
                    #region compare location and set attribute dto values
                    if (locDto.Region != null)
                    {
                        dto.HasRegion = true;
                    }
                    if (locDto.County != null)
                    {
                        dto.HasCounty = true;
                    }
                    if (locDto.TVRegion != null)
                    {
                        dto.HasTVRegion = true;
                    }
                    if (locDto.Address1 != null)
                    {
                        dto.HasAddress1 = true;
                    }
                    if (locDto.Address2 != null)
                    {
                        dto.HasAddress2 = true;
                    }
                    if (locDto.Is24Hours == true)
                    {
                        dto.Has24Hours = true;
                    }
                    if (locDto.HasPetrolForecourt == true)
                    {
                        dto.HasPetrolForecourt = true;
                    }
                    if (locDto.PetrolForecourtType != null)
                    {
                        dto.HasPetrolForecourtType = true;
                    }
                    if (locDto.Restaurant != null)
                    {
                        dto.HasRestaurant = true;
                    }
                    if (locDto.IsMezzFitted == true)
                    {
                        dto.HasMezzFitted = true;
                    }
                    if (locDto.SizeGrossFloorArea != null)
                    {
                        dto.HasSizeGrossArea = true;
                    }
                    if (locDto.SizeNetSalesArea != null)
                    {
                        dto.HasSizeNetSalesArea = true;
                    }
                    if (locDto.SizeMezzSalesArea != null)
                    {
                        dto.HasSizeMezzSalesArea = true;
                    }
                    if (locDto.City != null)
                    {
                        dto.HasCity = true;
                    }
                    if (locDto.Country != null)
                    {
                        dto.HasCountry = true;
                    }
                    if (locDto.ManagerName != null)
                    {
                        dto.HasManagerName = true;
                    }
                    if (locDto.RegionalManagerName != null)
                    {
                        dto.HasRegionalManagerName = true;
                    }
                    if (locDto.DivisionalManagerName != null)
                    {
                        dto.HasDivisionalManagerName = true;
                    }
                    if (locDto.AdvertisingZone != null)
                    {
                        dto.HasAdvertisingZone = true;
                    }
                    if (locDto.DistributionCentre != null)
                    {
                        dto.HasDistributionCentre = true;
                    }
                    if (locDto.HasNewsCube == true)
                    {
                        dto.HasNewsCube = true;
                    }
                    if (locDto.HasAtmMachines == true)
                    {
                        dto.HasAtmMachines = true;
                    }
                    if (locDto.NoOfCheckouts != null)
                    {
                        dto.HasNoOfCheckouts = true;
                    }
                    if (locDto.HasCustomerWC == true)
                    {
                        dto.HasCustomerWC = true;
                    }
                    if (locDto.HasBabyChanging == true)
                    {
                        dto.HasBabyChanging = true;
                    }
                    if (locDto.HasInStoreBakery == true)
                    {
                        dto.HasInLocationBakery = true;
                    }
                    if (locDto.HasHotFoodToGo == true)
                    {
                        dto.HasHotFoodToGo = true;
                    }
                    if (locDto.HasRotisserie == true)
                    {
                        dto.HasRotisserie = true;
                    }
                    if (locDto.HasFishmonger == true)
                    {
                        dto.HasFishmonger = true;
                    }
                    if (locDto.HasButcher == true)
                    {
                        dto.HasButcher = true;
                    }
                    if (locDto.HasPizza == true)
                    {
                        dto.HasPizza = true;
                    }
                    if (locDto.HasDeli == true)
                    {
                        dto.HasDeli = true;
                    }
                    if (locDto.HasSaladBar == true)
                    {
                        dto.HasSaladBar = true;
                    }
                    if (locDto.HasOrganic == true)
                    {
                        dto.HasOrganic = true;
                    }
                    if (locDto.HasGrocery == true)
                    {
                        dto.HasGrocery = true;
                    }
                    if (locDto.HasMobilePhones == true)
                    {
                        dto.HasMobilePhones = true;
                    }
                    if (locDto.HasDryCleaning == true)
                    {
                        dto.HasDryCleaning = true;
                    }
                    if (locDto.HasHomeShoppingAvailable == true)
                    {
                        dto.HasHomeShoppingAvailable = true;
                    }
                    if (locDto.HasOptician == true)
                    {
                        dto.HasOptician = true;
                    }
                    if (locDto.HasPharmacy == true)
                    {
                        dto.HasPharmacy = true;
                    }
                    if (locDto.HasTravel == true)
                    {
                        dto.HasTravel = true;
                    }
                    if (locDto.HasPhotoDepartment == true)
                    {
                        dto.HasPhotoDepartment = true;
                    }
                    if (locDto.HasCarServiceArea == true)
                    {
                        dto.HasCarServiceArea = true;
                    }
                    if (locDto.HasGardenCentre == true)
                    {
                        dto.HasGardenCentre = true;
                    }
                    if (locDto.HasClinic == true)
                    {
                        dto.HasClinic = true;
                    }
                    if (locDto.HasAlcohol == true)
                    {
                        dto.HasAlcohol = true;
                    }
                    if (locDto.HasFashion == true)
                    {
                        dto.HasFashion = true;
                    }
                    if (locDto.HasCafe == true)
                    {
                        dto.HasCafe = true;
                    }
                    if (locDto.HasRecycling == true)
                    {
                        dto.HasRecycling = true;
                    }
                    if (locDto.HasPhotocopier == true)
                    {
                        dto.HasPhotocopier = true;
                    }
                    if (locDto.HasLottery == true)
                    {
                        dto.HasLottery = true;
                    }
                    if (locDto.HasPostOffice == true)
                    {
                        dto.HasPostOffice = true;
                    }
                    if (locDto.HasMovieRental == true)
                    {
                        dto.HasMovieRental = true;
                    }
                    if (locDto.IsOpenMonday == true)
                    {
                        dto.HasOpenMonday = true;
                    }
                    if (locDto.IsOpenTuesday == true)
                    {
                        dto.HasOpenTuesday = true;
                    }
                    if (locDto.IsOpenWednesday == true)
                    {
                        dto.HasOpenWednesday = true;
                    }
                    if (locDto.IsOpenThursday == true)
                    {
                        dto.HasOpenThursday = true;
                    }
                    if (locDto.IsOpenFriday == true)
                    {
                        dto.HasOpenFriday = true;
                    }
                    if (locDto.IsOpenSaturday == true)
                    {
                        dto.HasOpenSaturday = true;
                    }
                    if (locDto.IsOpenSunday == true)
                    {
                        dto.HasOpenSunday = true;
                    }
                    if (locDto.HasJewellery == true)
                    {
                        dto.HasJewellery = true;
                    }
                    #endregion
                }
            }

            return dto;
        }

        #endregion
    }
}
