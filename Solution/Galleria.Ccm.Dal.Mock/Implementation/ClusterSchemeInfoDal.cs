﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created.
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ClusterSchemeInfoDal : MockDalBase<ClusterSchemeInfoDto>, IClusterSchemeInfoDal
    {
        public IEnumerable<ClusterSchemeInfoDto> FetchByEntityId(int entityId)
        {
             var dtoList = base.FetchByEntityId<ClusterSchemeDto>(entityId);

            //update cluster counts
            foreach (ClusterSchemeInfoDto dto in dtoList)
            {
                dto.ClusterCount = DalCache.GetDtos<ClusterDto>().Count(d => d.ClusterSchemeId == dto.Id);
                yield return dto;
            }
        }

        public IEnumerable<ClusterSchemeInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByProperties<ClusterSchemeDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }
    }
}
