﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25629: A.Kuszyk
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ClusterSchemeDal : MockDalBase<ClusterSchemeDto>, IClusterSchemeDal
    {

        public IEnumerable<ClusterSchemeDto> FetchByEntityId(int entityId)
        {
            return base.FetchByProperties(new String[] { "EntityId" }, new Object[] { entityId });
        }

        public void Upsert(IEnumerable<ClusterSchemeDto> dtoList, ClusterSchemeIsSetDto isSetDto)
        {
            base.Upsert(dtoList, isSetDto);
        }

        public void DeleteByEntityId(int entityId)
        {
            base.DeleteByProperties(new String[] { "EntityId" }, new Object[] { entityId });
        }
    }
}
