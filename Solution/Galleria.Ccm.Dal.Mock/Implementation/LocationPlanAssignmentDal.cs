﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationPlanAssignmentDal : MockDalBase<LocationPlanAssignmentDto>, ILocationPlanAssignmentDal
    {
        public IEnumerable<LocationPlanAssignmentDto> FetchByPlanogramId(Int32 planId)
        {
            return base.FetchByProperties<LocationPlanAssignmentDto>(new String[] { "PlanogramId" }, new Object[] { planId });
        }

        public IEnumerable<LocationPlanAssignmentDto> FetchByProductGroupId(Int32 productGroupId)
        {
            return base.FetchByProperties<LocationPlanAssignmentDto>(new String[] { "ProductGroupId" }, new Object[] { productGroupId });
        }

        public IEnumerable<LocationPlanAssignmentDto> FetchByLocationId(Int16 locationId)
        {
            return base.FetchByProperties<LocationPlanAssignmentDto>(new String[] { "LocationId" }, new Object[] { locationId });            
        }
    }
}
