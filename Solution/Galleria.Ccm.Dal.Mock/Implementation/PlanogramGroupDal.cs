﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25587 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramGroupDal : MockDalBase<PlanogramGroupDto>, IPlanogramGroupDal
    {

        #region IPlanogramGroupDal Members

        public IEnumerable<PlanogramGroupDto> FetchByPlanogramHierarchyId(Int32 hierarchyId)
        {
            return FetchByProperties(new String[] { "PlanogramHierarchyId" }, new Object[] { hierarchyId });
        }

        public void Upsert(IEnumerable<PlanogramGroupDto> dtoList, PlanogramGroupIsSetDto isSetDto)
        {
            base.Upsert(dtoList, isSetDto);
        }

        #endregion



    }
}
