﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27153 : A.Silva   ~ Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="IRenumberingStrategyInfoDal"/> for unit testing.
    /// </summary>
    public sealed class RenumberingStrategyInfoDal : MockDalBase<RenumberingStrategyInfoDto>, IRenumberingStrategyInfoDal
    {
        public override IEnumerable<RenumberingStrategyInfoDto> FetchByEntityId(Int32 entityId)
        {
            return FetchByProperties<RenumberingStrategyDto>(new[] {"EntityId"}, new Object[] {entityId});
        }

        /// <summary>
        ///     Returns an enumeration of <see cref="RenumberingStrategyInfoDto" /> matching the provided <paramref name="entityId" />, including deleted records.
        /// </summary>
        /// <param name="entityId">Id of the Entity that will be matched for fetching.</param>
        /// <returns>An enumeration of instances of <see cref="RenumberingStrategyInfoDto" /> wmatching the provided <paramref name="entityId" />.</returns>
        public IEnumerable<RenumberingStrategyInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return FetchByProperties<RenumberingStrategyDto>(new[] { "EntityId" }, new Object[] { entityId }, true);
        }
    }
}
