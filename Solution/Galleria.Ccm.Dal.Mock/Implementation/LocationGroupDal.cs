﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25445 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationGroupDal : MockDalBase<LocationGroupDto>, ILocationGroupDal
    {

        private void UpdateCalculatedProperties(IEnumerable<LocationGroupDto> dtoList)
        {
            foreach (LocationGroupDto dto in dtoList)
            {
                UpdateCalculatedProperties(dto);
            }
        }
        private void UpdateCalculatedProperties(LocationGroupDto dto)
        {
            dto.AssignedLocationsCount = 
                base.DalCache.GetDtos<LocationDto>().Where(l => l.LocationGroupId == dto.Id).Count();
        }

        #region ILocationGroupDal Members

        public new LocationGroupDto FetchById(Int32 id)
        {
            LocationGroupDto dto = base.FetchById(id);
            UpdateCalculatedProperties(dto);
            return dto;
        }

        public IEnumerable<LocationGroupDto> FetchByLocationHierarchyId(Int32 hierarchyId)
        {
            IEnumerable<LocationGroupDto> dtoList = 
                FetchByProperties(new String[] { "LocationHierarchyId" }, new Object[] { hierarchyId }).ToList();

            UpdateCalculatedProperties(dtoList);
            return dtoList;
        }

        public IEnumerable<LocationGroupDto> FetchByLocationHierarchyIdIncludingDeleted(Int32 locationHierarchyId)
        {
            IEnumerable<LocationGroupDto> dtoList =
                FetchByProperties(new String[] { "LocationHierarchyId" }, new Object[] { locationHierarchyId }, /*includeDeleted*/true).ToList();

            UpdateCalculatedProperties(dtoList);
            return dtoList;
        }

        public void Upsert(IEnumerable<LocationGroupDto> dtoList, LocationGroupIsSetDto isSetDto)
        {
            base.Upsert(dtoList, isSetDto);
        }

        #endregion





        
    }
}
