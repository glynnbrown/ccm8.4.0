﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-27059 : J.Pickup
//		Created
#endregion
#endregion

using System;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentRegionProductDal : MockDalBase<AssortmentRegionProductDto>, IAssortmentRegionProductDal
    {

        public IEnumerable<AssortmentRegionProductDto> FetchByRegionId(int AssortmentRegionId)
        {
            return FetchByProperties(new String[] { "AssortmentRegionId" }, new Object[] { AssortmentRegionId });
        }
    }
}
