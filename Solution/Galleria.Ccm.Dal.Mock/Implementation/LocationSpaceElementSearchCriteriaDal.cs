﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25631 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationSpaceElementSearchCriteriaDal : MockDalBase<LocationSpaceElementSearchCriteriaDto>, ILocationSpaceElementSearchCriteriaDal
    {

        //public override IEnumerable<String> ChildDtoTypes
        //{
        //    get
        //    {
        //        return new List<String>();
        //    }
        //}

        #region ILocationSpaceElementSearchCriteriaDal Members

        IEnumerable<LocationSpaceElementSearchCriteriaDto> ILocationSpaceElementSearchCriteriaDal.FetchByEntityId(Int32 entityId)
        {
            ILocationSpaceInfoDal locationSpaceInfoDal = _dalContext.GetDal<ILocationSpaceInfoDal>();
            ILocationSpaceProductGroupDal locationSpaceProductGroupDal = _dalContext.GetDal<ILocationSpaceProductGroupDal>();
            ILocationSpaceBayDal locationSpaceBayDal = _dalContext.GetDal<ILocationSpaceBayDal>();
            ILocationSpaceElementDal locationSpaceElementDal = _dalContext.GetDal<ILocationSpaceElementDal>();

            List<ProductGroupDto> productGroups = new List<ProductGroupDto>(DalCache.GetDtos<ProductGroupDto>());

            List<LocationSpaceInfoDto> locationSpaceInfos = new List<LocationSpaceInfoDto>(locationSpaceInfoDal.FetchByEntityId(entityId));
            List<LocationSpaceProductGroupDto> locationSpaceProductGroups = new List<LocationSpaceProductGroupDto>(DalCache.GetDtos<LocationSpaceProductGroupDto>());

            foreach (LocationSpaceInfoDto infoDto in locationSpaceInfos)
            {
                foreach (LocationSpaceProductGroupDto groupDto in locationSpaceProductGroups.Where(p => p.LocationSpaceId == infoDto.Id))
                {
                    foreach (LocationSpaceBayDto bayDto in locationSpaceBayDal.FetchByLocationSpaceProductGroupId(groupDto.Id))
                    {
                        foreach (LocationSpaceElementDto dto in locationSpaceElementDal.FetchByLocationSpaceBayId(bayDto.Id))
                        {
                            LocationSpaceElementSearchCriteriaDto returnValue = new LocationSpaceElementSearchCriteriaDto()
                            {
                                ProductGroupCode = productGroups.FirstOrDefault(p => p.Id == groupDto.ProductGroupId).Code,
                                LocationCode = infoDto.LocationCode,
                                BayOrder = bayDto.Order,
                                Order = dto.Order
                            };
                            yield return returnValue;
                        }
                    }
                }
            }
        }

        #endregion
    }
}
