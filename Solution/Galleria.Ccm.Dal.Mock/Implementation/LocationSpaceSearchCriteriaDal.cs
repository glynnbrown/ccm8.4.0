﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25631 : N.Haywood
//  Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationSpaceSearchCriteriaDal : MockDalBase<LocationSpaceSearchCriteriaDto>, ILocationSpaceSearchCriteriaDal
    {

        //public override IEnumerable<String> ChildDtoTypes
        //{
        //    get
        //    {
        //        return new List<String>();
        //    }
        //}

        #region ILocationSpaceSearchCriteriaDal Members

        IEnumerable<LocationSpaceSearchCriteriaDto> ILocationSpaceSearchCriteriaDal.FetchByEntityId(Int32 entityId)
        {
            ILocationSpaceInfoDal locationSpaceInfoDal = _dalContext.GetDal<ILocationSpaceInfoDal>();
            ILocationSpaceProductGroupDal locationSpaceProductGroupDal = _dalContext.GetDal<ILocationSpaceProductGroupDal>();
            IProductGroupDal productGroupDal = _dalContext.GetDal<IProductGroupDal>();
       
            List<ProductGroupDto> productGroups = new List<ProductGroupDto>(DalCache.GetDtos<ProductGroupDto>());

            List<LocationSpaceInfoDto> locationSpaceInfos = new List<LocationSpaceInfoDto>(locationSpaceInfoDal.FetchByEntityId(entityId));
            List<LocationSpaceProductGroupDto> locationSpaceProductGroups = new List<LocationSpaceProductGroupDto>(DalCache.GetDtos<LocationSpaceProductGroupDto>());

            foreach (LocationSpaceInfoDto infoDto in locationSpaceInfos)
            {
                foreach (LocationSpaceProductGroupDto groupDto in locationSpaceProductGroups.Where(p => p.LocationSpaceId == infoDto.Id))
                {
                    LocationSpaceSearchCriteriaDto returnValue = new LocationSpaceSearchCriteriaDto()
                    {
                        LocationSpaceId = infoDto.Id,
                        ProductGroupCode = productGroups.FirstOrDefault(p => p.Id == groupDto.ProductGroupId).Code,
                        LocationCode = infoDto.LocationCode
                    };
                    yield return returnValue;
                }
            }
        }

        #endregion
    }
}
