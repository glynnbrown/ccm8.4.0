﻿using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock DAL class for testing CRUD operations 
    /// </summary>
    internal class EntityComparisonAttributeDal : MockDalBase<EntityComparisonAttributeDto>, IEntityComparisonAttributeDal
    {
        /// <summary>
        /// Mock method to retrieve a mock EntityComparisonAttributeDto object.
        /// </summary>
        /// <param name="entityId">Id of the record to retrieve</param>
        /// <returns>Returns a pseudo copy of the <seealso cref="EntityComparisonAttributeDto"/></returns>
        public IEnumerable<EntityComparisonAttributeDto> FetchByEntityId(Object entityId)
        {
            return base.FetchByProperties<EntityComparisonAttributeDto>(new String[] { "EntityId" }, new Object[] { entityId }, false);
        }
    }
}
