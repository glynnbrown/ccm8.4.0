﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created.
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ClusterDal : MockDalBase<ClusterDto>, IClusterDal
    {

        public IEnumerable<ClusterDto> FetchByClusterSchemeId(int clusterSchemeId)
        {
            return base.FetchByProperties(new String[] { "ClusterSchemeId" }, new Object[] { clusterSchemeId });
        }

        public void Upsert(IEnumerable<ClusterDto> dtoList, ClusterIsSetDto isSetDto)
        {
            base.Upsert(dtoList, isSetDto);
        }
    }
}
