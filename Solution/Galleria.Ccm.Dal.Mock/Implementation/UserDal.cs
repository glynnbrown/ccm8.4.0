﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-25664 : A.Kuszyk
//		Created.
// V8-26222 : L.Luong
//		Modified
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    [PreloadedRecordCount(1)]
    public class UserDal : MockDalBase<UserDto>, IUserDal
    {
        public new UserDto FetchById(Int32 id)
        {
            return base.FetchSingleDtoByProperties(
                new String[] { "Id" }, 
                new Object[] { id }, 
                includeDeleted: true);
        }

        public UserDto FetchByUserName(string userName)
        {
            return base.FetchSingleDtoByProperties(new String[] { "UserName" },
                                                   new Object[] { userName },
                                                   includeDeleted: true);
        }           
    }
}
