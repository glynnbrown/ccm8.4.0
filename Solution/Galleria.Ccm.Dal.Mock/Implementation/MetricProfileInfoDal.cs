﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class MetricProfileInfoDal : MockDalBase<MetricProfileInfoDto>, IMetricProfileInfoDal
    {
        public new IEnumerable<MetricProfileInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<MetricProfileDto>(entityId);
        }
    }
}
