﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25669 : A.Kuszyk
//  Created.
// CCM-25242 : N.Haywood
//  Added fetch methods for imports
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIds.
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-26099 :I.George
//  Added FetchByMechandisingGroupIdCriteria
// V8-25556 : J.Pickup
//  Added FetchAllIncludingDeleted()  
#endregion
#region Version History: (CCM v8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductInfoDal : MockDalBase<ProductInfoDto>, IProductInfoDal
    {
        public IEnumerable<ProductInfoDto> FetchByEntityIdSearchCriteria(int entityId, string searchCriteria)
        {
            List<ProductInfoDto> returnList = new List<ProductInfoDto>();
            if (searchCriteria == null)
            {
                return returnList;
            }
            //remove special characters
            String processedCriteria = String.Empty;
            foreach (Char c in searchCriteria)
            {
                if (Char.IsLetterOrDigit(c))
                {
                    processedCriteria = String.Format(processedCriteria + c);
                }
            }

            //split into parts
            String[] criteriaParts = searchCriteria.ToLowerInvariant().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (ProductDto dto in DalCache.GetDtos<ProductDto>())
            {
                if (dto.EntityId == entityId)
                {
                    String productValue = String.Format("{0} {1}", dto.Gtin, dto.Name);

                    if (StatisfiesCriteria(productValue, criteriaParts))
                    {
                        returnList.Add(GetInfo(dto));
                    }
                }
            }

            return returnList;
        }

        public IEnumerable<ProductInfoDto> FetchByEntityIdProductGtins(int entityId, IEnumerable<string> productGtins)
        {
            return base.FetchByProperties<ProductDto>(new String[] { "EntityId", "Gtin" }, new Object[] { entityId, productGtins });
        }

        public List<ProductInfoDto> FetchByProductUniverseId(int productUniverseId)
        {
            List<ProductInfoDto> returnList = new List<ProductInfoDto>();

            //get a list of product ids for the universe
            List<Int32> productIds = this.DalCache.GetDtos<ProductUniverseProductDto>()
                .Where(p => p.ProductUniverseId == productUniverseId).Select(p => p.ProductId).ToList();

            foreach (ProductDto dto in this.DalCache.GetDtos<ProductDto>())
            {
                if (productIds.Contains(dto.Id))
                {
                    returnList.Add(GetInfo(dto));
                }
            }

            return returnList;
        }

        public IEnumerable<ProductInfoDto> FetchByProductIds(IEnumerable<int> productIds)
        {
            return base.FetchByProperties<ProductDto>(new String[] { "Id" }, new Object[] { productIds },true);
        }

        public new IEnumerable<ProductInfoDto> FetchByEntityId(Int32 entityId)
        {
            return FetchByEntityId<ProductDto>(entityId);
        }

        public IEnumerable<ProductInfoDto> FetchByEntityIdProductIds(int entityId, IEnumerable<int> productIds)
        {
            return FetchByProperties<ProductDto>(new String[] { "EntityId", "Id" }, new Object[] { entityId, productIds });
        }

        public new IEnumerable<ProductInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return FetchByEntityId<ProductDto>(entityId, true);
        }

        public IEnumerable<ProductInfoDto> FetchByMerchandisingGroupId(Int32 merchGroupId)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            IEnumerable<ProductUniverseDto> universeDtos = base.DalCache.GetDtos<ProductUniverseDto>().Where(p => p.ProductGroupId == merchGroupId);

            foreach (ProductUniverseDto universe in universeDtos)
            {
                foreach (ProductInfoDto dto in FetchByProductUniverseId(universe.Id))
                {
                    if (!dtoList.Contains(dto))
                    {
                        dtoList.Add(dto);
                    }
                }
            }
            return dtoList;           
       }

        public List<ProductInfoDto> FetchIllegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            return dtoList;
        }

        public List<ProductInfoDto> FetchLegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            return dtoList;
        }

        //public new IEnumerable<ProductInfoDto> FetchAllIncludingDeleted()
        //{
        //    return base.FetchAll<ProductDto>(includeDeleted: true);
        //}

        #region Private Methods

        private Boolean StatisfiesCriteria(String value, String[] containsParts)
        {
            String loweredValue = value.ToLowerInvariant();

            List<String> searchCriteria = new List<String>();
            List<String> searchOperators = new List<String>();

            for (Int32 i = 0; i < containsParts.Length; i++)
            {
                String part = containsParts[i];

                //part is a criteria
                if (part != "or" &&
                    part != "and")
                {
                    searchCriteria.Add(part);
                }
                else
                {
                    if (containsParts.Length > i + 1)
                    {
                        String nextPart = containsParts[i + 1];
                        if (nextPart != "or" &&
                            nextPart != "and")
                        {
                            if (!String.IsNullOrEmpty(nextPart) &&
                                searchCriteria.Count > searchOperators.Count)
                            {
                                searchOperators.Add(part);
                            }
                        }
                        else
                        {
                            //If the next part is OR or AND, but is the last part use the current as the operator
                            if (!(containsParts.Length == i + 2))
                            {
                                searchCriteria.Add(part);
                            }
                            else
                            {
                                searchOperators.Add(part);
                            }
                        }
                    }
                    else
                    {
                        //This is the last part of the search string
                        searchCriteria.Add(part);
                    }
                }
            }

            //Chech the conditions
            if (searchOperators.Contains("or"))
            {
                foreach (String criteria in searchCriteria)
                {
                    if (loweredValue.Contains(criteria))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                foreach (String criteria in searchCriteria)
                {
                    if (!loweredValue.Contains(criteria))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private ProductInfoDto GetInfo(ProductDto dto)
        {
            return new ProductInfoDto()
            {
                Id = dto.Id,
                EntityId = dto.EntityId,
                Gtin = dto.Gtin,
                Name = dto.Name

            };
        }

        #endregion

    }
}
