﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
// CCM-26140 : I.George
//  Added the Insert overide function
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationSpaceDal : MockDalBase<LocationSpaceDto>, ILocationSpaceDal
    {
        //public override IEnumerable<String> ChildDtoTypes
        //{
        //    get
        //    {
        //        yield return "LocationSpaceProductGroupDto";
        //    }
        //}

        #region ILocationSpaceDal Members

        //public override void DeleteById(Int32 id)
        //{
        //    DeleteById(id, new List<String> { "LocationSpaceProductGroupDto", "LocationSpaceBayDto", "LocationSpaceElementDto" });
        //}

        //public override void DeleteByEntityId(Int32 entityId)
        //{
        //    DeleteByEntityId(entityId, new List<String> { "LocationSpaceProductGroupDto", "LocationSpaceBayDto", "LocationSpaceElementDto" });
        //}

        public override LocationSpaceDto FetchById(Int32 id)
        {
            return base.FetchById(id);
        }

        public override void Insert(LocationSpaceDto dto)
        {
            base.Insert(dto, /*canReinsert*/true);
        }

        #endregion
    }
}
