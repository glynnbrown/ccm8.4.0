﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM V8.0.3
// CCM-29326 : Martin Shelley
//	Created as it was missing in previous versions of V8
#endregion

#region Version Histort: CCM V8.1.0
// V8-29598 : L.Luong
//  fetch methods should now fetch correct dtolist
#endregion

#region Version History: CCM840

// CCM-13408 : J.Mendes
//  Added LocationSpaceProductGroup fields to the Planogram Assignment

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanAssignmentStoreSpaceInfoDal : MockDalBase<PlanAssignmentStoreSpaceInfoDto>, IPlanAssignmentStoreSpaceInfoDal
    {
        public IEnumerable<PlanAssignmentStoreSpaceInfoDto> FetchByEntityIdProductGroupId(Int32 entityId, Int32 productGroupId, Int32? clusterSchemeId)
        {
            List<PlanAssignmentStoreSpaceInfoDto> dtoList = new List<PlanAssignmentStoreSpaceInfoDto>();
            ClusterSchemeDto clusterScheme = null;

            // get cluster scheme
            if (clusterSchemeId == null)
            {
                clusterScheme = base.DalCache.GetDtos<ClusterSchemeDto>().Where(c => c.EntityId == entityId && c.IsDefault == true).FirstOrDefault();
            }
            else 
            {
                clusterScheme = base.DalCache.GetDtos<ClusterSchemeDto>().Where(c => c.Id == clusterSchemeId).FirstOrDefault();
            }

            if (clusterScheme == null)
            {
                clusterScheme = base.DalCache.GetDtos<ClusterSchemeDto>().Where(c => c.EntityId == entityId).FirstOrDefault();
            }

            List<ClusterDto> clusters = base.DalCache.GetDtos<ClusterDto>().Where(c => c.ClusterSchemeId == clusterScheme.Id).ToList();
            List<ClusterLocationDto> clusterLocations = base.DalCache.GetDtos<ClusterLocationDto>().ToList();
            List<LocationDto> locations = base.DalCache.GetDtos<LocationDto>().Where(l => l.EntityId == entityId).ToList();
            List<LocationSpaceDto> locationSpaces = base.DalCache.GetDtos<LocationSpaceDto>().Where(l => l.EntityId == entityId).ToList();
            ProductGroupDto productGroup = base.DalCache.GetDtos<ProductGroupDto>().Where(p => p.Id == productGroupId).FirstOrDefault();

            foreach (LocationDto location in locations) 
            {
                ClusterLocationDto clusterLocation = null;
                PlanAssignmentStoreSpaceInfoDto planStoreSpace = null;
                ClusterDto selectedCluster = null;

                // gets the cluster which the location is in
                foreach(ClusterDto cluster in clusters)
                {
                    clusterLocation = clusterLocations.Where(c => c.LocationId == location.Id && c.ClusterId == cluster.Id).FirstOrDefault();
                    if(clusterLocation != null)
                    {
                        selectedCluster = cluster;
                    }
                }

                if (selectedCluster != null)
                {
                    LocationSpaceDto locationSpace = locationSpaces.Where(l => l.LocationId == location.Id).FirstOrDefault();
                    if (locationSpace != null)
                    {
                        LocationSpaceProductGroupDto locationSpaceProductGroup = base.DalCache.GetDtos<LocationSpaceProductGroupDto>().Where(l => l.ProductGroupId == productGroupId).FirstOrDefault();

                        if (locationSpaceProductGroup != null)
                        {
                            List<LocationSpaceBayDto> locationSpaceBays = base.DalCache.GetDtos<LocationSpaceBayDto>().Where(l => l.LocationSpaceProductGroupId == locationSpaceProductGroup.ProductGroupId).ToList();

                            planStoreSpace = new PlanAssignmentStoreSpaceInfoDto()
                            {
                                ClusterName = selectedCluster.Name,
                                ClusterSchemeName = clusterScheme.Name,
                                LocationId = location.Id,
                                LocationCode = location.Code,
                                LocationName = location.Name,
                                LocationAddress = location.Address1 + ", " + location.Address2 + ", " + location.City + ", " + location.Country,
                                BayCount = locationSpaceBays.Count(),
                                BayTotalWidth = locationSpaceBays.Sum(l => l.Width),
                                BayHeight = locationSpaceBays.Max(l => l.Height),
                                ProductGroupId = productGroup.Id,
                                ProductGroupCode = productGroup.Code,
                                ProductGroupName = productGroup.Name,
                                ProductCount = (int)locationSpaceProductGroup.ProductCount,
                                AverageBayWidth = locationSpaceProductGroup.AverageBayWidth,
                                AisleName = locationSpaceProductGroup.AisleName,
                                ValleyName = locationSpaceProductGroup.ValleyName,
                                ZoneName = locationSpaceProductGroup.ZoneName,
                                CustomAttribute01 = locationSpaceProductGroup.CustomAttribute01,
                                CustomAttribute02 = locationSpaceProductGroup.CustomAttribute02,
                                CustomAttribute03 = locationSpaceProductGroup.CustomAttribute03,
                                CustomAttribute04 = locationSpaceProductGroup.CustomAttribute04,
                                CustomAttribute05 = locationSpaceProductGroup.CustomAttribute05,
                            };
                            dtoList.Add(planStoreSpace);
                        }
                    }
                }
            }

            return dtoList;
        }

        public IEnumerable<PlanAssignmentStoreSpaceInfoDto> FetchByEntityIdLocationId(Int32 entityId, Int16 locationId, Int32? clusterSchemeId)
        {
            List<PlanAssignmentStoreSpaceInfoDto> dtoList = new List<PlanAssignmentStoreSpaceInfoDto>();
            ClusterSchemeDto clusterScheme = null;

            // get cluster scheme
            if (clusterSchemeId == null)
            {
                clusterScheme = base.DalCache.GetDtos<ClusterSchemeDto>().Where(c => c.EntityId == entityId && c.IsDefault == true).FirstOrDefault();
            }
            else
            {
                clusterScheme = base.DalCache.GetDtos<ClusterSchemeDto>().Where(c => c.Id == clusterSchemeId).FirstOrDefault();
            }

            if (clusterScheme == null)
            {
                clusterScheme = base.DalCache.GetDtos<ClusterSchemeDto>().Where(c => c.EntityId == entityId).FirstOrDefault();
            }

            List<ClusterDto> clusters = base.DalCache.GetDtos<ClusterDto>().Where(c => c.ClusterSchemeId == clusterScheme.Id).ToList();
            LocationDto location = base.DalCache.GetDtos<LocationDto>().Where(l => l.EntityId == entityId && l.Id == locationId).FirstOrDefault();
            List<LocationSpaceDto> locationSpaces = base.DalCache.GetDtos<LocationSpaceDto>().Where(l => l.EntityId == entityId).ToList();
            List<ProductGroupDto> productGroups = base.DalCache.GetDtos<ProductGroupDto>().ToList();
            List<ClusterLocationDto> clusterLocations = base.DalCache.GetDtos<ClusterLocationDto>().Where(c => c.LocationId == locationId).ToList();

            foreach (ProductGroupDto productGroup in productGroups)
            {
                ClusterLocationDto clusterLocation = null;
                PlanAssignmentStoreSpaceInfoDto planStoreSpace = null;
                ClusterDto selectedCluster = null;

                // gets the cluster where the location is in 
                foreach (ClusterDto cluster in clusters)
                {
                    clusterLocation = clusterLocations.Where(c => c.ClusterId == cluster.Id).FirstOrDefault();
                    if (clusterLocation != null)
                    {
                        selectedCluster = cluster;
                    }
                }

                if (selectedCluster != null)
                {
                    List<LocationSpaceProductGroupDto> locationSpaceProductGroups = base.DalCache.GetDtos<LocationSpaceProductGroupDto>().Where(l => l.ProductGroupId == productGroup.Id).ToList();

                    foreach(LocationSpaceProductGroupDto locationSpaceProductGroup in locationSpaceProductGroups)
                    {
                        LocationSpaceDto locationSpace = base.DalCache.GetDtos<LocationSpaceDto>().Where(l => l.Id == locationSpaceProductGroup.LocationSpaceId && l.LocationId == locationId).FirstOrDefault();
                        if (locationSpace != null)
                        {
                            List<LocationSpaceBayDto> locationSpaceBays = base.DalCache.GetDtos<LocationSpaceBayDto>().Where(l => l.LocationSpaceProductGroupId == locationSpaceProductGroup.ProductGroupId).ToList();

                            planStoreSpace = new PlanAssignmentStoreSpaceInfoDto()
                            {
                                ClusterName = selectedCluster.Name,
                                ClusterSchemeName = clusterScheme.Name,
                                LocationId = location.Id,
                                LocationCode = location.Code,
                                LocationName = location.Name,
                                LocationAddress = location.Address1 + ", " + location.Address2 + ", " + location.City + ", " + location.Country,
                                BayCount = locationSpaceBays.Count(),
                                BayTotalWidth = locationSpaceBays.Sum(l => l.Width),
                                BayHeight = locationSpaceBays.Max(l => l.Height),
                                ProductGroupId = productGroup.Id,
                                ProductGroupCode = productGroup.Code,
                                ProductGroupName = productGroup.Name,
                                ProductCount = (int)locationSpaceProductGroup.ProductCount,
                                AverageBayWidth = locationSpaceProductGroup.AverageBayWidth,
                                AisleName = locationSpaceProductGroup.AisleName,
                                ValleyName = locationSpaceProductGroup.ValleyName,
                                ZoneName = locationSpaceProductGroup.ZoneName,
                                CustomAttribute01 = locationSpaceProductGroup.CustomAttribute01,
                                CustomAttribute02 = locationSpaceProductGroup.CustomAttribute02,
                                CustomAttribute03 = locationSpaceProductGroup.CustomAttribute03,
                                CustomAttribute04 = locationSpaceProductGroup.CustomAttribute04,
                                CustomAttribute05 = locationSpaceProductGroup.CustomAttribute05,
                            };
                            dtoList.Add(planStoreSpace);
                        }
                    }
                
                }
            }

            return dtoList;
        }
    }
}
