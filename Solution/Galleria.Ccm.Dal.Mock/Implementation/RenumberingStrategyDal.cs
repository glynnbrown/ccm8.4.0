﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27153 : A.Silva
//      Created
// V8-27562 : A.Silva
//      Added FetchByEntityIdName.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="IRenumberingStrategyDal"/> for unit testing.
    /// </summary>
    public class RenumberingStrategyDal : MockDalBase<RenumberingStrategyDto>, IRenumberingStrategyDal
    {
        /// <summary>
        ///     Returns a <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="entityId" /> and <paramref name="name"/>.
        /// </summary>
        /// <param name="entityId">Id of the Entity that will be matched for fetching.</param>
        /// <param name="name">Name to be matched for fetching.</param>
        /// <returns>An instance of <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="entityId" /> and <paramref name="name"/>.</returns>
        public RenumberingStrategyDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return FetchSingleDtoByProperties(new[] {"EntityId", "Name"}, new Object[] {entityId, name});
        }
    }
}