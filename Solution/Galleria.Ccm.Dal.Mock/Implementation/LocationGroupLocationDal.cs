﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25445 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationGroupLocationDal : MockDalBase<LocationGroupLocationDto>, ILocationGroupLocationDal
    {
        #region Fetch

        public IEnumerable<LocationGroupLocationDto> FetchByLocationGroupId(Int32 locationGroupId)
        {
            List<LocationGroupLocationDto> dtoList = new List<LocationGroupLocationDto>();

            foreach (LocationDto locDto in base.DalCache.GetDtos<LocationDto>())
            {
                if (locDto.LocationGroupId == locationGroupId)
                {
                    dtoList.Add(
                        new LocationGroupLocationDto()
                        {
                            LocationId = locDto.Id,
                            Code = locDto.Code,
                            Name = locDto.Name,
                            LocationGroupId = locDto.LocationGroupId
                        });

                }
            }

            return dtoList;
        }

        #endregion

        #region Update

        public void Update(LocationGroupLocationDto dto)
        {
            Boolean updateOk = false;

            foreach (LocationDto locDto in base.DalCache.GetDtos<LocationDto>())
            {
                if (locDto.Id == dto.LocationId)
                {
                    //update the location dto
                    locDto.LocationGroupId = dto.LocationGroupId;
                    locDto.RowVersion = new RowVersion(locDto.RowVersion + 1);

                    ////update the locationgrouplocation dto
                    //dto.Code = locDto.Code;
                    //dto.Name = locDto.Name;

                    updateOk = true;
                }
            }

            if (!updateOk)
            {
                throw new Exception("Location id not found");
            }
        }

        #endregion

        #region Delete

        public void DeleteLocationById(Int16 locationId)
        {
            //just delete the location through it's own dal.
            using (ILocationDal dal = base.DalContext.GetDal<ILocationDal>())
            {
                dal.DeleteById(locationId);
            }
        }


        #endregion
    }
}
