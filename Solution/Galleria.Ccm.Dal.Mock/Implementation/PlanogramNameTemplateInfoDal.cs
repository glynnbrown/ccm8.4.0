﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.0.2
// V8-29010 : D.Pleasance
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramNameTemplateInfoDal : MockDalBase<PlanogramNameTemplateInfoDto>, IPlanogramNameTemplateInfoDal
    {
        public new IEnumerable<PlanogramNameTemplateInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByEntityId<PlanogramNameTemplateDto>(entityId);
        }        
    }
}