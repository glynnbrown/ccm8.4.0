﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26704 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentRegionDal : MockDalBase<AssortmentRegionDto>, IAssortmentRegionDal
    {
        public IEnumerable<AssortmentRegionDto> FetchByAssortmentId(int assortmentId)
        {
            return base.FetchByProperties(new String[] { "AssortmentId" }, new Object[] { assortmentId });
        }
    }
}
