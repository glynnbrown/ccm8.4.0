﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock dal implementation of IFixtureSubComponentDal
    /// </summary>
    public class FixtureSubComponentDal : MockDalBase<FixtureSubComponentDto>, IFixtureSubComponentDal
    {
        /// <summary>
        /// Returns all items for the given fixture component id.
        /// </summary>
        /// <param name="fixtureComponentId"></param>
        /// <returns></returns>
        public IEnumerable<FixtureSubComponentDto> FetchByFixtureComponentId(Int32 fixtureComponentId)
        {
            return FetchByProperties(new String[] { "FixtureComponentId" }, new Object[] { fixtureComponentId });
        }
    }
}
