﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ConsumerDecisionTreeLevelDal : MockDalBase<ConsumerDecisionTreeLevelDto>, IConsumerDecisionTreeLevelDal
    {
        public new ConsumerDecisionTreeLevelDto FetchById(Int32 id)
        {
            return base.FetchSingleDtoByProperties(new String[] { "Id" }, new Object[] { id });
        }

        public IEnumerable<ConsumerDecisionTreeLevelDto> FetchByConsumerDecisionTreeId(int consumerDecisionTreeId)
        {
            return base.FetchByProperties(new String[] { "ConsumerDecisionTreeId" }, new Object[] { consumerDecisionTreeId });
        }

        public new void Insert(ConsumerDecisionTreeLevelDto dto)
        {
            base.Insert(dto, /*canReinsert*/true);
        }
    }
}
