﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created.
// V8-26815 : A.Silva ~ Added LockById and UnlockById. Used in the User Dal when accessing files.
// V8-27269 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ValidationTemplateDal : MockDalBase<ValidationTemplateDto>, IValidationTemplateDal
    {
        /// <summary>
        ///     Locks a validation template for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <returns><c>True</c> if the validation template was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public bool LockById(Object id)
        {
            return true; // Not implemented as this dal does not use locking mechanisms.
        }

        /// <summary>
        ///     Unlocks a validation template after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }


        public ValidationTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name });
        }
    }
}