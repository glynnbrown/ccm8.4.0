﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25395 : A.Probyn
//		Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock dal implementation of IProductLibraryInfoDal
    /// </summary>
    public class ProductLibraryInfoDal : MockDalBase<ProductLibraryInfoDto>, IProductLibraryInfoDal
    {
        #region Data Transfer Object

        private ProductLibraryInfoDto GetDto(ProductLibraryDto productLibraryDto)
        {
            return new ProductLibraryInfoDto()
            {
                Id = productLibraryDto.Id
            };
        }

        #endregion


        //public IEnumerable<ProductLibraryInfoDto> FetchByIds(IEnumerable<Object> ids)
        //{
        //    List<ProductLibraryInfoDto> dtoList = new List<ProductLibraryInfoDto>();

        //    foreach (ProductLibraryDto dto in base.DalCache.GetDtos<ProductLibraryDto>())
        //    {
        //        if (ids.Contains(dto.Id))
        //        {
        //            dtoList.Add(GetDto(dto));
        //        }
        //    }

        //    return dtoList;
        //}

        public IEnumerable<ProductLibraryInfoDto> FetchByIds(IEnumerable<object> ids)
        {
            return base.FetchByProperties<ProductLibraryDto>(new String[] { "Id" }, new Object[] { ids }, /*incDeleted*/false);
        }

        public new IEnumerable<ProductLibraryInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<ProductLibraryDto>(entityId);
        }
    }
}
