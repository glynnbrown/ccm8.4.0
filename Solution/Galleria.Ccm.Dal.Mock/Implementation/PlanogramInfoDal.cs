﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25608 : N.Foster
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// CCM-26520 :J.Pickup
//	    Added FetchByCategoryCode & FetchByNullCategoryCode
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteriaAsync
// V8-28493 : L.Ineson
//  Added FetchByWorkpackageIdAutomationProcessingStatus
// V8-26322 : A.Silva
//      Amended FetchByWorkpackageIdAutomationProcessingStatus so that the automation process status is gotten as in the data base.

#endregion
#region Version History: (CCM 801)
// V8-28507 : D.Pleasance
//  Added FetchByWorkpackageIdPagingCriteria \ FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria
// V8-28838 : L.Ineson
//  Updated CreateDataTransferObject to get lock values.
#endregion

#region Version History: CCM810
// V8-29986 : A.Probyn
//  Added FetchNonDebugByCategoryCode
// V8-30213 : L.Luong
//  Added FetchNonDebugByLocationCode
#endregion
#region Version History : CCM 830
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Mock.Implementation;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramInfoDal : MockDalBase<PlanogramInfoDto>, IPlanogramInfoDal
    {
        #region Data Transfer Object

        private PlanogramInfoDto CreateDataTransferObject(PlanogramDto planDto)
        {
            PackageDto packageDto = base.DalCache.GetDtos<PackageDto>().First(p => Object.Equals(p.Id, planDto.Id));

            Byte? lockType;
            Int32? lockUserId;
            Boolean isPlanLocked = ((PackageDal)base.DalContext.GetDal<IPackageDal>()).IsPlanogramLocked(packageDto.Id, out lockUserId, out lockType);
            UserDto lockUser = (lockUserId.HasValue) ? base.DalContext.GetDal<IUserDal>().FetchById(lockUserId.Value) : null;

            PlanogramGroupPlanogramDto planGroupDto=
                base.DalCache.GetDtos<PlanogramGroupPlanogramDto>().FirstOrDefault(p => Object.Equals(p.PlanogramId, planDto.Id));

            String planGroupName =null;
            if(planGroupDto != null)
            {
                planGroupName =
                base.DalCache.GetDtos<PlanogramGroupDto>().First(p => p.Id == planGroupDto.PlanogramGroupId)
                .Name;
            }

            return new PlanogramInfoDto()
                    {
                        Id = Convert.ToInt32(planDto.Id),
                        Name = planDto.Name,
                        PlanogramType = planDto.PlanogramType,
                        DateCreated = packageDto.DateCreated,
                        DateLastModified = packageDto.DateLastModified,
                        DateDeleted = packageDto.DateDeleted,
                        AreaUnitsOfMeasure = planDto.AreaUnitsOfMeasure,
                        CategoryCode = planDto.CategoryCode,
                        CategoryName = planDto.CategoryName,
                        CurrencyUnitsOfMeasure = planDto.CurrencyUnitsOfMeasure,
                        DateMetadataCalculated = packageDto.DateMetadataCalculated,
                        DateValidationDataCalculated = packageDto.DateValidationDataCalculated,
                        Depth = planDto.Depth,
                        Height = planDto.Height,
                        LengthUnitsOfMeasure = planDto.LengthUnitsOfMeasure,
                        MetaAverageCases = planDto.MetaAverageCases,
                        MetaAverageDos = planDto.MetaAverageDos,
                        MetaAverageFacings = planDto.MetaAverageFacings,
                        MetaAverageFrontFacings = planDto.MetaAverageFrontFacings,
                        MetaAverageUnits = planDto.MetaAverageUnits,
                        MetaBayCount = planDto.MetaBayCount,
                        MetaBlocksDropped = planDto.MetaBlocksDropped,
                        MetaChangeFromPreviousStarRating = planDto.MetaChangeFromPreviousStarRating,
                        MetaChangesFromPreviousCount = planDto.MetaChangesFromPreviousCount,
                        MetaComponentCount = planDto.MetaComponentCount,
                        MetaMaxCases = planDto.MetaMaxCases,
                        MetaMaxDos = planDto.MetaMaxDos,
                        MetaMinCases = planDto.MetaMinCases,
                        MetaMinDos = planDto.MetaMinDos,
                        MetaNewProducts = planDto.MetaNewProducts,
                        MetaProductsPlaced = planDto.MetaProductsPlaced,
                        MetaNotAchievedInventory = planDto.MetaNotAchievedInventory,
                        MetaProductsUnplaced = planDto.MetaProductsUnplaced,
                        MetaTotalAreaWhiteSpace = planDto.MetaTotalAreaWhiteSpace,
                        MetaTotalComponentCollisions = planDto.MetaTotalComponentCollisions,
                        MetaTotalComponentsOverMerchandisedDepth = planDto.MetaTotalComponentsOverMerchandisedDepth,
                        MetaTotalLinearWhiteSpace = planDto.MetaTotalLinearWhiteSpace,
                        MetaTotalMerchandisableAreaSpace = planDto.MetaTotalMerchandisableAreaSpace,
                        MetaTotalComponentsOverMerchandisedHeight = planDto.MetaTotalComponentsOverMerchandisedHeight,
                        VolumeUnitsOfMeasure = planDto.VolumeUnitsOfMeasure,
                        Width = planDto.Width,
                        WeightUnitsOfMeasure = planDto.WeightUnitsOfMeasure,
                        MetaTotalFacings = planDto.MetaTotalFacings,
                        UserName = packageDto.UserName,
                        ProductPlacementX = planDto.ProductPlacementX,
                        ProductPlacementY = planDto.ProductPlacementY,
                        ProductPlacementZ = planDto.ProductPlacementZ,
                        MetaTotalUnits = planDto.MetaTotalUnits,
                        MetaTotalFrontFacings = planDto.MetaTotalFrontFacings,
                        MetaTotalMerchandisableLinearSpace = planDto.MetaTotalMerchandisableLinearSpace,
                        MetaTotalComponentsOverMerchandisedWidth = planDto.MetaTotalComponentsOverMerchandisedWidth,
                        MetaHasComponentsOutsideOfFixtureArea = planDto.MetaHasComponentsOutsideOfFixtureArea,
                        MetaTotalMerchandisableVolumetricSpace = planDto.MetaTotalMerchandisableVolumetricSpace,
                        MetaTotalVolumetricWhiteSpace = planDto.MetaTotalVolumetricWhiteSpace,
                        MetaUniqueProductCount = planDto.MetaUniqueProductCount,
                        MetaTotalPositionCollisions = planDto.MetaTotalPositionCollisions,
                        IsLocked = isPlanLocked,
                        LockType = lockType,
                        LockUserId = lockUserId,
                        LockDate = (isPlanLocked)? (DateTime?)DateTime.Now : null,
                        LockUserName = (lockUser != null)? lockUser.UserName : null,
                        LockUserDisplayName = (lockUser != null) ? lockUser.FirstName : null,

                        ClusterSchemeName = planDto.ClusterSchemeName,
                        ClusterName = planDto.ClusterName,
                        LocationCode = planDto.LocationCode,
                        LocationName = planDto.LocationName,
                        DateWip = planDto.DateWip,
                        DateApproved = planDto.DateApproved,
                        DateArchived = planDto.DateArchived,

                        PlanogramGroupId = (planGroupDto != null)? (Int32?)planGroupDto.PlanogramGroupId : null,
                        PlanogramGroupName = planGroupName,
                    };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns all planogram info objects for the specified product group
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByPlanogramGroupId(Int32 productGroupId)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            List<Int32> planIds =
                base.DalCache.GetDtos<PlanogramGroupPlanogramDto>()
                .Where(d => d.PlanogramGroupId == productGroupId)
                .Select(d => d.PlanogramId).ToList();

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if (planIds.Contains(Convert.ToInt32(planDto.Id)))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }

            return dtoList;
        }

        /// <summary>
        /// Returns all planogram info objects for the specified workpackage
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            List<Int32> planIds =
                base.DalCache.GetDtos<WorkpackagePlanogramDto>()
                .Where(d => d.WorkpackageId == workpackageId)
                .Select(d => d.DestinationPlanogramId).ToList();

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if (planIds.Contains(Convert.ToInt32(planDto.Id)))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }


            return dtoList;
        }

        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdPagingCriteria(Int32 workpackageId, Byte pageNumber, Int16 pageSize)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            List<Int32> planIds =
                base.DalCache.GetDtos<WorkpackagePlanogramDto>()
                .Where(d => d.WorkpackageId == workpackageId)
                .Select(d => d.DestinationPlanogramId).ToList();

            Int32 rowNumber = 1;

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if ((rowNumber > ((pageNumber - 1) * pageSize) && rowNumber <= (pageNumber * pageSize)))
                {
                    if (planIds.Contains(Convert.ToInt32(planDto.Id)))
                    {
                        dtoList.Add(CreateDataTransferObject(planDto));
                    }
                }

                rowNumber++;
            }
            
            return dtoList;
        }

        /// <summary>
        /// Returns infos for the given workpackage id and processing status
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdAutomationProcessingStatus(Int32 workpackageId,
            Byte automationProcessingStatus)
        {
            // Get the possible Ids by Planogram Processing Status.
            var matchingIdsByProcessingStatus = DalCache
                .GetDtos<WorkpackagePlanogramProcessingStatusDto>()
                .Where(dto => dto.AutomationStatus == automationProcessingStatus && dto.WorkpackageId == workpackageId)
                .Select(dto => dto.PlanogramId);
            var matchingIdsByWorkpackageId = DalCache
                .GetDtos<WorkpackagePlanogramDto>()
                .Where(d => d.WorkpackageId == workpackageId)
                .Select(d => d.DestinationPlanogramId);
            var matchingPlanogramIds = matchingIdsByProcessingStatus
                .Intersect(matchingIdsByWorkpackageId)
                .ToList();

            return DalCache
                .GetDtos<PlanogramDto>()
                .Where(planDto => matchingPlanogramIds.Contains(Convert.ToInt32(planDto.Id)))
                .Select(CreateDataTransferObject)
                .ToList();
        }

        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(int workpackageId, List<byte> automationProcessingStatusList, byte pageNumber, short pageSize)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            // Get the possible Ids by Planogram Processing Status.
            var matchingIdsByProcessingStatus = DalCache
                .GetDtos<WorkpackagePlanogramProcessingStatusDto>()
                .Where(dto => automationProcessingStatusList.Contains(dto.AutomationStatus))
                .Select(dto => dto.PlanogramId);
            var matchingIdsByWorkpackageId = DalCache
                .GetDtos<WorkpackagePlanogramDto>()
                .Where(d => d.WorkpackageId == workpackageId)
                .Select(d => d.DestinationPlanogramId);
            var matchingPlanogramIds = matchingIdsByProcessingStatus
                .Intersect(matchingIdsByWorkpackageId)
                .ToList();
            
            Int32 rowNumber = 1;

            foreach (PlanogramDto planDto in DalCache.GetDtos<PlanogramDto>()
                                                .Where(planDto => matchingPlanogramIds.Contains(Convert.ToInt32(planDto.Id)))
                                                .ToList())
            {
                if ((rowNumber > ((pageNumber - 1) * pageSize) && rowNumber <= (pageNumber * pageSize)))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));                    
                }

                rowNumber++;
            }

            return dtoList;
        }

        /// <summary>
        /// Returns all planograms based on the given search criteria
        /// </summary>
        /// <param name="name"></param>
        /// <param name="planogramGroupId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramInfoDto> FetchBySearchCriteria(String name, Int32? planogramGroupId, Int32 entityId)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();


            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                PlanogramGroupPlanogramDto pgp = null;
                if (planogramGroupId.HasValue)
                {
                    pgp = base.DalCache.GetDtos<PlanogramGroupPlanogramDto>()
                        .FirstOrDefault(p => p.PlanogramGroupId == planogramGroupId.Value && p.PlanogramId == Convert.ToInt32(planDto.Id));
                }


                if ((String.IsNullOrEmpty(name) || planDto.Name.ToLowerInvariant().Contains(name.ToLowerInvariant()))
                    && (!planogramGroupId.HasValue || pgp != null))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }


            //TODO: Need to remove dto's that don't have a matching entity id before returning.
            return dtoList;
        }

        /// <summary>
        /// Returns all planogram info objects for the given ids
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByIds(IEnumerable<Int32> idList)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if (idList.Contains(Convert.ToInt32(planDto.Id)))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }

            return dtoList;
        }


        /// <summary>
        /// Returns all planogram info objects for the specified category code
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByCategoryCode(String categoryCode)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            List<String> categoryIds =
                base.DalCache.GetDtos<PlanogramInfoDto>()
                .Where(d => d.CategoryCode == categoryCode)
                .Select(d => d.CategoryCode).ToList();

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if (categoryIds.Contains(planDto.CategoryCode))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }

            return dtoList;
        }
        
        /// <summary>
        /// Returns all non debug planogram info objects for the specified category code
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchNonDebugByCategoryCode(String categoryCode)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            List<String> categoryIds =
                base.DalCache.GetDtos<PlanogramInfoDto>()
                .Where(d => d.CategoryCode == categoryCode)
                .Select(d => d.CategoryCode).ToList();

            //Get planogram group planogram
            List<Int32> planogramGroupPlanogramIds = base.DalCache.GetDtos<PlanogramGroupPlanogramDto>()
                .Select(p => p.PlanogramId).Distinct().ToList();

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if (categoryIds.Contains(planDto.CategoryCode) && planogramGroupPlanogramIds.Contains((Int32)planDto.Id))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }

            return dtoList;
        }

        /// <summary>
        /// Returns all non debug planogram info objects for the specified location code
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchNonDebugByLocationCode(String locationCode)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            List<String> locationIds =
                base.DalCache.GetDtos<PlanogramInfoDto>()
                .Where(d => d.LocationCode == locationCode)
                .Select(d => d.LocationCode).ToList();

            //Get planogram group planogram
            List<Int32> planogramGroupPlanogramIds = base.DalCache.GetDtos<PlanogramGroupPlanogramDto>()
                .Select(p => p.PlanogramId).Distinct().ToList();

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if (locationIds.Contains(planDto.LocationCode) && planogramGroupPlanogramIds.Contains((Int32)planDto.Id))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }

            return dtoList;
        }


        /// <summary>
        /// Returns all planogram info objects with null specified for category code
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByNullCategoryCode()
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            List<String> categoryIds =
                base.DalCache.GetDtos<PlanogramInfoDto>()
                .Where(d => d.CategoryCode == null)
                .Select(d => d.CategoryCode).ToList();

            foreach (PlanogramDto planDto in base.DalCache.GetDtos<PlanogramDto>())
            {
                if (categoryIds.Contains(planDto.CategoryCode))
                {
                    dtoList.Add(CreateDataTransferObject(planDto));
                }
            }

            return dtoList;
        }

        public IEnumerable<PlanogramInfoDto> FetchByMetadataCalculationRequired()
        {
            return this.DalCache.GetDtos<PlanogramInfoDto>().Where(p => p.DateMetadataCalculated == null);
        }

        public IEnumerable<PlanogramInfoDto> FetchByValidationCalculationRequired()
        {
            return this.DalCache.GetDtos<PlanogramInfoDto>().Where(p => p.DateValidationDataCalculated == null);
        }

        #endregion
    }
}
