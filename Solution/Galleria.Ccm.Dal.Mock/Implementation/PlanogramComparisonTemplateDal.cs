﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="PlanogramComparisonTemplateDal"/>.
    /// </summary>
    public class PlanogramComparisonTemplateDal : MockDalBase<PlanogramComparisonTemplateDto>, IPlanogramComparisonTemplateDal
    {
        #region Fetch

        public PlanogramComparisonTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return FetchSingleDtoByProperties(new[] { "EntityId", "Name" }, new Object[] { entityId, name });
        }

        #endregion

        #region Locking

        /// <summary>
        ///     Lock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be locked.</param>
        public void LockById(Object id)
        {
            //  The Mock does not lock, do nothing.
        }

        /// <summary>
        ///     Unlock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be unlocked.</param>
        public void UnlockById(Object id)
        {
            //  The Mock does not unlock, do nothing.
        }

        #endregion
    }
}