﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationSpaceProductGroupInfoDal : MockDalBase<LocationSpaceProductGroupInfoDto>, ILocationSpaceProductGroupInfoDal
    {
        #region ILocationSpaceProductGroupInfoDal Members

        public IEnumerable<LocationSpaceProductGroupInfoDto> FetchByProductGroupId(Int32 productGroupId)
        {
            List<LocationSpaceDto> sourceLocationSpaceDtoList = this.DalCache.GetDtos<LocationSpaceDto>().ToList();
            List<LocationSpaceProductGroupDto> sourceLocationSpaceProductGroupDtoList = this.DalCache.GetDtos<LocationSpaceProductGroupDto>().ToList();

            var query = from
                //LocationSpaceProductGroup
                lspg in sourceLocationSpaceProductGroupDtoList
                        //LocationSpace
                        join ls in sourceLocationSpaceDtoList
                            on lspg.LocationSpaceId equals ls.Id
                        where 
                            lspg.ProductGroupId == productGroupId
                        

                        select new
                        {
                            lspg.Id,
                            ls.LocationId,
                            lspg.ProductGroupId,
                            lspg.BayCount,
                            lspg.ProductCount,
                            lspg.AverageBayWidth
                        };


            List<LocationSpaceProductGroupInfoDto> dtoList = new List<LocationSpaceProductGroupInfoDto>();
            foreach (var item in query)
            {
                dtoList.Add(new LocationSpaceProductGroupInfoDto()
                {
                    Id = item.Id,
                    LocationId = item.LocationId,
                    ProductGroupId = item.ProductGroupId,
                    BayCount = item.BayCount,
                    ProductCount = item.ProductCount,
                    AverageBayWidth = item.AverageBayWidth
                });
            }

            return dtoList;
        }

        #endregion
    }
}