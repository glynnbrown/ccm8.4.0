﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramLockDal : MockDalBase<PlanogramLockDal>, IPlanogramLockDal
    {
        #region Delete
        /// <summary>
        /// Deletes all locks of the specified type that
        /// are older than the specified lock date
        /// </summary>
        public Boolean DeleteByLockTypeDateLocked(Int32 userId, Byte lockType, DateTime dateLocked)
        {
            return false;
        }
        #endregion
    }
}
