﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="IPlanogramComparisonTemplateInfoDal"/>.
    /// </summary>
    public sealed class PlanogramComparisonTemplateInfoDal : MockDalBase<PlanogramComparisonTemplateInfoDto>, IPlanogramComparisonTemplateInfoDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="ids" />.
        /// </summary>
        /// <param name="ids"><see cref="Object" /> containing the ids of matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="ids"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            return FetchByProperties<PlanogramComparisonTemplateDto>(new[] {"Id"}, new Object[] {ids}, includeDeleted: true);
        }

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        public override IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<PlanogramComparisonTemplateDto>(entityId);
        }

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />, including the deleted ones.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return base.FetchByEntityId<PlanogramComparisonTemplateDto>(entityId, includeDeleted: true);
        }

        #endregion
    }
}