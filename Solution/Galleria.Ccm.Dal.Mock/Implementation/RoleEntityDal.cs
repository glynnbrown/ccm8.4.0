﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class RoleEntityDal : MockDalBase<RoleEntityDto>, IRoleEntityDal
    {

        public RoleEntityDto FetchById(Int32 roleId, Int32 entityId)
        {
            return 
                base.FetchSingleDtoByProperties(
                new String[] { "RoleId", "EntityId" }, 
                new Object[] { roleId, entityId }, 
                /*includeDeleted*/false);
        }

        public IEnumerable<RoleEntityDto> FetchByRoleId(Int32 roleId)
        {
            return base.FetchByProperties(
                new String[]{"RoleId"}, new Object[]{roleId}, 
                /*includeDeleted*/false);
        }
    }
}
