﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25919 : L.Ineson
//  Created
// V8-28444 : M.Shelley
//  Modified the FetchByWorkpackageId method to create a List of WorkflowTaskInfoDto items 
//  on the fly as the DalCache is not populated with these
#endregion
#region Version History: CCM810
// V8-30290 : A.Probyn
//  Fixed typo in FetchByWorkpackageId where Id was being used instead of WorkflowId
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkflowTaskInfoDal : MockDalBase<WorkflowTaskInfoDto>, IWorkflowTaskInfoDal
    {

        public IEnumerable<WorkflowTaskInfoDto> FetchByWorkflowId(Int32 workflowId)
        {
            return this.FetchByProperties<WorkflowTaskDto>(new String[] { "WorkflowId" }, new Object[] { workflowId }, false);
        }

        public IEnumerable<WorkflowTaskInfoDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            WorkpackageDto workpackage = base.DalCache.GetDtos<WorkpackageDto>().FirstOrDefault(w => w.Id == workpackageId);
            if (workpackage != null)
            {
                WorkflowDto workflow = base.DalCache.GetDtos<WorkflowDto>().FirstOrDefault(w => w.Id == workpackage.WorkflowId);
                if (workflow != null)
                {
                    var workTaskDtos = base.DalCache.GetDtos<WorkflowTaskDto>().Where(w => w.WorkflowId == workflow.Id);

                    // Create a list of WorkflowTaskInfoDto from the returned list of WorkflowTaskDto objects
                    List<WorkflowTaskInfoDto> returnDtoList = new List<WorkflowTaskInfoDto>();
                    foreach (var workTaskDto in workTaskDtos)
                    {
                        returnDtoList.Add(new WorkflowTaskInfoDto()
                            {
                                Id = workTaskDto.Id,
                                SequenceId = workTaskDto.SequenceId,
                                TaskType = workTaskDto.TaskType
                            });
                    }

                    return returnDtoList;

                    // Original code commented out as the DalCache contains no WorkflowTaskInfoDto items
                    //return this.FetchByProperties<WorkflowTaskInfoDto>(new String[] { "WorkflowId" }, new Object[] { workflow.Id }, false);
                }
            }

            return new List<WorkflowTaskInfoDto>();
        }
    }
}
