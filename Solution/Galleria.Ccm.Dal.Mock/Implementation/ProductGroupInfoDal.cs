﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25450 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion

#region Version History : CCM 802
// V8-29078 : A.Kuszyk
//  Added FetchByProductGroupCodes
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock dal implementation of IProductGroupInfoDal
    /// </summary>
    public class ProductGroupInfoDal : MockDalBase<ProductGroupInfoDto>, IProductGroupInfoDal
    {
        /// <summary>
        /// Returns infos for all deleted groups in the hierarchy
        /// </summary>
        /// <param name="locationHierarchyId"></param>
        /// <returns></returns>
        public IEnumerable<ProductGroupInfoDto> FetchDeletedByProductHierarchyId(Int32 locationHierarchyId)
        {
            List<ProductGroupInfoDto> dtoList = new List<ProductGroupInfoDto>();

            foreach (ProductGroupInfoDto dto in
                base.FetchByProperties<ProductGroupDto>(
                new String[] { "ProductHierarchyId" },
                new Object[] { locationHierarchyId }, true))
            {
                if (dto.DateDeleted.HasValue)
                {
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }

        public IEnumerable<ProductGroupInfoDto> FetchByProductGroupIds(IEnumerable<Int32> productGroupIds)
        {
            return FetchByProperties<ProductGroupDto>(new String[] { "Id" }, new Object[] { productGroupIds });
        }


        public IEnumerable<ProductGroupInfoDto> FetchByProductGroupCodes(IEnumerable<string> productGroupCodes)
        {
            return FetchByProperties<ProductGroupDto>(new String[] { "Code" }, new Object[] { productGroupCodes }, true);
        }
    }
}
