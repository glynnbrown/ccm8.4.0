﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25748 : K.Pickup
//  Changed to use framework Mock DAL base classes.
// V8-26322 : A.Silva
//  Amended Insert to make sure the destination planogram DTO gets created only if it did not exist already.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Made changes to improve performance of engine
// V8-28507 : D.Pleasance
//  Creation of planogram should have also included creation of the package
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkpackagePlanogramDal : MockDalBase<WorkpackagePlanogramDto>, IWorkpackagePlanogramDal
    {
        #region Fetch
        /// <summary>
        /// Returns all items for the specified workpackage
        /// </summary>
        public IEnumerable<WorkpackagePlanogramDto> FetchByWorkpackageId(int workpackageId)
        {
            return this.FetchByProperties(new String[] { "WorkpackageId" }, new Object[] { workpackageId }, false);
        }

        /// <summary>
        /// Returns the specified item
        /// </summary>
        public WorkpackagePlanogramDto FetchByWorkpackageIdDestinationPlanogramId(Int32 workpackageId, Int32 destinationPlanogramId)
        {
            WorkpackagePlanogramDto dto = this.FetchByProperties(new String[] { "WorkpackageId", "DestinationPlanogramId" }, new Object[] { workpackageId, destinationPlanogramId }, false).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }
        #endregion

        #region Insert

        public override void Insert(WorkpackagePlanogramDto dto)
        {
            //  insert the Destination Planogram DTO is required.
            var destinationPlanogramDto = DalCache
                .GetDtos<PlanogramDto>()
                .FirstOrDefault(o => Convert.ToInt32(o.Id) == dto.DestinationPlanogramId);
            if (destinationPlanogramDto == null)
            {
                var packageDto = new PackageDto() { Name = dto.Name };
                using (var dal = DalContext.GetDal<IPackageDal>())
                {
                    dal.Insert(packageDto);
                }

                destinationPlanogramDto = new PlanogramDto { Name = dto.Name };
                using (var dal = DalContext.GetDal<IPlanogramDal>())
                {
                    dal.Insert(packageDto, destinationPlanogramDto);
                }
                
                dto.DestinationPlanogramId = Convert.ToInt32(destinationPlanogramDto.Id);
            }

            base.Insert(dto);
        }

        #endregion

        #region Delete

        public override void DeleteById(int id)
        {
            DeleteByProperties(new String[] { "Id" }, new Object[] { id });
        }

        #endregion
    }
}
