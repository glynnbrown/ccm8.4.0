﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramImportTemplateMappingDal : MockDalBase<PlanogramImportTemplateMappingDto>, IPlanogramImportTemplateMappingDal
    {
        public IEnumerable<PlanogramImportTemplateMappingDto> FetchByPlanogramImportTemplateId(object planogramImportTemplateId)
        {
            return FetchByProperties(new String[] { "PlanogramImportTemplateId" }, new Object[] { planogramImportTemplateId });
        }
    }
}
