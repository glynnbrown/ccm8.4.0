﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26159: L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class PerformanceSelectionTimelineGroupDal : MockDalBase<PerformanceSelectionTimelineGroupDto>, IPerformanceSelectionTimelineGroupDal
    {
        public IEnumerable<PerformanceSelectionTimelineGroupDto> FetchByPerformanceSelectionId(int performanceSelectionId)
        {
            return base.FetchByProperties(new String[] { "PerformanceSelectionId" }, new Object[] { performanceSelectionId }, false);
        }
    }
}
