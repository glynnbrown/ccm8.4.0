﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27004 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class PlanogramValidationInfoDal : MockDalBase<PlanogramValidationInfoDto>, IPlanogramValidationInfoDal
    {
        #region Fetch

        /// <summary>
        ///     Fetches a collection of <see cref="PlanogramValidationInfoDto"/> instances matching the given <paramref name="planogramIds"/>.
        /// </summary>
        /// <param name="planogramIds">A collection of <see cref="Object"/> values that must be matched by the returned dtos.</param>
        /// <returns>A new collection of <see cref="PlanogramValidationInfoDto"/>.</returns>
        public IEnumerable<PlanogramValidationInfoDto> FetchPlanogramValidationsByPlanogramIds(
            IEnumerable<Int32> planogramIds)
        {
            return PlanogramValidationTemplateDtos(planogramIds).Select(CreateDataTransferObject).ToList();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Creates a <see cref="PlanogramValidationInfoDto"/> from a given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramValidationTemplateDto"/> to create the info dto from.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationInfoDto"/> with the data from teh given <paramref name="dto"/>.</returns>
        private static PlanogramValidationInfoDto CreateDataTransferObject(PlanogramValidationTemplateDto dto)
        {
            return new PlanogramValidationInfoDto
            {
                Id = Convert.ToInt32(dto.Id),
                PlanogramId = Convert.ToInt32(dto.PlanogramId),
                Name = dto.Name
            };
        }

        private IEnumerable<PlanogramValidationTemplateDto> PlanogramValidationTemplateDtos(IEnumerable<int> planogramIds)
        {
            var cacheDtos = base.DalCache.GetDtos<PlanogramValidationTemplateDto>();
            var planogramIdMatchDtos = cacheDtos.Where(dto => planogramIds.Contains(Convert.ToInt32(dto.PlanogramId)));
            return planogramIdMatchDtos;
        }

        #endregion
    }
}
