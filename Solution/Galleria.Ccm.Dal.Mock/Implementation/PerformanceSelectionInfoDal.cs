﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26159: L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class PerformanceSelectionInfoDal : MockDalBase<PerformanceSelectionInfoDto>, IPerformanceSelectionInfoDal
    {
        public new IEnumerable<PerformanceSelectionInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<PerformanceSelectionDto>(entityId);
        }

        public IEnumerable<PerformanceSelectionInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return base.FetchByProperties<PerformanceSelectionDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }

    }
}
