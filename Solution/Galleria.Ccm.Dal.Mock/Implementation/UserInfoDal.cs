﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26222 : L.Luong
//  Created

#endregion
#endregion

using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class UserInfoDal : MockDalBase<UserInfoDto>, IUserInfoDal
    {

        public new IEnumerable<UserInfoDto> FetchAll()
        {
            return FetchAll<UserDto>();
        }
    }
}