﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 810)
// V8-29861 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Mock.Implementation;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramEventLogInfoDal : MockDalBase<PlanogramEventLogInfoDto>, IPlanogramEventLogInfoDal
    {
        #region Data Transfer Object

        private PlanogramEventLogInfoDto CreateDataTransferObject(PlanogramEventLogDto planEventLogDto)
        {
            PlanogramDto planogramDto = base.DalCache.GetDtos<PlanogramDto>().First(p => Object.Equals(p.Id, planEventLogDto.PlanogramId));

            //Get index of plan event log
            IEnumerable<PlanogramEventLogDto> siblingEventLogs = base.DalCache.GetDtos<PlanogramEventLogDto>().Where(p => p.PlanogramId == planEventLogDto.PlanogramId);
            Int32 index = siblingEventLogs.OrderBy(p => p.DateTime).ToList().IndexOf(planEventLogDto);

            return new PlanogramEventLogInfoDto()
            {
                Id = Convert.ToInt32(planEventLogDto.Id),
                PlanogramId = Convert.ToInt32(planEventLogDto.PlanogramId),
                PlanogramName = planogramDto.Name,
                AffectedId = planEventLogDto.AffectedId,
                AffectedType = planEventLogDto.AffectedType,
                Content = planEventLogDto.Content,
                DateTime = planEventLogDto.DateTime,
                Description = planEventLogDto.Description,
                EntryType = planEventLogDto.EntryType,
                EventId = planEventLogDto.EventId,
                EventType = planEventLogDto.EventType,
                ExecutionOrder = index + 1,
                Score = planEventLogDto.Score,
                TaskSource = planEventLogDto.TaskSource,
                WorkpackageSource = planEventLogDto.WorkpackageSource                
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns all plangoram event logs for the specified workpackage id
        /// </summary>
        public IEnumerable<PlanogramEventLogInfoDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            List<PlanogramEventLogInfoDto> dtoList = new List<PlanogramEventLogInfoDto>();

            List<Int32> workpackagePlanIds = base.DalCache.GetDtos<WorkpackagePlanogramDto>().Where(p => p.WorkpackageId == workpackageId).Select(p => p.DestinationPlanogramId).ToList();

            List<PlanogramEventLogDto> workpackagePlanEventLogs =
                base.DalCache.GetDtos<PlanogramEventLogDto>()
                .Where(d => workpackagePlanIds.Contains(Convert.ToInt32(d.PlanogramId))).ToList();

            foreach (PlanogramEventLogDto planEventLogDto in base.DalCache.GetDtos<PlanogramEventLogDto>())
            {
                dtoList.Add(CreateDataTransferObject(planEventLogDto));
            }

            return dtoList;
        }

        public IEnumerable<PlanogramInfoDto> FetchByMetadataCalculationRequired()
        {
            return this.DalCache.GetDtos<PlanogramInfoDto>().Where(p => p.DateMetadataCalculated == null);
        }

        public IEnumerable<PlanogramInfoDto> FetchByValidationCalculationRequired()
        {
            return this.DalCache.GetDtos<PlanogramInfoDto>().Where(p => p.DateValidationDataCalculated == null);
        }

        #endregion
    }
}
