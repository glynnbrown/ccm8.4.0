﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkpackagePlanogramParameterValueDal : MockDalBase<WorkpackagePlanogramParameterValueDto>, IWorkpackagePlanogramParameterValueDal
    {
        public IEnumerable<WorkpackagePlanogramParameterValueDto> FetchByWorkpackagePlanogramParameterId(int workpackagePlanogramParameterId)
        {
            return this.FetchByProperties(new String[] { "WorkpackagePlanogramParameterId" }, new Object[] { workpackagePlanogramParameterId }, false);
        }
    }
}
