﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30738 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock implementation of IPrintTemplateComponentDetailDal
    /// </summary>
    public sealed class PrintTemplateComponentDetailDal : MockDalBase<PrintTemplateComponentDetailDto>, IPrintTemplateComponentDetailDal
    {

        public IEnumerable<PrintTemplateComponentDetailDto> FetchByPrintTemplateComponentId(Int32 printTemplateComponentId)
        {
            return FetchByProperties(new String[] { "PrintTemplateComponentId" }, new Object[] { printTemplateComponentId });
        }


        public void DeleteByPrintTemplateComponentId(int printTemplateComponentId)
        {
            DeleteByProperties(new String[] { "PrintTemplateComponentId" }, new Object[] { printTemplateComponentId });
        }
    }
}
