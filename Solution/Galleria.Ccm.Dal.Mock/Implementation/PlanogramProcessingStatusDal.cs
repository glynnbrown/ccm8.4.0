﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: v8.00

// V8-26322 : A.Silva
//      Created.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class PlanogramProcessingStatusDal
        : MockDalBase<PlanogramProcessingStatusDto>, IPlanogramProcessingStatusDal
    {
        public PlanogramProcessingStatusDto FetchByPlanogramId(Int32 planogramId)
        {
            var matchingDto =
                DalCache.GetDtos<PlanogramProcessingStatusDto>().FirstOrDefault(dto => dto.PlanogramId == planogramId);
            if (matchingDto != null) return matchingDto;

            matchingDto = new PlanogramProcessingStatusDto {PlanogramId = planogramId};
            DalCache.InsertDto(matchingDto);

            return matchingDto;
        }

        public void AutomationStatusIncrement(int planogramId, string statusDescription, DateTime dateLastUpdated)
        {
            throw new NotImplementedException();
        }

        public void MetaDataStatusIncrement(int planogramId, string statusDescription, DateTime dateLastUpdated)
        {
            throw new NotImplementedException();
        }

        public void ValidationStatusIncrement(int planogramId, string statusDescription, DateTime dateLastUpdated)
        {
            throw new NotImplementedException();
        }
    }
}