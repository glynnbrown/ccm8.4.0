﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock dal implementation of IFixturePackageInfo
    /// </summary>
    public class FixturePackageInfoDal : MockDalBase<FixturePackageInfoDto>, IFixturePackageInfoDal
    {
        #region Data Transfer Object

        private FixturePackageInfoDto GetDto(FixturePackageDto packageDto)
        {
            return new FixturePackageInfoDto()
            {
                Id = packageDto.Id,
                Name = packageDto.Name,
                Height = packageDto.Height,
                Width = packageDto.Width,
                FolderId = null,
                Depth = packageDto.Depth,
                AssemblyCount = packageDto.AssemblyCount,
                ComponentCount = packageDto.ComponentCount,
                Description = packageDto.Description,
                FixtureCount = packageDto.FixtureCount,
                ThumbnailImageData = packageDto.ThumbnailImageData
            };
        }

        #endregion


        public IEnumerable<FixturePackageInfoDto> FetchByIds(IEnumerable<object> ids)
        {
            List<FixturePackageInfoDto> dtoList = new List<FixturePackageInfoDto>();

            foreach (FixturePackageDto dto in base.DalCache.GetDtos<FixturePackageDto>())
            {
                if (ids.Contains(dto.Id))
                {
                    dtoList.Add(GetDto(dto));
                }
            }

            return dtoList;
        }


        public void SetFolderId(object fixturePackageId, object folderId)
        {
            //nothing to do.
        }
    }
}
