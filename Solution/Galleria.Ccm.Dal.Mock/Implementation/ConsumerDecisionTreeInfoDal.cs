﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created.
// V8-26520 : J.Pickup
//		FetchByProductGroupId, FetchByIds
// V8-25556 : J.Pickup
//		FetchAllIncludingDeleted()
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ConsumerDecisionTreeInfoDal : MockDalBase<ConsumerDecisionTreeInfoDto>, IConsumerDecisionTreeInfoDal
    {
        public new IEnumerable<ConsumerDecisionTreeInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByEntityId<ConsumerDecisionTreeDto>(entityId);
        }

        public IEnumerable<ConsumerDecisionTreeInfoDto> FetchByProductGroupId(int productGroupId)
        {
            return base.FetchByProperties<ConsumerDecisionTreeDto>(new String[] { "ProductGroupId" }, new Object[] { productGroupId }, false);
        }

        public new ConsumerDecisionTreeInfoDto FetchById(Int32 id)
        {
            return base.FetchSingleDtoByProperties<ConsumerDecisionTreeDto>(new String[] { "Id" }, new Object[] { id });
        }

        public new IEnumerable<ConsumerDecisionTreeInfoDto> FetchAllIncludingDeleted()
        {
            return base.FetchAll<ConsumerDecisionTreeDto>(includeDeleted:true);
        }
    }
}
