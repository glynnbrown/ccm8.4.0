﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created.
// V8-25556 : J.Pickup
//		Introduced FetchAllIncludingDeleted()
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ConsumerDecisionTreeNodeDal : MockDalBase<ConsumerDecisionTreeNodeDto>, IConsumerDecisionTreeNodeDal
    {
        public new ConsumerDecisionTreeNodeDto FetchById(Int32 id)
        {
            return base.FetchSingleDtoByProperties(new String[] {"Id"}, new Object[] {id});
        }

        public IEnumerable<ConsumerDecisionTreeNodeDto> FetchByConsumerDecisionTreeId(int consumerDecisionTreeId)
        {
            return base.FetchByProperties(new String[] { "ConsumerDecisionTreeId" }, new Object[] { consumerDecisionTreeId });
        }

        public IEnumerable<ConsumerDecisionTreeNodeDto> FetchAllIncludingDeleted()
        {
            return base.FetchAll<ConsumerDecisionTreeNodeDto>(includeDeleted: true);
        }

        public void Insert(ConsumerDecisionTreeNodeDto dto)
        {
            base.Insert(dto,true);
        }
    }
}
