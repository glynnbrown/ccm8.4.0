﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Reporting.Dal.DataTransferObjects;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock query dal
    /// </summary>
    /// <remarks>
    /// Note that this mock dal actually executes its queries
    /// against a physical database.
    /// </remarks>
    public class QueryDal : MockDalBase<ResultSetDto>, IQueryDal
    {
        #region Constants
        private const String _connectionString = @"Server={0};Database={1};Trusted_Connection=True;";
        private const String _server = @"PUCK\DEVSQL2008";
        private const String _database = @"Morrisons_GFS215_b7";
        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Builds a data transfer object from a data reader
        /// </summary>
        /// <param name="dr">The data reader to build from</param>
        /// <returns>A new data transfer object</returns>
        private static ResultSetDto GetDataTransferObject(SqlDataReader dr)
        {
            // initialize the result set
            ResultSetDto resultSet = new ResultSetDto();

            // add the schema information
            DataTable schemaTable = dr.GetSchemaTable();
            foreach (DataRow schemaRow in schemaTable.Rows)
            {
                resultSet.Columns.Add(new ResultSetColumnDto()
                {
                    Name = schemaRow["ColumnName"].ToString(),
                    DataType = Type.GetType(schemaRow["DataType"].ToString())
                });
            }

            // add the value information
            if (dr.HasRows)
            {
                Int32 fieldCount = dr.FieldCount;
                while (dr.Read())
                {
                    ResultSetRowDto row = new ResultSetRowDto(fieldCount);
                    for (Int32 i = 0; i < fieldCount; i++)
                    {
                        row.Values.Add(dr.GetValue(i));
                    }
                    resultSet.Rows.Add(row);
                }
            }

            // and return the result set
            return resultSet;
        }
        #endregion

        #region Execute
        /// <summary>
        /// Executes a query and returns the results
        /// </summary>
        public ResultSetDto Execute(String query)
        {
            ResultSetDto results = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(String.Format(_connectionString, _server, _database)))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            results = GetDataTransferObject(dr);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return results;
        }
        #endregion

        #region Evaluate
        /// <summary>
        /// Performs the evaluation of a query
        /// </summary>
        /// <param name="query">The query to evaluate</param>
        /// <returns>The results of the evaluation</returns>
        public ResultSetDto Evaluate(String query)
        {
            ResultSetDto results = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(String.Format(_connectionString, _server, _database)))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("SET SHOWPLAN_XML ON", connection))
                    {
                        command.ExecuteNonQuery();
                    }
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            results = GetDataTransferObject(dr);
                        }
                    }
                    using (SqlCommand command = new SqlCommand("SET SHOWPLAN_XML OFF", connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return results;
        }
        #endregion
    }
}
