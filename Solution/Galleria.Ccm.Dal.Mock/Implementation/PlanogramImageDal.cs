﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25591 : A.Kuszyk
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramImageDal : MockDalBase<PlanogramImageDto>, IPlanogramImageDal
    {
        public IEnumerable<PlanogramImageDto> FetchByPlanogramId(object planogramId)
        {
            return FetchByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId });
        }
    }
}
