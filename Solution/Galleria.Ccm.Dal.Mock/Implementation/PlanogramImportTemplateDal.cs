﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramImportTemplateDal : MockDalBase<PlanogramImportTemplateDto>, IPlanogramImportTemplateDal
    {
        public PlanogramImportTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name }, true);
        }

        public void LockById(object id)
        {
            //do nothing
        }

        public void UnlockById(object id)
        {
            //do nothing
        }
    }
}
