﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25450 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductLevelDal : MockDalBase<ProductLevelDto>, IProductLevelDal
    {

        #region IProductLevelDal Members

        public IEnumerable<ProductLevelDto> FetchByProductHierarchyId(Int32 hierarchyId)
        {
            return FetchByProperties(new String[] { "ProductHierarchyId" }, new Object[] { hierarchyId });
        }

        #endregion
    }
}
