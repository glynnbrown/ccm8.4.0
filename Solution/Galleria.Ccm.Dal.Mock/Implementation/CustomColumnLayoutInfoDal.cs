﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class CustomColumnLayoutInfoDal : MockDalBase<CustomColumnLayoutInfoDto>, ICustomColumnLayoutInfoDal
    {
        public IEnumerable<CustomColumnLayoutInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            return FetchByProperties<CustomColumnLayoutDto>(new String[] { "Id" }, new Object[] { ids });
        }

        public IEnumerable<CustomColumnLayoutInfoDto> FetchByType(IEnumerable<Object> ids, Byte type)
        {
            return FetchByProperties<CustomColumnLayoutDto>(new String[] { "Id", "Type" }, new Object[] { ids, type });
        }
    }
}