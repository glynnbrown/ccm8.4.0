﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30738 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock implementation of IPrintTemplateComponentDal
    /// </summary>
    public sealed class PrintTemplateComponentDal : MockDalBase<PrintTemplateComponentDto>, IPrintTemplateComponentDal
    {
        public IEnumerable<PrintTemplateComponentDto> FetchByPrintTemplateSectionId(int printTemplateSectionId)
        {
            return FetchByProperties(new String[] { "PrintTemplateSectionId" }, new Object[] { printTemplateSectionId });
        }
    }
}
