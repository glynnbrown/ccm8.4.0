﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationProductIllegalDal : MockDalBase<LocationProductIllegalDto>, ILocationProductIllegalDal
    {
        #region Fetch

        public LocationProductIllegalDto FetchById(Int16 locationId, Int32 productId)
        {
            return base.FetchSingleDtoByProperties(new String[] { "LocationId", "ProductId" }, new Object[] { locationId, productId });
        }

        public List<LocationProductIllegalDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> locationProductIdCombinations)
        {
            List<LocationProductIllegalDto> dtoList = new List<LocationProductIllegalDto>();

            foreach (LocationProductIllegalDto dto in this.DalCache.GetDtos<LocationProductIllegalDto>())
            {
                Tuple<Int16, Int32> combination = locationProductIdCombinations
                    .FirstOrDefault(c => c.Item1 == dto.LocationId && c.Item2 == dto.ProductId);

                if (combination != null)
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Delete

        public void DeleteById(Int16 locationId, Int32 productId)
        {
            base.DeleteByProperties(new String[] { "LocationId", "ProductId" }, new Object[] { locationId, productId });
        }

        #endregion
    }
}
