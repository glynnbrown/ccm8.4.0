﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)

// V8-27059 : J.Pickup
//      Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentProductDal : MockDalBase<AssortmentProductDto>, IAssortmentProductDal
    {
        public IEnumerable<AssortmentProductDto> FetchByAssortmentId(int assortmentId)
        {
            return base.FetchByProperties<AssortmentProductDto>(new String[] { "AssortmentId" }, new Object[] { assortmentId }, false);
        }

        public IEnumerable<AssortmentProductDto> FetchByAssortmentProductId(int assortmentProductId)
        {
            return base.FetchByProperties<AssortmentProductDto>(new String[] { "Id" }, new Object[] { assortmentProductId }, false);
        }
    }
}