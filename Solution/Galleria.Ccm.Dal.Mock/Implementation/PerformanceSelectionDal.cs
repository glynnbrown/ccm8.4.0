﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26159: L.Ineson
//  Created
// V8-27763 : N.Foster
//  Added FetchByName
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class PerformanceSelectionDal : MockDalBase<PerformanceSelectionDto>, IPerformanceSelectionDal
    {
        #region Fetch
        /// <summary>
        /// Returns the specified performance selection dto
        /// </summary>
        public PerformanceSelectionDto FetchByEntityIdName(Int32 entityId, String name)
        {
            var dto = base.FetchByProperties<PerformanceSelectionDto>(new String[] { "EntityId", "Name" }, new Object[] {entityId, name }, true).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion
    }
}
