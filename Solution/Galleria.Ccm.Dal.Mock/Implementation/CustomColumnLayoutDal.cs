﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

//V8-26671 : A.Silva ~ Created

#endregion

#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class CustomColumnLayoutDal : MockDalBase<CustomColumnLayoutDto>, ICustomColumnLayoutDal
    {
        private const bool AllowReinsert = true;

        public void LockById(object id)
        {
            // do nothing;
        }

        public void UnlockById(object id)
        {
            // do nothing;
        }

        public override void Insert(CustomColumnLayoutDto dto)
        {
            Insert(dto, AllowReinsert);
        }
    }
}
