﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class BlockingInfoDal : MockDalBase<BlockingInfoDto>, IBlockingInfoDal
    {
        public new IEnumerable<BlockingInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<BlockingDto>(entityId);
        }
    }
}
