﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25395 : A.Probyn
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductLibraryDal : MockDalBase<ProductLibraryDto>, IProductLibraryDal
    {
        #region IProductLibraryDal Members

        public void LockById(object id)
        {
            //do nothing
        }

        public void UnlockById(Object id)
        {
            //do nothing.
        }

        #endregion



        
    }
}
