﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created.
// CCM-25242 : N.Haywood
//  Added fetch methods for imports
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIds
// V8-25556 : D.Pleasance
//  Added FetchByEntityIdProductGtins
#endregion
#region Version History: (CCM 8.3.0)
// V8-32356 : N.Haywood
//  Added FetchByEntityIdMultipleSearchText
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductDal : MockDalBase<ProductDto>, IProductDal
    {
        public new ProductDto FetchById(Int32 id)
        {
            return base.FetchById(id);
        }

        public void Upsert(IEnumerable<ProductDto> dtoList, ProductIsSetDto isSetDto)
        {
            base.Upsert<ProductIsSetDto>(dtoList, isSetDto);
        }

        public IEnumerable<ProductDto> FetchByEntityId(Int32 id)
        {
            return base.FetchByEntityId<ProductDto>(id);
        }

        public void DeleteByEntityId(Int32 id)
        {
            base.DeleteByProperties(new String[] { "EntityId" }, new Object[] { id });
        }

        public IEnumerable<ProductDto> FetchByProductGroupId(Int32 productGroupId)
        {
            return FetchByProperties(new String[] { "ProductGroupId" }, new Object[] { productGroupId });
        }

        public IEnumerable<ProductDto> FetchByProductIds(IEnumerable<Int32> productIds)
        {
            return FetchByProperties(new String[] { "Id" }, new Object[] { productIds });
        }

        public IEnumerable<ProductDto> FetchByProductUniverseId(Int32 productUniverseId)
        {
            List<ProductDto> dtoList = new List<ProductDto>();

            //Get all product dtos assigned to this group
            List<Int32> productIdList = base.DalCache.GetDtos<ProductUniverseProductDto>().Where(p => p.ProductUniverseId == productUniverseId).Select(p => p.Id).ToList();

            foreach (ProductDto dto in base.DalCache.GetDtos<ProductDto>())
            {
                if (productIdList.Contains(dto.Id))
                {
                    if (dto.DateDeleted == null)
                    {
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }


        public IEnumerable<ProductDto> FetchByEntityIdProductGtins(Int32 entityId, IEnumerable<String> productGtins)
        {
            return base.FetchByProperties<ProductDto>(new String[] { "EntityId", "Gtin" }, new Object[] { entityId, productGtins });
        }


        public IEnumerable<ProductDto> FetchByMerchandisingGroupId(Int32 merchGroupId)
        {
            List<ProductDto> dtoList = new List<ProductDto>();

            IEnumerable<ProductUniverseDto> universeDtos = base.DalCache.GetDtos<ProductUniverseDto>().Where(p => p.ProductGroupId == merchGroupId);

            foreach (ProductUniverseDto universe in universeDtos)
            {
                foreach (ProductDto dto in FetchByProductUniverseId(universe.Id))
                {
                    if (!dtoList.Contains(dto))
                    {
                        dtoList.Add(Clone(dto));
                    }
                }
            }

            return dtoList;
        }

        public IEnumerable<ProductDto> FetchByEntityIdSearchText(Int32 entityId, String searchText)
        {
            List<ProductDto> returnList = new List<ProductDto>();
            if (searchText == null)
            {
                return returnList;
            }

            //remove special characters
            String processedCriteria = String.Empty;
            foreach (Char c in searchText)
            {
                if (Char.IsLetterOrDigit(c))
                {
                    processedCriteria = String.Format(processedCriteria + c);
                }
            }

            //split into parts
            String[] criteriaParts = searchText.ToLowerInvariant().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (ProductDto dto in DalCache.GetDtos<ProductDto>())
            {
                if (dto.EntityId == entityId)
                {
                    String productValue = String.Format("{0} {1}", dto.Gtin, dto.Name);

                    if (StatisfiesCriteria(productValue, criteriaParts))
                    {
                        returnList.Add(Clone(dto));
                    }
                }
            }

            return returnList;
        }

        public IEnumerable<ProductDto> FetchByEntityIdMultipleSearchText(Int32 entityId, IEnumerable<String> multipleSearchText)
        {
            List<ProductDto> returnList = new List<ProductDto>();
            if (multipleSearchText == null)
            {
                return returnList;
            }

            //remove special characters
            foreach (String s in multipleSearchText)
            {
                String processedCriteria = String.Empty;
                foreach (Char c in s)
                {
                    if (Char.IsLetterOrDigit(c))
                    {
                        processedCriteria = String.Format(processedCriteria + c);
                    }
                }

                //split into parts
                String[] criteriaParts = s.ToLowerInvariant().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                foreach (ProductDto dto in DalCache.GetDtos<ProductDto>())
                {
                    if (dto.EntityId == entityId)
                    {
                        String productValue = String.Format("{0} {1}", dto.Gtin, dto.Name);

                        if (StatisfiesCriteria(productValue, criteriaParts))
                        {
                            returnList.Add(Clone(dto));
                        }
                    }
                }
            }

            return returnList;
        }


        #region Private Methods

        private Boolean StatisfiesCriteria(String value, String[] containsParts)
        {
            String loweredValue = value.ToLowerInvariant();

            List<String> searchCriteria = new List<String>();
            List<String> searchOperators = new List<String>();

            for (Int32 i = 0; i < containsParts.Length; i++)
            {
                String part = containsParts[i];

                //part is a criteria
                if (part != "or" &&
                    part != "and")
                {
                    searchCriteria.Add(part);
                }
                else
                {
                    if (containsParts.Length > i + 1)
                    {
                        String nextPart = containsParts[i + 1];
                        if (nextPart != "or" &&
                            nextPart != "and")
                        {
                            if (!String.IsNullOrEmpty(nextPart) &&
                                searchCriteria.Count > searchOperators.Count)
                            {
                                searchOperators.Add(part);
                            }
                        }
                        else
                        {
                            //If the next part is OR or AND, but is the last part use the current as the operator
                            if (!(containsParts.Length == i + 2))
                            {
                                searchCriteria.Add(part);
                            }
                            else
                            {
                                searchOperators.Add(part);
                            }
                        }
                    }
                    else
                    {
                        //This is the last part of the search string
                        searchCriteria.Add(part);
                    }
                }
            }

            //Chech the conditions
            if (searchOperators.Contains("or"))
            {
                foreach (String criteria in searchCriteria)
                {
                    if (loweredValue.Contains(criteria))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                foreach (String criteria in searchCriteria)
                {
                    if (!loweredValue.Contains(criteria))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        #endregion
    }
}