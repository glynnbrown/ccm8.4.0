﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class BlockingGroupDal : MockDalBase<BlockingGroupDto>, IBlockingGroupDal
    {
        public IEnumerable<BlockingGroupDto> FetchByBlockingId(int blockingId)
        {
            return base.FetchByProperties(new String[] { "BlockingId" }, new Object[] { blockingId });
        }
    }
}
