﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class InventoryProfileInfoDal : MockDalBase<InventoryProfileInfoDto>, IInventoryProfileInfoDal
    {
        public new IEnumerable<InventoryProfileInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByEntityId<InventoryProfileDto>(entityId);
        }
    }
}
