﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramExportTemplatePerformanceMetricDal : MockDalBase<PlanogramExportTemplatePerformanceMetricDto>, IPlanogramExportTemplatePerformanceMetricDal
    {
        public IEnumerable<PlanogramExportTemplatePerformanceMetricDto> FetchByPlanogramExportTemplateId(object planogramExportTemplateId)
        {
            return FetchByProperties(new String[] { "PlanogramExportTemplateId" }, new Object[] { planogramExportTemplateId });
        }
    }
}
