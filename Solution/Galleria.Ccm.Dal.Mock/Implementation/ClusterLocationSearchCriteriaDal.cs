﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ClusterLocationSearchCriteriaDal : MockDalBase<ClusterLocationSearchCriteriaDto>, IClusterLocationSearchCriteriaDal
    {

        public IEnumerable<String> ChildDtoTypes
        {
            get
            {
                return new List<String>();
            }
        }

        #region IClusterLocationSearchCriteriaDal Members

        IEnumerable<ClusterLocationSearchCriteriaDto> IClusterLocationSearchCriteriaDal.FetchByEntityId(int entityId)
        {
            IClusterSchemeDal clusterSchemeDal = _dalContext.GetDal<IClusterSchemeDal>();
            ILocationDal locationDal = _dalContext.GetDal<ILocationDal>();
            List<ClusterDto> clusters = new List<ClusterDto>(DalCache.GetDtos<ClusterDto>());
            List<LocationDto> locations = new List<LocationDto>(locationDal.FetchByEntityId(entityId));
            foreach (ClusterLocationDto dto in DalCache.GetDtos<ClusterLocationDto>())
            {
                LocationDto location = locations.FirstOrDefault(l => l.Id == dto.LocationId);
                if (location != null)
                {
                    ClusterDto cluster = clusters.First(c => c.Id == dto.ClusterId);
                    ClusterLocationSearchCriteriaDto returnValue = new ClusterLocationSearchCriteriaDto()
                    {
                        ClusterSchemeName = clusterSchemeDal.FetchById(cluster.ClusterSchemeId).Name,
                        ClusterName = cluster.Name,
                        LocationCode = location.Code
                    };
                    yield return returnValue;
                }
            }
        }

        #endregion
    }
}
