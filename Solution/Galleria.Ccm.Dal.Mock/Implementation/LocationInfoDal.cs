﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628: A.Kuszyk
//  Created
// V8-25748 : K.Pickup
//  Changed to use framework Mock DAL base classes.
// V8-25556 : J.Pickup
//  Added FetchAllIncludingDeleted()    
#endregion
#region Version History: (CCM 8.1.0)
// V8-29667 : N.Haywood
//  Removed FetchAllIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationInfoDal : MockDalBase<LocationInfoDto>, ILocationInfoDal
    {
        public new IEnumerable<LocationInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByEntityId<LocationDto>(entityId);
        }

        public IEnumerable<LocationInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByProperties<LocationDto>(new String[] { "EntityId"}, new Object[] { entityId }, true);
        }

        public IEnumerable<LocationInfoDto> FetchByWorkpackageIdIncludingDeleted(int workpackageId)
        {
            var locationIds = DalCache.GetDtos<WorkpackagePlanogramDto>()
                .Where(workpackagePlanogramDto => workpackagePlanogramDto.Id == workpackageId)
                .SelectMany(workpackagePlanogramDto =>
                    DalCache.GetDtos<LocationPlanAssignmentDto>()
                    .Where(locationPlanAssignmentDto => locationPlanAssignmentDto.PlanogramId == workpackagePlanogramDto.DestinationPlanogramId))
                    .Select(locationPlanAssignmentDto => locationPlanAssignmentDto.LocationId);
            return base.FetchByProperties<LocationDto>(new String[] { "Id" }, new object[] {locationIds.ToArray()}, true);
        }

        public IEnumerable<LocationInfoDto> FetchByWorkpackagePlanogramIdIncludingDeleted(int planogramId)
        {
            return base.FetchByProperties<LocationDto>(new String[] { "PlanogramId" }, new Object[] { planogramId }, true);
        }

        public IEnumerable<LocationInfoDto> FetchByEntityIdLocationCodes(int entityId, IEnumerable<string> locationCodes)
        {
            return base.FetchByProperties<LocationDto>(new String[] { "EntityId", "Code" }, new Object[] { entityId, locationCodes });
        }

        //public IEnumerable<LocationInfoDto> FetchAllIncludingDeleted()
        //{
        //    return base.FetchAll<LocationDto>(true);
        //}
    }
}
