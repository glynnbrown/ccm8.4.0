﻿using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class WorkpackagePlanogramProcessingStatusDal
        : MockDalBase<WorkpackagePlanogramProcessingStatusDto>, IWorkpackagePlanogramProcessingStatusDal
    {
        public WorkpackagePlanogramProcessingStatusDto FetchByPlanogramIdWorkpackageId(Int32 planogramId, Int32 workpackageId)
        {
            var matchingDto =
                DalCache.GetDtos<WorkpackagePlanogramProcessingStatusDto>().FirstOrDefault(dto => dto.PlanogramId == planogramId && dto.WorkpackageId == workpackageId);
            if (matchingDto != null) return matchingDto;

            matchingDto = new WorkpackagePlanogramProcessingStatusDto { PlanogramId = planogramId, WorkpackageId = workpackageId };
            DalCache.InsertDto(matchingDto);

            return matchingDto;
        }

        public void AutomationStatusIncrement(int planogramId, Int32 workpackageId, string statusDescription, DateTime dateLastUpdated)
        {
            var dto = FetchByPlanogramIdWorkpackageId(planogramId, workpackageId);
            dto.AutomationStatus += 1;
            dto.AutomationStatusDescription = statusDescription;
            dto.AutomationDateLastUpdated = dateLastUpdated;
        }

        public IEnumerable<WorkpackagePlanogramProcessingStatusDto> FetchByPlanogramId(int planogramId)
        {
            return
                DalCache.GetDtos<WorkpackagePlanogramProcessingStatusDto>().Where(dto => dto.PlanogramId == planogramId).ToList();
            
        }
    }
}
