﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed Unused stored procedures
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationProductAttributeDal : MockDalBase<LocationProductAttributeDto>, ILocationProductAttributeDal
    {
        #region Helpers

        private void CheckForeignKeys(LocationProductAttributeDto dto)
        {
            //check fks are valid
            EntityDto entityDto = this.DalCache.GetDtos<EntityDto>().FirstOrDefault(e => e.Id == dto.EntityId);
            if (entityDto == null)
            {
                throw new ArgumentException("LocationToProductAttribute insert failed on Entity_Id fk");
            }

            LocationDto locationDto = this.DalCache.GetDtos<LocationDto>().FirstOrDefault(l => l.Id == dto.LocationId);
            if (locationDto == null)
            {
                throw new ArgumentException("LocationToProductAttribute insert failed on LocationId fk");
            }

            ProductDto productDto = this.DalCache.GetDtos<ProductDto>().FirstOrDefault(p => p.Id == dto.ProductId);
            if (productDto == null)
            {
                throw new ArgumentException("LocationToProductAttribute insert failed on ProductId fk");
            }
        }

        #endregion

        #region Fetch

        public LocationProductAttributeDto FetchByLocationIdProductId(Int16 locationId, Int32 productId)
        {
            return base.FetchSingleDtoByProperties(new String[] { "LocationId", "ProductId" }, new Object[] { locationId, productId }, true);
        }

        public List<LocationProductAttributeDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> combinationsList)
        {
            List<LocationProductAttributeDto> dtoList = new List<LocationProductAttributeDto>();

            foreach (LocationProductAttributeDto dto in this.DalCache.GetDtos<LocationProductAttributeDto>())
            {
                Tuple<Int16, Int32> combination = combinationsList.FirstOrDefault(
                    c => c.Item1 == dto.LocationId && c.Item2 == dto.ProductId);

                if (combination != null)
                {
                    dtoList.Add(Clone(dto));
                }
            }

            return dtoList;
        }

        public List<LocationProductAttributeDto> FetchByEntityIdLocationIdProductIds(Int32 entityId, Int16 locationId, IEnumerable<Int32> productIds)
        {
            List<LocationProductAttributeDto> dtoList = new List<LocationProductAttributeDto>();

            foreach (LocationProductAttributeDto dto in this.DalCache.GetDtos<LocationProductAttributeDto>())
            {
                dtoList.Add(Clone(dto));
            }

            return dtoList;
        }

        #endregion

       

        #region Delete

        public void DeleteById(Int16 locationId, Int32 productId)
        {
            base.DeleteByProperties(new String[] { "LocationId", "ProductId" }, new Object[] { locationId, productId });
        }

        #endregion

        #region Upsert
        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<LocationProductAttributeDto> locationProductAttributeDtoList, LocationProductAttributeIsSetDto isSetDto)
        {
            base.Upsert(locationProductAttributeDtoList, isSetDto);
        }
        #endregion

    }
}
