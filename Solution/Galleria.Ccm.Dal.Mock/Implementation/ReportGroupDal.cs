﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ReportGroupDal : MockDalBase<ReportGroupDto>, IReportGroupDal
    {
        public ReportGroupDto FetchById(Int32 id)
        {
            return base.FetchById((Int32)id);
        }

        public IEnumerable<ReportGroupDto> FetchByReportId(Object reportId)
        {
            return base.FetchByProperties(
                new String[] { "ReportId" },
                new Object[] { reportId },
                false
                );
        }

        public void Update(ReportGroupDto dto)
        {
            ReportGroupDto existingDto = this.DalCache.GetDtos<ReportGroupDto>().First(p => p.Id == dto.Id);

            this.DalCache.RemoveDto<ReportGroupDto>(existingDto);

            this.DalCache.InsertDto<ReportGroupDto>(Clone(dto));
        }

        public void DeleteById(Int32 id)
        {
            base.DeleteById((Int32)id);
        }
    }
}
