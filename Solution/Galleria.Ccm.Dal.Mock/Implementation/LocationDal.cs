﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628: A.Kuszyk
//  Created
// CCM-25445 : L.Ineson
//  Added FetchByLocationGroupId
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// V8-25556 : D.Pleasance
//  Added FetchByEntityIdIncludingDeleted, amended Upsert, added parameter updateDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationDal : MockDalBase<LocationDto>, ILocationDal
    {

        public void Upsert(IEnumerable<LocationDto> dtoList, LocationIsSetDto isSetDto)
        {
            base.Upsert<LocationIsSetDto>(dtoList, isSetDto);
        }


        public IEnumerable<LocationDto> FetchByLocationGroupId(int locationGroupId)
        {
            return base.FetchByProperties(new String[] { "LocationGroupId" }, new Object[] { locationGroupId });
        }


        public IEnumerable<LocationDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByProperties(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }

        public void Upsert(IEnumerable<LocationDto> dtoList, LocationIsSetDto isSetDto, bool updateDeleted)
        {
            base.Upsert(dtoList, isSetDto, updateDeleted);
        }
    }
}
