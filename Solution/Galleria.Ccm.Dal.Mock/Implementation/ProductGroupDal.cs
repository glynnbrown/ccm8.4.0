﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25450 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductGroupDal : MockDalBase<ProductGroupDto>, IProductGroupDal
    {

        #region IProductGroupDal Members

        public IEnumerable<ProductGroupDto> FetchByProductHierarchyId(Int32 hierarchyId)
        {
            return FetchByProperties(new String[] { "ProductHierarchyId" }, new Object[] { hierarchyId });
        }

        public IEnumerable<ProductGroupDto> FetchByProductHierarchyIdIncludingDeleted(Int32 productHierarchyId)
        {
            return FetchByProperties(new String[] { "ProductHierarchyId" }, new Object[] { productHierarchyId }, /*includeDeleted*/true);
        }

        public void Upsert(IEnumerable<ProductGroupDto> dtoList, ProductGroupIsSetDto isSetDto)
        {
            base.Upsert(dtoList, isSetDto);
        }

        #endregion

    }
}
