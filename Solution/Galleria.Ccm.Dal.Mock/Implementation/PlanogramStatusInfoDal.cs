﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25608 : N.Foster
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramStatusInfoDal : MockDalBase<PlanogramStatusInfoDto>, IPlanogramStatusInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto
        /// </summary>
        private static PlanogramStatusInfoDto GetDataTransferObject(PlanogramStatusDto dto)
        {
            return new PlanogramStatusInfoDto()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all available planogram status'
        /// </summary>
        public IEnumerable<PlanogramStatusInfoDto> FetchByEntityId(Int32 entityId)
        {
            return this.FetchByProperties<PlanogramStatusDto>(new String[] { "EntityId" }, new Object[] { entityId }, false);
        }

        /// <summary>
        /// Returns all available planogram status'
        /// </summary>
        public IEnumerable<PlanogramStatusInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return this.FetchByProperties<PlanogramStatusDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }
        #endregion
    }
}
