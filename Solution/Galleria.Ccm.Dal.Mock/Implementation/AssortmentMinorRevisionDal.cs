﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26704 : J.Pickup
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentMinorRevisionDal : MockDalBase<AssortmentMinorRevisionDto>, IAssortmentMinorRevisionDal
    {
        public AssortmentMinorRevisionDto FetchByEntityIdName(int entityId, string name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name  }, true);
        }

        public IEnumerable<AssortmentMinorRevisionDto> FetchAllIncludingDeleted()
        {
            return base.FetchAll<AssortmentMinorRevisionDto>(includeDeleted:true);
        }
    }
}
