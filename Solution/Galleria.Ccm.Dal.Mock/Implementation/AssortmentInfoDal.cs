﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-26140 : I.George
//      InitialVersion
#endregion

#region Version History: (CCM V8.2.0)
// V8-30461 : M.Shelley
//  Added Entity_Id to FetchByProductGroupId
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentInfoDal : MockDalBase<AssortmentInfoDto>, IAssortmentInfoDal
    {
        public IEnumerable<AssortmentInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByProperties<AssortmentDto>(new String[] { "EntityId" }, new Object[] { entityId }, false);
        }

        public IEnumerable<AssortmentInfoDto> FetchByProductGroupId(Int32 entityId, Int32 productGroupId)
        {
            return base.FetchByProperties<AssortmentDto>(new String[] { "EntityId", "ProductGroupId" }, new Object[] { entityId, productGroupId }, false);
        }
       
        public IEnumerable<AssortmentInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByProperties<AssortmentDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }

        public IEnumerable<AssortmentInfoDto> FetchByEntityIdAssortmentSearchCriteria(Int32 entityId, String assortmentSearchCritiera)
        {
           return base.FetchByProperties<AssortmentDto>(new String[] { "EntityId", "Name" }, new Object[] { entityId, assortmentSearchCritiera }, false);
        }

        public IEnumerable<AssortmentInfoDto> FetchByEntityIdAssortmentIds(Int32 entityId, IEnumerable<Int32> assortmentIds)
        {
            return base.FetchByProperties<AssortmentDto>(new String[] { "EntityId", "Id" }, new Object[] { entityId, assortmentIds }, false);
        }
    }
}
