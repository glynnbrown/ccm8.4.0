﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628: L.Luong
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramImportTemplateInfoDal : MockDalBase<PlanogramImportTemplateInfoDto>, IPlanogramImportTemplateInfoDal
    {
        public new IEnumerable<PlanogramImportTemplateInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByEntityId<PlanogramImportTemplateDto>(entityId);
        }

        public IEnumerable<PlanogramImportTemplateInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByProperties<PlanogramImportTemplateDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }
    }
}
