﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created.
// V8-26815 : A.Silva ~ Changed type of validationTemplateId in FetchByValidationTemplateId to Object.

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    internal class ValidationTemplateGroupDal : MockDalBase<ValidationTemplateGroupDto>, IValidationTemplateGroupDal
    {
        #region IValidationTemplateGroupDal Members

        public IEnumerable<ValidationTemplateGroupDto> FetchByValidationTemplateId(object validationTemplateId)
        {
            return FetchByProperties(new[] {"ValidationTemplateId"}, new[] {validationTemplateId});
        }

        #endregion
    }
}