﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25445 : L.Hodson
//  Created
// CCM-25444 : N.Haywood
//      Added FetchByLocationGroupIds
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock dal implementation of ILocationGroupInfoDal
    /// </summary>
    public class LocationGroupInfoDal : MockDalBase<LocationGroupInfoDto>, ILocationGroupInfoDal
    {
        /// <summary>
        /// Returns infos for all deleted groups in the hierarchy
        /// </summary>
        /// <param name="locationHierarchyId"></param>
        /// <returns></returns>
        public IEnumerable<LocationGroupInfoDto> FetchDeletedByLocationHierarchyId(Int32 locationHierarchyId)
        {
            List<LocationGroupInfoDto> dtoList = new List<LocationGroupInfoDto>();

            foreach (LocationGroupInfoDto dto in
                base.FetchByProperties<LocationGroupDto>(
                new String[] { "LocationHierarchyId" },
                new Object[] { locationHierarchyId }, true))
            {
                if (dto.DateDeleted.HasValue)
                {
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }

        public IEnumerable<LocationGroupInfoDto> FetchByLocationGroupIds(IEnumerable<Int32> groupIds)
        {
            return FetchByProperties<LocationGroupDto>(new String[] { "Id" }, new Object[] { groupIds }, true);
        }
    }
}
