﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using System.Collections.Generic;
using System;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class RoleMemberDal : MockDalBase<RoleMemberDto>, IRoleMemberDal
    {

        public IEnumerable<RoleMemberDto> FetchByRoleId(Int32 roleId)
        {
            return base.FetchByProperties(new String[] { "RoleId" }, new Object[] { roleId }, /*includeDeleted*/false);
        }

        public IEnumerable<RoleMemberDto> FetchByUserId(Int32 userId)
        {
            return base.FetchByProperties(new String[] { "UserId" }, new Object[] { userId }, /*includeDeleted*/false);
        }
    }
}
