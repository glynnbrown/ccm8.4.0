﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-27059 : J.Pickup
//		Created
#endregion
#endregion

using System;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentFileDal : MockDalBase<AssortmentFileDto>, IAssortmentFileDal
    {

        public IEnumerable<AssortmentFileDto> FetchByAssortmentId(int assortmentId)
        {
            return FetchByProperties(new String[] { "AssortmentId" }, new Object[] { assortmentId });
        }
    }
}
