﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

//V8-26671 : A.Silva ~ Created

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class CustomColumnDal : MockDalBase<CustomColumnDto>, ICustomColumnDal
    {
        #region ICustomColumnDal Members

        /// <summary>
        ///     Deletes an existing DAL item using the given <paramref name="path" /> as criteria.
        /// </summary>
        /// <param name="columnLayoutId">Unique identifier of the Custom Column Layout item which contains the item to delete.</param>
        /// <param name="path">Path of the Custom Column item in the DAL to be deleted.</param>
        public void DeleteByPath(string columnLayoutId, string path)
        {
            DeleteByProperties(new []{ "CustomColumnLayoutId", "Path"},new object[]{columnLayoutId, path});
        }

        /// <summary>
        ///     Fetches an collection of <see cref="CustomColumnDto" /> instances which have the given
        ///     <paramref name="columnLayoutId" /> value.
        /// </summary>
        /// <param name="columnLayoutId">
        ///     Unique identifier of the CustomColumnLayout that is parent to the fetched
        ///     CustomColumn collection.
        /// </param>
        /// <returns>
        ///     A collection of <see cref="CustomColumnDto" /> which belong to the given
        ///     <paramref name="columnLayoutId" />.
        /// </returns>
        public IEnumerable<CustomColumnDto> FetchByColumnLayoutId(object columnLayoutId)
        {
            return FetchByProperties(new[] {"CustomColumnLayoutId"}, new[] {columnLayoutId});
        }

        #endregion
    }
}
