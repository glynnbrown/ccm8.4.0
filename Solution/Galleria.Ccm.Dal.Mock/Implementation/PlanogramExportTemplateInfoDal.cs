﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramExportTemplateInfoDal : MockDalBase<PlanogramExportTemplateInfoDto>, IPlanogramExportTemplateInfoDal
    {
        public new IEnumerable<PlanogramExportTemplateInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByEntityId<PlanogramExportTemplateDto>(entityId);
        }

        public IEnumerable<PlanogramExportTemplateInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByProperties<PlanogramExportTemplateDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }
    }
}
