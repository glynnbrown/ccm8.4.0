﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 803)
// CCM-29750 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class HighlightInfoDal : MockDalBase<HighlightInfoDto>, IHighlightInfoDal
    {
        public new IEnumerable<HighlightInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<HighlightDto>(entityId);
        }

        public IEnumerable<HighlightInfoDto> FetchByIds(IEnumerable<object> ids)
        {
            return base.FetchByProperties<HighlightDto>(new String[] { "Id" }, new Object[]{ids}, /*incDeleted*/true);
        }

        public IEnumerable<HighlightInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByEntityId<HighlightDto>(entityId, true);
        }
    }
}
