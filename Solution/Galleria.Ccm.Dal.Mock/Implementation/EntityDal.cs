﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25559 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class EntityDal : MockDalBase<EntityDto>, IEntityDal
    {

        #region IEntityDal Members

        public EntityDto FetchDeletedByName(String name)
        {
            foreach (EntityDto dto in DalCache.GetDtos<EntityDto>())
            {
                if (dto.DateDeleted != null && dto.Name == name)
                {
                    return Clone(dto);
                }
            }

            throw new DtoDoesNotExistException();
        }

        public new void Insert(EntityDto dto)
        {
            base.Insert(dto, /*canReinsert*/false);
        }

        #endregion
    }
}
