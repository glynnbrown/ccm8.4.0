﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramImportTemplatePerformanceMetricDal : MockDalBase<PlanogramImportTemplatePerformanceMetricDto>, IPlanogramImportTemplatePerformanceMetricDal
    {
        public IEnumerable<PlanogramImportTemplatePerformanceMetricDto> FetchByPlanogramImportTemplateId(object planogramImportTemplateId)
        {
            return FetchByProperties(new String[] { "PlanogramImportTemplateId" }, new Object[] { planogramImportTemplateId });
        }
    }
}