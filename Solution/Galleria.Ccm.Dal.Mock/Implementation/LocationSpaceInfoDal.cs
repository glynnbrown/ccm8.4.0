﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationSpaceInfoDal : MockDalBase<LocationSpaceInfoDto>, ILocationSpaceInfoDal
    {
        //public override IEnumerable<String> ChildDtoTypes
        //{
        //    get
        //    {
        //        return new List<String>();
        //    }
        //}

        #region ILocationSpaceInfoDal Members

        public new IEnumerable<LocationSpaceInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<EntityDto> sourceEntityDtoList = this.DalCache.GetDtos<EntityDto>().ToList();
            List<LocationSpaceDto> sourceLocationSpaceDtoList = this.DalCache.GetDtos<LocationSpaceDto>().ToList();
            List<LocationDto> sourceLocationDtoList = this.DalCache.GetDtos<LocationDto>().ToList();

            var query = from
                            //LocationSpace
                ls in sourceLocationSpaceDtoList
                        //Location
                        join l in sourceLocationDtoList
                            //LocationSpace.Location_Id = Location.Location_Id
                            on ls.LocationId equals l.Id
                        where ls.EntityId == entityId

                        select new
                        {
                            ls.Id,
                            ls.RowVersion,
                            ls.EntityId,
                            LocationId = l.Id,
                            l.Code,
                            l.Name
                        };


            List<LocationSpaceInfoDto> dtoList = new List<LocationSpaceInfoDto>();
            foreach (var item in query)
            {
                dtoList.Add(new LocationSpaceInfoDto()
                {
                    Id = item.Id,
                    EntityId = item.EntityId,
                    LocationId = item.LocationId,
                    LocationCode = item.Code,
                    LocationName = item.Name
                });
            }

            return dtoList;
        }

        #endregion
    }
}
