﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ReportDal : MockDalBase<ReportDto>, IReportDal
    {
        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<ReportDto> FetchAll()
        {
            List<ReportDto> reportDtos = new List<ReportDto>();

            foreach (ReportDto dto in this.DalCache.GetDtos<ReportDto>())
            {
                if (dto.DateDeleted == null)
                {
                    reportDtos.Add(Clone(dto));
                }
            }
            return reportDtos;
        }
        
        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public ReportDto FetchById(Object id)
        {
            foreach (ReportDto dto in this.DalCache.GetDtos<ReportDto>())
            {
                if (dto.Id.Equals(id))
                {
                    return Clone(dto);
                }
            }
            throw new DtoDoesNotExistException();
        }

        public override void DeleteById(Object id)
        {
            foreach (ReportDto dto in this.DalCache.GetDtos<ReportDto>())
            {
                if (dto.Id.Equals(id))
                {
                    dto.DateDeleted = DateTime.UtcNow;
                }
            }
        }
    }
}
