﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  This was commented out for no reason. Re-instated.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ReportComponentDetailDal : MockDalBase<ReportComponentDetailDto>, IReportComponentDetailDal
    {
        public ReportComponentDetailDto FetchById(Int32 id)
        {
            foreach (ReportComponentDetailDto dto in this.DalCache.GetDtos<ReportComponentDetailDto>())
            {
                if (dto.Id == id)
                {
                    ReportComponentDetailDto returnValue = new ReportComponentDetailDto()
                    {
                        Id = dto.Id,
                        Key = dto.Key,
                        ReportComponentId = dto.ReportComponentId,
                        Value = dto.Value
                    };
                    return returnValue;
                }
            }
            throw new DtoDoesNotExistException();
        }

        public IEnumerable<ReportComponentDetailDto> FetchByReportComponentId(Int32 reportComponentId)
        {
            foreach (ReportComponentDetailDto dto in this.DalCache.GetDtos<ReportComponentDetailDto>())
            {
                if (dto.ReportComponentId == reportComponentId)
                {
                    ReportComponentDetailDto returnValue = new ReportComponentDetailDto()
                    {
                        Id = dto.Id,
                        Key = dto.Key,
                        ReportComponentId = reportComponentId,
                        Value = dto.Value
                    };
                    yield return returnValue;
                }
            }
        }

        public void DeleteByReportComponentId(Int32 reportComponentId)
        {
            List<ReportComponentDetailDto> dtosToDelete = new List<ReportComponentDetailDto>();

            foreach (ReportComponentDetailDto dto in this.DalCache.GetDtos<ReportComponentDetailDto>())
            {
                if (dto.ReportComponentId == reportComponentId)
                {
                    dtosToDelete.Add(dto);
                }
            }

            foreach (ReportComponentDetailDto dto in dtosToDelete)
            {
                this.DalCache.RemoveDto<ReportComponentDetailDto>(dto);
            }
        }
    }
}
