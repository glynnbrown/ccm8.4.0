﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27058 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramMetaDataImageDal : MockDalBase<PlanogramMetadataImageDto>, IPlanogramMetadataImageDal
    {
        public IEnumerable<PlanogramMetadataImageDto> FetchByPlanogramId(object planogramId)
        {
            return FetchByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId });
        }
    }
}
