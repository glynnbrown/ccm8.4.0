﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PerformanceSelectionMetricDal : MockDalBase<PerformanceSelectionMetricDto>, IPerformanceSelectionMetricDal
    {
        public IEnumerable<PerformanceSelectionMetricDto> FetchByPerformanceSelectionId(Int32 performanceSelectionId)
        {
            return base.FetchByProperties(new String[] { "PerformanceSelectionId" }, new Object[] { performanceSelectionId }, false);
        }
    }
}
