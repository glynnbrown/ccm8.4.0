﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed Unused stored procedures
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationProductAttributeInfoDal : MockDalBase<LocationProductAttributeInfoDal>, ILocationProductAttributeInfoDal
    {
        #region Helpers

        private LocationProductAttributeInfoDto GetInfo(LocationProductAttributeDto dto)
        {
            LocationProductAttributeDto attributeDto =
                this.DalCache.GetDtos<LocationProductAttributeDto>().First(p => p.ProductId == dto.ProductId && p.LocationId == dto.LocationId);

            return new LocationProductAttributeInfoDto()
            {
                EntityId = dto.EntityId,
                LocationId = dto.LocationId,
                ProductId = dto.ProductId,
                CasePackUnits = dto.CasePackUnits,
                DaysOfSupply = dto.DaysOfSupply,
                DeliveryFrequencyDays = dto.DeliveryFrequencyDays,
                GTIN = dto.GTIN,
                ShelfLife = dto.ShelfLife,
                DateCreated = dto.DateCreated,
                DateLastModified = dto.DateLastModified,
            };
        }

        private Boolean SatisfiesCriteria(String value, String[] containsParts)
        {
            String loweredValue = value.ToLowerInvariant();

            List<String> searchCriteria = new List<String>();
            List<String> searchOperators = new List<String>();

            for (Int32 i = 0; i < containsParts.Length; i++)
            {
                String part = containsParts[i];

                //part is a criteria
                if (part != "or" &&
                    part != "and")
                {
                    searchCriteria.Add(part);
                }
                else
                {
                    if (containsParts.Length > i + 1)
                    {
                        String nextPart = containsParts[i + 1];
                        if (nextPart != "or" &&
                            nextPart != "and")
                        {
                            if (!String.IsNullOrEmpty(nextPart) &&
                                searchCriteria.Count > searchOperators.Count)
                            {
                                searchOperators.Add(part);
                            }
                        }
                        else
                        {
                            //If the next part is OR or AND, but is the last part use the current as the operator
                            if (!(containsParts.Length == i + 2))
                            {
                                searchCriteria.Add(part);
                            }
                            else
                            {
                                searchOperators.Add(part);
                            }
                        }
                    }
                    else
                    {
                        //This is the last part of the search string, but it is an OR or AND so ignore it
                        //searchCriteria.Add(part);
                    }
                }
            }

            //Check the conditions
            if (searchOperators.Contains("or"))
            {
                foreach (String criteria in searchCriteria)
                {
                    if (loweredValue.Contains(criteria))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                foreach (String criteria in searchCriteria)
                {
                    if (!loweredValue.Contains(criteria))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private List<Int16> GetLocationIdsFromCodes(IEnumerable<String> locationCodes)
        {
            List<Int16> locationIds = new List<Int16>();
            foreach (String locationCode in locationCodes)
            {
                LocationDto dto = this.DalCache.GetDtos<LocationDto>().FirstOrDefault(l => l.Code == locationCode);
                if (dto != null)
                {
                    locationIds.Add(dto.Id);
                }
                else
                {
                    throw new ArgumentException(String.Format("Location Does not exist with Code {0}", locationCode));
                }
            }
            return locationIds;
        }
        #endregion

        #region Fetch

        public List<LocationProductAttributeInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<LocationProductAttributeInfoDto> dtoList = new List<LocationProductAttributeInfoDto>();

            foreach (LocationProductAttributeDto dto in this.DalCache.GetDtos<LocationProductAttributeDto>())
            {
                if (dto.EntityId == entityId)
                {
                    dtoList.Add(
                        new LocationProductAttributeInfoDto()
                        {
                            EntityId = entityId,
                            LocationId = dto.LocationId,
                            ProductId = dto.ProductId,
                            CasePackUnits = dto.CasePackUnits,
                            DaysOfSupply = dto.DaysOfSupply,
                            DeliveryFrequencyDays = dto.DeliveryFrequencyDays,
                            GTIN = dto.GTIN,
                            ShelfLife = dto.ShelfLife,
                            DateCreated = dto.DateCreated,
                            DateLastModified = dto.DateLastModified,
                        });
                }
            }
            return dtoList;
        }

        public List<LocationProductAttributeInfoDto> FetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria)
        {
            List<LocationProductAttributeInfoDto> returnList = new List<LocationProductAttributeInfoDto>();

            //remove special characters
            String processedCriteria = String.Empty;
            foreach (Char c in searchCriteria)
            {
                if (Char.IsLetterOrDigit(c))
                {
                    processedCriteria = String.Format(processedCriteria + c);
                }
            }

            //split into parts
            String[] criteriaParts = searchCriteria.ToLowerInvariant().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (LocationProductAttributeDto dto in this.DalCache.GetDtos<LocationProductAttributeDto>())
            {
                if (dto.EntityId == entityId)
                {
                    LocationDto locationDto = this.DalCache.GetDtos<LocationDto>().First(d => d.Id == dto.LocationId);
                    ProductDto productDto = this.DalCache.GetDtos<ProductDto>().First(d => d.Id == dto.ProductId);

                    String searchValue = String.Format("{0} {1} {2} {3}", productDto.Gtin, productDto.Name, locationDto.Code, locationDto.Name);

                    if (SatisfiesCriteria(searchValue, criteriaParts))
                    {
                        returnList.Add(GetInfo(dto));
                    }
                }
            }

            return returnList;
        }

        #endregion
    }
}
