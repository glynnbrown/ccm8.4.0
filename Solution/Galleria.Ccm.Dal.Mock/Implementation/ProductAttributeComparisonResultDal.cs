﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock DAL for testing CRUD operations
    /// </summary>
    internal class ProductAttributeComparisonResultDal : MockDalBase<ProductAttributeComparisonResultDto>, IProductAttributeComparisonResultDal
    {
        /// <summary>
        /// Method to retrieve a list of Attribute Comparison Results by planogrma id.
        /// </summary>
        /// <param name="planogramId">Planogram id to retrieve.</param>
        /// <returns>List of <seealso cref="ProductAttributeComparisonResultDto"/></returns>
        public IEnumerable<ProductAttributeComparisonResultDto> FetchByPlanogramId(Object planogramId)
        {
            return base.FetchByProperties<ProductAttributeComparisonResultDto>(new String[] { "PlanogramId" }, new Object[] { planogramId }, false);
        }

    }
}
