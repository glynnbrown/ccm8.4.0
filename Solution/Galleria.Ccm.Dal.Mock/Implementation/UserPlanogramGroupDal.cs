﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-25664 : A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class UserPlanogramGroupDal : MockDalBase<UserPlanogramGroupDto>, IUserPlanogramGroupDal
    {
        public IEnumerable<UserPlanogramGroupDto> FetchByUserId(int userId)
        {
            return base.FetchByProperties(new String[] { "UserId" }, new Object[] { userId });
        }
    }
}
