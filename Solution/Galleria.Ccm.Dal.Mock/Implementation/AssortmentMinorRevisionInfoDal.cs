﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version  CCM-V8

// V8-26140 : I.George
// Initial Verion
// V8-26520 : J.Pickup
// Added FetchByProductGroupId
// V8-27059 : J.Pickup
// Added missing implementations for FetchByEntityIdChangeDate & FetchDeletedByEntityId.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentMinorRevisionInfoDal : MockDalBase<AssortmentMinorRevisionInfoDto>, IAssortmentMinorRevisionInfoDal
    {
        public List<AssortmentMinorRevisionInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return base.FetchByProperties<AssortmentMinorRevisionDto>(new String[] { "EntityId" }, new Object[] { entityId }, true).ToList();
        }

        public new List<AssortmentMinorRevisionInfoDto> FetchByEntityId(int entityId)
        {
            return base.FetchByProperties<AssortmentMinorRevisionDto>(new String[] { "EntityId" }, new Object[] { entityId }, false).ToList();
        }

        public List<AssortmentMinorRevisionInfoDto> FetchByProductGroupId(int productGroupId)
        {
            return base.FetchByProperties<AssortmentMinorRevisionDto>(new String[] { "ProductGroupId" }, new Object[] { productGroupId }, false).ToList();
        }

        public AssortmentMinorRevisionInfoDto FetchByEntityIdName(int entityId, string name)
        {
            return base.FetchSingleDtoByProperties<AssortmentMinorRevisionDto>(new String[] { "EntityId", "Name" }, new Object[] { entityId, name });
        }

        public List<AssortmentMinorRevisionInfoDto> FetchByEntityIdChangeDate(int entityId, DateTime changeDate)
        {
            return base.FetchByProperties<AssortmentMinorRevisionDto>(new String[] { "EntityId", "DateLastModified" }, new Object[] { entityId, changeDate }, false).ToList();
        }

        public List<AssortmentMinorRevisionInfoDto> FetchDeletedByEntityId(int entityId, DateTime? deletedDate)
        {
            return base.FetchByProperties<AssortmentMinorRevisionDto>(new String[] { "EntityId", "DateDeleted" }, new Object[] { entityId, deletedDate }, true).ToList();
        }

        public List<AssortmentMinorRevisionInfoDto> FetchByEntityIdAssortmentMinorRevisionSearchCriteria(int entityId, string assortmentSearchCritiera)
        {

            return base.FetchByProperties<AssortmentMinorRevisionDto>( new String[] { "EntityId", "Name" }, new Object[] { entityId, assortmentSearchCritiera }, false).ToList();

        }
    }
}
