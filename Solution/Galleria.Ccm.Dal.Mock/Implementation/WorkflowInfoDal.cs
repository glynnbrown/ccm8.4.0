﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : N.Foster
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkflowInfoDal : MockDalBase<WorkflowInfoDto>, IWorkflowInfoDal
    {
        #region Fetch
        /// <summary>
        /// Returns all workflows for the specified entity
        /// </summary>
        public IEnumerable<WorkflowInfoDto> FetchByEntityId(Int32 entityId)
        {
            return this.FetchByProperties<WorkflowDto>(new String[] { "EntityId" }, new Object[] { entityId }, false);
        }

        /// <summary>
        /// Returns all workflows for the specified entity including deleted items
        /// </summary>
        public IEnumerable<WorkflowInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return this.FetchByProperties<WorkflowDto>(new String[] { "EntityId" }, new Object[] { entityId }, true);
        }
        #endregion
    }
}
