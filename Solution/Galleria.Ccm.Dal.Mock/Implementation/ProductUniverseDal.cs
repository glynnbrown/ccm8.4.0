﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created.
// CCM-25242 : N.Haywood
//  Added fetch methods for imports
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#region Version History : CCM820
// V8-30840 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductUniverseDal : MockDalBase<ProductUniverseDto>, IProductUniverseDal
    {
        public new IEnumerable<ProductUniverseDto> FetchById(int productUniverseId)
        {
            return base.FetchByProperties(new String[] { "Id" }, new Object[] { productUniverseId });
        }

        public ProductUniverseDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name});
        }

        public IEnumerable<ProductUniverseDto> FetchByEntityId(int entityId)
        {
            return base.FetchByProperties(new String[] { "EntityId" }, new Object[] { entityId });
        }

        public IEnumerable<ProductUniverseDto> FetchByProductGroupId(int productGroupId)
        {
            return base.FetchByProperties(new String[] { "ProductGroupId" }, new Object[] { productGroupId });
        }

        public void Upsert(IEnumerable<ProductUniverseDto> dtoList, ProductUniverseIsSetDto isSetDto)
        {
            base.Upsert<ProductUniverseIsSetDto>(dtoList, isSetDto);
        }

        public void DeleteByEntityId(int entityId)
        {
            base.DeleteByProperties(new String[] { "EntityId" }, new Object[] { entityId });
        }

        public void UpdateProductUniverses(Int32 entityId)
        {
           //create a new master product universe for every product group that does not already
            //have one.

            //get the product groups
            ProductHierarchyDto entityHierarchy = 
                this.DalCache.GetDtos<ProductHierarchyDto>().First(h => h.EntityId == entityId);
            Int32 hierarchyId= entityHierarchy.Id;

            IEnumerable<ProductGroupDto> groupDtos =
                this.DalCache.GetDtos<ProductGroupDto>().Where(g => g.ProductHierarchyId == hierarchyId);

            foreach (ProductGroupDto groupDto in groupDtos)
            {
                Int32 groupId= groupDto.Id;

                //get the existing master hierarchy
                ProductUniverseDto masterUniverseDto =
                    this.DalCache.GetDtos<ProductUniverseDto>().FirstOrDefault(p => p.ProductGroupId == groupId &&
                        p.IsMaster);

                if (masterUniverseDto == null)
                {
                    //create a new one
                    masterUniverseDto = new ProductUniverseDto()
                        {
                            IsMaster = true,
                            Name = groupDto.Code + "-" + groupDto.Name,
                            EntityId = entityId,
                            ProductGroupId = groupId,
                            UniqueContentReference = Guid.NewGuid(),
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        };
                    this.DalCache.InsertDto<ProductUniverseDto>(masterUniverseDto);

                    masterUniverseDto = 
                        this.DalCache.GetDtos<ProductUniverseDto>().FirstOrDefault(p => p.ProductGroupId == groupId && p.IsMaster);
                }

                //insert product records
                foreach (ProductDto prod in
                    this.DalCache.GetDtos<ProductDto>()
                    .Where(p => p.ProductGroupId == groupId && p.DateDeleted == null))
                {
                    ProductUniverseProductDto productUniverseProductDto =
                        new ProductUniverseProductDto()
                        {
                            Name = prod.Name,
                            Gtin = prod.Gtin,
                            ProductId = prod.Id,
                            ProductUniverseId = masterUniverseDto.Id
                        };
                    this.DalCache.InsertDto<ProductUniverseProductDto>(productUniverseProductDto);
                }


            }

        }
    }
}
