﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentInventoryRuleDal : MockDalBase<AssortmentInventoryRuleDto>, IAssortmentInventoryRuleDal
    {
        public IEnumerable<AssortmentInventoryRuleDto> FetchByAssortmentId(int assortmentId)
        {
            return base.FetchByProperties(new String[] { "AssortmentId" }, new Object[] { assortmentId });
        }
    }
}
