﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30738 : L.Ineson
//  Created
#endregion
#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using System;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock implementation of IPrintTemplateDal
    /// </summary>
    public sealed class PrintTemplateDal : MockDalBase<PrintTemplateDto>, IPrintTemplateDal
    {
        public PrintTemplateDto FetchById(Object id)
        {
            return base.FetchById((Int32)id);
        }

        public void DeleteById(Object id)
        {
            base.DeleteById((Int32)id);
        }

        public void LockById(object id)
        {
            //Do nothing
        }

        public void UnlockById(object id)
        {
            //Do nothing
        }
    }
}
