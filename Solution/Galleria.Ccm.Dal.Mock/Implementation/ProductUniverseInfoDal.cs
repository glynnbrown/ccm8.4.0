﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created.
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// V8-26560 : A.Probyn
//  Added FetchByProductUniverseIds
// V8-26520 : JPickup
//		FetchByProductGroupId
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductUniverseInfoDal : MockDalBase<ProductUniverseInfoDto>, IProductUniverseInfoDal
    {
        #region Data Transfer Object

        private ProductUniverseInfoDto GetDto(ProductUniverseDto productUniverseDto, IEnumerable<ProductUniverseProductDto> productDtos, ProductGroupDto productGroupDto)
        {
            return new ProductUniverseInfoDto()
            {
                Id = productUniverseDto.Id,
                EntityId = productUniverseDto.EntityId,
                Name = productUniverseDto.Name,
                IsMaster = productUniverseDto.IsMaster,
                ProductGroupId = productUniverseDto.ProductGroupId,
                DateCreated = productUniverseDto.DateCreated,
                DateLastModified = productUniverseDto.DateLastModified,
                UniqueContentReference = productUniverseDto.UniqueContentReference,
                ProductCount = productDtos.Count(),
                ProductGroupCode = (productGroupDto != null) ? productGroupDto.Code : String.Empty,
                ProductGroupName = (productGroupDto != null) ? productGroupDto.Name : String.Empty
            };
        }

        #endregion

        public IEnumerable<ProductUniverseInfoDto> FetchByEntityId(int entityId)
        {
            foreach (ProductUniverseInfoDto dto in base.FetchByEntityId<ProductUniverseDto>(entityId))
            {
                //update product count
                dto.ProductCount = DalCache.GetDtos<ProductUniverseProductDto>().Count(
                            d => d.ProductUniverseId == dto.Id);

                yield return dto;
            }
        }


        public IEnumerable<ProductUniverseInfoDto> FetchByProductUniverseIds(IEnumerable<Int32> productUniverseIds)
        {
            List<ProductUniverseInfoDto> dtoList = new List<ProductUniverseInfoDto>();

            foreach (ProductUniverseDto dto in base.DalCache.GetDtos<ProductUniverseDto>())
            {
                if (productUniverseIds.Contains(dto.Id))
                {
                    IEnumerable<ProductUniverseProductDto> productDtos = base.DalCache.GetDtos<ProductUniverseProductDto>().Where(p => p.ProductUniverseId == dto.Id);
                    ProductGroupDto productGroupDto = base.DalCache.GetDtos<ProductGroupDto>().FirstOrDefault(p => p.Id == dto.ProductGroupId);
                    dtoList.Add(GetDto(dto, productDtos, productGroupDto));
                }
            }

            return dtoList;
        }


        public IEnumerable<ProductUniverseInfoDto> FetchByProductGroupId(int productGroupId)
        {
            return base.FetchByProperties(new String[] { "ProductGroupId" }, new Object[] { productGroupId }, false);
        }
    }
}
