﻿using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkpackagePlanogramDebugDal : MockDalBase<WorkpackagePlanogramDebugDto>, IWorkpackagePlanogramDebugDal
    {
        public IEnumerable<WorkpackagePlanogramDebugDto> FetchBySourcePlanogramId(int sourcePlanogramId)
        {
            return base.FetchByProperties(new String[] { "SourcePlanogramId" }, new Object[] { sourcePlanogramId });
        }

        public void DeleteBySourcePlanogramIdWorkflowTaskId(int sourcePlanogramId, int workflowTaskId)
        {
            base.DeleteByProperties(new String[] { "SourcePlanogramId", "WorkflowTaskId" }, new Object[] { sourcePlanogramId, workflowTaskId });
        }
    }
}
