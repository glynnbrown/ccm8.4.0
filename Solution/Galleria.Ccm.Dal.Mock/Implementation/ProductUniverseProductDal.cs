﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created.
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductUniverseProductDal : MockDalBase<ProductUniverseProductDto>, IProductUniverseProductDal
    {
        public IEnumerable<ProductUniverseProductDto> FetchByProductUniverseId(int productUniverseId)
        {
            return base.FetchByProperties(new String[] { "ProductUniverseId" }, new Object[] { productUniverseId });
        }

        public void Upsert(IEnumerable<ProductUniverseProductDto> dtoList, ProductUniverseProductIsSetDto isSetDto)
        {
            base.Upsert<ProductUniverseProductIsSetDto>(dtoList, isSetDto);
        }
    }
}
