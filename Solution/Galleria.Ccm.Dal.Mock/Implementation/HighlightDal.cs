﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24801 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// V8-25436 : L.Luong
//  Added Insert, Update, Delete, LockbyId, UnlockById, FetchById
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class HighlightDal : MockDalBase<HighlightDto>, IHighlightDal
    {
        #region IHighlightDal Members

        public IEnumerable<HighlightDto> FetchByIds(IEnumerable<object> ids)
        {
            return base.FetchByProperties(new String[] { "Id" }, new Object[] { ids });
        }

        public void UnlockByIds(IEnumerable<object> ids)
        {
            //do nothing.
        }

        public HighlightDto FetchById(Object id)
        {
            return base.FetchById((Int32)id);
        }

        public HighlightDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name }, true);
        }

        public void Insert(HighlightDto dto)
        {
            base.Insert(dto);
        }

        public void Update(HighlightDto dto)
        {
            base.Update(dto);
        }

        public void DeleteById(Object id)
        {
            base.DeleteById((Int32)id);
        }

        public void LockById(Object id)
        {
            //do nothing
        }

        public void UnlockById(Object id)
        {
            //do nothing
        }

        #endregion



        
    }
}
