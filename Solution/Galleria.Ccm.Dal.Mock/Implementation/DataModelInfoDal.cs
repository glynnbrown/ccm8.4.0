﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class DataModelInfoDal : MockDalBase<DataModelInfoDto>, IDataModelInfoDal
    {
        public override IEnumerable<DataModelInfoDto> FetchAll()
        {
            return base.FetchAll<DataModelDto>();
        }
    }
}
