﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24863 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;
using System;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class SystemSettingsDal : MockDalBase<SystemSettingsDto>, ISystemSettingsDal
    {
        public IEnumerable<SystemSettingsDto> FetchDefault()
        {
            return FetchByProperties(new String[] { "EntityId" }, new Object[] { null }, false);
        }
    }
}
