﻿#region Header Information

#region Version History: (CCM 8.0.2)
// CCM-29010 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-31831 : A.Probyn
//  Added FetchByEntityIdName
#endregion

#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using System;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramNameTemplateDal : MockDalBase<PlanogramNameTemplateDto>, IPlanogramNameTemplateDal
    {
        public PlanogramNameTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name }, true);
        }
    }
}