﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30738 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock implementation of IPrintTemplateSectionDal
    /// </summary>
    public sealed class PrintTemplateSectionDal : MockDalBase<PrintTemplateSectionDto>, IPrintTemplateSectionDal
    {

        public IEnumerable<PrintTemplateSectionDto> FetchByPrintTemplateSectionGroupId(int printTemplateSectionGroupId)
        {
            return FetchByProperties(new String[] { "PrintTemplateSectionGroupId" }, new Object[] { printTemplateSectionGroupId });
        }
    }
}
