﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class EventLogNameDal : MockDalBase<EventLogNameDto>, IEventLogNameDal
    {
        #region Fetch
        /// <summary>
        /// Returns all dtos in the data source
        /// </summary>
        /// <returns>All dtos in the data source</returns>
        public new IEnumerable<EventLogNameDto> FetchAll()
        {
            return FetchAll<EventLogNameDto>().ToList();
        }

        /// <summary>
        /// Returns a single event log name record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public EventLogNameDto FetchByName(String name)
        {
            foreach (EventLogNameDto dto in this.DalCache.GetDtos<EventLogNameDto>())
            {
                if (dto.Name == name)
                {
                    return Clone(dto);
                }
            }
            return null;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(EventLogNameDto dto)
        {
            base.Insert(dto, /*canReinsert*/true);
        }
        #endregion
    }
}
