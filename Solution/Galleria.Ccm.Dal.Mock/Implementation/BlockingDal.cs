﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
// V8-27241 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class BlockingDal : MockDalBase<BlockingDto>, IBlockingDal
    {

        public BlockingDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name }, true);
        }
    }
}
