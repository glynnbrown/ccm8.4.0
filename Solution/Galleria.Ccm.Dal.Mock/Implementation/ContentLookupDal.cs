﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26520 : J.Pickup
//		Created.
#endregion
#region Version History: (CCM v8.1)
// V8-30155 : L.Ineson
//  Added FetchByPlanogramIds
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ContentLookupDal : MockDalBase<ContentLookupDto>, IContentLookupDal
    {
        public IEnumerable<ContentLookupDto> FetchByEntityId(int entityId)
        {
            return base.FetchByProperties(new String[] { "EntityId" }, new Object[] { entityId });
        }

        public ContentLookupDto FetchByPlanogramId(Int32 planogramId)
        {
            return base.FetchSingleDtoByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId });
        }


        public IEnumerable<ContentLookupDto> FetchByPlanogramIds(IEnumerable<Int32> planogramIds)
        {
            List<ContentLookupDto> dtoList = new List<ContentLookupDto>();

            foreach (ContentLookupDto dto in base.DalCache.GetDtos<ContentLookupDto>())
            {
                if (planogramIds.Contains(dto.PlanogramId))
                {
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }
    }
}
