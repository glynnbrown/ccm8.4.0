﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
// V8-28013 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class InventoryProfileDal : MockDalBase<InventoryProfileDto>, IInventoryProfileDal
    {
        public new IEnumerable<InventoryProfileDto> FetchById(int id)
        {
            return FetchByProperties(new String[] { "Id" }, new Object[] { id });
        }


        public InventoryProfileDto FetchByEntityIdName(int entityId, string name)
        {
            return base.FetchSingleDtoByProperties(new[] { "EntityId", "Name" }, new Object[] { entityId, name });
        }
    }
}
