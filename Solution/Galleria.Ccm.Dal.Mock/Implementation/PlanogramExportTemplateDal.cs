﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramExportTemplateDal : MockDalBase<PlanogramExportTemplateDto>, IPlanogramExportTemplateDal
    {
        public PlanogramExportTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name }, true);
        }

        public void LockById(object id)
        {
            //do nothing
        }

        public void UnlockById(object id)
        {
            //do nothing
        }
    }
}
