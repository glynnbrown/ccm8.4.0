﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25587 : L.Hodson
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class PlanogramHierarchyDal : MockDalBase<PlanogramHierarchyDto>, IPlanogramHierarchyDal
    {

        #region IPlanogramHierarchyDal Members

        public PlanogramHierarchyDto FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<PlanogramHierarchyDto>(entityId).FirstOrDefault();
            //return FetchByProperties(new String[]{"EntityId"}, new Object[]{ entityId}).FirstOrDefault();
        }

        public Dictionary<Guid, Guid> FetchPlanogramPlanogramGroupGroupings(Int32 entityId)
        {
            return new Dictionary<Guid, Guid>();
        }

        #endregion
    }
}
