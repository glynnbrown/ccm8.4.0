﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created.
// CCM-25449 : N.Haywood
//      Changed FetchByClusterId to fetch location variables
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ClusterLocationDal : MockDalBase<ClusterLocationDto>, IClusterLocationDal
    {

        public IEnumerable<ClusterLocationDto> FetchByClusterId(int clusterId)
        {
            foreach (ClusterLocationDto dto in DalCache.GetDtos<ClusterLocationDto>())
            {
                if (dto.ClusterId == clusterId)
                {
                    // Where appropriate, replace the values from the stored ClusterLocationDto with the values from
                    // the associated LocationDto.
                    LocationDto locationDto = DalCache.GetDtos<LocationDto>().First(
                        l => l.Id == dto.LocationId);
                    ClusterLocationDto returnValue = new ClusterLocationDto()
                    {
                        Id = dto.Id,
                        ClusterId = dto.ClusterId,
                        LocationId = dto.LocationId,
                        Address1 = locationDto.Address1,
                        Address2 = locationDto.Address2,
                        City = locationDto.City,
                        Code = locationDto.Code,
                        Country = locationDto.Country,
                        County = locationDto.County,
                        Name = locationDto.Name,
                        PostalCode = locationDto.PostalCode,
                        Region = locationDto.Region,
                        TVRegion = locationDto.TVRegion,
                    };
                    yield return returnValue;
                }
            }

            //return base.FetchByProperties(new String[] { "ClusterId" }, new Object[] { clusterId });
        }

        public void Upsert(IEnumerable<ClusterLocationDto> dtoList, ClusterLocationIsSetDto isSetDto)
        {
            base.Upsert<ClusterLocationIsSetDto>(dtoList, isSetDto);
        }

        public new void Insert(ClusterLocationDto dto)
        {
            base.Insert(dto, true);
        }
    }
}
