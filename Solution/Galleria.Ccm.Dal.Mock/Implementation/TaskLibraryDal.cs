﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28258 : N.Foster
//	Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class TaskLibraryDal : MockDalBase<TaskLibraryDto>, ITaskLibraryDal
    {
        #region Fetch

        #region FetchByFullName
        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        public TaskLibraryDto FetchByFullName(String fullName)
        {
            return base.FetchSingleDtoByProperties(
                new String[] { "FullName" },
                new Object[] { fullName },
                includeDeleted: true);
        }
        #endregion

        #endregion
    }
}
