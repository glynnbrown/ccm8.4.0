﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="PlanogramComparisonTemplateFieldDal"/>.
    /// </summary>
    public class PlanogramComparisonTemplateFieldDal : MockDalBase<PlanogramComparisonTemplateFieldDto>, IPlanogramComparisonTemplateFieldDal
    {
        public IEnumerable<PlanogramComparisonTemplateFieldDto> FetchByPlanogramComparisonTemplateId(Object planogramComparisonTemplateId)
        {
            return FetchByProperties(new[] { "PlanogramComparisonTemplateId" }, new[] { planogramComparisonTemplateId });
        }
    }
}