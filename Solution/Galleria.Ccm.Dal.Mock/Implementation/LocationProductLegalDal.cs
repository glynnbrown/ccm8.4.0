﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationProductLegalDal : MockDalBase<LocationProductLegalDto>, ILocationProductLegalDal
    {
        #region Fetch

        public LocationProductLegalDto FetchById(Int16 locationId, Int32 productId)
        {
            return base.FetchSingleDtoByProperties(new String[] { "LocationId", "ProductId" }, new Object[] { locationId, productId });
        }

        public List<LocationProductLegalDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> locationProductIdCombinations)
        {
            List<LocationProductLegalDto> dtoList = new List<LocationProductLegalDto>();

            foreach (LocationProductLegalDto dto in this.DalCache.GetDtos<LocationProductLegalDto>())
            {
                Tuple<Int16, Int32> combination = locationProductIdCombinations
                    .FirstOrDefault(c => c.Item1 == dto.LocationId && c.Item2 == dto.ProductId);

                if (combination != null)
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Delete

        public void DeleteById(Int16 locationId, Int32 productId)
        {
            base.DeleteByProperties(new String[] { "LocationId", "ProductId" }, new Object[] { locationId, productId });
        }

        #endregion
    }
}
