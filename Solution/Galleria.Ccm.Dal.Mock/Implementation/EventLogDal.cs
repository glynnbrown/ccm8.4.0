﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName property
//  Added new FetchByWorkpackageId
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class EventLogDal : MockDalBase<EventLogDto>, IEventLogDal
    {
        #region Fetch
        /// <summary>
        /// Returns all dtos in the data source
        /// </summary>
        /// <returns>All dtos in the data source</returns>
        public new IEnumerable<EventLogDto> FetchAll()
        {
            return FetchAll<EventLogDto>().ToList();
        }

        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchByEventLogNameId(int id)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            foreach (EventLogDto dto in this.DalCache.GetDtos<EventLogDto>())
            {
                if (dto.EventLogNameId == id)
                {
                    dtoList.Add(Clone(dto));
                }
            }

            return dtoList;
        }


        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">datetime after which dtos should be returned</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchAllOccuredAfterDateTime(DateTime lastUpdatedDateTime)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            foreach (EventLogDto dto in this.DalCache.GetDtos<EventLogDto>())
            {
                if (lastUpdatedDateTime < dto.DateTime)
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">the amount of dto's to return</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchTopSpecifiedAmountIncludingDeleted(Int32 amountOfRecordsToReturn)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            Int32 recordCount = 1;
            foreach (EventLogDto dto in this.DalCache.GetDtos<EventLogDto>())
            {
                if (recordCount <= amountOfRecordsToReturn)
                {
                    dtoList.Add(Clone(dto));
                    recordCount++;
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchByEventLogNameIdEntryTypeEntityId(Int16 TopRowCount, Nullable<Int32> EventLogNameID, Nullable<Int16> EntryType, Nullable<Int32> EntityID)
        {

            List<EventLogDto> dtoList = new List<EventLogDto>();
            foreach (EventLogDto dto in this.DalCache.GetDtos<EventLogDto>())
            {
                if ((EventLogNameID == null || dto.EventLogNameId == EventLogNameID) &&
                    (EntryType == null || dto.EntryType == EntryType) &&
                    (EntityID == null || dto.EntityId == EntityID))
                {
                    dtoList.Add(Clone(dto));
                }
            }

            return dtoList;
        }

        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchByMixedCriteria(Int16 topRowCount, Nullable<Int32> eventLogNameId, String source,
            Nullable<Int32> eventId, Nullable<Int16> entryType, Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> dateTime,
            String process, String computerName, String urlHelperLink, String description,
            String content, String gibraltarSessionId, DateTime startDate, DateTime endDate, String workpackageName, Int32? workpackageId)
        {

            List<EventLogDto> dtoList = new List<EventLogDto>();
            foreach (EventLogDto dto in this.DalCache.GetDtos<EventLogDto>())
            {
                if ((eventLogNameId == null || dto.EventLogNameId == eventLogNameId) &&
                    (source == null || dto.Source == source) &&
                    (eventId == null || dto.EventId == eventId) &&
                    (entryType == null || dto.EntryType == entryType) &&
                    (userId == null || dto.UserId == userId) &&
                    (entityId == null || dto.EntityId == entityId) &&
                    (dateTime == null || dto.DateTime == dateTime) &&
                    (content == null || dto.Content == content) &&
                    (description == null || dto.Description == description) &&
                    (gibraltarSessionId == null || dto.GibraltarSessionId == gibraltarSessionId) &&
                    (workpackageName == null || dto.WorkpackageName == workpackageName) &&
                    (workpackageId == null || dto.WorkpackageId == workpackageId)
                    )
                {
                    dtoList.Add(Clone(dto));
                }
            }

            return dtoList;
        }

        /// <summary>
        /// Returns the specified dto from the database
        /// </summary>
        /// <param name="id">The id of the dto to return</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchByWorkpackageId(int workpackageId)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            foreach (EventLogDto dto in this.DalCache.GetDtos<EventLogDto>())
            {
                if (dto.WorkpackageId == workpackageId)
                {
                    dtoList.Add(Clone(dto));
                }
            }

            return dtoList;
        }


        #endregion

        #region Insert

        /// <summary>
        /// Inserts the specified dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(EventLogDto dto)
        {
            dto.Id = DalCache.GetNextIdentity<EventLogNameDto>();

            EventLogDto toAdd = Clone(dto);
            if (toAdd.EntityId != null)
            {
                toAdd.EntityName = this.DalCache.GetDtos<EntityDto>().FirstOrDefault(e => e.Id == toAdd.EntityId).Name;
            }
            if (toAdd.UserId != null)
            {
                toAdd.UserName = this.DalCache.GetDtos<UserDto>().FirstOrDefault(u => u.Id == toAdd.UserId).FirstName + " " +
                    this.DalCache.GetDtos<UserDto>().FirstOrDefault(u => u.Id == toAdd.UserId).LastName;
            }
            toAdd.DateTime = DateTime.UtcNow;
            DalCache.InsertDto<EventLogDto>(toAdd);
        }
        /// <summary>
        /// Inserts the specified dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>

        public void InsertWithName(EventLogDto dto)
        {
            EventLogNameDto eventLogNameDto = DalCache.GetDtos<EventLogNameDto>().FirstOrDefault(
                d => d.Name == dto.EventLogName);
            if (eventLogNameDto == null)
            {
                eventLogNameDto = new EventLogNameDto()
                {
                    Id = DalCache.GetNextIdentity<EventLogNameDto>(),
                    Name = dto.EventLogName
                };
                DalCache.InsertDto<EventLogNameDto>(eventLogNameDto);
            }

            dto.EventLogNameId = eventLogNameDto.Id;
            Insert(dto);
        }
        #endregion

        #region Delete

        private void DeleteDto(EventLogDto dto)
        {
            //delete the dto
            this.DalCache.RemoveDto<EventLogDto>(dto);
        }

        public void DeleteAll()
        {
            //delete all entries
            foreach (EventLogDto dto in this.DalCache.GetDtos<EventLogDto>().ToList())
            {
                DeleteDto(dto);
            }
        }

        #endregion
    }
}