﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27004 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class PlanogramValidationMetricInfoDal : MockDalBase<PlanogramValidationMetricInfoDto>, IPlanogramValidationMetricInfoDal
    {
        #region Fetch

        /// <summary>
        ///     Fetches a collection of <see cref="PlanogramValidationMetricInfoDto"/> instances matching the given <paramref name="planogramIds"/>.
        /// </summary>
        /// <param name="planogramIds">A collection of <see cref="Int32"/> values that must be matched by the returned dtos.</param>
        /// <returns>A new collection of <see cref="PlanogramValidationMetricInfoDto"/>.</returns>
        public IEnumerable<PlanogramValidationMetricInfoDto> FetchPlanogramValidationMetricsByPlanogramIds(
            IEnumerable<Int32> planogramIds)
        {
            return PlanogramValidationTemplateMetricDtos(planogramIds).Select(CreateDataTransferObject).ToList();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Creates a <see cref="PlanogramValidationMetricInfoDto"/> from a given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramValidationTemplateGroupMetricDto"/> to create the info dto from.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationMetricInfoDto"/> with the data from teh given <paramref name="dto"/>.</returns>
        private static PlanogramValidationMetricInfoDto CreateDataTransferObject(PlanogramValidationTemplateGroupMetricDto dto)
        {
            return new PlanogramValidationMetricInfoDto
            {
                Id = Convert.ToInt32(dto.Id),
                PlanogramValidationGroupId = Convert.ToInt32(dto.PlanogramValidationTemplateGroupId),
                Field = dto.Field,
                ResultType = dto.ResultType
            };
        }

        private IEnumerable<PlanogramValidationTemplateDto> PlanogramValidationTemplateDtos(IEnumerable<Int32> planogramIds)
        {
            var cacheDtos = base.DalCache.GetDtos<PlanogramValidationTemplateDto>();
            var planogramIdMatchDtos = cacheDtos.Where(dto => planogramIds.Contains(Convert.ToInt32(dto.PlanogramId)));
            return planogramIdMatchDtos;
        }

        private IEnumerable<PlanogramValidationTemplateGroupDto> PlanogramValidationTemplateGroupDtos(IEnumerable<Int32> planogramIds)
        {
            var validationTemplateDtos = PlanogramValidationTemplateDtos(planogramIds);
            var validationTemplateIds = validationTemplateDtos.Select(dto => dto.Id);
            var cacheValidationTemplateGroupDtos = base.DalCache.GetDtos<PlanogramValidationTemplateGroupDto>();
            var validationTemplateGroupDtos = cacheValidationTemplateGroupDtos.Where(
                dto => validationTemplateIds.Contains(dto.PlanogramValidationTemplateId));
            return validationTemplateGroupDtos;
        }

        private IEnumerable<PlanogramValidationTemplateGroupMetricDto> PlanogramValidationTemplateMetricDtos(IEnumerable<Int32> planogramIds)
        {
            var validationTemplateGroupDtos = PlanogramValidationTemplateGroupDtos(planogramIds);
            var validationTemplateGroupIds = validationTemplateGroupDtos.Select(dto => dto.Id);
            var cacheValidationTemplateMetricDtos = base.DalCache.GetDtos<PlanogramValidationTemplateGroupMetricDto>();
            var validationTemplateMetricDtos = cacheValidationTemplateMetricDtos.Where(
                dto => validationTemplateGroupIds.Contains(dto.PlanogramValidationTemplateGroupId));
            return validationTemplateMetricDtos;
        }

        #endregion
    }
}
