﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    internal class ValidationTemplateGroupMetricDal : MockDalBase<ValidationTemplateGroupMetricDto>,
        IValidationTemplateGroupMetricDal
    {
        #region IValidationTemplateGroupMetricDal Members

        public IEnumerable<ValidationTemplateGroupMetricDto> FetchByValidationTemplateGroupId(
            Int32 validationTemplateGroupId)
        {
            return FetchByProperties(new[] {"ValidationTemplateGroupId"}, new object[] {validationTemplateGroupId});
        }

        #endregion
    }
}