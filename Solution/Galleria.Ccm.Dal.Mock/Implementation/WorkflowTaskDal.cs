﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : N.Foster
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkflowTaskDal : MockDalBase<WorkflowTaskDto>, IWorkflowTaskDal
    {
        #region Fetch
        /// <summary>
        /// Returns all items for the specified workflow
        /// </summary>
        public IEnumerable<WorkflowTaskDto> FetchByWorkflowId(Int32 workflowId)
        {
            return this.FetchByProperties(new String[] { "WorkflowId" }, new Object[] { workflowId }, false);
        }
        #endregion
    }
}
