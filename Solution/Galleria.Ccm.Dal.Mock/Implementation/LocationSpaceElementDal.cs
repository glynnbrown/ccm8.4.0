﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationSpaceElementDal : MockDalBase<LocationSpaceElementDto>, ILocationSpaceElementDal
    {
        //public override IEnumerable<String> ChildDtoTypes
        //{
        //    get
        //    {
        //        return new List<String>();
        //    }
        //}

        #region ILocationSpaceElementDal Members

        public IEnumerable<LocationSpaceElementDto> FetchByLocationSpaceBayId(Int32 locationSpaceBayId)
        {
            return FetchByProperties(new String[] { "LocationSpaceBayId" }, new Object[] { locationSpaceBayId });
        }

        public override void DeleteByEntityId(Int32 entityId)
        {
            //Get all location space Dto
            List<LocationSpaceDto> locationSpaceDtos = this.DalCache.GetDtos<LocationSpaceDto>().Where(p => p.EntityId == entityId).ToList();

            foreach (LocationSpaceDto locationSpaceDto in locationSpaceDtos)
            {
                List<LocationSpaceProductGroupDto> productGroupDtos = this.DalCache.GetDtos<LocationSpaceProductGroupDto>().Where(p => p.LocationSpaceId == locationSpaceDto.Id).ToList();

                foreach (LocationSpaceProductGroupDto productGroupDto in productGroupDtos)
                {
                    List<LocationSpaceBayDto> bayDtos = this.DalCache.GetDtos<LocationSpaceBayDto>().Where(p => p.LocationSpaceProductGroupId == productGroupDto.Id).ToList();

                    foreach (LocationSpaceBayDto bayDto in bayDtos)
                    {
                        List<LocationSpaceElementDto> elementDtos = this.DalCache.GetDtos<LocationSpaceElementDto>().Where(p => p.LocationSpaceBayId == bayDto.Id).ToList();

                        foreach (LocationSpaceElementDto elementDto in elementDtos)
                        {
                            DeleteById(elementDto.Id);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
