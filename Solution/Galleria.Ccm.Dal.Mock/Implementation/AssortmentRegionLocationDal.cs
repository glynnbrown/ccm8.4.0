﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26704 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentRegionLocationDal : MockDalBase<AssortmentRegionLocationDto>, IAssortmentRegionLocationDal
    {
        public IEnumerable<AssortmentRegionLocationDto> FetchByRegionId(int assortmentRegionId)
        {
            return base.FetchByProperties(new String[] { "AssortmentRegionId" }, new Object[] { assortmentRegionId });
        }
    }
}
