﻿#region Header Information
#region Version History 830
// V8-31699 : A.Heathcote 
//      Created
#endregion
#endregion

using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class UserEditorSettingsRecentDatasheetDal : MockDalBase<UserEditorSettingsRecentDatasheetDto>, IUserEditorSettingsRecentDatasheetDal
    {
        // blank, as all handled by the MockDalBase

    }
}
