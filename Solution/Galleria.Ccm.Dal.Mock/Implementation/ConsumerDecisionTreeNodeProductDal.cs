﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ConsumerDecisionTreeNodeProductDal : MockDalBase<ConsumerDecisionTreeNodeProductDto>, IConsumerDecisionTreeNodeProductDal 
    {
        public new ConsumerDecisionTreeNodeProductDto FetchById(Int32 id)
        {
            return base.FetchSingleDtoByProperties(new String[] { "Id" }, new Object[] { id });
        }

        public IEnumerable<ConsumerDecisionTreeNodeProductDto> FetchByConsumerDecisionTreeNodeId(int consumerDecisionTreeNodeId)
        {
            return base.FetchByProperties(new String[] { "ConsumerDecisionTreeNodeId" }, new Object[] { consumerDecisionTreeNodeId });
        }
    }
}
