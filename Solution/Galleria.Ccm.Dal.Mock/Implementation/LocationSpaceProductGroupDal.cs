﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LocationSpaceProductGroupDal : MockDalBase<LocationSpaceProductGroupDto>, ILocationSpaceProductGroupDal
    {
        //public override IEnumerable<String> ChildDtoTypes
        //{
        //    get
        //    {
        //        yield return "LocationSpaceBayDto";
        //    }
        //}

        #region ILocationSpaceProductGroupDal Members

        public IEnumerable<LocationSpaceProductGroupDto> FetchByLocationSpaceId(Int32 locationSpaceId)
        {
            return FetchByProperties(new String[] { "LocationSpaceId" }, new Object[] { locationSpaceId });
        }

        public void DeleteByLocationSpaceId(Int32 locationSpaceId)
        {
            DeleteByProperties(new String[] { "LocationSpaceId" }, new Object[] { locationSpaceId });
        }

        #endregion
    }
}
