﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created.
// V8-27132 : A.Kuszyk
//      Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ConsumerDecisionTreeDal : MockDalBase<ConsumerDecisionTreeDto>, IConsumerDecisionTreeDal
    {
        public new ConsumerDecisionTreeDto FetchById(int id)
        {
            return base.FetchSingleDtoByProperties(new String[] { "Id" }, new Object[] { id });
        }

        public IEnumerable<ConsumerDecisionTreeDto> FetchByEntityIdConsumerDecisionTreeIds(int entityId, IEnumerable<int> consumerDecisionTreeIds)
        {
            return base.FetchByProperties(
                new String[] { "EntityId", "Id" },
                new Object[] { entityId, consumerDecisionTreeIds });
        }

        public new ConsumerDecisionTreeDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name });
        }
    }
}
