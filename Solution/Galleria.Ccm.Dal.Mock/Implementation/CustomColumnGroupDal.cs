﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

//V8-26671 : A.Silva ~ Created

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class CustomColumnGroupDal : MockDalBase<CustomColumnGroupDto>, ICustomColumnGroupDal
    {
        #region ICustomColumnGroupDal Members

        /// <summary>
        ///     Deletes an existing DAL item using the given <paramref name="grouping" /> as criteria.
        /// </summary>
        /// <param name="columnLayoutId">Unique identifier of the Custom Column Layout item which contains the item to delete.</param>
        /// <param name="grouping">Name of the Column Group item in the DAL to be deleted.</param>
        public void DeleteByGrouping(string columnLayoutId, string grouping)
        {
            DeleteByProperties(new[] {"ColumnLayoutId", "Grouping"}, new object[] {columnLayoutId, grouping});
        }

        /// <summary>
        ///     Fetches an collection of <see cref="CustomColumnGroupDto" /> instances which have the given
        ///     <paramref name="columnLayoutId" /> value.
        /// </summary>
        /// <param name="columnLayoutId">
        ///     Unique identifier of the CustomColumnLayout that is parent to the fetched
        ///     CustomColumnGroup collection.
        /// </param>
        /// <returns>
        ///     A collection of <see cref="CustomColumnGroupDto" /> which belong to the given
        ///     <paramref name="columnLayoutId" />.
        /// </returns>
        public IEnumerable<CustomColumnGroupDto> FetchByCustomColumnLayoutId(object columnLayoutId)
        {
            return FetchByProperties(new[] {"ColumnLayoutId"}, new[] {columnLayoutId});
        }

        #endregion
    }
}