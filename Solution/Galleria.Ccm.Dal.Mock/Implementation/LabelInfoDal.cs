﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24265 : N.Haywood
//		Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
// V8-27940 : L.Luong
//      Added FetchByEntity and FetchByEntityIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LabelInfoDal : MockDalBase<LabelInfoDto>, ILabelInfoDal
    {
        #region Data Transfer Object

        private LabelInfoDto GetDto(LabelDto labelDto)
        {
            return new LabelInfoDto()
            {
                Id = labelDto.Id,
                EntityId = labelDto.EntityId,
                Name = labelDto.Name,
                Type = labelDto.Type,
                DateDeleted = labelDto.DateDeleted
            };
        }

        #endregion

        #region ILabelInfoDal Members

        public IEnumerable<LabelInfoDto> FetchByIds(IEnumerable<object> ids)
        {
           // return base.FetchByProperties(new String[] { "Id" }, new Object[] { ids });
            List<LabelInfoDto> dtoList = new List<LabelInfoDto>();

            foreach (LabelDto dto in base.DalCache.GetDtos<LabelDto>())
            {
                if (ids.Contains(dto.Id))
                {
                    dtoList.Add(GetDto(dto));
                }
            }

            return dtoList;
        }

        public new IEnumerable<LabelInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<LabelDto>(entityId);
        }

        public IEnumerable<LabelInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return FetchByProperties<LabelDto>(new[] { "EntityId" }, new object[] { entityId }, true);
        }

        public void UnlockByIds(IEnumerable<object> ids)
        {
            //do nothing.
        }

        #endregion
    }
}
