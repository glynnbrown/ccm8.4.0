﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24265 : N.Haywood
//		Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class LabelDal : MockDalBase<LabelDto>, ILabelDal
    {
        #region ILabelDal Members

        public LabelDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return base.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name }, true);
        }

        public void LockById(Object id)
        {
            //do nothing
        }

        public void UnlockById(Object id)
        {
            //do nothing
        }

        #endregion
    }
}
