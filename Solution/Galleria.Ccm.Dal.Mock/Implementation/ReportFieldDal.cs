﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ReportFieldDal : MockDalBase<ReportFieldDto>, IReportFieldDal
    {
        public ReportFieldDto FetchById(Int32 id)
        {
            return base.FetchById((Int32)id);
        }

        public IEnumerable<ReportFieldDto> FetchByReportId(Object reportId)
        {
            return base.FetchByProperties(
                new String[] { "ReportId" },
                new Object[] { reportId },
                false
                );
        }

        public void DeleteById(Int32 id)
        {
            base.DeleteById(id);
        }
    }
}
