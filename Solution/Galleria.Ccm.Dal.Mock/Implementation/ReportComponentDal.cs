﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  This was commented out for no reason. Re-instated.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ReportComponentDal : MockDalBase<ReportComponentDto>, IReportComponentDal
    {
        public IEnumerable<ReportComponentDto> FetchByReportSectionId(Int32 reportSectionId)
        {
            foreach (ReportComponentDto dto in this.DalCache.GetDtos<ReportComponentDto>())
            {
                if (dto.ReportSectionId == reportSectionId)
                {
                    ReportComponentDto returnValue = new ReportComponentDto()
                    {
                        Id = dto.Id,
                        ComponentType = dto.ComponentType,
                        ReportSectionId = reportSectionId,
                        Name = dto.Name,
                        Description = dto.Description,
                        X = dto.X,
                        Y = dto.Y,
                        Height = dto.Height,
                        Width = dto.Width
                    };
                    yield return returnValue;
                }
            }
        }
    }
}
