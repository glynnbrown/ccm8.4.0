﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30738 : L.Ineson
//  Created
#endregion
#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using System.Collections.Generic;
using System;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock implementation of IPrintTemplateDal
    /// </summary>
    public sealed class PrintTemplateInfoDal : MockDalBase<PrintTemplateInfoDto>, IPrintTemplateInfoDal
    {
        public new IEnumerable<PrintTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            return base.FetchByEntityId<PrintTemplateDto>(entityId);
        }

        public IEnumerable<PrintTemplateInfoDto> FetchByIds(IEnumerable<object> ids)
        {
            return base.FetchByProperties<PrintTemplateDto>(new String[] { "Id" }, new Object[] { ids }, /*incDeleted*/true);
        }
    }
}
