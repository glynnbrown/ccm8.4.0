﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ValidationTemplateInfoDal : MockDalBase<ValidationTemplateInfoDto>, IValidationTemplateInfoDal
    {
        public new IEnumerable<ValidationTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            return FetchByEntityId<ValidationTemplateDto>(entityId);
        }

        public IEnumerable<ValidationTemplateInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            return FetchByProperties<ValidationTemplateDto>(new[] {"EntityId"}, new object[] {entityId}, true);
        }
    }
}
