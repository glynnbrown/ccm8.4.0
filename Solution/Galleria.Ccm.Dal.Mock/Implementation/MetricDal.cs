﻿#region Header Information

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class MetricDal : MockDalBase<MetricDto>, IMetricDal
    {
        
    }
}
