﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ReportSectionDal : MockDalBase<ReportSectionDto>, IReportSectionDal
    {
        public IEnumerable<ReportSectionDto> FetchByReportId(Object reportId)
        {
            foreach (ReportSectionDto dto in this.DalCache.GetDtos<ReportSectionDto>())
            {
                if (dto.ReportId.Equals(reportId))
                {
                    ReportSectionDto returnValue = new ReportSectionDto()
                    {
                        Id = dto.Id,
                        Name = dto.Name,
                        Description = dto.Description,
                        ReportId = reportId,
                        SectionType = dto.SectionType,
                        Height = dto.Height,
                        ReportGroupId = dto.ReportGroupId,
                    };
                    yield return returnValue;
                }
            }
        }
    }
}
