﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25454: J.Pickup
//  Created
// V8-26764 : N.Foster
//  Added FetchByName
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class AssortmentDal : MockDalBase<AssortmentDto>, IAssortmentDal
    {
        #region Fetch

        /// <summary>
        /// Fetches the specified assortment by name
        /// </summary>
        public AssortmentDto FetchByName(String name)
        {
            return this.FetchSingleDtoByProperties(new String[] { "Name" }, new Object[] { name }, false);
        }

        public AssortmentDto FetchByEntityIdName(Int32 entityId, String name)
        {
            return this.FetchSingleDtoByProperties(new String[] { "EntityId", "Name" }, new Object[] { entityId, name }, false);
        }

        /// <summary>
        /// Fetches the specified assortment by name
        /// </summary>
        public IEnumerable<AssortmentDto> FetchAllIncludingDeleted()
        {
            return this.FetchAll<AssortmentDto>(includeDeleted:true);
        }

        public AssortmentDto FetchByUniqueContentReference(Guid ucr)
        {
            return this.FetchSingleDtoByProperties(new String[] { "UniqueContentReference" }, new Object[] { ucr }, true);
        }

        #endregion
    }
}