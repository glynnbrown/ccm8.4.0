﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26306 : L.Ineson
//  Created
#endregion
#endregion

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public sealed class UserEditorSettingDal : MockDalBase<UserEditorSettingDto>, IUserEditorSettingDal
    {
        //nothing to implement.
       
    }
}
