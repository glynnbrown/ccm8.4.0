﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class ProductImageDal : MockDalBase<ProductImageDto>, IProductImageDal
    {
        public IEnumerable<ProductImageDto> FetchByProductId(int productId)
        {
            return base.FetchByProperties(
                new String[] { "ProductId" },
                new Object[] { productId });
        }

        public IEnumerable<ProductImageDto> FetchByProductIdImageType(int productId, byte imageType)
        {
            return base.FetchByProperties(
                new String[] { "ProductId", "ImageType" },
                new Object[] { productId, imageType });
        }

        /// <summary>
        /// Manually copied from GFS, because of requirement to match to Image Dtos as well.
        /// </summary>
        public ProductImageDto FetchByProductIdImageTypeFacingTypeCompressionId(int productId, byte imageType, byte facingType, int compressionId)
        {
            foreach (ProductImageDto dto in this.DalCache.GetDtos<ProductImageDto>())
            {
                if ((dto.ProductId == productId) &&
                    (dto.ImageType == imageType) &&
                    (dto.FacingType == facingType))
                {
                    ImageDto imgDto = this.DalCache.GetDtos<ImageDto>().FirstOrDefault(i => i.Id == dto.ImageId);
                    if (imgDto != null)
                    {
                        if (imgDto.CompressionId == compressionId)
                        {
                            return Clone(dto);
                        }
                    }
                }
            }

            throw new DtoDoesNotExistException();
        }
    }
}
