﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkpackagePlanogramParameterDal : MockDalBase<WorkpackagePlanogramParameterDto>, IWorkpackagePlanogramParameterDal
    {
        #region Fetch
        /// <summary>
        /// Returns all items for the specified workpackage planogram
        /// </summary>
        public IEnumerable<WorkpackagePlanogramParameterDto> FetchByWorkpackagePlanogramId(Int32 workpackagePlanogramId)
        {
            return this.FetchByProperties(new String[] { "WorkpackagePlanogramId" }, new Object[] { workpackagePlanogramId }, false);
        }
        #endregion
    }
}
