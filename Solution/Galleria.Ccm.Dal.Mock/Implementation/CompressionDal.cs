﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class CompressionDal : MockDalBase<CompressionDto>, ICompressionDal
    {

        public new List<CompressionDto> FetchAll()
        {
            return base.FetchAll().ToList();
        }

        public List<CompressionDto> FetchAllEnabled()
        {
            return base.FetchAll()
                .ToList()
                .Where(dto => dto.Enabled==true)
                .ToList();
        }
    }
}
