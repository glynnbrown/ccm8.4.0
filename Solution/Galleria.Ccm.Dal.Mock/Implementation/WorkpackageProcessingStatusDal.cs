﻿using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;


namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkpackageProcessingStatusDal : MockDalBase<WorkpackageProcessingStatusDto>, IWorkpackageProcessingStatusDal
    {
        public WorkpackageProcessingStatusDto FetchByWorkpackageId(int workpackageId)
        {
            return base.FetchSingleDtoByProperties(new String[] { "WorkpackageId" }, new Object[] { workpackageId });
        }

        public void Increment(int workpackageId, String statusDescription, DateTime dateLastUpdated)
        {
            throw new NotImplementedException();
        }

        public void Initialize(WorkpackageProcessingStatusDto dto)
        {
            throw new NotImplementedException();
        }

        public void SetPending(int workpackageId, byte processingStatus, string statusDescription)
        {
            throw new NotImplementedException();
        }
    }
}
