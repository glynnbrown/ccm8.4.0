﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    public class WorkflowTaskParameterValueDal : MockDalBase<WorkflowTaskParameterValueDto>, IWorkflowTaskParameterValueDal
    {
        public IEnumerable<WorkflowTaskParameterValueDto> FetchByWorkflowTaskParameterId(int workflowTaskParameterId)
        {
            return this.FetchByProperties(new String[] { "WorkflowTaskParameterId" }, new Object[] { workflowTaskParameterId }, false);
        }
    }
}
