﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25748 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.UnitTesting.MockDal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mock.Implementation
{
    /// <summary>
    /// Mock dal implementation of IFixturePackageDal
    /// </summary>
    public class FixturePackageDal : MockDalBase<FixturePackageDto>, IFixturePackageDal
    {
        public void LockById(Object id)
        {
            throw new NotImplementedException();
        }

        public void UnlockById(Object id)
        {
            throw new NotImplementedException();
        }
    }
}
