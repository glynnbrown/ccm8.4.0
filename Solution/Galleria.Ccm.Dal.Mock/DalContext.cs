﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24801 : L.Hodson
//  Created
// V8-25745 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Ccm.Dal.Mock
{
    public class DalContext : MockDalContextBase
    {

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="dalCache">The dal cache</param>
        internal DalContext(MockDalCache dalCache) : base(dalCache) { }
        #endregion

        protected override IEnumerable<MockDalContextBase.AssemblyAndNamespace> DalImplementationLocations
        {
            get
            {
                yield return new MockDalContextBase.AssemblyAndNamespace(GetType().Assembly, "Galleria.Ccm.Dal.Mock.Implementation");
                yield return new MockDalContextBase.AssemblyAndNamespace(typeof(Galleria.Framework.Planograms.Dal.Mock.DalFactory).Assembly, "Galleria.Framework.Planograms.Dal.Mock.Implementation");
            }
        }
    }
}
