﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24801 : L.Hodson
//  Created
// V8-25745 : K.Pickup
//      Changed to use framework Mock DAL base classes.
//V8-26124 : L.Ineson
//  Added AddDefaultData method
#endregion
#endregion

using System;
using System.IO;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using System.Xml.XPath;

namespace Galleria.Ccm.Dal.Mock
{
    public class DalFactory : MockDalFactoryBase
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalFactory()
            : base(new DalFactoryConfigElement())
        {
            this.InitializeCache();
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalFactory(DalFactoryConfigElement dalFactoryConfig)
            : base(dalFactoryConfig)
        {
            this.InitializeCache();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initializes the dal cache
        /// </summary>
        private void InitializeCache()
        {
            this.InsertDataModels();
        }

        /// <summary>
        /// Inserts all data models into the mock dal
        /// </summary>
        private void InsertDataModels()
        {
            // create a data model report dto for each
            // report model xml file embedded in this assembly
            Assembly assembly = Assembly.GetExecutingAssembly();
            String resourcePrefix = "Galleria.Ccm.Dal.Mock.DataModels.";
            foreach (String resourceName in assembly.GetManifestResourceNames())
            {
                if ((resourceName.Length >= resourcePrefix.Length) &&
                    (resourceName.Substring(0, resourcePrefix.Length) == resourcePrefix))
                {
                    // get the model name
                    String name = resourceName.Substring(resourcePrefix.Length).Replace(".xml", String.Empty);

                    // get the script data
                    String data;
                    using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream(resourceName)))
                    {
                        data = reader.ReadToEnd();
                    }

                    String dataModelVersion = "0";
                    String dataModelFormatversion = "0";

                    // get the name and description from the data model
                    using (StringReader reader = new StringReader(data))
                    {
                        XPathDocument document = new XPathDocument(reader);
                        XPathNavigator navigator = document.CreateNavigator();
                        dataModelVersion = navigator.SelectSingleNode("/DataModel/@modelVersion").Value;
                        dataModelFormatversion = navigator.SelectSingleNode("/DataModel/@formatVersion").Value;
                    }

                    // now create a dto
                    using (IDalContext dalContext = this.CreateContext())
                    {
                        using (IDataModelDal dal = dalContext.GetDal<IDataModelDal>())
                        {
                            // create a new dto
                            DataModelDto dto = new DataModelDto()
                            {
                                FileName = String.Format("{0}.xml", name),
                                Name = name,
                                Description = String.Format("{0} Data Model", name),
                                Data = data,
                                Version = Convert.ToInt32(dataModelVersion),
                                FormatVersion = Convert.ToInt32(dataModelFormatversion)
                            };

                            // insert the dto into the dal
                            dal.Insert(dto);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates a new dal context
        /// </summary>
        /// <returns>A new dal context</returns>
        public override IDalContext CreateContext()
        {
            return new DalContext(_dalCache);
        }

        /// <summary>
        /// Called when this dal is required to create its database
        /// </summary>
        public override void CreateDatabase()
        {
            //Insert the default data
            AddDefaultData();
        }

        /// <summary>
        /// Inserts the default data into the dalCache
        /// </summary>
        private void AddDefaultData()
        {
            //TODO - Sort out - Add in db config element so that we can authenticate.
            this.DalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", "Mock"));

            #region Default Administrator

            //Add the admin role
            Int32 roleId= _dalCache.GetNextIdentity<DataTransferObjects.RoleDto>();
            DataTransferObjects.RoleDto role = new DataTransferObjects.RoleDto()
            {
                Id = roleId,
                Name = "Administrator",
                Description = "Built-in administrator role",
                IsAdministrator = true,
            };
            _dalCache.InsertDto(role);

            //Add the user
            Int32 userId = _dalCache.GetNextIdentity<DataTransferObjects.UserDto>();
            DataTransferObjects.UserDto user = new DataTransferObjects.UserDto()
            {
                Id = userId,
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                Description = "Default user",
                Title = String.Empty,
                FirstName = "System",
                LastName = "Administrator",
                //Initials = "fl",
                //EmailAddress = "first.last@galleria-rts.com",
                Office = String.Empty,
                Department = String.Empty,
                Company = String.Empty,
                //TelephoneCountryCode = String.Empty,
                //TelephoneAreaCode = String.Empty,
                TelephoneNumber = String.Empty,
                //IsDisabled = false,
            };
            _dalCache.InsertDto(user);

            //Add the role membership
            Int32 roleMemberId = _dalCache.GetNextIdentity<DataTransferObjects.RoleMemberDto>();
            DataTransferObjects.RoleMemberDto member = new DataTransferObjects.RoleMemberDto()
            {
                Id = roleMemberId,
                RoleId = roleId,
                UserId = userId
            };
            _dalCache.InsertDto(member);

            #endregion
        }

        #endregion
    }
}
