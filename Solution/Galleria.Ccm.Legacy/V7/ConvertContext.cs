﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Legacy.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Legacy.V7
{
    public class ConvertContext
    {
        Int32 bayIndex = -1;
        private SplitBay _currentBay = null;
        private SplitBay _lastBay = null;
        private SplitBay _nextBay = null;
        private List<SplitBay> _orderedBays;
        public Planogram ConvertedPlan { get; set; }
        public PlanogramSubComponent CurrentConvertedSubComponent { get; set; }
        public PlanogramFixtureItem CurrentConvertedFixtureItem { get; set; }
        public PlanogramFixtureComponent CurrentConvertedFixtureComponent { get; set; }
        public SplitPlan CurrentPlan { get; private set; }
        public SplitBay LastBay
        {
            get
            {
                return _lastBay;
            }
        }
        public SplitBay CurrentBay
        {
            get
            {
                return _currentBay;
            }
        }
        public SplitBay NextBay
        {
            get
            {
                return _nextBay;
            }
        }
        public BayRunPosition CurrentBayRunPosition { get; private set; }
        public SplitElement CurrentElement { get; set; }
        public SplitPosition CurrentPostion { get; set; }
        public ClientType Client { get; private set; }
        public GenericConvert Converter { get; private set; }

        public ConvertContext(SplitPlan plan, ClientType client)
        {
            this.CurrentPlan = plan;
            this.Client = client;
            this.Converter = Helpers.GetClientConverter(client);

            _orderedBays = plan.Bays.OrderBy(p => p.XPosition).ToList();
        }

        public Boolean ReadNextBay()
        {
            bayIndex++;
            if (bayIndex >= _orderedBays.Count) return false;

            _lastBay = _currentBay;
            _currentBay = _orderedBays[bayIndex];
            
            if (bayIndex + 1 < _orderedBays.Count)
            {
                _nextBay = _orderedBays[bayIndex + 1];
            }
            CurrentBayRunPosition = GetCurrentBayRunPosition();
            return true;
        }

        private BayRunPosition GetCurrentBayRunPosition()
        {
            if (LastBay == null)
            {
                if (NextBay == null)
                {
                    return BayRunPosition.MiddleOrNone; // only one bay
                }
                else if (NextBay.ParentPlanBay.Id.Equals(CurrentBay.ParentPlanBay.Id))
                {
                    return BayRunPosition.Start; //Start of a run
                }
                else
                {
                    return BayRunPosition.MiddleOrNone; // not in a bay run
                }
            }
            else if (NextBay == null)
            {
                if (LastBay.ParentPlanBay.Id.Equals(CurrentBay.ParentPlanBay.Id)    )
                {
                    return BayRunPosition.End; //End of a run
                }
                else
                {
                    return BayRunPosition.MiddleOrNone; // not in a bay run
                }
            }
            else
            {
                Boolean isEnd = LastBay.ParentPlanBay.Id.Equals(CurrentBay.ParentPlanBay.Id);
                Boolean isStart = NextBay.ParentPlanBay.Id.Equals(CurrentBay.ParentPlanBay.Id);

                if (isEnd && isStart)
                {
                    return BayRunPosition.Both;
                }
                else if(isEnd)
                {
                    return BayRunPosition.End;
                }
                else if (isStart)
                {
                    return BayRunPosition.Start;
                }
                else
                {
                    return BayRunPosition.MiddleOrNone;
                }
            }


        }
    }

    /// <summary>
    /// Enum to denote the position of a CCM v7 bay run
    /// </summary>
    public enum BayRunPosition
    {
        Start,
        End,
        MiddleOrNone,
        Both
    }
}
