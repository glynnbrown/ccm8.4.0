﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Legacy.Model;

namespace Galleria.Ccm.Legacy.V7
{
    /// <summary>
    /// Helper for creating fixture objects
    /// </summary>
    public class FixtureHelper
    {
        /// <summary>
        /// Creates all the objects requird to convert the current fixture in the context into
        /// the V8 structure. Including positons.
        /// </summary>
        /// <param name="context"></param>
        public static void AddFixture(ConvertContext context)
        {
            Int32 bayId = context.CurrentBay.Id;
            List<SplitElement> elements = context.CurrentPlan.Elements.Where(e => e.SplitBayId == bayId).OrderBy(p => p.YPosition).ToList();
            List<SplitTextBox> textBoxes = context.CurrentPlan.TextBoxes.Where(b => b.SplitBayId == bayId).ToList();

            PlanogramFixture fixture = context.ConvertedPlan.Fixtures.Add();
            fixture.Id = PlanHelper.GetNextFixtureId();

            // Truncate name to 50 characters if length exceeds 100
            fixture.Name = context.CurrentBay.Name.Substring(0, context.CurrentBay.Name.Length > 100 ? 100 : context.CurrentBay.Name.Length);
            fixture.Width = (Single)context.CurrentBay.Width;
            fixture.Height = (Single)context.CurrentBay.Height;
            fixture.Depth = (Single)context.CurrentBay.Depth;
            // Create bay fixture item reference
            PlanogramFixtureItem bayFixture = GetFixtureItem(context, fixture);
            context.CurrentConvertedFixtureItem = bayFixture;

            List<SplitTextBox> relatedTextBoxes = context.CurrentPlan.TextBoxes.Where(p =>
                p.SplitBayId == bayId &&
                p.Type == PlanTexboxType.Informational).ToList();

            foreach (SplitTextBox box in relatedTextBoxes)
            {
                // context.CurrentPostion = pos;
                PlanogramAnnotation anno = PlanogramAnnotation.NewPlanogramAnnotation();
                anno.PlanogramSubComponentId = context.CurrentConvertedSubComponent.Id;
                anno.PlanogramFixtureComponentId = context.CurrentConvertedFixtureComponent.Id;
                anno.PlanogramFixtureItemId = context.CurrentConvertedFixtureItem.Id;
                anno.Text = String.IsNullOrWhiteSpace(box.Description) ? "-" : box.Description;
                anno.Width = fixture.Width;
                anno.Height = 20;
                anno.X = bayFixture.X;
                anno.Y = -10;
                anno.Z = 0;

                context.ConvertedPlan.Annotations.Add(anno);
            }

            foreach (SplitElement element in elements)
            {
                context.CurrentElement = element;
                PlanogramFixtureComponent fComponent = GetFixtureComponent(context);
                context.CurrentConvertedFixtureComponent = fComponent;


                PlanogramComponent component = GetComponent(context);
                fComponent.PlanogramComponentId = component.Id;
                //context.ConvertedPlan.Components.Add(component);

                fixture.Components.Add(fComponent);

            }

            context.Converter.CreateBackboard(fixture);
        }
        private static PlanogramFixtureItem GetFixtureItem(ConvertContext context, PlanogramFixture fixture)
        {
            PlanogramFixtureItem output = context.ConvertedPlan.FixtureItems.Add(fixture);

            output.Id = PlanHelper.GetNextFixtureItemId();

            //Bay Position Relative to Fixture Item            
            output.X = (Single)context.CurrentBay.XPosition;
            output.Y = 0;
            output.Z = 0;
            output.Roll = 0;
            output.Slope = 0;
            output.Angle = 0;

            return output;
        }
        private static PlanogramComponent GetComponent(ConvertContext context)
        {
            PlanogramComponent component;

            var uint32Color = (((UInt32)255) << 24) |
                        (((UInt32)50) << 16) |
                        (((UInt32)50) << 8) |
                        (UInt32)50;

            switch (PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.ElementType))
            {
                case PlanElementType.Shelf:
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Shelf, 0, 0, 0, (Int32)uint32Color);
                    break;
                case PlanElementType.Bar:
                    uint32Color = (((UInt32)255) << 24) |
                      (((UInt32)59) << 16) |
                      (((UInt32)180) << 8) |
                      (UInt32)151;
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Bar, 0, 0, 0, (Int32)uint32Color);
                    break;
                case PlanElementType.Peg:
                    uint32Color = (((UInt32)255) << 24) |
                       (((UInt32)180) << 16) |
                       (((UInt32)151) << 8) |
                       (UInt32)59;
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Peg, 0, 0, 0, (Int32)uint32Color);
                    break;
                case PlanElementType.Chest:
                    uint32Color = (((UInt32)255) << 24) |
                       (((UInt32)059) << 16) |
                       (((UInt32)151) << 8) |
                       (UInt32)180;
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Chest, 0, 0, 0, (Int32)uint32Color);
                    break;
                case PlanElementType.Pallet:
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Pallet, 0, 0, 0, (Int32)uint32Color);
                    break;
                default:
                    component = PlanogramComponent.NewPlanogramComponent();
                    break;
            }


            component.Id = PlanHelper.GetNextComponentId();
            component.Name = context.CurrentElement.Name;

            PlanogramSubComponent subComponent = GetSubComponent(context, component);
            component.Height = subComponent.Height;
            component.Width = subComponent.Width;
            component.Depth = subComponent.Depth;

            //var uint32Color = (((UInt32)255) << 24) |
            //       (((UInt32)180) << 16) |
            //       (((UInt32)180) << 8) |
            //       (UInt32)180;

            return component;
        }
        private static PlanogramFixtureComponent GetFixtureComponent(ConvertContext context)
        {
            PlanogramFixtureComponent output = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
            output.Roll = 0;
            output.Slope = 0;
            output.Angle = 0;
            switch (PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.ElementType))
            {
                case PlanElementType.Shelf:
                    output.Slope = (Single)((-context.CurrentElement.ShelfSlope / 360) * (Math.PI * 2));
                    output.X = 0;
                    output.Y = (Single)(context.CurrentElement.YPosition - (context.CurrentElement.ShelfThick * Math.Cos(output.Slope)));
                    output.Z = (Single)(context.CurrentElement.ZPosition);
                    //output.Z = (Single)(context.CurrentElement.ShelfDepth * Math.Cos(output.Slope));
                    //output.Y -= (Single)(context.CurrentElement.ShelfDepth * Math.Sin(output.Slope));
                    break;
                case PlanElementType.Bar:
                    output.X = 0;
                    output.Y = (Single)(context.CurrentElement.YPosition - context.CurrentElement.BarThick);
                    output.Z = (Single)(context.CurrentElement.ZPosition);
                    break;
                case PlanElementType.Peg:
                    output.X = 0;
                    output.Y = (Single)(context.CurrentElement.YPosition - context.CurrentElement.PegHeight);
                    output.Z = (Single)(context.CurrentElement.ZPosition);
                    break;
                case PlanElementType.Chest:
                    output.X = 0;
                    output.Y = (Single)(context.CurrentElement.YPosition);
                    output.Z = (Single)(context.CurrentElement.ZPosition);
                    break;
                default:
                    output.X = 0;
                    output.Y = (Single)(context.CurrentElement.YPosition - context.CurrentElement.ShelfThick);
                    output.Z = (Single)(context.CurrentElement.ZPosition);
                    break;
            }



            return output;
        }
        private static PlanogramSubComponentCombineType GetCombineType(ConvertContext context)
        {
            PlanogramSubComponentCombineType output = (PlanogramSubComponentCombineType)context.CurrentElement.ParentPlanElement.CombineDirection;
            var elements = context.CurrentPlan.Elements.Where(e => e.ParentPlanElement.Equals(context.CurrentElement.ParentPlanElement)).OrderBy(e => e.Id).ToList();

            //if we have multiple elements sharing the original element then we need
            //a bit more processing.
            if (elements.Count > 1 && output != PlanogramSubComponentCombineType.Both)
            {
                Boolean first = true;
                Boolean last = false;

                foreach (SplitElement element in elements)
                {
                    Boolean equal = context.CurrentElement.Equals(element);
                    if (first)
                    {
                        first = equal;
                    }
                    last = equal;
                };

                if (first && !last)
                {
                    if (output == PlanogramSubComponentCombineType.LeftOnly)
                    {
                        output = PlanogramSubComponentCombineType.Both;
                    }
                    else
                    {
                        output = PlanogramSubComponentCombineType.RightOnly;
                    }
                }
                else if (!first && last)
                {
                    if (output == PlanogramSubComponentCombineType.RightOnly)
                    {
                        output = PlanogramSubComponentCombineType.Both;
                    }
                    else
                    {
                        output = PlanogramSubComponentCombineType.LeftOnly;
                    }
                }
                else if (!first && !last)
                {
                    output = PlanogramSubComponentCombineType.Both;
                }
            }

            return output;
            //switch (context.CurrentBayRunPosition)
            //{
            //    case BayRunPosition.Start:
            //        return PlanogramSubComponentCombineType.RightOnly;
            //    case BayRunPosition.End:
            //        return PlanogramSubComponentCombineType.LeftOnly;
            //    case BayRunPosition.MiddleOrNone:
            //        return PlanogramSubComponentCombineType.None;
            //    case BayRunPosition.Both:
            //        return PlanogramSubComponentCombineType.Both;
            //    default:
            //        return PlanogramSubComponentCombineType.None;
            //}
        }
        private static PlanogramSubComponent GetSubComponent(ConvertContext context, PlanogramComponent component)
        {
            PlanogramSubComponent output = component.SubComponents.First();


            Single positionYOffset = 0;

            switch (PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.ElementType))
            {
                case PlanElementType.Shelf:

                    output.Height = context.CurrentElement.ShelfThick;
                    output.Width = context.CurrentElement.ShelfWidth;
                    output.Depth = context.CurrentElement.ShelfDepth;

                    output.MerchandisingStrategyX = context.Converter.ShelfXMerchStrategy;
                    output.MerchandisableHeight = (Single)context.CurrentElement.ShelfHeight;
                    positionYOffset = (Single)context.CurrentElement.YPosition - (Single)context.CurrentElement.ShelfThick;
                    output.RiserHeight = (Single)context.CurrentElement.ShelfRiser;
                  
                    break;
                case PlanElementType.Bar:

                    output.Width = context.CurrentElement.BarWidth;
                    output.Height = context.CurrentElement.BarThick;
                    output.Depth = context.CurrentElement.BarDepth;

                    output.MerchandisableHeight = (Single)context.CurrentElement.BarHeight;
                    positionYOffset = (Single)context.CurrentElement.YPosition - (Single)context.CurrentElement.BarThick;

                    break;
                case PlanElementType.Peg:

                    output.Width = context.CurrentElement.PegWidth;
                    output.Height = context.CurrentElement.PegHeight;
                    output.Depth = 0.01f;

                    output.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                    output.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
                    output.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

                    output.MerchConstraintRow1SpacingX = (Single)context.CurrentElement.ParentPlanElement.PegHorizSpace;
                    output.MerchConstraintRow1StartX = (Single)context.CurrentElement.ParentPlanElement.PegHorzStart;
                    output.MerchConstraintRow1SpacingY = (Single)context.CurrentElement.ParentPlanElement.PegVertSpace;
                    output.MerchConstraintRow1StartY = (Single)context.CurrentElement.ParentPlanElement.PegVertStart;

                    output.MerchConstraintRow1Height = Math.Min(output.MerchConstraintRow1Height, output.MerchConstraintRow1SpacingY * 0.5F);
                    output.MerchConstraintRow1Width = Math.Min(output.MerchConstraintRow1Width, output.MerchConstraintRow1SpacingX * 0.5F);

                    output.MerchandisableHeight = (Single)context.CurrentElement.PegHeight;
                    positionYOffset = (Single)context.CurrentElement.YPosition - (Single)context.CurrentElement.PegHeight;

                    break;
                case PlanElementType.Chest:


                    output.Width = context.CurrentElement.ChestWidth;
                    output.Height = context.CurrentElement.ChestHeight;
                    output.Depth = context.CurrentElement.ChestDepth;

                    output.FaceThicknessBottom = context.CurrentElement.ChestHeight - context.CurrentElement.ChestInside;

                    output.FaceThicknessFront = context.CurrentElement.ChestWall;
                    output.FaceThicknessBack = context.CurrentElement.ChestWall;

                    //Handle left and right walls
                    switch (context.CurrentElement.ParentPlanElement.ChestWallRenderType)
                    {
                        case ChestWallRenderType.LeftAndRight:
                            output.FaceThicknessLeft = context.CurrentElement.ChestWall;
                            output.FaceThicknessRight = context.CurrentElement.ChestWall;
                            break;
                        case ChestWallRenderType.LeftOnly:
                            output.FaceThicknessLeft = context.CurrentElement.ChestWall;
                            output.FaceThicknessRight = 0;
                            break;
                        case ChestWallRenderType.RightOnly:
                            output.FaceThicknessLeft = 0;
                            output.FaceThicknessRight = context.CurrentElement.ChestWall;
                            break;
                        case ChestWallRenderType.None:
                            output.FaceThicknessLeft = 0;
                            output.FaceThicknessRight = 0;
                            break;

                    }
                    output.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                    output.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
                    output.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

                    output.MerchandisableHeight = (Single)context.CurrentElement.ChestMerchandising;
                    positionYOffset = (Single)(context.CurrentElement.ChestHeight - context.CurrentElement.ChestInside);

                    //GET WALLS
                    //output.SubComponents.AddRange(GetChestSubComponents());
                    break;
                case PlanElementType.Pallet:

                    output.Width = context.CurrentElement.ShelfWidth;
                    output.Height = context.CurrentElement.ShelfThick;
                    output.Depth = context.CurrentElement.ShelfDepth;

                    output.MerchandisableHeight = (Single)context.CurrentElement.ShelfHeight;
                    positionYOffset = (Single)context.CurrentElement.YPosition - (Single)context.CurrentElement.ShelfThick;
                    break;
                default:

                    output.Width = context.CurrentElement.ShelfWidth;
                    output.Height = context.CurrentElement.ShelfThick;
                    output.Depth = context.CurrentElement.ShelfDepth;

                    break;
            }
            output.CombineType = GetCombineType(context);
            output.Name = context.CurrentElement.Name;

            output.BackOverhang = (Single)context.CurrentElement.ParentPlanElement.BackOverhang;
            output.TopOverhang = (Single)context.CurrentElement.ParentPlanElement.TopOverhang;
            output.FrontOverhang = (Single)context.CurrentElement.ParentPlanElement.FrontOverhang;
            output.BottomOverhang = (Single)context.CurrentElement.ParentPlanElement.BottomOverhang;
            output.LeftOverhang = (Single)context.CurrentElement.ParentPlanElement.LeftOverhang;
            output.RightOverhang = (Single)context.CurrentElement.ParentPlanElement.RightOverhang;

            List<SplitPosition> relatedPositions = context.CurrentPlan.Positions
                   .Where(p => p.SplitElementId == context.CurrentElement.Id)
                   .OrderBy(e => e.XPosition)
                   .OrderBy(p => p.YPosition).ToList();

            
            context.CurrentConvertedSubComponent = output;

            foreach (SplitPosition pos in relatedPositions)
            {
                context.CurrentPostion = pos;
                Int32 PlanogramProductId = pos.ParentPlanPosition.ProductId;
                if (PlanogramProductId > 0 && pos.Units > 0)
                {
                    context.ConvertedPlan.Positions.Add(
                        ProductHelper.GetPosition(context, PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.ElementType), positionYOffset));
                }
            }

            List<SplitTextBox> relatedTextBoxes = context.CurrentPlan.TextBoxes.Where(p =>
                p.SplitElementId == context.CurrentElement.Id &&
                p.Type == PlanTexboxType.Preserve)
                   .OrderBy(e => e.XPosition)
                   .OrderBy(p => p.YPosition).ToList();

            output.MerchandisingStrategyX = (relatedTextBoxes.Any())
                ? PlanogramSubComponentXMerchStrategyType.Manual
                : output.MerchandisingStrategyX;

            foreach (SplitTextBox box in relatedTextBoxes)
            {
                // context.CurrentPostion = pos;
                PlanogramAnnotation anno = PlanogramAnnotation.NewPlanogramAnnotation();
                anno.PlanogramSubComponentId = context.CurrentConvertedSubComponent.Id;                
                anno.PlanogramFixtureComponentId = context.CurrentConvertedFixtureComponent.Id;
                anno.PlanogramFixtureItemId = context.CurrentConvertedFixtureItem.Id;
                anno.Text = String.IsNullOrWhiteSpace(box.Description) ? "-" : box.Description;
                anno.Width = (Single)box.Width;
                anno.Height = (Single)box.Height;
                anno.X = (Single)box.XPosition;
                //anno.Y = (Single)box.YPosition;
                anno.Y = 1.0F;
                anno.Z = (Single)box.ZPosition;

                context.ConvertedPlan.Annotations.Add(anno);
            }
            return output;
        }
    }
}
