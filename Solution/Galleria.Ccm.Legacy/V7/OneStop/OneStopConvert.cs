﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Legacy.Model;

namespace Galleria.Ccm.Legacy.V7
{
    public class OneStopConvert : GenericConvert
    {

        public override void UpdateProduct(PlanogramProduct v8Product, PlanPosition uniqueProductPosition, Product currentProduct)
        {
            if (currentProduct != null)
            {
                v8Product.Brand = currentProduct.Text3;
                v8Product.Vendor = currentProduct.Text4;
                if (currentProduct.Number5.HasValue)
                {
                    v8Product.Size = currentProduct.Number5.ToString();
                }
                v8Product.UnitOfMeasure = currentProduct.Text5;
                v8Product.Subcategory = currentProduct.Text13;
                v8Product.DeliveryFrequencyDays = currentProduct.Number1;
                if (currentProduct.Number2.HasValue)
                {
                    v8Product.ShelfLife = (Int16)currentProduct.Number2.Value;
                }
                v8Product.CostPrice = currentProduct.Number3;
                v8Product.SellPrice = currentProduct.Number4;
            }
        }
    }
}
