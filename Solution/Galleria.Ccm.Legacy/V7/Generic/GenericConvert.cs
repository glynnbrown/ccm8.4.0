﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Legacy.Model;

namespace Galleria.Ccm.Legacy.V7
{
    public class GenericConvert
    {
        public virtual String DosKeyField
        {
            get
            {
                return "DOS";
            }
        }
        public virtual String CasePackKeyField
        {
            get
            {
                return "CasePack";
            }
        }

        public virtual PlanogramSubComponentXMerchStrategyType ShelfXMerchStrategy
        {
            get
            {
                return PlanogramSubComponentXMerchStrategyType.Even;
            }
        }

        public virtual Int32 MaxInventoryPriority
        {
            get
            {
                return 5;
            }
        }

        public virtual void CreateBackboard(PlanogramFixture fixture)
        {
            PlanogramFixtureComponent backBoard = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, fixture.Depth, -2894893);
            backBoard.Z -= backBoard.GetPlanogramComponent().Depth;
        }

        public virtual void UpdateProduct(PlanogramProduct v8Product, PlanPosition uniqueProductPosition, Product currentProduct)
        {
            if (currentProduct != null)
            {
                v8Product.Brand = currentProduct.Text1;
                v8Product.Vendor = currentProduct.Text2;
                v8Product.Manufacturer = currentProduct.Text3;
                v8Product.UnitOfMeasure = currentProduct.Text4;
                v8Product.CorporateCode = currentProduct.Text5;
                v8Product.Barcode = currentProduct.Text6;
                Int16 value;
                if (Int16.TryParse(currentProduct.Text7, out value))
                {
                    v8Product.SellPackCount = value;
                }
                v8Product.SellPackDescription = currentProduct.Text8;
                Boolean booleanValue;
                if (Boolean.TryParse(currentProduct.Text7, out booleanValue))
                {
                    v8Product.IsPrivateLabel = booleanValue;
                }
                v8Product.Subcategory = currentProduct.Text10;
                v8Product.CustomerStatus = currentProduct.Text11;
                v8Product.Colour = currentProduct.Text12;
                v8Product.Flavour = currentProduct.Text13;
                v8Product.ShortDescription = currentProduct.Text14;

                v8Product.Size = $"{currentProduct.Number1}";
                v8Product.SellPrice = currentProduct.Number2;
                v8Product.CostPrice = currentProduct.Number3;
                v8Product.ShelfLife = Convert.ToInt16(currentProduct.Number4);
                v8Product.DeliveryFrequencyDays = currentProduct.Number5;
            }
        }

        /// <summary>
        /// Creates performance data from PerformanceValue01-10 position values. Multi-Sited position
        /// performance is summed (needs checking).
        /// </summary>
        /// <param name="context"></param>
        public void SetProductPerformanceData(ConvertContext context)
        {
            context.ConvertedPlan.Performance.ClearAll();

            foreach (var product in context.CurrentPlan.ParentPlan.PlanPositions.GroupBy(p => p.ProductId))
            {
                PlanogramPerformanceData data = PlanogramPerformanceData.NewPlanogramPerformanceData();
                data.PlanogramProductId = -product.Key;
                //add data first so it has a parent
                context.ConvertedPlan.Performance.PerformanceData.Add(data);

                data.P1 = product.Sum(p => p.AverageWeeklySales);
                data.P2 = product.Sum(p => p.AverageWeeklyUnits);
                data.P3 = product.Sum(p => p.AverageWeeklyProfit);
                data.P4 = product.Sum(p => p.PerformanceValue01);
                data.P5 = product.Sum(p => p.PerformanceValue02);
                data.P6 = product.Sum(p => p.PerformanceValue03);
                data.P7 = product.Sum(p => p.PerformanceValue04);
                data.P8 = product.Sum(p => p.PerformanceValue05);
                data.P9 = product.Sum(p => p.PerformanceValue06);
                data.P10 = product.Sum(p => p.PerformanceValue07);
                data.P11 = product.Sum(p => p.PerformanceValue08);
                data.P12 = product.Sum(p => p.PerformanceValue09);
                data.P13 = product.Sum(p => p.PerformanceValue10);
            }
            Byte performanceId = 1;
            PlanogramPerformanceMetric averageWeeklySales = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(performanceId);
            averageWeeklySales.Name = String.Format("Average Weekly Sales", performanceId);
            averageWeeklySales.Type = MetricType.Decimal;
            context.ConvertedPlan.Performance.Metrics.Add(averageWeeklySales);
            performanceId++;

            PlanogramPerformanceMetric averageWeeklyUnits = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(performanceId);
            averageWeeklyUnits.Name = String.Format("Average Weekly Units", performanceId);
            averageWeeklyUnits.Type = MetricType.Decimal;
            context.ConvertedPlan.Performance.Metrics.Add(averageWeeklyUnits);
            averageWeeklyUnits.SpecialType = MetricSpecialType.RegularSalesUnits;
            performanceId++;

            PlanogramPerformanceMetric averageWeeklyProfit = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(performanceId);
            averageWeeklyProfit.Name = String.Format("Average Weekly Profit", performanceId);
            averageWeeklyProfit.Type = MetricType.Decimal;
            context.ConvertedPlan.Performance.Metrics.Add(averageWeeklyProfit);
            performanceId++;

           // Byte regularSalesUnitMetric = 2;
            for (Byte x = performanceId; x < performanceId + 10; x++)
            {
                PlanogramPerformanceMetric p = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(x);
                p.Name = String.Format("P{0}", x - performanceId + 1);
                p.Type = MetricType.Decimal;
                context.ConvertedPlan.Performance.Metrics.Add(p);

                ////Special types must be set after they are added to the plan
                //if (x == regularSalesUnitMetric)
                //{
                //    p.SpecialType = PlanogramMetricSpecialType.RegularSalesUnits;
                //}
            }
        }        
    }

    public class TestConvert : GenericConvert
    {
        public override void CreateBackboard(PlanogramFixture fixture)
        {
            PlanogramFixtureComponent backBoard = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1, -2894893);
            backBoard.Z -= backBoard.GetPlanogramComponent().Depth;
        }
    }
}
