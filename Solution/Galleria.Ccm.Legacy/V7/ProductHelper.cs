﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32609 : L.Ineson
//  Remapped PegProngOffset to PegProngOffsetX
#endregion
#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 1. I have added one more condition to set the value product.IsFrontOnly when is not a tray product type because, according with Steve, V7 does not have the IsFrontOnly option for units, only trays has.
// 2. With M.Brumby's help I have improved the way V7 Cap Rights are converted to V8 main faces.
//      - I made some values to be converted to Int16 instead of Bytes to make the end result closer to the reality even if exist negative numbers. If negative numbers exist they will be set up as they are instead and they will not be cast to a byte.
//      - If there is V7 Cap Rights and broken units to be placed in the V8 main facing then it should be placed the number of units, V7 Wide. TrayUnitsWide is 0 everytime TrayCountWide is equal to 0. 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Legacy.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Legacy.V7
{
    /// <summary>
    /// Helper for creating product and product positon objects
    /// </summary>
    public class ProductHelper
    {
        #region Product
        /// <summary>
        /// Creates a distinct list of products from the list of plan positions as there is no 
        /// product list in V7. Template plan positions are also used so that we retain any
        /// products that have been dropped.
        /// </summary>
        /// <remarks> Its preferable to use the position records rather than tblProduct so that we use the product attributes
        /// from when the plan was actually created. Otherwise this would be alots simpler!</remarks>
        /// <param name="context"></param>
        /// <param name="placedProductsCount"></param>
        /// <param name="allProductsCount"></param>
        public static void CreateProducts(ConvertContext context, out Int32 placedProductsCount, out Int32 allProductsCount)
        {

            SplitPlan splitPlan = context.CurrentPlan;
            IEnumerable<PlanPosition> uniquePositions = splitPlan.ParentPlan.PlanPositions.Distinct(new ProductIdComparer());

            // this is the placed products count (condition excludes preserved space)
            placedProductsCount = uniquePositions.Where(p => p.ProductId > 0).Count();

            List<PlanPosition> uniqueTemplatePositions = splitPlan.ParentPlan.TemplatePlanPositions.Distinct(new ProductIdComparer()).ToList();
            //Remove all positions from the template list that already exist in the plan list.
            //this could be doen using the distinct function but I want to make sure that any plan
            //records take precident.
            uniqueTemplatePositions.RemoveAll(t => uniquePositions.Where(p => p.ProductId == t.ProductId).Count() > 0);

            //union the two lists together to get the full list of all products
            uniquePositions = uniquePositions.Union(uniqueTemplatePositions).ToList();
            // according to Mark this is the all products count (condition excludes preserved space)
            allProductsCount = uniquePositions.Where(p => p.ProductId > 0).Count();
            List<Assortment> assortmentProducts = context.CurrentPlan.ParentPlan.Assortment.ToList();

            Dictionary<Int32, Product> productLookup = ProductList.FetchByProductIds(
                uniquePositions.Select(p => p.ProductId)
                .Union(context.CurrentPlan.ParentPlan.Assortment.Select(p => p.ProductId))
                .Distinct().ToList()
                ).ToDictionary(p => p.Id);

            List<PlanogramProduct> output = new List<PlanogramProduct>(uniquePositions.Count());

            foreach (PlanPosition uniqueProduct in uniquePositions)
            {
                if (uniqueProduct.ProductId <= 0) continue;
                if (splitPlan.Positions.Where(p => p.ProductId == uniqueProduct.ProductId).GroupBy(pg => pg.TrayProduct).Count() > 1)
                {
                    throw new Exception("Message.PlanProduct_UnitTrayError");
                }

                PlanogramProduct product = CreatePlanogramProduct(context, splitPlan, uniqueProduct, productLookup);
                assortmentProducts.RemoveAll(p => p.ProductCode == product.Gtin);
                output.Add(product);
            }

            //There could still be some more products to add if for some reason templateplanposition was not populated!           
            foreach (Assortment row in assortmentProducts)
            {
                PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
                Product currentProduct = null;

                if (!productLookup.TryGetValue(row.ProductId, out currentProduct)) continue;

                UpdatePlanogramProductAttributesFromProduct(product, currentProduct);

                context.Converter.UpdateProduct(product, null, currentProduct);

                output.Add(product);
            }
            context.ConvertedPlan.Products.RaiseListChangedEvents = false;
            context.ConvertedPlan.Products.AddRange(output);
            context.ConvertedPlan.Products.RaiseListChangedEvents = true;
        }

        private static void UpdatePlanogramProductAttributesFromProduct(PlanogramProduct product, Product currentProduct)
        {
            product.Name = currentProduct.Name;
            product.Gtin = currentProduct.Code;

            if (product.Gtin.Length > 14)
            {
                product.Gtin = product.Gtin.Substring(currentProduct.Code.Length - 14, 14);
            }

            product.Id = -currentProduct.Id;

            //CCM products start sqeezed.
            product.Depth = (Single) currentProduct.Depth;
            product.Height = (Single) currentProduct.Height;
            product.Width = (Single) currentProduct.Width;
            product.SqueezeHeight = (Single) currentProduct.SqueezeHeight;
            product.SqueezeWidth = (Single) currentProduct.SqueezeWidth;
            product.SqueezeDepth = (Single) currentProduct.SqueezeDepth;


            product.AlternateDepth = 0;
            product.AlternateHeight = 0;
            product.AlternateWidth = 0;
            product.CanBreakTrayBack = currentProduct.BreakTrayBack > 0;
            product.CanBreakTrayDown = currentProduct.BreaktrayDown > 0;

            product.CanBreakTrayTop = false;
            product.CanBreakTrayUp = currentProduct.BreakTrayUp > 0;
            product.DisplayDepth = 0;
            product.DisplayHeight = 0;
            product.DisplayWidth = 0;
            product.FingerSpaceToTheSide = 0;
            product.ForceBottomCap = false;
            product.ForceMiddleCap = currentProduct.CanMiddleCap;
            product.FrontOverhang = 0;
            product.IsActive = true;
            product.IsFrontOnly = false;
            product.IsPlaceHolderProduct = false; //TODO
            //CCM V7 MaxRightCap is the max number of right TOP caps a position can have.
            product.MaxRightCap = 99;
            product.MaxStack = (Byte) currentProduct.MaxStack;
            product.MaxTopCap = (Byte) currentProduct.MaxCap;
            product.MerchandisingStyle = currentProduct.TrayProduct != 0
                ? PlanogramProductMerchandisingStyle.Tray
                : PlanogramProductMerchandisingStyle.Unit;
            product.MinDeep = (Byte) currentProduct.MinDeep;
            product.MaxDeep = (Byte) currentProduct.MaxDeep;
            product.NestingDepth = currentProduct.NestDepth == null ? 0 : (Single) currentProduct.NestDepth;
            product.NestingHeight = currentProduct.NestHeight == null ? 0 : (Single) currentProduct.NestHeight;
            product.NestingWidth = currentProduct.NestWidth == null ? 0 : (Single) currentProduct.NestWidth;
            product.NumberOfPegHoles = currentProduct.PegX > 0 ? (Byte) 1 : (Byte) 0; //TODO
            product.OrientationType = PlanogramProductOrientationType.Front0;
            product.PegDepth = currentProduct.PegDepth;
            product.PegProngOffsetX = currentProduct.PegProng == null ? 0 : (Single) currentProduct.PegProng; //CHECK THIS
            product.PegX = currentProduct.PegX;
            product.PegX2 = 0;
            product.PegX3 = 0;
            product.PegY = currentProduct.PegY;
            product.PegY2 = 0;
            product.PegY3 = 0;
            product.PointOfPurchaseDepth = 0;
            product.PointOfPurchaseHeight = 0;
            product.PointOfPurchaseWidth = 0;

            product.StatusType = PlanogramProductStatusType.Active;
            product.TrayDeep = (Byte) currentProduct.TrayDeep;
            product.TrayHigh = (Byte) currentProduct.TrayHigh;
            product.TrayWide = (Byte) currentProduct.TrayWide;
            product.TrayThickDepth = currentProduct.TrayThickDepth;
            product.TrayThickHeight = currentProduct.TrayThickHeight;
            product.TrayThickWidth = currentProduct.TrayThickWidth;
            product.TrayWidth = (Single) (currentProduct.TrayWidth);
            product.TrayHeight = (Single) (currentProduct.TrayHeight);
            product.TrayDepth = (Single) (currentProduct.TrayDepth);
            product.TrayPackUnits = (Int16) (currentProduct.TraypackUnits);
            product.CasePackUnits = (Int16) currentProduct.CasepackUnits;


            SetProductCustomAttributes(product, currentProduct);
        }

        private static PlanogramProduct CreatePlanogramProduct(ConvertContext context, SplitPlan splitPlan,
            PlanPosition uniqueProduct, Dictionary<Int32, Product> productLookup)
        {
            Product currentProduct = null;
            PlanogramProduct product = CreatePlanogramProductFromPositionDetails(splitPlan, uniqueProduct, productLookup, ref currentProduct);

            SetProductCustomAttributes(product, currentProduct);

            context.Converter.UpdateProduct(product, uniqueProduct, currentProduct);
            return product;
        }

        private static PlanogramProduct CreatePlanogramProductFromPositionDetails(SplitPlan splitPlan,
            PlanPosition uniqueProduct, Dictionary<Int32, Product> productLookup, ref Product currentProduct)
        {
            PlanElement firstElement = splitPlan.ParentPlan.PlanElements.FirstOrDefault(e => e.Id == uniqueProduct.PlanElementId);

            PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
            product.Gtin = uniqueProduct.ProductId.ToString();

            if (productLookup.TryGetValue(uniqueProduct.ProductId, out currentProduct))
            {
                product.Name = currentProduct.Name;
                product.Gtin = currentProduct.Code;
            }

            product.Gtin = (product.Gtin.Length > 14) ? product.Gtin.Substring(product.Gtin.Length - 14, 14) : product.Gtin;
            product.Id = -uniqueProduct.ProductId;

            //CCM products start sqeezed.
            product.Depth = (Single) uniqueProduct.ProductDepth;
            product.Height = (Single) uniqueProduct.ProductHeight;
            product.Width = (Single) uniqueProduct.ProductWidth;
            product.SqueezeHeight = (Single) uniqueProduct.SqueezeHeight;
            product.SqueezeWidth = (Single) uniqueProduct.SqueezeWidth;
            product.SqueezeDepth = (Single) uniqueProduct.SqueezeDepth;


            product.AlternateDepth = 0;
            product.AlternateHeight = 0;
            product.AlternateWidth = 0;
            product.CanBreakTrayBack = uniqueProduct.BreakTrayBack > 0;
            product.CanBreakTrayDown = uniqueProduct.BreakTrayDown > 0;
            //A top capped V7 tray position is top capped as units which is break tray top in V8
            product.CanBreakTrayTop = uniqueProduct.BreakTrayTop > 0 ||
                                      (uniqueProduct.CapTopHigh > 0 && uniqueProduct.TrayCountHigh > 0);
            product.CanBreakTrayUp = uniqueProduct.BreakTrayUp > 0;
            product.CasePackUnits = (Int16) uniqueProduct.CasePack;
            product.DisplayDepth = 0;
            product.DisplayHeight = 0;
            product.DisplayWidth = 0;
            product.FingerSpaceToTheSide = 0;
            product.ForceBottomCap = false;
            product.ForceMiddleCap = uniqueProduct.MiddleCapping > 0;
            product.FrontOverhang = 0;
            product.IsActive = true;
            product.IsFrontOnly = uniqueProduct.TrayProduct != 0 ? uniqueProduct.FrontOnly != 0 : false;
            product.IsPlaceHolderProduct = false; //TODO
            product.IsFrontOnly = false;
            //CCM V7 MaxRightCap is the max number of right TOP caps a position can have.
            product.MaxRightCap = 99;
            product.MaxStack = (Byte) uniqueProduct.MaxStack;
            product.MaxTopCap = (Byte) uniqueProduct.MaxCap;
            product.MerchandisingStyle = uniqueProduct.TrayProduct != 0
                ? PlanogramProductMerchandisingStyle.Tray
                : PlanogramProductMerchandisingStyle.Unit;
            product.MinDeep = (Byte) uniqueProduct.MinDeep;
            product.MaxDeep = (Byte) uniqueProduct.MaxDeep;
            product.NestingDepth = uniqueProduct.NestDepth == null ? 0 : (Single) uniqueProduct.NestDepth;
            product.NestingHeight = uniqueProduct.NestHeight == null ? 0 : (Single) uniqueProduct.NestHeight;
            product.NestingWidth = uniqueProduct.NestWidth == null ? 0 : (Single) uniqueProduct.NestWidth;
            product.NumberOfPegHoles = uniqueProduct.PegX > 0 ? (Byte) 1 : (Byte) 0; //TODO
            product.OrientationType = PlanogramProductOrientationType.Front0;
            product.PegDepth = uniqueProduct.PegDepth;
            product.PegProngOffsetX = uniqueProduct.PegProng == null ? 0 : (Single) uniqueProduct.PegProng; //CHECK THIS
            product.PegX = uniqueProduct.PegX;
            product.PegX2 = 0;
            product.PegX3 = 0;
            product.PegY = uniqueProduct.PegY;
            product.PegY2 = 0;
            product.PegY3 = 0;
            product.PointOfPurchaseDepth = 0;
            product.PointOfPurchaseHeight = 0;
            product.PointOfPurchaseWidth = 0;

            product.StatusType = PlanogramProductStatusType.Active;
            product.TrayDeep = (Byte) uniqueProduct.TrayDeep;
            product.TrayHigh = (Byte) uniqueProduct.TrayHigh;
            product.TrayWide = (Byte) uniqueProduct.TrayWide;
            product.TrayThickDepth = uniqueProduct.TrayThickDepth;
            product.TrayThickHeight = uniqueProduct.TrayThickHeight;
            product.TrayThickWidth = uniqueProduct.TrayThickWidth;
            product.TrayWidth = (Single) (uniqueProduct.TrayWidth);
            product.TrayHeight = (Single) (uniqueProduct.TrayHeight);
            product.TrayDepth = (Single) (uniqueProduct.TrayDepth);
            product.TrayPackUnits = (Int16) (uniqueProduct.TrayUnits);
            product.CasePackUnits = (Int16) uniqueProduct.CasePack;

            if (firstElement != null)
            {
                product.FingerSpaceAbove = (Single) firstElement.FingerSpace;
            }
            return product;
        }

        private static void SetProductCustomAttributes(PlanogramProduct v8Product, Product currentProduct)
        {
            if (currentProduct != null)
            {
                v8Product.CustomAttributes.Text1 = currentProduct.Text1;
                v8Product.CustomAttributes.Text2 = currentProduct.Text2;
                v8Product.CustomAttributes.Text3 = currentProduct.Text3;
                v8Product.CustomAttributes.Text4 = currentProduct.Text4;
                v8Product.CustomAttributes.Text5 = currentProduct.Text5;
                v8Product.CustomAttributes.Text6 = currentProduct.Text6;
                v8Product.CustomAttributes.Text7 = currentProduct.Text7;
                v8Product.CustomAttributes.Text8 = currentProduct.Text8;
                v8Product.CustomAttributes.Text9 = currentProduct.Text9;
                v8Product.CustomAttributes.Text10 = currentProduct.Text10;
                v8Product.CustomAttributes.Text11 = currentProduct.Text11;
                v8Product.CustomAttributes.Text12 = currentProduct.Text12;
                v8Product.CustomAttributes.Text13 = currentProduct.Text13;
                v8Product.CustomAttributes.Text14 = currentProduct.Text14;
                v8Product.CustomAttributes.Text15 = currentProduct.Text15;
                v8Product.CustomAttributes.Text16 = currentProduct.Text16;
                v8Product.CustomAttributes.Text17 = currentProduct.Text17;
                v8Product.CustomAttributes.Text18 = currentProduct.Text18;
                v8Product.CustomAttributes.Text19 = currentProduct.Text19;
                v8Product.CustomAttributes.Text20 = currentProduct.Text20;

                if (currentProduct.Number1.HasValue) v8Product.CustomAttributes.Value1 = currentProduct.Number1.Value;
                if (currentProduct.Number2.HasValue) v8Product.CustomAttributes.Value2 = currentProduct.Number2.Value;
                if (currentProduct.Number3.HasValue) v8Product.CustomAttributes.Value3 = currentProduct.Number3.Value;
                if (currentProduct.Number4.HasValue) v8Product.CustomAttributes.Value4 = currentProduct.Number4.Value;
                if (currentProduct.Number5.HasValue) v8Product.CustomAttributes.Value5 = currentProduct.Number5.Value;
                if (currentProduct.Number6.HasValue) v8Product.CustomAttributes.Value6 = currentProduct.Number6.Value;
                if (currentProduct.Number7.HasValue) v8Product.CustomAttributes.Value7 = currentProduct.Number7.Value;
                if (currentProduct.Number8.HasValue) v8Product.CustomAttributes.Value8 = currentProduct.Number8.Value;
                if (currentProduct.Number9.HasValue) v8Product.CustomAttributes.Value9 = currentProduct.Number9.Value;
                if (currentProduct.Number10.HasValue) v8Product.CustomAttributes.Value10 = currentProduct.Number10.Value;
                if (currentProduct.Number11.HasValue) v8Product.CustomAttributes.Value11 = currentProduct.Number11.Value;
                if (currentProduct.Number12.HasValue) v8Product.CustomAttributes.Value12 = currentProduct.Number12.Value;
                if (currentProduct.Number13.HasValue) v8Product.CustomAttributes.Value13 = currentProduct.Number13.Value;
                if (currentProduct.Number14.HasValue) v8Product.CustomAttributes.Value14 = currentProduct.Number14.Value;
                if (currentProduct.Number15.HasValue) v8Product.CustomAttributes.Value15 = currentProduct.Number15.Value;
                if (currentProduct.Number16.HasValue) v8Product.CustomAttributes.Value16 = currentProduct.Number16.Value;
                if (currentProduct.Number17.HasValue) v8Product.CustomAttributes.Value17 = currentProduct.Number17.Value;
                if (currentProduct.Number18.HasValue) v8Product.CustomAttributes.Value18 = currentProduct.Number18.Value;
                if (currentProduct.Number19.HasValue) v8Product.CustomAttributes.Value19 = currentProduct.Number19.Value;
                if (currentProduct.Number20.HasValue) v8Product.CustomAttributes.Value20 = currentProduct.Number20.Value;

            }
        }

        /// <summary>
        /// Product Id comparer for distinct products from positions.
        /// </summary>
        private class ProductIdComparer : IEqualityComparer<PlanPosition>
        {
            public bool Equals(PlanPosition x, PlanPosition y)
            {
                return x.ProductId == y.ProductId;
            }

            public int GetHashCode(PlanPosition obj)
            {
                return obj.ProductId.GetHashCode();
            }
        }
        #endregion

        #region Position
        /// <summary>
        /// Gets a PlanogramPositon object to represent the CurrentPosition in the context
        /// </summary>
        /// <param name="context"></param>
        /// <param name="elementType"></param>
        /// <param name="yOffset"></param>
        /// <returns></returns>
        public static PlanogramPosition GetPosition(ConvertContext context, PlanElementType elementType, Single yOffset)
        {
            PlanogramPosition output = PlanogramPosition.NewPlanogramPosition();

            output.PlanogramSubComponentId = context.CurrentConvertedSubComponent.Id;
            output.PlanogramProductId = -context.CurrentPostion.ProductId;
            output.PlanogramFixtureComponentId = context.CurrentConvertedFixtureComponent.Id;
            output.PlanogramFixtureItemId = context.CurrentConvertedFixtureItem.Id;

            //output.BaySequenceNumber = PlanHelper.CurrentFixtureItemId;
            //output.ElementSequenceNumber = PlanHelper.CurrentElementSequence;
            output.PositionSequenceNumber = PlanHelper.GetNextPositionSequence();

            //output = PlanPosition.ConvertCCMIntColourToGFSIntColour(Colour);

            output.TotalUnits = context.CurrentPostion.Units;
            if (context.CurrentPostion.TrayProduct == 0)
            {
                SetUnitFacings(context, output);
            }
            else
            {
                SetTrayFacings(context, output);
            }
            
            //Set XYZ positions to GFS XYZ
            switch (elementType)
            {
                case PlanElementType.Shelf:
                    output.X = context.CurrentPostion.XPosition;
                    output.Y = context.CurrentPostion.YPosition - yOffset;
                    output.Z = 0;
                    break;
                case PlanElementType.Pallet:
                    output.X = context.CurrentPostion.XPosition;
                    output.Y = context.CurrentPostion.YPosition - yOffset;
                    output.Z = 0;
                    break;
                case PlanElementType.Bar:
                    output.X = context.CurrentPostion.XPosition;
                    output.Y = (Single)(context.CurrentPostion.YPosition - context.CurrentPostion.ProductHeight - yOffset);
                    output.Z = (Single)(context.CurrentPostion.ProductDepth * context.CurrentPostion.Deep);
                    break;
                case PlanElementType.Peg:
                    output.X = context.CurrentPostion.XPosition;
                    output.Y = (Single)(context.CurrentPostion.YPosition - context.CurrentPostion.ProductHeight - yOffset);
                    output.Z = (Single)(context.CurrentPostion.ProductDepth * context.CurrentPostion.Deep);
                    break;
                case PlanElementType.Chest:
                    output.X = context.CurrentPostion.XPosition;
                    output.Z = -context.CurrentPostion.YPosition;
                    output.Y = yOffset;
                    break;
            }

            return output;
        }

        private static PlanogramPositionOrientationType GetFrontOrientationType(Int32 orientationId)
        {
            switch (orientationId)
            {
                case 0: //Front
                    return PlanogramPositionOrientationType.Front0;
                case 1: //Front 90
                    return PlanogramPositionOrientationType.Front90;
                case 2: //Right 0
                    return PlanogramPositionOrientationType.Right0;
                case 3: //Right 90
                    return PlanogramPositionOrientationType.Right90;
                case 4: //Top 0
                    return PlanogramPositionOrientationType.Top0;
                case 5: //Top 90
                    return PlanogramPositionOrientationType.Top90;
            }

            return PlanogramPositionOrientationType.Front0;
        }
        private static PlanogramPositionOrientationType GetRightOrientationType(Int32 orientationId)
        {
            switch (orientationId)
            {
                case 0: //Front
                    return PlanogramPositionOrientationType.Right0;
                case 1: //Front 90
                    return PlanogramPositionOrientationType.Top90;
                case 2: //Right 0
                    return PlanogramPositionOrientationType.Front0;
                case 3: //Right 90
                    return PlanogramPositionOrientationType.Top0;
                case 4: //Top 0
                    return PlanogramPositionOrientationType.Right90;
                case 5: //Top 90
                    return PlanogramPositionOrientationType.Front90;
            }

            return PlanogramPositionOrientationType.Right0;
        }
        private static PlanogramPositionOrientationType GetTopOrientationType(Int32 orientationId)
        {
            switch (orientationId)
            {
                case 0: //Front
                    return PlanogramPositionOrientationType.Top0;
                case 1: //Front 90
                    return PlanogramPositionOrientationType.Right90;
                case 2: //Right 0
                    return PlanogramPositionOrientationType.Top90;
                case 3: //Right 90
                    return PlanogramPositionOrientationType.Front90;
                case 4: //Top 0
                    return PlanogramPositionOrientationType.Front0;
                case 5: //Top 90
                    return PlanogramPositionOrientationType.Right0;
            }

            return PlanogramPositionOrientationType.Top0;
        }

        /// <summary>
        /// Set all the facing values for the supplied position with the assumption
        /// that the position is not a tray position
        /// </summary>
        /// <param name="position"></param>
        private static void SetUnitFacings(ConvertContext context, PlanogramPosition position)
        {
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;

            position.OrientationType = GetFrontOrientationType(context.CurrentPostion.OrientationId);

            //Note as this is not a tray product the High/Wide/Deep
            //values will not include the break tray values so can
            //be used directly as FrontHigh/Wide/Deep
            position.FacingsHigh = (Byte)(context.CurrentPostion.High + context.CurrentPostion.NestHigh);
            position.FacingsDeep = (Byte)(context.CurrentPostion.Deep + context.CurrentPostion.NestDeep);
            position.FacingsWide = (Byte)(context.CurrentPostion.Wide + context.CurrentPostion.NestWide);

            //Right caps
            if (context.CurrentPostion.CapRightWide > 0 && context.CurrentPostion.CapRightDeep > 0)
            {
                position.OrientationTypeX = GetRightOrientationType(context.CurrentPostion.OrientationId);
                position.FacingsXHigh = (Byte)context.CurrentPostion.High;
                position.FacingsXWide = (Byte)context.CurrentPostion.CapRightWide;
                position.FacingsXDeep = (Byte)context.CurrentPostion.CapRightDeep;
            }

            //Top caps.
            if (context.CurrentPostion.CapTopDeep > 0 && context.CurrentPostion.CapTopHigh > 0)
            {
                position.OrientationTypeY = GetTopOrientationType(context.CurrentPostion.OrientationId);
                position.FacingsYDeep = (Byte)context.CurrentPostion.CapTopDeep;
                position.FacingsYHigh = (Byte)context.CurrentPostion.CapTopHigh;
                position.FacingsYWide = (Byte)context.CurrentPostion.Wide;
            }

            //Right Top caps
            if (context.CurrentPostion.CapRightCapTopHigh > 0 && context.CurrentPostion.CapRightCapTopDeep > 0)
            {
                //TODO
                //position.FrontTopCapRightHigh = (Byte)context.CurrentPostion.CapRightCapTopHigh;
                //position.FrontTopCapRightWide = (Byte)context.CurrentPostion.CapRightWide;
                //position.FrontTopCapRightDeep = (Byte)context.CurrentPostion.CapRightCapTopDeep;
            }

            //TODO
            ////Get the max number of units high
            //position.UnitsHigh = (Int16)Math.Max(position.FacingsHigh + position.FacingsYHigh, position.FrontRightHigh + position.FrontTopCapRightHigh);

            ////Get the total number of units wide
            //position.UnitsWide = (Int16)(position.FrontWide + position.FrontRightWide);

            ////Get the max number of units deep
            //position.UnitsDeep = (Int16)Math.Max(position.FrontDeep + position.TrayBreakBackDeep, position.FrontRightDeep);
            //position.UnitsDeep = (Int16)Math.Max(position.FrontTopCapDeep, position.UnitsDeep);
            //position.UnitsDeep = (Int16)Math.Max(position.FrontTopCapRightDeep, position.UnitsDeep);
        }
        /// <summary>
        /// Set all the facing values for the supplied position with the assumption
        /// that the position is a tray position
        /// </summary>
        /// <param name="position"></param>
        private static void SetTrayFacings(ConvertContext context, PlanogramPosition position)
        {
            //Note that the units in a tray have not been oriented so we
            //have to orient them before using htem in any calculations.

            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;

            //Set front trays
            if (context.CurrentPostion.TrayCountWide > 0)
            {
                position.OrientationType = GetFrontOrientationType(context.CurrentPostion.OrientationId);
                position.FacingsDeep = (Int16)context.CurrentPostion.TrayCountDeep;
                position.FacingsHigh = (Int16)context.CurrentPostion.TrayCountHigh;
                position.FacingsWide = (Int16)context.CurrentPostion.TrayCountWide;              
            }
            else
            {
                position.FacingsDeep = 0;
                position.FacingsHigh = 0;
                position.FacingsWide = 0;
            }

            Int32 trayUnitsWide = (position.FacingsWide * PlanPosition.OrientDimension<Int32>(PlanPosition.Dimension.Width, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));

            //Set right caps
            if (context.CurrentPostion.CapRightWideTrayCount > 0 && context.CurrentPostion.CapRightDeepTrayCount > 0)
            {
                if (context.CurrentPostion.TrayCountWide > 0)
                {
                    position.OrientationTypeX = GetRightOrientationType(context.CurrentPostion.OrientationId);
                    position.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Tray;

                    //CCM has no CapRightHighTrayCount so TrayCountHigh is used.
                    position.FacingsXHigh = (Int16)context.CurrentPostion.TrayCountHigh;
                    position.FacingsXWide = (Int16)context.CurrentPostion.CapRightWideTrayCount;
                    position.FacingsXDeep = (Int16)context.CurrentPostion.CapRightDeepTrayCount;
                }
                else
                {
                    position.OrientationType = GetRightOrientationType(context.CurrentPostion.OrientationId);
                    //CCM has no CapRightHighTrayCount so TrayCountHigh is used.
                    position.FacingsHigh = (Int16)context.CurrentPostion.TrayCountHigh;
                    position.FacingsWide = (Int16)context.CurrentPostion.CapRightWideTrayCount;
                    position.FacingsDeep = (Int16)context.CurrentPostion.CapRightDeepTrayCount;

                    trayUnitsWide = 0;

                }
                //CCM has no CapRightHighTrayCount so TrayCountHigh is used.
                //position.FrontRightHigh = (Byte)(context.CurrentPostion.TrayCountHigh * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Height, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, Tcontext.CurrentPostion.rayDeep, context.CurrentPostion.OrientationId));
                //position.FrontRightWide = (Byte)(context.CurrentPostion.CapRightWideTrayCount * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Depth, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                //position.FrontRightDeep = (Byte)(context.CurrentPostion.CapRightDeepTrayCount * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Width, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
            }

            //Tray has broken down
            if (position.FacingsWide == 0 && context.CurrentPostion.CapRightWideTrayCount == 0)
            {
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
                position.FacingsHigh = (Int16)(context.CurrentPostion.High);
                position.FacingsWide = (Int16)(context.CurrentPostion.Wide);
                position.FacingsDeep = (Int16)(context.CurrentPostion.Deep);
            }
            else
            {
                //Tray has broken deep (Currently only if there is a front tray)
                if (position.FacingsDeep > 0 && context.CurrentPostion.Deep > (position.FacingsDeep * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Depth, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId)))
                {
                    position.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;

                    if (trayUnitsWide > 0)
                    {
                        position.FacingsZDeep = (Int16)(context.CurrentPostion.Deep - (position.FacingsDeep * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Depth, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId)));
                        position.FacingsZHigh = (Int16)(position.FacingsHigh * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Height, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                        position.FacingsZWide = (Int16)(position.FacingsWide * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Width, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                    }else
                    {       
                        //tray position was right capped so use right capped unit counts                 
                        position.FacingsZDeep = (Int16)(context.CurrentPostion.Deep - (position.FacingsDeep * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Width, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId)));
                        position.FacingsZHigh = (Int16)(position.FacingsHigh * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Height, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                        position.FacingsZWide = (Int16)(position.FacingsWide * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Depth, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                    }

                    CalculateBackBreakWidth(context, position);
                    
                    CalculateBackBreakHeight(context, position);
                }

                //Tray has broken up

                //Can't mix caps
                if (position.FacingsXWide == 0)
                {
                    //Wide in CCM includes Front units and break up units, but no right cap units.
                    if (context.CurrentPostion.Wide > trayUnitsWide)
                    {
                        position.OrientationTypeX = GetFrontOrientationType(context.CurrentPostion.OrientationId);
                        position.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                        position.FacingsXDeep = (Int16)(context.CurrentPostion.Deep);
                        position.FacingsXHigh = (Int16)(context.CurrentPostion.High);
                        position.FacingsXWide = (Int16)(context.CurrentPostion.Wide - trayUnitsWide);
                    }
                }
                Int32 unitsHigh = (position.FacingsHigh * PlanPosition.OrientDimension<Int32>(PlanPosition.Dimension.Height, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                //unitsHigh = Math.Max(unitsHigh, (position.TraysRightHigh * PlanPosition.OrientDimension<Int32>(PlanPosition.Dimension.Height, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId)));
                
                //Tray has broken top
                if (position.FacingsHigh > 0 && context.CurrentPostion.High > (position.FacingsHigh * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Height, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId)))
                {
                    position.OrientationTypeY = position.OrientationType;
                    position.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                    //get the number of units broken top
                    unitsHigh = context.CurrentPostion.High - unitsHigh;

                    //set the break tray top values
                    position.FacingsYHigh = (Byte)unitsHigh;
                    if (trayUnitsWide > 0)
                    {
                        position.FacingsYDeep =
                            (Byte)
                                (position.FacingsDeep*
                                 PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Depth,
                                     context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide,
                                     context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));

                        position.FacingsYWide =
                            (Byte)
                                (position.FacingsWide*
                                 PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Width,
                                     context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide,
                                     context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                    }
                    else
                    {
                        position.FacingsYDeep =
                            (Byte)
                                (position.FacingsDeep *
                                 PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Width,
                                     context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide,
                                     context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));

                        position.FacingsYWide =
                            (Byte)
                                (position.FacingsWide *
                                 PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Depth,
                                     context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide,
                                     context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                    }
                    
                    CalculateTopBreakWidth(context, position);

                    CalculateTopBreakDepth(context, position);

                    //Cant do this in V8.. might have to make a new position
                    ////Infer the break tray top right values
                    //if (position.TraysRightWide > 0)
                    //{
                    //    position.TrayBreakTopRightHigh = (Byte)unitsHigh;
                    //    position.TrayBreakTopRightDeep = (Byte)(position.TraysRightDeep * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Width, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                    //    position.TrayBreakTopRightWide = (Byte)(position.TraysRightWide * PlanPosition.OrientDimension<Single>(PlanPosition.Dimension.Depth, context.CurrentPostion.TrayHigh, context.CurrentPostion.TrayWide, context.CurrentPostion.TrayDeep, context.CurrentPostion.OrientationId));
                    //}
                }
            }

            //Top caps. (Only if we haven't broken top)
            if (position.FacingsYHigh == 0 && context.CurrentPostion.CapTopDeep > 0 && context.CurrentPostion.CapTopHigh > 0)
            {
                //CCM7 Top capped trays are actually units
                position.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                position.OrientationTypeY = GetTopOrientationType(context.CurrentPostion.OrientationId);
                position.FacingsYDeep = (Byte)context.CurrentPostion.CapTopDeep;
                position.FacingsYHigh = (Byte)context.CurrentPostion.CapTopHigh;
                position.FacingsYWide = (Byte)position.FacingsWide;
            }

            ////Right Top caps
            //if (CapRightCapTopHigh > 0 && CapRightCapTopDeep > 0)
            //{
            //    position.FrontTopCapRightHigh = (Byte)CapRightCapTopHigh;
            //    position.FrontTopCapRightWide = (Byte)CapRightWide;
            //    position.FrontTopCapRightDeep = (Byte)CapRightCapTopDeep;
            //}

            ////Get the max number of units high
            //position.UnitsHigh = (Int16)Math.Max(position.FrontHigh + position.FrontTopCapHigh + position.TrayBreakTopHigh, position.FrontRightHigh + position.FrontTopCapRightHigh + position.TrayBreakTopHigh);

            ////Get the total number of units wide
            //position.UnitsWide = (Int16)(position.FrontWide + position.FrontRightWide + position.TrayBreakUpWide);

            ////Get the max number of units deep
            //position.UnitsDeep = (Int16)Math.Max(position.FrontDeep + position.TrayBreakBackDeep, position.FrontRightDeep);
            //position.UnitsDeep = (Int16)Math.Max(position.FrontTopCapDeep, position.UnitsDeep);
            //position.UnitsDeep = (Int16)Math.Max(position.FrontTopCapRightDeep, position.UnitsDeep);
            //position.UnitsDeep = (Int16)Math.Max(position.TrayBreakUpDeep, position.UnitsDeep);
        }

        private static void CalculateBackBreakHeight(ConvertContext context, PlanogramPosition position)
        {
// Work out if the back break component can fit in behind the front component
            Double totalBreakHeight = CalculateTotalBreakBackHeight(context, position);
            Double totalTrayHeight = CalculateTotalTrayHeight(context, position);

            // High & Wide
            while (totalBreakHeight >= totalTrayHeight && position.FacingsZHigh > 0)
            {
                position.FacingsZHigh--;
                totalBreakHeight = CalculateTotalBreakBackHeight(context, position);
            }
        }

        private static void CalculateTopBreakDepth(ConvertContext context, PlanogramPosition position)
        {
// Calculate the Depth
            Double totalBreakDepth = CalculateTotalBreakTopDepth(context, position);
            Double totalTrayDepth = CalculateTotalTrayDepth(context, position);

            while (totalBreakDepth >= totalTrayDepth && position.FacingsYDeep > 0)
            {
                position.FacingsYDeep--;
                totalBreakDepth = CalculateTotalBreakTopDepth(context, position);
            }
        }

        private static void CalculateBackBreakWidth(ConvertContext context, PlanogramPosition position)
        {
            Double totalBreakWidth = CalculateTotalBreakBackWidth(context, position);
            Double totalTrayWidth = CalculateTotalTrayWidth(context, position);

            // Calculate the width
            while (totalBreakWidth >= totalTrayWidth && position.FacingsZWide > 0)
            {
                position.FacingsZWide--;
                totalBreakWidth = CalculateTotalBreakBackWidth(context, position);
            }
        }

        private static void CalculateTopBreakWidth(ConvertContext context, PlanogramPosition position)
        {
            Double totalBreakWidth = CalculateTotalBreakTopWidth(context, position);
            Double totalTrayWidth = CalculateTotalTrayWidth(context, position);

            // Calculate the width
            while (totalBreakWidth >= totalTrayWidth && position.FacingsYWide > 0)
            {
                position.FacingsYWide--;
                totalBreakWidth = CalculateTotalBreakTopWidth(context, position);
            }
        }

        private static Double CalculateTotalTrayWidth(ConvertContext context, PlanogramPosition position)
        {
            return position.FacingsWide * PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Width, context.CurrentPostion.TrayHeight, context.CurrentPostion.TrayWidth, context.CurrentPostion.TrayDepth, context.CurrentPostion.OrientationId);
        }

        private static Double CalculateTotalBreakBackWidth(ConvertContext context, PlanogramPosition position)
        {
            return position.FacingsZWide * PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Width, context.CurrentPostion.ProductHeight, context.CurrentPostion.ProductWidth, context.CurrentPostion.ProductDepth, context.CurrentPostion.OrientationId);
        }

        private static Double CalculateTotalBreakTopWidth(ConvertContext context, PlanogramPosition position)
        {
            return position.FacingsYWide * PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Width, context.CurrentPostion.ProductHeight, context.CurrentPostion.ProductWidth, context.CurrentPostion.ProductDepth, context.CurrentPostion.OrientationId);
        }

        private static Double CalculateTotalTrayHeight(ConvertContext context, PlanogramPosition position)
        {
            return position.FacingsHigh * PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Height, context.CurrentPostion.TrayHeight, context.CurrentPostion.TrayWidth, context.CurrentPostion.TrayDepth, context.CurrentPostion.OrientationId);
        }

        private static Double CalculateTotalBreakBackHeight(ConvertContext context, PlanogramPosition position)
        {
            return position.FacingsZHigh * PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Height, context.CurrentPostion.ProductHeight, context.CurrentPostion.ProductWidth, context.CurrentPostion.ProductDepth, context.CurrentPostion.OrientationId);
        }

        private static Double CalculateTotalTrayDepth(ConvertContext context, PlanogramPosition position)
        {
            return position.FacingsDeep * PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Depth, context.CurrentPostion.TrayHeight, context.CurrentPostion.TrayWidth, context.CurrentPostion.TrayDepth, context.CurrentPostion.OrientationId);
        }

        private static Double CalculateTotalBreakTopDepth(ConvertContext context, PlanogramPosition position)
        {
            return position.FacingsYDeep * PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Depth, context.CurrentPostion.ProductHeight, context.CurrentPostion.ProductWidth, context.CurrentPostion.ProductDepth, context.CurrentPostion.OrientationId);
        }

        /// <summary>
        /// Performs any post processing on postions that might need to be done.
        /// </summary>
        /// <param name="context"></param>
        public static void PostProcessPositons(ConvertContext context)
        {
            //Correct position Z values. This requires positon details which needs the
            //whole plan structure finished to work.
            foreach (PlanogramPosition position in context.ConvertedPlan.Positions)
            {
                PlanogramPositionDetails details = position.GetPositionDetails();

                Single newZ = ((position.GetPlanogramSubComponent().Depth - position.Z) - details.MainTotalSize.Depth);

                position.Z = newZ;
            }
        }
        #endregion

        #region Assortment

        public static void CreateAssortment(ConvertContext context)
        {
            context.ConvertedPlan.Assortment.Name = "CCM v7";

            foreach (Assortment row in context.CurrentPlan.ParentPlan.Assortment)
            {
                PlanogramAssortmentProduct astProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(row.ProductCode, row.ProductName);
                context.ConvertedPlan.Assortment.Products.Add(astProduct);
                astProduct.IsRanged = row.Recommended;
                astProduct.Units = Convert.ToInt16(row.RecomendedUnits);
                
                astProduct.Facings = Convert.ToByte(row.RecomendedFacings);
                astProduct.Rank = Convert.ToInt16(row.GlobalRank);
                astProduct.Segmentation = row.Segmentation;
                
                switch (row.Rule1Type)
                {
                    case ProductRuleType.Exact:
                        astProduct.ExactListFacings = Convert.ToByte(row.RuleValue1);
                        break;
                    case ProductRuleType.ExactZero:
                        astProduct.ExactListFacings = 0;
                        break;
                    case ProductRuleType.MaximumList:
                        astProduct.MaxListFacings = Convert.ToByte(row.RuleValue1);
                        break;
                    case ProductRuleType.MinimumDeList:
                        astProduct.MinListFacings = Convert.ToByte(row.RuleValue1);
                        break;
                    case ProductRuleType.PreserveList:
                        astProduct.PreserveListFacings = Convert.ToByte(row.RuleValue1);
                        break;
                }
                switch (row.Rule2Type)
                {
                    case ProductRuleType.Exact:
                        astProduct.ExactListFacings = Convert.ToByte(row.RuleValue2);
                        break;
                    case ProductRuleType.ExactZero:
                        astProduct.ExactListFacings = 0;
                        break;
                    case ProductRuleType.MaximumList:
                        astProduct.MaxListFacings = Convert.ToByte(row.RuleValue2);
                        break;
                    case ProductRuleType.MinimumDeList:
                        astProduct.MinListFacings = Convert.ToByte(row.RuleValue2);
                        break;
                    case ProductRuleType.PreserveList:
                        astProduct.PreserveListFacings = Convert.ToByte(row.RuleValue2);
                        break;
                }
            }

        }

        #endregion
    }
}
