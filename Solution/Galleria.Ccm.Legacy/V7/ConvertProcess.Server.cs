﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
#endregion
#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 4. I have changed the converted planogram name file to be more similiar with GFS and CCM names.
//   - Sometimes the names may not match totally because some Chars are invalid and can't be a file name. The other reason is due to the fact that the file name may be too long so we need to cut it.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Processes;
using Galleria.Ccm.Legacy.Model;
using System.Diagnostics;
using System.Threading.Tasks;
using Galleria.Ccm.Legacy.Dal.V7.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using System.IO;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.V7
{
    public delegate void ProcessStateHandler(PlanType planType, Int32 currentValue, Int32 maxValue, Int32 totalCurrentValue, Int32 totalMaxValue);

    public partial class ConvertProcess
    {
        private const String PlanogramFileExtension = ".pog";

        private const String ClusterPlanogramFileNameType = "CP";
        private const String StorePlanogramFilenameType = "SP";
        private const String MerchandisingPlanogramFilenameType = "TP";
        private const String ManualPlanogramFilenameType = "MP";
        private const String PlanogramFilenameType = "P";

        private Int32 _totalPlanCount = 0;
        private Int32 _totalPlansFinishedCount = 0;

        public event ProcessStateHandler OnProcessUpdate;

        #region Execute
        protected override void OnExecute()
        {
            Helpers.SetupDal(this.ServerName, this.DatabaseName);
            PlanProcess planProcess = new PlanProcess(CategoryReviewIdsCSV);
            PlanScripts.dbVersion = planProcess.GetDbVersion();
            List<Int32> merchPlanIds = planProcess.FetchMerchandisingPlanIds();
            List<Int32> storePlanIds = planProcess.FetchStorePlanIds();
            List<Int32> clusterPlanIds = planProcess.FetchClusterPlanIds();
            _totalPlansFinishedCount = 0;
            _totalPlanCount = merchPlanIds.Count + storePlanIds.Count + clusterPlanIds.Count;
            //planProcess.FixOriginalTemplateElementIds();

            //SplitPlan split = new SplitPlan(Plan.GetPlanById(62, PlanType.Store));
            //Planogram convertedPlan = Convert(split);
            //Package package = Package.NewPackage(1, PackageLockType.System);
            //package.Planograms.Add(convertedPlan);
            //package.SaveAs(1, @"C:\Users\usr040\Documents\TEST\" + 62.ToString() + ".pog");
            //package.Dispose();

            //SplitPlan split = new SplitPlan(Plan.GetPlanById(41, PlanType.Cluster));
            //Planogram convertedPlan = Convert(split);
            //Package package = Package.NewPackage(1, PackageLockType.System);
            //package.Planograms.Add(convertedPlan);
            //package.SaveAs(1, @"C:\Users\usr040\Documents\TEST\" + 41.ToString() + ".pog");
            //package.Dispose();
            //SplitPlan split = new SplitPlan(Plan.GetPlanById(109202, PlanType.Manual));
            //Planogram convertedPlan = Convert(split);
            //Package package = Package.NewPackage(1, PackageLockType.System);
            //package.Planograms.Add(convertedPlan);


            //package.SaveAs(1, Path.GetTempPath() + PlanType.Manual.ToString() + @"\" + 109202.ToString() + ".pog");
            //package.Dispose();

            //SplitPlan split = new SplitPlan(Plan.GetPlanById(183, PlanType.Merchandising));
            //Planogram convertedPlan = Convert(split);
            //Package package = Package.NewPackage(1, PackageLockType.System);
            //package.Planograms.Add(convertedPlan);
            //package.SaveAs(1, @"C:\Users\usr040\Documents\TEST\" + 183.ToString() + ".pog");
            //package.Dispose();


            //ONESTOP rules
            //Plan plan = Plan.GetPlanById(1231, PlanType.Store);
            //Boolean useFixturePlan = plan.FixturePlanId > 0;
            //SplitPlan split = GetSplitPlan(plan, PlanType.Store, useFixturePlan);
            //Planogram convertedPlan = Convert(split);
            //Package package = Package.NewPackage(1, PackageLockType.System);
            //package.Planograms.Add(convertedPlan);
            //String path = Path.GetTempPath() + @"LegacyPogs\" + PlanType.Store.ToString() + @"\";
            //Directory.CreateDirectory(path);
            //package.SaveAs(1, path + 1231.ToString() + ".pog");
            //package.Dispose();

            //Plan plan = Plan.GetPlanById(1782, PlanType.Store);
            //Boolean useFixturePlan = plan.FixturePlanId > 0;
            //SplitPlan split = GetSplitPlan(plan, PlanType.Store, useFixturePlan);
            //Planogram convertedPlan = Convert(split);
            //Package package = Package.NewPackage(1, PackageLockType.System);
            //package.Planograms.Add(convertedPlan);
            //String path = Path.GetTempPath() + @"LegacyPogs\" + PlanType.Store.ToString() + @"\";
            //Directory.CreateDirectory(path);
            //package.SaveAs(1, path + 1782.ToString() + ".pog");
            //package.Dispose();

            //mor
            //Plan plan = Plan.GetPlanById(40, PlanType.Store);
            //Boolean useFixturePlan = plan.FixturePlanId > 0;
            //SplitPlan split = GetSplitPlan(plan, PlanType.Store, useFixturePlan);
            //Planogram convertedPlan = Convert(split);
            //Package package = Package.NewPackage(1, PackageLockType.System);
            //package.Planograms.Add(convertedPlan);
            //String path = Path.GetTempPath() + @"LegacyPogs\" + PlanType.Store.ToString() + @"\";
            //Directory.CreateDirectory(path);
            //package.SaveAs(1, path + 40.ToString() + ".pog");
            //package.Dispose();

            //Not Doing Manual plans ProcessPlans(planProcess.FetchManualPlanIds(), PlanType.Manual);
            ProcessPlans(merchPlanIds, PlanType.Merchandising);
            ProcessPlans(storePlanIds, PlanType.Store);
            ProcessPlans(clusterPlanIds, PlanType.Cluster);
        }
       
        private void ProcessPlans(List<Int32> planIds, PlanType planType)
        {
            ParallelOptions parallelOptions = new ParallelOptions()
            {
                MaxDegreeOfParallelism = this.ProcessThreads
            };
            Int32 finsishedPlans = 0;
            OnProcessUpdate?.Invoke(planType, finsishedPlans, planIds.Count, _totalPlansFinishedCount, _totalPlanCount);

            Parallel.ForEach<Int32>(planIds, parallelOptions, p =>
            {
                try
                {
                    Plan plan = Plan.GetPlanById(p, planType);
                    Boolean useFixturePlan = plan.FixturePlanId > 0;
                    SplitPlan split = GetSplitPlan(plan, planType, useFixturePlan);

                    if (split != null)
                    {
                        Planogram convertedPlan = Convert(split);
                        Package package = Package.NewPackage(1, PackageLockType.System);
                        package.Planograms.Add(convertedPlan);
                        String pathToSaveTo = GenerateWordkingPathAndDirectoryForPlanogram(plan);

                        package = package.SaveAs(0, pathToSaveTo);
                        package.Unlock();
                        package.Dispose();
                    }
                }
                catch
                {
                    GeneralHelpers.Log($"Failed to convert {planType} plan ID:{p}", OutputPath);
                }

                finsishedPlans++;
                _totalPlansFinishedCount++;
                OnProcessUpdate?.Invoke(planType, finsishedPlans, planIds.Count, _totalPlansFinishedCount, _totalPlanCount);
            });
        }

        private String GenerateWordkingPathAndDirectoryForPlanogram(Plan plan)
        {
            String path = GenerateWorkDirectoryPathForPlanogram(plan);

            Directory.CreateDirectory(path);
            String fileName = GenerateFilenameForPlanogram(plan);

            // Code added to try to prevent System.IO.PathTooLongExceptions
            String combinePath = Path.Combine(path, GeneralHelpers.StripInvalidFileNameChars(fileName));

            // If the path itself is greater or it is close to the MAX_Lenght then is not worth it to trim the file name. It it crashes the user should appoint the creation of the files to a shorter directory path.
            if (combinePath.Length > 256)
            {
               combinePath = combinePath.Substring(0, 252) + PlanogramFileExtension;
            }

            return combinePath;
        }

        private String GenerateWorkDirectoryPathForPlanogram(Plan plan)
        {
            String path = Path.Combine(
                GeneralHelpers.StripInvalidFilePathChars(OutputPath),
                GeneralHelpers.StripInvalidFileNameChars(plan.CategoryReviewName));

            path = Path.Combine(path,
                GeneralHelpers.StripInvalidFileNameChars(plan.PlanType.ToString()));

            if (plan.PlanType == PlanType.Cluster || plan.PlanType == PlanType.Store)
            {
                path = Path.Combine(path, plan.Islive > 0 ? "Live" : "WIP");
            }
            return path;
        }

        private static String GenerateFilenameForPlanogram(Plan plan)
        {
            String type;
            switch (plan.PlanType)
            {
                case PlanType.Cluster:
                    type = ClusterPlanogramFileNameType;
                    break;
                case PlanType.Store:
                    type = StorePlanogramFilenameType;
                    break;
                case PlanType.Merchandising:
                    type = MerchandisingPlanogramFilenameType;
                    break;
                case PlanType.Manual:
                    type = ManualPlanogramFilenameType;
                    break;
                default:
                    type = PlanogramFilenameType;
                    break;
            }
            return $"{type}{plan.Id}{PlanogramFileExtension}";
        }


        private Planogram Convert(SplitPlan plan)
        {
            ConvertContext context = new ConvertContext(plan, this.Client);

            context.ConvertedPlan = Planogram.NewPlanogram();

            context.ConvertedPlan.Name = plan.ParentPlan.UniqueItemName;
            context.ConvertedPlan.LocationCode = plan.ParentPlan.StoreCode;
            if(!String.IsNullOrWhiteSpace(plan.ParentPlan.StoreFlexi))
            {
                String[] flexArray = plan.ParentPlan.StoreFlexi.Split(':');
                if (flexArray.Length > 1)
                {
                    context.ConvertedPlan.LocationName = flexArray[1].Trim();
                }
            }

            if (!String.IsNullOrWhiteSpace(plan.ParentPlan.MerchandisingGroupFlexi))
            {
                String[] flexArray = plan.ParentPlan.MerchandisingGroupFlexi.Split(':');
                if (flexArray.Length > 1)
                {
                    context.ConvertedPlan.CategoryCode = flexArray[0].Trim();
                    context.ConvertedPlan.CategoryName = flexArray[1].Trim();
                }
            }
            Int32 count1, count2;

            AddFixtures(context);

            SetInventory(context);

            ProductHelper.CreateProducts(context, out count1, out count2);
            context.Converter.SetProductPerformanceData(context);

           

            ProductHelper.PostProcessPositons(context);
            ProductHelper.CreateAssortment(context);

            return context.ConvertedPlan;
        }

        private static void AddFixtures(ConvertContext context)
        {
            while (context.ReadNextBay())
            {
                FixtureHelper.AddFixture(context);
            }
        }

        #endregion

        #region Methods

        private SplitPlan GetSplitPlan(Plan plan, PlanType type, Boolean useFixturePlan)
        {
            try
            {
                return new SplitPlan(plan, useFixturePlan);
            }
            catch
            {
                if (useFixturePlan)
                {
                    SplitPlan output = GetSplitPlan(plan, type, false);

                    if (output != null) Log(
                        $"The {type} plan {plan.Name} (ID={plan.Id}) was created without using its related fixture plan");

                    return output;
                }
                else
                {
                    Log($"The conversion of the {type} plan {plan.Name} (ID={plan.Id}) failed.");
                }
            }

            return null;
        }

       
        private static void SetInventory(ConvertContext context)
        {
            IEnumerable<IGrouping<String, PlanProfile>> profileRows = context.CurrentPlan.ParentPlan.PlanProfileRows
                .Where(p => p.ProfileGroupType == 5 && p.IsActive && 
                    p.Priority <= context.Converter.MaxInventoryPriority)
                .GroupBy(p => p.KeyField);

            Single? minCasePack = null;
            Single? minDos = null;

            context.ConvertedPlan.Inventory.MinShelfLife = 0;
            context.ConvertedPlan.Inventory.IsShelfLifeValidated = false;
            context.ConvertedPlan.Inventory.MinDeliveries = 0;
            context.ConvertedPlan.Inventory.IsDeliveriesValidated = false;
            foreach (IGrouping<String, PlanProfile> group in profileRows)
            {
                if (String.Equals(group.Key, context.Converter.DosKeyField, StringComparison.InvariantCultureIgnoreCase))
                {
                    minDos = group.Max(p => p.Minimum);                    
                }
                else if (String.Equals(group.Key, context.Converter.CasePackKeyField, StringComparison.InvariantCultureIgnoreCase))
                {
                    minCasePack = group.Max(p => p.Minimum);
                }
            }

            if (minDos.HasValue)
            {
                context.ConvertedPlan.Inventory.MinDos = minDos.Value;
            }
            else
            {
                context.ConvertedPlan.Inventory.IsDosValidated = false;
                context.ConvertedPlan.Inventory.MinDos = 0;
            }

            if (minCasePack.HasValue)
            {
                context.ConvertedPlan.Inventory.MinCasePacks = minCasePack.Value;
            }
            else
            {
                context.ConvertedPlan.Inventory.IsCasePacksValidated = false;
                context.ConvertedPlan.Inventory.MinCasePacks = 0;
            }
        }

        private readonly Object _logLock = new Object();
        private void Log(String message)
        {
            lock (_logLock)
            {

                String path = Path.Combine(OutputPath, "Log");
                Directory.CreateDirectory(path);
                using (StreamWriter wr = new StreamWriter(Path.Combine(path,"log.txt"), true))
                {
                    wr.WriteLine($"{DateTime.Now:yyyy-MM-dd} - {message}");
                }
            }
        }
        #endregion
    }
}
