﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using V7DAL = Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Model;
using System.IO;

namespace Galleria.Ccm.Legacy.V7
{
    public class Helpers
    {
        const String cMssql = "V7DAL";

        public static void SetupDal(String serverName, String databaseName)
        {

            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(cMssql);
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", serverName));
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", databaseName));
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("DefaultUser", System.Security.Principal.WindowsIdentity.GetCurrent().Name));

            // now create a new instance of the Mssql dal                
            IDalFactory dalFactory = new V7DAL.DalFactory(dalFactoryConfig);

            DalContainer.RegisterFactory(cMssql, dalFactory);
            DalContainer.DalName = cMssql;
        }


        public static GenericConvert GetClientConverter(ClientType client)
        {
            switch (client)
            {
                case ClientType.OneStop:
                    return new OneStopConvert();
                case ClientType.Test:
                    return new TestConvert();
                case ClientType.Generic:
                default:
                    return new GenericConvert();
            }
        }

    }

}
