﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Legacy.Msp.Model;
using Galleria.Framework.Planograms.External;

namespace Galleria.Ccm.Legacy.Msp
{
    
    public class OneStopConvert : GenericConvert
    {
        public override void UpdateProduct(PlanogramProduct v8Product, Product mspProduct)
        {
            v8Product.CasePackUnits = Convert.ToInt16(mspProduct.User_Number_9);            
            v8Product.CaseCost = Convert.ToSingle(mspProduct.Cost_Price);
            v8Product.SellPrice = Convert.ToSingle(mspProduct.Price);
            v8Product.TaxRate = Convert.ToSingle(mspProduct.VatRate / 100);
        }
    }
}
