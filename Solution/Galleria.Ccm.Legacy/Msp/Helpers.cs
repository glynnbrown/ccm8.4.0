﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Model;
using MSPDAL = Galleria.Ccm.Legacy.Dal.Msp;

namespace Galleria.Ccm.Legacy.Msp
{
    public class Helpers
    {
        #region Constants
        private const string _accessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:Database Password=2010121315";
        #endregion

        public static void SetupDal(String filePath)
        {
            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(filePath);
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("ConnectionString", String.Format(
                  _accessConnectionString, filePath)));
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("DefaultUser", System.Security.Principal.WindowsIdentity.GetCurrent().Name));


            // now create a new instance of the Mssql dal                
            IDalFactory dalFactory = new MSPDAL.DalFactory(dalFactoryConfig);

            DalContainer.RegisterFactory(filePath, dalFactory);
            DalContainer.DalName = filePath;
        }

        public static void RemoveDal(String filePath)
        {
            DalContainer.RemoveFactory(filePath);
        }


        public static GenericConvert GetClientConverter(ClientType client)
        {
            switch (client)
            {
                case ClientType.OneStop:
                    return new OneStopConvert();
                case ClientType.Test:
                    return new GenericConvert();
                case ClientType.Generic:
                default:
                    return new GenericConvert();
            }
        }
    }
}
