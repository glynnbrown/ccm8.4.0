﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Processes;


namespace Galleria.Ccm.Legacy.Msp
{
    [Serializable]
    public partial class ConvertProcess : ProcessBase<ConvertProcess>
    {
        #region Properties
        public String ImportPath { get; set; }
        public String OutputPath { get; set; }
        public ClientType Client { get; set; }
        public Int32 ProcessThreads { get; set; }
        public String CategoryReviewIdsCSV { get; set; }
        public Boolean PlacedProductsOnly { get; set; }
        #endregion
    }
}
