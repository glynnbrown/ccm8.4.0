﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Legacy.Msp.Model;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Legacy.Msp
{
    /// <summary>
    /// Helper for creating fixture objects
    /// </summary>
    public class FixtureHelper
    {
        /// <summary>
        /// Creates all the objects requird to convert the current fixture in the context into
        /// the V8 structure. Including positons.
        /// </summary>
        /// <param name="context"></param>
        public static void AddFixture(ConvertContext context)
        {
            
            Single xOffset = 0;
            List<PlanElement> elements = context.CurrentBay.PlanElements.ToList();
            PlanogramFixture lastFixture = null;
            for (Int32 b = 0; b < context.CurrentBay.Number_Of_Bays; b++)
            {
                PlanogramFixture fixture = context.ConvertedPlan.Fixtures.Add();

                // Truncate name to 50 characters if length exceeds 100
                fixture.Name = context.CurrentBay.Name.Substring(0, context.CurrentBay.Name.Length > 100 ? 100 : context.CurrentBay.Name.Length);
                fixture.Width = (Single)context.CurrentBay.Width;
                fixture.Height = (Single)(context.CurrentBay.Height + context.CurrentBay.Base_Height);
                fixture.Depth = (Single)context.CurrentBay.Depth;
                // Create bay fixture item reference
                PlanogramFixtureItem bayFixture = GetFixtureItem(context, fixture);
                bayFixture.X += xOffset;
                context.CurrentConvertedFixtureItem = bayFixture;

                List<PlanElement> bayElements =  elements.Where(
                        e => e.X_Position.GreaterOrEqualThan(bayFixture.X) && e.X_Position.LessThan(bayFixture.X + fixture.Width)).ToList();

                foreach (PlanElement element in bayElements)
                {
                    context.CurrentElement = element;
                    PlanogramFixtureComponent fComponent = GetFixtureComponent(context);
                    context.CurrentConvertedFixtureComponent = fComponent;

                    PlanogramComponent component = GetComponent(context);
                    fComponent.PlanogramComponentId = component.Id;

                    fixture.Components.Add(fComponent);
                    elements.Remove(element);

                    context.MspElementIdToFixtureComponentLookup.Add(element.Plan_Element_Code, fComponent);
                }
                

                context.Converter.CreateBackboard(fixture, context.CurrentBay);
                context.Converter.CreateBase(fixture, context.CurrentBay);
                xOffset += fixture.Width;
                lastFixture = fixture;
            }

            //Ensure we have no leftover elements
            if (elements.Count > 0)
            {
                foreach (PlanElement element in elements.ToList())
                {
                    context.CurrentElement = element;
                    PlanogramFixtureComponent fComponent = GetFixtureComponent(context);
                    context.CurrentConvertedFixtureComponent = fComponent;

                    PlanogramComponent component = GetComponent(context);
                    fComponent.PlanogramComponentId = component.Id;

                    lastFixture.Components.Add(fComponent);
                    elements.Remove(element);
                }
            }

        }
        private static PlanogramFixtureItem GetFixtureItem(ConvertContext context, PlanogramFixture fixture)
        {
            PlanogramFixtureItem output = context.ConvertedPlan.FixtureItems.Add(fixture);

            //Bay Position Relative to Fixture Item            
            output.X = (Single)context.CurrentBay.Bay_Position;
            output.Y = 0;
            output.Z = 0;
            output.Roll = 0;
            output.Slope = 0;
            output.Angle = 0;

            return output;
        }
        private static PlanogramComponent GetComponent(ConvertContext context)
        {
            PlanogramComponent component;

            switch (PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.Type))
            {
                case PlanElementType.Shelf:
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Shelf, 0, 0, 0, SpacePlanningImportHelper.DecodeColour(context.CurrentElement.Colour));
                    break;
                case PlanElementType.Bar:
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Bar, 0, 0, 0, SpacePlanningImportHelper.DecodeColour(context.CurrentElement.Colour));
                    break;
                case PlanElementType.Peg:
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Peg, 0, 0, 0, SpacePlanningImportHelper.DecodeColour(context.CurrentElement.Colour));
                    break;
                case PlanElementType.Chest:
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Chest, 0, 0, 0, SpacePlanningImportHelper.DecodeColour(context.CurrentElement.Colour));
                    break;
                case PlanElementType.Pallet:
                    component = context.ConvertedPlan.Components.Add(PlanogramComponentType.Pallet, 0, 0, 0, SpacePlanningImportHelper.DecodeColour(context.CurrentElement.Colour));
                    break;
                default:
                    component = PlanogramComponent.NewPlanogramComponent();
                    break;
            }

            component.Name = context.CurrentElement.Name;

            PlanogramSubComponent subComponent = GetSubComponent(context, component);
            component.Height = subComponent.Height;
            component.Width = subComponent.Width;
            component.Depth = subComponent.Depth;

            return component;
        }
        private static PlanogramFixtureComponent GetFixtureComponent(ConvertContext context)
        {
            PlanogramFixtureComponent output = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
            output.Roll = 0;
            output.Slope = 0;
            output.Angle = 0;
            switch (PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.Type))
            {
                case PlanElementType.Shelf:
                    output.Slope = (Single)((-context.CurrentElement.Dimension_4 / 360) * (Math.PI * 2));
                    output.X = (Single)(context.CurrentElement.X_Position - context.CurrentConvertedFixtureItem.X);
                    output.Y = (Single)((context.CurrentElement.Y_Position) - (context.CurrentElement.Dimension_3 * Math.Cos(output.Slope)));
                    output.Z = (Single)(context.CurrentElement.Z_Position);
                    //output.Z = (Single)(context.CurrentElement.ShelfDepth * Math.Cos(output.Slope));
                    //output.Y -= (Single)(context.CurrentElement.ShelfDepth * Math.Sin(output.Slope));
                    break;
                case PlanElementType.Bar:
                    output.X = (Single)(context.CurrentElement.X_Position - context.CurrentConvertedFixtureItem.X);
                    output.Y = (Single)(context.CurrentElement.Y_Position - context.CurrentElement.Dimension_3);
                    output.Z = (Single)(context.CurrentElement.Z_Position);
                    break;
                case PlanElementType.Peg:
                    output.X = (Single)(context.CurrentElement.X_Position - context.CurrentConvertedFixtureItem.X);
                    output.Y = (Single)(context.CurrentElement.Y_Position - context.CurrentElement.Dimension_0);
                    output.Z = (Single)(context.CurrentElement.Z_Position);
                    break;
                case PlanElementType.Chest:
                    output.X = (Single)(context.CurrentElement.X_Position - context.CurrentConvertedFixtureItem.X);
                    output.Y = (Single)(context.CurrentElement.Y_Position - context.CurrentElement.Dimension_0);
                    output.Z = (Single)(context.CurrentElement.Z_Position - context.CurrentElement.Dimension_2);
                    break;
                default:
                    output.X = (Single)(context.CurrentElement.X_Position - context.CurrentConvertedFixtureItem.X);
                    output.Y = (Single)(context.CurrentElement.Y_Position);// - context.CurrentElement.ShelfThick);
                    output.Z = (Single)(context.CurrentElement.Z_Position);
                    break;
            }



            return output;
        }
        //private static PlanogramSubComponentCombineType GetCombineType(ConvertContext context)
        //{
        //    PlanogramSubComponentCombineType output = (PlanogramSubComponentCombineType)context.CurrentElement.ParentPlanElement.CombineDirection;
        //    var elements = context.CurrentPlan.Elements.Where(e => e.ParentPlanElement.Equals(context.CurrentElement.ParentPlanElement)).OrderBy(e => e.Id).ToList();

        //    //if we have multiple elements sharing the original element then we need
        //    //a bit more processing.
        //    if (elements.Count > 1 && output != PlanogramSubComponentCombineType.Both)
        //    {
        //        Boolean first = true;
        //        Boolean last = false;

        //        foreach (SplitElement element in elements)
        //        {
        //            Boolean equal = context.CurrentElement.Equals(element);
        //            if (first)
        //            {
        //                first = equal;
        //            }
        //            last = equal;
        //        };

        //        if (first && !last)
        //        {
        //            if (output == PlanogramSubComponentCombineType.LeftOnly)
        //            {
        //                output = PlanogramSubComponentCombineType.Both;
        //            }
        //            else
        //            {
        //                output = PlanogramSubComponentCombineType.RightOnly;
        //            }
        //        }
        //        else if (!first && last)
        //        {
        //            if (output == PlanogramSubComponentCombineType.RightOnly)
        //            {
        //                output = PlanogramSubComponentCombineType.Both;
        //            }
        //            else
        //            {
        //                output = PlanogramSubComponentCombineType.LeftOnly;
        //            }
        //        }
        //        else if (!first && !last)
        //        {
        //            output = PlanogramSubComponentCombineType.Both;
        //        }
        //    }

        //    return output;
        //}
        private static PlanogramSubComponent GetSubComponent(ConvertContext context, PlanogramComponent component)
        {
            PlanogramSubComponent output = component.SubComponents.First();


            Single positionYOffset = 0;

            switch (PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.Type))
            {
                case PlanElementType.Shelf:
                    //ShelfHeight,  ShelfWidth,     ShelfDepth,     ShelfThick,     ShelfSlope,     ShelfRiser,     YPosition,      XPosition,      ZPosition,      FingerSpace
                    //Dimension_0_, Dimension_1_,   Dimension_2_,   Dimension_3_,   Dimension_4_,   Dimension_5_,   Y_Position_,    X_Position_,    Z_Position_,    IIF(Use_Finger_Space_ <>0,Finger_Space_,0)

                    output.Height = (Single)context.CurrentElement.Dimension_3;
                    output.Width = (Single)context.CurrentElement.Dimension_1;
                    output.Depth = (Single)context.CurrentElement.Dimension_2;

                    output.MerchandisingStrategyX = context.Converter.ShelfXMerchStrategy;

                    output.MerchandisableHeight = (Single)context.CurrentElement.Dimension_0;
                    positionYOffset = (Single)context.CurrentElement.Y_Position - (Single)context.CurrentElement.Dimension_3;
                    output.RiserHeight = (Single)context.CurrentElement.Dimension_5;
                    break;
                case PlanElementType.Bar:
                    //BarHeight,    BarWidth,       BarDepth,       BarThick,       BarDistance,    BarHangerDepth, YPosition,      XPosition,      ZPosition,      FingerSpace
                    //Dimension_0_, Dimension_1_,   Dimension_2_,   Dimension_3_,   Dimension_4_,   Dimension_5_,   Y_Position_,    X_Position_,    Z_Position_,    IIF(Use_Finger_Space_ <>0,Finger_Space_,0)

                    output.Width = (Single)context.CurrentElement.Dimension_1;
                    output.Height = (Single)context.CurrentElement.Dimension_3;
                    output.Depth = (Single)context.CurrentElement.Dimension_2;

                    output.MerchandisableHeight = (Single)context.CurrentElement.Dimension_0;
                    positionYOffset = (Single)context.CurrentElement.Y_Position - (Single)context.CurrentElement.Dimension_3;

                    break;
                case PlanElementType.Peg:
                    //PegHeight,    PegWidth,       PegVertSpace,   PegHorzSpace,     PegVertStart,     PegHorzStart,   PegDepth,       YPosition,      XPosition,      ZPosition,      FingerSpace
                    //Dimension_0_, Dimension_1_,   Dimension_2_,   Dimension_3_,     Dimension_4_,     Dimension_5_,   Dimension_7_,   Y_Position_,    X_Position_,    Z_Position_,    IIF(Use_Finger_Space_ <>0,Finger_Space_,0)
                    output.Width = (Single)context.CurrentElement.Dimension_1;
                    output.Height = (Single)context.CurrentElement.Dimension_0;
                    output.Depth = (Single)0.01F;

                    output.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                    output.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
                    output.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

                    output.MerchConstraintRow1SpacingX = (Single)context.CurrentElement.Dimension_3;
                    output.MerchConstraintRow1StartX = (Single)context.CurrentElement.Dimension_5;
                    output.MerchConstraintRow1SpacingY = (Single)context.CurrentElement.Dimension_2;
                    output.MerchConstraintRow1StartY = (Single)context.CurrentElement.Dimension_4;

                    output.MerchConstraintRow1Height = Math.Min(output.MerchConstraintRow1Height, output.MerchConstraintRow1SpacingY * 0.5F);
                    output.MerchConstraintRow1Width = Math.Min(output.MerchConstraintRow1Width, output.MerchConstraintRow1SpacingX * 0.5F);

                    output.MerchandisableDepth = (Single)context.CurrentElement.Dimension_7;
                    positionYOffset = (Single)context.CurrentElement.Y_Position - (Single)context.CurrentElement.Dimension_0;

                    break;
                case PlanElementType.Chest:
                    //ChestHeight,  ChestWidth,     ChestDepth,     ChestWall,                                          ChestInside,    ChestMerchandising, ChestDivider,   ChestAbove,     YPosition,      XPosition,      ZPosition,      FingerSpace
                    //Dimension_0_, Dimension_1_,   Dimension_2_,   IIf([Sub_Type_]=1,[Dimension_3_],0) AS Dimension3,  Dimension_4_,   Dimension_5_,       Dimension_6_,   Dimension_7_,   Y_Position_,    X_Position_,    Z_Position_,    IIF(Use_Finger_Space_ <>0,Finger_Space_,0)
                    output.Width = (Single)context.CurrentElement.Dimension_1;
                    output.Height = (Single)context.CurrentElement.Dimension_0;
                    output.Depth = (Single)context.CurrentElement.Dimension_2;

                    output.FaceThicknessBottom = (Single)(context.CurrentElement.Dimension_0 - context.CurrentElement.Dimension_4);

                    if (context.CurrentElement.Sub_Type == 1)
                    {
                        output.FaceThicknessFront = (Single)context.CurrentElement.Dimension_3;
                        output.FaceThicknessBack = (Single)context.CurrentElement.Dimension_3;
                        output.FaceThicknessLeft = (Single)context.CurrentElement.Dimension_3;
                        output.FaceThicknessRight = (Single)context.CurrentElement.Dimension_3;
                    }
                    else
                    {
                        output.FaceThicknessFront = 0;
                        output.FaceThicknessBack = 0;
                        output.FaceThicknessLeft = 0;
                        output.FaceThicknessRight = 0;
                    }
                    output.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                    output.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
                    output.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

                    output.MerchandisableHeight = (Single)context.CurrentElement.Dimension_5;
                    positionYOffset = (Single)(context.CurrentElement.Dimension_1 - context.CurrentElement.Dimension_4);

                    break;
                //case PlanElementType.Pallet:

                //    output.Width = context.CurrentElement.ShelfWidth;
                //    output.Height = context.CurrentElement.ShelfThick;
                //    output.Depth = context.CurrentElement.ShelfDepth;

                //    output.MerchandisableHeight = (Single)context.CurrentElement.ShelfHeight;
                //    positionYOffset = (Single)context.CurrentElement.YPosition - (Single)context.CurrentElement.ShelfThick;
                //    break;
                default:

                    output.Height = (Single)context.CurrentElement.Dimension_0;
                    output.Width = (Single)context.CurrentElement.Dimension_1;
                    output.Depth = (Single)context.CurrentElement.Dimension_2;

                    break;
            }
            //output.CombineType = GetCombineType(context);
            output.Name = context.CurrentElement.Name;

            //output.BackOverhang = (Single)context.CurrentElement.ParentPlanElement.BackOverhang;
            //output.TopOverhang = (Single)context.CurrentElement.ParentPlanElement.TopOverhang;
            //output.FrontOverhang = (Single)context.CurrentElement.ParentPlanElement.FrontOverhang;
            //output.BottomOverhang = (Single)context.CurrentElement.ParentPlanElement.BottomOverhang;
            //output.LeftOverhang = (Single)context.CurrentElement.ParentPlanElement.LeftOverhang;
            //output.RightOverhang = (Single)context.CurrentElement.ParentPlanElement.RightOverhang;

            context.CurrentConvertedSubComponent = output;

            foreach (PlanPosition pos in context.CurrentElement.Positions)
            {
                context.CurrentPostion = pos;
                if (pos.Units > 0)
                {
                    context.ConvertedPlan.Positions.RaiseListChangedEvents = false;
                    context.ConvertedPlan.Positions.Add(
                        ProductHelper.GetPosition(context, PlanElementTypeHelper.InternalNameToEnum(context.CurrentElement.Type), positionYOffset));
                    context.ConvertedPlan.Positions.RaiseListChangedEvents = true;
                }
            }

            output.CombineType = String.IsNullOrWhiteSpace(context.CurrentElement.Combined_With) ? PlanogramSubComponentCombineType.None : PlanogramSubComponentCombineType.Both;

            //If the width is negative then this is an element that has been combined
            //we will need to go back to the parent element to reduce its width and
            //then correct this elements width to be positive
            if (output.Width < 0)
            {
                output.Width = -output.Width;
                Int32 elementId = 0;
                if (Int32.TryParse(context.CurrentElement.Combined_With, out elementId))
                {
                    PlanogramFixtureComponent fcomp = null;
                    if (context.MspElementIdToFixtureComponentLookup.TryGetValue(elementId, out fcomp))
                    {
                        PlanogramComponent comp = fcomp.GetPlanogramComponent();
                        comp.Width -= output.Width;
                        comp.SubComponents[0].Width -= output.Width;
                    }
                }
            }

            return output;
        }
    }
}
