﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Legacy.Msp.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Legacy.Msp
{
    public class ConvertContext
    {
        private Int32 bayIndex = -1;
        private PlanBay _currentBay = null;
        private PlanBay _lastBay = null;
        private PlanBay _nextBay = null;
        private List<PlanBay> _orderedBays;

        public ClientType Client { get; private set; }
        public GenericConvert Converter { get; private set; }

        public PlanProposal CurrentPlan { get; set; }
        public PlanElement CurrentElement { get; set; }

        public String OutputFileName { get; set; }
        public String OutputPath { get; set; }
        #region Converted Properties
        public Planogram ConvertedPlan { get; set; }
        public PlanogramSubComponent CurrentConvertedSubComponent { get; set; }
        public PlanogramFixtureItem CurrentConvertedFixtureItem { get; set; }
        public PlanogramFixtureComponent CurrentConvertedFixtureComponent { get; set; }
        public PlanPosition CurrentPostion { get; set; }

        private Dictionary<Int32, PlanogramFixtureComponent> _mspElementIdToFixtureComponentLookup = new Dictionary<Int32, PlanogramFixtureComponent>();
        public Dictionary<Int32, PlanogramFixtureComponent> MspElementIdToFixtureComponentLookup
        {
            get
            {
                return _mspElementIdToFixtureComponentLookup;
            }
        }
        #endregion

        #region BayProperties

        public PlanBay LastBay
        {
            get
            {
                return _lastBay;
            }
        }
        public PlanBay CurrentBay
        {
            get
            {
                return _currentBay;
            }
        }
        public PlanBay NextBay
        {
            get
            {
                return _nextBay;
            }
        }

        #endregion

        public ProductList Products { get; set; }

        public ConvertContext(PlanProposal plan, ClientType client, String outputPath, Boolean placedProductsOnly)
        {
            this.OutputPath = outputPath;
            this.CurrentPlan = plan;
            this.Client = client;
            this.Converter = Helpers.GetClientConverter(client);

            if (plan.PlanBays != null)
            {
                _orderedBays = plan.PlanBays.OrderBy(p => p.Bay_Position).ToList();
            }
            else
            {
                _orderedBays = new List<PlanBay>();
            }
            this.Products = ProductList.GetProductsProposalByCode(plan.Plan_Proposal_Code, placedProductsOnly, plan.FilePath);
        }


        #region Methods
        public Boolean ReadNextBay()
        {
            bayIndex++;
            if (bayIndex >= _orderedBays.Count) return false;

            _lastBay = _currentBay;
            _currentBay = _orderedBays[bayIndex];

            if (bayIndex + 1 < _orderedBays.Count)
            {
                _nextBay = _orderedBays[bayIndex + 1];
            }
           
            return true;
        }
        #endregion
    }
}
