﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Legacy.Msp.Model;
using Galleria.Framework.Planograms.External;

namespace Galleria.Ccm.Legacy.Msp
{
    public class GenericConvert
    {
        public virtual PlanogramSubComponentXMerchStrategyType ShelfXMerchStrategy
        {
            get
            {
                return PlanogramSubComponentXMerchStrategyType.Even;
            }
        }

        public virtual void CreateBackboard(PlanogramFixture fixture, PlanBay bay)
        {
            PlanogramFixtureComponent backBoard = fixture.Components.Add(PlanogramComponentType.Backboard, (Single)bay.Width, (Single)(bay.Height + bay.Base_Height), (Single)bay.Depth, SpacePlanningImportHelper.DecodeColour(bay.Colour));
            PlanogramComponent bbc = backBoard.GetPlanogramComponent();
            backBoard.Z -= bbc.Depth;
            //Backboard should only have 1 sub component
            if (bbc.SubComponents.Count > 0)
            {
                bbc.SubComponents[0].NotchHeight = bay.Use_Notch_Spacing;
                bbc.SubComponents[0].IsNotchPlacedOnFront = bay.Use_Notch_Spacing > 0;
                bbc.SubComponents[0].NotchStartX = 2;
                bbc.SubComponents[0].NotchSpacingX = 2;
                bbc.SubComponents[0].NotchStartY = (Single)bay.FirstNotch;
                bbc.SubComponents[0].NotchSpacingY = (Single)bay.Notch_Spacing;
                bbc.SubComponents[0].NotchHeight = 1;
                bbc.SubComponents[0].NotchWidth = 2;
            }
        }

        public virtual void CreateBase(PlanogramFixture fixture, PlanBay bay)
        {

            PlanogramFixtureComponent backBoard = fixture.Components.Add(PlanogramComponentType.Base, (Single)bay.Base_Width, (Single)bay.Base_Height, (Single)(bay.Base_Depth - bay.Depth), SpacePlanningImportHelper.DecodeColour(bay.Base_Colour));
           
        }

        public virtual void UpdateProduct(PlanogramProduct v8Product, Product mspProduct)
        {            
            v8Product.SellPrice = Convert.ToSingle(mspProduct.Price);
            v8Product.CostPrice = Convert.ToSingle(mspProduct.Cost_Price);
            v8Product.TaxRate = Convert.ToSingle(mspProduct.VatRate / 100);
        }

        /// <summary>
        /// Any capped position in MSP turns the manually placed flag on. A client may not always
        /// want this flag to come through to CCM so this function can be altered per client if
        /// need be.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public virtual Boolean IsManualPosition(PlanPosition position)
        {            
            return position.IsManuallyPlaced;
        }

        /// <summary>
        /// Creates performance data from PerformanceValue01-10 position values. Multi-Sited position
        /// performance is summed (needs checking).
        /// </summary>
        /// <param name="context"></param>
        public void SetProductPerformanceData(ConvertContext context)
        {
            //context.ConvertedPlan.Performance.ClearAll();

            //foreach (var product in context.Products)
            //{
            //    PlanogramPerformanceData data = PlanogramPerformanceData.NewPlanogramPerformanceData();
            //    data.PlanogramProductId = -product.Product_Code;
            //    //add data first so it has a parent
            //    context.ConvertedPlan.Performance.PerformanceData.Add(data);

            //    data.P1 = (Single)product.Sales;                
            //}
            //Byte performanceId = 1;
            //PlanogramPerformanceMetric averageWeeklySales = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(performanceId);
            //averageWeeklySales.Name = String.Format("Average Weekly Sales", performanceId);
            //averageWeeklySales.MetricType = PlanogramMetricType.Decimal;
            //context.ConvertedPlan.Performance.Metrics.Add(averageWeeklySales);
            //performanceId++;

            //PlanogramPerformanceMetric averageWeeklyUnits = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(performanceId);
            //averageWeeklyUnits.Name = String.Format("Average Weekly Units", performanceId);
            //averageWeeklyUnits.MetricType = PlanogramMetricType.Decimal;
            //context.ConvertedPlan.Performance.Metrics.Add(averageWeeklyUnits);
            //averageWeeklyUnits.SpecialType = PlanogramMetricSpecialType.RegularSalesUnits;
            //performanceId++;

        }        
    }
}
