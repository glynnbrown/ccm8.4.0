﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
// V8-32609 : L.Ineson
//  Remapped PegProngOffset to PegProngOffsetX
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Ccm.Legacy.Msp.Model;
using Galleria.Framework.Planograms.External;

namespace Galleria.Ccm.Legacy.Msp
{
    public class ProductHelper
    {

        public enum Dimension
        {
            Height,
            Width,
            Depth
        };

        public static void CreateProducts(ConvertContext context)
        {
            List<PlanogramProduct> output = new List<PlanogramProduct>(context.Products.Count);

            foreach (Product originalProduct in context.Products)
            {
                PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
                product.Id = -originalProduct.Product_Code;
                product.Name = originalProduct.Name;
                product.Gtin = originalProduct.Prod_ID;
                if (product.Gtin.Length > 14)
                {
                    product.Gtin = product.Gtin.Substring(originalProduct.Prod_ID.Length - 14, 14);
                }

                product.FillColour = SpacePlanningImportHelper.DecodeColour(originalProduct.Colour);
                product.Depth = (Single)originalProduct.Depth;
                product.Height = (Single)originalProduct.Height;
                product.Width = (Single)originalProduct.Width;
                //product.SqueezeHeight = (Single)originalProduct.SqueezeHeight;
                //product.SqueezeWidth = (Single)originalProduct.SqueezeWidth;
                //product.SqueezeDepth = (Single)originalProduct.SqueezeDepth;


                product.AlternateDepth = 0;
                product.AlternateHeight = 0;
                product.AlternateWidth = 0;
                //product.CanBreakTrayBack = originalProduct.BreakTrayBack > 0;
                //product.CanBreakTrayDown = originalProduct.BreakTrayDown > 0;
                //product.CanBreakTrayTop = originalProduct.BreakTrayTop > 0 || (originalProduct.CapTopHigh > 0 && originalProduct.TrayCountHigh > 0);
                //product.CanBreakTrayUp = originalProduct.BreakTrayUp > 0;

                product.DisplayDepth = 0;
                product.DisplayHeight = 0;
                product.DisplayWidth = 0;

                //product.FingerSpaceAbove = (Single)originalProduct;

                product.FingerSpaceToTheSide = 0;
                product.ForceBottomCap = false;
                //product.ForceMiddleCap = originalProduct.MiddleCapping > 0;
                product.FrontOverhang = 0;
                product.IsActive = true;
               // product.IsFrontOnly = originalProduct.FrontOnly != 0;
                product.IsPlaceHolderProduct = false; 
                //product.MaxNestingDeep = originalProduct.MaxNestDeep == null ? Byte.MinValue : (Byte)originalProduct.MaxNestDeep;
                //product.MaxNestingHigh = originalProduct.MaxNestHigh == null ? Byte.MinValue : (Byte)originalProduct.MaxNestHigh;              
                product.MaxRightCap = 99;
                product.MaxStack = Convert.ToByte(originalProduct.Max_Stack);
                product.MaxTopCap = Convert.ToByte(originalProduct.Max_Cap);
                product.MerchandisingStyle = originalProduct.Tray_Height > 0 ? PlanogramProductMerchandisingStyle.Tray : PlanogramProductMerchandisingStyle.Unit;
                //product.MinDeep = (Byte)originalProduct.MinDeep;
                //product.MaxDeep = (Byte)originalProduct.MaxDeep;
                //product.NestingDepth = (Single)originalProduct.Nesting_Depth;
                if (originalProduct.Nesting_Height > 0)
                {
                    product.NestingHeight = (Single)(originalProduct.Height - originalProduct.Nesting_Height);
                }
                //product.NestingWidth = (Single)originalProduct.Nesting_Width;
                product.NumberOfPegHoles = originalProduct.Peg_Centre_Left > 0 ? (Byte)1 : (Byte)0; //TODO
                product.OrientationType = PlanogramProductOrientationType.Front0;
                product.PegDepth = 0;
                product.PegProngOffsetX = 0; //CHECK THIS
                product.PegX = (Single)originalProduct.Peg_Centre_Left;
                product.PegX2 = 0;
                product.PegX3 = 0;
                product.PegY = (Single)originalProduct.Peg_Centre_Top;
                product.PegY2 = 0;
                product.PegY3 = 0;
                product.PointOfPurchaseDepth = 0;
                product.PointOfPurchaseHeight = 0;
                product.PointOfPurchaseWidth = 0;

                product.StatusType = PlanogramProductStatusType.Active;
                product.TrayDeep = (Byte)originalProduct.Tray_Units_Deep;
                product.TrayHigh = (Byte)originalProduct.Tray_Units_High;
                product.TrayWide = (Byte)originalProduct.Tray_Units_Wide;
                product.TrayThickDepth = (Single)originalProduct.Tray_Thick;
                product.TrayThickHeight = (Single)originalProduct.Tray_Height;
                product.TrayThickWidth = (Single)originalProduct.Tray_Thick;
               // product.TrayWidth = (Single)(originalProduct.TrayWidth);
               // product.TrayHeight = (Single)(originalProduct.Tray_Height);
                //product.TrayDepth = (Single)(originalProduct.TrayDepth);
               // product.TrayPackUnits = (Int16)(originalProduct.TrayUnits);
                //product.CasePackUnits = (Int16)originalProduct.CasePack;
                
                product.Subcategory = originalProduct.Sub_Category_Name;
                product.Manufacturer = originalProduct.Manufacturer_Name;
                product.ManufacturerCode = originalProduct.Manufacturer_Code.ToString();

                //Update with client specific values
                context.Converter.UpdateProduct(product, originalProduct);

                #region Custom Attributes
                product.CustomAttributes.Text1 = originalProduct.User_String_1;
                product.CustomAttributes.Text2 = originalProduct.User_String_2;
                product.CustomAttributes.Text3 = originalProduct.User_String_3;
                product.CustomAttributes.Text4 = originalProduct.User_String_4;
                product.CustomAttributes.Text5 = originalProduct.User_String_5;
                product.CustomAttributes.Text6 = originalProduct.User_String_6;
                product.CustomAttributes.Text7 = originalProduct.User_String_7;
                product.CustomAttributes.Text8 = originalProduct.User_String_8;
                product.CustomAttributes.Text9 = originalProduct.User_String_9;
                product.CustomAttributes.Text10 = originalProduct.User_String_10;

                product.CustomAttributes.Value1 = (Single)originalProduct.User_Number_1;
                product.CustomAttributes.Value2 = (Single)originalProduct.User_Number_2;
                product.CustomAttributes.Value3 = (Single)originalProduct.User_Number_3;
                product.CustomAttributes.Value4 = (Single)originalProduct.User_Number_4;
                product.CustomAttributes.Value5 = (Single)originalProduct.User_Number_5;
                product.CustomAttributes.Value6 = (Single)originalProduct.User_Number_6;
                product.CustomAttributes.Value7 = (Single)originalProduct.User_Number_7;
                product.CustomAttributes.Value8 = (Single)originalProduct.User_Number_8;
                product.CustomAttributes.Value9 = (Single)originalProduct.User_Number_9;
                product.CustomAttributes.Value10 = (Single)originalProduct.User_Number_10;
                product.CustomAttributes.Value11 = (Single)originalProduct.User_Number_11;
                product.CustomAttributes.Value12 = (Single)originalProduct.User_Number_12;
                product.CustomAttributes.Value13 = (Single)originalProduct.User_Number_13;
                product.CustomAttributes.Value14 = (Single)originalProduct.User_Number_14;
                product.CustomAttributes.Value15 = (Single)originalProduct.User_Number_15;
                product.CustomAttributes.Value16 = (Single)originalProduct.User_Number_16;
                product.CustomAttributes.Value17 = (Single)originalProduct.User_Number_17;
                product.CustomAttributes.Value18 = (Single)originalProduct.User_Number_18;
                product.CustomAttributes.Value19 = (Single)originalProduct.User_Number_19;
                product.CustomAttributes.Value20 = (Single)originalProduct.User_Number_20;
                #endregion


                if (originalProduct.Inventory_Cases > 0)
                {
                    context.ConvertedPlan.Inventory.IsCasePacksValidated = true;
                    context.ConvertedPlan.Inventory.MinCasePacks = Math.Max(originalProduct.Inventory_Cases, context.ConvertedPlan.Inventory.MinCasePacks);
                }
                output.Add(product);
            }

            

            context.ConvertedPlan.Products.RaiseListChangedEvents = false;
            context.ConvertedPlan.Products.AddRange(output);
            context.ConvertedPlan.Products.RaiseListChangedEvents = true;
        }


        #region Position

        /// <summary>
        /// Gets a PlanogramPositon object to represent the CurrentPosition in the context
        /// </summary>
        /// <param name="context"></param>
        /// <param name="elementType"></param>
        /// <param name="yOffset"></param>
        /// <returns></returns>
        public static PlanogramPosition GetPosition(ConvertContext context, PlanElementType elementType, Single yOffset)
        {
            PlanogramPosition output = PlanogramPosition.NewPlanogramPosition();

            output.PlanogramSubComponentId = context.CurrentConvertedSubComponent.Id;
            output.PlanogramProductId = -context.CurrentPostion.Product_Code;
            output.PlanogramFixtureComponentId = context.CurrentConvertedFixtureComponent.Id;
            output.PlanogramFixtureItemId = context.CurrentConvertedFixtureItem.Id;

            PlanogramProduct product = context.ConvertedPlan.Products.FindById(output.PlanogramProductId);

            //output.BaySequenceNumber = PlanHelper.CurrentFixtureItemId;
            //output.ElementSequenceNumber = PlanHelper.CurrentElementSequence;
            //output.PositionSequenceNumber = PlanHelper.GetNextPositionSequence();

            //output = PlanPosition.ConvertCCMIntColourToGFSIntColour(Colour);

            if (context.CurrentPostion.Tray_Height == 0)
            {
                SetUnitFacings(context, output);
            }
            else
            {
                SetTrayFacings(context, output, product);
            }

            output.TotalUnits = context.CurrentPostion.Units;

            //Set XYZ positions to GFS XYZ
            switch (elementType)
            {
                case PlanElementType.Shelf:
                    output.X = (Single)context.CurrentPostion.X_Pos;
                    output.Y = (Single)context.CurrentPostion.Y_Pos - yOffset;
                    output.Z = 0;
                    break;
                case PlanElementType.Pallet:
                    output.X = (Single)context.CurrentPostion.X_Pos;
                    output.Y = (Single)context.CurrentPostion.Y_Pos - yOffset;
                    output.Z = 0;
                    break;
                case PlanElementType.Bar:
                    output.X = (Single)context.CurrentPostion.X_Pos;
                    output.Y = (Single)(context.CurrentPostion.Y_Pos - context.CurrentPostion.Height - yOffset);
                    output.Z = (Single)(context.CurrentPostion.Depth * context.CurrentPostion.No_Deep);
                    break;
                case PlanElementType.Peg:
                    output.X = (Single)context.CurrentPostion.X_Pos;
                    output.Y = (Single)(context.CurrentPostion.Y_Pos - yOffset);
                    output.Z = (Single)(context.CurrentPostion.Depth * context.CurrentPostion.No_Deep);
                    product.NumberOfPegHoles = 1;
                    break;
                case PlanElementType.Chest:
                    output.X = (Single)context.CurrentPostion.X_Pos;
                    output.Z = (Single)(context.CurrentPostion.Y_Pos + context.CurrentElement.Dimension_3);
                    output.Y = yOffset;
                    break;
            }
            
             
            product.SqueezeHeight = (Single)context.CurrentPostion.SqueezeHeight;
            product.SqueezeWidth = (Single)context.CurrentPostion.SqueezeWidth;
            if (product.SqueezeWidth == 1 && context.CurrentElement.Space_Remaining < 0)
            {
                product.SqueezeWidth = (Single)((context.CurrentConvertedSubComponent.Width + context.CurrentElement.Space_Remaining) / context.CurrentConvertedSubComponent.Width);
            }

            if (context.CurrentElement.Use_Finger_Space > 0)
            {
                product.FingerSpaceAbove = (Single)context.CurrentElement.Finger_Space;
            }

            output.IsManuallyPlaced = context.Converter.IsManualPosition(context.CurrentPostion);

            PlanogramAssortmentProduct assortmentProduct = product.GetPlanogramAssortmentProduct();

            if (assortmentProduct == null)
            {
               assortmentProduct =  context.ConvertedPlan.Assortment.Products.Add(product);
            }
            assortmentProduct.Facings = Convert.ToByte(output.FacingsWide + output.FacingsXWide);
            assortmentProduct.Units = Convert.ToInt16(output.TotalUnits);
            assortmentProduct.IsRanged = true;

            return output;
        }

        private static PlanogramPositionOrientationType GetFrontOrientationType(PlanPosition position)
        {
            switch (position.Forward_Face)
            {
                case 0: //FRONT

                    switch(position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Front0;
                        case 90:
                            return PlanogramPositionOrientationType.Front90;
                        case 180:
                            return PlanogramPositionOrientationType.Front180;
                        case 270:
                            return PlanogramPositionOrientationType.Front270;
                        default:
                            return PlanogramPositionOrientationType.Front0;
                    }
                    
                case 1: //TOP
                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Top0;
                        case 90:
                            return PlanogramPositionOrientationType.Top90;
                        case 180:
                            return PlanogramPositionOrientationType.Top180;
                        case 270:
                            return PlanogramPositionOrientationType.Top270;
                        default:
                            return PlanogramPositionOrientationType.Top0;
                    }
                case 2: //RIGHT
                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Right0;
                        case 90:
                            return PlanogramPositionOrientationType.Right90;
                        case 180:
                            return PlanogramPositionOrientationType.Right180;
                        case 270:
                            return PlanogramPositionOrientationType.Right270;
                        default:
                            return PlanogramPositionOrientationType.Right0;
                    }
                    
            }

            return PlanogramPositionOrientationType.Front0;
        }
        private static PlanogramPositionOrientationType GetRightOrientationType(PlanPosition position)
        {
            switch (position.Forward_Face)
            {
                case 0: //FRONT

                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Right0;
                        case 90:
                            return PlanogramPositionOrientationType.Top90;
                        case 180:
                            return PlanogramPositionOrientationType.Left0;
                        case 270:
                            return PlanogramPositionOrientationType.Bottom90;
                        default:
                            return PlanogramPositionOrientationType.Right0;
                    }

                case 1: //TOP
                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Right90;
                        case 90:
                            return PlanogramPositionOrientationType.Front90;
                        case 180:
                            return PlanogramPositionOrientationType.Left90;
                        case 270:
                            return PlanogramPositionOrientationType.Back90;
                        default:
                            return PlanogramPositionOrientationType.Right90;
                    }
                case 2: //RIGHT
                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Back0;
                        case 90:
                            return PlanogramPositionOrientationType.Top0;
                        case 180:
                            return PlanogramPositionOrientationType.Bottom0;
                        case 270:
                            return PlanogramPositionOrientationType.Front0;
                        default:
                            return PlanogramPositionOrientationType.Back0;
                    }

            }
            return PlanogramPositionOrientationType.Right0;
        }
        private static PlanogramPositionOrientationType GetTopOrientationType(PlanPosition position)
        {
            switch (position.Forward_Face)
            {
                case 0: //FRONT

                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Top0;
                        case 90:
                            return PlanogramPositionOrientationType.Left90;
                        case 180:
                            return PlanogramPositionOrientationType.Bottom0;
                        case 270:
                            return PlanogramPositionOrientationType.Right90;
                        default:
                            return PlanogramPositionOrientationType.Top0;
                    }

                case 1: //TOP
                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Back0;
                        case 90:
                            return PlanogramPositionOrientationType.Left0;
                        case 180:
                            return PlanogramPositionOrientationType.Front0;
                        case 270:
                            return PlanogramPositionOrientationType.Right0;
                        default:
                            return PlanogramPositionOrientationType.Top0;
                    }
                case 2: //RIGHT
                    switch (position.Rotation)
                    {
                        case 0:
                            return PlanogramPositionOrientationType.Top90;
                        case 90:
                            return PlanogramPositionOrientationType.Front90;
                        case 180:
                            return PlanogramPositionOrientationType.Bottom90;
                        case 270:
                            return PlanogramPositionOrientationType.Back90;
                        default:
                            return PlanogramPositionOrientationType.Top90;
                    }

            }

            return PlanogramPositionOrientationType.Top0;
        }

        private static T OrientDimension<T>(Dimension dim, T height, T width, T depth, Int32 forward_Face, Int32 rotation)
        {
            switch (forward_Face)
            {
                case 0: //FRONT

                    switch (rotation)
                    {
                        case 90:
                        case 270:
                            switch (dim)
                            {
                                case Dimension.Height:
                                    return width;
                                case Dimension.Width:
                                    return height;
                                case Dimension.Depth:
                                    return depth;
                            }
                            break;
                        case 0:
                        case 180:
                        default:
                            switch (dim)
                            {
                                case Dimension.Height:
                                    return height;
                                case Dimension.Width:
                                    return width;
                                case Dimension.Depth:
                                    return depth;
                            }
                            break;
                    }
                    break;
                case 1: //TOP
                    switch (rotation)
                    {
                        case 90:
                        case 270:
                            switch (dim)
                            {
                                case Dimension.Height:
                                    return width;
                                case Dimension.Width:
                                    return depth;
                                case Dimension.Depth:
                                    return height;
                            }
                            break;
                        case 0:
                        case 180:
                        default:
                            switch (dim)
                            {
                                case Dimension.Height:
                                    return depth;
                                case Dimension.Width:
                                    return width;
                                case Dimension.Depth:
                                    return height;
                            }
                            break;
                    }
                    break;
                case 2: //RIGHT
                    switch (rotation)
                    {
                        case 90:
                        case 270:
                            switch (dim)
                            {
                                case Dimension.Height:
                                    return depth;
                                case Dimension.Width:
                                    return height;
                                case Dimension.Depth:
                                    return width;
                            }
                            break;
                        case 0:
                        case 180:
                        default:
                            switch (dim)
                            {
                                case Dimension.Height:
                                    return height;
                                case Dimension.Width:
                                    return depth;
                                case Dimension.Depth:
                                    return width;
                            }
                            break;
                    }
                    break;

            }

            return default(T);
        }
        /// <summary>
        /// Set all the facing values for the supplied position with the assumption
        /// that the position is not a tray position
        /// </summary>
        /// <param name="position"></param>
        private static void SetUnitFacings(ConvertContext context, PlanogramPosition position)
        {
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;

            position.OrientationType = GetFrontOrientationType(context.CurrentPostion);

            //Note as this is not a tray product the High/Wide/Deep
            //values will not include the break tray values so can
            //be used directly as FrontHigh/Wide/Deep
            position.FacingsHigh = (Byte)(context.CurrentPostion.No_High);// + context.CurrentPostion.NestHigh);
            position.FacingsDeep = (Byte)(context.CurrentPostion.No_Deep);// + context.CurrentPostion.NestDeep);
            position.FacingsWide = (Byte)(context.CurrentPostion.No_Wide);// + context.CurrentPostion.NestWide);

            //Right caps
            if (context.CurrentPostion.Cap_Rgt_Wide > 0 && context.CurrentPostion.Cap_Rgt_Deep > 0)
            {
                position.IsXPlacedLeft = false;
                position.OrientationTypeX = GetRightOrientationType(context.CurrentPostion);
                position.FacingsXHigh = (Byte)context.CurrentPostion.No_High;
                position.FacingsXWide = (Byte)(context.CurrentPostion.Cap_Rgt_Wide + context.CurrentPostion.Cap_Lft_Wide);
                position.FacingsXDeep = (Byte)context.CurrentPostion.Cap_Rgt_Deep;
            }
            else
            //Left caps
            if (context.CurrentPostion.Cap_Rgt_Wide > 0 && context.CurrentPostion.Cap_Rgt_Deep > 0)
            {
                position.IsXPlacedLeft = true;
                position.OrientationTypeX = GetRightOrientationType(context.CurrentPostion);
                position.FacingsXHigh = (Byte)context.CurrentPostion.No_High;
                position.FacingsXWide = (Byte)context.CurrentPostion.Cap_Lft_Wide;
                position.FacingsXDeep = (Byte)context.CurrentPostion.Cap_Lft_Deep;
            }

            //Top caps.
            if (context.CurrentPostion.No_Capping > 0 && context.CurrentPostion.No_Capping_Deep > 0)
            {
                position.IsYPlacedBottom = false;
                position.OrientationTypeY = GetTopOrientationType(context.CurrentPostion);
                position.FacingsYDeep = (Byte)context.CurrentPostion.No_Capping_Deep;
                position.FacingsYHigh = (Byte)(context.CurrentPostion.No_Capping + context.CurrentPostion.Cap_Bot_High);
                position.FacingsYWide = (Byte)context.CurrentPostion.No_Wide;
            }
            else
            //Bottom caps.
            if (context.CurrentPostion.No_Capping > 0 && context.CurrentPostion.No_Capping_Deep > 0)
            {
                position.IsYPlacedBottom = true;
                position.OrientationTypeY = GetTopOrientationType(context.CurrentPostion);
                position.FacingsYDeep = (Byte)context.CurrentPostion.Cap_Bot_Deep;
                position.FacingsYHigh = (Byte)context.CurrentPostion.Cap_Bot_High;
                position.FacingsYWide = (Byte)context.CurrentPostion.No_Wide;
            }            
        }
        /// <summary>
        /// Set all the facing values for the supplied position with the assumption
        /// that the position is a tray position
        /// </summary>
        /// <param name="position"></param>
        private static void SetTrayFacings(ConvertContext context, PlanogramPosition position, PlanogramProduct product)
        {
            //Note that the units in a tray have not been oriented so we
            //have to orient them before using htem in any calculations.

            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;

            //Set front trays
            if (context.CurrentPostion.No_Wide > 0)
            {
                position.OrientationType = GetFrontOrientationType(context.CurrentPostion);
                position.FacingsHigh = (Byte)(context.CurrentPostion.No_High / product.TrayHigh);
                position.FacingsWide = (Byte)(context.CurrentPostion.No_Wide / product.TrayWide);
                position.FacingsDeep = (Byte)(context.CurrentPostion.No_Deep / product.TrayDeep);

                Byte trayHigh = OrientDimension<Byte>(Dimension.Height, product.TrayHigh, product.TrayWide, product.TrayDeep, context.CurrentPostion.Forward_Face, context.CurrentPostion.Rotation);
                Byte trayWide = OrientDimension<Byte>(Dimension.Width, product.TrayHigh, product.TrayWide, product.TrayDeep, context.CurrentPostion.Forward_Face, context.CurrentPostion.Rotation);
                Byte trayDeep = OrientDimension<Byte>(Dimension.Depth, product.TrayHigh, product.TrayWide, product.TrayDeep, context.CurrentPostion.Forward_Face, context.CurrentPostion.Rotation);

                if (product.TrayHigh != trayHigh ||
                    product.TrayWide != trayWide ||
                    product.TrayDeep != trayDeep)
                {
                    GeneralHelpers.Log(String.Format("{2} - Tray high, wide and deep for ({0}){1} have been altered to adhere to V8 merchandising.", product.Gtin, product.Name, context.OutputFileName), context.OutputPath);
                }

                product.TrayHigh = trayHigh;
                product.TrayWide = trayWide;
                product.TrayDeep = trayDeep;
            }
            else
            {
                position.FacingsDeep = 0;
                position.FacingsHigh = 0;
                position.FacingsWide = 0;
            }
            
            //Right caps
            if (context.CurrentPostion.Cap_Rgt_Wide > 0 && context.CurrentPostion.Cap_Rgt_Deep > 0)
            {
                position.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                position.IsXPlacedLeft = false;
                position.OrientationTypeX = GetRightOrientationType(context.CurrentPostion);
                position.FacingsXHigh = (Byte)context.CurrentPostion.No_High;
                position.FacingsXWide = (Byte)(context.CurrentPostion.Cap_Rgt_Wide + context.CurrentPostion.Cap_Lft_Wide);
                position.FacingsXDeep = (Byte)context.CurrentPostion.Cap_Rgt_Deep;
            }
            else
            //Left caps
            if (context.CurrentPostion.Cap_Rgt_Wide > 0 && context.CurrentPostion.Cap_Rgt_Deep > 0)
            {
                position.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                position.IsXPlacedLeft = true;
                position.OrientationTypeX = GetRightOrientationType(context.CurrentPostion);
                position.FacingsXHigh = (Byte)context.CurrentPostion.No_High;
                position.FacingsXWide = (Byte)context.CurrentPostion.Cap_Lft_Wide;
                position.FacingsXDeep = (Byte)context.CurrentPostion.Cap_Lft_Deep;
            }

            //Top caps.
            if (context.CurrentPostion.No_Capping > 0 && context.CurrentPostion.No_Capping_Deep > 0)
            {
                position.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                position.IsYPlacedBottom = false;
                position.OrientationTypeY = GetTopOrientationType(context.CurrentPostion);
                position.FacingsYDeep = (Byte)context.CurrentPostion.No_Capping_Deep;
                position.FacingsYHigh = (Byte)(context.CurrentPostion.No_Capping);
                position.FacingsYWide = (Byte)context.CurrentPostion.No_Wide;
            }
        }

        /// <summary>
        /// Performs any post processing on postions that might need to be done.
        /// </summary>
        /// <param name="context"></param>
        public static void PostProcessPositons(ConvertContext context)
        {
            //Correct position Z values. This requires positon details which needs the
            //whole plan structure finished to work.
            foreach (PlanogramPosition position in context.ConvertedPlan.Positions)
            {
                PlanogramPositionDetails details = position.GetPositionDetails();

                Single newZ = ((position.GetPlanogramSubComponent().Depth - position.Z) - details.MainTotalSize.Depth);

                position.Z = newZ;
            }
        }
        #endregion
    }
}
