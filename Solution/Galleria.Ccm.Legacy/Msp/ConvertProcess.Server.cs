﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Processes;
using System.Diagnostics;
using System.Threading.Tasks;
using Galleria.Ccm.Legacy.Msp.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using System.IO;
namespace Galleria.Ccm.Legacy.Msp
{
    public delegate void ProcessStateHandler(Int32 currentValue, Int32 maxValue, Int32 totalCurrentValue);

    public partial class ConvertProcess
    {
       
        private Int32 _totalPlansFinishedCount = 0, _finishedFiles = 0, _totalFiles = 0;
        public event ProcessStateHandler OnProcessUpdate;

        #region Execute
        protected override void OnExecute()
        {
            
            ParallelOptions parallelOptions = new ParallelOptions()
            {
                MaxDegreeOfParallelism = this.ProcessThreads
            };
            String[] filePaths = Directory.GetFiles(this.ImportPath, "*.spp", SearchOption.AllDirectories);
            _totalFiles = filePaths.Length;
            if (OnProcessUpdate != null)
            {
                OnProcessUpdate(_finishedFiles, _totalFiles, _totalPlansFinishedCount);
            }
            _totalPlansFinishedCount = 0;
            Parallel.ForEach<String>(filePaths, parallelOptions, filePath =>
            {
                try
                {
                    Helpers.SetupDal(filePath);

                    List<Int32> planCodes = PlanProcess.FetchPlanProposalCodes(filePath);


                    ProcessPlans(planCodes, filePath);
                    Helpers.RemoveDal(filePath);

                    _finishedFiles++;
                    if (OnProcessUpdate != null)
                    {
                        OnProcessUpdate(_finishedFiles, _totalFiles, _totalPlansFinishedCount);
                    }
                }catch
                {
                    GeneralHelpers.Log(String.Format("Failed to convert plans in file {0}", filePaths), OutputPath);
                }
            });

        }

        private void ProcessPlans(List<Int32> planCodes, String filePath)
        {
            String fileName = Path.GetFileNameWithoutExtension(filePath);

            foreach (Int32 p in planCodes)
            {
                try
                {
                    PlanProposal plan = PlanProposal.GetPlanProposalByCode(p, filePath);


                    if (plan != null)
                    {

                        ConvertContext context;
                        Planogram convertedPlan = Convert(plan, fileName, out context);
                        Package package = Package.NewPackage(1, PackageLockType.System);
                        package.Planograms.Add(convertedPlan);

                        String path = Path.Combine(
                            GeneralHelpers.StripInvalidFilePathChars(OutputPath),
                            GeneralHelpers.StripInvalidFilePathChars(fileName));

                        Directory.CreateDirectory(path);
                        String outputPath = Path.Combine(path, context.OutputFileName);

                        //if (File.Exists(outputPath))
                        //{
                        //    GeneralHelpers.Log(String.Format("File \"{0}\" not saved as it already exists.", outputPath), OutputPath);
                        //}
                        //Int32 i = 1;
                        //while (File.Exists(outputPath))
                        //{
                        //    outputPath = Path.Combine(path, String.Format("{0} ~ {1}", i, context.OutputFileName));
                        //    i++;
                        //}

                        package = package.SaveAs(0, outputPath);
                        package.Unlock();
                        package.Dispose();
                    }
                }
                catch
                {
                    GeneralHelpers.Log(String.Format("Failed to convert plan proposal {0} from {1}", p, fileName), OutputPath);
                }

                _totalPlansFinishedCount++;
                if (OnProcessUpdate != null)
                {
                    OnProcessUpdate(_finishedFiles, _totalFiles, _totalPlansFinishedCount);
                }
            }
        }



        private Planogram Convert(PlanProposal plan, String fileName, out ConvertContext context)
        {
            context = new ConvertContext(plan, this.Client, this.OutputPath, this.PlacedProductsOnly);

            context.ConvertedPlan = Planogram.NewPlanogram();
            context.ConvertedPlan.Name = GetPlanName(plan, fileName);

            context.OutputFileName = GeneralHelpers.StripInvalidFileNameChars(String.Format("(MSP{0}){1}.pog", plan.Plan_Proposal_Code, context.ConvertedPlan.Name));

            context.ConvertedPlan.Inventory.MinDeliveries = 0;            
            context.ConvertedPlan.Inventory.MinDos = 0;
            context.ConvertedPlan.Inventory.MinShelfLife = 0;
            context.ConvertedPlan.Inventory.MinCasePacks = 0;

            context.ConvertedPlan.Inventory.IsDeliveriesValidated = false;
            context.ConvertedPlan.Inventory.IsDosValidated = false;
            context.ConvertedPlan.Inventory.IsShelfLifeValidated = false;
            context.ConvertedPlan.Inventory.IsCasePacksValidated = false;


            ProductHelper.CreateProducts(context);
            context.Converter.SetProductPerformanceData(context);

            while (context.ReadNextBay())
            {
                FixtureHelper.AddFixture(context);
            }

            return context.ConvertedPlan;
        }

        private String GetPlanName(PlanProposal plan, String filename)
        {
            String output = Guid.NewGuid().ToString();


            output = String.Format("{0}", plan.Name);
       

            return output;

        }
      

        #endregion
    }
}
