﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Galleria.Ccm.Legacy
{
    public class GeneralHelpers
    {
        public static String StripInvalidFilePathChars(String path)
        {
            foreach (Char c in Path.GetInvalidPathChars())
            {
                path = path.Replace(c, '_');
            }

            return path;
        }
        public static String StripInvalidFileNameChars(String name)
        {
            foreach (Char c in Path.GetInvalidFileNameChars())
            {
                name = name.Replace(c, '_');
            }

            return name;
        }

        public static ClientType StringToClientType(String client)
        {
            ClientType output;

            if (Enum.TryParse<ClientType>(client, true, out output))
            {
                return output;
            }

            return ClientType.Generic;
        }

        private static Object logLock = new Object();
        public static void Log(String message, String outputPath)
        {
            lock (logLock)
            {
                String path = Path.Combine(outputPath, "Log");
                Directory.CreateDirectory(path);
                using (StreamWriter wr = new StreamWriter(Path.Combine(path, "log.txt"), true))
                {
                    wr.WriteLine(String.Format("{0:yyyy-MM-dd} - {1}", DateTime.Now, message));
                }
            }
        }
    }


    public enum ClientType
    {
        Generic,
        OneStop,
        Test
    }
}
