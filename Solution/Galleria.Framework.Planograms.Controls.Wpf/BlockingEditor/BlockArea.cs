﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// V8-28257 : L.Ineson
//  Made sure that fill pattern gets applied when a new blocking group is loaded.
#endregion
#endregion

using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor
{
    public class BlockArea : Control, IDisposable
    {
        #region Fields
        private IPlanogramBlockingGroup _group;
        #endregion

        #region Properties

        #region Context

        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register("Context", typeof(IPlanogramBlockingLocation), typeof(BlockArea),
            new PropertyMetadata(null, OnContextPropertyChanged));

        private static void OnContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockArea)obj).OnContextChanged(e.OldValue as IPlanogramBlockingLocation, e.NewValue as IPlanogramBlockingLocation);
        }

        public IPlanogramBlockingLocation Context
        {
            get { return (IPlanogramBlockingLocation)GetValue(ContextProperty); }
            private set { SetValue(ContextProperty, value); }
        }

        private void OnContextChanged(IPlanogramBlockingLocation oldValue, IPlanogramBlockingLocation newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= Context_PropertyChanged;
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += Context_PropertyChanged;
            }

            OnBlockingGroupChanged();
            UpdateSizeAndPosition();
        }

        
        #endregion

        #region IsSelected

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(Boolean), typeof(BlockArea),
            new PropertyMetadata(false, OnIsSelectedPropertyChanged));

        private static void OnIsSelectedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockArea senderControl = obj as BlockArea;

            //raise the appropriate event
            if ((Boolean)e.NewValue)
            {
                senderControl.OnSelected();
            }
            else
            {
                senderControl.OnDeselected();
            }
        }

        /// <summary>
        /// Gets/Sets if the item is selected
        /// </summary>
        public Boolean IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        #region Selected

        public static readonly RoutedEvent SelectedEvent =
            EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(BlockArea));

        public event RoutedEventHandler Selected
        {
            add { AddHandler(SelectedEvent, value); }
            remove { RemoveHandler(SelectedEvent, value); }
        }

        protected virtual void OnSelected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(BlockArea.SelectedEvent));
        }

        #endregion

        #region Deselected

        public static readonly RoutedEvent DeselectedEvent =
            EventManager.RegisterRoutedEvent("Deselected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(BlockArea));

        public event RoutedEventHandler Deselected
        {
            add { AddHandler(DeselectedEvent, value); }
            remove { RemoveHandler(DeselectedEvent, value); }
        }

        protected virtual void OnDeselected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(BlockArea.DeselectedEvent));
        }

        #endregion

        #endregion

        #region Constructors

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override.")]
        static BlockArea()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(BlockArea), new FrameworkPropertyMetadata(typeof(BlockArea)));
        }

        public BlockArea(IPlanogramBlockingLocation context)
        {
            this.Context = context;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the template of this control is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            UpdateSizeAndPosition();
        }

        /// <summary>
        /// Catches the mouse button down event
        /// Preview so it can handle before anything else
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            if (this.IsSelected)
            {
                //if already selected we need to re-raise just in case this needs to change from a multiple to single selection
                OnSelected();
            }
            else
            {
                this.IsSelected = true;
            }

            e.Handled = false;
        }

        /// <summary>
        /// Called whenever a property changes on the context
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Context_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "PlanogramBlockingGroupId":
                    OnBlockingGroupChanged();
                    break;

                case "X":
                case "Y":
                case "Width":
                case "Height":
                    UpdateSizeAndPosition();
                    break;

            }
        }

        /// <summary>
        /// Called whenever  a property changes on the associated blocking group.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Group_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Colour" || e.PropertyName == "FillPatternType")
            {
                //Create the pattern brush
                Brush foreground = new SolidColorBrush(ColorHelper.IntToColor(_group.Colour, true));
                Brush background = Brushes.Transparent;

                this.Background = 
                    WpfMaterialHelper.CreateRelativePatternBrush(foreground, background, ToFillPatternType(_group.FillPatternType));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called whenever the blocking group is changed.
        /// </summary>
        private void OnBlockingGroupChanged()
        {
            IPlanogramBlockingGroup newgroup = (this.Context != null) ? this.Context.GetBlockingGroup() : null;
            if (newgroup != _group)
            {
                if (_group != null)
                {
                    _group.PropertyChanged -= Group_PropertyChanged;
                }

                //update for the new group
                if (newgroup != null)
                {
                    newgroup.PropertyChanged += Group_PropertyChanged;

                    Brush foreground = new SolidColorBrush(ColorHelper.IntToColor(newgroup.Colour, true));
                    Brush background = Brushes.Transparent;
                    this.Background = WpfMaterialHelper.CreateRelativePatternBrush(foreground, background, ToFillPatternType(newgroup.FillPatternType));
                }
                else
                {
                    this.Background = Brushes.Transparent;
                }
                _group = newgroup;
            }
        }

        /// <summary>
        /// Sets the size and position of this control.
        /// </summary>
        private void UpdateSizeAndPosition()
        {
            if (this.Context == null) return;

            Size areaSize = BlockingEditor.GetCanvasSize(this);
            if (areaSize.IsEmpty) return;

            Canvas.SetLeft(this, Math.Max(0, areaSize.Width * Math.Min(1, this.Context.X)));
            Canvas.SetTop(this, Math.Max(0, areaSize.Height * Math.Min(1, this.Context.Y)));

            this.Width = Math.Max(0, areaSize.Width * Math.Min(1, this.Context.Width));
            this.Height = Math.Max(0, areaSize.Height * Math.Min(1, this.Context.Height));
        }

        /// <summary>
        /// Returns the center point of this block
        /// relative to the canvas.
        /// </summary>
        /// <returns></returns>
        public Point GetCenter()
        {
            return new Point(
                Canvas.GetLeft(this) + (this.Width / 2D),
                Canvas.GetTop(this) +(this.Height / 2D));

        }

        /// <summary>
        /// Converts a PlanogramBlockingFillPatternType to a ModelMaterial3D.FillPatternType
        /// </summary>
        private static ModelMaterial3D.FillPatternType ToFillPatternType(PlanogramBlockingFillPatternType fillPatternType)
        {
            switch (fillPatternType)
            {
                case PlanogramBlockingFillPatternType.Border: return ModelMaterial3D.FillPatternType.Border;
                case PlanogramBlockingFillPatternType.Crosshatch: return ModelMaterial3D.FillPatternType.Crosshatch;
                case PlanogramBlockingFillPatternType.DiagonalDown: return ModelMaterial3D.FillPatternType.DiagonalDown;
                case PlanogramBlockingFillPatternType.DiagonalUp: return ModelMaterial3D.FillPatternType.DiagonalUp;
                case PlanogramBlockingFillPatternType.Dotted: return ModelMaterial3D.FillPatternType.Dotted;
                case PlanogramBlockingFillPatternType.Horizontal: return ModelMaterial3D.FillPatternType.Horizontal;
                case PlanogramBlockingFillPatternType.Solid: return ModelMaterial3D.FillPatternType.Solid;
                case PlanogramBlockingFillPatternType.Vertical: return ModelMaterial3D.FillPatternType.Vertical;

                default:
                    Debug.Fail("PlanogramBlockingFillPatternType not handled");
                    return ModelMaterial3D.FillPatternType.Solid;
                
            }
        }

        private void UpdateBackground()
        {
            Brush foreground = new SolidColorBrush(ColorHelper.IntToColor(_group.Colour, true));
            Brush background = Brushes.Transparent;

            this.Background =
                WpfMaterialHelper.CreateRelativePatternBrush(foreground, background, ToFillPatternType(_group.FillPatternType));
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.Context = null;
                _isDisposed = true;
            }
        }

        #endregion
    }
}
