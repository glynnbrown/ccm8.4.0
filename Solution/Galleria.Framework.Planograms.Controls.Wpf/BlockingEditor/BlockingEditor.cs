﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion

#region Version History: CCM802
// V8-28997 : A.Silva
//  Added DividerPlaced RoutedEvent to notify when one of the dividers has been finally placed down (after DragCompleted).
#endregion

#region Version History : CCM 8.2.0
// V8-30792 : A.Kuszyk
//  Added support for a position highlight layer.
// V8-30854 : A.Kuszyk
//  Fixed issue with highlight layer being displayed everytime the viewport was resized.
// V8-30846 : A.Kuszyk
//  Protected ResizeWatermarkViewport against NaN and Infinity related exceptions.
// V8-30874 : A.Kuszyk
//  Updated ResetHighlightPlanogramVisual to only generate highlight colours if a highlight is present.
// V8-30982 : A.Kuszyk
//  Exposed Highlight viewport.
// V8-30995 : A.Probyn
//  Added defensive code to ResizeContentViewport for drag/drop issue with tiled windows.
// V8-31273 : A.Kuszyk
//  Ensured top-down components are rotated in highlight visual.
// V8-30982 : M.Shelley
//  Change the call to the ZoomToFitHeight method to pass false to disable the enlargement of the 
//  PlanogramViewport3D bounds so that the highlight position layer lines up with the underlying
//  highlight and watermark viewports
// V8-31501 : L.Ineson
//  Added IDisposable to make sure that this gets properly cleaned up.
#endregion
#region Version History : CCM 8.3.0
// V8-31651 : L.Ineson
//  Divider tool no longer gets reset back to pointer each time a divider is added.
// V8-31652 : L.Ineson
//  Added SelectedDivider property
// V8-32636 : A.Probyn
//  Added new ShowAnnotations to be set to the same  values as ShowPositions (it used to use this value anyway).
// CCM-18321 : Lee.Bailey
//  Multi-select Blocks & Dividers and apply certain settings to all selected
#endregion
#endregion

using Galleria.Framework.Planograms.Controls.Wpf.PlanogramViewport;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;

namespace Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor
{
    [TemplatePart(Name = BlockingEditor.PartContentLayoutKey, Type = typeof(Grid))]
    [TemplatePart(Name = BlockingEditor.BlockCanvasKey, Type = typeof(Panel))]
    public class BlockingEditor : Control, IDisposable
    {
        #region Constants
        const String PartContentLayoutKey = "PART_ContentLayout";
        const String BlockCanvasKey = "PART_BlockCanvas";
        internal static Double SnapToPercent = 0.0001D;
        #endregion

        #region Fields

        private Canvas _blockCanvas;
        private Canvas _dividerCanvas;
        private Grid _partContentLayout;
        private ScrollViewer _partScrollViewer;

        private PlanogramViewport3D _watermarkViewport;
        private Panel _watermarkPanel;

        private PlanogramViewport3D _highlightViewport;
        private Panel _highlightPanel;

        private readonly ObservableCollection<IPlanogramBlockingLocation> _selectedItems = new ObservableCollection<IPlanogramBlockingLocation>();

        private Boolean _suppressSelectionChangedHandlers;
        private Boolean _isMultiSelect = false; //indicates whether multiselect is currently on.

        private Rectangle _dividerShadow;

        private IPlanogramBlockingGroup _connectionFocus;

        private PlanogramViewModel _planogramVisualVM;
        private PlanogramViewModel _highlightVisualVM;

        #endregion

        #region Properties

        #region Blocking

        public static readonly DependencyProperty BlockingProperty =
            DependencyProperty.Register("Blocking", typeof(IPlanogramBlocking), typeof(BlockingEditor),
            new PropertyMetadata(null, OnBlockingPropertyChanged));

        private static void OnBlockingPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockingEditor)obj).OnBlockingChanged(e.OldValue as IPlanogramBlocking, e.NewValue as IPlanogramBlocking);
        }

        /// <summary>
        /// Gets/Sets the blocking source.
        /// </summary>
        public IPlanogramBlocking Blocking
        {
            get { return (IPlanogramBlocking)GetValue(BlockingProperty); }
            set { SetValue(BlockingProperty, value); }
        }

        /// <summary>
        /// Called whenever the blocking source changes.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnBlockingChanged(IPlanogramBlocking oldValue, IPlanogramBlocking newValue)
        {

            if (oldValue != null)
            {
                //unhook property changed
                //oldValue.PropertyChanged -= Blocking_PropertyChanged;

                //unhook dividers
                INotifyCollectionChanged notifiableDividers = oldValue.Dividers as INotifyCollectionChanged;
                if (notifiableDividers != null)
                {
                    notifiableDividers.CollectionChanged -= Dividers_CollectionChanged;
                }

                //hook locations
                INotifyCollectionChanged notifiableLocations = oldValue.Locations as INotifyCollectionChanged;
                if (notifiableLocations != null)
                {
                    notifiableLocations.CollectionChanged -= Locations_CollectionChanged;
                }

            }


            if (newValue != null)
            {
                //hook property changed
                // newValue.PropertyChanged += Blocking_PropertyChanged;

                //hook dividers
                INotifyCollectionChanged notifiableDividers = newValue.Dividers as INotifyCollectionChanged;
                if (notifiableDividers != null)
                {
                    notifiableDividers.CollectionChanged += Dividers_CollectionChanged;
                }

                //hook locations
                INotifyCollectionChanged notifiableLocations = newValue.Locations as INotifyCollectionChanged;
                if (notifiableLocations != null)
                {
                    notifiableLocations.CollectionChanged += Locations_CollectionChanged;
                }
            }

            ResetControls();
        }

        #endregion

        #region Watermark
        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.Register("Watermark", typeof(ImageSource), typeof(BlockingEditor),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the imagesource to use for the watermark.
        /// </summary>
        public ImageSource Watermark
        {
            get { return (ImageSource)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }
        #endregion

        #region Planogram

        public static readonly DependencyProperty PlanogramProperty =
            DependencyProperty.Register("Planogram", typeof(Planogram), typeof(BlockingEditor),
            new UIPropertyMetadata(null, OnPlanogramPropertyChanged));

        private static void OnPlanogramPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockingEditor)obj).ResetPlanogramVisual();
            ((BlockingEditor)obj).ResetHighlightPlanogramVisual();
        }

        public Planogram Planogram
        {
            get { return (Planogram)GetValue(PlanogramProperty); }
            set { SetValue(PlanogramProperty, value); }
        }

        #endregion

        #region VerticalRulerMax

        public static readonly DependencyProperty VerticalRulerMaxProperty =
            DependencyProperty.Register("VerticalRulerMax", typeof(Double), typeof(BlockingEditor),
            new UIPropertyMetadata(200D, OnVerticalRulerMaxPropertyChanged, OnVerticalRulerMaxCoerceValue));

        private static Object OnVerticalRulerMaxCoerceValue(DependencyObject obj, Object proposedValue)
        {
            //ensure value is not negative
            Double value = (Double)proposedValue;
            if (value < 0) return 0;
            return value;
        }

        private static void OnVerticalRulerMaxPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockingEditor)obj).ResizeContentViewport();
        }

        /// <summary>
        /// Gets/Sets the maximum value to show on the vertical ruler
        /// </summary>
        public Double VerticalRulerMax
        {
            get { return (Double)GetValue(VerticalRulerMaxProperty); }
            set { SetValue(VerticalRulerMaxProperty, value); }
        }

        #endregion

        #region HorizontalRulerMax

        public static readonly DependencyProperty HorizontalRulerMaxProperty =
            DependencyProperty.Register("HorizontalRulerMax", typeof(Double), typeof(BlockingEditor),
            new UIPropertyMetadata(1000D, OnHorizontalRulerMaxPropertyChanged, OnHorizontalRulerMaxCoerceValue));

        private static Object OnHorizontalRulerMaxCoerceValue(DependencyObject obj, Object proposedValue)
        {
            //ensure value is not negative
            Double value = (Double)proposedValue;
            if (value < 0) return 0;
            return value;
        }

        private static void OnHorizontalRulerMaxPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockingEditor)obj).ResizeContentViewport();
        }

        /// <summary>
        /// Gets/Sets the maximum value to show on the horizontal ruler
        /// </summary>
        public Double HorizontalRulerMax
        {
            get { return (Double)GetValue(HorizontalRulerMaxProperty); }
            set { SetValue(HorizontalRulerMaxProperty, value); }
        }

        #endregion

        #region RulerSize

        public static readonly DependencyProperty RulerSizeProperty =
            DependencyProperty.Register("RulerSize", typeof(Thickness), typeof(BlockingEditor),
            new PropertyMetadata(new Thickness(20)));

        public Thickness RulerSize
        {
            get { return (Thickness)GetValue(RulerSizeProperty); }
        }

        #endregion

        #region IsReadOnly

        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(Boolean), typeof(BlockingEditor),
            new PropertyMetadata(false));
        /// <summary>
        /// Gets/Sets whether the user can edit the blocking.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return (Boolean)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        #endregion

        #region ToolType

        public static readonly DependencyProperty ToolTypeProperty =
            DependencyProperty.Register("ToolType", typeof(BlockingToolType), typeof(BlockingEditor),
            new FrameworkPropertyMetadata(BlockingToolType.Pointer, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnToolTypePropertyChanged));

        private static void OnToolTypePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockingEditor)obj).OnToolTypeChanged((BlockingToolType)e.OldValue, (BlockingToolType)e.NewValue);
        }

        /// <summary>
        /// Gets/Sets the active tool type.
        /// </summary>
        public BlockingToolType ToolType
        {
            get { return (BlockingToolType)GetValue(ToolTypeProperty); }
            set { SetValue(ToolTypeProperty, value); }
        }

        private void OnToolTypeChanged(BlockingToolType oldValue, BlockingToolType newValue)
        {
            if (oldValue == newValue) return;

            if (oldValue == BlockingToolType.BlockConnector
                && _connectionFocus != null)
            {
                var firstItem = this.SelectedItems.FirstOrDefault();

                //update connections
                _connectionFocus.UpdateAssociatedLocations(this.SelectedItems.ToList());

                this.SelectedItem = firstItem;
            }

            if (newValue == BlockingToolType.BlockConnector)
            {
                _isMultiSelect = true;

                _connectionFocus =
                    (this.SelectedItem != null) ?
                    this.SelectedItem.GetBlockingGroup()
                    : null;

                if (_connectionFocus != null)
                {
                    //select all associated locations
                    _selectedItems.Clear();

                    foreach (var loc in _connectionFocus.GetBlockingLocations())
                    {
                        BlockArea area = GetBlockAreaFromLocation(loc);
                        area.IsSelected = true;
                    }
                }

            }
            else
            {
                _isMultiSelect = false;
                _connectionFocus = null;
            }
        }

        #endregion

        #region CanvasSize

        public static readonly DependencyProperty CanvasSizeProperty =
            DependencyProperty.RegisterAttached("CanvasSize", typeof(Size), typeof(BlockingEditor),
            new FrameworkPropertyMetadata(Size.Empty, FrameworkPropertyMetadataOptions.Inherits));

        public static Size GetCanvasSize(DependencyObject obj)
        {
            return (Size)obj.GetValue(CanvasSizeProperty);
        }

        private void UpdateCanvasSize()
        {
            if (_partContentLayout == null) return;
            this.SetValue(CanvasSizeProperty, new Size(_partContentLayout.Width, _partContentLayout.Height));
        }

        #endregion

        #region SelectedItem

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(IPlanogramBlockingLocation), typeof(BlockingEditor),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnSelectedItemPropertyChanged));

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public IPlanogramBlockingLocation SelectedItem
        {
            get { return (IPlanogramBlockingLocation)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        private static void OnSelectedItemPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingEditor senderControl = (BlockingEditor)obj;

            if (!senderControl._suppressSelectionChangedHandlers)
            {
                if (senderControl.SelectedItems.Count > 0)
                {
                    senderControl.SelectedItems.Clear();
                }

                if (e.NewValue != null)
                {
                    senderControl.SelectedItems.Add((IPlanogramBlockingLocation)e.NewValue);
                }
            }
        }

        #endregion

        #region SelectedItems
        /// <summary>
        /// Returns the editable collection of selected locations
        /// </summary>
        public ObservableCollection<IPlanogramBlockingLocation> SelectedItems
        {
            get { return _selectedItems; }
        }

        #endregion

        #region LabelFormat

        public static readonly DependencyProperty LabelFormatProperty =
          DependencyProperty.Register("LabelFormat", typeof(String), typeof(BlockingEditor),
          new PropertyMetadata(null));

        public String LabelFormat
        {
            get { return (String)GetValue(LabelFormatProperty); }
            set { SetValue(LabelFormatProperty, value); }
        }

        #endregion

        #region Highlight

        public static readonly DependencyProperty HighlightProperty =
            DependencyProperty.Register("Highlight", typeof(IPlanogramHighlight), typeof(BlockingEditor),
            new UIPropertyMetadata(null, OnHighlightPropertyChanged));

        private static void OnHighlightPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockingEditor)obj).ResetHighlightPlanogramVisual();
        }

        public IPlanogramHighlight Highlight
        {
            get { return (IPlanogramHighlight)GetValue(HighlightProperty); }
            set { SetValue(HighlightProperty, value); }
        }

        #endregion

        #region ShowHighlightLayer

        public static readonly DependencyProperty ShowHighlightLayerProperty =
            DependencyProperty.Register("ShowHighlightLayer", typeof(Boolean), typeof(BlockingEditor),
            new UIPropertyMetadata(false, OnShowHighlightLayerPropertyChanged));

        private static void OnShowHighlightLayerPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((BlockingEditor)obj).ResetHighlightPlanogramVisual();
        }

        public Boolean ShowHighlightLayer
        {
            get { return (Boolean)GetValue(ShowHighlightLayerProperty); }
            set { SetValue(ShowHighlightLayerProperty, value); }
        }

        #endregion

        #region ZoomMode

        public static readonly DependencyProperty ZoomModeProperty =
            DependencyProperty.Register("ZoomMode", typeof(BlockingZoomMode), typeof(BlockingEditor),
            new UIPropertyMetadata(BlockingZoomMode.FitToHeight, OnZoomModePropertyChanged));

        private static void OnZoomModePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingEditor senderControl = (BlockingEditor)obj;

            senderControl.ResetZoomPercent();

            if ((BlockingZoomMode)e.NewValue != BlockingZoomMode.Manual)
            {
                senderControl.ResizeContentViewport();
            }
        }

        /// <summary>
        /// Gets/Sets the current zoom mode.
        /// </summary>
        public BlockingZoomMode ZoomMode
        {
            get { return (BlockingZoomMode)GetValue(ZoomModeProperty); }
            set { SetValue(ZoomModeProperty, value); }

        }

        #endregion

        #region ZoomPercent

        public static readonly DependencyProperty ZoomPercentProperty =
            DependencyProperty.Register("ZoomPercent", typeof(Double), typeof(BlockingEditor),
            new PropertyMetadata(Double.NaN, OnZoomPercentPropertyChanged));

        private static void OnZoomPercentPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingEditor senderControl = (BlockingEditor)obj;
            if (senderControl.ZoomMode != BlockingZoomMode.Manual) return;

            if ((Double)e.NewValue != Double.NaN)
            {
                ((BlockingEditor)obj).ResizeContentViewport();
            }
        }

        public Double ZoomPercent
        {
            get { return (Double)GetValue(ZoomPercentProperty); }
            set { SetValue(ZoomPercentProperty, value); }
        }

        private void ResetZoomPercent()
        {
            if (this.ZoomMode == BlockingZoomMode.Manual)
            {
                if (_partScrollViewer.ViewportHeight == 0
                    || _partScrollViewer.ViewportWidth == 0)
                {
                    this.ZoomPercent = 100;
                    return;
                }


                //get the current percent.
                if (this.HorizontalRulerMax > this.VerticalRulerMax)
                {
                    this.ZoomPercent = (_partContentLayout.Width / _partScrollViewer.ViewportWidth) * 100;
                }
                else
                {
                    this.ZoomPercent = (_partContentLayout.Height / _partScrollViewer.ViewportHeight) * 100;
                }

            }
            else
            {
                this.ZoomPercent = Double.NaN;
            }
        }

        #endregion

        /// <summary>
        /// Exposes the blocking editor's viewport that is used to display the highlight view.
        /// </summary>
        public PlanogramViewport3D HighlightViewport { get { return _highlightViewport; } }

        #region SelectedDivider

        /// <summary>
        /// SelectedDivider dependency property definition.
        /// </summary>
        public static readonly DependencyProperty SelectedDividerProperty =
            DependencyProperty.Register("SelectedDivider", typeof(IPlanogramBlockingDivider), typeof(BlockingEditor),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnSelectedDividerPropertyChanged));

        /// <summary>
        /// Gets/Sets the selected divider
        /// </summary>
        public IPlanogramBlockingDivider SelectedDivider
        {
            get { return (IPlanogramBlockingDivider)GetValue(SelectedDividerProperty); }
            set { SetValue(SelectedDividerProperty, value); }
        }

        /// <summary>
        /// Called whenever the value of SelectedDividerProperty changes.
        /// </summary>
        private static void OnSelectedDividerPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingEditor senderControl = (BlockingEditor)obj;

            IPlanogramBlockingDivider selectedDiv = e.NewValue as IPlanogramBlockingDivider;

            if (senderControl._dividerCanvas == null) return;
            foreach (Divider d in senderControl._dividerCanvas.Children.OfType<Divider>())
            {
                d.IsSelected = (d.Context == selectedDiv);
            }

        }

        #endregion

        #endregion

        #region Events

        #region SelectionChanged

        public static readonly RoutedEvent SelectionChangedEvent =
            EventManager.RegisterRoutedEvent("SelectionChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(BlockingEditor));

        public event RoutedEventHandler SelectionChanged
        {
            add { AddHandler(SelectionChangedEvent, value); }
            remove { RemoveHandler(SelectionChangedEvent, value); }
        }

        protected virtual void OnSelectionChanged()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(BlockingEditor.SelectionChangedEvent));
        }

        #endregion

        #region DividerPlaced

        /// <summary>
        ///     Identifies the <see cref="DividerPlaced"/> routed event.
        /// </summary>
        /// <returns>The identifier for the <see cref="DividerPlaced"/> routed event.</returns>
        public static readonly RoutedEvent DividerPlacedEvent =
            EventManager.RegisterRoutedEvent("DividerPlaced", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(BlockingEditor));

        /// <summary>
        ///     Occurs when the <see cref="Divider"/> stops being dragged.
        /// </summary>
        public event RoutedEventHandler DividerPlaced
        {
            add { AddHandler(DividerPlacedEvent, value); }
            remove { RemoveHandler(DividerPlacedEvent, value); }
        }

        /// <summary>
        ///     Raise the <see cref="DividerPlaced"/> routed event.
        /// </summary>
        private void NotifyDividerPlaced()
        {
            RaiseEvent(new RoutedEventArgs(DividerPlacedEvent));
        }

        #endregion

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override.")]
        static BlockingEditor()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(BlockingEditor), new FrameworkPropertyMetadata(typeof(BlockingEditor)));
        }

        public BlockingEditor()
        {
            this.AddHandler(BlockArea.SelectedEvent, (RoutedEventHandler)OnBlockAreaSelected);
            this.AddHandler(BlockArea.DeselectedEvent, (RoutedEventHandler)OnBlockAreaDeselected);
            this.AddHandler(Divider.SelectedEvent, (RoutedEventHandler)OnDividerSelected);
            this.AddHandler(Divider.PlacedEvent, (RoutedEventHandler)OnDividerPlaced);
            _selectedItems.CollectionChanged += SelectedItems_CollectionChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the template for this control has been applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            // We apply this transform the the canvas controls in order to get the x axis at the bottom of the control.
            var canvasScaleTransform = new ScaleTransform()
            {
                ScaleX = 1,
                ScaleY = -1,
                CenterX = 0.5D,
                CenterY = 0.5D
            };

            // Prepare the watermark panel.
            _watermarkViewport = this.GetTemplateChild("PART_WatermarkViewport") as PlanogramViewport3D;
            _watermarkViewport.Loaded += WatermarkViewport_Loaded;
            _watermarkPanel = this.GetTemplateChild("PART_WatermarkPanel") as Panel; 

            // Prepare the block canvas.
            _blockCanvas = this.GetTemplateChild(BlockCanvasKey) as Canvas;
            if (_blockCanvas == null) throw new ArgumentNullException();
            _blockCanvas.LayoutTransform = canvasScaleTransform;

            // Event handlers are added to the block canvas, because this has a background.
            // The divider canvas has a null background, allowing click events to reach BlockAreas.
            // However, this prevents mouse enter, leave and move events from being handled. As such
            // we attach them to the block canvas, despite the fact that they operate on the 
            // divider canvas.
            _blockCanvas.MouseEnter += BlockCanvas_MouseEnter;
            _blockCanvas.MouseLeave += BlockCanvas_MouseLeave;
            _blockCanvas.MouseMove += BlockCanvas_MouseMove; 

            // Prepare the highlight panel.
            _highlightViewport = this.GetTemplateChild("PART_HighlightViewport") as PlanogramViewport3D;
            _highlightViewport.Loaded += HighlightViewport_Loaded;
            _highlightPanel = this.GetTemplateChild("PART_HighlightPanel") as Panel;

            // Prepare the divider canvas.
            _dividerCanvas = this.GetTemplateChild("PART_DividerCanvas") as Canvas;
            if (_dividerCanvas == null) throw new ArgumentNullException();
            _dividerCanvas.LayoutTransform = canvasScaleTransform;
            _dividerCanvas.PreviewMouseLeftButtonDown += DividerCanvas_PreviewMouseLeftButtonDown; // Handles divider click events.

            // Prepare the scrollview, viewport and content layout.
            _partScrollViewer = this.GetTemplateChild("PART_Scrollviewer") as ScrollViewer;
            var viewportDPD = DependencyPropertyDescriptor.FromProperty(ScrollViewer.ViewportHeightProperty, typeof(ScrollViewer));
            viewportDPD.AddValueChanged(_partScrollViewer, OnViewportSizeChanged);
            viewportDPD = DependencyPropertyDescriptor.FromProperty(ScrollViewer.ViewportWidthProperty, typeof(ScrollViewer));
            viewportDPD.AddValueChanged(_partScrollViewer, OnViewportSizeChanged);
            _partContentLayout = this.GetTemplateChild(PartContentLayoutKey) as Grid;
            if (_partContentLayout == null) throw new ArgumentNullException();

            ResizeContentViewport();

            // Update the plan viewports and, the panels that they are brushed to.
            ResetPlanogramVisual();
            ResetHighlightPlanogramVisual();                
        }


        /// <summary>
        /// Called whenever the mouse wheel is moved whilst this control
        /// is focused.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);

            if (e.Delta < 0)
            {
                ZoomOut();
            }
            else if (e.Delta > 0)
            {
                ZoomIn();
            }
        }

        #region Canvas events

        /// <summary>
        /// Called whenever the mouse enters the canvas area.
        /// </summary>
        private void BlockCanvas_MouseEnter(object sender, MouseEventArgs e)
        {
            switch (this.ToolType)
            {
                case BlockingToolType.HorizontalDivider:
                    ShowDividerShadow(e.GetPosition(_dividerCanvas), PlanogramBlockingDividerType.Horizontal);
                    break;

                case BlockingToolType.VerticalDivider:
                    ShowDividerShadow(e.GetPosition(_dividerCanvas), PlanogramBlockingDividerType.Vertical);
                    break;
            }
        }

        /// <summary>
        /// Called whenever the mouse leaves the canvas area.
        /// </summary>
        private void BlockCanvas_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!_dividerCanvas.IsMouseOver)
            {
                //remove the divider shadow.
                if (_dividerShadow != null)
                {
                    _dividerCanvas.Children.Remove(_dividerShadow);
                    _dividerShadow = null;
                }
            }
        }

        /// <summary>
        /// Called whenever the mouse moves over the canvas
        /// </summary>
        private void BlockCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            switch (this.ToolType)
            {
                case BlockingToolType.HorizontalDivider:
                    ShowDividerShadow(e.GetPosition(_dividerCanvas), PlanogramBlockingDividerType.Horizontal);
                    break;

                case BlockingToolType.VerticalDivider:
                    ShowDividerShadow(e.GetPosition(_dividerCanvas), PlanogramBlockingDividerType.Vertical);
                    break;
            }
        }

        /// <summary>
        /// Called whenever the user clicks the canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DividerCanvas_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (this.ToolType)
            {
                case BlockingToolType.Pointer:
                case BlockingToolType.BlockConnector:
                    // Do nothing - click events are handled in BlockArea.
                    break;

                case BlockingToolType.HorizontalDivider:
                    {
                        e.Handled = true;

                        //add a new horizontal divider at the mouse position.
                        AddNewDivider(e.GetPosition(_dividerCanvas), /*horiz*/true);
                    }
                    break;

                case BlockingToolType.VerticalDivider:
                    {
                        e.Handled = true;

                        //add a new vertical divider at the mouse position.
                        AddNewDivider(e.GetPosition(_dividerCanvas), /*horiz*/false);
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the size of the content layout changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewportSizeChanged(object sender, EventArgs e)
        {
            ResizeContentViewport();
        }

        #endregion

        #region Divider Events

        /// <summary>
        /// Called whenever the dividers collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dividers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ResetControls();
        }

        /// <summary>
        ///     Responds to the <see cref="Divider.Placed"/> routed event as it bubbles up from the dividers in this instance.
        /// </summary>
        /// <remarks>Just notifies a <see cref="DividerPlaced"/> routed event.</remarks>
        private void OnDividerPlaced(Object sender, RoutedEventArgs e)
        {
            NotifyDividerPlaced();
        }

        /// <summary>
        /// Responds to the selection of a divider
        /// </summary>
        private void OnDividerSelected(Object sender, RoutedEventArgs e)
        {
            this.SelectedDivider = ((Divider)e.OriginalSource).Context;
        }

        #endregion

        #region Block Area Events

        /// <summary>
        /// Called whenever the locations collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Locations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ResetControls();
        }

        /// <summary>
        /// Responds to changes in the selected items collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //if (!_suppressSelectionChangedHandlers)
            //{
            //    _suppressSelectionChangedHandlers = true;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (IPlanogramBlockingLocation loc in e.NewItems)
                    {
                        BlockArea area = GetBlockAreaFromLocation(loc);
                        if (area != null)
                        {
                            area.IsSelected = true;
                        }
                    }
                    break;


                case NotifyCollectionChangedAction.Remove:
                    foreach (IPlanogramBlockingLocation loc in e.OldItems)
                    {
                        BlockArea area = GetBlockAreaFromLocation(loc);
                        if (area != null)
                        {
                            area.IsSelected = false;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_blockCanvas != null)
                        {
                            //ensure all required areas are selected
                            foreach (BlockArea area in _blockCanvas.Children.OfType<BlockArea>())
                            {
                                area.IsSelected = (_selectedItems.Contains(area.Context));
                            }
                        }
                    }
                    break;
            }


            //set the selected item property
            Boolean oldSuppress = _suppressSelectionChangedHandlers;
            _suppressSelectionChangedHandlers = true;
            this.SelectedItem = _selectedItems.LastOrDefault();
            _suppressSelectionChangedHandlers = oldSuppress;


            OnSelectionChanged();


            //_suppressSelectionChangedHandlers = false;
            //}
        }

        /// <summary>
        /// Responds to the selection of a block area
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBlockAreaSelected(Object sender, RoutedEventArgs e)
        {
            if (!_suppressSelectionChangedHandlers)
            {
                _suppressSelectionChangedHandlers = true;
                BlockArea senderArea = (BlockArea)e.OriginalSource;
                if (senderArea != null)
                {

                    if (ToolType == BlockingToolType.Pointer && Keyboard.Modifiers == ModifierKeys.Control)
                    {

                        if( this.SelectedItems.Contains(senderArea.Context))
                        {
                            if (this.SelectedItems.Count > 1)
                            {
                                _selectedItems.Remove(senderArea.Context);                                
                                OnSelectionChanged();
                            }
                        }
                        else
                        {
                            _selectedItems.Add(senderArea.Context);
                            OnSelectionChanged();
                        }
                    }
                    else
                    {

                        //check if we should actually deselect first
                        if (_isMultiSelect
                            && this.SelectedItems.Count > 1
                            && this.SelectedItems.Contains(senderArea.Context))
                        {
                            senderArea.IsSelected = false;
                            _selectedItems.Remove(senderArea.Context);
                        }
                        else
                        {
                            //process the selection
                            Boolean keepExistingSelection = _isMultiSelect;

                            if (!keepExistingSelection)
                            {
                                _selectedItems.Clear();
                            }
                            if (!_selectedItems.Contains(senderArea.Context))
                            {
                                _selectedItems.Add(senderArea.Context);
                            }

                            //raise out the event
                            OnSelectionChanged();
                        }
                    }
                }

                _suppressSelectionChangedHandlers = false;
            }
        }

        /// <summary>
        /// Responds to the deselect of a block area
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBlockAreaDeselected(Object sender, RoutedEventArgs e)
        {
            if (!_suppressSelectionChangedHandlers)
            {
                _suppressSelectionChangedHandlers = true;

                BlockArea senderArea = (BlockArea)e.OriginalSource;
                if (senderArea != null)
                {
                    //process the selection
                    if (this.SelectedItems.Count > 0)
                    {
                        Boolean itemRemoved =
                            this.SelectedItems.Remove(senderArea.Context);

                        if (itemRemoved)
                        {
                            OnSelectionChanged();
                        }
                    }
                }

                _suppressSelectionChangedHandlers = false;
            }
        }

        #endregion

        /// <summary>
        /// Called when the watermark viewport gets loaded.
        /// </summary>
        private void WatermarkViewport_Loaded(object sender, RoutedEventArgs e)
        {
            _watermarkViewport.Loaded -= WatermarkViewport_Loaded;
            ResetPlanogramVisual();
        }

        private void HighlightViewport_Loaded(object sender, RoutedEventArgs e)
        {
            _highlightViewport.Loaded -= HighlightViewport_Loaded;
            ResetHighlightPlanogramVisual();
        }

        #endregion

        #region Methods

        private void ResizeContentViewport()
        {
            if (_partContentLayout == null) return;
            if (this.ActualHeight == 0) return;
            if (this.ActualWidth == 0) return;
            if (this.VerticalRulerMax == 0) return;
            if (this.HorizontalRulerMax == 0) return;
            if (_partScrollViewer.ViewportHeight == 0) return;
            if (_partScrollViewer.ViewportWidth == 0) return;

            Double viewportHeight = _partScrollViewer.ViewportHeight - this.RulerSize.Top - this.RulerSize.Bottom;
            Double viewportWidth = _partScrollViewer.ViewportWidth - this.RulerSize.Left - this.RulerSize.Right;
            Double viewportWidthToHeight = viewportWidth / viewportHeight;


            //resize according to the current zoom mode.
            if (this.ZoomMode == BlockingZoomMode.FitToHeight
                || (this.ZoomMode == BlockingZoomMode.FitAll && this.VerticalRulerMax > this.HorizontalRulerMax))
            {
                //Fit to height.
                Double scale = viewportHeight / this.VerticalRulerMax;
                _partContentLayout.Width = (this.HorizontalRulerMax * scale >= 0) ? (this.HorizontalRulerMax * scale) : 0;
                _partContentLayout.Height = viewportHeight;
            }
            else if (this.ZoomMode == BlockingZoomMode.FitAll)
            {
                //fit to width
                Double scale = viewportWidth / this.HorizontalRulerMax;
                Double newWidth = (this.HorizontalRulerMax * scale >= 0) ? (this.HorizontalRulerMax * scale) : 0;
                Double newHeight = (this.VerticalRulerMax * scale >= 0) ? (this.VerticalRulerMax * scale) : 0;

                while (newHeight > viewportHeight)
                {
                    scale -= 0.1;
                    newWidth = this.HorizontalRulerMax * scale;
                    newHeight = this.VerticalRulerMax * scale;
                }

                _partContentLayout.Width = newWidth;
                _partContentLayout.Height = newHeight;
            }
            else if (this.ZoomMode == BlockingZoomMode.Manual)
            {
                if (this.HorizontalRulerMax > this.VerticalRulerMax)
                {
                    Double heightToWidth = this.VerticalRulerMax / this.HorizontalRulerMax;
                    _partContentLayout.Width = viewportWidth * (this.ZoomPercent / 100);
                    _partContentLayout.Height = viewportWidth * heightToWidth * (this.ZoomPercent / 100);
                }
                else
                {
                    Double widthToHeight = this.HorizontalRulerMax / this.VerticalRulerMax;
                    _partContentLayout.Height = viewportHeight * (this.ZoomPercent / 100);
                    _partContentLayout.Width = viewportHeight * widthToHeight * (this.ZoomPercent / 100);

                }
            }


            UpdateCanvasSize();
            ResetControls();

            //make sure the viewport visual updates.
            ResizeWatermarkViewport(_watermarkViewport, _watermarkPanel);
            ResizeWatermarkViewport(_highlightViewport, _highlightPanel,this.ShowHighlightLayer);
        }

        /// <summary>
        /// Called whenever the dividers should be completely redrawn.
        /// </summary>
        private void ResetControls()
        {
            if (_blockCanvas == null || _dividerCanvas == null) return;

            IEnumerable<IPlanogramBlockingLocation> selectedLocations = this.SelectedItems.ToList();

            //remove all existing controls from the canvas.
            List<IDisposable> disposableKids = _blockCanvas.Children
                .OfType<IDisposable>()
                .Union(_dividerCanvas.Children.OfType<IDisposable>())
                .ToList();
            _blockCanvas.Children.Clear();
            _dividerCanvas.Children.Clear();
            foreach (var d in disposableKids)
            {
                d.Dispose();
            }

            _dividerShadow = null;

            if (this.Blocking != null)
            {
                //draw out the new areas
                foreach (var loc in this.Blocking.Locations)
                {
                    _blockCanvas.Children.Add(new BlockArea(loc));
                }

                //draw out the new dividers
                foreach (var div in this.Blocking.Dividers)
                {
                    _dividerCanvas.Children.Add(new Divider(div));
                }

                //reselect the selected items
                foreach (IPlanogramBlockingLocation item in selectedLocations)
                {
                    BlockArea area = GetBlockAreaFromLocation(item);
                    if (area != null)
                    {
                        area.IsSelected = true;
                    }
                }
            }

        }

        /// <summary>
        /// Returns the block area control for the given location.
        /// </summary>
        private BlockArea GetBlockAreaFromLocation(IPlanogramBlockingLocation loc)
        {
            if (_blockCanvas == null) return null;
            if (loc == null) return null;

            foreach (BlockArea area in _blockCanvas.Children.OfType<BlockArea>())
            {
                if (area.Context == loc)
                {
                    return area;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the percentage position of the given canvas point.
        /// </summary>
        /// <param name="mousePoint"></param>
        /// <returns></returns>
        internal Point GetPercentagePosition(Point canvasPoint)
        {
            if (_partContentLayout == null) return canvasPoint;

            return
                new Point(canvasPoint.X / _partContentLayout.ActualWidth,
                    canvasPoint.Y / _partContentLayout.ActualHeight);
        }


        /// <summary>
        /// Adds a new divider at the given position
        /// </summary>
        /// <param name="canvasPoint">canvas relative position</param>
        /// <param name="isHorizontal">If true a horiz divider will be added, if false then a vertical </param>
        internal void AddNewDivider(Point canvasPoint, Boolean isHorizontal)
        {
            if (this.Blocking == null) return;

            Point percentPos = GetPercentagePosition(canvasPoint);

            //snap the percentage
            percentPos.X = Math.Round(percentPos.X / BlockingEditor.SnapToPercent, MidpointRounding.AwayFromZero) * BlockingEditor.SnapToPercent;
            percentPos.Y = Math.Round(percentPos.Y / BlockingEditor.SnapToPercent, MidpointRounding.AwayFromZero) * BlockingEditor.SnapToPercent;

            IPlanogramBlockingDivider newDivider =
            this.Blocking.AddNewDivider(Convert.ToSingle(percentPos.X), Convert.ToSingle(percentPos.Y),
                (isHorizontal) ? Model.PlanogramBlockingDividerType.Horizontal : Model.PlanogramBlockingDividerType.Vertical);

            //select the new divider.
            this.SelectedDivider = newDivider;
        }

        /// <summary>
        /// Shows the divider hover shadow.
        /// </summary>
        private void ShowDividerShadow(Point canvasPoint, PlanogramBlockingDividerType dividerType)
        {
            if (_dividerShadow == null)
            {
                _dividerShadow = new Rectangle();
                _dividerShadow.Fill = Brushes.Black;
                _dividerCanvas.Children.Add(_dividerShadow);
            }

            Boolean isHorizontal = (dividerType == PlanogramBlockingDividerType.Horizontal);
            Point percentPoint = GetPercentagePosition(canvasPoint);
            Canvas cvs = _dividerCanvas;

            //get the values for the new divider
            Single dividerX, dividerY, dividerLength;
            Byte dividerLevel;
            BlockingHelper.GetDividerValues(this.Blocking,
                Convert.ToSingle(percentPoint.X), Convert.ToSingle(percentPoint.Y), dividerType,
                out dividerX, out dividerY, out dividerLength, out dividerLevel);

            //update the draw path
            Double drawWidth = (isHorizontal) ? (cvs.ActualWidth * dividerLength) : 3;
            Double drawHeight = (!isHorizontal) ? (cvs.ActualHeight * dividerLength) : 3;

            Double left = Math.Max(0, cvs.ActualWidth * Math.Min(1, dividerX));
            if (!isHorizontal) left -= (drawWidth / 2D);

            Double top = Math.Max(0, cvs.ActualHeight * Math.Min(1, dividerY));
            if (isHorizontal) top -= (drawHeight / 2D);


            Canvas.SetLeft(_dividerShadow, left);
            Canvas.SetTop(_dividerShadow, top);

            _dividerShadow.Width = drawWidth;
            _dividerShadow.Height = drawHeight;
        }

        /// <summary>
        /// Reloads the planogram visual
        /// </summary>
        private void ResetPlanogramVisual()
        {
            PlanogramViewport3D viewport = _watermarkViewport;
            if (viewport == null) return;
            if (!viewport.IsLoaded) return;

            //drop the old vm.
            viewport.Planogram = null;
            if (_planogramVisualVM != null)
            {
                _planogramVisualVM.Dispose();
                _planogramVisualVM = null;
            }

            
            _watermarkPanel.Background = null;

            if (this.Planogram == null) return;

            //create a basic visual of only the fixture.
            PlanRenderSettings renderSettings =
                new PlanRenderSettings()
                {
                    ShowChestWalls = false,
                    RotateTopDownComponents = true,
                    ShowShelfRisers = false,
                    ShowFixtureImages = false,
                    ShowFixtureFillPatterns = false,
                    ShowNotches = true,
                    ShowPegHoles = true,
                    ShowDividerLines = false,
                    ShowPositions = true,
                    ShowAnnotations = true,
                    ShowPositionUnits = false,
                    ShowDividers = false,
                };


            viewport.RenderSettings = renderSettings;
            _planogramVisualVM = new PlanogramViewModel(this.Planogram);
            viewport.Planogram = _planogramVisualVM.DesignView;

            ResizeWatermarkViewport(_watermarkViewport, _watermarkPanel);
        }

        private void ResetHighlightPlanogramVisual()
        {
            PlanogramViewport3D viewport = _highlightViewport;
            if (viewport == null) return;
            //if (!viewport.IsLoaded) return;

            //drop the old vm.
            viewport.Planogram = null;
            if (_highlightVisualVM != null)
            {
                _highlightVisualVM.Dispose();
                _highlightVisualVM = null;
            }

            if (this.Planogram == null) return;

            //create a basic visual of only the fixture.
            PlanRenderSettings renderSettings =
                new PlanRenderSettings()
                {
                    ShowChestWalls = false,
                    RotateTopDownComponents = true,
                    ShowShelfRisers = false,
                    ShowFixtureImages = false,
                    ShowFixtureFillPatterns = false,
                    ShowNotches = false,
                    ShowPegHoles = false,
                    ShowDividerLines = false,
                    ShowPositions = true,
                    ShowAnnotations = true,
                    ShowPositionUnits = false,
                    ShowDividers = false,
                    PositionHighlights = 
                        this.Highlight == null ? 
                        null : 
                        new PlanogramPositionHighlightColours(PlanogramHighlightHelper.ProcessPositionHighlight(Planogram, Highlight)),
                    ShowFixtures = false,
                };

            viewport.RenderSettings = renderSettings;
            _highlightVisualVM= new PlanogramViewModel(this.Planogram);
            viewport.Planogram = _highlightVisualVM.DesignView;


            ResizeWatermarkViewport(_highlightViewport, _highlightPanel, ShowHighlightLayer);
        }

        private void ResizeWatermarkViewport(PlanogramViewport3D viewport, Panel panel, Boolean showLayer = true)
        {
            if (viewport == null) return;
            if (viewport.RenderSettings == null) return;
            //if (!viewport.IsLoaded) return;

            // Get the width and height of the content layout part, being careful about the fact that the values
            // might be NaN or Infinity.
            Double partContentLayoutHeight = 
                (Double.IsNaN(_partContentLayout.Height) || Double.IsInfinity(_partContentLayout.Height)) ?
                0d :
                _partContentLayout.Height;
            Double partContentLayoutWidth =
                (Double.IsNaN(_partContentLayout.Width) || Double.IsInfinity(_partContentLayout.Width)) ?
                0d :
                _partContentLayout.Width;

            viewport.Height = partContentLayoutHeight;
            viewport.Width = partContentLayoutWidth;
            viewport.InvalidateVisual();
            viewport.Arrange(new Rect(new Size(partContentLayoutWidth, partContentLayoutHeight)));

            viewport.ZoomToFitHeight(false);

            //get the bounding rect of the model

            //get the bounding area that we want to show.
            //Note - this is not necessarily the full bounds of the plan as there may 
            //be overhanging products/ carpark shelf that we dont want to include.

            Rect3D planBounds = viewport.GetModel3DBounds();
            if (planBounds == Rect3D.Empty) return;

            Rect3D zoomingBounds =
                new Rect3D(0, planBounds.Y, 0, this.HorizontalRulerMax, this.VerticalRulerMax, planBounds.SizeZ);


            Rect planRect = viewport.Get2DBounds(zoomingBounds);
            if (planRect == Rect.Empty) return;

            //apply an visualbrush of this viewport as the content layout background
            VisualBrush watermarkBrush = new VisualBrush(viewport);
            watermarkBrush.Opacity = GetPanelOpacity(panel);

            //set the brush viewport.
            watermarkBrush.ViewboxUnits = BrushMappingMode.Absolute;
            watermarkBrush.Viewbox = planRect;

            watermarkBrush.Viewport = new Rect(0, 0, 1, 1);
            watermarkBrush.ViewportUnits = BrushMappingMode.RelativeToBoundingBox;

            panel.Background = showLayer ? watermarkBrush : null; 
        }

        private double GetPanelOpacity(Panel panel)
        {
            if (panel == _watermarkPanel) return 0.4f;
            return 1f;
        }

        /// <summary>
        /// Zooms in on the blocking area.
        /// </summary>
        public void ZoomIn()
        {
            //set the zoom mode to manual
            this.ZoomMode = BlockingZoomMode.Manual;

            if (this.ZoomPercent < 500)
            {
                this.ZoomPercent += 10;
            }
        }

        /// <summary>
        /// Zooms out the blocking area.
        /// </summary>
        public void ZoomOut()
        {
            //set the zoom mode to manual
            this.ZoomMode = BlockingZoomMode.Manual;

            if (this.ZoomPercent > 100)
            {
                this.ZoomPercent -= 10;
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (_isDisposed) return;

            this.Planogram = null;

            //drop dpds
            if (_partScrollViewer != null)
            {
                var viewportDPD = DependencyPropertyDescriptor.FromProperty(ScrollViewer.ViewportHeightProperty, typeof(ScrollViewer));
                viewportDPD.RemoveValueChanged(_partScrollViewer, OnViewportSizeChanged);
                viewportDPD = DependencyPropertyDescriptor.FromProperty(ScrollViewer.ViewportWidthProperty, typeof(ScrollViewer));
                viewportDPD.RemoveValueChanged(_partScrollViewer, OnViewportSizeChanged);
            }

            _isDisposed = true;
        }

        //~BlockingEditor()
        //{
        //    Dispose(false);
        //}

        #endregion
    }

    /// <summary>
    /// Denotes the different tooltypes that may be used by this
    /// control.
    /// </summary>
    public enum BlockingToolType
    {
        Pointer,
        VerticalDivider,
        HorizontalDivider,
        BlockConnector
    }

    public enum BlockingZoomMode
    {
        FitAll,
        FitToHeight,
        Manual
    }

}
