﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion

#region Version History: CCM802

// V8-28997 : A.Silva
//      Added Placed RoutedEvent to notify when the divider has been finally placed down (after DragCompleted).

#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor
{

    public class Divider : Thumb, IDisposable
    {
        #region Properties

        #region Context

        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register("Context", typeof(IPlanogramBlockingDivider), typeof(Divider),
            new PropertyMetadata(null, OnContextPropertyChanged));

        private static void OnContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Divider)obj).OnContextChanged(e.OldValue as IPlanogramBlockingDivider, e.NewValue as IPlanogramBlockingDivider);
        }

        public IPlanogramBlockingDivider Context
        {
            get { return (IPlanogramBlockingDivider)GetValue(ContextProperty); }
            private set { SetValue(ContextProperty, value); }
        }

        private void OnContextChanged(IPlanogramBlockingDivider oldValue, IPlanogramBlockingDivider newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= Context_PropertyChanged;
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += Context_PropertyChanged;
            }

        }

        #endregion

        #region Orientation

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(Divider),
            new PropertyMetadata(Orientation.Vertical));

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        #endregion

        #region IsSelected

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(Boolean), typeof(Divider),
            new PropertyMetadata(false, OnIsSelectedPropertyChanged));

        private static void OnIsSelectedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            Divider senderControl = (Divider)obj;

            if ((Boolean)e.NewValue) senderControl.RaiseSelectedEvent();
        }

        /// <summary>
        /// Gets/Sets whether this divider is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return (Boolean)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        //#region Updated

        //public static readonly RoutedEvent UpdatedEvent =
        //    EventManager.RegisterRoutedEvent("Updated", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Divider));

        //public event RoutedEventHandler Updated
        //{
        //    add { AddHandler(UpdatedEvent, value); }
        //    remove { RemoveHandler(UpdatedEvent, value); }
        //}

        //protected virtual void NotifyUpdated()
        //{
        //    //raise out the selected event
        //    RaiseEvent(new RoutedEventArgs(Divider.UpdatedEvent));
        //}

        //#endregion

        #region Placed

        /// <summary>
        ///     Identifies the <see cref="Placed"/> routed event.
        /// </summary>
        /// <returns>The identifier for the <see cref="Placed"/> routed event.</returns>
        public static readonly RoutedEvent PlacedEvent = 
            EventManager.RegisterRoutedEvent("Placed", RoutingStrategy.Bubble, typeof (RoutedEventHandler), typeof (Divider));

        /// <summary>
        ///     Occurs when the <see cref="Divider"/> stops being dragged.
        /// </summary>
        public event RoutedEventHandler Placed
        {
            add { AddHandler(PlacedEvent, value); }
            remove { RemoveHandler(PlacedEvent, value); }
        }

        /// <summary>
        ///     Raise the <see cref="Placed"/> routed event.
        /// </summary>
        private void NotifyPlaced()
        {
            RaiseEvent(new RoutedEventArgs(PlacedEvent));
        }

        #endregion

        #region Selected

        /// <summary>
        /// Event to notify when this divider has been selected.
        /// </summary>
        public static readonly RoutedEvent SelectedEvent =
            EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Divider));

        /// <summary>
        /// Event to notify when this divider has been selected.
        /// </summary>
        public event RoutedEventHandler Selected
        {
            add { AddHandler(SelectedEvent, value); }
            remove { RemoveHandler(SelectedEvent, value); }
        }

        /// <summary>
        ///Raises the Selected event
        /// </summary>
        private void RaiseSelectedEvent()
        {
            RaiseEvent(new RoutedEventArgs(SelectedEvent));
        }

        #endregion

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override.")]
        static Divider()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(Divider), new FrameworkPropertyMetadata(typeof(Divider)));
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Divider(IPlanogramBlockingDivider context)
        {
            this.Context = context;
            this.Orientation = (context.Type == PlanogramBlockingDividerType.Vertical) ? Orientation.Vertical : Orientation.Horizontal;

            this.DragDelta += Divider_DragDelta;
            this.DragCompleted += OnDragCompleted;
            this.Loaded += Divider_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called on initial load
        /// </summary>
        private void Divider_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= Divider_Loaded;
            UpdateSizeAndPosition();
        }

        /// <summary>
        /// Called whenever the user tries to drag this divider
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Divider_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Size areaSize = BlockingEditor.GetCanvasSize(this);
            if (areaSize.IsEmpty) return;

            Double newValue;
            if (this.Orientation == System.Windows.Controls.Orientation.Vertical)
            {
                Double percentChange = e.HorizontalChange / areaSize.Width;
                newValue = this.Context.X + percentChange;
            }
            else
            {
                Double percentChange = e.VerticalChange / areaSize.Height;
                newValue = this.Context.Y + percentChange;
            }

            //snap the new value
            newValue = Math.Round(newValue / BlockingEditor.SnapToPercent, MidpointRounding.AwayFromZero) * BlockingEditor.SnapToPercent;

            //Debug.WriteLine("Move to {0}", newValue);

            //apply the change
            this.Context.MoveTo(Convert.ToSingle(newValue));
        }

        /// <summary>
        /// Called whenever a property c
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Context_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "X":
                case "Y":
                case "Length":
                    UpdateSizeAndPosition();
                    break;
            }
        }

        /// <summary>
        /// Selects the divider on mouse press.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            this.IsSelected = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the position and size of this divider
        /// </summary>
        public void UpdateSizeAndPosition()
        {
            if (this.Context == null) return;
            if (this.ActualWidth == 0) return;
            if (this.ActualHeight == 0) return;

            Size areaSize = BlockingEditor.GetCanvasSize(this);
            if (areaSize.IsEmpty) return;

            Double left = Math.Max(0, areaSize.Width * Math.Min(1, this.Context.X));
            if (this.Orientation == System.Windows.Controls.Orientation.Vertical)
            {
                left -= (this.ActualWidth / 2D);
            }

            Double top = Math.Max(0, areaSize.Height * Math.Min(1, this.Context.Y));
            if (this.Orientation == System.Windows.Controls.Orientation.Horizontal)
            {
                top -= (this.ActualHeight / 2D);
            }

            Canvas.SetLeft(this, left);
            Canvas.SetTop(this, top);

            //set the length
            if (this.Orientation == Orientation.Vertical)
            {
                this.Height = Math.Max(0,  areaSize.Height * Math.Min(1, this.Context.Length));
            }
            else
            {
                this.Width = Math.Max(0, areaSize.Width * Math.Min(1, this.Context.Length));
            }
        }

        /// <summary>
        ///     Responds to the <see cref="Thumb.DragCompleted"/> routed event.
        /// </summary>
        /// <remarks>Just notifies a <see cref="Placed"/> routed event.</remarks>
        private void OnDragCompleted(Object sender, DragCompletedEventArgs e)
        {
            NotifyPlaced();
        }

        #endregion
        
        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.Context = null;
                _isDisposed = true;
            }
        }

        #endregion
    }
}