﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor
{
    /// <summary>
    /// Control to display a ruler.
    /// </summary>
    [TemplatePart(Name = Ruler.PartCanvasKey, Type = typeof(Canvas))]
    public sealed class Ruler : Control
    {
        #region Constants
        public const String PartCanvasKey = "PART_Canvas";
        #endregion

        #region Fields
        private Canvas _partCanvas;
        #endregion

        #region Properties

        #region Orientation

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(RulerOrientation), typeof(Ruler),
            new PropertyMetadata(RulerOrientation.Top));

        private static void OnOrientationPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        public RulerOrientation Orientation
        {
            get { return (RulerOrientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        #endregion

        #region MinimumValue

        public static readonly DependencyProperty MinimumValueProperty =
            DependencyProperty.Register("MinimumValue", typeof(Double), typeof(Ruler),
            new PropertyMetadata(0D, OnMinimumValuePropertyChanged));

        private static void OnMinimumValuePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        public Double MinimumValue
        {
            get { return (Double)GetValue(MinimumValueProperty); }
            set { SetValue(MinimumValueProperty, value); }
        }

        #endregion

        #region MaximumValue

        public static readonly DependencyProperty MaximumValueProperty =
           DependencyProperty.Register("MaximumValue", typeof(Double), typeof(Ruler),
           new PropertyMetadata(0D, OnMaximumValuePropertyChanged));

        private static void OnMaximumValuePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        public Double MaximumValue
        {
            get { return (Double)GetValue(MaximumValueProperty); }
            set { SetValue(MaximumValueProperty, value); }
        }

        #endregion

        #region Major Interval

        public static readonly DependencyProperty MajorIntervalProperty =
           DependencyProperty.Register("MajorInterval", typeof(Double), typeof(Ruler),
           new PropertyMetadata(0D, OnMajorIntervalPropertyChanged));

        private static void OnMajorIntervalPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        public Double MajorInterval
        {
            get { return (Double)GetValue(MajorIntervalProperty); }
            set { SetValue(MajorIntervalProperty, value); }
        }

        #endregion

        #region Minor Interval

        public static readonly DependencyProperty MinorIntervalProperty =
          DependencyProperty.Register("MinorInterval", typeof(Double), typeof(Ruler),
          new PropertyMetadata(0D, OnMinorIntervalPropertyChanged));

        private static void OnMinorIntervalPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        public Double MinorInterval
        {
            get { return (Double)GetValue(MinorIntervalProperty); }
            set { SetValue(MinorIntervalProperty, value); }
        }

        #endregion

        #region LabelStyle

        public static readonly DependencyProperty LabelStyleProperty =
          DependencyProperty.Register("LabelStyle", typeof(Style), typeof(Ruler),
          new PropertyMetadata(null, OnLabelStylePropertyChanged));

        private static void OnLabelStylePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        public Style LabelStyle
        {
            get { return (Style)GetValue(LabelStyleProperty); }
            set { SetValue(LabelStyleProperty, value); }
        }

        #endregion

        #region LabelFormat

        public static readonly DependencyProperty LabelFormatProperty =
          DependencyProperty.Register("LabelFormat", typeof(String), typeof(Ruler),
          new PropertyMetadata(null, OnLabelFormatPropertyChanged));

        private static void OnLabelFormatPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        public String LabelFormat
        {
            get { return (String)GetValue(LabelFormatProperty); }
            set { SetValue(LabelFormatProperty, value); }
        }

        #endregion

        #region LineColour

        public static readonly DependencyProperty LineColourProperty =
            DependencyProperty.Register("LineColour", typeof(Brush), typeof(Ruler),
            new PropertyMetadata(Brushes.DarkGray, OnLineColourPropertyChanged));

        private static void OnLineColourPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Ruler)obj).Redraw();
        }

        /// <summary>
        /// Gets/Sets the colour of the ruler lines
        /// </summary>
        public Brush LineColour
        {
            get { return (Brush)GetValue(LineColourProperty); }
            set { SetValue(LineColourProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override.")]
        static Ruler()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(Ruler), new FrameworkPropertyMetadata(typeof(Ruler)));
        }

        public Ruler()
        {
            this.SizeChanged += Ruler_SizeChanged;
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _partCanvas = this.GetTemplateChild(PartCanvasKey) as Canvas;
            if (_partCanvas == null) throw new ArgumentNullException();

            Redraw();
        }

        private void Ruler_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Redraw();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Redraws this ruler.
        /// </summary>
        private void Redraw()
        {
            if (_partCanvas == null) return;
            if (this.ActualWidth == 0) return;
            if (this.ActualHeight == 0) return;

            _partCanvas.Children.Clear();

            String labelFormat = 
                (!String.IsNullOrEmpty(this.LabelFormat))? 
                this.LabelFormat : "{0}";


            #region Horizonatal
            if (this.Orientation == RulerOrientation.Top ||
                this.Orientation == RulerOrientation.Bottom)
            {
                Double rulerRatio = this.ActualWidth / (this.MaximumValue - this.MinimumValue);
                Double minPx = 0;
                Double maxPx = this.ActualWidth;
                Double lineY = (this.Orientation == RulerOrientation.Top) ? 10 : this.ActualHeight - 10;

                //add the main horiz line
                Rectangle line = new Rectangle();
                line.Fill = this.LineColour;
                Canvas.SetLeft(line, minPx);
                Canvas.SetTop(line, lineY);
                line.Height = 1;
                line.Width = maxPx - minPx;
                _partCanvas.Children.Add(line);


                //now draw the ticks
                Double tickHeight = this.ActualHeight - 10;

                for (Double tickValue = this.MinimumValue; tickValue <= this.MaximumValue; tickValue += this.MinorInterval)
                {
                    Double tickX = tickValue * rulerRatio;

                    line = new Rectangle()
                        {
                            Fill = this.LineColour,
                            Height = tickHeight * 0.5,
                            Width = 1
                        };

                    //if this is a major line then add the label.
                    if (tickValue % this.MajorInterval == 0)
                    {
                        line.Width = 2;
                        line.Height = tickHeight;

                        //dont draw min and max labels
                        if (tickValue != this.MinimumValue && tickValue != this.MaximumValue)
                        {
                            TextBlock text = new TextBlock();
                            text.Style = this.LabelStyle;
                            text.Text = String.Format(CultureInfo.CurrentCulture, labelFormat, tickValue);
                            text.Measure(new Size(100, 100));

                            Canvas.SetLeft(text, tickX - (text.DesiredSize.Width / 2D));
                            Canvas.SetTop(text, (this.Orientation == RulerOrientation.Top) ? lineY - text.DesiredSize.Height - 0.1D : lineY + 0.1D);

                            _partCanvas.Children.Add(text);
                        }
                    }

                    Canvas.SetLeft(line, tickX - (line.Width / 2D));
                    Canvas.SetTop(line,  (this.Orientation == RulerOrientation.Top) ? lineY : lineY - line.Height);
                    _partCanvas.Children.Add(line);
                }

            }
            #endregion

            #region Vertical
            else
            {
                Double spacing = 12;

                Double rulerRatio = this.ActualHeight / (this.MaximumValue - this.MinimumValue);
                Double min = 0;
                Double max = this.ActualHeight;
                Double lineX = (this.Orientation == RulerOrientation.Left) ? spacing : this.ActualWidth - spacing;

                //add the main vert line
                Rectangle line = new Rectangle();
                line.Fill = this.LineColour;
                Canvas.SetTop(line, min);
                Canvas.SetLeft(line, lineX);
                line.Width = 1;
                line.Height = max - min;
                _partCanvas.Children.Add(line);


                //now draw the ticks
                Double tickWidth = this.ActualWidth - spacing;
                for (Double tickValue = this.MinimumValue; tickValue <= this.MaximumValue; tickValue += this.MinorInterval)
                {
                    Double tickY = tickValue * rulerRatio;

                    line = new Rectangle()
                        {
                            Fill = this.LineColour,
                            Width = tickWidth * 0.5,
                            Height = 1
                        };

                    //if this is a major line then add the label.
                    if (tickValue % this.MajorInterval == 0)
                    {
                        line.Height = 2;
                        line.Width = tickWidth;

                        //dont draw min and max labels
                        if (tickValue != this.MinimumValue && tickValue != this.MaximumValue)
                        {
                            TextBlock text = new TextBlock();
                            text.Style = this.LabelStyle;
                            text.Text = String.Format(CultureInfo.CurrentCulture, labelFormat, tickValue);
                            text.Measure(new Size(100, 100));

                            if (this.Orientation == RulerOrientation.Left)
                            {
                                Canvas.SetTop(text, this.ActualHeight - tickY + (text.DesiredSize.Height / 2D));
                                Canvas.SetLeft(text, lineX - (text.DesiredSize.Width / 2D) - 0.02D);

                                text.RenderTransform = new RotateTransform(-90,  .5, .5);
                            }
                            else
                            {
                                Canvas.SetTop(text, this.ActualHeight - tickY - (text.DesiredSize.Height / 2D));
                                Canvas.SetLeft(text, lineX + 0.1D + (text.DesiredSize.Width / 2D));

                                text.RenderTransform = new RotateTransform(90, .5, .5);
                            }

                          

                            _partCanvas.Children.Add(text);
                        }
                    }

                    Canvas.SetTop(line, this.ActualHeight - tickY - (line.Height / 2D));
                    Canvas.SetLeft(line, (this.Orientation == RulerOrientation.Left) ? lineX : lineX - line.Width);
                    _partCanvas.Children.Add(line);
                }
            }
            #endregion
        }

        #endregion
    }

    public enum RulerOrientation
    {
        Top,
        Bottom,
        Left,
        Right
    }
}
