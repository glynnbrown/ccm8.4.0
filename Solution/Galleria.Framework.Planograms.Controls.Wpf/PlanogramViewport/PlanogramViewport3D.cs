﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// CCM-27468 : L.Ineson
//  Corrected GetModel3DBounds to use modelconstruct3d
#endregion
#endregion

using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using HelixToolkit.Wpf;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Framework.Planograms.Controls.Wpf.PlanogramViewport
{
    [TemplatePart(Name = "PART_Viewport", Type = typeof(HelixViewport3D))]
    public class PlanogramViewport3D : Control
    {
        #region Fields
        private HelixViewport3D _helixViewport;
        private ModelConstruct3D _currentModel;
        private Boolean _isReloading;
        #endregion

        #region Properties

        #region Planogram
        public static readonly DependencyProperty PlanogramProperty =
            DependencyProperty.Register("Planogram", typeof(IPlanRenderable), typeof(PlanogramViewport3D),
            new UIPropertyMetadata(null, OnPlanogramPropertyChanged));

        private static void OnPlanogramPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PlanogramViewport3D)obj).OnPlanogramChanged();
        }

        /// <summary>
        /// Gets/Sets the planogram to draw.
        /// </summary>
        public IPlanRenderable Planogram
        {
            get { return (IPlanRenderable)GetValue(PlanogramProperty); }
            set { SetValue(PlanogramProperty, value); }
        }

        private void OnPlanogramChanged()
        {
            if (this.Planogram != null)
            {
                if (this.RenderSettings == null)
                {
                    this.RenderSettings = new PlanRenderSettings();
                }
                else
                {
                    OnRenderSettingsChanged();
                }
            }
            else
            {
                this.PlanogramRenderData = null;
            }
        }

        #endregion

        #region RenderSettings

        public static readonly DependencyProperty RenderSettingsProperty =
            DependencyProperty.Register("RenderSettings", typeof(IPlanRenderSettings), typeof(PlanogramViewport3D),
            new PropertyMetadata(null, OnRenderSettingsPropertyChanged));

        private static void OnRenderSettingsPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PlanogramViewport3D)obj).OnRenderSettingsChanged();
        }

        public IPlanRenderSettings RenderSettings
        {
            get { return (IPlanRenderSettings)GetValue(RenderSettingsProperty); }
            set { SetValue(RenderSettingsProperty, value); }
        }

        private void OnRenderSettingsChanged()
        {
            if (this.Planogram != null && this.RenderSettings != null)
            {
                this.PlanogramRenderData = new Plan3DData(this.Planogram, this.RenderSettings);
            }
            else
            {
                this.PlanogramRenderData = null;
            }
        }

        #endregion

        #region PlanogramRenderData

        public static readonly DependencyProperty PlanogramRenderDataProperty =
            DependencyProperty.Register("PlanogramRenderData", typeof(Plan3DData), typeof(PlanogramViewport3D),
            new UIPropertyMetadata(null, OnPlanogramRenderDataPropertyChanged));

        private static void OnPlanogramRenderDataPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                ((Plan3DData)e.OldValue).Dispose();
            }


            OnRenderPropertyChanged(obj, e);
        }

        public Plan3DData PlanogramRenderData
        {
            get { return (Plan3DData)GetValue(PlanogramRenderDataProperty); }
            private set { SetValue(PlanogramRenderDataProperty, value); }
        }

        #endregion

        #region CameraType

        public static readonly DependencyProperty CameraTypeProperty =
            DependencyProperty.Register("CameraType", typeof(CameraViewType), typeof(PlanogramViewport3D),
            new PropertyMetadata(CameraViewType.Perspective, OnRenderPropertyChanged));

        public CameraViewType CameraType
        {
            get { return (CameraViewType)GetValue(CameraTypeProperty); }
            set { SetValue(CameraTypeProperty, value); }
        }

        #endregion

        #region CanUserRotate
        public static readonly DependencyProperty CanUserRotateProperty =
            DependencyProperty.Register("CanUserRotate", typeof(Boolean), typeof(PlanogramViewport3D),
            new PropertyMetadata(false, OnRenderPropertyChanged));

        public Boolean CanUserRotate
        {
            get { return (Boolean)GetValue(CanUserRotateProperty); }
            set { SetValue(CanUserRotateProperty, value); }
        }

        #endregion

        #region ShowViewCube
        public static readonly DependencyProperty ShowViewCubeProperty =
            DependencyProperty.Register("ShowViewCube", typeof(Boolean), typeof(PlanogramViewport3D),
            new PropertyMetadata(false));

        public Boolean ShowViewCube
        {
            get { return (Boolean)GetValue(ShowViewCubeProperty); }
            set { SetValue(ShowViewCubeProperty, value); }
        }
        #endregion

        #region ShowCoordinateSystem
        public static readonly DependencyProperty ShowCoordinateSystemProperty =
            DependencyProperty.Register("ShowCoordinateSystem", typeof(Boolean), typeof(PlanogramViewport3D),
            new PropertyMetadata(false, OnRenderPropertyChanged));

        public Boolean ShowCoordinateSystem
        {
            get { return (Boolean)GetValue(ShowCoordinateSystemProperty); }
            set { SetValue(ShowCoordinateSystemProperty, value); }
        }
        #endregion

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override.")]
        static PlanogramViewport3D()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(PlanogramViewport3D), new FrameworkPropertyMetadata(typeof(PlanogramViewport3D)));
        }

        public PlanogramViewport3D()
        {
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _helixViewport = this.GetTemplateChild("PART_Viewport") as HelixViewport3D;
            if (_helixViewport == null) throw new ArgumentNullException();

            RenderOptions.SetBitmapScalingMode(_helixViewport, BitmapScalingMode.NearestNeighbor);

            Reload();
        }

        private static void OnRenderPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramViewport3D senderControl = (PlanogramViewport3D)obj;
            if (!senderControl._isReloading)
            {
                senderControl.Reload();
            }
        }

        #endregion

        #region Methods

        private void Reload()
        {
            //drop the old model.
            if (_currentModel != null)
            {
                _currentModel.Dispose();
                _currentModel = null;
            }

            if (_helixViewport == null) return;

            //clear the viewer children.
            foreach (Visual3D visual in _helixViewport.Children.ToList())
            {
                if (visual is ModelConstruct3D)
                {
                    _helixViewport.Children.Remove(visual);
                }
            }


            if (this.PlanogramRenderData == null) return;

            PlanCameraHelper.SetupForCameraType(
                _helixViewport, this.CameraType, this.CanUserRotate, this.ShowViewCube);


            //add the model
            ModelConstruct3D model = new ModelConstruct3D(this.PlanogramRenderData);
            _helixViewport.Children.Add(model);
            _currentModel = model;
        }

        public void ZoomToFit()
        {
            if (_helixViewport == null) return;
            if (_currentModel == null) return;

            PlanCameraHelper.ZoomToFit(_helixViewport, _currentModel);
        }

        public void ZoomToFitHeight(Boolean doInflate = true)
        {
            if (_helixViewport == null) return;
            if (_currentModel == null) return;

            PlanCameraHelper.ZoomToFitHeight(_helixViewport, _currentModel, doInflate);
        }

        public void ZoomToFitWidth()
        {
            if (_helixViewport == null) return;
            if (_currentModel == null) return;

            PlanCameraHelper.ZoomToFitWidth(_helixViewport, _currentModel);
        }

        public Rect GetModelBoundingRect()
        {
            if(_currentModel == null) return Rect.Empty;
            return ModelConstruct3DHelper.GetModel2DBounds(_helixViewport.Viewport, _currentModel);
        }

        public Rect Get2DBounds(Rect3D boundingArea)
        {
            if (_currentModel == null) return Rect.Empty;
            return ModelConstruct3DHelper.Get2DBounds(_helixViewport.Viewport, boundingArea, Rect.Empty);
        }

        public Rect3D GetModel3DBounds()
        {
            if (_currentModel == null) return Rect3D.Empty;

            return HelixToolkit.Wpf.Visual3DHelper.FindBounds(_helixViewport.Viewport.Children);
        }
        #endregion
    }
}
