﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
//V8-31206 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.30)
// V8-32636 : A.Probyn
//  Added ShowAnnotations
// CCM-18454 : L.Ineson
//  Corrected perspective camera zoom.
#endregion
#region Version History: (CCM 8.4.0)
// CCM-19219 : J.Mendes
//  Optimezed the print template rendering and pdf exporting speed by reusing a clone of the existing Plan3Ddata that is already calculated in the plan visual documents.
#endregion
#endregion

using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Framework.Planograms.Controls.Wpf.PlanogramViewport
{
    /// <summary>
    /// Organises the layout of the planogram image to be produced.
    /// </summary>
    public sealed partial class PlanogramImageControl : UserControl, IDisposable
    {
        #region Fields
        private IPlanogramImageSettings _imageSettings;
        private Planogram _plan;

        private PlanogramImageViewType _viewType;
        private PlanogramViewModel _planView;
        private Plan3DData _planModelData;
        private List<Plan3DData> _planModelDataList;
        private Boolean _isPlanModelDataCloned = false; 
        private ModelConstruct3D _planModel;

        private Int32 _bayRangeStart;
        private Int32 _bayRangeEnd;
        private IEnumerable<PlanogramFixtureItem> _selectedBays;

        private Boolean _applyLabelsAsOverlay;

        #endregion

        #region Properties

        /// <summary>
        /// Get/Sets whether the software render pass should be performed.
        /// </summary>
        public Boolean ApplySoftwareRender { get; set; }

        /// <summary>
        /// Gets/Sets whether the image is to be scaled unproportionally
        /// to fill the given dimensions.
        /// </summary>
        public Boolean FitUnproportionally { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="plan">the plan to create the image for</param>
        /// <param name="bayRangeStart">the bay range start</param>
        /// <param name="bayRangeEnd">the bay range end</param>
        /// <param name="imgHeight">the height of the image</param>
        /// <param name="imgWidth">the width of the image</param>
        /// <param name="settings">the image settings</param>
        internal PlanogramImageControl(Planogram plan, Int32 bayRangeStart, Int32 bayRangeEnd,
            Double imgHeight, Double imgWidth, IPlanogramImageSettings settings, List<Plan3DData> planModelDataList)
        {
            this.ApplySoftwareRender = true;
            this.FitUnproportionally = false;

            this.Width = imgWidth;
            this.Height = imgHeight;

            InitializeComponent();

            _plan = plan;
            _imageSettings = settings;

            _planModelDataList = planModelDataList;

            _viewType = (settings != null) ? settings.ViewType : PlanogramImageViewType.Design;


            _bayRangeStart = (bayRangeStart > 0) ? bayRangeStart : 1;
            _bayRangeEnd = (bayRangeEnd > 0 && bayRangeEnd <= plan.FixtureItems.Count) ? bayRangeEnd : plan.FixtureItems.Count;

            _selectedBays =
                plan.FixtureItems.OrderBy(p => p.X)
                .Skip(_bayRangeStart - 1).Take(_bayRangeEnd - _bayRangeStart + 1).ToList();

        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when applying the control template
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //flag if labels should be created as an overlay.
            if (_viewType != PlanogramImageViewType.Perspective && this.FitUnproportionally)
            {
                _applyLabelsAsOverlay = true;
            }

            //draw
            Draw();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The main draw method for the control.
        /// Controls the creation of models and performs the layout.
        /// </summary>
        private void Draw()
        {
            //Correct the positions of the viewer and software renderer
            Rect viewerRct = GetViewerPositionalRect();

            Canvas.SetLeft(this.Viewer, viewerRct.Left);
            Canvas.SetTop(this.Viewer, viewerRct.Top);
            this.Viewer.Width = viewerRct.Width;
            this.Viewer.Height = viewerRct.Height;

            //Create the plan model data
            PlanRenderSettings renderSettings = CreateRenderSettings(_imageSettings);
            if (_applyLabelsAsOverlay)
            {
                renderSettings.ProductLabel = null;
                renderSettings.FixtureLabel = null;
            }

            //Only set up existing planogram model data if there are any planogram model with the same settings in the memory list and ShowProductImages is equal to true.
            //We should only add items with ShowProductImages = true to the memory list because is this setting that can slow down the process considerably. 
            //Otherwise make it to recalculate again and don't bother to use extra memory.
            if (_planModelDataList != null && _planModelDataList.Count > 0)
            {
                foreach (Plan3DData item in _planModelDataList)
                {
                    if (renderSettings.ShowProductImages == true
                        && item.Settings.CameraType == renderSettings.CameraType
                        && item.Settings.CanRenderAsync == renderSettings.CanRenderAsync
                        && item.Settings.FixtureLabel == renderSettings.FixtureLabel
                        && item.Settings.FixtureLabelText == renderSettings.FixtureLabelText
                        && item.Settings.GenerateNormals == renderSettings.GenerateNormals
                        && item.Settings.GenerateTextureCoordinates == renderSettings.GenerateTextureCoordinates
                        && item.Settings.PositionHighlights == renderSettings.PositionHighlights
                        && item.Settings.PositionLabelText == renderSettings.PositionLabelText
                        && item.Settings.ProductLabel == renderSettings.ProductLabel
                        && item.Settings.RotateTopDownComponents == renderSettings.RotateTopDownComponents
                        && item.Settings.SelectionColour == renderSettings.SelectionColour
                        && item.Settings.SelectionFillPattern == renderSettings.SelectionFillPattern
                        && item.Settings.SelectionLineThickness == renderSettings.SelectionLineThickness                         
                        && item.Settings.ShowAnnotations == renderSettings.ShowAnnotations
                        && item.Settings.ShowChestWalls == renderSettings.ShowChestWalls
                        && item.Settings.ShowDividerLines == renderSettings.ShowDividerLines
                        && item.Settings.ShowDividers == renderSettings.ShowDividers
                        && item.Settings.ShowFixtureFillPatterns == renderSettings.ShowFixtureFillPatterns
                        && item.Settings.ShowFixtureImages == renderSettings.ShowFixtureImages
                        && item.Settings.ShowFixtures == renderSettings.ShowFixtures
                        && item.Settings.ShowNotches == renderSettings.ShowNotches
                        && item.Settings.ShowPegHoles == renderSettings.ShowPegHoles
                        && item.Settings.ShowPegs == renderSettings.ShowPegs
                        && item.Settings.ShowPositionUnits == renderSettings.ShowPositionUnits
                        && item.Settings.ShowPositions == renderSettings.ShowPositions
                        && item.Settings.ShowProductFillColours == renderSettings.ShowProductFillColours
                        && item.Settings.ShowProductFillPatterns == renderSettings.ShowProductFillPatterns
                        && item.Settings.ShowProductImages == renderSettings.ShowProductImages
                        && item.Settings.ShowProductShapes == renderSettings.ShowProductShapes
                        && item.Settings.ShowShelfRisers == renderSettings.ShowShelfRisers
                        && item.Settings.ShowTopDownProductLabelsAsFront == renderSettings.ShowTopDownProductLabelsAsFront
                        && item.Settings.ShowWireframeOnly == renderSettings.ShowWireframeOnly
                        && item.Settings.SuppressPerformanceIntensiveUpdates == renderSettings.SuppressPerformanceIntensiveUpdates)
                    {
                        _planModelData = item;
                        _isPlanModelDataCloned = true;
                        break;
                    }
                }
            }
            CreatePlanModelData(_plan, _viewType, renderSettings);


            //Initialize the viewr and set its model.
            SetupViewerAndModel();
            this.LayoutRoot.UpdateLayout();

            //Apply overlays for labels etc.
            ApplyLabelOverlays();

            //Create the software render
            if (this.ApplySoftwareRender)
            {
                Canvas.SetLeft(this.SoftwareRender, viewerRct.Left);
                Canvas.SetTop(this.SoftwareRender, viewerRct.Top);
                this.SoftwareRender.Width = viewerRct.Width;
                this.SoftwareRender.Height = viewerRct.Height;


                Int32 scale = 2;
                Int32 textureSize = 1024;

                this.SoftwareRender.Height = this.Height;
                this.SoftwareRender.Width = this.Width;
                RenderOptions.SetBitmapScalingMode(this.SoftwareRender, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(this.SoftwareRender, EdgeMode.Aliased);

                this.SoftwareRender.Source = Renderer.Render(this.Viewer.Viewport, this.Viewer.Camera, scale, textureSize);

                this.Viewer.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                //remove the overlay.
                this.LayoutRoot.Children.Remove(this.SoftwareRender);
            }

            this.LayoutRoot.UpdateLayout();

        }

        /// <summary>
        /// Creates the PlanogramViewModel and Plan3DData.
        /// </summary>
        /// <param name="planogram"></param>
        /// <param name="viewType"></param>
        /// <param name="renderSettings"></param>
        private void CreatePlanModelData(Planogram planogram, PlanogramImageViewType viewType, PlanRenderSettings renderSettings)
        {
                //Create the planView
                PlanogramViewModel planView = new PlanogramViewModel(planogram);
                _planView = planView;

                //force images to be loaded synchronously.
                planView.IsImageLoadAsync = false;

            //create the model data
            Plan3DData planData = _planModelData;
            if (planData == null)
            {
                    planData = new Plan3DData(
                            (viewType == PlanogramImageViewType.Design) ?
                            (IPlanRenderable)planView.DesignView : (IPlanRenderable)planView,
                             renderSettings);

                //We should only add planogram model data to the memory list if ShowProductImages = true because is this setting that can slow down the process considerably. 
                //Otherwise make it to recalculate again and don't bother to use extra memory.
                if (renderSettings.ShowProductImages)
                {
                    if (_planModelDataList == null)
                    {
                        _planModelDataList = new List<Plan3DData>();
                    }

                    Plan3DData planDataClone = planData.Clone();
                    _planModelDataList.Add(planDataClone);
                }
                
            }
            _planModelData = planData;


                //hide annotations if not required.
            Boolean drawTextBoxes = (_imageSettings != null) ? _imageSettings.TextBoxes : true;
                if (!drawTextBoxes)
                {
                    foreach (var anno in planData.EnumerateChildModels().OfType<PlanAnnotation3DData>())
                    {
                        anno.IsVisible = false;
                    }
                }

                //if not all bays are selected then we need to hide the rest and move products etc:
                IEnumerable<Object> bayIds = _selectedBays.Select(f => f.Id).ToArray();
            if (planogram.FixtureItems.Count != bayIds.Count())
            {
                planData.Freeze();

                #region [V8-31202] Move Overhanging Positions

                IEnumerable<PlanFixture3DData> orderedBays =
                planData.EnumerateFixtureModels().OrderBy(p => p.Position.X).ToArray();

                Int32 totalBayCount = orderedBays.Count();

                //if the bay range does not start from the begining of the plan:
                if (_bayRangeStart > 1)
                {
                    PlanFixture3DData firstBay = orderedBays.ElementAt(_bayRangeStart - 1);

                    //remove positions from the first bay that overhang left
                    foreach (PlanPosition3DData posData in firstBay.GetAllChildModels().OfType<PlanPosition3DData>())
                    {
                        if (posData.Position.X < (-posData.Size.Width / 2F))
                        {
                            firstBay.RemoveChild(posData);
                        }
                    }


                    //move positions onto the first bay in range
                    PlanFixture3DData prevBay = orderedBays.ElementAt(_bayRangeStart - 2);
                    foreach (PlanPosition3DData posData in prevBay.GetAllChildModels().OfType<PlanPosition3DData>())
                    {
                        if ((posData.Position.X + posData.Size.Width) > (prevBay.Size.Width + posData.Size.Width / 2F))
                        {
                            //get the adjacent parent
                            PlanComponent3DData newParentComponent =
                                firstBay.EnumerateComponentModels()
                                .FirstOrDefault(c => c.Position.Y == posData.ParentModelData.ParentModelData.Position.Y);
                            if (newParentComponent == null) continue;

                            PlanSubComponent3DData newParentSub =
                                newParentComponent.EnumerateSubComponentModels().FirstOrDefault(s => s.Position.Y == posData.ParentModelData.Position.Y);
                            if (newParentSub == null) continue;

                            //remove from the old parent
                            IPlanPositionRenderable posRenderable = posData.PlanPosition;
                            posData.ParentModelData.RemoveChild(posData);

                            //add to the new
                            PlanPosition3DData newPosData = new PlanPosition3DData(posRenderable, planData.Settings);
                            newParentSub.AddChild(newPosData);

                            //update the pos position.
                            newPosData.Position = new PointValue(newPosData.Position.X - prevBay.Size.Width, newPosData.Position.Y, newPosData.Position.Z);
                        }
                    }

                }

                if (_bayRangeEnd > 0 && _bayRangeEnd < totalBayCount)
                {
                    PlanFixture3DData lastBay = orderedBays.ElementAt(_bayRangeEnd - 1);

                    //remove positions that overhang right
                    foreach (PlanPosition3DData posData in lastBay.GetAllChildModels().OfType<PlanPosition3DData>())
                    {
                        if ((posData.Position.X + posData.Size.Width) > (lastBay.Size.Width + posData.Size.Width / 2F))
                        {
                            lastBay.RemoveChild(posData);
                        }
                    }

                    //add positions that overhang left on the next bay
                    PlanFixture3DData nextBay = orderedBays.ElementAt(_bayRangeEnd);
                    foreach (PlanPosition3DData posData in lastBay.GetAllChildModels().OfType<PlanPosition3DData>())
                    {
                        if (posData.Position.X < (-posData.Size.Width / 2F))
                        {
                            //get the adjacent parent
                            PlanComponent3DData newParentComponent =
                                nextBay.EnumerateComponentModels()
                                .FirstOrDefault(c => c.Position.Y == posData.ParentModelData.ParentModelData.Position.Y);
                            if (newParentComponent == null) continue;

                            PlanSubComponent3DData newParentSub =
                                newParentComponent.EnumerateSubComponentModels().FirstOrDefault(s => s.Position.Y == posData.ParentModelData.Position.Y);
                            if (newParentSub == null) continue;

                            //update the pos position.
                            posData.Position = new PointValue(posData.Position.X + lastBay.Size.Width, posData.Position.Y, posData.Position.Z);

                            //remove from the old parent
                            posData.ParentModelData.RemoveChild(posData);

                            //add to the new
                            newParentSub.AddChild(posData);
                        }

                    }
                }
                #endregion


                //hide bays not in the range
                foreach (PlanFixture3DData bayData in planData.EnumerateFixtureModels())
                {
                    if (!bayIds.Contains(bayData.Fixture.PlanogramFixtureItemId))
                    {
                        bayData.IsVisible = false;
                    }
                    else
                    {
                        bayData.IsVisible = true;
                    }
                }
            }
            else
            {
                planData.Freeze();
                foreach (PlanFixture3DData bayData in planData.EnumerateFixtureModels()) bayData.IsVisible = true;
            }
        }

        /// <summary>
        /// Creates the render settings from the current values.
        /// </summary>
        /// <returns></returns>
        private static PlanRenderSettings CreateRenderSettings(IPlanogramImageSettings settings)
        {
            if (settings != null)
            {
                return new PlanRenderSettings()
                {
                    CanRenderAsync = false, //must render synchronously to create the image.
                    ShowChestWalls = settings.ChestWalls,
                    RotateTopDownComponents = settings.RotateTopDownComponents,
                    ShowShelfRisers = settings.Risers,
                    ShowFixtureImages = settings.FixtureImages,
                    ShowFixtureFillPatterns = settings.FixtureFillPatterns,
                    ShowNotches = settings.Notches,
                    ShowPegHoles = settings.PegHoles,
                    ShowDividerLines = settings.DividerLines,
                    ShowPositions = settings.Positions,
                    ShowPositionUnits = settings.PositionUnits,
                    ShowProductImages = settings.ProductImages,
                    ShowProductShapes = settings.ProductShapes,
                    ShowProductFillColours = settings.ProductFillColours,
                    ShowProductFillPatterns = settings.ProductFillPatterns,
                    ShowPegs = settings.Pegs,
                    ShowDividers = settings.Dividers,
                    ShowAnnotations = settings.Annotations,
                    ProductLabel = settings.ProductLabel,
                    FixtureLabel = settings.FixtureLabel,
                    PositionHighlights = settings.PositionHighlights,
                    PositionLabelText = settings.PositionLabelText,
                    FixtureLabelText = settings.FixtureLabelText,

                    //setting the camera type will speed things up:
                    CameraType = (CameraViewType)Enum.Parse(typeof(CameraViewType), settings.ViewType.ToString())
                };
            }
            else
            {
                return new PlanRenderSettings()
                {
                    CanRenderAsync = false, //must render synchronously to create the image.
                };

            }
        }

        /// <summary>
        /// Gets the position and size of the viewer.
        /// </summary>
        private Rect GetViewerPositionalRect()
        {
            //Determine the viewer size and position
            Double topOffset = 0;
            Double bottomOffset = 0;
            Double leftOffset = 0;
            Double rightOffset = 0;

            ////top offset items
            //topOffset += _bayInstructionsOffset;

            ////left offset items
            //leftOffset += _bayHeightOffset;

            ////bottom off set items
            //bottomOffset += _bayWidthOffset;
            //bottomOffset += _bayNameOffset;
            //bottomOffset += _bayLabelOffset;
            //bottomOffset += _legendOffset;


            return new Rect(leftOffset, topOffset,
                this.Width - leftOffset - rightOffset,
                this.Height - topOffset - bottomOffset);

        }

        /// <summary>
        /// Sets up the viewer and adds the plan model.
        /// </summary>
        private void SetupViewerAndModel()
        {
            //set the viewer size.
            this.Viewer.Arrange(new Rect(new System.Windows.Point(0, 0), new System.Windows.Point(this.Width, this.Height)));
            this.Viewer.UpdateLayout();

            //Setup the viewer camera
            CameraViewType cameraType = CameraViewType.Front;
            switch (_viewType)
            {
                case PlanogramImageViewType.Design:
                case PlanogramImageViewType.Front:
                    cameraType = CameraViewType.Front;
                    break;

                case PlanogramImageViewType.Perspective:
                    cameraType = CameraViewType.Perspective;
                    break;

                case PlanogramImageViewType.Left:
                    cameraType = CameraViewType.Left;
                    break;

                case PlanogramImageViewType.Right:
                    cameraType = CameraViewType.Right;
                    break;

                case PlanogramImageViewType.Top:
                    cameraType = CameraViewType.Top;
                    break;

                case PlanogramImageViewType.Bottom:
                    cameraType = CameraViewType.Bottom;
                    break;
            }
            PlanCameraHelper.SetupForCameraType(this.Viewer, cameraType, false, false);

            //Create the plan model
            ModelConstruct3D planModel = new ModelConstruct3D(_planModelData);
            _planModel = planModel;

            planModel.IsDynamicTextScalingEnabled = true;

            //if we are scaling to fit all of the available space then
            // add a transfrom to the model.
            if (this.FitUnproportionally)
            {
                Double xRatio = this.Width / planModel.GetBounds().SizeX;
                Double yRatio = this.Height / planModel.GetBounds().SizeY;

                Transform3DGroup transformGroup = new Transform3DGroup();
                transformGroup.Children.Add(planModel.Transform);
                transformGroup.Children.Add(new ScaleTransform3D(xRatio, yRatio, 1));
                planModel.Transform = transformGroup;
            }

            //Add the model to the viewer
            this.Viewer.Children.Add(planModel);



            if (_viewType == PlanogramImageViewType.Perspective
                && _imageSettings != null
                && _imageSettings.CameraLookDirection.HasValue
                && _imageSettings.CameraPosition.HasValue)
            {
                if (this.Viewer.Camera as PerspectiveCamera == null)
                {
                    PlanCameraHelper.SetupForCameraType(this.Viewer, CameraViewType.Perspective, false, false);
                }

                this.Viewer.Camera.LookDirection = _imageSettings.CameraLookDirection.Value.ToVector3D();
                this.Viewer.Camera.Position = _imageSettings.CameraPosition.Value.ToPoint3D();

                PlanCameraHelper.ZoomToFit(this.Viewer, planModel);
            }
            else
            {
                //zoom to fit.
                PlanCameraHelper.ZoomToFit(this.Viewer, planModel);
            }
        }

        /// <summary>
        /// Applies overlays
        /// </summary>
        private void ApplyLabelOverlays()
        {
            if (!_applyLabelsAsOverlay) return;

            Viewport3D vp = this.Viewer.Viewport;
            Double viewerXOffset = Canvas.GetLeft(this.Viewer);
            Double viewerYOffset = Canvas.GetTop(this.Viewer);

            Canvas overlayCanvas = this.LayoutRoot;
            ModelConstruct3D planModel = _planModel;

            //Fixture Labels
            if (_imageSettings.FixtureLabel != null)
            {
                Boolean showOverImages = _imageSettings.FixtureLabel.ShowOverImages;
                Boolean isDisplayingImages = _imageSettings.FixtureImages;

                foreach (PlanComponent3DData componentData in _planModelData.EnumerateComponentModels())
                {
                    if (!componentData.IsVisible) continue;
                    if (!showOverImages && isDisplayingImages
                        && componentData.EnumerateSubComponentModels().Any(s=> s.HasAvailableImages())) continue;

                    String labelText = componentData.GetLabelText();
                    if (String.IsNullOrWhiteSpace(labelText)) continue;

                    ModelConstruct3D componentModel = planModel.FindModel(componentData);
                    if (componentModel == null) continue;

                    Rect3D worldBounds = GetWorldSpaceBounds(componentModel);

                    Rect elementBoundary = ModelConstruct3DHelper.Get2DBounds(vp, worldBounds, Rect.Empty);

                    elementBoundary = new Rect(
                        elementBoundary.X + viewerXOffset,
                        elementBoundary.Y + viewerYOffset,
                        elementBoundary.Width, elementBoundary.Height);

                    overlayCanvas.Children.Add(
                        CreateLabel(_imageSettings.FixtureLabel, labelText, elementBoundary));
                }
            }

            //Position labels.
            if (_imageSettings.ProductLabel != null)
            {
                Boolean showOverImages = _imageSettings.ProductLabel.ShowOverImages;
                Boolean isDisplayingImages = _imageSettings.ProductImages;

                foreach (PlanPosition3DData posData in _planModelData.EnumeratePositionModels())
                {
                    if (!posData.IsVisible) continue;
                    if (!showOverImages && isDisplayingImages && posData.HasAvailableImages()) continue;

                    String labelText = posData.GetLabelText();
                    if (String.IsNullOrWhiteSpace(labelText)) continue;

                    ModelConstruct3D posModel = planModel.FindModel(posData);
                    if (posModel == null) continue;

                    Rect3D worldBounds = GetWorldSpaceBounds(posModel);

                    Rect elementBoundary = ModelConstruct3DHelper.Get2DBounds(vp, worldBounds, Rect.Empty);
                    if (elementBoundary.IsEmpty) continue;

                    elementBoundary = new Rect(
                        elementBoundary.X + viewerXOffset,
                        elementBoundary.Y + viewerYOffset,
                        elementBoundary.Width, elementBoundary.Height);

                    overlayCanvas.Children.Add(
                        CreateLabel(_imageSettings.ProductLabel, labelText, elementBoundary));
                }
            }


        }

        /// <summary>
        /// Creates a single label control.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="labelTextStr"></param>
        /// <param name="maxBounds"></param>
        /// <returns></returns>
        private static ContentControl CreateLabel(IPlanogramLabel settings, String labelTextStr, Rect maxBounds)
        {
            ContentControl labelControl = new ContentControl();

            Rect bounds = new Rect(maxBounds.X - 0.5, maxBounds.Y - 0.5, maxBounds.Width + 1, maxBounds.Height + 1);
            Double minFontSize = 4D;

            Double fontSize = settings.FontSize;
            Double minSingleLineHeight = minFontSize * 1.55; //7.75;

            Canvas.SetLeft(labelControl, bounds.Left);
            Canvas.SetTop(labelControl, bounds.Top);

            Double innerMaxWidth = maxBounds.Width;
            Double innerMaxHeight = maxBounds.Height;

            Double finalFointSize;
            Boolean isRotated = false;


            TextBlock ft = CreateLabelTextBlock(settings, labelTextStr, innerMaxWidth, innerMaxHeight, out finalFointSize);

            //work out if the label should be rotated
            if (settings.IsRotateToFitOn)
            {
                //test what rotated values would be
                // and decide if it is better.
                Double rotatedFinalFontSize;
                TextBlock rotatedFt = CreateLabelTextBlock(settings, labelTextStr, innerMaxHeight, innerMaxWidth, out rotatedFinalFontSize);
                if (rotatedFinalFontSize > finalFointSize)
                {
                    ft = rotatedFt;
                    finalFointSize = rotatedFinalFontSize;
                    isRotated = true;
                }
            }

            ft.MaxHeight = Math.Max(finalFointSize * 1.5, Math.Max(1, (isRotated) ? innerMaxWidth : innerMaxHeight));
            ft.TextTrimming = TextTrimming.CharacterEllipsis;


            //create the elements
            Border labelBorder = new Border()
            {
                SnapsToDevicePixels = false,
                Background = new SolidColorBrush(ColorHelper.IntToColor(settings.BackgroundColour)),
                BorderThickness = new Thickness(settings.BorderThickness),
                BorderBrush = new SolidColorBrush(ColorHelper.IntToColor(settings.BorderColour)),
                MaxHeight = bounds.Height,
                MaxWidth = bounds.Width
            };


            if (isRotated)
            {
                labelBorder.MaxHeight = bounds.Width;
                labelBorder.MaxWidth = bounds.Height;
                labelBorder.LayoutTransform = new RotateTransform(-90);
            }

            //String text = labelTextStr;

            ////ammend text as per required wrapping.
            //switch (settings.TextWrapping)
            //{
            //    default:
            //    case PlanogramLabelTextWrapping.Word:
            //        //default.
            //        break;

            //    case PlanogramLabelTextWrapping.Letter:
            //        text = text.Replace(" ", "\u00a0"); //replace oridinary spaces with non line breaking ones.
            //        String newText = String.Empty;
            //        foreach (Char c in text.ToCharArray())
            //        {
            //            newText = String.Format("{0}{1}{2}", newText, c, "\u200B");
            //        }
            //        text = newText;
            //        break;

            //    case PlanogramLabelTextWrapping.None:
            //        text = text.Replace(" ", "\u00a0"); //replace oridinary spaces with non line breaking ones.
            //        break;
            //}


            HorizontalAlignment horizAlign = System.Windows.HorizontalAlignment.Center;
            switch (settings.LabelHorizontalPlacement)
            {
                case PlanogramLabelHorizontalAlignment.Left:
                    horizAlign = System.Windows.HorizontalAlignment.Left;
                    break;
                case PlanogramLabelHorizontalAlignment.Right:
                    horizAlign = System.Windows.HorizontalAlignment.Right;
                    break;
            }
            VerticalAlignment vertAlign = System.Windows.VerticalAlignment.Center;
            switch (settings.LabelVerticalPlacement)
            {
                case PlanogramLabelVerticalAlignment.Top:
                    vertAlign = System.Windows.VerticalAlignment.Top;
                    break;
                case PlanogramLabelVerticalAlignment.Bottom:
                    vertAlign = System.Windows.VerticalAlignment.Bottom;
                    break;
            }
            ft.HorizontalAlignment = horizAlign;
            ft.VerticalAlignment = vertAlign;


            //TextBlock labelText = new TextBlock()
            //{
            //    SnapsToDevicePixels = true,
            //    Text = text,
            //    TextTrimming = TextTrimming.CharacterEllipsis,
            //    TextWrapping = TextWrapping.Wrap,
            //    Foreground = new SolidColorBrush(ColorHelper.IntToColor(settings.TextColour)),
            //    TextAlignment = TextAlignment.Center,
            //    HorizontalAlignment = horizAlign,
            //    VerticalAlignment = vertAlign,
            //    FontFamily = new FontFamily(settings.Font),
            //    FontSize = fontSize,
            //    Padding = new Thickness(0),
            //    //Background = Brushes.White
            //};


            //if (settings.IsShrinkToFitOn)
            //{
            //    Double availableWidth = (isRotated) ? bounds.Height : bounds.Width;
            //    Double availableHeight = (isRotated) ? bounds.Width : bounds.Height;

            //    labelText.MaxWidth = availableWidth;
            //    labelText.Measure(new Size(availableWidth, Double.PositiveInfinity));
            //    Double textHeight = labelText.DesiredSize.Height;


            //    while ((textHeight + 1) > availableHeight)
            //    {
            //        if (fontSize > minFontSize)
            //        {
            //            fontSize--;
            //            labelText.FontSize = fontSize;

            //            labelText.Measure(new Size(availableWidth, Double.PositiveInfinity));
            //            textHeight = labelText.DesiredSize.Height;
            //        }
            //        else
            //        {
            //            //we have gone to the min font size if we are still over then break height.
            //            //make sure at least one line fits.
            //            if (minSingleLineHeight > availableHeight)
            //            {
            //                labelBorder.MaxHeight = minSingleLineHeight;

            //                if (isRotated)
            //                {
            //                    bounds.X -= (minSingleLineHeight / 2.0);
            //                }
            //                else
            //                {
            //                    bounds.Y -= (minSingleLineHeight / 2.0);
            //                }
            //            }
            //            break;
            //        }
            //    }

            //}

            //labelBorder.Child = labelText;
            labelBorder.Child = ft;

            //align the label within its bounds
            labelBorder.Measure(new Size(bounds.Width, bounds.Height));



            //+ adjust vertical alignment - always center
            Double vDiff = bounds.Height - labelBorder.DesiredSize.Height;
            System.Windows.VerticalAlignment vAlign = System.Windows.VerticalAlignment.Center;
            switch (vAlign)
            {
                case System.Windows.VerticalAlignment.Center:
                    {
                        if (labelBorder.DesiredSize.Height < bounds.Height)
                        {
                            Canvas.SetTop(labelControl, bounds.Top + (vDiff / 2.0));
                        }
                    }
                    break;

                case System.Windows.VerticalAlignment.Stretch:
                    {
                        labelBorder.Height = bounds.Height;
                    }
                    break;

                case System.Windows.VerticalAlignment.Bottom:
                    {
                        if (labelBorder.DesiredSize.Height < bounds.Height)
                        {
                            Canvas.SetTop(labelControl, bounds.Top + vDiff);
                        }
                    }
                    break;

                case System.Windows.VerticalAlignment.Top: /*do nothing*/ break;

            }



            //+ adjust horizontal alignment
            Double hDiff = bounds.Width - labelBorder.DesiredSize.Width;
            HorizontalAlignment hAlign = HorizontalAlignment.Center;
            switch (hAlign)
            {
                case HorizontalAlignment.Left: /*do nothing*/ break;

                case HorizontalAlignment.Center:
                    {
                        if (labelBorder.DesiredSize.Width < bounds.Width)
                        {
                            Canvas.SetLeft(labelControl, bounds.Left + (hDiff / 2.0));
                        }
                    }
                    break;

                case HorizontalAlignment.Right:
                    {
                        if (labelBorder.DesiredSize.Width < bounds.Width)
                        {
                            Canvas.SetLeft(labelControl, bounds.Left + hDiff);
                        }
                    }
                    break;

                case HorizontalAlignment.Stretch:
                    {
                        labelBorder.Width = bounds.Width;
                    }
                    break;
            }



            //add the label to this control.
            labelControl.Content = labelBorder;

            return labelControl;
        }

        /// <summary>
        /// Measures and creates a textblock for a label overlay.
        /// </summary>
        /// <param name="anno"></param>
        /// <param name="labelTextStr"></param>
        /// <param name="maxWidth"></param>
        /// <param name="maxHeight"></param>
        /// <param name="finalFontSize"></param>
        /// <returns></returns>
        private static TextBlock CreateLabelTextBlock(IPlanogramLabel anno, String labelTextStr, Double maxWidth, Double maxHeight, out Double finalFontSize)
        {
            TextBlock ft;

            const Double minFontSize = 4;

            maxWidth = Math.Max(1, maxWidth);


            String text = labelTextStr;

            //ammend text as per required wrapping.
            switch (anno.TextWrapping)
            {
                default:
                case PlanogramLabelTextWrapping.Word:
                    //default.
                    break;

                case PlanogramLabelTextWrapping.Letter:
                    text = text.Replace(" ", "\u00a0"); //replace oridinary spaces with non line breaking ones.
                    String newText = String.Empty;
                    foreach (Char c in text.ToCharArray())
                    {
                        newText = String.Format("{0}{1}{2}", newText, c, "\u200B");
                    }
                    text = newText;
                    break;

                case PlanogramLabelTextWrapping.None:
                    text = text.Replace(" ", "\u00a0"); //replace oridinary spaces with non line breaking ones.
                    break;
            }


            ft = new TextBlock()
            {
                Text = text,
                FontSize = anno.FontSize,
                Foreground = new SolidColorBrush(ColorHelper.IntToColor(anno.TextColour)),
                FontFamily = new FontFamily(anno.Font),
                FontStyle = (anno.TextBoxFontItalic) ? FontStyles.Italic : FontStyles.Normal,
                FontWeight = (anno.TextBoxFontBold) ? FontWeights.Bold : FontWeights.Normal,
                TextWrapping = TextWrapping.Wrap
            };

            if (anno.TextBoxFontUnderlined)
            {
                TextDecorationCollection decorations = new TextDecorationCollection();
                decorations.Add(TextDecorations.Underline);
                ft.TextDecorations = decorations;
            }

            switch (anno.TextBoxTextAlignment)
            {
                case PlanogramLabelTextAlignment.Left: ft.TextAlignment = TextAlignment.Left; break;
                case PlanogramLabelTextAlignment.Center: ft.TextAlignment = TextAlignment.Center; break;
                case PlanogramLabelTextAlignment.Justified: ft.TextAlignment = TextAlignment.Justify; break;
                case PlanogramLabelTextAlignment.Right: ft.TextAlignment = TextAlignment.Right; break;
            }

            ft.TextTrimming = TextTrimming.None;
            ft.MaxWidth = maxWidth;


            ft.Measure(new Size(1000, 1000));


            //shrink font if allowed
            Double fontSize = anno.FontSize;
            if (anno.IsShrinkToFitOn
                && fontSize > Math.Max(0.1, minFontSize)
                && (ft.DesiredSize.Height > maxHeight || ft.DesiredSize.Width > maxWidth))
            {
                while (fontSize >= 1)
                {
                    ft.FontSize = fontSize;
                    ft.Measure(new Size(1000, 1000));

                    if (ft.DesiredSize.Height > maxHeight || ft.DesiredSize.Width > maxWidth)
                    {
                        fontSize--;
                    }
                    else break;
                }

            }

            finalFontSize = fontSize;

            return ft;
        }

        /// <summary>
        /// Returns the axis-aligned 3D bounding box for the given item.
        /// Takes into consideration the unproportional scale transform.
        /// </summary>
        private Rect3D GetWorldSpaceBounds(ModelConstruct3D item)
        {
            Rect3D bounds = ModelConstruct3DHelper.GetWorldSpaceBounds(item.ModelData);
            if (!this.FitUnproportionally) return bounds;


            //apply the plan model scale transform.
            return new MatrixTransform3D(_planModel.Transform.Value).TransformBounds(bounds);
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (_isDisposed) return;

            if (isDisposing)
            {
                _planModel.Dispose();
                _planModel = null;
                if (!_isPlanModelDataCloned)
                {
                    _planModelData.Dispose();
                    _planModelData = null;
                }
                _planView.Dispose();
                _planView = null;

            }

            _isDisposed = true;
        }

        #endregion
    }
}
