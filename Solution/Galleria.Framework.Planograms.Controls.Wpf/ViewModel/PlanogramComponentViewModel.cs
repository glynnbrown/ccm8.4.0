﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion

#region Version History: (CCM 8.3)
// V8-32523 : L.Ineson
//  Added support for component level annotations.
#endregion
#endregion

using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple implementation of a PlanogramComponent viewmodel.
    /// </summary>
    public class PlanogramComponentViewModel : PlanogramComponentViewModelBase
    {
        #region Fields
        private CastedObservableCollection<PlanogramSubComponentViewModel, PlanogramSubComponentViewModelBase> _subComponentsRO;
        private CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase> _annotationsRO;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of child subcomponent views
        /// </summary>
        public CastedObservableCollection<PlanogramSubComponentViewModel, PlanogramSubComponentViewModelBase> SubComponents
        {
            get
            {
                if (_subComponentsRO == null)
                {
                    _subComponentsRO =
                        new CastedObservableCollection<PlanogramSubComponentViewModel, PlanogramSubComponentViewModelBase>(
                            base.SubComponentViews, true);
                }
                return _subComponentsRO;
            }
        }

        /// <summary>
        /// Returns the collection of annotations held by this component.
        /// </summary>
        public CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase> Annotations
        {
            get
            {
                if (_annotationsRO == null)
                {
                    _annotationsRO = new CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase>(base.AnnotationViews, true);
                }
                return _annotationsRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for a FixtureComponent
        /// </summary>
        /// <param name="parentFixtureView"></param>
        /// <param name="fixtureComponent"></param>
        public PlanogramComponentViewModel(PlanogramFixtureViewModel parentFixtureView, 
            PlanogramFixtureComponent fixtureComponent, PlanogramComponent component)
            : base(parentFixtureView, fixtureComponent, component)
        {
            InitializeChildViews();
        }

        /// <summary>
        /// Constructor for an AssemblyComponent
        /// </summary>
        /// <param name="parentAssemblyView"></param>
        /// <param name="assemblyComponent"></param>
        public PlanogramComponentViewModel(PlanogramAssemblyViewModel parentAssemblyView, 
            PlanogramAssemblyComponent assemblyComponent, PlanogramComponent component)
            : base(parentAssemblyView, assemblyComponent, component)
        {
            InitializeChildViews();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new viewmodel for the given subcomponent.
        /// </summary>
        /// <param name="subComponent"></param>
        /// <returns></returns>
        protected override PlanogramSubComponentViewModelBase CreateSubComponentView(PlanogramSubComponent subComponent)
        {
            return new PlanogramSubComponentViewModel(this, subComponent);
        }

        /// <summary>
        /// Returns a view of the given annotation model.
        /// </summary>
        protected override PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation annotation)
        {
            return new PlanogramAnnotationViewModel(annotation);
        }

        #endregion
    }
}
