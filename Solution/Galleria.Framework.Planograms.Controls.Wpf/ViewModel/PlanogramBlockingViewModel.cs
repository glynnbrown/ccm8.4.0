﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 820
// V8-31077 : A.Kuszyk
//  Created.
// V8-31321 : D.Pleasance
//  Removed SelectorSequenceGroups \ SelectorGroups and added Available properties onto PlanogramBlockingGroupViewModel \ PlanogramSequenceGroupViewModel
#endregion
#endregion

using Galleria.Framework.Collections;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// An implementation of a view model representing a <see cref="PlanogramBlocking"/> model.
    /// </summary>
    public class PlanogramBlockingViewModel : PlanogramBlockingViewModelBase
    {
        #region Fields

        /// <summary>
        /// The view models that represent the <see cref="PlanogramBlockingGroup"/>s on the <see cref="PlanogramBlocking"/>.
        /// </summary>
        private CastedObservableCollection<PlanogramBlockingGroupViewModel, PlanogramBlockingGroupViewModelBase> _groupsRO;

        /// <summary>
        /// The view models that represent the <see cref="PlanogramSequenceGroup"/>s on the <see cref="Planogram"/>.
        /// </summary>
        private ObservableCollection<PlanogramSequenceGroupViewModel> _sequenceGroups;

        /// <summary>
        /// The <see cref="PlanogramSequence"/> that this <see cref="Model"/> is related to.
        /// </summary>
        private PlanogramSequence _sequence;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the view models that represent the <see cref="PlanogramBlockingGroup"/>s on the <see cref="PlanogramBlocking"/>.
        /// </summary>
        public CastedObservableCollection<PlanogramBlockingGroupViewModel, PlanogramBlockingGroupViewModelBase> Groups
        {
            get
            {
                if (_groupsRO == null)
                {
                    _groupsRO =
                        new CastedObservableCollection<PlanogramBlockingGroupViewModel, PlanogramBlockingGroupViewModelBase>(
                            base.Groups, true);
                }
                return _groupsRO;
            }
        }
        
        /// <summary>
        /// Gets the view models that represent the <see cref="PlanogramSequenceGroup"/>s on the <see cref="Planogram"/>.
        /// </summary>
        public ObservableCollection<PlanogramSequenceGroupViewModel> SequenceGroups 
        { 
            get { return _sequenceGroups; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Instantiates a new view model for the <paramref name="blocking"/> and <paramref name="sequence"/> provided.
        /// </summary>
        /// <param name="blocking">The <see cref="PlanogramBlocking"/> that this view model should represent.</param>
        /// <param name="sequence">The associated <see cref="PlanogramSeqeuence"/> from the <see cref="Planogram"/>.</param>
        public PlanogramBlockingViewModel(PlanogramBlocking blocking, PlanogramSequence sequence)
            : base(blocking)
        {
            _sequenceGroups = new ObservableCollection<PlanogramSequenceGroupViewModel>(
                sequence.Groups.Select(g => new PlanogramSequenceGroupViewModel(g, this)));
            _sequence = sequence;
            _sequence.Groups.BulkCollectionChanged += Sequence_GroupsBulkCollectionChanged;
            Groups.CollectionChanged += Groups_CollectionChanged;
        }

	    #endregion

        #region Event Handlers

        /// <summary>
        /// Called the <see cref="Groups"/> collection changes, firing a property changed for the <see cref="SelectorGroups"/> property.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Groups_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (PlanogramSequenceGroupViewModel group in SequenceGroups)
            {
                group.NotifyAvailableGroupsChanged();
            }
        }

        #region SequenceGroups Bulk Collection Changed
        /// <summary>
        /// Called when the <see cref="_sequence"/> model's groups collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sequence_GroupsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramSequenceGroup group in e.ChangedItems)
                    {

                        PlanogramSequenceGroupViewModel view = new PlanogramSequenceGroupViewModel(group, this);
                        if (view != null)
                        {
                            _sequenceGroups.Add(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramSequenceGroup group in e.ChangedItems)
                    {
                        PlanogramSequenceGroupViewModel view =
                            _sequenceGroups.FirstOrDefault(a => a.Model == group);
                        if (view != null)
                        {
                            _sequenceGroups.Remove(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_sequenceGroups.Count > 0) _sequenceGroups.Clear();

                        foreach (PlanogramSequenceGroup group in _sequence.Groups)
                        {
                            PlanogramSequenceGroupViewModel view = new PlanogramSequenceGroupViewModel(group, this);
                            if (view != null)
                            {
                                _sequenceGroups.Add(view);
                            }
                        }
                    }
                    break;
            }

            foreach (PlanogramBlockingGroupViewModel group in Groups)
            {
                group.NotifyAvailableSequenceGroupsChanged();
            }
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Implementation of a factor method for creating <see cref="PlanogramBlockingGroupViewModelBase"/> implementations.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <returns></returns>
        protected override PlanogramBlockingGroupViewModelBase CreateBlockingGroupView(PlanogramBlockingGroup blockingGroup)
        {
            return new PlanogramBlockingGroupViewModel(blockingGroup, this);
        }

        /// <summary>
        /// Notifies each of the <see cref="SequenceGroup"/>s and <see cref="Group"/>s that
        /// they need to update their blocking/sequencing properties..
        /// </summary>
        public void NotifyBlockingOrSequenceGroupsChanged()
        {
            foreach (PlanogramSequenceGroupViewModel sequenceGroup in SequenceGroups)
            {
                sequenceGroup.NotifyBlockingGroupChanged();
            }
            foreach (PlanogramBlockingGroupViewModel blockingGroup in Groups)
            {
                blockingGroup.NotifySequenceGroupChanged();
            }
        }

        protected override void OnDisposing(bool disposing)
        {
            base.OnDisposing(disposing);
            Groups.CollectionChanged -= Groups_CollectionChanged;
            _sequence.Groups.BulkCollectionChanged -= Sequence_GroupsBulkCollectionChanged;
        }

        #endregion
    }
}
