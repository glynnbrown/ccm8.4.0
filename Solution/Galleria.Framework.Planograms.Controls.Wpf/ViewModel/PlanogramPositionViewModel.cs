﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion
#endregion

using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Windows;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple implementation of a planogram position viewmodel.
    /// </summary>
    public class PlanogramPositionViewModel : PlanogramPositionViewModelBase, IPlanPositionRenderable
    {
        #region Fields
        private readonly PlanogramProductViewModel _planogramProductView;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the related product view.
        /// </summary>
        public PlanogramProductViewModel Product
        {
            get { return _planogramProductView; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentSubComponent"></param>
        /// <param name="position"></param>
        /// <param name="productView"></param>
        public PlanogramPositionViewModel(PlanogramSubComponentViewModel parentSubComponent, PlanogramPosition position)
            : base(parentSubComponent, position)
        {
            _planogramProductView = GetProductView() as PlanogramProductViewModel;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the text to be displayed by the position label based on
        /// the given format string.
        /// </summary>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public override String GetLabelText(String formatString)
        {
            return Product.Gtin + ' ' + Product.Name;
        }

        /// <summary>
        /// Really shoddy thing to do, but force the property change to fire on the ui thread.
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void FirePropertyChangedOnAppDispatcher(string propertyName)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => OnPropertyChanged(propertyName)));
        }

        #endregion

        #region IPlanogramPosition Members

        IPlanProductRenderable IPlanPositionRenderable.Product
        {
            get { return _planogramProductView; }
        }

        IPlanSubComponentRenderable IPlanPositionRenderable.ParentSubComponent
        {
            get { return base.ParentSubComponentView; }
        }

        #endregion
    }
}
