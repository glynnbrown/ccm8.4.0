﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion
#endregion

using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple implementation of a product viewmodel.
    /// </summary>
    public class PlanogramProductViewModel : PlanogramProductViewModelBase
    {

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="product"></param>
        public PlanogramProductViewModel(PlanogramProduct product)
            : base(product)
        {

        }

        #endregion

    }
}
