﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion
#endregion

using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple implementation of a planogram viewmodel.
    /// </summary>
    public class PlanogramViewModel : PlanogramViewModelBase
    {
        #region Fields

        private readonly DesignPlanogramViewModel _designView;

        private CastedObservableCollection<PlanogramFixtureViewModel, PlanogramFixtureViewModelBase> _fixturesRO;
        private CastedObservableCollection<PlanogramProductViewModel, PlanogramProductViewModelBase> _productsRO;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of planogram fixture views
        /// </summary>
        public CastedObservableCollection<PlanogramFixtureViewModel, PlanogramFixtureViewModelBase> Fixtures
        {
            get
            {
                if (_fixturesRO == null)
                {
                    _fixturesRO =
                        new CastedObservableCollection<PlanogramFixtureViewModel, PlanogramFixtureViewModelBase>
                            (base.FixtureViews, true);
                }
                return _fixturesRO;
            }
        }

        /// <summary>
        /// Returns the collection of planogram product views.
        /// </summary>
        public CastedObservableCollection<PlanogramProductViewModel, PlanogramProductViewModelBase> Products
        {
            get
            {
                if (_productsRO == null)
                {
                    _productsRO =
                        new CastedObservableCollection<PlanogramProductViewModel, PlanogramProductViewModelBase>
                            (base.ProductViews, true);
                }
                return _productsRO;
            }
        }

        /// <summary>
        /// Gets the design view of this planogram.
        /// </summary>
        public DesignPlanogramViewModel DesignView
        {
            get {return _designView;}
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="plan"></param>
        public PlanogramViewModel(Planogram plan)
        {
            _designView = new DesignPlanogramViewModel(this);

            SetModel(plan, ViewStateObjectModelChangeReason.Direct);
        }

        #endregion

        #region Methods

        protected override PlanogramFixtureViewModelBase CreateFixtureView(PlanogramFixtureItem fixtureItem)
        {
            PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
            if (fixture == null) return null;
            return new PlanogramFixtureViewModel(this, fixtureItem, fixture);
        }

        protected override PlanogramProductViewModelBase CreateProductView(PlanogramProduct product)
        {
            return new PlanogramProductViewModel(product);
        }

        #endregion

        #region IDisposable

        protected override void OnDisposing(Boolean disposing)
        {
            base.OnDisposing(disposing);

            _designView.Dispose();
        }

        #endregion

    }


}
