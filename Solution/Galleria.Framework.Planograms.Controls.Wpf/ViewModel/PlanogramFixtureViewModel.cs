﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion
#endregion

using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple implementation of a planogram fixture viewmodel.
    /// </summary>
    public class PlanogramFixtureViewModel : PlanogramFixtureViewModelBase
    {
        #region Fields

        private CastedObservableCollection<PlanogramAssemblyViewModel, PlanogramAssemblyViewModelBase> _assembliesRO;
        private CastedObservableCollection<PlanogramComponentViewModel, PlanogramComponentViewModelBase> _componentsRO;
        private CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase> _annotationsRO;

        #endregion

        #region Properties

        /// <summary>
        /// Returns direct assembly children of this fixture.
        /// </summary>
        public CastedObservableCollection<PlanogramAssemblyViewModel, PlanogramAssemblyViewModelBase> Assemblies
        {
            get
            {
                if (_assembliesRO == null)
                {
                    _assembliesRO =
                        new CastedObservableCollection<PlanogramAssemblyViewModel, PlanogramAssemblyViewModelBase>
                            (base.AssemblyViews, true);
                }
                return _assembliesRO;
            }
        }

        /// <summary>
        /// Returns direct component children of this fixture.
        /// </summary>
        public CastedObservableCollection<PlanogramComponentViewModel, PlanogramComponentViewModelBase> Components
        {
            get
            {
                if (_componentsRO == null)
                {
                    _componentsRO =
                        new CastedObservableCollection<PlanogramComponentViewModel, PlanogramComponentViewModelBase>
                            (base.ComponentViews, true);
                }
                return _componentsRO;
            }
        }

        /// <summary>
        /// Returns the collection of child annotation views.
        /// </summary>
        public CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase> Annotations
        {
            get
            {
                if (_annotationsRO == null)
                {
                    _annotationsRO =
                        new CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase>
                            (base.AnnotationViews, true);
                }
                return _annotationsRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentPlanogramView"></param>
        /// <param name="fixtureItem"></param>
        public PlanogramFixtureViewModel(PlanogramViewModel parentPlanView, 
            PlanogramFixtureItem fixtureItem, PlanogramFixture fixture)
            : base(parentPlanView, fixtureItem, fixture)
        {
            InitializeChildViews();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new viewmodel of the given fixture assembly
        /// </summary>
        /// <param name="fixtureAssembly"></param>
        /// <returns></returns>
        protected override PlanogramAssemblyViewModelBase CreateAssemblyView(PlanogramFixtureAssembly fixtureAssembly)
        {
            PlanogramAssembly assembly = fixtureAssembly.GetPlanogramAssembly();
            if (assembly == null) return null;
            return new PlanogramAssemblyViewModel(this, fixtureAssembly, assembly);
        }

        /// <summary>
        /// Creates a new viewmodel of the given component
        /// </summary>
        /// <param name="fixtureComponent"></param>
        /// <returns></returns>
        protected override PlanogramComponentViewModelBase CreateComponentView(PlanogramFixtureComponent fixtureComponent)
        {
            PlanogramComponent component = fixtureComponent.GetPlanogramComponent();
            if (component == null) return null;
            return new PlanogramComponentViewModel(this, fixtureComponent, component);
        }

        /// <summary>
        /// Creates a new viewmodel of the given annotation.
        /// </summary>
        /// <param name="fixtureAnnotation"></param>
        /// <returns></returns>
        protected override PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation fixtureAnnotation)
        {
            return new PlanogramAnnotationViewModel(fixtureAnnotation);
        }

        #endregion

    }

}
