﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 820
// V8-31077 : A.Kuszyk
//  Created.
// V8-31162 : A.Kuszyk
//  Implemented IDataErrorInfo.
// V8-31259 : A.Kuszyk
//  Protected against null value assignment in SequenceGroup property.
// V8-30737 : M.Shelley 
//  Added the blocking group meta performance data
// V8-31321 : D.Pleasance
//  Added AvailableSequenceGroups
#endregion

#region Version History: CCM 830
// CCM-18674 : M.Pettit
//  Added LimitBlockSpaceByPercentageOfType enum and property for UI dropdown
#endregion

#endregion

using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// An implementation of a <see cref="PlanogramBlockingGroupViewModelBase"/> class, representing a <see cref="PlanogramBlockingGroup"/>.
    /// </summary>
    public class PlanogramBlockingGroupViewModel : PlanogramBlockingGroupViewModelBase, IDataErrorInfo
    {
        #region Fields

        /// <summary>
        /// The view model of the parent <see cref="PlanogramBlocking"/> that this group belongs to.
        /// </summary>
        private PlanogramBlockingViewModel _blocking;

        private PlanogramBlockingGroup _blockingGroup;
        BulkObservableCollection<PlanogramBlockingGroupItemViewModel> _metaPropertyItemsValues = 
            new BulkObservableCollection<PlanogramBlockingGroupItemViewModel>();

        //UI-specific to determine which value the user can enter when limiting block percentage. They can enter to select a percentage of Block space or Planogram space
        //The model always stores the planogram space percentage
        private LimitBlockChangeByPercentageOfType _limitBlockChangeByPercentageOfType = LimitBlockChangeByPercentageOfType.PlanogramSpace;

        #endregion

        #region Properties

        /// <summary>
        /// The <see cref="SequenceGroups"/> arranged in ascending name order, with a blank option first
        /// for selection purposes.
        /// </summary>
        public IEnumerable<PlanogramSequenceGroupViewModel> AvailableSequenceGroups
        {
            get
            {
                List<PlanogramSequenceGroupViewModel> selectorGroups = new List<PlanogramSequenceGroupViewModel>();
                selectorGroups.Add(new PlanogramSequenceGroupViewModel(PlanogramSequenceGroup.NewPlanogramSequenceGroup(), _blocking));
                selectorGroups.AddRange(_blocking.SequenceGroups.OrderBy(g => g.Name));
                return selectorGroups;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="PlanogramSequenceGroup"/> that is associated with this group, based
        /// on matching colours.
        /// </summary>
        public PlanogramSequenceGroupViewModel SequenceGroup
        {
            get
            {
                return _blocking.SequenceGroups.FirstOrDefault(g => g.Colour.Equals(this.Colour));
            }

            set
            {
                if (value == null) return;
                if (_blocking.SequenceGroups.Contains(value))
                {
                    Colour = value.Colour;
                    Name = value.Name;
                }
                else if(_blocking.SequenceGroups.Any())
                {
                    // We only need to re-set the colour if this blocks colour is found in the sequence groups.
                    if (_blocking.SequenceGroups.Select(g => g.Colour).Contains(this.Colour))
                    {
                        Colour = BlockingHelper.GetNextBlockingGroupColour(Model.Parent, Model.Parent.Parent.Sequence);
                    }
                }
                _blocking.NotifyBlockingOrSequenceGroupsChanged();
                OnPropertyChanged("SequenceGroup");
            }
        }

        public BulkObservableCollection<PlanogramBlockingGroupItemViewModel> MetaPropertyItemValues
        {
            get
            {
                FetchPerformanceValues();

                return _metaPropertyItemsValues;
            }
        }

        public Boolean ShowPerformanceMetrics
        {
            get { return (_metaPropertyItemsValues != null && _metaPropertyItemsValues.Any()); }
        }

        /// <summary>
        /// The type of percentage dispalyed to/ entered by the user when limiting block space
        /// </summary>
        public LimitBlockChangeByPercentageOfType LimitBlockSpaceByPercentageOfType
        {
            get { return _limitBlockChangeByPercentageOfType; }
            set
            {
                _limitBlockChangeByPercentageOfType = value;
                OnPropertyChanged("LimitBlockSpaceByPercentageOfType");
            }
        }

        #endregion


        #region Constructor

        /// <summary>
        /// Instantiates a new view model for the given <paramref name="blockingGroup"/> and parent <paramref name="blocking"/>.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <param name="blocking"></param>
        public PlanogramBlockingGroupViewModel(PlanogramBlockingGroup blockingGroup, PlanogramBlockingViewModel blocking)
            : base(blockingGroup)
        {
            _blocking = blocking;
            _blockingGroup = blockingGroup;
            OnPropertyChanged("SequenceGroup");

            FetchPerformanceValues();
        } 

        #endregion

        #region Methods

        /// <summary>
        /// Raises a property changed event for the <see cref="SequenceGroup"/> property.
        /// </summary>
        public void NotifySequenceGroupChanged()
        {
            OnPropertyChanged("SequenceGroup");
        } 
        
        /// <summary>
        /// For the currently selected node, retrieve the performance data metrics and associated
        /// values to display in a list box
        /// </summary>
        private void FetchPerformanceValues()
        {
            _metaPropertyItemsValues.Clear();

            if (_blockingGroup == null)
            {
                return;
            }

            // Get the parent planogram to get the performance data 
            var parentPlan = _blocking.Model.Parent as Planogram;
            if (parentPlan == null || parentPlan.Performance == null)
            {
                return;
            }

            // Find the plan performance metrics details
            var metaPropertyItems = new List<PlanogramBlockingGroupItemViewModel>();
            var perfMetrics = parentPlan.Performance.Metrics;

            for (Byte metricCount = 0; metricCount < perfMetrics.Count(); metricCount++)
            {
                var newMetricItem = new PlanogramBlockingGroupItemViewModel()
                {
                    MetaPropertyName = perfMetrics[metricCount].Name,

                    MetaPropertyValue = GetPerformanceMetricValue(_blockingGroup, metricCount),
                    MetaPropertyPercent = GetPerformanceMetricPercent(_blockingGroup, metricCount),
                };

                if (newMetricItem.MetaPropertyValue != null || newMetricItem.MetaPropertyPercent != null)
                {
                    metaPropertyItems.Add(newMetricItem);
                }
            }

            _metaPropertyItemsValues.AddRange(metaPropertyItems);
        }

        /// <summary>
        /// Return the meta performance data property given the approrpiate metric number
        /// </summary>
        /// <param name="blockingGroup">The CDT node to retrieve the data from</param>
        /// <param name="metricCount">The metric number</param>
        /// <returns>The matching performance data value</returns>
        private static Single? GetPerformanceMetricValue(PlanogramBlockingGroup blockingGroup, Byte metricCount)
        {
            if (blockingGroup == null) return null;

            switch (metricCount)
            {
                case 0: return blockingGroup.MetaP1;
                case 1: return blockingGroup.MetaP2;
                case 2: return blockingGroup.MetaP3;
                case 3: return blockingGroup.MetaP4;
                case 4: return blockingGroup.MetaP5;
                case 5: return blockingGroup.MetaP6;
                case 6: return blockingGroup.MetaP7;
                case 7: return blockingGroup.MetaP8;
                case 8: return blockingGroup.MetaP9;
                case 9: return blockingGroup.MetaP10;
                case 10: return blockingGroup.MetaP11;
                case 11: return blockingGroup.MetaP12;
                case 12: return blockingGroup.MetaP13;
                case 13: return blockingGroup.MetaP14;
                case 14: return blockingGroup.MetaP15;
                case 15: return blockingGroup.MetaP16;
                case 16: return blockingGroup.MetaP17;
                case 17: return blockingGroup.MetaP18;
                case 18: return blockingGroup.MetaP19;
                case 19: return blockingGroup.MetaP20;

                default:
                    return null;
            }
        }

        /// <summary>
        /// Return the meta percentage performance data property given the approrpiate metric number
        /// </summary>
        /// <param name="blockingGroup">The CDT node to retrieve the data from</param>
        /// <param name="metricCount">The metric number</param>
        /// <returns>The matching meta percentage performance data value</returns>
        private static Single? GetPerformanceMetricPercent(PlanogramBlockingGroup blockingGroup, Byte metricCount)
        {
            if (blockingGroup == null) return null;

            switch (metricCount)
            {
                case 0: return blockingGroup.MetaP1Percentage;
                case 1: return blockingGroup.MetaP2Percentage;
                case 2: return blockingGroup.MetaP3Percentage;
                case 3: return blockingGroup.MetaP4Percentage;
                case 4: return blockingGroup.MetaP5Percentage;
                case 5: return blockingGroup.MetaP6Percentage;
                case 6: return blockingGroup.MetaP7Percentage;
                case 7: return blockingGroup.MetaP8Percentage;
                case 8: return blockingGroup.MetaP9Percentage;
                case 9: return blockingGroup.MetaP10Percentage;
                case 10: return blockingGroup.MetaP11Percentage;
                case 11: return blockingGroup.MetaP12Percentage;
                case 12: return blockingGroup.MetaP13Percentage;
                case 13: return blockingGroup.MetaP14Percentage;
                case 14: return blockingGroup.MetaP15Percentage;
                case 15: return blockingGroup.MetaP16Percentage;
                case 16: return blockingGroup.MetaP17Percentage;
                case 17: return blockingGroup.MetaP18Percentage;
                case 18: return blockingGroup.MetaP19Percentage;
                case 19: return blockingGroup.MetaP20Percentage;

                default:
                    return null;
            }
        }

        /// <summary>
        /// Raises a property changed event for the <see cref="AvailableSequenceGroups"/> property.
        /// </summary>
        public void NotifyAvailableSequenceGroupsChanged()
        {
            OnPropertyChanged("AvailableSequenceGroups");
        }

        #endregion

        #region IDataErrorInfo members
        string IDataErrorInfo.Error
        {
            get 
            {
                return ((IDataErrorInfo)Model).Error;
            }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                if (columnName.Equals("SequenceGroup"))
                {
                    return ((IDataErrorInfo)Model)[PlanogramBlockingGroup.ColourProperty.Name];
                }
                else
                {
                    return ((IDataErrorInfo)Model)[columnName];
                }
            }
        } 
        #endregion
    }

    /// <summary>
    /// UI-specific to determine which value the user can enter when limiting block percentage.
    /// </summary>
    public enum LimitBlockChangeByPercentageOfType
    {
        PlanogramSpace = 0,
        BlockSpace = 1
    }

    public static class LimitBlockChangeByPercentageOfTypeHelper
    {
        public static readonly Dictionary<LimitBlockChangeByPercentageOfType, String> FriendlyNames =
            new Dictionary<LimitBlockChangeByPercentageOfType, String>()
            {
                {LimitBlockChangeByPercentageOfType.PlanogramSpace, Message.Enum_LimitBlockChangeBypercentageOfType_PlanogramSpace},
                {LimitBlockChangeByPercentageOfType.BlockSpace, Message.Enum_LimitBlockChangeBypercentageOfType_BlockSpace},
            };
    }

    public class PlanogramBlockingGroupItemViewModel
    {
        public String MetaPropertyName { get; set; }
        public Single? MetaPropertyValue { get; set; }
        public Single? MetaPropertyPercent { get; set; }
    }
}
