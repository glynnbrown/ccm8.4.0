﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 820
// V8-31077 : A.Kuszyk
//  Created.
// V8-31259 : A.Kuszyk
//  Protected against null value assignment in BlockingGroup property.
// V8-31321 : D.Pleasance
//  Added AvailableGroups
#endregion

#region Version History : CCM830
// V8-32661 : A.Kuszyk
//  Added components collection to take care of sequence template link between sequence groups and components.
// V8-32891 : D.Pleasance
//  Added defensive code to constructor and components param
#endregion
#endregion

using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// An implementation of a <see cref="PlanogramSequenceGroupViewModelBase"/> view model 
    /// representing a <see cref="PlanogramSequenceGroup"/>.
    /// </summary>
    public class PlanogramSequenceGroupViewModel : PlanogramSequenceGroupViewModelBase
    {
        #region Fields

        /// <summary>
        /// The view model representing the <see cref="PlangramBlocking"/> that this view model
        /// refers to.
        /// </summary>
        private PlanogramBlockingViewModel _blocking;

        /// <summary>
        /// The <see cref="PlanogramComponentViewModelBase"/>s that this sequence group is associated with. When this
        /// view model's colour or name changes, these components will be updated as well.
        /// </summary>
        private IEnumerable<PlanogramComponentViewModelBase> _components;

        #endregion

        #region Properties

        /// <summary>
        /// The <see cref="Groups"/> arranged in ascending name order, with a blank option first
        /// for selection purposes.
        /// </summary>
        public IEnumerable<PlanogramBlockingGroupViewModel> AvailableGroups
        {
            get
            {
                if (_blocking == null) return null;
                List<PlanogramBlockingGroupViewModel> selectorGroups = new List<PlanogramBlockingGroupViewModel>();
                selectorGroups.Add(new PlanogramBlockingGroupViewModel(PlanogramBlockingGroup.NewPlanogramBlockingGroup(), _blocking));
                selectorGroups.AddRange(_blocking.Groups.OrderBy(g => g.Name));
                return selectorGroups;
            }
        }

        /// <summary>
        /// Helper property to enable the mapping of this <see cref="PlanogramSequenceGroup"/> to a 
        /// <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public PlanogramBlockingGroupViewModel BlockingGroup
        {
            get
            {
                if (_blocking == null) return null;
                return _blocking.Groups.FirstOrDefault(g => g.Colour == this.Colour);
            }
            set
            {
                if (value == null || _blocking == null) return;
                if (_blocking.Groups.Contains(value))
                {
                    value.Colour = this.Colour;
                    value.Name = this.Name;
                    value.NotifySequenceGroupChanged();
                }
                else
                {
                    // We need to un-assign the current blocking group from
                    // this sequence group.
                    PlanogramBlockingGroupViewModel blockingGroup = _blocking.Groups.FirstOrDefault(g => g.Colour == this.Colour);
                    if (blockingGroup != null)
                    {
                        blockingGroup.Colour = BlockingHelper.GetNextBlockingGroupColour(_blocking.Model, this.Model.Parent);
                        blockingGroup.NotifySequenceGroupChanged();
                    }
                }
                _blocking.NotifyBlockingOrSequenceGroupsChanged();
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Instantiates a new view model for the given <paramref name="sequenceGroup"/> and its associated <paramref name="blocking"/>.
        /// </summary>
        /// <param name="sequenceGroup"></param>
        /// <param name="blocking"></param>
        public PlanogramSequenceGroupViewModel(PlanogramSequenceGroup sequenceGroup, PlanogramBlockingViewModel blocking)
            : base(sequenceGroup)
        {
            _blocking = blocking;
            _components = null;
            OnPropertyChanged("BlockingGroup");
        }

        /// <summary>
        /// Instantiates a new view model for the given <paramref name="sequenceGroup"/> and its associated <paramref name="components"/>.
        /// </summary>
        /// <param name="sequenceGroup"></param>
        /// <param name="blocking"></param>
        public PlanogramSequenceGroupViewModel(PlanogramSequenceGroup sequenceGroup, IEnumerable<PlanogramComponentViewModelBase> components)
            : base(sequenceGroup)
        {
            _blocking = null;
            if (components != null)
            {
                _components = components.ToList();
            }
        }      
        #endregion

        #region Event Handlers
        protected override void OnPropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e == null || e.PropertyName == null)
            {
                return;
            }
            else if (_components != null && e.PropertyName.Equals(PlanogramSequenceGroup.NameProperty.Name))
            {
                foreach (PlanogramComponentViewModelBase componentView in _components)
                {
                    componentView.Name = Name;
                }
            }
            else if (_components != null && e.PropertyName.Equals(PlanogramSequenceGroup.ColourProperty.Name))
            {

                foreach (PlanogramSubComponentViewModelBase subComponentView in _components.SelectMany(c => c.EnumerateSubComponentViews()))
                {
                    subComponentView.FillColourFront = Colour;
                }
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Raises a property changed notification for the <see cref="BlockingGroup"/> property.
        /// </summary>
        public void NotifyBlockingGroupChanged()
        {
            OnPropertyChanged("BlockingGroup");
        }

        /// <summary>
        /// Raises a property changed notification for the <see cref="AvailableGroups"/> property.
        /// </summary>
        public void NotifyAvailableGroupsChanged()
        {
            OnPropertyChanged("AvailableGroups");
        }

        #endregion
    }
}
