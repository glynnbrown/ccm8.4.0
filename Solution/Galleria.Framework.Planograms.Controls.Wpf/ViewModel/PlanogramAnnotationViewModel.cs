﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion
#endregion

using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple viewmodel for a PlanogramAnnotation
    /// </summary>
    public class PlanogramAnnotationViewModel : PlanogramAnnotationViewModelBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="annotation"></param>
        public PlanogramAnnotationViewModel(PlanogramAnnotation annotation)
            :base(annotation)
        {
        }

    }
}
