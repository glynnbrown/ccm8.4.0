﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion
#region Version History : CCM 820
// V8-30982 : A.Kuszyk
//  Added Component property.
#endregion
#endregion

using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple implementation of a subcomponent viewmodel.
    /// </summary>
    public class PlanogramSubComponentViewModel : PlanogramSubComponentViewModelBase
    {
        #region Fields

        private CastedObservableCollection<PlanogramPositionViewModel, PlanogramPositionViewModelBase> _positionsRO;
        private CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase> _annotationsRO;
        private ReadOnlyObservableCollection<PlanogramSubComponentDivider> _dividersRO;

        #endregion

        #region Properties

        /// <summary>
        /// The parent component that this subcomponent belongs to.
        /// </summary>
        public PlanogramComponentViewModelBase Component
        {
            get { return ParentComponentView; }
        }

        /// <summary>
        /// Returns the collection of positions held by this subcomponent.
        /// </summary>
        public CastedObservableCollection<PlanogramPositionViewModel, PlanogramPositionViewModelBase> Positions
        {
            get
            {
                if (_positionsRO == null)
                {
                    _positionsRO =
                        new CastedObservableCollection<PlanogramPositionViewModel, PlanogramPositionViewModelBase>(base.PositionViews, true);
                }
                return _positionsRO;
            }
        }

        /// <summary>
        /// Returns the collection of annotations held by this subcomponent.
        /// </summary>
        public CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase> Annotations
        {
            get
            {
                if (_annotationsRO == null)
                {
                    _annotationsRO = new CastedObservableCollection<PlanogramAnnotationViewModel, PlanogramAnnotationViewModelBase>(base.AnnotationViews, true);
                }
                return _annotationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of dividers placed on this subcomponent.
        /// </summary>
        public ReadOnlyObservableCollection<PlanogramSubComponentDivider> Dividers
        {
            get
            {
                if (_dividersRO == null)
                {
                    _dividersRO = new ReadOnlyObservableCollection<PlanogramSubComponentDivider>(base.DividerViews);
                }
                return _dividersRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentComponentView"></param>
        /// <param name="subComponent"></param>
        public PlanogramSubComponentViewModel(PlanogramComponentViewModel parentComponentView, PlanogramSubComponent subComponent)
            : base(parentComponentView, subComponent)
        {
            InitializeChildViews();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the view of the given annotation model
        /// </summary>
        protected override PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation annotation)
        {
            return new PlanogramAnnotationViewModel(annotation);
        }

        /// <summary>
        /// Returns the view of the given position model
        /// </summary>
        protected override PlanogramPositionViewModelBase CreatePositionView(PlanogramPosition position)
        {
            return new PlanogramPositionViewModel(this, position);
        }

        /// <summary>
        /// Updates the dividers collection
        /// </summary>
        internal void UpdateDividers(IEnumerable<PlanogramSubComponentDivider> dividers)
        {
            if (DividerViews.Count > 0)
            {
                DividerViews.Clear();
            }

            foreach (PlanogramSubComponentDivider d in dividers)
            {
                DividerViews.Add(d);
            }
        }

        #endregion

    }


}
