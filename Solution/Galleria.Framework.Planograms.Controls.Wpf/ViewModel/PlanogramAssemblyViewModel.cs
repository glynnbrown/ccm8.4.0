﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
#endregion
#endregion

using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;

namespace Galleria.Framework.Planograms.Controls.Wpf.ViewModel
{
    /// <summary>
    /// Simple viewmodel implementation for PlanogramAssembly
    /// </summary>
    public class PlanogramAssemblyViewModel : PlanogramAssemblyViewModelBase
    {
        #region Fields

        private PlanogramFixtureViewModel _parentFixtureView;
        private CastedObservableCollection<PlanogramComponentViewModel, PlanogramComponentViewModelBase> _componentsRO;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of child component viewmodels.
        /// </summary>
        public CastedObservableCollection<PlanogramComponentViewModel, PlanogramComponentViewModelBase> Components
        {
            get
            {
                if (_componentsRO == null)
                {
                    _componentsRO =
                        new CastedObservableCollection<PlanogramComponentViewModel, PlanogramComponentViewModelBase>(base.ComponentViews, true);
                }
                return _componentsRO;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentFixtureView"></param>
        /// <param name="fixtureAssembly"></param>
        public PlanogramAssemblyViewModel(PlanogramFixtureViewModel parentFixtureView,
            PlanogramFixtureAssembly fixtureAssembly, PlanogramAssembly assembly)
            : base(parentFixtureView, fixtureAssembly, assembly)
        {
            _parentFixtureView = parentFixtureView;
            InitializeChildViews();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new viewmodel for the given assembly component.
        /// </summary>
        /// <param name="assemblyComponent"></param>
        /// <returns></returns>
        protected override PlanogramComponentViewModelBase CreateComponentView(PlanogramAssemblyComponent assemblyComponent)
        {
            PlanogramComponent component = assemblyComponent.GetPlanogramComponent();
            if (component == null) return null;
            return new PlanogramComponentViewModel(this, assemblyComponent, component);
        }

        #endregion

    }
}
