﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Controls.Wpf.Themes
{
    /// <summary>
    /// Holds the resource keys for all resources 
    /// defined in the application resource dictionaries.
    /// </summary>
    public static class ResourceKeys
    {
        #region Theme_BlockingEditor

        public static readonly String TmpDefaultBlockingEditor = "TmpDefaultBlockingEditor";
        public static readonly String StyDefaultBlockingEditor = "StyDefaultBlockingEditor";

        public static readonly String TmpDefaultBlockingRuler = "TmpDefaultBlockingRuler";
        public static readonly String StyDefaultBlockingRuler = "StyDefaultBlockingRuler";

        public static readonly String TmpDefaultBlockingDivider = "TmpDefaultBlockingDivider";
        public static readonly String StyDefaultBlockingDivider = "StyDefaultBlockingDivider";

        public static readonly String TmpDefaultBlockArea = "TmpDefaultBlockArea";
        public static readonly String StyDefaultBlockArea = "StyDefaultBlockArea";

        #endregion

        #region Theme_PlanogramViewport

        public static readonly String TmpDefaultPlanogramViewport3D = "TmpDefaultPlanogramViewport3D";
        public static readonly String StyDefaultPlanogramViewport3D = "StyDefaultPlanogramViewport3D";


        #endregion
    }
}
