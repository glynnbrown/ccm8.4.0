﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25743 L.Ineson ~ Created
// CCM-25977 : N.Haywood
//  Used equals instead of == when finding IModelConstruct3DData
// V8-26636 : L.Ineson
//  Added framework structure extensions.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30311 : N.Haywood
//  Removed NearPlaneDistance
#endregion

#region Version History: (CCM 8.2.0)
// V8-30982 : M.Shelley
//  Change the call to the ZoomToFitBounds method to allow the disabling the enlargement of the 
//  PlanogramViewport3D bounds so that the highlight position layer lines up with the underlying
//  highlight and watermark viewports in the Blocking Editor
#endregion

#region Version History: (CCM 8.3.0)
//V8-31589 : L.Ineson
//  Added SetupDefaultLighting helper.
// V8-31722 : A.Kuszyk
//  Set NearPlaneDistance for orthographic cameras.
#endregion

#endregion

using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Framework.Planograms.Controls.Wpf.Helpers
{
    /// <summary>
    /// Contains helper methods for use with rendering controls.
    /// </summary>
    public static class RenderControlsHelper
    {

        /// <summary>
        /// Returns the model under the given planogram which represents this plan item.
        /// </summary>
        /// <param name="parentPlanModel"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static IModelConstruct3DData FindItemModelData(IModelConstruct3DData parentPlanModel, Object planPartItem)
        {

            IModelConstruct3DData data = null;

            if (parentPlanModel != null)
            {
                foreach (IModelConstruct3DData childData in parentPlanModel.ModelChildren)
                {
                    IPlanPart3DData wrapper = childData as IPlanPart3DData;
                    if (wrapper != null)
                    {
                        if (wrapper.PlanPart.Equals(planPartItem))
                        {
                            return childData;
                        }
                    }

                    data = FindItemModelData(childData, planPartItem);
                    if (data != null)
                    {
                        return data;
                    }
                }

            }
            return data;
        }

        /// <summary>
        /// Finds child visual for the given plan item.
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static ModelConstruct3D FindItemModel(ModelConstruct3D parentModel, Object planPartItem)
        {
            ModelConstruct3D visual = null;

            if (parentModel != null)
            {
                foreach (ModelConstruct3D childModel in parentModel.ModelChildren)
                {
                    IPlanPart3DData wrapper = childModel.ModelData as IPlanPart3DData;
                    if (wrapper != null)
                    {
                        if (wrapper.PlanPart.Equals(planPartItem))
                        {
                            return childModel;
                        }
                    }

                    visual = FindItemModel(childModel, planPartItem);
                    if (visual != null)
                    {
                        return visual;
                    }
                }

            }
            return visual;
        }

        /// <summary>
        /// Finds the bounding box for the specified visual.
        /// </summary>
        /// <param name="visual">
        /// The visual.
        /// </param>
        /// <param name="transform">
        /// The transform of visual.
        /// </param>
        /// <returns>
        /// </returns>
        public static Rect3D FindBounds(ModelVisual3D visual, GeneralTransform3D transform)
        {
            var bounds = Rect3D.Empty;

            GeneralTransform3DGroup childTransform = new GeneralTransform3DGroup();
            childTransform.Children.Add(transform);
            //childTransform.Children.Add(visual.Transform); //come out wrong if this is included.

            Model3D model = visual.Content;
            if (model != null)
            {
                // apply transform
                var transformedBounds = childTransform.TransformBounds(model.Bounds);
                if (!double.IsNaN(transformedBounds.X))
                {
                    bounds.Union(transformedBounds);
                }
            }

            foreach (var child in GetChildren(visual))
            {
                var b = FindBounds(child, childTransform);
                bounds.Union(b);
            }

            return bounds;
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <param name="visual">
        /// The visual.
        /// </param>
        /// <returns>
        /// </returns>
        private static IEnumerable<ModelVisual3D> GetChildren(Visual3D visual)
        {
            int n = VisualTreeHelper.GetChildrenCount(visual);
            for (int i = 0; i < n; i++)
            {
                var child = VisualTreeHelper.GetChild(visual, i) as ModelVisual3D;
                if (child == null)
                {
                    continue;
                }

                yield return child;
            }
        }

    }
}
