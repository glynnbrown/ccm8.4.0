﻿// Copyright © Galleria RTS Ltd 2016

using Galleria.Framework.Rendering;
using HelixToolkit.Wpf;
using System.Linq;
using System.Windows.Media.Media3D;

namespace Galleria.Framework.Planograms.Controls.Wpf.Helpers
{

    /// <summary>
    /// Contains helper methods for adding lights to a planogram viewport.
    /// </summary>
    public class PlanLightingHelper
    {
        /// <summary>
        /// Applies the default lighting set to the viewport.
        /// </summary>
        /// <param name="mainViewport">the view port to add lights to.</param>
        /// <param name="viewType"> the view type</param>
        public static void SetupDefaultLighting(HelixViewport3D mainViewport, CameraViewType viewType)
        {
            //clear any existing lighting:
            foreach (var child in mainViewport.Children.ToList())
            {
                if (child is SunLight) mainViewport.Children.Remove(child);
            }



            //Front light:
            mainViewport.Children.Add(new SunLight());

            //Back light:
            mainViewport.Children.Add(
                new SunLight()
                {
                    Ambient = 0.1D,
                    Transform = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), 180))
                });


            //If perspective then we are done so return out.
            if (viewType == CameraViewType.Perspective) return;


            //Top Light:
            mainViewport.Children.Add(
               new SunLight()
               {
                   Ambient = 0.1D,
                   Transform = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), 90))
               });

            //Bottom Light:
            mainViewport.Children.Add(
               new SunLight()
               {
                   Ambient = 0.1D,
                   Transform = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), 180))
               });


            //Left Light:
            mainViewport.Children.Add(
               new SunLight()
               {
                   Ambient = 0.1D,
                   Transform = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), 90))
               });


        }
    }
}
