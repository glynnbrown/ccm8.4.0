﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26888 L.Ineson 
//  Created
#endregion
#region Version History: (CCM 8.01)
// V8-28685 L.Ineson 
//  Stopped images failing when plan has annotations by adding defensive code.
//  Also now hides annotation models until we sort out rendering them properly.
// V8-28671 : D.Pleasance
//  Amended GetGeometry method to prevent the duplication of transform matrix
#endregion
#region Version History: (CCM 8.10)
//  V8-28803 : M.Brumby
//      Annotations are back and this time its renderable.
// V8-29231 : L.Ineson
//  Added FixtureLabelText
#endregion
#region Version History: (CCM 8.11)
// V8-30390 : M.Brumby
//  Just made things betterer.
#endregion
#region Version History: (CCM 8.20)
//V8-30702 : L.Ineson
//  Added additional code to deal with rendering geometry that has a material group.
// V8-31202 : L.Ineson
//  Products which overhang bays now get moved to the bay on which they are most prevalant when displaying 
// bays per page.
// V8-31206 : L.Ineson
//  Moved bulk of layout code into PlanogramImageControl
#endregion
#region Version History: (CCM 8.30)
// CCM-18454 : L.Ineson
//  Improved image quality and compression.
#endregion
#region Version History: (CCM 8.40)
// CCM-19218 : J.Mendes
//  Optimized image quality and compression without compromising the image/file size.
//      Increased the dpi from 250 to 300 because is the higher common resolution for printing.
//      Reduced the the QualityLevel of the jpgEncoder from 75 to 52 to not compromise the file size.
#endregion
#endregion

using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Planograms.Controls.Wpf.PlanogramViewport;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Threading;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace Galleria.Framework.Planograms.Controls.Wpf.Helpers
{
    public sealed class PlanogramImageRenderer : IPlanogramImageRenderer
    {
        #region Fields

        private IPlanogramImageSettings _imageSettings;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the height of the produced image
        /// </summary>
        public Double ImageHeight { get; set; }

        /// <summary>
        /// Gets/Sets the width of the produced image
        /// </summary>
        public Double ImageWidth { get; set; }


        /// <summary>
        /// Gets/Sets whether the software renderer should be used.
        /// </summary>
        public Boolean IsSoftwareRender { get; set; }

        /// <summary>
        /// Gets/Sets the image settings to use
        /// </summary>
        public IPlanogramImageSettings ImageSettings
        {
            get { return _imageSettings; }
            set { _imageSettings = value; }
        }

        /// <summary>
        /// Get/Sets whether the image should be scaled
        /// to fit the height and width fully. If off 
        /// then it will be scaled proportionately to the maximum.
        /// </summary>
        public Boolean FitUnproportionately { get; set; }

        public List<Plan3DData> PlanogramModelDataList { get; set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramImageRenderer() { }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the image data for the given plan
        /// based on the current settings.
        /// </summary>
        public Byte[] CreateImageData(Planogram plan)
        {
            return CreateImageData(plan, 0, 0);
        }

        /// <summary>
        /// Returns the image data for the given plan
        /// based on the current settings.
        /// </summary>
        /// <param name="plan">the plan to render</param>
        /// <param name="bayRangeStart">the number of bay to start from, 0 if from start.</param>
        /// <param name="bayRangeEnd">the number of bay to end on, 0 if to end.</param>
        public Byte[] CreateImageData(Planogram plan, Int32 bayRangeStart, Int32 bayRangeEnd)
        {
            Byte[] imageBlob = null;
            
            // use a background dispatcher to ensure that
            // we are executing the render on an STA thread
            using (BackgroundDispatcher dispatcher = new BackgroundDispatcher())
            {
                dispatcher.Invoke(true, () =>
                {
                    try
                    {
                        PlanogramImageControl renderControl =
                            new PlanogramImageControl(plan, bayRangeStart, bayRangeEnd,
                                this.ImageHeight, this.ImageWidth, this.ImageSettings, PlanogramModelDataList);

                        renderControl.ApplySoftwareRender = this.IsSoftwareRender;
                        renderControl.FitUnproportionally = this.FitUnproportionately;

                        renderControl.InvalidateVisual();
                        renderControl.Arrange(new Rect(new Point(0, 0), new Point(this.ImageWidth, this.ImageHeight)));
                        renderControl.UpdateLayout();


                        // scale dimensions from 96 dpi to get better quality
                        Double scale = 300 / 96;

                        // perform a straight render.
                        RenderTargetBitmap renderTargetBitmap =
                            new RenderTargetBitmap(
                            (Int32)(scale * this.ImageWidth),
                            (Int32)(scale * this.ImageHeight),
                            scale * 96,
                            scale * 96,
                            PixelFormats.Default);
                        renderTargetBitmap.Render(renderControl);

                        // encode
                        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                        {
                            //Encode as jpeg so the pdf isnt huge.
                            JpegBitmapEncoder jpgEncoder = new JpegBitmapEncoder();
                            jpgEncoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                            jpgEncoder.QualityLevel = 52;
                            jpgEncoder.Save(memoryStream);

                            //PngBitmapEncoder encoder = new PngBitmapEncoder();
                            //encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                            //encoder.Save(memoryStream);

                            // Export the stream to a byte array
                            imageBlob = CreateImageBlob(memoryStream, true);

                            // Shutdown rendering thread (if alive)
                            if (renderTargetBitmap.Dispatcher.Thread.IsAlive)
                            {
                                renderTargetBitmap.Dispatcher.InvokeShutdown();
                            }

                        }

                        renderControl.Dispose();
                        renderControl = null;

                    }
                    catch (Exception)
                    {
                        //catch everything so that this does not kill the processor.
                        Debug.WriteLine("Image render failed");
                    }
                });


            }
            return imageBlob;
        }

        /// <summary>
        /// Creates a Byte array from a Stream
        /// </summary>
        /// <param name="dataStream">Stream to read</param>
        /// <param name="CloseStream">Closes the Stream once finished if True.</param>
        /// <returns>Array of Bytes</returns>
        private static Byte[] CreateImageBlob(Stream dataStream, Boolean CloseStream)
        {
            Byte[] blob = new Byte[dataStream.Length];
            //Ensure stream is at the start
            dataStream.Position = 0;

            //Stream read does not necessarily read all the bytes asked for so
            //a loop is required to ensure all bytes are read.
            for (Int32 pos = 0; pos < dataStream.Length; )
            {
                Int32 len = dataStream.Read(blob, pos, (Int32)dataStream.Length - pos);
                if (len == 0)
                {
                    break;
                }
                pos += len;

            }

            if (CloseStream) dataStream.Close();
            return blob;
        }

        /// <summary>
        /// Gets a bitmap image from a memory stream
        /// </summary>
        /// <param name="dataStream">memory stream</param>
        /// <returns>BitmapImage</returns>
        public static BitmapImage GetBitmapImage(MemoryStream dataStream)
        {
            BitmapImage img = new BitmapImage();
            dataStream.Position = 0;
            img.BeginInit();
            img.StreamSource = new MemoryStream(dataStream.ToArray());
            img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            img.CacheOption = BitmapCacheOption.Default;
            img.EndInit();
            img.Freeze();
            return img;
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        public void Dispose()
        {
            //nothing to do.
        }

        #endregion
     
    }


    #region Supporting classes

    internal static class Renderer
    {

        #region Helpers

        /// <summary>
        /// Work out the Distance to the trangle from a screen position X/Y
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <returns></returns>
        private static Double ZDepth(Single X, Single Y, Vector3D point1, Vector3D point2, Vector3D point3)
        {
            //Use Maths to work things out. YAY Maths.
            Double i = (((point2.Y - point1.Y) * (point3.Z - point1.Z)) - ((point3.Y - point1.Y) * (point2.Z - point1.Z)));
            Double j = (((point2.Z - point1.Z) * (point3.X - point1.X)) - ((point3.Z - point1.Z) * (point2.X - point1.X)));
            Double k = (((point2.X - point1.X) * (point3.Y - point1.Y)) - ((point3.X - point1.X) * (point2.Y - point1.Y)));

            return ((point1.X - X) * i + (point1.Y - Y) * j) / k + point1.Z;
        }


        /// <summary>
        /// Check to see if a point X/Y is on the correct side of one of the triangle lines
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private static Boolean InsideLine(Single X, Single Y, Int16 lineNumber, Vector3D point1, Vector3D point2, Vector3D point3)
        {
            Vector3D currentPosition = new Vector3D(X, Y, point1.Z);
            return InsideLine(currentPosition, lineNumber, point1, point2, point3);
        }

        /// <summary>
        ///  Check to see if a point X/Y is on the correct side of one of the triangle lines
        /// </summary>
        /// <param name="currentPosition"></param>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private static Boolean InsideLine(Vector3D currentPosition, Int16 lineNumber, Vector3D point1, Vector3D point2, Vector3D point3)
        {
            if (lineNumber == 1)
            {
                return Vector3D.CrossProduct(point2 - point1, currentPosition - point1).Z <= 0;
            }
            if (lineNumber == 2)
            {
                return Vector3D.CrossProduct(currentPosition - point1, point3 - point1).Z <= 0;
            }
            if (lineNumber == 3)
            {
                return Vector3D.CrossProduct(point2 - currentPosition, point3 - currentPosition).Z <= 0;
            }

            return false;
        }

        /// <summary>
        /// Check to see if a point XY is inside the triangle
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <returns></returns>
        private static Boolean InTriangle(Single X, Single Y, Vector3D point1, Vector3D point2, Vector3D point3, out Single xOut, out Single yOut)
        {
            //if we are on the correct side of each line then we are in the triangle
            return InTriangle(X, Y, point1, point2, point3, true, out xOut, out yOut);
        }

        private static Boolean InTriangle(Single X, Single Y, Vector3D point1, Vector3D point2, Vector3D point3, Boolean checkNear, out Single xOut, out Single yOut)
        {
            const Single near = 0.1f;
            xOut = X;
            yOut = Y;
            Vector3D currentPosition = new Vector3D(X, Y, point1.Z);
            Boolean l1 = InsideLine(currentPosition, 1, point1, point2, point3);
            Boolean l2 = InsideLine(currentPosition, 2, point1, point2, point3);
            Boolean l3 = InsideLine(currentPosition, 3, point1, point2, point3);

            Boolean inSide = l1 && l2 && l3;

            if (!inSide && checkNear)
            {
                for (Single dx = -near; dx <= near; dx += near)
                {
                    for (Single dy = -near; dy <= near; dy += near)
                    {
                        inSide = InTriangle(X + dx, Y + dy, point1, point2, point3, false, out xOut, out yOut);
                        if (inSide)
                        {
                            xOut = X + dx;
                            yOut = Y + dy;
                            return inSide;
                        }
                    }
                }
            }

            //if we are on the correct side of each line then we are in the triangle
            return inSide;
        }


        /// <summary>
        /// Work out if the point XY is on the edge of the triangle. This will return
        /// false if a line is not allowed on that edge.
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <returns></returns>
        private static Boolean EdgeDetection(Single X, Single Y, Vector3D point1, Vector3D point2, Vector3D point3, Boolean line1, Boolean line2, Boolean line3)
        {
            line1 = line1 && EdgeDetection(X, Y, 1, point1, point2, point3);
            line2 = line2 && EdgeDetection(X, Y, 2, point1, point2, point3);
            line3 = line3 && EdgeDetection(X, Y, 3, point1, point2, point3);
            //Check the edge of each line in the triangle
            return line1 || line2 || line3;
        }

        /// <summary>
        /// Work out if the point XY is on the edge of a line. This will return
        /// false if a line is not allowed on that edge. This assumes that the 
        /// point XY has already passed a check to see if it is within the triangle.
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private static Boolean EdgeDetection(Single X, Single Y, Int16 lineNumber, Vector3D point1, Vector3D point2, Vector3D point3)
        {
            //Check arround the point to see if there are any points not on the correct
            //side of the line.
            Int32 range = 1;
            for (Int32 i = -range; i <= range; i++)
            {
                for (Int32 j = -range; j <= range; j++)
                {
                    //if a point is not on the correct side of the line
                    //then consider this an edge.
                    if (!InsideLine(X + i, Y + j, lineNumber, point1, point2, point3))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Draws a trangle.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="maxHeight"></param>
        /// <param name="maxWidth"></param>
        /// <param name="colour"></param>
        /// <param name="pixelData"></param>
        /// <param name="depthBuffer"></param>
        /// <param name="estimateBorders"></param>
        private static void AddTriangleToBufferAlt1(MeshGeometry3D mesh, Matrix3D worldViewProjection, Int32 indicesStart, Int32 maxHeight, Int32 maxWidth, Color colour, ref byte[] pixelData, ref Double[] depthBuffer, Dictionary<UInt64, Boolean> lineReference, Boolean drawBorders)
        {
            Point3D p1 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart]]);
            Point3D p2 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 1]]);
            Point3D p3 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 2]]);
            Vector3D temp1 = RenderHelper.Point3DToVector3D(p1);
            Vector3D temp2 = RenderHelper.Point3DToVector3D(p2);
            Vector3D temp3 = RenderHelper.Point3DToVector3D(p3);
            Boolean IsBackFacing = Vector3D.CrossProduct(temp1 - temp2, temp3 - temp1).Z <= 0;
            //IsBackFacing = false;
            //Don't draw if the triangle is facing away from the camera
            if (!IsBackFacing)
            {
                #region MinMax
                Double[] Xlist = new Double[]
                    {
                        p1.X,
                        p2.X,
                        p3.X
                    };
                Int32 minX = (Int32)Xlist.Min() - 1;
                Int32 maxX = (Int32)(Xlist.Max() + 1);

                Double[] Ylist = new Double[]
                    {
                        p1.Y,
                        p2.Y,
                        p3.Y
                    };

                Double[] Zlist = new Double[]
                    {
                        p1.Z,
                        p2.Z,
                        p3.Z
                    };
                Int32 minY = (Int32)Ylist.Min() - 1;
                Int32 maxY = (Int32)(Ylist.Max() + 1);
                if (minY < 0) minY = 0;
                if (minX < 0) minX = 0;
                if (maxY > maxHeight) maxY = maxHeight;
                if (maxX > maxWidth) maxX = maxWidth;

                #endregion
                if (maxX - minX >= 1 && maxY - minY >= 1)
                {
                    #region Basic Colouring

                    //A very rough normalization of the colour.
                    Vector3D normal = mesh.Normals[mesh.TriangleIndices[indicesStart]];// temp1; // translateToScreen.Transform(temp1); //this is not really the normal
                    normal += mesh.Normals[mesh.TriangleIndices[indicesStart + 1]];
                    normal += mesh.Normals[mesh.TriangleIndices[indicesStart + 2]];
                    // normal = AbsNormalise(normal);
                    normal.Normalize();
                    Double lightIntensity = Math.Min(Math.Max(Vector3D.DotProduct(normal, new Vector3D(0, 0.5, 1)), 0), 1);
                    //lightIntensity = 1;
                    Color finalColour = new Color();
                    finalColour.R = (Byte)(colour.R * lightIntensity);
                    finalColour.G = (Byte)(colour.G * lightIntensity);
                    finalColour.B = (Byte)(colour.B * lightIntensity);
                    //finalColour.R = (Byte)(50 + 180 * lightIntensity);
                    //finalColour.G = (Byte)(50 * lightIntensity);
                    //finalColour.B = (Byte)(50 * lightIntensity);
                    #endregion

                    //Round down To pixel
                    //temp1.X = (Int32)temp1.X;
                    //temp2.X = (Int32)temp2.X;
                    //temp3.X = (Int32)temp3.X;
                    //temp1.Y = (Int32)temp1.Y;
                    //temp2.Y = (Int32)temp2.Y;
                    //temp3.Y = (Int32)temp3.Y;

                    temp1.X = Math.Round(temp1.X, 0);
                    temp2.X = Math.Round(temp2.X, 0);
                    temp3.X = Math.Round(temp3.X, 0);
                    temp1.Y = Math.Round(temp1.Y, 0);
                    temp2.Y = Math.Round(temp2.Y, 0);
                    temp3.Y = Math.Round(temp3.Y, 0);

                    p1.X = Math.Round(p1.X, 0);
                    p2.X = Math.Round(p2.X, 0);
                    p3.X = Math.Round(p3.X, 0);
                    p1.Y = Math.Round(p1.Y, 0);
                    p2.Y = Math.Round(p2.Y, 0);
                    p3.Y = Math.Round(p3.Y, 0);

                    Boolean line1 = false;
                    Boolean line2 = false;
                    Boolean line3 = false;
                    if (drawBorders)
                    {

                        line1 = CheckLineReference(lineReference, mesh, indicesStart, indicesStart + 1);
                        line2 = CheckLineReference(lineReference, mesh, indicesStart, indicesStart + 2);
                        line3 = CheckLineReference(lineReference, mesh, indicesStart + 2, indicesStart + 1);

                        //Get the distance between each point
                        Double distAC = p1.Distance(p3);
                        Double distAB = p1.Distance(p2);
                        Double distCB = p3.Distance(p2);

                        //Get the vector difference between each point
                        Vector3D diffAC = p3 - p1;
                        Vector3D diffAB = p2 - p1;
                        Vector3D diffCB = p2 - p3;

                        //set the drawing starting points
                        Point3D pointAC = p1;
                        Point3D pointAB = p1;
                        Point3D pointCB = p3;

                        //Draw triangle sides - (not greate for perspective, ok ish for Orthographic)
                        if (line1) DrawBorderLines(pointAB, diffAB, distAB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                        if (line2) DrawBorderLines(pointAC, diffAC, distAC, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                        if (line3) DrawBorderLines(pointCB, diffCB, distCB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);

                    }

                    //Itterate through all the pixels in the bounding area of the triangle
                    for (Int32 x = minX; x <= maxX; x++)
                    {
                        for (Int32 y = minY; y <= maxY; y++)
                        {
                            Single xFound = x, yFound = y;
                            //check that the current point is in the triangle
                            if (InTriangle(x, y, temp1, temp2, temp3, false, out xFound, out yFound))
                            {
                                //Workout the depth from the current points
                                Double depth = ZDepth(xFound, yFound, temp1, temp2, temp3);
                                Int32 Xoffset = x;
                                Int32 Yoffset = y;

                                //check to see if the point is within the screen maximums.
                                if (Xoffset < maxWidth &&
                                    Yoffset < maxHeight &&
                                    (Yoffset * maxWidth + Xoffset) * 4 + 3 < pixelData.Length)
                                {
                                    //If the current point is closer to the screen than the depth buffers record
                                    //of hte closest distance to the screen for this pixel then replace the
                                    //pixel colour with the current colour and update the depth buffer.
                                    if (depthBuffer[(Yoffset * maxWidth + Xoffset)] > depth) //add depth check
                                    {

                                        //Check to see if we should draw a border colour
                                        //if (EdgeDetection(xFound, yFound, temp1, temp2, temp3, line1, line2, line3))
                                        //{
                                        //    //we are using PixelFormats.Pbgra32 so add colors in the
                                        //    // order of Blue, Green, Red then Alpha
                                        //    pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 0] = 0;
                                        //    pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 1] = 0;
                                        //    pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 2] = 0;
                                        //}
                                        //else if(depthBuffer[(Yoffset * maxWidth + Xoffset)] > depth)
                                        //{
                                        //we are using PixelFormats.Pbgra32 so add colors in the
                                        // order of Blue, Green, Red then Alpha
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 0] = finalColour.B;
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 1] = finalColour.G;
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 2] = finalColour.R;
                                        //}
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 3] = (Byte)255;
                                        depthBuffer[(Yoffset * maxWidth + Xoffset)] = depth;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calculate the liniar interpolation for the given points
        /// </summary>
        /// <param name="kDistance">current distance along span k</param>
        /// <param name="k0">start of span k</param>
        /// <param name="k1">end of span k</param>
        /// <param name="f0">start of span f</param>
        /// <param name="f1">end of span f</param>
        /// <returns></returns>
        internal static Double Lerp(Double kDistance, Double k0, Double k1, Double f0, Double f1)
        {
            return Lerp(kDistance, k0, k1, f0, f1, 1d / (k1 - k0));
        }
        /// <summary>
        /// Calculate the liniar interpolation for the given points
        /// </summary>
        /// <param name="kDistance">current distance along span k</param>
        /// <param name="k0">start of span k</param>
        /// <param name="k1">end of span k</param>
        /// <param name="f0">start of span f</param>
        /// <param name="f1">end of span f</param>
        /// <param name="oneOverK1MinusK0"></param>
        /// <returns></returns>
        private static Double Lerp(Double kDistance, Double k0, Double k1, Double f0, Double f1, Double oneOverK1MinusK0)
        {
            if ((k1 - k0) == 0)
            {
                return f1;// (f0 + f1) * 0.5d;
            }
            return f0 + (kDistance - k0) * (f1 - f0) * oneOverK1MinusK0;
        }

        private static void OrderValues(ref Double v1, ref Double v2)
        {
            v1 = Math.Min(v1, v2);
            v2 = Math.Max(v1, v2);
        }

        private static void AddTriangleToBuffer(MeshGeometry3D mesh, ref Matrix3D worldViewProjection, ref Int32 indicesStart,
            ref Int32 maxHeight, ref Int32 maxWidth, ref Color colour, ref byte[] pixelData,
            ref Double[] depthBuffer, Dictionary<UInt64, Boolean> lineReference, Boolean drawBorders,
            RenderTargetBitmap bmp, ref byte[] pixels, ref Matrix3D worldSpace, Viewport3D viewport,
            Double texCoordCorrectionX, Double texCoordCorrectionY)
        {
            if (pixels == null) return;

            Point3D p1 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart]]);
            Point3D p2 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 1]]);
            Point3D p3 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 2]]);
            Vector3D temp1 = RenderHelper.Point3DToVector3D(p1);
            Vector3D temp2 = RenderHelper.Point3DToVector3D(p2);
            Vector3D temp3 = RenderHelper.Point3DToVector3D(p3);

            Boolean IsBackFacing = Vector3D.CrossProduct(temp1 - temp2, temp3 - temp1).Z <= 0;
            //IsBackFacing = false;
            //Don't draw if the triangle is facing away from the camera
            if (!IsBackFacing)
            {
                Point3D pos1 = worldSpace.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart]]);
                Point3D pos2 = worldSpace.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 1]]);
                Point3D pos3 = worldSpace.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 2]]);
                p1.X = Math.Round(p1.X, 0);
                p2.X = Math.Round(p2.X, 0);
                p3.X = Math.Round(p3.X, 0);
                p1.Y = Math.Round(p1.Y, 0);
                p2.Y = Math.Round(p2.Y, 0);
                p3.Y = Math.Round(p3.Y, 0);

                #region MinMax
                Double[] Xlist = new Double[]
                    {
                        p1.X,
                        p2.X,
                        p3.X
                    };
                Double minX = (Int32)(Math.Round(Xlist.Min(), 0));
                Double maxX = (Int32)(Math.Round(Xlist.Max(), 0));

                Double[] Ylist = new Double[]
                    {
                        p1.Y,
                        p2.Y,
                        p3.Y
                    };

                Double[] Zlist = new Double[]
                    {
                        p1.Z,
                        p2.Z,
                        p3.Z
                    };
                Double minY = (Int32)(Math.Round(Ylist.Min(), 0));
                Double maxY = (Int32)(Math.Round(Ylist.Max(), 0));

                //Don't shrink if we can't afford the space (2 pixels for border, 1 pixel for colour)
                if (maxY - minY > 3)
                {
                    //Shrink the triangle by 1 pixel high for better edges
                    //between models.
                    minY++;
                    p1.Y = Math.Max(p1.Y, minY);
                    p2.Y = Math.Max(p2.Y, minY);
                    p3.Y = Math.Max(p3.Y, minY);
                }
                //Don't shrink if we can't afford the space (2 pixels for border, 1 pixel for colour)
                if (maxX - minX > 3)
                {
                    //Shrink the triangle by 1 pixel wide for better edges
                    //between models.
                    maxX--;
                    p1.X = Math.Min(p1.X, maxX);
                    p2.X = Math.Min(p2.X, maxX);
                    p3.X = Math.Min(p3.X, maxX);
                }
                if (minY < 0) minY = 0;
                if (minX < 0) minX = 0;
                if (maxY > maxHeight) maxY = maxHeight;
                if (maxX > maxWidth) maxX = maxWidth;

                #endregion
                if (maxX - minX > 0 && maxY - minY > 0 &&
                    pos1.Z >= 0 && pos2.Z >= 0 && pos3.Z >= 0)
                {

                    Point t1 = mesh.TextureCoordinates[mesh.TriangleIndices[indicesStart]];
                    Point t2 = mesh.TextureCoordinates[mesh.TriangleIndices[indicesStart + 1]];
                    Point t3 = mesh.TextureCoordinates[mesh.TriangleIndices[indicesStart + 2]];

                    #region Basic Colouring
                    Double lightIntensity = 1;
                    //A very rough normalization of the colour.
                    Vector3D normal = new Vector3D(0, 1, 0);
                    if (mesh.Normals != null && mesh.Normals.Count > 0)
                    {
                        try
                        {
                            normal = mesh.Normals[mesh.TriangleIndices[indicesStart]];// temp1; // translateToScreen.Transform(temp1); //this is not really the normal
                            normal += mesh.Normals[mesh.TriangleIndices[indicesStart + 1]];
                            normal += mesh.Normals[mesh.TriangleIndices[indicesStart + 2]];
                            normal = worldSpace.Transform(normal);
                            normal.Normalize();
                        }
                        catch (Exception ex)
                        {
                            if (ex is IndexOutOfRangeException || ex is ArgumentOutOfRangeException)
                            {
                                normal = temp1;
                                normal.Normalize();
                            }
                            else throw ex;
                        }

                        PerspectiveCamera camera = (viewport.Camera as PerspectiveCamera);
                        Vector3D lightDirection = new Vector3D(0, 8, -9);
                        Double ambientIntensity = 0.4;
                        lightDirection.Normalize();
                        lightIntensity = Math.Min(Math.Max(
                            Vector3D.DotProduct(normal, lightDirection), 0) + ambientIntensity, 1);
                    }




                    Color finalColour = new Color();
                    finalColour.R = (Byte)(colour.R * lightIntensity);
                    finalColour.G = (Byte)(colour.G * lightIntensity);
                    finalColour.B = (Byte)(colour.B * lightIntensity);
                    #endregion

                    //p1.Y = (Int32)p1.Y;
                    //p2.Y = (Int32)p2.Y;
                    //p3.Y = (Int32)p3.Y;

                    Boolean line1 = false;
                    Boolean line2 = false;
                    Boolean line3 = false;

                    //one over the world space Z is used to make the
                    //texture coords interpolate in a linear way.
                    //Note: This is NOT the screen space Z values
                    Double oneOverZP1 = 1d / pos1.Z;
                    Double oneOverZP2 = 1d / pos2.Z;
                    Double oneOverZP3 = 1d / pos3.Z;

                    p1.Z = oneOverZP1;
                    p2.Z = oneOverZP2;
                    p3.Z = oneOverZP3;

                    //Create our values that will be interpolated
                    List<Tuple<Point3D, Point, Double>> orderedPoints = new List<Tuple<Point3D, Point, Double>>()
                    {
                        //Vertex,TextureCoord, linearDepth
                        new Tuple<Point3D, Point, Double>(p1, new Point((t1.X * texCoordCorrectionX) * oneOverZP1, (t1.Y * texCoordCorrectionY) * oneOverZP1), oneOverZP1),
                        new Tuple<Point3D, Point, Double>(p2, new Point((t2.X * texCoordCorrectionX) * oneOverZP2, (t2.Y * texCoordCorrectionY) * oneOverZP2), oneOverZP2),
                        new Tuple<Point3D, Point, Double>(p3, new Point((t3.X * texCoordCorrectionX) * oneOverZP3, (t3.Y * texCoordCorrectionY) * oneOverZP3), oneOverZP3)
                    };
                    //Order our points for rendering 
                    orderedPoints = orderedPoints.OrderBy(p => p.Item1.X).ThenBy(p => p.Item1.Y).ThenBy(p => p.Item3).ToList();

                    if (drawBorders)
                    {
                        line1 = CheckLineReference(lineReference, mesh, indicesStart, indicesStart + 1);
                        line2 = CheckLineReference(lineReference, mesh, indicesStart, indicesStart + 2);
                        line3 = CheckLineReference(lineReference, mesh, indicesStart + 2, indicesStart + 1);


                        //Get the distance between each point
                        Double distAC = p1.Distance(p3);
                        Double distAB = p1.Distance(p2);
                        Double distCB = p3.Distance(p2);

                        //Get the vector difference between each point
                        Vector3D diffAC = p3 - p1;
                        Vector3D diffAB = p2 - p1;
                        Vector3D diffCB = p2 - p3;

                        //set the drawing starting points
                        Point3D pointAC = p1;
                        Point3D pointAB = p1;
                        Point3D pointCB = p3;

                        //Draw triangle sides - (not greate for perspective, ok ish for Orthographic)
                        if (line1) DrawBorderLines(pointAB, diffAB, distAB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                        if (line2) DrawBorderLines(pointAC, diffAC, distAC, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                        if (line3) DrawBorderLines(pointCB, diffCB, distCB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);

                    }


                    Double xStart = Math.Round(orderedPoints[0].Item1.X, 0);
                    Double xEnd = Math.Round(orderedPoints[2].Item1.X, 0);
                    Double oneOverK1minusK0_1 = 1d / (orderedPoints[2].Item1.X - orderedPoints[0].Item1.X);
                    Double oneOverK1minusK0_2 = 1d / (orderedPoints[2].Item1.X - orderedPoints[1].Item1.X);
                    Double oneOverK1minusK0_3 = 1d / (orderedPoints[1].Item1.X - orderedPoints[0].Item1.X);
                    for (Double x = xStart; x <= xEnd; x++)
                    {

                        Double y1, y2, tex1x, tex2x, tex1y, tex2y, oneOverZ1, oneOverZ2;

                        if (x > orderedPoints[1].Item1.X)
                        {
                            y1 = Lerp(x, orderedPoints[1].Item1.X, orderedPoints[2].Item1.X, orderedPoints[1].Item1.Y, orderedPoints[2].Item1.Y, oneOverK1minusK0_2);
                            y2 = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item1.Y, orderedPoints[2].Item1.Y, oneOverK1minusK0_1);

                            oneOverZ1 = Lerp(x, orderedPoints[1].Item1.X, orderedPoints[2].Item1.X, orderedPoints[1].Item3, orderedPoints[2].Item3, oneOverK1minusK0_2);
                            oneOverZ2 = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item3, orderedPoints[2].Item3, oneOverK1minusK0_1);

                            tex1x = Lerp(x, orderedPoints[1].Item1.X, orderedPoints[2].Item1.X, orderedPoints[1].Item2.X, orderedPoints[2].Item2.X, oneOverK1minusK0_2);
                            tex2x = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item2.X, orderedPoints[2].Item2.X, oneOverK1minusK0_1);

                            tex1y = Lerp(x, orderedPoints[1].Item1.X, orderedPoints[2].Item1.X, orderedPoints[1].Item2.Y, orderedPoints[2].Item2.Y, oneOverK1minusK0_2);
                            tex2y = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item2.Y, orderedPoints[2].Item2.Y, oneOverK1minusK0_1);
                        }
                        else
                        {
                            y1 = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[1].Item1.X, orderedPoints[0].Item1.Y, orderedPoints[1].Item1.Y, oneOverK1minusK0_3);
                            y2 = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item1.Y, orderedPoints[2].Item1.Y, oneOverK1minusK0_1);

                            oneOverZ1 = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[1].Item1.X, orderedPoints[0].Item3, orderedPoints[1].Item3, oneOverK1minusK0_3);
                            oneOverZ2 = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item3, orderedPoints[2].Item3, oneOverK1minusK0_1);

                            tex1x = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[1].Item1.X, orderedPoints[0].Item2.X, orderedPoints[1].Item2.X, oneOverK1minusK0_3);
                            tex2x = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item2.X, orderedPoints[2].Item2.X, oneOverK1minusK0_1);

                            tex1y = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[1].Item1.X, orderedPoints[0].Item2.Y, orderedPoints[1].Item2.Y, oneOverK1minusK0_3);
                            tex2y = Lerp(x, orderedPoints[0].Item1.X, orderedPoints[2].Item1.X, orderedPoints[0].Item2.Y, orderedPoints[2].Item2.Y, oneOverK1minusK0_1);
                        }

                        Double yStart = y1;
                        Double yEnd = y2;
                        if (y1 > y2)
                        {
                            yStart = y2;
                            yEnd = y1;
                        }
                        yStart = Math.Round(yStart, 0);
                        yEnd = Math.Round(yEnd, 0);

                        Double increment = 1;// Math.Max((yEnd - yStart), 1);
                        for (Double y = yStart; y <= yEnd; y += increment)
                        {
                            if (y >= 0 && x >= 0 && y < maxHeight && x < maxWidth)
                            {
                                //Limit to rounded up maxX and maxY values due to fp rounding issues
                                Int32 Xoffset = (Int32)Math.Min(x, maxX);
                                Int32 Yoffset = (Int32)Math.Min(y, maxY);


                                //Calculate texture coords
                                Double oneOverZ = Lerp(y, y1, y2, oneOverZ1, oneOverZ2);
                                oneOverZ = Math.Round(oneOverZ, 14);
                                //check to see if the point is within the screen maximums.
                                if (Xoffset < maxWidth &&
                                    Yoffset < maxHeight &&
                                    (Yoffset * maxWidth + Xoffset) * 4 + 3 < pixelData.Length)
                                {
                                    //If the current point is closer to the screen than the depth buffers record
                                    //of hte closest distance to the screen for this pixel then replace the
                                    //pixel colour with the current colour and update the depth buffer.
                                    if (depthBuffer[(Yoffset * maxWidth + Xoffset)] > -oneOverZ) //add depth check
                                    {
                                        Int32 bufferIndex = (Yoffset * maxWidth + Xoffset);
                                        if (bmp != null)
                                        {
                                            Double depth = 1d / oneOverZ;
                                            Double tx = Math.Abs(Lerp(y, y1, y2, tex1x, tex2x) * depth);
                                            Double ty = Math.Abs(Lerp(y, y1, y2, tex1y, tex2y) * depth);
                                            Int32 XTexOffset = (Int32)Math.Min(tx * (bmp.PixelWidth - 1), bmp.PixelWidth - 1);
                                            Int32 YTexOffset = (Int32)Math.Min(ty * (bmp.PixelHeight - 1), bmp.PixelHeight - 1);
                                            Int32 tOffset = (XTexOffset + (YTexOffset * bmp.PixelWidth)) * 4 + 0;
                                            Byte blue = pixels[tOffset + 0];
                                            Byte green = pixels[tOffset + 1];
                                            Byte red = pixels[tOffset + 2];
                                            Byte alpha = pixels[tOffset + 3];
                                            if (alpha > 0)
                                            {
                                                // MergeColour((Yoffset * maxWidth + Xoffset) * 4, tOffset, ref pixelData, ref pixels, lightIntensity);

                                                //Make the colour lighter if there is less alpha
                                                //this helps text look nicer
                                                Double alphaBrightness = 255d / alpha;

                                                pixelData[bufferIndex * 4 + 0] = (Byte)(blue * Math.Max(lightIntensity, 0.0) * alphaBrightness);
                                                pixelData[bufferIndex * 4 + 1] = (Byte)(green * Math.Max(lightIntensity, 0.0) * alphaBrightness);
                                                pixelData[bufferIndex * 4 + 2] = (Byte)(red * Math.Max(lightIntensity, 0.0) * alphaBrightness);
                                                pixelData[bufferIndex * 4 + 3] = (Byte)255;
                                                depthBuffer[bufferIndex] = -oneOverZ;
                                            }
                                        }
                                        else
                                        {
                                            //we are using PixelFormats.Pbgra32 so add colors in the
                                            // order of Blue, Green, Red then Alpha
                                            pixelData[bufferIndex * 4 + 0] = finalColour.B;
                                            pixelData[bufferIndex * 4 + 1] = finalColour.G;
                                            pixelData[bufferIndex * 4 + 2] = finalColour.R;
                                            pixelData[bufferIndex * 4 + 3] = (Byte)255;

                                            depthBuffer[bufferIndex] = -oneOverZ;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Draws a trangle defined by points p1, p2 and p3 into the supplied pixelData array.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="maxHeight"></param>
        /// <param name="maxWidth"></param>
        /// <param name="colour"></param>
        /// <param name="pixelData"></param>
        /// <param name="depthBuffer"></param>
        /// <param name="estimateBorders"></param>
        private static void AddTriangleToBufferOld(MeshGeometry3D mesh, Matrix3D worldViewProjection, Int32 indicesStart, Int32 maxHeight, Int32 maxWidth, Color colour, ref byte[] pixelData, ref Double[] depthBuffer, Dictionary<UInt64, Boolean> lineReference, Boolean drawBorders)
        {
            Point3D p1 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart]]);
            Point3D p2 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 1]]);
            Point3D p3 = worldViewProjection.Transform(mesh.Positions[mesh.TriangleIndices[indicesStart + 2]]);
            Vector3D temp1 = RenderHelper.Point3DToVector3D(p1);
            Vector3D temp2 = RenderHelper.Point3DToVector3D(p2);
            Vector3D temp3 = RenderHelper.Point3DToVector3D(p3);
            Boolean IsBackFacing = Vector3D.CrossProduct(temp1 - temp2, temp3 - temp1).Z <= 0;
            //IsBackFacing = false;
            //Don't draw if the triangle is facing away from the camera
            if (!IsBackFacing)
            {
                #region MinMax
                Double[] Xlist = new Double[]
                    {
                        p1.X,
                        p2.X,
                        p3.X
                    };
                Int32 minX = (Int32)Xlist.Min();
                Int32 maxX = (Int32)(Xlist.Max());

                Double[] Ylist = new Double[]
                    {
                        p1.Y,
                        p2.Y,
                        p3.Y
                    };

                Double[] Zlist = new Double[]
                    {
                        p1.Z,
                        p2.Z,
                        p3.Z
                    };
                Int32 minY = (Int32)Ylist.Min();
                Int32 maxY = (Int32)(Ylist.Max());
                if (minY < 0) minY = 0;
                if (minX < 0) minX = 0;
                if (maxY > maxHeight) maxY = maxHeight;
                if (maxX > maxWidth) maxX = maxWidth;

                #endregion

                #region Basic Colouring

                //A very rough normalization of the colour.
                Vector3D normal = temp1; // translateToScreen.Transform(temp1); //this is not really the normal
                // normal = AbsNormalise(normal);
                normal.Normalize();
                Double lightIntensity = Math.Min(Math.Max(Vector3D.DotProduct(normal, new Vector3D(1, 1, 1)), 0), 1);
                //lightIntensity = 1;
                Color finalColour = new Color();
                finalColour.R = (Byte)(colour.R * lightIntensity);
                finalColour.G = (Byte)(colour.G * lightIntensity);
                finalColour.B = (Byte)(colour.B * lightIntensity);

                #endregion

                //Round up To pixel
                //p1.X += 1f - (p1.X - (Int32)p1.X);
                //p2.X += 1f - (p2.X - (Int32)p2.X);
                //p3.X += 1f - (p3.X - (Int32)p3.X);
                //p1.Y += 1f - (p1.Y - (Int32)p1.Y);
                //p2.Y += 1f - (p2.Y - (Int32)p2.Y);
                //p3.Y += 1f - (p3.Y - (Int32)p3.Y);

                //Round down To pixel
                p1.X = (Int32)p1.X;
                p2.X = (Int32)p2.X;
                p3.X = (Int32)p3.X;
                p1.Y = (Int32)p1.Y;
                p2.Y = (Int32)p2.Y;
                p3.Y = (Int32)p3.Y;

                //Get the distance between each point
                Double distAC = p1.Distance(p3);
                Double distAB = p1.Distance(p2);
                Double distCB = p3.Distance(p2);

                //Get the vector difference between each point
                Vector3D diffAC = p3 - p1;
                Vector3D diffAB = p2 - p1;
                Vector3D diffCB = p2 - p3;

                //get the max distance of the two side we will be itterating across
                Double distMax = Math.Max(distAC, distAB);

                //get the fraction of the difference moving the point along the first side                
                Double subOuter = 1f / distMax;

                //set the drawing starting points
                Point3D pointAC = p1;
                Point3D pointAB = p1;
                Point3D pointCB = p3;
                Point3D innerPoint = p1;

                distMax = Math.Max(distAC, distAB);
                subOuter = 1f / distMax;

                //move along one of the outer edges of the triangle drawing
                //lines untill it reaches the opposite triangle edge.
                for (Int32 outer = 0; outer < distMax; outer++)
                {
                    //Get the two points of the line we are going to draw pixel by pixel.
                    Point v1 = new Point(pointAB.X, pointAB.Y);
                    Point v2 = new Point(pointCB.X, pointCB.Y);

                    //get the length of the line
                    Double distInner = v1.Distance(v2);

                    //get the fraction of the distance to move by each itteration
                    Double subInner = (Double)(1d / distInner);

                    //Itterate down the length of the line to draw
                    for (Int32 inner = 0; inner <= distInner; inner++)
                    {
                        //Check to make sure the point is in the screen minimums
                        if (innerPoint.X >= 0 && innerPoint.Y >= 0)
                        {

                            //Limit to rounded down maxX and maxY values due to fp rounding issues
                            Int32 Xoffset = (Int32)Math.Min((Int32)innerPoint.X, (Int32)maxX);
                            Int32 Yoffset = (Int32)Math.Min((Int32)innerPoint.Y, (Int32)maxY);

                            try
                            {
                                //check to see if the point is within the screen maximums.
                                if (Xoffset < maxWidth &&
                                    Yoffset < maxHeight &&
                                    (Yoffset * maxWidth + Xoffset) * 4 + 3 < pixelData.Length)
                                {
                                    //If the current point is closer to the screen than the depth buffers record
                                    //of hte closest distance to the screen for this pixel then replace the
                                    //pixel colour with the current colour and update the depth buffer.
                                    if (depthBuffer[(Yoffset * maxWidth + Xoffset)] > innerPoint.Z) //add depth check
                                    {
                                        //we are using PixelFormats.Pbgra32 so add colors in the
                                        // order of Blue, Green, Red then Alpha
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 0] = finalColour.B;
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 1] = finalColour.G;
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 2] = finalColour.R;
                                        pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 3] = (Byte)255;
                                        depthBuffer[(Yoffset * maxWidth + Xoffset)] = innerPoint.Z;
                                    }
                                }
                            }
                            catch (Exception e)
                            {

                            }

                        }
                        //increment the line being drawn to the next point
                        innerPoint += (pointCB - pointAB) / distInner;

                    }

                    //Increment all the outer points
                    pointCB += diffCB / distMax;
                    pointAB += diffAB / distMax;
                    pointAC += diffAC / distMax;

                    //set the inner point to the next starting position
                    innerPoint = pointAB;
                }

                //Reset starting points
                pointAC = p1;
                pointAB = p1;
                pointCB = p3;

                if (drawBorders)
                {

                    Boolean line1 = CheckLineReference(lineReference, mesh, indicesStart, indicesStart + 1);
                    Boolean line2 = CheckLineReference(lineReference, mesh, indicesStart, indicesStart + 2);
                    Boolean line3 = CheckLineReference(lineReference, mesh, indicesStart + 2, indicesStart + 1);

                    //Draw triangle sides - (not greate for perspective, ok ish for Orthographic)
                    if (line1) DrawBorderLines(pointAB, diffAB, distAB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                    if (line2) DrawBorderLines(pointAC, diffAC, distAC, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                    if (line3) DrawBorderLines(pointCB, diffCB, distCB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);

                    //if (Math.Abs(distAB) < Math.Abs(distAC) || Math.Abs(distAB) < Math.Abs(distCB)) DrawBorderLines(pointAB, diffAB, distAB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                    //if (Math.Abs(distAC) < Math.Abs(distAB) || Math.Abs(distAC) < Math.Abs(distCB)) DrawBorderLines(pointAC, diffAC, distAC, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                    //if (Math.Abs(distCB) < Math.Abs(distAC) || Math.Abs(distCB) < Math.Abs(distAB)) DrawBorderLines(pointCB, diffCB, distCB, (Int32)maxX, (Int32)maxY, maxHeight, maxWidth, ref pixelData, ref depthBuffer);
                }
            }


        }

        /// <summary>
        /// Draws a 1 pixel width line along a triangle edge
        /// </summary>
        /// <param name="start"></param>
        /// <param name="diff"></param>
        /// <param name="dist"></param>
        /// <param name="maxX"></param>
        /// <param name="maxY"></param>
        /// <param name="maxHeight"></param>
        /// <param name="maxWidth"></param>
        /// <param name="pixelData"></param>
        /// <param name="depthBuffer"></param>
        /// <returns></returns>
        private static Boolean DrawBorderLines(Point3D start, Vector3D diff, Double dist, Int32 maxX, Int32 maxY, Int32 maxHeight, Int32 maxWidth, ref byte[] pixelData, ref Double[] depthBuffer)
        {
            Boolean output = false;
            Point3D point = start;
            Double subOuter = 1f / dist;
            //pointAB = _project1;
            //pointCB = _project3;
            for (Int32 outer = 0; outer <= dist; outer++)
            {
                if (point.X >= 0 && point.Y >= 0)
                {
                    //Limit to rounded down maxX and maxY values due to fp rounding issues
                    Int32 Xoffset = (Int32)Math.Round(point.X);//(Int32)Math.Min((Int32)point.X, maxX);
                    Int32 Yoffset = (Int32)Math.Round(point.Y);//(Int32)Math.Min((Int32)point.Y, maxY);

                    try
                    {
                        if (Xoffset < maxWidth &&
                            Yoffset < maxHeight &&
                            (Yoffset * maxWidth + Xoffset) * 4 + 3 < pixelData.Length)
                        {

                            if (depthBuffer[(Yoffset * maxWidth + Xoffset)] >= -(point.Z * 1.0001f)) //add depth check
                            {
                                //we are using PixelFormats.Pbgra32 so add colors in the
                                // order of Blue, Green, Red then Alpha
                                pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 0] = 0;
                                pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 1] = 0;
                                pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 2] = 0;
                                pixelData[(Yoffset * maxWidth + Xoffset) * 4 + 3] = (Byte)255;
                                depthBuffer[(Yoffset * maxWidth + Xoffset)] = -(point.Z * 1.0001f);
                            }
                        }
                    }
                    catch (Exception e)
                    {

                    }

                }

                point += diff * subOuter;
            }

            return output;
        }

        /// <summary>
        /// Finds edges that are only connected to one triangle.
        /// </summary>
        /// <param name="mesh">
        /// A mesh geometry.
        /// </param>
        /// <returns>
        /// The edge indices for the edges that are only used by one triangle.
        /// </returns>
        public static Dictionary<UInt64, Boolean> CreateLineReferences(MeshGeometry3D mesh)
        {
            Dictionary<UInt64, Boolean> dict = new Dictionary<UInt64, Boolean>();

            for (Int32 i = 0; i < mesh.TriangleIndices.Count / 3; i++)
            {
                Int32 i0 = i * 3;
                for (Int32 j = 0; j < 3; j++)
                {
                    Int32 index0 = mesh.TriangleIndices[i0 + j];
                    Int32 index1 = mesh.TriangleIndices[i0 + ((j + 1) % 3)];
                    Int32 minIndex = Math.Min(index0, index1);
                    Int32 maxIndex = Math.Max(index1, index0);
                    UInt64 key = CreateKey((UInt32)minIndex, (UInt32)maxIndex);
                    if (dict.ContainsKey(key))
                    {
                        dict[key] = false;
                    }
                    else
                    {
                        dict.Add(key, true);
                    }
                }
            }

            return dict;
        }

        /// <summary>
        /// Checks a dictionary of line references to see if a line should be drawn or not.
        /// </summary>
        /// <param name="lineReference"></param>
        /// <param name="mesh"></param>
        /// <param name="indices1"></param>
        /// <param name="indices2"></param>
        /// <returns></returns>
        public static Boolean CheckLineReference(Dictionary<UInt64, Boolean> lineReference, MeshGeometry3D mesh, Int32 indices1, Int32 indices2)
        {
            Boolean output = false;
            UInt32 index0 = (UInt32)mesh.TriangleIndices[indices1];
            UInt32 index1 = (UInt32)mesh.TriangleIndices[indices2];
            UInt32 minIndex = Math.Min(index0, index1);
            UInt32 maxIndex = Math.Max(index1, index0);


            lineReference.TryGetValue(CreateKey(minIndex, maxIndex), out output);

            return output;
        }

        /// <summary>
        /// Create a 64-bit key from two 32-bit indices
        /// </summary>
        /// <param name="i0">
        /// The i 0.
        /// </param>
        /// <param name="i1">
        /// The i 1.
        /// </param>
        /// <returns>
        /// The create key.
        /// </returns>
        private static UInt64 CreateKey(uint i0, uint i1)
        {
            return ((UInt64)i0 << 32) + i1;
        }

        /// <summary>
        /// Render the supplied viewport to a bitmap source
        /// </summary>
        /// <param name="viewer"></param>
        /// <param name="sizeMultiplier">multiplies the render area size 
        /// (a value of 2 or higher is recomended if the objects have textures)</param>
        /// <param name="textureSize">Size to render textures at. (512 seems to be the best balance, larger textures
        /// only make a noticable difference if the size multiplier goes above 2)</param>
        /// <returns></returns>
        public static BitmapSource Render(Viewport3D viewport, Camera camera, Double sizeMultiplier = 2, Int32 textureSize = 512)
        {
            Matrix3D viewMatrix = Matrix3D.Identity;
            Matrix3D projectionMatrix = Matrix3D.Identity;
            Boolean drawBorders = false;
            Int32 dpi = 96;
            //get camera view matrix (pos and direction of camera)
            viewMatrix = RenderHelper.GetViewMatrix(camera);

            if (camera is PerspectiveCamera)
            {
                PerspectiveCamera perspectiveCamera = camera as PerspectiveCamera;

                //get screen space projection - perspective
                projectionMatrix = RenderHelper.CreatePerspectiveMatrix(perspectiveCamera, viewport.ActualWidth / viewport.ActualHeight);
            }
            else if (camera is OrthographicCamera)
            {
                //drawBorders = true;
                OrthographicCamera orthographicCamera = camera as OrthographicCamera;

                //get screen space projection - orthographic
                projectionMatrix = RenderHelper.CreateOrthographicMatrix(orthographicCamera, viewport.ActualWidth / viewport.ActualHeight);
            }

            Int32 width = (Int32)(viewport.ActualWidth * sizeMultiplier);
            Int32 height = (Int32)(viewport.ActualHeight * sizeMultiplier);

            //Create screen transforms
            TranslateTransform3D translateToScreen = new TranslateTransform3D(1, 1, 0);

            Double scaleWidth = (viewport.ActualWidth / 2) * sizeMultiplier;
            Double scaleHeight = (viewport.ActualHeight / 2) * sizeMultiplier;
            ScaleTransform3D scaleToScreen = new ScaleTransform3D(scaleWidth, scaleHeight, 1);

            //The pixel format use has 4 bytes per point (Pbgra32)
            Byte[] pixelData = new Byte[width * height * 4];
            Double[] depthBuffer = new Double[width * height];

            //first set all pixels to white to avoid black transparency.
            for (Int32 i = 0; i < (width * height * 4); i++) pixelData[i] = 255;
            

            //initialise depth buffer (larger numbers are further away)
            for (int x = 0; x < depthBuffer.Length; x++)
            {
                depthBuffer[x] = Double.MaxValue;
            }


            // get all geometry from the viewport
            List<GeometryContext> geometryList = GetGeometry(viewport);

            // enumerate through the geometry
            foreach (GeometryContext geometry in geometryList)
            {
                // get the mesh geometry
                MeshGeometry3D meshGeometry = geometry.Geometry.Geometry as MeshGeometry3D;
                if (meshGeometry == null) continue;

                //not drawing software borders anymore.
                Dictionary<UInt64, Boolean> lineReferences =
                    (drawBorders)? CreateLineReferences(meshGeometry) : new Dictionary<UInt64, Boolean>();

                //create the worldViewProjection matrix
                //world = 3D space, i.e. the model transformed to it's position
                //view = the camera view position and direction
                //projection = how the points are converted into screen space (transformed values will range from -1 to 1).
                //Matrices must be multiplied in this order to get the right result. Multiplying the
                //view and projection matrices befor hand will result in an incorrect matrix.
                Matrix3D worldViewProjection = geometry.Transform * viewMatrix * projectionMatrix;
                Matrix3D worldSpace = geometry.Transform * viewMatrix;

                //Finally add in the screen translations to place points in the drawable area
                worldViewProjection *= translateToScreen.Value * scaleToScreen.Value;

                #region Process the geometry material
                Color colour = Colors.White;
                RenderTargetBitmap bmp = new RenderTargetBitmap(textureSize, textureSize, dpi, dpi, PixelFormats.Pbgra32);
                //RenderOptions.SetBitmapScalingMode(bmp, BitmapScalingMode.HighQuality);
                //RenderOptions.SetEdgeMode(bmp, EdgeMode.Unspecified);

                Byte[] pixels = null;
                Double texCoordCorrectionY = 1;
                Double texCoordCorrectionX = 1;

                //Extract out all diffuse materials.
                List<DiffuseMaterial> materials = new List<DiffuseMaterial>();
                if (geometry.Material is DiffuseMaterial)
                {
                    materials.Add((DiffuseMaterial)geometry.Material);
                }
                else if (geometry.Material is MaterialGroup)
                {
                    foreach (Material m in ((MaterialGroup)geometry.Material).Children)
                    {
                        DiffuseMaterial diffuseM = m as DiffuseMaterial;
                        if (diffuseM != null) materials.Add(diffuseM);
                    }
                }
                else
                {
                    pixels = new Byte[0];
                }



                if (materials.Any())
                {
                    foreach (DiffuseMaterial difMaterial in materials)
                    {
                        //DiffuseMaterial difMaterial = geometry.Geometry.Material as DiffuseMaterial;
                        colour = difMaterial.Color;

                        DrawingVisual drawingVisual = new DrawingVisual();
                        DrawingContext drawingContext = drawingVisual.RenderOpen();
                        RenderOptions.SetClearTypeHint(bmp, ClearTypeHint.Auto);
                        RenderOptions.SetClearTypeHint(drawingVisual, ClearTypeHint.Auto);
                        drawingContext.DrawRectangle(difMaterial.Brush, null, new Rect(0, 0, textureSize, textureSize));
                        drawingContext.Close();
                        bmp.Clear();
                        bmp.Render(drawingVisual);
                        int bitsperpixel = bmp.Format.BitsPerPixel;
                        int stride = bmp.PixelWidth * bitsperpixel / 8;
                        pixels = new byte[bmp.PixelHeight * stride];
                        bmp.CopyPixels(pixels, stride, 0);

                        //texture coordinate correction values to ensure all coords are within 0-1 range;
                        //Note: this assumes that the whole of the texture is used.
                        texCoordCorrectionY = 1d / meshGeometry.TextureCoordinates.Max(t => t.Y);
                        texCoordCorrectionX = 1d / meshGeometry.TextureCoordinates.Max(t => t.X);
                    }
                }
                #endregion

                if (pixels.Any())
                {
                    //Itterate through all the triangles defined by the indices. Each triangle
                    //is created from 3 indecies which reference a position index.
                    for (Int32 i = 0; i < meshGeometry.TriangleIndices.Count; i += 3)
                    {
                        AddTriangleToBuffer(meshGeometry, ref worldViewProjection, ref i,
                            ref height, ref width, ref colour, ref pixelData, ref depthBuffer,
                            lineReferences, drawBorders, bmp, ref pixels, ref worldSpace, viewport, texCoordCorrectionX, texCoordCorrectionY);
                    }
                }


            }

            //Output the resultant bitmap source
            return BitmapSource.Create(
                width, height,
                dpi, dpi,
                PixelFormats.Pbgra32,
                null, pixelData, width * 4);
        }

        /// <summary>
        /// Get all the geometries from the viewport to draw
        /// </summary>
        /// <param name="viewport"></param>
        /// <returns></returns>
        public static List<GeometryContext> GetGeometry(Viewport3D viewport)
        {
            List<GeometryContext> geometry = new List<GeometryContext>();
            foreach (Visual3D visual in viewport.Children)
            {
                ModelVisual3D modelVisual = visual as ModelVisual3D;
                if (modelVisual != null)
                {
                    GetGeometry(geometry, modelVisual, new Matrix3D[0]);
                }
            }
            return geometry;
        }

        /// <summary>
        /// Get the geometries from the supplied model visual and add them to the list
        /// </summary>
        /// <param name="geometryList"></param>
        /// <param name="modelVisual"></param>
        /// <param name="transform">Previous levels transform array</param>
        private static void GetGeometry(List<GeometryContext> geometryList, ModelVisual3D modelVisual, Matrix3D[] transform)
        {
            Boolean geometryVisible = true;

            //ModelPart3Ds are a special case that can be hidden 
            //Note: If other models start having this property an interface should be created
            //if (modelVisual is ModelConstructPart3D)
            //{
            //    geometryVisible = (modelVisual as ModelConstructPart3D).ModelPartData.IsVisible;
            //}


            if (geometryVisible)
            {
                //Create transform matrix array for this level
                Matrix3D[] visualMatrixArray = new Matrix3D[transform.Length + 1];
                transform.CopyTo(visualMatrixArray, 0);
                visualMatrixArray[visualMatrixArray.Length - 1] = modelVisual.Transform.Value;

                // get the geometry from this model visual
                Model3DGroup modelGroup = modelVisual.Content as Model3DGroup;

                if (modelGroup != null)
                {
                    //The supplied geometry transform must include all preceding transforms related to the geometry
                    GetGeometry(geometryList, modelGroup, visualMatrixArray);
                }

                // recurse through all model visual children
                foreach (Visual3D visual in modelVisual.Children)
                {
                    ModelVisual3D modelVisualChild = visual as ModelVisual3D;
                    if (modelVisualChild != null)
                    {
                        //The supplied geometry transform must include all preceding transforms related to the geometry
                        GetGeometry(geometryList, modelVisualChild, visualMatrixArray);
                    }
                }
            }

        }

        /// <summary>
        /// Get the geometries from the supplied model group and add them to the list
        /// </summary>
        /// <param name="geometryList"></param>
        /// <param name="modelVisual"></param>
        /// <param name="transform">Previous levels transform array</param>
        private static void GetGeometry(List<GeometryContext> geometryList, Model3DGroup modelGroup, Matrix3D[] transform)
        {
            //Create transform matrix array for this level
            Matrix3D[] groupMatrixArray = new Matrix3D[transform.Length + 1];
            transform.CopyTo(groupMatrixArray, 0);
            groupMatrixArray[groupMatrixArray.Length - 1] = modelGroup.Transform.Value;

            foreach (Model3D model in modelGroup.Children)
            {
                //The supplied geometry transform must include all preceding transforms related to the geometry
                GetGeometry(geometryList, model, groupMatrixArray);
            }
        }

        /// <summary>
        /// Get the geometries from the supplied model and add them to the list
        /// </summary>
        /// <param name="geometryList"></param>
        /// <param name="modelVisual"></param>
        /// <param name="transform">Previous levels transform array</param>
        private static void GetGeometry(List<GeometryContext> geometryList, Model3D model, Matrix3D[] transform)
        {


            Model3DGroup modelGroup = model as Model3DGroup;
            if (modelGroup != null)
            {
                //The supplied geometry transform must include all preceding transforms related to the geometry
                GetGeometry(geometryList, modelGroup, transform);
            }
            else
            {
                //Create transform matrix array for this level
                Matrix3D[] modelMatrixArray = new Matrix3D[transform.Length + 1];
                transform.CopyTo(modelMatrixArray, 0);
                modelMatrixArray[modelMatrixArray.Length - 1] = model.Transform.Value;

                Matrix3D finalMatrix = Matrix3D.Identity;

                //Transforms have to be multiplied in the correct order starting
                //from the child object up through all its parents
                for (Int32 t = modelMatrixArray.Length - 1; t >= 0; t--)
                {
                    finalMatrix *= modelMatrixArray[t];
                }

                GeometryModel3D geometry = model as GeometryModel3D;
                if (geometry != null)
                {
                    MeshGeometry3D mesh = geometry.Geometry as MeshGeometry3D;
                    if (mesh == null) return;
                    if (mesh.TextureCoordinates.Count == 0) return;

                    //Add the geometry with the final combind transform matrix.
                    geometryList.Add(new GeometryContext(geometry, finalMatrix));
                }
            }
        }
    }

    /// <summary>
    /// Context to hold geometry information as well as the 
    /// overall transform required to draw the geometry in 
    /// the correct place
    /// </summary>
    internal struct GeometryContext
    {
        private GeometryModel3D _geometry;
        private Matrix3D _transform;

        /// <summary>
        /// Model Geometry
        /// </summary>
        public GeometryModel3D Geometry
        {
            get { return _geometry; }
        }

        /// <summary>
        /// Total transform of the model
        /// </summary>
        public Matrix3D Transform
        {
            get { return _transform; }
        }

        /// <summary>
        /// Geometry material
        /// </summary>
        public Material Material
        {
            get { return Geometry.Material; }
        }

        public GeometryContext(GeometryModel3D geometry, Matrix3D transform)
        {
            _geometry = geometry;
            _transform = transform;
        }



    }


    internal class RenderHelper
    {
        internal static Vector3D Point3DToVector3D(Point3D point)
        {
            return new Vector3D(point.X, point.Y, point.Z);
        }

        /// <summary>
        /// Creates the view matrix for a projection camera (e.g Perpective or Orthographic)
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        internal static Matrix3D GetViewMatrix(ProjectionCamera camera)
        {
            //ensure look direction is transformed correctly before use
            //this normally takes effect when the camera has been rotated
            Vector3D zAxis = camera.Transform.Transform(camera.LookDirection);
            zAxis.Normalize();

            //Transform up direction so we don't get a sudden flipping
            Vector3D xAxis = Vector3D.CrossProduct(camera.Transform.Transform(camera.UpDirection), zAxis);

            xAxis.Normalize();

            Vector3D yAxis = Vector3D.CrossProduct(zAxis, xAxis);

            //ensure look direction is transformed correctly before use
            //this normally takes effect when the camera has been rotated
            Vector3D position = (Vector3D)camera.Transform.Transform(camera.Position);
            double offsetX = -Vector3D.DotProduct(xAxis, position);
            double offsetY = -Vector3D.DotProduct(yAxis, position);
            double offsetZ = -Vector3D.DotProduct(zAxis, position);

            return new Matrix3D(
                xAxis.X, yAxis.X, zAxis.X, 0,
                xAxis.Y, yAxis.Y, zAxis.Y, 0,
                xAxis.Z, yAxis.Z, zAxis.Z, 0,
                offsetX, offsetY, offsetZ, 1);
        }

        /// <summary>
        /// Creates the effective view matrix for the given
        /// camera.
        /// </summary>
        internal static Matrix3D GetViewMatrix(Camera camera)
        {
            if (camera == null)
            {
                throw new ArgumentNullException("camera");
            }

            ProjectionCamera projectionCamera = camera as ProjectionCamera;
            if (projectionCamera != null)
            {
                //Perpective or Orthographic cameras use this
                return GetViewMatrix(projectionCamera);
            }

            MatrixCamera matrixCamera = camera as MatrixCamera;

            if (matrixCamera != null)
            {
                return matrixCamera.ViewMatrix;
            }

            throw new ArgumentException(String.Format("Unsupported camera type '{0}'.", camera.GetType().FullName), "camera");
        }

        /// <summary>
        /// Creates the projection matrix for a perspective camera
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="aspectRatio"></param>
        /// <returns></returns>
        internal static Matrix3D CreatePerspectiveMatrix(PerspectiveCamera camera, Double aspectRatio)
        {
            double hFoV = DegreesToRadians(camera.FieldOfView);
            double zn = camera.NearPlaneDistance;
            double zf = camera.FarPlaneDistance;

            double xScale = 1 / Math.Tan(hFoV / 2);
            double yScale = aspectRatio * xScale;
            double m33 = (zf == double.PositiveInfinity) ? 1 : (zf / (zf - zn));
            double m43 = zn * m33;

            return new Matrix3D(
                xScale, 0, 0, 0,
                     0, yScale, 0, 0,
                     0, 0, m33, -1,
                     0, 0, m43, 0);

        }

        /// <summary>
        /// Creates the projection matrix for an orthographic camera
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="aspectRatio"></param>
        /// <returns></returns>
        internal static Matrix3D CreateOrthographicMatrix(OrthographicCamera camera, Double aspectRatio)
        {
            double w = camera.Width;
            double h = w / aspectRatio;
            double zn = camera.NearPlaneDistance;
            double zf = camera.FarPlaneDistance;

            double m33 = 1 / (zf - zn);
            double m43 = zn * m33;

            return new Matrix3D(
                -2 / w, 0, 0, 0,
                  0, -2 / h, 0, 0,
                  0, 0, m33, 0,
                  0, 0, m43, 1);

        }

        /// <summary>
        /// Converts the given degree value to radians.
        /// </summary>
        private static Double DegreesToRadians(Double degrees)
        {
            return degrees * (Math.PI / 180f);
        }

    }

    #endregion
}
