﻿// Copyright © Galleria RTS Ltd 2016
using System;
using System.IO;
using System.Windows.Media.Imaging;


namespace Galleria.Framework.Planograms.Controls.Wpf.Helpers
{
    /// <summary>
    /// Contains helper methods for manipulating images.
    /// </summary>
    public static class ImageHelper
    {

        public static BitmapImage GetBitmapImage(Byte[] imageBlob)
        {
            BitmapImage img = null;
            if (imageBlob != null)
            {
                using (MemoryStream mem = new MemoryStream(imageBlob))
                {
                    img = GetBitmapImage(mem);
                }
            }

            return img;
        }

        /// <summary>
        /// Gets a bitmap image from a memory stream
        /// </summary>
        /// <param name="dataStream">memory stream</param>
        /// <returns>BitmapImage</returns>
        public static BitmapImage GetBitmapImage(MemoryStream dataStream)
        {
            BitmapImage img = new BitmapImage();
            dataStream.Position = 0;
            img.BeginInit();
            img.StreamSource = new MemoryStream(dataStream.ToArray());
            img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            img.CacheOption = BitmapCacheOption.Default;
            img.EndInit();
            img.Freeze();
            return img;
        }
    }
}
