﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Rendering;
using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Framework.Planograms.Controls.Wpf.Helpers
{
    /// <summary>
    /// Contains helpers for dealing with the Viewport3D camera
    /// </summary>
    public static class PlanCameraHelper
    {
        /// <summary>
        /// Applies settings to the given viewport based on the camera type.
        /// </summary>
        /// <param name="mainViewport"></param>
        /// <param name="viewType"></param>
        [CLSCompliant(false)]
        public static void SetupForCameraType(HelixViewport3D mainViewport, CameraViewType viewType,
            Boolean allowRotation = true, Boolean showViewCube = true)
        {
            //Perspective Camera
            if (viewType == CameraViewType.Perspective)
            {
                mainViewport.IsRotationEnabled = allowRotation;
                mainViewport.ShowViewCube = showViewCube;

                mainViewport.Camera =
                    new PerspectiveCamera()
                    {
                        UpDirection = new Vector3D(0, 1, 0),
                        LookDirection = new Vector3D(0, 0, -244),
                        FieldOfView = 60
                    };

            }

            //Orthographic Camera
            else
            {
                //Reduce the rendering edge mode for performance.
                RenderOptions.SetEdgeMode(mainViewport, EdgeMode.Aliased);

                mainViewport.IsRotationEnabled = false;
                mainViewport.ShowViewCube = false;

                Vector3D cameraUp = new Vector3D(0, 1, 0);
                Vector3D cameraLook = new Vector3D(0, 0, -244);

                switch (viewType)
                {
                    case CameraViewType.Back:
                        cameraLook = new Vector3D(0, 0, 244);
                        break;
                    case CameraViewType.Top:
                        cameraLook = new Vector3D(0, -244, 0);
                        cameraUp = new Vector3D(0, 0, -1);
                        break;
                    case CameraViewType.Bottom:
                        cameraLook = new Vector3D(0, 244, 0);
                        cameraUp = new Vector3D(0, 0, 1);
                        break;
                    case CameraViewType.Left:
                        cameraLook = new Vector3D(244, 0, 0);
                        break;
                    case CameraViewType.Right:
                        cameraLook = new Vector3D(-244, 0, 0);
                        break;
                }

                mainViewport.Camera =
                    new OrthographicCamera()
                    {
                        UpDirection = cameraUp,
                        LookDirection = cameraLook,
                        // Setting the NearPlaneDistance to a large value is a bit of a hack. See V8-31722
                        // and V8-30634 for more information. There seems to be an issue with orthographic
                        // cameras clipping the Z direction. On normal sized plans, this is only evident if you
                        // zoom very far in (30634), but on plans that are very deep (31722) this can result
                        // in the front of the plan (i.e. the backboard and shelves) not being rendered. 
                        // Setting this value to Double.NegativeInfinity or Double.MinValue correct this issue
                        // but cause subsequent problems for panning and the calculation of mouse co-ordinates.
                        // If NearPlaneDistance is set to the order of magnitude above the plan's depth, then
                        // the plan appears to be rendered correctly and panning, etc. works ok. Given the 
                        // size of plans we expect in the software, -10000 seems like a reasonable value - 
                        // this would be equivalant to a position with 100 facings deep (the maximum) with each
                        // product 100 units deep.
                        NearPlaneDistance = -10000,
                    };

            }

        }

        /// <summary>
        /// Zooms the given viewport to fit the model
        /// </summary>
        [CLSCompliant(false)]
        public static void ZoomToFit(HelixViewport3D helixViewport, ModelConstruct3D modelVisual)
        {
            if (helixViewport != null && helixViewport.CameraController != null)
            {
                Viewport3D viewport = helixViewport.Viewport;

                //Perspective
                if (viewport.Camera is PerspectiveCamera)
                {
                    helixViewport.ZoomExtents();

                    //get the model bounds
                    if (modelVisual == null) return;

                    Rect bounds = Rect.Empty;
                    bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual, Rect.Empty, false));
                    if (bounds.IsEmpty || bounds == null) return;

                    //keep zooming in until we go outside the viewport
                    while (bounds.X > 0 && bounds.Y > 0
                        && (bounds.X + bounds.Width) <= viewport.ActualWidth
                        && (bounds.Y + bounds.Height) <= viewport.ActualHeight)
                    {
                        ZoomIn(helixViewport);

                        //reget the bounds
                        bounds = Rect.Empty;
                        bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual, Rect.Empty, false));
                        if (bounds.IsEmpty || bounds == null) return;
                    }

                    //zoom out once again
                    ZoomOut(helixViewport);
                }

                // Orthographic
                else if (viewport.Camera is OrthographicCamera)
                {
                    //zoom to fit all first
                    helixViewport.ZoomExtents();


                    //In order to perform the zoom by rectangle we must be able to invert the camera
                    // Sometimes however the model is so big that when fully zoomed to extent this is not possible.
                    // If this is the case then try zooming in a bit.
                    Matrix3D matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                    if (!matrixCamera.HasInverse)
                    {
                        Int32 tryCount = 0;
                        while (!matrixCamera.HasInverse && tryCount < 15)
                        {
                            ZoomIn(helixViewport);
                            matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                            tryCount++;
                        }

                    }

                    //get the bounds of the full model
                    Rect bounds = Rect.Empty;
                    if (modelVisual != null)
                    {
                        bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual, Rect.Empty, false));
                    }
                    if (bounds.IsEmpty || Double.IsNaN(bounds.Height))
                    {
                        return;
                    }

                    //zoom
                    ZoomToFitBounds(bounds, viewport, helixViewport.Camera);
                }

            }

        }

        /// <summary>
        /// Zooms the given viewport to fit the model
        /// </summary>
        [CLSCompliant(false)]
        public static void ZoomToFit(HelixViewport3D helixViewport, Rect3D bounds3D)
        {
            if (helixViewport != null && helixViewport.CameraController != null)
            {
                Viewport3D viewport = helixViewport.Viewport;

                //Perspective
                if (viewport.Camera is PerspectiveCamera)
                {
                    helixViewport.ZoomExtents();
                }

                // Orthographic
                else if (viewport.Camera is OrthographicCamera)
                {
                    //zoom to fit all first
                    helixViewport.ZoomExtents();


                    //In order to perform the zoom by rectangle we must be able to invert the camera
                    // Sometimes however the model is so big that when fully zoomed to extent this is not possible.
                    // If this is the case then try zooming in a bit.
                    Matrix3D matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                    if (!matrixCamera.HasInverse)
                    {
                        Int32 tryCount = 0;
                        while (!matrixCamera.HasInverse && tryCount < 15)
                        {
                            ZoomIn(helixViewport);
                            matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                            tryCount++;
                        }

                    }

                    //get the bounds of the full model
                    Rect bounds = Rect.Empty;
                    if (!bounds3D.IsEmpty)
                    {
                        bounds.Union(ModelConstruct3DHelper.Get2DBounds(viewport, bounds3D, Rect.Empty));
                    }
                    if (bounds.IsEmpty || Double.IsNaN(bounds.Height))
                    {
                        return;
                    }

                    //zoom
                    ZoomToFitBounds(bounds, viewport, helixViewport.Camera);
                }

            }

        }

        /// <summary>
        /// Zooms to fit the model by height.
        /// </summary>
        [CLSCompliant(false)]
        public static void ZoomToFitHeight(HelixViewport3D helixViewport, ModelConstruct3D modelVisual, Boolean doInflate = true)
        {
            if (helixViewport != null && helixViewport.CameraController != null)
            {
                Viewport3D viewport = helixViewport.Viewport;

                #region Perspective
                if (viewport.Camera is PerspectiveCamera)
                {

                    helixViewport.ZoomExtents();


                    //get the model bounds
                    if (modelVisual == null) return;

                    Rect bounds = Rect.Empty;
                    bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual, Rect.Empty, false));
                    if (bounds.IsEmpty || bounds == null) return;

                    //keep zooming in until we go outside the viewport
                    while (bounds.X > 0 && bounds.Y > 0
                        && (bounds.X + bounds.Width) <= viewport.ActualWidth
                        && (bounds.Y + bounds.Height) <= viewport.ActualHeight)
                    {
                        ZoomIn(helixViewport);

                        //reget the bounds
                        bounds = Rect.Empty;
                        bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual, Rect.Empty, false));
                        if (bounds.IsEmpty || bounds == null) return;
                    }

                    //zoom out once again
                    ZoomOut(helixViewport);

                }
                #endregion

                #region Orthographic
                else if (viewport.Camera is OrthographicCamera)
                {
                    //zoom to fit all first
                    helixViewport.ZoomExtents();


                    //In order to perform the zoom by rectangle we must be able to invert the camera
                    // Sometimes however the model is so big that when fully zoomed to extent this is not possible.
                    // If this is the case then try zooming in a bit.
                    Matrix3D matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                    if (!matrixCamera.HasInverse)
                    {
                        Int32 tryCount = 0;
                        while (!matrixCamera.HasInverse && tryCount < 15)
                        {
                            ZoomIn(helixViewport);
                            matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                            tryCount++;
                        }

                    }

                    //get the bounds of the full model
                    Rect bounds = Rect.Empty;
                    if (modelVisual != null)
                    {
                        bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual));
                    }

                    if (bounds.IsEmpty || Double.IsNaN(bounds.Height))
                    {
                        return;
                    }



                    Double viewportXYRatio = viewport.ActualWidth / viewport.ActualHeight;

                    //if the height of the model is smaller than the width
                    // then adjust the width to the viewport ratio as we should always fit to height.
                    if (bounds.Height < bounds.Width)
                    {
                        bounds.Width = bounds.Height * viewportXYRatio;
                    }
                    //TODO -> What if height is smaller than width but when fitted width would be smaller then actualviewportwidth
                    // model would be to left...


                    //zoom
                    ZoomToFitBounds(bounds, viewport, helixViewport.Camera, doInflate);
                }
                #endregion

            }
        }

        /// <summary>
        /// Zooms to fit the model by width
        /// </summary>
        [CLSCompliant(false)]
        public static void ZoomToFitWidth(HelixViewport3D helixViewport, ModelConstruct3D modelVisual)
        {
            if (helixViewport != null && helixViewport.CameraController != null)
            {
                Viewport3D viewport = helixViewport.Viewport;

                #region Perspective
                if (viewport.Camera is PerspectiveCamera)
                {

                    ////reset the camera
                    //helixViewport.ResetCamera();

                    //PerspectiveCamera camera = (PerspectiveCamera)this.xViewer.Camera;
                    //camera.UpDirection = new Vector3D(0, 1, 0);
                    //camera.LookDirection = new Vector3D(0, 0, -244);
                    //camera.FieldOfView = 60;


                    helixViewport.ZoomExtents();


                    //while the bounds are not quite right, zoom in
                    //if (_modelVisual != null)
                    //{
                    //    Rect bounds = Model3DHelper.GetModelBounds(this.xViewer.Viewport, _modelVisual, Rect.Empty);
                    //    if (!bounds.IsEmpty)
                    //    {
                    //        while ((bounds.Width < (viewport.ActualWidth - (viewport.ActualWidth * 0.15)))
                    //        && (bounds.Height < (viewport.ActualHeight - (viewport.ActualHeight * 0.15))))
                    //        {
                    //            ZoomIn();
                    //            bounds = Model3DHelper.GetModelBounds(this.xViewer.Viewport, _modelVisual, Rect.Empty);
                    //        }
                    //    }

                    //}
                }
                #endregion

                #region Orthographic
                else if (viewport.Camera is OrthographicCamera)
                {
                    //zoom to fit all first
                    helixViewport.ZoomExtents();

                    //In order to perform the zoom by rectangle we must be able to invert the camera
                    // Sometimes however the model is so big that when fully zoomed to extent this is not possible.
                    // If this is the case then try zooming in a bit.
                    Matrix3D matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                    if (!matrixCamera.HasInverse)
                    {
                        Int32 tryCount = 0;
                        while (!matrixCamera.HasInverse && tryCount < 15)
                        {
                            ZoomIn(helixViewport);
                            matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                            tryCount++;
                        }

                    }

                    //get the bounds of the full model
                    Rect bounds = Rect.Empty;
                    if (modelVisual != null)
                    {
                        bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual));
                    }

                    if (bounds.IsEmpty || Double.IsNaN(bounds.Height))
                    {
                        return;
                    }

                    Double viewportXYRatio = viewport.ActualWidth / viewport.ActualHeight;

                    //if the width of the model is smaller than the width
                    // then adjust the height to the viewport ratio as we should always fit to width.
                    if (bounds.Width < bounds.Height)
                    {
                        bounds.Height = bounds.Width * viewportXYRatio;
                    }
                    //TODO -> What if width is smaller than height but when fitted width would be smaller then actualviewportwidth
                    // model would be to left...


                    //zoom
                    ZoomToFitBounds(bounds, viewport, helixViewport.Camera);
                }
                #endregion

            }
        }

        /// <summary>
        /// Zooms in
        /// </summary>
        [CLSCompliant(false)]
        public static void ZoomIn(HelixViewport3D helixViewport)
        {
            if (helixViewport != null)
            {
                helixViewport.CameraController.Zoom(-0.1);
            }
        }

        /// <summary>
        /// Zooms Out
        /// </summary>
        [CLSCompliant(false)]
        public static void ZoomOut(HelixViewport3D helixViewport)
        {
            if (helixViewport != null)
            {
                helixViewport.CameraController.Zoom(0.1);
            }
        }

        /// <summary>
        /// Zooms to fit the given bounds,
        /// adjusting for viewport ratio as necessary.
        /// </summary>
        /// <param name="bounds"></param>
        public static void ZoomToFitBounds(Rect bounds, Viewport3D viewport, ProjectionCamera camera, Boolean doInflate = true)
        {
            if (!bounds.IsEmpty)
            {
                if (doInflate)
                {
                    bounds.Inflate(5, 5);
                }   
                                                           
                //adjust the bounds to match the viewport ratio.
                Double viewportXYRatio = viewport.ActualWidth / viewport.ActualHeight;
                Double ratioWidth = bounds.Height * viewportXYRatio;
                Double ratioHeight = bounds.Width / viewportXYRatio;
                if (bounds.Width < ratioWidth)
                {
                    bounds.X -= ((ratioWidth - bounds.Width) / 2);
                    bounds.Width = ratioWidth;
                }
                else if (bounds.Height < ratioHeight)
                {
                    bounds.Y -= ((ratioHeight - bounds.Height) / 2);
                    bounds.Height = ratioHeight;
                }

                //zoom it.
                ZoomToRectangle(camera, viewport, bounds);
            }
        }

        /// <summary>
        /// Zooms the camera to the specified rectangle.
        /// </summary>
        /// <param name="camera">
        /// The camera.
        /// </param>
        /// <param name="viewport">
        /// The viewport.
        /// </param>
        /// <param name="zoomRectangle">
        /// The zoom rectangle.
        /// </param>
        public static void ZoomToRectangle(ProjectionCamera camera, Viewport3D viewport, Rect zoomRectangle)
        {
            var topLeftRay = Viewport3DHelper.Point2DtoRay3D(viewport, zoomRectangle.TopLeft);
            var topRightRay = Viewport3DHelper.Point2DtoRay3D(viewport, zoomRectangle.TopRight);
            var centerRay = Viewport3DHelper.Point2DtoRay3D(
                viewport,
                new Point(
                    (zoomRectangle.Left + zoomRectangle.Right) * 0.5, (zoomRectangle.Top + zoomRectangle.Bottom) * 0.5));

            if (topLeftRay == null || topRightRay == null || centerRay == null)
            {
                // could not invert camera matrix
                return;
            }

            var u = topLeftRay.Direction;
            var v = topRightRay.Direction;
            var w = centerRay.Direction;
            u.Normalize();
            v.Normalize();
            w.Normalize();
            var perspectiveCamera = camera as PerspectiveCamera;
            if (perspectiveCamera != null)
            {
                var distance = camera.LookDirection.Length;

                // option 1: change distance
                var newDistance = distance * zoomRectangle.Width / viewport.ActualWidth;
                var newLookDirection = newDistance * w;
                var newPosition = perspectiveCamera.Position + (distance - newDistance) * w;
                var newTarget = newPosition + newLookDirection;
                HelixToolkit.Wpf.CameraHelper.LookAt(camera, newTarget, newLookDirection, 200);

                // option 2: change fov
                //    double newFieldOfView = Math.Acos(Vector3D.DotProduct(u, v));
                //    var newTarget = camera.Position + distance * w;
                //    pcamera.FieldOfView = newFieldOfView * 180 / Math.PI;
                //    LookAt(camera, newTarget, distance * w, 0);
            }

            var orthographicCamera = camera as OrthographicCamera;
            if (orthographicCamera != null)
            {
                orthographicCamera.Width *= zoomRectangle.Width / viewport.ActualWidth;
                var oldTarget = camera.Position + camera.LookDirection;
                var distance = camera.LookDirection.Length;
                Point3D newTarget;
                if (centerRay.PlaneIntersection(oldTarget, w, out newTarget))
                {
                    //orthographicCamera.LookDirection = w * distance; //this is flipping the view axis..
                    orthographicCamera.Position = newTarget - orthographicCamera.LookDirection;
                }
            }
        }

        /// <summary>
        /// Zooms to fit the currently selected plan items.
        /// </summary>
        [CLSCompliant(false)]
        public static void ZoomItems(HelixViewport3D helixViewport, IEnumerable<ModelVisual3D> models)
        {
            if (helixViewport != null)
            {
                Viewport3D viewport = helixViewport.Viewport;

                #region Perspective
                if (viewport.Camera is PerspectiveCamera)
                {
                    //reset the camera
                    helixViewport.ResetCamera();

                    PerspectiveCamera camera = (PerspectiveCamera)helixViewport.Camera;
                    camera.UpDirection = new Vector3D(0, 1, 0);
                    camera.LookDirection = new Vector3D(0, 0, -244);
                    camera.FieldOfView = 60;

                    //find the bounds of them all
                    Rect3D bounds = Rect3D.Empty;
                    foreach (ModelVisual3D visual in models)
                    {
                        bounds.Union(RenderControlsHelper.FindBounds(visual, Viewport3DHelper.GetTransform(viewport, visual)));
                    }

                    //zoom extents
                    helixViewport.ZoomExtents(bounds);
                }
                #endregion

                #region Orthographic
                else if (viewport.Camera is OrthographicCamera)
                {

                    //zoom to fit all first
                    helixViewport.ZoomExtents();

                    //In order to perform the zoom by rectangle we must be able to invert the camera
                    // Sometimes however the model is so big that when fully zoomed to extent this is not possible.
                    // If this is the case then try zooming in a bit.
                    Matrix3D matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                    if (!matrixCamera.HasInverse)
                    {
                        Int32 tryCount = 0;
                        while (!matrixCamera.HasInverse && tryCount < 15)
                        {
                            ZoomIn(helixViewport);
                            matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                            tryCount++;
                        }

                    }

                    //find the bounds of them all
                    Rect3D bounds = Rect3D.Empty;
                    foreach (ModelVisual3D visual in models)
                    {
                        bounds.Union(RenderControlsHelper.FindBounds(visual, Viewport3DHelper.GetTransform(viewport, visual)));
                    }

                    //zoom extents
                    helixViewport.ZoomExtents(bounds);


                    ////get the bounds of all the selected models.
                    //Rect bounds = Rect.Empty;
                    //foreach (ModelVisual3D visual in models)
                    //{
                    //    if (visual is ModelConstruct3D)
                    //    {
                    //        bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, (ModelConstruct3D)visual));
                    //    }
                    //}

                    ////zoom
                    //ZoomToFitBounds(bounds, viewport, helixViewport.Camera);

                }
                #endregion
            }
        }

        /// <summary>
        /// Controls the viewer state for zoom to area mode.
        /// </summary>
        /// <param name="isZoomToAreaOn"></param>
        [CLSCompliant(false)]
        public static void SetZoomToAreaMode(HelixViewport3D helixViewport, Boolean isZoomToAreaOn)
        {
            if (isZoomToAreaOn)
            {
                helixViewport.ZoomRectangleGesture = new MouseGesture(MouseAction.LeftClick);
            }
            else
            {
                helixViewport.ZoomRectangleGesture = new MouseGesture(MouseAction.RightClick, ModifierKeys.Control | ModifierKeys.Shift);
            }
        }

        /// <summary>
        /// Sets the panning mode state of the viewer.
        /// </summary>
        /// <param name="isPanningModeOn"></param>
        [CLSCompliant(false)]
        public static void SetPanningMode(HelixViewport3D helixViewport, Boolean isPanningModeOn)
        {
            if (isPanningModeOn)
            {
                helixViewport.PanGesture = new MouseGesture(MouseAction.LeftClick);
            }
            else
            {
                helixViewport.PanGesture = new MouseGesture(MouseAction.LeftClick, ModifierKeys.Alt);
            }
        }
    }
}
