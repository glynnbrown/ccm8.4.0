﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentProductBuddyDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentProductBuddyDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentProductBuddyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentProductBuddyDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentProductBuddyId]),
                ProductId = (Int32)GetValue(dr[FieldNames.AssortmentProductBuddyProductId]),
                ProductGtin = (String)GetValue(dr[FieldNames.AssortmentProductBuddyProductGtin]),
                SourceType = (Byte)GetValue(dr[FieldNames.AssortmentProductBuddySourceType]),
                TreatmentType = (Byte)GetValue(dr[FieldNames.AssortmentProductBuddyTreatmentType]),
                TreatmentTypePercentage = (Single)GetValue(dr[FieldNames.AssortmentProductBuddyTreatmentTypePercentage]),
                ProductAttributeType = (Byte)GetValue(dr[FieldNames.AssortmentProductBuddyProductAttributeType]),
                S1ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentProductBuddyS1ProductId]),
                S1Percentage = (Single?)GetValue(dr[FieldNames.AssortmentProductBuddyS1Percentage]),
                S1ProductGtin = (String)GetValue(dr[FieldNames.AssortmentProductBuddyS1ProductGtin]),
                S2ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentProductBuddyS2ProductId]),
                S2Percentage = (Single?)GetValue(dr[FieldNames.AssortmentProductBuddyS2Percentage]),
                S2ProductGtin = (String)GetValue(dr[FieldNames.AssortmentProductBuddyS2ProductGtin]),
                S3ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentProductBuddyS3ProductId]),
                S3Percentage = (Single?)GetValue(dr[FieldNames.AssortmentProductBuddyS3Percentage]),
                S3ProductGtin = (String)GetValue(dr[FieldNames.AssortmentProductBuddyS3ProductGtin]),
                S4ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentProductBuddyS4ProductId]),
                S4Percentage = (Single?)GetValue(dr[FieldNames.AssortmentProductBuddyS4Percentage]),
                S4ProductGtin = (String)GetValue(dr[FieldNames.AssortmentProductBuddyS4ProductGtin]),
                S5ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentProductBuddyS5ProductId]),
                S5Percentage = (Single?)GetValue(dr[FieldNames.AssortmentProductBuddyS5Percentage]),
                S5ProductGtin = (String)GetValue(dr[FieldNames.AssortmentProductBuddyS5ProductGtin]),
                AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentProductBuddyAssortmentId])

            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all product dtos for the specified assortment
        /// </summary>
        /// <param name="assortmentId">The assortment id</param>
        /// <returns>All product dtos for the assortment</returns>
        public IEnumerable<AssortmentProductBuddyDto> FetchByAssortmentId(Int32 assortmentId)
        {
            List<AssortmentProductBuddyDto> dtoList = new List<AssortmentProductBuddyDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductBuddyFetchByAssortmentId))
            {
                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentProductBuddyAssortmentId,
                    SqlDbType.Int,
                    assortmentId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment local product id</param>
        /// <returns>The assortment local product</returns>
        public AssortmentProductBuddyDto FetchById(Int32 id)
        {
            AssortmentProductBuddyDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductBuddyFetchById))
            {
                // assortment Product buddy id
                CreateParameter(command,
                    FieldNames.AssortmentProductBuddyId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentProductBuddyDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductBuddyInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentProductBuddyId,
                    SqlDbType.Int);

                //Other properties 
                CreateParameter(command, FieldNames.ProductId, SqlDbType.Int, dto.ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddySourceType, SqlDbType.TinyInt, dto.SourceType);
                CreateParameter(command, FieldNames.AssortmentProductBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                CreateParameter(command, FieldNames.AssortmentProductBuddyTreatmentTypePercentage, SqlDbType.Real, dto.TreatmentTypePercentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyProductAttributeType, SqlDbType.TinyInt, dto.ProductAttributeType);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS1ProductId, SqlDbType.Int, dto.S1ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS2ProductId, SqlDbType.Int, dto.S2ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS3ProductId, SqlDbType.Int, dto.S3ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS4ProductId, SqlDbType.Int, dto.S4ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS5ProductId, SqlDbType.Int, dto.S5ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyAssortmentId, SqlDbType.Int, dto.AssortmentId);

                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentProductBuddyDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductBuddyUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentProductBuddyId,
                    SqlDbType.Int,
                    dto.Id);

                //Other properties 
                CreateParameter(command, FieldNames.ProductId, SqlDbType.Int, dto.ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddySourceType, SqlDbType.TinyInt, dto.SourceType);
                CreateParameter(command, FieldNames.AssortmentProductBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                CreateParameter(command, FieldNames.AssortmentProductBuddyTreatmentTypePercentage, SqlDbType.Real, dto.TreatmentTypePercentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyProductAttributeType, SqlDbType.TinyInt, dto.ProductAttributeType);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS1ProductId, SqlDbType.Int, dto.S1ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS2ProductId, SqlDbType.Int, dto.S2ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS3ProductId, SqlDbType.Int, dto.S3ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS4ProductId, SqlDbType.Int, dto.S4ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS5ProductId, SqlDbType.Int, dto.S5ProductId);
                CreateParameter(command, FieldNames.AssortmentProductBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                CreateParameter(command, FieldNames.AssortmentProductBuddyAssortmentId, SqlDbType.Int, dto.AssortmentId);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductBuddyDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentProductBuddyId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}