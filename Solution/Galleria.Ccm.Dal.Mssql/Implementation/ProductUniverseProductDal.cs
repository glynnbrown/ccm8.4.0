﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class ProductUniverseProductDal : Galleria.Framework.Dal.Mssql.DalBase, IProductUniverseProductDal
    {
        #region Constants

        private const String _importTableName = "#tmpProductUniverseProduct";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[ProductUniverse_Id] [INT], " +
                "[Product_Id] [INT] " +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class ProductUniverseProductBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<ProductUniverseProductDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ProductUniverseProductBulkCopyDataReader(IEnumerable<ProductUniverseProductDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 2; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.ProductUniverseId;
                        break;
                    case 1:
                        value = _enumerator.Current.ProductId;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public ProductUniverseProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductUniverseProductDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductUniverseProductId]),
                Gtin = (String)GetValue(dr[FieldNames.ProductUniverseProductGtin]),
                Name = (String)GetValue(dr[FieldNames.ProductUniverseProductName]),
                ProductId = (Int32)GetValue(dr[FieldNames.ProductUniverseProductProductId]),
                ProductUniverseId = (Int32)GetValue(dr[FieldNames.ProductUniverseProductProductUniverseId]),
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The product universe product id</param>
        /// <returns>The product universe product</returns>
        public ProductUniverseProductDto FetchById(Int32 id)
        {
            ProductUniverseProductDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductUniverseProductFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductUniverseProductId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<ProductUniverseProductDto> FetchByProductUniverseId(Int32 productUniverseId)
        {
            List<ProductUniverseProductDto> dtoList = new List<ProductUniverseProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductUniverseProductFetchByProductUniverseId))
                {
                    //Other properties, 
                    CreateParameter(command, FieldNames.ProductUniverseProductProductUniverseId, SqlDbType.Int, productUniverseId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ProductUniverseProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductUniverseProductInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductUniverseProductId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductUniverseProductProductUniverseId, SqlDbType.Int, dto.ProductUniverseId);
                    CreateParameter(command, FieldNames.ProductUniverseProductProductId, SqlDbType.Int, dto.ProductId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ProductUniverseProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductUniverseProductUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductUniverseProductId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductUniverseProductProductUniverseId, SqlDbType.Int, dto.ProductUniverseId);
                    CreateParameter(command, FieldNames.ProductUniverseProductProductId, SqlDbType.Int, dto.ProductId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<ProductUniverseProductDto> dtoList, ProductUniverseProductIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new ProductUniverseProductIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<ProductUniverseProductDtoKey, ProductUniverseProductDto> keyedDtoList = new Dictionary<ProductUniverseProductDtoKey, ProductUniverseProductDto>();
            foreach (ProductUniverseProductDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    ProductUniverseProductBulkCopyDataReader dr = new ProductUniverseProductBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<ProductUniverseProductDtoKey, Int32> ids = new Dictionary<ProductUniverseProductDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.ProductUniverseProductBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.ProductUniverseProductSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Int32 outputDataId = (Int32)GetValue(dr[FieldNames.ProductUniverseProductId]);

                            ProductUniverseProductDtoKey key = new ProductUniverseProductDtoKey();
                            key.ProductUniverseId = (Int32)GetValue(dr[FieldNames.ProductUniverseProductProductUniverseId]);
                            key.ProductId = (Int32)GetValue(dr[FieldNames.ProductUniverseProductProductId]);
                            ids[key] = outputDataId;
                        }
                    }
                }

                foreach (ProductUniverseProductDto dto in dtoList)
                {
                    Int32 outputDataId;

                    if (ids.TryGetValue(dto.DtoKey, out outputDataId))
                    {
                        dto.Id = outputDataId;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(string importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private string GenerateSetPropertiesString(ProductUniverseProductIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsProductUniverseIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ProductUniverseProductProductUniverseId);
            }
            if (isSetDto.IsProductIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ProductUniverseProductProductId);
            }

            return setProperties;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductUniverseProductDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductUniverseProductId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
