﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 J.Pickup
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Data;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Mssql.Schema;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    class HighlightFilterDal : DalBase, IHighlightFilterDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static HighlightFilterDto GetDataTransferObject(IDataRecord dr)
        {
            return new HighlightFilterDto
            {
                Id = (Int32)GetValue(dr[FieldNames.HighlightFilterId]),
                HighlightId = (Int32)GetValue(dr[FieldNames.HighlightFilterHighlightId]),
                Field = (String)GetValue(dr[FieldNames.HighlightFilterField]),
                Type = (Byte)GetValue(dr[FieldNames.HighlightFilterType]),
                Value = (String)GetValue(dr[FieldNames.HighlightFilterValue]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="highlightCharachteristicDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="highlightCharachteristicDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="highlightCharachteristicDto" /> with the data for the provided <paramref name="id" />.</returns>
        public IEnumerable<HighlightFilterDto> FetchByHighlightId(Object id)
        {
            List<HighlightFilterDto> dtoList = new List<HighlightFilterDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightFilterFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightFilterHighlightId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="HighlightCharacteristicDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(HighlightFilterDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightFilterInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.HighlightFilterId, SqlDbType.Int);

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightFilterHighlightId, SqlDbType.Int, dto.HighlightId);
                    CreateParameter(command, FieldNames.HighlightFilterField, SqlDbType.NVarChar, dto.Field);
                    CreateParameter(command, FieldNames.HighlightFilterType, SqlDbType.SmallInt, dto.Type);
                    CreateParameter(command, FieldNames.HighlightFilterValue, SqlDbType.NVarChar, dto.Value);


                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="HighlightCharacteristicDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(HighlightFilterDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightFilterUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightFilterId, SqlDbType.Int, dto.Id);

                  
                    //Other properties
                    CreateParameter(command, FieldNames.HighlightFilterHighlightId, SqlDbType.Int, dto.HighlightId);
                    CreateParameter(command, FieldNames.HighlightFilterField, SqlDbType.NVarChar, dto.Field);
                    CreateParameter(command, FieldNames.HighlightFilterType, SqlDbType.SmallInt, dto.Type);
                    CreateParameter(command, FieldNames.HighlightFilterValue, SqlDbType.NVarChar, dto.Value);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    // Nothing to update.
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightFilterDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightFilterId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
