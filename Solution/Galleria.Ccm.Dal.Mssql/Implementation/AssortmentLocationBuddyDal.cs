﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentLocationBuddyDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentLocationBuddyDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentLocationBuddyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentLocationBuddyDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentLocationBuddyId]),
                LocationId = (Int16)GetValue(dr[FieldNames.AssortmentLocationBuddyLocationId]),
                LocationCode = (String)GetValue(dr[FieldNames.AssortmentLocationBuddyLocationCode]),
                TreatmentType = (Byte)GetValue(dr[FieldNames.AssortmentLocationBuddyTreatmentType]),
                S1LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentLocationBuddyS1LocationId]),
                S1Percentage = (Single?)GetValue(dr[FieldNames.AssortmentLocationBuddyS1Percentage]),
                S1LocationCode = (String)GetValue(dr[FieldNames.AssortmentLocationBuddyS1LocationCode]),
                S2LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentLocationBuddyS2LocationId]),
                S2Percentage = (Single?)GetValue(dr[FieldNames.AssortmentLocationBuddyS2Percentage]),
                S2LocationCode = (String)GetValue(dr[FieldNames.AssortmentLocationBuddyS2LocationCode]),
                S3LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentLocationBuddyS3LocationId]),
                S3Percentage = (Single?)GetValue(dr[FieldNames.AssortmentLocationBuddyS3Percentage]),
                S3LocationCode = (String)GetValue(dr[FieldNames.AssortmentLocationBuddyS3LocationCode]),
                S4LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentLocationBuddyS4LocationId]),
                S4Percentage = (Single?)GetValue(dr[FieldNames.AssortmentLocationBuddyS4Percentage]),
                S4LocationCode = (String)GetValue(dr[FieldNames.AssortmentLocationBuddyS4LocationCode]),
                S5LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentLocationBuddyS5LocationId]),
                S5Percentage = (Single?)GetValue(dr[FieldNames.AssortmentLocationBuddyS5Percentage]),
                S5LocationCode = (String)GetValue(dr[FieldNames.AssortmentLocationBuddyS5LocationCode]),
                AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentLocationBuddyAssortmentId])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all product dtos for the specified assortment
        /// </summary>
        /// <param name="assortmentId">The assortment id</param>
        /// <returns>All product dtos for the assortment</returns>
        public IEnumerable<AssortmentLocationBuddyDto> FetchByAssortmentId(Int32 assortmentId)
        {
            List<AssortmentLocationBuddyDto> dtoList = new List<AssortmentLocationBuddyDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationBuddyFetchByAssortmentId))
            {
                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentLocationBuddyAssortmentId,
                    SqlDbType.Int,
                    assortmentId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment local product id</param>
        /// <returns>The assortment local product</returns>
        public AssortmentLocationBuddyDto FetchById(Int32 id)
        {
            AssortmentLocationBuddyDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationBuddyFetchById))
            {
                // assortment local product id
                CreateParameter(command,
                    FieldNames.AssortmentLocationBuddyId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentLocationBuddyDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationBuddyInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentLocationBuddyId,
                    SqlDbType.Int);

                CreateParameter(command, FieldNames.LocationId, SqlDbType.SmallInt, dto.LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS1LocationId, SqlDbType.SmallInt, dto.S1LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS2LocationId, SqlDbType.SmallInt, dto.S2LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS3LocationId, SqlDbType.SmallInt, dto.S3LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS4LocationId, SqlDbType.SmallInt, dto.S4LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS5LocationId, SqlDbType.SmallInt, dto.S5LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyAssortmentId, SqlDbType.Int, dto.AssortmentId);

                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentLocationBuddyDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationBuddyUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentLocationBuddyId,
                    SqlDbType.Int,
                    dto.Id);

                CreateParameter(command, FieldNames.LocationId, SqlDbType.SmallInt, dto.LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS1LocationId, SqlDbType.SmallInt, dto.S1LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS2LocationId, SqlDbType.SmallInt, dto.S2LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS3LocationId, SqlDbType.SmallInt, dto.S3LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS4LocationId, SqlDbType.SmallInt, dto.S4LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS5LocationId, SqlDbType.SmallInt, dto.S5LocationId);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                CreateParameter(command, FieldNames.AssortmentLocationBuddyAssortmentId, SqlDbType.Int, dto.AssortmentId);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationBuddyDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentLocationBuddyId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}