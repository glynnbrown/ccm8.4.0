﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// AssortmentLocation Dal
    /// </summary>
    public class AssortmentLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentLocationDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            try
            {
                return new AssortmentLocationDto()
                {
                    Id = (Int32)GetValue(dr[FieldNames.AssortmentLocationId]),
                    LocationId = (Int16)GetValue(dr[FieldNames.AssortmentLocationLocationId]),
                    Code = (String)GetValue(dr[FieldNames.AssortmentLocationCode]),
                    Name = (String)GetValue(dr[FieldNames.AssortmentLocationLocationName]),
                    AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentLocationAssortmentId])
                };

            }
            catch (Exception) { return null; }
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<AssortmentLocationDto> FetchByAssortmentId(Int32 id)
        {
            List<AssortmentLocationDto> dtoList = new List<AssortmentLocationDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationFetchByAssortmentId))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentLocationAssortmentId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentLocationDto FetchById(Int32 id)
        {
            AssortmentLocationDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentLocationId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentLocationLocationId, SqlDbType.Int, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentLocationAssortmentId, SqlDbType.Int, dto.AssortmentId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentLocationLocationId, SqlDbType.Int, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentLocationAssortmentId, SqlDbType.Int, dto.AssortmentId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto: None
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}