﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramAssortmentProductBuddyDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssortmentProductBuddyDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssortmentProductBuddy DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssortmentProductBuddyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssortmentProductBuddyDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyId]),
                ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyProductGtin]),
                SourceType = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddySourceType]),
                TreatmentType = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyTreatmentType]),
                TreatmentTypePercentage = (Single)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyTreatmentTypePercentage]),
                ProductAttributeType = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyProductAttributeType]),
                S1ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS1ProductGtin]),
                S1Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS1Percentage]),
                S2ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS2ProductGtin]),
                S2Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS2Percentage]),
                S3ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS3ProductGtin]),
                S3Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS3Percentage]),
                S4ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS4ProductGtin]),
                S4Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS4Percentage]),
                S5ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS5ProductGtin]),
                S5Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyS5Percentage]),
                PlanogramAssortmentId = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentProductBuddyPlanogramAssortmentId]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssortmentProductBuddy DTOs with a matching Planogram Assortment Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramAssortmentProductBuddy DTOs.</returns>
        public IEnumerable<PlanogramAssortmentProductBuddyDto> FetchByPlanogramAssortmentId(object id)
        {
            var dtoList = new List<PlanogramAssortmentProductBuddyDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductBuddyFetchByPlanogramAssortmentId))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyPlanogramAssortmentId, SqlDbType.Int, (Int32)id);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssortmentProductBuddyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductBuddyInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddySourceType, SqlDbType.TinyInt, dto.SourceType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyTreatmentTypePercentage, SqlDbType.Real, dto.TreatmentTypePercentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyProductAttributeType, SqlDbType.TinyInt, dto.ProductAttributeType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS1ProductGtin, SqlDbType.NVarChar, dto.S1ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS2ProductGtin, SqlDbType.NVarChar, dto.S2ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS3ProductGtin, SqlDbType.NVarChar, dto.S3ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS4ProductGtin, SqlDbType.NVarChar, dto.S4ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS5ProductGtin, SqlDbType.NVarChar, dto.S5ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentProductBuddyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssortmentProductBuddyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductBuddyUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddySourceType, SqlDbType.TinyInt, dto.SourceType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyTreatmentTypePercentage, SqlDbType.Real, dto.TreatmentTypePercentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyProductAttributeType, SqlDbType.TinyInt, dto.ProductAttributeType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS1ProductGtin, SqlDbType.NVarChar, dto.S1ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS2ProductGtin, SqlDbType.NVarChar, dto.S2ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS3ProductGtin, SqlDbType.NVarChar, dto.S3ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS4ProductGtin, SqlDbType.NVarChar, dto.S4ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS5ProductGtin, SqlDbType.NVarChar, dto.S5ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentProductBuddyDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductBuddyDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductBuddyId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentProductBuddyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentProductBuddyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
