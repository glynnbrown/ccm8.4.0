﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;


namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// UserInfo Dal
    /// </summary>
    public class UserInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IUserInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static UserInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new UserInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.UserId]),
                UserName = (String)GetValue(dr[FieldNames.UserUserName]),
                FirstName = (String)GetValue(dr[FieldNames.UserFirstName]),
                LastName = (String)GetValue(dr[FieldNames.UserLastName]),
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns all user info from database
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<UserInfoDto> FetchAll()
        {
            List<UserInfoDto> dtoList = new List<UserInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserInfoFetchAll))
                {
                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion
    }
}
