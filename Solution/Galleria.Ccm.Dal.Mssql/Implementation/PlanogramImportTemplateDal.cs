﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27939 : L.Luong
//  Created

#endregion
#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion
#region Version History: (CCM 8.30)
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramImportTemplateDal
    /// </summary>
    public class PlanogramImportTemplateDal : DalBase, IPlanogramImportTemplateDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramImportTemplateDto GetDataTransferObject(IDataRecord dr)
        {
            return new PlanogramImportTemplateDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplateId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PlanogramImportTemplateRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplateEntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramImportTemplateName]),
                FileType = (Byte)GetValue(dr[FieldNames.PlanogramImportTemplateFileType]),
                FileVersion = (String)GetValue(dr[FieldNames.PlanogramImportTemplateFileVersion]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.PlanogramImportTemplateDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.PlanogramImportTemplateDateLastModified]),
                AllowAdvancedBaySplitting = (Boolean)GetValue(dr[FieldNames.PlanogramImportTemplateAllowAdvancedBaySplitting]),
                CanSplitComponents = (Boolean)GetValue(dr[FieldNames.PlanogramImportTemplateCanSplitComponents]),
                SplitBaysByComponentStart = (Boolean)GetValue(dr[FieldNames.PlanogramImportTemplateSplitBaysByComponentStart]),
                SplitBaysByComponentEnd = (Boolean)GetValue(dr[FieldNames.PlanogramImportTemplateSplitBaysByComponentEnd]),
                SplitByFixedSize = (Boolean)GetValue(dr[FieldNames.PlanogramImportTemplateSplitByFixedSize]),
                SplitByRecurringPattern = (Boolean)GetValue(dr[FieldNames.PlanogramImportTemplateSplitByRecurringPattern]),
                SplitByBayCount = (Boolean)GetValue(dr[FieldNames.PlanogramImportTemplateSplitByBayCount]),
                SplitFixedSize = (Double)GetValue(dr[FieldNames.PlanogramImportTemplateSplitFixedSize]),
                SplitRecurringPattern = (String)GetValue(dr[FieldNames.PlanogramImportTemplateSplitRecurringPattern]),
                SplitBayCount = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplateSplitBayCount])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="PlanogramImportTemplateDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="PlanogramImportTemplateDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="PlanogramImportTemplateDto" /> with the data for the provided <paramref name="id" />.</returns>
        public PlanogramImportTemplateDto FetchById(Object id)
        {
            PlanogramImportTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImportTemplateId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public PlanogramImportTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            PlanogramImportTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.PlanogramImportTemplateEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateName, SqlDbType.NVarChar, name);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="PlanogramImportTemplateDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(PlanogramImportTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateId, SqlDbType.Int);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);


                    //Other properties
                    CreateParameter(command, FieldNames.PlanogramImportTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateFileType, SqlDbType.TinyInt, dto.FileType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateFileVersion, SqlDbType.NVarChar, dto.FileVersion);

                    //Bay splitting properties
                    CreateParameter(command, FieldNames.PlanogramImportTemplateAllowAdvancedBaySplitting, SqlDbType.Bit, dto.AllowAdvancedBaySplitting);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateCanSplitComponents, SqlDbType.Bit, dto.CanSplitComponents);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitBaysByComponentStart, SqlDbType.Bit, dto.SplitBaysByComponentStart);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitBaysByComponentEnd, SqlDbType.Bit, dto.SplitBaysByComponentEnd);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitByFixedSize, SqlDbType.Bit, dto.SplitByFixedSize);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitByRecurringPattern, SqlDbType.Bit, dto.SplitByRecurringPattern);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitByBayCount, SqlDbType.Bit, dto.SplitByBayCount);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitFixedSize, SqlDbType.Float, dto.SplitFixedSize);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitRecurringPattern, SqlDbType.NVarChar, dto.SplitRecurringPattern);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitBayCount, SqlDbType.Int, dto.SplitBayCount);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="PlanogramImportTemplateDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(PlanogramImportTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImportTemplateId, SqlDbType.Int, dto.Id);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateRowVersion, SqlDbType.Timestamp,
                        ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);


                    //Other properties
                    CreateParameter(command, FieldNames.PlanogramImportTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateFileType, SqlDbType.TinyInt, dto.FileType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateFileVersion, SqlDbType.NVarChar, dto.FileVersion);

                    //Bay splitting properties
                    CreateParameter(command, FieldNames.PlanogramImportTemplateAllowAdvancedBaySplitting, SqlDbType.Bit, dto.AllowAdvancedBaySplitting);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateCanSplitComponents, SqlDbType.Bit, dto.CanSplitComponents);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitBaysByComponentStart, SqlDbType.Bit, dto.SplitBaysByComponentStart);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitBaysByComponentEnd, SqlDbType.Bit, dto.SplitBaysByComponentEnd);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitByFixedSize, SqlDbType.Bit, dto.SplitByFixedSize);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitByRecurringPattern, SqlDbType.Bit, dto.SplitByRecurringPattern);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitByBayCount, SqlDbType.Bit, dto.SplitByBayCount);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitFixedSize, SqlDbType.Float, dto.SplitFixedSize);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitRecurringPattern, SqlDbType.NVarChar, dto.SplitRecurringPattern);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateSplitBayCount, SqlDbType.Int, dto.SplitBayCount);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImportTemplateId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a PlanogramImportTemplate for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the PlanogramImportTemplate.</param>
        /// <returns><c>True</c> if the PlanogramImportTemplate was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void LockById(Object id)
        {
            // Not implemented as this dal does not use locking mechamisms.
        }

        /// <summary>
        ///     Unlocks a PlanogramImportTemplate after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the PlanogramImportTemplate.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }

        #endregion

    }
}
