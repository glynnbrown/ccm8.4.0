﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27939 : L.Luong
//  Created

#endregion

#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramImportTemplateInfoDal
    /// </summary>
    public class PlanogramImportTemplateInfoDal : DalBase, IPlanogramImportTemplateInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramImportTemplateInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramImportTemplateInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplateId]),
                EntityId = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplateEntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramImportTemplateName])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramImportTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<PlanogramImportTemplateInfoDto> dtoList = new List<PlanogramImportTemplateInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramImportTemplateInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.PlanogramImportTemplateEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        #endregion
    }
}
