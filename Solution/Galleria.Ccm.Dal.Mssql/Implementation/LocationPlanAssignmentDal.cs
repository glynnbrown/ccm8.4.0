﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//		Created (Auto-generated)
#endregion

#region Version History: (CCM 8.2.0)
// V8-30732 : M.Shelley
//  Added 2 new fields : OutputDestination and PlanPublishType
// V8-30508 : M.Shelley
//  Added a new field : PublishStatus
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
	/// <summary>
	/// LocationPlanAssignment Dal
	/// </summary>
	public class LocationPlanAssignmentDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationPlanAssignmentDal
	{

		#region Data Transfer Object

		/// <summary>
		/// Returns a dto from a data reader
		/// </summary>
		/// <param name="dr">The data reader to load from</param>
		/// <returns>A new dto</returns>
		public static LocationPlanAssignmentDto GetDataTransferObject(SqlDataReader dr)
		{
            Byte? PublishType = (Byte?)GetValue(dr[FieldNames.LocationPlanAssignmentPublishType]);
            String OutputDestination = (String)GetValue(dr[FieldNames.LocationPlanAssignmentOutputDestination]);
            var PublishStatus = (Byte?)GetValue(dr[FieldNames.LocationPlanAssignmentPublishStatus]);

			return new LocationPlanAssignmentDto()
			{
				Id = (Int32)GetValue(dr[FieldNames.LocationPlanAssignmentId]),
				PlanogramId = (Int32)GetValue(dr[FieldNames.LocationPlanAssignmentPlanId]),
				ProductGroupId = (Int32)GetValue(dr[FieldNames.LocationPlanAssignmentProductGroupId]),
				LocationId = (Int16)GetValue(dr[FieldNames.LocationPlanAssignmentLocationId]),
				AssignedByUserId = (Int32?)GetValue(dr[FieldNames.LocationPlanAssignmentAssignedByUserId]),
				PublishUserId=(Int32?)GetValue(dr[FieldNames.LocationPlanAssignmentPublishUserId]),
				DateAssigned = (DateTime?)GetValue(dr[FieldNames.LocationPlanAssignmentDateAssigned]),
				DatePublished = (DateTime?)GetValue(dr[FieldNames.LocationPlanAssignmentDatePublished]),
                DateCommunicated = (DateTime?)GetValue(dr[FieldNames.LocationPlanAssignmentDateCommunicated]),
                DateLive = (DateTime?)GetValue(dr[FieldNames.LocationPlanAssignmentDateLive]),
                PublishType = (Byte?)GetValue(dr[FieldNames.LocationPlanAssignmentPublishType]),
                OutputDestination = (String)GetValue(dr[FieldNames.LocationPlanAssignmentOutputDestination]),
                PublishStatus = (Byte?)GetValue(dr[FieldNames.LocationPlanAssignmentPublishStatus]),
            };
		}

		#endregion

		#region Fetch

		/// <summary>
		/// Returns all file infos in the database
		/// </summary>
		/// <returns>A list of LocationPlanAssignment dtos</returns>
		public LocationPlanAssignmentDto FetchById(Int32 id)
		{
			using (DalCommand command = CreateCommand(ProcedureNames.LocationPlanAssignmentFetchById))
			{
				// id
				CreateParameter(
					command,
					FieldNames.LocationPlanAssignmentId,
					SqlDbType.Int,
					id);

				// execute
				using (SqlDataReader dr = command.ExecuteReader())
				{
					if (dr.Read())
					{
						return GetDataTransferObject(dr);
					}
					else
					{
						throw new DtoDoesNotExistException();
					}
				}
			}
		}

		/// <summary>
		/// Returns all file infos in the database
		/// </summary>
		/// <returns>A list of LocationPlanAssignment dtos</returns>
		public IEnumerable<LocationPlanAssignmentDto> FetchByPlanogramId(Int32 id)
		{
            List<LocationPlanAssignmentDto> dtoList = new List<LocationPlanAssignmentDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationPlanAssignmentFetchByPlanogramId))
                {
                    // id
                    CreateParameter(
                        command,
                        FieldNames.LocationPlanAssignmentPlanId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
			return dtoList;
		}

		/// <summary>
		/// Returns all LocationPlanAssignmentDtos for a product group
		/// </summary>
		/// <returns>A list of LocationPlanAssignment dtos</returns>
		public IEnumerable<LocationPlanAssignmentDto> FetchByProductGroupId(Int32 productGroupId)
		{
			List<LocationPlanAssignmentDto> dtoList = new List<LocationPlanAssignmentDto>();
			try
			{
				using (DalCommand command = CreateCommand(ProcedureNames.LocationPlanAssignmentFetchByProductGroupId))
				{
					// id
					CreateParameter(
						command,
						FieldNames.LocationPlanAssignmentProductGroupId,
						SqlDbType.Int,
						productGroupId);

					// execute
					using (SqlDataReader dr = command.ExecuteReader())
					{
						while (dr.Read())
						{
							dtoList.Add(GetDataTransferObject(dr));
						}
					}
				}
			}
			catch (SqlException e)
			{
				DalExceptionHelper.ThrowException(e);
			}
			return dtoList;
		}

        // ToDo: Put in correct SP name
        /// <summary>
        /// Returns all LocationPlanAssignmentDtos for a location
        /// </summary>
        /// <returns>A list of LocationPlanAssignment dtos</returns>
        public IEnumerable<LocationPlanAssignmentDto> FetchByLocationId(Int16 locationId)
        {
            List<LocationPlanAssignmentDto> dtoList = new List<LocationPlanAssignmentDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationPlanAssignmentFetchByLocationId))
                {
                    // location id
                    CreateParameter(command, FieldNames.LocationPlanAssignmentLocationId, SqlDbType.SmallInt, locationId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

		#endregion

		#region Insert

		/// <summary>
		/// Inserts a specified dto into the db
		/// </summary>
		/// <param name="dto">dto to insert</param>
		public void Insert(LocationPlanAssignmentDto dto)
		{
			try
			{
				using (DalCommand command = CreateCommand(ProcedureNames.LocationPlanAssignmentInsert))
				{
					SqlParameter idParameter = CreateParameter(command, FieldNames.LocationPlanAssignmentId, SqlDbType.Int);

					CreateParameter(command, FieldNames.LocationPlanAssignmentProductGroupId, SqlDbType.Int, dto.ProductGroupId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentPlanId, SqlDbType.Int, dto.PlanogramId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentLocationId, SqlDbType.SmallInt, dto.LocationId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentAssignedByUserId, SqlDbType.Int, dto.AssignedByUserId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentPublishUserId, SqlDbType.Int, dto.PublishUserId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentDateAssigned, SqlDbType.DateTime, dto.DateAssigned);
					CreateParameter(command, FieldNames.LocationPlanAssignmentDatePublished, SqlDbType.DateTime, dto.DatePublished);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentDateCommunicated, SqlDbType.DateTime, dto.DateCommunicated);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentDateLive, SqlDbType.DateTime, dto.DateLive);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentPublishType, SqlDbType.TinyInt, dto.PublishType);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentOutputDestination, SqlDbType.NVarChar, dto.OutputDestination);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentPublishStatus , SqlDbType.TinyInt, dto.PublishStatus);

					//execute
					command.ExecuteNonQuery();

					//update dto
                    var result = idParameter.Value;
                    if (result != null && !(result is DBNull))
                    {
                        dto.Id = (Int32) idParameter.Value;
                    }
				}
			}
			catch (SqlException e)
			{
				DalExceptionHelper.ThrowException(e);
			}
		}

		#endregion

		#region Update

		/// <summary>
		/// Update a specified dto in the db
		/// </summary>
		/// <param name="dto">dto to update</param>
		public void Update(LocationPlanAssignmentDto dto)
		{
			try
			{
				using (DalCommand command = CreateCommand(ProcedureNames.LocationPlanAssignmentUpdateById))
				{
					//Id
					CreateParameter(command, FieldNames.LocationPlanAssignmentId, SqlDbType.Int, dto.Id);

					//Other properties 
					CreateParameter(command, FieldNames.LocationPlanAssignmentProductGroupId, SqlDbType.Int, dto.ProductGroupId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentPlanId, SqlDbType.Int, dto.PlanogramId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentLocationId, SqlDbType.SmallInt, dto.LocationId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentAssignedByUserId, SqlDbType.Int, dto.AssignedByUserId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentPublishUserId, SqlDbType.Int, dto.PublishUserId);
					CreateParameter(command, FieldNames.LocationPlanAssignmentDateAssigned, SqlDbType.DateTime, dto.DateAssigned);
					CreateParameter(command, FieldNames.LocationPlanAssignmentDatePublished, SqlDbType.DateTime, dto.DatePublished);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentDateCommunicated, SqlDbType.DateTime, dto.DateCommunicated);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentDateLive, SqlDbType.DateTime, dto.DateLive);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentPublishType, SqlDbType.TinyInt, dto.PublishType);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentOutputDestination, SqlDbType.NVarChar, dto.OutputDestination);
                    CreateParameter(command, FieldNames.LocationPlanAssignmentPublishStatus, SqlDbType.TinyInt, dto.PublishStatus);

					//execute
					command.ExecuteNonQuery();
				}
			}
			catch (SqlException e)
			{
				DalExceptionHelper.ThrowException(e);
			}
		}

		#endregion

		#region Delete

		/// <summary>
		/// Deletes a dto that matches the specified id
		/// </summary>
		/// <param name="id">specified id</param>
		public void DeleteById(Int32 id)
		{
			try
			{
				using (DalCommand command = CreateCommand(ProcedureNames.LocationPlanAssignmentDeleteById))
				{
					//Id
					CreateParameter(command, FieldNames.LocationPlanAssignmentId, SqlDbType.Int, id);

					//Execute
					command.ExecuteNonQuery();
				}
			}
			catch (SqlException e)
			{
				DalExceptionHelper.ThrowException(e);
			}
		}

		#endregion
	}
}
