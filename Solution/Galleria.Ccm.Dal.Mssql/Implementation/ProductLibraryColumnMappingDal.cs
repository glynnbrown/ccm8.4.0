﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.30)
// V8-32361 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IProductLibraryColumnMappingDal
    /// </summary>
    public sealed class ProductLibraryColumnMappingDal : DalBase, IProductLibraryColumnMappingDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static ProductLibraryColumnMappingDto GetDataTransferObject(IDataRecord dr)
        {
            return new ProductLibraryColumnMappingDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductLibraryColumnMappingId]),
                ProductLibraryId = (Int32)GetValue(dr[FieldNames.ProductLibraryColumnMappingProductLibraryId]),
                Source = (String)GetValue(dr[FieldNames.ProductLibraryColumnMappingSource]),
                Destination = (String)GetValue(dr[FieldNames.ProductLibraryColumnMappingDestination]),
                IsMandatory = (Boolean)GetValue(dr[FieldNames.ProductLibraryColumnMappingIsMandatory]),
                Type = (String)GetValue(dr[FieldNames.ProductLibraryColumnMappingType]),
                Min = (Single)GetValue(dr[FieldNames.ProductLibraryColumnMappingMin]),
                Max = (Single)GetValue(dr[FieldNames.ProductLibraryColumnMappingMax]),
                CanDefault = (Boolean)GetValue(dr[FieldNames.ProductLibraryColumnMappingCanDefault]),
                Default = (String)GetValue(dr[FieldNames.ProductLibraryColumnMappingDefault]),
                TextFixedWidthColumnStart = (Int32)GetValue(dr[FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnStart]),
                TextFixedWidthColumnEnd = (Int32)GetValue(dr[FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnEnd]),
            };
        }

        #endregion

        #region Fetch


        public ProductLibraryColumnMappingDto FetchById(Int32 id)
        {
            ProductLibraryColumnMappingDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryColumnMappingFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new Galleria.Framework.Dal.DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<ProductLibraryColumnMappingDto> FetchByProductLibraryId(Object id)
        {
            List<ProductLibraryColumnMappingDto> dtoList = new List<ProductLibraryColumnMappingDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryColumnMappingFetchByProductLibraryId))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingProductLibraryId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(ProductLibraryColumnMappingDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryColumnMappingInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.ProductLibraryColumnMappingId, SqlDbType.Int);

                    //Other properties
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingProductLibraryId, SqlDbType.Int, dto.ProductLibraryId);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingSource, SqlDbType.NVarChar, dto.Source);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingDestination, SqlDbType.NVarChar, dto.Destination);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingIsMandatory, SqlDbType.Bit, dto.IsMandatory);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingType, SqlDbType.NVarChar, dto.Type);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingMin, SqlDbType.Real, dto.Min);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingMax, SqlDbType.Real, dto.Max);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingCanDefault, SqlDbType.Bit, dto.CanDefault);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingDefault, SqlDbType.NVarChar, dto.Default);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnStart, SqlDbType.Int, dto.TextFixedWidthColumnStart);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnEnd, SqlDbType.Int, dto.TextFixedWidthColumnEnd);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(ProductLibraryColumnMappingDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryColumnMappingUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingId, SqlDbType.Int, dto.Id);

                    //Other properties
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingProductLibraryId, SqlDbType.Int, dto.ProductLibraryId);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingSource, SqlDbType.NVarChar, dto.Source);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingDestination, SqlDbType.NVarChar, dto.Destination);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingIsMandatory, SqlDbType.Bit, dto.IsMandatory);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingType, SqlDbType.NVarChar, dto.Type);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingMin, SqlDbType.Real, dto.Min);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingMax, SqlDbType.Real, dto.Max);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingCanDefault, SqlDbType.Bit, dto.CanDefault);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingDefault, SqlDbType.NVarChar, dto.Default);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnStart, SqlDbType.Int, dto.TextFixedWidthColumnStart);
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnEnd, SqlDbType.Int, dto.TextFixedWidthColumnEnd);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    // Nothing to update.
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryColumnMappingDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLibraryColumnMappingId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}
