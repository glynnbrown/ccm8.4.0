﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//		Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class RoleEntityDal : Galleria.Framework.Dal.Mssql.DalBase, IRoleEntityDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        private RoleEntityDto GetDataTransferObject(SqlDataReader dr)
        {
            return new RoleEntityDto
            {
                Id = (Int32)GetValue(dr[FieldNames.RoleEntityId]),
                RoleId = (Int32)GetValue(dr[FieldNames.RoleEntityRoleId]),
                EntityId = (Int32)GetValue(dr[FieldNames.RoleEntity_EntityId])
            };
        }

        #endregion


        #region Fetch

        public RoleEntityDto FetchById(Int32 roleId, Int32 entityId)
        {
            RoleEntityDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleEntityFetchById))
                {
                    CreateParameter(command, FieldNames.RoleEntityRoleId, SqlDbType.Int, roleId);
                    CreateParameter(command, FieldNames.RoleEntity_EntityId, SqlDbType.Int, entityId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<RoleEntityDto> FetchByRoleId(Int32 roleId)
        {
            List<RoleEntityDto> dtoList = new List<RoleEntityDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleEntityFetchByRoleId))
                {
                    CreateParameter(command, FieldNames.RoleEntityRoleId, SqlDbType.Int, roleId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public IEnumerable<RoleEntityDto> FetchByEntityId(Int32 entityId)
        {
            List<RoleEntityDto> dtoList = new List<RoleEntityDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleEntityFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.RoleEntity_EntityId, SqlDbType.Int, entityId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public IEnumerable<RoleEntityDto> FetchAll()
        {
            List<RoleEntityDto> dtoList = new List<RoleEntityDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleEntityFetchAll))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert

        public void Insert(RoleEntityDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleEntityInsert))
                {
                    //Parameters to store the returned Id and rowversion
                    SqlParameter idParameter = CreateParameter(command, FieldNames.RoleEntityId, SqlDbType.Int);

                    CreateParameter(command, FieldNames.RoleEntityRoleId, SqlDbType.Int, dto.RoleId);
                    CreateParameter(command, FieldNames.RoleEntity_EntityId, SqlDbType.Int, dto.EntityId);

                    //Execute the insert
                    command.ExecuteNonQuery();

                    //Update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleEntityDelete))
                {
                    CreateParameter(command, FieldNames.RoleEntityId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}
