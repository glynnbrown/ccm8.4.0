﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion

#region Version History: (CCM 8.2.0)
// V8-30802 : M.Shelley
//  Lowered the default SQL command timeout to prevent unresponsive behaviour
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class ProductGroupDal : Galleria.Framework.Dal.Mssql.DalBase, IProductGroupDal
    {
        #region Constants

        private const String _importTableName = "#tmpProductGroup";

        private const String _createImportTableSql =
           "CREATE TABLE [{0}](" +
           "[ProductHierarchy_Id] [INT] NOT NULL," +
           "[ProductLevel_Id] [INT] NOT NULL," +
           "[ProductGroup_Name] [NVARCHAR](100) COLLATE database_default NOT NULL," +
           "[ProductGroup_Code] [NVARCHAR](50) COLLATE database_default NULL," +
           "[ProductGroup_ParentGroupId] [INT] NULL" +
           ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        private const int TimeoutLimit = 10;

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class ProductGroupBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<ProductGroupDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ProductGroupBulkCopyDataReader(IEnumerable<ProductGroupDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 5; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.ProductHierarchyId;
                        break;
                    case 1:
                        value = _enumerator.Current.ProductLevelId;
                        break;
                    case 2:
                        value = _enumerator.Current.Name;
                        break;
                    case 3:
                        value = _enumerator.Current.Code;
                        break;
                    case 4:
                        value = _enumerator.Current.ParentGroupId;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private ProductGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductGroupDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductGroupId]),
                ProductHierarchyId = (Int32)GetValue(dr[FieldNames.ProductHierarchyId]),
                ProductLevelId = (Int32)GetValue(dr[FieldNames.ProductGroupProductLevelId]),
                Name = (String)GetValue(dr[FieldNames.ProductGroupName]),
                Code = (String)GetValue(dr[FieldNames.ProductGroupCode]),
                ParentGroupId = (Int32?)GetValue(dr[FieldNames.ProductGroupParentGroupId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ProductGroupDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ProductGroupDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.ProductGroupDateDeleted])
            };
        }
        #endregion

        #region Fetch

        public IEnumerable<ProductGroupDto> FetchByProductHierarchyId(Int32 hierarchyId)
        {
            List<ProductGroupDto> dtoList = new List<ProductGroupDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductGroupFetchByProductHierarchyId))
                {
                    CreateParameter(command, FieldNames.ProductHierarchyId, SqlDbType.Int, hierarchyId);
                    command.CommandTimeout = TimeoutLimit;

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public ProductGroupDto FetchById(Int32 id)
        {
            ProductGroupDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductGroupFetchById))
                {
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dto;
        }

        public IEnumerable<ProductGroupDto> FetchByProductHierarchyIdIncludingDeleted(int hierarchyId)
        {
            List<ProductGroupDto> dtoList = new List<ProductGroupDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductGroupFetchByProductHierarchyIdIncludingDeleted))
                {
                    CreateParameter(command, FieldNames.ProductHierarchyId, SqlDbType.Int, hierarchyId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ProductGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductGroupInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductGroupDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductGroupDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductGroupDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductGroupProductHierarchyId, SqlDbType.Int, dto.ProductHierarchyId);
                    CreateParameter(command, FieldNames.ProductGroupProductLevelId, SqlDbType.Int, dto.ProductLevelId);
                    CreateParameter(command, FieldNames.ProductGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductGroupCode, SqlDbType.NVarChar, dto.Code);
                    CreateParameter(command, FieldNames.ProductGroupParentGroupId, SqlDbType.Int, dto.ParentGroupId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ProductGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductGroupUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, dto.Id);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductGroupDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductGroupDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductGroupDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductGroupProductHierarchyId, SqlDbType.Int, dto.ProductHierarchyId);
                    CreateParameter(command, FieldNames.ProductGroupProductLevelId, SqlDbType.Int, dto.ProductLevelId);
                    CreateParameter(command, FieldNames.ProductGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductGroupCode, SqlDbType.NVarChar, dto.Code);
                    CreateParameter(command, FieldNames.ProductGroupParentGroupId, SqlDbType.Int, dto.ParentGroupId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }


        #endregion

        #region Upsert

        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<ProductGroupDto> dtoList, ProductGroupIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new ProductGroupIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<ProductGroupDtoKey, ProductGroupDto> keyedDtoList = new Dictionary<ProductGroupDtoKey, ProductGroupDto>();
            foreach (ProductGroupDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    ProductGroupBulkCopyDataReader dr = new ProductGroupBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<ProductGroupDtoKey, Int32> ids = new Dictionary<ProductGroupDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.ProductGroupBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.ProductGroupSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Int32 outputDataId = (Int32)GetValue(dr[FieldNames.ProductGroupId]);

                            ProductGroupDtoKey key = new ProductGroupDtoKey();
                            key.ProductLevelId = (Int32)GetValue(dr[FieldNames.ProductGroupProductLevelId]);
                            key.Code = (String)GetValue(dr[FieldNames.ProductGroupCode]);
                            ids[key] = outputDataId;
                        }
                    }
                }

                foreach (ProductGroupDto dto in dtoList)
                {
                    Int32 outputDataId;

                    if (ids.TryGetValue(dto.DtoKey, out outputDataId))
                    {
                        dto.Id = outputDataId;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private String GenerateSetPropertiesString(ProductGroupIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsProductHierarchyIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ProductGroupProductHierarchyId);
            }
            if (isSetDto.IsProductLevelIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ProductGroupProductLevelId);
            }
            if (isSetDto.IsNameSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ProductGroupName);
            }
            if (isSetDto.IsCodeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ProductGroupCode);
            }
            if (isSetDto.IsParentGroupIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ProductGroupParentGroupId);
            }

            return setProperties;
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductGroupDeleteById))
                {
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}
