﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version.
// V8-25916 : N.Foster
//  Added RowVersion, DateDeleted to package
// V8-25787 : N.Foster
//  Removed DeleteById functionality
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26709 : L.Ineson
//  Removed old unit of measure property.
// V8-26891 : L.Ineson
// Updated basic insert and update used by dal tests to pick up package details.
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties
//  Extended for DateMetadataCalculated
// V8-27237 : A.Silva   ~ Added DateValidationDataCalculated.
// V8-24779 : D.Pleasance
//  Added Planogram.SourcePath
// V8-28448 : L.ineson
//  Fixed issues with basic insert used by unit testing.
#endregion
#region Version History: CCM802
// V8-28987 : I.George
// Added LocationCode, LocationName, ClusterSchemeName and ClusterName fields 
#endregion
#region Version History: CCM803
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-28242 : M.Shelley
//  Added Status, DateWIP, DateApproved, DateArchived 
#endregion
#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30036 : L.Ineson
//  Added MetaPercentOfProductNotAchievedInventory
// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents
#endregion
#region Version History: CCM811
// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>
// V8-30561 : N.Foster
//  Ensured data last modified is set to parent package last modified date when planogram updated
#endregion
#region Version History : CCM820
// V8-30728 : A.Kuszyk
//  Added HighlightSequenceStrategy field.
// V8-30705 : A.Probyn
//  Extended for new assortment rule related meta data properties
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
#endregion
#region Version History : CCM830
// V8-32396 : A.Probyn
// Added MetaNumberOfDelistFamilyRulesBroken
// V8-32814 : M.Pettit
//  Added MetaCountOfProductsBuddied
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// The MSSQl DAL for the Planogram DTO.
    /// </summary>
    public class PlanogramDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new Planogram DTO from this data reader.
        /// </summary>
        /// <param name="dr">The data reader to load from.</param>
        /// <returns>A new Planogram DTO.</returns>
        private PlanogramDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramId]),
                PackageId = (Int32)GetValue(dr[FieldNames.PlanogramId]),
                UniqueContentReference = GetValue(dr[FieldNames.PlanogramUniqueContentReference]).ToString(),
                Name = (String)GetValue(dr[FieldNames.PlanogramName]),
                Height = (Single)GetValue(dr[FieldNames.PlanogramHeight]),
                Width = (Single)GetValue(dr[FieldNames.PlanogramWidth]),
                Depth = (Single)GetValue(dr[FieldNames.PlanogramDepth]),
                UserName = (String)GetValue(dr[FieldNames.PlanogramUserName]),
                LengthUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramLengthUnitsOfMeasure]),
                AreaUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramAreaUnitsOfMeasure]),
                VolumeUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramVolumeUnitsOfMeasure]),
                WeightUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramWeightUnitsOfMeasure]),
                AngleUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramAngleUnitsOfMeasure]),
                CurrencyUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramCurrencyUnitsOfMeasure]),
                ProductPlacementX = (Byte)GetValue(dr[FieldNames.PlanogramProductPlacementX]),
                ProductPlacementY = (Byte)GetValue(dr[FieldNames.PlanogramProductPlacementY]),
                ProductPlacementZ = (Byte)GetValue(dr[FieldNames.PlanogramProductPlacementZ]),
                CategoryCode = (String)GetValue(dr[FieldNames.PlanogramCategoryCode]),
                CategoryName = (String)GetValue(dr[FieldNames.PlanogramCategoryName]),
                SourcePath = (String)GetValue(dr[FieldNames.PlanogramSourcePath]),
                MetaBayCount = (Int32?)GetValue(dr[FieldNames.PlanogramMetaBayCount]),
                MetaUniqueProductCount = (Int32?)GetValue(dr[FieldNames.PlanogramMetaUniqueProductCount]),
                MetaComponentCount = (Int32?)GetValue(dr[FieldNames.PlanogramMetaComponentCount]),
                MetaTotalMerchandisableLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaTotalMerchandisableLinearSpace]),
                MetaTotalMerchandisableAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaTotalMerchandisableAreaSpace]),
                MetaTotalMerchandisableVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaTotalMerchandisableVolumetricSpace]),
                MetaTotalLinearWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaTotalLinearWhiteSpace]),
                MetaTotalAreaWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaTotalAreaWhiteSpace]),
                MetaTotalVolumetricWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaTotalVolumetricWhiteSpace]),
                MetaProductsPlaced = (Int32?)GetValue(dr[FieldNames.PlanogramMetaProductsPlaced]),
                MetaProductsUnplaced = (Int32?)GetValue(dr[FieldNames.PlanogramMetaProductsUnplaced]),
                MetaNewProducts = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNewProducts]),
                MetaChangesFromPreviousCount = (Int32?)GetValue(dr[FieldNames.PlanogramMetaChangesFromPreviousCount]),
                MetaChangeFromPreviousStarRating = (Int32?)GetValue(dr[FieldNames.PlanogramMetaChangeFromPreviousStarRating]),
                MetaBlocksDropped = (Int32?)GetValue(dr[FieldNames.PlanogramMetaBlocksDropped]),
                MetaNotAchievedInventory = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNotAchievedInventory]),
                MetaTotalFacings = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalFacings]),
                MetaAverageFacings = (Single?)GetValue(dr[FieldNames.PlanogramMetaAverageFacings]),
                MetaTotalUnits = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalUnits]),
                MetaAverageUnits = (Single?)GetValue(dr[FieldNames.PlanogramMetaAverageUnits]),
                MetaMinDos = (Single?)GetValue(dr[FieldNames.PlanogramMetaMinDos]),
                MetaMaxDos = (Single?)GetValue(dr[FieldNames.PlanogramMetaMaxDos]),
                MetaAverageDos = (Single?)GetValue(dr[FieldNames.PlanogramMetaAverageDos]),
                MetaMinCases = (Single?)GetValue(dr[FieldNames.PlanogramMetaMinCases]),
                MetaAverageCases = (Single?)GetValue(dr[FieldNames.PlanogramMetaAverageCases]),
                MetaMaxCases = (Single?)GetValue(dr[FieldNames.PlanogramMetaMaxCases]),
                MetaTotalComponentsOverMerchandisedWidth = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalComponentsOverMerchandisedWidth]),
                MetaHasComponentsOutsideOfFixtureArea = (Boolean?)GetValue(dr[FieldNames.PlanogramMetaHasComponentsOutsideOfFixtureArea]),
                MetaTotalComponentsOverMerchandisedHeight = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalComponentsOverMerchandisedHeight]),
                MetaTotalComponentsOverMerchandisedDepth = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalComponentsOverMerchandisedDepth]),
                MetaTotalComponentCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalComponentCollisions]),
                MetaTotalPositionCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalPositionCollisions]),
                MetaTotalFrontFacings = (Int16?)GetValue(dr[FieldNames.PlanogramMetaTotalFrontFacings]),
                MetaAverageFrontFacings = (Single?)GetValue(dr[FieldNames.PlanogramMetaAverageFrontFacings]),
                LocationCode = (String)GetValue(dr[FieldNames.PlanogramLocationCode]),
                LocationName = (String)GetValue(dr[FieldNames.PlanogramLocationName]),
                ClusterSchemeName = (String)GetValue(dr[FieldNames.PlanogramClusterSchemeName]),
                ClusterName = (String)GetValue(dr[FieldNames.PlanogramClusterName]),
                MetaNoOfErrors = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNoOfErrors]),
                MetaNoOfWarnings = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNoOfWarnings]),
                MetaTotalErrorScore = (Int32?)GetValue(dr[FieldNames.PlanogramMetaTotalErrorScore]),
                MetaHighestErrorScore = (Byte?)GetValue(dr[FieldNames.PlanogramMetaHighestErrorScore]),
                Status = (Byte)GetValue(dr[FieldNames.PlanogramStatus]),
                DateWip = (DateTime?)GetValue(dr[FieldNames.PlanogramDateWip]),
                DateApproved = (DateTime?)GetValue(dr[FieldNames.PlanogramDateApproved]),
                DateArchived = (DateTime?)GetValue(dr[FieldNames.PlanogramDateArchived]),
                MetaCountOfProductNotAchievedCases = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductNotAchievedCases]),
                MetaCountOfProductNotAchievedDOS = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductNotAchievedDOS]),
                MetaCountOfProductOverShelfLifePercent = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductOverShelfLifePercent]),
                MetaCountOfProductNotAchievedDeliveries = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductNotAchievedDeliveries]),
                MetaPercentOfProductNotAchievedCases = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedCases]),
                MetaPercentOfProductNotAchievedDOS = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedDOS]),
                MetaPercentOfProductOverShelfLifePercent = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductOverShelfLifePercent]),
                MetaPercentOfProductNotAchievedDeliveries = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedDeliveries]),
                MetaCountOfPositionsOutsideOfBlockSpace = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfPositionsOutsideOfBlockSpace]),
                MetaPercentOfPositionsOutsideOfBlockSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfPositionsOutsideOfBlockSpace]),
                MetaPercentOfPlacedProductsRecommendedInAssortment = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment]),
                MetaCountOfPlacedProductsRecommendedInAssortment = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfPlacedProductsRecommendedInAssortment]),
                MetaCountOfPlacedProductsNotRecommendedInAssortment = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment]),
                MetaCountOfRecommendedProductsInAssortment = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfRecommendedProductsInAssortment]),
                PlanogramType = (Byte)GetValue(dr[FieldNames.PlanogramPlanogramType]),
                MetaPercentOfProductNotAchievedInventory = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedInventory]),
                MetaNoOfDebugEvents = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNoOfDebugEvents]),
                MetaNoOfInformationEvents = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNoOfInformationEvents]),
                HighlightSequenceStrategy = (String)GetValue(dr[FieldNames.PlanogramHighlightSequenceStrategy]),
                MetaNumberOfAssortmentRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfAssortmentRulesBroken]),
                MetaNumberOfProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfProductRulesBroken]),
                MetaNumberOfFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfFamilyRulesBroken]),
                MetaNumberOfInheritanceRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfInheritanceRulesBroken]),
                MetaNumberOfLocalProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfLocalProductRulesBroken]),
                MetaNumberOfDistributionRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDistributionRulesBroken]),
                MetaNumberOfCoreRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfCoreRulesBroken]),
                MetaPercentageOfAssortmentRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfAssortmentRulesBroken]),
                MetaPercentageOfProductRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfProductRulesBroken]),
                MetaPercentageOfFamilyRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfFamilyRulesBroken]),
                MetaPercentageOfInheritanceRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfInheritanceRulesBroken]),
                MetaPercentageOfLocalProductRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfLocalProductRulesBroken]),
                MetaPercentageOfDistributionRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfDistributionRulesBroken]),
                MetaPercentageOfCoreRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfCoreRulesBroken]),
                MetaNumberOfDelistProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDelistProductRulesBroken]),
                MetaNumberOfForceProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfForceProductRulesBroken]),
                MetaNumberOfPreserveProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfPreserveProductRulesBroken]),
                MetaNumberOfMinimumHurdleProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfMinimumHurdleProductRulesBroken]),
                MetaNumberOfMaximumProductFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfMaximumProductFamilyRulesBroken]),
                MetaNumberOfMinimumProductFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfMinimumProductFamilyRulesBroken]),
                MetaNumberOfDependencyFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDependencyFamilyRulesBroken]),
                MetaNumberOfDelistFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDelistFamilyRulesBroken]),
                MetaCountOfProductsBuddied = (Int16?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductsBuddied]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the Planogram DTO with an Id that matches the given Package Id.
        /// </summary>
        /// <param name="packageId">The Id of the Planogram (not Package) to retrive.</param>
        /// <returns>The Planogram with and Id of the given Package Id.</returns>
        public IEnumerable<PlanogramDto> FetchByPackageId(Object packageId)
        {
            var dtoList = new List<PlanogramDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramFetchById))
                {
                    CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, (Int32)packageId);
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="packageDto">The package dto to insert</param>
        /// <param name="planogramDto">The planogram dto to insert</param>
        public void Insert(PackageDto packageDto, PlanogramDto planogramDto)
        {
            // this method actually maps to an update
            // remember that in the CCM database a package
            // and planogram are represented by the same record
            // so by the time we come to insert out planogram
            // record, the record already exists as it was
            // inserted by the package dal first
            this.Update(packageDto, planogramDto);
        }

        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        public void Insert(PlanogramDto planogramDto)
        {
            //This only exists for testing so insert using package dal first
            PackageDto packageDto = new PackageDto();
            packageDto.Id = planogramDto.Id;
            packageDto.Name = planogramDto.Name;
            packageDto.DateCreated = DateTime.UtcNow;
            packageDto.DateLastModified = DateTime.UtcNow;
            packageDto.DateMetadataCalculated = null;
            packageDto.DateValidationDataCalculated = null;

            //force the entity
            using (IEntityDal dal = base.DalContext.GetDal<IEntityDal>())
            {
                var entitys = dal.FetchAll();
                var entityDto = entitys.FirstOrDefault();
                if (entityDto == null)
                {
                    entityDto = new EntityDto();
                    entityDto.Name = "Entity";
                    dal.Insert(entityDto);
                }

                if (entityDto != null)
                {
                    packageDto.EntityId = entityDto.Id;
                }
            }

            //force the username
            using (IUserInfoDal dal = base.DalContext.GetDal<IUserInfoDal>())
            {
                var userInfos = dal.FetchAll();
                var userDto = userInfos.FirstOrDefault(u => u.UserName == planogramDto.UserName);
                if (userDto == null) userDto = userInfos.FirstOrDefault();
                if (userDto != null)
                {
                    planogramDto.UserName = userDto.UserName;
                    packageDto.UserName = planogramDto.UserName;
                }
            }

            using (IPackageDal dal = base.DalContext.GetDal<IPackageDal>())
            {
                dal.Insert(packageDto);
            }

            //force the guid
            Guid ucr;
            if (!Guid.TryParse(planogramDto.UniqueContentReference, out ucr))
            {
                planogramDto.UniqueContentReference = Guid.NewGuid().ToString();
            }

            this.Insert(packageDto, planogramDto);
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given Planogram DTO in the database.
        /// </summary>
        /// <param name="packageDto">The package dto to insert</param>
        /// <param name="planogramDto">The planogram dto to insert</param>
        public void Update(PackageDto packageDto, PlanogramDto planogramDto)
        {
            try
            {
                // update the planogram dto id as it 
                // always matches the package id
                if (packageDto.Id != null) planogramDto.Id = packageDto.Id;

                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramUpdateById))
                {
                    // parameters 
                    SqlParameter rowVersionParameter = 
                        CreateParameter(command, FieldNames.PlanogramRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, packageDto.RowVersion);
                    CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, planogramDto.Id);
                    CreateParameter(command, FieldNames.PlanogramUniqueContentReference, SqlDbType.UniqueIdentifier, Guid.Parse(planogramDto.UniqueContentReference));
                    CreateParameter(command, FieldNames.PlanogramName, SqlDbType.NVarChar, planogramDto.Name);
                    CreateParameter(command, FieldNames.PlanogramStatus, SqlDbType.TinyInt, planogramDto.Status);
                    CreateParameter(command, FieldNames.PlanogramHeight, SqlDbType.Real, planogramDto.Height);
                    CreateParameter(command, FieldNames.PlanogramWidth, SqlDbType.Real, planogramDto.Width);
                    CreateParameter(command, FieldNames.PlanogramDepth, SqlDbType.Real, planogramDto.Depth);
                    CreateParameter(command, FieldNames.PlanogramUserName, SqlDbType.NVarChar, planogramDto.UserName);
                    CreateParameter(command, FieldNames.PlanogramLengthUnitsOfMeasure, SqlDbType.TinyInt, planogramDto.LengthUnitsOfMeasure);
                    CreateParameter(command, FieldNames.PlanogramAreaUnitsOfMeasure, SqlDbType.TinyInt, planogramDto.AreaUnitsOfMeasure);
                    CreateParameter(command, FieldNames.PlanogramVolumeUnitsOfMeasure, SqlDbType.TinyInt, planogramDto.VolumeUnitsOfMeasure);
                    CreateParameter(command, FieldNames.PlanogramWeightUnitsOfMeasure, SqlDbType.TinyInt, planogramDto.WeightUnitsOfMeasure);
                    CreateParameter(command, FieldNames.PlanogramAngleUnitsOfMeasure, SqlDbType.TinyInt, planogramDto.AngleUnitsOfMeasure);
                    CreateParameter(command, FieldNames.PlanogramCurrencyUnitsOfMeasure, SqlDbType.TinyInt, planogramDto.CurrencyUnitsOfMeasure);
                    CreateParameter(command, FieldNames.PlanogramProductPlacementX, SqlDbType.TinyInt, planogramDto.ProductPlacementX);
                    CreateParameter(command, FieldNames.PlanogramProductPlacementY, SqlDbType.TinyInt, planogramDto.ProductPlacementY);
                    CreateParameter(command, FieldNames.PlanogramProductPlacementZ, SqlDbType.TinyInt, planogramDto.ProductPlacementZ);
                    CreateParameter(command, FieldNames.PlanogramCategoryCode, SqlDbType.NVarChar, planogramDto.CategoryCode);
                    CreateParameter(command, FieldNames.PlanogramCategoryName, SqlDbType.NVarChar, planogramDto.CategoryName);
                    CreateParameter(command, FieldNames.PlanogramSourcePath, SqlDbType.NVarChar, planogramDto.SourcePath);
                    CreateParameter(command, FieldNames.PlanogramMetaBayCount, SqlDbType.Int, planogramDto.MetaBayCount);
                    CreateParameter(command, FieldNames.PlanogramMetaUniqueProductCount, SqlDbType.Int, planogramDto.MetaUniqueProductCount);
                    CreateParameter(command, FieldNames.PlanogramMetaComponentCount, SqlDbType.Int, planogramDto.MetaComponentCount);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalMerchandisableLinearSpace, SqlDbType.Real, planogramDto.MetaTotalMerchandisableLinearSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalMerchandisableAreaSpace, SqlDbType.Real, planogramDto.MetaTotalMerchandisableAreaSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalMerchandisableVolumetricSpace, SqlDbType.Real, planogramDto.MetaTotalMerchandisableVolumetricSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalLinearWhiteSpace, SqlDbType.Real, planogramDto.MetaTotalLinearWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalAreaWhiteSpace, SqlDbType.Real, planogramDto.MetaTotalAreaWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalVolumetricWhiteSpace, SqlDbType.Real, planogramDto.MetaTotalVolumetricWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaProductsPlaced, SqlDbType.Int, planogramDto.MetaProductsPlaced);
                    CreateParameter(command, FieldNames.PlanogramMetaProductsUnplaced, SqlDbType.Int, planogramDto.MetaProductsUnplaced);
                    CreateParameter(command, FieldNames.PlanogramMetaNewProducts, SqlDbType.Int, planogramDto.MetaNewProducts);
                    CreateParameter(command, FieldNames.PlanogramMetaChangesFromPreviousCount, SqlDbType.Int, planogramDto.MetaChangesFromPreviousCount);
                    CreateParameter(command, FieldNames.PlanogramMetaChangeFromPreviousStarRating, SqlDbType.Int, planogramDto.MetaChangeFromPreviousStarRating);
                    CreateParameter(command, FieldNames.PlanogramMetaBlocksDropped, SqlDbType.Int, planogramDto.MetaBlocksDropped);
                    CreateParameter(command, FieldNames.PlanogramMetaNotAchievedInventory, SqlDbType.Int, planogramDto.MetaNotAchievedInventory);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalFacings, SqlDbType.Int, planogramDto.MetaTotalFacings);
                    CreateParameter(command, FieldNames.PlanogramMetaAverageFacings, SqlDbType.Real, planogramDto.MetaAverageFacings);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalUnits, SqlDbType.Int, planogramDto.MetaTotalUnits);
                    CreateParameter(command, FieldNames.PlanogramMetaAverageUnits, SqlDbType.Real, planogramDto.MetaAverageUnits);
                    CreateParameter(command, FieldNames.PlanogramMetaMinDos, SqlDbType.Real, planogramDto.MetaMinDos);
                    CreateParameter(command, FieldNames.PlanogramMetaMaxDos, SqlDbType.Real, planogramDto.MetaMaxDos);
                    CreateParameter(command, FieldNames.PlanogramMetaAverageDos, SqlDbType.Real, planogramDto.MetaAverageDos);
                    CreateParameter(command, FieldNames.PlanogramMetaMinCases, SqlDbType.Real, planogramDto.MetaMinCases);
                    CreateParameter(command, FieldNames.PlanogramMetaAverageCases, SqlDbType.Real, planogramDto.MetaAverageCases);
                    CreateParameter(command, FieldNames.PlanogramMetaMaxCases, SqlDbType.Real, planogramDto.MetaMaxCases);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalComponentsOverMerchandisedWidth, SqlDbType.Int, planogramDto.MetaTotalComponentsOverMerchandisedWidth);
                    CreateParameter(command, FieldNames.PlanogramMetaHasComponentsOutsideOfFixtureArea, SqlDbType.Bit, planogramDto.MetaHasComponentsOutsideOfFixtureArea);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalComponentsOverMerchandisedHeight, SqlDbType.Int, planogramDto.MetaTotalComponentsOverMerchandisedHeight);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalComponentsOverMerchandisedDepth, SqlDbType.Int, planogramDto.MetaTotalComponentsOverMerchandisedDepth);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalComponentCollisions, SqlDbType.Int, planogramDto.MetaTotalComponentCollisions);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalPositionCollisions, SqlDbType.Int, planogramDto.MetaTotalPositionCollisions);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalFrontFacings, SqlDbType.SmallInt, planogramDto.MetaTotalFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramMetaAverageFrontFacings, SqlDbType.Real, planogramDto.MetaAverageFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramLocationCode, SqlDbType.NVarChar, planogramDto.LocationCode);
                    CreateParameter(command, FieldNames.PlanogramLocationName, SqlDbType.NVarChar, planogramDto.LocationName);
                    CreateParameter(command, FieldNames.PlanogramClusterSchemeName, SqlDbType.NVarChar, planogramDto.ClusterSchemeName);
                    CreateParameter(command, FieldNames.PlanogramClusterName, SqlDbType.NVarChar, planogramDto.ClusterName);
                    CreateParameter(command, FieldNames.PlanogramMetaNoOfErrors, SqlDbType.Int, planogramDto.MetaNoOfErrors);
                    CreateParameter(command, FieldNames.PlanogramMetaNoOfWarnings, SqlDbType.Int, planogramDto.MetaNoOfWarnings);
                    CreateParameter(command, FieldNames.PlanogramMetaNoOfInformationEvents, SqlDbType.Int, planogramDto.MetaNoOfInformationEvents);
                    CreateParameter(command, FieldNames.PlanogramMetaNoOfDebugEvents, SqlDbType.Int, planogramDto.MetaNoOfDebugEvents);
                    CreateParameter(command, FieldNames.PlanogramMetaTotalErrorScore, SqlDbType.Int, planogramDto.MetaTotalErrorScore);
                    CreateParameter(command, FieldNames.PlanogramMetaHighestErrorScore, SqlDbType.TinyInt, planogramDto.MetaHighestErrorScore);
                    CreateParameter(command, FieldNames.PlanogramDateWip, SqlDbType.SmallDateTime, planogramDto.DateWip);
                    CreateParameter(command, FieldNames.PlanogramDateApproved, SqlDbType.SmallDateTime, planogramDto.DateApproved);
                    CreateParameter(command, FieldNames.PlanogramDateArchived, SqlDbType.SmallDateTime, planogramDto.DateArchived);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfProductNotAchievedCases, SqlDbType.Int, planogramDto.MetaCountOfProductNotAchievedCases);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfProductNotAchievedDOS, SqlDbType.Int, planogramDto.MetaCountOfProductNotAchievedDOS);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfProductOverShelfLifePercent, SqlDbType.Int, planogramDto.MetaCountOfProductOverShelfLifePercent);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfProductNotAchievedDeliveries, SqlDbType.Int, planogramDto.MetaCountOfProductNotAchievedDeliveries);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentOfProductNotAchievedCases, SqlDbType.Real, planogramDto.MetaPercentOfProductNotAchievedCases);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentOfProductNotAchievedDOS, SqlDbType.Real, planogramDto.MetaPercentOfProductNotAchievedDOS);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentOfProductOverShelfLifePercent, SqlDbType.Real, planogramDto.MetaPercentOfProductOverShelfLifePercent);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentOfProductNotAchievedDeliveries, SqlDbType.Real, planogramDto.MetaPercentOfProductNotAchievedDeliveries);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfPositionsOutsideOfBlockSpace, SqlDbType.Int, planogramDto.MetaCountOfPositionsOutsideOfBlockSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentOfPositionsOutsideOfBlockSpace, SqlDbType.Real, planogramDto.MetaPercentOfPositionsOutsideOfBlockSpace);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment, SqlDbType.Real, planogramDto.MetaPercentOfPlacedProductsRecommendedInAssortment);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfPlacedProductsRecommendedInAssortment, SqlDbType.Int, planogramDto.MetaCountOfPlacedProductsRecommendedInAssortment);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment, SqlDbType.Int, planogramDto.MetaCountOfPlacedProductsNotRecommendedInAssortment);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfRecommendedProductsInAssortment, SqlDbType.Int, planogramDto.MetaCountOfRecommendedProductsInAssortment);
                    CreateParameter(command, FieldNames.PlanogramPlanogramType, SqlDbType.TinyInt, planogramDto.PlanogramType);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentOfProductNotAchievedInventory, SqlDbType.Real, planogramDto.MetaPercentOfProductNotAchievedInventory);
                    CreateParameter(command, FieldNames.PlanogramDateLastModified, SqlDbType.SmallDateTime, packageDto.DateLastModified);
                    CreateParameter(command, FieldNames.PlanogramHighlightSequenceStrategy, SqlDbType.NVarChar, planogramDto.HighlightSequenceStrategy);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfAssortmentRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfAssortmentRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfProductRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfFamilyRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfFamilyRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfInheritanceRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfInheritanceRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfLocalProductRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfLocalProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfDistributionRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfDistributionRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfCoreRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfCoreRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentageOfAssortmentRulesBroken, SqlDbType.Real, planogramDto.MetaPercentageOfAssortmentRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentageOfProductRulesBroken, SqlDbType.Real, planogramDto.MetaPercentageOfProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentageOfFamilyRulesBroken, SqlDbType.Real, planogramDto.MetaPercentageOfFamilyRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentageOfInheritanceRulesBroken, SqlDbType.Real, planogramDto.MetaPercentageOfInheritanceRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentageOfLocalProductRulesBroken, SqlDbType.Real, planogramDto.MetaPercentageOfLocalProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentageOfDistributionRulesBroken, SqlDbType.Real, planogramDto.MetaPercentageOfDistributionRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaPercentageOfCoreRulesBroken, SqlDbType.Real, planogramDto.MetaPercentageOfCoreRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfDelistProductRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfDelistProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfForceProductRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfForceProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfPreserveProductRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfPreserveProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfMinimumHurdleProductRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfMinimumHurdleProductRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfMaximumProductFamilyRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfMaximumProductFamilyRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfMinimumProductFamilyRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfMinimumProductFamilyRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfDependencyFamilyRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfDependencyFamilyRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaNumberOfDelistFamilyRulesBroken, SqlDbType.SmallInt, planogramDto.MetaNumberOfDelistFamilyRulesBroken);
                    CreateParameter(command, FieldNames.PlanogramMetaCountOfProductsBuddied, SqlDbType.SmallInt, planogramDto.MetaCountOfProductsBuddied);

                    // execute
                    command.ExecuteNonQuery();

                    // update the package dto
                    if (packageDto != null)
                    {
                        packageDto.RowVersion = new RowVersion(rowVersionParameter.Value);
                        packageDto.Name = planogramDto.Name;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the given Planogram DTO in the database.
        /// </summary>
        public void Update(PlanogramDto planogramDto)
        {
            //This only exists for testing so just re-fetch the package dto.
            PackageDto packageDto = null;
            using (IPackageDal dal = base.DalContext.GetDal<IPackageDal>())
            {
                packageDto = dal.FetchById(planogramDto.Id);
            }


            this.Update(packageDto, planogramDto);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a Planogram from the database by it's Id value.
        /// </summary>
        /// <param name="id">The Id of the Planogram to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}

