﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//		Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class RoleMemberDal : Galleria.Framework.Dal.Mssql.DalBase, IRoleMemberDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        private RoleMemberDto GetDataTransferObject(SqlDataReader dr)
        {
            return new RoleMemberDto
            {
                Id = (Int32)GetValue(dr[FieldNames.RoleMemberId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.RoleMemberRowVersion])),
                UserId = (Int32)GetValue(dr[FieldNames.RoleMemberUserId]),
                RoleId = (Int32)GetValue(dr[FieldNames.RoleMemberRoleId])
            };
        }

        #endregion


        #region Fetch

        public RoleMemberDto FetchById(Int32 id)
        {
            RoleMemberDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleMemberFetchById))
                {
                    CreateParameter(command, FieldNames.RoleMemberId, SqlDbType.Int, id);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<RoleMemberDto> FetchByRoleId(Int32 roleId)
        {
            List<RoleMemberDto> dtoList = new List<RoleMemberDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleMemberFetchByRoleId))
                {
                    CreateParameter(command, FieldNames.RoleMemberRoleId, SqlDbType.Int, roleId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public IEnumerable<RoleMemberDto> FetchByUserId(Int32 userId)
        {
            List<RoleMemberDto> dtoList = new List<RoleMemberDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleMemberFetchByUserId))
                {
                    CreateParameter(command, FieldNames.RoleMemberUserId, SqlDbType.Int, userId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public IEnumerable<RoleMemberDto> FetchAll()
        {
            List<RoleMemberDto> dtoList = new List<RoleMemberDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleMemberFetchAll))
                {
                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(RoleMemberDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleMemberInsert))
                {
                    //Parameters to store the returned Id and rowversion
                    SqlParameter idParameter = CreateParameter(command, FieldNames.RoleMemberId, SqlDbType.Int);
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.RoleMemberRowVersion, SqlDbType.Timestamp);

                    CreateParameter(command, FieldNames.RoleMemberUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.RoleMemberRoleId, SqlDbType.Int, dto.RoleId);

                    //Execute the insert
                    command.ExecuteNonQuery();

                    //Update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(RoleMemberDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleMemberUpdate))
                {
                    SqlParameter versionParameter =
                        CreateParameter(command,
                        FieldNames.RoleMemberRowVersion,
                        SqlDbType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);

                    CreateParameter(command, FieldNames.RoleMemberId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.RoleMemberUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.RoleMemberRoleId, SqlDbType.Int, dto.RoleId);

                    //Execute the insert
                    command.ExecuteNonQuery();

                    //Update the dto
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleMemberDelete))
                {
                    CreateParameter(command, FieldNames.RoleMemberId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion


    }
}
