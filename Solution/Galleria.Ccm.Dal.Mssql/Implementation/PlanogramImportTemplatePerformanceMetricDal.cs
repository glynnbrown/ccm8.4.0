﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.0.3)
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramImportTemplatePerformanceMetric Dal
    /// </summary>
    public class PlanogramImportTemplatePerformanceMetricDal : DalBase, IPlanogramImportTemplatePerformanceMetricDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramImportTemplatePerformanceMetricDto GetDataTransferObject(IDataRecord dr)
        {
            return new PlanogramImportTemplatePerformanceMetricDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricId]),
                PlanogramImportTemplateId = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricPlanogramImportTemplateId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricName]),
                Description = (String)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricDescription]),
                Direction = (Byte)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricDirection]),
                SpecialType = (Byte)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricSpecialType]),
                MetricType = (Byte)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricMetricType]),
                MetricId = (Byte)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricMetricId]),
                ExternalField = (String)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricExternalField]),
                AggregationType = (Byte)GetValue(dr[FieldNames.PlanogramImportTemplatePerformanceMetricAggregationType])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramImportTemplatePerformanceMetricDto"/> items corresponding to the given <paramref name="planogramImportTemplateId"/>.
        /// </summary>
        /// <param name="planogramImportTemplateId">The unique id value for the Planogram Import Template containing the groups to be retrieved.</param>
        /// <returns>An instance implementing <see cref="IEnumerable{PlanogramImportTemplatePerformanceMetricDto}"/> with all the matching groups.</returns>
        public IEnumerable<PlanogramImportTemplatePerformanceMetricDto> FetchByPlanogramImportTemplateId(Object planogramImportTemplateId)
        {
            var dtoList = new List<PlanogramImportTemplatePerformanceMetricDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplatePerformanceMetricFetchByPlanogramImportTemplateId))
                {
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricPlanogramImportTemplateId, SqlDbType.Int,
                        (Int32) planogramImportTemplateId);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the data contained in the given <paramref name="dto"/> into the dal.
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramImportTemplatePerformanceMetricDto"/> containing the data to be persisted.</param>
        public void Insert(PlanogramImportTemplatePerformanceMetricDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplatePerformanceMetricInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricPlanogramImportTemplateId, SqlDbType.Int, dto.PlanogramImportTemplateId);                    
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricMetricType, SqlDbType.TinyInt, dto.MetricType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricMetricId, SqlDbType.TinyInt, dto.MetricId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricExternalField, SqlDbType.NVarChar, dto.ExternalField);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the data contained in the given <paramref name="dto"/> in the dal
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramImportTemplatePerformanceMetricDto"/> containing the data to be persisted.</param>
        public void Update(PlanogramImportTemplatePerformanceMetricDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplatePerformanceMetricUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricPlanogramImportTemplateId, SqlDbType.Int, dto.PlanogramImportTemplateId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricMetricType, SqlDbType.TinyInt, dto.MetricType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricMetricId, SqlDbType.TinyInt, dto.MetricId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricExternalField, SqlDbType.NVarChar, dto.ExternalField);
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the Planogram Import Template Performance Metric matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Unique id value to determine which Planogram Import Template Performance Metric to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplatePerformanceMetricDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImportTemplatePerformanceMetricId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}