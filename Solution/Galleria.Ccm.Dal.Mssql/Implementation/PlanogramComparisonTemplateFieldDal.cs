﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     MSSQL implementation of <see cref="IPlanogramComparisonTemplateFieldDal"/>.
    /// </summary>
    public class PlanogramComparisonTemplateFieldDal : DalBase, IPlanogramComparisonTemplateFieldDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Initialize a new <see cref="PlanogramComparisonTemplateFieldDto"/> from the given <see cref="IDataRecord"/>.
        /// </summary>
        /// <param name="source">The <see cref="IDataRecord"/> that contains the data to be used in the new item.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateFieldDto"/> with the data contained in the given <paramref name="source"/></returns>
        private static PlanogramComparisonTemplateFieldDto GetDataTransferObject(IDataRecord source)
        {
            return new PlanogramComparisonTemplateFieldDto
                   {
                       Id = (Int32)GetValue(source[FieldNames.PlanogramComparisonTemplateFieldId]),
                       PlanogramComparisonTemplateId = (Int32)GetValue(source[FieldNames.PlanogramComparisonTemplateFieldPlanogramComparisonTemplateId]),
                       ItemType = (Byte)GetValue(source[FieldNames.PlanogramComparisonTemplateFieldItemType]),
                       FieldPlaceholder = (String)GetValue(source[FieldNames.PlanogramComparisonTemplateFieldFieldPlaceholder]),
                       DisplayName = (String)GetValue(source[FieldNames.PlanogramComparisonTemplateFieldDisplayName]),
                       Number = (Int16)GetValue(source[FieldNames.PlanogramComparisonTemplateFieldNumber]),
                       Display = (Boolean)GetValue(source[FieldNames.PlanogramComparisonTemplateFieldDisplay]),
                   };
        }

        #endregion
        
        #region Fetch

        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Template Field</c> model object that belongs to a <c>Planogram Comparison Template</c> with the given<paramref name="planogramComparisonTemplateId"/>.
        /// </summary>
        /// <param name="planogramComparisonTemplateId"><see cref="Object"/> containing the id of the parent <c>Planogram Comparison Template</c>.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateFieldDto"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateFieldDto> FetchByPlanogramComparisonTemplateId(Object planogramComparisonTemplateId)
        {
            var dtos = new List<PlanogramComparisonTemplateFieldDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateFieldFetchByPlanogramComparisonTemplateId))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldPlanogramComparisonTemplateId, SqlDbType.Int, planogramComparisonTemplateId);

                    //  Read the data.
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }

            return dtos;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(PlanogramComparisonTemplateFieldDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateFieldInsert))
                {
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldId, SqlDbType.Int);
                    CreateOtherParameters(dto, command);

                    command.ExecuteNonQuery();

                    dto.Id = (Int32) idParameter.Value;
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        public void Update(PlanogramComparisonTemplateFieldDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateFieldUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldId, SqlDbType.Int, dto.Id);
                    CreateOtherParameters(dto, command);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the data linked to the given <paramref name="id"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateFieldDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldId, SqlDbType.Int, id);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        #endregion

        private void CreateOtherParameters(PlanogramComparisonTemplateFieldDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldPlanogramComparisonTemplateId, SqlDbType.Int, dto.PlanogramComparisonTemplateId);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldItemType, SqlDbType.TinyInt, dto.ItemType);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldFieldPlaceholder, SqlDbType.NVarChar, dto.FieldPlaceholder);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldDisplayName, SqlDbType.NVarChar, dto.DisplayName);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldNumber, SqlDbType.SmallInt, dto.Number);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateFieldDisplay, SqlDbType.Bit, dto.Display);
        }
    }
}