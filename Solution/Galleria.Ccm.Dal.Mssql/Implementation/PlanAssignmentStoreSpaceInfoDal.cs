#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM V8.0.0
// CCM-25879 : Martin Shelley
//		Created (Auto-generated)
#endregion

#region Version History: CCM V8.0.2
// V8-28986 : M.Shelley
//  Added FetchByEntityIdLocationId
#endregion

#region Version History: CCM V8.0.3
// V8-: M.Shelley
//  Added ClusterSchemeName and ClusterName 
#endregion

#region Version History: CCM V8.1.0
// V8-29598 : L.Luong
//  Added Height
#endregion

#region Version History: CCM 8.2.0
// V8-30802 : M.Shelley
//  Lowered the default SQL command timeout to prevent unresponsive behaviour
#endregion

#region Version History: CCM 8.4.0

// CCM-13408 : J.Mendes
// Added LocationSpaceProductGroup fields to the Planogram Assignment
// Changed the TimeoutLimit to the SQL default value of 30 seconds because occasionally I was getting timeout issues after we have added the new LocationSpaceProductGroup fields to the SPs

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanAssignmentStoreSpaceInfo Dal
    /// </summary>
    public class PlanAssignmentStoreSpaceInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanAssignmentStoreSpaceInfoDal
    {
        #region Fields

        private const int TimeoutLimit = 30;    // The SQL timeout in seconds

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// BayDimensions temporarily removed
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanAssignmentStoreSpaceInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanAssignmentStoreSpaceInfoDto()
            {
                LocationId = (Int16)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoLocationId]),
                LocationCode = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoLocationCode]),
                LocationName = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoLocationName]),
                LocationAddress = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoLocationAddress]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoProductGroupId]),
                ProductGroupCode = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoProductGroupCode]),
                ProductGroupName = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoProductGroupName]),
                BayTotalWidth = (Double)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfo_BayTotalWidth]),
                BayCount = (Int32)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoBayCount]),
                BayHeight = (Double)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoBayHeight]),
                ClusterSchemeName = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoClusterSchemeName]),
                ClusterName = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoClusterName]),
                ProductCount = (Int32)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoProductCount]),
                AverageBayWidth = (Double)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoAverageBayWidth]),
                AisleName = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoAisleName]),
                ValleyName = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoValleyName]),
                ZoneName = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoZoneName]),
                CustomAttribute01 = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoCustomAttribute01]),
                CustomAttribute02 = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoCustomAttribute02]),
                CustomAttribute03 = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoCustomAttribute03]),
                CustomAttribute04 = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoCustomAttribute04]),
                CustomAttribute05 = (String)GetValue(dr[FieldNames.PlanAssignmentStoreSpaceInfoCustomAttribute05]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos mtching the entity and product group id in the database
        /// </summary>
        /// <returns>A list of entity info dtos</returns>
        public IEnumerable<PlanAssignmentStoreSpaceInfoDto> FetchByEntityIdProductGroupId(Int32 entityId, Int32 productGroupId, Int32? clusterSchemeId)
        {
            List<PlanAssignmentStoreSpaceInfoDto> dtoList = new List<PlanAssignmentStoreSpaceInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanAssignmentStoreSpaceInfoFetchByEntityIdProductGroupId))
                {
                    CreateParameter(command, FieldNames.EntityId , SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, productGroupId);
                    CreateParameter(command, FieldNames.ClusterSchemeId, SqlDbType.Int, clusterSchemeId);

                    command.CommandTimeout = TimeoutLimit;

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns all dtos mtching the entity and location id in the database
        /// </summary>
        /// <returns>A list of entity info dtos</returns>
        public IEnumerable<PlanAssignmentStoreSpaceInfoDto> FetchByEntityIdLocationId(Int32 entityId, Int16 locationId, Int32? clusterSchemeId)
        {
            List<PlanAssignmentStoreSpaceInfoDto> dtoList = new List<PlanAssignmentStoreSpaceInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanAssignmentStoreSpaceInfoFetchByEntityIdLocationId))
                {
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.LocationId, SqlDbType.Int, locationId);
                    CreateParameter(command, FieldNames.ClusterSchemeId, SqlDbType.Int, clusterSchemeId);

                    command.CommandTimeout = TimeoutLimit;

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion
    }
}