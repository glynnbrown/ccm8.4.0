﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS. 
// V8-26091 : L.Ineson
//  Added additional FaceThickness properties
//  Removed FaceTopOffset and CanMultiMerchX,Y,Z
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM802
// V8-29023 : M.Pettit
//  Added Merchandisable Depth
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramSubComponentDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramSubComponentDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogramSubComponent";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[PlanogramSubComponent_Id] [int] NOT NULL, " +
                "[PlanogramComponent_Id] [int] NOT NULL, " +
                "[PlanogramSubComponent_Mesh3DId] [int] NULL, " +
                "[PlanogramSubComponent_ImageIdFront] [int] NULL, " +
                "[PlanogramSubComponent_ImageIdBack] [int] NULL, " +
                "[PlanogramSubComponent_ImageIdTop] [int] NULL, " +
                "[PlanogramSubComponent_ImageIdBottom] [int] NULL, " +
                "[PlanogramSubComponent_ImageIdLeft] [int] NULL, " +
                "[PlanogramSubComponent_ImageIdRight] [int] NULL, " +
                "[PlanogramSubComponent_Name] [nvarchar](50) COLLATE database_default NOT NULL, " +
                "[PlanogramSubComponent_Height] [real] NOT NULL, " +
                "[PlanogramSubComponent_Width] [real] NOT NULL, " +
                "[PlanogramSubComponent_Depth] [real] NOT NULL, " +
                "[PlanogramSubComponent_X] [real] NOT NULL, " +
                "[PlanogramSubComponent_Y] [real] NOT NULL, " +
                "[PlanogramSubComponent_Z] [real] NOT NULL, " +
                "[PlanogramSubComponent_Slope] [real] NOT NULL, " +
                "[PlanogramSubComponent_Angle] [real] NOT NULL, " +
                "[PlanogramSubComponent_Roll] [real] NOT NULL, " +
                "[PlanogramSubComponent_ShapeType] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_MerchandisableHeight] [real] NOT NULL, " +
                "[PlanogramSubComponent_IsVisible] [bit] NOT NULL, " +
                "[PlanogramSubComponent_HasCollisionDetection] [bit] NOT NULL, " +
                "[PlanogramSubComponent_FillPatternTypeFront] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_FillPatternTypeBack] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_FillPatternTypeTop] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_FillPatternTypeBottom] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_FillPatternTypeLeft] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_FillPatternTypeRight] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_FillColourFront] [int] NOT NULL, " +
                "[PlanogramSubComponent_FillColourBack] [int] NOT NULL, " +
                "[PlanogramSubComponent_FillColourTop] [int] NOT NULL, " +
                "[PlanogramSubComponent_FillColourBottom] [int] NOT NULL, " +
                "[PlanogramSubComponent_FillColourLeft] [int] NOT NULL, " +
                "[PlanogramSubComponent_FillColourRight] [int] NOT NULL, " +
                "[PlanogramSubComponent_LineColour] [int] NOT NULL, " +
                "[PlanogramSubComponent_TransparencyPercentFront] [int] NOT NULL, " +
                "[PlanogramSubComponent_TransparencyPercentBack] [int] NOT NULL, " +
                "[PlanogramSubComponent_TransparencyPercentTop] [int] NOT NULL, " +
                "[PlanogramSubComponent_TransparencyPercentBottom] [int] NOT NULL, " +
                "[PlanogramSubComponent_TransparencyPercentLeft] [int] NOT NULL, " +
                "[PlanogramSubComponent_TransparencyPercentRight] [int] NOT NULL, " +
                "[PlanogramSubComponent_FaceThicknessFront] [real] NOT NULL, " +
                "[PlanogramSubComponent_FaceThicknessBack] [real] NOT NULL, " +
                "[PlanogramSubComponent_FaceThicknessTop] [real] NOT NULL, " +
                "[PlanogramSubComponent_FaceThicknessBottom] [real] NOT NULL, " +
                "[PlanogramSubComponent_FaceThicknessLeft] [real] NOT NULL, " +
                "[PlanogramSubComponent_FaceThicknessRight] [real] NOT NULL, " +
                "[PlanogramSubComponent_RiserHeight] [real] NOT NULL, " +
                "[PlanogramSubComponent_RiserThickness] [real] NOT NULL, " +
                "[PlanogramSubComponent_IsRiserPlacedOnFront] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsRiserPlacedOnBack] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsRiserPlacedOnLeft] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsRiserPlacedOnRight] [bit] NOT NULL, " +
                "[PlanogramSubComponent_RiserFillPatternType] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_RiserColour] [int] NOT NULL, " +
                "[PlanogramSubComponent_RiserTransparencyPercent] [int] NOT NULL, " +
                "[PlanogramSubComponent_NotchStartX] [real] NOT NULL, " +
                "[PlanogramSubComponent_NotchSpacingX] [real] NOT NULL, " +
                "[PlanogramSubComponent_NotchStartY] [real] NOT NULL, " +
                "[PlanogramSubComponent_NotchSpacingY] [real] NOT NULL, " +
                "[PlanogramSubComponent_NotchHeight] [real] NOT NULL, " +
                "[PlanogramSubComponent_NotchWidth] [real] NOT NULL, " +
                "[PlanogramSubComponent_IsNotchPlacedOnFront] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsNotchPlacedOnBack] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsNotchPlacedOnLeft] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsNotchPlacedOnRight] [bit] NOT NULL, " +
                "[PlanogramSubComponent_NotchStyleType] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionHeight] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionWidth] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionDepth] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionStartX] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionSpacingX] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionStartY] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionSpacingY] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionStartZ] [real] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionSpacingZ] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow1StartX] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow1SpacingX] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow1StartY] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow1SpacingY] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow1Height] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow1Width] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow2StartX] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow2SpacingX] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow2StartY] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow2SpacingY] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow2Height] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchConstraintRow2Width] [real] NOT NULL, " +
                "[PlanogramSubComponent_LineThickness] [real] NOT NULL, " +
                "[PlanogramSubComponent_MerchandisingType] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_CombineType] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_IsProductOverlapAllowed] [bit] NOT NULL, " +
                "[PlanogramSubComponent_MerchandisingStrategyX] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_MerchandisingStrategyY] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_MerchandisingStrategyZ] [tinyint] NOT NULL, " +
                "[PlanogramSubComponent_LeftOverhang] [real] NOT NULL, " +
                "[PlanogramSubComponent_RightOverhang] [real] NOT NULL, " +
                "[PlanogramSubComponent_FrontOverhang] [real] NOT NULL, " +
                "[PlanogramSubComponent_BackOverhang] [real] NOT NULL, " +
                "[PlanogramSubComponent_TopOverhang] [real] NOT NULL, " +
                "[PlanogramSubComponent_BottomOverhang] [real] NOT NULL, " +
                "[PlanogramSubComponent_IsDividerObstructionAtStart] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsDividerObstructionAtEnd] [bit] NOT NULL, " +
                "[PlanogramSubComponent_IsDividerObstructionByFacing] [bit] NOT NULL, " +
                "[PlanogramSubComponent_MerchandisableDepth] [real] NOT NULL, " +
                "[PlanogramSubComponent_IsProductSqueezeAllowed] [BIT] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionFillColour] [INT] NOT NULL, " +
                "[PlanogramSubComponent_DividerObstructionFillPattern] [TINYINT] NOT NULL " +
            ")";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramSubComponentBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<PlanogramSubComponentDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramSubComponentBulkCopyDataReader(IEnumerable<PlanogramSubComponentDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 109; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.PlanogramComponentId;
                        break;
                    case 2:
                        value = _enumerator.Current.Mesh3DId;
                        break;
                    case 3:
                        value = _enumerator.Current.ImageIdFront;
                        break;
                    case 4:
                        value = _enumerator.Current.ImageIdBack;
                        break;
                    case 5:
                        value = _enumerator.Current.ImageIdTop;
                        break;
                    case 6:
                        value = _enumerator.Current.ImageIdBottom;
                        break;
                    case 7:
                        value = _enumerator.Current.ImageIdLeft;
                        break;
                    case 8:
                        value = _enumerator.Current.ImageIdRight;
                        break;
                    case 9:
                        value = _enumerator.Current.Name;
                        break;
                    case 10:
                        value = _enumerator.Current.Height;
                        break;
                    case 11:
                        value = _enumerator.Current.Width;
                        break;
                    case 12:
                        value = _enumerator.Current.Depth;
                        break;
                    case 13:
                        value = _enumerator.Current.X;
                        break;
                    case 14:
                        value = _enumerator.Current.Y;
                        break;
                    case 15:
                        value = _enumerator.Current.Z;
                        break;
                    case 16:
                        value = _enumerator.Current.Slope;
                        break;
                    case 17:
                        value = _enumerator.Current.Angle;
                        break;
                    case 18:
                        value = _enumerator.Current.Roll;
                        break;
                    case 19:
                        value = _enumerator.Current.ShapeType;
                        break;
                    case 20:
                        value = _enumerator.Current.MerchandisableHeight;
                        break;
                    case 21:
                        value = _enumerator.Current.IsVisible;
                        break;
                    case 22:
                        value = _enumerator.Current.HasCollisionDetection;
                        break;
                    case 23:
                        value = _enumerator.Current.FillPatternTypeFront;
                        break;
                    case 24:
                        value = _enumerator.Current.FillPatternTypeBack;
                        break;
                    case 25:
                        value = _enumerator.Current.FillPatternTypeTop;
                        break;
                    case 26:
                        value = _enumerator.Current.FillPatternTypeBottom;
                        break;
                    case 27:
                        value = _enumerator.Current.FillPatternTypeLeft;
                        break;
                    case 28:
                        value = _enumerator.Current.FillPatternTypeRight;
                        break;
                    case 29:
                        value = _enumerator.Current.FillColourFront;
                        break;
                    case 30:
                        value = _enumerator.Current.FillColourBack;
                        break;
                    case 31:
                        value = _enumerator.Current.FillColourTop;
                        break;
                    case 32:
                        value = _enumerator.Current.FillColourBottom;
                        break;
                    case 33:
                        value = _enumerator.Current.FillColourLeft;
                        break;
                    case 34:
                        value = _enumerator.Current.FillColourRight;
                        break;
                    case 35:
                        value = _enumerator.Current.LineColour;
                        break;
                    case 36:
                        value = _enumerator.Current.TransparencyPercentFront;
                        break;
                    case 37:
                        value = _enumerator.Current.TransparencyPercentBack;
                        break;
                    case 38:
                        value = _enumerator.Current.TransparencyPercentTop;
                        break;
                    case 39:
                        value = _enumerator.Current.TransparencyPercentBottom;
                        break;
                    case 40:
                        value = _enumerator.Current.TransparencyPercentLeft;
                        break;
                    case 41:
                        value = _enumerator.Current.TransparencyPercentRight;
                        break;
                    case 42:
                        value = _enumerator.Current.FaceThicknessFront;
                        break;
                    case 43:
                        value = _enumerator.Current.FaceThicknessBack;
                        break;
                    case 44:
                        value = _enumerator.Current.FaceThicknessTop;
                        break;
                    case 45:
                        value = _enumerator.Current.FaceThicknessBottom;
                        break;
                    case 46:
                        value = _enumerator.Current.FaceThicknessLeft;
                        break;
                    case 47:
                        value = _enumerator.Current.FaceThicknessRight;
                        break;
                    case 48:
                        value = _enumerator.Current.RiserHeight;
                        break;
                    case 49:
                        value = _enumerator.Current.RiserThickness;
                        break;
                    case 50:
                        value = _enumerator.Current.IsRiserPlacedOnFront;
                        break;
                    case 51:
                        value = _enumerator.Current.IsRiserPlacedOnBack;
                        break;
                    case 52:
                        value = _enumerator.Current.IsRiserPlacedOnLeft;
                        break;
                    case 53:
                        value = _enumerator.Current.IsRiserPlacedOnRight;
                        break;
                    case 54:
                        value = _enumerator.Current.RiserFillPatternType;
                        break;
                    case 55:
                        value = _enumerator.Current.RiserColour;
                        break;
                    case 56:
                        value = _enumerator.Current.RiserTransparencyPercent;
                        break;
                    case 57:
                        value = _enumerator.Current.NotchStartX;
                        break;
                    case 58:
                        value = _enumerator.Current.NotchSpacingX;
                        break;
                    case 59:
                        value = _enumerator.Current.NotchStartY;
                        break;
                    case 60:
                        value = _enumerator.Current.NotchSpacingY;
                        break;
                    case 61:
                        value = _enumerator.Current.NotchHeight;
                        break;
                    case 62:
                        value = _enumerator.Current.NotchWidth;
                        break;
                    case 63:
                        value = _enumerator.Current.IsNotchPlacedOnFront;
                        break;
                    case 64:
                        value = _enumerator.Current.IsNotchPlacedOnBack;
                        break;
                    case 65:
                        value = _enumerator.Current.IsNotchPlacedOnLeft;
                        break;
                    case 66:
                        value = _enumerator.Current.IsNotchPlacedOnRight;
                        break;
                    case 67:
                        value = _enumerator.Current.NotchStyleType;
                        break;
                    case 68:
                        value = _enumerator.Current.DividerObstructionHeight;
                        break;
                    case 69:
                        value = _enumerator.Current.DividerObstructionWidth;
                        break;
                    case 70:
                        value = _enumerator.Current.DividerObstructionDepth;
                        break;
                    case 71:
                        value = _enumerator.Current.DividerObstructionStartX;
                        break;
                    case 72:
                        value = _enumerator.Current.DividerObstructionSpacingX;
                        break;
                    case 73:
                        value = _enumerator.Current.DividerObstructionStartY;
                        break;
                    case 74:
                        value = _enumerator.Current.DividerObstructionSpacingY;
                        break;
                    case 75:
                        value = _enumerator.Current.DividerObstructionStartZ;
                        break;
                    case 76:
                        value = _enumerator.Current.DividerObstructionSpacingZ;
                        break;
                    case 77:
                        value = _enumerator.Current.MerchConstraintRow1StartX;
                        break;
                    case 78:
                        value = _enumerator.Current.MerchConstraintRow1SpacingX;
                        break;
                    case 79:
                        value = _enumerator.Current.MerchConstraintRow1StartY;
                        break;
                    case 80:
                        value = _enumerator.Current.MerchConstraintRow1SpacingY;
                        break;
                    case 81:
                        value = _enumerator.Current.MerchConstraintRow1Height;
                        break;
                    case 82:
                        value = _enumerator.Current.MerchConstraintRow1Width;
                        break;
                    case 83:
                        value = _enumerator.Current.MerchConstraintRow2StartX;
                        break;
                    case 84:
                        value = _enumerator.Current.MerchConstraintRow2SpacingX;
                        break;
                    case 85:
                        value = _enumerator.Current.MerchConstraintRow2StartY;
                        break;
                    case 86:
                        value = _enumerator.Current.MerchConstraintRow2SpacingY;
                        break;
                    case 87:
                        value = _enumerator.Current.MerchConstraintRow2Height;
                        break;
                    case 88:
                        value = _enumerator.Current.MerchConstraintRow2Width;
                        break;
                    case 89:
                        value = _enumerator.Current.LineThickness;
                        break;
                    case 90:
                        value = _enumerator.Current.MerchandisingType;
                        break;
                    case 91:
                        value = _enumerator.Current.CombineType;
                        break;
                    case 92:
                        value = _enumerator.Current.IsProductOverlapAllowed;
                        break;
                    case 93:
                        value = _enumerator.Current.MerchandisingStrategyX;
                        break;
                    case 94:
                        value = _enumerator.Current.MerchandisingStrategyY;
                        break;
                    case 95:
                        value = _enumerator.Current.MerchandisingStrategyZ;
                        break;
                    case 96:
                        value = _enumerator.Current.LeftOverhang;
                        break;
                    case 97:
                        value = _enumerator.Current.RightOverhang;
                        break;
                    case 98:
                        value = _enumerator.Current.FrontOverhang;
                        break;
                    case 99:
                        value = _enumerator.Current.BackOverhang;
                        break;
                    case 100:
                        value = _enumerator.Current.TopOverhang;
                        break;
                    case 101:
                        value = _enumerator.Current.BottomOverhang;
                        break;
                    case 102:
                        value = _enumerator.Current.IsDividerObstructionAtStart;
                        break;
                    case 103:
                        value = _enumerator.Current.IsDividerObstructionAtEnd;
                        break;
                    case 104:
                        value = _enumerator.Current.IsDividerObstructionByFacing;
                        break;
                    case 105:
                        value = _enumerator.Current.MerchandisableDepth;
                        break;
                    case 106:
                        value = _enumerator.Current.IsProductSqueezeAllowed;
                        break;
                    case 107:
                        value = _enumerator.Current.DividerObstructionFillColour;
                        break;
                    case 108:
                        value = _enumerator.Current.DividerObstructionFillPattern;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramSubComponentDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramSubComponentDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentId]),
                PlanogramComponentId = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentPlanogramComponentId]),
                Mesh3DId = (Int32?)GetValue(dr[FieldNames.PlanogramSubComponentMesh3DId]),
                ImageIdFront = (Int32?)GetValue(dr[FieldNames.PlanogramSubComponentImageIdFront]),
                ImageIdBack = (Int32?)GetValue(dr[FieldNames.PlanogramSubComponentImageIdBack]),
                ImageIdTop = (Int32?)GetValue(dr[FieldNames.PlanogramSubComponentImageIdTop]),
                ImageIdBottom = (Int32?)GetValue(dr[FieldNames.PlanogramSubComponentImageIdBottom]),
                ImageIdLeft = (Int32?)GetValue(dr[FieldNames.PlanogramSubComponentImageIdLeft]),
                ImageIdRight = (Int32?)GetValue(dr[FieldNames.PlanogramSubComponentImageIdRight]),
                Name = (String)GetValue(dr[FieldNames.PlanogramSubComponentName]),
                Height = (Single)GetValue(dr[FieldNames.PlanogramSubComponentHeight]),
                Width = (Single)GetValue(dr[FieldNames.PlanogramSubComponentWidth]),
                Depth = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDepth]),
                X = (Single)GetValue(dr[FieldNames.PlanogramSubComponentX]),
                Y = (Single)GetValue(dr[FieldNames.PlanogramSubComponentY]),
                Z = (Single)GetValue(dr[FieldNames.PlanogramSubComponentZ]),
                Slope = (Single)GetValue(dr[FieldNames.PlanogramSubComponentSlope]),
                Angle = (Single)GetValue(dr[FieldNames.PlanogramSubComponentAngle]),
                Roll = (Single)GetValue(dr[FieldNames.PlanogramSubComponentRoll]),
                ShapeType = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentShapeType]),
                MerchandisableHeight = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchandisableHeight]),
                MerchandisableDepth = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchandisableDepth]),
                IsVisible = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsVisible]),
                HasCollisionDetection = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentHasCollisionDetection]),
                FillPatternTypeFront = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentFillPatternTypeFront]),
                FillPatternTypeBack = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentFillPatternTypeBack]),
                FillPatternTypeTop = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentFillPatternTypeTop]),
                FillPatternTypeBottom = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentFillPatternTypeBottom]),
                FillPatternTypeLeft = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentFillPatternTypeLeft]),
                FillPatternTypeRight = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentFillPatternTypeRight]),
                FillColourFront = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentFillColourFront]),
                FillColourBack = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentFillColourBack]),
                FillColourTop = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentFillColourTop]),
                FillColourBottom = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentFillColourBottom]),
                FillColourLeft = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentFillColourLeft]),
                FillColourRight = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentFillColourRight]),
                LineColour = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentLineColour]),
                TransparencyPercentFront = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentTransparencyPercentFront]),
                TransparencyPercentBack = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentTransparencyPercentBack]),
                TransparencyPercentTop = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentTransparencyPercentTop]),
                TransparencyPercentBottom = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentTransparencyPercentBottom]),
                TransparencyPercentLeft = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentTransparencyPercentLeft]),
                TransparencyPercentRight = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentTransparencyPercentRight]),
                FaceThicknessFront = (Single)GetValue(dr[FieldNames.PlanogramSubComponentFaceThicknessFront]),
                FaceThicknessBack = (Single)GetValue(dr[FieldNames.PlanogramSubComponentFaceThicknessBack]),
                FaceThicknessTop = (Single)GetValue(dr[FieldNames.PlanogramSubComponentFaceThicknessTop]),
                FaceThicknessBottom = (Single)GetValue(dr[FieldNames.PlanogramSubComponentFaceThicknessBottom]),
                FaceThicknessLeft = (Single)GetValue(dr[FieldNames.PlanogramSubComponentFaceThicknessLeft]),
                FaceThicknessRight = (Single)GetValue(dr[FieldNames.PlanogramSubComponentFaceThicknessRight]),
                RiserHeight = (Single)GetValue(dr[FieldNames.PlanogramSubComponentRiserHeight]),
                RiserThickness = (Single)GetValue(dr[FieldNames.PlanogramSubComponentRiserThickness]),
                IsRiserPlacedOnFront = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsRiserPlacedOnFront]),
                IsRiserPlacedOnBack = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsRiserPlacedOnBack]),
                IsRiserPlacedOnLeft = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsRiserPlacedOnLeft]),
                IsRiserPlacedOnRight = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsRiserPlacedOnRight]),
                RiserFillPatternType = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentRiserFillPatternType]),
                RiserColour = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentRiserColour]),
                RiserTransparencyPercent = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentRiserTransparencyPercent]),
                NotchStartX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentNotchStartX]),
                NotchSpacingX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentNotchSpacingX]),
                NotchStartY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentNotchStartY]),
                NotchSpacingY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentNotchSpacingY]),
                NotchHeight = (Single)GetValue(dr[FieldNames.PlanogramSubComponentNotchHeight]),
                NotchWidth = (Single)GetValue(dr[FieldNames.PlanogramSubComponentNotchWidth]),
                IsNotchPlacedOnFront = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsNotchPlacedOnFront]),
                IsNotchPlacedOnBack = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsNotchPlacedOnBack]),
                IsNotchPlacedOnLeft = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsNotchPlacedOnLeft]),
                IsNotchPlacedOnRight = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsNotchPlacedOnRight]),
                NotchStyleType = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentNotchStyleType]),
                DividerObstructionHeight = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionHeight]),
                DividerObstructionWidth = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionWidth]),
                DividerObstructionDepth = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionDepth]),
                DividerObstructionStartX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionStartX]),
                DividerObstructionSpacingX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionSpacingX]),
                DividerObstructionStartY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionStartY]),
                DividerObstructionSpacingY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionSpacingY]),
                DividerObstructionStartZ = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionStartZ]),
                DividerObstructionSpacingZ = (Single)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionSpacingZ]),
                IsDividerObstructionAtStart = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsDividerObstructionAtStart]),
                IsDividerObstructionAtEnd = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsDividerObstructionAtEnd]),
                IsDividerObstructionByFacing = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsDividerObstructionByFacing]),
                DividerObstructionFillColour = (Int32)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionFillColour]),
                DividerObstructionFillPattern = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentDividerObstructionFillPattern]),
                MerchConstraintRow1StartX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow1StartX]),
                MerchConstraintRow1SpacingX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingX]),
                MerchConstraintRow1StartY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow1StartY]),
                MerchConstraintRow1SpacingY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingY]),
                MerchConstraintRow1Height = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow1Height]),
                MerchConstraintRow1Width = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow1Width]),
                MerchConstraintRow2StartX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow2StartX]),
                MerchConstraintRow2SpacingX = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingX]),
                MerchConstraintRow2StartY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow2StartY]),
                MerchConstraintRow2SpacingY = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingY]),
                MerchConstraintRow2Height = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow2Height]),
                MerchConstraintRow2Width = (Single)GetValue(dr[FieldNames.PlanogramSubComponentMerchConstraintRow2Width]),
                LineThickness = (Single)GetValue(dr[FieldNames.PlanogramSubComponentLineThickness]),
                MerchandisingType = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentMerchandisingType]),
                CombineType = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentCombineType]),
                IsProductOverlapAllowed = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsProductOverlapAllowed]),
                IsProductSqueezeAllowed = (Boolean)GetValue(dr[FieldNames.PlanogramSubComponentIsProductSqueezeAllowed]),
                MerchandisingStrategyX = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentMerchandisingStrategyX]),
                MerchandisingStrategyY = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentMerchandisingStrategyY]),
                MerchandisingStrategyZ = (Byte)GetValue(dr[FieldNames.PlanogramSubComponentMerchandisingStrategyZ]),
                LeftOverhang = (Single)GetValue(dr[FieldNames.PlanogramSubComponentLeftOverhang]),
                RightOverhang = (Single)GetValue(dr[FieldNames.PlanogramSubComponentRightOverhang]),
                FrontOverhang = (Single)GetValue(dr[FieldNames.PlanogramSubComponentFrontOverhang]),
                BackOverhang = (Single)GetValue(dr[FieldNames.PlanogramSubComponentBackOverhang]),
                TopOverhang = (Single)GetValue(dr[FieldNames.PlanogramSubComponentTopOverhang]),
                BottomOverhang = (Single)GetValue(dr[FieldNames.PlanogramSubComponentBottomOverhang]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramSubComponentDto> FetchByPlanogramComponentId(Object planogramComponentId)
        {
            List<PlanogramSubComponentDto> dtoList = new List<PlanogramSubComponentDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSubComponentFetchByPlanogramComponentId))
                {
                    CreateParameter(command, FieldNames.PlanogramSubComponentPlanogramComponentId, SqlDbType.Int, planogramComponentId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramSubComponentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSubComponentInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramSubComponentId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramSubComponentPlanogramComponentId, SqlDbType.Int, dto.PlanogramComponentId);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMesh3DId, SqlDbType.Int, dto.Mesh3DId);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdFront, SqlDbType.Int, dto.ImageIdFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdBack, SqlDbType.Int, dto.ImageIdBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdTop, SqlDbType.Int, dto.ImageIdTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdBottom, SqlDbType.Int, dto.ImageIdBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdLeft, SqlDbType.Int, dto.ImageIdLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdRight, SqlDbType.Int, dto.ImageIdRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramSubComponentHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.PlanogramSubComponentWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramSubComponentY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramSubComponentZ, SqlDbType.Real, dto.Z);
                    CreateParameter(command, FieldNames.PlanogramSubComponentSlope, SqlDbType.Real, dto.Slope);
                    CreateParameter(command, FieldNames.PlanogramSubComponentAngle, SqlDbType.Real, dto.Angle);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRoll, SqlDbType.Real, dto.Roll);
                    CreateParameter(command, FieldNames.PlanogramSubComponentShapeType, SqlDbType.TinyInt, dto.ShapeType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisableHeight, SqlDbType.Real, dto.MerchandisableHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisableDepth, SqlDbType.Real, dto.MerchandisableDepth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsVisible, SqlDbType.Bit, dto.IsVisible);
                    CreateParameter(command, FieldNames.PlanogramSubComponentHasCollisionDetection, SqlDbType.Bit, dto.HasCollisionDetection);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeFront, SqlDbType.TinyInt, dto.FillPatternTypeFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeBack, SqlDbType.TinyInt, dto.FillPatternTypeBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeTop, SqlDbType.TinyInt, dto.FillPatternTypeTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeBottom, SqlDbType.TinyInt, dto.FillPatternTypeBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeLeft, SqlDbType.TinyInt, dto.FillPatternTypeLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeRight, SqlDbType.TinyInt, dto.FillPatternTypeRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourFront, SqlDbType.Int, dto.FillColourFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourBack, SqlDbType.Int, dto.FillColourBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourTop, SqlDbType.Int, dto.FillColourTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourBottom, SqlDbType.Int, dto.FillColourBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourLeft, SqlDbType.Int, dto.FillColourLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourRight, SqlDbType.Int, dto.FillColourRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentLineColour, SqlDbType.Int, dto.LineColour);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentFront, SqlDbType.Int, dto.TransparencyPercentFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentBack, SqlDbType.Int, dto.TransparencyPercentBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentTop, SqlDbType.Int, dto.TransparencyPercentTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentBottom, SqlDbType.Int, dto.TransparencyPercentBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentLeft, SqlDbType.Int, dto.TransparencyPercentLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentRight, SqlDbType.Int, dto.TransparencyPercentRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessFront, SqlDbType.Real, dto.FaceThicknessFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessBack, SqlDbType.Real, dto.FaceThicknessBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessTop, SqlDbType.Real, dto.FaceThicknessTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessBottom, SqlDbType.Real, dto.FaceThicknessBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessLeft, SqlDbType.Real, dto.FaceThicknessLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessRight, SqlDbType.Real, dto.FaceThicknessRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserHeight, SqlDbType.Real, dto.RiserHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserThickness, SqlDbType.Real, dto.RiserThickness);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnFront, SqlDbType.Bit, dto.IsRiserPlacedOnFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnBack, SqlDbType.Bit, dto.IsRiserPlacedOnBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnLeft, SqlDbType.Bit, dto.IsRiserPlacedOnLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnRight, SqlDbType.Bit, dto.IsRiserPlacedOnRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserFillPatternType, SqlDbType.TinyInt, dto.RiserFillPatternType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserColour, SqlDbType.Int, dto.RiserColour);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserTransparencyPercent, SqlDbType.Int, dto.RiserTransparencyPercent);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchStartX, SqlDbType.Real, dto.NotchStartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchSpacingX, SqlDbType.Real, dto.NotchSpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchStartY, SqlDbType.Real, dto.NotchStartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchSpacingY, SqlDbType.Real, dto.NotchSpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchHeight, SqlDbType.Real, dto.NotchHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchWidth, SqlDbType.Real, dto.NotchWidth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnFront, SqlDbType.Bit, dto.IsNotchPlacedOnFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnBack, SqlDbType.Bit, dto.IsNotchPlacedOnBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnLeft, SqlDbType.Bit, dto.IsNotchPlacedOnLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnRight, SqlDbType.Bit, dto.IsNotchPlacedOnRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchStyleType, SqlDbType.TinyInt, dto.NotchStyleType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionHeight, SqlDbType.Real, dto.DividerObstructionHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionWidth, SqlDbType.Real, dto.DividerObstructionWidth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionDepth, SqlDbType.Real, dto.DividerObstructionDepth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionStartX, SqlDbType.Real, dto.DividerObstructionStartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionSpacingX, SqlDbType.Real, dto.DividerObstructionSpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionStartY, SqlDbType.Real, dto.DividerObstructionStartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionSpacingY, SqlDbType.Real, dto.DividerObstructionSpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionStartZ, SqlDbType.Real, dto.DividerObstructionStartZ);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionSpacingZ, SqlDbType.Real, dto.DividerObstructionSpacingZ);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsDividerObstructionAtStart, SqlDbType.Bit, dto.IsDividerObstructionAtStart);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsDividerObstructionAtEnd, SqlDbType.Bit, dto.IsDividerObstructionAtEnd);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsDividerObstructionByFacing, SqlDbType.Bit, dto.IsDividerObstructionByFacing);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1StartX, SqlDbType.Real, dto.MerchConstraintRow1StartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingX, SqlDbType.Real, dto.MerchConstraintRow1SpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1StartY, SqlDbType.Real, dto.MerchConstraintRow1StartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingY, SqlDbType.Real, dto.MerchConstraintRow1SpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1Height, SqlDbType.Real, dto.MerchConstraintRow1Height);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1Width, SqlDbType.Real, dto.MerchConstraintRow1Width);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2StartX, SqlDbType.Real, dto.MerchConstraintRow2StartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingX, SqlDbType.Real, dto.MerchConstraintRow2SpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2StartY, SqlDbType.Real, dto.MerchConstraintRow2StartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingY, SqlDbType.Real, dto.MerchConstraintRow2SpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2Height, SqlDbType.Real, dto.MerchConstraintRow2Height);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2Width, SqlDbType.Real, dto.MerchConstraintRow2Width);
                    CreateParameter(command, FieldNames.PlanogramSubComponentLineThickness, SqlDbType.Real, dto.LineThickness);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingType, SqlDbType.TinyInt, dto.MerchandisingType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentCombineType, SqlDbType.TinyInt, dto.CombineType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsProductOverlapAllowed, SqlDbType.Bit, dto.IsProductOverlapAllowed);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsProductSqueezeAllowed, SqlDbType.Real, dto.IsProductSqueezeAllowed);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingStrategyX, SqlDbType.TinyInt, dto.MerchandisingStrategyX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingStrategyY, SqlDbType.TinyInt, dto.MerchandisingStrategyY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingStrategyZ, SqlDbType.TinyInt, dto.MerchandisingStrategyZ);
                    CreateParameter(command, FieldNames.PlanogramSubComponentLeftOverhang, SqlDbType.Real, dto.LeftOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRightOverhang, SqlDbType.Real, dto.RightOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFrontOverhang, SqlDbType.Real, dto.FrontOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentBackOverhang, SqlDbType.Real, dto.BackOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTopOverhang, SqlDbType.Real, dto.TopOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentBottomOverhang, SqlDbType.Real, dto.BottomOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionFillColour, SqlDbType.Int, dto.DividerObstructionFillColour);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionFillPattern, SqlDbType.TinyInt, dto.DividerObstructionFillPattern);
                    

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, PlanogramSubComponentDto> index = new Dictionary<Object, PlanogramSubComponentDto>();
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramSubComponentBulkCopyDataReader dr = new PlanogramSubComponentBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSubComponentBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramSubComponentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSubComponentUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramSubComponentId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramSubComponentPlanogramComponentId, SqlDbType.Int, dto.PlanogramComponentId);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMesh3DId, SqlDbType.Int, dto.Mesh3DId);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdFront, SqlDbType.Int, dto.ImageIdFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdBack, SqlDbType.Int, dto.ImageIdBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdTop, SqlDbType.Int, dto.ImageIdTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdBottom, SqlDbType.Int, dto.ImageIdBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdLeft, SqlDbType.Int, dto.ImageIdLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentImageIdRight, SqlDbType.Int, dto.ImageIdRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramSubComponentHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.PlanogramSubComponentWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramSubComponentY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramSubComponentZ, SqlDbType.Real, dto.Z);
                    CreateParameter(command, FieldNames.PlanogramSubComponentSlope, SqlDbType.Real, dto.Slope);
                    CreateParameter(command, FieldNames.PlanogramSubComponentAngle, SqlDbType.Real, dto.Angle);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRoll, SqlDbType.Real, dto.Roll);
                    CreateParameter(command, FieldNames.PlanogramSubComponentShapeType, SqlDbType.TinyInt, dto.ShapeType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisableHeight, SqlDbType.Real, dto.MerchandisableHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisableDepth, SqlDbType.Real, dto.MerchandisableDepth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsVisible, SqlDbType.Bit, dto.IsVisible);
                    CreateParameter(command, FieldNames.PlanogramSubComponentHasCollisionDetection, SqlDbType.Bit, dto.HasCollisionDetection);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeFront, SqlDbType.TinyInt, dto.FillPatternTypeFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeBack, SqlDbType.TinyInt, dto.FillPatternTypeBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeTop, SqlDbType.TinyInt, dto.FillPatternTypeTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeBottom, SqlDbType.TinyInt, dto.FillPatternTypeBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeLeft, SqlDbType.TinyInt, dto.FillPatternTypeLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillPatternTypeRight, SqlDbType.TinyInt, dto.FillPatternTypeRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourFront, SqlDbType.Int, dto.FillColourFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourBack, SqlDbType.Int, dto.FillColourBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourTop, SqlDbType.Int, dto.FillColourTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourBottom, SqlDbType.Int, dto.FillColourBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourLeft, SqlDbType.Int, dto.FillColourLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFillColourRight, SqlDbType.Int, dto.FillColourRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentLineColour, SqlDbType.Int, dto.LineColour);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentFront, SqlDbType.Int, dto.TransparencyPercentFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentBack, SqlDbType.Int, dto.TransparencyPercentBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentTop, SqlDbType.Int, dto.TransparencyPercentTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentBottom, SqlDbType.Int, dto.TransparencyPercentBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentLeft, SqlDbType.Int, dto.TransparencyPercentLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTransparencyPercentRight, SqlDbType.Int, dto.TransparencyPercentRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessFront, SqlDbType.Real, dto.FaceThicknessFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessBack, SqlDbType.Real, dto.FaceThicknessBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessTop, SqlDbType.Real, dto.FaceThicknessTop);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessBottom, SqlDbType.Real, dto.FaceThicknessBottom);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessLeft, SqlDbType.Real, dto.FaceThicknessLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFaceThicknessRight, SqlDbType.Real, dto.FaceThicknessRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserHeight, SqlDbType.Real, dto.RiserHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserThickness, SqlDbType.Real, dto.RiserThickness);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnFront, SqlDbType.Bit, dto.IsRiserPlacedOnFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnBack, SqlDbType.Bit, dto.IsRiserPlacedOnBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnLeft, SqlDbType.Bit, dto.IsRiserPlacedOnLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsRiserPlacedOnRight, SqlDbType.Bit, dto.IsRiserPlacedOnRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserFillPatternType, SqlDbType.TinyInt, dto.RiserFillPatternType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserColour, SqlDbType.Int, dto.RiserColour);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRiserTransparencyPercent, SqlDbType.Int, dto.RiserTransparencyPercent);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchStartX, SqlDbType.Real, dto.NotchStartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchSpacingX, SqlDbType.Real, dto.NotchSpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchStartY, SqlDbType.Real, dto.NotchStartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchSpacingY, SqlDbType.Real, dto.NotchSpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchHeight, SqlDbType.Real, dto.NotchHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchWidth, SqlDbType.Real, dto.NotchWidth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnFront, SqlDbType.Bit, dto.IsNotchPlacedOnFront);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnBack, SqlDbType.Bit, dto.IsNotchPlacedOnBack);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnLeft, SqlDbType.Bit, dto.IsNotchPlacedOnLeft);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsNotchPlacedOnRight, SqlDbType.Bit, dto.IsNotchPlacedOnRight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentNotchStyleType, SqlDbType.TinyInt, dto.NotchStyleType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionHeight, SqlDbType.Real, dto.DividerObstructionHeight);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionWidth, SqlDbType.Real, dto.DividerObstructionWidth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionDepth, SqlDbType.Real, dto.DividerObstructionDepth);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionStartX, SqlDbType.Real, dto.DividerObstructionStartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionSpacingX, SqlDbType.Real, dto.DividerObstructionSpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionStartY, SqlDbType.Real, dto.DividerObstructionStartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionSpacingY, SqlDbType.Real, dto.DividerObstructionSpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionStartZ, SqlDbType.Real, dto.DividerObstructionStartZ);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionSpacingZ, SqlDbType.Real, dto.DividerObstructionSpacingZ);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsDividerObstructionAtStart, SqlDbType.Bit, dto.IsDividerObstructionAtStart);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsDividerObstructionAtEnd, SqlDbType.Bit, dto.IsDividerObstructionAtEnd);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsDividerObstructionByFacing, SqlDbType.Bit, dto.IsDividerObstructionByFacing);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1StartX, SqlDbType.Real, dto.MerchConstraintRow1StartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingX, SqlDbType.Real, dto.MerchConstraintRow1SpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1StartY, SqlDbType.Real, dto.MerchConstraintRow1StartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1SpacingY, SqlDbType.Real, dto.MerchConstraintRow1SpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1Height, SqlDbType.Real, dto.MerchConstraintRow1Height);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow1Width, SqlDbType.Real, dto.MerchConstraintRow1Width);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2StartX, SqlDbType.Real, dto.MerchConstraintRow2StartX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingX, SqlDbType.Real, dto.MerchConstraintRow2SpacingX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2StartY, SqlDbType.Real, dto.MerchConstraintRow2StartY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2SpacingY, SqlDbType.Real, dto.MerchConstraintRow2SpacingY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2Height, SqlDbType.Real, dto.MerchConstraintRow2Height);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchConstraintRow2Width, SqlDbType.Real, dto.MerchConstraintRow2Width);
                    CreateParameter(command, FieldNames.PlanogramSubComponentLineThickness, SqlDbType.Real, dto.LineThickness);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingType, SqlDbType.TinyInt, dto.MerchandisingType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentCombineType, SqlDbType.TinyInt, dto.CombineType);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsProductOverlapAllowed, SqlDbType.Bit, dto.IsProductOverlapAllowed);
                    CreateParameter(command, FieldNames.PlanogramSubComponentIsProductSqueezeAllowed, SqlDbType.Real, dto.IsProductSqueezeAllowed);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingStrategyX, SqlDbType.TinyInt, dto.MerchandisingStrategyX);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingStrategyY, SqlDbType.TinyInt, dto.MerchandisingStrategyY);
                    CreateParameter(command, FieldNames.PlanogramSubComponentMerchandisingStrategyZ, SqlDbType.TinyInt, dto.MerchandisingStrategyZ);
                    CreateParameter(command, FieldNames.PlanogramSubComponentLeftOverhang, SqlDbType.Real, dto.LeftOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentRightOverhang, SqlDbType.Real, dto.RightOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentFrontOverhang, SqlDbType.Real, dto.FrontOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentBackOverhang, SqlDbType.Real, dto.BackOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentTopOverhang, SqlDbType.Real, dto.TopOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentBottomOverhang, SqlDbType.Real, dto.BottomOverhang);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionFillColour, SqlDbType.Int, dto.DividerObstructionFillColour);
                    CreateParameter(command, FieldNames.PlanogramSubComponentDividerObstructionFillPattern, SqlDbType.TinyInt, dto.DividerObstructionFillPattern);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSubComponentDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramSubComponentId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramSubComponentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
