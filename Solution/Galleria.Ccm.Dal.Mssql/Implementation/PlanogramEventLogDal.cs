﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26836 : L.Luong 
//   Created (Auto-generated)
// V8-27404 : L.Luong
//   Changed LevelType to EntryType and added Content
// V8-27759 : A.Kuszyk
//  Added AffectedType and AffectedId fields.
// V8-28059 : A.Kuszyk
//  Added WorkpackageSource, TaskSource and EventId fields.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new Score property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramEventLog Dal
    /// </summary>
    public class PlanogramEventLogDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramEventLogDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramEventLogDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramEventLogDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramEventLogId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramEventLogPlanogramId]),
                EventType = (Byte)GetValue(dr[FieldNames.PlanogramEventLogEventType]),
                EntryType = (Byte)GetValue(dr[FieldNames.PlanogramEventLogEntryType]),
                AffectedType = (Byte?)GetValue(dr[FieldNames.PlanogramEventLogAffectedType]),
                AffectedId = (Int32?)GetValue(dr[FieldNames.PlanogramEventLogAffectedId]),
                WorkpackageSource = (String)GetValue(dr[FieldNames.PlanogramEventLogWorkpackageSource]),
                TaskSource = (String)GetValue(dr[FieldNames.PlanogramEventLogTaskSource]),
                EventId = (Int32)GetValue(dr[FieldNames.PlanogramEventLogEventId]),
                DateTime = (DateTime)GetValue(dr[FieldNames.PlanogramEventLogDateTime]),
                Description = (String)GetValue(dr[FieldNames.PlanogramEventLogDescription]),
                Content = (String)GetValue(dr[FieldNames.PlanogramEventLogContent]),
                Score = (Byte)GetValue(dr[FieldNames.PlanogramEventLogScore])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramEventLog DTOs with a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramEventLog DTOs.</returns>
        public IEnumerable<PlanogramEventLogDto> FetchByPlanogramId(object planogramId)
        {
            var dtoList = new List<PlanogramEventLogDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramEventLogFetchByPlanogramId))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramEventLogPlanogramId, SqlDbType.Int, (Int32)planogramId);

                    // Excute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramEventLogDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramEventLogInsert))
                {
                    // ID
                    var idParameter = CreateParameter(command, FieldNames.PlanogramEventLogId, SqlDbType.Int);

                    // Other Parameters
                    CreateParameter(command, FieldNames.PlanogramEventLogPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramEventLogEventType, SqlDbType.TinyInt, dto.EventType);
                    CreateParameter(command, FieldNames.PlanogramEventLogEntryType, SqlDbType.TinyInt, dto.EntryType);
                    CreateParameter(command, FieldNames.PlanogramEventLogAffectedType, SqlDbType.TinyInt, dto.AffectedType);
                    CreateParameter(command, FieldNames.PlanogramEventLogAffectedId, SqlDbType.Int, dto.AffectedId);
                    CreateParameter(command, FieldNames.PlanogramEventLogWorkpackageSource, SqlDbType.NVarChar, dto.WorkpackageSource);
                    CreateParameter(command, FieldNames.PlanogramEventLogTaskSource, SqlDbType.NVarChar, dto.TaskSource);
                    CreateParameter(command, FieldNames.PlanogramEventLogEventId, SqlDbType.Int, dto.EventId);
                    CreateParameter(command, FieldNames.PlanogramEventLogDateTime, SqlDbType.DateTime, dto.DateTime);
                    CreateParameter(command, FieldNames.PlanogramEventLogDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramEventLogContent, SqlDbType.NVarChar, dto.Content);
                    CreateParameter(command, FieldNames.PlanogramEventLogScore, SqlDbType.TinyInt, dto.Score);
                    // Excute
                    command.ExecuteNonQuery();

                    // Update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramEventLogDto> dtos)
        {
            foreach (PlanogramEventLogDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramEventLogDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramEventLogUpdateById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramEventLogId, SqlDbType.Int, dto.Id);

                    // Other parameter
                    CreateParameter(command, FieldNames.PlanogramEventLogPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramEventLogEventType, SqlDbType.TinyInt, dto.EventType);
                    CreateParameter(command, FieldNames.PlanogramEventLogEntryType, SqlDbType.TinyInt, dto.EntryType);
                    CreateParameter(command, FieldNames.PlanogramEventLogAffectedType, SqlDbType.TinyInt, dto.AffectedType);
                    CreateParameter(command, FieldNames.PlanogramEventLogAffectedId, SqlDbType.Int, dto.AffectedId);
                    CreateParameter(command, FieldNames.PlanogramEventLogWorkpackageSource, SqlDbType.NVarChar, dto.WorkpackageSource);
                    CreateParameter(command, FieldNames.PlanogramEventLogTaskSource, SqlDbType.NVarChar, dto.TaskSource);
                    CreateParameter(command, FieldNames.PlanogramEventLogEventId, SqlDbType.Int, dto.EventId);
                    CreateParameter(command, FieldNames.PlanogramEventLogDateTime, SqlDbType.DateTime, dto.DateTime);
                    CreateParameter(command, FieldNames.PlanogramEventLogDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramEventLogContent, SqlDbType.NVarChar, dto.Content);
                    CreateParameter(command, FieldNames.PlanogramEventLogScore, SqlDbType.TinyInt, dto.Score);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramEventLogDto> dtos)
        {
            foreach (PlanogramEventLogDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramEventLogDeleteById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramEventLogId, SqlDbType.Int, (Int32)id);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramEventLogDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramEventLogDto> dtos)
        {
            foreach (PlanogramEventLogDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion
    }
}
