﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// AssortmentMinorRevisionAmendDistribtionAction Dal
    /// </summary>
    public class AssortmentMinorRevisionAmendDistributionActionDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionAmendDistributionActionDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionAmendDistributionActionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionAmendDistributionActionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionId]),
                AssortmentMinorRevisionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionAssortmentMinorRevisionId]),
                ProductGtin = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionProductGtin]),
                ProductName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionProductName]),
                ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionProductId]),
                Priority = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionPriority]),
                Comments = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionComments])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionAmendDistributionActionDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionAmendDistributionActionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment id
        /// </summary>
        /// <param name="entityId">specified Assortment id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionAmendDistributionActionDto> FetchByAssortmentMinorRevisionId(Int32 AssortmentMinorRevisionId)
        {
            List<AssortmentMinorRevisionAmendDistributionActionDto> dtoList = new List<AssortmentMinorRevisionAmendDistributionActionDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionFetchByAssortmentMinorRevisionId))
                {
                    //Assortment Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionAssortmentMinorRevisionId, SqlDbType.Int, AssortmentMinorRevisionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionAmendDistributionActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionComments, SqlDbType.NVarChar, dto.Comments);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionAmendDistributionActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionComments, SqlDbType.NVarChar, dto.Comments);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
