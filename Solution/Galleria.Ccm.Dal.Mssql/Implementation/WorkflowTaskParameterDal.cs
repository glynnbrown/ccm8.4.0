﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : L.Ineson
//  Created
// V8-25886 : L.Ineson
//  Removed value property
//  Added isHidden and isreadonly
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkflowTaskParameterDal
    /// </summary>
    public sealed class WorkflowTaskParameterDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkflowTaskParameterDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        public WorkflowTaskParameterDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkflowTaskParameterDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkflowTaskParameterId]),
                ParameterId = (Int32)GetValue(dr[FieldNames.WorkflowTaskParameterParameterId]),
                WorkflowTaskId = (Int32)GetValue(dr[FieldNames.WorkflowTaskParameterWorkflowTaskId]),
                IsHidden = (Boolean)GetValue(dr[FieldNames.WorkflowTaskParameterIsHidden]),
                IsReadOnly = (Boolean)GetValue(dr[FieldNames.WorkflowTaskParameterIsReadOnly]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Fetches dtos for the given workflow task id
        /// </summary>
        public IEnumerable<WorkflowTaskParameterDto> FetchByWorkflowTaskId(Int32 workflowTaskId)
        {
            List<WorkflowTaskParameterDto> dtoList = new List<WorkflowTaskParameterDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterFetchByWorkflowTaskId))
                {
                    CreateParameter(command, FieldNames.WorkflowTaskParameterWorkflowTaskId, SqlDbType.Int, workflowTaskId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Fetches all dtos for the specified workflow
        /// </summary>
        public IEnumerable<WorkflowTaskParameterDto> FetchByWorkflowId(Int32 workflowId)
        {
            List<WorkflowTaskParameterDto> dtoList = new List<WorkflowTaskParameterDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterFetchByWorkflowId))
                {
                    CreateParameter(command, FieldNames.WorkflowTaskWorkflowId, SqlDbType.Int, workflowId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(WorkflowTaskParameterDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkflowTaskParameterId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterParameterId, SqlDbType.Int, dto.ParameterId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterWorkflowTaskId, SqlDbType.Int, dto.WorkflowTaskId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterIsHidden, SqlDbType.Bit, dto.IsHidden);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterIsReadOnly, SqlDbType.Bit, dto.IsReadOnly);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given dto
        /// </summary>
        public void Update(WorkflowTaskParameterDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterUpdateById))
                {
                    // parameter
                    CreateParameter(command, FieldNames.WorkflowTaskParameterId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterParameterId, SqlDbType.Int, dto.ParameterId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterWorkflowTaskId, SqlDbType.Int, dto.WorkflowTaskId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterIsHidden, SqlDbType.Bit, dto.IsHidden);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterIsReadOnly, SqlDbType.Bit, dto.IsReadOnly);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the dto with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkflowTaskParameterId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
