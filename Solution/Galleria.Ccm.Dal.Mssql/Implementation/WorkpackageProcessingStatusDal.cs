﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26985 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Added status description, dateStarted and dateLastUpdated fields
#endregion

#region Version History: CCM8.2.0
// V8-30646 : M.Shelley
//  Added SetPending method
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public sealed class WorkpackageProcessingStatusDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackageProcessingStatusDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        public WorkpackageProcessingStatusDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackageProcessingStatusDto()
            {
                WorkpackageId = (Int32)GetValue(dr[FieldNames.WorkpackageProcessingStatusWorkpackageId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.WorkpackageProcessingStatusRowVersion])),
                Status = (Byte)GetValue(dr[FieldNames.WorkpackageProcessingStatusStatus]),
                StatusDescription = (String)GetValue(dr[FieldNames.WorkpackageProcessingStatusStatusDescription]),
                ProgressMax = (Int32)GetValue(dr[FieldNames.WorkpackageProcessingstatusProgressMax]),
                ProgressCurrent = (Int32)GetValue(dr[FieldNames.WorkpackageProcessingStatusProgressCurrent]),
                DateStarted = (DateTime?)GetValue(dr[FieldNames.WorkpackageProcessingStatusDateStarted]),
                DateLastUpdated = (DateTime?)GetValue(dr[FieldNames.WorkpackageProcessingStatusDateUpdated])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        public WorkpackageProcessingStatusDto FetchByWorkpackageId(int workpackageId)
        {
            WorkpackageProcessingStatusDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageProcessingStatusFetchByWorkpackageId))
                {
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusWorkpackageId, SqlDbType.Int, workpackageId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        
        #endregion

        #region Initialize
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        public void Initialize(WorkpackageProcessingStatusDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageProcessingStatusInitializeByWorkpackageId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusWorkpackageId, SqlDbType.Int, dto.WorkpackageId);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.WorkpackageProcessingStatusRowVersion, SqlDbType.Timestamp, 
                                                                        ParameterDirection.InputOutput, dto.RowVersion);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusStatus, SqlDbType.TinyInt, dto.Status);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusStatusDescription, SqlDbType.NVarChar, dto.StatusDescription);
                    CreateParameter(command, FieldNames.WorkpackageProcessingstatusProgressMax, SqlDbType.Int, dto.ProgressMax);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusProgressCurrent, SqlDbType.Int, dto.ProgressCurrent);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusDateStarted, SqlDbType.DateTime, dto.DateStarted);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusDateUpdated, SqlDbType.DateTime, dto.DateLastUpdated);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        public void Update(WorkpackageProcessingStatusDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageProcessingStatusUpdateByWorkpackageId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusWorkpackageId, SqlDbType.Int, dto.WorkpackageId);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.WorkpackageProcessingStatusRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusStatus, SqlDbType.TinyInt, dto.Status);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusStatusDescription, SqlDbType.NVarChar, dto.StatusDescription);
                    CreateParameter(command, FieldNames.WorkpackageProcessingstatusProgressMax, SqlDbType.Int, dto.ProgressMax);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusProgressCurrent, SqlDbType.Int, dto.ProgressCurrent);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusDateStarted, SqlDbType.DateTime, dto.DateStarted);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusDateUpdated, SqlDbType.DateTime, dto.DateLastUpdated);
                    
                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Increment
        /// <summary>
        /// Increments the progress of a workpackage
        /// </summary>
        public void Increment(Int32 workpackageId, String statusDescription, DateTime dateLastUpdated)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageProcessingStatusIncrementByWorkpackageId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusStatusDescription, SqlDbType.NVarChar, statusDescription);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusDateUpdated, SqlDbType.DateTime, dateLastUpdated);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        /// <summary>
        /// Set the processing status of a work package to "Pending"
        /// </summary>
        /// <param name="workpackageId">The workpackage id</param>
        /// <param name="statusDescription">The processing status description</param>

        public void SetPending(int workpackageId, Byte processingStatus, string statusDescription)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageProcessingStatusSetPendingByWorkpackageId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusStatus, SqlDbType.TinyInt, processingStatus);
                    CreateParameter(command, FieldNames.WorkpackageProcessingStatusStatusDescription, SqlDbType.NVarChar, statusDescription);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
    }
}
