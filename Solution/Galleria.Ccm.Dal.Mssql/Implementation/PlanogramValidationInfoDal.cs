﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27004 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     Mssql dal implementation of <see cref="IPlanogramValidationInfoDal"/>.
    /// </summary>
    public sealed class PlanogramValidationInfoDal : DalBase, IPlanogramValidationInfoDal
    {
        #region Constants

        private const String TempImportTable = "#tmpPlanogram";

        #endregion

        #region Nested Classes

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        internal class BulkCopyIntDataReader : IDataReader
        {
            #region Fields
            IEnumerator<Int32> _enumerator; // an enumerator containing the values to write to the database
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyIntDataReader(IEnumerable<Int32> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = _enumerator.Current;
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        private static PlanogramValidationInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramValidationInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramValidationInfoId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramValidationInfoPlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramValidationInfoName])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetches a collection of <see cref="PlanogramValidationInfoDto"/> instances matching the given <paramref name="planogramIds"/>.
        /// </summary>
        /// <param name="planogramIds">A collection of <see cref="Int32"/> values that must be matched by the returned dtos.</param>
        /// <returns>A new collection of <see cref="PlanogramValidationInfoDto"/>.</returns>
        public IEnumerable<PlanogramValidationInfoDto> FetchPlanogramValidationsByPlanogramIds(
            IEnumerable<Int32> planogramIds)
        {
            var dtos = new List<PlanogramValidationInfoDto>();

            CreateTempTable(TempImportTable);
            PopulateTempTable(TempImportTable, planogramIds);

            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationInfoFetchByPlanogramIds))
                using (var dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtos.Add(GetDataTransferObject(dr));
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            DropTempTable(TempImportTable);

            return dtos;
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Creates the temporary store table on the current connection.
        /// </summary>
        /// <param name="tempTableName">Name of the temporary table to be created.</param>
        private void CreateTempTable(String tempTableName)
        {
            const String createTempImportTableSql = "CREATE TABLE [{0}] (Planogram_Id INT)";
            var commandText = String.Format(CultureInfo.InvariantCulture, createTempImportTableSql,
                tempTableName);

            //  Create the temp table.
            using (var command = CreateCommand(commandText, CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        ///     Drops the temporary store table on the current connection.
        /// </summary>
        /// <param name="tempTableName">Name of the temporary table to be dropped.</param>
        private void DropTempTable(String tempTableName)
        {
            const String dropTempImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
            var commandText = String.Format(CultureInfo.InvariantCulture, dropTempImportTableSql,
                tempTableName);

            //  Drop the temp table.
            using (var command = CreateCommand(commandText, CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        private void PopulateTempTable(String detinationTableName, IEnumerable<Int32> values)
        {
            using (var bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = String.Format("[{0}]", detinationTableName);
                bulkCopy.WriteToServer(new BulkCopyIntDataReader(values));
            }
        }

        #endregion
    }
}
