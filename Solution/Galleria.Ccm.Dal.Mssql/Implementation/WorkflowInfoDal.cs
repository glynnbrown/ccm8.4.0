﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : L.Ineson
//  Created
// CCM-25868 : L.Ineson
//  Added Description
// V8-25787 : N.Foster
//  Added RowVersion
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkflowDal
    /// </summary>
    public sealed class WorkflowInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkflowInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public WorkflowInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkflowInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkflowId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.WorkflowRowVersion])),
                Name = (String)GetValue(dr[FieldNames.WorkflowName]),
                Description = (String)GetValue(dr[FieldNames.WorkflowDescription]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<WorkflowInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<WorkflowInfoDto> dtoList = new List<WorkflowInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.WorkflowEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns all dtos for the given entity id including those which have been deleted.
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<WorkflowInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            List<WorkflowInfoDto> dtoList = new List<WorkflowInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowInfoFetchByEntityIdIncludingDeleted))
                {
                    CreateParameter(command, FieldNames.WorkflowEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified dto
        /// </summary>
        public WorkflowInfoDto FetchById(Int32 id)
        {
            WorkflowInfoDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowInfoFetchById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkflowId, SqlDbType.Int, id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = this.GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion
    }
}
