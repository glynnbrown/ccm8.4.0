﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS.   
// V8-25881 : A.Probyn
//  Added MetaData properties  
// V8-27058 : A.Probyn
//  Updated MetaData properties
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// The MSSQL DAL for the PlanogramFixtureAssembly DTO.
    /// </summary>
    public class PlanogramFixtureAssemblyDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramFixtureAssemblyDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a PlanogramFixtureAssembly Dto from the given data reader.
        /// </summary>
        /// <param name="dr">The data reader from which data should be loaded.</param>
        /// <returns>A PlanogramFixtureAssembly Dto.</returns>
        public PlanogramFixtureAssemblyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramFixtureAssemblyDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramFixtureAssemblyId]),
                PlanogramFixtureId = (Int32)GetValue(dr[FieldNames.PlanogramFixtureAssemblyPlanogramFixtureId]),
                PlanogramAssemblyId = (Int32)GetValue(dr[FieldNames.PlanogramFixtureAssemblyPlanogramAssemblyId]),
                X = (Single)GetValue(dr[FieldNames.PlanogramFixtureAssemblyX]),
                Y = (Single)GetValue(dr[FieldNames.PlanogramFixtureAssemblyY]),
                Z = (Single)GetValue(dr[FieldNames.PlanogramFixtureAssemblyZ]),
                Slope = (Single)GetValue(dr[FieldNames.PlanogramFixtureAssemblySlope]),
                Angle = (Single)GetValue(dr[FieldNames.PlanogramFixtureAssemblyAngle]),
                Roll = (Single)GetValue(dr[FieldNames.PlanogramFixtureAssemblyRoll]),
                MetaComponentCount = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaComponentCount]),
                MetaTotalMerchandisableLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace]),
                MetaTotalMerchandisableAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace]),
                MetaTotalMerchandisableVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace]),
                MetaTotalLinearWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace]),
                MetaTotalAreaWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace]),
                MetaTotalVolumetricWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace]),
                MetaProductsPlaced = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaProductsPlaced]),
                MetaNewProducts = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaNewProducts]),
                MetaChangesFromPreviousCount = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaChangesFromPreviousCount]),
                MetaChangeFromPreviousStarRating = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating]),
                MetaBlocksDropped = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaBlocksDropped]),
                MetaNotAchievedInventory = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaNotAchievedInventory]),
                MetaTotalFacings = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalFacings]),
                MetaAverageFacings = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaAverageFacings]),
                MetaTotalUnits = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalUnits]),
                MetaAverageUnits = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaAverageUnits]),
                MetaMinDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaMinDos]),
                MetaMaxDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaMaxDos]),
                MetaAverageDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaAverageDos]),
                MetaMinCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaMinCases]),
                MetaAverageCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaAverageCases]),
                MetaMaxCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaMaxCases]),
                MetaSpaceToUnitsIndex = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaSpaceToUnitsIndex]),
                MetaTotalFrontFacings = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaTotalFrontFacings]),
                MetaAverageFrontFacings = (Single?)GetValue(dr[FieldNames.PlanogramFixtureAssemblyMetaAverageFrontFacings]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramFixtureAssembly DTOs for the given  PlanogramFixture Id.
        /// </summary>
        /// <param name="planogramFixtureId">The Id of the PlanogramFixture for which Assemblys should be returned.</param>
        /// <returns>A list of PlanogramFixtureAssembly DTOs.</returns>
        public IEnumerable<PlanogramFixtureAssemblyDto> FetchByPlanogramFixtureId(object planogramFixtureId)
        {
            var dtoList = new List<PlanogramFixtureAssemblyDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramFixtureAssemblyFetchByPlanogramFixtureId))
                {
                    CreateParameter(
                        command,
                        FieldNames.PlanogramFixtureAssemblyPlanogramFixtureId,
                        SqlDbType.Int,
                        (Int32)planogramFixtureId);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given PlanogramFixtureAssembly Dto into the database.
        /// </summary>
        /// <param name="dto">The PlanogramFixtureAssembly Dto to insert.</param>
        public void Insert(PlanogramFixtureAssemblyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramFixtureAssemblyInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramFixtureAssemblyId, SqlDbType.Int);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given PlanogramFixtureAssembly Dto in the database.
        /// </summary>
        /// <param name="dto">Teh PlanogramFixtureAssembly Dto to update.</param>
        public void Update(PlanogramFixtureAssemblyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramFixtureAssemblyUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramFixtureAssemblyId, SqlDbType.Int, dto.Id);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a record in the database with the given Id value.
        /// </summary>
        /// <param name="id">The Id of the PlanogramFixtureAssembly to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramFixtureAssemblyDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramFixtureAssemblyId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramFixtureAssemblyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Adds all DTO properties, apart from Id, as parameters to the command.
        /// </summary>
        /// <param name="dto">The DTO whose properties should be added.</param>
        /// <param name="command">The command to add parameters to.</param>
        private void CreateAllParametersButId(PlanogramFixtureAssemblyDto dto, DalCommand command)
        {
            CreateParameter(command,FieldNames.PlanogramFixtureAssemblyPlanogramFixtureId,SqlDbType.Int,dto.PlanogramFixtureId);
            CreateParameter(command,FieldNames.PlanogramFixtureAssemblyPlanogramAssemblyId,SqlDbType.Int,dto.PlanogramAssemblyId);
            CreateParameter(command,FieldNames.PlanogramFixtureAssemblyX,SqlDbType.Real,dto.X);
            CreateParameter(command,FieldNames.PlanogramFixtureAssemblyY,SqlDbType.Real,dto.Y);
            CreateParameter(command,FieldNames.PlanogramFixtureAssemblyZ,SqlDbType.Real,dto.Z);
            CreateParameter(command,FieldNames.PlanogramFixtureAssemblySlope,SqlDbType.Real,dto.Slope);
            CreateParameter(command,FieldNames.PlanogramFixtureAssemblyAngle,SqlDbType.Real,dto.Angle);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyRoll, SqlDbType.Real, dto.Roll);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaComponentCount, SqlDbType.Int, dto.MetaComponentCount);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace, SqlDbType.Real, dto.MetaTotalMerchandisableLinearSpace);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace, SqlDbType.Real, dto.MetaTotalMerchandisableAreaSpace);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace, SqlDbType.Real, dto.MetaTotalMerchandisableVolumetricSpace);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace, SqlDbType.Real, dto.MetaTotalLinearWhiteSpace);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace, SqlDbType.Real, dto.MetaTotalAreaWhiteSpace);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace, SqlDbType.Real, dto.MetaTotalVolumetricWhiteSpace);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaProductsPlaced, SqlDbType.Int, dto.MetaProductsPlaced);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaNewProducts, SqlDbType.Int, dto.MetaNewProducts);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaChangesFromPreviousCount, SqlDbType.Int, dto.MetaChangesFromPreviousCount);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating, SqlDbType.Int, dto.MetaChangeFromPreviousStarRating);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaBlocksDropped, SqlDbType.Int, dto.MetaBlocksDropped);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaNotAchievedInventory, SqlDbType.Int, dto.MetaNotAchievedInventory);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalFacings, SqlDbType.Int, dto.MetaTotalFacings);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaAverageFacings, SqlDbType.SmallInt, dto.MetaAverageFacings);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalUnits, SqlDbType.Int, dto.MetaTotalUnits);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaAverageUnits, SqlDbType.Int, dto.MetaAverageUnits);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaMinDos, SqlDbType.Real, dto.MetaMinDos);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaMaxDos, SqlDbType.Real, dto.MetaMaxDos);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaAverageDos, SqlDbType.Real, dto.MetaAverageDos);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaMinCases, SqlDbType.Real, dto.MetaMinCases);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaAverageCases, SqlDbType.Real, dto.MetaAverageCases);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaMaxCases, SqlDbType.Real, dto.MetaMaxCases);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaSpaceToUnitsIndex, SqlDbType.SmallInt, dto.MetaSpaceToUnitsIndex);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaTotalFrontFacings, SqlDbType.SmallInt, dto.MetaTotalFrontFacings);
            CreateParameter(command, FieldNames.PlanogramFixtureAssemblyMetaAverageFrontFacings, SqlDbType.Real, dto.MetaAverageFrontFacings);
            
            
        }
        #endregion
    }
}
