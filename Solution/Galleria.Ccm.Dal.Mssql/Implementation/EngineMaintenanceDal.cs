﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM803
// V8-29606 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class EngineMaintenanceDal : Galleria.Framework.Dal.Mssql.DalBase, IEngineMaintenanceDal
    {
        #region Fetch
        /// <summary>
        /// Returns all planogram that require their metadata to be calculated
        /// </summary>
        public IEnumerable<Int32> FetchPlanogramsByMetadataCalculationRequired()
        {
            List<Int32> planogramIds = new List<Int32>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMaintenanceFetchPlanogramsByMetadataCalculationRequired))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            planogramIds.Add((Int32)GetValue(dr[FieldNames.PlanogramId]));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return planogramIds;
        }

        /// <summary>
        /// Returns all planograms that require their validation to be calculated
        /// </summary>
        public IEnumerable<Int32> FetchPlanogramsByValidationCalculationRequired()
        {
            List<Int32> planogramIds = new List<Int32>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMaintenanceFetchPlanogramsByValidationCalculationRequired))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            planogramIds.Add((Int32)GetValue(dr[FieldNames.PlanogramId]));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return planogramIds;
        }

        /// <summary>
        /// Returns all planograms that require deletion
        /// </summary>
        public IEnumerable<Int32> FetchPlanogramsByDeletionRequired()
        {
            List<Int32> planogramIds = new List<Int32>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMaintenanceFetchPlanogramsByDeletionRequired))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            planogramIds.Add((Int32)GetValue(dr[FieldNames.PlanogramId]));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return planogramIds;
        }

        #endregion
    }
}
