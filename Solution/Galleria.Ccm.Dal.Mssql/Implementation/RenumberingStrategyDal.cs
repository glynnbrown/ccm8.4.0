﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27153 : A.Silva
//      Created
// V8-27562 : A.Silva
//      Added FetchByEntityIdName.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public sealed class RenumberingStrategyDal : DalBase, IRenumberingStrategyDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static RenumberingStrategyDto GetDataTransferObject(IDataRecord dr)
        {
            var renumberingStrategyDto = new RenumberingStrategyDto
            {
                Id = (Int32) GetValue(dr[FieldNames.RenumberingStrategyId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.RenumberingStrategyRowVersion])),
                EntityId = (Int32) GetValue(dr[FieldNames.RenumberingStrategyEntityId]),
                Name = (String) GetValue(dr[FieldNames.RenumberingStrategyName]),
                DateCreated = (DateTime) GetValue(dr[FieldNames.RenumberingStrategyDateCreated]),
                DateLastModified = (DateTime) GetValue(dr[FieldNames.RenumberingStrategyDateLastModified]),
                DateDeleted = (DateTime?) GetValue(dr[FieldNames.RenumberingStrategyDateDeleted]),
                BayComponentXRenumberStrategyPriority =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyBayComponentXRenumberStrategyPriority]),
                BayComponentYRenumberStrategyPriority =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyBayComponentYRenumberStrategyPriority]),
                BayComponentZRenumberStrategyPriority =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyBayComponentZRenumberStrategyPriority]),
                PositionXRenumberStrategyPriority =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyPositionXRenumberStrategyPriority]),
                PositionYRenumberStrategyPriority =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyPositionYRenumberStrategyPriority]),
                PositionZRenumberStrategyPriority =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyPositionZRenumberStrategyPriority]),
                BayComponentXRenumberStrategy =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyBayComponentXRenumberStrategy]),
                BayComponentYRenumberStrategy =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyBayComponentYRenumberStrategy]),
                BayComponentZRenumberStrategy =
                    (Byte) GetValue(dr[FieldNames.RenumberingStrategyBayComponentZRenumberStrategy]),
                PositionXRenumberStrategy = (Byte) GetValue(dr[FieldNames.RenumberingStrategyPositionXRenumberStrategy]),
                PositionYRenumberStrategy = (Byte) GetValue(dr[FieldNames.RenumberingStrategyPositionYRenumberStrategy]),
                PositionZRenumberStrategy = (Byte) GetValue(dr[FieldNames.RenumberingStrategyPositionZRenumberStrategy]),
                RestartComponentRenumberingPerBay =
                    (Boolean) GetValue(dr[FieldNames.RenumberingStrategyRestartComponentRenumberingPerBay]),
                IgnoreNonMerchandisingComponents =
                    (Boolean) GetValue(dr[FieldNames.RenumberingStrategyIgnoreNonMerchandisingComponents]),
                RestartPositionRenumberingPerComponent =
                    (Boolean) GetValue(dr[FieldNames.RenumberingStrategyRestartPositionRenumberingPerComponent]),
                UniqueNumberMultiPositionProductsPerComponent =
                    (Boolean) GetValue(dr[FieldNames.RenumberingStrategyUniqueNumberMultiPositionProductsPerComponent]),
                ExceptAdjacentPositions = (Boolean) GetValue(dr[FieldNames.RenumberingStrategyExceptAdjacentPositions]),
                RestartPositionRenumberingPerBay =
                    (Boolean)GetValue(dr[FieldNames.RenumberingStrategyRestartPositionRenumberingPerBay]),
                RestartComponentRenumberingPerComponentType =
                    (Boolean)GetValue(dr[FieldNames.RenumberingStrategyRestartComponentRenumberingPerComponentType]),
            };
            return renumberingStrategyDto;
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="RenumberingStrategyDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="RenumberingStrategyDto" /> with the data for the provided <paramref name="id" />.</returns>
        public RenumberingStrategyDto FetchById(Int32 id)
        {
            RenumberingStrategyDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.RenumberingStrategyFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.RenumberingStrategyId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        ///     Returns an enumeration of <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> that will be matched for fetching.</param>
        /// <returns>
        ///     An enumeration of instances of <see cref="RenumberingStrategyDto" /> wmatching the provided
        ///     <paramref name="entityId" />.
        /// </returns>
        public IEnumerable<RenumberingStrategyDto> FetchByEntityId(Int32 entityId)
        {
            var dtos = new List<RenumberingStrategyDto>();

            try
            {
                using (var command = CreateCommand(ProcedureNames.RenumberingStrategyFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.RenumberingStrategyEntityId, SqlDbType.Int, entityId);

                    // Execute.
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtos;
        }

        /// <summary>
        ///     Returns a <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="entityId" /> and <paramref name="name"/>.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> from which to fetch the <see cref="RenumberingStrategyDto"/>.</param>
        /// <param name="name">The name to be matched by the fetched <see cref="RenumberingStrategyDto"/>.</param>
        /// <returns>A new instance of <see cref="RenumberingStrategyDto" /> with the data for the provided <paramref name="id" />.</returns>
        public RenumberingStrategyDto FetchByEntityIdName(Int32 entityId, String name)
        {
            RenumberingStrategyDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.RenumberingStrategyFetchByEntityIdName))
                {
                    //Id
                    CreateParameter(command, FieldNames.RenumberingStrategyEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.RenumberingStrategyName, SqlDbType.NVarChar, name);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="RenumberingStrategyDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(RenumberingStrategyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.RenumberingStrategyInsert))
                {
                    // Id
                    var idParameter = CreateParameter(command, FieldNames.RenumberingStrategyId, SqlDbType.Int);

                    // Row version.
                    var rowVersionParameter = CreateParameter(command, FieldNames.RenumberingStrategyRowVersion,
                        SqlDbType.Timestamp);

                    // Date created.
                    var dateCreatedParameter = CreateParameter(command, FieldNames.RenumberingStrategyDateCreated,
                        SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    // Date last modified.
                    var dateLastModifiedParameter = CreateParameter(command,
                        FieldNames.RenumberingStrategyDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);

                    // Date deleted.
                    var dateDeletedParameter = CreateParameter(command, FieldNames.RenumberingStrategyDateDeleted,
                        SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    // Other properties.
                    CreateParameter(command, FieldNames.RenumberingStrategyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.RenumberingStrategyName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentXRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.BayComponentXRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentYRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.BayComponentYRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentZRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.BayComponentZRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionXRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.PositionXRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionYRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.PositionYRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionZRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.PositionZRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentXRenumberStrategy,
                        SqlDbType.TinyInt, dto.BayComponentXRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentYRenumberStrategy,
                        SqlDbType.TinyInt, dto.BayComponentYRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentZRenumberStrategy,
                        SqlDbType.TinyInt, dto.BayComponentZRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionXRenumberStrategy,
                        SqlDbType.TinyInt, dto.PositionXRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionYRenumberStrategy,
                        SqlDbType.TinyInt, dto.PositionYRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionZRenumberStrategy,
                        SqlDbType.TinyInt, dto.PositionZRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartComponentRenumberingPerBay,
                        SqlDbType.Bit, dto.RestartComponentRenumberingPerBay);
                    CreateParameter(command, FieldNames.RenumberingStrategyIgnoreNonMerchandisingComponents,
                        SqlDbType.Bit, dto.IgnoreNonMerchandisingComponents);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartPositionRenumberingPerComponent,
                        SqlDbType.Bit, dto.RestartPositionRenumberingPerComponent);
                    CreateParameter(command, FieldNames.RenumberingStrategyUniqueNumberMultiPositionProductsPerComponent,
                        SqlDbType.Bit, dto.UniqueNumberMultiPositionProductsPerComponent);
                    CreateParameter(command, FieldNames.RenumberingStrategyExceptAdjacentPositions,
                        SqlDbType.Bit, dto.ExceptAdjacentPositions);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartPositionRenumberingPerBay,
                        SqlDbType.Bit, dto.RestartPositionRenumberingPerBay);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartComponentRenumberingPerComponentType,
                        SqlDbType.Bit, dto.RestartComponentRenumberingPerComponentType);

                    //Execute
                    command.ExecuteNonQuery();

                    // Update dto.
                    dto.Id = (Int32) idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime) dateCreatedParameter.Value;
                    dto.DateLastModified = (DateTime) dateLastModifiedParameter.Value;
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value
                        ? null
                        : (DateTime?) dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="RenumberingStrategyDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(RenumberingStrategyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.RenumberingStrategyUpdateById))
                {
                    // Id.
                    CreateParameter(command, FieldNames.RenumberingStrategyId, SqlDbType.Int, dto.Id);

                    // Row version.
                    var rowVersionParameter = CreateParameter(command, FieldNames.RenumberingStrategyRowVersion,
                        SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    // Date created.
                    var dateCreatedParameter = CreateParameter(command, FieldNames.RenumberingStrategyDateCreated,
                        SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    // Date last modified.
                    var dateLastModifiedParameter = CreateParameter(command,
                        FieldNames.RenumberingStrategyDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);

                    // Date deleted.
                    var dateDeletedParameter = CreateParameter(command, FieldNames.RenumberingStrategyDateDeleted,
                        SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    // Other properties
                    CreateParameter(command, FieldNames.RenumberingStrategyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.RenumberingStrategyName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentXRenumberStrategyPriority,
    SqlDbType.TinyInt, dto.BayComponentXRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentYRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.BayComponentYRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentZRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.BayComponentZRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionXRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.PositionXRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionYRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.PositionYRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionZRenumberStrategyPriority,
                        SqlDbType.TinyInt, dto.PositionZRenumberStrategyPriority);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentXRenumberStrategy,
                        SqlDbType.TinyInt, dto.BayComponentXRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentYRenumberStrategy,
                        SqlDbType.TinyInt, dto.BayComponentYRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyBayComponentZRenumberStrategy,
                        SqlDbType.TinyInt, dto.BayComponentZRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionXRenumberStrategy,
                        SqlDbType.TinyInt, dto.PositionXRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionYRenumberStrategy,
                        SqlDbType.TinyInt, dto.PositionYRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyPositionZRenumberStrategy,
                        SqlDbType.TinyInt, dto.PositionZRenumberStrategy);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartComponentRenumberingPerBay,
                        SqlDbType.Bit, dto.RestartComponentRenumberingPerBay);
                    CreateParameter(command, FieldNames.RenumberingStrategyIgnoreNonMerchandisingComponents,
                        SqlDbType.Bit, dto.IgnoreNonMerchandisingComponents);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartPositionRenumberingPerComponent,
                        SqlDbType.Bit, dto.RestartPositionRenumberingPerComponent);
                    CreateParameter(command, FieldNames.RenumberingStrategyUniqueNumberMultiPositionProductsPerComponent,
                        SqlDbType.Bit, dto.UniqueNumberMultiPositionProductsPerComponent);
                    CreateParameter(command, FieldNames.RenumberingStrategyExceptAdjacentPositions,
                        SqlDbType.Bit, dto.ExceptAdjacentPositions);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartPositionRenumberingPerBay,
                        SqlDbType.Bit, dto.RestartPositionRenumberingPerBay);
                    CreateParameter(command, FieldNames.RenumberingStrategyRestartComponentRenumberingPerComponentType,
                        SqlDbType.Bit, dto.RestartComponentRenumberingPerComponentType);

                    // Execute.
                    command.ExecuteNonQuery();

                    // Update dto.
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)dateCreatedParameter.Value;
                    dto.DateLastModified = (DateTime) dateLastModifiedParameter.Value;
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value
                        ? null
                        : (DateTime?) dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.RenumberingStrategyDeleteById))
                {
                    // Id.
                    CreateParameter(command, FieldNames.RenumberingStrategyId, SqlDbType.Int, id);

                    // Execute.
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}