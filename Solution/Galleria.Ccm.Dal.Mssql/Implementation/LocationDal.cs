﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25619 : A.Kuszyk
//		Created (Auto-generated)
// CCM-25445 : L.Ineson
//  Added FetchByLocationGroupId
// V8-25748 : K.Pickup
//      Changed data type of LocationId to Int16.
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
// V8-25556 : D.Pleasance
//  Added FetchByEntityIdIncludingDeleted, amended Upsert, added parameter updateDeleted
#endregion
#region Version History: (CCM v8.0.1)
// V8-28878 : D.Pleasance
//  Fixed upsert, final column was being ignored and also final 2 columns were in 
//  the wrong order of how it was expected in temp table that is created.
#endregion 
#region Version History: (CCM 8.3.0)
// V8-32180 : N.Haywood
//  Reduced length of telCountryCode, telAreaCode, faxCountryCode and faxAreaCode to 5
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Location Dal
    /// </summary>
    public class LocationDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationDal
    {
        #region Constants

        private const String _importTableName = "#tmpLocation";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[Entity_Id] [INT], " +
                "[LocationGroup_Id] [INT], " +
                "[Location_Code] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Name] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Region] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_TVRegion] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Location] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_DefaultClusterAttribute] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Address1] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Address2] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_City] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_County] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_State] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_PostalCode] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Country] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Longitude] [REAL], " +
                "[Location_Latitude] [REAL], " +
                "[Location_DateOpen] [DATE], " +
                "[Location_DateLastRefitted] [DATE], " +
                "[Location_DateClosed] [DATE], " +
                "[Location_CarParkSpaces] [SMALLINT], " +
                "[Location_CarParkManagement] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_PetrolForecourtType] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_Restaurant] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_SizeGrossFloorArea] [INT], " +
                "[Location_SizeNetSalesArea] [INT], " +
                "[Location_SizeMezzSalesArea] [INT], " +
                "[Location_TelephoneCountryCode] [NVARCHAR](5) COLLATE database_default, " +
                "[Location_TelephoneAreaCode] [NVARCHAR](5) COLLATE database_default, " +
                "[Location_TelephoneNumber] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_FaxCountryCode] [NVARCHAR](5) COLLATE database_default, " +
                "[Location_FaxAreaCode] [NVARCHAR](5) COLLATE database_default, " +
                "[Location_FaxNumber] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_OpeningHours] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_AverageOpeningHours] [REAL], " +
                "[Location_ManagerName] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_ManagerEmail] [NVARCHAR](256) COLLATE database_default, " +
                "[Location_RegionalManagerName] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_RegionalManagerEmail] [NVARCHAR](256) COLLATE database_default, " +
                "[Location_DivisionalManagerName] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_DivisionalManagerEmail] [NVARCHAR](256) COLLATE database_default, " +
                "[Location_AdvertisingZone] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_DistributionCentre] [NVARCHAR](50) COLLATE database_default, " +
                "[Location_NoOfCheckouts] [TINYINT], " +
                "[Location_IsMezzFitted] [BIT], " +
                "[Location_IsFreehold] [BIT], " +
                "[Location_Is24Hours] [BIT], " +
                "[Location_IsOpenMonday] [BIT], " +
                "[Location_IsOpenTuesday] [BIT], " +
                "[Location_IsOpenWednesday] [BIT], " +
                "[Location_IsOpenThursday] [BIT], " +
                "[Location_IsOpenFriday] [BIT], " +
                "[Location_IsOpenSaturday] [BIT], " +
                "[Location_IsOpenSunday] [BIT], " +
                "[Location_HasPetrolForecourt] [BIT], " +
                "[Location_HasNewsCube] [BIT], " +
                "[Location_HasAtmMachines] [BIT], " +
                "[Location_HasCustomerWC] [BIT], " +
                "[Location_HasBabyChanging] [BIT], " +
                "[Location_HasInStoreBakery] [BIT], " +
                "[Location_HasHotFoodToGo] [BIT], " +
                "[Location_HasRotisserie] [BIT], " +
                "[Location_HasFishmonger] [BIT], " +
                "[Location_HasButcher] [BIT], " +
                "[Location_HasPizza] [BIT], " +
                "[Location_HasDeli] [BIT], " +
                "[Location_HasSaladBar] [BIT], " +
                "[Location_HasOrganic] [BIT], " +
                "[Location_HasGrocery] [BIT], " +
                "[Location_HasMobilePhones] [BIT], " +
                "[Location_HasDryCleaning] [BIT], " +
                "[Location_HasHomeShoppingAvailable] [BIT], " +
                "[Location_HasOptician] [BIT], " +
                "[Location_HasPharmacy] [BIT], " +
                "[Location_HasTravel] [BIT], " +
                "[Location_HasPhotoDepartment] [BIT], " +
                "[Location_HasCarServiceArea] [BIT], " +
                "[Location_HasGardenCentre] [BIT], " +
                "[Location_HasClinic] [BIT], " +
                "[Location_HasAlcohol] [BIT], " +
                "[Location_HasFashion] [BIT], " +
                "[Location_HasCafe] [BIT], " +
                "[Location_HasRecycling] [BIT], " +
                "[Location_HasPhotocopier] [BIT], " +
                "[Location_HasLottery] [BIT], " +
                "[Location_HasPostOffice] [BIT], " +
                "[Location_HasMovieRental] [BIT], " +
                "[Location_HasJewellery] [BIT], " +
                "[Location_LocationType] [NVARCHAR](100) COLLATE database_default" +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class LocationBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public LocationBulkCopyDataReader(IEnumerable<LocationDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 89; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.EntityId;
                        break;
                    case 1:
                        value = _enumerator.Current.LocationGroupId;
                        break;
                    case 2:
                        value = _enumerator.Current.Code;
                        break;
                    case 3:
                        value = _enumerator.Current.Name;
                        break;
                    case 4:
                        value = _enumerator.Current.Region;
                        break;
                    case 5:
                        value = _enumerator.Current.TVRegion;
                        break;
                    case 6:
                        value = _enumerator.Current.Location;
                        break;
                    case 7:
                        value = _enumerator.Current.DefaultClusterAttribute;
                        break;
                    case 8:
                        value = _enumerator.Current.Address1;
                        break;
                    case 9:
                        value = _enumerator.Current.Address2;
                        break;
                    case 10:
                        value = _enumerator.Current.City;
                        break;
                    case 11:
                        value = _enumerator.Current.County;
                        break;
                    case 12:
                        value = _enumerator.Current.State;
                        break;
                    case 13:
                        value = _enumerator.Current.PostalCode;
                        break;
                    case 14:
                        value = _enumerator.Current.Country;
                        break;
                    case 15:
                        value = _enumerator.Current.Longitude;
                        break;
                    case 16:
                        value = _enumerator.Current.Latitude;
                        break;
                    case 17:
                        value = _enumerator.Current.DateOpen;
                        break;
                    case 18:
                        value = _enumerator.Current.DateLastRefitted;
                        break;
                    case 19:
                        value = _enumerator.Current.DateClosed;
                        break;
                    case 20:
                        value = _enumerator.Current.CarParkSpaces;
                        break;
                    case 21:
                        value = _enumerator.Current.CarParkManagement;
                        break;
                    case 22:
                        value = _enumerator.Current.PetrolForecourtType;
                        break;
                    case 23:
                        value = _enumerator.Current.Restaurant;
                        break;
                    case 24:
                        value = _enumerator.Current.SizeGrossFloorArea;
                        break;
                    case 25:
                        value = _enumerator.Current.SizeNetSalesArea;
                        break;
                    case 26:
                        value = _enumerator.Current.SizeMezzSalesArea;
                        break;
                    case 27:
                        value = _enumerator.Current.TelephoneCountryCode;
                        break;
                    case 28:
                        value = _enumerator.Current.TelephoneAreaCode;
                        break;
                    case 29:
                        value = _enumerator.Current.TelephoneNumber;
                        break;
                    case 30:
                        value = _enumerator.Current.FaxCountryCode;
                        break;
                    case 31:
                        value = _enumerator.Current.FaxAreaCode;
                        break;
                    case 32:
                        value = _enumerator.Current.FaxNumber;
                        break;
                    case 33:
                        value = _enumerator.Current.OpeningHours;
                        break;
                    case 34:
                        value = _enumerator.Current.AverageOpeningHours;
                        break;
                    case 35:
                        value = _enumerator.Current.ManagerName;
                        break;
                    case 36:
                        value = _enumerator.Current.ManagerEmail;
                        break;
                    case 37:
                        value = _enumerator.Current.RegionalManagerName;
                        break;
                    case 38:
                        value = _enumerator.Current.RegionalManagerEmail;
                        break;
                    case 39:
                        value = _enumerator.Current.DivisionalManagerName;
                        break;
                    case 40:
                        value = _enumerator.Current.DivisionalManagerEmail;
                        break;
                    case 41:
                        value = _enumerator.Current.AdvertisingZone;
                        break;
                    case 42:
                        value = _enumerator.Current.DistributionCentre;
                        break;
                    case 43:
                        value = _enumerator.Current.NoOfCheckouts;
                        break;
                    case 44:
                        value = _enumerator.Current.IsMezzFitted;
                        break;
                    case 45:
                        value = _enumerator.Current.IsFreehold;
                        break;
                    case 46:
                        value = _enumerator.Current.Is24Hours;
                        break;
                    case 47:
                        value = _enumerator.Current.IsOpenMonday;
                        break;
                    case 48:
                        value = _enumerator.Current.IsOpenTuesday;
                        break;
                    case 49:
                        value = _enumerator.Current.IsOpenWednesday;
                        break;
                    case 50:
                        value = _enumerator.Current.IsOpenThursday;
                        break;
                    case 51:
                        value = _enumerator.Current.IsOpenFriday;
                        break;
                    case 52:
                        value = _enumerator.Current.IsOpenSaturday;
                        break;
                    case 53:
                        value = _enumerator.Current.IsOpenSunday;
                        break;
                    case 54:
                        value = _enumerator.Current.HasPetrolForecourt;
                        break;
                    case 55:
                        value = _enumerator.Current.HasNewsCube;
                        break;
                    case 56:
                        value = _enumerator.Current.HasAtmMachines;
                        break;
                    case 57:
                        value = _enumerator.Current.HasCustomerWC;
                        break;
                    case 58:
                        value = _enumerator.Current.HasBabyChanging;
                        break;
                    case 59:
                        value = _enumerator.Current.HasInStoreBakery;
                        break;
                    case 60:
                        value = _enumerator.Current.HasHotFoodToGo;
                        break;
                    case 61:
                        value = _enumerator.Current.HasRotisserie;
                        break;
                    case 62:
                        value = _enumerator.Current.HasFishmonger;
                        break;
                    case 63:
                        value = _enumerator.Current.HasButcher;
                        break;
                    case 64:
                        value = _enumerator.Current.HasPizza;
                        break;
                    case 65:
                        value = _enumerator.Current.HasDeli;
                        break;
                    case 66:
                        value = _enumerator.Current.HasSaladBar;
                        break;
                    case 67:
                        value = _enumerator.Current.HasOrganic;
                        break;
                    case 68:
                        value = _enumerator.Current.HasGrocery;
                        break;
                    case 69:
                        value = _enumerator.Current.HasMobilePhones;
                        break;
                    case 70:
                        value = _enumerator.Current.HasDryCleaning;
                        break;
                    case 71:
                        value = _enumerator.Current.HasHomeShoppingAvailable;
                        break;
                    case 72:
                        value = _enumerator.Current.HasOptician;
                        break;
                    case 73:
                        value = _enumerator.Current.HasPharmacy;
                        break;
                    case 74:
                        value = _enumerator.Current.HasTravel;
                        break;
                    case 75:
                        value = _enumerator.Current.HasPhotoDepartment;
                        break;
                    case 76:
                        value = _enumerator.Current.HasCarServiceArea;
                        break;
                    case 77:
                        value = _enumerator.Current.HasGardenCentre;
                        break;
                    case 78:
                        value = _enumerator.Current.HasClinic;
                        break;
                    case 79:
                        value = _enumerator.Current.HasAlcohol;
                        break;
                    case 80:
                        value = _enumerator.Current.HasFashion;
                        break;
                    case 81:
                        value = _enumerator.Current.HasCafe;
                        break;
                    case 82:
                        value = _enumerator.Current.HasRecycling;
                        break;
                    case 83:
                        value = _enumerator.Current.HasPhotocopier;
                        break;
                    case 84:
                        value = _enumerator.Current.HasLottery;
                        break;
                    case 85:
                        value = _enumerator.Current.HasPostOffice;
                        break;
                    case 86:
                        value = _enumerator.Current.HasMovieRental;
                        break;
                    case 87:
                        value = _enumerator.Current.HasJewellery;
                        break;
                    case 88:
                        value = _enumerator.Current.LocationType;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static LocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationDto()
            {
                Id = (Int16)GetValue(dr[FieldNames.LocationId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.LocationRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationEntityId]),
                LocationGroupId = (Int32)GetValue(dr[FieldNames.LocationLocationGroupId]),
                Code = (String)GetValue(dr[FieldNames.LocationCode]),
                Name = (String)GetValue(dr[FieldNames.LocationName]),
                Region = (String)GetValue(dr[FieldNames.LocationRegion]),
                TVRegion = (String)GetValue(dr[FieldNames.LocationTVRegion]),
                Location = (String)GetValue(dr[FieldNames.LocationLocation]),
                DefaultClusterAttribute = (String)GetValue(dr[FieldNames.LocationDefaultClusterAttribute]),
                Address1 = (String)GetValue(dr[FieldNames.LocationAddress1]),
                Address2 = (String)GetValue(dr[FieldNames.LocationAddress2]),
                City = (String)GetValue(dr[FieldNames.LocationCity]),
                County = (String)GetValue(dr[FieldNames.LocationCounty]),
                State = (String)GetValue(dr[FieldNames.LocationState]),
                PostalCode = (String)GetValue(dr[FieldNames.LocationPostalCode]),
                Country = (String)GetValue(dr[FieldNames.LocationCountry]),
                Longitude = (Single?)GetValue(dr[FieldNames.LocationLongitude]),
                Latitude = (Single?)GetValue(dr[FieldNames.LocationLatitude]),
                DateOpen = (DateTime?)GetValue(dr[FieldNames.LocationDateOpen]),
                DateLastRefitted = (DateTime?)GetValue(dr[FieldNames.LocationDateLastRefitted]),
                DateClosed = (DateTime?)GetValue(dr[FieldNames.LocationDateClosed]),
                CarParkSpaces = (Int16?)GetValue(dr[FieldNames.LocationCarParkSpaces]),
                CarParkManagement = (String)GetValue(dr[FieldNames.LocationCarParkManagement]),
                PetrolForecourtType = (String)GetValue(dr[FieldNames.LocationPetrolForecourtType]),
                Restaurant = (String)GetValue(dr[FieldNames.LocationRestaurant]),
                SizeGrossFloorArea = (Int32?)GetValue(dr[FieldNames.LocationSizeGrossFloorArea]),
                SizeNetSalesArea = (Int32?)GetValue(dr[FieldNames.LocationSizeNetSalesArea]),
                SizeMezzSalesArea = (Int32?)GetValue(dr[FieldNames.LocationSizeMezzSalesArea]),
                TelephoneCountryCode = (String)GetValue(dr[FieldNames.LocationTelephoneCountryCode]),
                TelephoneAreaCode = (String)GetValue(dr[FieldNames.LocationTelephoneAreaCode]),
                TelephoneNumber = (String)GetValue(dr[FieldNames.LocationTelephoneNumber]),
                FaxCountryCode = (String)GetValue(dr[FieldNames.LocationFaxCountryCode]),
                FaxAreaCode = (String)GetValue(dr[FieldNames.LocationFaxAreaCode]),
                FaxNumber = (String)GetValue(dr[FieldNames.LocationFaxNumber]),
                OpeningHours = (String)GetValue(dr[FieldNames.LocationOpeningHours]),
                AverageOpeningHours = (Single?)GetValue(dr[FieldNames.LocationAverageOpeningHours]),
                ManagerName = (String)GetValue(dr[FieldNames.LocationManagerName]),
                ManagerEmail = (String)GetValue(dr[FieldNames.LocationManagerEmail]),
                RegionalManagerName = (String)GetValue(dr[FieldNames.LocationRegionalManagerName]),
                RegionalManagerEmail = (String)GetValue(dr[FieldNames.LocationRegionalManagerEmail]),
                DivisionalManagerName = (String)GetValue(dr[FieldNames.LocationDivisionalManagerName]),
                DivisionalManagerEmail = (String)GetValue(dr[FieldNames.LocationDivisionalManagerEmail]),
                AdvertisingZone = (String)GetValue(dr[FieldNames.LocationAdvertisingZone]),
                DistributionCentre = (String)GetValue(dr[FieldNames.LocationDistributionCentre]),
                NoOfCheckouts = (Byte?)GetValue(dr[FieldNames.LocationNoOfCheckouts]),
                IsMezzFitted = (Boolean?)GetValue(dr[FieldNames.LocationIsMezzFitted]),
                IsFreehold = (Boolean?)GetValue(dr[FieldNames.LocationIsFreehold]),
                Is24Hours = (Boolean?)GetValue(dr[FieldNames.LocationIs24Hours]),
                IsOpenMonday = (Boolean?)GetValue(dr[FieldNames.LocationIsOpenMonday]),
                IsOpenTuesday = (Boolean?)GetValue(dr[FieldNames.LocationIsOpenTuesday]),
                IsOpenWednesday = (Boolean?)GetValue(dr[FieldNames.LocationIsOpenWednesday]),
                IsOpenThursday = (Boolean?)GetValue(dr[FieldNames.LocationIsOpenThursday]),
                IsOpenFriday = (Boolean?)GetValue(dr[FieldNames.LocationIsOpenFriday]),
                IsOpenSaturday = (Boolean?)GetValue(dr[FieldNames.LocationIsOpenSaturday]),
                IsOpenSunday = (Boolean?)GetValue(dr[FieldNames.LocationIsOpenSunday]),
                HasPetrolForecourt = (Boolean?)GetValue(dr[FieldNames.LocationHasPetrolForecourt]),
                HasNewsCube = (Boolean?)GetValue(dr[FieldNames.LocationHasNewsCube]),
                HasAtmMachines = (Boolean?)GetValue(dr[FieldNames.LocationHasAtmMachines]),
                HasCustomerWC = (Boolean?)GetValue(dr[FieldNames.LocationHasCustomerWC]),
                HasBabyChanging = (Boolean?)GetValue(dr[FieldNames.LocationHasBabyChanging]),
                HasInStoreBakery = (Boolean?)GetValue(dr[FieldNames.LocationHasInStoreBakery]),
                HasHotFoodToGo = (Boolean?)GetValue(dr[FieldNames.LocationHasHotFoodToGo]),
                HasRotisserie = (Boolean?)GetValue(dr[FieldNames.LocationHasRotisserie]),
                HasFishmonger = (Boolean?)GetValue(dr[FieldNames.LocationHasFishmonger]),
                HasButcher = (Boolean?)GetValue(dr[FieldNames.LocationHasButcher]),
                HasPizza = (Boolean?)GetValue(dr[FieldNames.LocationHasPizza]),
                HasDeli = (Boolean?)GetValue(dr[FieldNames.LocationHasDeli]),
                HasSaladBar = (Boolean?)GetValue(dr[FieldNames.LocationHasSaladBar]),
                HasOrganic = (Boolean?)GetValue(dr[FieldNames.LocationHasOrganic]),
                HasGrocery = (Boolean?)GetValue(dr[FieldNames.LocationHasGrocery]),
                HasMobilePhones = (Boolean?)GetValue(dr[FieldNames.LocationHasMobilePhones]),
                HasDryCleaning = (Boolean?)GetValue(dr[FieldNames.LocationHasDryCleaning]),
                HasHomeShoppingAvailable = (Boolean?)GetValue(dr[FieldNames.LocationHasHomeShoppingAvailable]),
                HasOptician = (Boolean?)GetValue(dr[FieldNames.LocationHasOptician]),
                HasPharmacy = (Boolean?)GetValue(dr[FieldNames.LocationHasPharmacy]),
                HasTravel = (Boolean?)GetValue(dr[FieldNames.LocationHasTravel]),
                HasPhotoDepartment = (Boolean?)GetValue(dr[FieldNames.LocationHasPhotoDepartment]),
                HasCarServiceArea = (Boolean?)GetValue(dr[FieldNames.LocationHasCarServiceArea]),
                HasGardenCentre = (Boolean?)GetValue(dr[FieldNames.LocationHasGardenCentre]),
                HasClinic = (Boolean?)GetValue(dr[FieldNames.LocationHasClinic]),
                HasAlcohol = (Boolean?)GetValue(dr[FieldNames.LocationHasAlcohol]),
                HasFashion = (Boolean?)GetValue(dr[FieldNames.LocationHasFashion]),
                HasCafe = (Boolean?)GetValue(dr[FieldNames.LocationHasCafe]),
                HasRecycling = (Boolean?)GetValue(dr[FieldNames.LocationHasRecycling]),
                HasPhotocopier = (Boolean?)GetValue(dr[FieldNames.LocationHasPhotocopier]),
                HasLottery = (Boolean?)GetValue(dr[FieldNames.LocationHasLottery]),
                HasPostOffice = (Boolean?)GetValue(dr[FieldNames.LocationHasPostOffice]),
                HasMovieRental = (Boolean?)GetValue(dr[FieldNames.LocationHasMovieRental]),
                HasJewellery = (Boolean?)GetValue(dr[FieldNames.LocationHasJewellery]),
                LocationType = (String)GetValue(dr[FieldNames.LocationLocationType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.LocationDateDeleted])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public LocationDto FetchById(Int16 id)
        {
            LocationDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationId, SqlDbType.SmallInt, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<LocationDto> FetchByEntityId(Int32 entityId)
        {
            List<LocationDto> dtoList = new List<LocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that match the specified location group id
        /// </summary>
        /// <param name="entityId">specified location group id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<LocationDto> FetchByLocationGroupId(Int32 locationGroupId)
        {
            List<LocationDto> dtoList = new List<LocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationFetchByLocationGroupId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.LocationLocationGroupId, SqlDbType.Int, locationGroupId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id including deleted items
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<LocationDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            List<LocationDto> dtoList = new List<LocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationFetchByEntityIdIncludingDeleted))
                {
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(LocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationId, SqlDbType.SmallInt);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.LocationRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LocationLocationGroupId, SqlDbType.Int, dto.LocationGroupId);
                    CreateParameter(command, FieldNames.LocationCode, SqlDbType.NVarChar, dto.Code);
                    CreateParameter(command, FieldNames.LocationName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LocationRegion, SqlDbType.NVarChar, dto.Region);
                    CreateParameter(command, FieldNames.LocationTVRegion, SqlDbType.NVarChar, dto.TVRegion);
                    CreateParameter(command, FieldNames.LocationLocation, SqlDbType.NVarChar, dto.Location);
                    CreateParameter(command, FieldNames.LocationDefaultClusterAttribute, SqlDbType.NVarChar, dto.DefaultClusterAttribute);
                    CreateParameter(command, FieldNames.LocationAddress1, SqlDbType.NVarChar, dto.Address1);
                    CreateParameter(command, FieldNames.LocationAddress2, SqlDbType.NVarChar, dto.Address2);
                    CreateParameter(command, FieldNames.LocationCity, SqlDbType.NVarChar, dto.City);
                    CreateParameter(command, FieldNames.LocationCounty, SqlDbType.NVarChar, dto.County);
                    CreateParameter(command, FieldNames.LocationState, SqlDbType.NVarChar, dto.State);
                    CreateParameter(command, FieldNames.LocationPostalCode, SqlDbType.NVarChar, dto.PostalCode);
                    CreateParameter(command, FieldNames.LocationCountry, SqlDbType.NVarChar, dto.Country);
                    CreateParameter(command, FieldNames.LocationLongitude, SqlDbType.Real, dto.Longitude);
                    CreateParameter(command, FieldNames.LocationLatitude, SqlDbType.Real, dto.Latitude);
                    CreateParameter(command, FieldNames.LocationDateOpen, SqlDbType.Date, dto.DateOpen);
                    CreateParameter(command, FieldNames.LocationDateLastRefitted, SqlDbType.Date, dto.DateLastRefitted);
                    CreateParameter(command, FieldNames.LocationDateClosed, SqlDbType.Date, dto.DateClosed);
                    CreateParameter(command, FieldNames.LocationCarParkSpaces, SqlDbType.SmallInt, dto.CarParkSpaces);
                    CreateParameter(command, FieldNames.LocationCarParkManagement, SqlDbType.NVarChar, dto.CarParkManagement);
                    CreateParameter(command, FieldNames.LocationPetrolForecourtType, SqlDbType.NVarChar, dto.PetrolForecourtType);
                    CreateParameter(command, FieldNames.LocationRestaurant, SqlDbType.NVarChar, dto.Restaurant);
                    CreateParameter(command, FieldNames.LocationSizeGrossFloorArea, SqlDbType.Int, dto.SizeGrossFloorArea);
                    CreateParameter(command, FieldNames.LocationSizeNetSalesArea, SqlDbType.Int, dto.SizeNetSalesArea);
                    CreateParameter(command, FieldNames.LocationSizeMezzSalesArea, SqlDbType.Int, dto.SizeMezzSalesArea);
                    CreateParameter(command, FieldNames.LocationTelephoneCountryCode, SqlDbType.NVarChar, dto.TelephoneCountryCode);
                    CreateParameter(command, FieldNames.LocationTelephoneAreaCode, SqlDbType.NVarChar, dto.TelephoneAreaCode);
                    CreateParameter(command, FieldNames.LocationTelephoneNumber, SqlDbType.NVarChar, dto.TelephoneNumber);
                    CreateParameter(command, FieldNames.LocationFaxCountryCode, SqlDbType.NVarChar, dto.FaxCountryCode);
                    CreateParameter(command, FieldNames.LocationFaxAreaCode, SqlDbType.NVarChar, dto.FaxAreaCode);
                    CreateParameter(command, FieldNames.LocationFaxNumber, SqlDbType.NVarChar, dto.FaxNumber);
                    CreateParameter(command, FieldNames.LocationOpeningHours, SqlDbType.NVarChar, dto.OpeningHours);
                    CreateParameter(command, FieldNames.LocationAverageOpeningHours, SqlDbType.Real, dto.AverageOpeningHours);
                    CreateParameter(command, FieldNames.LocationManagerName, SqlDbType.NVarChar, dto.ManagerName);
                    CreateParameter(command, FieldNames.LocationManagerEmail, SqlDbType.NVarChar, dto.ManagerEmail);
                    CreateParameter(command, FieldNames.LocationRegionalManagerName, SqlDbType.NVarChar, dto.RegionalManagerName);
                    CreateParameter(command, FieldNames.LocationRegionalManagerEmail, SqlDbType.NVarChar, dto.RegionalManagerEmail);
                    CreateParameter(command, FieldNames.LocationDivisionalManagerName, SqlDbType.NVarChar, dto.DivisionalManagerName);
                    CreateParameter(command, FieldNames.LocationDivisionalManagerEmail, SqlDbType.NVarChar, dto.DivisionalManagerEmail);
                    CreateParameter(command, FieldNames.LocationAdvertisingZone, SqlDbType.NVarChar, dto.AdvertisingZone);
                    CreateParameter(command, FieldNames.LocationDistributionCentre, SqlDbType.NVarChar, dto.DistributionCentre);
                    CreateParameter(command, FieldNames.LocationNoOfCheckouts, SqlDbType.TinyInt, dto.NoOfCheckouts);
                    CreateParameter(command, FieldNames.LocationIsMezzFitted, SqlDbType.Bit, dto.IsMezzFitted);
                    CreateParameter(command, FieldNames.LocationIsFreehold, SqlDbType.Bit, dto.IsFreehold);
                    CreateParameter(command, FieldNames.LocationIs24Hours, SqlDbType.Bit, dto.Is24Hours);
                    CreateParameter(command, FieldNames.LocationIsOpenMonday, SqlDbType.Bit, dto.IsOpenMonday);
                    CreateParameter(command, FieldNames.LocationIsOpenTuesday, SqlDbType.Bit, dto.IsOpenTuesday);
                    CreateParameter(command, FieldNames.LocationIsOpenWednesday, SqlDbType.Bit, dto.IsOpenWednesday);
                    CreateParameter(command, FieldNames.LocationIsOpenThursday, SqlDbType.Bit, dto.IsOpenThursday);
                    CreateParameter(command, FieldNames.LocationIsOpenFriday, SqlDbType.Bit, dto.IsOpenFriday);
                    CreateParameter(command, FieldNames.LocationIsOpenSaturday, SqlDbType.Bit, dto.IsOpenSaturday);
                    CreateParameter(command, FieldNames.LocationIsOpenSunday, SqlDbType.Bit, dto.IsOpenSunday);
                    CreateParameter(command, FieldNames.LocationHasPetrolForecourt, SqlDbType.Bit, dto.HasPetrolForecourt);
                    CreateParameter(command, FieldNames.LocationHasNewsCube, SqlDbType.Bit, dto.HasNewsCube);
                    CreateParameter(command, FieldNames.LocationHasAtmMachines, SqlDbType.Bit, dto.HasAtmMachines);
                    CreateParameter(command, FieldNames.LocationHasCustomerWC, SqlDbType.Bit, dto.HasCustomerWC);
                    CreateParameter(command, FieldNames.LocationHasBabyChanging, SqlDbType.Bit, dto.HasBabyChanging);
                    CreateParameter(command, FieldNames.LocationHasInStoreBakery, SqlDbType.Bit, dto.HasInStoreBakery);
                    CreateParameter(command, FieldNames.LocationHasHotFoodToGo, SqlDbType.Bit, dto.HasHotFoodToGo);
                    CreateParameter(command, FieldNames.LocationHasRotisserie, SqlDbType.Bit, dto.HasRotisserie);
                    CreateParameter(command, FieldNames.LocationHasFishmonger, SqlDbType.Bit, dto.HasFishmonger);
                    CreateParameter(command, FieldNames.LocationHasButcher, SqlDbType.Bit, dto.HasButcher);
                    CreateParameter(command, FieldNames.LocationHasPizza, SqlDbType.Bit, dto.HasPizza);
                    CreateParameter(command, FieldNames.LocationHasDeli, SqlDbType.Bit, dto.HasDeli);
                    CreateParameter(command, FieldNames.LocationHasSaladBar, SqlDbType.Bit, dto.HasSaladBar);
                    CreateParameter(command, FieldNames.LocationHasOrganic, SqlDbType.Bit, dto.HasOrganic);
                    CreateParameter(command, FieldNames.LocationHasGrocery, SqlDbType.Bit, dto.HasGrocery);
                    CreateParameter(command, FieldNames.LocationHasMobilePhones, SqlDbType.Bit, dto.HasMobilePhones);
                    CreateParameter(command, FieldNames.LocationHasDryCleaning, SqlDbType.Bit, dto.HasDryCleaning);
                    CreateParameter(command, FieldNames.LocationHasHomeShoppingAvailable, SqlDbType.Bit, dto.HasHomeShoppingAvailable);
                    CreateParameter(command, FieldNames.LocationHasOptician, SqlDbType.Bit, dto.HasOptician);
                    CreateParameter(command, FieldNames.LocationHasPharmacy, SqlDbType.Bit, dto.HasPharmacy);
                    CreateParameter(command, FieldNames.LocationHasTravel, SqlDbType.Bit, dto.HasTravel);
                    CreateParameter(command, FieldNames.LocationHasPhotoDepartment, SqlDbType.Bit, dto.HasPhotoDepartment);
                    CreateParameter(command, FieldNames.LocationHasCarServiceArea, SqlDbType.Bit, dto.HasCarServiceArea);
                    CreateParameter(command, FieldNames.LocationHasGardenCentre, SqlDbType.Bit, dto.HasGardenCentre);
                    CreateParameter(command, FieldNames.LocationHasClinic, SqlDbType.Bit, dto.HasClinic);
                    CreateParameter(command, FieldNames.LocationHasAlcohol, SqlDbType.Bit, dto.HasAlcohol);
                    CreateParameter(command, FieldNames.LocationHasFashion, SqlDbType.Bit, dto.HasFashion);
                    CreateParameter(command, FieldNames.LocationHasCafe, SqlDbType.Bit, dto.HasCafe);
                    CreateParameter(command, FieldNames.LocationHasRecycling, SqlDbType.Bit, dto.HasRecycling);
                    CreateParameter(command, FieldNames.LocationHasPhotocopier, SqlDbType.Bit, dto.HasPhotocopier);
                    CreateParameter(command, FieldNames.LocationHasLottery, SqlDbType.Bit, dto.HasLottery);
                    CreateParameter(command, FieldNames.LocationHasPostOffice, SqlDbType.Bit, dto.HasPostOffice);
                    CreateParameter(command, FieldNames.LocationHasMovieRental, SqlDbType.Bit, dto.HasMovieRental);
                    CreateParameter(command, FieldNames.LocationHasJewellery, SqlDbType.Bit, dto.HasJewellery);
                    CreateParameter(command, FieldNames.LocationLocationType, SqlDbType.NVarChar, dto.LocationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int16)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(LocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationId, SqlDbType.SmallInt, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.LocationRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LocationLocationGroupId, SqlDbType.Int, dto.LocationGroupId);
                    CreateParameter(command, FieldNames.LocationCode, SqlDbType.NVarChar, dto.Code);
                    CreateParameter(command, FieldNames.LocationName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LocationRegion, SqlDbType.NVarChar, dto.Region);
                    CreateParameter(command, FieldNames.LocationTVRegion, SqlDbType.NVarChar, dto.TVRegion);
                    CreateParameter(command, FieldNames.LocationLocation, SqlDbType.NVarChar, dto.Location);
                    CreateParameter(command, FieldNames.LocationDefaultClusterAttribute, SqlDbType.NVarChar, dto.DefaultClusterAttribute);
                    CreateParameter(command, FieldNames.LocationAddress1, SqlDbType.NVarChar, dto.Address1);
                    CreateParameter(command, FieldNames.LocationAddress2, SqlDbType.NVarChar, dto.Address2);
                    CreateParameter(command, FieldNames.LocationCity, SqlDbType.NVarChar, dto.City);
                    CreateParameter(command, FieldNames.LocationCounty, SqlDbType.NVarChar, dto.County);
                    CreateParameter(command, FieldNames.LocationState, SqlDbType.NVarChar, dto.State);
                    CreateParameter(command, FieldNames.LocationPostalCode, SqlDbType.NVarChar, dto.PostalCode);
                    CreateParameter(command, FieldNames.LocationCountry, SqlDbType.NVarChar, dto.Country);
                    CreateParameter(command, FieldNames.LocationLongitude, SqlDbType.Real, dto.Longitude);
                    CreateParameter(command, FieldNames.LocationLatitude, SqlDbType.Real, dto.Latitude);
                    CreateParameter(command, FieldNames.LocationDateOpen, SqlDbType.Date, dto.DateOpen);
                    CreateParameter(command, FieldNames.LocationDateLastRefitted, SqlDbType.Date, dto.DateLastRefitted);
                    CreateParameter(command, FieldNames.LocationDateClosed, SqlDbType.Date, dto.DateClosed);
                    CreateParameter(command, FieldNames.LocationCarParkSpaces, SqlDbType.SmallInt, dto.CarParkSpaces);
                    CreateParameter(command, FieldNames.LocationCarParkManagement, SqlDbType.NVarChar, dto.CarParkManagement);
                    CreateParameter(command, FieldNames.LocationPetrolForecourtType, SqlDbType.NVarChar, dto.PetrolForecourtType);
                    CreateParameter(command, FieldNames.LocationRestaurant, SqlDbType.NVarChar, dto.Restaurant);
                    CreateParameter(command, FieldNames.LocationSizeGrossFloorArea, SqlDbType.Int, dto.SizeGrossFloorArea);
                    CreateParameter(command, FieldNames.LocationSizeNetSalesArea, SqlDbType.Int, dto.SizeNetSalesArea);
                    CreateParameter(command, FieldNames.LocationSizeMezzSalesArea, SqlDbType.Int, dto.SizeMezzSalesArea);
                    CreateParameter(command, FieldNames.LocationTelephoneCountryCode, SqlDbType.NVarChar, dto.TelephoneCountryCode);
                    CreateParameter(command, FieldNames.LocationTelephoneAreaCode, SqlDbType.NVarChar, dto.TelephoneAreaCode);
                    CreateParameter(command, FieldNames.LocationTelephoneNumber, SqlDbType.NVarChar, dto.TelephoneNumber);
                    CreateParameter(command, FieldNames.LocationFaxCountryCode, SqlDbType.NVarChar, dto.FaxCountryCode);
                    CreateParameter(command, FieldNames.LocationFaxAreaCode, SqlDbType.NVarChar, dto.FaxAreaCode);
                    CreateParameter(command, FieldNames.LocationFaxNumber, SqlDbType.NVarChar, dto.FaxNumber);
                    CreateParameter(command, FieldNames.LocationOpeningHours, SqlDbType.NVarChar, dto.OpeningHours);
                    CreateParameter(command, FieldNames.LocationAverageOpeningHours, SqlDbType.Real, dto.AverageOpeningHours);
                    CreateParameter(command, FieldNames.LocationManagerName, SqlDbType.NVarChar, dto.ManagerName);
                    CreateParameter(command, FieldNames.LocationManagerEmail, SqlDbType.NVarChar, dto.ManagerEmail);
                    CreateParameter(command, FieldNames.LocationRegionalManagerName, SqlDbType.NVarChar, dto.RegionalManagerName);
                    CreateParameter(command, FieldNames.LocationRegionalManagerEmail, SqlDbType.NVarChar, dto.RegionalManagerEmail);
                    CreateParameter(command, FieldNames.LocationDivisionalManagerName, SqlDbType.NVarChar, dto.DivisionalManagerName);
                    CreateParameter(command, FieldNames.LocationDivisionalManagerEmail, SqlDbType.NVarChar, dto.DivisionalManagerEmail);
                    CreateParameter(command, FieldNames.LocationAdvertisingZone, SqlDbType.NVarChar, dto.AdvertisingZone);
                    CreateParameter(command, FieldNames.LocationDistributionCentre, SqlDbType.NVarChar, dto.DistributionCentre);
                    CreateParameter(command, FieldNames.LocationNoOfCheckouts, SqlDbType.TinyInt, dto.NoOfCheckouts);
                    CreateParameter(command, FieldNames.LocationIsMezzFitted, SqlDbType.Bit, dto.IsMezzFitted);
                    CreateParameter(command, FieldNames.LocationIsFreehold, SqlDbType.Bit, dto.IsFreehold);
                    CreateParameter(command, FieldNames.LocationIs24Hours, SqlDbType.Bit, dto.Is24Hours);
                    CreateParameter(command, FieldNames.LocationIsOpenMonday, SqlDbType.Bit, dto.IsOpenMonday);
                    CreateParameter(command, FieldNames.LocationIsOpenTuesday, SqlDbType.Bit, dto.IsOpenTuesday);
                    CreateParameter(command, FieldNames.LocationIsOpenWednesday, SqlDbType.Bit, dto.IsOpenWednesday);
                    CreateParameter(command, FieldNames.LocationIsOpenThursday, SqlDbType.Bit, dto.IsOpenThursday);
                    CreateParameter(command, FieldNames.LocationIsOpenFriday, SqlDbType.Bit, dto.IsOpenFriday);
                    CreateParameter(command, FieldNames.LocationIsOpenSaturday, SqlDbType.Bit, dto.IsOpenSaturday);
                    CreateParameter(command, FieldNames.LocationIsOpenSunday, SqlDbType.Bit, dto.IsOpenSunday);
                    CreateParameter(command, FieldNames.LocationHasPetrolForecourt, SqlDbType.Bit, dto.HasPetrolForecourt);
                    CreateParameter(command, FieldNames.LocationHasNewsCube, SqlDbType.Bit, dto.HasNewsCube);
                    CreateParameter(command, FieldNames.LocationHasAtmMachines, SqlDbType.Bit, dto.HasAtmMachines);
                    CreateParameter(command, FieldNames.LocationHasCustomerWC, SqlDbType.Bit, dto.HasCustomerWC);
                    CreateParameter(command, FieldNames.LocationHasBabyChanging, SqlDbType.Bit, dto.HasBabyChanging);
                    CreateParameter(command, FieldNames.LocationHasInStoreBakery, SqlDbType.Bit, dto.HasInStoreBakery);
                    CreateParameter(command, FieldNames.LocationHasHotFoodToGo, SqlDbType.Bit, dto.HasHotFoodToGo);
                    CreateParameter(command, FieldNames.LocationHasRotisserie, SqlDbType.Bit, dto.HasRotisserie);
                    CreateParameter(command, FieldNames.LocationHasFishmonger, SqlDbType.Bit, dto.HasFishmonger);
                    CreateParameter(command, FieldNames.LocationHasButcher, SqlDbType.Bit, dto.HasButcher);
                    CreateParameter(command, FieldNames.LocationHasPizza, SqlDbType.Bit, dto.HasPizza);
                    CreateParameter(command, FieldNames.LocationHasDeli, SqlDbType.Bit, dto.HasDeli);
                    CreateParameter(command, FieldNames.LocationHasSaladBar, SqlDbType.Bit, dto.HasSaladBar);
                    CreateParameter(command, FieldNames.LocationHasOrganic, SqlDbType.Bit, dto.HasOrganic);
                    CreateParameter(command, FieldNames.LocationHasGrocery, SqlDbType.Bit, dto.HasGrocery);
                    CreateParameter(command, FieldNames.LocationHasMobilePhones, SqlDbType.Bit, dto.HasMobilePhones);
                    CreateParameter(command, FieldNames.LocationHasDryCleaning, SqlDbType.Bit, dto.HasDryCleaning);
                    CreateParameter(command, FieldNames.LocationHasHomeShoppingAvailable, SqlDbType.Bit, dto.HasHomeShoppingAvailable);
                    CreateParameter(command, FieldNames.LocationHasOptician, SqlDbType.Bit, dto.HasOptician);
                    CreateParameter(command, FieldNames.LocationHasPharmacy, SqlDbType.Bit, dto.HasPharmacy);
                    CreateParameter(command, FieldNames.LocationHasTravel, SqlDbType.Bit, dto.HasTravel);
                    CreateParameter(command, FieldNames.LocationHasPhotoDepartment, SqlDbType.Bit, dto.HasPhotoDepartment);
                    CreateParameter(command, FieldNames.LocationHasCarServiceArea, SqlDbType.Bit, dto.HasCarServiceArea);
                    CreateParameter(command, FieldNames.LocationHasGardenCentre, SqlDbType.Bit, dto.HasGardenCentre);
                    CreateParameter(command, FieldNames.LocationHasClinic, SqlDbType.Bit, dto.HasClinic);
                    CreateParameter(command, FieldNames.LocationHasAlcohol, SqlDbType.Bit, dto.HasAlcohol);
                    CreateParameter(command, FieldNames.LocationHasFashion, SqlDbType.Bit, dto.HasFashion);
                    CreateParameter(command, FieldNames.LocationHasCafe, SqlDbType.Bit, dto.HasCafe);
                    CreateParameter(command, FieldNames.LocationHasRecycling, SqlDbType.Bit, dto.HasRecycling);
                    CreateParameter(command, FieldNames.LocationHasPhotocopier, SqlDbType.Bit, dto.HasPhotocopier);
                    CreateParameter(command, FieldNames.LocationHasLottery, SqlDbType.Bit, dto.HasLottery);
                    CreateParameter(command, FieldNames.LocationHasPostOffice, SqlDbType.Bit, dto.HasPostOffice);
                    CreateParameter(command, FieldNames.LocationHasMovieRental, SqlDbType.Bit, dto.HasMovieRental);
                    CreateParameter(command, FieldNames.LocationHasJewellery, SqlDbType.Bit, dto.HasJewellery);
                    CreateParameter(command, FieldNames.LocationLocationType, SqlDbType.NVarChar, dto.LocationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Upsert
        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<LocationDto> dtoList, LocationIsSetDto isSetDto, Boolean updateDeleted)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new LocationIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationDtoKey, LocationDto> keyedDtoList = new Dictionary<LocationDtoKey, LocationDto>();
            foreach (LocationDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationBulkCopyDataReader dr = new LocationBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<LocationDtoKey, Tuple<Int16, RowVersion>> ids = new Dictionary<LocationDtoKey, Tuple<Int16, RowVersion>>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.LocationSetProperties, SqlDbType.NVarChar, setProperties);
                    CreateParameter(command, FieldNames.LocationUpdateDeleted, SqlDbType.Bit, updateDeleted);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Tuple<Int16, RowVersion> outputData =
                                new Tuple<Int16, RowVersion>
                                    (
                                        (Int16)GetValue(dr[FieldNames.LocationId]),
                                        new RowVersion(GetValue(dr[FieldNames.LocationRowVersion]))
                                    );

                            LocationDtoKey key = new LocationDtoKey();
                            key.EntityId = (Int32)GetValue(dr[FieldNames.LocationEntityId]);
                            key.Code = (String)GetValue(dr[FieldNames.LocationCode]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationDto dto in dtoList)
                {
                    Tuple<Int16, RowVersion> outputData = null;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != null)
                    {
                        dto.Id = outputData.Item1;
                        dto.RowVersion = outputData.Item2;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private String GenerateSetPropertiesString(LocationIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsEntityIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationEntityId);
            }
            if (isSetDto.IsLocationGroupIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationLocationGroupId);
            }
            if (isSetDto.IsCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationCode);
            }
            if (isSetDto.IsNameSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationName);
            }
            if (isSetDto.IsRegionSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationRegion);
            }
            if (isSetDto.IsTVRegionSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationTVRegion);
            }
            if (isSetDto.IsLocationSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationLocation);
            }
            if (isSetDto.IsDefaultClusterAttributeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationDefaultClusterAttribute);
            }
            if (isSetDto.IsAddress1Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationAddress1);
            }
            if (isSetDto.IsAddress2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationAddress2);
            }
            if (isSetDto.IsCitySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationCity);
            }
            if (isSetDto.IsCountySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationCounty);
            }
            if (isSetDto.IsStateSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationState);
            }
            if (isSetDto.IsPostalCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationPostalCode);
            }
            if (isSetDto.IsCountrySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationCountry);
            }
            if (isSetDto.IsLongitudeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationLongitude);
            }
            if (isSetDto.IsLatitudeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationLatitude);
            }
            if (isSetDto.IsDateOpenSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationDateOpen);
            }
            if (isSetDto.IsDateLastRefittedSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationDateLastRefitted);
            }
            if (isSetDto.IsDateClosedSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationDateClosed);
            }
            if (isSetDto.IsCarParkSpacesSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationCarParkSpaces);
            }
            if (isSetDto.IsCarParkManagementSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationCarParkManagement);
            }
            if (isSetDto.IsPetrolForecourtTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationPetrolForecourtType);
            }
            if (isSetDto.IsRestaurantSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationRestaurant);
            }
            if (isSetDto.IsSizeGrossFloorAreaSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationSizeGrossFloorArea);
            }
            if (isSetDto.IsSizeNetSalesAreaSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationSizeNetSalesArea);
            }
            if (isSetDto.IsSizeMezzSalesAreaSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationSizeMezzSalesArea);
            }
            if (isSetDto.IsTelephoneCountryCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationTelephoneCountryCode);
            }
            if (isSetDto.IsTelephoneAreaCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationTelephoneAreaCode);
            }
            if (isSetDto.IsTelephoneNumberSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationTelephoneNumber);
            }
            if (isSetDto.IsFaxCountryCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationFaxCountryCode);
            }
            if (isSetDto.IsFaxAreaCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationFaxAreaCode);
            }
            if (isSetDto.IsFaxNumberSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationFaxNumber);
            }
            if (isSetDto.IsOpeningHoursSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationOpeningHours);
            }
            if (isSetDto.IsAverageOpeningHoursSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationAverageOpeningHours);
            }
            if (isSetDto.IsManagerNameSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationManagerName);
            }
            if (isSetDto.IsManagerEmailSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationManagerEmail);
            }
            if (isSetDto.IsRegionalManagerNameSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationRegionalManagerName);
            }
            if (isSetDto.IsRegionalManagerEmailSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationRegionalManagerEmail);
            }
            if (isSetDto.IsDivisionalManagerNameSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationDivisionalManagerName);
            }
            if (isSetDto.IsDivisionalManagerEmailSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationDivisionalManagerEmail);
            }
            if (isSetDto.IsAdvertisingZoneSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationAdvertisingZone);
            }
            if (isSetDto.IsDistributionCentreSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationDistributionCentre);
            }
            if (isSetDto.IsNoOfCheckoutsSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationNoOfCheckouts);
            }
            if (isSetDto.IsIsMezzFittedSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsMezzFitted);
            }
            if (isSetDto.IsIsFreeholdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsFreehold);
            }
            if (isSetDto.IsIs24HoursSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIs24Hours);
            }
            if (isSetDto.IsIsOpenMondaySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsOpenMonday);
            }
            if (isSetDto.IsIsOpenTuesdaySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsOpenTuesday);
            }
            if (isSetDto.IsIsOpenWednesdaySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsOpenWednesday);
            }
            if (isSetDto.IsIsOpenThursdaySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsOpenThursday);
            }
            if (isSetDto.IsIsOpenFridaySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsOpenFriday);
            }
            if (isSetDto.IsIsOpenSaturdaySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsOpenSaturday);
            }
            if (isSetDto.IsIsOpenSundaySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationIsOpenSunday);
            }
            if (isSetDto.IsHasPetrolForecourtSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasPetrolForecourt);
            }
            if (isSetDto.IsHasNewsCubeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasNewsCube);
            }
            if (isSetDto.IsHasAtmMachinesSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasAtmMachines);
            }
            if (isSetDto.IsHasCustomerWCSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasCustomerWC);
            }
            if (isSetDto.IsHasBabyChangingSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasBabyChanging);
            }
            if (isSetDto.IsHasInStoreBakerySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasInStoreBakery);
            }
            if (isSetDto.IsHasHotFoodToGoSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasHotFoodToGo);
            }
            if (isSetDto.IsHasRotisserieSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasRotisserie);
            }
            if (isSetDto.IsHasFishmongerSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasFishmonger);
            }
            if (isSetDto.IsHasButcherSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasButcher);
            }
            if (isSetDto.IsHasPizzaSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasPizza);
            }
            if (isSetDto.IsHasDeliSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasDeli);
            }
            if (isSetDto.IsHasSaladBarSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasSaladBar);
            }
            if (isSetDto.IsHasOrganicSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasOrganic);
            }
            if (isSetDto.IsHasGrocerySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasGrocery);
            }
            if (isSetDto.IsHasMobilePhonesSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasMobilePhones);
            }
            if (isSetDto.IsHasDryCleaningSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasDryCleaning);
            }
            if (isSetDto.IsHasHomeShoppingAvailableSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasHomeShoppingAvailable);
            }
            if (isSetDto.IsHasOpticianSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasOptician);
            }
            if (isSetDto.IsHasPharmacySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasPharmacy);
            }
            if (isSetDto.IsHasTravelSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasTravel);
            }
            if (isSetDto.IsHasPhotoDepartmentSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasPhotoDepartment);
            }
            if (isSetDto.IsHasCarServiceAreaSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasCarServiceArea);
            }
            if (isSetDto.IsHasGardenCentreSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasGardenCentre);
            }
            if (isSetDto.IsHasClinicSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasClinic);
            }
            if (isSetDto.IsHasAlcoholSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasAlcohol);
            }
            if (isSetDto.IsHasFashionSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasFashion);
            }
            if (isSetDto.IsHasCafeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasCafe);
            }
            if (isSetDto.IsHasRecyclingSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasRecycling);
            }
            if (isSetDto.IsHasPhotocopierSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasPhotocopier);
            }
            if (isSetDto.IsHasLotterySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasLottery);
            }
            if (isSetDto.IsHasPostOfficeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasPostOffice);
            }
            if (isSetDto.IsHasMovieRentalSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasMovieRental);
            }
            if (isSetDto.IsHasJewellerySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationHasJewellery);
            }
            if (isSetDto.IsLocationTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.LocationLocationType);
            }

            return setProperties;
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int16 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationId, SqlDbType.SmallInt, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes dtos that match the specified entity id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationDeleteByEntityId))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.Int, entityId);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}