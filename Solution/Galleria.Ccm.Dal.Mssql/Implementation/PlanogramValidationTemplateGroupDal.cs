﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//  Created (Auto-generated)
// V8-26954 : A.Silva
//  Added ResultType.
// V8-26812 : A.Silva
//  Added ValidationType.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramValidationTemplateGroup Dal
    /// </summary>
    public class PlanogramValidationTemplateGroupDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramValidationTemplateGroupDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramValidationTemplateGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramValidationTemplateGroupDto
            {
                Id = (Int32) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupId]),
                PlanogramValidationTemplateId =
                    (Int32) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId]),
                Name = (String) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupName]),
                Threshold1 = (Single) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupThreshold1]),
                Threshold2 = (Single) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupThreshold2]),
                ResultType = (Byte) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupResultType]),
                ValidationType = (Byte) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupValidationType])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a dto that matches the specified id
        /// </summary>
        /// <param name="planogramValidationTemplateId">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramValidationTemplateGroupDto> FetchByPlanogramValidationTemplateId(
            object planogramValidationTemplateId)
        {
            var dtoList = new List<PlanogramValidationTemplateGroupDto>();
            try
            {
                using (
                    var command =
                        CreateCommand(
                            ProcedureNames.PlanogramValidationTemplateGroupFetchByPlanogramValidationTemplateId)
                    )
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId,
                        SqlDbType.Int,
                        (Int32) planogramValidationTemplateId);

                    // Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramValidationTemplateGroupDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateGroupInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupId,
                        SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId,
                        SqlDbType.Int,
                        dto.PlanogramValidationTemplateId);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupName, SqlDbType.NVarChar,
                        dto.Name);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupResultType, SqlDbType.TinyInt,
                        dto.ResultType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupValidationType,
                        SqlDbType.TinyInt,
                        dto.ValidationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32) idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramValidationTemplateGroupDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateGroupUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupPlanogramValidationTemplateId,
                        SqlDbType.Int,
                        dto.PlanogramValidationTemplateId);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupName, SqlDbType.NVarChar,
                        dto.Name);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupResultType, SqlDbType.TinyInt,
                        dto.ResultType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupValidationType,
                        SqlDbType.TinyInt,
                        dto.ValidationType);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramValidationTemplateGroupDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}