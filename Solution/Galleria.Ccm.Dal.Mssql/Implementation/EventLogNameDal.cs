﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// EventLogName Dal
    /// </summary>
    public class EventLogNameDal : Galleria.Framework.Dal.Mssql.DalBase, IEventLogNameDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public EventLogNameDto GetDataTransferObject(SqlDataReader dr)
        {
            return new EventLogNameDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.EventLogNameId]),
                Name = (String)GetValue(dr[FieldNames.EventLogNameName])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all entity infos in the database
        /// </summary>
        /// <returns>A list of event log dtos</returns>
        public IEnumerable<EventLogNameDto> FetchAll()
        {
            List<EventLogNameDto> dtoList = new List<EventLogNameDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.EventLogNameFetchAll))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a single event log name record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public EventLogNameDto FetchById(int id)
        {
            EventLogNameDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogNameFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.EventLogNameId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a single event log name record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public EventLogNameDto FetchByName(String name)
        {
            EventLogNameDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogNameFetchByName))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.EventLogNameName,
                        SqlDbType.NVarChar,
                        name);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(EventLogNameDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogNameInsert))
                {
                    // id
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.EventLogNameId,
                        SqlDbType.Int);

                    //Source
                    CreateParameter(command,
                        FieldNames.EventLogNameName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
