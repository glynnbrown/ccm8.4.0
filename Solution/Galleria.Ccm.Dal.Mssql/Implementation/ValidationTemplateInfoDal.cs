﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Added the names of the procedures for FetchByEntityId and FetchByEntityIdIncludingDeleted.

#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed FetchByEntityIdIncludingDeleted
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class ValidationTemplateInfoDal : Framework.Dal.Mssql.DalBase, IValidationTemplateInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static ValidationTemplateInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ValidationTemplateInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ValidationTemplateId]),
                EntityId = (Int32)GetValue(dr[FieldNames.ValidationTemplateEntityId]),
                Name = (String)GetValue(dr[FieldNames.ValidationTemplateName])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ValidationTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {

            List<ValidationTemplateInfoDto> dtos = new List<ValidationTemplateInfoDto>();

            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.ValidationTemplateEntityId, SqlDbType.Int, entityId);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtos;
        }


        #endregion
    }
}
