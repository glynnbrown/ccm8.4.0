﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#region Version History : CCM 820
// V8-30765 :I.George
//	Added PlanogramBlockingGroup metaData Properties
#endregion
#region Version History : CCM 830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramBlockingGroup Dal
    /// </summary>
    public class PlanogramBlockingGroupDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramBlockingGroupDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramBlockingGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramBlockingGroupDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramBlockingGroupId]),
                PlanogramBlockingId = (Int32)GetValue(dr[FieldNames.PlanogramBlockingGroupPlanogramBlockingId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramBlockingGroupName]),
                Colour = (Int32)GetValue(dr[FieldNames.PlanogramBlockingGroupColour]),
                FillPatternType = (Byte)GetValue(dr[FieldNames.PlanogramBlockingGroupFillPatternType]),
                CanCompromiseSequence = (Boolean)GetValue(dr[FieldNames.PlanogramBlockingGroupCanCompromiseSequence]),
                IsRestrictedByComponentType = (Boolean)GetValue(dr[FieldNames.PlanogramBlockingGroupIsRestrictedByComponentType]),
                CanMerge = (Boolean)GetValue(dr[FieldNames.PlanogramBlockingGroupCanMerge]),
                IsLimited = (Boolean)GetValue(dr[FieldNames.PlanogramBlockingGroupIsLimited]),
                LimitedPercentage = (Single)GetValue(dr[FieldNames.PlanogramBlockingGroupLimitedPercentage]),
                BlockPlacementPrimaryType = (Byte)GetValue(dr[FieldNames.PlanogramBlockingGroupBlockPlacementPrimaryType]),
                BlockPlacementSecondaryType = (Byte)GetValue(dr[FieldNames.PlanogramBlockingGroupBlockPlacementSecondaryType]),
                BlockPlacementTertiaryType = (Byte)GetValue(dr[FieldNames.PlanogramBlockingGroupBlockPlacementTertiaryType]),
                TotalSpacePercentage = (Single)GetValue(dr[FieldNames.PlanogramBlockingGroupTotalSpacePercentage]),
                MetaCountOfProducts = (Int32?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaCountOfProducts]),
                MetaCountOfProductRecommendedOnPlanogram = (Int32?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram]),
                MetaCountOfProductPlacedOnPlanogram = (Int32?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram]),
                MetaOriginalPercentAllocated = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaOriginalPercentAllocated]),
                MetaPerformancePercentAllocated = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaPerformancePercentAllocated]),
                MetaFinalPercentAllocated = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaFinalPercentAllocated]),
                MetaLinearProductPlacementPercent = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaLinearProductPlacementPercent]),
                MetaAreaProductPlacementPercent = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaAreaProductPlacementPercent]),
                MetaVolumetricProductPlacementPercent = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaVolumetricProductPlacementPercent]),
                P1 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP1Performance]),
                P2 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP2Performance]),
                P3 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP3Performance]),
                P4 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP4Performance]),
                P5 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP5Performance]),
                P6 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP6Performance]),
                P7 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP7Performance]),
                P8 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP8Performance]),
                P9 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP9Performance]),
                P10 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP10Performance]),
                P11 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP11Performance]),
                P12 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP12Performance]),
                P13 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP13Performance]),
                P14 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP14Performance]),
                P15 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP15Performance]),
                P16 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP16Performance]),
                P17 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP17Performance]),
                P18 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP18Performance]),
                P19 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP19Performance]),
                P20 = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP20Performance]),
                MetaP1Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP1Percentage]),
                MetaP2Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP2Percentage]),
                MetaP3Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP3Percentage]),
                MetaP4Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP4Percentage]),
                MetaP5Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP5Percentage]),
                MetaP6Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP6Percentage]),
                MetaP7Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP7Percentage]),
                MetaP8Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP8Percentage]),
                MetaP9Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP9Percentage]),
                MetaP10Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP10Percentage]),
                MetaP11Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP11Percentage]),
                MetaP12Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP12Percentage]),
                MetaP13Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP13Percentage]),
                MetaP14Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP14Percentage]),
                MetaP15Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP15Percentage]),
                MetaP16Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP16Percentage]),
                MetaP17Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP17Percentage]),
                MetaP18Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP18Percentage]),
                MetaP19Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP19Percentage]),
                MetaP20Percentage = (Single?)GetValue(dr[FieldNames.PlanogramBlockingGroupMetaP20Percentage]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramBlockingGroupDto> FetchByPlanogramBlockingId(Object planogramBlockingId)
        {
            List<PlanogramBlockingGroupDto> dtoList = new List<PlanogramBlockingGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingGroupFetchByPlanogramBlockingId))
                {
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupPlanogramBlockingId, SqlDbType.Int, planogramBlockingId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        private void SetupBlockingGroupMetaPerformance(DalCommand command, PlanogramBlockingGroupDto dto)
        {
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP1Performance, SqlDbType.Real, dto.P1);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP2Performance, SqlDbType.Real, dto.P2);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP3Performance, SqlDbType.Real, dto.P3);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP4Performance, SqlDbType.Real, dto.P4);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP5Performance, SqlDbType.Real, dto.P5);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP6Performance, SqlDbType.Real, dto.P6);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP7Performance, SqlDbType.Real, dto.P7);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP8Performance, SqlDbType.Real, dto.P8);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP9Performance, SqlDbType.Real, dto.P9);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP10Performance, SqlDbType.Real, dto.P10);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP11Performance, SqlDbType.Real, dto.P11);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP12Performance, SqlDbType.Real, dto.P12);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP13Performance, SqlDbType.Real, dto.P13);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP14Performance, SqlDbType.Real, dto.P14);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP15Performance, SqlDbType.Real, dto.P15);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP16Performance, SqlDbType.Real, dto.P16);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP17Performance, SqlDbType.Real, dto.P17);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP18Performance, SqlDbType.Real, dto.P18);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP19Performance, SqlDbType.Real, dto.P19);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP20Performance, SqlDbType.Real, dto.P20);

            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP1Percentage, SqlDbType.Real, dto.MetaP1Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP2Percentage, SqlDbType.Real, dto.MetaP2Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP3Percentage, SqlDbType.Real, dto.MetaP3Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP4Percentage, SqlDbType.Real, dto.MetaP4Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP5Percentage, SqlDbType.Real, dto.MetaP5Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP6Percentage, SqlDbType.Real, dto.MetaP6Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP7Percentage, SqlDbType.Real, dto.MetaP7Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP8Percentage, SqlDbType.Real, dto.MetaP8Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP9Percentage, SqlDbType.Real, dto.MetaP9Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP10Percentage, SqlDbType.Real, dto.MetaP10Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP11Percentage, SqlDbType.Real, dto.MetaP11Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP12Percentage, SqlDbType.Real, dto.MetaP12Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP13Percentage, SqlDbType.Real, dto.MetaP13Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP14Percentage, SqlDbType.Real, dto.MetaP14Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP15Percentage, SqlDbType.Real, dto.MetaP15Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP16Percentage, SqlDbType.Real, dto.MetaP16Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP17Percentage, SqlDbType.Real, dto.MetaP17Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP18Percentage, SqlDbType.Real, dto.MetaP18Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP19Percentage, SqlDbType.Real, dto.MetaP19Percentage);
            CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaP20Percentage, SqlDbType.Real, dto.MetaP20Percentage);
        }

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramBlockingGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingGroupInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramBlockingGroupId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupPlanogramBlockingId, SqlDbType.Int, dto.PlanogramBlockingId);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupCanCompromiseSequence, SqlDbType.Bit, dto.CanCompromiseSequence);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupIsRestrictedByComponentType, SqlDbType.Bit, dto.IsRestrictedByComponentType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupCanMerge, SqlDbType.Bit, dto.CanMerge);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupIsLimited, SqlDbType.Bit, dto.IsLimited);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupLimitedPercentage, SqlDbType.Real, dto.LimitedPercentage);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupBlockPlacementPrimaryType, SqlDbType.TinyInt, dto.BlockPlacementPrimaryType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupBlockPlacementSecondaryType, SqlDbType.TinyInt, dto.BlockPlacementSecondaryType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupBlockPlacementTertiaryType, SqlDbType.TinyInt, dto.BlockPlacementTertiaryType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupTotalSpacePercentage, SqlDbType.Real, dto.TotalSpacePercentage);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaCountOfProducts, SqlDbType.Int, dto.MetaCountOfProducts);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram, SqlDbType.Int, dto.MetaCountOfProductRecommendedOnPlanogram);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram, SqlDbType.Int, dto.MetaCountOfProductPlacedOnPlanogram);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaOriginalPercentAllocated, SqlDbType.Real, dto.MetaOriginalPercentAllocated);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaPerformancePercentAllocated, SqlDbType.Real, dto.MetaPerformancePercentAllocated);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaFinalPercentAllocated, SqlDbType.Real, dto.MetaFinalPercentAllocated);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaLinearProductPlacementPercent, SqlDbType.Real, dto.MetaLinearProductPlacementPercent);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaAreaProductPlacementPercent, SqlDbType.Real, dto.MetaAreaProductPlacementPercent);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaVolumetricProductPlacementPercent, SqlDbType.Real, dto.MetaVolumetricProductPlacementPercent);

                    // Add in Meta performance parameters
                    SetupBlockingGroupMetaPerformance(command, dto);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            foreach (PlanogramBlockingGroupDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramBlockingGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingGroupUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupPlanogramBlockingId, SqlDbType.Int, dto.PlanogramBlockingId);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupCanCompromiseSequence, SqlDbType.Bit, dto.CanCompromiseSequence);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupIsRestrictedByComponentType, SqlDbType.Bit, dto.IsRestrictedByComponentType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupCanMerge, SqlDbType.Bit, dto.CanMerge);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupIsLimited, SqlDbType.Bit, dto.IsLimited);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupLimitedPercentage, SqlDbType.Real, dto.LimitedPercentage);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupBlockPlacementPrimaryType, SqlDbType.TinyInt, dto.BlockPlacementPrimaryType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupBlockPlacementSecondaryType, SqlDbType.TinyInt, dto.BlockPlacementSecondaryType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupBlockPlacementTertiaryType, SqlDbType.TinyInt, dto.BlockPlacementTertiaryType);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupTotalSpacePercentage, SqlDbType.Real, dto.TotalSpacePercentage);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaCountOfProducts, SqlDbType.Int, dto.MetaCountOfProducts);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram, SqlDbType.Int, dto.MetaCountOfProductRecommendedOnPlanogram);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram, SqlDbType.Int, dto.MetaCountOfProductPlacedOnPlanogram);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaOriginalPercentAllocated, SqlDbType.Real, dto.MetaOriginalPercentAllocated);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaPerformancePercentAllocated, SqlDbType.Real, dto.MetaPerformancePercentAllocated);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaFinalPercentAllocated, SqlDbType.Real, dto.MetaFinalPercentAllocated);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaLinearProductPlacementPercent, SqlDbType.Real, dto.MetaLinearProductPlacementPercent);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaAreaProductPlacementPercent, SqlDbType.Real, dto.MetaAreaProductPlacementPercent);
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupMetaVolumetricProductPlacementPercent, SqlDbType.Real, dto.MetaVolumetricProductPlacementPercent);

                    // Add in Meta performance parameters
                    SetupBlockingGroupMetaPerformance(command, dto);

                    //execute
                    command.ExecuteNonQuery();

                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            foreach (PlanogramBlockingGroupDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramBlockingGroupDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            foreach (PlanogramBlockingGroupDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}



