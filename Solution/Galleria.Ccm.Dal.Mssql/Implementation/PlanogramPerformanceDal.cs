﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26270 : A. Kuszyk
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramPerformanceDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramPerformanceDal
    {
        #region  Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private PlanogramPerformanceDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramPerformanceDto()
            {
                Id = (Object)GetValue(dr[FieldNames.PlanogramPerformanceId]),
                PlanogramId = (Object)GetValue(dr[FieldNames.PlanogramPerformancePlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramPerformanceName]),
                Description = (String)GetValue(dr[FieldNames.PlanogramPerformanceDescription])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PlanogramPerformanceDto FetchByPlanogramId(Object id)
        {
            PlanogramPerformanceDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramPerformancePlanogramId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramPerformanceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramPerformanceId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramPerformancePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDescription, SqlDbType.NVarChar, dto.Description);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramPerformanceDto> dtos)
        {
            foreach (PlanogramPerformanceDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramPerformanceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramPerformanceId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramPerformancePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDescription, SqlDbType.NVarChar, dto.Description);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramPerformanceDto> dtos)
        {
            foreach (PlanogramPerformanceDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramPerformanceId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramPerformanceDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramPerformanceDto> dtos)
        {
            foreach (PlanogramPerformanceDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
