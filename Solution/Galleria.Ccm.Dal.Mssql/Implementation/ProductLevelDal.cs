﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class ProductLevelDal : Galleria.Framework.Dal.Mssql.DalBase, IProductLevelDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private ProductLevelDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductLevelDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductLevelId]),
                ProductHierarchyId = (Int32)GetValue(dr[FieldNames.ProductLevelProductHierarchyId]),
                Name = (String)GetValue(dr[FieldNames.ProductLevelName]),
                ShapeNo = (Byte)GetValue(dr[FieldNames.ProductLevelShapeNo]),
                Colour = (Int32)GetValue(dr[FieldNames.ProductLevelColour]),
                ParentLevelId = (Int32?)GetValue(dr[FieldNames.ProductLevelParentLevelId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ProductLevelDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ProductLevelDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.ProductLevelDateDeleted])
            };
        }
        #endregion

        #region Fetch



        public IEnumerable<ProductLevelDto> FetchByProductHierarchyId(Int32 hierarchyId)
        {
            List<ProductLevelDto> dtoList = new List<ProductLevelDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductLevelFetchByProductHierarchyId))
                {
                    CreateParameter(command, FieldNames.ProductLevelProductHierarchyId, SqlDbType.Int, hierarchyId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public ProductLevelDto FetchById(Int32 id)
        {
            ProductLevelDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductLevelFetchById))
                {
                    CreateParameter(command, FieldNames.ProductLevelId, SqlDbType.Int, id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ProductLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductLevelInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductLevelId, SqlDbType.Int);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductLevelDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductLevelDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductLevelDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductLevelProductHierarchyId, SqlDbType.Int, dto.ProductHierarchyId);
                    CreateParameter(command, FieldNames.ProductLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductLevelShapeNo, SqlDbType.TinyInt, dto.ShapeNo);
                    CreateParameter(command, FieldNames.ProductLevelColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.ProductLevelParentLevelId, SqlDbType.Int, dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }


        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ProductLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductLevelUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLevelId, SqlDbType.Int, dto.Id);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductLevelDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductLevelDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductLevelDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductLevelProductHierarchyId, SqlDbType.Int, dto.ProductHierarchyId);
                    CreateParameter(command, FieldNames.ProductLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductLevelShapeNo, SqlDbType.TinyInt, dto.ShapeNo);
                    CreateParameter(command, FieldNames.ProductLevelColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.ProductLevelParentLevelId, SqlDbType.Int, dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductLevelDeleteById))
                {
                    CreateParameter(command, FieldNames.ProductLevelId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
