﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// LocationGroup Dal
    /// </summary>
    public class LocationGroupDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationGroupDal
    {

        #region Constants

        private const String _importTableName = "#tmpLocationGroup";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[LocationHierarchy_Id] [INT] NOT NULL, " +
                "[LocationLevel_Id] [INT] NOT NULL, " +
                "[LocationGroup_Name] [NVARCHAR](100) COLLATE database_default NOT NULL, " +
                "[LocationGroup_Code] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[LocationGroup_ParentGroupId] [INT] NULL" +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion


        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class LocationGroupBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationGroupDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public LocationGroupBulkCopyDataReader(IEnumerable<LocationGroupDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 5; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.LocationHierarchyId;
                        break;
                    case 1:
                        value = _enumerator.Current.LocationLevelId;
                        break;
                    case 2:
                        value = _enumerator.Current.Name;
                        break;
                    case 3:
                        value = _enumerator.Current.Code;
                        break;
                    case 4:
                        value = _enumerator.Current.ParentGroupId;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion


        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static LocationGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationGroupDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationGroupId]),
                LocationHierarchyId = (Int32)GetValue(dr[FieldNames.LocationGroupLocationHierarchyId]),
                LocationLevelId = (Int32)GetValue(dr[FieldNames.LocationGroupLocationLevelId]),
                Name = (String)GetValue(dr[FieldNames.LocationGroupName]),
                Code = (String)GetValue(dr[FieldNames.LocationGroupCode]),
                ParentGroupId = (Int32?)GetValue(dr[FieldNames.LocationGroupParentGroupId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationGroupDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationGroupDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.LocationGroupDateDeleted]),
                AssignedLocationsCount = (Int32)GetValue(dr[FieldNames.LocationGroupAssignedLocationsCount]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public LocationGroupDto FetchById(Int32 id)
        {
            LocationGroupDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationGroupId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<LocationGroupDto> FetchByLocationHierarchyId(Int32 locationHierarchyId)
        {
            List<LocationGroupDto> dtoList = new List<LocationGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupFetchByLocationHierarchyId))
                {
                    CreateParameter(command, FieldNames.LocationGroupLocationHierarchyId, SqlDbType.Int, locationHierarchyId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns all dtos with the given hierarchy id including deleted.
        /// </summary>
        /// <param name="hierarchyId"></param>
        /// <returns></returns>
        public IEnumerable<LocationGroupDto> FetchByLocationHierarchyIdIncludingDeleted(Int32 hierarchyId)
        {
            List<LocationGroupDto> dtoList = new List<LocationGroupDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupFetchByLocationHierarchyIdIncludingDeleted))
                {
                    CreateParameter(command, FieldNames.LocationGroupLocationHierarchyId, SqlDbType.Int, hierarchyId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(LocationGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationGroupId, SqlDbType.Int);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationGroupDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationGroupDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationGroupDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationGroupLocationHierarchyId, SqlDbType.Int, dto.LocationHierarchyId);
                    CreateParameter(command, FieldNames.LocationGroupLocationLevelId, SqlDbType.Int, dto.LocationLevelId);
                    CreateParameter(command, FieldNames.LocationGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LocationGroupCode, SqlDbType.NVarChar, dto.Code);
                    CreateParameter(command, FieldNames.LocationGroupParentGroupId, SqlDbType.Int, dto.ParentGroupId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(LocationGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationGroupId, SqlDbType.Int, dto.Id);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationGroupDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationGroupDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationGroupDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationGroupLocationHierarchyId, SqlDbType.Int, dto.LocationHierarchyId);
                    CreateParameter(command, FieldNames.LocationGroupLocationLevelId, SqlDbType.Int, dto.LocationLevelId);
                    CreateParameter(command, FieldNames.LocationGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LocationGroupCode, SqlDbType.NVarChar, dto.Code);
                    CreateParameter(command, FieldNames.LocationGroupParentGroupId, SqlDbType.Int, dto.ParentGroupId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<LocationGroupDto> dtoList, LocationGroupIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new LocationGroupIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationGroupDtoKey, LocationGroupDto> keyedDtoList = new Dictionary<LocationGroupDtoKey, LocationGroupDto>();
            foreach (LocationGroupDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationGroupBulkCopyDataReader dr = new LocationGroupBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<LocationGroupDtoKey, Int32> ids = new Dictionary<LocationGroupDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.LocationGroupSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Int32 outputDataId = (Int32)GetValue(dr[FieldNames.LocationGroupId]);

                            LocationGroupDtoKey key = new LocationGroupDtoKey();
                            key.LocationLevelId= (Int32)GetValue(dr[FieldNames.LocationGroupLocationLevelId]);
                            key.Code = (String)GetValue(dr[FieldNames.LocationGroupCode]);
                            ids[key] = outputDataId;
                        }
                    }
                }

                foreach (LocationGroupDto dto in dtoList)
                {
                    Int32 outputDataId;

                    if (ids.TryGetValue(dto.DtoKey, out outputDataId))
                    {
                        dto.Id = outputDataId;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private String GenerateSetPropertiesString(LocationGroupIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsLocationHierarchyIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationGroupLocationHierarchyId);
            }
            if (isSetDto.IsLocationLevelIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationGroupLocationLevelId);
            }
            if (isSetDto.IsNameSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationGroupName);
            }
            if (isSetDto.IsCodeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationGroupCode);
            }
            if (isSetDto.IsParentGroupIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationGroupParentGroupId);
            }

            return setProperties;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion


        
    }
}