﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM 8.0
// V8-25669 : A.Kuszyk
//		Created.
// CCM-25242 : N.Haywood
//  Added fetch methods for imports
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIds.
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-26099 :I.George
//  Added FetchByMerchandisingGroupIdCriteria
// V8-25556 : J.Pickup
//  Added FetchAllIncludingDeleted()  
#endregion
#region Version History: CCM 801
// V8-28693 : J.Pickup
//  Added a String.Empty check against whereClause as the SQL procedure seems to use this in a way it cannot be empty.
#endregion
#region Version History: (CCM 8.1.0)
// V8-29667 : N.Haywood
//  Removed FetchAllIncludingDeleted
#endregion
#region Version History: (CCM v8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class ProductInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IProductInfoDal
    {
        #region Constants

        private const String _importTableName = "#tmpProducts";
        private const String _createGTINTableSql = "CREATE TABLE [{0}] (Product_GTIN NVARCHAR(14) COLLATE database_default)";
        private const String _createIdTableSql = "CREATE TABLE [{0}] (Product_Id INT)";
        private const String _dropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        #endregion

        #region Nested Classes

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        private class BulkCopyDataReader : IDataReader
        {
            #region Fields
            IEnumerator<String> _enumerator; // an enumerator containing the values to write to the database
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyDataReader(IEnumerable<String> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = _enumerator.Current ?? (object) DBNull.Value;
                return value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        private class BulkCopyIntDataReader : IDataReader
        {
            #region Fields
            IEnumerator<Int32> _enumerator; // an enumerator containing the values to write to the database
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyIntDataReader(IEnumerable<Int32> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = _enumerator.Current;
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private ProductInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductId]),
                EntityId = (Int32)GetValue(dr[FieldNames.ProductEntityId]),
                Gtin = (String)GetValue(dr[FieldNames.ProductGtin]),
                Name = (String)GetValue(dr[FieldNames.ProductName])
            };
        }

        #endregion

        #region Fetch

        public IEnumerable<ProductInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public IEnumerable<ProductInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByEntityIdIncludingDeleted))
                {
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #region FetchByEntityIdSearchCriteria

        public IEnumerable<ProductInfoDto> FetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            try
            {
                String whereClause = CreateSearchCriteriaWhereClause(searchCriteria, "SearchValue");

                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByEntityIdSearchCriteria))
                {
                    CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, "WhereClause", SqlDbType.NVarChar, whereClause);

                    // execute
                    if (whereClause != String.Empty)
                    {
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                dtoList.Add(GetDataTransferObject(dr));
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        private String CreateSearchCriteriaWhereClause(String text, String searchFieldName)
        {
            //split into parts
            String[] criteriaParts = text.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            String whereClause = String.Empty;
            Int32 part = 0;

            foreach (String criteriaPart in criteriaParts)
            {
                //remove punctuation and special chars
                String parsedPart = ParseString(criteriaPart);

                if (!String.IsNullOrEmpty(parsedPart))
                {
                    //check if the part is an operator.
                    if ((parsedPart.Equals("or") || parsedPart.Equals("and")) &&
                        part != 0 &&                        //If this isn't the start of the criteria
                        part != criteriaParts.Length - 1)   //If this isn't the end of the criteria
                    {
                        if (!String.IsNullOrEmpty(whereClause) &&
                            !whereClause.EndsWith("OR") &&
                            !whereClause.EndsWith("AND"))
                        {
                            whereClause = String.Format("{0} {1}", whereClause, parsedPart.ToUpperInvariant());
                        }
                        else if (criteriaParts[part - 1].ToLowerInvariant().Equals("or") ||
                                 criteriaParts[part - 1].ToLowerInvariant().Equals("and"))
                        {
                            //If the previous part was an operator add this as a search term
                            whereClause = String.Format("{0} {1} LIKE '%{2}%'", whereClause, searchFieldName, parsedPart);
                        }
                    }
                    else //this is a search term
                    {
                        //if there was no clause added last then add an and
                        if (!String.IsNullOrEmpty(whereClause) &&
                            !(whereClause.EndsWith("OR") || whereClause.EndsWith("AND")))
                        {
                            whereClause = String.Format("{0} AND", whereClause);
                        }
                        //add the search term
                        whereClause = String.Format("{0} {1} LIKE '%{2}%'", whereClause, searchFieldName, parsedPart);
                    }
                }
                // indicates which part of criteriaParts is being processed
                part++;
            }

            //Remove trailing operators
            if (whereClause.Contains("%' ") &&
               !whereClause.EndsWith("%'"))
            {
                whereClause = whereClause.Remove(whereClause.LastIndexOf("%' ") + 2);
            }

            return whereClause;
        }

        /// <summary>
        /// Removes all special characters from a string
        /// </summary>
        private String ParseString(String value)
        {
            String parsedPart = String.Empty;
            foreach (Char c in value.Trim().ToLowerInvariant())
            {
                if (Char.IsLetter(c) || Char.IsNumber(c) || Char.IsWhiteSpace(c))
                {
                    parsedPart = String.Format("{0}{1}", parsedPart, c);
                }
            }

            return parsedPart;
        }

        #endregion

        public IEnumerable<ProductInfoDto> FetchByEntityIdProductGtins(Int32 entityId, IEnumerable<String> productGtins)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            // create a temp table for products
            CreateProductTempTable(_createGTINTableSql);

            // perform a batch insert to temp table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                bulkCopy.WriteToServer(new BulkCopyDataReader(productGtins));
            }

            try
            {

                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByEntityIdProductGtins))
                {
                    // entity id
                    CreateParameter(command,
                        FieldNames.ProductEntityId,
                        SqlDbType.Int,
                        entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }

            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop the temporary table
            DropProductTempTable();

            // and return the items
            return dtoList;
        }

        public List<ProductInfoDto> FetchByProductUniverseId(Int32 productUniverseId)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByProductUniverseId))
                {
                    CreateParameter(command, FieldNames.ProductUniverseId, SqlDbType.Int, productUniverseId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public IEnumerable<ProductInfoDto> FetchByProductIds(IEnumerable<int> productIds)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            //create temp table for locations
            CreateProductTempTable(_createIdTableSql);

            // perform a batch insert to temp table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                bulkCopy.WriteToServer(new BulkCopyIntDataReader(productIds));
            }


            try
            {
                //execute
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByProductIds))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop temp table
            DropProductTempTable();

            //return items
            return dtoList;
        }

        public IEnumerable<ProductInfoDto> FetchByEntityIdProductIds(Int32 entityId, IEnumerable<Int32> productIds)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            // create a temp table for products
            CreateProductTempTable(_createIdTableSql);

            // perform a batch insert to temp table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                bulkCopy.WriteToServer(new BulkCopyIntDataReader(productIds));
            }

            try
            {

                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByEntityIdProductIds))
                {
                    // entity id
                    CreateParameter(command,
                        FieldNames.ProductEntityId,
                        SqlDbType.Int,
                        entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }

            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop the temporary table
            DropProductTempTable();

            // and return the items
            return dtoList;
        }

        public IEnumerable<ProductInfoDto> FetchByMerchandisingGroupId(Int32 merchGroupId)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();
            try
            {
                using(DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchByMerchandisingGroupId))
                {
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, merchGroupId);
                  //Execute
                 using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch(SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        public List<ProductInfoDto> FetchIllegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchProductInfoIllegalsByLocationCodeProductGroupCode))
                {
                    CreateParameter(command, FieldNames.LocationCode, SqlDbType.NVarChar, locationCode);
                    CreateParameter(command, FieldNames.ProductGroupCode, SqlDbType.NVarChar, productGroupCode);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public List<ProductInfoDto> FetchLegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInfoFetchProductInfoLegalsByLocationCodeProductGroupCode))
                {
                    CreateParameter(command, FieldNames.LocationCode, SqlDbType.NVarChar, locationCode);
                    CreateParameter(command, FieldNames.ProductGroupCode, SqlDbType.NVarChar, productGroupCode);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Creates the product temporary table on the given connection
        /// </summary>
        private void CreateProductTempTable(String createSql)
        {
            // create the temp table
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, createSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Drops the store temporary table on the given connection
        /// </summary>
        /// <param name="connection">SQL connection to use</param>
        private void DropProductTempTable()
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
