﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-31831 : A.Probyn
//  Added FetchByEntityIdName
//  Removed SeparatorType
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Planogram Name Template Dal
    /// </summary>
    public class PlanogramNameTemplateDal : DalBase, IPlanogramNameTemplateDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramNameTemplateDto GetDataTransferObject(IDataRecord dr)
        {
            return new PlanogramNameTemplateDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramNameTemplateId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PlanogramNameTemplateRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.EntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramNameTemplateName]),
                PlanogramAttribute = (Byte)GetValue(dr[FieldNames.PlanogramNameTemplatePlanogramAttribute]),
                BuilderFormula = (String)GetValue(dr[FieldNames.PlanogramNameTemplateBuilderFormula]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.PlanogramNameTemplateDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.PlanogramNameTemplateDateLastModified])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a <see cref="PlanogramNameTemplateDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="PlanogramNameTemplateDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="PlanogramNameTemplateDto" /> with the data for the provided <paramref name="id" />.</returns>
        public PlanogramNameTemplateDto FetchById(Object id)
        {
            PlanogramNameTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramNameTemplateFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramNameTemplateId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        public PlanogramNameTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            PlanogramNameTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramNameTemplateFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.PlanogramNameTemplateName, SqlDbType.NVarChar, name);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new Galleria.Framework.Dal.DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="PlanogramNameTemplateDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(PlanogramNameTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramNameTemplateInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramNameTemplateId, SqlDbType.Int);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.PlanogramNameTemplateRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramNameTemplateDateCreated, SqlDbType.DateTime, 
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramNameTemplateDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);
                    
                    //Other properties
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramNameTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramNameTemplatePlanogramAttribute, SqlDbType.TinyInt, dto.PlanogramAttribute);
                    CreateParameter(command, FieldNames.PlanogramNameTemplateBuilderFormula, SqlDbType.NVarChar, dto.BuilderFormula);
                    
                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///  Updates the provided <see cref="PlanogramNameTemplateDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(PlanogramNameTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramNameTemplateUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramNameTemplateId, SqlDbType.Int, dto.Id);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.PlanogramNameTemplateRowVersion, SqlDbType.Timestamp, 
                        ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramNameTemplateDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramNameTemplateDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);
                    
                    //Other properties
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramNameTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramNameTemplatePlanogramAttribute, SqlDbType.TinyInt, dto.PlanogramAttribute);
                    CreateParameter(command, FieldNames.PlanogramNameTemplateBuilderFormula, SqlDbType.NVarChar, dto.BuilderFormula);
                    
                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);                    
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramNameTemplateDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramNameTemplateId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}