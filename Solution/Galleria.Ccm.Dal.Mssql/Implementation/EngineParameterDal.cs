﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-28258 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using Galleria.Framework.Engine.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class EngineParameterDal : Galleria.Framework.Dal.Mssql.DalBase, IEngineParameterDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a data transfer object from a data reader
        /// </summary>
        private static EngineParameterDto GetDataTransferObject(SqlDataReader dr)
        {
            return new EngineParameterDto
            {
                RowVersion = new RowVersion(GetValue(dr[FieldNames.EngineParameterRowVersion])),
                Name = (String)GetValue(dr[FieldNames.EngineParameterName]),
                Value = GetValue(dr[FieldNames.EngineParameterValue])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the specified parameter from the database
        /// </summary>
        public EngineParameterDto FetchByName(String name)
        {
            EngineParameterDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineParameterFetchByName))
                {
                    // parameters
                    CreateParameter(command, FieldNames.EngineParameterName, SqlDbType.NVarChar, name);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates the specified parameter in the database
        /// </summary>
        public void Update(EngineParameterDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineParameterUpdateByName))
                {
                    // parameters
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.EngineParameterRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    CreateParameter(command, FieldNames.EngineParameterName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.EngineParameterValue, SqlDbType.Variant, dto.Value);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
