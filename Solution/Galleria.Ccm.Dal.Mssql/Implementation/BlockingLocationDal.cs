﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// BlockingLocation Dal
    /// </summary>
    public class BlockingLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IBlockingLocationDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static BlockingLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new BlockingLocationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.BlockingLocationId]),
                BlockingId = (Int32)GetValue(dr[FieldNames.BlockingLocationBlockingId]),
                BlockingGroupId = (Int32)GetValue(dr[FieldNames.BlockingLocationBlockingGroupId]),
                BlockingDividerTopId = (Int32?)GetValue(dr[FieldNames.BlockingLocationBlockingDividerTopId]),
                BlockingDividerBottomId = (Int32?)GetValue(dr[FieldNames.BlockingLocationBlockingDividerBottomId]),
                BlockingDividerLeftId = (Int32?)GetValue(dr[FieldNames.BlockingLocationBlockingDividerLeftId]),
                BlockingDividerRightId = (Int32?)GetValue(dr[FieldNames.BlockingLocationBlockingDividerRightId]),
                SpacePercentage = (Single)GetValue(dr[FieldNames.BlockingLocationSpacePercentage]),
            };
        }

        #endregion

        #region Fetch



        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<BlockingLocationDto> FetchByBlockingId(Int32 blockingId)
        {
            List<BlockingLocationDto> dtoList = new List<BlockingLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingLocationFetchByBlockingId))
                {
                    CreateParameter(command, FieldNames.BlockingLocationBlockingId, SqlDbType.Int, blockingId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(BlockingLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.BlockingLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingLocationBlockingId, SqlDbType.Int, dto.BlockingId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingGroupId, SqlDbType.Int, dto.BlockingGroupId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerTopId, SqlDbType.Int, dto.BlockingDividerTopId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerBottomId, SqlDbType.Int, dto.BlockingDividerBottomId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerLeftId, SqlDbType.Int, dto.BlockingDividerLeftId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerRightId, SqlDbType.Int, dto.BlockingDividerRightId);
                    CreateParameter(command, FieldNames.BlockingLocationSpacePercentage, SqlDbType.Real, dto.SpacePercentage);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(BlockingLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingLocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingLocationBlockingId, SqlDbType.Int, dto.BlockingId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingGroupId, SqlDbType.Int, dto.BlockingGroupId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerTopId, SqlDbType.Int, dto.BlockingDividerTopId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerBottomId, SqlDbType.Int, dto.BlockingDividerBottomId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerLeftId, SqlDbType.Int, dto.BlockingDividerLeftId);
                    CreateParameter(command, FieldNames.BlockingLocationBlockingDividerRightId, SqlDbType.Int, dto.BlockingDividerRightId);
                    CreateParameter(command, FieldNames.BlockingLocationSpacePercentage, SqlDbType.Real, dto.SpacePercentage);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}