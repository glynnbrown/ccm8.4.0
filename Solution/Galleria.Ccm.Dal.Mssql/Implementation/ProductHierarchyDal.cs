﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ProductHierarchy Dal
    /// </summary>
    public class ProductHierarchyDal : Galleria.Framework.Dal.Mssql.DalBase, IProductHierarchyDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ProductHierarchyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductHierarchyDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductHierarchyId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ProductHierarchyRowVersion])),
                Name = (String)GetValue(dr[FieldNames.ProductHierarchyName]),
                EntityId = (Int32)GetValue(dr[FieldNames.ProductHierarchyEntityId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ProductHierarchyDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ProductHierarchyDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.ProductHierarchyDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ProductHierarchyDto FetchById(Int32 id)
        {
            ProductHierarchyDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductHierarchyFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductHierarchyId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public ProductHierarchyDto FetchByEntityId(Int32 entityId)
        {
            ProductHierarchyDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductHierarchyFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.ProductHierarchyEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ProductHierarchyDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductHierarchyInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductHierarchyId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ProductHierarchyRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductHierarchyDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductHierarchyDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductHierarchyDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductHierarchyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ProductHierarchyName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ProductHierarchyDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductHierarchyUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductHierarchyId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ProductHierarchyRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductHierarchyDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductHierarchyDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductHierarchyDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductHierarchyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ProductHierarchyName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductHierarchyDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductHierarchyId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
