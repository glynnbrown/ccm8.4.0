﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class ProductImageDal : Galleria.Framework.Dal.Mssql.DalBase, IProductImageDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private ProductImageDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductImageDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductImageId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ProductImageRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.ProductImageEntityId]),
                ProductId = (Int32)GetValue(dr[FieldNames.ProductImageProductId]),
                ImageId = (Int32)GetValue(dr[FieldNames.ProductImageImageId]),
                ImageType = (Byte)(Byte)GetValue(dr[FieldNames.ProductImageImageType]),
                FacingType = (Byte)(Byte)GetValue(dr[FieldNames.ProductImageFacingType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ProductImageDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ProductImageDateLastModified])
            };
        }
        #endregion

        #region Fetch

        public ProductImageDto FetchById(Int32 id)
        {
            ProductImageDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductImageFetchById))
                {
                    CreateParameter(command, FieldNames.ProductImageId, SqlDbType.Int, id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<ProductImageDto> FetchByEntityId(Int32 entityId)
        {
            List<ProductImageDto> dtoList = new List<ProductImageDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductImageFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.ProductImageEntityId, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public IEnumerable<ProductImageDto> FetchByProductId(Int32 productId)
        {
            List<ProductImageDto> dtoList = new List<ProductImageDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductImageFetchByProductId))
                {
                    CreateParameter(command, FieldNames.ProductImageProductId, SqlDbType.Int, productId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public IEnumerable<ProductImageDto> FetchByProductIdImageType(Int32 productId, Byte ImageType)
        {
            List<ProductImageDto> dtoList = new List<ProductImageDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductImageFetchByProductIdImageType))
                {
                    CreateParameter(command, FieldNames.ProductImageProductId, SqlDbType.Int, productId);
                    CreateParameter(command, FieldNames.ProductImageImageType, SqlDbType.TinyInt, ImageType);
                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public ProductImageDto FetchByProductIdImageTypeFacingTypeCompressionId(Int32 productId, Byte imageType, Byte facingType, Int32 compressionId)
        {
            ProductImageDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductImageFetchByProductIdImageTypeFacingTypeCompressionId))
                {
                    CreateParameter(command, FieldNames.ProductImageProductId, SqlDbType.Int, productId);
                    CreateParameter(command, FieldNames.ProductImageImageType, SqlDbType.TinyInt, imageType);
                    CreateParameter(command, FieldNames.ProductImageFacingType, SqlDbType.TinyInt, facingType);
                    CreateParameter(command, FieldNames.ImageCompressionId, SqlDbType.Int, compressionId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dto;
        }

        #endregion

        #region Insert

        public void Insert(ProductImageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductImageInsert))
                {
                    // Parameter to store returned new Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductImageId, SqlDbType.Int);
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.ProductImageRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductImageDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductImageDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Add in the other parameters
                    CreateParameter(command, FieldNames.ProductImageImageId, SqlDbType.Int, dto.ImageId);
                    CreateParameter(command, FieldNames.ProductImageImageType, SqlDbType.TinyInt, dto.ImageType);
                    CreateParameter(command, FieldNames.ProductImageFacingType, SqlDbType.TinyInt, dto.FacingType);
                    CreateParameter(command, FieldNames.ProductImageProductId, SqlDbType.Int, dto.ProductId);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    if (!(idParameter.Value is System.DBNull))
                    {
                        dto.Id = (Int32)idParameter.Value;
                        dto.RowVersion = new RowVersion(versionParameter.Value);
                        dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                        dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductImageDeleteById))
                {
                    CreateParameter(command, FieldNames.ProductImageId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
