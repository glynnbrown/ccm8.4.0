﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : L.Ineson
//  Created
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link
//  Added Entity_Id link
// V8-27411 : M.Pettit
//  Added ProcessingStatusDescription, ProgressDateStarted, ProgressDateLastUpdated properties
//  Added DateCompleted and UserId, UserFullName properties
// V8-28493 : L.Ineson
//  Added status planogram counts.
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//      Extended for PlanogramLocationType
// V8-29842 : A.Probyn
//      Extended to include WorkpackageType
// V8-29590 : A.Probyn
//      Extended for new PlanogramWarningCount, PlanogramTotalErrorScore, PlanogramHighestErrorScore
#endregion
#region Version History: CCM820
// V8-30887 : L.Ineson
//  Added workflow name
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkpackageInfoDal
    /// </summary>
    public sealed class WorkpackageInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackageInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public WorkpackageInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackageInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkpackageId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.WorkpackageRowVersion])),
                Name = (String)GetValue(dr[FieldNames.WorkpackageName]),
                UserId = (Int32)GetValue(dr[FieldNames.WorkpackageUserId]),
                UserFullName = (String)GetValue(dr[FieldNames.WorkpackageInfoUserFullName]),
                PlanogramLocationType = (Byte)GetValue(dr[FieldNames.WorkpackagePlanogramLocationType]),
                WorkpackageType = (Byte)GetValue(dr[FieldNames.WorkpackageType]),
                PlanogramCount = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramCount]),
                PlanogramPendingCount = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramPendingCount]),
                PlanogramProcessingCount = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramProcessingCount]),
                PlanogramCompleteCount = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramCompleteCount]),
                PlanogramQueuedCount = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramQueuedCount]),
                PlanogramFailedCount = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramFailedCount]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.WorkpackageDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.WorkpackageDateLastModified]),
                DateCompleted = (DateTime?)GetValue(dr[FieldNames.WorkpackageDateCompleted]),
                ProcessingStatus = (Byte)GetValue(dr[FieldNames.WorkpackageProcessingStatusStatus]),
                ProcessingStatusDescription = (String)GetValue(dr[FieldNames.WorkpackageProcessingStatusStatusDescription]),
                ProgressMax = (Int32)GetValue(dr[FieldNames.WorkpackageProcessingstatusProgressMax]),
                ProgressCurrent = (Int32)GetValue(dr[FieldNames.WorkpackageProcessingStatusProgressCurrent]),
                ProgressDateStarted = (DateTime?)GetValue(dr[FieldNames.WorkpackageProcessingStatusDateStarted]),
                ProgressDateLastUpdated = (DateTime?)GetValue(dr[FieldNames.WorkpackageProcessingStatusDateUpdated]),
                EntityId = (Int32)GetValue(dr[FieldNames.WorkpackageEntityId]),
                PlanogramWarningCount = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramWarningCount]),
                PlanogramHighestErrorScore = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramHighestErrorScore]),
                PlanogramTotalErrorScore = (Int32)GetValue(dr[FieldNames.WorkpackageInfoPlanogramTotalErrorScore]),
                WorkflowName = (String)GetValue(dr[FieldNames.WorkpackageInfoWorkflowName]),
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns workpackage infos for the given planogram group id
        /// </summary>
        public IEnumerable<WorkpackageInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<WorkpackageInfoDto> dtoList = new List<WorkpackageInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.WorkpackageEntityId, SqlDbType.Int, entityId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified workpackage
        /// </summary>
        public WorkpackageInfoDto FetchById(Int32 id)
        {
            WorkpackageInfoDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageInfoFetchById))
                {
                    CreateParameter(command, FieldNames.WorkpackageId, SqlDbType.Int, id);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion
    }
}
