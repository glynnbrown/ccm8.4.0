﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class LocationProductLegalDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationProductLegalDal
    {
        #region Constants

        private const String _importTableName = "#tmpLocationProductLegal";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
            "[Product_Id] [INT] NOT NULL," +
            "[Location_Id] [SMALLINT] NOT NULL," +
            "[Entity_Id] [INT] NOT NULL" +
            ")";

        private const String _createFetchCriteriaTableSql =
               "CREATE TABLE  [{0}](" +
               "[Location_Id] [SMALLINT] NOT NULL, " +
               "[Product_Id] [INT] NOT NULL" +
               ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        #endregion

        #region Nested Classes

        private class LocationProductLegalBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationProductLegalDto> _enumerator; //The enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">the values to insert</param>
            public LocationProductLegalBulkCopyDataReader(IEnumerable<LocationProductLegalDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return 3; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            public object GetValue(Int32 i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.ProductId;
                        break;

                    case 1:
                        value = _enumerator.Current.LocationId;
                        break;

                    case 2:
                        value = _enumerator.Current.EntityId;
                        break;

                    default:
                        value = null;
                        break;
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }

            #endregion

            #region NotImplemented

            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }

            #endregion
        }


        private class LocationProductLegalBulkCopyFetchCriteriaDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<Tuple<Int16, Int32>> _enumerator; //The enumerator containing fetch criteria
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">the values to insert</param>
            public LocationProductLegalBulkCopyFetchCriteriaDataReader(IEnumerable<Tuple<Int16, Int32>> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return 2; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            public object GetValue(Int32 i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Item1; //location
                        break;

                    case 1:
                        value = _enumerator.Current.Item2; //product
                        break;

                    default:
                        value = null;
                        break;
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }

            #endregion

            #region NotImplemented

            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }

            #endregion
        }

        #endregion

        #region DataTransferObject

        private LocationProductLegalDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationProductLegalDto
            {
                ProductId = (Int32)GetValue(dr[FieldNames.LocationProductLegalProductId]),
                LocationId = (Int16)GetValue(dr[FieldNames.LocationProductLegalLocationId]),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationProductLegalEntityId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.LocationProductLegalRowVersion])),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationProductLegalDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationProductLegalDateLastModified]),
            };
        }

        #endregion

        #region Fetch

        public IEnumerable<LocationProductLegalDto> FetchByEntityId(Int32 entityId)
        {
            List<LocationProductLegalDto> dtoList = new List<LocationProductLegalDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationProductLegalEntityId, SqlDbType.Int, entityId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public LocationProductLegalDto FetchById(Int16 locationId, Int32 productId)
        {
            LocationProductLegalDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalFetchById))
                {
                    CreateParameter(command, FieldNames.LocationProductLegalLocationId, SqlDbType.SmallInt, locationId);
                    CreateParameter(command, FieldNames.LocationProductLegalProductId, SqlDbType.Int, productId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        public List<LocationProductLegalDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> locationProductIdCombinations)
        {
            List<LocationProductLegalDto> dtoList = new List<LocationProductLegalDto>();
            try
            {
                // create the temporary table
                CreateTempImportTable(_createFetchCriteriaTableSql, _importTableName);

                //perform batch insert of combinations to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationProductLegalBulkCopyFetchCriteriaDataReader dr = new LocationProductLegalBulkCopyFetchCriteriaDataReader(locationProductIdCombinations);
                    bulkCopy.WriteToServer(dr);
                }

                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalFetchByLocationIdProductIdCombinations))
                {
                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropTempImportTable(_importTableName);
            }

            return dtoList;
        }
        #endregion

        #region Insert

        public void Insert(LocationProductLegalDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalInsert))
                {
                    //Add parameters
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.LocationProductLegalRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationProductLegalDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationProductLegalDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    CreateParameter(command, FieldNames.LocationProductLegalLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.LocationProductLegalProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.LocationProductLegalEntityId, SqlDbType.Int, dto.EntityId);

                    command.ExecuteNonQuery();

                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(LocationProductLegalDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalUpdate))
                {
                    // Parameter 
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.LocationProductLegalRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationProductLegalDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationProductLegalDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    CreateParameter(command, FieldNames.LocationProductLegalLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.LocationProductLegalProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.LocationProductLegalEntityId, SqlDbType.Int, dto.EntityId);

                    command.ExecuteNonQuery();

                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        public void Upsert(IEnumerable<LocationProductLegalDto> dtoList)
        {
            //to avoid duplicates, we create a dictionary of dtos
            //to ensure that only the last dto is actually inserted
            Dictionary<LocationProductLegalIdSearchCriteria, LocationProductLegalDto> existingDtoList =
                new Dictionary<LocationProductLegalIdSearchCriteria, LocationProductLegalDto>();
            foreach (LocationProductLegalDto dto in dtoList)
            {
                //Create criteria
                LocationProductLegalIdSearchCriteria criteria = new LocationProductLegalIdSearchCriteria()
                {
                    LocationId = dto.LocationId,
                    ProductId = dto.ProductId
                };

                //add to the dto list
                if (existingDtoList.ContainsKey(criteria))
                {
                    existingDtoList[criteria] = dto;
                }
                else
                {
                    existingDtoList.Add(criteria, dto);
                }
            }

            try
            {
                CreateTempImportTable(_createImportTableSql, _importTableName);

                //perform batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationProductLegalBulkCopyDataReader dr = new LocationProductLegalBulkCopyDataReader(existingDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //perform a batch import
                Dictionary<Tuple<Int32, Int16>, RowVersion> rowVersions = new Dictionary<Tuple<Int32, Int16>, RowVersion>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalBulkUpsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Tuple<Int32, Int16> outputData =
                                new Tuple<Int32, Int16>
                                    (
                                        (Int32)GetValue(dr[FieldNames.LocationProductLegalProductId]),
                                        (Int16)GetValue(dr[FieldNames.LocationProductLegalLocationId])
                                    );
                            rowVersions.Add(outputData, new RowVersion(GetValue(dr[FieldNames.LocationProductLegalRowVersion])));
                        }
                    }
                }

                //Assign each dto it's rowVersion from the dictionary
                foreach (LocationProductLegalDto dto in dtoList)
                {
                    RowVersion rowVersion;
                    rowVersions.TryGetValue(new Tuple<Int32, Int16>(dto.ProductId, dto.LocationId), out rowVersion);

                    if (rowVersion != null)
                    {
                        dto.RowVersion = rowVersion;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropTempImportTable(_importTableName);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int16 locationId, Int32 productId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalDeleteById))
                {
                    CreateParameter(command, FieldNames.LocationProductLegalLocationId, SqlDbType.SmallInt, locationId);
                    CreateParameter(command, FieldNames.LocationProductLegalProductId, SqlDbType.Int, productId);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductLegalDeleteByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationProductLegalEntityId, SqlDbType.Int, entityId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        private void CreateTempImportTable(String sql, String tempTableName)
        {
            try
            {
                using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, sql, tempTableName), CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }


        /// <summary>
        /// Drops the sepecified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropTempImportTable(String tempTableName)
        {
            try
            {
                using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, tempTableName), CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch { }
        }
        #endregion
    }
}
