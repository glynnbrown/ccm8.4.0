﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS.    
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramImageDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramImageDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramImageDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramImageDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramImageId]),
                PlanogramId = (Object)GetValue(dr[FieldNames.PlanogramImagePlanogramId]), 
                FileName = (String)GetValue(dr[FieldNames.PlanogramImageFileName]), 
                Description = (String)GetValue(dr[FieldNames.PlanogramImageDescription]), 
                ImageData = (Byte[])GetValue(dr[FieldNames.PlanogramImageImageData])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramImageDto> FetchByPlanogramId(Object planogramId)
        {
            List<PlanogramImageDto> dtoList = new List<PlanogramImageDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramImageFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramImagePlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramImageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramImageInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramImageId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramImagePlanogramId, SqlDbType.Int, dto.PlanogramId); 
                    CreateParameter(command, FieldNames.PlanogramImageFileName, SqlDbType.NVarChar, dto.FileName); 
                    CreateParameter(command, FieldNames.PlanogramImageDescription, SqlDbType.NVarChar, dto.Description); 
                    CreateParameter(command, FieldNames.PlanogramImageImageData, SqlDbType.VarBinary, dto.ImageData);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramImageDto> dtos)
        {
            foreach (PlanogramImageDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramImageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramImageUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImageId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramImagePlanogramId, SqlDbType.Int, dto.PlanogramId); 
                    CreateParameter(command, FieldNames.PlanogramImageFileName, SqlDbType.NVarChar, dto.FileName); 
                    CreateParameter(command, FieldNames.PlanogramImageDescription, SqlDbType.NVarChar, dto.Description); 
                    CreateParameter(command, FieldNames.PlanogramImageImageData, SqlDbType.VarBinary, dto.ImageData);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramImageDto> dtos)
        {
            foreach (PlanogramImageDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramImageDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImageId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramImageDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramImageDto> dtos)
        {
            foreach (PlanogramImageDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
