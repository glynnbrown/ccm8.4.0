﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// V8-25557 : J.Pickup
//  Added FetchAllIncludingDeleted();
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentMinorRevisionDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private AssortmentMinorRevisionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionDto
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.AssortmentMinorRevisionRowVersion])),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.AssortmentMinorRevisionUcr]),
                EntityId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionEntityId]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.ProductGroupId]),
                Name = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionName]),
                ConsumerDecisionTreeId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionConsumerDecisionTreeId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.AssortmentMinorRevisionDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.AssortmentMinorRevisionDateLastModified]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.AssortmentMinorRevisionParentUniqueContentReference])
            };

        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns specified dto from the database
        /// </summary>
        /// <param name="id">The AssortmentMinorRevision id</param>
        /// <returns>A AssortmentMinorRevision dto</returns>
        public AssortmentMinorRevisionDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionFetchById))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the lastest dto for the given entityid and name
        /// </summary>
        /// <param name="entityId">The id of the entity </param>
        /// <param name="name">The name of the content item</param>
        /// <returns></returns>
        public AssortmentMinorRevisionDto FetchByEntityIdName(Int32 entityId, String name)
        {
            AssortmentMinorRevisionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionFetchByEntityIdName))
                {
                    // params
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionName, SqlDbType.NVarChar, name);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the all dtos
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssortmentMinorRevisionDto> FetchAll()
        {
            List<AssortmentMinorRevisionDto> dtos = new List<AssortmentMinorRevisionDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionFetchAll))
                {
                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtos;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(AssortmentMinorRevisionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionInsert))
                {
                    // id parameter
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionId,
                        SqlDbType.Int);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionRowVersion,
                        SqlDbType.Timestamp);

                    // unique content reference
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionUcr,
                        SqlDbType.UniqueIdentifier,
                        dto.UniqueContentReference);

                    // entity id
                    CreateParameter(command,
                        FieldNames.EntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // Product group id
                    CreateParameter(command,
                        FieldNames.ProductGroupId,
                        SqlDbType.Int,
                        dto.ProductGroupId);

                    // name
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // ConsumerDecisionTreeId
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionConsumerDecisionTreeId,
                        SqlDbType.Int,
                        dto.ConsumerDecisionTreeId);

                    CreateParameter(command, FieldNames.AssortmentMinorRevisionParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    //date created 
                    SqlParameter dateCreatedParameter =
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDateCreated, SqlDbType.SmallDateTime);

                    //date last modified
                    SqlParameter dateLastModifiedParameter =
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDateLastModified, SqlDbType.SmallDateTime);
                    
                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (int)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)dateCreatedParameter.Value;
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentMinorRevisionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionUpdateById))
                {
                    // id parameter
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionId,
                        SqlDbType.Int,
                        dto.Id);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionRowVersion,
                        SqlDbType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);

                    // unique content reference
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionUcr,
                        SqlDbType.UniqueIdentifier,
                        dto.UniqueContentReference);

                    // entity id
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // Product group id
                    CreateParameter(command,
                        FieldNames.ProductGroupId,
                        SqlDbType.Int,
                        dto.ProductGroupId);

                    // name
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // ConsumerDecisionTreeId
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionConsumerDecisionTreeId,
                        SqlDbType.Int,
                        dto.ConsumerDecisionTreeId);

                    CreateParameter(command, FieldNames.AssortmentMinorRevisionParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    //date created 
                    SqlParameter dateCreatedParameter =
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDateCreated, SqlDbType.SmallDateTime);

                    //date last modified
                    SqlParameter dateLastModifiedParameter =
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDateLastModified, SqlDbType.SmallDateTime);
                    
                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeleteById))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.AssortmentMinorRevisionId,
                        SqlDbType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
