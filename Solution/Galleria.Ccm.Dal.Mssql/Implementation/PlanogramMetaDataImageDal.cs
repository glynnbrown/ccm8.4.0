﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27058 : A.Probyn
//  Initial version.   
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramMetadataImageDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramMetadataImageDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramMetadataImageDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramMetadataImageDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramMetadataImageId]),
                PlanogramId = (Object)GetValue(dr[FieldNames.PlanogramMetadataImagePlanogramId]),
                FileName = (String)GetValue(dr[FieldNames.PlanogramMetadataImageFileName]),
                Description = (String)GetValue(dr[FieldNames.PlanogramMetadataImageDescription]),
                ImageData = (Byte[])GetValue(dr[FieldNames.PlanogramMetadataImageImageData])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramMetadataImageDto> FetchByPlanogramId(Object planogramId)
        {
            List<PlanogramMetadataImageDto> dtoList = new List<PlanogramMetadataImageDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramMetadataImageFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramMetadataImagePlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramMetadataImageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramMetadataImageInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramMetadataImageId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramMetadataImagePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramMetadataImageFileName, SqlDbType.NVarChar, dto.FileName);
                    CreateParameter(command, FieldNames.PlanogramMetadataImageDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramMetadataImageImageData, SqlDbType.VarBinary, dto.ImageData);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            foreach (PlanogramMetadataImageDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramMetadataImageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramMetadataImageUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramMetadataImageId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramMetadataImagePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramMetadataImageFileName, SqlDbType.NVarChar, dto.FileName);
                    CreateParameter(command, FieldNames.PlanogramMetadataImageDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramMetadataImageImageData, SqlDbType.VarBinary, dto.ImageData);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified dtos
        /// </summary>
        public void Update(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            foreach (PlanogramMetadataImageDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramMetadataImageDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramMetadataImageId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramMetadataImageDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            foreach (PlanogramMetadataImageDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
