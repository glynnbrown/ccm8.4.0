﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class LocationSpaceProductGroupDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationSpaceProductGroupDal
    {
        #region Constants

        private const String _importTableName = "#tmpLocationSpaceProductGroup";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[LocationSpace_Id] [INT], " +
                "[ProductGroup_Id] [INT], " +
                "[LocationSpaceProductGroup_BayCount] [REAL], " +
                "[LocationSpaceProductGroup_ProductCount] [INT], " +
                "[LocationSpaceProductGroup_AverageBayWidth] [REAL], " +
                "[LocationSpaceProductGroup_AisleName] NVARCHAR(100), " +
                "[LocationSpaceProductGroup_ValleyName] NVARCHAR(100), " +
                "[LocationSpaceProductGroup_ZoneName] NVARCHAR(100), " +
                "[LocationSpaceProductGroup_CustomAttribute01] NVARCHAR(max), " +
                "[LocationSpaceProductGroup_CustomAttribute02] NVARCHAR(max), " +
                "[LocationSpaceProductGroup_CustomAttribute03] NVARCHAR(max), " +
                "[LocationSpaceProductGroup_CustomAttribute04] NVARCHAR(max), " +
                "[LocationSpaceProductGroup_CustomAttribute05] NVARCHAR(max) " +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public LocationSpaceProductGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationSpaceProductGroupDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupId]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupProductGroupId]),
                LocationSpaceId = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupLocationSpaceId]),
                BayCount = (Single)GetValue(dr[FieldNames.LocationSpaceProductGroupBayCount]),
                ProductCount = (Int32?)GetValue(dr[FieldNames.LocationSpaceProductGroupProductCount]),
                AverageBayWidth = (Single)GetValue(dr[FieldNames.LocationSpaceProductGroupAverageBayWidth]),
                AisleName = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupAisleName]),
                ValleyName = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupValleyName]),
                ZoneName = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupZoneName]),
                CustomAttribute01 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute01]),
                CustomAttribute02 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute02]),
                CustomAttribute03 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute03]),
                CustomAttribute04 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute04]),
                CustomAttribute05 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute05]),
            };
        }
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class LocationSpaceProductGroupBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationSpaceProductGroupDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public LocationSpaceProductGroupBulkCopyDataReader(IEnumerable<LocationSpaceProductGroupDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 13; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.LocationSpaceId;
                        break;
                    case 1:
                        value = _enumerator.Current.ProductGroupId;
                        break;
                    case 2:
                        value = _enumerator.Current.BayCount;
                        break;
                    case 3:
                        value = _enumerator.Current.ProductCount;
                        break;
                    case 4:
                        value = _enumerator.Current.AverageBayWidth;
                        break;
                    case 5:
                        value = _enumerator.Current.AisleName;
                        break;
                    case 6:
                        value = _enumerator.Current.ValleyName;
                        break;
                    case 7:
                        value = _enumerator.Current.ZoneName;
                        break;
                    case 8:
                        value = _enumerator.Current.CustomAttribute01;
                        break;
                    case 9:
                        value = _enumerator.Current.CustomAttribute02;
                        break;
                    case 10:
                        value = _enumerator.Current.CustomAttribute03;
                        break;
                    case 11:
                        value = _enumerator.Current.CustomAttribute04;
                        break;
                    case 12:
                        value = _enumerator.Current.CustomAttribute05;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a single Location Space Product Group record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public LocationSpaceProductGroupDto FetchById(int id)
        {
            LocationSpaceProductGroupDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns all of the LocationSpaceProductGroup records against to a location space record.
        /// </summary>
        /// <param name="locationSpaceId"></param>
        /// <returns></returns>
        public IEnumerable<LocationSpaceProductGroupDto> FetchByLocationSpaceId(Int32 locationSpaceId)
        {
            List<LocationSpaceProductGroupDto> dtoList = new List<LocationSpaceProductGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupFetchByLocationSpaceId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupLocationSpaceId,
                        SqlDbType.Int,
                        locationSpaceId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(LocationSpaceProductGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupInsert))
                {
                    // Parameter to store returned new Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationSpaceProductGroupId, SqlDbType.Int);

                    // Product Group Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupProductGroupId,
                        SqlDbType.Int,
                        dto.ProductGroupId);
                    //LocationSpace Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupLocationSpaceId,
                        SqlDbType.Int,
                        dto.LocationSpaceId);
                    //Bay Count
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupBayCount,
                        SqlDbType.Real,
                        dto.BayCount);
                    //Product Count
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupProductCount,
                        SqlDbType.Int,
                        dto.ProductCount);
                    //Average Bay Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupAverageBayWidth,
                        SqlDbType.Real,
                        dto.AverageBayWidth);
                    //Aisle Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupAisleName,
                        SqlDbType.NVarChar,
                        dto.AisleName);
                    //Valley Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupValleyName,
                        SqlDbType.NVarChar,
                        dto.ValleyName);
                    //Zone Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupZoneName,
                        SqlDbType.NVarChar,
                        dto.ZoneName);
                    //CustomAttribute01
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute01,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute01);
                    //CustomAttribute02
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute02,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute02);
                    //CustomAttribute03
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute03,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute03);
                    //CustomAttribute04
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute04,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute04);
                    //CustomAttribute05
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute05,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute05);


                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Performs an insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to insert</param>
        public void Insert(IEnumerable<LocationSpaceProductGroupDto> dtoList)
        {
            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationSpaceProductGroupDtoKey, LocationSpaceProductGroupDto> keyedDtoList = new Dictionary<LocationSpaceProductGroupDtoKey, LocationSpaceProductGroupDto>();
            foreach (LocationSpaceProductGroupDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationSpaceProductGroupBulkCopyDataReader dr = new LocationSpaceProductGroupBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch upsert
                Dictionary<LocationSpaceProductGroupDtoKey, Int32> ids = new Dictionary<LocationSpaceProductGroupDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupId]);

                            LocationSpaceProductGroupDtoKey key = new LocationSpaceProductGroupDtoKey();
                            key.LocationSpaceId = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupLocationSpaceId]);
                            key.ProductGroupId = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupProductGroupId]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationSpaceProductGroupDto dto in dtoList)
                {
                    Int32 outputData = 0;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != 0)
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        #endregion

        #region Update

        public void Update(LocationSpaceProductGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupUpdateById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupId,
                        SqlDbType.Int,
                        dto.Id);

                    // Product Group Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupProductGroupId,
                        SqlDbType.Int,
                        dto.ProductGroupId);
                    //LocationSpace Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupLocationSpaceId,
                        SqlDbType.Int,
                        dto.LocationSpaceId);
                    //Bay Count
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupBayCount,
                        SqlDbType.Real,
                        dto.BayCount);
                    //Product Count
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupProductCount,
                        SqlDbType.Int,
                        dto.ProductCount);
                    //Average Bay Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupAverageBayWidth,
                        SqlDbType.Real,
                        dto.AverageBayWidth);
                    //Aisle Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupAisleName,
                        SqlDbType.NVarChar,
                        dto.AisleName);
                    //Valley Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupValleyName,
                        SqlDbType.NVarChar,
                        dto.ValleyName);
                    //Zone Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupZoneName,
                        SqlDbType.NVarChar,
                        dto.ZoneName);
                    //CustomAttribute01
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute01,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute01);
                    //CustomAttribute02
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute02,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute02);
                    //CustomAttribute03
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute03,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute03);
                    //CustomAttribute04
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute04,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute04);
                    //CustomAttribute05
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupCustomAttribute05,
                        SqlDbType.NVarChar,
                        dto.CustomAttribute05);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupId,
                        SqlDbType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        public void DeleteByLocationSpaceId(Int32 locationSpaceId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupDeleteByLocationSpaceId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupLocationSpaceId,
                        SqlDbType.Int,
                        locationSpaceId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Upserts dtos into the data source
        /// </summary>
        /// <param name="dtoList">The dtos to insert</param>
        public void Upsert(IEnumerable<LocationSpaceProductGroupDto> dtoList)
        {
            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationSpaceProductGroupDtoKey, LocationSpaceProductGroupDto> keyedDtoList = new Dictionary<LocationSpaceProductGroupDtoKey, LocationSpaceProductGroupDto>();
            foreach (LocationSpaceProductGroupDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationSpaceProductGroupBulkCopyDataReader dr = new LocationSpaceProductGroupBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch upsert
                Dictionary<LocationSpaceProductGroupDtoKey, Int32> ids = new Dictionary<LocationSpaceProductGroupDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupBulkUpsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupId]);

                            LocationSpaceProductGroupDtoKey key = new LocationSpaceProductGroupDtoKey();
                            key.LocationSpaceId = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupLocationSpaceId]);
                            key.ProductGroupId = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupProductGroupId]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationSpaceProductGroupDto dto in dtoList)
                {
                    Int32 outputData = 0;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != 0)
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(string importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
