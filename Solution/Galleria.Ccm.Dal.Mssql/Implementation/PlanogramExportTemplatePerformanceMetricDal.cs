﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramExportTemplatePerformanceMetric Dal
    /// </summary>
    public class PlanogramExportTemplatePerformanceMetricDal : DalBase, IPlanogramExportTemplatePerformanceMetricDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramExportTemplatePerformanceMetricDto GetDataTransferObject(IDataRecord dr)
        {
            return new PlanogramExportTemplatePerformanceMetricDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricId]),
                PlanogramExportTemplateId = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricPlanogramExportTemplateId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricName]),
                Description = (String)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricDescription]),
                Direction = (Byte)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricDirection]),
                SpecialType = (Byte)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricSpecialType]),
                MetricType = (Byte)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricMetricType]),
                MetricId = (Byte)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricMetricId]),
                ExternalField = (String)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricExternalField]),
                AggregationType = (Byte)GetValue(dr[FieldNames.PlanogramExportTemplatePerformanceMetricAggregationType])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramExportTemplatePerformanceMetricDto"/> items corresponding to the given <paramref name="PlanogramExportTemplateId"/>.
        /// </summary>
        /// <param name="PlanogramExportTemplateId">The unique id value for the Planogram Import Template containing the groups to be retrieved.</param>
        /// <returns>An instance implementing <see cref="IEnumerable{PlanogramExportTemplatePerformanceMetricDto}"/> with all the matching groups.</returns>
        public IEnumerable<PlanogramExportTemplatePerformanceMetricDto> FetchByPlanogramExportTemplateId(Object PlanogramExportTemplateId)
        {
            var dtoList = new List<PlanogramExportTemplatePerformanceMetricDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplatePerformanceMetricFetchByPlanogramExportTemplateId))
                {
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricPlanogramExportTemplateId, SqlDbType.Int,
                        (Int32)PlanogramExportTemplateId);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the data contained in the given <paramref name="dto"/> into the dal.
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramExportTemplatePerformanceMetricDto"/> containing the data to be persisted.</param>
        public void Insert(PlanogramExportTemplatePerformanceMetricDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplatePerformanceMetricInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricPlanogramExportTemplateId, SqlDbType.Int, dto.PlanogramExportTemplateId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricMetricType, SqlDbType.TinyInt, dto.MetricType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricMetricId, SqlDbType.TinyInt, dto.MetricId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricExternalField, SqlDbType.NVarChar, dto.ExternalField);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the data contained in the given <paramref name="dto"/> in the dal
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramExportTemplatePerformanceMetricDto"/> containing the data to be persisted.</param>
        public void Update(PlanogramExportTemplatePerformanceMetricDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplatePerformanceMetricUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricPlanogramExportTemplateId, SqlDbType.Int, dto.PlanogramExportTemplateId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricMetricType, SqlDbType.TinyInt, dto.MetricType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricMetricId, SqlDbType.TinyInt, dto.MetricId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricExternalField, SqlDbType.NVarChar, dto.ExternalField);
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the Planogram Export Template Performance Metric matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Unique id value to determine which Planogram Export Template Performance Metric to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplatePerformanceMetricDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramExportTemplatePerformanceMetricId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
