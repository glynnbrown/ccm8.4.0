﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 J.Pickup
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Data;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Mssql.Schema;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    class HighlightGroupDal : DalBase, IHighlightGroupDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static HighlightGroupDto GetDataTransferObject(IDataRecord dr)
        {
            return new HighlightGroupDto
            {
                Id = (Int32)GetValue(dr[FieldNames.HighlightGroupId]),
                HighlightId = (Int32)GetValue(dr[FieldNames.HighlightGroupHighlightId]),
                Order = (Int32)GetValue(dr[FieldNames.HighlightGroupOrder]),
                Name = (String)GetValue(dr[FieldNames.HighlightGroupName]),
                DisplayName = (String)GetValue(dr[FieldNames.HighlightGroupDisplayName]),
                FillColour = (Int32)GetValue(dr[FieldNames.HighlightGroupFillColour]),
                FillPatternType = (Byte)GetValue(dr[FieldNames.HighlightGroupFillPatternType]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="highlightCharachteristicDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="highlightCharachteristicDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="highlightCharachteristicDto" /> with the data for the provided <paramref name="id" />.</returns>
        public IEnumerable<HighlightGroupDto> FetchByHighlightId(Object id)
        {
            List<HighlightGroupDto> dtoList = new List<HighlightGroupDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightGroupFetchByHighlightId))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightGroupHighlightId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        //if (!dr.Read()) throw new Galleria.Framework.Dal.DtoDoesNotExistException();
                        while (dr.Read())
                        { 
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="HighlightCharacteristicDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(HighlightGroupDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightGroupInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.HighlightGroupId, SqlDbType.Int);

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightGroupHighlightId, SqlDbType.Int, dto.HighlightId);
                    CreateParameter(command, FieldNames.HighlightGroupOrder, SqlDbType.Int, dto.Order);
                    CreateParameter(command, FieldNames.HighlightGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.HighlightGroupDisplayName, SqlDbType.NVarChar, dto.DisplayName);
                    CreateParameter(command, FieldNames.HighlightGroupFillColour, SqlDbType.Int, dto.FillColour);
                    CreateParameter(command, FieldNames.HighlightGroupFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);


                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="HighlightCharacteristicDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(HighlightGroupDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightGroupUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightGroupId, SqlDbType.Int, dto.Id);

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightGroupHighlightId, SqlDbType.Int, dto.HighlightId);
                    CreateParameter(command, FieldNames.HighlightGroupOrder, SqlDbType.Int, dto.Order);
                    CreateParameter(command, FieldNames.HighlightGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.HighlightGroupDisplayName, SqlDbType.NVarChar, dto.DisplayName);
                    CreateParameter(command, FieldNames.HighlightGroupFillColour, SqlDbType.Int, dto.FillColour);
                    CreateParameter(command, FieldNames.HighlightGroupFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    // Nothing to update.
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
