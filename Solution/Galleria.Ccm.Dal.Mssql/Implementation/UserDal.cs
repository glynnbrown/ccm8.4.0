﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25664 : A. Kuszyk
//      Created.
// V8-26222 : L.Luong
//      Added date deleted    
#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// User Dal
    /// </summary>
    public class UserDal : Galleria.Framework.Dal.Mssql.DalBase, IUserDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public UserDto GetDataTransferObject(SqlDataReader dr)
        {
            return new UserDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.UserId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.UserRowVersion])),
                UserName = (String)GetValue(dr[FieldNames.UserUserName]),
                FirstName = (String)GetValue(dr[FieldNames.UserFirstName]),
                LastName = (String)GetValue(dr[FieldNames.UserLastName]),
                Description = (String)GetValue(dr[FieldNames.UserDescription]),
                Office = (String)GetValue(dr[FieldNames.UserOffice]),
                TelephoneNumber = (String)GetValue(dr[FieldNames.UserTelephoneNumber]),
                EMailAddress = (String)GetValue(dr[FieldNames.UserEMailAddress]),
                Title = (String)GetValue(dr[FieldNames.UserTitle]),
                Department = (String)GetValue(dr[FieldNames.UserDepartment]),
                Company = (String)GetValue(dr[FieldNames.UserCompany]),
                Manager = (String)GetValue(dr[FieldNames.UserManager]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.UserDateDeleted])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public UserDto FetchById(int id)
        {
            UserDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.UserId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified username
        /// </summary>
        /// <param name="id">specified username</param>
        /// <returns>matching dto</returns>
        public UserDto FetchByUserName(string userName)
        {
            UserDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserFetchByUserName))
                {
                    //UserName
                    CreateParameter(command, FieldNames.UserUserName, SqlDbType.NVarChar, userName);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(UserDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.UserId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.UserRowVersion, SqlDbType.Timestamp);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.UserDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.UserUserName, SqlDbType.NVarChar, dto.UserName);
                    CreateParameter(command, FieldNames.UserFirstName, SqlDbType.NVarChar, dto.FirstName);
                    CreateParameter(command, FieldNames.UserLastName, SqlDbType.NVarChar, dto.LastName);
                    CreateParameter(command, FieldNames.UserDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.UserOffice, SqlDbType.NVarChar, dto.Office);
                    CreateParameter(command, FieldNames.UserTelephoneNumber, SqlDbType.NVarChar, dto.TelephoneNumber);
                    CreateParameter(command, FieldNames.UserEMailAddress, SqlDbType.NVarChar, dto.EMailAddress);
                    CreateParameter(command, FieldNames.UserTitle, SqlDbType.NVarChar, dto.Title);
                    CreateParameter(command, FieldNames.UserDepartment, SqlDbType.NVarChar, dto.Department);
                    CreateParameter(command, FieldNames.UserCompany, SqlDbType.NVarChar, dto.Company);
                    CreateParameter(command, FieldNames.UserManager, SqlDbType.NVarChar, dto.Manager);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        } 
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(UserDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.UserId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.UserRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.UserDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.UserUserName, SqlDbType.NVarChar, dto.UserName);
                    CreateParameter(command, FieldNames.UserFirstName, SqlDbType.NVarChar, dto.FirstName);
                    CreateParameter(command, FieldNames.UserLastName, SqlDbType.NVarChar, dto.LastName);
                    CreateParameter(command, FieldNames.UserDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.UserOffice, SqlDbType.NVarChar, dto.Office);
                    CreateParameter(command, FieldNames.UserTelephoneNumber, SqlDbType.NVarChar, dto.TelephoneNumber);
                    CreateParameter(command, FieldNames.UserEMailAddress, SqlDbType.NVarChar, dto.EMailAddress);
                    CreateParameter(command, FieldNames.UserTitle, SqlDbType.NVarChar, dto.Title);
                    CreateParameter(command, FieldNames.UserDepartment, SqlDbType.NVarChar, dto.Department);
                    CreateParameter(command, FieldNames.UserCompany, SqlDbType.NVarChar, dto.Company);
                    CreateParameter(command, FieldNames.UserManager, SqlDbType.NVarChar, dto.Manager);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        } 
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.UserId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        } 
        #endregion
    }
}
