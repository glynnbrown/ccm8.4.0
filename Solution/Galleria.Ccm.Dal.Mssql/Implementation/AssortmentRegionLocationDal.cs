﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26704 : A.Kuszyk
//  Added LocationCode.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentRegionLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentRegionLocationDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentRegionLocationDto GetDataTransferObject(SqlDataReader dr)
        {
                return new AssortmentRegionLocationDto()
                {
                    Id = (Int32)GetValue(dr[FieldNames.AssortmentRegionLocationId]),
                    AssortmentRegionId = (Int32)GetValue(dr[FieldNames.AssortmentRegionId]),
                    LocationId = (Int16)GetValue(dr[FieldNames.AssortmentRegionLocationLocationId]),
                    LocationCode = (String)GetValue(dr[FieldNames.AssortmentRegionLocationLocationCode])
                };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all region dtos for the specified assortment
        /// </summary>
        /// <param name="assortmentId">The assortment id</param>
        /// <returns>All region dtos for the assortment</returns>
        public IEnumerable<AssortmentRegionLocationDto> FetchByRegionId(Int32 assortmentRegionId)
        {
            List<AssortmentRegionLocationDto> dtoList = new List<AssortmentRegionLocationDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionLocationFetchByAssortmentRegionId))
            {
                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionLocationRegionId,
                    SqlDbType.Int,
                    assortmentRegionId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment region id</param>
        /// <returns>The assortment region</returns>
        public AssortmentRegionLocationDto FetchById(Int32 id)
        {
            AssortmentRegionLocationDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionLocationFetchById))
            {
                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionLocationId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentRegionLocationDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionLocationInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentRegionLocationId,
                    SqlDbType.Int);

                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionLocationRegionId,
                    SqlDbType.Int,
                    dto.AssortmentRegionId);

                // location id
                CreateParameter(command,
                    FieldNames.AssortmentRegionLocationLocationId,
                    SqlDbType.SmallInt,
                    dto.LocationId);

                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentRegionLocationDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionLocationUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentRegionLocationId,
                    SqlDbType.Int,
                    dto.Id);

                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionLocationRegionId,
                    SqlDbType.Int,
                    dto.AssortmentRegionId);

                // location id
                CreateParameter(command,
                    FieldNames.AssortmentRegionLocationLocationId,
                    SqlDbType.SmallInt,
                    dto.LocationId);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionLocationDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentRegionLocationId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}