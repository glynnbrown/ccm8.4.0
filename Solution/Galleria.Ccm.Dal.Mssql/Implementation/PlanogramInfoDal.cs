﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-25587 : L.Ineson
//  Created
// V8-25460 : L.Ineson
//  Added PlanogramInfoFetchByIds
// V8-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25787 : N.Foster
//  Added RowVersion
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26492 : L.Ineson
//  Added uom properties
// CCM-26520 :J.Pickup
//	Added FetchByCategoryCode & FetchByNullCategoryCode
// V8-26822 : L.Ineson
//  Added planogram group id and name
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties
// V8-27411 : M.Pettit
//  Added Locking fields
//  Added AutomationDateStarted, AutomationDateLastUpdated properties
//  Changed dto's Status property to a byte as it is now an enum value
//  Added LockUserDisplayName property
//  Added dto ValidationIsOutOfDate property
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteriaAsync
// V8-28493 : L.Ineson
//  Added FetchByWorkpackageIdAutomationProcessingStatus

#endregion

#region Version History: CCM801

// V8-28507 : D.Pleasance
//  Added FetchByWorkpackageIdPagingCriteria \ FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria

#endregion

#region Version History: CCM803

// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-29606 : N.Foster
//  Removed PlanogramInfo_FetchByMetadataCalculationRequired
//  Removed PlanogramInfo_FetchByValidationCalculationRequired

#endregion

#region Version History: CCM810

// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-29986 : A.Probyn
//  Added FetchNonDebugByCategoryCode
// V8-30079 : L.Ineson
//  Amended FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria to take a list of statuses
// V8-  : L.Luong
//  Added FetchNonDebugByLocationCode

#endregion

#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>.
// V8-30225 : M.Shelley
//  Added the Planogram UCR
// V8-30359 : A.Probyn
//  Extended for new MostRecentLinkedWorkpackageName

#endregion

#region Version History: CCM830
// CCM-32015 : N.Haywood
//  Added mission fields
// CCM-31831 : A.Probyn
//  Added more missing custom fields
// V8-32396 : A.Probyn
//  Added MetaNumberOfDelistFamilyRulesBroken
// V8-32521 : J.Pickup
// Added MetaHasComponentsOutsideOfFixtureArea
#endregion

#region Version History : CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql dal implementation of IPlanogramInfoDal
    /// </summary>
    public sealed class PlanogramInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramInfoDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogram";
        private const String _createIdTableSql = "CREATE TABLE [{0}] (Planogram_Id INT)";
        private const String _dropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        private const String _createAutomationStatusTableSql = "CREATE TABLE [{0}] (WorkpackagePlanogramProcessingStatus_AutomationStatus TINYINT)";
        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public PlanogramInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            PlanogramInfoDto dto = new PlanogramInfoDto();

            dto.Id = (Int32)GetValue(dr[FieldNames.PlanogramInfoId]);
            dto.RowVersion = new RowVersion(GetValue(dr[FieldNames.PlanogramInfoRowVersion]));
            dto.Name = (String)GetValue(dr[FieldNames.PlanogramInfoName]);
            dto.UniqueContentReference = (Guid)GetValue(dr[FieldNames.PlanogramUniqueContentReference]);
            dto.Status = (Byte)GetValue(dr[FieldNames.PlanogramInfoStatus]);
            dto.Height = (Single)GetValue(dr[FieldNames.PlanogramInfoHeight]);
            dto.Width = (Single)GetValue(dr[FieldNames.PlanogramInfoWidth]);
            dto.Depth = (Single)GetValue(dr[FieldNames.PlanogramInfoDepth]);
            dto.UserName = (String)GetValue(dr[FieldNames.PlanogramInfoUserName]);
            dto.DateCreated = (DateTime)GetValue(dr[FieldNames.PlanogramInfoDateCreated]);
            dto.DateLastModified = (DateTime)GetValue(dr[FieldNames.PlanogramInfoDateLastModified]);
            dto.DateDeleted = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoDateDeleted]);
            dto.MetaUniqueProductCount = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaUniqueProductCount]);
            dto.MetaBayCount = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaBayCount]);
            dto.MetaComponentCount = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaComponentCount]);
            dto.CategoryCode = (String)GetValue(dr[FieldNames.PlanogramInfoCategoryCode]);
            dto.CategoryName = (String)GetValue(dr[FieldNames.PlanogramInfoCategoryName]);
            dto.LengthUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramLengthUnitsOfMeasure]);
            dto.AreaUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramAreaUnitsOfMeasure]);
            dto.VolumeUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramVolumeUnitsOfMeasure]);
            dto.WeightUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramWeightUnitsOfMeasure]);
            dto.CurrencyUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramCurrencyUnitsOfMeasure]);
            dto.MetaTotalMerchandisableLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalMerchandisableLinearSpace]);
            dto.MetaTotalMerchandisableAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalMerchandisableAreaSpace]);
            dto.MetaTotalMerchandisableVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalMerchandisableVolumetricSpace]);
            dto.MetaTotalLinearWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalLinearWhiteSpace]);
            dto.MetaTotalAreaWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalAreaWhiteSpace]);
            dto.MetaTotalVolumetricWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalVolumetricWhiteSpace]);
            dto.MetaProductsPlaced = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaProductsPlaced]);
            dto.MetaProductsUnplaced = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaProductsUnplaced]);
            dto.MetaNewProducts = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaNewProducts]);
            dto.MetaChangesFromPreviousCount = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaChangesFromPreviousCount]);
            dto.MetaChangeFromPreviousStarRating = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaChangeFromPreviousStarRating]);
            dto.MetaBlocksDropped = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaBlocksDropped]);
            dto.MetaNotAchievedInventory = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaNotAchievedInventory]);
            dto.MetaTotalFacings = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalFacings]);
            dto.MetaAverageFacings = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaAverageFacings]);
            dto.MetaTotalUnits = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalUnits]);
            dto.MetaAverageUnits = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaAverageUnits]);
            dto.MetaMinDos = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaMinDos]);
            dto.MetaMaxDos = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaMaxDos]);
            dto.MetaAverageDos = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaAverageDos]);
            dto.MetaMinCases = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaMinCases]);
            dto.MetaAverageCases = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaAverageCases]);
            dto.MetaMaxCases = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaMaxCases]);
            dto.PlanogramGroupId = (Int32?)GetValue(dr[FieldNames.PlanogramInfoPlanogramGroupId]);
            dto.PlanogramGroupName = (String)GetValue(dr[FieldNames.PlanogramInfoPlanogramGroupName]);

            Byte? automationStatus = (Byte?)GetValue(dr[FieldNames.PlanogramInfoAutomationProcessingStatus]);
            if (automationStatus.HasValue)
            {
                dto.AutomationProcessingStatus = automationStatus.Value;
                dto.AutomationProcessingStatusDescription = (String)GetValue(dr[FieldNames.PlanogramInfoAutomationProcessingStatusDescription]);
                dto.AutomationProgressMax = (Int32)GetValue(dr[FieldNames.PlanogramInfoAutomationProgressMax]);
                dto.AutomationProgressCurrent = (Int32)GetValue(dr[FieldNames.PlanogramInfoAutomationProgressCurrent]);
            }
            else
            {
                dto.AutomationProcessingStatus = 0;
            }
            dto.AutomationDateStarted = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoAutomationDateStarted]);
            dto.AutomationDateLastUpdated = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoAutomationDateUpdated]);

            dto.MetaDataProcessingStatus = (Byte)GetValue(dr[FieldNames.PlanogramInfoMetaDataProcessingStatus]);
            dto.MetaDataProcessingStatusDescription = (String)GetValue(dr[FieldNames.PlanogramInfoMetaDataProcessingStatusDescription]);
            dto.MetaDataProgressMax = (Int32)GetValue(dr[FieldNames.PlanogramInfoMetaDataProgressMax]);
            dto.MetaDataProgressCurrent = (Int32)GetValue(dr[FieldNames.PlanogramInfoMetaDataProgressCurrent]);
            dto.MetaDataDateStarted = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoMetaDataDateStarted]);
            dto.MetaDataDateLastUpdated = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoMetaDataDateUpdated]);
            dto.ValidationProcessingStatus = (Byte)GetValue(dr[FieldNames.PlanogramInfoValidationProcessingStatus]);
            dto.ValidationProcessingStatusDescription = (String)GetValue(dr[FieldNames.PlanogramInfoValidationProcessingStatusDescription]);
            dto.ValidationProgressMax = (Int32)GetValue(dr[FieldNames.PlanogramInfoValidationProgressMax]);
            dto.ValidationProgressCurrent = (Int32)GetValue(dr[FieldNames.PlanogramInfoValidationProgressCurrent]);
            dto.ValidationDateStarted = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoValidationDateStarted]);
            dto.ValidationDateLastUpdated = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoValidationDateUpdated]);
            dto.MetaTotalComponentsOverMerchandisedWidth = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalComponentsOverMerchandisedWidth]);
            dto.MetaTotalComponentsOverMerchandisedHeight = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalComponentsOverMerchandisedHeight]);
            dto.MetaTotalComponentsOverMerchandisedDepth = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalComponentsOverMerchandisedDepth]);
            dto.MetaTotalComponentCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalComponentCollisions]);
            dto.MetaTotalPositionCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalPositionCollisions]);
            dto.MetaTotalFrontFacings = (Int16?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalFrontFacings]);
            dto.MetaAverageFrontFacings = (Single?)GetValue(dr[FieldNames.PlanogramInfoMetaAverageFrontFacings]);
            dto.DateMetadataCalculated = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoDateMetadataCalculated]);
            dto.DateValidationDataCalculated = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoDateValidationDataCalculated]);
            dto.IsLocked = (Boolean)GetValue(dr[FieldNames.PlanogramInfoIsLocked]);
            dto.LockType = (Byte?)GetValue(dr[FieldNames.PlanogramInfoLockType]);
            dto.LockUserId = (Int32?)GetValue(dr[FieldNames.PlanogramInfoLockUserId]);
            dto.LockUserName = (String)GetValue(dr[FieldNames.PlanogramInfoLockUserName]);
            dto.LockUserDisplayName = (String)GetValue(dr[FieldNames.PlanogramInfoLockFullName]);
            dto.LockDate = (DateTime?)GetValue(dr[FieldNames.PlanogramInfoLockDate]);
            dto.ValidationIsOutOfDate = (Boolean)GetValue(dr[FieldNames.PlanogramInfoValidationIsOutOfDate]);
            dto.ClusterSchemeName = (String)GetValue(dr[FieldNames.PlanogramInfoClusterSchemeName]);
            dto.ClusterName = (String)GetValue(dr[FieldNames.PlanogramInfoClusterName]);
            dto.LocationCode = (String)GetValue(dr[FieldNames.PlanogramInfoLocationCode]);
            dto.LocationName = (String)GetValue(dr[FieldNames.PlanogramInfoLocationName]);
            dto.MetaNoOfErrors = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaNoOfErrors]);
            dto.MetaNoOfWarnings = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaNoOfWarnings]);
            dto.MetaTotalErrorScore = (Int32?)GetValue(dr[FieldNames.PlanogramInfoMetaTotalErrorScore]);
            dto.MetaHighestErrorScore = (Byte?)GetValue(dr[FieldNames.PlanogramInfoMetaHighestErrorScore]);
            dto.DateWip = (DateTime?)GetValue(dr[FieldNames.PlanogramDateWip]);
            dto.DateApproved = (DateTime?)GetValue(dr[FieldNames.PlanogramDateApproved]);
            dto.DateArchived = (DateTime?)GetValue(dr[FieldNames.PlanogramDateArchived]);
            dto.PlanogramType = (Byte)GetValue(dr[FieldNames.PlanogramPlanogramType]);
            dto.MetaNoOfDebugEvents = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNoOfDebugEvents]);
            dto.MetaNoOfInformationEvents = (Int32?)GetValue(dr[FieldNames.PlanogramMetaNoOfInformationEvents]);
            dto.MostRecentLinkedWorkpackageName = (String)GetValue(dr[FieldNames.PlanogramInfoMostRecentLinkedWorkpackageName]);

            dto.AngleUnitsOfMeasure = (Byte)GetValue(dr[FieldNames.PlanogramAngleUnitsOfMeasure]);
            dto.HighlightSequenceStrategy = (String)GetValue(dr[FieldNames.PlanogramHighlightSequenceStrategy]);
            dto.AssignedLocationCount = (Int32)GetValue(dr[FieldNames.PlanogramInfoAssignedLocationCount]);
            dto.MetaCountOfPlacedProductsNotRecommendedInAssortment = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment]);
            dto.MetaCountOfPlacedProductsRecommendedInAssortment = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfPlacedProductsRecommendedInAssortment]);
            dto.MetaCountOfPositionsOutsideOfBlockSpace = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfPositionsOutsideOfBlockSpace]);
            dto.MetaCountOfProductNotAchievedCases = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductNotAchievedCases]);
            dto.MetaCountOfProductNotAchievedDeliveries = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductNotAchievedDeliveries]);
            dto.MetaCountOfProductNotAchievedDOS = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductNotAchievedDOS]);
            dto.MetaCountOfProductOverShelfLifePercent = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductOverShelfLifePercent]);
            dto.MetaCountOfRecommendedProductsInAssortment = (Int32?)GetValue(dr[FieldNames.PlanogramMetaCountOfRecommendedProductsInAssortment]);
            dto.MetaNumberOfAssortmentRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfAssortmentRulesBroken]);
            dto.MetaNumberOfCoreRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfCoreRulesBroken]);
            dto.MetaNumberOfDelistProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDelistProductRulesBroken]);
            dto.MetaNumberOfDependencyFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDependencyFamilyRulesBroken]);
            dto.MetaNumberOfDelistFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDelistFamilyRulesBroken]);
            dto.MetaNumberOfDistributionRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfDistributionRulesBroken]);
            dto.MetaNumberOfFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfFamilyRulesBroken]);
            dto.MetaNumberOfForceProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfForceProductRulesBroken]);
            dto.MetaNumberOfInheritanceRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfInheritanceRulesBroken]);
            dto.MetaNumberOfLocalProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfLocalProductRulesBroken]);
            dto.MetaNumberOfMaximumProductFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfMaximumProductFamilyRulesBroken]);
            dto.MetaNumberOfMinimumHurdleProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfMinimumHurdleProductRulesBroken]);
            dto.MetaNumberOfMinimumProductFamilyRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfMinimumProductFamilyRulesBroken]);
            dto.MetaNumberOfPreserveProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfPreserveProductRulesBroken]);
            dto.MetaNumberOfProductRulesBroken = (Int16?)GetValue(dr[FieldNames.PlanogramMetaNumberOfProductRulesBroken]);
            dto.MetaPercentageOfAssortmentRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfAssortmentRulesBroken]);
            dto.MetaPercentageOfCoreRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfCoreRulesBroken]);
            dto.MetaPercentageOfDistributionRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfDistributionRulesBroken]);
            dto.MetaPercentageOfFamilyRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfFamilyRulesBroken]);
            dto.MetaPercentageOfInheritanceRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfInheritanceRulesBroken]);
            dto.MetaPercentageOfLocalProductRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfLocalProductRulesBroken]);
            dto.MetaPercentageOfProductRulesBroken = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentageOfProductRulesBroken]);
            dto.MetaPercentOfPlacedProductsRecommendedInAssortment = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment]);
            dto.MetaPercentOfPositionsOutsideOfBlockSpace = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfPositionsOutsideOfBlockSpace]);
            dto.MetaPercentOfProductNotAchievedCases = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedCases]);
            dto.MetaPercentOfProductNotAchievedDeliveries = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedDeliveries]);
            dto.MetaPercentOfProductNotAchievedDOS = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedDOS]);
            dto.MetaPercentOfProductOverShelfLifePercent = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductOverShelfLifePercent]);
            dto.MetaPercentOfProductNotAchievedInventory = (Single?)GetValue(dr[FieldNames.PlanogramMetaPercentOfProductNotAchievedInventory]);
            dto.MetaHasComponentsOutsideOfFixtureArea = (Boolean?)GetValue(dr[FieldNames.PlanogramMetaHasComponentsOutsideOfFixtureArea]);
            dto.MetaCountOfProductsBuddied = (Int16?)GetValue(dr[FieldNames.PlanogramMetaCountOfProductsBuddied]);

            dto.Text1 = (String)GetValue(dr[FieldNames.CustomAttributeDataText1]);
            dto.Text2 = (String)GetValue(dr[FieldNames.CustomAttributeDataText2]);
            dto.Text3 = (String)GetValue(dr[FieldNames.CustomAttributeDataText3]);
            dto.Text4 = (String)GetValue(dr[FieldNames.CustomAttributeDataText4]);
            dto.Text5 = (String)GetValue(dr[FieldNames.CustomAttributeDataText5]);
            dto.Text6 = (String)GetValue(dr[FieldNames.CustomAttributeDataText6]);
            dto.Text7 = (String)GetValue(dr[FieldNames.CustomAttributeDataText7]);
            dto.Text8 = (String)GetValue(dr[FieldNames.CustomAttributeDataText8]);
            dto.Text9 = (String)GetValue(dr[FieldNames.CustomAttributeDataText9]);
            dto.Text10 = (String)GetValue(dr[FieldNames.CustomAttributeDataText10]);
            dto.Text11 = (String)GetValue(dr[FieldNames.CustomAttributeDataText11]);
            dto.Text12 = (String)GetValue(dr[FieldNames.CustomAttributeDataText12]);
            dto.Text13 = (String)GetValue(dr[FieldNames.CustomAttributeDataText13]);
            dto.Text14 = (String)GetValue(dr[FieldNames.CustomAttributeDataText14]);
            dto.Text15 = (String)GetValue(dr[FieldNames.CustomAttributeDataText15]);
            dto.Text16 = (String)GetValue(dr[FieldNames.CustomAttributeDataText16]);
            dto.Text17 = (String)GetValue(dr[FieldNames.CustomAttributeDataText17]);
            dto.Text18 = (String)GetValue(dr[FieldNames.CustomAttributeDataText18]);
            dto.Text19 = (String)GetValue(dr[FieldNames.CustomAttributeDataText19]);
            dto.Text20 = (String)GetValue(dr[FieldNames.CustomAttributeDataText20]);
            dto.Text21 = (String)GetValue(dr[FieldNames.CustomAttributeDataText21]);
            dto.Text22 = (String)GetValue(dr[FieldNames.CustomAttributeDataText22]);
            dto.Text23 = (String)GetValue(dr[FieldNames.CustomAttributeDataText23]);
            dto.Text24 = (String)GetValue(dr[FieldNames.CustomAttributeDataText24]);
            dto.Text25 = (String)GetValue(dr[FieldNames.CustomAttributeDataText25]);
            dto.Text26 = (String)GetValue(dr[FieldNames.CustomAttributeDataText26]);
            dto.Text27 = (String)GetValue(dr[FieldNames.CustomAttributeDataText27]);
            dto.Text28 = (String)GetValue(dr[FieldNames.CustomAttributeDataText28]);
            dto.Text29 = (String)GetValue(dr[FieldNames.CustomAttributeDataText29]);
            dto.Text30 = (String)GetValue(dr[FieldNames.CustomAttributeDataText30]);
            dto.Text31 = (String)GetValue(dr[FieldNames.CustomAttributeDataText31]);
            dto.Text32 = (String)GetValue(dr[FieldNames.CustomAttributeDataText32]);
            dto.Text33 = (String)GetValue(dr[FieldNames.CustomAttributeDataText33]);
            dto.Text34 = (String)GetValue(dr[FieldNames.CustomAttributeDataText34]);
            dto.Text35 = (String)GetValue(dr[FieldNames.CustomAttributeDataText35]);
            dto.Text36 = (String)GetValue(dr[FieldNames.CustomAttributeDataText36]);
            dto.Text37 = (String)GetValue(dr[FieldNames.CustomAttributeDataText37]);
            dto.Text38 = (String)GetValue(dr[FieldNames.CustomAttributeDataText38]);
            dto.Text39 = (String)GetValue(dr[FieldNames.CustomAttributeDataText39]);
            dto.Text40 = (String)GetValue(dr[FieldNames.CustomAttributeDataText40]);
            dto.Text41 = (String)GetValue(dr[FieldNames.CustomAttributeDataText41]);
            dto.Text42 = (String)GetValue(dr[FieldNames.CustomAttributeDataText42]);
            dto.Text43 = (String)GetValue(dr[FieldNames.CustomAttributeDataText43]);
            dto.Text44 = (String)GetValue(dr[FieldNames.CustomAttributeDataText44]);
            dto.Text45 = (String)GetValue(dr[FieldNames.CustomAttributeDataText45]);
            dto.Text46 = (String)GetValue(dr[FieldNames.CustomAttributeDataText46]);
            dto.Text47 = (String)GetValue(dr[FieldNames.CustomAttributeDataText47]);
            dto.Text48 = (String)GetValue(dr[FieldNames.CustomAttributeDataText48]);
            dto.Text49 = (String)GetValue(dr[FieldNames.CustomAttributeDataText49]);
            dto.Text50 = (String)GetValue(dr[FieldNames.CustomAttributeDataText50]);
            dto.Value1 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue1]);
            dto.Value2 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue2]);
            dto.Value3 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue3]);
            dto.Value4 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue4]);
            dto.Value5 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue5]);
            dto.Value6 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue6]);
            dto.Value7 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue7]);
            dto.Value8 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue8]);
            dto.Value9 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue9]);
            dto.Value10 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue10]);
            dto.Value11 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue11]);
            dto.Value12 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue12]);
            dto.Value13 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue13]);
            dto.Value14 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue14]);
            dto.Value15 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue15]);
            dto.Value16 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue16]);
            dto.Value17 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue17]);
            dto.Value18 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue18]);
            dto.Value19 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue19]);
            dto.Value20 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue20]);
            dto.Value21 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue21]);
            dto.Value22 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue22]);
            dto.Value23 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue23]);
            dto.Value24 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue24]);
            dto.Value25 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue25]);
            dto.Value26 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue26]);
            dto.Value27 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue27]);
            dto.Value28 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue28]);
            dto.Value29 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue29]);
            dto.Value30 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue30]);
            dto.Value31 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue31]);
            dto.Value32 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue32]);
            dto.Value33 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue33]);
            dto.Value34 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue34]);
            dto.Value35 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue35]);
            dto.Value36 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue36]);
            dto.Value37 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue37]);
            dto.Value38 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue38]);
            dto.Value39 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue39]);
            dto.Value40 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue40]);
            dto.Value41 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue41]);
            dto.Value42 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue42]);
            dto.Value43 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue43]);
            dto.Value44 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue44]);
            dto.Value45 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue45]);
            dto.Value46 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue46]);
            dto.Value47 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue47]);
            dto.Value48 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue48]);
            dto.Value49 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue49]);
            dto.Value50 = (Single?)GetValue(dr[FieldNames.CustomAttributeDataValue50]);
            dto.Flag1 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag1]);
            dto.Flag2 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag2]);
            dto.Flag3 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag3]);
            dto.Flag4 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag4]);
            dto.Flag5 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag5]);
            dto.Flag6 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag6]);
            dto.Flag7 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag7]);
            dto.Flag8 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag8]);
            dto.Flag9 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag9]);
            dto.Flag10 = (Boolean?)GetValue(dr[FieldNames.CustomAttributeDataFlag10]);
            dto.Date1 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate1]);
            dto.Date2 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate2]);
            dto.Date3 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate3]);
            dto.Date4 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate4]);
            dto.Date5 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate5]);
            dto.Date6 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate6]);
            dto.Date7 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate7]);
            dto.Date8 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate8]);
            dto.Date9 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate9]);
            dto.Date10 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate10]);
            dto.Note1 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote1]);
            dto.Note2 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote2]);
            dto.Note3 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote3]);
            dto.Note4 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote4]);
            dto.Note5 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote5]);


            return dto;
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns infos for the given planogram group id
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByPlanogramGroupId(Int32 planogramGroupId)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByPlanogramGroupId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoPlanogramGroupId, SqlDbType.Int, planogramGroupId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns infos for the given workpackage id
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByWorkpackageId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoWorkpackageId, SqlDbType.Int, workpackageId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdPagingCriteria(Int32 workpackageId, Byte pageNumber, Int16 pageSize)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByWorkpackageIdPagingCriteria))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.GenericPageNumber, SqlDbType.TinyInt, pageNumber);
                    CreateParameter(command, FieldNames.GenericPageSize, SqlDbType.SmallInt, pageSize);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns infos for the given workpackage id and processing status
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdAutomationProcessingStatus(Int32 workpackageId, Byte automationProcessingStatus)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByWorkpackageIdAutomationProcessingStatus))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.PlanogramInfoAutomationProcessingStatus, SqlDbType.TinyInt, automationProcessingStatus);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(
            Int32 workpackageId, List<Byte> statusList, Byte pageNumber, Int16 pageSize)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByWorkpackageIdAutomationProcessingStatusPagingCriteria))
                {
                    //create a temp table for the automation statuses
                    CreateTempTable(_createAutomationStatusTableSql);

                    // perform a batch insert to temp table
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                    {
                        bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                        bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                        bulkCopy.WriteToServer(new BulkCopyDataReader<Byte>(statusList));
                    }

                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.GenericPageNumber, SqlDbType.TinyInt, pageNumber);
                    CreateParameter(command, FieldNames.GenericPageSize, SqlDbType.SmallInt, pageSize);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop the temporary table
            DropTempTable();

            return dtoList;
        }

        /// <summary>
        /// Returns infos for all plans which match the search criteria
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchBySearchCriteria(String name, Int32? planogramGroupId, Int32 entityId)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchBySearchCriteria))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoName, SqlDbType.NVarChar, name);
                    CreateParameter(command, FieldNames.PlanogramInfoPlanogramGroupId, SqlDbType.Int, planogramGroupId);
                    CreateParameter(command, FieldNames.PlanogramInfoEntityID, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns all planograms for the given ids.
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByIds(IEnumerable<Int32> idList)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();

            // create a temp table for products
            CreateTempTable(_createIdTableSql);

            // perform a batch insert to temp table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                bulkCopy.WriteToServer(new BulkCopyDataReader<Int32>(idList));
            }

            try
            {
                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByIds))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop the temporary table
            DropTempTable();

            // and return the items
            return dtoList;

        }

        /// <summary>
        /// Returns infos for the given planogram category code
        /// </summary>
        /// <param name="planogramGroupId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramInfoDto> FetchByCategoryCode(String categoryCode)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByCategoryCode))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoCategoryCode, SqlDbType.NVarChar, categoryCode);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns non debug planogram infos for the given planogram category code
        /// </summary>
        /// <param name="planogramGroupId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramInfoDto> FetchNonDebugByCategoryCode(String categoryCode)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchNonDebugByCategoryCode))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoCategoryCode, SqlDbType.NVarChar, categoryCode);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns non debug planogram infos for the given planogram location code
        /// </summary>
        /// <param name="planogramGroupId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramInfoDto> FetchNonDebugByLocationCode(String locationCode)
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchNonDebugByLocationCode))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramInfoLocationCode, SqlDbType.NVarChar, locationCode);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns infos for the given planogram category code
        /// </summary>
        public IEnumerable<PlanogramInfoDto> FetchByNullCategoryCode()
        {
            List<PlanogramInfoDto> dtoList = new List<PlanogramInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInfoFetchByNullCategoryCode))
                {
                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateTempTable(String createSql)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, createSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the store temporary table on the given connection
        /// </summary>
        private void DropTempTable()
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
