﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS.    
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-27058 : A.Probyn
//  Updated for new meta data properties
// V8-27558 : L.Ineson
//  Removed IsBay
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion
#region Version History: CCM810
// V8-30210 : M.Brumby
//  Update was using incorrect types for DOS and case pack values
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramFixtureItemDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramFixtureItemDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramFixtureItemDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramFixtureItemDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramFixtureItemId]),
                PlanogramId = (Object)GetValue(dr[FieldNames.PlanogramFixtureItemPlanogramId]),
                PlanogramFixtureId = (Object)GetValue(dr[FieldNames.PlanogramFixtureItemPlanogramFixtureId]),
                X = (Single)GetValue(dr[FieldNames.PlanogramFixtureItemX]),
                Y = (Single)GetValue(dr[FieldNames.PlanogramFixtureItemY]),
                Z = (Single)GetValue(dr[FieldNames.PlanogramFixtureItemZ]),
                Slope = (Single)GetValue(dr[FieldNames.PlanogramFixtureItemSlope]),
                Angle = (Single)GetValue(dr[FieldNames.PlanogramFixtureItemAngle]),
                Roll = (Single)GetValue(dr[FieldNames.PlanogramFixtureItemRoll]),
                BaySequenceNumber = (Int16)GetValue(dr[FieldNames.PlanogramFixtureItemBaySequenceNumber]),
                MetaComponentCount = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaComponentCount]),
                MetaTotalMerchandisableLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalMerchandisableLinearSpace]),
                MetaTotalMerchandisableAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalMerchandisableAreaSpace]),
                MetaTotalMerchandisableVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace]),
                MetaTotalLinearWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalLinearWhiteSpace]),
                MetaTotalAreaWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalAreaWhiteSpace]),
                MetaTotalVolumetricWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalVolumetricWhiteSpace]),
                MetaProductsPlaced = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaProductsPlaced]),
                MetaNewProducts = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaNewProducts]),
                MetaChangesFromPreviousCount = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaChangesFromPreviousCount]),
                MetaChangeFromPreviousStarRating = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaChangeFromPreviousStarRating]),
                MetaBlocksDropped = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaBlocksDropped]),
                MetaNotAchievedInventory = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaNotAchievedInventory]),
                MetaTotalFacings = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalFacings]),
                MetaAverageFacings = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaAverageFacings]),
                MetaTotalUnits = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalUnits]),
                MetaAverageUnits = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaAverageUnits]),
                MetaMinDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaMinDos]),
                MetaMaxDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaMaxDos]),
                MetaAverageDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaAverageDos]),
                MetaMinCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaMinCases]),
                MetaAverageCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaAverageCases]),
                MetaMaxCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaMaxCases]),
                MetaSpaceToUnitsIndex = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaSpaceToUnitsIndex]),
                MetaTotalComponentsOverMerchandisedWidth = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth]),
                MetaTotalComponentsOverMerchandisedHeight = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight]),
                MetaTotalComponentsOverMerchandisedDepth = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth]),
                MetaTotalComponentCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalComponentCollisions]),
                MetaTotalPositionCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalPositionCollisions]),
                MetaTotalFrontFacings = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaTotalFrontFacings]),
                MetaAverageFrontFacings = (Single?)GetValue(dr[FieldNames.PlanogramFixtureItemMetaAverageFrontFacings])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramFixtureItemDto> FetchByPlanogramId(Object planogramId)
        {
            List<PlanogramFixtureItemDto> dtoList = new List<PlanogramFixtureItemDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureItemFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramFixtureItemPlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramFixtureItemDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureItemInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramFixtureItemId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramFixtureItemPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemPlanogramFixtureId, SqlDbType.Int, dto.PlanogramFixtureId);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemZ, SqlDbType.Real, dto.Z);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemSlope, SqlDbType.Real, dto.Slope);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemAngle, SqlDbType.Real, dto.Angle);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemRoll, SqlDbType.Real, dto.Roll);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemBaySequenceNumber, SqlDbType.SmallInt, dto.BaySequenceNumber);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaComponentCount, SqlDbType.Int, dto.MetaComponentCount);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableLinearSpace, SqlDbType.Real, dto.MetaTotalMerchandisableLinearSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableAreaSpace, SqlDbType.Real, dto.MetaTotalMerchandisableAreaSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace, SqlDbType.Real, dto.MetaTotalMerchandisableVolumetricSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalLinearWhiteSpace, SqlDbType.Real, dto.MetaTotalLinearWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalAreaWhiteSpace, SqlDbType.Real, dto.MetaTotalAreaWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalVolumetricWhiteSpace, SqlDbType.Real, dto.MetaTotalVolumetricWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaProductsPlaced, SqlDbType.Int, dto.MetaProductsPlaced);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaNewProducts, SqlDbType.Int, dto.MetaNewProducts);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaChangesFromPreviousCount, SqlDbType.Int, dto.MetaChangesFromPreviousCount);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaChangeFromPreviousStarRating, SqlDbType.Int, dto.MetaChangeFromPreviousStarRating);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaBlocksDropped, SqlDbType.Int, dto.MetaBlocksDropped);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaNotAchievedInventory, SqlDbType.Int, dto.MetaNotAchievedInventory);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalFacings, SqlDbType.Int, dto.MetaTotalFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageFacings, SqlDbType.SmallInt, dto.MetaAverageFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalUnits, SqlDbType.Int, dto.MetaTotalUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageUnits, SqlDbType.Int, dto.MetaAverageUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMinDos, SqlDbType.Real, dto.MetaMinDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMaxDos, SqlDbType.Real, dto.MetaMaxDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageDos, SqlDbType.Real, dto.MetaAverageDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMinCases, SqlDbType.Real, dto.MetaMinCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageCases, SqlDbType.Real, dto.MetaAverageCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMaxCases, SqlDbType.Real, dto.MetaMaxCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaSpaceToUnitsIndex, SqlDbType.SmallInt, dto.MetaSpaceToUnitsIndex);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth, SqlDbType.Int, dto.MetaTotalComponentsOverMerchandisedWidth);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight, SqlDbType.Int, dto.MetaTotalComponentsOverMerchandisedHeight);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth, SqlDbType.Int, dto.MetaTotalComponentsOverMerchandisedDepth);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentCollisions, SqlDbType.Int, dto.MetaTotalComponentCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalPositionCollisions, SqlDbType.Int, dto.MetaTotalPositionCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalFrontFacings, SqlDbType.SmallInt, dto.MetaTotalFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageFrontFacings, SqlDbType.Real, dto.MetaAverageFrontFacings);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            foreach (PlanogramFixtureItemDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramFixtureItemDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureItemUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramFixtureItemId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemPlanogramFixtureId, SqlDbType.Int, dto.PlanogramFixtureId);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemZ, SqlDbType.Real, dto.Z);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemSlope, SqlDbType.Real, dto.Slope);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemAngle, SqlDbType.Real, dto.Angle);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemRoll, SqlDbType.Real, dto.Roll);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemBaySequenceNumber, SqlDbType.SmallInt, dto.BaySequenceNumber);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaComponentCount, SqlDbType.Int, dto.MetaComponentCount);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableLinearSpace, SqlDbType.Real, dto.MetaTotalMerchandisableLinearSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableAreaSpace, SqlDbType.Real, dto.MetaTotalMerchandisableAreaSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace, SqlDbType.Real, dto.MetaTotalMerchandisableVolumetricSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalLinearWhiteSpace, SqlDbType.Real, dto.MetaTotalLinearWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalAreaWhiteSpace, SqlDbType.Real, dto.MetaTotalAreaWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalVolumetricWhiteSpace, SqlDbType.Real, dto.MetaTotalVolumetricWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaProductsPlaced, SqlDbType.Int, dto.MetaProductsPlaced);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaNewProducts, SqlDbType.Int, dto.MetaNewProducts);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaChangesFromPreviousCount, SqlDbType.Int, dto.MetaChangesFromPreviousCount);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaChangeFromPreviousStarRating, SqlDbType.Int, dto.MetaChangeFromPreviousStarRating);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaBlocksDropped, SqlDbType.Int, dto.MetaBlocksDropped);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaNotAchievedInventory, SqlDbType.Int, dto.MetaNotAchievedInventory);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalFacings, SqlDbType.Int, dto.MetaTotalFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageFacings, SqlDbType.SmallInt, dto.MetaAverageFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalUnits, SqlDbType.Int, dto.MetaTotalUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageUnits, SqlDbType.Int, dto.MetaAverageUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMinDos, SqlDbType.Real, dto.MetaMinDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMaxDos, SqlDbType.Real, dto.MetaMaxDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageDos, SqlDbType.Real, dto.MetaAverageDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMinCases, SqlDbType.Real, dto.MetaMinCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageCases, SqlDbType.Real, dto.MetaAverageCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaMaxCases, SqlDbType.Real, dto.MetaMaxCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaSpaceToUnitsIndex, SqlDbType.SmallInt, dto.MetaSpaceToUnitsIndex);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth, SqlDbType.Int, dto.MetaTotalComponentsOverMerchandisedWidth);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight, SqlDbType.Int, dto.MetaTotalComponentsOverMerchandisedHeight);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth, SqlDbType.Int, dto.MetaTotalComponentsOverMerchandisedDepth);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalComponentCollisions, SqlDbType.Int, dto.MetaTotalComponentCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalPositionCollisions, SqlDbType.Int, dto.MetaTotalPositionCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaTotalFrontFacings, SqlDbType.SmallInt, dto.MetaTotalFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureItemMetaAverageFrontFacings, SqlDbType.Real, dto.MetaAverageFrontFacings);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            foreach (PlanogramFixtureItemDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureItemDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramFixtureItemId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramFixtureItemDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            foreach (PlanogramFixtureItemDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
