﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PerformanceSelectionTimelineGroup Dal
    /// </summary>
    public class PerformanceSelectionTimelineGroupDal : Galleria.Framework.Dal.Mssql.DalBase, IPerformanceSelectionTimelineGroupDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PerformanceSelectionTimelineGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PerformanceSelectionTimelineGroupDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PerformanceSelectionTimelineGroupId]),
                PerformanceSelectionId = (Int32)GetValue(dr[FieldNames.PerformanceSelectionTimelineGroupPerformanceSelectionId]),
                Code = (Int64)GetValue(dr[FieldNames.PerformanceSelectionTimelineGroupCode])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="performanceSelectionId">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PerformanceSelectionTimelineGroupDto> FetchByPerformanceSelectionId(Int32 performanceSelectionId)
        {
            List<PerformanceSelectionTimelineGroupDto> dtoList = new List<PerformanceSelectionTimelineGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionTimelineGroupFetchByPerformanceSelectionId))
                {
                    CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupPerformanceSelectionId, SqlDbType.Int, performanceSelectionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PerformanceSelectionTimelineGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionTimelineGroupInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupPerformanceSelectionId, SqlDbType.Int, dto.PerformanceSelectionId);
                    CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupCode, SqlDbType.BigInt, dto.Code);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PerformanceSelectionTimelineGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionTimelineGroupUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupPerformanceSelectionId, SqlDbType.Int, dto.PerformanceSelectionId);
                    CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupCode, SqlDbType.BigInt, dto.Code);

                    //execute
                    command.ExecuteNonQuery();
               }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionTimelineGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionTimelineGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}