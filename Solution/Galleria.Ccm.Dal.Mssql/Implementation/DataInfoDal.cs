﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.3)
// V8-29135 : D.Pleasance
//  Created (From SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Dal.Mssql.Schema;
using System.Data;
using System.Data.SqlClient;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Our mssql implemention of the IDataInfoDal
    /// </summary>
    public class DataInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IDataInfoDal
    {
        #region Fetch

        /// <summary>
        /// Returns the data info for the entityId
        /// </summary>
        /// <returns>Data Info Dto</returns>
        public DataInfoDto FetchByEntityId(Int32 entityId)
        {
            DataInfoDto dto = new DataInfoDto();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.DataInfoFetchByEntityId))
                {
                    // entity id
                    CreateParameter(command,
                        FieldNames.EntityId,
                        SqlDbType.Int,
                        entityId);

                    // data info - product hierarchy
                    SqlParameter hasProductHierarchyParameter = CreateParameter(command,
                        FieldNames.DataInfoHasProductHierarchy,
                        SqlDbType.Bit);

                    // data info - location hierarchy
                    SqlParameter hasLocationHierarchyParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocationHierarchy,
                        SqlDbType.Bit);
                    
                    // data info - location
                    SqlParameter hasLocationParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocation,
                        SqlDbType.Bit);

                    // data info - product
                    SqlParameter hasProductParameter = CreateParameter(command,
                        FieldNames.DataInfoHasProduct,
                        SqlDbType.Bit);
                    
                    // data info - assortment
                    SqlParameter hasAssortmentParameter = CreateParameter(command,
                        FieldNames.DataInfoHasAssortment,
                        SqlDbType.Bit);

                    // data info - location space
                    SqlParameter hasLocationSpaceParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocationSpace,
                        SqlDbType.Bit);

                    // data info - location space bay
                    SqlParameter hasLocationSpaceBayParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocationSpaceBay,
                        SqlDbType.Bit);

                    // data info - location space element
                    SqlParameter hasLocationSpaceElementParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocationSpaceElement,
                        SqlDbType.Bit);

                    // data info - cluster
                    SqlParameter hasClusterSchemeParameter = CreateParameter(command,
                        FieldNames.DataInfoHasClusterScheme,
                        SqlDbType.Bit);

                    // data info - location product attribute
                    SqlParameter hasLocationProductAttributeParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocationProductAttribute,
                        SqlDbType.Bit);

                    // data info - location product illegal
                    SqlParameter hasLocationProductIllegalParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocationProductIllegal,
                        SqlDbType.Bit);

                    // data info - location product legal
                    SqlParameter hasLocationProductLegalParameter = CreateParameter(command,
                        FieldNames.DataInfoHasLocationProductLegal,
                        SqlDbType.Bit);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto with returned values
                    dto.HasProductHierarchy = (Boolean)hasProductHierarchyParameter.Value;
                    dto.HasLocationHierarchy = (Boolean)hasLocationHierarchyParameter.Value;
                    dto.HasLocation = (Boolean)hasLocationParameter.Value;
                    dto.HasProduct = (Boolean)hasProductParameter.Value;
                    dto.HasAssortment = (Boolean)hasAssortmentParameter.Value;
                    dto.HasLocationSpace = (Boolean)hasLocationSpaceParameter.Value;
                    dto.HasLocationSpaceBay = (Boolean)hasLocationSpaceBayParameter.Value;
                    dto.HasLocationSpaceElement = (Boolean)hasLocationSpaceElementParameter.Value;
                    dto.HasClusterScheme = (Boolean)hasClusterSchemeParameter.Value;
                    dto.HasLocationProductAttribute = (Boolean)hasLocationProductAttributeParameter.Value;
                    dto.HasLocationProductIllegal = (Boolean)hasLocationProductIllegalParameter.Value;
                    dto.HasLocationProductLegal = (Boolean)hasLocationProductLegalParameter.Value;

                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion
    }
}
