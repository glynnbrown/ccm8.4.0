﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 J.Pickup
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Data;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Mssql.Schema;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    class HighlightCharacteristicDal : DalBase, IHighlightCharacteristicDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static HighlightCharacteristicDto GetDataTransferObject(IDataRecord dr)
        {
            return new HighlightCharacteristicDto
            {
                Id = (Int32)GetValue(dr[FieldNames.HighlightCharacteristicId]),
                HighlightId = (Int32)GetValue(dr[FieldNames.HighlightCharacteristicHighlightId]),
                IsAndFilter = (Boolean)GetValue(dr[FieldNames.HighlightCharacteristicIsAndFilter]),
                Name = (String)GetValue(dr[FieldNames.HighlightCharacteristicName]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="highlightCharachteristicDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="highlightCharachteristicDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="highlightCharachteristicDto" /> with the data for the provided <paramref name="id" />.</returns>
        public IEnumerable<HighlightCharacteristicDto> FetchByHighlightId(Object id)
        {
            List<HighlightCharacteristicDto> dtoList = new List<HighlightCharacteristicDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicFetchByHighlightCharacteristicId))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightCharacteristicHighlightId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="HighlightCharacteristicDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(HighlightCharacteristicDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.HighlightCharacteristicId, SqlDbType.Int);

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightCharacteristicName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.HighlightCharacteristicHighlightId, SqlDbType.Int, dto.HighlightId);
                    CreateParameter(command, FieldNames.HighlightCharacteristicIsAndFilter, SqlDbType.Bit, dto.IsAndFilter);


                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="HighlightCharacteristicDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(HighlightCharacteristicDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightCharacteristicId, SqlDbType.Int, dto.Id);

                  
                    //Other properties
                    CreateParameter(command, FieldNames.HighlightCharacteristicName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.HighlightCharacteristicHighlightId, SqlDbType.Int, dto.HighlightId);
                    CreateParameter(command, FieldNames.HighlightCharacteristicIsAndFilter, SqlDbType.Bit, dto.IsAndFilter);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    // Nothing to update.
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightCharacteristicId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
