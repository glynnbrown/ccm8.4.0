﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS. 
// V8-25881 : A.Probyn
//  Added MetaData properties   
// V8-27058 : A.Probyn
//  Updated MetaData properties
// V8-27474 : A.Silva
//  Added ComponentSequenceNumber property.
// V8-27605 : A.silva
//  Added missing metadata properties.
#endregion

#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion

#region Version History: CCM802
// V8-28811 : L.Luong
//  Added MetaPercentageLinearSpaceFilled
#endregion

#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added NotchNumber property to allow saving to the database
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29514 : L.Ineson
//	Removed PlanogramAssemblyComponent_MetaComponentCount
#endregion

#region Version History: (CCM 8.3)
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// The MSSQL DAl for the PlanogramAssemblyComponent DTO.
    /// </summary>
    public class PlanogramAssemblyComponentDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssemblyComponentDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssemblyComponent Dto from the given reader.
        /// </summary>
        /// <param name="dr">The data reader to load from.</param>
        /// <returns>A PlanogramAssemblyComponent DTO.</returns>
        public PlanogramAssemblyComponentDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssemblyComponentDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssemblyComponentId]),
                PlanogramAssemblyId = (Int32)GetValue(dr[FieldNames.PlanogramAssemblyComponentPlanogramAssemblyId]),
                PlanogramComponentId = (Int32)GetValue(dr[FieldNames.PlanogramAssemblyComponentPlanogramComponentId]),
                ComponentSequenceNumber = (Int16?)GetValue(dr[FieldNames.PlanogramAssemblyComponentComponentSequenceNumber]),
                X = (Single)GetValue(dr[FieldNames.PlanogramAssemblyComponentX]),
                Y = (Single)GetValue(dr[FieldNames.PlanogramAssemblyComponentY]),
                Z = (Single)GetValue(dr[FieldNames.PlanogramAssemblyComponentZ]),
                Slope = (Single)GetValue(dr[FieldNames.PlanogramAssemblyComponentSlope]),
                Angle = (Single)GetValue(dr[FieldNames.PlanogramAssemblyComponentAngle]),
                Roll = (Single)GetValue(dr[FieldNames.PlanogramAssemblyComponentRoll]),
                MetaTotalMerchandisableLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace]),
                MetaTotalMerchandisableAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace]),
                MetaTotalMerchandisableVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace]),
                MetaTotalLinearWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalLinearWhiteSpace]),
                MetaTotalAreaWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalAreaWhiteSpace]),
                MetaTotalVolumetricWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace]),
                MetaProductsPlaced = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaProductsPlaced]),
                MetaNewProducts = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaNewProducts]),
                MetaChangesFromPreviousCount = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaChangesFromPreviousCount]),
                MetaChangeFromPreviousStarRating = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaChangeFromPreviousStarRating]),
                MetaBlocksDropped = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaBlocksDropped]),
                MetaNotAchievedInventory = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaNotAchievedInventory]),
                MetaTotalFacings = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalFacings]),
                MetaAverageFacings = (Int16?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaAverageFacings]),
                MetaTotalUnits = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalUnits]),
                MetaAverageUnits = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaAverageUnits]),
                MetaMinDos = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaMinDos]),
                MetaMaxDos = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaMaxDos]),
                MetaAverageDos = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaAverageDos]),
                MetaMinCases = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaMinCases]),
                MetaAverageCases = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaAverageCases]),
                MetaMaxCases = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaMaxCases]),
                MetaSpaceToUnitsIndex = (Int16?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaSpaceToUnitsIndex]),
                MetaTotalFrontFacings = (Int16?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalFrontFacings]),
                MetaAverageFrontFacings = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaAverageFrontFacings]),
                MetaIsComponentSlopeWithNoRiser = (Boolean?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser]),
                MetaIsOverMerchandisedDepth = (Boolean?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedDepth]),
                MetaIsOverMerchandisedHeight = (Boolean?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedHeight]),
                MetaIsOverMerchandisedWidth = (Boolean?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedWidth]),
                MetaTotalPositionCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalPositionCollisions]),
                MetaTotalComponentCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaTotalComponentCollisions]),
                MetaPercentageLinearSpaceFilled = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled]),
                NotchNumber = (Int32?)GetValue(dr[FieldNames.PlanogramAssemblyComponentNotchNumber]),
                MetaWorldX = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaWorldX]),
                MetaWorldY = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaWorldY]),
                MetaWorldZ = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaWorldZ]),
                MetaWorldAngle = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaWorldAngle]),
                MetaWorldSlope = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaWorldSlope]),
                MetaWorldRoll = (Single?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaWorldRoll]),
                MetaIsOutsideOfFixtureArea = (Boolean?)GetValue(dr[FieldNames.PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssemblyComponent DTOs for the given PlanogramAssembly Id.
        /// </summary>
        /// <param name="planogramAssemblyId">The PlanogramAssembly Id for which PlanogramAssemblyComponents should be returned.</param>
        /// <returns>A list of PlanogramAssemblyComponent DTOs.</returns>
        public IEnumerable<PlanogramAssemblyComponentDto> FetchByPlanogramAssemblyId(object planogramAssemblyId)
        {
            var dtoList = new List<PlanogramAssemblyComponentDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyComponentFetchByPlanogramAssemblyId))
                {
                    CreateParameter(
                        command,
                        FieldNames.PlanogramAssemblyComponentPlanogramAssemblyId,
                        SqlDbType.Int,
                        (Int32)planogramAssemblyId);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given PlanogramAssemblyCompenent DTO into the database.
        /// </summary>
        /// <param name="dto">The PlanogramAssemblyCompenent to insert.</param>
        public void Insert(PlanogramAssemblyComponentDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyComponentInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssemblyComponentId, SqlDbType.Int);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified dtos
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            foreach (PlanogramAssemblyComponentDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given PlanogramAssemblyComponent DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssemblyComponentDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyComponentUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssemblyComponentId, SqlDbType.Int, dto.Id);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            foreach (PlanogramAssemblyComponentDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the DTO in the database from the give PlanogramAssemblyComponent Id.
        /// </summary>
        /// <param name="id">The PlanogramAssemblyComponent Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyComponentDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssemblyComponentId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssemblyComponentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        /// <param name="dtos"></param>
        public void Delete(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            foreach (PlanogramAssemblyComponentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Creates parameters for the given command for each of the dto's properties, apart from it's Id.
        /// </summary>
        /// <param name="dto">The DTO whose properties should be used.</param>
        /// <param name="command">The command to add parameters to.</param>
        private void CreateAllParametersButId(PlanogramAssemblyComponentDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentPlanogramAssemblyId, SqlDbType.Int, dto.PlanogramAssemblyId);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentPlanogramComponentId, SqlDbType.Int, dto.PlanogramComponentId);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentComponentSequenceNumber, SqlDbType.SmallInt, dto.ComponentSequenceNumber);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentX, SqlDbType.Real, dto.X);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentY, SqlDbType.Real, dto.Y);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentZ, SqlDbType.Real, dto.Z);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentSlope, SqlDbType.Real, dto.Slope);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentAngle, SqlDbType.Real, dto.Angle);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentRoll, SqlDbType.Real, dto.Roll);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace, SqlDbType.Real, dto.MetaTotalMerchandisableLinearSpace);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace, SqlDbType.Real, dto.MetaTotalMerchandisableAreaSpace);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace, SqlDbType.Real, dto.MetaTotalMerchandisableVolumetricSpace);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalLinearWhiteSpace, SqlDbType.Real, dto.MetaTotalLinearWhiteSpace);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalAreaWhiteSpace, SqlDbType.Real, dto.MetaTotalAreaWhiteSpace);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace, SqlDbType.Real, dto.MetaTotalVolumetricWhiteSpace);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaProductsPlaced, SqlDbType.Int, dto.MetaProductsPlaced);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaNewProducts, SqlDbType.Int, dto.MetaNewProducts);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaChangesFromPreviousCount, SqlDbType.Int, dto.MetaChangesFromPreviousCount);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaChangeFromPreviousStarRating, SqlDbType.Int, dto.MetaChangeFromPreviousStarRating);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaBlocksDropped, SqlDbType.Int, dto.MetaBlocksDropped);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaNotAchievedInventory, SqlDbType.Int, dto.MetaNotAchievedInventory);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalFacings, SqlDbType.Int, dto.MetaTotalFacings);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaAverageFacings, SqlDbType.SmallInt, dto.MetaAverageFacings);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalUnits, SqlDbType.Int, dto.MetaTotalUnits);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaAverageUnits, SqlDbType.Int, dto.MetaAverageUnits);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaMinDos, SqlDbType.Real, dto.MetaMinDos);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaMaxDos, SqlDbType.Real, dto.MetaMaxDos);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaAverageDos, SqlDbType.Real, dto.MetaAverageDos);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaMinCases, SqlDbType.Real, dto.MetaMinCases);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaAverageCases, SqlDbType.Real, dto.MetaAverageCases);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaMaxCases, SqlDbType.Real, dto.MetaMaxCases);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaSpaceToUnitsIndex, SqlDbType.SmallInt, dto.MetaSpaceToUnitsIndex);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalFrontFacings, SqlDbType.SmallInt, dto.MetaTotalFrontFacings);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaAverageFrontFacings, SqlDbType.Real, dto.MetaAverageFrontFacings);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser, SqlDbType.Bit, dto.MetaIsComponentSlopeWithNoRiser);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedDepth, SqlDbType.Bit, dto.MetaIsOverMerchandisedDepth);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedHeight, SqlDbType.Bit, dto.MetaIsOverMerchandisedHeight);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaIsOverMerchandisedWidth, SqlDbType.Bit, dto.MetaIsOverMerchandisedWidth);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalPositionCollisions, SqlDbType.Int, dto.MetaTotalPositionCollisions);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaTotalComponentCollisions, SqlDbType.Int, dto.MetaTotalComponentCollisions);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled, SqlDbType.Real, dto.MetaPercentageLinearSpaceFilled);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentNotchNumber, SqlDbType.Int, dto.NotchNumber);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaWorldX, SqlDbType.Real, dto.MetaWorldX);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaWorldY, SqlDbType.Real, dto.MetaWorldY);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaWorldZ, SqlDbType.Real, dto.MetaWorldZ);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaWorldAngle, SqlDbType.Real, dto.MetaWorldAngle);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaWorldSlope, SqlDbType.Real, dto.MetaWorldSlope);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaWorldRoll, SqlDbType.Real, dto.MetaWorldRoll);
            CreateParameter(command, FieldNames.PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea, SqlDbType.Bit, dto.MetaIsOutsideOfFixtureArea);
        } 
        #endregion
    }
}
