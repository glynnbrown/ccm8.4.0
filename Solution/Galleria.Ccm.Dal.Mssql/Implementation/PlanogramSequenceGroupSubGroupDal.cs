﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 830
// V8-32504 : A.Kuszyk
//	Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using System.Data;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramSequenceGroupSubGroupDal : DalBase, IPlanogramSequenceGroupSubGroupDal
    {
        #region Data TransferObject

        private static PlanogramSequenceGroupSubGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramSequenceGroupSubGroupDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramSequenceGroupSubGroupId]),
                PlanogramSequenceGroupId = (Int32)GetValue(dr[FieldNames.PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramSequenceGroupSubGroupName]),
                Alignment = (Byte)GetValue(dr[FieldNames.PlanogramSequenceGroupSubGroupAlignment])
            };
        }

        #endregion

        #region Fetch

        public IEnumerable<PlanogramSequenceGroupSubGroupDto> FetchByPlanogramSequenceGroupId(Object sequenceGroupId)
        {
            IList<PlanogramSequenceGroupSubGroupDto> dtos = new List<PlanogramSequenceGroupSubGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupSubGroupFetchByPlanogramSequenceGroupId))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId, SqlDbType.Int, (Int32)sequenceGroupId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtos;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupSubGroupDto"/> to insert.</param>
        public void Insert(PlanogramSequenceGroupSubGroupDto dto)
        {
            try
            {
                SqlParameter idParameter;
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupSubGroupInsert))
                {
                    idParameter = CreateParameter(command, FieldNames.PlanogramSequenceGroupSubGroupId, SqlDbType.Int);
                    CreateDtoParameters(command, dto);

                    command.ExecuteNonQuery();
                }

                dto.Id = (Int32)idParameter.Value;
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Inserts the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupSubGroupDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupSubGroupDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupSubGroupDto"/> to update.</param>
        public void Update(PlanogramSequenceGroupSubGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupSubGroupUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupSubGroupId, SqlDbType.Int, dto.Id);
                    CreateDtoParameters(command, dto);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Updates the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceGroupSubGroupDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupSubGroupDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the record with a matching <paramref name="id"/> from the database.
        /// </summary>
        /// <param name="id">The Id of the record to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupSubGroupDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupSubGroupId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Deletes the given <paramref name="dto"/> from the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupSubGroupDto"/> to delete.</param>
        public void Delete(PlanogramSequenceGroupSubGroupDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes the given <paramref name="dtos"/> from the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceGroupSubGroupDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupSubGroupDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion

        private void CreateDtoParameters(DalCommand command, PlanogramSequenceGroupSubGroupDto dto)
        {
            CreateParameter(command, FieldNames.PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId, SqlDbType.Int,
                dto.PlanogramSequenceGroupId);
            CreateParameter(command, FieldNames.PlanogramSequenceGroupSubGroupName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramSequenceGroupSubGroupAlignment, SqlDbType.TinyInt, dto.Alignment);
        }
    }
}
