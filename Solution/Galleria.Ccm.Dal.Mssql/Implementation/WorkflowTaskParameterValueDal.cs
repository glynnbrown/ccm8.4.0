﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25886 : L.Ineson
//  Created
// V8-27426 : L.Ineson
//  Added 2 more values
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkflowTaskParameterValueDal
    /// </summary>
    public sealed class WorkflowTaskParameterValueDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkflowTaskParameterValueDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        public WorkflowTaskParameterValueDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkflowTaskParameterValueDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkflowTaskParameterValueId]),
                WorkflowTaskParameterId = (Int32)GetValue(dr[FieldNames.WorkflowTaskParameterValueWorkflowTaskParameterId]),
                Value1 = GetValue(dr[FieldNames.WorkflowTaskParameterValueValue1]),
                Value2 = GetValue(dr[FieldNames.WorkflowTaskParameterValueValue2]),
                Value3 = GetValue(dr[FieldNames.WorkflowTaskParameterValueValue3]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Fetches dtos for the given workflow task parameter id
        /// </summary>
        public IEnumerable<WorkflowTaskParameterValueDto> FetchByWorkflowTaskParameterId(Int32 workflowTaskParameterId)
        {
            List<WorkflowTaskParameterValueDto> dtoList = new List<WorkflowTaskParameterValueDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterValueFetchByWorkflowTaskParameterId))
                {
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueWorkflowTaskParameterId, SqlDbType.Int, workflowTaskParameterId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(WorkflowTaskParameterValueDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterValueInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkflowTaskParameterValueId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueWorkflowTaskParameterId, SqlDbType.Int, dto.WorkflowTaskParameterId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueValue1, SqlDbType.Variant, dto.Value1);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueValue2, SqlDbType.Variant, dto.Value2);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueValue3, SqlDbType.Variant, dto.Value3);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given dto
        /// </summary>
        public void Update(WorkflowTaskParameterValueDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterValueUpdateById))
                {
                    // parameter
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueWorkflowTaskParameterId, SqlDbType.Int, dto.WorkflowTaskParameterId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueValue1, SqlDbType.Variant, dto.Value1);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueValue2, SqlDbType.Variant, dto.Value2);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueValue3, SqlDbType.Variant, dto.Value3);


                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the dto with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskParameterValueDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkflowTaskParameterValueId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
 