﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
// V8-27241 : A.Kuszyk
//  Changed FetchByName to FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Blocking Dal
    /// </summary>
    public class BlockingDal : Galleria.Framework.Dal.Mssql.DalBase, IBlockingDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static BlockingDto GetDataTransferObject(SqlDataReader dr)
        {
            return new BlockingDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.BlockingId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.BlockingRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.BlockingEntityId]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.BlockingProductGroupId]),
                Name = (String)GetValue(dr[FieldNames.BlockingName]),
                Description = (String)GetValue(dr[FieldNames.BlockingDescription]),
                Height = (Single)GetValue(dr[FieldNames.BlockingHeight]),
                Width = (Single)GetValue(dr[FieldNames.BlockingWidth]),
                PlanogramId = (Int32?)GetValue(dr[FieldNames.BlockingPlanogramId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.BlockingDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.BlockingDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.BlockingDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public BlockingDto FetchById(Int32 id)
        {
            BlockingDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified name
        /// </summary>
        /// <param name="name">specified name</param>
        /// <returns>matching dto</returns>
        public BlockingDto FetchByEntityIdName(Int32 entityId, String name)
        {
            BlockingDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingFetchByEntityIdName))
                {
                    //Name & entity id
                    CreateParameter(command, FieldNames.BlockingName, SqlDbType.NVarChar, name);
                    CreateParameter(command, FieldNames.BlockingEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(BlockingDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.BlockingId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.BlockingRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.BlockingDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.BlockingDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.BlockingDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.BlockingProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.BlockingName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.BlockingDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.BlockingHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.BlockingWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.BlockingPlanogramId, SqlDbType.Int, dto.PlanogramId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(BlockingDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.BlockingRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.BlockingDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.BlockingDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.BlockingDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.BlockingProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.BlockingName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.BlockingDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.BlockingHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.BlockingWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.BlockingPlanogramId, SqlDbType.Int, dto.PlanogramId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}