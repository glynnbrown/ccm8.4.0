﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26124 : I.George
//		Created (Auto-generated)
#endregion
#region Version History:(CCM820)
//CCM-30836 : J.Pickup
// Introduced IPtype.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using System.Data;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class InventoryProfileInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IInventoryProfileInfoDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a data transfer object
        /// </summary>
        /// <param name="dr"> A read data</param>
        /// <returns> An Inventory Info dto</returns>
        public static InventoryProfileInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new InventoryProfileInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.InventoryProfileId]),
                Name = (String)GetValue(dr[FieldNames.InventoryProfileName]),
                InventoryProfileType = (Byte)GetValue(dr[FieldNames.InventoryProfileType])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// returns the dto that match the specified entity id
        /// </summary>
        /// <param name="entityId"> specified entity id</param>
        /// <returns> matching dtos</returns>
        public IEnumerable<InventoryProfileInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<InventoryProfileInfoDto> dtoList = new List<InventoryProfileInfoDto>();
            try
                {
                using(DalCommand  command = CreateCommand(ProcedureNames.InventoryProfileInfoFetchByEntityId))
                {
                    //EnitityId
                    CreateParameter(command, FieldNames.InventoryProfileEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while(dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
                catch(SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion
    }
}
