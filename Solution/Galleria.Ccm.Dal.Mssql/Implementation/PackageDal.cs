﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25476 : A. Kuszyk
//  Initial version.
// V8-25949 : N.Foster
//  Changed LockById to return success or failure
// V8-25787 : N.Foster
//  Resolved issues with the creating of new packages and planograms
// V8-25881 : A.Probyn
//  Replaced PlanogramCount with MetaPlanogramCount
//  Added DateMetadataCalculated
// V8-27237 : A.Silva   ~ Added DateValidationDataCalculated.
// V8-27411 : M.Pettit
//  Package locking now requires userId and locktype
// V8-27919 : L.Ineson
//  Added fetchArgument parameter to FetchById
#endregion
#region Version History: CCM802
// V8-28840 : L.Luong
//  Added EntityId.
#endregion
#region Version History: CCM810
// V8-28242 : M.Shelley
//  Added UpdatePlanogramAttributes
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30059 : L.Ineson
//  Added setProperties to update planogram attributes.
#endregion
#region Version History: CCM811
// V8-30561 : N.Foster
//  When updating a package, the date last modified date is no longer updated
#endregion
#region Version History: CCM830
// CCM-13446 : R.Cooper
//  Date last modified is now passed as a parameter value for the insert method, but only when a valid date value is present. As the date last
//  modified value cannot be NULL, a comparison is made to DateTime.MinValue to determine whether a valid date value is present.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Linq;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PackageDal : Galleria.Framework.Dal.Mssql.DalBase, IPackageDal
    {
        private const String _importTableName = "#tmpPlanogram";
        private const String _createUcrTableSql = "CREATE TABLE [{0}] (Planogram_UniqueContentReference UNIQUEIDENTIFIER)";
        private const String _dropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        #region Data Transfer Object
        /// <summary>
        /// Returns a data transfer object from a Dto
        /// </summary>
        private static PackageDto GetDataTransferObject(IDataReader dr)
        {
            return new PackageDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PlanogramRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.PlanogramEntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramName]),
                UserName = (String)GetValue(dr[FieldNames.PlanogramUserName]),
                MetaPlanogramCount = 1, // hardcoded as a package and planogram are the same thing within the ccm database
                DateCreated = (DateTime)GetValue(dr[FieldNames.PlanogramDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.PlanogramDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.PlanogramDateDeleted]),
                DateMetadataCalculated = (DateTime?)GetValue(dr[FieldNames.PlanogramDateMetadataCalculated]),
                DateValidationDataCalculated = (DateTime?)GetValue(dr[FieldNames.PlanogramDateValidationDataCalculated])
            };
        }
        #endregion

        #region Locking/Unlocking
        /// <summary>
        /// Locks a package for editing
        /// </summary>
        /// <returns>Indicates the result of the lock (0 = FAILED, 1 = SUCCESS, 2 = ALREADY LOCKED)</returns>
        public Byte LockById(Object id, Object lockUserId, Byte lockType, Boolean lockReadOnly)
        {
            Byte result = 1; // SUCCESS
            try
            {
                // nothing to do if the lock is a read-only lock
                // as this has no effect on the database
                if (!lockReadOnly)
                {
                    using (DalCommand command = this.CreateCommand(ProcedureNames.PlanogramLockLockByPlanogramId))
                    {
                        // parameters
                        this.CreateParameter(command, FieldNames.PlanogramLockPlanogramId, SqlDbType.Int, id);
                        this.CreateParameter(command, FieldNames.PlanogramLockType, SqlDbType.TinyInt, lockType);
                        this.CreateParameter(command, FieldNames.PlanogramLockComputerName, SqlDbType.NVarChar, Environment.MachineName);
                        this.CreateParameter(command, FieldNames.PlanogramLockDateLocked, SqlDbType.SmallDateTime, DateTime.UtcNow);
                        this.CreateParameter(command, FieldNames.PlanogramLockUserId, SqlDbType.Int, lockUserId);

                        // execute
                        result = (Byte)command.ExecuteScalar();
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return result;
        }

        /// <summary>
        /// Unlocks a package after editing
        /// </summary>
        /// <returns>Indicates the result of the lock (0 = FAILED, 1 = SUCCESS)</returns>
        public Byte UnlockById(Object id, Object lockUserId, Byte lockType, Boolean lockReadOnly)
        {
            Byte result = 1; // SUCCESS
            try
            {
                // nothing to do if the lock is a read-only lock
                // as this has no effect on the database
                if (!lockReadOnly)
                {
                    using (DalCommand command = this.CreateCommand(ProcedureNames.PlanogramLockUnlockByPlanogramId))
                    {
                        // parameters
                        this.CreateParameter(command, FieldNames.PlanogramLockPlanogramId, SqlDbType.Int, id);
                        this.CreateParameter(command, FieldNames.PlanogramLockType, SqlDbType.TinyInt, lockType);
                        this.CreateParameter(command, FieldNames.PlanogramLockUserId, SqlDbType.Int, lockUserId);

                        // execute
                        result = (Byte)command.ExecuteScalar();
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return result;
        }
        #endregion

        #region UpdatePlanogramAttributes

        /// <summary>
        /// Updates attributes for a number of planograms.
        /// </summary>
        /// <returns> Indicates the result of the lock (0 = FAILED, 1 = SUCCESS)</returns>
        public Byte UpdatePlanogramAttributes(IEnumerable<PlanogramAttributesDto> planAttributesDtoList)
        {
            Byte result = 1; // SUCCESS

            try
            {
                foreach (PlanogramAttributesDto planAttrDto in planAttributesDtoList)
                {
                    String setProperties = GenerateSetPropertiesString(planAttrDto);

                    using (DalCommand command = this.CreateCommand(ProcedureNames.PlanogramUpdateAttributesById))
                    {
                        // In the repository the Planogram Id is an Int32
                        Int32 planId = (Int32) planAttrDto.PlanogramId;

                        // parameters
                        this.CreateParameter(command, FieldNames.PlanogramSetProperties, SqlDbType.NVarChar, setProperties);
                        this.CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, planId);
                        this.CreateParameter(command, FieldNames.PlanogramCategoryCode, SqlDbType.NVarChar, planAttrDto.CategoryCode);
                        this.CreateParameter(command, FieldNames.PlanogramCategoryName, SqlDbType.NVarChar, planAttrDto.CategoryName);
                        this.CreateParameter(command, FieldNames.PlanogramLocationCode, SqlDbType.NVarChar, planAttrDto.LocationCode);
                        this.CreateParameter(command, FieldNames.PlanogramLocationName, SqlDbType.NVarChar, planAttrDto.LocationName);
                        this.CreateParameter(command, FieldNames.PlanogramClusterSchemeName, SqlDbType.NVarChar, planAttrDto.ClusterSchemeName);
                        this.CreateParameter(command, FieldNames.PlanogramClusterName, SqlDbType.NVarChar, planAttrDto.ClusterName);
                        this.CreateParameter(command, FieldNames.PlanogramDateApproved, SqlDbType.SmallDateTime, planAttrDto.DateApproved);
                        this.CreateParameter(command, FieldNames.PlanogramDateArchived, SqlDbType.SmallDateTime, planAttrDto.DateArchived);
                        this.CreateParameter(command, FieldNames.PlanogramDateWip, SqlDbType.SmallDateTime, planAttrDto.DateWip);
                        this.CreateParameter(command, FieldNames.PlanogramStatus, SqlDbType.TinyInt, planAttrDto.Status);
                        this.CreateParameter(command, FieldNames.PlanogramPlanogramType, SqlDbType.TinyInt, planAttrDto.PlanogramType);

                        // execute
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return result;
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private String GenerateSetPropertiesString(PlanogramAttributesDto isSetDto)
        {
            String isSet = String.Empty;
            String format = "{0}#{1}#";

            if (isSetDto.CategoryCodeIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramCategoryCode);
            if (isSetDto.CategoryNameIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramCategoryName);
            if (isSetDto.LocationCodeIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramLocationCode);
            if (isSetDto.LocationNameIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramLocationName);
            if (isSetDto.ClusterSchemeNameIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramClusterSchemeName);
            if (isSetDto.ClusterNameIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramClusterName);
            if (isSetDto.DateApprovedIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramDateApproved);
            if (isSetDto.DateArchivedIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramDateArchived);
            if (isSetDto.DateWipIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramDateWip);
            if (isSetDto.StatusIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramStatus);
            if (isSetDto.PlanogramTypeIsSet) isSet = String.Format(format, isSet, FieldNames.PlanogramPlanogramType);

            return isSet;
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a PackageDto that represents the Planogram with Id id.
        /// </summary>
        /// <param name="id">The Id of the Planogram to represent.</param>
        /// <returns>A PackageDto representing a Planogram with the given id.</returns>
        public PackageDto FetchById(Object id)
        {
            return FetchById(id, null);
        }

        /// <summary>
        /// Returns a PackageDto that represents the Planogram with Id id.
        /// </summary>
        /// <param name="id">The Id of the Planogram to represent.</param>
        /// <param name="fetchArgument">optional fetch argument (currently not used by this dal)</param>
        /// <returns>A PackageDto representing a Planogram with the given id.</returns>
        public PackageDto FetchById(Object id, Object fetchArgument)
        {
            PackageDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PackageFetchById))
                {
                    CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, (Int32)id);
                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IList<Object> FetchIdsExceptFor(IEnumerable<String> ucrs)
        {
            var results = new List<Object>();

            try
            {
                CreateTempTable(ucrs);

                using (var command = CreateCommand(ProcedureNames.FetchIdsExceptUcrs))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            results.Add(dr[FieldNames.PlanogramId]);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            finally
            {
                DropTempTable();
            }

            return results;
        }

        public IList<Object> FetchByUcrsDateLastModified(IEnumerable<String> ucrs, DateTime dateLastChanged)
        {
            var results = new List<Object>();

            try
            {
                CreateTempTable(ucrs);

                using (var command = CreateCommand(ProcedureNames.FetchByUcrsDateLastModified))
                {
                    CreateParameter(command, "DateLastModified", SqlDbType.SmallDateTime, dateLastChanged);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            results.Add(dr[FieldNames.PlanogramId]);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            finally
            {
                DropTempTable();
            }

            return results;
        }

        public IList<Object> FetchDeletedByUcrs(IEnumerable<String> ucrs)
        {
            var results = new List<Object>();

            try
            {
                CreateTempTable(ucrs);

                using (var command = CreateCommand(ProcedureNames.FetchDeletedByUcrs))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            results.Add(dr[FieldNames.PlanogramUniqueContentReference]);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            finally
            {
                DropTempTable();
            }

            return results;
        }

        
        private void DropTempTable()
        {
            using (var command = CreateCommand(String.Format(_dropTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        private void CreateTempTable(IEnumerable<String> ucrExceptions)
        {
            using (var command = CreateCommand(String.Format(_createUcrTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = CommandTimeout;
                bulkCopy.DestinationTableName = _importTableName;
                bulkCopy.WriteToServer(new BulkCopyDataReader<Guid>(ucrExceptions.Select(Guid.Parse).ToList()));
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the database
        /// </summary>
        public void Insert(PackageDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PackageInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PlanogramRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramDateCreated, SqlDbType.SmallDateTime);
                    SqlParameter dateLastModifiedParameter;

                    // evaluate DateLastModified value to determine whether a valid date value is present.
                    if(Equals(DateTime.MinValue, dto.DateLastModified))
                    {
                        dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramDateLastModified, SqlDbType.SmallDateTime);
                    }
                    else
                    {
                        dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramDateLastModified, SqlDbType.SmallDateTime, dto.DateLastModified);
                    }

                    CreateParameter(command, FieldNames.PlanogramName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramDateMetadataCalculated, SqlDbType.SmallDateTime, dto.DateMetadataCalculated);
                    CreateParameter(command, FieldNames.PlanogramDateValidationDataCalculated, SqlDbType.SmallDateTime, dto.DateValidationDataCalculated);
                    CreateParameter(command, FieldNames.PlanogramUserName, SqlDbType.NVarChar, dto.UserName);
                    CreateParameter(command, FieldNames.PlanogramEntityId, SqlDbType.Int, dto.EntityId);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)dateCreatedParameter.Value;
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates a dto within the database
        /// </summary>
        public void Update(PackageDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PackageUpdateById))
                {
                    // parameters
                    var rowVersionParameter = CreateParameter(command, FieldNames.PlanogramRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramDateCreated, SqlDbType.SmallDateTime, dto.DateCreated);
                    CreateParameter(command, FieldNames.PlanogramDateLastModified, SqlDbType.SmallDateTime, dto.DateLastModified);
                    CreateParameter(command, FieldNames.PlanogramDateDeleted, SqlDbType.SmallDateTime, dto.DateDeleted);
                    CreateParameter(command, FieldNames.PlanogramDateMetadataCalculated, SqlDbType.SmallDateTime, dto.DateMetadataCalculated);
                    CreateParameter(command, FieldNames.PlanogramDateValidationDataCalculated, SqlDbType.SmallDateTime, dto.DateValidationDataCalculated);
                    CreateParameter(command, FieldNames.PlanogramEntityId, SqlDbType.Int, dto.EntityId);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified package from the database
        /// </summary>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PackageDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
