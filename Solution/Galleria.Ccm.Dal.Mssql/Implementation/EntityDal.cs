﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// CCM-25559 : L.Hodson
//  Copied from SA
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
// V8-25556 : D.Pleasance
//  Added FetchAll
//CCM-26179: I.George
// added more fields
// CCM-26306 : L.Ineson
//  Moved uom values into system settings
// V8-27964 : A.Silva
//      Added ProductLevelId and EntityDateLastMerchSynced.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Entity Dal
    /// </summary>
    public class EntityDal : Galleria.Framework.Dal.Mssql.DalBase, IEntityDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public EntityDto GetDataTransferObject(SqlDataReader dr)
        {
            return new EntityDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.EntityId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.EntityRowVersion])),
                Name = (String)GetValue(dr[FieldNames.EntityName]),
                Description = (String)GetValue(dr[FieldNames.EntityDescription]),
                GFSId = (Int32?)GetValue(dr[FieldNames.EntityGFSEntityId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.EntityDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.EntityDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.EntityDateDeleted]),
                ProductLevelId = (Int32?)GetValue(dr[FieldNames.ProductLevelId]),
                DateLastMerchSynced = (DateTime?)GetValue(dr[FieldNames.EntityDateLastMerchSynced])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public EntityDto FetchById(Int32 id)
        {
            EntityDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="name">The name of the entity record to fetch</param>
        /// <returns>The specified dto</returns>
        public EntityDto FetchDeletedByName(String name)
        {
            EntityDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityFetchDeletedByName))
                {
                    CreateParameter(command, FieldNames.EntityName, SqlDbType.NVarChar, name);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns all entites in the database
        /// </summary>
        /// <returns>A list of entity dtos</returns>
        public IEnumerable<EntityDto> FetchAll()
        {
            List<EntityDto> dtoList = new List<EntityDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityFetchAll))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(EntityDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.EntityId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.EntityRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.EntityDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.EntityDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.EntityDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.EntityName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.EntityDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.EntityGFSEntityId, SqlDbType.Int, dto.GFSId);
                    CreateParameter(command, FieldNames.ProductLevelId, SqlDbType.Int, dto.ProductLevelId);
                    CreateParameter(command, FieldNames.EntityDateLastMerchSynced, SqlDbType.DateTime, dto.DateLastMerchSynced);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(EntityDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.EntityRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.EntityDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.EntityDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.EntityDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.EntityName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.EntityDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.EntityGFSEntityId, SqlDbType.Int, dto.GFSId);
                    CreateParameter(command, FieldNames.ProductLevelId, SqlDbType.Int, dto.ProductLevelId);
                    CreateParameter(command, FieldNames.EntityDateLastMerchSynced, SqlDbType.DateTime, dto.DateLastMerchSynced);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}