﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// V8-27264 : A.Kuszyk
//  Amended GetDataTransferObject to set IsPrimaryRegionalProduct.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// AssortmentProduct Dal
    /// </summary>
    public class AssortmentProductDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentProductDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentProductDto GetDataTransferObject(SqlDataReader dr)
        {
                return new AssortmentProductDto()
                {
                    Id = (Int32)GetValue(dr[FieldNames.AssortmentProductId]),
                    GTIN = (String)GetValue(dr[FieldNames.AssortmentProductGTIN]), 
                    ProductId = (Int32)GetValue(dr[FieldNames.AssortmentProductProductId]),
                    AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentId]),
                    Name = (String)GetValue(dr[FieldNames.AssortmentProductName]),
                    IsRanged = (Boolean)GetValue(dr[FieldNames.AssortmentProductIsRanged]),
                    Rank = (Int16)GetValue(dr[FieldNames.AssortmentProductRank]),
                    Segmentation = (String)GetValue(dr[FieldNames.AssortmentProductSegmentation]),
                    Facings = (Byte)GetValue(dr[FieldNames.AssortmentProductFacings]),
                    Units = (Int16)GetValue(dr[FieldNames.AssortmentProductUnits]),
                    ProductTreatmentType = (Byte)GetValue(dr[FieldNames.AssortmentProductProductTreatmentType]),
                    ProductLocalizationType = (Byte)GetValue(dr[FieldNames.AssortmentProductProductLocalizationType]),
                    Comments = (String)GetValue(dr[FieldNames.AssortmentProductComments]),
                    ExactListFacings = (Byte?)GetValue(dr[FieldNames.AssortmentProductExactListFacings]),
                    ExactListUnits = (Int16?)GetValue(dr[FieldNames.AssortmentProductExactListUnits]),
                    PreserveListFacings = (Byte?)GetValue(dr[FieldNames.AssortmentProductPreserveListFacings]),
                    PreserveListUnits = (Int16?)GetValue(dr[FieldNames.AssortmentProductPreserveListUnits]),
                    MaxListFacings = (Byte?)GetValue(dr[FieldNames.AssortmentProductMaxListFacings]),
                    MaxListUnits = (Int16?)GetValue(dr[FieldNames.AssortmentProductMaxListUnits]),
                    MinListFacings = (Byte?)GetValue(dr[FieldNames.AssortmentProductMinListFacings]),
                    MinListUnits = (Int16?)GetValue(dr[FieldNames.AssortmentProductMinListUnits]),
                    FamilyRuleName = (String)GetValue(dr[FieldNames.AssortmentProductFamilyRuleName]),
                    FamilyRuleType = (Byte)GetValue(dr[FieldNames.AssortmentProductFamilyRuleType]),
                    FamilyRuleValue = (Byte?)GetValue(dr[FieldNames.AssortmentProductFamilyRuleValue]),
                    IsPrimaryRegionalProduct = (Boolean)GetValue(dr[FieldNames.AssortmentProductIsPrimaryRegionalProduct])
                };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentProductDto FetchById(Int32 id)
        {
            AssortmentProductDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentProductId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the sorrtment id
        /// </summary>
        /// <param name="id">assortment id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<AssortmentProductDto> FetchByAssortmentId(Int32 id)
        {
            List<AssortmentProductDto> dtoList = new List<AssortmentProductDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductFetchByAssortmentId))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentProductAssortmentId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentProductId, SqlDbType.Int, ParameterDirection.InputOutput, dto.Id);

                    //Other properties 
                     CreateParameter(command, FieldNames.AssortmentProductProductId, SqlDbType.Int, dto.ProductId);
                     CreateParameter(command, FieldNames.AssortmentProductAssortmentId, SqlDbType.Int, dto.AssortmentId); 
                     CreateParameter(command, FieldNames.AssortmentProductIsRanged, SqlDbType.Bit, dto.IsRanged); 
                     CreateParameter(command, FieldNames.AssortmentProductRank, SqlDbType.SmallInt, dto.Rank); 
                     CreateParameter(command, FieldNames.AssortmentProductSegmentation, SqlDbType.NVarChar, dto.Segmentation); 
                     CreateParameter(command, FieldNames.AssortmentProductFacings, SqlDbType.TinyInt, dto.Facings); 
                     CreateParameter(command, FieldNames.AssortmentProductUnits, SqlDbType.SmallInt, dto.Units); 
                     CreateParameter(command, FieldNames.AssortmentProductProductTreatmentType, SqlDbType.TinyInt, dto.ProductTreatmentType); 
                     CreateParameter(command, FieldNames.AssortmentProductProductLocalizationType, SqlDbType.TinyInt, dto.ProductLocalizationType); 
                     CreateParameter(command, FieldNames.AssortmentProductComments, SqlDbType.NVarChar, dto.Comments); 
                     CreateParameter(command, FieldNames.AssortmentProductExactListFacings, SqlDbType.TinyInt, dto.ExactListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductExactListUnits, SqlDbType.SmallInt, dto.ExactListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductPreserveListFacings, SqlDbType.TinyInt, dto.PreserveListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductPreserveListUnits, SqlDbType.SmallInt, dto.PreserveListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductMaxListFacings, SqlDbType.TinyInt, dto.MaxListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductMaxListUnits, SqlDbType.SmallInt, dto.MaxListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductMinListFacings, SqlDbType.TinyInt, dto.MinListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductMinListUnits, SqlDbType.SmallInt, dto.MinListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductFamilyRuleName, SqlDbType.NVarChar, dto.FamilyRuleName); 
                     CreateParameter(command, FieldNames.AssortmentProductFamilyRuleType, SqlDbType.TinyInt, dto.FamilyRuleType); 
                     CreateParameter(command, FieldNames.AssortmentProductFamilyRuleValue, SqlDbType.TinyInt, dto.FamilyRuleValue); 

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductUpdateById))
                {
                                        //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentProductId, SqlDbType.Int, ParameterDirection.Input, dto.Id);

                    //Other properties 
                     CreateParameter(command, FieldNames.AssortmentProductProductId, SqlDbType.Int, dto.ProductId);
                     CreateParameter(command, FieldNames.AssortmentProductAssortmentId, SqlDbType.Int, dto.AssortmentId); 
                     CreateParameter(command, FieldNames.AssortmentProductIsRanged, SqlDbType.Bit, dto.IsRanged); 
                     CreateParameter(command, FieldNames.AssortmentProductRank, SqlDbType.SmallInt, dto.Rank); 
                     CreateParameter(command, FieldNames.AssortmentProductSegmentation, SqlDbType.NVarChar, dto.Segmentation); 
                     CreateParameter(command, FieldNames.AssortmentProductFacings, SqlDbType.TinyInt, dto.Facings); 
                     CreateParameter(command, FieldNames.AssortmentProductUnits, SqlDbType.SmallInt, dto.Units); 
                     CreateParameter(command, FieldNames.AssortmentProductProductTreatmentType, SqlDbType.TinyInt, dto.ProductTreatmentType); 
                     CreateParameter(command, FieldNames.AssortmentProductProductLocalizationType, SqlDbType.TinyInt, dto.ProductLocalizationType); 
                     CreateParameter(command, FieldNames.AssortmentProductComments, SqlDbType.NVarChar, dto.Comments); 
                     CreateParameter(command, FieldNames.AssortmentProductExactListFacings, SqlDbType.TinyInt, dto.ExactListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductExactListUnits, SqlDbType.SmallInt, dto.ExactListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductPreserveListFacings, SqlDbType.TinyInt, dto.PreserveListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductPreserveListUnits, SqlDbType.SmallInt, dto.PreserveListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductMaxListFacings, SqlDbType.TinyInt, dto.MaxListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductMaxListUnits, SqlDbType.SmallInt, dto.MaxListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductMinListFacings, SqlDbType.TinyInt, dto.MinListFacings); 
                     CreateParameter(command, FieldNames.AssortmentProductMinListUnits, SqlDbType.SmallInt, dto.MinListUnits); 
                     CreateParameter(command, FieldNames.AssortmentProductFamilyRuleName, SqlDbType.NVarChar, dto.FamilyRuleName); 
                     CreateParameter(command, FieldNames.AssortmentProductFamilyRuleType, SqlDbType.TinyInt, dto.FamilyRuleType); 
                     CreateParameter(command, FieldNames.AssortmentProductFamilyRuleValue, SqlDbType.TinyInt, dto.FamilyRuleValue); 

                    //execute
                    command.ExecuteNonQuery();

                    //update dto: None
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentProductDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentProductId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}