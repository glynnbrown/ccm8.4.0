﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//		Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class RolePermissionDal : Galleria.Framework.Dal.Mssql.DalBase, IRolePermissionDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        private RolePermissionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new RolePermissionDto
            {
                Id = (Int32)GetValue(dr[FieldNames.RolePermissionId]),
                RoleId = (Int32)GetValue(dr[FieldNames.RolePermissionRoleId]),
                PermissionType = (Int16)GetValue(dr[FieldNames.RolePermissionPermissionType])
            };
        }

        #endregion


        #region Fetch

        public RolePermissionDto FetchById(Int32 id)
        {
            RolePermissionDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RolePermissionFetchById))
                {
                    CreateParameter(command, FieldNames.RolePermissionId, SqlDbType.Int, id);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<RolePermissionDto> FetchAll()
        {
            List<RolePermissionDto> dtoList = new List<RolePermissionDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RolePermissionFetchAll))
                {
                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public IEnumerable<RolePermissionDto> FetchByRoleId(Int32 roleId)
        {
            List<RolePermissionDto> dtoList = new List<RolePermissionDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RolePermissionFetchByRoleId))
                {
                    CreateParameter(command, FieldNames.RolePermissionRoleId, SqlDbType.Int, roleId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(RolePermissionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RolePermissionInsert))
                {
                    //Parameters to store the returned Id and rowversion
                    SqlParameter idParameter = CreateParameter(command, FieldNames.RolePermissionId, SqlDbType.Int);

                    CreateParameter(command, FieldNames.RolePermissionRoleId, SqlDbType.Int, dto.RoleId);
                    CreateParameter(command, FieldNames.RolePermissionPermissionType, SqlDbType.SmallInt, dto.PermissionType);

                    //Execute the insert
                    command.ExecuteNonQuery();

                    //Update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(RolePermissionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RolePermissionUpdate))
                {
                    CreateParameter(command, FieldNames.RolePermissionId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.RolePermissionRoleId, SqlDbType.Int, dto.RoleId);
                    CreateParameter(command, FieldNames.RolePermissionPermissionType, SqlDbType.SmallInt, dto.PermissionType);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RolePermissionDelete))
                {
                    CreateParameter(command, FieldNames.RolePermissionId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}
