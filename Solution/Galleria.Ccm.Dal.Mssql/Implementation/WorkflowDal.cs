﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : L.Ineson
//  Created
// CCM-25827 : N.Haywood
//  Removed dates from insert and update methods
// V8-25868 : L.Ineson
//  Put date parameters back into insert and update methods
//  Added description property
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkflowDal
    /// </summary>
    public sealed class WorkflowDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkflowDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public WorkflowDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkflowDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkflowId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.WorkflowRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.WorkflowEntityId]),
                Name = (String)GetValue(dr[FieldNames.WorkflowName]),
                Description = (String)GetValue(dr[FieldNames.WorkflowDescription]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.WorkflowDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.WorkflowDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.WorkflowDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns the dto with the given id
        /// </summary>
        public WorkflowDto FetchById(Int32 id)
        {
            WorkflowDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkflowId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(WorkflowDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowInsert))
                {
                    //Special parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkflowId, SqlDbType.Int);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.WorkflowRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.WorkflowDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.WorkflowDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.WorkflowDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.WorkflowEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.WorkflowName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.WorkflowDescription, SqlDbType.NVarChar, dto.Description);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void Update(WorkflowDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkflowId, SqlDbType.Int, dto.Id);

                    //Special parameters
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.WorkflowRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.WorkflowDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.WorkflowDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.WorkflowDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.WorkflowEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.WorkflowName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.WorkflowDescription, SqlDbType.NVarChar, dto.Description);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Deletes the dto with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkflowId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
