﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25919 : L.Ineson
//  Created
#endregion
#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkflowTaskInfoDal
    /// </summary>
    public sealed class WorkflowTaskInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkflowTaskInfoDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public WorkflowTaskInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkflowTaskInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkflowTaskInfoId]),
                SequenceId = (Byte)GetValue(dr[FieldNames.WorkflowTaskInfoSequenceId]),
                TaskType = (String)GetValue(dr[FieldNames.WorkflowTaskInfoTaskType]),
                DisplayName = (String)GetValue(dr[FieldNames.WorkflowTaskInfoDisplayName]),
                DisplayDescription = (String)GetValue(dr[FieldNames.WorkflowTaskInfoDisplayDescription]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given workflow id
        /// </summary>
        public IEnumerable<WorkflowTaskInfoDto> FetchByWorkflowId(Int32 workflowId)
        {
            List<WorkflowTaskInfoDto> dtoList = new List<WorkflowTaskInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskInfoFetchByWorkflowId))
                {
                    CreateParameter(command, FieldNames.WorkflowTaskInfoWorkflowId, SqlDbType.Int, workflowId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        /// <summary>
        /// Returns all dtos for the given workpackage id
        /// </summary>
        public IEnumerable<WorkflowTaskInfoDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            List<WorkflowTaskInfoDto> dtoList = new List<WorkflowTaskInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskInfoFetchByWorkpackageId))
                {
                    CreateParameter(command, FieldNames.WorkflowTaskInfoWorkpackageId, SqlDbType.Int, workpackageId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion
    }
}
