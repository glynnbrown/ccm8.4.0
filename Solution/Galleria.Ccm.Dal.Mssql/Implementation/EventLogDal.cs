﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName, WorkpackageId property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// EventLog Dal
    /// </summary>
    public class EventLogDal : Galleria.Framework.Dal.Mssql.DalBase, IEventLogDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public EventLogDto GetDataTransferObject(SqlDataReader dr)
        {
            return new EventLogDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.EventLogId]),
                EntityId = (Int32?)GetValue(dr[FieldNames.EventLogEntityId]),
                EntityName = (String)GetValue(dr[FieldNames.EntityName]),
                EventLogNameId = (Int32)GetValue(dr[FieldNames.EventLogEventLogNameId]),
                EventLogName = (String)GetValue(dr[FieldNames.EventLogNameName]),
                Source = (String)GetValue(dr[FieldNames.EventLogSource]),
                EventId = (Int32)GetValue(dr[FieldNames.EventLogEventId]),
                EntryType = (Int16)GetValue(dr[FieldNames.EventLogEntryType]),
                UserId = (Int32?)GetValue(dr[FieldNames.EventLogUserId]),
                UserName = (String)GetValue(dr[FieldNames.UserUserName]),
                DateTime = (DateTime)GetValue(dr[FieldNames.EventLogDateTime]),
                Process = (String)GetValue(dr[FieldNames.EventLogProcess]),
                ComputerName = (String)GetValue(dr[FieldNames.EventLogComputerName]),
                URLHelperLink = (String)GetValue(dr[FieldNames.EventLogURLHelperLink]),
                Description = (String)GetValue(dr[FieldNames.EventLogDescription]),
                Content = (String)GetValue(dr[FieldNames.EventLogContent]),
                GibraltarSessionId = (String)GetValue(dr[FieldNames.EventLogGibraltarSessionId]),
                WorkpackageName = (String)GetValue(dr[FieldNames.EventLogWorkpackageName]),
                WorkpackageId = (Int32?)GetValue(dr[FieldNames.EventLogWorkpackageId])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all entity infos in the database
        /// </summary>
        /// <returns>A list of event log dtos</returns>
        public IEnumerable<EventLogDto> FetchAll()
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchAll))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }



        /// <summary>
        /// Returns all events that occurred after given datetime
        /// </summary>
        /// <returns>A list of event log dtos</returns>
        public IEnumerable<EventLogDto> FetchAllOccuredAfterDateTime(DateTime lastUpdatedDateTime)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchAllOccuredAfterDateTime))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.EventLogDateTime,
                    SqlDbType.DateTime,
                    lastUpdatedDateTime);

                //execute reader
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }


        /// <summary>
        /// Returns all events that occurred after given datetime
        /// </summary>
        /// <returns>A list of event log dtos</returns>
        public IEnumerable<EventLogDto> FetchTopSpecifiedAmountIncludingDeleted(Int32 amountOfRecordsToReturn)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchTopSpecifiedAmountIncludingDeleted))
            {
                //NumberOfRecordsToReturn parameter
                CreateParameter(command,
                    "NumberOfRecordsToReturn",
                    SqlDbType.Int,
                    amountOfRecordsToReturn);

                //execute reader
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }


        /// <summary>
        /// Returns a single event log name record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public EventLogDto FetchById(Int32 id)
        {
            EventLogDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.EventLogId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchByEventLogNameId(Int32 id)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchByEventLogNameId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.EventLogNameId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchByEventLogNameIdEntryTypeEntityId(Int16 TopRowCount, Nullable<Int32> EventLogNameID, Nullable<Int16> EntryType, Nullable<Int32> EntityID)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchByEventLogNameIdEntryTypeEntityId))
                {
                    // EventLogNameId
                    CreateParameter(command, "TopRowCount", SqlDbType.Int, TopRowCount);
                    // EventLogNameId
                    CreateParameter(command, FieldNames.EventLogEventLogNameId, SqlDbType.Int, EventLogNameID);
                    // EventLogEntryType
                    CreateParameter(command, FieldNames.EventLogEntryType, SqlDbType.SmallInt, EntryType);
                    // EventLogEntityId
                    CreateParameter(command, FieldNames.EventLogEntityId, SqlDbType.Int, EntityID);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns all event Log in the database which fulfill given criteria
        /// </summary>
        /// <returns>A list of event log dtos</returns>
        public IEnumerable<EventLogDto> FetchByMixedCriteria(Int16 topRowCount, Nullable<Int32> eventLogNameId, String source,
            Nullable<Int32> eventId, Nullable<Int16> entryType, Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> dateTime,
            String process, String computerName, String urlHelperLink, String description,
            String content, String gibraltarSessionId, DateTime startDate, DateTime endDate, String workpackageName, Int32? workpackageId)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchByMixedCriteria))
                {
                    // TopRowCount
                    CreateParameter(command, "TopRowCount", SqlDbType.Int, topRowCount);
                    CreateParameter(command, FieldNames.EventLogEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.EventLogNameId, SqlDbType.Int, eventLogNameId);
                    CreateParameter(command, FieldNames.EventLogSource, SqlDbType.NVarChar, source);
                    CreateParameter(command, FieldNames.EventLogEventId, SqlDbType.Int, eventId);
                    CreateParameter(command, FieldNames.EventLogEntryType, SqlDbType.SmallInt, entryType);
                    CreateParameter(command, FieldNames.EventLogUserId, SqlDbType.Int, userId);
                    CreateParameter(command, FieldNames.EventLogDateTime, SqlDbType.DateTime, dateTime);
                    CreateParameter(command, FieldNames.EventLogProcess, SqlDbType.NVarChar, process);
                    CreateParameter(command, FieldNames.EventLogComputerName, SqlDbType.NVarChar, computerName);
                    CreateParameter(command, FieldNames.EventLogURLHelperLink, SqlDbType.NVarChar, urlHelperLink);
                    CreateParameter(command, FieldNames.EventLogDescription, SqlDbType.NVarChar, description);
                    CreateParameter(command, FieldNames.EventLogContent, SqlDbType.NVarChar, content);
                    CreateParameter(command, FieldNames.EventLogGibraltarSessionId, SqlDbType.NVarChar, gibraltarSessionId);
                    CreateParameter(command, FieldNames.EventLogStartDate, SqlDbType.DateTime, startDate);
                    CreateParameter(command, FieldNames.EventLogEndDate, SqlDbType.DateTime, endDate);
                    CreateParameter(command, FieldNames.EventLogWorkpackageName, SqlDbType.NVarChar, workpackageName);
                    CreateParameter(command, FieldNames.EventLogWorkpackageId, SqlDbType.Int, workpackageId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<EventLogDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogFetchByWorkpackageId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.EventLogWorkpackageId,
                        SqlDbType.Int,
                        workpackageId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(EventLogDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogInsert))
                {
                    // id
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.EventLogId,
                        SqlDbType.Int);

                    // Entity id
                    CreateParameter(command,
                        FieldNames.EventLogEntityId,
                        SqlDbType.Int,
                        dto.EntityId);
                    //Source
                    CreateParameter(command,
                        FieldNames.EventLogSource,
                        SqlDbType.NVarChar,
                        dto.Source);
                    // LogName
                    CreateParameter(command,
                        FieldNames.EventLogEventLogNameId,
                        SqlDbType.Int,
                        dto.EventLogNameId);
                    // Event id
                    CreateParameter(command,
                        FieldNames.EventLogEventId,
                        SqlDbType.Int,
                        dto.EventId);
                    //Entry Type,
                    CreateParameter(command,
                        FieldNames.EventLogEntryType,
                        SqlDbType.Int,
                        dto.EntryType);
                    // User id
                    CreateParameter(command,
                        FieldNames.EventLogUserId,
                        SqlDbType.Int,
                        dto.UserId);
                    //Process,
                    CreateParameter(command,
                        FieldNames.EventLogProcess,
                        SqlDbType.NVarChar,
                        dto.Process);
                    //ComputerName,
                    CreateParameter(command,
                        FieldNames.EventLogComputerName,
                        SqlDbType.NVarChar,
                        dto.ComputerName);
                    //URLHelperLink,
                    CreateParameter(command,
                        FieldNames.EventLogURLHelperLink,
                        SqlDbType.NVarChar,
                        dto.URLHelperLink);
                    //Description,
                    CreateParameter(command,
                        FieldNames.EventLogDescription,
                        SqlDbType.NVarChar,
                        dto.Description);
                    //Content
                    CreateParameter(command,
                        FieldNames.EventLogContent,
                        SqlDbType.NVarChar,
                        dto.Content);
                    //GibraltarSessionId
                    CreateParameter(command,
                        FieldNames.EventLogGibraltarSessionId,
                        SqlDbType.NVarChar,
                        dto.GibraltarSessionId);
                    //WorkpackageName
                    CreateParameter(command,
                        FieldNames.EventLogWorkpackageName,
                        SqlDbType.NVarChar,
                        dto.WorkpackageName);
                    //WorkpackageId
                    CreateParameter(command,
                        FieldNames.EventLogWorkpackageId,
                        SqlDbType.Int,
                        dto.WorkpackageId);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        public void InsertWithName(EventLogDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogInsertWithName))
                {
                    // id
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.EventLogId,
                        SqlDbType.Int);

                    // Entity id
                    CreateParameter(command,
                        FieldNames.EventLogEntityId,
                        SqlDbType.Int,
                        dto.EntityId);
                    //Source
                    CreateParameter(command,
                        FieldNames.EventLogSource,
                        SqlDbType.NVarChar,
                        dto.Source);
                    // LogName
                    CreateParameter(command,
                        FieldNames.EventLogNameName,
                        SqlDbType.NVarChar,
                        dto.EventLogName);
                    // Event id
                    CreateParameter(command,
                        FieldNames.EventLogEventId,
                        SqlDbType.Int,
                        dto.EventId);
                    //Entry Type,
                    CreateParameter(command,
                        FieldNames.EventLogEntryType,
                        SqlDbType.Int,
                        dto.EntryType);
                    // User id
                    CreateParameter(command,
                        FieldNames.EventLogUserId,
                        SqlDbType.Int,
                        dto.UserId);
                    //Process,
                    CreateParameter(command,
                        FieldNames.EventLogProcess,
                        SqlDbType.NVarChar,
                        dto.Process);
                    //ComputerName,
                    CreateParameter(command,
                        FieldNames.EventLogComputerName,
                        SqlDbType.NVarChar,
                        dto.ComputerName);
                    //URLHelperLink,
                    CreateParameter(command,
                        FieldNames.EventLogURLHelperLink,
                        SqlDbType.NVarChar,
                        dto.URLHelperLink);
                    //Description,
                    CreateParameter(command,
                        FieldNames.EventLogDescription,
                        SqlDbType.NVarChar,
                        dto.Description);
                    //Content
                    CreateParameter(command,
                        FieldNames.EventLogContent,
                        SqlDbType.NVarChar,
                        dto.Content);
                    //GibraltarSessionId
                    CreateParameter(command,
                        FieldNames.EventLogGibraltarSessionId,
                        SqlDbType.NVarChar,
                        dto.GibraltarSessionId);
                    //WorkpackageName
                    CreateParameter(command,
                        FieldNames.EventLogWorkpackageName,
                        SqlDbType.NVarChar,
                        dto.WorkpackageName);
                    //WorkpackageId
                    CreateParameter(command,
                        FieldNames.EventLogWorkpackageId,
                        SqlDbType.Int,
                        dto.WorkpackageId);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete

        public void DeleteAll()
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EventLogDeleteAll))
                {
                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
