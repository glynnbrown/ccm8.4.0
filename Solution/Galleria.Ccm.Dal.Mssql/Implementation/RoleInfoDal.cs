﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//		Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class RoleInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IRoleInfoDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        private RoleInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new RoleInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.RoleId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.RoleRowVersion])),
                Name = (String)GetValue(dr[FieldNames.RoleName]),
                Description = (String)GetValue(dr[FieldNames.RoleDescription]),
                IsAdministrator = (Boolean)GetValue(dr[FieldNames.RoleIsAdministrator]),
            };
        }

        #endregion


        #region Fetch

        /// <summary>
        /// Returns the specifed role info object
        /// </summary>
        /// <param name="id">The role id</param>
        /// <returns>The specified role info object</returns>
        public RoleInfoDto FetchById(Int32 id)
        {
            RoleInfoDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleInfoFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.RoleInfoId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        public IEnumerable<RoleInfoDto> FetchAll()
        {
            List<RoleInfoDto> dtoList = new List<RoleInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleInfoFetchAll))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public IEnumerable<RoleInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<RoleInfoDto> dtoList = new List<RoleInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.RoleEntity_EntityId, SqlDbType.Int, entityId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion


    }
}
