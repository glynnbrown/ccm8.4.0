﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class ImageDal : Galleria.Framework.Dal.Mssql.DalBase, IImageDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private ImageDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ImageDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ImageId]),
                EntityId = (Int32)GetValue(dr[FieldNames.ImageEntityId]),
                OriginalFileId = (Int32)GetValue(dr[FieldNames.ImageOriginalFileId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ImageRowVersion])),
                BlobId = (Int32)GetValue(dr[FieldNames.ImageBlobId]),
                CompressionId = (Int32)GetValue(dr[FieldNames.ImageCompressionId]),
                Height = (Int16)GetValue(dr[FieldNames.ImageHeight]),
                Width = (Int16)GetValue(dr[FieldNames.ImageWidth]),
                SizeInBytes = (Int64)GetValue(dr[FieldNames.ImageSizeInBytes]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ImageDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ImageDateLastModified])
            };
        }
        #endregion

        #region Fetch

        public ImageDto FetchById(int id)
        {
            ImageDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ImageFetchById))
                {
                    CreateParameter(command, FieldNames.ImageId, SqlDbType.Int, id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        public void Insert(ImageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ImageInsert))
                {
                    // Output parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ImageId, SqlDbType.Int);
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.ImageRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ImageDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ImageDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Add in the other parameters
                    CreateParameter(command, FieldNames.ImageOriginalFileId, SqlDbType.Int, dto.OriginalFileId);
                    CreateParameter(command, FieldNames.ImageBlobId, SqlDbType.Int, dto.BlobId);
                    CreateParameter(command, FieldNames.ImageCompressionId, SqlDbType.Int, dto.CompressionId);
                    CreateParameter(command, FieldNames.ImageWidth, SqlDbType.SmallInt, dto.Width);
                    CreateParameter(command, FieldNames.ImageHeight, SqlDbType.SmallInt, dto.Height);
                    CreateParameter(command, FieldNames.ImageSizeInBytes, SqlDbType.BigInt, dto.SizeInBytes);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(ImageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ImageUpdate))
                {
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.ImageRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ImageDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Add in the other parameters
                    CreateParameter(command, FieldNames.ImageId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.ImageOriginalFileId, SqlDbType.Int, dto.OriginalFileId);
                    CreateParameter(command, FieldNames.ImageBlobId, SqlDbType.Int, dto.BlobId);
                    CreateParameter(command, FieldNames.ImageCompressionId, SqlDbType.Int, dto.CompressionId);
                    CreateParameter(command, FieldNames.ImageWidth, SqlDbType.SmallInt, dto.Width);
                    CreateParameter(command, FieldNames.ImageHeight, SqlDbType.SmallInt, dto.Height);
                    CreateParameter(command, FieldNames.ImageSizeInBytes, SqlDbType.BigInt, dto.SizeInBytes);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
