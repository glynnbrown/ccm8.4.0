﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentInventoryRuleDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentInventoryRuleDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentInventoryRuleDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentInventoryRuleDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentInventoryRuleId]),
                ProductId  = (Int32)GetValue(dr[FieldNames.AssortmentInventoryRuleProductId]),
                ProductGtin = (String)GetValue(dr[FieldNames.AssortmentInventoryRuleProductGtin]),
                CasePack = (Single)GetValue(dr[FieldNames.AssortmentInventoryRuleCasePack]),
                DaysOfSupply = (Single)GetValue(dr[FieldNames.AssortmentInventoryRuleDaysOfSupply]),
                ShelfLife = (Single)GetValue(dr[FieldNames.AssortmentInventoryRuleShelfLife]),
                ReplenishmentDays = (Single)GetValue(dr[FieldNames.AssortmentInventoryRuleReplenishmentDays]),
                WasteHurdleUnits = (Single)GetValue(dr[FieldNames.AssortmentInventoryRuleWasteHurdleUnits]),
                WasteHurdleCasePack = (Single)GetValue(dr[FieldNames.AssortmentInventoryRuleWasteHurdleCasePack]),
                MinUnits = (Int32)GetValue(dr[FieldNames.AssortmentInventoryRuleMinUnits]),
                MinFacings = (Int32)GetValue(dr[FieldNames.AssortmentInventoryRuleMinFacings]),
                AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentInventoryRuleAssortmentId]),

            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all product dtos for the specified assortment
        /// </summary>
        /// <param name="assortmentId">The assortment id</param>
        /// <returns>All product dtos for the assortment</returns>
        public IEnumerable<AssortmentInventoryRuleDto> FetchByAssortmentId(Int32 assortmentId)
        {
            List<AssortmentInventoryRuleDto> dtoList = new List<AssortmentInventoryRuleDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInventoryRuleFetchByAssortmentId))
            {
                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentInventoryRuleAssortmentId,
                    SqlDbType.Int,
                    assortmentId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment local product id</param>
        /// <returns>The assortment local product</returns>
        public AssortmentInventoryRuleDto FetchById(Int32 id)
        {
            AssortmentInventoryRuleDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInventoryRuleFetchById))
            {
                // assortment local product id
                CreateParameter(command,
                    FieldNames.AssortmentInventoryRuleId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentInventoryRuleDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInventoryRuleInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentInventoryRuleId,
                    SqlDbType.Int);

                //other properties
                CreateParameter(command, FieldNames.AssortmentInventoryRuleAssortmentId, SqlDbType.Int, dto.AssortmentId);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleProductId, SqlDbType.Int, dto.ProductId);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleCasePack, SqlDbType.Real, dto.CasePack);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleDaysOfSupply, SqlDbType.Real, dto.DaysOfSupply);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleShelfLife, SqlDbType.Real, dto.ShelfLife);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleReplenishmentDays, SqlDbType.Real, dto.ReplenishmentDays);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleWasteHurdleUnits, SqlDbType.Real, dto.WasteHurdleUnits);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleWasteHurdleCasePack, SqlDbType.Real, dto.WasteHurdleCasePack);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleMinUnits, SqlDbType.Int, dto.MinUnits);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleMinFacings, SqlDbType.Int, dto.MinFacings);
                
                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentInventoryRuleDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInventoryRuleUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentInventoryRuleId,
                    SqlDbType.Int,
                    dto.Id);

                //other properties
                CreateParameter(command, FieldNames.AssortmentInventoryRuleAssortmentId, SqlDbType.Int, dto.AssortmentId);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleProductId, SqlDbType.Int, dto.ProductId);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleCasePack, SqlDbType.Real, dto.CasePack);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleDaysOfSupply, SqlDbType.Real, dto.DaysOfSupply);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleShelfLife, SqlDbType.Real, dto.ShelfLife);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleReplenishmentDays, SqlDbType.Real, dto.ReplenishmentDays);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleWasteHurdleUnits, SqlDbType.Real, dto.WasteHurdleUnits);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleWasteHurdleCasePack, SqlDbType.Real, dto.WasteHurdleCasePack);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleMinUnits, SqlDbType.Int, dto.MinUnits);
                CreateParameter(command, FieldNames.AssortmentInventoryRuleMinFacings, SqlDbType.Int, dto.MinFacings);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInventoryRuleDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentInventoryRuleId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}