﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentMinorRevisionDeListActionLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionDeListActionLocationDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionDeListActionLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionDeListActionLocationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionLocationId]),
                AssortmentMinorRevisionDeListActionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionLocationAssortmentMinorRevisionDeListActionId]),
                LocationCode = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionLocationLocationCode]),
                LocationName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionLocationLocationName]),
                LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionLocationLocationId])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionDeListActionLocationDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionDeListActionLocationDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionLocationFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment minor revision action id
        /// </summary>
        /// <param name="entityId">specified AssortmentMinorRevisionDeListAction id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionDeListActionLocationDto> FetchByAssortmentMinorRevisionDeListActionId(Int32 AssortmentMinorRevisionDeListActionId)
        {
            List<AssortmentMinorRevisionDeListActionLocationDto> dtoList = new List<AssortmentMinorRevisionDeListActionLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionLocationFetchByAssortmentMinorRevisionListActionId))
                {
                    //Assortment Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationAssortmentMinorRevisionDeListActionId, SqlDbType.Int, AssortmentMinorRevisionDeListActionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionDeListActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationAssortmentMinorRevisionDeListActionId, SqlDbType.Int, dto.AssortmentMinorRevisionDeListActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionDeListActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionLocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationAssortmentMinorRevisionDeListActionId, SqlDbType.Int, dto.AssortmentMinorRevisionDeListActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
