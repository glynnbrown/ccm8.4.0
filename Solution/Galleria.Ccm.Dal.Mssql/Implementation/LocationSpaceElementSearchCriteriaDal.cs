﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Location Space Element Search Criteria Dal
    /// </summary>
    public class LocationSpaceElementSearchCriteriaDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationSpaceElementSearchCriteriaDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public LocationSpaceElementSearchCriteriaDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationSpaceElementSearchCriteriaDto()
            {
                ProductGroupCode = (String)GetValue(dr[FieldNames.ProductGroupCode]),
                LocationCode = (String)GetValue(dr[FieldNames.LocationCode]),
                BayOrder = (Byte)GetValue(dr[FieldNames.LocationSpaceBayOrder]),
                Order = (Byte)GetValue(dr[FieldNames.LocationSpaceElementOrder]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<LocationSpaceElementSearchCriteriaDto> FetchByEntityId(int entityId)
        {
            List<LocationSpaceElementSearchCriteriaDto> dtoList = new List<LocationSpaceElementSearchCriteriaDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementSearchCriteriaFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationSpaceEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        #endregion
    }
}
