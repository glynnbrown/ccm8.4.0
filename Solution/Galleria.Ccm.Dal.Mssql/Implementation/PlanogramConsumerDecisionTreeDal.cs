﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramConsumerDecisionTree Dal
    /// </summary>
    public class PlanogramConsumerDecisionTreeDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramConsumerDecisionTreeDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramConsumerDecisionTreeDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramConsumerDecisionTreeDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreePlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeName]),
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramConsumerDecisionTree DTOs with a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramConsumerDecisionTree DTOs.</returns>
        public PlanogramConsumerDecisionTreeDto FetchByPlanogramId(object planogramId)
        {
            PlanogramConsumerDecisionTreeDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeFetchByPlanogramId))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreePlanogramId, SqlDbType.Int, (Int32)planogramId);

                    // Excute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramConsumerDecisionTreeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeInsert))
                {
                    // ID
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeId, SqlDbType.Int);

                    // Other Parameters
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeName, SqlDbType.NVarChar, dto.Name);

                    // Excute
                    command.ExecuteNonQuery();

                    // Update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramConsumerDecisionTreeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeUpdateById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeId, SqlDbType.Int, dto.Id);

                    // Other parameter
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeName, SqlDbType.NVarChar, dto.Name);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeDeleteById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeId, SqlDbType.Int, (Int32)id);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramConsumerDecisionTreeDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
