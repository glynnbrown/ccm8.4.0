﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26147 : L.Ineson
//  Created (Auto-generated)
// CCM-26332 : L.Ineson
//  Added Upsert
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-31456 : N.Foster
//  Added method to fetch batch of custom attributes, and bulk update
#endregion
#region Version History: CCM 840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Linq;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// CustomAttributeData Dal
    /// </summary>
    public class CustomAttributeDataDal : Galleria.Framework.Dal.Mssql.DalBase, ICustomAttributeDataDal
    {
        #region Constants

        private const String _importTableName = "#tmpCustomAttributeData";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createParentIdTableSql = "CREATE TABLE [{0}] (CustomAttributeData_ParentId INT)";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[CustomAttributeData_Id] [INT] NOT NULL, " +
                "[CustomAttributeData_ParentId] [INT] NOT NULL, " +
                "[CustomAttributeData_ParentType] [TINYINT] NOT NULL, " +
                "[CustomAttributeData_Text1] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text2] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text3] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text4] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text5] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text6] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text7] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text8] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text9] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text10] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text11] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text12] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text13] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text14] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text15] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text16] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text17] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text18] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text19] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text20] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text21] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text22] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text23] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text24] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text25] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text26] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text27] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text28] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text29] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text30] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text31] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text32] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text33] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text34] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text35] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text36] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text37] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text38] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text39] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text40] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text41] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text42] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text43] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text44] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text45] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text46] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text47] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text48] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text49] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Text50] [NVARCHAR](255) COLLATE database_default NULL, " +
                "[CustomAttributeData_Value1] [REAL] NULL, " +
                "[CustomAttributeData_Value2] [REAL] NULL, " +
                "[CustomAttributeData_Value3] [REAL] NULL, " +
                "[CustomAttributeData_Value4] [REAL] NULL, " +
                "[CustomAttributeData_Value5] [REAL] NULL, " +
                "[CustomAttributeData_Value6] [REAL] NULL, " +
                "[CustomAttributeData_Value7] [REAL] NULL, " +
                "[CustomAttributeData_Value8] [REAL] NULL, " +
                "[CustomAttributeData_Value9] [REAL] NULL, " +
                "[CustomAttributeData_Value10] [REAL] NULL, " +
                "[CustomAttributeData_Value11] [REAL] NULL, " +
                "[CustomAttributeData_Value12] [REAL] NULL, " +
                "[CustomAttributeData_Value13] [REAL] NULL, " +
                "[CustomAttributeData_Value14] [REAL] NULL, " +
                "[CustomAttributeData_Value15] [REAL] NULL, " +
                "[CustomAttributeData_Value16] [REAL] NULL, " +
                "[CustomAttributeData_Value17] [REAL] NULL, " +
                "[CustomAttributeData_Value18] [REAL] NULL, " +
                "[CustomAttributeData_Value19] [REAL] NULL, " +
                "[CustomAttributeData_Value20] [REAL] NULL, " +
                "[CustomAttributeData_Value21] [REAL] NULL, " +
                "[CustomAttributeData_Value22] [REAL] NULL, " +
                "[CustomAttributeData_Value23] [REAL] NULL, " +
                "[CustomAttributeData_Value24] [REAL] NULL, " +
                "[CustomAttributeData_Value25] [REAL] NULL, " +
                "[CustomAttributeData_Value26] [REAL] NULL, " +
                "[CustomAttributeData_Value27] [REAL] NULL, " +
                "[CustomAttributeData_Value28] [REAL] NULL, " +
                "[CustomAttributeData_Value29] [REAL] NULL, " +
                "[CustomAttributeData_Value30] [REAL] NULL, " +
                "[CustomAttributeData_Value31] [REAL] NULL, " +
                "[CustomAttributeData_Value32] [REAL] NULL, " +
                "[CustomAttributeData_Value33] [REAL] NULL, " +
                "[CustomAttributeData_Value34] [REAL] NULL, " +
                "[CustomAttributeData_Value35] [REAL] NULL, " +
                "[CustomAttributeData_Value36] [REAL] NULL, " +
                "[CustomAttributeData_Value37] [REAL] NULL, " +
                "[CustomAttributeData_Value38] [REAL] NULL, " +
                "[CustomAttributeData_Value39] [REAL] NULL, " +
                "[CustomAttributeData_Value40] [REAL] NULL, " +
                "[CustomAttributeData_Value41] [REAL] NULL, " +
                "[CustomAttributeData_Value42] [REAL] NULL, " +
                "[CustomAttributeData_Value43] [REAL] NULL, " +
                "[CustomAttributeData_Value44] [REAL] NULL, " +
                "[CustomAttributeData_Value45] [REAL] NULL, " +
                "[CustomAttributeData_Value46] [REAL] NULL, " +
                "[CustomAttributeData_Value47] [REAL] NULL, " +
                "[CustomAttributeData_Value48] [REAL] NULL, " +
                "[CustomAttributeData_Value49] [REAL] NULL, " +
                "[CustomAttributeData_Value50] [REAL] NULL, " +
                "[CustomAttributeData_Flag1] [BIT] NULL, " +
                "[CustomAttributeData_Flag2] [BIT] NULL, " +
                "[CustomAttributeData_Flag3] [BIT] NULL, " +
                "[CustomAttributeData_Flag4] [BIT] NULL, " +
                "[CustomAttributeData_Flag5] [BIT] NULL, " +
                "[CustomAttributeData_Flag6] [BIT] NULL, " +
                "[CustomAttributeData_Flag7] [BIT] NULL, " +
                "[CustomAttributeData_Flag8] [BIT] NULL, " +
                "[CustomAttributeData_Flag9] [BIT] NULL, " +
                "[CustomAttributeData_Flag10] [BIT] NULL, " +
                "[CustomAttributeData_Date1] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date2] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date3] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date4] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date5] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date6] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date7] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date8] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date9] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Date10] [SMALLDATETIME] NULL, " +
                "[CustomAttributeData_Note1] [NVARCHAR](1000) COLLATE database_default NULL, " +
                "[CustomAttributeData_Note2] [NVARCHAR](1000) COLLATE database_default NULL, " +
                "[CustomAttributeData_Note3] [NVARCHAR](1000) COLLATE database_default NULL, " +
                "[CustomAttributeData_Note4] [NVARCHAR](1000) COLLATE database_default NULL, " +
                "[CustomAttributeData_Note5] [NVARCHAR](1000) COLLATE database_default NULL " +
            ")";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class CustomAttributeDataBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<CustomAttributeDataDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public CustomAttributeDataBulkCopyDataReader(IEnumerable<CustomAttributeDataDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 128; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.ParentId;
                        break;
                    case 2:
                        value = _enumerator.Current.ParentType;
                        break;
                    case 3:
                        value = _enumerator.Current.Text1;
                        break;
                    case 4:
                        value = _enumerator.Current.Text2;
                        break;
                    case 5:
                        value = _enumerator.Current.Text3;
                        break;
                    case 6:
                        value = _enumerator.Current.Text4;
                        break;
                    case 7:
                        value = _enumerator.Current.Text5;
                        break;
                    case 8:
                        value = _enumerator.Current.Text6;
                        break;
                    case 9:
                        value = _enumerator.Current.Text7;
                        break;
                    case 10:
                        value = _enumerator.Current.Text8;
                        break;
                    case 11:
                        value = _enumerator.Current.Text9;
                        break;
                    case 12:
                        value = _enumerator.Current.Text10;
                        break;
                    case 13:
                        value = _enumerator.Current.Text11;
                        break;
                    case 14:
                        value = _enumerator.Current.Text12;
                        break;
                    case 15:
                        value = _enumerator.Current.Text13;
                        break;
                    case 16:
                        value = _enumerator.Current.Text14;
                        break;
                    case 17:
                        value = _enumerator.Current.Text15;
                        break;
                    case 18:
                        value = _enumerator.Current.Text16;
                        break;
                    case 19:
                        value = _enumerator.Current.Text17;
                        break;
                    case 20:
                        value = _enumerator.Current.Text18;
                        break;
                    case 21:
                        value = _enumerator.Current.Text19;
                        break;
                    case 22:
                        value = _enumerator.Current.Text20;
                        break;
                    case 23:
                        value = _enumerator.Current.Text21;
                        break;
                    case 24:
                        value = _enumerator.Current.Text22;
                        break;
                    case 25:
                        value = _enumerator.Current.Text23;
                        break;
                    case 26:
                        value = _enumerator.Current.Text24;
                        break;
                    case 27:
                        value = _enumerator.Current.Text25;
                        break;
                    case 28:
                        value = _enumerator.Current.Text26;
                        break;
                    case 29:
                        value = _enumerator.Current.Text27;
                        break;
                    case 30:
                        value = _enumerator.Current.Text28;
                        break;
                    case 31:
                        value = _enumerator.Current.Text29;
                        break;
                    case 32:
                        value = _enumerator.Current.Text30;
                        break;
                    case 33:
                        value = _enumerator.Current.Text31;
                        break;
                    case 34:
                        value = _enumerator.Current.Text32;
                        break;
                    case 35:
                        value = _enumerator.Current.Text33;
                        break;
                    case 36:
                        value = _enumerator.Current.Text34;
                        break;
                    case 37:
                        value = _enumerator.Current.Text35;
                        break;
                    case 38:
                        value = _enumerator.Current.Text36;
                        break;
                    case 39:
                        value = _enumerator.Current.Text37;
                        break;
                    case 40:
                        value = _enumerator.Current.Text38;
                        break;
                    case 41:
                        value = _enumerator.Current.Text39;
                        break;
                    case 42:
                        value = _enumerator.Current.Text40;
                        break;
                    case 43:
                        value = _enumerator.Current.Text41;
                        break;
                    case 44:
                        value = _enumerator.Current.Text42;
                        break;
                    case 45:
                        value = _enumerator.Current.Text43;
                        break;
                    case 46:
                        value = _enumerator.Current.Text44;
                        break;
                    case 47:
                        value = _enumerator.Current.Text45;
                        break;
                    case 48:
                        value = _enumerator.Current.Text46;
                        break;
                    case 49:
                        value = _enumerator.Current.Text47;
                        break;
                    case 50:
                        value = _enumerator.Current.Text48;
                        break;
                    case 51:
                        value = _enumerator.Current.Text49;
                        break;
                    case 52:
                        value = _enumerator.Current.Text50;
                        break;
                    case 53:
                        value = _enumerator.Current.Value1;
                        break;
                    case 54:
                        value = _enumerator.Current.Value2;
                        break;
                    case 55:
                        value = _enumerator.Current.Value3;
                        break;
                    case 56:
                        value = _enumerator.Current.Value4;
                        break;
                    case 57:
                        value = _enumerator.Current.Value5;
                        break;
                    case 58:
                        value = _enumerator.Current.Value6;
                        break;
                    case 59:
                        value = _enumerator.Current.Value7;
                        break;
                    case 60:
                        value = _enumerator.Current.Value8;
                        break;
                    case 61:
                        value = _enumerator.Current.Value9;
                        break;
                    case 62:
                        value = _enumerator.Current.Value10;
                        break;
                    case 63:
                        value = _enumerator.Current.Value11;
                        break;
                    case 64:
                        value = _enumerator.Current.Value12;
                        break;
                    case 65:
                        value = _enumerator.Current.Value13;
                        break;
                    case 66:
                        value = _enumerator.Current.Value14;
                        break;
                    case 67:
                        value = _enumerator.Current.Value15;
                        break;
                    case 68:
                        value = _enumerator.Current.Value16;
                        break;
                    case 69:
                        value = _enumerator.Current.Value17;
                        break;
                    case 70:
                        value = _enumerator.Current.Value18;
                        break;
                    case 71:
                        value = _enumerator.Current.Value19;
                        break;
                    case 72:
                        value = _enumerator.Current.Value20;
                        break;
                    case 73:
                        value = _enumerator.Current.Value21;
                        break;
                    case 74:
                        value = _enumerator.Current.Value22;
                        break;
                    case 75:
                        value = _enumerator.Current.Value23;
                        break;
                    case 76:
                        value = _enumerator.Current.Value24;
                        break;
                    case 77:
                        value = _enumerator.Current.Value25;
                        break;
                    case 78:
                        value = _enumerator.Current.Value26;
                        break;
                    case 79:
                        value = _enumerator.Current.Value27;
                        break;
                    case 80:
                        value = _enumerator.Current.Value28;
                        break;
                    case 81:
                        value = _enumerator.Current.Value29;
                        break;
                    case 82:
                        value = _enumerator.Current.Value30;
                        break;
                    case 83:
                        value = _enumerator.Current.Value31;
                        break;
                    case 84:
                        value = _enumerator.Current.Value32;
                        break;
                    case 85:
                        value = _enumerator.Current.Value33;
                        break;
                    case 86:
                        value = _enumerator.Current.Value34;
                        break;
                    case 87:
                        value = _enumerator.Current.Value35;
                        break;
                    case 88:
                        value = _enumerator.Current.Value36;
                        break;
                    case 89:
                        value = _enumerator.Current.Value37;
                        break;
                    case 90:
                        value = _enumerator.Current.Value38;
                        break;
                    case 91:
                        value = _enumerator.Current.Value39;
                        break;
                    case 92:
                        value = _enumerator.Current.Value40;
                        break;
                    case 93:
                        value = _enumerator.Current.Value41;
                        break;
                    case 94:
                        value = _enumerator.Current.Value42;
                        break;
                    case 95:
                        value = _enumerator.Current.Value43;
                        break;
                    case 96:
                        value = _enumerator.Current.Value44;
                        break;
                    case 97:
                        value = _enumerator.Current.Value45;
                        break;
                    case 98:
                        value = _enumerator.Current.Value46;
                        break;
                    case 99:
                        value = _enumerator.Current.Value47;
                        break;
                    case 100:
                        value = _enumerator.Current.Value48;
                        break;
                    case 101:
                        value = _enumerator.Current.Value49;
                        break;
                    case 102:
                        value = _enumerator.Current.Value50;
                        break;
                    case 103:
                        value = _enumerator.Current.Flag1;
                        break;
                    case 104:
                        value = _enumerator.Current.Flag2;
                        break;
                    case 105:
                        value = _enumerator.Current.Flag3;
                        break;
                    case 106:
                        value = _enumerator.Current.Flag4;
                        break;
                    case 107:
                        value = _enumerator.Current.Flag5;
                        break;
                    case 108:
                        value = _enumerator.Current.Flag6;
                        break;
                    case 109:
                        value = _enumerator.Current.Flag7;
                        break;
                    case 110:
                        value = _enumerator.Current.Flag8;
                        break;
                    case 111:
                        value = _enumerator.Current.Flag9;
                        break;
                    case 112:
                        value = _enumerator.Current.Flag10;
                        break;
                    case 113:
                        value = _enumerator.Current.Date1;
                        break;
                    case 114:
                        value = _enumerator.Current.Date2;
                        break;
                    case 115:
                        value = _enumerator.Current.Date3;
                        break;
                    case 116:
                        value = _enumerator.Current.Date4;
                        break;
                    case 117:
                        value = _enumerator.Current.Date5;
                        break;
                    case 118:
                        value = _enumerator.Current.Date6;
                        break;
                    case 119:
                        value = _enumerator.Current.Date7;
                        break;
                    case 120:
                        value = _enumerator.Current.Date8;
                        break;
                    case 121:
                        value = _enumerator.Current.Date9;
                        break;
                    case 122:
                        value = _enumerator.Current.Date10;
                        break;
                    case 123:
                        value = _enumerator.Current.Note1;
                        break;
                    case 124:
                        value = _enumerator.Current.Note2;
                        break;
                    case 125:
                        value = _enumerator.Current.Note3;
                        break;
                    case 126:
                        value = _enumerator.Current.Note4;
                        break;
                    case 127:
                        value = _enumerator.Current.Note5;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static CustomAttributeDataDto GetDataTransferObject(SqlDataReader dr)
        {
            return new CustomAttributeDataDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.CustomAttributeDataId]),
                ParentId = (Int32)GetValue(dr[FieldNames.CustomAttributeDataParentId]),
                ParentType = (Byte)GetValue(dr[FieldNames.CustomAttributeDataParentType]),
                Text1 = (String)GetValue(dr[FieldNames.CustomAttributeDataText1]),
                Text2 = (String)GetValue(dr[FieldNames.CustomAttributeDataText2]),
                Text3 = (String)GetValue(dr[FieldNames.CustomAttributeDataText3]),
                Text4 = (String)GetValue(dr[FieldNames.CustomAttributeDataText4]),
                Text5 = (String)GetValue(dr[FieldNames.CustomAttributeDataText5]),
                Text6 = (String)GetValue(dr[FieldNames.CustomAttributeDataText6]),
                Text7 = (String)GetValue(dr[FieldNames.CustomAttributeDataText7]),
                Text8 = (String)GetValue(dr[FieldNames.CustomAttributeDataText8]),
                Text9 = (String)GetValue(dr[FieldNames.CustomAttributeDataText9]),
                Text10 = (String)GetValue(dr[FieldNames.CustomAttributeDataText10]),
                Text11 = (String)GetValue(dr[FieldNames.CustomAttributeDataText11]),
                Text12 = (String)GetValue(dr[FieldNames.CustomAttributeDataText12]),
                Text13 = (String)GetValue(dr[FieldNames.CustomAttributeDataText13]),
                Text14 = (String)GetValue(dr[FieldNames.CustomAttributeDataText14]),
                Text15 = (String)GetValue(dr[FieldNames.CustomAttributeDataText15]),
                Text16 = (String)GetValue(dr[FieldNames.CustomAttributeDataText16]),
                Text17 = (String)GetValue(dr[FieldNames.CustomAttributeDataText17]),
                Text18 = (String)GetValue(dr[FieldNames.CustomAttributeDataText18]),
                Text19 = (String)GetValue(dr[FieldNames.CustomAttributeDataText19]),
                Text20 = (String)GetValue(dr[FieldNames.CustomAttributeDataText20]),
                Text21 = (String)GetValue(dr[FieldNames.CustomAttributeDataText21]),
                Text22 = (String)GetValue(dr[FieldNames.CustomAttributeDataText22]),
                Text23 = (String)GetValue(dr[FieldNames.CustomAttributeDataText23]),
                Text24 = (String)GetValue(dr[FieldNames.CustomAttributeDataText24]),
                Text25 = (String)GetValue(dr[FieldNames.CustomAttributeDataText25]),
                Text26 = (String)GetValue(dr[FieldNames.CustomAttributeDataText26]),
                Text27 = (String)GetValue(dr[FieldNames.CustomAttributeDataText27]),
                Text28 = (String)GetValue(dr[FieldNames.CustomAttributeDataText28]),
                Text29 = (String)GetValue(dr[FieldNames.CustomAttributeDataText29]),
                Text30 = (String)GetValue(dr[FieldNames.CustomAttributeDataText30]),
                Text31 = (String)GetValue(dr[FieldNames.CustomAttributeDataText31]),
                Text32 = (String)GetValue(dr[FieldNames.CustomAttributeDataText32]),
                Text33 = (String)GetValue(dr[FieldNames.CustomAttributeDataText33]),
                Text34 = (String)GetValue(dr[FieldNames.CustomAttributeDataText34]),
                Text35 = (String)GetValue(dr[FieldNames.CustomAttributeDataText35]),
                Text36 = (String)GetValue(dr[FieldNames.CustomAttributeDataText36]),
                Text37 = (String)GetValue(dr[FieldNames.CustomAttributeDataText37]),
                Text38 = (String)GetValue(dr[FieldNames.CustomAttributeDataText38]),
                Text39 = (String)GetValue(dr[FieldNames.CustomAttributeDataText39]),
                Text40 = (String)GetValue(dr[FieldNames.CustomAttributeDataText40]),
                Text41 = (String)GetValue(dr[FieldNames.CustomAttributeDataText41]),
                Text42 = (String)GetValue(dr[FieldNames.CustomAttributeDataText42]),
                Text43 = (String)GetValue(dr[FieldNames.CustomAttributeDataText43]),
                Text44 = (String)GetValue(dr[FieldNames.CustomAttributeDataText44]),
                Text45 = (String)GetValue(dr[FieldNames.CustomAttributeDataText45]),
                Text46 = (String)GetValue(dr[FieldNames.CustomAttributeDataText46]),
                Text47 = (String)GetValue(dr[FieldNames.CustomAttributeDataText47]),
                Text48 = (String)GetValue(dr[FieldNames.CustomAttributeDataText48]),
                Text49 = (String)GetValue(dr[FieldNames.CustomAttributeDataText49]),
                Text50 = (String)GetValue(dr[FieldNames.CustomAttributeDataText50]),
                Value1 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue1]),
                Value2 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue2]),
                Value3 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue3]),
                Value4 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue4]),
                Value5 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue5]),
                Value6 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue6]),
                Value7 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue7]),
                Value8 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue8]),
                Value9 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue9]),
                Value10 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue10]),
                Value11 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue11]),
                Value12 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue12]),
                Value13 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue13]),
                Value14 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue14]),
                Value15 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue15]),
                Value16 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue16]),
                Value17 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue17]),
                Value18 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue18]),
                Value19 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue19]),
                Value20 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue20]),
                Value21 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue21]),
                Value22 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue22]),
                Value23 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue23]),
                Value24 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue24]),
                Value25 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue25]),
                Value26 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue26]),
                Value27 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue27]),
                Value28 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue28]),
                Value29 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue29]),
                Value30 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue30]),
                Value31 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue31]),
                Value32 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue32]),
                Value33 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue33]),
                Value34 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue34]),
                Value35 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue35]),
                Value36 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue36]),
                Value37 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue37]),
                Value38 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue38]),
                Value39 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue39]),
                Value40 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue40]),
                Value41 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue41]),
                Value42 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue42]),
                Value43 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue43]),
                Value44 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue44]),
                Value45 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue45]),
                Value46 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue46]),
                Value47 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue47]),
                Value48 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue48]),
                Value49 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue49]),
                Value50 = (Single)GetValue(dr[FieldNames.CustomAttributeDataValue50]),
                Flag1 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag1]),
                Flag2 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag2]),
                Flag3 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag3]),
                Flag4 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag4]),
                Flag5 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag5]),
                Flag6 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag6]),
                Flag7 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag7]),
                Flag8 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag8]),
                Flag9 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag9]),
                Flag10 = (Boolean)GetValue(dr[FieldNames.CustomAttributeDataFlag10]),
                Date1 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate1]),
                Date2 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate2]),
                Date3 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate3]),
                Date4 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate4]),
                Date5 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate5]),
                Date6 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate6]),
                Date7 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate7]),
                Date8 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate8]),
                Date9 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate9]),
                Date10 = (DateTime?)GetValue(dr[FieldNames.CustomAttributeDataDate10]),
                Note1 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote1]),
                Note2 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote2]),
                Note3 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote3]),
                Note4 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote4]),
                Note5 = (String)GetValue(dr[FieldNames.CustomAttributeDataNote5])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public CustomAttributeDataDto FetchByParentTypeParentId(Byte parentType, Object parentId)
        {
            CustomAttributeDataDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataFetchByParentTypeParentId))
                {
                    CreateParameter(command, FieldNames.CustomAttributeDataParentId, SqlDbType.Int, parentId);
                    CreateParameter(command, FieldNames.CustomAttributeDataParentType, SqlDbType.TinyInt, parentType);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a collection of custom attributes for the specified parents
        /// </summary>
        public IEnumerable<CustomAttributeDataDto> FetchByParentTypeParentIds(Byte parentType, IEnumerable<Object> parentIds)
        {
            List<CustomAttributeDataDto> dtoList = new List<CustomAttributeDataDto>();

            // perform a batch insert to temp table
            CreateTempImportTable(_createParentIdTableSql);
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                bulkCopy.WriteToServer(new BulkCopyDataReader<Int32>(parentIds.Select(item => (Int32)item)));
            }

            try
            {
                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataFetchByParentTypeParentIds))
                {
                    CreateParameter(command, FieldNames.CustomAttributeDataParentType, SqlDbType.Int, parentType);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }

            // return the dto list
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(CustomAttributeDataDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.CustomAttributeDataId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.CustomAttributeDataParentId, SqlDbType.Int, dto.ParentId);
                    CreateParameter(command, FieldNames.CustomAttributeDataParentType, SqlDbType.TinyInt, dto.ParentType);
                    CreateParameter(command, FieldNames.CustomAttributeDataText1, SqlDbType.NVarChar, dto.Text1);
                    CreateParameter(command, FieldNames.CustomAttributeDataText2, SqlDbType.NVarChar, dto.Text2);
                    CreateParameter(command, FieldNames.CustomAttributeDataText3, SqlDbType.NVarChar, dto.Text3);
                    CreateParameter(command, FieldNames.CustomAttributeDataText4, SqlDbType.NVarChar, dto.Text4);
                    CreateParameter(command, FieldNames.CustomAttributeDataText5, SqlDbType.NVarChar, dto.Text5);
                    CreateParameter(command, FieldNames.CustomAttributeDataText6, SqlDbType.NVarChar, dto.Text6);
                    CreateParameter(command, FieldNames.CustomAttributeDataText7, SqlDbType.NVarChar, dto.Text7);
                    CreateParameter(command, FieldNames.CustomAttributeDataText8, SqlDbType.NVarChar, dto.Text8);
                    CreateParameter(command, FieldNames.CustomAttributeDataText9, SqlDbType.NVarChar, dto.Text9);
                    CreateParameter(command, FieldNames.CustomAttributeDataText10, SqlDbType.NVarChar, dto.Text10);
                    CreateParameter(command, FieldNames.CustomAttributeDataText11, SqlDbType.NVarChar, dto.Text11);
                    CreateParameter(command, FieldNames.CustomAttributeDataText12, SqlDbType.NVarChar, dto.Text12);
                    CreateParameter(command, FieldNames.CustomAttributeDataText13, SqlDbType.NVarChar, dto.Text13);
                    CreateParameter(command, FieldNames.CustomAttributeDataText14, SqlDbType.NVarChar, dto.Text14);
                    CreateParameter(command, FieldNames.CustomAttributeDataText15, SqlDbType.NVarChar, dto.Text15);
                    CreateParameter(command, FieldNames.CustomAttributeDataText16, SqlDbType.NVarChar, dto.Text16);
                    CreateParameter(command, FieldNames.CustomAttributeDataText17, SqlDbType.NVarChar, dto.Text17);
                    CreateParameter(command, FieldNames.CustomAttributeDataText18, SqlDbType.NVarChar, dto.Text18);
                    CreateParameter(command, FieldNames.CustomAttributeDataText19, SqlDbType.NVarChar, dto.Text19);
                    CreateParameter(command, FieldNames.CustomAttributeDataText20, SqlDbType.NVarChar, dto.Text20);
                    CreateParameter(command, FieldNames.CustomAttributeDataText21, SqlDbType.NVarChar, dto.Text21);
                    CreateParameter(command, FieldNames.CustomAttributeDataText22, SqlDbType.NVarChar, dto.Text22);
                    CreateParameter(command, FieldNames.CustomAttributeDataText23, SqlDbType.NVarChar, dto.Text23);
                    CreateParameter(command, FieldNames.CustomAttributeDataText24, SqlDbType.NVarChar, dto.Text24);
                    CreateParameter(command, FieldNames.CustomAttributeDataText25, SqlDbType.NVarChar, dto.Text25);
                    CreateParameter(command, FieldNames.CustomAttributeDataText26, SqlDbType.NVarChar, dto.Text26);
                    CreateParameter(command, FieldNames.CustomAttributeDataText27, SqlDbType.NVarChar, dto.Text27);
                    CreateParameter(command, FieldNames.CustomAttributeDataText28, SqlDbType.NVarChar, dto.Text28);
                    CreateParameter(command, FieldNames.CustomAttributeDataText29, SqlDbType.NVarChar, dto.Text29);
                    CreateParameter(command, FieldNames.CustomAttributeDataText30, SqlDbType.NVarChar, dto.Text30);
                    CreateParameter(command, FieldNames.CustomAttributeDataText31, SqlDbType.NVarChar, dto.Text31);
                    CreateParameter(command, FieldNames.CustomAttributeDataText32, SqlDbType.NVarChar, dto.Text32);
                    CreateParameter(command, FieldNames.CustomAttributeDataText33, SqlDbType.NVarChar, dto.Text33);
                    CreateParameter(command, FieldNames.CustomAttributeDataText34, SqlDbType.NVarChar, dto.Text34);
                    CreateParameter(command, FieldNames.CustomAttributeDataText35, SqlDbType.NVarChar, dto.Text35);
                    CreateParameter(command, FieldNames.CustomAttributeDataText36, SqlDbType.NVarChar, dto.Text36);
                    CreateParameter(command, FieldNames.CustomAttributeDataText37, SqlDbType.NVarChar, dto.Text37);
                    CreateParameter(command, FieldNames.CustomAttributeDataText38, SqlDbType.NVarChar, dto.Text38);
                    CreateParameter(command, FieldNames.CustomAttributeDataText39, SqlDbType.NVarChar, dto.Text39);
                    CreateParameter(command, FieldNames.CustomAttributeDataText40, SqlDbType.NVarChar, dto.Text40);
                    CreateParameter(command, FieldNames.CustomAttributeDataText41, SqlDbType.NVarChar, dto.Text41);
                    CreateParameter(command, FieldNames.CustomAttributeDataText42, SqlDbType.NVarChar, dto.Text42);
                    CreateParameter(command, FieldNames.CustomAttributeDataText43, SqlDbType.NVarChar, dto.Text43);
                    CreateParameter(command, FieldNames.CustomAttributeDataText44, SqlDbType.NVarChar, dto.Text44);
                    CreateParameter(command, FieldNames.CustomAttributeDataText45, SqlDbType.NVarChar, dto.Text45);
                    CreateParameter(command, FieldNames.CustomAttributeDataText46, SqlDbType.NVarChar, dto.Text46);
                    CreateParameter(command, FieldNames.CustomAttributeDataText47, SqlDbType.NVarChar, dto.Text47);
                    CreateParameter(command, FieldNames.CustomAttributeDataText48, SqlDbType.NVarChar, dto.Text48);
                    CreateParameter(command, FieldNames.CustomAttributeDataText49, SqlDbType.NVarChar, dto.Text49);
                    CreateParameter(command, FieldNames.CustomAttributeDataText50, SqlDbType.NVarChar, dto.Text50);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue1, SqlDbType.Real, dto.Value1);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue2, SqlDbType.Real, dto.Value2);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue3, SqlDbType.Real, dto.Value3);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue4, SqlDbType.Real, dto.Value4);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue5, SqlDbType.Real, dto.Value5);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue6, SqlDbType.Real, dto.Value6);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue7, SqlDbType.Real, dto.Value7);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue8, SqlDbType.Real, dto.Value8);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue9, SqlDbType.Real, dto.Value9);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue10, SqlDbType.Real, dto.Value10);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue11, SqlDbType.Real, dto.Value11);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue12, SqlDbType.Real, dto.Value12);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue13, SqlDbType.Real, dto.Value13);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue14, SqlDbType.Real, dto.Value14);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue15, SqlDbType.Real, dto.Value15);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue16, SqlDbType.Real, dto.Value16);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue17, SqlDbType.Real, dto.Value17);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue18, SqlDbType.Real, dto.Value18);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue19, SqlDbType.Real, dto.Value19);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue20, SqlDbType.Real, dto.Value20);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue21, SqlDbType.Real, dto.Value21);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue22, SqlDbType.Real, dto.Value22);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue23, SqlDbType.Real, dto.Value23);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue24, SqlDbType.Real, dto.Value24);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue25, SqlDbType.Real, dto.Value25);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue26, SqlDbType.Real, dto.Value26);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue27, SqlDbType.Real, dto.Value27);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue28, SqlDbType.Real, dto.Value28);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue29, SqlDbType.Real, dto.Value29);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue30, SqlDbType.Real, dto.Value30);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue31, SqlDbType.Real, dto.Value31);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue32, SqlDbType.Real, dto.Value32);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue33, SqlDbType.Real, dto.Value33);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue34, SqlDbType.Real, dto.Value34);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue35, SqlDbType.Real, dto.Value35);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue36, SqlDbType.Real, dto.Value36);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue37, SqlDbType.Real, dto.Value37);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue38, SqlDbType.Real, dto.Value38);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue39, SqlDbType.Real, dto.Value39);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue40, SqlDbType.Real, dto.Value40);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue41, SqlDbType.Real, dto.Value41);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue42, SqlDbType.Real, dto.Value42);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue43, SqlDbType.Real, dto.Value43);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue44, SqlDbType.Real, dto.Value44);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue45, SqlDbType.Real, dto.Value45);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue46, SqlDbType.Real, dto.Value46);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue47, SqlDbType.Real, dto.Value47);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue48, SqlDbType.Real, dto.Value48);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue49, SqlDbType.Real, dto.Value49);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue50, SqlDbType.Real, dto.Value50);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag1, SqlDbType.Bit, dto.Flag1);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag2, SqlDbType.Bit, dto.Flag2);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag3, SqlDbType.Bit, dto.Flag3);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag4, SqlDbType.Bit, dto.Flag4);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag5, SqlDbType.Bit, dto.Flag5);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag6, SqlDbType.Bit, dto.Flag6);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag7, SqlDbType.Bit, dto.Flag7);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag8, SqlDbType.Bit, dto.Flag8);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag9, SqlDbType.Bit, dto.Flag9);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag10, SqlDbType.Bit, dto.Flag10);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate1, SqlDbType.SmallDateTime, dto.Date1);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate2, SqlDbType.SmallDateTime, dto.Date2);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate3, SqlDbType.SmallDateTime, dto.Date3);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate4, SqlDbType.SmallDateTime, dto.Date4);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate5, SqlDbType.SmallDateTime, dto.Date5);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate6, SqlDbType.SmallDateTime, dto.Date6);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate7, SqlDbType.SmallDateTime, dto.Date7);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate8, SqlDbType.SmallDateTime, dto.Date8);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate9, SqlDbType.SmallDateTime, dto.Date9);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate10, SqlDbType.SmallDateTime, dto.Date10);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote1, SqlDbType.NVarChar, dto.Note1);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote2, SqlDbType.NVarChar, dto.Note2);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote3, SqlDbType.NVarChar, dto.Note3);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote4, SqlDbType.NVarChar, dto.Note4);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote5, SqlDbType.NVarChar, dto.Note5);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<CustomAttributeDataDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, CustomAttributeDataDto> index = new Dictionary<Object, CustomAttributeDataDto>();
            foreach (CustomAttributeDataDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_createImportTableSql);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    CustomAttributeDataBulkCopyDataReader dr = new CustomAttributeDataBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(CustomAttributeDataDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.CustomAttributeDataId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.CustomAttributeDataParentId, SqlDbType.Int, dto.ParentId);
                    CreateParameter(command, FieldNames.CustomAttributeDataParentType, SqlDbType.TinyInt, dto.ParentType);
                    CreateParameter(command, FieldNames.CustomAttributeDataText1, SqlDbType.NVarChar, dto.Text1);
                    CreateParameter(command, FieldNames.CustomAttributeDataText2, SqlDbType.NVarChar, dto.Text2);
                    CreateParameter(command, FieldNames.CustomAttributeDataText3, SqlDbType.NVarChar, dto.Text3);
                    CreateParameter(command, FieldNames.CustomAttributeDataText4, SqlDbType.NVarChar, dto.Text4);
                    CreateParameter(command, FieldNames.CustomAttributeDataText5, SqlDbType.NVarChar, dto.Text5);
                    CreateParameter(command, FieldNames.CustomAttributeDataText6, SqlDbType.NVarChar, dto.Text6);
                    CreateParameter(command, FieldNames.CustomAttributeDataText7, SqlDbType.NVarChar, dto.Text7);
                    CreateParameter(command, FieldNames.CustomAttributeDataText8, SqlDbType.NVarChar, dto.Text8);
                    CreateParameter(command, FieldNames.CustomAttributeDataText9, SqlDbType.NVarChar, dto.Text9);
                    CreateParameter(command, FieldNames.CustomAttributeDataText10, SqlDbType.NVarChar, dto.Text10);
                    CreateParameter(command, FieldNames.CustomAttributeDataText11, SqlDbType.NVarChar, dto.Text11);
                    CreateParameter(command, FieldNames.CustomAttributeDataText12, SqlDbType.NVarChar, dto.Text12);
                    CreateParameter(command, FieldNames.CustomAttributeDataText13, SqlDbType.NVarChar, dto.Text13);
                    CreateParameter(command, FieldNames.CustomAttributeDataText14, SqlDbType.NVarChar, dto.Text14);
                    CreateParameter(command, FieldNames.CustomAttributeDataText15, SqlDbType.NVarChar, dto.Text15);
                    CreateParameter(command, FieldNames.CustomAttributeDataText16, SqlDbType.NVarChar, dto.Text16);
                    CreateParameter(command, FieldNames.CustomAttributeDataText17, SqlDbType.NVarChar, dto.Text17);
                    CreateParameter(command, FieldNames.CustomAttributeDataText18, SqlDbType.NVarChar, dto.Text18);
                    CreateParameter(command, FieldNames.CustomAttributeDataText19, SqlDbType.NVarChar, dto.Text19);
                    CreateParameter(command, FieldNames.CustomAttributeDataText20, SqlDbType.NVarChar, dto.Text20);
                    CreateParameter(command, FieldNames.CustomAttributeDataText21, SqlDbType.NVarChar, dto.Text21);
                    CreateParameter(command, FieldNames.CustomAttributeDataText22, SqlDbType.NVarChar, dto.Text22);
                    CreateParameter(command, FieldNames.CustomAttributeDataText23, SqlDbType.NVarChar, dto.Text23);
                    CreateParameter(command, FieldNames.CustomAttributeDataText24, SqlDbType.NVarChar, dto.Text24);
                    CreateParameter(command, FieldNames.CustomAttributeDataText25, SqlDbType.NVarChar, dto.Text25);
                    CreateParameter(command, FieldNames.CustomAttributeDataText26, SqlDbType.NVarChar, dto.Text26);
                    CreateParameter(command, FieldNames.CustomAttributeDataText27, SqlDbType.NVarChar, dto.Text27);
                    CreateParameter(command, FieldNames.CustomAttributeDataText28, SqlDbType.NVarChar, dto.Text28);
                    CreateParameter(command, FieldNames.CustomAttributeDataText29, SqlDbType.NVarChar, dto.Text29);
                    CreateParameter(command, FieldNames.CustomAttributeDataText30, SqlDbType.NVarChar, dto.Text30);
                    CreateParameter(command, FieldNames.CustomAttributeDataText31, SqlDbType.NVarChar, dto.Text31);
                    CreateParameter(command, FieldNames.CustomAttributeDataText32, SqlDbType.NVarChar, dto.Text32);
                    CreateParameter(command, FieldNames.CustomAttributeDataText33, SqlDbType.NVarChar, dto.Text33);
                    CreateParameter(command, FieldNames.CustomAttributeDataText34, SqlDbType.NVarChar, dto.Text34);
                    CreateParameter(command, FieldNames.CustomAttributeDataText35, SqlDbType.NVarChar, dto.Text35);
                    CreateParameter(command, FieldNames.CustomAttributeDataText36, SqlDbType.NVarChar, dto.Text36);
                    CreateParameter(command, FieldNames.CustomAttributeDataText37, SqlDbType.NVarChar, dto.Text37);
                    CreateParameter(command, FieldNames.CustomAttributeDataText38, SqlDbType.NVarChar, dto.Text38);
                    CreateParameter(command, FieldNames.CustomAttributeDataText39, SqlDbType.NVarChar, dto.Text39);
                    CreateParameter(command, FieldNames.CustomAttributeDataText40, SqlDbType.NVarChar, dto.Text40);
                    CreateParameter(command, FieldNames.CustomAttributeDataText41, SqlDbType.NVarChar, dto.Text41);
                    CreateParameter(command, FieldNames.CustomAttributeDataText42, SqlDbType.NVarChar, dto.Text42);
                    CreateParameter(command, FieldNames.CustomAttributeDataText43, SqlDbType.NVarChar, dto.Text43);
                    CreateParameter(command, FieldNames.CustomAttributeDataText44, SqlDbType.NVarChar, dto.Text44);
                    CreateParameter(command, FieldNames.CustomAttributeDataText45, SqlDbType.NVarChar, dto.Text45);
                    CreateParameter(command, FieldNames.CustomAttributeDataText46, SqlDbType.NVarChar, dto.Text46);
                    CreateParameter(command, FieldNames.CustomAttributeDataText47, SqlDbType.NVarChar, dto.Text47);
                    CreateParameter(command, FieldNames.CustomAttributeDataText48, SqlDbType.NVarChar, dto.Text48);
                    CreateParameter(command, FieldNames.CustomAttributeDataText49, SqlDbType.NVarChar, dto.Text49);
                    CreateParameter(command, FieldNames.CustomAttributeDataText50, SqlDbType.NVarChar, dto.Text50);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue1, SqlDbType.Real, dto.Value1);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue2, SqlDbType.Real, dto.Value2);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue3, SqlDbType.Real, dto.Value3);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue4, SqlDbType.Real, dto.Value4);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue5, SqlDbType.Real, dto.Value5);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue6, SqlDbType.Real, dto.Value6);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue7, SqlDbType.Real, dto.Value7);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue8, SqlDbType.Real, dto.Value8);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue9, SqlDbType.Real, dto.Value9);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue10, SqlDbType.Real, dto.Value10);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue11, SqlDbType.Real, dto.Value11);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue12, SqlDbType.Real, dto.Value12);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue13, SqlDbType.Real, dto.Value13);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue14, SqlDbType.Real, dto.Value14);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue15, SqlDbType.Real, dto.Value15);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue16, SqlDbType.Real, dto.Value16);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue17, SqlDbType.Real, dto.Value17);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue18, SqlDbType.Real, dto.Value18);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue19, SqlDbType.Real, dto.Value19);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue20, SqlDbType.Real, dto.Value20);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue21, SqlDbType.Real, dto.Value21);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue22, SqlDbType.Real, dto.Value22);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue23, SqlDbType.Real, dto.Value23);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue24, SqlDbType.Real, dto.Value24);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue25, SqlDbType.Real, dto.Value25);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue26, SqlDbType.Real, dto.Value26);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue27, SqlDbType.Real, dto.Value27);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue28, SqlDbType.Real, dto.Value28);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue29, SqlDbType.Real, dto.Value29);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue30, SqlDbType.Real, dto.Value30);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue31, SqlDbType.Real, dto.Value31);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue32, SqlDbType.Real, dto.Value32);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue33, SqlDbType.Real, dto.Value33);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue34, SqlDbType.Real, dto.Value34);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue35, SqlDbType.Real, dto.Value35);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue36, SqlDbType.Real, dto.Value36);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue37, SqlDbType.Real, dto.Value37);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue38, SqlDbType.Real, dto.Value38);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue39, SqlDbType.Real, dto.Value39);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue40, SqlDbType.Real, dto.Value40);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue41, SqlDbType.Real, dto.Value41);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue42, SqlDbType.Real, dto.Value42);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue43, SqlDbType.Real, dto.Value43);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue44, SqlDbType.Real, dto.Value44);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue45, SqlDbType.Real, dto.Value45);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue46, SqlDbType.Real, dto.Value46);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue47, SqlDbType.Real, dto.Value47);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue48, SqlDbType.Real, dto.Value48);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue49, SqlDbType.Real, dto.Value49);
                    CreateParameter(command, FieldNames.CustomAttributeDataValue50, SqlDbType.Real, dto.Value50);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag1, SqlDbType.Bit, dto.Flag1);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag2, SqlDbType.Bit, dto.Flag2);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag3, SqlDbType.Bit, dto.Flag3);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag4, SqlDbType.Bit, dto.Flag4);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag5, SqlDbType.Bit, dto.Flag5);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag6, SqlDbType.Bit, dto.Flag6);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag7, SqlDbType.Bit, dto.Flag7);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag8, SqlDbType.Bit, dto.Flag8);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag9, SqlDbType.Bit, dto.Flag9);
                    CreateParameter(command, FieldNames.CustomAttributeDataFlag10, SqlDbType.Bit, dto.Flag10);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate1, SqlDbType.SmallDateTime, dto.Date1);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate2, SqlDbType.SmallDateTime, dto.Date2);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate3, SqlDbType.SmallDateTime, dto.Date3);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate4, SqlDbType.SmallDateTime, dto.Date4);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate5, SqlDbType.SmallDateTime, dto.Date5);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate6, SqlDbType.SmallDateTime, dto.Date6);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate7, SqlDbType.SmallDateTime, dto.Date7);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate8, SqlDbType.SmallDateTime, dto.Date8);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate9, SqlDbType.SmallDateTime, dto.Date9);
                    CreateParameter(command, FieldNames.CustomAttributeDataDate10, SqlDbType.SmallDateTime, dto.Date10);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote1, SqlDbType.NVarChar, dto.Note1);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote2, SqlDbType.NVarChar, dto.Note2);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote3, SqlDbType.NVarChar, dto.Note3);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote4, SqlDbType.NVarChar, dto.Note4);
                    CreateParameter(command, FieldNames.CustomAttributeDataNote5, SqlDbType.NVarChar, dto.Note5);

                    //execute
                    command.ExecuteNonQuery();

                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<CustomAttributeDataDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(_createImportTableSql);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    CustomAttributeDataBulkCopyDataReader dr = new CustomAttributeDataBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<CustomAttributeDataDto> dtoList, CustomAttributeDataIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new CustomAttributeDataIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<CustomAttributeDataDtoKey, CustomAttributeDataDto> keyedDtoList = new Dictionary<CustomAttributeDataDtoKey, CustomAttributeDataDto>();
            foreach (CustomAttributeDataDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
                if (dto.Id == null) dto.Id = 0;
            }

            try
            {
                CreateTempImportTable(_createImportTableSql);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    CustomAttributeDataBulkCopyDataReader dr = new CustomAttributeDataBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<CustomAttributeDataDtoKey, Int32> ids = new Dictionary<CustomAttributeDataDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.CustomAttributeDataSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.CustomAttributeDataId]);

                            CustomAttributeDataDtoKey key =
                                new CustomAttributeDataDtoKey()
                                {
                                    ParentId = (Int32)GetValue(dr[FieldNames.CustomAttributeDataParentId]),
                                    ParentType = (Byte)GetValue(dr[FieldNames.CustomAttributeDataParentType]),
                                };
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (CustomAttributeDataDto dto in dtoList)
                {
                    Int32 outputData;
                    if (ids.TryGetValue(dto.DtoKey, out outputData))
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private String GenerateSetPropertiesString(CustomAttributeDataIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsParentIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataParentId);
            }
            if (isSetDto.IsParentTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataParentType);
            }
            if (isSetDto.IsText1Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText1);
            }
            if (isSetDto.IsText2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText2);
            }
            if (isSetDto.IsText3Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText3);
            }
            if (isSetDto.IsText4Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText4);
            }
            if (isSetDto.IsText5Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText5);
            }
            if (isSetDto.IsText6Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText6);
            }
            if (isSetDto.IsText7Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText7);
            }
            if (isSetDto.IsText8Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText8);
            }
            if (isSetDto.IsText9Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText9);
            }
            if (isSetDto.IsText10Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText10);
            }
            if (isSetDto.IsText11Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText11);
            }
            if (isSetDto.IsText12Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText12);
            }
            if (isSetDto.IsText13Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText13);
            }
            if (isSetDto.IsText14Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText14);
            }
            if (isSetDto.IsText15Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText15);
            }
            if (isSetDto.IsText16Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText16);
            }
            if (isSetDto.IsText17Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText17);
            }
            if (isSetDto.IsText18Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText18);
            }
            if (isSetDto.IsText19Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText19);
            }
            if (isSetDto.IsText20Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText20);
            }
            if (isSetDto.IsText21Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText21);
            }
            if (isSetDto.IsText22Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText22);
            }
            if (isSetDto.IsText23Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText23);
            }
            if (isSetDto.IsText24Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText24);
            }
            if (isSetDto.IsText25Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText25);
            }
            if (isSetDto.IsText26Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText26);
            }
            if (isSetDto.IsText27Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText27);
            }
            if (isSetDto.IsText28Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText28);
            }
            if (isSetDto.IsText29Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText29);
            }
            if (isSetDto.IsText30Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText30);
            }
            if (isSetDto.IsText31Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText31);
            }
            if (isSetDto.IsText32Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText32);
            }
            if (isSetDto.IsText33Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText33);
            }
            if (isSetDto.IsText34Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText34);
            }
            if (isSetDto.IsText35Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText35);
            }
            if (isSetDto.IsText36Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText36);
            }
            if (isSetDto.IsText37Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText37);
            }
            if (isSetDto.IsText38Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText38);
            }
            if (isSetDto.IsText39Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText39);
            }
            if (isSetDto.IsText40Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText40);
            }
            if (isSetDto.IsText41Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText41);
            }
            if (isSetDto.IsText42Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText42);
            }
            if (isSetDto.IsText43Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText43);
            }
            if (isSetDto.IsText44Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText44);
            }
            if (isSetDto.IsText45Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText45);
            }
            if (isSetDto.IsText46Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText46);
            }
            if (isSetDto.IsText47Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText47);
            }
            if (isSetDto.IsText48Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText48);
            }
            if (isSetDto.IsText49Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText49);
            }
            if (isSetDto.IsText50Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataText50);
            }
            if (isSetDto.IsValue1Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue1);
            }
            if (isSetDto.IsValue2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue2);
            }
            if (isSetDto.IsValue3Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue3);
            }
            if (isSetDto.IsValue4Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue4);
            }
            if (isSetDto.IsValue5Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue5);
            }
            if (isSetDto.IsValue6Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue6);
            }
            if (isSetDto.IsValue7Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue7);
            }
            if (isSetDto.IsValue8Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue8);
            }
            if (isSetDto.IsValue9Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue9);
            }
            if (isSetDto.IsValue10Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue10);
            }
            if (isSetDto.IsValue11Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue11);
            }
            if (isSetDto.IsValue12Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue12);
            }
            if (isSetDto.IsValue13Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue13);
            }
            if (isSetDto.IsValue14Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue14);
            }
            if (isSetDto.IsValue15Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue15);
            }
            if (isSetDto.IsValue16Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue16);
            }
            if (isSetDto.IsValue17Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue17);
            }
            if (isSetDto.IsValue18Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue18);
            }
            if (isSetDto.IsValue19Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue19);
            }
            if (isSetDto.IsValue20Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue20);
            }
            if (isSetDto.IsValue21Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue21);
            }
            if (isSetDto.IsValue22Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue22);
            }
            if (isSetDto.IsValue23Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue23);
            }
            if (isSetDto.IsValue24Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue24);
            }
            if (isSetDto.IsValue25Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue25);
            }
            if (isSetDto.IsValue26Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue26);
            }
            if (isSetDto.IsValue27Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue27);
            }
            if (isSetDto.IsValue28Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue28);
            }
            if (isSetDto.IsValue29Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue29);
            }
            if (isSetDto.IsValue30Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue30);
            }
            if (isSetDto.IsValue31Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue31);
            }
            if (isSetDto.IsValue32Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue32);
            }
            if (isSetDto.IsValue33Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue33);
            }
            if (isSetDto.IsValue34Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue34);
            }
            if (isSetDto.IsValue35Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue35);
            }
            if (isSetDto.IsValue36Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue36);
            }
            if (isSetDto.IsValue37Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue37);
            }
            if (isSetDto.IsValue38Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue38);
            }
            if (isSetDto.IsValue39Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue39);
            }
            if (isSetDto.IsValue40Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue40);
            }
            if (isSetDto.IsValue41Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue41);
            }
            if (isSetDto.IsValue42Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue42);
            }
            if (isSetDto.IsValue43Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue43);
            }
            if (isSetDto.IsValue44Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue44);
            }
            if (isSetDto.IsValue45Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue45);
            }
            if (isSetDto.IsValue46Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue46);
            }
            if (isSetDto.IsValue47Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue47);
            }
            if (isSetDto.IsValue48Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue48);
            }
            if (isSetDto.IsValue49Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue49);
            }
            if (isSetDto.IsValue50Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataValue50);
            }
            if (isSetDto.IsFlag1Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag1);
            }
            if (isSetDto.IsFlag2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag2);
            }
            if (isSetDto.IsFlag3Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag3);
            }
            if (isSetDto.IsFlag4Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag4);
            }
            if (isSetDto.IsFlag5Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag5);
            }
            if (isSetDto.IsFlag6Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag6);
            }
            if (isSetDto.IsFlag7Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag7);
            }
            if (isSetDto.IsFlag8Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag8);
            }
            if (isSetDto.IsFlag9Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag9);
            }
            if (isSetDto.IsFlag10Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataFlag10);
            }
            if (isSetDto.IsDate1Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate1);
            }
            if (isSetDto.IsDate2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate2);
            }
            if (isSetDto.IsDate3Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate3);
            }
            if (isSetDto.IsDate4Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate4);
            }
            if (isSetDto.IsDate5Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate5);
            }
            if (isSetDto.IsDate6Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate6);
            }
            if (isSetDto.IsDate7Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate7);
            }
            if (isSetDto.IsDate8Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate8);
            }
            if (isSetDto.IsDate9Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate9);
            }
            if (isSetDto.IsDate10Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataDate10);
            }
            if (isSetDto.IsNote1Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataNote1);
            }
            if (isSetDto.IsNote2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataNote2);
            }
            if (isSetDto.IsNote3Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataNote3);
            }
            if (isSetDto.IsNote4Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataNote4);
            }
            if (isSetDto.IsNote5Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.CustomAttributeDataNote5);
            }

            return setProperties;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CustomAttributeDataDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.CustomAttributeDataId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(CustomAttributeDataDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<CustomAttributeDataDto> dtos)
        {
            foreach (CustomAttributeDataDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String commandText)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, commandText, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}