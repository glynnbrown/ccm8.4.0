﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MSSQL implementation of IPrintTemplateComponentDal
    /// </summary>
    public sealed class PrintTemplateComponentDal : Galleria.Framework.Dal.Mssql.DalBase, IPrintTemplateComponentDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PrintTemplateComponentDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PrintTemplateComponentDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PrintTemplateComponentId]),
                PrintTemplateSectionId = (Int32)GetValue(dr[FieldNames.PrintTemplateComponentPrintTemplateSectionId]),
                Type = (Byte)GetValue(dr[FieldNames.PrintTemplateComponentType]),
                X = (Single)GetValue(dr[FieldNames.PrintTemplateComponentX]),
                Y = (Single)GetValue(dr[FieldNames.PrintTemplateComponentY]),
                Height = (Single)GetValue(dr[FieldNames.PrintTemplateComponentHeight]),
                Width = (Single)GetValue(dr[FieldNames.PrintTemplateComponentWidth]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PrintTemplateComponentDto FetchById(Int32 id)
        {
            PrintTemplateComponentDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateComponentId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }



        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PrintTemplateComponentDto> FetchByPrintTemplateSectionId(Int32 printTemplateSectionId)
        {
            List<PrintTemplateComponentDto> dtoList = new List<PrintTemplateComponentDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentFetchByPrintTemplateSectionId))
                {
                    CreateParameter(command, FieldNames.PrintTemplateComponentPrintTemplateSectionId, SqlDbType.Int, printTemplateSectionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PrintTemplateComponentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PrintTemplateComponentId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateComponentPrintTemplateSectionId, SqlDbType.Int, dto.PrintTemplateSectionId);
                    CreateParameter(command, FieldNames.PrintTemplateComponentType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.PrintTemplateComponentX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PrintTemplateComponentY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PrintTemplateComponentHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.PrintTemplateComponentWidth, SqlDbType.Real, dto.Width);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PrintTemplateComponentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateComponentId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateComponentPrintTemplateSectionId, SqlDbType.Int, dto.PrintTemplateSectionId);
                    CreateParameter(command, FieldNames.PrintTemplateComponentType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.PrintTemplateComponentX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PrintTemplateComponentY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PrintTemplateComponentHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.PrintTemplateComponentWidth, SqlDbType.Real, dto.Width);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateComponentId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}