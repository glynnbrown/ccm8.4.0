﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.0.2
// V8-29010 : D.Pleasance
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramNameTemplateInfoDal
    /// </summary>
    public class PlanogramNameTemplateInfoDal : DalBase, IPlanogramNameTemplateInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramNameTemplateInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramNameTemplateInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramNameTemplateId]),
                EntityId = (Int32)GetValue(dr[FieldNames.EntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramNameTemplateName])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramNameTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<PlanogramNameTemplateInfoDto> dtoList = new List<PlanogramNameTemplateInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramNameTemplateInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        #endregion
    }
}