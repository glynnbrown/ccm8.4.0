﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class CompressionDal : Galleria.Framework.Dal.Mssql.DalBase, ICompressionDal
    {
        #region DataTransferObject

        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private CompressionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new CompressionDto
            {
                Id = (Int32)GetValue(dr[FieldNames.CompressionId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.CompressionRowVersion])),
                Name = (String)GetValue(dr[FieldNames.CompressionName]),
                Width = (Int32)GetValue(dr[FieldNames.CompressionWidth]),
                Height = (Int32)GetValue(dr[FieldNames.CompressionHeight]),
                ColourDepth = (Int32)GetValue(dr[FieldNames.CompressionColourDepth]),
                MaintainAspectRatio = (Boolean)GetValue(dr[FieldNames.CompressionMaintainAspectRatio]),
                Enabled = (Boolean)GetValue(dr[FieldNames.CompressionEnabled]),
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Return all Compression types
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public List<CompressionDto> FetchAll()
        {
            List<CompressionDto> dtoList = new List<CompressionDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CompressionFetchAll))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        /// <summary>
        /// Return all Compression types that are enabled
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public List<CompressionDto> FetchAllEnabled()
        {
            List<CompressionDto> dtoList = new List<CompressionDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CompressionFetchAllEnabled))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        /// <summary>
        /// Returns a Compressiondto from the database
        /// </summary>
        /// <param name="id">The Compression id</param>
        /// <returns>A Compression dto</returns>
        public CompressionDto FetchById(Int32 id)
        {
            CompressionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CompressionFetchById))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.CompressionId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(CompressionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CompressionInsert))
                {
                    // id parameter
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.CompressionId,
                        SqlDbType.Int);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.CompressionRowVersion,
                        SqlDbType.Timestamp);

                    // Compression name
                    CreateParameter(command,
                        FieldNames.CompressionName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // Compression width
                    CreateParameter(command,
                        FieldNames.CompressionWidth,
                        SqlDbType.Int,
                        dto.Width);

                    // Compression Height
                    CreateParameter(command,
                        FieldNames.CompressionHeight,
                        SqlDbType.Int,
                        dto.Height);

                    // Compression Colour Depth
                    CreateParameter(command,
                        FieldNames.CompressionColourDepth,
                        SqlDbType.Int,
                        dto.ColourDepth);

                    // Compression Maintain Aspect Ratio
                    CreateParameter(command,
                        FieldNames.CompressionMaintainAspectRatio,
                        SqlDbType.Bit,
                        dto.MaintainAspectRatio);

                    // Compression Enabled
                    CreateParameter(command,
                        FieldNames.CompressionEnabled,
                        SqlDbType.Bit,
                        dto.Enabled);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(CompressionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CompressionUpdate))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.CompressionId,
                        SqlDbType.Int,
                        dto.Id);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.CompressionRowVersion,
                        SqlDbType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);

                    // Compression name
                    CreateParameter(command,
                        FieldNames.CompressionName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // Compression width
                    CreateParameter(command,
                        FieldNames.CompressionWidth,
                        SqlDbType.Int,
                        dto.Width);

                    // Compression Height
                    CreateParameter(command,
                        FieldNames.CompressionHeight,
                        SqlDbType.Int,
                        dto.Height);

                    // Compression Colour Depth
                    CreateParameter(command,
                        FieldNames.CompressionColourDepth,
                        SqlDbType.Int,
                        dto.ColourDepth);

                    // Compression Maintain Aspect Ratio
                    CreateParameter(command,
                        FieldNames.CompressionMaintainAspectRatio,
                        SqlDbType.Bit,
                        dto.MaintainAspectRatio);

                    // Compression Enabled
                    CreateParameter(command,
                        FieldNames.CompressionEnabled,
                        SqlDbType.Bit,
                        dto.Enabled);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.CompressionDeleteById))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.CompressionId,
                        SqlDbType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
