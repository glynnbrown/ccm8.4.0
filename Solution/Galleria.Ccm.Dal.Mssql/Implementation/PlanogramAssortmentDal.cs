﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramAssortmentDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssortmentDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssortment DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssortmentDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssortmentDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentPlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramAssortmentName])
            };
        }
        #endregion  

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssortment DTOs with a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramAssortment DTOs.</returns>
        public PlanogramAssortmentDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramAssortmentDto dto = null;
            var dtoList = new List<PlanogramAssortmentDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentFetchByPlanogramId))
                {
                    // parameters
                    CreateParameter(command,FieldNames.PlanogramAssortmentPlanogramId,SqlDbType.Int,(Int32)planogramId);

                    // execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssortmentDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssortmentId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramAssortmentPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentName, SqlDbType.NVarChar, dto.Name);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentDto> dtos)
        {
            foreach (PlanogramAssortmentDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssortmentDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramAssortmentPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentName, SqlDbType.NVarChar, dto.Name);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentDto> dtos)
        {
            foreach (PlanogramAssortmentDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentDto> dtos)
        {
            foreach (PlanogramAssortmentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
