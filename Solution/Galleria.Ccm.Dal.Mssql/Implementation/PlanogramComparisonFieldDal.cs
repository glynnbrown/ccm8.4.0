﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31963 : A.Silva
//  Added Display field.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramComparisonFieldDal : Framework.Dal.Mssql.DalBase, IPlanogramComparisonFieldDal
    {
        #region Constants
        private const String ImportTableName = "#tmpPlanogramComparisonField";
        private const String DropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String CreateImportTableSql =
            "CREATE TABLE [{0}](" +
            "[PlanogramComparisonField_Id] [INT] NOT NULL," +
            "[PlanogramComparison_Id] [INT] NOT NULL, " +
            "[PlanogramComparisonField_ItemType] [TINYINT] NOT NULL, " +
            "[PlanogramComparisonField_FieldPlaceholder] [NVARCHAR](100) COLLATE database_default NOT NULL," +
            "[PlanogramComparisonField_DisplayName] [NVARCHAR](100) COLLATE database_default NULL, " +
            "[PlanogramComparisonField_Number] [SMALLINT] NOT NULL," +
            "[PlanogramComparisonField_Display] [BIT] NOT NULL" +
            ")";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramComparisonFieldBulkCopyDataReader : IDataReader
        {
            #region Constants

            private const Int32 IdColumnNumber = 0;
            private const Int32 PlanogramComparisonIdColumnNumber = 1;
            private const Int32 ItemTypeColumnNumber = 2;
            private const Int32 FieldPlaceholderColumnNumber = 3;
            private const Int32 DisplayNameColumnNumber = 4;
            private const Int32 NumberColumnNumber = 5;
            private const Int32 DisplayColumnNumber = 6;
            private const Int32 ColumnCount = 7;

            #endregion

            #region Fields
            private readonly IEnumerator<PlanogramComparisonFieldDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramComparisonFieldBulkCopyDataReader(IEnumerable<PlanogramComparisonFieldDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return ColumnCount; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public Object GetValue(Int32 i)
            {
                Object value;
                switch (i)
                {
                    case IdColumnNumber:
                        value = _enumerator.Current.Id;
                        break;
                    case PlanogramComparisonIdColumnNumber:
                        value = _enumerator.Current.PlanogramComparisonId;
                        break;
                    case ItemTypeColumnNumber:
                        value = _enumerator.Current.ItemType;
                        break;
                    case FieldPlaceholderColumnNumber:
                        value = _enumerator.Current.FieldPlaceholder;
                        break;
                    case DisplayNameColumnNumber:
                        value = _enumerator.Current.DisplayName;
                        break;
                    case NumberColumnNumber:
                        value = _enumerator.Current.Number;
                        break;
                    case DisplayColumnNumber:
                        value = _enumerator.Current.Display;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                return value ?? DBNull.Value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public Int32 Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public Boolean IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public Boolean NextResult()
            {
                throw new NotImplementedException();
            }

            public Int32 RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public Boolean GetBoolean(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Byte GetByte(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetBytes(Int32 i, Int64 fieldOffset, Byte[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public Char GetChar(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetChars(Int32 i, Int64 fieldoffset, Char[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetDataTypeName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Decimal GetDecimal(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Double GetDouble(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Single GetFloat(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int16 GetInt16(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetInt32(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetInt64(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetOrdinal(String name)
            {
                throw new NotImplementedException();
            }

            public String GetString(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetValues(Object[] values)
            {
                throw new NotImplementedException();
            }

            public Boolean IsDBNull(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Object this[String name]
            {
                get { throw new NotImplementedException(); }
            }

            public Object this[Int32 i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Create data transfer object with the data from the given <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="dr">The <see cref="SqlDataReader"/> instance from which to load data.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonFieldDto"/> with the required values.</returns>
        public PlanogramComparisonFieldDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramComparisonFieldDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramComparisonFieldId]),
                PlanogramComparisonId = (Int32)GetValue(dr[FieldNames.PlanogramComparisonFieldPlanogramComparisonId]),
                ItemType = (Byte)GetValue(dr[FieldNames.PlanogramComparisonFieldItemType]),
                FieldPlaceholder = (String)GetValue(dr[FieldNames.PlanogramComparisonFieldFieldPlaceholder]),
                DisplayName = (String)GetValue(dr[FieldNames.PlanogramComparisonFieldDisplayName]),
                Number = (Int16)GetValue(dr[FieldNames.PlanogramComparisonFieldNumber]),
                Display = (Boolean)GetValue(dr[FieldNames.PlanogramComparisonFieldDisplay])
            };
        }
        #endregion
        
        #region Fetch

        /// <summary>
        ///     Create a collection of data transfer object with the data assigned to the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> containing the Id of the parent Planogram Comparison.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonFieldDto"/> with the required values.</returns>
        public IEnumerable<PlanogramComparisonFieldDto> FetchByPlanogramComparisonId(Object id)
        {
            var dtoList = new List<PlanogramComparisonFieldDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonFieldFetchByPlanogramComparisonId))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonFieldPlanogramComparisonId, SqlDbType.Int, (Int32) id);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the values in the given <paramref name="dto"/> into the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldDto"/> instance containing the values.</param>
        public void Insert(PlanogramComparisonFieldDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonFieldInsert))
                {
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramComparisonFieldId, SqlDbType.Int);
                    CreateOtherParameters(dto, command);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        private void CreateOtherParameters(PlanogramComparisonFieldDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramComparisonFieldPlanogramComparisonId, SqlDbType.Int, dto.PlanogramComparisonId);
            CreateParameter(command, FieldNames.PlanogramComparisonFieldItemType, SqlDbType.TinyInt, dto.ItemType);
            CreateParameter(command, FieldNames.PlanogramComparisonFieldFieldPlaceholder, SqlDbType.NVarChar, dto.FieldPlaceholder);
            CreateParameter(command, FieldNames.PlanogramComparisonFieldDisplayName, SqlDbType.NVarChar, dto.DisplayName);
            CreateParameter(command, FieldNames.PlanogramComparisonFieldNumber, SqlDbType.SmallInt, dto.Number);
            CreateParameter(command, FieldNames.PlanogramComparisonFieldDisplay, SqlDbType.Bit, dto.Display);
        }

        /// <summary>
        ///     Insert the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonFieldDto"/> instances containing the values.</param>
        public void Insert(IEnumerable<PlanogramComparisonFieldDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            var index = new Dictionary<Object, PlanogramComparisonFieldDto>();
            IList<PlanogramComparisonFieldDto> fieldDtos = dtos as IList<PlanogramComparisonFieldDto> ?? dtos.ToList();
            foreach (PlanogramComparisonFieldDto dto in fieldDtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(ImportTableName);

                // perform a batch insert to temp table
                using (var bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = CommandTimeout;
                    bulkCopy.DestinationTableName = String.Format("[{0}]", ImportTableName);
                    var dr = new PlanogramComparisonFieldBulkCopyDataReader(fieldDtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonFieldBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(ImportTableName);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the values in the given <paramref name="dto"/> into the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldDto"/> instance containing the values.</param>
        public void Update(PlanogramComparisonFieldDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonFieldUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonFieldId, SqlDbType.Int, dto.Id);
                    CreateOtherParameters(dto, command);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Update the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonFieldDto"/> instances containing the values.</param>
        public void Update(IEnumerable<PlanogramComparisonFieldDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(ImportTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = CommandTimeout;
                    bulkCopy.DestinationTableName = String.Format("[{0}]", ImportTableName);
                    PlanogramComparisonFieldBulkCopyDataReader dr = new PlanogramComparisonFieldBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonFieldBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(ImportTableName);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the values assigned to the given <paramref name="id"/> from the data base.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> contaning the ID assigned to the values.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonFieldDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonFieldId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Delete the values in the given <paramref name="dto"/> from the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldDto"/> contaning the values.</param>
        public void Delete(PlanogramComparisonFieldDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Delete the values in the given <paramref name="dtos"/> from the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonFieldDto"/> contaning the values.</param>
        public void Delete(IEnumerable<PlanogramComparisonFieldDto> dtos)
        {
            foreach (PlanogramComparisonFieldDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, CreateImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, DropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}