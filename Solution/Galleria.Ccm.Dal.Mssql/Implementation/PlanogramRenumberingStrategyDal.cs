﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27153 : A.Silva
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramRenumberingStrategyDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramRenumberingStrategyDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Gets the values from the given <paramref name="dr"/> into a new instance of <see cref="PlanogramRenumberingStrategyDto"/>.
        /// </summary>
        /// <param name="dr">Instance of <see cref="SqlDataReader"/> to retrieve the records values from.</param>
        /// <returns>An instance of <see cref="PlanogramRenumberingStrategyDto"/>.</returns>
        private static PlanogramRenumberingStrategyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramRenumberingStrategyDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramRenumberingStrategyId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramRenumberingStrategyPlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramRenumberingStrategyName]),
                BayComponentXRenumberStrategyPriority = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority]),
                BayComponentYRenumberStrategyPriority = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority]),
                BayComponentZRenumberStrategyPriority = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority]),
                PositionXRenumberStrategyPriority = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategyPriority]),
                PositionYRenumberStrategyPriority = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategyPriority]),
                PositionZRenumberStrategyPriority = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategyPriority]),
                BayComponentXRenumberStrategy = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategy]),
                BayComponentYRenumberStrategy = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategy]),
                BayComponentZRenumberStrategy = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategy]),
                PositionXRenumberStrategy = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategy]),
                PositionYRenumberStrategy = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategy]),
                PositionZRenumberStrategy = (Byte)GetValue(dr[FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategy]),
                RestartComponentRenumberingPerBay = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerBay]),
                IgnoreNonMerchandisingComponents = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents]),
                RestartPositionRenumberingPerComponent = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent]),
                UniqueNumberMultiPositionProductsPerComponent = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent]),
                ExceptAdjacentPositions = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyExceptAdjacentPositions]),
                IsEnabled = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyIsEnabled]),
                RestartPositionRenumberingPerBay = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerBay]),
                RestartComponentRenumberingPerComponentType = (Boolean)GetValue(dr[FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerComponentType]),
            };
        }

        private void CreateInsertUpdateParametersFromDto(PlanogramRenumberingStrategyDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPlanogramId, SqlDbType.Int, dto.PlanogramId);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority,
                SqlDbType.TinyInt, dto.BayComponentXRenumberStrategyPriority);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority,
                SqlDbType.TinyInt, dto.BayComponentYRenumberStrategyPriority);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority,
                SqlDbType.TinyInt, dto.BayComponentZRenumberStrategyPriority);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategyPriority,
                SqlDbType.TinyInt, dto.PositionXRenumberStrategyPriority);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategyPriority,
                SqlDbType.TinyInt, dto.PositionYRenumberStrategyPriority);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategyPriority,
                SqlDbType.TinyInt, dto.PositionZRenumberStrategyPriority);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyBayComponentXRenumberStrategy,
                SqlDbType.TinyInt, dto.BayComponentXRenumberStrategy);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyBayComponentYRenumberStrategy,
                SqlDbType.TinyInt, dto.BayComponentYRenumberStrategy);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyBayComponentZRenumberStrategy,
                SqlDbType.TinyInt, dto.BayComponentZRenumberStrategy);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPositionXRenumberStrategy,
                SqlDbType.TinyInt, dto.PositionXRenumberStrategy);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPositionYRenumberStrategy,
                SqlDbType.TinyInt, dto.PositionYRenumberStrategy);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPositionZRenumberStrategy,
                SqlDbType.TinyInt, dto.PositionZRenumberStrategy);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerBay,
                SqlDbType.Bit, dto.RestartComponentRenumberingPerBay);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents,
                SqlDbType.Bit, dto.IgnoreNonMerchandisingComponents);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent,
                SqlDbType.Bit, dto.RestartPositionRenumberingPerComponent);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent,
                SqlDbType.Bit, dto.UniqueNumberMultiPositionProductsPerComponent);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyExceptAdjacentPositions, SqlDbType.Bit, dto.ExceptAdjacentPositions);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyIsEnabled, SqlDbType.Bit, dto.IsEnabled);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyRestartPositionRenumberingPerBay,
                SqlDbType.Bit, dto.RestartPositionRenumberingPerBay);
            CreateParameter(command, FieldNames.PlanogramRenumberingStrategyRestartComponentRenumberingPerComponentType,
                SqlDbType.Bit, dto.RestartComponentRenumberingPerComponentType);
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Fetches all the <see cref="PlanogramRenumberingStrategyDto"/> for a given <paramref name="planogramId"/>.
        /// </summary>
        /// <param name="planogramId">Unique identifier of the planogram to retrieve items from.</param>
        /// <returns>A new collection of <see cref="PlanogramRenumberingStrategyDto"/> instances that match the given <paramref name="planogramId"/>.</returns>
        public PlanogramRenumberingStrategyDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramRenumberingStrategyDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramRenumberingStrategyFetchByPlanogramId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramRenumberingStrategyPlanogramId, SqlDbType.Int, planogramId);

                    // execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a planogram with the values from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramRenumberingStrategyDto"/> containing the values to insert.</param>
        public void Insert(PlanogramRenumberingStrategyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramRenumberingStrategyInsert))
                {
                    // Id parameter
                    var idParameter = CreateParameter(command, FieldNames.PlanogramRenumberingStrategyId, SqlDbType.Int);

                    // Other property parameters
                    CreateInsertUpdateParametersFromDto(dto, command);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            foreach (PlanogramRenumberingStrategyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates a planogram with the values from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramRenumberingStrategyDto"/> containing the values to update.</param>
        public void Update(PlanogramRenumberingStrategyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramRenumberingStrategyUpdateById))
                {
                    // Id parameter
                    CreateParameter(command, FieldNames.PlanogramRenumberingStrategyId, SqlDbType.Int, dto.Id);

                    // Other property parameters
                    CreateInsertUpdateParametersFromDto(dto, command);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            foreach (PlanogramRenumberingStrategyDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the planogram renumbering strategy matching the given <paramref name="id"/> from the planogram.
        /// </summary>
        /// <param name="id">Unique id for the planogram renumbering strategy to be removed.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramRenumberingStrategyDeleteById))
                {
                    // Id parameter.
                    CreateParameter(command, FieldNames.PlanogramRenumberingStrategyId, SqlDbType.Int, id);

                    // Execute.
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramRenumberingStrategyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            foreach (PlanogramRenumberingStrategyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
