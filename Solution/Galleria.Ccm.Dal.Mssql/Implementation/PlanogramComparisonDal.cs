﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts.
// V8-32005 : A.Silva
//  Added Name, Description and DataOrderType.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     MSSQL implementation of the <see cref="IPlanogramComparisonDal"/>.
    /// </summary>
    public class PlanogramComparisonDal : Framework.Dal.Mssql.DalBase, IPlanogramComparisonDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Create data transfer object with the data from the given <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="dr">The <see cref="SqlDataReader"/> instance from which to load data.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonDto"/> with the required values.</returns>
        private static PlanogramComparisonDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramComparisonDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramComparisonId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramComparisonPlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramComparisonName]),
                Description = (String)GetValue(dr[FieldNames.PlanogramComparisonDescription]),
                DateLastCompared = (DateTime?)GetValue(dr[FieldNames.PlanogramComparisonDateLastCompared]),
                IgnoreNonPlacedProducts = (Boolean)GetValue(dr[FieldNames.PlanogramComparisonIgnoreNonPlacedProducts]),
                DataOrderType = (Byte)GetValue(dr[FieldNames.PlanogramComparisonDataOrderType])
            };
        }

        #endregion  

        #region Fetch

        /// <summary>
        ///     Create a data transfer object with the data assigned to the given <paramref name="planogramId"/>.
        /// </summary>
        /// <param name="planogramId">The <see cref="Object"/> containing the Id of the parent Planogram.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonDto"/> with the required values.</returns>
        public PlanogramComparisonDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramComparisonDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonPlanogramId, SqlDbType.Int, (Int32) planogramId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the values in the given <paramref name="dto"/> into the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonDto"/> instance containing the values.</param>
        public void Insert(PlanogramComparisonDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonInsert))
                {
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramComparisonId, SqlDbType.Int);
                    CreateOtherParameters(dto, command);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Insert the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> instances containing the values.</param>
        public void Insert(IEnumerable<PlanogramComparisonDto> dtos)
        {
            foreach (PlanogramComparisonDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the values in the given <paramref name="dto"/> into the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonDto"/> instance containing the values.</param>
        public void Update(PlanogramComparisonDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonId, SqlDbType.Int, dto.Id);
                    CreateOtherParameters(dto, command);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Update the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> instances containing the values.</param>
        public void Update(IEnumerable<PlanogramComparisonDto> dtos)
        {
            foreach (PlanogramComparisonDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the values assigned to the given <paramref name="id"/> from the data base.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> contaning the ID assigned to the values.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonId, SqlDbType.Int, (Int32)id);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Delete the values in the given <paramref name="dto"/> from the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonDto"/> contaning the values.</param>
        public void Delete(PlanogramComparisonDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Delete the values in the given <paramref name="dtos"/> from the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonDto"/> contaning the values.</param>
        public void Delete(IEnumerable<PlanogramComparisonDto> dtos)
        {
            foreach (PlanogramComparisonDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion

        #region Helper Methods

        private void CreateOtherParameters(PlanogramComparisonDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramComparisonPlanogramId, SqlDbType.Int, dto.PlanogramId);
            CreateParameter(command, FieldNames.PlanogramComparisonName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramComparisonDescription, SqlDbType.NVarChar, dto.Description);
            CreateParameter(command, FieldNames.PlanogramComparisonDateLastCompared, SqlDbType.DateTime, dto.DateLastCompared);
            CreateParameter(command, FieldNames.PlanogramComparisonIgnoreNonPlacedProducts, SqlDbType.Bit, dto.IgnoreNonPlacedProducts);
            CreateParameter(command, FieldNames.PlanogramComparisonDataOrderType, SqlDbType.TinyInt, dto.DataOrderType);
        }

        #endregion
    }
}