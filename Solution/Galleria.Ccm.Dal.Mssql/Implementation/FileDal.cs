﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// File Dal
    /// </summary>
    public class FileDal : Galleria.Framework.Dal.Mssql.DalBase, IFileDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public FileDto GetDataTransferObject(SqlDataReader dr)
        {
            return new FileDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.FileId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.FileRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.FileEntityId]),
                Name = (String)GetValue(dr[FieldNames.FileName]),
                SourceFilePath = (String)GetValue(dr[FieldNames.FileSourceFilePath]),
                SizeInBytes = (Int64)GetValue(dr[FieldNames.FileSizeInBytes]),
                BlobId = (Int32)GetValue(dr[FieldNames.FileBlobId]),
                UserId = (Int32?)GetValue(dr[FieldNames.FileUserId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.FileDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.FileDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.FileDateDeleted])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public FileDto FetchById(int id)
        {
            FileDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.FileFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.FileId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(FileDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.FileInsert))
                {
                    SqlParameter idParameter = CreateParameter(command, FieldNames.FileId, SqlDbType.Int);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.FileRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.FileDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.FileDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.FileDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    // entity id
                    CreateParameter(command,
                        FieldNames.FileEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // name
                    CreateParameter(command,
                        FieldNames.FileName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // SourceFilePath
                    CreateParameter(command,
                        FieldNames.FileSourceFilePath,
                        SqlDbType.NVarChar,
                        dto.SourceFilePath);

                    // File Size In Bytes
                    CreateParameter(command,
                        FieldNames.FileSizeInBytes,
                        SqlDbType.BigInt,
                        dto.SizeInBytes);

                    // Data
                    CreateParameter(command,
                        FieldNames.FileBlobId,
                        SqlDbType.Int,
                        dto.BlobId);

                    // File userid
                    CreateParameter(command,
                        FieldNames.FileUserId,
                        SqlDbType.Int,
                        dto.UserId);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the data source
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(FileDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.FileUpdateById))
                {
                    CreateParameter(command, FieldNames.FileId, SqlDbType.Int, dto.Id);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.FileRowVersion, SqlDbType.Timestamp,
                        ParameterDirection.InputOutput, dto.RowVersion);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.FileDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.FileDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.FileDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    // entity id
                    CreateParameter(command,
                        FieldNames.FileEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // name
                    CreateParameter(command,
                        FieldNames.FileName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // SourceFilePath
                    CreateParameter(command,
                        FieldNames.FileSourceFilePath,
                        SqlDbType.NVarChar,
                        dto.SourceFilePath);

                    // File Size In Bytes
                    CreateParameter(command,
                        FieldNames.FileSizeInBytes,
                        SqlDbType.BigInt,
                        dto.SizeInBytes);

                    // Data
                    CreateParameter(command,
                        FieldNames.FileBlobId,
                        SqlDbType.Int,
                        dto.BlobId);

                    // File userid
                    CreateParameter(command,
                        FieldNames.FileUserId,
                        SqlDbType.Int,
                        dto.UserId);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;

                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delte
        /// <summary>
        /// Deletes the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.FileDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.FileId,
                        SqlDbType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}