﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Location Space Element Dal
    /// </summary>
    public class LocationSpaceElementDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationSpaceElementDal
    {
        #region Constants

        private const String _importTableName = "#tmpLocationSpaceElement";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[LocationSpaceBay_Id] [INT], " +
                "[LocationSpaceElement_Order] [TINYINT], " +
                "[LocationSpaceElement_X] [REAL], " +
                "[LocationSpaceElement_Y] [REAL], " +
                "[LocationSpaceElement_Z] [REAL], " +
                "[LocationSpaceElement_MerchandisableHeight] [REAL], " +
                "[LocationSpaceElement_Height] [REAL], " +
                "[LocationSpaceElement_Width] [REAL], " +
                "[LocationSpaceElement_Depth] [REAL], " +
                "[LocationSpaceElement_FaceThickness] [REAL], " +
                "[LocationSpaceElement_NotchNumber] [REAL] " +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public LocationSpaceElementDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationSpaceElementDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationSpaceElementId]),
                LocationSpaceBayId = (Int32)GetValue(dr[FieldNames.LocationSpaceElementLocationSpaceBayId]),
                Order = (Byte)GetValue(dr[FieldNames.LocationSpaceElementOrder]),
                X = (Single)GetValue(dr[FieldNames.LocationSpaceElementX]),
                Y = (Single)GetValue(dr[FieldNames.LocationSpaceElementY]),
                Z = (Single)GetValue(dr[FieldNames.LocationSpaceElementZ]),
                MerchandisableHeight = (Single)GetValue(dr[FieldNames.LocationSpaceElementMerchandisableHeight]),
                Height = (Single)GetValue(dr[FieldNames.LocationSpaceElementHeight]),
                Width = (Single)GetValue(dr[FieldNames.LocationSpaceElementWidth]),
                Depth = (Single)GetValue(dr[FieldNames.LocationSpaceElementDepth]),
                FaceThickness = (Single)GetValue(dr[FieldNames.LocationSpaceElementFaceThickness]),
                NotchNumber = (Nullable<Single>)GetValue(dr[FieldNames.LocationSpaceElementNotchNumber]),
            };
        }
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class LocationSpaceElementBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationSpaceElementDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public LocationSpaceElementBulkCopyDataReader(IEnumerable<LocationSpaceElementDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 11; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.LocationSpaceBayId;
                        break;
                    case 1:
                        value = _enumerator.Current.Order;
                        break;
                    case 2:
                        value = _enumerator.Current.X;
                        break;
                    case 3:
                        value = _enumerator.Current.Y;
                        break;
                    case 4:
                        value = _enumerator.Current.Z;
                        break;
                    case 5:
                        value = _enumerator.Current.MerchandisableHeight;
                        break;
                    case 6:
                        value = _enumerator.Current.Height;
                        break;
                    case 7:
                        value = _enumerator.Current.Width;
                        break;
                    case 8:
                        value = _enumerator.Current.Depth;
                        break;
                    case 9:
                        value = _enumerator.Current.FaceThickness;
                        break;
                    case 10:
                        value = _enumerator.Current.NotchNumber;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a single Location Space Element record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public LocationSpaceElementDto FetchById(int id)
        {
            LocationSpaceElementDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<LocationSpaceElementDto> FetchByLocationSpaceBayId(int locationSpaceBay_Id)
        {
            List<LocationSpaceElementDto> dtoList = new List<LocationSpaceElementDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementFetchByLocationSpaceBayId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementLocationSpaceBayId,
                        SqlDbType.Int,
                        locationSpaceBay_Id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(LocationSpaceElementDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementInsert))
                {
                    // Parameter to store returned new Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationSpaceElementId, SqlDbType.Int);

                    //Location Space Bay Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementLocationSpaceBayId,
                        SqlDbType.Int,
                        dto.LocationSpaceBayId);

                    //Location Space Element Order
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementOrder,
                        SqlDbType.TinyInt,
                        dto.Order);

                    // Location Space Element X
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementX,
                        SqlDbType.Real,
                        dto.X);

                    // Location Space Element Y
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementY,
                        SqlDbType.Real,
                        dto.Y);

                    // Location Space Element Z
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementZ,
                        SqlDbType.Real,
                        dto.Z);

                    // Location Space Element Merchandisable Height
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementMerchandisableHeight,
                        SqlDbType.Real,
                        dto.MerchandisableHeight);

                    // Location Space Element Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementHeight,
                        SqlDbType.Real,
                        dto.Height);

                    // LocationSpaceElementWidth
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementWidth,
                        SqlDbType.Real,
                        dto.Width);

                    // Location Space Element Depth
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementDepth,
                        SqlDbType.Real,
                        dto.Depth);

                    // Location Space Element Face Thickness
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementFaceThickness,
                        SqlDbType.Real,
                        dto.FaceThickness);

                    // Location Space Element Notch Number
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementNotchNumber,
                        SqlDbType.Real,
                        dto.NotchNumber);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Upserts dtos into the data source
        /// </summary>
        /// <param name="dtoList">The dtos to insert</param>
        public void Insert(IEnumerable<LocationSpaceElementDto> dtoList)
        {
            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationSpaceElementDtoKey, LocationSpaceElementDto> keyedDtoList = new Dictionary<LocationSpaceElementDtoKey, LocationSpaceElementDto>();
            foreach (LocationSpaceElementDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationSpaceElementBulkCopyDataReader dr = new LocationSpaceElementBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch upsert
                Dictionary<LocationSpaceElementDtoKey, Int32> ids = new Dictionary<LocationSpaceElementDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.LocationSpaceElementId]);

                            LocationSpaceElementDtoKey key = new LocationSpaceElementDtoKey();
                            key.LocationSpaceBayId = (Int32)GetValue(dr[FieldNames.LocationSpaceElementLocationSpaceBayId]);
                            key.Order = (Byte)GetValue(dr[FieldNames.LocationSpaceElementOrder]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationSpaceElementDto dto in dtoList)
                {
                    Int32 outputData = 0;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != 0)
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the data source
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(LocationSpaceElementDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementUpdateById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementId,
                        SqlDbType.Int,
                        dto.Id);

                    //Location Space Bay Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementLocationSpaceBayId,
                        SqlDbType.Int,
                        dto.LocationSpaceBayId);

                    //Location Space Element Order
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementOrder,
                        SqlDbType.TinyInt,
                        dto.Order);

                    // Location Space Element X
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementX,
                        SqlDbType.Real,
                        dto.X);

                    // Location Space Element Y
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementY,
                        SqlDbType.Real,
                        dto.Y);

                    // Location Space Element Z
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementZ,
                        SqlDbType.Real,
                        dto.Z);

                    // Location Space Element Merchandisable Height
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementMerchandisableHeight,
                        SqlDbType.Real,
                        dto.MerchandisableHeight);

                    // Location Space Element Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementHeight,
                        SqlDbType.Real,
                        dto.Height);

                    // LocationSpaceElementWidth
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementWidth,
                        SqlDbType.Real,
                        dto.Width);

                    // Location Space Element Depth
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementDepth,
                        SqlDbType.Real,
                        dto.Depth);

                    // Location Space Element Face Thickness
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementFaceThickness,
                        SqlDbType.Real,
                        dto.FaceThickness);

                    // Location Space Element Notch Number
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementNotchNumber,
                        SqlDbType.Real,
                        dto.NotchNumber);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int element_id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceElementId,
                        SqlDbType.Int,
                        element_id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified dto from the data source
        /// </summary>
        /// <param name="id">The entity id to delete against</param>
        public void DeleteByEntityId(int entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementDeleteByEntityId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceEntityId,
                        SqlDbType.Int,
                        entityId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Upsert

        /// <summary>
        /// Upserts dtos into the data source
        /// </summary>
        /// <param name="dtoList">The dtos to insert</param>
        public void Upsert(IEnumerable<LocationSpaceElementDto> dtoList)
        {
            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationSpaceElementDtoKey, LocationSpaceElementDto> keyedDtoList = new Dictionary<LocationSpaceElementDtoKey, LocationSpaceElementDto>();
            foreach (LocationSpaceElementDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationSpaceElementBulkCopyDataReader dr = new LocationSpaceElementBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch upsert
                Dictionary<LocationSpaceElementDtoKey, Int32> ids = new Dictionary<LocationSpaceElementDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceElementBulkUpsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.LocationSpaceElementId]);

                            LocationSpaceElementDtoKey key = new LocationSpaceElementDtoKey();
                            key.LocationSpaceBayId = (Int32)GetValue(dr[FieldNames.LocationSpaceElementLocationSpaceBayId]);
                            key.Order = (Byte)GetValue(dr[FieldNames.LocationSpaceElementOrder]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationSpaceElementDto dto in dtoList)
                {
                    Int32 outputData = 0;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != 0)
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(string importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
