﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26985 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Altered names of existing properties in PlanogramProcessingStatusDto
//  Renamed Increment to AutomationIncrement
//  Added MetaData, Validation properties to dto
//  Added MetaData, Validation increment methods
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public sealed class PlanogramProcessingStatusDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramProcessingStatusDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        public PlanogramProcessingStatusDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramProcessingStatusDto()
            {
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramProcessingStatusPlanogramId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PlanogramProcessingStatusRowVersion])),
                MetaDataStatus = (Byte)GetValue(dr[FieldNames.PlanogramProcessingStatusMetaDataStatus]),
                MetaDataStatusDescription = (String)GetValue(dr[FieldNames.PlanogramProcessingStatusMetaDataStatusDescription]),
                MetaDataProgressMax = (Int32)GetValue(dr[FieldNames.PlanogramProcessingStatusMetaDataProgressMax]),
                MetaDataProgressCurrent = (Int32)GetValue(dr[FieldNames.PlanogramProcessingStatusMetaDataProgressCurrent]),
                MetaDataDateStarted = (DateTime?)GetValue(dr[FieldNames.PlanogramProcessingStatusMetaDataDateStarted]),
                MetaDataDateLastUpdated = (DateTime?)GetValue(dr[FieldNames.PlanogramProcessingStatusMetaDataDateUpdated]),
                ValidationStatus = (Byte)GetValue(dr[FieldNames.PlanogramProcessingStatusValidationStatus]),
                ValidationStatusDescription = (String)GetValue(dr[FieldNames.PlanogramProcessingStatusValidationStatusDescription]),
                ValidationProgressMax = (Int32)GetValue(dr[FieldNames.PlanogramProcessingStatusValidationProgressMax]),
                ValidationProgressCurrent = (Int32)GetValue(dr[FieldNames.PlanogramProcessingStatusValidationProgressCurrent]),
                ValidationDateStarted = (DateTime?)GetValue(dr[FieldNames.PlanogramProcessingStatusValidationDateStarted]),
                ValidationDateLastUpdated = (DateTime?)GetValue(dr[FieldNames.PlanogramProcessingStatusValidationDateUpdated]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        public PlanogramProcessingStatusDto FetchByPlanogramId(int workpackageId)
        {
            PlanogramProcessingStatusDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProcessingStatusFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusPlanogramId, SqlDbType.Int, workpackageId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        public void Update(PlanogramProcessingStatusDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProcessingStatusUpdateByPlanogramId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PlanogramProcessingStatusRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataStatus, SqlDbType.TinyInt, dto.MetaDataStatus);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataStatusDescription, SqlDbType.NVarChar, dto.MetaDataStatusDescription);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataProgressMax, SqlDbType.Int, dto.MetaDataProgressMax);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataProgressCurrent, SqlDbType.Int, dto.MetaDataProgressCurrent);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataDateStarted, SqlDbType.DateTime, dto.MetaDataDateStarted);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataDateUpdated, SqlDbType.DateTime, dto.MetaDataDateLastUpdated);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationStatus, SqlDbType.TinyInt, dto.ValidationStatus);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationStatusDescription, SqlDbType.NVarChar, dto.ValidationStatusDescription);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationProgressMax, SqlDbType.Int, dto.ValidationProgressMax);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationProgressCurrent, SqlDbType.Int, dto.ValidationProgressCurrent);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationDateStarted, SqlDbType.DateTime, dto.ValidationDateStarted);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationDateUpdated, SqlDbType.DateTime, dto.ValidationDateLastUpdated);
                    
                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion


        #region MetaDataStatusIncrement
        /// <summary>
        /// Increments the automation progress of a planogram
        /// </summary>
        public void MetaDataStatusIncrement(Int32 planogramId, String statusDescription, DateTime dateLastUpdated)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProcessingStatusIncrementMetaDataStatusByPlanogramId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusPlanogramId, SqlDbType.Int, planogramId);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataStatusDescription, SqlDbType.NVarChar, statusDescription);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusMetaDataDateUpdated, SqlDbType.DateTime, dateLastUpdated);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region ValidationStatusIncrement
        /// <summary>
        /// Increments the Validation progress of a planogram
        /// </summary>
        public void ValidationStatusIncrement(Int32 planogramId, String statusDescription, DateTime dateLastUpdated)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProcessingStatusIncrementValidationStatusByPlanogramId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusPlanogramId, SqlDbType.Int, planogramId);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationStatusDescription, SqlDbType.NVarChar, statusDescription);
                    CreateParameter(command, FieldNames.PlanogramProcessingStatusValidationDateUpdated, SqlDbType.DateTime, dateLastUpdated);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
