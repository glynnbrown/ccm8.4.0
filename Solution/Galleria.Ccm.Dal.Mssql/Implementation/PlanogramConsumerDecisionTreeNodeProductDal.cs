﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong 
//  Created (Auto-generated)
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramConsumerDecisionTreeNode Dal
    /// </summary>
    public class PlanogramConsumerDecisionTreeNodeProductDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramConsumerDecisionTreeNodeProductDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public PlanogramConsumerDecisionTreeNodeProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramConsumerDecisionTreeNodeProductDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeProductId]),
                PlanogramConsumerDecisionTreeNodeId = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId]),
                PlanogramProductId = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramProductId]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a dto that matches the specified id
        /// </summary>
        /// <param name="ConsumerDecisionTreeNodeId">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> FetchByPlanogramConsumerDecisionTreeNodeId(object planogramConsumerDecisionTreeNodeId)
        {
            var dtoList = new List<PlanogramConsumerDecisionTreeNodeProductDto>();
            try
            {
                using (
                    DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeProductFetchByPlanogramConsumerDecisionTreeNodeId)
                    )
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId, SqlDbType.Int,
                        (Int32)planogramConsumerDecisionTreeNodeId);

                    // Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeProductInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId, SqlDbType.Int,
                        dto.PlanogramConsumerDecisionTreeNodeId);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramProductId, SqlDbType.Int, dto.PlanogramProductId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified dtos
        /// </summary>
        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeProductDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeProductUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId, SqlDbType.Int,
                        dto.PlanogramConsumerDecisionTreeNodeId);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductPlanogramProductId, SqlDbType.Int, dto.PlanogramProductId);


                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeProductDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeProductDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeProductId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
