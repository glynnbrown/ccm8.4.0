﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : L.Ineson
//  Created
#endregion
#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkflowTaskDal
    /// </summary>
    public sealed class WorkflowTaskDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkflowTaskDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public WorkflowTaskDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkflowTaskDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkflowTaskId]),
                WorkflowId = (Int32)GetValue(dr[FieldNames.WorkflowTaskWorkflowId]),
                SequenceId = (Byte)GetValue(dr[FieldNames.WorkflowTaskSequenceId]),
                TaskType = (String)GetValue(dr[FieldNames.WorkflowTaskTaskType]),
                DisplayName = (String)GetValue(dr[FieldNames.WorkflowTaskDisplayName]),
                DisplayDescription = (String)GetValue(dr[FieldNames.WorkflowTaskDisplayDescription]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns dtos for the given workflow id
        /// </summary>
        /// <param name="workflowId"></param>
        /// <returns></returns>
        public IEnumerable<WorkflowTaskDto> FetchByWorkflowId(Int32 workflowId)
        {
            List<WorkflowTaskDto> dtoList = new List<WorkflowTaskDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskFetchByWorkflowId))
                {
                    CreateParameter(command, FieldNames.WorkflowTaskWorkflowId, SqlDbType.Int, workflowId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(WorkflowTaskDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkflowTaskId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.WorkflowTaskWorkflowId, SqlDbType.Int, dto.WorkflowId);
                    CreateParameter(command, FieldNames.WorkflowTaskSequenceId, SqlDbType.TinyInt, dto.SequenceId);
                    CreateParameter(command, FieldNames.WorkflowTaskTaskType, SqlDbType.NVarChar, dto.TaskType);
                    CreateParameter(command, FieldNames.WorkflowTaskDisplayName, SqlDbType.NVarChar, dto.DisplayName);
                    CreateParameter(command, FieldNames.WorkflowTaskDisplayDescription, SqlDbType.NVarChar, dto.DisplayDescription);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void Update(WorkflowTaskDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkflowTaskId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.WorkflowTaskWorkflowId, SqlDbType.Int, dto.WorkflowId);
                    CreateParameter(command, FieldNames.WorkflowTaskSequenceId, SqlDbType.TinyInt, dto.SequenceId);
                    CreateParameter(command, FieldNames.WorkflowTaskTaskType, SqlDbType.NVarChar, dto.TaskType);
                    CreateParameter(command, FieldNames.WorkflowTaskDisplayName, SqlDbType.NVarChar, dto.DisplayName);
                    CreateParameter(command, FieldNames.WorkflowTaskDisplayDescription, SqlDbType.NVarChar, dto.DisplayDescription);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the dto with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkflowTaskDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkflowTaskId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
