﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// AssortmentMinorRevisionReplaceAction Dal
    /// </summary>
    public class AssortmentMinorRevisionReplaceActionDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionReplaceActionDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionReplaceActionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionReplaceActionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionId]),
                AssortmentMinorRevisionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionAssortmentMinorRevisionId]),
                ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionProductId]),
                ProductGtin = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionProductGtin]),
                ProductName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionProductName]),
                ReplacementProductId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductId]),
                ReplacementProductGtin = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductGtin]),
                ReplacementProductName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductName]),
                Priority = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionPriority]),
                Comments = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionComments])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionReplaceActionDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionReplaceActionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment minor revision id
        /// </summary>
        /// <param name="entityId">specified assortment minor revision id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionReplaceActionDto> FetchByAssortmentMinorRevisionId(Int32 assortmentMinorRevisionId)
        {
            List<AssortmentMinorRevisionReplaceActionDto> dtoList = new List<AssortmentMinorRevisionReplaceActionDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionFetchByAssortmentMinorRevisionId))
                {
                    //Assortment minor revision id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionAssortmentMinorRevisionId, SqlDbType.Int, assortmentMinorRevisionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionReplaceActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductId, SqlDbType.Int, dto.ReplacementProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductGtin, SqlDbType.NVarChar, dto.ReplacementProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductName, SqlDbType.NVarChar, dto.ReplacementProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionComments, SqlDbType.NVarChar, dto.Comments);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionReplaceActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductId, SqlDbType.Int, dto.ReplacementProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductGtin, SqlDbType.NVarChar, dto.ReplacementProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionReplacementProductName, SqlDbType.NVarChar, dto.ReplacementProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionComments, SqlDbType.NVarChar, dto.Comments);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
