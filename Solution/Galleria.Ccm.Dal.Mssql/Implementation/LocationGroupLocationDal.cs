﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql dal implementation of ILocationGroupLocationDal
    /// </summary>
    public class LocationGroupLocationDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationGroupLocationDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public LocationGroupLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationGroupLocationDto()
            {
                LocationId = (Int16)GetValue(dr[FieldNames.LocationId]),
                Code = (String)GetValue(dr[FieldNames.LocationCode]),
                Name = (String)GetValue(dr[FieldNames.LocationName]),
                LocationGroupId = (Int32)GetValue(dr[FieldNames.LocationLocationGroupId]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Fetches a list of dtos by location group id
        /// </summary>
        /// <param name="locationGroupId"></param>
        /// <returns></returns>
        public IEnumerable<LocationGroupLocationDto> FetchByLocationGroupId(Int32 locationGroupId)
        {
            List<LocationGroupLocationDto> dtoList = new List<LocationGroupLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationGroupLocationFetchByLocationGroupId))
                {
                    CreateParameter(command, FieldNames.LocationLocationGroupId, SqlDbType.Int, locationGroupId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        //not allowed so no method required.

        #endregion

        #region Update

        public void Update(LocationGroupLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationUpdateLocationGroupId))
                {
                    CreateParameter(command, FieldNames.LocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.LocationLocationGroupId, SqlDbType.Int, dto.LocationGroupId);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}
