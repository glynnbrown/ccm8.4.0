﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created (Auto-generated)
// CCM-25816 : N.Haywood
//      Removed date parameters from insert and update
// CCM-27078 : I.George
//  Added ProductGroupId to ClusterSchemeProcedures
#endregion
#region Version History: (CCM v8.0.1)
// V8-28878 : D.Pleasance
//  Fixed upsert, ProductGroup_Id being populated with entity data and vice versa (ClusterSchemeBulkCopyDataReader)
#endregion 
#region Version History: (CCM v8.0.3)
// V8-29216 : D.Pleasance
//  Removed ClusterScheme_DateDeleted
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion 
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ClusterScheme Dal
    /// </summary>
    public class ClusterSchemeDal : Galleria.Framework.Dal.Mssql.DalBase, IClusterSchemeDal
    {

        #region Constants

        private const String _importTableName = "#tmpClusterScheme";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[ClusterScheme_UniqueContentReference] [UNIQUEIDENTIFIER], " +
                "[ClusterScheme_Name] [NVARCHAR](255) COLLATE database_default, " +
                "[ClusterScheme_IsDefault] [BIT], " +
                "[Entity_Id] [INT]," +
                "[ProductGroup_Id] [INT] " +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class ClusterSchemeBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<ClusterSchemeDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ClusterSchemeBulkCopyDataReader(IEnumerable<ClusterSchemeDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 5; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.UniqueContentReference;
                        break;
                    case 1:
                        value = _enumerator.Current.Name;
                        break;
                    case 2:
                        value = _enumerator.Current.IsDefault;
                        break;
                    case 3:
                        value = _enumerator.Current.EntityId;
                        break;
                    case 4:
                        value = _enumerator.Current.ProductGroupId;
                        break;

                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static ClusterSchemeDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ClusterSchemeDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ClusterSchemeId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ClusterSchemeRowVersion])),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.ClusterSchemeUniqueContentReference]),
                Name = (String)GetValue(dr[FieldNames.ClusterSchemeName]),
                IsDefault = (Boolean)GetValue(dr[FieldNames.ClusterSchemeIsDefault]),
                EntityId = (Int32)GetValue(dr[FieldNames.ClusterSchemeEntityId]),
                ProductGroupId = (Int32?)GetValue(dr[FieldNames.ClusterSchemeProductGroupId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ClusterSchemeDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ClusterSchemeDateLastModified]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.ClusterSchemeParentUniqueContentReference])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ClusterSchemeDto FetchById(Int32 id)
        {
            ClusterSchemeDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ClusterSchemeId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ClusterSchemeDto> FetchByEntityId(Int32 entityId)
        {
            List<ClusterSchemeDto> dtoList = new List<ClusterSchemeDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.ClusterSchemeEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ClusterSchemeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ClusterSchemeId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ClusterSchemeRowVersion, SqlDbType.Timestamp);

                    //Other properties 
                    CreateParameter(command, FieldNames.ClusterSchemeUniqueContentReference, SqlDbType.UniqueIdentifier, dto.UniqueContentReference);
                    CreateParameter(command, FieldNames.ClusterSchemeName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ClusterSchemeIsDefault, SqlDbType.Bit, dto.IsDefault);
                    CreateParameter(command, FieldNames.ClusterSchemeEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ClusterSchemeProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.ClusterSchemeParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ClusterSchemeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ClusterSchemeId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ClusterSchemeRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //Other properties 
                    CreateParameter(command, FieldNames.ClusterSchemeUniqueContentReference, SqlDbType.UniqueIdentifier, dto.UniqueContentReference);
                    CreateParameter(command, FieldNames.ClusterSchemeName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ClusterSchemeIsDefault, SqlDbType.Bit, dto.IsDefault);
                    CreateParameter(command, FieldNames.ClusterSchemeEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ClusterSchemeProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.ClusterSchemeParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<ClusterSchemeDto> dtoList, ClusterSchemeIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new ClusterSchemeIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<ClusterSchemeDtoKey, ClusterSchemeDto> keyedDtoList = new Dictionary<ClusterSchemeDtoKey, ClusterSchemeDto>();
            foreach (ClusterSchemeDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    ClusterSchemeBulkCopyDataReader dr = new ClusterSchemeBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<ClusterSchemeDtoKey, Tuple<Int32, RowVersion>> ids = new Dictionary<ClusterSchemeDtoKey, Tuple<Int32, RowVersion>>();
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.ClusterSchemeSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Tuple<Int32, RowVersion> outputData =
                                new Tuple<Int32, RowVersion>
                                    (
                                        (Int32)GetValue(dr[FieldNames.ClusterSchemeId]),
                                        new RowVersion(GetValue(dr[FieldNames.ClusterSchemeRowVersion]))
                                    );

                            ClusterSchemeDtoKey key = new ClusterSchemeDtoKey();
                            key.EntityId = (Int32)GetValue(dr[FieldNames.ClusterSchemeEntityId]);
                            key.Name = (String)GetValue(dr[FieldNames.ClusterSchemeName]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (ClusterSchemeDto dto in dtoList)
                {
                    Tuple<Int32, RowVersion> outputData = null;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != null)
                    {
                        dto.Id = outputData.Item1;
                        dto.RowVersion = outputData.Item2;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private String GenerateSetPropertiesString(ClusterSchemeIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsUniqueContentReferenceSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ClusterSchemeUniqueContentReference);
            }
            if (isSetDto.IsNameSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ClusterSchemeName);
            }
            if (isSetDto.IsIsDefaultSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ClusterSchemeIsDefault);
            }
            if (isSetDto.IsEntityIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ClusterSchemeEntityId);
            }
            if (isSetDto.IsProductGroupIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ClusterSchemeProductGroupId);
            }
           return setProperties;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ClusterSchemeId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes dtos that match the specified entity id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeDeleteByEntityId))
                {
                    //Id
                    CreateParameter(command, FieldNames.ClusterSchemeEntityId, SqlDbType.Int, entityId);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}