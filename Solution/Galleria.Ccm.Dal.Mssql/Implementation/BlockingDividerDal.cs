﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// BlockingDivider Dal
    /// </summary>
    public class BlockingDividerDal : Galleria.Framework.Dal.Mssql.DalBase, IBlockingDividerDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static BlockingDividerDto GetDataTransferObject(SqlDataReader dr)
        {
            return new BlockingDividerDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.BlockingDividerId]),
                BlockingId = (Int32)GetValue(dr[FieldNames.BlockingDividerBlockingId]),
                Type = (Byte)GetValue(dr[FieldNames.BlockingDividerType]),
                Level = (Byte)GetValue(dr[FieldNames.BlockingDividerLevel]),
                X = (Single)GetValue(dr[FieldNames.BlockingDividerX]),
                Y = (Single)GetValue(dr[FieldNames.BlockingDividerY]),
                Length = (Single)GetValue(dr[FieldNames.BlockingDividerLength]),
                IsSnapped = (Boolean)GetValue(dr[FieldNames.BlockingDividerIsSnapped]),
            };
        }

        #endregion

        #region Fetch



        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<BlockingDividerDto> FetchByBlockingId(Int32 blockingId)
        {
            List<BlockingDividerDto> dtoList = new List<BlockingDividerDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingDividerFetchByBlockingId))
                {
                    CreateParameter(command, FieldNames.BlockingDividerBlockingId, SqlDbType.Int, blockingId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(BlockingDividerDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingDividerInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.BlockingDividerId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingDividerBlockingId, SqlDbType.Int, dto.BlockingId);
                    CreateParameter(command, FieldNames.BlockingDividerType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.BlockingDividerLevel, SqlDbType.TinyInt, dto.Level);
                    CreateParameter(command, FieldNames.BlockingDividerX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.BlockingDividerY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.BlockingDividerLength, SqlDbType.Real, dto.Length);
                    CreateParameter(command, FieldNames.BlockingDividerIsSnapped, SqlDbType.Bit, dto.IsSnapped);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(BlockingDividerDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingDividerUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingDividerId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingDividerBlockingId, SqlDbType.Int, dto.BlockingId);
                    CreateParameter(command, FieldNames.BlockingDividerType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.BlockingDividerLevel, SqlDbType.TinyInt, dto.Level);
                    CreateParameter(command, FieldNames.BlockingDividerX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.BlockingDividerY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.BlockingDividerLength, SqlDbType.Real, dto.Length);
                    CreateParameter(command, FieldNames.BlockingDividerIsSnapped, SqlDbType.Bit, dto.IsSnapped);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingDividerDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingDividerId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}