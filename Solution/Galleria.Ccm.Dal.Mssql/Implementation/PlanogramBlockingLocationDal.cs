﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramBlockingLocation Dal
    /// </summary>
    public class PlanogramBlockingLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramBlockingLocationDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramBlockingLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramBlockingLocationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramBlockingLocationId]),
                PlanogramBlockingId = (Int32)GetValue(dr[FieldNames.PlanogramBlockingLocationPlanogramBlockingId]),
                PlanogramBlockingGroupId = (Int32)GetValue(dr[FieldNames.PlanogramBlockingLocationPlanogramBlockingGroupId]),
                PlanogramBlockingDividerTopId = (Int32?)GetValue(dr[FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerTopId]),
                PlanogramBlockingDividerBottomId = (Int32?)GetValue(dr[FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerBottomId]),
                PlanogramBlockingDividerLeftId = (Int32?)GetValue(dr[FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerLeftId]),
                PlanogramBlockingDividerRightId = (Int32?)GetValue(dr[FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerRightId]),
                SpacePercentage = (Single)GetValue(dr[FieldNames.PlanogramBlockingLocationSpacePercentage]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramBlockingLocationDto> FetchByPlanogramBlockingId(Object planogramBlockingId)
        {
            List<PlanogramBlockingLocationDto> dtoList = new List<PlanogramBlockingLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingLocationFetchByPlanogramBlockingId))
                {
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingId, SqlDbType.Int, planogramBlockingId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramBlockingLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramBlockingLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingId, SqlDbType.Int, dto.PlanogramBlockingId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingGroupId, SqlDbType.Int, dto.PlanogramBlockingGroupId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerTopId, SqlDbType.Int, dto.PlanogramBlockingDividerTopId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerBottomId, SqlDbType.Int, dto.PlanogramBlockingDividerBottomId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerLeftId, SqlDbType.Int, dto.PlanogramBlockingDividerLeftId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerRightId, SqlDbType.Int, dto.PlanogramBlockingDividerRightId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationSpacePercentage, SqlDbType.Real, dto.SpacePercentage);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            foreach (PlanogramBlockingLocationDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramBlockingLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingLocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingId, SqlDbType.Int, dto.PlanogramBlockingId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingGroupId, SqlDbType.Int, dto.PlanogramBlockingGroupId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerTopId, SqlDbType.Int, dto.PlanogramBlockingDividerTopId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerBottomId, SqlDbType.Int, dto.PlanogramBlockingDividerBottomId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerLeftId, SqlDbType.Int, dto.PlanogramBlockingDividerLeftId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationPlanogramBlockingDividerRightId, SqlDbType.Int, dto.PlanogramBlockingDividerRightId);
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationSpacePercentage, SqlDbType.Real, dto.SpacePercentage);

                    //execute
                    command.ExecuteNonQuery();

                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            foreach (PlanogramBlockingLocationDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramBlockingLocationDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            foreach (PlanogramBlockingLocationDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}