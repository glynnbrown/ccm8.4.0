﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM 8.0
// V8-25669 : A.Kuszyk
//		Created (auto-generated).
// CCM-25242 : N.Haywood
//  Added fetch methods for imports
// CCM-25827 : N.Haywood
//  Removed dates from insert and update methods
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIds.
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
// V8-25556 : D.Pleasance
//  Added FetchByEntityIdProductGtins
// V8-26041 : A.Kuszyk
//  Changed FillPattern to FillPatternType
// V8-26041 : A.Kuszyk
//  Added ShapeType and changed FillColour to Int32.
// V8-27150 : L.Ineson
//  Added FetchByMerchandisingGroupId & FetchByEntityIdSearchText
// V8-27424 : L.Luong 
//  Added FinancialGroupCode and FinancialGroupName
// V8-27424 : A.Probyn
//  Fixed typo in _createImportTableSql
// V8-26385 : L.Luong
//  Added RowVersion
// V8-26777 : L.Ineson
//  Removed CanMerch properties
#endregion
#region Version History: CCM 8.2.0
// V8-30865 : N.Haywood
//  Increased some attribute lengths
#endregion
#region Version History CCM 8.3.0
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct and ajusted the switch statment from case 63 onwards.
//  as well as  changed the FieldCount return number from 120 to 119
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-32025 : A.Probyn
//  Added missing collate to CreateProductTempTable
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32191 : A.Probyn
//  Corrected data reader case statement.
// V8-32356 : N.Haywood
//  Added FetchByEntityIdMultipleSearchText
// CCM-18494 : R.Cooper
//  Changed temporary table method to use SQL Bulk Copy rather than single line inserts.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Product Dal
    /// </summary>
    public class ProductDal : Galleria.Framework.Dal.Mssql.DalBase, IProductDal
    {
        #region Constants

        private const String _importTableName = "#tmpProduct";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[Entity_Id] [INT], " +
                "[ProductGroup_Id] [INT], " +
                "[Product_ReplacementProductId] [INT], " +
                "[Product_Gtin] [NVARCHAR](14) COLLATE database_default, " +
                "[Product_Name] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_Height] [REAL], " +
                "[Product_Width] [REAL], " +
                "[Product_Depth] [REAL], " +
                "[Product_DisplayHeight] [REAL], " +
                "[Product_DisplayWidth] [REAL], " +
                "[Product_DisplayDepth] [REAL], " +
                "[Product_AlternateHeight] [REAL], " +
                "[Product_AlternateWidth] [REAL], " +
                "[Product_AlternateDepth] [REAL], " +
                "[Product_PointOfPurchaseHeight] [REAL], " +
                "[Product_PointOfPurchaseWidth] [REAL], " +
                "[Product_PointOfPurchaseDepth] [REAL], " +
                "[Product_NumberOfPegHoles] [TINYINT], " +
                "[Product_PegX] [REAL], " +
                "[Product_PegX2] [REAL], " +
                "[Product_PegX3] [REAL], " +
                "[Product_PegY] [REAL], " +
                "[Product_PegY2] [REAL], " +
                "[Product_PegY3] [REAL], " +
                "[Product_PegProngOffsetX] [REAL], " +
                "[Product_PegProngOffsetY] [REAL], " +
                "[Product_PegDepth] [REAL], " +
                "[Product_SqueezeHeight] [REAL], " +
                "[Product_SqueezeWidth] [REAL], " +
                "[Product_SqueezeDepth] [REAL], " +
                "[Product_NestingHeight] [REAL], " +
                "[Product_NestingWidth] [REAL], " +
                "[Product_NestingDepth] [REAL], " +
                "[Product_CasePackUnits] [SMALLINT], " +
                "[Product_CaseHigh] [TINYINT], " +
                "[Product_CaseWide] [TINYINT], " +
                "[Product_CaseDeep] [TINYINT], " +
                "[Product_CaseHeight] [REAL], " +
                "[Product_CaseWidth] [REAL], " +
                "[Product_CaseDepth] [REAL], " +
                "[Product_MaxStack] [TINYINT], " +
                "[Product_MaxTopCap] [TINYINT], " +
                "[Product_MaxRightCap] [TINYINT], " +
                "[Product_MinDeep] [TINYINT], " +
                "[Product_MaxDeep] [TINYINT], " +
                "[Product_TrayPackUnits] [SMALLINT], " +
                "[Product_TrayHigh] [TINYINT], " +
                "[Product_TrayWide] [TINYINT], " +
                "[Product_TrayDeep] [TINYINT], " +
                "[Product_TrayHeight] [REAL], " +
                "[Product_TrayWidth] [REAL], " +
                "[Product_TrayDepth] [REAL], " +
                "[Product_TrayThickHeight] [REAL], " +
                "[Product_TrayThickWidth] [REAL], " +
                "[Product_TrayThickDepth] [REAL], " +
                "[Product_FrontOverhang] [REAL], " +
                "[Product_FingerSpaceAbove] [REAL], " +
                "[Product_FingerSpaceToTheSide] [REAL], " +
                "[Product_StatusType] [TINYINT], " +
                "[Product_OrientationType] [TINYINT], " +
                "[Product_MerchandisingStyle] [TINYINT], " +
                "[Product_IsFrontOnly] [BIT], " +
                "[Product_IsPlaceHolderProduct] [BIT], " +
                "[Product_IsActive] [BIT], " +
                "[Product_CanBreakTrayUp] [BIT], " +
                "[Product_CanBreakTrayDown] [BIT], " +
                "[Product_CanBreakTrayBack] [BIT], " +
                "[Product_CanBreakTrayTop] [BIT], " +
                "[Product_ForceMiddleCap] [BIT], " +
                "[Product_ForceBottomCap] [BIT], " +
                "[Product_Shape] [NVARCHAR](255), " +
                "[Product_ShapeType] [TINYINT], " +
                "[Product_FillColour] [INT], " +
                "[Product_FillPatternType] [TINYINT], " +
                "[Product_PointOfPurchaseDescription] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_ShortDescription] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_Subcategory] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_CustomerStatus] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_Colour] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_Flavour] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_PackagingShape] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_PackagingType] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_CountryOfOrigin] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_CountryOfProcessing] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_ShelfLife] [SMALLINT], " +
                "[Product_DeliveryFrequencyDays] [REAL], " +
                "[Product_DeliveryMethod] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_Brand] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_VendorCode] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_Vendor] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_ManufacturerCode] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_Manufacturer] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_Size] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_UnitOfMeasure] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_DateIntroduced] [DATETIME], " +
                "[Product_DateDiscontinued] [DATETIME], " +
                "[Product_DateEffective] [DATETIME], " +
                "[Product_Health] [NVARCHAR](20) COLLATE database_default, " +
                "[Product_CorporateCode] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_Barcode] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_SellPrice] [REAL], " +
                "[Product_SellPackCount] [SMALLINT], " +
                "[Product_SellPackDescription] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_RecommendedRetailPrice] [REAL], " +
                "[Product_ManufacturerRecommendedRetailPrice] [REAL], " +
                "[Product_CostPrice] [REAL], " +
                "[Product_CaseCost] [REAL], " +
                "[Product_TaxRate] [REAL], " +
                "[Product_ConsumerInformation] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_Texture] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_StyleNumber] [SMALLINT], " +
                "[Product_Pattern] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_Model] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_GarmentType] [NVARCHAR](100) COLLATE database_default, " +
                "[Product_IsPrivateLabel] [BIT], " +
                "[Product_IsNew] [BIT], " +
                "[Product_FinancialGroupCode] [NVARCHAR](50) COLLATE database_default, " +
                "[Product_FinancialGroupName] [NVARCHAR](100) COLLATE database_default " +
            ")";

        private const String _createGtinTableSql = "CREATE TABLE [{0}] (Product_Gtin varchar(14) COLLATE database_default)";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class ProductBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<ProductDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ProductBulkCopyDataReader(IEnumerable<ProductDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 118; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.EntityId;
                        break;
                    case 1:
                        value = _enumerator.Current.ProductGroupId;
                        break;
                    case 2:
                        value = _enumerator.Current.ReplacementProductId;
                        break;
                    case 3:
                        value = _enumerator.Current.Gtin;
                        break;
                    case 4:
                        value = _enumerator.Current.Name;
                        break;
                    case 5:
                        value = _enumerator.Current.Height;
                        break;
                    case 6:
                        value = _enumerator.Current.Width;
                        break;
                    case 7:
                        value = _enumerator.Current.Depth;
                        break;
                    case 8:
                        value = _enumerator.Current.DisplayHeight;
                        break;
                    case 9:
                        value = _enumerator.Current.DisplayWidth;
                        break;
                    case 10:
                        value = _enumerator.Current.DisplayDepth;
                        break;
                    case 11:
                        value = _enumerator.Current.AlternateHeight;
                        break;
                    case 12:
                        value = _enumerator.Current.AlternateWidth;
                        break;
                    case 13:
                        value = _enumerator.Current.AlternateDepth;
                        break;
                    case 14:
                        value = _enumerator.Current.PointOfPurchaseHeight;
                        break;
                    case 15:
                        value = _enumerator.Current.PointOfPurchaseWidth;
                        break;
                    case 16:
                        value = _enumerator.Current.PointOfPurchaseDepth;
                        break;
                    case 17:
                        value = _enumerator.Current.NumberOfPegHoles;
                        break;
                    case 18:
                        value = _enumerator.Current.PegX;
                        break;
                    case 19:
                        value = _enumerator.Current.PegX2;
                        break;
                    case 20:
                        value = _enumerator.Current.PegX3;
                        break;
                    case 21:
                        value = _enumerator.Current.PegY;
                        break;
                    case 22:
                        value = _enumerator.Current.PegY2;
                        break;
                    case 23:
                        value = _enumerator.Current.PegY3;
                        break;
                    case 24:
                        value = _enumerator.Current.PegProngOffsetX;
                        break;
                    case 25:
                        value = _enumerator.Current.PegProngOffsetY;
                        break;
                    case 26:
                        value = _enumerator.Current.PegDepth;
                        break;
                    case 27:
                        value = _enumerator.Current.SqueezeHeight;
                        break;
                    case 28:
                        value = _enumerator.Current.SqueezeWidth;
                        break;
                    case 29:
                        value = _enumerator.Current.SqueezeDepth;
                        break;
                    case 30:
                        value = _enumerator.Current.NestingHeight;
                        break;
                    case 31:
                        value = _enumerator.Current.NestingWidth;
                        break;
                    case 32:
                        value = _enumerator.Current.NestingDepth;
                        break;
                    case 33:
                        value = _enumerator.Current.CasePackUnits;
                        break;
                    case 34:
                        value = _enumerator.Current.CaseHigh;
                        break;
                    case 35:
                        value = _enumerator.Current.CaseWide;
                        break;
                    case 36:
                        value = _enumerator.Current.CaseDeep;
                        break;
                    case 37:
                        value = _enumerator.Current.CaseHeight;
                        break;
                    case 38:
                        value = _enumerator.Current.CaseWidth;
                        break;
                    case 39:
                        value = _enumerator.Current.CaseDepth;
                        break;
                    case 40:
                        value = _enumerator.Current.MaxStack;
                        break;
                    case 41:
                        value = _enumerator.Current.MaxTopCap;
                        break;
                    case 42:
                        value = _enumerator.Current.MaxRightCap;
                        break;
                    case 43:
                        value = _enumerator.Current.MinDeep;
                        break;
                    case 44:
                        value = _enumerator.Current.MaxDeep;
                        break;
                    case 45:
                        value = _enumerator.Current.TrayPackUnits;
                        break;
                    case 46:
                        value = _enumerator.Current.TrayHigh;
                        break;
                    case 47:
                        value = _enumerator.Current.TrayWide;
                        break;
                    case 48:
                        value = _enumerator.Current.TrayDeep;
                        break;
                    case 49:
                        value = _enumerator.Current.TrayHeight;
                        break;
                    case 50:
                        value = _enumerator.Current.TrayWidth;
                        break;
                    case 51:
                        value = _enumerator.Current.TrayDepth;
                        break;
                    case 52:
                        value = _enumerator.Current.TrayThickHeight;
                        break;
                    case 53:
                        value = _enumerator.Current.TrayThickWidth;
                        break;
                    case 54:
                        value = _enumerator.Current.TrayThickDepth;
                        break;
                    case 55:
                        value = _enumerator.Current.FrontOverhang;
                        break;
                    case 56:
                        value = _enumerator.Current.FingerSpaceAbove;
                        break;
                    case 57:
                        value = _enumerator.Current.FingerSpaceToTheSide;
                        break;
                    case 58:
                        value = _enumerator.Current.StatusType;
                        break;
                    case 59:
                        value = _enumerator.Current.OrientationType;
                        break;
                    case 60:
                        value = _enumerator.Current.MerchandisingStyle;
                        break;
                    case 61:
                        value = _enumerator.Current.IsFrontOnly;
                        break;
                    case 62:
                        value = _enumerator.Current.IsPlaceHolderProduct;
                        break;
                    case 63:
                        value = _enumerator.Current.IsActive;
                        break;
                    case 64:
                        value = _enumerator.Current.CanBreakTrayUp;
                        break;
                    case 65:
                        value = _enumerator.Current.CanBreakTrayDown;
                        break;
                    case 66:
                        value = _enumerator.Current.CanBreakTrayBack;
                        break;
                    case 67:
                        value = _enumerator.Current.CanBreakTrayTop;
                        break;
                    case 68:
                        value = _enumerator.Current.ForceMiddleCap;
                        break;
                    case 69:
                        value = _enumerator.Current.ForceBottomCap;
                        break;
                    case 70:
                        value = _enumerator.Current.Shape;
                        break;
                    case 71:
                        value = _enumerator.Current.ShapeType;
                        break;
                    case 72:
                        value = _enumerator.Current.FillColour;
                        break;
                    case 73:
                        value = _enumerator.Current.FillPatternType;
                        break;
                    case 74:
                        value = _enumerator.Current.PointOfPurchaseDescription;
                        break;
                    case 75:
                        value = _enumerator.Current.ShortDescription;
                        break;
                    case 76:
                        value = _enumerator.Current.Subcategory;
                        break;
                    case 77:
                        value = _enumerator.Current.CustomerStatus;
                        break;
                    case 78:
                        value = _enumerator.Current.Colour;
                        break;
                    case 79:
                        value = _enumerator.Current.Flavour;
                        break;
                    case 80:
                        value = _enumerator.Current.PackagingShape;
                        break;
                    case 81:
                        value = _enumerator.Current.PackagingType;
                        break;
                    case 82:
                        value = _enumerator.Current.CountryOfOrigin;
                        break;
                    case 83:
                        value = _enumerator.Current.CountryOfProcessing;
                        break;
                    case 84:
                        value = _enumerator.Current.ShelfLife;
                        break;
                    case 85:
                        value = _enumerator.Current.DeliveryFrequencyDays;
                        break;
                    case 86:
                        value = _enumerator.Current.DeliveryMethod;
                        break;
                    case 87:
                        value = _enumerator.Current.Brand;
                        break;
                    case 88:
                        value = _enumerator.Current.VendorCode;
                        break;
                    case 89:
                        value = _enumerator.Current.Vendor;
                        break;
                    case 90:
                        value = _enumerator.Current.ManufacturerCode;
                        break;
                    case 91:
                        value = _enumerator.Current.Manufacturer;
                        break;
                    case 92:
                        value = _enumerator.Current.Size;
                        break;
                    case 93:
                        value = _enumerator.Current.UnitOfMeasure;
                        break;
                    case 94:
                        value = _enumerator.Current.DateIntroduced;
                        break;
                    case 95:
                        value = _enumerator.Current.DateDiscontinued;
                        break;
                    case 96:
                        value = _enumerator.Current.DateEffective;
                        break;
                    case 97:
                        value = _enumerator.Current.Health;
                        break;
                    case 98:
                        value = _enumerator.Current.CorporateCode;
                        break;
                    case 99:
                        value = _enumerator.Current.Barcode;
                        break;
                    case 100:
                        value = _enumerator.Current.SellPrice;
                        break;
                    case 101:
                        value = _enumerator.Current.SellPackCount;
                        break;
                    case 102:
                        value = _enumerator.Current.SellPackDescription;
                        break;
                    case 103:
                        value = _enumerator.Current.RecommendedRetailPrice;
                        break;
                    case 104:
                        value = _enumerator.Current.ManufacturerRecommendedRetailPrice;
                        break;
                    case 105:
                        value = _enumerator.Current.CostPrice;
                        break;
                    case 106:
                        value = _enumerator.Current.CaseCost;
                        break;
                    case 107:
                        value = _enumerator.Current.TaxRate;
                        break;
                    case 108:
                        value = _enumerator.Current.ConsumerInformation;
                        break;
                    case 109:
                        value = _enumerator.Current.Texture;
                        break;
                    case 110:
                        value = _enumerator.Current.StyleNumber;
                        break;
                    case 111:
                        value = _enumerator.Current.Pattern;
                        break;
                    case 112:
                        value = _enumerator.Current.Model;
                        break;
                    case 113:
                        value = _enumerator.Current.GarmentType;
                        break;
                    case 114:
                        value = _enumerator.Current.IsPrivateLabel;
                        break;
                    case 115:
                        value = _enumerator.Current.IsNewProduct;
                        break;
                    case 116:
                        value = _enumerator.Current.FinancialGroupCode;
                        break;
                    case 117:
                        value = _enumerator.Current.FinancialGroupName;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        private class BulkCopyIntDataReader : IDataReader
        {
            #region Fields
            IEnumerator<Int32> _enumerator; // an enumerator containing the values to write to the database
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyIntDataReader(IEnumerable<Int32> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = _enumerator.Current;
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        private class BulkCopyTextDataReader : IDataReader
        {
            #region Fields
            IEnumerator<String> _enumerator; // an enumerator containing the values to write to the database
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyTextDataReader(IEnumerable<String> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = _enumerator.Current;
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static ProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ProductRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.ProductEntityId]),
                ProductGroupId = (Int32?)GetValue(dr[FieldNames.ProductGroupId]),
                ReplacementProductId = (Int32?)GetValue(dr[FieldNames.ProductReplacementProductId]),
                Gtin = (String)GetValue(dr[FieldNames.ProductGtin]),
                Name = (String)GetValue(dr[FieldNames.ProductName]),
                Height = (Single)GetValue(dr[FieldNames.ProductHeight]),
                Width = (Single)GetValue(dr[FieldNames.ProductWidth]),
                Depth = (Single)GetValue(dr[FieldNames.ProductDepth]),
                DisplayHeight = (Single)GetValue(dr[FieldNames.ProductDisplayHeight]),
                DisplayWidth = (Single)GetValue(dr[FieldNames.ProductDisplayWidth]),
                DisplayDepth = (Single)GetValue(dr[FieldNames.ProductDisplayDepth]),
                AlternateHeight = (Single)GetValue(dr[FieldNames.ProductAlternateHeight]),
                AlternateWidth = (Single)GetValue(dr[FieldNames.ProductAlternateWidth]),
                AlternateDepth = (Single)GetValue(dr[FieldNames.ProductAlternateDepth]),
                PointOfPurchaseHeight = (Single)GetValue(dr[FieldNames.ProductPointOfPurchaseHeight]),
                PointOfPurchaseWidth = (Single)GetValue(dr[FieldNames.ProductPointOfPurchaseWidth]),
                PointOfPurchaseDepth = (Single)GetValue(dr[FieldNames.ProductPointOfPurchaseDepth]),
                NumberOfPegHoles = (Byte)GetValue(dr[FieldNames.ProductNumberOfPegHoles]),
                PegX = (Single)GetValue(dr[FieldNames.ProductPegX]),
                PegX2 = (Single)GetValue(dr[FieldNames.ProductPegX2]),
                PegX3 = (Single)GetValue(dr[FieldNames.ProductPegX3]),
                PegY = (Single)GetValue(dr[FieldNames.ProductPegY]),
                PegY2 = (Single)GetValue(dr[FieldNames.ProductPegY2]),
                PegY3 = (Single)GetValue(dr[FieldNames.ProductPegY3]),
                PegProngOffsetX = (Single)(dr[FieldNames.ProductPegProngOffsetX] is DBNull ? 0.0F : GetValue(dr[FieldNames.ProductPegProngOffsetX])),
                PegProngOffsetY = (Single)(dr[FieldNames.ProductPegProngOffsetY] is DBNull ? 0.0F : GetValue(dr[FieldNames.ProductPegProngOffsetY])),
                PegDepth = (Single)GetValue(dr[FieldNames.ProductPegDepth]),
                SqueezeHeight = (Single)GetValue(dr[FieldNames.ProductSqueezeHeight]),
                SqueezeWidth = (Single)GetValue(dr[FieldNames.ProductSqueezeWidth]),
                SqueezeDepth = (Single)GetValue(dr[FieldNames.ProductSqueezeDepth]),
                NestingHeight = (Single)GetValue(dr[FieldNames.ProductNestingHeight]),
                NestingWidth = (Single)GetValue(dr[FieldNames.ProductNestingWidth]),
                NestingDepth = (Single)GetValue(dr[FieldNames.ProductNestingDepth]),
                CasePackUnits = (Int16)GetValue(dr[FieldNames.ProductCasePackUnits]),
                CaseHigh = (Byte)GetValue(dr[FieldNames.ProductCaseHigh]),
                CaseWide = (Byte)GetValue(dr[FieldNames.ProductCaseWide]),
                CaseDeep = (Byte)GetValue(dr[FieldNames.ProductCaseDeep]),
                CaseHeight = (Single)GetValue(dr[FieldNames.ProductCaseHeight]),
                CaseWidth = (Single)GetValue(dr[FieldNames.ProductCaseWidth]),
                CaseDepth = (Single)GetValue(dr[FieldNames.ProductCaseDepth]),
                MaxStack = (Byte)GetValue(dr[FieldNames.ProductMaxStack]),
                MaxTopCap = (Byte)GetValue(dr[FieldNames.ProductMaxTopCap]),
                MaxRightCap = (Byte)GetValue(dr[FieldNames.ProductMaxRightCap]),
                MinDeep = (Byte)GetValue(dr[FieldNames.ProductMinDeep]),
                MaxDeep = (Byte)GetValue(dr[FieldNames.ProductMaxDeep]),
                TrayPackUnits = (Int16)GetValue(dr[FieldNames.ProductTrayPackUnits]),
                TrayHigh = (Byte)GetValue(dr[FieldNames.ProductTrayHigh]),
                TrayWide = (Byte)GetValue(dr[FieldNames.ProductTrayWide]),
                TrayDeep = (Byte)GetValue(dr[FieldNames.ProductTrayDeep]),
                TrayHeight = (Single)GetValue(dr[FieldNames.ProductTrayHeight]),
                TrayWidth = (Single)GetValue(dr[FieldNames.ProductTrayWidth]),
                TrayDepth = (Single)GetValue(dr[FieldNames.ProductTrayDepth]),
                TrayThickHeight = (Single)GetValue(dr[FieldNames.ProductTrayThickHeight]),
                TrayThickWidth = (Single)GetValue(dr[FieldNames.ProductTrayThickWidth]),
                TrayThickDepth = (Single)GetValue(dr[FieldNames.ProductTrayThickDepth]),
                FrontOverhang = (Single)GetValue(dr[FieldNames.ProductFrontOverhang]),
                FingerSpaceAbove = (Single)GetValue(dr[FieldNames.ProductFingerSpaceAbove]),
                FingerSpaceToTheSide = (Single)GetValue(dr[FieldNames.ProductFingerSpaceToTheSide]),
                StatusType = (Byte)GetValue(dr[FieldNames.ProductStatusType]),
                OrientationType = (Byte)GetValue(dr[FieldNames.ProductOrientationType]),
                MerchandisingStyle = (Byte)GetValue(dr[FieldNames.ProductMerchandisingStyle]),
                IsFrontOnly = (Boolean)GetValue(dr[FieldNames.ProductIsFrontOnly]),
                IsPlaceHolderProduct = (Boolean)GetValue(dr[FieldNames.ProductIsPlaceHolderProduct]),
                IsActive = (Boolean)GetValue(dr[FieldNames.ProductIsActive]),
                CanBreakTrayUp = (Boolean)GetValue(dr[FieldNames.ProductCanBreakTrayUp]),
                CanBreakTrayDown = (Boolean)GetValue(dr[FieldNames.ProductCanBreakTrayDown]),
                CanBreakTrayBack = (Boolean)GetValue(dr[FieldNames.ProductCanBreakTrayBack]),
                CanBreakTrayTop = (Boolean)GetValue(dr[FieldNames.ProductCanBreakTrayTop]),
                ForceMiddleCap = (Boolean)GetValue(dr[FieldNames.ProductForceMiddleCap]),
                ForceBottomCap = (Boolean)GetValue(dr[FieldNames.ProductForceBottomCap]),
                Shape = (String)GetValue(dr[FieldNames.ProductShape]),
                ShapeType = (Byte)GetValue(dr[FieldNames.ProductShapeType]),
                FillColour = (Int32)GetValue(dr[FieldNames.ProductFillColour]),
                FillPatternType = (Byte)GetValue(dr[FieldNames.ProductFillPatternType]),
                PointOfPurchaseDescription = (String)GetValue(dr[FieldNames.ProductPointOfPurchaseDescription]),
                ShortDescription = (String)GetValue(dr[FieldNames.ProductShortDescription]),
                Subcategory = (String)GetValue(dr[FieldNames.ProductSubcategory]),
                CustomerStatus = (String)GetValue(dr[FieldNames.ProductCustomerStatus]),
                Colour = (String)GetValue(dr[FieldNames.ProductColour]),
                Flavour = (String)GetValue(dr[FieldNames.ProductFlavour]),
                PackagingShape = (String)GetValue(dr[FieldNames.ProductPackagingShape]),
                PackagingType = (String)GetValue(dr[FieldNames.ProductPackagingType]),
                CountryOfOrigin = (String)GetValue(dr[FieldNames.ProductCountryOfOrigin]),
                CountryOfProcessing = (String)GetValue(dr[FieldNames.ProductCountryOfProcessing]),
                ShelfLife = (Int16)GetValue(dr[FieldNames.ProductShelfLife]),
                DeliveryFrequencyDays = (Single?)GetValue(dr[FieldNames.ProductDeliveryFrequencyDays]),
                DeliveryMethod = (String)GetValue(dr[FieldNames.ProductDeliveryMethod]),
                Brand = (String)GetValue(dr[FieldNames.ProductBrand]),
                VendorCode = (String)GetValue(dr[FieldNames.ProductVendorCode]),
                Vendor = (String)GetValue(dr[FieldNames.ProductVendor]),
                ManufacturerCode = (String)GetValue(dr[FieldNames.ProductManufacturerCode]),
                Manufacturer = (String)GetValue(dr[FieldNames.ProductManufacturer]),
                Size = (String)GetValue(dr[FieldNames.ProductSize]),
                UnitOfMeasure = (String)GetValue(dr[FieldNames.ProductUnitOfMeasure]),
                DateIntroduced = (DateTime?)GetValue(dr[FieldNames.ProductDateIntroduced]),
                DateDiscontinued = (DateTime?)GetValue(dr[FieldNames.ProductDateDiscontinued]),
                DateEffective = (DateTime?)GetValue(dr[FieldNames.ProductDateEffective]),
                Health = (String)GetValue(dr[FieldNames.ProductHealth]),
                CorporateCode = (String)GetValue(dr[FieldNames.ProductCorporateCode]),
                Barcode = (String)GetValue(dr[FieldNames.ProductBarcode]),
                SellPrice = (Single?)GetValue(dr[FieldNames.ProductSellPrice]),
                SellPackCount = (Int16?)GetValue(dr[FieldNames.ProductSellPackCount]),
                SellPackDescription = (String)GetValue(dr[FieldNames.ProductSellPackDescription]),
                RecommendedRetailPrice = (Single?)GetValue(dr[FieldNames.ProductRecommendedRetailPrice]),
                ManufacturerRecommendedRetailPrice = (Single?)GetValue(dr[FieldNames.ProductManufacturerRecommendedRetailPrice]),
                CostPrice = (Single?)GetValue(dr[FieldNames.ProductCostPrice]),
                CaseCost = (Single?)GetValue(dr[FieldNames.ProductCaseCost]),
                TaxRate = (Single?)GetValue(dr[FieldNames.ProductTaxRate]),
                ConsumerInformation = (String)GetValue(dr[FieldNames.ProductConsumerInformation]),
                Texture = (String)GetValue(dr[FieldNames.ProductTexture]),
                StyleNumber = (Int16?)GetValue(dr[FieldNames.ProductStyleNumber]),
                Pattern = (String)GetValue(dr[FieldNames.ProductPattern]),
                Model = (String)GetValue(dr[FieldNames.ProductModel]),
                GarmentType = (String)GetValue(dr[FieldNames.ProductGarmentType]),
                IsPrivateLabel = (Boolean)GetValue(dr[FieldNames.ProductIsPrivateLabel]),
                IsNewProduct = (Boolean)GetValue(dr[FieldNames.ProductIsNew]),
                FinancialGroupCode = (String)GetValue(dr[FieldNames.ProductFinancialGroupCode]),
                FinancialGroupName = (String)GetValue(dr[FieldNames.ProductFinancialGroupName]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ProductDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ProductDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.ProductDateDeleted])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ProductDto FetchById(Int32 id)
        {
            ProductDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ProductDto> FetchByEntityId(Int32 entityId)
        {
            List<ProductDto> dtoList = new List<ProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<ProductDto> FetchByProductGroupId(Int32 productGroupId)
        {
            List<ProductDto> dtoList = new List<ProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByProductGroupId))
                {
                    //Other properties, 
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, productGroupId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id and product gtins
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <param name="isROM"></param>
        /// <returns></returns>
        public IEnumerable<ProductDto> FetchByProductIds(IEnumerable<Int32> productIds)
        {
            List<ProductDto> dtoList = new List<ProductDto>();

            // create a temp table for products
            CreateProductTempTable(productIds);

            try
            {
                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByProductIds))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop the temporary table
            DropProductTempTable();

            // and return the items
            return dtoList;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<ProductDto> FetchByProductUniverseId(Int32 productUniverseId)
        {
            List<ProductDto> dtoList = new List<ProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByProductUniverseId))
                {
                    //Other properties, 
                    CreateParameter(command, FieldNames.ProductUniverseId, SqlDbType.Int, productUniverseId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id and product gtins
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <param name="isROM"></param>
        /// <returns></returns>
        public IEnumerable<ProductDto> FetchByEntityIdProductGtins(Int32 entityId, IEnumerable<String> productGtins)
        {
            List<ProductDto> dtoList = new List<ProductDto>();
            String tmpTableName = "#tmpProducts";
            try
            {
                //create temp table
                CreateProductTempTable(_createGtinTableSql, tmpTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + tmpTableName + "]";
                    bulkCopy.WriteToServer(new BulkCopyTextDataReader(productGtins));
                }

                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByEntityIdProductGtins))
                {
                    // parameters
                    CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }

            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                DropProductTempTable();
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a list of distinct products for all product universes belonging to the given merch group id.
        /// </summary>
        /// <param name="merchGroupId"></param>
        /// <returns></returns>
        public IEnumerable<ProductDto> FetchByMerchandisingGroupId(Int32 merchGroupId)
        {
            List<ProductDto> dtoList = new List<ProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByMerchandisingGroupId))
                {
                    //Other properties, 
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, merchGroupId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a list of products for the given entity that match the search criteria.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public IEnumerable<ProductDto> FetchByEntityIdSearchText(Int32 entityId, String searchText)
        {
            List<ProductDto> dtoList = new List<ProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByEntityIdSearchText))
                {
                    //Other properties, 
                    CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.ProductSearchText, SqlDbType.NVarChar, searchText);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a list of products for the given entity that match the search criteria.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="multipleSearchText"></param>
        /// <returns></returns>
        public IEnumerable<ProductDto> FetchByEntityIdMultipleSearchText(Int32 entityId, IEnumerable<String> multipleSearchText)
        {
            List<ProductDto> dtoList = new List<ProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductFetchByEntityIdMultipleSearchText))
                {
                    CreateProductSearchTextTempTable(multipleSearchText);

                    //Other properties, 
                    CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropProductTempTable();
            }
            return dtoList;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ProductRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.ProductReplacementProductId, SqlDbType.Int, dto.ReplacementProductId);
                    CreateParameter(command, FieldNames.ProductGtin, SqlDbType.NVarChar, dto.Gtin);
                    CreateParameter(command, FieldNames.ProductName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.ProductWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.ProductDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.ProductDisplayHeight, SqlDbType.Real, dto.DisplayHeight);
                    CreateParameter(command, FieldNames.ProductDisplayWidth, SqlDbType.Real, dto.DisplayWidth);
                    CreateParameter(command, FieldNames.ProductDisplayDepth, SqlDbType.Real, dto.DisplayDepth);
                    CreateParameter(command, FieldNames.ProductAlternateHeight, SqlDbType.Real, dto.AlternateHeight);
                    CreateParameter(command, FieldNames.ProductAlternateWidth, SqlDbType.Real, dto.AlternateWidth);
                    CreateParameter(command, FieldNames.ProductAlternateDepth, SqlDbType.Real, dto.AlternateDepth);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseHeight, SqlDbType.Real, dto.PointOfPurchaseHeight);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseWidth, SqlDbType.Real, dto.PointOfPurchaseWidth);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseDepth, SqlDbType.Real, dto.PointOfPurchaseDepth);
                    CreateParameter(command, FieldNames.ProductNumberOfPegHoles, SqlDbType.TinyInt, dto.NumberOfPegHoles);
                    CreateParameter(command, FieldNames.ProductPegX, SqlDbType.Real, dto.PegX);
                    CreateParameter(command, FieldNames.ProductPegX2, SqlDbType.Real, dto.PegX2);
                    CreateParameter(command, FieldNames.ProductPegX3, SqlDbType.Real, dto.PegX3);
                    CreateParameter(command, FieldNames.ProductPegY, SqlDbType.Real, dto.PegY);
                    CreateParameter(command, FieldNames.ProductPegY2, SqlDbType.Real, dto.PegY2);
                    CreateParameter(command, FieldNames.ProductPegY3, SqlDbType.Real, dto.PegY3);
                    CreateParameter(command, FieldNames.ProductPegProngOffsetX, SqlDbType.Real, dto.PegProngOffsetX);
                    CreateParameter(command, FieldNames.ProductPegProngOffsetY, SqlDbType.Real, dto.PegProngOffsetY);
                    CreateParameter(command, FieldNames.ProductPegDepth, SqlDbType.Real, dto.PegDepth);
                    CreateParameter(command, FieldNames.ProductSqueezeHeight, SqlDbType.Real, dto.SqueezeHeight);
                    CreateParameter(command, FieldNames.ProductSqueezeWidth, SqlDbType.Real, dto.SqueezeWidth);
                    CreateParameter(command, FieldNames.ProductSqueezeDepth, SqlDbType.Real, dto.SqueezeDepth);
                    CreateParameter(command, FieldNames.ProductNestingHeight, SqlDbType.Real, dto.NestingHeight);
                    CreateParameter(command, FieldNames.ProductNestingWidth, SqlDbType.Real, dto.NestingWidth);
                    CreateParameter(command, FieldNames.ProductNestingDepth, SqlDbType.Real, dto.NestingDepth);
                    CreateParameter(command, FieldNames.ProductCasePackUnits, SqlDbType.SmallInt, dto.CasePackUnits);
                    CreateParameter(command, FieldNames.ProductCaseHigh, SqlDbType.TinyInt, dto.CaseHigh);
                    CreateParameter(command, FieldNames.ProductCaseWide, SqlDbType.TinyInt, dto.CaseWide);
                    CreateParameter(command, FieldNames.ProductCaseDeep, SqlDbType.TinyInt, dto.CaseDeep);
                    CreateParameter(command, FieldNames.ProductCaseHeight, SqlDbType.Real, dto.CaseHeight);
                    CreateParameter(command, FieldNames.ProductCaseWidth, SqlDbType.Real, dto.CaseWidth);
                    CreateParameter(command, FieldNames.ProductCaseDepth, SqlDbType.Real, dto.CaseDepth);
                    CreateParameter(command, FieldNames.ProductMaxStack, SqlDbType.TinyInt, dto.MaxStack);
                    CreateParameter(command, FieldNames.ProductMaxTopCap, SqlDbType.TinyInt, dto.MaxTopCap);
                    CreateParameter(command, FieldNames.ProductMaxRightCap, SqlDbType.TinyInt, dto.MaxRightCap);
                    CreateParameter(command, FieldNames.ProductMinDeep, SqlDbType.TinyInt, dto.MinDeep);
                    CreateParameter(command, FieldNames.ProductMaxDeep, SqlDbType.TinyInt, dto.MaxDeep);
                    CreateParameter(command, FieldNames.ProductTrayPackUnits, SqlDbType.SmallInt, dto.TrayPackUnits);
                    CreateParameter(command, FieldNames.ProductTrayHigh, SqlDbType.TinyInt, dto.TrayHigh);
                    CreateParameter(command, FieldNames.ProductTrayWide, SqlDbType.TinyInt, dto.TrayWide);
                    CreateParameter(command, FieldNames.ProductTrayDeep, SqlDbType.TinyInt, dto.TrayDeep);
                    CreateParameter(command, FieldNames.ProductTrayHeight, SqlDbType.Real, dto.TrayHeight);
                    CreateParameter(command, FieldNames.ProductTrayWidth, SqlDbType.Real, dto.TrayWidth);
                    CreateParameter(command, FieldNames.ProductTrayDepth, SqlDbType.Real, dto.TrayDepth);
                    CreateParameter(command, FieldNames.ProductTrayThickHeight, SqlDbType.Real, dto.TrayThickHeight);
                    CreateParameter(command, FieldNames.ProductTrayThickWidth, SqlDbType.Real, dto.TrayThickWidth);
                    CreateParameter(command, FieldNames.ProductTrayThickDepth, SqlDbType.Real, dto.TrayThickDepth);
                    CreateParameter(command, FieldNames.ProductFrontOverhang, SqlDbType.Real, dto.FrontOverhang);
                    CreateParameter(command, FieldNames.ProductFingerSpaceAbove, SqlDbType.Real, dto.FingerSpaceAbove);
                    CreateParameter(command, FieldNames.ProductFingerSpaceToTheSide, SqlDbType.Real, dto.FingerSpaceToTheSide);
                    CreateParameter(command, FieldNames.ProductStatusType, SqlDbType.TinyInt, dto.StatusType);
                    CreateParameter(command, FieldNames.ProductOrientationType, SqlDbType.TinyInt, dto.OrientationType);
                    CreateParameter(command, FieldNames.ProductMerchandisingStyle, SqlDbType.TinyInt, dto.MerchandisingStyle);
                    CreateParameter(command, FieldNames.ProductIsFrontOnly, SqlDbType.Bit, dto.IsFrontOnly);
                    CreateParameter(command, FieldNames.ProductIsPlaceHolderProduct, SqlDbType.Bit, dto.IsPlaceHolderProduct);
                    CreateParameter(command, FieldNames.ProductIsActive, SqlDbType.Bit, dto.IsActive);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayUp, SqlDbType.Bit, dto.CanBreakTrayUp);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayDown, SqlDbType.Bit, dto.CanBreakTrayDown);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayBack, SqlDbType.Bit, dto.CanBreakTrayBack);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayTop, SqlDbType.Bit, dto.CanBreakTrayTop);
                    CreateParameter(command, FieldNames.ProductForceMiddleCap, SqlDbType.Bit, dto.ForceMiddleCap);
                    CreateParameter(command, FieldNames.ProductForceBottomCap, SqlDbType.Bit, dto.ForceBottomCap);
                    CreateParameter(command, FieldNames.ProductShape, SqlDbType.NVarChar, dto.Shape);
                    CreateParameter(command, FieldNames.ProductShapeType, SqlDbType.TinyInt, dto.ShapeType);
                    CreateParameter(command, FieldNames.ProductFillColour, SqlDbType.Int, dto.FillColour);
                    CreateParameter(command, FieldNames.ProductFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseDescription, SqlDbType.NVarChar, dto.PointOfPurchaseDescription);
                    CreateParameter(command, FieldNames.ProductShortDescription, SqlDbType.NVarChar, dto.ShortDescription);
                    CreateParameter(command, FieldNames.ProductSubcategory, SqlDbType.NVarChar, dto.Subcategory);
                    CreateParameter(command, FieldNames.ProductCustomerStatus, SqlDbType.NVarChar, dto.CustomerStatus);
                    CreateParameter(command, FieldNames.ProductColour, SqlDbType.NVarChar, dto.Colour);
                    CreateParameter(command, FieldNames.ProductFlavour, SqlDbType.NVarChar, dto.Flavour);
                    CreateParameter(command, FieldNames.ProductPackagingShape, SqlDbType.NVarChar, dto.PackagingShape);
                    CreateParameter(command, FieldNames.ProductPackagingType, SqlDbType.NVarChar, dto.PackagingType);
                    CreateParameter(command, FieldNames.ProductCountryOfOrigin, SqlDbType.NVarChar, dto.CountryOfOrigin);
                    CreateParameter(command, FieldNames.ProductCountryOfProcessing, SqlDbType.NVarChar, dto.CountryOfProcessing);
                    CreateParameter(command, FieldNames.ProductShelfLife, SqlDbType.SmallInt, dto.ShelfLife);
                    CreateParameter(command, FieldNames.ProductDeliveryFrequencyDays, SqlDbType.Real, dto.DeliveryFrequencyDays);
                    CreateParameter(command, FieldNames.ProductDeliveryMethod, SqlDbType.NVarChar, dto.DeliveryMethod);
                    CreateParameter(command, FieldNames.ProductBrand, SqlDbType.NVarChar, dto.Brand);
                    CreateParameter(command, FieldNames.ProductVendorCode, SqlDbType.NVarChar, dto.VendorCode);
                    CreateParameter(command, FieldNames.ProductVendor, SqlDbType.NVarChar, dto.Vendor);
                    CreateParameter(command, FieldNames.ProductManufacturerCode, SqlDbType.NVarChar, dto.ManufacturerCode);
                    CreateParameter(command, FieldNames.ProductManufacturer, SqlDbType.NVarChar, dto.Manufacturer);
                    CreateParameter(command, FieldNames.ProductSize, SqlDbType.NVarChar, dto.Size);
                    CreateParameter(command, FieldNames.ProductUnitOfMeasure, SqlDbType.NVarChar, dto.UnitOfMeasure);
                    CreateParameter(command, FieldNames.ProductDateIntroduced, SqlDbType.DateTime, dto.DateIntroduced);
                    CreateParameter(command, FieldNames.ProductDateDiscontinued, SqlDbType.DateTime, dto.DateDiscontinued);
                    CreateParameter(command, FieldNames.ProductDateEffective, SqlDbType.DateTime, dto.DateEffective);
                    CreateParameter(command, FieldNames.ProductHealth, SqlDbType.NVarChar, dto.Health);
                    CreateParameter(command, FieldNames.ProductCorporateCode, SqlDbType.NVarChar, dto.CorporateCode);
                    CreateParameter(command, FieldNames.ProductBarcode, SqlDbType.NVarChar, dto.Barcode);
                    CreateParameter(command, FieldNames.ProductSellPrice, SqlDbType.Real, dto.SellPrice);
                    CreateParameter(command, FieldNames.ProductSellPackCount, SqlDbType.SmallInt, dto.SellPackCount);
                    CreateParameter(command, FieldNames.ProductSellPackDescription, SqlDbType.NVarChar, dto.SellPackDescription);
                    CreateParameter(command, FieldNames.ProductRecommendedRetailPrice, SqlDbType.Real, dto.RecommendedRetailPrice);
                    CreateParameter(command, FieldNames.ProductManufacturerRecommendedRetailPrice, SqlDbType.Real, dto.ManufacturerRecommendedRetailPrice);
                    CreateParameter(command, FieldNames.ProductCostPrice, SqlDbType.Real, dto.CostPrice);
                    CreateParameter(command, FieldNames.ProductCaseCost, SqlDbType.Real, dto.CaseCost);
                    CreateParameter(command, FieldNames.ProductTaxRate, SqlDbType.Real, dto.TaxRate);
                    CreateParameter(command, FieldNames.ProductConsumerInformation, SqlDbType.NVarChar, dto.ConsumerInformation);
                    CreateParameter(command, FieldNames.ProductTexture, SqlDbType.NVarChar, dto.Texture);
                    CreateParameter(command, FieldNames.ProductStyleNumber, SqlDbType.SmallInt, dto.StyleNumber);
                    CreateParameter(command, FieldNames.ProductPattern, SqlDbType.NVarChar, dto.Pattern);
                    CreateParameter(command, FieldNames.ProductModel, SqlDbType.NVarChar, dto.Model);
                    CreateParameter(command, FieldNames.ProductGarmentType, SqlDbType.NVarChar, dto.GarmentType);
                    CreateParameter(command, FieldNames.ProductIsPrivateLabel, SqlDbType.Bit, dto.IsPrivateLabel);
                    CreateParameter(command, FieldNames.ProductIsNew, SqlDbType.Bit, dto.IsNewProduct);
                    CreateParameter(command, FieldNames.ProductFinancialGroupCode, SqlDbType.NVarChar, dto.FinancialGroupCode);
                    CreateParameter(command, FieldNames.ProductFinancialGroupName, SqlDbType.NVarChar, dto.FinancialGroupName);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ProductRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.ProductDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.ProductDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.ProductReplacementProductId, SqlDbType.Int, dto.ReplacementProductId);
                    CreateParameter(command, FieldNames.ProductGtin, SqlDbType.NVarChar, dto.Gtin);
                    CreateParameter(command, FieldNames.ProductName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.ProductWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.ProductDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.ProductDisplayHeight, SqlDbType.Real, dto.DisplayHeight);
                    CreateParameter(command, FieldNames.ProductDisplayWidth, SqlDbType.Real, dto.DisplayWidth);
                    CreateParameter(command, FieldNames.ProductDisplayDepth, SqlDbType.Real, dto.DisplayDepth);
                    CreateParameter(command, FieldNames.ProductAlternateHeight, SqlDbType.Real, dto.AlternateHeight);
                    CreateParameter(command, FieldNames.ProductAlternateWidth, SqlDbType.Real, dto.AlternateWidth);
                    CreateParameter(command, FieldNames.ProductAlternateDepth, SqlDbType.Real, dto.AlternateDepth);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseHeight, SqlDbType.Real, dto.PointOfPurchaseHeight);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseWidth, SqlDbType.Real, dto.PointOfPurchaseWidth);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseDepth, SqlDbType.Real, dto.PointOfPurchaseDepth);
                    CreateParameter(command, FieldNames.ProductNumberOfPegHoles, SqlDbType.TinyInt, dto.NumberOfPegHoles);
                    CreateParameter(command, FieldNames.ProductPegX, SqlDbType.Real, dto.PegX);
                    CreateParameter(command, FieldNames.ProductPegX2, SqlDbType.Real, dto.PegX2);
                    CreateParameter(command, FieldNames.ProductPegX3, SqlDbType.Real, dto.PegX3);
                    CreateParameter(command, FieldNames.ProductPegY, SqlDbType.Real, dto.PegY);
                    CreateParameter(command, FieldNames.ProductPegY2, SqlDbType.Real, dto.PegY2);
                    CreateParameter(command, FieldNames.ProductPegY3, SqlDbType.Real, dto.PegY3);
                    CreateParameter(command, FieldNames.ProductPegProngOffsetX, SqlDbType.Real, dto.PegProngOffsetX);
                    CreateParameter(command, FieldNames.ProductPegProngOffsetY, SqlDbType.Real, dto.PegProngOffsetY);
                    CreateParameter(command, FieldNames.ProductPegDepth, SqlDbType.Real, dto.PegDepth);
                    CreateParameter(command, FieldNames.ProductSqueezeHeight, SqlDbType.Real, dto.SqueezeHeight);
                    CreateParameter(command, FieldNames.ProductSqueezeWidth, SqlDbType.Real, dto.SqueezeWidth);
                    CreateParameter(command, FieldNames.ProductSqueezeDepth, SqlDbType.Real, dto.SqueezeDepth);
                    CreateParameter(command, FieldNames.ProductNestingHeight, SqlDbType.Real, dto.NestingHeight);
                    CreateParameter(command, FieldNames.ProductNestingWidth, SqlDbType.Real, dto.NestingWidth);
                    CreateParameter(command, FieldNames.ProductNestingDepth, SqlDbType.Real, dto.NestingDepth);
                    CreateParameter(command, FieldNames.ProductCasePackUnits, SqlDbType.SmallInt, dto.CasePackUnits);
                    CreateParameter(command, FieldNames.ProductCaseHigh, SqlDbType.TinyInt, dto.CaseHigh);
                    CreateParameter(command, FieldNames.ProductCaseWide, SqlDbType.TinyInt, dto.CaseWide);
                    CreateParameter(command, FieldNames.ProductCaseDeep, SqlDbType.TinyInt, dto.CaseDeep);
                    CreateParameter(command, FieldNames.ProductCaseHeight, SqlDbType.Real, dto.CaseHeight);
                    CreateParameter(command, FieldNames.ProductCaseWidth, SqlDbType.Real, dto.CaseWidth);
                    CreateParameter(command, FieldNames.ProductCaseDepth, SqlDbType.Real, dto.CaseDepth);
                    CreateParameter(command, FieldNames.ProductMaxStack, SqlDbType.TinyInt, dto.MaxStack);
                    CreateParameter(command, FieldNames.ProductMaxTopCap, SqlDbType.TinyInt, dto.MaxTopCap);
                    CreateParameter(command, FieldNames.ProductMaxRightCap, SqlDbType.TinyInt, dto.MaxRightCap);
                    CreateParameter(command, FieldNames.ProductMinDeep, SqlDbType.TinyInt, dto.MinDeep);
                    CreateParameter(command, FieldNames.ProductMaxDeep, SqlDbType.TinyInt, dto.MaxDeep);
                    CreateParameter(command, FieldNames.ProductTrayPackUnits, SqlDbType.SmallInt, dto.TrayPackUnits);
                    CreateParameter(command, FieldNames.ProductTrayHigh, SqlDbType.TinyInt, dto.TrayHigh);
                    CreateParameter(command, FieldNames.ProductTrayWide, SqlDbType.TinyInt, dto.TrayWide);
                    CreateParameter(command, FieldNames.ProductTrayDeep, SqlDbType.TinyInt, dto.TrayDeep);
                    CreateParameter(command, FieldNames.ProductTrayHeight, SqlDbType.Real, dto.TrayHeight);
                    CreateParameter(command, FieldNames.ProductTrayWidth, SqlDbType.Real, dto.TrayWidth);
                    CreateParameter(command, FieldNames.ProductTrayDepth, SqlDbType.Real, dto.TrayDepth);
                    CreateParameter(command, FieldNames.ProductTrayThickHeight, SqlDbType.Real, dto.TrayThickHeight);
                    CreateParameter(command, FieldNames.ProductTrayThickWidth, SqlDbType.Real, dto.TrayThickWidth);
                    CreateParameter(command, FieldNames.ProductTrayThickDepth, SqlDbType.Real, dto.TrayThickDepth);
                    CreateParameter(command, FieldNames.ProductFrontOverhang, SqlDbType.Real, dto.FrontOverhang);
                    CreateParameter(command, FieldNames.ProductFingerSpaceAbove, SqlDbType.Real, dto.FingerSpaceAbove);
                    CreateParameter(command, FieldNames.ProductFingerSpaceToTheSide, SqlDbType.Real, dto.FingerSpaceToTheSide);
                    CreateParameter(command, FieldNames.ProductStatusType, SqlDbType.TinyInt, dto.StatusType);
                    CreateParameter(command, FieldNames.ProductOrientationType, SqlDbType.TinyInt, dto.OrientationType);
                    CreateParameter(command, FieldNames.ProductMerchandisingStyle, SqlDbType.TinyInt, dto.MerchandisingStyle);
                    CreateParameter(command, FieldNames.ProductIsFrontOnly, SqlDbType.Bit, dto.IsFrontOnly);
                    CreateParameter(command, FieldNames.ProductIsPlaceHolderProduct, SqlDbType.Bit, dto.IsPlaceHolderProduct);
                    CreateParameter(command, FieldNames.ProductIsActive, SqlDbType.Bit, dto.IsActive);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayUp, SqlDbType.Bit, dto.CanBreakTrayUp);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayDown, SqlDbType.Bit, dto.CanBreakTrayDown);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayBack, SqlDbType.Bit, dto.CanBreakTrayBack);
                    CreateParameter(command, FieldNames.ProductCanBreakTrayTop, SqlDbType.Bit, dto.CanBreakTrayTop);
                    CreateParameter(command, FieldNames.ProductForceMiddleCap, SqlDbType.Bit, dto.ForceMiddleCap);
                    CreateParameter(command, FieldNames.ProductForceBottomCap, SqlDbType.Bit, dto.ForceBottomCap);
                    CreateParameter(command, FieldNames.ProductShape, SqlDbType.NVarChar, dto.Shape);
                    CreateParameter(command, FieldNames.ProductShapeType, SqlDbType.TinyInt, dto.ShapeType);
                    CreateParameter(command, FieldNames.ProductFillColour, SqlDbType.Int, dto.FillColour);
                    CreateParameter(command, FieldNames.ProductFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);
                    CreateParameter(command, FieldNames.ProductPointOfPurchaseDescription, SqlDbType.NVarChar, dto.PointOfPurchaseDescription);
                    CreateParameter(command, FieldNames.ProductShortDescription, SqlDbType.NVarChar, dto.ShortDescription);
                    CreateParameter(command, FieldNames.ProductSubcategory, SqlDbType.NVarChar, dto.Subcategory);
                    CreateParameter(command, FieldNames.ProductCustomerStatus, SqlDbType.NVarChar, dto.CustomerStatus);
                    CreateParameter(command, FieldNames.ProductColour, SqlDbType.NVarChar, dto.Colour);
                    CreateParameter(command, FieldNames.ProductFlavour, SqlDbType.NVarChar, dto.Flavour);
                    CreateParameter(command, FieldNames.ProductPackagingShape, SqlDbType.NVarChar, dto.PackagingShape);
                    CreateParameter(command, FieldNames.ProductPackagingType, SqlDbType.NVarChar, dto.PackagingType);
                    CreateParameter(command, FieldNames.ProductCountryOfOrigin, SqlDbType.NVarChar, dto.CountryOfOrigin);
                    CreateParameter(command, FieldNames.ProductCountryOfProcessing, SqlDbType.NVarChar, dto.CountryOfProcessing);
                    CreateParameter(command, FieldNames.ProductShelfLife, SqlDbType.SmallInt, dto.ShelfLife);
                    CreateParameter(command, FieldNames.ProductDeliveryFrequencyDays, SqlDbType.Real, dto.DeliveryFrequencyDays);
                    CreateParameter(command, FieldNames.ProductDeliveryMethod, SqlDbType.NVarChar, dto.DeliveryMethod);
                    CreateParameter(command, FieldNames.ProductBrand, SqlDbType.NVarChar, dto.Brand);
                    CreateParameter(command, FieldNames.ProductVendorCode, SqlDbType.NVarChar, dto.VendorCode);
                    CreateParameter(command, FieldNames.ProductVendor, SqlDbType.NVarChar, dto.Vendor);
                    CreateParameter(command, FieldNames.ProductManufacturerCode, SqlDbType.NVarChar, dto.ManufacturerCode);
                    CreateParameter(command, FieldNames.ProductManufacturer, SqlDbType.NVarChar, dto.Manufacturer);
                    CreateParameter(command, FieldNames.ProductSize, SqlDbType.NVarChar, dto.Size);
                    CreateParameter(command, FieldNames.ProductUnitOfMeasure, SqlDbType.NVarChar, dto.UnitOfMeasure);
                    CreateParameter(command, FieldNames.ProductDateIntroduced, SqlDbType.DateTime, dto.DateIntroduced);
                    CreateParameter(command, FieldNames.ProductDateDiscontinued, SqlDbType.DateTime, dto.DateDiscontinued);
                    CreateParameter(command, FieldNames.ProductDateEffective, SqlDbType.DateTime, dto.DateEffective);
                    CreateParameter(command, FieldNames.ProductHealth, SqlDbType.NVarChar, dto.Health);
                    CreateParameter(command, FieldNames.ProductCorporateCode, SqlDbType.NVarChar, dto.CorporateCode);
                    CreateParameter(command, FieldNames.ProductBarcode, SqlDbType.NVarChar, dto.Barcode);
                    CreateParameter(command, FieldNames.ProductSellPrice, SqlDbType.Real, dto.SellPrice);
                    CreateParameter(command, FieldNames.ProductSellPackCount, SqlDbType.SmallInt, dto.SellPackCount);
                    CreateParameter(command, FieldNames.ProductSellPackDescription, SqlDbType.NVarChar, dto.SellPackDescription);
                    CreateParameter(command, FieldNames.ProductRecommendedRetailPrice, SqlDbType.Real, dto.RecommendedRetailPrice);
                    CreateParameter(command, FieldNames.ProductManufacturerRecommendedRetailPrice, SqlDbType.Real, dto.ManufacturerRecommendedRetailPrice);
                    CreateParameter(command, FieldNames.ProductCostPrice, SqlDbType.Real, dto.CostPrice);
                    CreateParameter(command, FieldNames.ProductCaseCost, SqlDbType.Real, dto.CaseCost);
                    CreateParameter(command, FieldNames.ProductTaxRate, SqlDbType.Real, dto.TaxRate);
                    CreateParameter(command, FieldNames.ProductConsumerInformation, SqlDbType.NVarChar, dto.ConsumerInformation);
                    CreateParameter(command, FieldNames.ProductTexture, SqlDbType.NVarChar, dto.Texture);
                    CreateParameter(command, FieldNames.ProductStyleNumber, SqlDbType.SmallInt, dto.StyleNumber);
                    CreateParameter(command, FieldNames.ProductPattern, SqlDbType.NVarChar, dto.Pattern);
                    CreateParameter(command, FieldNames.ProductModel, SqlDbType.NVarChar, dto.Model);
                    CreateParameter(command, FieldNames.ProductGarmentType, SqlDbType.NVarChar, dto.GarmentType);
                    CreateParameter(command, FieldNames.ProductIsPrivateLabel, SqlDbType.Bit, dto.IsPrivateLabel);
                    CreateParameter(command, FieldNames.ProductIsNew, SqlDbType.Bit, dto.IsNewProduct);
                    CreateParameter(command, FieldNames.ProductFinancialGroupCode, SqlDbType.NVarChar, dto.FinancialGroupCode);
                    CreateParameter(command, FieldNames.ProductFinancialGroupName, SqlDbType.NVarChar, dto.FinancialGroupName);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<ProductDto> dtoList, ProductIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new ProductIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<ProductDtoKey, ProductDto> keyedDtoList = new Dictionary<ProductDtoKey, ProductDto>();
            foreach (ProductDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    ProductBulkCopyDataReader dr = new ProductBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<ProductDtoKey, Tuple<Int32, RowVersion>> ids = new Dictionary<ProductDtoKey, Tuple<Int32, RowVersion>>();
                using (DalCommand command = CreateCommand(ProcedureNames.ProductBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.ProductSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Tuple<Int32, RowVersion> outputData =
                                new Tuple<Int32, RowVersion>
                                    (
                                        (Int32)GetValue(dr[FieldNames.ProductId]),
                                        new RowVersion(GetValue(dr[FieldNames.ProductRowVersion]))
                                    );

                            ProductDtoKey key = new ProductDtoKey();
                            key.EntityId = (Int32)GetValue(dr[FieldNames.ProductEntityId]);
                            key.Gtin = (String)GetValue(dr[FieldNames.ProductGtin]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (ProductDto dto in dtoList)
                {
                    Tuple<Int32, RowVersion> outputData = null;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != null)
                    {
                        dto.Id = outputData.Item1;
                        dto.RowVersion = outputData.Item2;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private String GenerateSetPropertiesString(ProductIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsEntityIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductEntityId);
            }
            if (isSetDto.IsProductGroupIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductGroupId);
            }
            if (isSetDto.IsReplacementProductIdSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductReplacementProductId);
            }
            if (isSetDto.IsGtinSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductGtin);
            }
            if (isSetDto.IsNameSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductName);
            }
            if (isSetDto.IsHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductHeight);
            }
            if (isSetDto.IsWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductWidth);
            }
            if (isSetDto.IsDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDepth);
            }
            if (isSetDto.IsDisplayHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDisplayHeight);
            }
            if (isSetDto.IsDisplayWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDisplayWidth);
            }
            if (isSetDto.IsDisplayDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDisplayDepth);
            }
            if (isSetDto.IsAlternateHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductAlternateHeight);
            }
            if (isSetDto.IsAlternateWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductAlternateWidth);
            }
            if (isSetDto.IsAlternateDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductAlternateDepth);
            }
            if (isSetDto.IsPointOfPurchaseHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPointOfPurchaseHeight);
            }
            if (isSetDto.IsPointOfPurchaseWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPointOfPurchaseWidth);
            }
            if (isSetDto.IsPointOfPurchaseDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPointOfPurchaseDepth);
            }
            if (isSetDto.IsNumberOfPegHolesSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductNumberOfPegHoles);
            }
            if (isSetDto.IsPegXSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegX);
            }
            if (isSetDto.IsPegX2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegX2);
            }
            if (isSetDto.IsPegX3Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegX3);
            }
            if (isSetDto.IsPegYSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegY);
            }
            if (isSetDto.IsPegY2Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegY2);
            }
            if (isSetDto.IsPegY3Set)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegY3);
            }
            //if (isSetDto.IsPegProngOffsetSet)
            //{
            //    setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegProngOffset);
            //}
            if (isSetDto.IsPegProngOffsetXSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegProngOffsetX);
            }
            if (isSetDto.IsPegProngOffsetYSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegProngOffsetY);
            }
            if (isSetDto.IsPegDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPegDepth);
            }
            if (isSetDto.IsSqueezeHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSqueezeHeight);
            }
            if (isSetDto.IsSqueezeWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSqueezeWidth);
            }
            if (isSetDto.IsSqueezeDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSqueezeDepth);
            }
            if (isSetDto.IsNestingHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductNestingHeight);
            }
            if (isSetDto.IsNestingWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductNestingWidth);
            }
            if (isSetDto.IsNestingDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductNestingDepth);
            }
            if (isSetDto.IsCasePackUnitsSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCasePackUnits);
            }
            if (isSetDto.IsCaseHighSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCaseHigh);
            }
            if (isSetDto.IsCaseWideSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCaseWide);
            }
            if (isSetDto.IsCaseDeepSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCaseDeep);
            }
            if (isSetDto.IsCaseHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCaseHeight);
            }
            if (isSetDto.IsCaseWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCaseWidth);
            }
            if (isSetDto.IsCaseDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCaseDepth);
            }
            if (isSetDto.IsMaxStackSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductMaxStack);
            }
            if (isSetDto.IsMaxTopCapSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductMaxTopCap);
            }
            if (isSetDto.IsMaxRightCapSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductMaxRightCap);
            }
            if (isSetDto.IsMinDeepSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductMinDeep);
            }
            if (isSetDto.IsMaxDeepSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductMaxDeep);
            }
            if (isSetDto.IsTrayPackUnitsSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayPackUnits);
            }
            if (isSetDto.IsTrayHighSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayHigh);
            }
            if (isSetDto.IsTrayWideSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayWide);
            }
            if (isSetDto.IsTrayDeepSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayDeep);
            }
            if (isSetDto.IsTrayHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayHeight);
            }
            if (isSetDto.IsTrayWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayWidth);
            }
            if (isSetDto.IsTrayDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayDepth);
            }
            if (isSetDto.IsTrayThickHeightSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayThickHeight);
            }
            if (isSetDto.IsTrayThickWidthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayThickWidth);
            }
            if (isSetDto.IsTrayThickDepthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTrayThickDepth);
            }
            if (isSetDto.IsFrontOverhangSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFrontOverhang);
            }
            if (isSetDto.IsFingerSpaceAboveSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFingerSpaceAbove);
            }
            if (isSetDto.IsFingerSpaceToTheSideSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFingerSpaceToTheSide);
            }
            if (isSetDto.IsStatusTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductStatusType);
            }
            if (isSetDto.IsOrientationTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductOrientationType);
            }
            if (isSetDto.IsMerchandisingStyleSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductMerchandisingStyle);
            }
            if (isSetDto.IsIsFrontOnlySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductIsFrontOnly);
            }
            if (isSetDto.IsIsPlaceHolderProductSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductIsPlaceHolderProduct);
            }
            if (isSetDto.IsIsActiveSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductIsActive);
            }
            if (isSetDto.IsCanBreakTrayUpSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCanBreakTrayUp);
            }
            if (isSetDto.IsCanBreakTrayDownSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCanBreakTrayDown);
            }
            if (isSetDto.IsCanBreakTrayBackSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCanBreakTrayBack);
            }
            if (isSetDto.IsCanBreakTrayTopSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCanBreakTrayTop);
            }
            if (isSetDto.IsForceMiddleCapSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductForceMiddleCap);
            }
            if (isSetDto.IsForceBottomCapSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductForceBottomCap);
            }
            if (isSetDto.IsShapeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductShape);
            }
            if (isSetDto.IsShapeTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductShapeType);
            }
            if (isSetDto.IsFillColourSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFillColour);
            }
            if (isSetDto.IsFillPatternTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFillPatternType);
            }
            if (isSetDto.IsPointOfPurchaseDescriptionSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPointOfPurchaseDescription);
            }
            if (isSetDto.IsShortDescriptionSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductShortDescription);
            }
            if (isSetDto.IsSubcategorySet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSubcategory);
            }
            if (isSetDto.IsCustomerStatusSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCustomerStatus);
            }
            if (isSetDto.IsColourSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductColour);
            }
            if (isSetDto.IsFlavourSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFlavour);
            }
            if (isSetDto.IsPackagingShapeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPackagingShape);
            }
            if (isSetDto.IsPackagingTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPackagingType);
            }
            if (isSetDto.IsCountryOfOriginSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCountryOfOrigin);
            }
            if (isSetDto.IsCountryOfProcessingSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCountryOfProcessing);
            }
            if (isSetDto.IsShelfLifeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductShelfLife);
            }
            if (isSetDto.IsDeliveryFrequencyDaysSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDeliveryFrequencyDays);
            }
            if (isSetDto.IsDeliveryMethodSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDeliveryMethod);
            }
            if (isSetDto.IsBrandSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductBrand);
            }
            if (isSetDto.IsVendorCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductVendorCode);
            }
            if (isSetDto.IsVendorSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductVendor);
            }
            if (isSetDto.IsManufacturerCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductManufacturerCode);
            }
            if (isSetDto.IsManufacturerSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductManufacturer);
            }
            if (isSetDto.IsSizeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSize);
            }
            if (isSetDto.IsUnitOfMeasureSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductUnitOfMeasure);
            }
            if (isSetDto.IsDateIntroducedSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDateIntroduced);
            }
            if (isSetDto.IsDateDiscontinuedSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDateDiscontinued);
            }
            if (isSetDto.IsDateEffectiveSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductDateEffective);
            }
            if (isSetDto.IsHealthSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductHealth);
            }
            if (isSetDto.IsCorporateCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCorporateCode);
            }
            if (isSetDto.IsBarcodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductBarcode);
            }
            if (isSetDto.IsSellPriceSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSellPrice);
            }
            if (isSetDto.IsSellPackCountSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSellPackCount);
            }
            if (isSetDto.IsSellPackDescriptionSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductSellPackDescription);
            }
            if (isSetDto.IsRecommendedRetailPriceSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductRecommendedRetailPrice);
            }
            if (isSetDto.IsManufacturerRecommendedRetailPriceSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductManufacturerRecommendedRetailPrice);
            }
            if (isSetDto.IsCostPriceSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCostPrice);
            }
            if (isSetDto.IsCaseCostSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductCaseCost);
            }
            if (isSetDto.IsTaxRateSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTaxRate);
            }
            if (isSetDto.IsConsumerInformationSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductConsumerInformation);
            }
            if (isSetDto.IsTextureSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductTexture);
            }
            if (isSetDto.IsStyleNumberSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductStyleNumber);
            }
            if (isSetDto.IsPatternSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductPattern);
            }
            if (isSetDto.IsModelSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductModel);
            }
            if (isSetDto.IsGarmentTypeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductGarmentType);
            }
            if (isSetDto.IsIsPrivateLabelSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductIsPrivateLabel);
            }
            if (isSetDto.IsIsNewProductSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductIsNew);
            }
            if (isSetDto.IsFinancialGroupCodeSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFinancialGroupCode);
            }
            if (isSetDto.IsFinancialGroupNameSet)
            {
                setProperties = String.Format("{0}#{1}#", setProperties, FieldNames.ProductFinancialGroupName);
            }

            return setProperties;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes dtos that match the specified entity id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductDeleteByEntityId))
                {
                    this.CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, entityId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the temporary table on the given connection
        /// </summary>
        private void CreateProductTempTable(String createSql, String tempTableName)
        {
            // create the temp table
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, createSql, tempTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateProductTempTable(IEnumerable<Object> productIds)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpProducts (Product_Id INT)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpProducts (Product_Id) VALUES (@Product_Id)", CommandType.Text))
            {
                // product gtin
                SqlParameter productIdParameter = CreateParameter(command,
                    "@Product_Id",
                    SqlDbType.Int,
                    string.Empty);

                // insert the product gtins
                foreach (Int32 productId in productIds)
                {
                    productIdParameter.Value = productId;
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateProductTempTable(IEnumerable<Int32> productIds)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpProducts (Product_Id INT)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpProducts (Product_Id) VALUES (@Product_Id)", CommandType.Text))
            {
                // product gtin
                SqlParameter productIdParameter = CreateParameter(command,
                    "@Product_Id",
                    SqlDbType.Int,
                    string.Empty);

                // insert the product gtins
                foreach (Int32 productId in productIds)
                {
                    productIdParameter.Value = productId;
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateProductTempTable(IEnumerable<String> productGtins)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpProducts (Product_Gtin varchar(14) COLLATE database_default)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpProducts (Product_Gtin) VALUES (@Product_Gtin)", CommandType.Text))
            {
                // product gtin
                SqlParameter productGtinParameter = CreateParameter(command,
                    "@Product_Gtin",
                    SqlDbType.NVarChar,
                    string.Empty);

                // insert the product gtins
                foreach (string productGtin in productGtins)
                {
                    if (productGtin.Length <= 14)
                    {
                        productGtinParameter.Value = productGtin;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateProductSearchTextTempTable(IEnumerable<String> productSearchText)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpProducts (Product_SearchText varchar(50) COLLATE database_default)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpProducts (Product_SearchText) VALUES (@Product_SearchText)", CommandType.Text))
            {
                // product gtin
                SqlParameter productSearchTextParameter = CreateParameter(command,
                    "@Product_SearchText",
                    SqlDbType.NVarChar,
                    string.Empty);

                // insert the product gtins
                foreach (string productGtin in productSearchText)
                {
                    if (productGtin.Length <= 50)
                    {
                        productSearchTextParameter.Value = productGtin;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Drops the temporary table on the given connection
        /// </summary>
        /// <param name="connection">SQL connection to use</param>
        private void DropProductTempTable()
        {
            try
            {
                using (DalCommand command = CreateCommand("DROP TABLE #tmpProducts", CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch { }
        }

        #endregion



    }
}
