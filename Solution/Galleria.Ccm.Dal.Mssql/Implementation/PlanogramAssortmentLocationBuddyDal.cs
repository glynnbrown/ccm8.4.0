﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramAssortmentLocationBuddyDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssortmentLocationBuddyDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssortmentLocationBuddy DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssortmentLocationBuddyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssortmentLocationBuddyDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyId]),
                LocationCode = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyLocationCode]),
                TreatmentType = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyTreatmentType]),
                S1LocationCode = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS1LocationCode]),
                S1Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS1Percentage]),
                S2LocationCode = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS2LocationCode]),
                S2Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS2Percentage]),
                S3LocationCode = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS3LocationCode]),
                S3Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS3Percentage]),
                S4LocationCode = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS4LocationCode]),
                S4Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS4Percentage]),
                S5LocationCode = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS5LocationCode]),
                S5Percentage = (Single?)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyS5Percentage]),
                PlanogramAssortmentId = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentLocationBuddyPlanogramAssortmentId]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssortmentLocationBuddy DTOs with a matching Planogram Assortment Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramAssortmentLocationBuddy DTOs.</returns>
        public IEnumerable<PlanogramAssortmentLocationBuddyDto> FetchByPlanogramAssortmentId(object id)
        {
            var dtoList = new List<PlanogramAssortmentLocationBuddyDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocationBuddyFetchByPlanogramAssortmentId))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyPlanogramAssortmentId, SqlDbType.Int, (Int32)id);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssortmentLocationBuddyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocationBuddyInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS1LocationCode, SqlDbType.NVarChar, dto.S1LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS2LocationCode, SqlDbType.NVarChar, dto.S2LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS3LocationCode, SqlDbType.NVarChar, dto.S3LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS4LocationCode, SqlDbType.NVarChar, dto.S4LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS5LocationCode, SqlDbType.NVarChar, dto.S5LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentLocationBuddyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssortmentLocationBuddyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocationBuddyUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyTreatmentType, SqlDbType.TinyInt, dto.TreatmentType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS1LocationCode, SqlDbType.NVarChar, dto.S1LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS1Percentage, SqlDbType.Real, dto.S1Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS2LocationCode, SqlDbType.NVarChar, dto.S2LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS2Percentage, SqlDbType.Real, dto.S2Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS3LocationCode, SqlDbType.NVarChar, dto.S3LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS3Percentage, SqlDbType.Real, dto.S3Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS4LocationCode, SqlDbType.NVarChar, dto.S4LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS4Percentage, SqlDbType.Real, dto.S4Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS5LocationCode, SqlDbType.NVarChar, dto.S5LocationCode);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyS5Percentage, SqlDbType.Real, dto.S5Percentage);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentLocationBuddyDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocationBuddyDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocationBuddyId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentLocationBuddyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentLocationBuddyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
