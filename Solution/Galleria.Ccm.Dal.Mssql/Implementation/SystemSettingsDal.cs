﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-26306 : L.Ineson
//      Created.
 
#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// SystemSettings Dal
    /// </summary>
    public class SystemSettingsDal : Galleria.Framework.Dal.Mssql.DalBase, ISystemSettingsDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public SystemSettingsDto GetDataTransferObject(SqlDataReader dr)
        {
            return new SystemSettingsDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.SystemSettingsId]),
                Key = (String)GetValue(dr[FieldNames.SystemSettingsKey]),
                Value = (String)GetValue(dr[FieldNames.SystemSettingsValue]),
                UserId = (Int32?)GetValue(dr[FieldNames.SystemSettingsUserId]),
                EntityId = (Int32?)GetValue(dr[FieldNames.SystemSettingsEntityId])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public SystemSettingsDto FetchById(Int32 id)
        {
            SystemSettingsDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SystemSettingsFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.SystemSettingsId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<SystemSettingsDto> FetchByEntityId(Int32 entityId)
        {
            List<SystemSettingsDto> dtoList = new List<SystemSettingsDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SystemSettingsFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.SystemSettingsEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that correspond to the default SystemSettings, i.e. dtos with EntityId = null.
        /// </summary>
        /// <returns>matching dtos</returns>
        public IEnumerable<SystemSettingsDto> FetchDefault()
        {
            List<SystemSettingsDto> dtoList = new List<SystemSettingsDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SystemSettingsFetchDefault))
                {
                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(SystemSettingsDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SystemSettingsInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.SystemSettingsId, SqlDbType.Int);


                    //Other properties 
                    CreateParameter(command, FieldNames.SystemSettingsKey, SqlDbType.NVarChar, dto.Key);
                    CreateParameter(command, FieldNames.SystemSettingsValue, SqlDbType.NVarChar, dto.Value);
                    CreateParameter(command, FieldNames.SystemSettingsUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.SystemSettingsEntityId, SqlDbType.Int, dto.EntityId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(SystemSettingsDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SystemSettingsUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.SystemSettingsId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.SystemSettingsKey, SqlDbType.NVarChar, dto.Key);
                    CreateParameter(command, FieldNames.SystemSettingsValue, SqlDbType.NVarChar, dto.Value);
                    CreateParameter(command, FieldNames.SystemSettingsUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.SystemSettingsEntityId, SqlDbType.Int, dto.EntityId);

                    //execute
                    command.ExecuteNonQuery();

                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.SystemSettingsDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.SystemSettingsId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
