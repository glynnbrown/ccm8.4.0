﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// The MSSQL DAL for the PlanogramAssembly DTO.
    /// </summary>
    public class PlanogramAssemblyDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssemblyDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssembly DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssemblyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssemblyDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssemblyId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramAssemblyPlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramAssemblyName]),
                Height = (Single)GetValue(dr[FieldNames.PlanogramAssemblyHeight]),
                Width = (Single)GetValue(dr[FieldNames.PlanogramAssemblyWidth]),
                Depth = (Single)GetValue(dr[FieldNames.PlanogramAssemblyDepth]),
                NumberOfComponents = (Int16)GetValue(dr[FieldNames.PlanogramAssemblyNumberOfComponents]),
                TotalComponentCost = (Single)GetValue(dr[FieldNames.PlanogramAssemblyTotalComponentCost])
            };
        }
        #endregion  

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssembly DTOs with a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramAssembly DTOs.</returns>
        public IEnumerable<PlanogramAssemblyDto> FetchByPlanogramId(object planogramId)
        {
            var dtoList = new List<PlanogramAssemblyDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyFetchByPlanogramId))
                {
                    CreateParameter(
                        command,
                        FieldNames.PlanogramAssemblyPlanogramId,
                        SqlDbType.Int,
                        (Int32)planogramId);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssemblyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssemblyId, SqlDbType.Int);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            foreach (PlanogramAssemblyDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssemblyDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssemblyId, SqlDbType.Int, dto.Id);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            foreach (PlanogramAssemblyDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssemblyDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssemblyId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssemblyDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            foreach (PlanogramAssemblyDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Creates Parameters on the command for each of the DTO's properties, apart from Id.
        /// </summary>
        /// <param name="dto">The DTO whose properties should be added.</param>
        /// <param name="command">The command to add the parameters to.</param>
        private void CreateAllParametersButId(PlanogramAssemblyDto dto, DalCommand command)
        {
            CreateParameter(command,FieldNames.PlanogramAssemblyPlanogramId,SqlDbType.Int,dto.PlanogramId);
            CreateParameter(command,FieldNames.PlanogramAssemblyName,SqlDbType.NVarChar,dto.Name);
            CreateParameter(command,FieldNames.PlanogramAssemblyHeight,SqlDbType.Real,dto.Height);
            CreateParameter(command,FieldNames.PlanogramAssemblyWidth,SqlDbType.Real,dto.Width);
            CreateParameter(command,FieldNames.PlanogramAssemblyDepth,SqlDbType.Real,dto.Depth);
            CreateParameter(command,FieldNames.PlanogramAssemblyNumberOfComponents,SqlDbType.SmallInt,dto.NumberOfComponents);
            CreateParameter(command,FieldNames.PlanogramAssemblyTotalComponentCost,SqlDbType.Real,dto.TotalComponentCost);
        }
        #endregion
    }
}
