using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using AutoMapper;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Data Access Layer class for accessing records held in the ProductAttributeComparisonResult <seealso cref="ProductAttributeComparisonResultDto"/>
    /// </summary>
    public class ProductAttributeComparisonResultDal : Framework.Dal.Mssql.DalBase, IProductAttributeComparisonResultDal
    {

        #region Constants
        private const String ImportTableName = "#tmpProductComparisonAttributeResults";
        private const String DropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String CreateImportTableSql =
           @"CREATE TABLE [{0}](
	            [ProductComparisonAttributeResults_Id] [int] NOT NULL,
	            [ProductComparisonAttributeResults_PlanogramId] [int] NOT NULL,
	            [ProductComparisonAttributeResults_ProductGTIN] [nvarchar](14) NOT NULL,
	            [ProductComparisonAttributeResults_ComparedProductAttribute] [nvarchar](250) NOT NULL,
	            [ProductComparisonAttributeResults_MasterDataValue] [nvarchar](250) NULL,
	            [ProductComparisonAttributeResults_ProductValue] [nvarchar](250) NULL)
            ";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class ProductAttributeComparisonResultBulkCopyDataReader : IDataReader
        {
            #region Constants

            private const Int32 IdColumnNumber = 0;
            private const Int32 PlanogramIdColumnNumber = 1;
            private const Int32 ProductGtinColumnNumber = 2;
            private const Int32 ComparedProductAttrbiuteColumnNumber = 3;
            private const Int32 MasterDataColumnNumber = 4;
            private const Int32 ProductDataColumnNumber = 5;

            private const Int32 ColumnCount = 6;

            #endregion

            #region Fields
            private readonly IEnumerator<ProductAttributeComparisonResultDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ProductAttributeComparisonResultBulkCopyDataReader(IEnumerable<ProductAttributeComparisonResultDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return ColumnCount; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public Object GetValue(Int32 i)
            {
                Object value;
                switch (i)
                {
                    case IdColumnNumber:
                        value = _enumerator.Current.Id;
                        break;
                    case PlanogramIdColumnNumber:
                        value = _enumerator.Current.PlanogramId;
                        break;
                    case ProductGtinColumnNumber:
                        value = _enumerator.Current.ProductGtin;
                        break;
                    case ComparedProductAttrbiuteColumnNumber:
                        value = _enumerator.Current.ComparedProductAttribute;
                        break;
                    case MasterDataColumnNumber:
                        value = _enumerator.Current.MasterDataValue;
                        break;
                    case ProductDataColumnNumber:
                        value = _enumerator.Current.ProductValue;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                return value ?? DBNull.Value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public Int32 Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public Boolean IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public Boolean NextResult()
            {
                throw new NotImplementedException();
            }

            public Int32 RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public Boolean GetBoolean(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Byte GetByte(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetBytes(Int32 i, Int64 fieldOffset, Byte[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public Char GetChar(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetChars(Int32 i, Int64 fieldoffset, Char[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetDataTypeName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Decimal GetDecimal(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Double GetDouble(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Single GetFloat(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int16 GetInt16(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetInt32(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetInt64(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetOrdinal(String name)
            {
                throw new NotImplementedException();
            }

            public String GetString(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetValues(Object[] values)
            {
                throw new NotImplementedException();
            }

            public Boolean IsDBNull(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Object this[String name]
            {
                get { throw new NotImplementedException(); }
            }

            public Object this[Int32 i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Create data transfer object with the data from the given <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="dr">The <see cref="SqlDataReader"/> instance from which to load data.</param>
        /// <returns>A new instance of <see cref="ProductAttributeComparisonResultDto"/> with the required values.</returns>
        public ProductAttributeComparisonResultDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ProductAttributeComparisonResultDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductComparisonAttributeResultsId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.ProductComparisonAttributeResultsPlanogramId]),
                ProductGtin = (String)GetValue(dr[FieldNames.ProductComparisonAttributeResultsProductGtin]),
                ComparedProductAttribute = (String)GetValue(dr[FieldNames.ProductComparisonAttributeResultsComparedProductAttribute]),
                MasterDataValue = (String)GetValue(dr[FieldNames.ProductComparisonAttributeResultsMasterDataValue]),
                ProductValue = (String)GetValue(dr[FieldNames.ProductComparisonAttributeResultsProductValue])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Festches all the comparison results by the planogramid
        /// </summary>
        /// <param name="id">Id of the planogram to retrieve all the results for</param>
        /// <returns>List of comparison results that relate to the planogrma id <see cref="ProductAttributeComparisonResultDto"/></returns>
        public IEnumerable<ProductAttributeComparisonResultDto> FetchByPlanogramId(Object id)
        {
            var dtoList = new List<ProductAttributeComparisonResultDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductAttributeComparisonResultFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsPlanogramId, SqlDbType.Int, (Int32)id);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a record into the ProductAttributeComparisonResult table <see cref="ProductAttributeComparisonResultDto"/>
        /// </summary>
        /// <param name="dto">Dto containing the data to insert <see cref="ProductAttributeComparisonResultDto"/></param>
        public void Insert(ProductAttributeComparisonResultDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductAttributeComparisonResultInsert))
                {
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ProductComparisonAttributeResultsId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsComparedProductAttribute, SqlDbType.NVarChar, dto.ComparedProductAttribute);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsMasterDataValue, SqlDbType.NVarChar, dto.MasterDataValue);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsProductValue, SqlDbType.NVarChar, dto.ProductValue);
                    command.ExecuteNonQuery();

                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Insert the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="ProductAttributeComparisonResultDto"/> instances containing the values.</param>
        public void Insert(IEnumerable<ProductAttributeComparisonResultDto> dtos)
        {
            Dictionary<Object, ProductAttributeComparisonResultDto> index = new Dictionary<Object, ProductAttributeComparisonResultDto>();
            foreach (ProductAttributeComparisonResultDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(ImportTableName);

                // perform a batch insert to temp table
                using (var bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = CommandTimeout;
                    bulkCopy.DestinationTableName = String.Format("[{0}]", ImportTableName);
                    var dr = new ProductAttributeComparisonResultBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.ProductAttributeComparisonResultBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = (Int32)newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(ImportTableName);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the values in the given <paramref name="dto"/> into the data base.
        /// </summary>
        /// <param name="dto">The <see cref="ProductAttributeComparisonResultDto"/> instance containing the values.</param>
        public void Update(ProductAttributeComparisonResultDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductAttributeComparisonResultUpdate))
                {
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsComparedProductAttribute, SqlDbType.NVarChar, dto.ComparedProductAttribute);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsMasterDataValue, SqlDbType.NVarChar, dto.MasterDataValue);
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsProductValue, SqlDbType.NVarChar, dto.ProductValue);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Update the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="ProductAttributeComparisonResultDto"/> instances containing the values.</param>
        public void Update(IEnumerable<ProductAttributeComparisonResultDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(ImportTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = CommandTimeout;
                    bulkCopy.DestinationTableName = String.Format("[{0}]", ImportTableName);
                    var dr = new ProductAttributeComparisonResultBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.ProductAttributeComparisonResultBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(ImportTableName);
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Removes a record from the ProductAttributeComparisonResult table using the relevant ID.
        /// </summary>
        /// <param name="id">Id of the record to delete <seealso cref="ProductAttributeComparisonResultDto"/></param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ProductAttributeComparisonResultDeleteById))
                {
                    CreateParameter(command, FieldNames.ProductComparisonAttributeResultsId, SqlDbType.Int, ParameterDirection.Input);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// DeleteS FROM THE ProductAttributeComparisonResult TABLE
        /// </summary>
        /// <param name="dto">Data to be removed from ProductAttributeComparisonResult table <seealso cref="ProductAttributeComparisonResultDto"/></param>
        public void Delete(ProductAttributeComparisonResultDto dto)
        {
           DeleteById(dto.Id);
        }

        /// <summary>
        /// Bulk delete of data from the ProductAttributeComparisonResult table
        /// </summary>
        /// <param name="dtos">List of records to be removed from the ProductAttributeComparisonResult table <seealso cref="ProductAttributeComparisonResultDto"/></param>
        public void Delete(IEnumerable<ProductAttributeComparisonResultDto> dtos)
        {
            foreach (ProductAttributeComparisonResultDto dto in dtos)
                Delete(dto);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, CreateImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, DropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion

      
    }
}