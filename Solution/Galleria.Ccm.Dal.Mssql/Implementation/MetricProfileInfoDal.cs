﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MetricProfile Dal
    /// </summary>
    public class MetricProfileInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IMetricProfileInfoDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static MetricProfileInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new MetricProfileInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.MetricProfileId]),
                Name = (String)GetValue(dr[FieldNames.MetricProfileName]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<MetricProfileInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<MetricProfileInfoDto> dtoList = new List<MetricProfileInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricProfileInfoFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.MetricProfileEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion
    }
}