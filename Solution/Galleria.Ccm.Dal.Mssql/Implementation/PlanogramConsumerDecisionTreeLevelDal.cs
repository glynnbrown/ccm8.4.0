﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramConsumerDecisionTreeLevel Dal
    /// </summary>
    public class PlanogramConsumerDecisionTreeLevelDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramConsumerDecisionTreeLevelDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramConsumerDecisionTreeLevelDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramConsumerDecisionTreeLevelDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeLevelId]),
                PlanogramConsumerDecisionTreeId = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeLevelName]),
                ParentLevelId = (Int32?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeLevelParentLevelId]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a dto that matches the specified id
        /// </summary>
        /// <param name="ConsumerDecisionTreeId">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramConsumerDecisionTreeLevelDto> FetchByPlanogramConsumerDecisionTreeId(object planogramConsumerDecisionTreeId)
        {
            var dtoList = new List<PlanogramConsumerDecisionTreeLevelDto>();
            try
            {
                using (
                    DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeLevelFetchByPlanogramConsumerDecisionTreeId)
                    )
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId, SqlDbType.Int,
                        (Int32)planogramConsumerDecisionTreeId);

                    // Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramConsumerDecisionTreeLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeLevelInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelId,
                        SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId, SqlDbType.Int,
                        dto.PlanogramConsumerDecisionTreeId);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelParentLevelId, SqlDbType.Int,
                        dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        /// <param name="dtos"></param>
        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeLevelDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeLevelDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramConsumerDecisionTreeLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeLevelUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId, SqlDbType.Int,
                        dto.PlanogramConsumerDecisionTreeId);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelParentLevelId, SqlDbType.Int,
                        dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramConsumerDecisionTreeLevelDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeLevelDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeLevelDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeLevelId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramConsumerDecisionTreeLevelDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeLevelDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeLevelDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
