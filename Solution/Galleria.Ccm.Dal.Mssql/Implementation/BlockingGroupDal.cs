﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// BlockingGroup Dal
    /// </summary>
    public class BlockingGroupDal : Galleria.Framework.Dal.Mssql.DalBase, IBlockingGroupDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static BlockingGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new BlockingGroupDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.BlockingGroupId]),
                BlockingId = (Int32)GetValue(dr[FieldNames.BlockingGroupBlockingId]),
                Name = (String)GetValue(dr[FieldNames.BlockingGroupName]),
                Colour = (Int32)GetValue(dr[FieldNames.BlockingGroupColour]),
                FillPatternType = (Byte)GetValue(dr[FieldNames.BlockingGroupFillPatternType]),
                CanCompromiseSequence = (Boolean)GetValue(dr[FieldNames.BlockingGroupCanCompromiseSequence]),
                IsRestrictedByComponentType = (Boolean)GetValue(dr[FieldNames.BlockingGroupIsRestrictedByComponentType]),
                CanOptimise = (Boolean)GetValue(dr[FieldNames.BlockingGroupCanOptimise]),
                CanMerge = (Boolean)GetValue(dr[FieldNames.BlockingGroupCanMerge]),
                BlockPlacementPrimaryType = (Byte)GetValue(dr[FieldNames.BlockingGroupBlockPlacementPrimaryType]),
                BlockPlacementSecondaryType = (Byte)GetValue(dr[FieldNames.BlockingGroupBlockPlacementSecondaryType]),
                BlockPlacementTertiaryType = (Byte)GetValue(dr[FieldNames.BlockingGroupBlockPlacementTertiaryType]),
                TotalSpacePercentage = (Single)GetValue(dr[FieldNames.BlockingGroupTotalSpacePercentage]),
            };
        }

        #endregion

        #region Fetch



        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<BlockingGroupDto> FetchByBlockingId(Int32 blockingId)
        {
            List<BlockingGroupDto> dtoList = new List<BlockingGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingGroupFetchByBlockingId))
                {
                    CreateParameter(command, FieldNames.BlockingGroupBlockingId, SqlDbType.Int, blockingId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(BlockingGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingGroupInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.BlockingGroupId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingGroupBlockingId, SqlDbType.Int, dto.BlockingId);
                    CreateParameter(command, FieldNames.BlockingGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.BlockingGroupColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.BlockingGroupFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);
                    CreateParameter(command, FieldNames.BlockingGroupCanCompromiseSequence, SqlDbType.Bit, dto.CanCompromiseSequence);
                    CreateParameter(command, FieldNames.BlockingGroupIsRestrictedByComponentType, SqlDbType.Bit, dto.IsRestrictedByComponentType);
                    CreateParameter(command, FieldNames.BlockingGroupCanOptimise, SqlDbType.Bit, dto.CanOptimise);
                    CreateParameter(command, FieldNames.BlockingGroupCanMerge, SqlDbType.Bit, dto.CanMerge);
                    CreateParameter(command, FieldNames.BlockingGroupBlockPlacementPrimaryType, SqlDbType.TinyInt, dto.BlockPlacementPrimaryType);
                    CreateParameter(command, FieldNames.BlockingGroupBlockPlacementSecondaryType, SqlDbType.TinyInt, dto.BlockPlacementSecondaryType);
                    CreateParameter(command, FieldNames.BlockingGroupBlockPlacementTertiaryType, SqlDbType.TinyInt, dto.BlockPlacementTertiaryType);
                    CreateParameter(command, FieldNames.BlockingGroupTotalSpacePercentage, SqlDbType.Real, dto.TotalSpacePercentage);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(BlockingGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingGroupUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingGroupId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.BlockingGroupBlockingId, SqlDbType.Int, dto.BlockingId);
                    CreateParameter(command, FieldNames.BlockingGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.BlockingGroupColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.BlockingGroupFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);
                    CreateParameter(command, FieldNames.BlockingGroupCanCompromiseSequence, SqlDbType.Bit, dto.CanCompromiseSequence);
                    CreateParameter(command, FieldNames.BlockingGroupIsRestrictedByComponentType, SqlDbType.Bit, dto.IsRestrictedByComponentType);
                    CreateParameter(command, FieldNames.BlockingGroupCanOptimise, SqlDbType.Bit, dto.CanOptimise);
                    CreateParameter(command, FieldNames.BlockingGroupCanMerge, SqlDbType.Bit, dto.CanMerge);
                    CreateParameter(command, FieldNames.BlockingGroupBlockPlacementPrimaryType, SqlDbType.TinyInt, dto.BlockPlacementPrimaryType);
                    CreateParameter(command, FieldNames.BlockingGroupBlockPlacementSecondaryType, SqlDbType.TinyInt, dto.BlockPlacementSecondaryType);
                    CreateParameter(command, FieldNames.BlockingGroupBlockPlacementTertiaryType, SqlDbType.TinyInt, dto.BlockPlacementTertiaryType);
                    CreateParameter(command, FieldNames.BlockingGroupTotalSpacePercentage, SqlDbType.Real, dto.TotalSpacePercentage);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.BlockingGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}