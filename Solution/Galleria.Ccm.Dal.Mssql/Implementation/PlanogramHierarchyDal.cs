﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25587 : L.Ineson
//		Created (Auto-generated)
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramHierarchy Dal
    /// </summary>
    public class PlanogramHierarchyDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramHierarchyDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramHierarchyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramHierarchyDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramHierarchyId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PlanogramHierarchyRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.PlanogramHierarchyEntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramHierarchyName]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.PlanogramHierarchyDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.PlanogramHierarchyDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.PlanogramHierarchyDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PlanogramHierarchyDto FetchById(Int32 id)
        {
            PlanogramHierarchyDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramHierarchyFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramHierarchyId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public PlanogramHierarchyDto FetchByEntityId(Int32 entityId)
        {
            PlanogramHierarchyDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramHierarchyFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.PlanogramHierarchyEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramHierarchyDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramHierarchyInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramHierarchyId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PlanogramHierarchyRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramHierarchyDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramHierarchyDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.PlanogramHierarchyDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramHierarchyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramHierarchyName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramHierarchyDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramHierarchyUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramHierarchyId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PlanogramHierarchyRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramHierarchyDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramHierarchyDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.PlanogramHierarchyDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramHierarchyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramHierarchyName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramHierarchyDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramHierarchyId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        # region FetchPlanogramPlanogramGroupGroupings

        public Dictionary<Guid, Guid> FetchPlanogramPlanogramGroupGroupings(Int32 entityId)
        {
            Dictionary<Guid, Guid> planGroupings = new Dictionary<Guid, Guid>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramHierarchyFetchPlanogramGroupingsByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.PlanogramHierarchyEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Guid planUcr = (Guid) GetValue(dr[FieldNames.PlanogramUniqueContentReference]);
                            Guid groupUcr = (Guid) GetValue(dr[FieldNames.PlanogramGroupCode]);

                            planGroupings[planUcr] = groupUcr;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return planGroupings;
        }

        #endregion
    }
}