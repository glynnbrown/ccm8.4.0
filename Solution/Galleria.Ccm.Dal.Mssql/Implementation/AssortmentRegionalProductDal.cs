﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentRegionProductDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentRegionProductDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentRegionProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentRegionProductDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentRegionalProductId]),
                AssortmentRegionId = (Int32)GetValue(dr[FieldNames.AssortmentRegionalProductAssortmentRegionId]),
                PrimaryProductId = (Int32)GetValue(dr[FieldNames.AssortmentRegionalProductPrimaryProductId]),
                PrimaryProductGTIN = (String)GetValue(dr[FieldNames.AssortmentRegionalProductPrimaryProductGTIN]),
                PrimaryProductDescription = (String)GetValue(dr[FieldNames.AssortmentRegionalProductPrimaryProductDescription]),
                RegionalProductId = (Int32?)GetValue(dr[FieldNames.AssortmentRegionalProductRegionalProductId]),
                RegionalProductGTIN = (String)GetValue(dr[FieldNames.AssortmentRegionalProductRegionalProductGTIN]),
                RegionalProductDescription = (String)GetValue(dr[FieldNames.AssortmentRegionalProductRegionalProductDescription]),
                RegionName = (String)GetValue(dr[FieldNames.AssortmentRegionName])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all region product dtos for the specified assortment region
        /// </summary>
        /// <param name="assortmentId">The assortment region id</param>
        /// <returns>All region dtos for the assortment</returns>
        public IEnumerable<AssortmentRegionProductDto> FetchByRegionId(Int32 assortmentRegionId)
        {
            List<AssortmentRegionProductDto> dtoList = new List<AssortmentRegionProductDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionalProductFetchByAssortmentRegionId))
            {
                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductAssortmentRegionId,
                    SqlDbType.Int,
                    assortmentRegionId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment region product id</param>
        /// <returns>The assortment region product</returns>
        public AssortmentRegionProductDto FetchById(Int32 id)
        {
            AssortmentRegionProductDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionalProductFetchById))
            {
                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentRegionProductDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionalProductInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentRegionalProductId,
                    SqlDbType.Int);

                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductAssortmentRegionId,
                    SqlDbType.Int,
                    dto.AssortmentRegionId);

                // primary product id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductPrimaryProductId,
                    SqlDbType.Int,
                    dto.PrimaryProductId);

                // regional product id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductRegionalProductId,
                    SqlDbType.Int,
                    dto.RegionalProductId);

                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentRegionProductDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionalProductUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentRegionalProductId,
                    SqlDbType.Int,
                    dto.Id);

                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductAssortmentRegionId,
                    SqlDbType.Int,
                    dto.AssortmentRegionId);

                // primary product id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductPrimaryProductId,
                    SqlDbType.Int,
                    dto.PrimaryProductId);

                // regional product id
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductRegionalProductId,
                    SqlDbType.Int,
                    dto.RegionalProductId);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionalProductDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentRegionalProductId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}