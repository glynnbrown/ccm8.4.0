﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramAssortmentRegionDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssortmentRegionDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssortmentRegion DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssortmentRegionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssortmentRegionDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentRegionId]),
                PlanogramAssortmentId = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramAssortmentRegionName]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssortmentRegion DTOs with a matching Planogram Assortment Id.
        /// </summary>
        /// <returns>A List of PlanogramAssortmentRegion DTOs.</returns>
        public IEnumerable<PlanogramAssortmentRegionDto> FetchByPlanogramAssortmentId(object id)
        {
            var dtoList = new List<PlanogramAssortmentRegionDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentRegionFetchByPlanogramAssortmentId))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId, SqlDbType.Int, (Int32)id);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssortmentRegionDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentRegionInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssortmentRegionId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentRegionName, SqlDbType.NVarChar, dto.Name);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            foreach (PlanogramAssortmentRegionDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssortmentRegionDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentRegionUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentRegionId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramAssortmentRegionPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentRegionName, SqlDbType.NVarChar, dto.Name);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            foreach (PlanogramAssortmentRegionDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentRegionDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentRegionId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentRegionDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            foreach (PlanogramAssortmentRegionDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion
    }
}
