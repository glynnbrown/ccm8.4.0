﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     MSSQL DAL implementation for <see cref="IPlanogramSequenceDal"/>.
    /// </summary>
    public class PlanogramSequenceDal : DalBase, IPlanogramSequenceDal
    {
        #region Data TransferObject

        /// <summary>
        ///     Returns a <see cref="PlanogramSequenceDto"/> from the values in <paramref name="dr"/>.
        /// </summary>
        /// <param name="dr">The <see cref="SqlDataReader"/> to load from.</param>
        /// <returns>A new <see cref="PlanogramSequenceDto"/>.</returns>
        private static PlanogramSequenceDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramSequenceDto
            {
                Id = (Int32) GetValue(dr[FieldNames.PlanogramSequenceId]),
                PlanogramId = (Int32) GetValue(dr[FieldNames.PlanogramSequencePlanogramId])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Gets a <see cref="PlanogramSequenceDto"/> matching the <paramref name="planogramId"/>.
        /// </summary>
        /// <param name="planogramId">Unique identifier for the Planogram that contains the Planogram Sequence to be fetched.</param>
        /// <returns>A new instance of <see cref="PlanogramSequenceDto"/>.</returns>
        public PlanogramSequenceDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramSequenceDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceFetchByPlanogramId))
                {
                    // Create Id parameter.
                    CreateParameter(command, FieldNames.PlanogramSequencePlanogramId, SqlDbType.Int, (Int32) planogramId);

                    //  Execute.
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceDto"/> to insert.</param>
        public void Insert(PlanogramSequenceDto dto)
        {
            try
            {
                SqlParameter idParameter;
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceInsert))
                {
                    idParameter = CreateParameter(command, FieldNames.PlanogramSequenceId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramSequencePlanogramId, SqlDbType.Int, dto.PlanogramId);

                    command.ExecuteNonQuery();
                }

                dto.Id = (Int32) idParameter.Value;
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Inserts the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceDto> dtos)
        {
            foreach (PlanogramSequenceDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceDto"/> to update.</param>
        public void Update(PlanogramSequenceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramSequencePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Updates the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceDto> dtos)
        {
            foreach (PlanogramSequenceDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the record with a matching <paramref name="id"/> from the database.
        /// </summary>
        /// <param name="id">The Id of the record to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceId, SqlDbType.Int, (Int32) id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Deletes the given <paramref name="dto"/> from the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceDto"/> to delete.</param>
        public void Delete(PlanogramSequenceDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes the given <paramref name="dtos"/> from the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceDto> dtos)
        {
            foreach (PlanogramSequenceDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion
    }
}
