﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History : CCM 830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramBlockingDivider Dal
    /// </summary>
    public class PlanogramBlockingDividerDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramBlockingDividerDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramBlockingDividerDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramBlockingDividerDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramBlockingDividerId]),
                PlanogramBlockingId = (Int32)GetValue(dr[FieldNames.PlanogramBlockingDividerPlanogramBlockingId]),
                Type = (Byte)GetValue(dr[FieldNames.PlanogramBlockingDividerType]),
                Level = (Byte)GetValue(dr[FieldNames.PlanogramBlockingDividerLevel]),
                X = (Single)GetValue(dr[FieldNames.PlanogramBlockingDividerX]),
                Y = (Single)GetValue(dr[FieldNames.PlanogramBlockingDividerY]),
                Length = (Single)GetValue(dr[FieldNames.PlanogramBlockingDividerLength]),
                IsSnapped = (Boolean)GetValue(dr[FieldNames.PlanogramBlockingDividerIsSnapped]),
                IsLimited = (Boolean)GetValue(dr[FieldNames.PlanogramBlockingDividerIsLimited]),
                LimitedPercentage = (Single)GetValue(dr[FieldNames.PlanogramBlockingDividerLimitedPercentage]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramBlockingDividerDto> FetchByPlanogramBlockingId(Object planogramBlockingId)
        {
            List<PlanogramBlockingDividerDto> dtoList = new List<PlanogramBlockingDividerDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingDividerFetchByPlanogramBlockingId))
                {
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerPlanogramBlockingId, SqlDbType.Int, planogramBlockingId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramBlockingDividerDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingDividerInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramBlockingDividerId, SqlDbType.Int);


                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerPlanogramBlockingId, SqlDbType.Int, dto.PlanogramBlockingId);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerLevel, SqlDbType.TinyInt, dto.Level);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerLength, SqlDbType.Real, dto.Length);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerIsSnapped, SqlDbType.Bit, dto.IsSnapped);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerIsLimited, SqlDbType.Bit, dto.IsLimited);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerLimitedPercentage, SqlDbType.Real, dto.LimitedPercentage);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramBlockingDividerDto> dtos)
        {
            foreach (PlanogramBlockingDividerDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramBlockingDividerDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingDividerUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerPlanogramBlockingId, SqlDbType.Int, dto.PlanogramBlockingId);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerLevel, SqlDbType.TinyInt, dto.Level);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerLength, SqlDbType.Real, dto.Length);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerIsSnapped, SqlDbType.Bit, dto.IsSnapped);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerIsLimited, SqlDbType.Bit, dto.IsLimited);
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerLimitedPercentage, SqlDbType.Real, dto.LimitedPercentage);

                    //execute
                    command.ExecuteNonQuery();

                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramBlockingDividerDto> dtos)
        {
            foreach (PlanogramBlockingDividerDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingDividerDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingDividerId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramBlockingDividerDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramBlockingDividerDto> dtos)
        {
            foreach (PlanogramBlockingDividerDto dto in dtos)
            {
                this.Delete(dto);
            }
        }


        #endregion
    }
}