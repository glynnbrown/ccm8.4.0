﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-25556 : J.Pickup
//      ConsumerDecisionTreeNodeGFSId added to procedures & introduced FetchAllIncludingDeleted()
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ConsumerDecisionTreeNode Dal
    /// </summary>
    public class ConsumerDecisionTreeNodeDal : Galleria.Framework.Dal.Mssql.DalBase, IConsumerDecisionTreeNodeDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ConsumerDecisionTreeNodeDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ConsumerDecisionTreeNodeDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeId]),
                GFSId = (Int32?)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeGFSId]),
                ConsumerDecisionTreeId = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeConsumerDecisionTreeId]),
                Name = (String)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeName]),
                ConsumerDecisionTreeLevelId = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeConsumerDecisionTreeLevelId]),
                ParentNodeId = (Int32?)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeParentNodeId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeDateLastModified])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ConsumerDecisionTreeNodeDto FetchById(Int32 id)
        {
            ConsumerDecisionTreeNodeDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified cdt id
        /// </summary>
        /// <param name="consumerDecisionTreeId">specified cdt id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ConsumerDecisionTreeNodeDto> FetchByConsumerDecisionTreeId(Int32 consumerDecisionTreeId)
        {
            List<ConsumerDecisionTreeNodeDto> dtoList = new List<ConsumerDecisionTreeNodeDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeFetchByConsumerDecisionTreeId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeConsumerDecisionTreeId, SqlDbType.Int, consumerDecisionTreeId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        /// <summary>
        /// Returns the dtos that match the specified cdt id
        /// </summary>
        /// <param name="consumerDecisionTreeId">specified cdt id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ConsumerDecisionTreeNodeDto> FetchAll()
        {
            List<ConsumerDecisionTreeNodeDto> dtoList = new List<ConsumerDecisionTreeNodeDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeFetchAll))
                {

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ConsumerDecisionTreeNodeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeGFSId, SqlDbType.Int, dto.GFSId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeConsumerDecisionTreeId, SqlDbType.Int, dto.ConsumerDecisionTreeId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeConsumerDecisionTreeLevelId, SqlDbType.Int, dto.ConsumerDecisionTreeLevelId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeParentNodeId, SqlDbType.Int, dto.ParentNodeId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ConsumerDecisionTreeNodeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeGFSId, SqlDbType.Int, dto.GFSId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeConsumerDecisionTreeId, SqlDbType.Int, dto.ConsumerDecisionTreeId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeConsumerDecisionTreeLevelId, SqlDbType.Int, dto.ConsumerDecisionTreeLevelId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeParentNodeId, SqlDbType.Int, dto.ParentNodeId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}