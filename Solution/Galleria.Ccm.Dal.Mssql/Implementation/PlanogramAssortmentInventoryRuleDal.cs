﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramAssortmentInventoryRuleDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssortmentInventoryRuleDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssortmentInventoryRule DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssortmentInventoryRuleDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssortmentInventoryRuleDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleId]),
                ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleProductGtin]),
                CasePack = (Single)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleCasePack]),
                DaysOfSupply = (Single)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleDaysOfSupply]),
                ShelfLife = (Single)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleShelfLife]),
                ReplenishmentDays = (Single)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleReplenishmentDays]),
                WasteHurdleUnits = (Single)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleUnits]),
                WasteHurdleCasePack = (Single)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleCasePack]),
                MinUnits = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleMinUnits]),
                MinFacings = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRuleMinFacings]),
                PlanogramAssortmentId = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentInventoryRulePlanogramAssortmentId]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssortmentInventoryRule DTOs with a matching Planogram Assortment Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramAssortmentInventoryRule DTOs.</returns>
        public IEnumerable<PlanogramAssortmentInventoryRuleDto> FetchByPlanogramAssortmentId(object id)
        {
            var dtoList = new List<PlanogramAssortmentInventoryRuleDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentInventoryRuleFetchByPlanogramAssortmentId))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRulePlanogramAssortmentId, SqlDbType.Int, (Int32)id);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssortmentInventoryRuleDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentInventoryRuleInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRulePlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleCasePack, SqlDbType.Real, dto.CasePack);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleDaysOfSupply, SqlDbType.Real, dto.DaysOfSupply);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleShelfLife, SqlDbType.Real, dto.ShelfLife);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleReplenishmentDays, SqlDbType.Real, dto.ReplenishmentDays);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleUnits,SqlDbType.Real,dto.WasteHurdleUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleCasePack,SqlDbType.Real,dto.WasteHurdleCasePack);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleMinUnits,SqlDbType.Int,dto.MinUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleMinFacings,SqlDbType.Int,dto.MinFacings);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            foreach (PlanogramAssortmentInventoryRuleDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssortmentInventoryRuleDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentInventoryRuleUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRulePlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleCasePack, SqlDbType.Real, dto.CasePack);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleDaysOfSupply, SqlDbType.Real, dto.DaysOfSupply);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleShelfLife, SqlDbType.Real, dto.ShelfLife);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleReplenishmentDays, SqlDbType.Real, dto.ReplenishmentDays);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleUnits, SqlDbType.Real, dto.WasteHurdleUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleWasteHurdleCasePack, SqlDbType.Real, dto.WasteHurdleCasePack);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleMinUnits, SqlDbType.Int, dto.MinUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleMinFacings, SqlDbType.Int, dto.MinFacings);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            foreach (PlanogramAssortmentInventoryRuleDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentInventoryRuleDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentInventoryRuleId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentInventoryRuleDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            foreach (PlanogramAssortmentInventoryRuleDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
