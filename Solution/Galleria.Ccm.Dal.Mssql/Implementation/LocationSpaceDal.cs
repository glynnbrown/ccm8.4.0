﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion
#region Version History: (CCM 803)
// V8-29223 : D.Pleasance
//      Removed DateDeleted
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion 
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Location Space Dal
    /// </summary>
    public class LocationSpaceDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationSpaceDal
    {
        #region Constants

        private const String _importTableName = "#tmpLocationSpace";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[LocationSpace_UniqueContentReference] [UNIQUEIDENTIFIER], " +
                "[Entity_Id] [INT], " +
                "[Location_Id] [INT] " +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public LocationSpaceDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationSpaceDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationSpaceId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.LocationSpaceRowVersion])),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.LocationSpaceUniqueContentReference]),
                LocationId = (Int16)GetValue(dr[FieldNames.LocationSpaceLocationId]),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationSpaceEntityId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationSpaceDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationSpaceDateLastModified]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.LocationSpaceParentUniqueContentReference])
            };
        }
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class LocationSpaceBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationSpaceDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public LocationSpaceBulkCopyDataReader(IEnumerable<LocationSpaceDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 3; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.UniqueContentReference;
                        break;
                    case 1:
                        value = _enumerator.Current.EntityId;
                        break;
                    case 2:
                        value = _enumerator.Current.LocationId;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Fetch


        /// <summary>
        /// Returns a single Location Space record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public LocationSpaceDto FetchById(int id)
        {
            LocationSpaceDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(LocationSpaceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceInsert))
                {
                    // Parameter to store returned new Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationSpaceId, SqlDbType.Int);
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.LocationSpaceRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationSpaceDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationSpaceDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    
                    //UCR
                    CreateParameter(command,
                       FieldNames.LocationSpaceUniqueContentReference,
                       SqlDbType.UniqueIdentifier,
                       dto.UniqueContentReference);

                    //location id
                    CreateParameter(command,
                       FieldNames.LocationSpaceLocationId,
                       SqlDbType.SmallInt,
                       dto.LocationId);

                    // entity id
                    CreateParameter(command,
                        FieldNames.LocationSpaceEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    CreateParameter(command, FieldNames.LocationSpaceParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the data source
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(LocationSpaceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceUpdateById))
                {
                    SqlParameter versionParameter =
                           CreateParameter(command,
                           FieldNames.LocationSpaceRowVersion,
                           SqlDbType.Timestamp,
                           ParameterDirection.InputOutput,
                           dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationSpaceDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationSpaceDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    
                    // Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceId,
                        SqlDbType.Int,
                        dto.Id);

                    //UCR
                    CreateParameter(command,
                       FieldNames.LocationSpaceUniqueContentReference,
                       SqlDbType.UniqueIdentifier,
                       dto.UniqueContentReference);

                    // location id
                    CreateParameter(command,
                       FieldNames.LocationSpaceLocationId,
                       SqlDbType.SmallInt,
                       dto.LocationId);

                    // entity id
                    CreateParameter(command,
                        FieldNames.LocationSpaceEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    CreateParameter(command, FieldNames.LocationSpaceParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceId,
                        SqlDbType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified dtos from the data source
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceDeleteByEntityId))
                {
                    // entity id
                    CreateParameter(command,
                        FieldNames.LocationSpaceEntityId,
                        SqlDbType.Int,
                        entityId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Upserts dtos into the data source
        /// </summary>
        /// <param name="dtoList">The dtos to insert</param>
        public void Upsert(IEnumerable<LocationSpaceDto> dtoList)
        {
            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationSpaceDtoKey, LocationSpaceDto> keyedDtoList = new Dictionary<LocationSpaceDtoKey, LocationSpaceDto>();
            foreach (LocationSpaceDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationSpaceBulkCopyDataReader dr = new LocationSpaceBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch upsert
                Dictionary<LocationSpaceDtoKey, Tuple<Int32, RowVersion>> ids = new Dictionary<LocationSpaceDtoKey, Tuple<Int32, RowVersion>>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBulkUpsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Tuple<Int32, RowVersion> outputData =
                                new Tuple<Int32, RowVersion>
                                    (
                                        (Int32)GetValue(dr[FieldNames.LocationSpaceId]),
                                        new RowVersion(GetValue(dr[FieldNames.LocationSpaceRowVersion]))
                                    );

                            LocationSpaceDtoKey key = new LocationSpaceDtoKey();
                            key.LocationId = (Int16)GetValue(dr[FieldNames.LocationSpaceLocationId]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationSpaceDto dto in dtoList)
                {
                    Tuple<Int32, RowVersion> outputData = null;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != null)
                    {
                        dto.Id = outputData.Item1;
                        dto.RowVersion = outputData.Item2;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(string importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
