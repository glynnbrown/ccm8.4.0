﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// GFS-25455 : J.Pickup
//  Added FetchByEntityIdAssortmentIds inc private methods for temporary table creation.
// CCM-26140 : I.George
//  Added FetchByEntityIdIncludingDeleted method
#endregion

#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion

#region Version History: (CCM 8.2.0)
// V8-30461 : M.Shelley
//  Added Entity_Id to FetchByProductGroupId
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation

{
    public class AssortmentInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentInfoDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private AssortmentInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentId]),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.AssortmentUniqueContentReference]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.AssortmentProductGroupId]),
                EntityId = (Int32)GetValue(dr[FieldNames.AssortmentEntityId]),
                Name = (String)GetValue(dr[FieldNames.AssortmentName]),
                ConsumerDecisionTreeId = (Int32?)GetValue(dr[FieldNames.AssortmentConsumerDecisionTreeId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.AssortmentDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.AssortmentDateLastModified]),
                ProductGroupName = (String)GetValue(dr[FieldNames.AssortmentProductGroupName]),
                ProductGroupCode = (String)GetValue(dr[FieldNames.AssortmentProductGroupCode]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.AssortmentParentUniqueContentReference])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<AssortmentInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<AssortmentInfoDto> dtoList = new List<AssortmentInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.AssortmentEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }


        /// <summary>
        /// Returns all dtos for the given product group id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<AssortmentInfoDto> FetchByProductGroupId(Int32 entityId, Int32 productGroupId)
        {
            List<AssortmentInfoDto> dtoList = new List<AssortmentInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInfoFetchByProductGroupId))
                {
                    CreateParameter(command, FieldNames.AssortmentEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.AssortmentProductGroupId, SqlDbType.Int, productGroupId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }


        /// <summary>
        /// Returns the lastest dto for the given entityid and search criteira
        /// </summary>
        /// <param name="entityId">The id of the entity </param>
        /// <param name="name">The name of the content item</param>
        /// <returns></returns>
        public IEnumerable<AssortmentInfoDto> FetchByEntityIdAssortmentSearchCriteria(Int32 entityId, String assortmentSearchCriteria)
        {
            List<AssortmentInfoDto> dtoList = new List<AssortmentInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInfoFetchByEntityIdAssortmentSearchCriteria))
                {
                    CreateParameter(command, FieldNames.AssortmentEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.AssortmentSearchCriteria, SqlDbType.NVarChar, assortmentSearchCriteria);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }


        /// <summary>
        /// Returns the dtos that match the specified entity id and assortment ids
        /// </summary>
        /// <param name="AssortmentId"></param>
        /// <returns></returns>
        public IEnumerable<AssortmentInfoDto> FetchByEntityIdAssortmentIds(Int32 entityId, IEnumerable<Int32> assortmentIds)
        {
            List<AssortmentInfoDto> dtoList = new List<AssortmentInfoDto>();
            try
            {
                // create a temp table for products
                CreateAssortmentTempTable(assortmentIds);

                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInfoFetchByEntityIdAssortmentIds))
                {
                    // parameters
                    CreateParameter(command, FieldNames.AssortmentEntityId, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }

            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                DropAssortmentTempTable();
            }
            return dtoList;
        }
        
        #endregion

        #region Private Methods


        /// <summary>
        /// Creates the assortment temporary table on the given connection
        /// </summary>
        private void CreateAssortmentTempTable(IEnumerable<Int32> productIds)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpAssortments (Assortment_Id INT)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpAssortments (Assortment_Id) VALUES (@Assortment_Id)", CommandType.Text))
            {
                // product Ids
                SqlParameter assortmentIdParameter = CreateParameter(command,
                    "@Assortment_Id",
                    SqlDbType.Int,
                    string.Empty);

                // insert the assortment ids
                foreach (Int32 productId in productIds)
                {
                    assortmentIdParameter.Value = productId;
                    command.ExecuteNonQuery();
                }
            }
        }



        /// <summary>
        /// Drops the assortment temporary table on the given connection
        /// </summary>
        /// <param name="connection">SQL connection to use</param>
        private void DropAssortmentTempTable()
        {
            using (DalCommand command = CreateCommand("DROP TABLE #tmpAssortments", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}