﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramLockDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramLockDal
    {
        #region Delete
        /// <summary>
        /// Deletes locks of the specified type
        /// that are older than the specified lock date
        /// </summary>
        public Boolean DeleteByLockTypeDateLocked(Int32 userId, Byte lockType, DateTime dateLocked)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramLockDeleteByLockTypeDateLocked))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramLockUserId, SqlDbType.Int, userId);
                    CreateParameter(command, FieldNames.PlanogramLockType, SqlDbType.TinyInt, lockType);
                    CreateParameter(command, FieldNames.PlanogramLockDateLocked, SqlDbType.SmallDateTime, dateLocked);

                    //execute
                    Int32 rowsAffected = command.ExecuteNonQuery();

                    // indicate if rows were deleted
                    return rowsAffected > 0;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return false;
        }
        #endregion
    }
}
