﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed stored procedures that are not used
#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class LocationProductAttributeInfoDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationProductAttributeInfoDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private LocationProductAttributeInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationProductAttributeInfoDto
            {

                ProductId = (Int32)GetValue(dr[FieldNames.LocationProductAttributeInfoProductId]),
                LocationId = (Int16)GetValue(dr[FieldNames.LocationProductAttributeInfoLocationId]),
                LocationName = (String)GetValue(dr[FieldNames.LocationName]),
                ProductName = (String)GetValue(dr[FieldNames.ProductName]),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationProductAttributeInfoEntityId]),
                GTIN = (String)GetValue(dr[FieldNames.LocationProductAttributeInfoGTIN]),
                CasePackUnits = (Int16?)GetValue(dr[FieldNames.LocationProductAttributeCasePackUnits]),
                DaysOfSupply = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeInfoDaysOfSupply]),
                DeliveryFrequencyDays = (Single?)GetValue(dr[FieldNames.LocationProductAttributeInfoDeliveryFrequencyDays]),
                ShelfLife = (Int16?)GetValue(dr[FieldNames.LocationProductAttributeInfoShelfLife]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationProductAttributeInfoDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationProductAttributeInfoDateLastModified]),
            };
        }
        #endregion

        #region Methods

        /// <summary>
        /// Creates the location temporary table
        /// </summary>
        private void CreateLocationTempTable(IEnumerable<String> locationCodes)
        {
            //create temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpLocations (Location_Code varchar(50) COLLATE database_default)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            //populate table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpLocations (Location_Code) VALUES (@Location_Code)", CommandType.Text))
            {
                //location code
                SqlParameter locationCodeParameter = CreateParameter(command, "@Location_Code",
                    SqlDbType.NVarChar, String.Empty);

                //insert location codes
                foreach (String locationCode in locationCodes)
                {
                    //to prevent exception when code too long
                    if (locationCode.Length <= 50)
                    {
                        locationCodeParameter.Value = locationCode;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Creates the location temporary table
        /// </summary>
        private void CreateLocationTempTable(IEnumerable<Int16> locationIds)
        {
            //create temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpLocations (Location_Id SMALLINT)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            //populate table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpLocations (Location_Id) VALUES (@Location_Id)", CommandType.Text))
            {
                //location code
                SqlParameter locationIdParameter = CreateParameter(command, "@Location_Id",
                    SqlDbType.NVarChar, String.Empty);

                //insert location codes
                foreach (Int16 locationId in locationIds)
                {
                    locationIdParameter.Value = locationId;
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Drops location temporary table
        /// </summary>
        private void DropLocationTempTable()
        {
            try
            {
                using (DalCommand command = CreateCommand("DROP TABLE #tmpLocations", CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch { }
        }
        #endregion

        #region Fetch

        public List<LocationProductAttributeInfoDto> FetchByEntityId(int entityId)
        {
            List<LocationProductAttributeInfoDto> dtoList = new List<LocationProductAttributeInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationProductAttributeInfoEntityId, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            catch (Exception)
            {
                //check what is coming out
            }

            return dtoList;
        }


        public List<LocationProductAttributeInfoDto> FetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria)
        {
            List<LocationProductAttributeInfoDto> dtoList = new List<LocationProductAttributeInfoDto>();

            try
            {
                String whereClause = CreateSearchCriteriaWhereClause(searchCriteria, "SearchValue");

                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeInfoFetchByEntityIdSearchCriteria))
                {
                    CreateParameter(command, FieldNames.LocationProductAttributeInfoEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, "WhereClause", SqlDbType.NVarChar, whereClause);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }
        private String CreateSearchCriteriaWhereClause(String text, String searchFieldName)
        {
            //split into parts
            String[] criteriaParts = text.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            String whereClause = String.Empty;
            Int32 part = 0;

            foreach (String criteriaPart in criteriaParts)
            {
                //remove punctuation and special chars
                String parsedPart = ParseString(criteriaPart);

                if (!String.IsNullOrEmpty(parsedPart))
                {
                    //check if the part is an operator.
                    if ((parsedPart.Equals("or") || parsedPart.Equals("and")) &&
                        part != 0 &&                        //If this isn't the start of the criteria
                        part != criteriaParts.Length - 1)   //If this isn't the end of the criteria
                    {
                        if (!String.IsNullOrEmpty(whereClause) &&
                            !whereClause.EndsWith("OR") &&
                            !whereClause.EndsWith("AND"))
                        {
                            whereClause = String.Format("{0} {1}", whereClause, parsedPart.ToUpperInvariant());
                        }
                        else if (criteriaParts[part - 1].ToLowerInvariant().Equals("or") ||
                                 criteriaParts[part - 1].ToLowerInvariant().Equals("and"))
                        {
                            //If the previous part was an operator add this as a search term
                            whereClause = String.Format("{0} {1} LIKE '%{2}%'", whereClause, searchFieldName, parsedPart);
                        }
                    }
                    else if ((parsedPart.Equals("or") || parsedPart.Equals("and")) &&
                        (part == 0 ||                        //If this isn't the start of the criteria
                        part == criteriaParts.Length - 1))   //If this isn't the end of the criteria
                    {
                        //part is an operator but is at the start or end of the criteria, so ignore
                    }
                    else //this is a search term
                    {
                        //if there was no clause added last then add an and
                        if (!String.IsNullOrEmpty(whereClause) &&
                            !(whereClause.EndsWith("OR") || whereClause.EndsWith("AND")))
                        {
                            whereClause = String.Format("{0} AND", whereClause);
                        }
                        //add the search term
                        whereClause = String.Format("{0} {1} LIKE '%{2}%'", whereClause, searchFieldName, parsedPart);
                    }
                }
                // indicates which part of criteriaParts is being processed
                part++;
            }

            //Remove trailing operators
            if (whereClause.Contains("%' ") &&
               !whereClause.EndsWith("%'"))
            {
                whereClause = whereClause.Remove(whereClause.LastIndexOf("%' ") + 2);
            }

            return whereClause;
        }

        /// <summary>
        /// Removes all special characters from a string
        /// </summary>
        private String ParseString(String value)
        {
            String parsedPart = String.Empty;
            foreach (Char c in value.Trim().ToLowerInvariant())
            {
                if (Char.IsLetter(c) || Char.IsNumber(c) || Char.IsWhiteSpace(c))
                {
                    parsedPart = String.Format("{0}{1}", parsedPart, c);
                }
            }

            return parsedPart;
        }

        #endregion
    }
}
