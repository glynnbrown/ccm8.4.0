﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25546 : L.Ineson
//  Created
// V8-25787 : N.Foster
//  Added Processing Status
// V8-25881 : A.Probyn
//  Fixed bug in GetDataTransferObject where field value wasn't being looked up
// V8-27411 : M.Pettit
//  Added UserName property
// V8-28232 : N.Foster
//  Add support for batch operations
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Made changes to improve performance of engine
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkpackagePlanogramDal
    /// </summary>
    public sealed class WorkpackagePlanogramDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackagePlanogramDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a data reader from a data transfer object
        /// </summary>
        private static WorkpackagePlanogramDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackagePlanogramDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramId]),
                WorkpackageId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramWorkpackageId]),
                SourcePlanogramId = (Int32?)GetValue(dr[FieldNames.WorkpackagePlanogramSourcePlanogramId]),
                DestinationPlanogramId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramDestinationPlanogramId]),
                Name = (String)GetValue(dr[FieldNames.WorkpackagePlanogramPlanogramName]),
                UserName = (String)GetValue(dr[FieldNames.WorkpackagePlanogramPlanogramUserName]),
                ProcessingStatus = (Byte)GetValue(dr[FieldNames.WorkpackagePlanogramPlanogramAutomationProcessingStatus])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Fetches all items for the specified workpackage
        /// </summary>
        public IEnumerable<WorkpackagePlanogramDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            List<WorkpackagePlanogramDto> dtoList = new List<WorkpackagePlanogramDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramFetchByWorkpackageId))
                {
                    CreateParameter(command, FieldNames.WorkpackagePlanogramWorkpackageId, SqlDbType.Int, workpackageId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dtoList;
        }

        /// <summary>
        /// Fetches the specified workpackage planogram
        /// </summary>
        public WorkpackagePlanogramDto FetchByWorkpackageIdDestinationPlanogramId(Int32 workpackageId, Int32 destinationPlanogramId)
        {
            WorkpackagePlanogramDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramFetchByWorkpackageIdDestinationPlanogramId))
                {
                    CreateParameter(command, FieldNames.WorkpackagePlanogramWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDestinationPlanogramId, SqlDbType.Int, destinationPlanogramId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified item into the database
        /// </summary>
        public void Insert(WorkpackagePlanogramDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkpackagePlanogramId, SqlDbType.Int);
                    SqlParameter destinationIdParameter = CreateParameter(command, FieldNames.WorkpackagePlanogramDestinationPlanogramId, SqlDbType.Int, ParameterDirection.InputOutput, dto.DestinationPlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramWorkpackageId, SqlDbType.Int, dto.WorkpackageId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramSourcePlanogramId, SqlDbType.Int, dto.SourcePlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramPlanogramName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramPlanogramUserName, SqlDbType.NVarChar, dto.UserName);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramPlanogramAutomationProcessingStatus, SqlDbType.TinyInt, dto.ProcessingStatus);

                    // execute
                    command.ExecuteNonQuery();

                    // update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.DestinationPlanogramId = (Int32)destinationIdParameter.Value;
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        /// <summary>
        /// Inserts a batch of dtos into the database
        /// </summary>
        /// <param name="dtos">The list of dtos to insert</param>
        public void Insert(IEnumerable<WorkpackagePlanogramDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk insert for performance
            foreach (WorkpackagePlanogramDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified item within the database
        /// </summary>
        public void Update(WorkpackagePlanogramDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramUpdateById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramWorkpackageId, SqlDbType.Int, dto.WorkpackageId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramSourcePlanogramId, SqlDbType.Int, dto.SourcePlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDestinationPlanogramId, SqlDbType.Int, dto.DestinationPlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramPlanogramName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramPlanogramAutomationProcessingStatus, SqlDbType.TinyInt, dto.ProcessingStatus);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        /// <summary>
        /// Updates a batch of dtos in the database
        /// </summary>
        /// <param name="dtos">The list of dtos to update</param>
        public void Update(IEnumerable<WorkpackagePlanogramDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk update for performance
            foreach (WorkpackagePlanogramDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified item from the database
        /// </summary>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramDeleteById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramId, SqlDbType.Int, id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        /// <summary>
        /// Deletes the specified item from the database
        /// </summary>
        public void Delete(WorkpackagePlanogramDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes a batch of dtos from the database
        /// </summary>
        /// <param name="dtos">The list of dtos to delete</param>
        public void Delete(IEnumerable<WorkpackagePlanogramDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk delete for performance
            foreach (WorkpackagePlanogramDto dto in dtos)
            {
                this.DeleteById(dto.Id);
            }
        }
        #endregion
    }
}
