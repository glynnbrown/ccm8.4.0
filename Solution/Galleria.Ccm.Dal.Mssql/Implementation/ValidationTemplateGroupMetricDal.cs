﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26514 : A.Silva ~ Added Score fields.
// V8-26812 : A.Silva ~ Added ValidationType fields.
#endregion

#region Version History: CCM803
// V8-29596 : L.Luong
//  Added Criteria
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     ValidationTemplateGroupMetric Dal
    /// </summary>
    public class ValidationTemplateGroupMetricDal : DalBase, IValidationTemplateGroupMetricDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static ValidationTemplateGroupMetricDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ValidationTemplateGroupMetricDto
            {
                Id = (Int32) GetValue(dr[FieldNames.ValidationTemplateGroupMetricId]),
                ValidationTemplateGroupId =
                    (Int32) GetValue(dr[FieldNames.ValidationTemplateGroupMetricValidationTemplateGroupId]),
                Field = (String) GetValue(dr[FieldNames.ValidationTemplateGroupMetricField]),
                Threshold1 = (Single) GetValue(dr[FieldNames.ValidationTemplateGroupMetricThreshold1]),
                Threshold2 = (Single)GetValue(dr[FieldNames.ValidationTemplateGroupMetricThreshold2]),
                Score1 = (Single)GetValue(dr[FieldNames.ValidationTemplateGroupMetricScore1]),
                Score2 = (Single)GetValue(dr[FieldNames.ValidationTemplateGroupMetricScore2]),
                Score3 = (Single)GetValue(dr[FieldNames.ValidationTemplateGroupMetricScore3]),
                AggregationType = (Byte)GetValue(dr[FieldNames.ValidationTemplateGroupMetricAggregationType]),
                ValidationType = (Byte) GetValue(dr[FieldNames.ValidationTemplateGroupMetricValidationType]),
                Criteria = (String)GetValue(dr[FieldNames.ValidationTemplateGroupMetricCriteria])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a dto that matches the specified id
        /// </summary>
        /// <param name="validationTemplateGroupId"></param>
        /// <returns>matching dto</returns>
        public IEnumerable<ValidationTemplateGroupMetricDto> FetchByValidationTemplateGroupId(
            Int32 validationTemplateGroupId)
        {
            var dtoList = new List<ValidationTemplateGroupMetricDto>();
            try
            {
                using (
                    var command =
                        CreateCommand(ProcedureNames.ValidationTemplateGroupMetricFetchByValidationTemplateGroupId))
                {
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricValidationTemplateGroupId,
                        SqlDbType.Int, validationTemplateGroupId);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ValidationTemplateGroupMetricDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateGroupMetricInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.ValidationTemplateGroupMetricId,
                        SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricValidationTemplateGroupId,
                        SqlDbType.Int, dto.ValidationTemplateGroupId);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricScore1, SqlDbType.Real,
                        dto.Score1);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricScore2, SqlDbType.Real,
                        dto.Score2);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricScore3, SqlDbType.Real,
                        dto.Score3);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricAggregationType, SqlDbType.TinyInt,
                        dto.AggregationType);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricValidationType, SqlDbType.TinyInt,
                        dto.ValidationType);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricCriteria, SqlDbType.NVarChar,
                          dto.Criteria);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32) idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ValidationTemplateGroupMetricDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateGroupMetricUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricValidationTemplateGroupId,
                        SqlDbType.Int, dto.ValidationTemplateGroupId);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricScore1, SqlDbType.Real,
                        dto.Score1);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricScore2, SqlDbType.Real,
                        dto.Score2);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricScore3, SqlDbType.Real,
                        dto.Score3);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricAggregationType, SqlDbType.TinyInt,
                        dto.AggregationType);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricValidationType, SqlDbType.TinyInt,
                        dto.ValidationType);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricCriteria, SqlDbType.NVarChar,
                          dto.Criteria);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateGroupMetricDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ValidationTemplateGroupMetricId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}