﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-27132 : A.Kuszyk
//  Added Product Gtin field.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ConsumerDecisionTreeNodeProduct Dal
    /// </summary>
    public class ConsumerDecisionTreeNodeProductDal : Galleria.Framework.Dal.Mssql.DalBase, IConsumerDecisionTreeNodeProductDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ConsumerDecisionTreeNodeProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ConsumerDecisionTreeNodeProductDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeProductId]),
                ConsumerDecisionTreeNodeId = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeProductConsumerDecisionTreeNodeId]),
                ProductId = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeProductProductId]),
                ProductGtin = (String)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeProductProductGtin]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeProductDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeNodeProductDateLastModified])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ConsumerDecisionTreeNodeProductDto FetchById(Int32 id)
        {
            ConsumerDecisionTreeNodeProductDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeProductFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified cdt id
        /// </summary>
        /// <param name="consumerDecisionTreeId">specified cdt id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ConsumerDecisionTreeNodeProductDto> FetchByConsumerDecisionTreeNodeId(Int32 consumerDecisionTreeNodeId)
        {
            List<ConsumerDecisionTreeNodeProductDto> dtoList = new List<ConsumerDecisionTreeNodeProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeProductFetchByConsumerDecisionTreeNodeId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductConsumerDecisionTreeNodeId, SqlDbType.Int, consumerDecisionTreeNodeId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ConsumerDecisionTreeNodeProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeProductInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductConsumerDecisionTreeNodeId, SqlDbType.Int, dto.ConsumerDecisionTreeNodeId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductProductId, SqlDbType.Int, dto.ProductId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ConsumerDecisionTreeNodeProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeProductUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductConsumerDecisionTreeNodeId, SqlDbType.Int, dto.ConsumerDecisionTreeNodeId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductProductId, SqlDbType.Int, dto.ProductId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeNodeProductDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeNodeProductId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}