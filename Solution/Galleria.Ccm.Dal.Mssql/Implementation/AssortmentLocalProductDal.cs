﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26704 : A.Kuszyk
//  Added LocationCode and ProductGtin.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation

{
    public class AssortmentLocalProductDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentLocalProductDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentLocalProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentLocalProductDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentLocalProductId]),
                AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentLocalProductAssortmentId]),
                LocationId = (Int16)GetValue(dr[FieldNames.AssortmentLocalProductLocationId]),
                ProductId = (Int32)GetValue(dr[FieldNames.AssortmentLocalProductProductId]),
                LocationCode = (String)GetValue(dr[FieldNames.AssortmentLocalProductLocationCode]),
                ProductGTIN = (String)GetValue(dr[FieldNames.AssortmentLocalProductProductGTIN])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all product dtos for the specified assortment
        /// </summary>
        /// <param name="assortmentId">The assortment id</param>
        /// <returns>All product dtos for the assortment</returns>
        public IEnumerable<AssortmentLocalProductDto> FetchByAssortmentId(Int32 assortmentId)
        {
            List<AssortmentLocalProductDto> dtoList = new List<AssortmentLocalProductDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocalProductFetchByAssortmentId))
            {
                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductAssortmentId,
                    SqlDbType.Int,
                    assortmentId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment local product id</param>
        /// <returns>The assortment local product</returns>
        public AssortmentLocalProductDto FetchById(Int32 id)
        {
            AssortmentLocalProductDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocalProductFetchById))
            {
                // assortment local product id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentLocalProductDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocalProductInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentLocalProductId,
                    SqlDbType.Int);

                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductAssortmentId,
                    SqlDbType.Int,
                    dto.AssortmentId);

                // product id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductProductId,
                    SqlDbType.Int,
                    dto.ProductId);

                // location id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductLocationId,
                    SqlDbType.SmallInt,
                    dto.LocationId);

                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentLocalProductDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocalProductUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentLocalProductId,
                    SqlDbType.Int,
                    dto.Id);

                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductAssortmentId,
                    SqlDbType.Int,
                    dto.AssortmentId);

                // product id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductProductId,
                    SqlDbType.Int,
                    dto.ProductId);

                // location id
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductLocationId,
                    SqlDbType.SmallInt,
                    dto.LocationId);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentLocalProductDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentLocalProductId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}