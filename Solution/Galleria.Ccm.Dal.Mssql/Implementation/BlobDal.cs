﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation

{
    public class BlobDal : Galleria.Framework.Dal.Mssql.DalBase, IBlobDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private BlobDto GetDataTransferObject(SqlDataReader dr)
        {
            return new BlobDto
            {
                Id = (Int32)GetValue(dr[FieldNames.BlobId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.BlobRowVersion])),
                Data = (Byte[])GetValue(dr[FieldNames.BlobData]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.BlobDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.BlobDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.BlobDateDeleted])
            };
        }
        #endregion

        #region Fetch

        public BlobDto FetchById(int id)
        {
            BlobDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlobFetchById))
                {
                    CreateParameter(command, FieldNames.BlobId, SqlDbType.Int, id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        public void Insert(BlobDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlobInsert))
                {
                    // Parameter to store returned new Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.BlobId, SqlDbType.Int);
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.BlobRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.BlobDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.BlobDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.BlobDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Add in the other parameters
                    CreateParameter(command, FieldNames.BlobData, SqlDbType.Image, dto.Data);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (int)idParameter.Value;
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(BlobDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlobUpdateById))
                {
                    SqlParameter versionParameter =
                        CreateParameter(command,
                        FieldNames.BlobRowVersion,
                        SqlDbType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.BlobDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.BlobDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.BlobDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    CreateParameter(command, FieldNames.BlobId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.BlobData, SqlDbType.Image, dto.Data);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlobDeleteById))
                {
                    CreateParameter(command, FieldNames.BlobId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
