﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25559 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class EntityInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IEntityInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>An entity info dto</returns>
        private EntityInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new EntityInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.EntityId]),
                Name = (String)GetValue(dr[FieldNames.EntityName]),
                Description = (String)GetValue(dr[FieldNames.EntityDescription])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all entity infos in the database
        /// </summary>
        /// <returns>A list of entity info dtos</returns>
        public IEnumerable<EntityInfoDto> FetchAll()
        {
            List<EntityInfoDto> dtoList = new List<EntityInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityInfoFetchAll))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion
    }
}
