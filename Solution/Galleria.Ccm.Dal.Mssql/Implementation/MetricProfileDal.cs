﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
// V8-27548 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MetricProfile Dal
    /// </summary>
    public class MetricProfileDal : Galleria.Framework.Dal.Mssql.DalBase, IMetricProfileDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static MetricProfileDto GetDataTransferObject(SqlDataReader dr)
        {
            return new MetricProfileDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.MetricProfileId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.MetricProfileRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.MetricProfileEntityId]),
                Name = (String)GetValue(dr[FieldNames.MetricProfileName]),
                Metric1MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric1MetricId]),
                Metric1Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric1Ratio]),
                Metric2MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric2MetricId]),
                Metric2Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric2Ratio]),
                Metric3MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric3MetricId]),
                Metric3Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric3Ratio]),
                Metric4MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric4MetricId]),
                Metric4Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric4Ratio]),
                Metric5MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric5MetricId]),
                Metric5Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric5Ratio]),
                Metric6MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric6MetricId]),
                Metric6Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric6Ratio]),
                Metric7MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric7MetricId]),
                Metric7Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric7Ratio]),
                Metric8MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric8MetricId]),
                Metric8Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric8Ratio]),
                Metric9MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric9MetricId]),
                Metric9Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric9Ratio]),
                Metric10MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric10MetricId]),
                Metric10Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric10Ratio]),
                Metric11MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric11MetricId]),
                Metric11Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric11Ratio]),
                Metric12MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric12MetricId]),
                Metric12Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric12Ratio]),
                Metric13MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric13MetricId]),
                Metric13Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric13Ratio]),
                Metric14MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric14MetricId]),
                Metric14Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric14Ratio]),
                Metric15MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric15MetricId]),
                Metric15Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric15Ratio]),
                Metric16MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric16MetricId]),
                Metric16Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric16Ratio]),
                Metric17MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric17MetricId]),
                Metric17Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric17Ratio]),
                Metric18MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric18MetricId]),
                Metric18Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric18Ratio]),
                Metric19MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric19MetricId]),
                Metric19Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric19Ratio]),
                Metric20MetricId = (Int32?)GetValue(dr[FieldNames.MetricProfileMetric20MetricId]),
                Metric20Ratio = (Single?)GetValue(dr[FieldNames.MetricProfileMetric20Ratio]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.MetricProfileDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.MetricProfileDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.MetricProfileDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public MetricProfileDto FetchById(Int32 id)
        {
            MetricProfileDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricProfileFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.MetricProfileId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified entity id and name.
        /// </summary>
        public MetricProfileDto FetchByEntityIdName(Int32 entityId, String name)
        {
            MetricProfileDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricProfileFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.MetricProfileEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.MetricProfileName, SqlDbType.NVarChar, name);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(MetricProfileDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricProfileInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.MetricProfileId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.MetricProfileRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.MetricProfileDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.MetricProfileDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.MetricProfileDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.MetricProfileEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.MetricProfileName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.MetricProfileMetric1MetricId, SqlDbType.Int, dto.Metric1MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric1Ratio, SqlDbType.Real, dto.Metric1Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric2MetricId, SqlDbType.Int, dto.Metric2MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric2Ratio, SqlDbType.Real, dto.Metric2Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric3MetricId, SqlDbType.Int, dto.Metric3MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric3Ratio, SqlDbType.Real, dto.Metric3Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric4MetricId, SqlDbType.Int, dto.Metric4MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric4Ratio, SqlDbType.Real, dto.Metric4Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric5MetricId, SqlDbType.Int, dto.Metric5MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric5Ratio, SqlDbType.Real, dto.Metric5Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric6MetricId, SqlDbType.Int, dto.Metric6MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric6Ratio, SqlDbType.Real, dto.Metric6Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric7MetricId, SqlDbType.Int, dto.Metric7MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric7Ratio, SqlDbType.Real, dto.Metric7Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric8MetricId, SqlDbType.Int, dto.Metric8MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric8Ratio, SqlDbType.Real, dto.Metric8Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric9MetricId, SqlDbType.Int, dto.Metric9MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric9Ratio, SqlDbType.Real, dto.Metric9Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric10MetricId, SqlDbType.Int, dto.Metric10MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric10Ratio, SqlDbType.Real, dto.Metric10Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric11MetricId, SqlDbType.Int, dto.Metric11MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric11Ratio, SqlDbType.Real, dto.Metric11Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric12MetricId, SqlDbType.Int, dto.Metric12MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric12Ratio, SqlDbType.Real, dto.Metric12Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric13MetricId, SqlDbType.Int, dto.Metric13MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric13Ratio, SqlDbType.Real, dto.Metric13Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric14MetricId, SqlDbType.Int, dto.Metric14MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric14Ratio, SqlDbType.Real, dto.Metric14Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric15MetricId, SqlDbType.Int, dto.Metric15MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric15Ratio, SqlDbType.Real, dto.Metric15Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric16MetricId, SqlDbType.Int, dto.Metric16MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric16Ratio, SqlDbType.Real, dto.Metric16Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric17MetricId, SqlDbType.Int, dto.Metric17MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric17Ratio, SqlDbType.Real, dto.Metric17Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric18MetricId, SqlDbType.Int, dto.Metric18MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric18Ratio, SqlDbType.Real, dto.Metric18Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric19MetricId, SqlDbType.Int, dto.Metric19MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric19Ratio, SqlDbType.Real, dto.Metric19Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric20MetricId, SqlDbType.Int, dto.Metric20MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric20Ratio, SqlDbType.Real, dto.Metric20Ratio);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(MetricProfileDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricProfileUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.MetricProfileId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.MetricProfileRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.MetricProfileDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.MetricProfileDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.MetricProfileDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.MetricProfileEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.MetricProfileName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.MetricProfileMetric1MetricId, SqlDbType.Int, dto.Metric1MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric1Ratio, SqlDbType.Real, dto.Metric1Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric2MetricId, SqlDbType.Int, dto.Metric2MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric2Ratio, SqlDbType.Real, dto.Metric2Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric3MetricId, SqlDbType.Int, dto.Metric3MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric3Ratio, SqlDbType.Real, dto.Metric3Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric4MetricId, SqlDbType.Int, dto.Metric4MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric4Ratio, SqlDbType.Real, dto.Metric4Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric5MetricId, SqlDbType.Int, dto.Metric5MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric5Ratio, SqlDbType.Real, dto.Metric5Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric6MetricId, SqlDbType.Int, dto.Metric6MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric6Ratio, SqlDbType.Real, dto.Metric6Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric7MetricId, SqlDbType.Int, dto.Metric7MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric7Ratio, SqlDbType.Real, dto.Metric7Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric8MetricId, SqlDbType.Int, dto.Metric8MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric8Ratio, SqlDbType.Real, dto.Metric8Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric9MetricId, SqlDbType.Int, dto.Metric9MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric9Ratio, SqlDbType.Real, dto.Metric9Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric10MetricId, SqlDbType.Int, dto.Metric10MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric10Ratio, SqlDbType.Real, dto.Metric10Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric11MetricId, SqlDbType.Int, dto.Metric11MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric11Ratio, SqlDbType.Real, dto.Metric11Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric12MetricId, SqlDbType.Int, dto.Metric12MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric12Ratio, SqlDbType.Real, dto.Metric12Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric13MetricId, SqlDbType.Int, dto.Metric13MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric13Ratio, SqlDbType.Real, dto.Metric13Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric14MetricId, SqlDbType.Int, dto.Metric14MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric14Ratio, SqlDbType.Real, dto.Metric14Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric15MetricId, SqlDbType.Int, dto.Metric15MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric15Ratio, SqlDbType.Real, dto.Metric15Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric16MetricId, SqlDbType.Int, dto.Metric16MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric16Ratio, SqlDbType.Real, dto.Metric16Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric17MetricId, SqlDbType.Int, dto.Metric17MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric17Ratio, SqlDbType.Real, dto.Metric17Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric18MetricId, SqlDbType.Int, dto.Metric18MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric18Ratio, SqlDbType.Real, dto.Metric18Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric19MetricId, SqlDbType.Int, dto.Metric19MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric19Ratio, SqlDbType.Real, dto.Metric19Ratio);
                    CreateParameter(command, FieldNames.MetricProfileMetric20MetricId, SqlDbType.Int, dto.Metric20MetricId);
                    CreateParameter(command, FieldNames.MetricProfileMetric20Ratio, SqlDbType.Real, dto.Metric20Ratio);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricProfileDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.MetricProfileId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}