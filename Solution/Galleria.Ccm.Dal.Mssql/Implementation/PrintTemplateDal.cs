﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MSSQL implementation of IPrintTemplateDal
    /// </summary>
    public sealed class PrintTemplateDal : Galleria.Framework.Dal.Mssql.DalBase, IPrintTemplateDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PrintTemplateDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PrintTemplateDto()
            {
                Id = GetValue(dr[FieldNames.PrintTemplateId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PrintTemplateRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.PrintTemplateEntityId]),
                Name = (String)GetValue(dr[FieldNames.PrintTemplateName]),
                Description = (String)GetValue(dr[FieldNames.PrintTemplateDescription]),
                PaperSize = (Byte)GetValue(dr[FieldNames.PrintTemplatePaperSize]),
                IsPlanMirrored = (Boolean)GetValue(dr[FieldNames.PrintTemplateIsPlanMirrored]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.PrintTemplateDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.PrintTemplateDateLastModified]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PrintTemplateDto FetchById(Object id)
        {
            PrintTemplateDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PrintTemplateDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PrintTemplateId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PrintTemplateRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PrintTemplateDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.PrintTemplateDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PrintTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PrintTemplateDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PrintTemplatePaperSize, SqlDbType.TinyInt, dto.PaperSize);
                    CreateParameter(command, FieldNames.PrintTemplateIsPlanMirrored, SqlDbType.Bit, dto.IsPlanMirrored);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PrintTemplateDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PrintTemplateRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PrintTemplateDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.PrintTemplateDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PrintTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PrintTemplateDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PrintTemplatePaperSize, SqlDbType.TinyInt, dto.PaperSize);
                    CreateParameter(command, FieldNames.PrintTemplateIsPlanMirrored, SqlDbType.Bit, dto.IsPlanMirrored);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks an item for editing.
        /// </summary>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void LockById(Object id)
        {
            // Not implemented as this dal does not use locking mechamisms.
        }

        /// <summary>
        ///     Unlocks an item after editing.
        /// </summary>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }

        #endregion
    }
}