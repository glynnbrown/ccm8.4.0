﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//  Created (Auto-generated)
// V8-26954 : A.Silva
//  Added ResultType.
// V8-26812 : A.Silva
//  Added ValidationType.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29596 : L.Luong
//  Added Criteria
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramValidationTemplateGroupMetric Dal
    /// </summary>
    public class PlanogramValidationTemplateGroupMetricDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramValidationTemplateGroupMetricDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramValidationTemplateGroupMetricDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramValidationTemplateGroupMetricDto
            {
                Id = (Int32) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricId]),
                PlanogramValidationTemplateGroupId =
                    (Int32)
                        GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId]),
                Field = (String) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricField]),
                Threshold1 = (Single) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricThreshold1]),
                Threshold2 = (Single) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricThreshold2]),
                Score1 = (Single) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricScore1]),
                Score2 = (Single) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricScore2]),
                Score3 = (Single) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricScore3]),
                AggregationType = (Byte)GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricAggregationType]),
                ResultType = (Byte) GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricResultType]),
                ValidationType = (Byte)GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricValidationType]),
                Criteria = (String)GetValue(dr[FieldNames.PlanogramValidationTemplateGroupMetricCriteria])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a dto that matches the specified id
        /// </summary>
        /// <param name="planogramValidationTemplateGroupId"></param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramValidationTemplateGroupMetricDto> FetchByPlanogramValidationTemplateGroupId(
            object planogramValidationTemplateGroupId)
        {
            var dtoList = new List<PlanogramValidationTemplateGroupMetricDto>();
            try
            {
                using (
                    var command =
                        CreateCommand(
                            ProcedureNames
                                .PlanogramValidationTemplateGroupMetricFetchByPlanogramValidationTemplateGroupId))
                {
                    CreateParameter(command,
                        FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId,
                        SqlDbType.Int, (Int32) planogramValidationTemplateGroupId);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramValidationTemplateGroupMetricDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateGroupMetricInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command,
                        FieldNames.PlanogramValidationTemplateGroupMetricId,
                        SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command,
                        FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId,
                        SqlDbType.Int, dto.PlanogramValidationTemplateGroupId);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricScore1, SqlDbType.Real,
                        dto.Score1);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricScore2, SqlDbType.Real,
                        dto.Score2);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricScore3, SqlDbType.Real,
                        dto.Score3);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricAggregationType,
                        SqlDbType.TinyInt,
                        dto.AggregationType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricResultType,
                        SqlDbType.TinyInt,
                        dto.ResultType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricValidationType,
                        SqlDbType.TinyInt,
                        dto.ValidationType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricCriteria,
                         SqlDbType.NVarChar,
                         dto.Criteria);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32) idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupMetricDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramValidationTemplateGroupMetricDto dto)
        {
            try
            {
                using (
                    var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateGroupMetricUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command,
                        FieldNames.PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId,
                        SqlDbType.Int, dto.PlanogramValidationTemplateGroupId);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricScore1, SqlDbType.Real,
                        dto.Score1);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricScore2, SqlDbType.Real,
                        dto.Score2);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricScore3, SqlDbType.Real,
                        dto.Score3);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricAggregationType,
                        SqlDbType.TinyInt,
                        dto.AggregationType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricResultType,
                        SqlDbType.TinyInt,
                        dto.ResultType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricValidationType,
                        SqlDbType.TinyInt,
                        dto.ValidationType);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricCriteria,
                          SqlDbType.NVarChar,
                          dto.Criteria);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupMetricDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(object id)
        {
            try
            {
                using (
                    var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateGroupMetricDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateGroupMetricId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramValidationTemplateGroupMetricDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            foreach (PlanogramValidationTemplateGroupMetricDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}