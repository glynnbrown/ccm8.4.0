﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0.0)
// CCM-25628 : A.Kuszyk
//		Created (Auto-generated)
// CCM-25454 : J.Pickup
//		Added new method FetchByEntityIdIncludingDeleted
// V8-25556 : J.Pickup
//      Added FetchAllIncludingDeleted() 
// V8-27918 : J.Pickup
//      Added FetchByWorkpackageIdIncludingDeleted()
#endregion

#region Version History: (CCM 8.1.0)
// V8-29667 : N.Haywood
//  Removed FetchAllIncludingDeleted
#endregion

#region Version History: (CCM 8.2.0)
// V8-30802 : M.Shelley
//  Lowered the default SQL command timeout to prevent unresponsive behaviour
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// LocationInfo Dal
    /// </summary>
    public class LocationInfoDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationInfoDal
    {
        #region Constants
        private const String _importTableName = "#tmpLocations";
        private const String _createCodeTableSql = "CREATE TABLE [{0}] (Location_Code NVARCHAR(50) COLLATE database_default)";
        private const String _dropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        private const int TimeoutLimit = 10;

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static LocationInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationInfoDto()
            {
                Id = (Int16)GetValue(dr[FieldNames.LocationId]),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationEntityId]),
                LocationGroupId = (Int32)GetValue(dr[FieldNames.LocationLocationGroupId]),
                Code = (String)GetValue(dr[FieldNames.LocationCode]),
                Name = (String)GetValue(dr[FieldNames.LocationName]),
                Region = (String)GetValue(dr[FieldNames.LocationRegion]),
                County = (String)GetValue(dr[FieldNames.LocationCounty]),
                TVRegion = (String)GetValue(dr[FieldNames.LocationTVRegion]),
                Address1 = (String)GetValue(dr[FieldNames.LocationAddress1]),
                Address2 = (String)GetValue(dr[FieldNames.LocationAddress2]),
                PostalCode = (String)GetValue(dr[FieldNames.LocationPostalCode]),
                City = (String)GetValue(dr[FieldNames.LocationCity]),
                Country = (String)GetValue(dr[FieldNames.LocationCountry]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.LocationDateDeleted])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<LocationInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<LocationInfoDto> dtoList = new List<LocationInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationInfoFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.Int, entityId);
                    command.CommandTimeout = TimeoutLimit;    // Reduced from 

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id inlcuding records marked as deleted
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<LocationInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            List<LocationInfoDto> dtoList = new List<LocationInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationInfoFetchByEntityIdIncludingDeleted))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that linked to the the specified workpackage id inlcuding records marked as deleted
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<LocationInfoDto> FetchByWorkpackageIdIncludingDeleted(Int32 workpackageId)
        {
            List<LocationInfoDto> dtoList = new List<LocationInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationInfoFetchByWorkpackageIdIncludingDeleted))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.WorkpackageId, SqlDbType.Int, workpackageId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that linked to the the specified workpackage planogram id inlcuding records marked as deleted
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<LocationInfoDto> FetchByWorkpackagePlanogramIdIncludingDeleted(Int32 planogramId)
        {
            List<LocationInfoDto> dtoList = new List<LocationInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationInfoFetchByWorkpackagePlanogramIdIncludingDeleted))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.PlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a list of LocationInfos with matching Entity Id and Location Codes.
        /// </summary>
        /// <param name="entityId">The Entity Id to match.</param>
        /// <param name="locationCodes">The Location Codes to match.</param>
        /// <returns>List of LocationInfo DTOs.</returns>
        public IEnumerable<LocationInfoDto> FetchByEntityIdLocationCodes(int entityId, IEnumerable<string> locationCodes)
        {
            List<LocationInfoDto> dtoList = new List<LocationInfoDto>();

            //create temp table for locations
            CreateLocationTempTable(locationCodes);

            try
            {
                //execute
                using (DalCommand command = CreateCommand(ProcedureNames.LocationInfoFetchByEntityIdLocationCodes))
                {
                    //entityid
                    CreateParameter(command, FieldNames.LocationEntityId,
                        SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop temp table
            DropLocationTempTable();

            //return items
            return dtoList;
        }
        #endregion

        #region Private Methods
        
        /// <summary>
        /// Creates the location temporary table
        /// </summary>
        private void CreateLocationTempTable(IEnumerable<String> locationCodes)
        {
            //create temp table
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createCodeTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            //populate table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpLocations (Location_Code) VALUES (@Location_Code)", CommandType.Text))
            {
                //location code
                SqlParameter locationCodeParameter = CreateParameter(command, "@Location_Code",
                    SqlDbType.NVarChar, String.Empty);

                //insert location codes
                foreach (String locationCode in locationCodes)
                {
                    //to prevent exception when code too long
                    if (locationCode.Length <= 50)
                    {
                        locationCodeParameter.Value = locationCode;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Drops location temporary table
        /// </summary>
        private void DropLocationTempTable()
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}