﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26270 : A. Kuszyk
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramPerformanceMetricDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramPerformanceMetricDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private PlanogramPerformanceMetricDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramPerformanceMetricDto()
            {
                Id = (Object)GetValue(dr[FieldNames.PlanogramPerformanceMetricId]),
                PlanogramPerformanceId = (Object)GetValue(dr[FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramPerformanceMetricName]),
                Description = (String)GetValue(dr[FieldNames.PlanogramPerformanceMetricDescription]),
                Direction = (Byte)GetValue(dr[FieldNames.PlanogramPerformanceMetricDirection]),
                SpecialType = (Byte)GetValue(dr[FieldNames.PlanogramPerformanceMetricSpecialType]),
                MetricType = (Byte)GetValue(dr[FieldNames.PlanogramPerformanceMetricMetricType]),
                MetricId = (Byte)GetValue(dr[FieldNames.PlanogramPerformanceMetricMetricId]),
                AggregationType = (Byte)GetValue(dr[FieldNames.PlanogramPerformanceMetricAggregationType])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramPerformanceMetricDto> FetchByPlanogramPerformanceId(Object id)
        {
            List<PlanogramPerformanceMetricDto> dtoList = new List<PlanogramPerformanceMetricDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceMetricFetchByPlanogramPerformanceId))
                {
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramPerformanceMetricDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceMetricInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramPerformanceMetricId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId, SqlDbType.Int, dto.PlanogramPerformanceId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricMetricType, SqlDbType.TinyInt, dto.MetricType);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricMetricId, SqlDbType.TinyInt, dto.MetricId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramPerformanceMetricDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceMetricUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricPlanogramPerformanceId, SqlDbType.Int, dto.PlanogramPerformanceId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricMetricType, SqlDbType.TinyInt, dto.MetricType);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricMetricId, SqlDbType.TinyInt, dto.MetricId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceMetricDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramPerformanceMetricId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramPerformanceMetricDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
