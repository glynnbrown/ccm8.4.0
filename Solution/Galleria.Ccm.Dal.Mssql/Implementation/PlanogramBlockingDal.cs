﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramBlocking Dal
    /// </summary>
    public class PlanogramBlockingDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramBlockingDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramBlockingDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramBlockingDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramBlockingId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramBlockingPlanogramId]),
                Type = (Byte)GetValue(dr[FieldNames.PlanogramBlockingType])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramBlockingDto> FetchByPlanogramId(Object planogramId)
        {
            List<PlanogramBlockingDto> dtoList = new List<PlanogramBlockingDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramBlockingPlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramBlockingDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramBlockingId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramBlockingType, SqlDbType.TinyInt, dto.Type);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramBlockingDto> dtos)
        {
            foreach (PlanogramBlockingDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramBlockingDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramBlockingPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramBlockingType, SqlDbType.TinyInt, dto.Type);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified dtos
        /// </summary>
        public void Update(IEnumerable<PlanogramBlockingDto> dtos)
        {
            foreach (PlanogramBlockingDto dto in dtos)
            {
                this.Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramBlockingDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramBlockingId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramBlockingDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramBlockingDto> dtos)
        {
            foreach (PlanogramBlockingDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}