﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// LocationSpaceBay Dal
    /// </summary>
    public class LocationSpaceBayDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationSpaceBayDal
    {
        #region Constants

        private const String _importTableName = "#tmpLocationSpaceBay";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[LocationSpaceProductGroup_Id] [INT], " +
                "[LocationSpaceBay_Order] [TINYINT], " +
                "[LocationSpaceBay_Height] [REAL], " +
                "[LocationSpaceBay_Width] [REAL], " +
                "[LocationSpaceBay_Depth] [REAL], " +
                "[LocationSpaceBay_BaseHeight] [REAL], " +
                "[LocationSpaceBay_BaseWidth] [REAL], " +
                "[LocationSpaceBay_BaseDepth] [REAL], " +
                "[LocationSpaceBay_IsAisleStart] [BIT], " +
                "[LocationSpaceBay_IsAisleEnd] [BIT], " +
                "[LocationSpaceBay_IsAisleRight] [BIT], " +
                "[LocationSpaceBay_IsAisleLeft] [BIT], " +
                "[LocationSpaceBay_BayLocationRef] [NVARCHAR](100) COLLATE database_default, " +
                "[LocationSpaceBay_BayFloorDrawingNo] [NVARCHAR](100) COLLATE database_default, " +
                "[LocationSpaceBay_FixtureName] [NVARCHAR](150) COLLATE database_default, " +
                "[LocationSpaceBay_ManufacturerName] [NVARCHAR](100) COLLATE database_default, " +
                "[LocationSpaceBay_BayType] [TINYINT], " +
                "[LocationSpaceBay_FixtureType] [TINYINT], " +
                "[LocationSpaceBay_BayTypePostFix] [NVARCHAR](50) COLLATE database_default, " +
                "[LocationSpaceBay_BayTypeCalculation] [REAL], " +
                "[LocationSpaceBay_NotchPitch] [REAL], " +
                "[LocationSpaceBay_Barcode] [NVARCHAR](50) COLLATE database_default, " +
                "[LocationSpaceBay_Colour] [NVARCHAR](50) COLLATE database_default, " +
                "[LocationSpaceBay_AssetNumber] [NVARCHAR](100) COLLATE database_default, " +
                "[LocationSpaceBay_Temperature] [REAL], " +
                "[LocationSpaceBay_IsPromotional] [BIT], " +
                "[LocationSpaceBay_HasPower] [BIT], " +
                "[LocationSpaceBay_NotchStartY] [REAL], " +
                "[LocationSpaceBay_FixtureShape] [TINYINT] " +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public LocationSpaceBayDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationSpaceBayDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationSpaceBayId]),
                LocationSpaceProductGroupId = (Int32)GetValue(dr[FieldNames.LocationSpaceBayLocationSpaceProductGroupId]),
                Order = (Byte)GetValue(dr[FieldNames.LocationSpaceBayOrder]),
                Height = (Single)GetValue(dr[FieldNames.LocationSpaceBayHeight]),
                Width = (Single)GetValue(dr[FieldNames.LocationSpaceBayWidth]),
                Depth = (Single)GetValue(dr[FieldNames.LocationSpaceBayDepth]),
                BaseHeight = (Single)GetValue(dr[FieldNames.LocationSpaceBayBaseHeight]),
                BaseWidth = (Single)GetValue(dr[FieldNames.LocationSpaceBayBaseWidth]),
                BaseDepth = (Single)GetValue(dr[FieldNames.LocationSpaceBayBaseDepth]),

                IsAisleStart = (Boolean)GetValue(dr[FieldNames.LocationSpaceBayIsAisleStart]),
                IsAisleEnd = (Boolean)GetValue(dr[FieldNames.LocationSpaceBayIsAisleEnd]),
                IsAisleRight = (Boolean)GetValue(dr[FieldNames.LocationSpaceBayIsAisleRight]),
                IsAisleLeft = (Boolean)GetValue(dr[FieldNames.LocationSpaceBayIsAisleLeft]),
                BayLocationRef = (String)GetValue(dr[FieldNames.LocationSpaceBayBayLocationRef]),
                BayFloorDrawingNo = (String)GetValue(dr[FieldNames.LocationSpaceBayBayFloorDrawingNo]),
                FixtureName = (String)GetValue(dr[FieldNames.LocationSpaceBayFixtureName]),
                ManufacturerName = (String)GetValue(dr[FieldNames.LocationSpaceBayManufacturerName]),
                BayType = (Byte)GetValue(dr[FieldNames.LocationSpaceBayBayType]),
                FixtureType = (Byte)GetValue(dr[FieldNames.LocationSpaceBayFixtureType]),
                BayTypePostFix = (String)GetValue(dr[FieldNames.LocationSpaceBayBayTypePostFix]),
                BayTypeCalculation = (Single?)GetValue(dr[FieldNames.LocationSpaceBayBayTypeCalculation]),
                NotchPitch = (Single?)GetValue(dr[FieldNames.LocationSpaceBayNotchPitch]),
                Barcode = (String)GetValue(dr[FieldNames.LocationSpaceBayBarcode]),
                Colour = (String)GetValue(dr[FieldNames.LocationSpaceBayColour]),
                AssetNumber = (String)GetValue(dr[FieldNames.LocationSpaceBayAssetNumber]),
                Temperature = (Single?)GetValue(dr[FieldNames.LocationSpaceBayTemperature]),
                IsPromotional = (Boolean)GetValue(dr[FieldNames.LocationSpaceBayIsPromotional]),
                HasPower = (Boolean)GetValue(dr[FieldNames.LocationSpaceBayHasPower]),
                NotchStartY = (Single)GetValue(dr[FieldNames.LocationSpaceBayNotchStartY]),
                FixtureShape = (Byte)GetValue(dr[FieldNames.LocationSpaceBayFixtureShape])
            };
        }
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class LocationSpaceBayBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationSpaceBayDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public LocationSpaceBayBulkCopyDataReader(IEnumerable<LocationSpaceBayDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 29; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.LocationSpaceProductGroupId;
                        break;
                    case 1:
                        value = _enumerator.Current.Order;
                        break;
                    case 2:
                        value = _enumerator.Current.Height;
                        break;
                    case 3:
                        value = _enumerator.Current.Width;
                        break;
                    case 4:
                        value = _enumerator.Current.Depth;
                        break;
                    case 5:
                        value = _enumerator.Current.BaseHeight;
                        break;
                    case 6:
                        value = _enumerator.Current.BaseWidth;
                        break;
                    case 7:
                        value = _enumerator.Current.BaseDepth;
                        break;
                    case 8:
                        value = _enumerator.Current.IsAisleStart;
                        break;
                    case 9:
                        value = _enumerator.Current.IsAisleEnd;
                        break;
                    case 10:
                        value = _enumerator.Current.IsAisleRight;
                        break;
                    case 11:
                        value = _enumerator.Current.IsAisleLeft;
                        break;
                    case 12:
                        value = _enumerator.Current.BayLocationRef;
                        break;
                    case 13:
                        value = _enumerator.Current.BayFloorDrawingNo;
                        break;
                    case 14:
                        value = _enumerator.Current.FixtureName;
                        break;
                    case 15:
                        value = _enumerator.Current.ManufacturerName;
                        break;
                    case 16:
                        value = _enumerator.Current.BayType;
                        break;
                    case 17:
                        value = _enumerator.Current.FixtureType;
                        break;
                    case 18:
                        value = _enumerator.Current.BayTypePostFix;
                        break;
                    case 19:
                        value = _enumerator.Current.BayTypeCalculation;
                        break;
                    case 20:
                        value = _enumerator.Current.NotchPitch;
                        break;
                    case 21:
                        value = _enumerator.Current.Barcode;
                        break;
                    case 22:
                        value = _enumerator.Current.Colour;
                        break;
                    case 23:
                        value = _enumerator.Current.AssetNumber;
                        break;
                    case 24:
                        value = _enumerator.Current.Temperature;
                        break;
                    case 25:
                        value = _enumerator.Current.IsPromotional;
                        break;
                    case 26:
                        value = _enumerator.Current.HasPower;
                        break;
                    case 27:
                        value = _enumerator.Current.NotchStartY;
                        break;
                    case 28:
                        value = _enumerator.Current.FixtureShape;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a single Location Space Bay record in the database
        /// </summary>
        /// <returns>A single dtos</returns>
        public LocationSpaceBayDto FetchById(int id)
        {
            LocationSpaceBayDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayFetchById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<LocationSpaceBayDto> FetchByLocationSpaceProductGroupId(Int32 locationSpaceProductGroupId)
        {
            List<LocationSpaceBayDto> dtoList = new List<LocationSpaceBayDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayFetchByLocationSpaceProductGroupId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayLocationSpaceProductGroupId,
                        SqlDbType.Int,
                        locationSpaceProductGroupId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the data source
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(LocationSpaceBayDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayInsert))
                {

                    // Parameter to store returned new Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationSpaceBayId, SqlDbType.Int);

                    // Location Space Content Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayLocationSpaceProductGroupId,
                        SqlDbType.Int,
                        dto.LocationSpaceProductGroupId);
                    //Order
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayOrder,
                        SqlDbType.TinyInt,
                        dto.Order);
                    //Height
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayHeight,
                        SqlDbType.Real,
                        dto.Height);
                    //Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayWidth,
                        SqlDbType.Real,
                        dto.Width);
                    //Depth
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayDepth,
                        SqlDbType.Real,
                        dto.Depth);
                    //Base Height
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBaseHeight,
                        SqlDbType.Real,
                        dto.BaseHeight);
                    //Base Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBaseWidth,
                        SqlDbType.Real,
                        dto.BaseWidth);
                    //Base Depth
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBaseDepth,
                        SqlDbType.Real,
                        dto.BaseDepth);
                    //Is Aisle Start
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleStart,
                        SqlDbType.Bit,
                        dto.IsAisleStart);
                    //Is Aisle End
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleEnd,
                        SqlDbType.Bit,
                        dto.IsAisleEnd);
                    //Is Aisle Right
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleRight,
                        SqlDbType.Bit,
                        dto.IsAisleRight);
                    //Is Aisle Left
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleLeft,
                        SqlDbType.Bit,
                        dto.IsAisleLeft);
                    //Bay Location Ref
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayLocationRef,
                        SqlDbType.NVarChar,
                        dto.BayLocationRef);
                    //Bay Floor Drawing No
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayFloorDrawingNo,
                        SqlDbType.NVarChar,
                        dto.BayFloorDrawingNo);
                    //Fixture Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayFixtureName,
                        SqlDbType.NVarChar,
                        dto.FixtureName);
                    //Manufacturer Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayManufacturerName,
                        SqlDbType.NVarChar,
                        dto.ManufacturerName);
                    //Bay Type
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayType,
                        SqlDbType.TinyInt,
                        dto.BayType);
                    //Fixture Type
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayFixtureType,
                        SqlDbType.TinyInt,
                        dto.FixtureType);
                    //Bay Type Post Fix
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayTypePostFix,
                        SqlDbType.NVarChar,
                        dto.BayTypePostFix);
                    //Bay Calculation
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayTypeCalculation,
                        SqlDbType.Real,
                        dto.BayTypeCalculation);
                    //Notch Pitch
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayNotchPitch,
                        SqlDbType.Real,
                        dto.NotchPitch);
                    //Barcode
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBarcode,
                        SqlDbType.NVarChar,
                        dto.Barcode);
                    //Bay Colour
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayColour,
                        SqlDbType.NVarChar,
                        dto.Colour);
                    //Asset Number
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayAssetNumber,
                        SqlDbType.NVarChar,
                        dto.AssetNumber);
                    //Temperature
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayTemperature,
                        SqlDbType.Real,
                        dto.Temperature);
                    //Is Promotional
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsPromotional,
                        SqlDbType.Bit,
                        dto.IsPromotional);
                    //Has Power
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayHasPower,
                        SqlDbType.Bit,
                        dto.HasPower);
                    //Notch Start Y
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayNotchStartY,
                        SqlDbType.Real,
                        dto.NotchStartY);
                    //FixtureShape
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayFixtureShape,
                        SqlDbType.TinyInt,
                        dto.FixtureShape);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Upserts dtos into the data source
        /// </summary>
        /// <param name="dtoList">The dtos to insert</param>
        public void Insert(IEnumerable<LocationSpaceBayDto> dtoList)
        {
            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationSpaceBayDtoKey, LocationSpaceBayDto> keyedDtoList = new Dictionary<LocationSpaceBayDtoKey, LocationSpaceBayDto>();
            foreach (LocationSpaceBayDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationSpaceBayBulkCopyDataReader dr = new LocationSpaceBayBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch upsert
                Dictionary<LocationSpaceBayDtoKey, Int32> ids = new Dictionary<LocationSpaceBayDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.LocationSpaceBayId]);

                            LocationSpaceBayDtoKey key = new LocationSpaceBayDtoKey();
                            key.LocationSpaceProductGroupId = (Int32)GetValue(dr[FieldNames.LocationSpaceBayLocationSpaceProductGroupId]);
                            key.Order = (Byte)GetValue(dr[FieldNames.LocationSpaceBayOrder]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationSpaceBayDto dto in dtoList)
                {
                    Int32 outputData = 0;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != 0)
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the data source
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(LocationSpaceBayDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayUpdateById))
                {
                    // Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayId,
                        SqlDbType.Int,
                        dto.Id);

                    // Location Space Product Group Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayLocationSpaceProductGroupId,
                        SqlDbType.Int,
                        dto.LocationSpaceProductGroupId);
                    //Order
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayOrder,
                        SqlDbType.TinyInt,
                        dto.Order);
                    //Height
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayHeight,
                        SqlDbType.Real,
                        dto.Height);
                    //Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayWidth,
                        SqlDbType.Real,
                        dto.Width);
                    //Depth
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayDepth,
                        SqlDbType.Real,
                        dto.Depth);
                    //Base Height
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBaseHeight,
                        SqlDbType.Real,
                        dto.BaseHeight);
                    //Base Width
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBaseWidth,
                        SqlDbType.Real,
                        dto.BaseWidth);
                    //Base Depth
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBaseDepth,
                        SqlDbType.Real,
                        dto.BaseDepth);
                    //Is Aisle Start
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleStart,
                        SqlDbType.Bit,
                        dto.IsAisleStart);
                    //Is Aisle End
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleEnd,
                        SqlDbType.Bit,
                        dto.IsAisleEnd);
                    //Is Aisle Right
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleRight,
                        SqlDbType.Bit,
                        dto.IsAisleRight);
                    //Is Aisle Left
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsAisleLeft,
                        SqlDbType.Bit,
                        dto.IsAisleLeft);
                    //Bay Location Ref
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayLocationRef,
                        SqlDbType.NVarChar,
                        dto.BayLocationRef);
                    //Bay Floor Drawing No
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayFloorDrawingNo,
                        SqlDbType.NVarChar,
                        dto.BayFloorDrawingNo);
                    //Fixture Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayFixtureName,
                        SqlDbType.NVarChar,
                        dto.FixtureName);
                    //Manufacturer Name
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayManufacturerName,
                        SqlDbType.NVarChar,
                        dto.ManufacturerName);
                    //Bay Type
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayType,
                        SqlDbType.TinyInt,
                        dto.BayType);
                    //Fixture Type
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayFixtureType,
                        SqlDbType.TinyInt,
                        dto.FixtureType);
                    //Bay Type Post Fix
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayTypePostFix,
                        SqlDbType.NVarChar,
                        dto.BayTypePostFix);
                    //Bay Calculation
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBayTypeCalculation,
                        SqlDbType.Real,
                        dto.BayTypeCalculation);
                    //Notch Pitch
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayNotchPitch,
                        SqlDbType.Real,
                        dto.NotchPitch);
                    //Barcode
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayBarcode,
                        SqlDbType.NVarChar,
                        dto.Barcode);
                    //Bay Colour
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayColour,
                        SqlDbType.NVarChar,
                        dto.Colour);
                    //Asset Number
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayAssetNumber,
                        SqlDbType.NVarChar,
                        dto.AssetNumber);
                    //Temperature
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayTemperature,
                        SqlDbType.Real,
                        dto.Temperature);
                    //Is Promotional
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayIsPromotional,
                        SqlDbType.Bit,
                        dto.IsPromotional);
                    //Has Power
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayHasPower,
                        SqlDbType.Bit,
                        dto.HasPower);
                    //Notch Start Y
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayNotchStartY,
                        SqlDbType.Real,
                        dto.NotchStartY);
                    //FixtureShape
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayFixtureShape,
                        SqlDbType.TinyInt,
                        dto.FixtureShape);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayDeleteById))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceBayId,
                        SqlDbType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified dto from the data source
        /// </summary>
        /// <param name="id">The entity id to delete against</param>
        public void DeleteByEntityId(int entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayDeleteByEntityId))
                {
                    // id
                    CreateParameter(command,
                        FieldNames.LocationSpaceEntityId,
                        SqlDbType.Int,
                        entityId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Upsert

        /// <summary>
        /// Upserts dtos into the data source
        /// </summary>
        /// <param name="dtoList">The dtos to insert</param>
        public void Upsert(IEnumerable<LocationSpaceBayDto> dtoList)
        {
            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationSpaceBayDtoKey, LocationSpaceBayDto> keyedDtoList = new Dictionary<LocationSpaceBayDtoKey, LocationSpaceBayDto>();
            foreach (LocationSpaceBayDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationSpaceBayBulkCopyDataReader dr = new LocationSpaceBayBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch upsert
                Dictionary<LocationSpaceBayDtoKey, Int32> ids = new Dictionary<LocationSpaceBayDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceBayBulkUpsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.LocationSpaceBayId]);

                            LocationSpaceBayDtoKey key = new LocationSpaceBayDtoKey();
                            key.LocationSpaceProductGroupId = (Int32)GetValue(dr[FieldNames.LocationSpaceBayLocationSpaceProductGroupId]);
                            key.Order = (Byte)GetValue(dr[FieldNames.LocationSpaceBayOrder]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (LocationSpaceBayDto dto in dtoList)
                {
                    Int32 outputData = 0;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != 0)
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(string importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
