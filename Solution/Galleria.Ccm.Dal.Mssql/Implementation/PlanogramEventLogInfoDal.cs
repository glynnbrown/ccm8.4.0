﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramEventLogInfoDal Dal
    /// </summary>
    public class PlanogramEventLogInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramEventLogInfoDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramEventLogInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramEventLogInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramEventLogId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramEventLogPlanogramId]),
                EventType = (Byte)GetValue(dr[FieldNames.PlanogramEventLogEventType]),
                EntryType = (Byte)GetValue(dr[FieldNames.PlanogramEventLogEntryType]),
                AffectedType = (Byte?)GetValue(dr[FieldNames.PlanogramEventLogAffectedType]),
                AffectedId = (Int32?)GetValue(dr[FieldNames.PlanogramEventLogAffectedId]),
                WorkpackageSource = (String)GetValue(dr[FieldNames.PlanogramEventLogWorkpackageSource]),
                TaskSource = (String)GetValue(dr[FieldNames.PlanogramEventLogTaskSource]),
                EventId = (Int32)GetValue(dr[FieldNames.PlanogramEventLogEventId]),
                DateTime = (DateTime)GetValue(dr[FieldNames.PlanogramEventLogDateTime]),
                Description = (String)GetValue(dr[FieldNames.PlanogramEventLogDescription]),
                Content = (String)GetValue(dr[FieldNames.PlanogramEventLogContent]),
                Score = (Byte)GetValue(dr[FieldNames.PlanogramEventLogScore]),
                PlanogramName = (String)GetValue(dr[FieldNames.PlanogramEventLogInfoPlanogramName]),
                ExecutionOrder = (Int64)GetValue(dr[FieldNames.PlanogramEventLogInfoExecutionOrder])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramEventLogInfo DTOs with a matching Workpackage Id.
        /// </summary>
        /// <param name="workpackageId">The workpackage Id to match.</param>
        /// <returns>A List of PlanogramEventLogInfo DTOs.</returns>
        public IEnumerable<PlanogramEventLogInfoDto> FetchByWorkpackageId(Int32 workpackageId)
        {
            var dtoList = new List<PlanogramEventLogInfoDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramEventLogInfoFetchByWorkpackageId))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramEventLogInfoWorkpackageId, SqlDbType.Int, (Int32)workpackageId);

                    // Excute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion
    }
}