﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 J.Pickup
//  Created
#endregion
#region Version History: (CCM 8.10)
// V8-28661 : L.Ineson
//  Added FetchByEntityIdName
#endregion
#endregion


using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Data;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Mssql.Schema;
using System.Data.SqlClient;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Highlight Dal
    /// </summary>
    public class HighlightDal : DalBase, IHighlightDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static HighlightDto GetDataTransferObject(IDataRecord dr)
        {
            return new HighlightDto
            {
                Id = (Int32)GetValue(dr[FieldNames.HighlightId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.HighlightRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.HighlightEntityId]),
                Name = (String)GetValue(dr[FieldNames.HighlightName]),
                Type = (Byte)GetValue(dr[FieldNames.HighlightType]),
                Field1 = (String)GetValue(dr[FieldNames.HighlightField1]),
                Field2 = (String)GetValue(dr[FieldNames.HighlightField2]),
                IsAndFilter = (Boolean)GetValue(dr[FieldNames.HighlightIsAndFilter]),
                IsFilterEnabled = (Boolean)GetValue(dr[FieldNames.HighlightIsFilterEnabled]),
                IsPreFilter = (Boolean)GetValue(dr[FieldNames.HighlightIsPreFilter]),
                MethodType = (Byte)GetValue(dr[FieldNames.HighlightMethodType]),
                QuadrantXConstant = (Single)GetValue(dr[FieldNames.HighlightQuadrantXConstant]),
                QuadrantYConstant = (Single)GetValue(dr[FieldNames.HighlightQuadrantYConstant]),
                QuadrantXType = (Byte)GetValue(dr[FieldNames.HighlightQuadrantXType]),
                QuadrantYType  = (Byte)GetValue(dr[FieldNames.HighlightQuadrantYType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.HighlightDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.HighlightDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.HighlightDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="highlightDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="highlightDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="highlightDto" /> with the data for the provided <paramref name="id" />.</returns>
        public HighlightDto FetchById(Object id)
        {
            HighlightDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new Galleria.Framework.Dal.DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        public HighlightDto FetchByEntityIdName(Int32 entityId, String name)
        {
            HighlightDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.HighlightEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.HighlightName, SqlDbType.NVarChar, name);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new Galleria.Framework.Dal.DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="highlightDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(HighlightDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.HighlightId, SqlDbType.Int);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.HighlightRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.HighlightDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.HighlightDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    var dateDeletedParameter = CreateParameter(command, FieldNames.HighlightDateDeleted, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.HighlightName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.HighlightType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.HighlightField1, SqlDbType.NVarChar, dto.Field1);
                    CreateParameter(command, FieldNames.HighlightField2, SqlDbType.NVarChar, dto.Field2);
                    CreateParameter(command, FieldNames.HighlightIsAndFilter, SqlDbType.Bit, dto.IsAndFilter);
                    CreateParameter(command, FieldNames.HighlightIsFilterEnabled, SqlDbType.Bit, dto.IsFilterEnabled);
                    CreateParameter(command, FieldNames.HighlightIsPreFilter, SqlDbType.Bit, dto.IsPreFilter);
                    CreateParameter(command, FieldNames.HighlightMethodType, SqlDbType.TinyInt, dto.MethodType);
                    CreateParameter(command, FieldNames.HighlightQuadrantXConstant, SqlDbType.Real, dto.QuadrantXConstant);
                    CreateParameter(command, FieldNames.HighlightQuadrantYConstant, SqlDbType.Real, dto.QuadrantYConstant);
                    CreateParameter(command, FieldNames.HighlightQuadrantXType, SqlDbType.TinyInt, dto.QuadrantXType);
                    CreateParameter(command, FieldNames.HighlightQuadrantYType, SqlDbType.TinyInt, dto.QuadrantYType);


                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value
                        ? null
                        : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="HighlightDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(HighlightDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightId, SqlDbType.Int, dto.Id);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.HighlightRowVersion, SqlDbType.Timestamp,
                        ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.HighlightDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.HighlightDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    var dateDeletedParameter = CreateParameter(command, FieldNames.HighlightDateDeleted, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.HighlightName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.HighlightType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.HighlightField1, SqlDbType.NVarChar, dto.Field1);
                    CreateParameter(command, FieldNames.HighlightField2, SqlDbType.NVarChar, dto.Field2);
                    CreateParameter(command, FieldNames.HighlightIsAndFilter, SqlDbType.Bit, dto.IsAndFilter);
                    CreateParameter(command, FieldNames.HighlightIsFilterEnabled, SqlDbType.Bit, dto.IsFilterEnabled);
                    CreateParameter(command, FieldNames.HighlightIsPreFilter, SqlDbType.Bit, dto.IsPreFilter);
                    CreateParameter(command, FieldNames.HighlightMethodType, SqlDbType.TinyInt, dto.MethodType);
                    CreateParameter(command, FieldNames.HighlightQuadrantXConstant, SqlDbType.Real, dto.QuadrantXConstant);
                    CreateParameter(command, FieldNames.HighlightQuadrantYConstant, SqlDbType.Real, dto.QuadrantYConstant);
                    CreateParameter(command, FieldNames.HighlightQuadrantXType, SqlDbType.TinyInt, dto.QuadrantXType);
                    CreateParameter(command, FieldNames.HighlightQuadrantYType, SqlDbType.TinyInt, dto.QuadrantYType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value
                        ? null
                        : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a Highlight for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the highlight.</param>
        /// <returns><c>True</c> if the highlight was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void LockById(Object id)
        {
            // Not implemented as this dal does not use locking mechamisms.
        }

        /// <summary>
        ///     Unlocks a highlight after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the highlight.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }

        #endregion
    }
}
