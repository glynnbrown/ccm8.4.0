﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MSSQL implementation of IPrintTemplateComponentDetailDal
    /// </summary>
    public sealed class PrintTemplateComponentDetailDal : Galleria.Framework.Dal.Mssql.DalBase, IPrintTemplateComponentDetailDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PrintTemplateComponentDetailDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PrintTemplateComponentDetailDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PrintTemplateComponentDetailId]),
                PrintTemplateComponentId = (Int32)GetValue(dr[FieldNames.PrintTemplateComponentDetailPrintTemplateComponentId]),
                Key = (String)GetValue(dr[FieldNames.PrintTemplateComponentDetailKey]),
                Value = (String)GetValue(dr[FieldNames.PrintTemplateComponentDetailValue]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PrintTemplateComponentDetailDto FetchById(Int32 id)
        {
            PrintTemplateComponentDetailDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentDetailFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PrintTemplateComponentDetailDto> FetchByPrintTemplateComponentId(Int32 printTemplateComponentId)
        {
            List<PrintTemplateComponentDetailDto> dtoList = new List<PrintTemplateComponentDetailDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentDetailFetchByPrintTemplateComponentId))
                {
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailPrintTemplateComponentId, SqlDbType.Int, printTemplateComponentId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PrintTemplateComponentDetailDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentDetailInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PrintTemplateComponentDetailId, SqlDbType.Int);

                     //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailPrintTemplateComponentId, SqlDbType.Int, dto.PrintTemplateComponentId);
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailKey, SqlDbType.NVarChar, dto.Key);
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailValue, SqlDbType.NVarChar, dto.Value);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PrintTemplateComponentDetailDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentDetailUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailPrintTemplateComponentId, SqlDbType.Int, dto.PrintTemplateComponentId);
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailKey, SqlDbType.NVarChar, dto.Key);
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailValue, SqlDbType.NVarChar, dto.Value);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentDetailDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateComponentDetailId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes all dtos for the given parent PrintTemplateComponentId
        /// </summary>
        public void DeleteByPrintTemplateComponentId(Int32 printTemplateComponentId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateComponentDetailDeleteByPrintTemplateComponentId))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateComponentId, SqlDbType.Int, printTemplateComponentId);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion


       
    }
}