﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26520 : J.Pickup
//  Created Initial Version
// CCM-27891 : J.Pickup
//	Validation and renumbering names added
#endregion
#region Version History: (CCM 801)
// V8-28519 : J.Pickup
//  Removed Date Deleted
#endregion
#region Version History: (CCM 803)
// V8-29746 : A.Probyn
//  Updated to use linked data Id's as well as name
#endregion
#region Version History: (CCM 811)
// V8-30155 : L.Ineson
//  Added FetchByPlanogramIds
#endregion
#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IContentLookupDal
    /// </summary>
    public class ContentLookupDal : Galleria.Framework.Dal.Mssql.DalBase, IContentLookupDal
    {
        #region Constants
        private const String _importTableName = "#tmpContentLookup";
        private const String _createPlanogramIdTableSql = "CREATE TABLE [{0}] (Planogram_Id INT)";
        private const String _dropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region DataTransferObject

        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private ContentLookupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ContentLookupDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ContentLookupId]),
                AssortmentName = (String)GetValue(dr[FieldNames.ContentLookupAssortmentName]),
                ProductUniverseName = (String)GetValue(dr[FieldNames.ContentLookupProductUniverseName]),
                BlockingName = (String)GetValue(dr[FieldNames.ContentLookupBlockingName]),
                InventoryProfileName = (String)GetValue(dr[FieldNames.ContentLookupInventoryProfileName]),
                ConsumerDecisionTreeName = (String)GetValue(dr[FieldNames.ContentLookupConsumerDecisionTreeName]),
                AssortmentMinorRevisionName = (String)GetValue(dr[FieldNames.ContentLookupAssortmentMinorRevisionName]),
                PerformanceSelectionName = (String)GetValue(dr[FieldNames.ContentLookupPerformanceSelectionName]),
                MetricProfileName = (String)GetValue(dr[FieldNames.ContentLookupMetricProfileName]),
                PlanogramName = (String)GetValue(dr[FieldNames.ContentLookupPlanogramName]),
                SequenceName = (String)GetValue(dr[FieldNames.ContentLookupSequenceName]),
                ClusterName = (String)GetValue(dr[FieldNames.ContentLookupClusterName]),
                ClusterSchemeName = (String)GetValue(dr[FieldNames.ContentLookupClusterSchemeName]),
                RenumberingStrategyName = (String)GetValue(dr[FieldNames.ContentLookupRenumberingStrategyName]),
                ValidationTemplateName = (String)GetValue(dr[FieldNames.ContentLookupValidationTemplateName]),
                AssortmentId = (Int32?)GetValue(dr[FieldNames.ContentLookupAssortmentId]),
                ProductUniverseId = (Int32?)GetValue(dr[FieldNames.ContentLookupProductUniverseId]),
                BlockingId = (Int32?)GetValue(dr[FieldNames.ContentLookupBlockingId]),
                InventoryProfileId = (Int32?)GetValue(dr[FieldNames.ContentLookupInventoryProfileId]),
                ConsumerDecisionTreeId = (Int32?)GetValue(dr[FieldNames.ContentLookupConsumerDecisionTreeId]),
                AssortmentMinorRevisionId = (Int32?)GetValue(dr[FieldNames.ContentLookupAssortmentMinorRevisionId]),
                PerformanceSelectionId = (Int32?)GetValue(dr[FieldNames.ContentLookupPerformanceSelectionId]),
                MetricProfileId = (Int32?)GetValue(dr[FieldNames.ContentLookupMetricProfileId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.ContentLookupPlanogramId]),
                SequenceId = (Int32?)GetValue(dr[FieldNames.ContentLookupSequenceId]),
                ClusterId = (Int32?)GetValue(dr[FieldNames.ContentLookupClusterId]),
                ClusterSchemeId = (Int32?)GetValue(dr[FieldNames.ContentLookupClusterSchemeId]),
                RenumberingStrategyId = (Int32?)GetValue(dr[FieldNames.ContentLookupRenumberingStrategyId]),
                ValidationTemplateId = (Int32?)GetValue(dr[FieldNames.ContentLookupValidationTemplateId]),
                PlanogramNameTemplateName = (String)GetValue(dr[FieldNames.ContentLookupPlanogramNameTemplateName]),
                PlanogramNameTemplateId = (Int32?)GetValue(dr[FieldNames.ContentLookupPlanogramNameTemplateId]),
                UserId = (Int32)GetValue(dr[FieldNames.ContentLookupUserId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ContentLookupRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.ContentLookupEntityId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ContentLookupDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ContentLookupDateLastModified]),
                PrintTemplateName = (String)GetValue(dr[FieldNames.ContentLookupPrintTemplateName]),
                PrintTemplateId = (Int32?)GetValue(dr[FieldNames.ContentLookupPrintTemplateId])
            };
        }

        #endregion

        #region Fetch

        #region FetchById

        /// <summary>
        /// Returns specified dto from the database
        /// </summary>
        /// <param name="id">The assortment id</param>
        /// <returns>A assortment dto</returns>
        public ContentLookupDto FetchById(Int32 id)
        {
            ContentLookupDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ContentLookupFetchById))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.ContentLookupId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        #endregion

        #region FetchByPlanogramId

        /// <summary>
        /// Returns specified dto from the database
        /// </summary>
        /// <param name="id">The assortment id</param>
        /// <returns>A assortment dto</returns>
        public ContentLookupDto FetchByPlanogramId(Int32 planogramId)
        {
            ContentLookupDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ContentLookupFetchByPlanogramId))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.ContentLookupPlanogramId,
                        SqlDbType.Int,
                        planogramId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        #endregion

        #region FetchByEntityId

        /// <summary>
        /// Returns dtos for the given entity id.
        /// </summary>
        public IEnumerable<ContentLookupDto> FetchByEntityId(Int32 entityId)
        {
            List<ContentLookupDto> dtoList = new List<ContentLookupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ContentLookupFetchByEntityId))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.ContentLookupEntityId,
                        SqlDbType.Int,
                        entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region FetchByPlanogramIds

        /// <summary>
        /// Returns dtos for the given planogram ids.
        /// </summary>
        public IEnumerable<ContentLookupDto> FetchByPlanogramIds(IEnumerable<Int32> planogramIds)
        {
            List<ContentLookupDto> dtoList = new List<ContentLookupDto>();

            // create a temp table
            CreateTempTable(_createPlanogramIdTableSql);

            // perform a batch insert to temp table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                bulkCopy.WriteToServer(new BulkCopyDataReader<Int32>(planogramIds));
            }

            try
            {
                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.ContentLookupFetchByPlanogramIds))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop the temporary table
            DropTempTable();

            // and return the items
            return dtoList;
        }

        #endregion

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(ContentLookupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ContentLookupInsert))
                {
                    //content Lookup id 
                    SqlParameter idParameter =
                    CreateParameter(command, FieldNames.ContentLookupId, SqlDbType.Int);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.ContentLookupRowVersion,
                        SqlDbType.Timestamp);

                    // entity id
                    CreateParameter(command,
                        FieldNames.ContentLookupEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // user id
                    CreateParameter(command,
                        FieldNames.ContentLookupUserId,
                        SqlDbType.Int,
                        dto.UserId);

                    //planogram id
                    CreateParameter(command,
                        FieldNames.ContentLookupPlanogramId,
                        SqlDbType.Int,
                        dto.PlanogramId);

                    //product universe id
                    CreateParameter(command,
                        FieldNames.ContentLookupProductUniverseId,
                        SqlDbType.Int,
                        dto.ProductUniverseId);

                    //assortment id
                    CreateParameter(command,
                        FieldNames.ContentLookupAssortmentId,
                        SqlDbType.Int,
                        dto.AssortmentId);

                    //blocking id
                    CreateParameter(command,
                        FieldNames.ContentLookupBlockingId,
                        SqlDbType.Int,
                        dto.BlockingId);

                    //sequence id
                    CreateParameter(command,
                        FieldNames.ContentLookupSequenceId,
                        SqlDbType.Int,
                        dto.SequenceId);


                    //metric profile id
                    CreateParameter(command,
                        FieldNames.ContentLookupMetricProfileId,
                        SqlDbType.Int,
                        dto.MetricProfileId);

                    //Inventory profile id
                    CreateParameter(command,
                        FieldNames.ContentLookupInventoryProfileId,
                        SqlDbType.Int,
                        dto.InventoryProfileId);

                    //ConsumerDecisionTree id
                    CreateParameter(command,
                        FieldNames.ContentLookupConsumerDecisionTreeId,
                        SqlDbType.Int,
                        dto.ConsumerDecisionTreeId);

                    //Minor Assortment id
                    CreateParameter(command,
                        FieldNames.ContentLookupAssortmentMinorRevisionId,
                        SqlDbType.Int,
                        dto.AssortmentMinorRevisionId);

                    //performance Selection id
                    CreateParameter(command,
                        FieldNames.ContentLookupPerformanceSelectionId,
                        SqlDbType.Int,
                        dto.PerformanceSelectionId);

                    //cluster id
                    CreateParameter(command,
                        FieldNames.ContentLookupClusterId,
                        SqlDbType.Int,
                        dto.ClusterId);

                    //cluster scheme id
                    CreateParameter(command,
                        FieldNames.ContentLookupClusterSchemeId,
                        SqlDbType.Int,
                        dto.ClusterSchemeId);

                    //renumbering strategy id
                    CreateParameter(command,
                        FieldNames.ContentLookupRenumberingStrategyId,
                        SqlDbType.Int,
                        dto.RenumberingStrategyId);

                    //validation template id
                    CreateParameter(command,
                        FieldNames.ContentLookupValidationTemplateId,
                        SqlDbType.Int,
                        dto.ValidationTemplateId);

                    //planogram name template id
                    CreateParameter(command,
                        FieldNames.ContentLookupPlanogramNameTemplateId,
                        SqlDbType.Int,
                        dto.PlanogramNameTemplateId);

                    //print template id
                    CreateParameter(command,
                        FieldNames.ContentLookupPrintTemplateId,
                        SqlDbType.Int,
                        dto.PrintTemplateId);

                    //date created 
                    SqlParameter dateCreatedParameter =
                    CreateParameter(command, FieldNames.ContentLookupDateCreated, SqlDbType.SmallDateTime);

                    //date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ContentLookupDateLastModified, SqlDbType.SmallDateTime);


                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (int)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)dateCreatedParameter.Value;
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(ContentLookupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ContentLookupUpdateById))
                {
                    // id parameter
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.ContentLookupId,
                        SqlDbType.Int,
                        dto.Id);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.ContentLookupRowVersion,
                        SqlDbType.Timestamp, ParameterDirection.InputOutput,
                        dto.RowVersion);

                    // entity id
                    CreateParameter(command,
                        FieldNames.ContentLookupEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // user id
                    CreateParameter(command,
                        FieldNames.ContentLookupUserId,
                        SqlDbType.Int,
                        dto.UserId);

                    //planogram id
                    CreateParameter(command,
                        FieldNames.ContentLookupPlanogramId,
                        SqlDbType.Int,
                        dto.PlanogramId);

                    //product universe id
                    CreateParameter(command,
                        FieldNames.ContentLookupProductUniverseId,
                        SqlDbType.Int,
                        dto.ProductUniverseId);

                    //assortment id
                    CreateParameter(command,
                        FieldNames.ContentLookupAssortmentId,
                        SqlDbType.Int,
                        dto.AssortmentId);

                    //blocking id
                    CreateParameter(command,
                        FieldNames.ContentLookupBlockingId,
                        SqlDbType.Int,
                        dto.BlockingId);

                    //sequence id
                    CreateParameter(command,
                        FieldNames.ContentLookupSequenceId,
                        SqlDbType.Int,
                        dto.SequenceId);


                    //metric profile id
                    CreateParameter(command,
                        FieldNames.ContentLookupMetricProfileId,
                        SqlDbType.Int,
                        dto.MetricProfileId);

                    //Inventory profile id
                    CreateParameter(command,
                        FieldNames.ContentLookupInventoryProfileId,
                        SqlDbType.Int,
                        dto.InventoryProfileId);

                    //ConsumerDecisionTree id
                    CreateParameter(command,
                        FieldNames.ContentLookupConsumerDecisionTreeId,
                        SqlDbType.Int,
                        dto.ConsumerDecisionTreeId);

                    //Minor Assortment id
                    CreateParameter(command,
                        FieldNames.ContentLookupAssortmentMinorRevisionId,
                        SqlDbType.Int,
                        dto.AssortmentMinorRevisionId);

                    //performance Selection id
                    CreateParameter(command,
                        FieldNames.ContentLookupPerformanceSelectionId,
                        SqlDbType.Int,
                        dto.PerformanceSelectionId);

                    //cluster id
                    CreateParameter(command,
                        FieldNames.ContentLookupClusterId,
                        SqlDbType.Int,
                        dto.ClusterId);

                    //cluster scheme id
                    CreateParameter(command,
                        FieldNames.ContentLookupClusterSchemeId,
                        SqlDbType.Int,
                        dto.ClusterSchemeId);

                    //renumbering strategy id
                    CreateParameter(command,
                        FieldNames.ContentLookupRenumberingStrategyId,
                        SqlDbType.Int,
                        dto.RenumberingStrategyId);

                    //validation template id
                    CreateParameter(command,
                        FieldNames.ContentLookupValidationTemplateId,
                        SqlDbType.Int,
                        dto.ValidationTemplateId);

                    //planogram name template id
                    CreateParameter(command,
                        FieldNames.ContentLookupPlanogramNameTemplateId,
                        SqlDbType.Int,
                        dto.PlanogramNameTemplateId);

                    //print template id
                    CreateParameter(command,
                        FieldNames.ContentLookupPrintTemplateId,
                        SqlDbType.Int,
                        dto.PrintTemplateId);

                    //date created 
                    CreateParameter(command, FieldNames.ContentLookupDateCreated, SqlDbType.SmallDateTime, dto.DateCreated);

                    //date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.ContentLookupDateLastModified, SqlDbType.SmallDateTime);


                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Deletes dtos that match the specified entity id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 contentLookupId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ContentLookupDeleteById))
                {
                    this.CreateParameter(command, FieldNames.ContentLookupId, SqlDbType.Int, contentLookupId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateTempTable(String createSql)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, createSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the store temporary table on the given connection
        /// </summary>
        private void DropTempTable()
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion

    }
}
