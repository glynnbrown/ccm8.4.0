﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 J.Pickup
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Data;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Mssql.Schema;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    class HighlightCharacteristicRuleDal : DalBase, IHighlightCharacteristicRuleDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static HighlightCharacteristicRuleDto GetDataTransferObject(IDataRecord dr)
        {
            return new HighlightCharacteristicRuleDto
            {
                Id = (Int32)GetValue(dr[FieldNames.HighlightCharacteristicRuleId]),
                HighlightCharacteristicId = (Int32) GetValue(dr[FieldNames.HighlightCharacteristicRuleCharacteristicId]),
                Field = (String)GetValue(dr[FieldNames.HighlightCharacteristicRuleField]),
                Type = (Byte)GetValue(dr[FieldNames.HighlightCharacteristicRuleType]),
                Value = (String)GetValue(dr[FieldNames.HighlightCharacteristicRuleValue])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="highlightCharachteristicDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="highlightCharachteristicDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="highlightCharachteristicDto" /> with the data for the provided <paramref name="id" />.</returns>
        public IEnumerable<HighlightCharacteristicRuleDto> FetchByHighlightCharacteristicId(Int32 id)
        {
            List<HighlightCharacteristicRuleDto> dtoList = new List<HighlightCharacteristicRuleDto>();

            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicRuleFetchByHighlightCharacteristicId))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleCharacteristicId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="HighlightCharacteristicDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(HighlightCharacteristicRuleDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicRuleInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.HighlightCharacteristicRuleId, SqlDbType.Int);

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleCharacteristicId, SqlDbType.Int, dto.HighlightCharacteristicId);
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleField, SqlDbType.NVarChar, dto.Field);
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleType, SqlDbType.SmallInt, dto.Type);
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleValue, SqlDbType.NVarChar, dto.Value);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="HighlightCharacteristicDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(HighlightCharacteristicRuleDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicRuleUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleId, SqlDbType.Int, dto.Id);

                    //Other properties
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleCharacteristicId, SqlDbType.Int, dto.HighlightCharacteristicId);
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleField, SqlDbType.NVarChar, dto.Field);
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleType, SqlDbType.SmallInt, dto.Type);
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleValue, SqlDbType.NVarChar, dto.Value);


                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    // Nothing to update.
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.HighlightCharacteristicRuleDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.HighlightCharacteristicRuleId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
