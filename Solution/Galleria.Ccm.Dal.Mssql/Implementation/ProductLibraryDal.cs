﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.30)
// V8-32361 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;


namespace Galleria.Ccm.Dal.Mssql.Implementation
{

    /// <summary>
    /// Mssql implementation of IProductLibraryDal
    /// </summary>
    public sealed class ProductLibraryDal : DalBase, IProductLibraryDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static ProductLibraryDto GetDataTransferObject(IDataRecord dr)
        {
            return new ProductLibraryDto
            {
                Id = (Int32)GetValue(dr[FieldNames.ProductLibraryId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ProductLibraryRowVersion])),
                Name = (String)GetValue(dr[FieldNames.ProductLibraryName]),
                EntityId = (Int32)GetValue(dr[FieldNames.ProductLibraryEntityId]),
                FileType = (Byte)GetValue(dr[FieldNames.ProductLibraryFileType]),
                ExcelWorkbookSheet = (String)GetValue(dr[FieldNames.ProductLibraryExcelWorkbookSheet]),
                StartAtRow = (Int32)GetValue(dr[FieldNames.ProductLibraryStartAtRow]),
                IsFirstRowHeaders = (Boolean)GetValue(dr[FieldNames.ProductLibraryIsFirstRowHeaders]),
                TextDataFormat = (Byte?)GetValue(dr[FieldNames.ProductLibraryTextDataFormat]),
                TextDelimiterType = (Byte?)GetValue(dr[FieldNames.ProductLibraryTextDelimiterType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ProductLibraryDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ProductLibraryDateLastModified]),
            };
        }

        #endregion

        #region Fetch

        public ProductLibraryDto FetchById(Object id)
        {
            ProductLibraryDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLibraryId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new Galleria.Framework.Dal.DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        public void Insert(ProductLibraryDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.ProductLibraryId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ProductLibraryRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.ProductLibraryDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductLibraryDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);

                    //Other properties
                    CreateParameter(command, FieldNames.ProductLibraryName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductLibraryEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ProductLibraryFileType, SqlDbType.TinyInt, dto.FileType);
                    CreateParameter(command, FieldNames.ProductLibraryExcelWorkbookSheet, SqlDbType.NVarChar, dto.ExcelWorkbookSheet);
                    CreateParameter(command, FieldNames.ProductLibraryStartAtRow, SqlDbType.Int, dto.StartAtRow);
                    CreateParameter(command, FieldNames.ProductLibraryIsFirstRowHeaders, SqlDbType.Bit, dto.IsFirstRowHeaders);
                    CreateParameter(command, FieldNames.ProductLibraryTextDataFormat, SqlDbType.TinyInt, dto.TextDataFormat);
                    CreateParameter(command, FieldNames.ProductLibraryTextDelimiterType, SqlDbType.TinyInt, dto.TextDelimiterType);
                    

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(ProductLibraryDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLibraryId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ProductLibraryRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.ProductLibraryDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.ProductLibraryDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);


                    //Other properties
                    CreateParameter(command, FieldNames.ProductLibraryName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ProductLibraryEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ProductLibraryFileType, SqlDbType.TinyInt, dto.FileType);
                    CreateParameter(command, FieldNames.ProductLibraryExcelWorkbookSheet, SqlDbType.NVarChar, dto.ExcelWorkbookSheet);
                    CreateParameter(command, FieldNames.ProductLibraryStartAtRow, SqlDbType.Int, dto.StartAtRow);
                    CreateParameter(command, FieldNames.ProductLibraryIsFirstRowHeaders, SqlDbType.Bit, dto.IsFirstRowHeaders);
                    CreateParameter(command, FieldNames.ProductLibraryTextDataFormat, SqlDbType.TinyInt, dto.TextDataFormat);
                    CreateParameter(command, FieldNames.ProductLibraryTextDelimiterType, SqlDbType.TinyInt, dto.TextDelimiterType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ProductLibraryDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductLibraryId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Galleria.Framework.Dal.DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a ProductLibrary for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the highlight.</param>
        /// <returns><c>True</c> if the highlight was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void LockById(Object id)
        {
            // Not implemented as this dal does not use locking mechamisms.
        }

        /// <summary>
        ///     Unlocks an item after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the highlight.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }

        #endregion
    }
}
