﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentMinorRevisionListActionDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionListActionDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionListActionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionListActionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionId]),
                AssortmentMinorRevisionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionAssortmentMinorRevisionId]),
                ProductGtin = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionProductGtin]),
                ProductName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionProductName]),
                ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionProductId]),
                Priority = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionPriority]),
                Comments = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionComments]),
                AssortmentConsumerDecisionTreeNodeId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionAssortmentConsumerDecisionTreeNodeId])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionListActionDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionListActionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment id
        /// </summary>
        /// <param name="entityId">specified Assortment id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionListActionDto> FetchByAssortmentMinorRevisionId(Int32 AssortmentMinorRevisionContentId)
        {
            List<AssortmentMinorRevisionListActionDto> dtoList = new List<AssortmentMinorRevisionListActionDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionFetchByAssortmentMinorRevisionId))
                {
                    //Assortment Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionAssortmentMinorRevisionId, SqlDbType.Int, AssortmentMinorRevisionContentId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionListActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionComments, SqlDbType.NVarChar, dto.Comments);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionAssortmentConsumerDecisionTreeNodeId, SqlDbType.Int, dto.AssortmentConsumerDecisionTreeNodeId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionListActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionComments, SqlDbType.NVarChar, dto.Comments);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionAssortmentConsumerDecisionTreeNodeId, SqlDbType.Int, dto.AssortmentConsumerDecisionTreeNodeId);
                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
