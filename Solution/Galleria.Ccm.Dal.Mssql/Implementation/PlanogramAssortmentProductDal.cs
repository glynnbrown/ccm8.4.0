﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26799 : A.Kuszyk
//  Removed LocationCode property.
// V8-27058 : A.Probyn
//  Updated Gtin to be GTIN
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-30762 : I.George
//  Added MetaData property
// V8-30705 : A.Probyn
//  Added DelistedDueToAssortmentRule property
// V8-31479 : Added procedures to bulk insert/update assortment products
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramAssortmentProductDal : Framework.Dal.Mssql.DalBase, IPlanogramAssortmentProductDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogramAssortmentProduct";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
            "[PlanogramAssortmentProduct_Id] [INT] NOT NULL," +
            "[PlanogramAssortment_Id] [INT] NOT NULL, " +
            "[PlanogramAssortmentProduct_Gtin] [NVARCHAR](14) COLLATE database_default NOT NULL," +
            "[PlanogramAssortmentProduct_Name] [NVARCHAR](100) COLLATE database_default NULL, " +
            "[PlanogramAssortmentProduct_IsRanged] [BIT] NOT NULL," +
            "[PlanogramAssortmentProduct_Rank] [SMALLINT] NOT NULL," +
            "[PlanogramAssortmentProduct_Segmentation] [NVARCHAR](300) COLLATE database_default NULL," +
            "[PlanogramAssortmentProduct_Facings] [TINYINT] NOT NULL," +
            "[PlanogramAssortmentProduct_Units] [SMALLINT] NOT NULL," +
            "[PlanogramAssortmentProduct_ProductTreatmentType] [TINYINT] NOT NULL, " +
            "[PlanogramAssortmentProduct_ProductLocalizationType] [TINYINT] NOT NULL," +
            "[PlanogramAssortmentProduct_Comments] [NVARCHAR](100) COLLATE database_default NULL," +
            "[PlanogramAssortmentProduct_ExactListFacings] [TINYINT] NULL," +
            "[PlanogramAssortmentProduct_ExactListUnits] [SMALLINT] NULL," +
            "[PlanogramAssortmentProduct_PreserveListFacings] [TINYINT] NULL," +
            "[PlanogramAssortmentProduct_PreserveListUnits] [SMALLINT] NULL," +
            "[PlanogramAssortmentProduct_MaxListFacings] [TINYINT] NULL," +
            "[PlanogramAssortmentProduct_MaxListUnits] [SMALLINT] NULL," +
            "[PlanogramAssortmentProduct_MinListFacings] [TINYINT] NULL," +
            "[PlanogramAssortmentProduct_MinListUnits] [SMALLINT] NULL," +
            "[PlanogramAssortmentProduct_FamilyRuleName] [NVARCHAR](100) COLLATE database_default NULL," +
            "[PlanogramAssortmentProduct_FamilyRuleType] [TINYINT] NOT NULL," +
            "[PlanogramAssortmentProduct_FamilyRuleValue] [TINYINT] NULL," +
            "[PlanogramAssortmentProduct_IsPrimaryRegionalProduct] [BIT] NOT NULL," +
            "[PlanogramAssortmentProduct_MetaIsProductPlacedOnPlanogram] [BIT] NULL," +
            "[PlanogramAssortmentProduct_DelistedDueToAssortmentRule] [BIT] NOT NULL)";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramAssortmentProductBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<PlanogramAssortmentProductDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramAssortmentProductBulkCopyDataReader(IEnumerable<PlanogramAssortmentProductDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 26; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.PlanogramAssortmentId;
                        break;
                    case 2:
                        value = _enumerator.Current.Gtin;
                        break;
                    case 3:
                        value = _enumerator.Current.Name;
                        break;
                    case 4:
                        value = _enumerator.Current.IsRanged;
                        break;
                    case 5:
                        value = _enumerator.Current.Rank;
                        break;
                    case 6:
                        value = _enumerator.Current.Segmentation;
                        break;
                    case 7:
                        value = _enumerator.Current.Facings;
                        break;
                    case 8:
                        value = _enumerator.Current.Units;
                        break;
                    case 9:
                        value = _enumerator.Current.ProductTreatmentType;
                        break;
                    case 10:
                        value = _enumerator.Current.ProductLocalizationType;
                        break;
                    case 11:
                        value = _enumerator.Current.Comments;
                        break;
                    case 12:
                        value = _enumerator.Current.ExactListFacings;
                        break;
                    case 13:
                        value = _enumerator.Current.ExactListUnits;
                        break;
                    case 14:
                        value = _enumerator.Current.PreserveListFacings;
                        break;
                    case 15:
                        value = _enumerator.Current.PreserveListUnits;
                        break;
                    case 16:
                        value = _enumerator.Current.MaxListFacings;
                        break;
                    case 17:
                        value = _enumerator.Current.MaxListUnits;
                        break;
                    case 18:
                        value = _enumerator.Current.MinListFacings;
                        break;
                    case 19:
                        value = _enumerator.Current.MinListUnits;
                        break;
                    case 20:
                        value = _enumerator.Current.FamilyRuleName;
                        break;
                    case 21:
                        value = _enumerator.Current.FamilyRuleType;
                        break;
                    case 22:
                        value = _enumerator.Current.FamilyRuleValue;
                        break;
                    case 23:
                        value = _enumerator.Current.IsPrimaryRegionalProduct;
                        break;
                    case 24:
                        value = _enumerator.Current.MetaIsProductPlacedOnPlanogram;
                        break;
                    case 25:
                        value = _enumerator.Current.DelistedDueToAssortmentRule;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssortmentProduct DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssortmentProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssortmentProductDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentProductId]),
                PlanogramAssortmentId = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentProductPlanogramAssortmentId]),
                Gtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductGtin]),
                Name = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductName]),
                IsRanged = (Boolean)GetValue(dr[FieldNames.PlanogramAssortmentProductIsRanged]),
                Rank = (Int16)GetValue(dr[FieldNames.PlanogramAssortmentProductRank]),
                Segmentation = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductSegmentation]),
                Facings = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentProductFacings]),
                Units = (Int16)GetValue(dr[FieldNames.PlanogramAssortmentProductUnits]),
                ProductTreatmentType = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentProductProductTreatmentType]),
                ProductLocalizationType = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentProductProductLocalizationType]),
                Comments = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductComments]),
                ExactListFacings = (Byte?)GetValue(dr[FieldNames.PlanogramAssortmentProductExactListFacings]),
                ExactListUnits = (Int16?)GetValue(dr[FieldNames.PlanogramAssortmentProductExactListUnits]),
                PreserveListFacings = (Byte?)GetValue(dr[FieldNames.PlanogramAssortmentProductPreserveListFacings]),
                PreserveListUnits = (Int16?)GetValue(dr[FieldNames.PlanogramAssortmentProductPreserveListUnits]),
                MaxListFacings = (Byte?)GetValue(dr[FieldNames.PlanogramAssortmentProductMaxListFacings]),
                MaxListUnits = (Int16?)GetValue(dr[FieldNames.PlanogramAssortmentProductMaxListUnits]),
                MinListFacings = (Byte?)GetValue(dr[FieldNames.PlanogramAssortmentProductMinListFacings]),
                MinListUnits = (Int16?)GetValue(dr[FieldNames.PlanogramAssortmentProductMinListUnits]),
                FamilyRuleName = (String)GetValue(dr[FieldNames.PlanogramAssortmentProductFamilyRuleName]),
                FamilyRuleType = (Byte)GetValue(dr[FieldNames.PlanogramAssortmentProductFamilyRuleType]),
                FamilyRuleValue = (Byte?)GetValue(dr[FieldNames.PlanogramAssortmentProductFamilyRuleValue]),
                IsPrimaryRegionalProduct = (Boolean)GetValue(dr[FieldNames.PlanogramAssortmentProductIsPrimaryRegionalProduct]),
                DelistedDueToAssortmentRule = (Boolean)GetValue(dr[FieldNames.PlanogramAssortmentProductDelistedDueToAssortmentRule]),
                MetaIsProductPlacedOnPlanogram = (Boolean?)GetValue(dr[FieldNames.PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssortmentProduct DTOs with a matching Planogram Assortment Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramAssortmentProduct DTOs.</returns>
        public IEnumerable<PlanogramAssortmentProductDto> FetchByPlanogramAssortmentId(object id)
        {
            var dtoList = new List<PlanogramAssortmentProductDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductFetchByPlanogramAssortmentId))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductPlanogramAssortmentId, SqlDbType.Int, (Int32)id);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssortmentProductDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssortmentProductId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductGtin, SqlDbType.NVarChar, dto.Gtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductIsRanged, SqlDbType.Bit, dto.IsRanged);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductRank, SqlDbType.SmallInt, dto.Rank);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductSegmentation, SqlDbType.NVarChar, dto.Segmentation);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFacings, SqlDbType.TinyInt, dto.Facings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductUnits, SqlDbType.SmallInt, dto.Units);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductProductTreatmentType, SqlDbType.TinyInt, dto.ProductTreatmentType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductProductLocalizationType, SqlDbType.TinyInt, dto.ProductLocalizationType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductComments, SqlDbType.NVarChar, dto.Comments);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductExactListFacings, SqlDbType.TinyInt, dto.ExactListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductExactListUnits, SqlDbType.SmallInt, dto.ExactListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductPreserveListFacings, SqlDbType.TinyInt, dto.PreserveListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductPreserveListUnits, SqlDbType.SmallInt, dto.PreserveListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMaxListFacings, SqlDbType.TinyInt, dto.MaxListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMaxListUnits, SqlDbType.SmallInt, dto.MaxListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMinListFacings, SqlDbType.TinyInt, dto.MinListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMinListUnits, SqlDbType.SmallInt, dto.MinListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFamilyRuleName, SqlDbType.NVarChar, dto.FamilyRuleName);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFamilyRuleType, SqlDbType.TinyInt, dto.FamilyRuleType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFamilyRuleValue, SqlDbType.TinyInt, dto.FamilyRuleValue);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductIsPrimaryRegionalProduct, SqlDbType.Bit, dto.IsPrimaryRegionalProduct);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram, SqlDbType.Bit, dto.MetaIsProductPlacedOnPlanogram);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductDelistedDueToAssortmentRule, SqlDbType.Bit, dto.DelistedDueToAssortmentRule);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, PlanogramAssortmentProductDto> index = new Dictionary<Object, PlanogramAssortmentProductDto>();
            foreach (PlanogramAssortmentProductDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramAssortmentProductBulkCopyDataReader dr = new PlanogramAssortmentProductBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramAssortmentProductBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssortmentProductDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductGtin, SqlDbType.NVarChar, dto.Gtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductIsRanged, SqlDbType.Bit, dto.IsRanged);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductRank, SqlDbType.SmallInt, dto.Rank);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductSegmentation, SqlDbType.NVarChar, dto.Segmentation);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFacings, SqlDbType.TinyInt, dto.Facings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductUnits, SqlDbType.SmallInt, dto.Units);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductProductTreatmentType, SqlDbType.TinyInt, dto.ProductTreatmentType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductProductLocalizationType, SqlDbType.TinyInt, dto.ProductLocalizationType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductComments, SqlDbType.NVarChar, dto.Comments);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductExactListFacings, SqlDbType.TinyInt, dto.ExactListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductExactListUnits, SqlDbType.SmallInt, dto.ExactListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductPreserveListFacings, SqlDbType.TinyInt, dto.PreserveListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductPreserveListUnits, SqlDbType.SmallInt, dto.PreserveListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMaxListFacings, SqlDbType.TinyInt, dto.MaxListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMaxListUnits, SqlDbType.SmallInt, dto.MaxListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMinListFacings, SqlDbType.TinyInt, dto.MinListFacings);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMinListUnits, SqlDbType.SmallInt, dto.MinListUnits);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFamilyRuleName, SqlDbType.NVarChar, dto.FamilyRuleName);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFamilyRuleType, SqlDbType.TinyInt, dto.FamilyRuleType);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductFamilyRuleValue, SqlDbType.TinyInt, dto.FamilyRuleValue);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductIsPrimaryRegionalProduct, SqlDbType.Bit, dto.IsPrimaryRegionalProduct);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram, SqlDbType.Bit, dto.MetaIsProductPlacedOnPlanogram);
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductDelistedDueToAssortmentRule, SqlDbType.Bit, dto.DelistedDueToAssortmentRule);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramAssortmentProductBulkCopyDataReader dr = new PlanogramAssortmentProductBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramAssortmentProductBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentProductDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentProductId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            foreach (PlanogramAssortmentProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}


