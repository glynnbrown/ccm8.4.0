﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// BlockingInfoDal Dal
    /// </summary>
    public class BlockingInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IBlockingInfoDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static BlockingInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new BlockingInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.BlockingId]),
                Name = (String)GetValue(dr[FieldNames.BlockingName]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<BlockingInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<BlockingInfoDto> dtoList = new List<BlockingInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.BlockingInfoFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.BlockingEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

 
    }
}