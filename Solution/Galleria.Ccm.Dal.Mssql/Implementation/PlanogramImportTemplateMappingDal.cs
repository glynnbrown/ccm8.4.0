﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27939 : L.Luong
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramImportTemplateMapping Dal
    /// </summary>
    public class PlanogramImportTemplateMappingDal : DalBase, IPlanogramImportTemplateMappingDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramImportTemplateMappingDto GetDataTransferObject(IDataRecord dr)
        {
            return new PlanogramImportTemplateMappingDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplateMappingId]),
                PlanogramImportTemplateId = (Int32)GetValue(dr[FieldNames.PlanogramImportTemplateMappingPlanogramImportTemplateId]),
                FieldType = (Byte)GetValue(dr[FieldNames.PlanogramImportTemplateMappingFieldType]),
                Field = (String)GetValue(dr[FieldNames.PlanogramImportTemplateMappingField]),
                ExternalField = (String)GetValue(dr[FieldNames.PlanogramImportTemplateMappingExternalField]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramImportTemplateMappingDto"/> items corresponding to the given <paramref name="planogramImportTemplateId"/>.
        /// </summary>
        /// <param name="planogramImportTemplateId">The unique id value for the Planogram Import Template containing the groups to be retrieved.</param>
        /// <returns>An instance implementing <see cref="IEnumerable{PlanogramImportTemplateMappingDto}"/> with all the matching groups.</returns>
        public IEnumerable<PlanogramImportTemplateMappingDto> FetchByPlanogramImportTemplateId(Object planogramImportTemplateId)
        {
            var dtoList = new List<PlanogramImportTemplateMappingDto>();
            try
            {
                using (
                    var command = CreateCommand(ProcedureNames.PlanogramImportTemplateMappingFetchByPlanogramImportTemplateId)
                    )
                {
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingPlanogramImportTemplateId, SqlDbType.Int,
                        (Int32) planogramImportTemplateId);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the data contained in the given <paramref name="dto"/> into the dal.
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramImportTemplateMappingDto"/> containing the data to be persisted.</param>
        public void Insert(PlanogramImportTemplateMappingDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateMappingInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramImportTemplateMappingId,
                        SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingPlanogramImportTemplateId, SqlDbType.Int,
                        dto.PlanogramImportTemplateId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingFieldType, SqlDbType.TinyInt, dto.FieldType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingExternalField, SqlDbType.NVarChar,
                        dto.ExternalField);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the data contained in the given <paramref name="dto"/> in the dal
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramImportTemplateMappingDto"/> containing the data to be persisted.</param>
        public void Update(PlanogramImportTemplateMappingDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateMappingUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingPlanogramImportTemplateId, SqlDbType.Int,
                        dto.PlanogramImportTemplateId);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingFieldType, SqlDbType.NVarChar, dto.FieldType);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingExternalField, SqlDbType.NVarChar,
                        dto.ExternalField);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the Planogram Import Template Mapping matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Unique id value to determine with Planogram Import Template Mapping to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramImportTemplateMappingDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramImportTemplateMappingId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}
