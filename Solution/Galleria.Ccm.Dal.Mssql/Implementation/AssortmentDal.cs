﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// GFS-25455 : J.Pickup
//  Added : DeleteByEntityId
// V8-26764 : N.Foster
//  Added FetchByName
// V8-27241 : A.Kuszyk
//  Changed FetchByName to FetchByEntityIdName.
// V8-255556 : J.Pickup
//      Added FetchByUniqueContentReference
#endregion
#region Version History: CCM803
// V8-29134 : D.Pleasance
//  Removed date deleted column, delete is now outright.
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion 
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private AssortmentDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentDto
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.AssortmentRowVersion])),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.AssortmentUniqueContentReference]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.AssortmentProductGroupId]),
                EntityId = (Int32)GetValue(dr[FieldNames.AssortmentEntityId]),
                Name = (String)GetValue(dr[FieldNames.AssortmentName]),
                ConsumerDecisionTreeId = (Int32?)GetValue(dr[FieldNames.AssortmentConsumerDecisionTreeId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.AssortmentDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.AssortmentDateLastModified]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.AssortmentParentUniqueContentReference])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns specified dto from the database
        /// </summary>
        public AssortmentDto FetchById(Int32 id)
        {
            AssortmentDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFetchById))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.AssortmentId,
                        SqlDbType.Int,
                        id);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns specified dto from the database
        /// </summary>
        public IEnumerable<AssortmentDto> FetchAll()
        {
            List<AssortmentDto> dtos = new List<AssortmentDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFetchAll))
                {
                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtos;
        }

        /// <summary>
        /// Returns the specified dto from the database
        /// based on the name
        /// </summary>
        public AssortmentDto FetchByEntityIdName(Int32 entityId, String name)
        {
            AssortmentDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFetchByEntityIdName))
                {
                    // parameters
                    CreateParameter(command, FieldNames.AssortmentName, SqlDbType.NVarChar, name);
                    CreateParameter(command, FieldNames.AssortmentEntityId, SqlDbType.Int, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns specified dto from the database
        /// </summary>
        public AssortmentDto FetchByUniqueContentReference(Guid ucr)
        {
            AssortmentDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.Assortment_FetchByUniqueContentReference))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.AssortmentUniqueContentReference,
                        SqlDbType.UniqueIdentifier,
                        ucr);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
       
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(AssortmentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentInsert))
                {
                    // id parameter
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.AssortmentId,
                        SqlDbType.Int);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.AssortmentRowVersion,
                        SqlDbType.Timestamp);

                    // unique content reference
                    CreateParameter(command,
                        FieldNames.AssortmentUniqueContentReference,
                        SqlDbType.UniqueIdentifier,
                        dto.UniqueContentReference);

                    //product group id parameter
                    CreateParameter(command,
                       FieldNames.AssortmentProductGroupId,
                       SqlDbType.Int,
                       dto.ProductGroupId);

                    // entity id
                    CreateParameter(command,
                        FieldNames.AssortmentEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // name
                    CreateParameter(command,
                        FieldNames.AssortmentName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // ConsumerDecisionTreeId
                    CreateParameter(command,
                        FieldNames.AssortmentConsumerDecisionTreeId,
                        SqlDbType.Int,
                        dto.ConsumerDecisionTreeId);

                    CreateParameter(command, FieldNames.AssortmentParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    //date created 
                    SqlParameter dateCreatedParameter =
                    CreateParameter(command, FieldNames.AssortmentDateCreated, SqlDbType.SmallDateTime);

                    //date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.AssortmentDateLastModified, SqlDbType.SmallDateTime);
                    
                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (int)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)dateCreatedParameter.Value;
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentUpdateById))
                {
                    // id parameter
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.AssortmentId,
                        SqlDbType.Int,
                        dto.Id);

                    // row version parameter
                    SqlParameter rowVersionParameter = CreateParameter(command,
                        FieldNames.AssortmentRowVersion,
                        SqlDbType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);

                    //product group id parameter
                    CreateParameter(command,
                       FieldNames.AssortmentProductGroupId,
                       SqlDbType.Int,
                       dto.ProductGroupId);

                    // entity id
                    CreateParameter(command,
                        FieldNames.AssortmentEntityId,
                        SqlDbType.Int,
                        dto.EntityId);

                    // name
                    CreateParameter(command,
                        FieldNames.AssortmentName,
                        SqlDbType.NVarChar,
                        dto.Name);

                    // ConsumerDecisionTreeId
                    CreateParameter(command,
                        FieldNames.AssortmentConsumerDecisionTreeId,
                        SqlDbType.Int,
                        dto.ConsumerDecisionTreeId);

                    // UCR
                    CreateParameter(command,
                        FieldNames.AssortmentUniqueContentReference,
                        SqlDbType.UniqueIdentifier,
                        dto.UniqueContentReference);

                    //date last modified
                    SqlParameter dateLastModifiedParameter =
                    CreateParameter(command, FieldNames.AssortmentDateLastModified, SqlDbType.SmallDateTime);

                    //date created
                    SqlParameter dateCreatedParameter =
                    CreateParameter(command, FieldNames.AssortmentDateCreated, SqlDbType.SmallDateTime, ParameterDirection.InputOutput, dto.DateCreated);

                    CreateParameter(command, FieldNames.AssortmentParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto id</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentDeleteById))
                {
                    // id parameter
                    CreateParameter(command,
                        FieldNames.AssortmentId,
                        SqlDbType.Int,
                        id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }


        /// <summary>
        /// Deletes dtos that match the specified entity id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentDeleteByEntityId))
                {
                    this.CreateParameter(command, FieldNames.ProductEntityId, SqlDbType.Int, entityId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        

        #endregion
    }
}