﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27647 : L.Luong 
//   Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History : CCM 820
// CCM-30773 : J.Pickup
//		Added InventoryMetricType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramInventory Dal
    /// </summary>
    public class PlanogramInventoryDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramInventoryDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramInventoryDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramInventoryDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramInventoryId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramInventoryPlanogramId]),
                DaysOfPerformance = (Single)GetValue(dr[FieldNames.PlanogramInventoryDaysOfPerformance]),
                MinCasePacks = (Single)GetValue(dr[FieldNames.PlanogramInventoryMinCasePacks]),
                MinDos = (Single)GetValue(dr[FieldNames.PlanogramInventoryMinDos]),
                MinShelfLife = (Single)GetValue(dr[FieldNames.PlanogramInventoryMinShelfLife]),
                MinDeliveries = (Single)GetValue(dr[FieldNames.PlanogramInventoryMinDeliveries]),
                IsCasePacksValidated = (Boolean)GetValue(dr[FieldNames.PlanogramInventoryIsCasePacksValidated]),
                IsDosValidated = (Boolean)GetValue(dr[FieldNames.PlanogramInventoryIsDosValidated]),
                IsShelfLifeValidated = (Boolean)GetValue(dr[FieldNames.PlanogramInventoryIsShelfLifeValidated]),
                IsDeliveriesValidated = (Boolean)GetValue(dr[FieldNames.PlanogramInventoryIsDeliveriesValidated]),
                InventoryMetricType = (Byte)GetValue(dr[FieldNames.PlanogramInventoryMetricType])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramInventory DTOs with a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramInventory DTOs.</returns>
        public PlanogramInventoryDto FetchByPlanogramId(object planogramId)
        {
            PlanogramInventoryDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInventoryFetchByPlanogramId))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramInventoryPlanogramId, SqlDbType.Int, (Int32)planogramId);

                    // Excute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramInventoryDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInventoryInsert))
                {
                    // ID
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramInventoryId, SqlDbType.Int);

                    // Other Parameters
                    CreateParameter(command, FieldNames.PlanogramInventoryPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramInventoryDaysOfPerformance, SqlDbType.Real, dto.DaysOfPerformance);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinCasePacks, SqlDbType.Real, dto.MinCasePacks);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinDos, SqlDbType.Real, dto.MinDos);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinShelfLife, SqlDbType.Real, dto.MinShelfLife);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinDeliveries, SqlDbType.Real, dto.MinDeliveries);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsCasePacksValidated, SqlDbType.Bit, dto.IsCasePacksValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsDosValidated, SqlDbType.Bit, dto.IsDosValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsShelfLifeValidated, SqlDbType.Bit, dto.IsShelfLifeValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsDeliveriesValidated, SqlDbType.Bit, dto.IsDeliveriesValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryMetricType, SqlDbType.TinyInt, dto.InventoryMetricType);

                    // Excute
                    command.ExecuteNonQuery();

                    // Update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramInventoryDto> dtos)
        {
            foreach (PlanogramInventoryDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramInventoryDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInventoryUpdateById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramInventoryId, SqlDbType.Int, dto.Id);

                    // Other parameter
                    CreateParameter(command, FieldNames.PlanogramInventoryPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramInventoryDaysOfPerformance, SqlDbType.Real, dto.DaysOfPerformance);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinCasePacks, SqlDbType.Real, dto.MinCasePacks);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinDos, SqlDbType.Real, dto.MinDos);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinShelfLife, SqlDbType.Real, dto.MinShelfLife);
                    CreateParameter(command, FieldNames.PlanogramInventoryMinDeliveries, SqlDbType.Real, dto.MinDeliveries);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsCasePacksValidated, SqlDbType.Bit, dto.IsCasePacksValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsDosValidated, SqlDbType.Bit, dto.IsDosValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsShelfLifeValidated, SqlDbType.Bit, dto.IsShelfLifeValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryIsDeliveriesValidated, SqlDbType.Bit, dto.IsDeliveriesValidated);
                    CreateParameter(command, FieldNames.PlanogramInventoryMetricType, SqlDbType.TinyInt, dto.InventoryMetricType);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramInventoryDto> dtos)
        {
            foreach (PlanogramInventoryDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramInventoryDeleteById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramInventoryId, SqlDbType.Int, (Int32)id);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramInventoryDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramInventoryDto> dtos)
        {
            foreach (PlanogramInventoryDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion
    }
}
