﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MSSQL implementation of IPrintTemplateSectionDal
    /// </summary>
    public sealed class PrintTemplateSectionDal : Galleria.Framework.Dal.Mssql.DalBase, IPrintTemplateSectionDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PrintTemplateSectionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PrintTemplateSectionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PrintTemplateSectionId]),
                PrintTemplateSectionGroupId = (Int32)GetValue(dr[FieldNames.PrintTemplateSectionPrintTemplateSectionGroupId]),
                Number = (Byte)GetValue(dr[FieldNames.PrintTemplateSectionNumber]),
                Orientation = (Byte)GetValue(dr[FieldNames.PrintTemplateSectionOrientation]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PrintTemplateSectionDto FetchById(Int32 id)
        {
            PrintTemplateSectionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateSectionId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }



        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PrintTemplateSectionDto> FetchByPrintTemplateSectionGroupId(Int32 printTemplateSectionGroupId)
        {
            List<PrintTemplateSectionDto> dtoList = new List<PrintTemplateSectionDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionFetchByPrintTemplateSectionGroupId))
                {
                    CreateParameter(command, FieldNames.PrintTemplateSectionPrintTemplateSectionGroupId, SqlDbType.Int, printTemplateSectionGroupId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PrintTemplateSectionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PrintTemplateSectionId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateSectionPrintTemplateSectionGroupId, SqlDbType.Int, dto.PrintTemplateSectionGroupId);
                    CreateParameter(command, FieldNames.PrintTemplateSectionNumber, SqlDbType.TinyInt, dto.Number);
                    CreateParameter(command, FieldNames.PrintTemplateSectionOrientation, SqlDbType.TinyInt, dto.Orientation);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PrintTemplateSectionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateSectionId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateSectionPrintTemplateSectionGroupId, SqlDbType.Int, dto.PrintTemplateSectionGroupId);
                    CreateParameter(command, FieldNames.PrintTemplateSectionNumber, SqlDbType.TinyInt, dto.Number);
                    CreateParameter(command, FieldNames.PrintTemplateSectionOrientation, SqlDbType.TinyInt, dto.Orientation);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateSectionId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}