﻿using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public sealed class WorkpackagePlanogramProcessingStatusDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackagePlanogramProcessingStatusDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        public WorkpackagePlanogramProcessingStatusDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackagePlanogramProcessingStatusDto()
            {
                PlanogramId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusPlanogramId]),
                WorkpackageId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusWorkpackageId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusRowVersion])),
                AutomationStatus = (Byte)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusAutomationStatus]),
                AutomationStatusDescription = (String)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusAutomationStatusDescription]),
                AutomationProgressMax = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusAutomationProgressMax]),
                AutomationProgressCurrent = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusAutomationProgressCurrent]),
                AutomationDateStarted = (DateTime?)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusAutomationDateStarted]),
                AutomationDateLastUpdated = (DateTime?)GetValue(dr[FieldNames.WorkpackagePlanogramProcessingStatusAutomationDateUpdated]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        public IEnumerable<WorkpackagePlanogramProcessingStatusDto> FetchByPlanogramId(int planogramId)
        {
            List<WorkpackagePlanogramProcessingStatusDto> dtoList = new List<WorkpackagePlanogramProcessingStatusDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramProcessingStatusFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusPlanogramId, SqlDbType.Int, planogramId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        public WorkpackagePlanogramProcessingStatusDto FetchByPlanogramIdWorkpackageId(int planogramId, int workpackageId)
        {
            WorkpackagePlanogramProcessingStatusDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramProcessingStatusFetchByPlanogramIdWorkpackageId))
                {
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusPlanogramId, SqlDbType.Int, planogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusWorkpackageId, SqlDbType.Int, workpackageId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        public void Update(WorkpackagePlanogramProcessingStatusDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramProcessingStatusUpdateByPlanogramIdWorkpackageId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusPlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusWorkpackageId, SqlDbType.Int, dto.WorkpackageId);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationStatus, SqlDbType.TinyInt, dto.AutomationStatus);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationStatusDescription, SqlDbType.NVarChar, dto.AutomationStatusDescription);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationProgressMax, SqlDbType.Int, dto.AutomationProgressMax);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationProgressCurrent, SqlDbType.Int, dto.AutomationProgressCurrent);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationDateStarted, SqlDbType.DateTime, dto.AutomationDateStarted);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationDateUpdated, SqlDbType.DateTime, dto.AutomationDateLastUpdated);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region AutomationStatusIncrement
        /// <summary>
        /// Increments the automation progress of a workpackage planogram
        /// </summary>
        public void AutomationStatusIncrement(Int32 planogramId, Int32 workpackageId,String statusDescription, DateTime dateLastUpdated)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramProcessingStatusIncrementAutomationStatusByPlanogramIdWorkpackageId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusPlanogramId, SqlDbType.Int, planogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationStatusDescription, SqlDbType.NVarChar, statusDescription);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramProcessingStatusAutomationDateUpdated, SqlDbType.DateTime, dateLastUpdated);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
