﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25886 : L.Ineson
//  Created
// V8-27426 : L.Ineson
//  Added 2 more values
// V8-28232 : N.Foster
//  Added support for batch operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
     /// <summary>
    /// Mssql implementation of IWorkpackagePlanogramParameterValueDal
    /// </summary>
    public sealed class WorkpackagePlanogramParameterValueDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackagePlanogramParameterValueDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        public WorkpackagePlanogramParameterValueDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackagePlanogramParameterValueDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramParameterValueId]),
                WorkpackagePlanogramParameterId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramParameterValueWorkpackagePlanogramParameterId]),
                Value1 = GetValue(dr[FieldNames.WorkpackagePlanogramParameterValueValue1]),
                Value2 = GetValue(dr[FieldNames.WorkpackagePlanogramParameterValueValue2]),
                Value3 = GetValue(dr[FieldNames.WorkpackagePlanogramParameterValueValue3]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Fetches dtos for the given workflow task parameter id
        /// </summary>
        public IEnumerable<WorkpackagePlanogramParameterValueDto> FetchByWorkpackagePlanogramParameterId(Int32 workflowTaskParameterId)
        {
            List<WorkpackagePlanogramParameterValueDto> dtoList = new List<WorkpackagePlanogramParameterValueDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterValueFetchByWorkpackagePlanogramParameterId))
                {
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueWorkpackagePlanogramParameterId, SqlDbType.Int, workflowTaskParameterId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(WorkpackagePlanogramParameterValueDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterValueInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueWorkpackagePlanogramParameterId, SqlDbType.Int, dto.WorkpackagePlanogramParameterId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueValue1, SqlDbType.Variant, dto.Value1);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueValue2, SqlDbType.Variant, dto.Value2);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueValue3, SqlDbType.Variant, dto.Value3);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts a batch of dtos into the database
        /// </summary>
        /// <param name="dtos">The list of dtos to insert</param>
        public void Insert(IEnumerable<WorkpackagePlanogramParameterValueDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk insert for performance
            foreach (WorkpackagePlanogramParameterValueDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given dto
        /// </summary>
        public void Update(WorkpackagePlanogramParameterValueDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterValueUpdateById))
                {
                    // parameter
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueWorkpackagePlanogramParameterId, SqlDbType.Int, dto.WorkpackagePlanogramParameterId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueValue1, SqlDbType.Variant, dto.Value1);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueValue2, SqlDbType.Variant, dto.Value2);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueValue3, SqlDbType.Variant, dto.Value3);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates a batch of dtos in the database
        /// </summary>
        /// <param name="dtos">The list of dtos to update</param>
        public void Update(IEnumerable<WorkpackagePlanogramParameterValueDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk update for performance
            foreach (WorkpackagePlanogramParameterValueDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the dto with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterValueDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterValueId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item from the database
        /// </summary>
        public void Delete(WorkpackagePlanogramParameterValueDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes a batch of dtos from the database
        /// </summary>
        /// <param name="dtos">The list of dtos to delete</param>
        public void Delete(IEnumerable<WorkpackagePlanogramParameterValueDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk delete for performance
            foreach (WorkpackagePlanogramParameterValueDto dto in dtos)
            {
                this.DeleteById(dto.Id);
            }
        }
        #endregion
    }
}
