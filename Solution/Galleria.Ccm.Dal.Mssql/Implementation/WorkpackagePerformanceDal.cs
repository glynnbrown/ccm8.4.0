﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkpackagePerformanceDal
    /// </summary>
    public class WorkpackagePerformanceDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackagePerformanceDal
    {
        #region Constants
        private const Int32 _deleteTimeout = 3600;
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the database
        /// </summary>
        public void Insert(WorkpackagePerformanceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePerformanceInsert))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePerformanceWorkpackageId, SqlDbType.Int, dto.WorkpackageId);
                    SqlParameter keyParameter = CreateParameter(command, FieldNames.WorkpackagePerformanceKey, SqlDbType.Int);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Key = (Int32)keyParameter.Value;
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified item from the database
        /// </summary>
        public void DeleteByWorkpackageId(Int32 workpackageId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePerformanceDeleteByWorkpackageId))
                {
                    command.CommandTimeout = _deleteTimeout;

                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePerformanceWorkpackageId, SqlDbType.Int, workpackageId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion
    }
}
