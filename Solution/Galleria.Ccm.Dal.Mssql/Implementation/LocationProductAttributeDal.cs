﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed stored procedures that are not used
#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class LocationProductAttributeDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationProductAttributeDal
    {
        #region Constants
        private const String _dropTempTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _tempTableName = "#tmpLocationProductAttribute";
        private const String _fetchTempTableSql =
            "CREATE TABLE [{0}](" +
            "[Location_Id] [SMALLINT] NOT NULL, " +
            "[Product_Id] [INT] NOT NULL " +
            ")";
        private const String _upsertTempTableSql =
            "CREATE TABLE [{0}](" +
            "[Product_Id] [INT] NOT NULL, " +
            "[Location_Id] [SMALLINT] NOT NULL, " +
            "[Entity_Id] [INT] NOT NULL," +
            "[LocationProductAttribute_GTIN] [NVARCHAR](14) COLLATE database_default NULL," +
            "[LocationProductAttribute_ProductDescription] [NVARCHAR](50) COLLATE database_default NULL, " +
            "[LocationProductAttribute_Height] [REAL] NULL, " +
            "[LocationProductAttribute_Width] [REAL] NULL, " +
            "[LocationProductAttribute_Depth] [REAL] NULL, " +
            "[LocationProductAttribute_CasePackUnits] [SMALLINT] NULL, " +
            "[LocationProductAttribute_CaseHigh] [TINYINT] NULL, " +
            "[LocationProductAttribute_CaseWide] [TINYINT] NULL, " +
            "[LocationProductAttribute_CaseDeep] [TINYINT] NULL, " +
            "[LocationProductAttribute_CaseHeight] [REAL] NULL, " +
            "[LocationProductAttribute_CaseWidth] [REAL] NULL, " +
            "[LocationProductAttribute_CaseDepth] [REAL] NULL, " +
            "[LocationProductAttribute_DaysOfSupply] [TINYINT] NULL, " +
            "[LocationProductAttribute_MinDeep] [TINYINT] NULL, " +
            "[LocationProductAttribute_MaxDeep] [TINYINT] NULL, " +
            "[LocationProductAttribute_TrayPackUnits] [SMALLINT] NULL, " +
            "[LocationProductAttribute_TrayHigh] [TINYINT] NULL, " +
            "[LocationProductAttribute_TrayWide] [TINYINT] NULL, " +
            "[LocationProductAttribute_TrayDeep] [TINYINT] NULL, " +
            "[LocationProductAttribute_TrayHeight] [REAL] NULL, " +
            "[LocationProductAttribute_TrayWidth] [REAL] NULL, " +
            "[LocationProductAttribute_TrayDepth] [REAL] NULL, " +
            "[LocationProductAttribute_TrayThickHeight] [REAL] NULL, " +
            "[LocationProductAttribute_TrayThickWidth] [REAL] NULL, " +
            "[LocationProductAttribute_TrayThickDepth] [REAL] NULL, " +
            "[LocationProductAttribute_StatusType] [TINYINT] NULL, " +
            "[LocationProductAttribute_ShelfLife] [SMALLINT] NULL, " +
            "[LocationProductAttribute_DeliveryFrequencyDays] [REAL] NULL, " +
            "[LocationProductAttribute_DeliveryMethod] [NVARCHAR](20) COLLATE database_default NULL, " +
            "[LocationProductAttribute_VendorCode] [NVARCHAR](20) COLLATE database_default NULL, " +
            "[LocationProductAttribute_Vendor] [NVARCHAR](50) COLLATE database_default NULL, " +
            "[LocationProductAttribute_ManufacturerCode] [NVARCHAR](20) COLLATE database_default NULL, " +
            "[LocationProductAttribute_Manufacturer] [NVARCHAR](50) COLLATE database_default NULL, " +
            "[LocationProductAttribute_Size] [NVARCHAR](20) COLLATE database_default NULL, " +
            "[LocationProductAttribute_UnitOfMeasure] [NVARCHAR](20) COLLATE database_default NULL, " +
            "[LocationProductAttribute_SellPrice] [REAL] NULL, " +
            "[LocationProductAttribute_SellPackCount] [SMALLINT] NULL, " +
            "[LocationProductAttribute_SellPackDescription] [NVARCHAR](100) COLLATE database_default NULL, " +
            "[LocationProductAttribute_RecommendedRetailPrice] [REAL] NULL, " +
            "[LocationProductAttribute_CostPrice] [REAL] NULL, " +
            "[LocationProductAttribute_CaseCost] [REAL] NULL, " +
            "[LocationProductAttribute_ConsumerInformation] [NVARCHAR](50) COLLATE database_default NULL, " +
            "[LocationProductAttribute_Pattern] [NVARCHAR](20) COLLATE database_default NULL, " +
            "[LocationProductAttribute_Model] [NVARCHAR](20) COLLATE database_default NULL," +
            "[LocationProductAttribute_CorporateCode] [NVARCHAR](20) COLLATE database_default NULL," +
            //"[LocationProductAttribute_CustomAttribute01] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute02] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute03] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute04] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute05] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute06] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute07] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute08] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute09] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute10] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute11] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute12] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute13] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute14] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute15] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute16] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute17] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute18] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute19] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute20] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute21] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute22] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute23] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute24] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute25] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute26] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute27] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute28] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute29] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute30] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute31] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute32] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute33] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute34] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute35] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute36] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute37] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute38] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute39] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute40] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute41] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute42] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute43] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute44] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute45] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute46] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute47] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute48] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute49] [SQL_VARIANT] NULL," +
            //"[LocationProductAttribute_CustomAttribute50] [SQL_VARIANT] NULL" +
            ")";

        private const String _createIdTableSql = "CREATE TABLE [{0}] (Product_Id INT)";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class LocationProductAttributeBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationProductAttributeDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public LocationProductAttributeBulkCopyDataReader(IEnumerable<LocationProductAttributeDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 48; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.ProductId;
                        break;
                    case 1:
                        value = _enumerator.Current.LocationId;
                        break;
                    case 2:
                        value = _enumerator.Current.EntityId;
                        break;
                    case 3:
                        value = _enumerator.Current.GTIN;
                        break;
                    case 4:
                        value = _enumerator.Current.Description;
                        break;
                    case 5:
                        value = _enumerator.Current.Height;
                        break;
                    case 6:
                        value = _enumerator.Current.Width;
                        break;
                    case 7:
                        value = _enumerator.Current.Depth;
                        break;
                    case 8:
                        value = _enumerator.Current.CasePackUnits;
                        break;
                    case 9:
                        value = _enumerator.Current.CaseHigh;
                        break;
                    case 10:
                        value = _enumerator.Current.CaseWide;
                        break;
                    case 11:
                        value = _enumerator.Current.CaseDeep;
                        break;
                    case 12:
                        value = _enumerator.Current.CaseHeight;
                        break;
                    case 13:
                        value = _enumerator.Current.CaseWidth;
                        break;
                    case 14:
                        value = _enumerator.Current.CaseDepth;
                        break;
                    case 15:
                        value = _enumerator.Current.DaysOfSupply;
                        break;
                    case 16:
                        value = _enumerator.Current.MinDeep;
                        break;
                    case 17:
                        value = _enumerator.Current.MaxDeep;
                        break;
                    case 18:
                        value = _enumerator.Current.TrayPackUnits;
                        break;
                    case 19:
                        value = _enumerator.Current.TrayHigh;
                        break;
                    case 20:
                        value = _enumerator.Current.TrayWide;
                        break;
                    case 21:
                        value = _enumerator.Current.TrayDeep;
                        break;
                    case 22:
                        value = _enumerator.Current.TrayHeight;
                        break;
                    case 23:
                        value = _enumerator.Current.TrayWidth;
                        break;
                    case 24:
                        value = _enumerator.Current.TrayDepth;
                        break;
                    case 25:
                        value = _enumerator.Current.TrayThickHeight;
                        break;
                    case 26:
                        value = _enumerator.Current.TrayThickWidth;
                        break;
                    case 27:
                        value = _enumerator.Current.TrayThickDepth;
                        break;
                    case 28:
                        value = _enumerator.Current.StatusType;
                        break;
                    case 29:
                        value = _enumerator.Current.ShelfLife;
                        break;
                    case 30:
                        value = _enumerator.Current.DeliveryFrequencyDays;
                        break;
                    case 31:
                        value = _enumerator.Current.DeliveryMethod;
                        break;
                    case 32:
                        value = _enumerator.Current.VendorCode;
                        break;
                    case 33:
                        value = _enumerator.Current.Vendor;
                        break;
                    case 34:
                        value = _enumerator.Current.ManufacturerCode;
                        break;
                    case 35:
                        value = _enumerator.Current.Manufacturer;
                        break;
                    case 36:
                        value = _enumerator.Current.Size;
                        break;
                    case 37:
                        value = _enumerator.Current.UnitOfMeasure;
                        break;
                    case 38:
                        value = _enumerator.Current.SellPrice;
                        break;
                    case 39:
                        value = _enumerator.Current.SellPackCount;
                        break;
                    case 40:
                        value = _enumerator.Current.SellPackDescription;
                        break;
                    case 41:
                        value = _enumerator.Current.RecommendedRetailPrice;
                        break;
                    case 42:
                        value = _enumerator.Current.CostPrice;
                        break;
                    case 43:
                        value = _enumerator.Current.CaseCost;
                        break;
                    case 44:
                        value = _enumerator.Current.ConsumerInformation;
                        break;
                    case 45:
                        value = _enumerator.Current.Pattern;
                        break;
                    case 46:
                        value = _enumerator.Current.Model;
                        break;
                    case 47:
                        value = _enumerator.Current.CorporateCode;
                        break;

                    //#region Custom Attributes
                    //case 48:
                    //    value = _enumerator.Current.CustomAttribute01;
                    //    break;
                    //case 49:
                    //    value = _enumerator.Current.CustomAttribute02;
                    //    break;
                    //case 50:
                    //    value = _enumerator.Current.CustomAttribute03;
                    //    break;
                    //case 51:
                    //    value = _enumerator.Current.CustomAttribute04;
                    //    break;
                    //case 52:
                    //    value = _enumerator.Current.CustomAttribute05;
                    //    break;
                    //case 53:
                    //    value = _enumerator.Current.CustomAttribute06;
                    //    break;
                    //case 54:
                    //    value = _enumerator.Current.CustomAttribute07;
                    //    break;
                    //case 55:
                    //    value = _enumerator.Current.CustomAttribute08;
                    //    break;
                    //case 56:
                    //    value = _enumerator.Current.CustomAttribute09;
                    //    break;
                    //case 57:
                    //    value = _enumerator.Current.CustomAttribute10;
                    //    break;
                    //case 58:
                    //    value = _enumerator.Current.CustomAttribute11;
                    //    break;
                    //case 59:
                    //    value = _enumerator.Current.CustomAttribute12;
                    //    break;
                    //case 60:
                    //    value = _enumerator.Current.CustomAttribute13;
                    //    break;
                    //case 61:
                    //    value = _enumerator.Current.CustomAttribute14;
                    //    break;
                    //case 62:
                    //    value = _enumerator.Current.CustomAttribute15;
                    //    break;
                    //case 63:
                    //    value = _enumerator.Current.CustomAttribute16;
                    //    break;
                    //case 64:
                    //    value = _enumerator.Current.CustomAttribute17;
                    //    break;
                    //case 65:
                    //    value = _enumerator.Current.CustomAttribute18;
                    //    break;
                    //case 66:
                    //    value = _enumerator.Current.CustomAttribute19;
                    //    break;
                    //case 67:
                    //    value = _enumerator.Current.CustomAttribute20;
                    //    break;
                    //case 68:
                    //    value = _enumerator.Current.CustomAttribute21;
                    //    break;
                    //case 69:
                    //    value = _enumerator.Current.CustomAttribute22;
                    //    break;
                    //case 70:
                    //    value = _enumerator.Current.CustomAttribute23;
                    //    break;
                    //case 71:
                    //    value = _enumerator.Current.CustomAttribute24;
                    //    break;
                    //case 72:
                    //    value = _enumerator.Current.CustomAttribute25;
                    //    break;
                    //case 73:
                    //    value = _enumerator.Current.CustomAttribute26;
                    //    break;
                    //case 74:
                    //    value = _enumerator.Current.CustomAttribute27;
                    //    break;
                    //case 75:
                    //    value = _enumerator.Current.CustomAttribute28;
                    //    break;
                    //case 76:
                    //    value = _enumerator.Current.CustomAttribute29;
                    //    break;
                    //case 77:
                    //    value = _enumerator.Current.CustomAttribute30;
                    //    break;
                    //case 78:
                    //    value = _enumerator.Current.CustomAttribute31;
                    //    break;
                    //case 79:
                    //    value = _enumerator.Current.CustomAttribute32;
                    //    break;
                    //case 80:
                    //    value = _enumerator.Current.CustomAttribute33;
                    //    break;
                    //case 81:
                    //    value = _enumerator.Current.CustomAttribute34;
                    //    break;
                    //case 82:
                    //    value = _enumerator.Current.CustomAttribute35;
                    //    break;
                    //case 83:
                    //    value = _enumerator.Current.CustomAttribute36;
                    //    break;
                    //case 84:
                    //    value = _enumerator.Current.CustomAttribute37;
                    //    break;
                    //case 85:
                    //    value = _enumerator.Current.CustomAttribute38;
                    //    break;
                    //case 86:
                    //    value = _enumerator.Current.CustomAttribute39;
                    //    break;
                    //case 87:
                    //    value = _enumerator.Current.CustomAttribute40;
                    //    break;
                    //case 88:
                    //    value = _enumerator.Current.CustomAttribute41;
                    //    break;
                    //case 89:
                    //    value = _enumerator.Current.CustomAttribute42;
                    //    break;
                    //case 90:
                    //    value = _enumerator.Current.CustomAttribute43;
                    //    break;
                    //case 91:
                    //    value = _enumerator.Current.CustomAttribute44;
                    //    break;
                    //case 92:
                    //    value = _enumerator.Current.CustomAttribute45;
                    //    break;
                    //case 93:
                    //    value = _enumerator.Current.CustomAttribute46;
                    //    break;
                    //case 94:
                    //    value = _enumerator.Current.CustomAttribute47;
                    //    break;
                    //case 95:
                    //    value = _enumerator.Current.CustomAttribute48;
                    //    break;
                    //case 96:
                    //    value = _enumerator.Current.CustomAttribute49;
                    //    break;
                    //case 97:
                    //    value = _enumerator.Current.CustomAttribute50;
                    //    break;
                    //#endregion
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        /// <summary>
        /// Bulk reader for a key value list
        /// </summary>
        private class BulkCopyDataReader<T1, T2> : IDataReader
        {
            #region Fields
            private IEnumerator<Tuple<T1, T2>> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return 2; }
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public BulkCopyDataReader(IEnumerable<Tuple<T1, T2>> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public Object GetValue(Int32 i)
            {
                Object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Item1;
                        break;
                    case 1:
                        value = _enumerator.Current.Item2;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        /// <summary>
        /// A simple data reader used to bulk insert product data
        /// into the sql database
        /// </summary>
        private class ProductCodeBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<Int32> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ProductCodeBulkCopyDataReader(IEnumerable<Int32> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 1; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private LocationProductAttributeDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationProductAttributeDto
            {
                ProductId = (Int32)GetValue(dr[FieldNames.LocationProductAttributeProductId]),
                LocationId = (Int16)GetValue(dr[FieldNames.LocationProductAttributeLocationId]),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationProductAttributeEntityId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.LocationProductAttributeRowVersion])),
                GTIN = (String)GetValue(dr[FieldNames.LocationProductAttributeGTIN]),
                Description = (String)GetValue(dr[FieldNames.LocationProductAttributeDescription]),
                Height = (Single?)GetValue(dr[FieldNames.LocationProductAttributeHeight]),
                Width = (Single?)GetValue(dr[FieldNames.LocationProductAttributeWidth]),
                Depth = (Single?)GetValue(dr[FieldNames.LocationProductAttributeDepth]),
                CasePackUnits = (Int16?)GetValue(dr[FieldNames.LocationProductAttributeCasePackUnits]),
                CaseHigh = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeCaseHigh]),
                CaseWide = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeCaseWide]),
                CaseDeep = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeCaseDeep]),
                CaseHeight = (Single?)GetValue(dr[FieldNames.LocationProductAttributeCaseHeight]),
                CaseDepth = (Single?)GetValue(dr[FieldNames.LocationProductAttributeCaseDepth]),
                CaseWidth = (Single?)GetValue(dr[FieldNames.LocationProductAttributeCaseWidth]),
                DaysOfSupply = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeDaysOfSupply]),
                MinDeep = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeMinDeep]),
                MaxDeep = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeMaxDeep]),
                TrayPackUnits = (Int16?)GetValue(dr[FieldNames.LocationProductAttributeTrayPackUnits]),
                TrayHigh = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeTrayHigh]),
                TrayWide = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeTrayWide]),
                TrayDeep = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeTrayDeep]),
                TrayHeight = (Single?)GetValue(dr[FieldNames.LocationProductAttributeTrayHeight]),
                TrayDepth = (Single?)GetValue(dr[FieldNames.LocationProductAttributeTrayDepth]),
                TrayWidth = (Single?)GetValue(dr[FieldNames.LocationProductAttributeTrayWidth]),
                TrayThickHeight = (Single?)GetValue(dr[FieldNames.LocationProductAttributeTrayThickHeight]),
                TrayThickDepth = (Single?)GetValue(dr[FieldNames.LocationProductAttributeTrayThickDepth]),
                TrayThickWidth = (Single?)GetValue(dr[FieldNames.LocationProductAttributeTrayThickWidth]),
                StatusType = (Byte?)GetValue(dr[FieldNames.LocationProductAttributeStatusType]),
                ShelfLife = (Int16?)GetValue(dr[FieldNames.LocationProductAttributeShelfLife]),
                DeliveryFrequencyDays = (Single?)GetValue(dr[FieldNames.LocationProductAttributeDeliveryFrequencyDays]),
                DeliveryMethod = (String)GetValue(dr[FieldNames.LocationProductAttributeDeliveryMethod]),
                VendorCode = (String)GetValue(dr[FieldNames.LocationProductAttributeVendorCode]),
                Vendor = (String)GetValue(dr[FieldNames.LocationProductAttributeVendor]),
                ManufacturerCode = (String)GetValue(dr[FieldNames.LocationProductAttributeManufacturerCode]),
                Manufacturer = (String)GetValue(dr[FieldNames.LocationProductAttributeManufacturer]),
                Size = (String)GetValue(dr[FieldNames.LocationProductAttributeSize]),
                UnitOfMeasure = (String)GetValue(dr[FieldNames.LocationProductAttributeUnitOfMeasure]),
                SellPrice = (Single?)GetValue(dr[FieldNames.LocationProductAttributeSellPrice]),
                SellPackCount = (Int16?)GetValue(dr[FieldNames.LocationProductAttributeSellPackCount]),
                SellPackDescription = (String)GetValue(dr[FieldNames.LocationProductAttributeSellPackDescription]),
                RecommendedRetailPrice = (Single?)GetValue(dr[FieldNames.LocationProductAttributeRecommendedRetailPrice]),
                CostPrice = (Single?)GetValue(dr[FieldNames.LocationProductAttributeCostPrice]),
                CaseCost = (Single?)GetValue(dr[FieldNames.LocationProductAttributeCaseCost]),
                ConsumerInformation = (String)GetValue(dr[FieldNames.LocationProductAttributeConsumerInformation]),
                Pattern = (String)GetValue(dr[FieldNames.LocationProductAttributePattern]),
                Model = (String)GetValue(dr[FieldNames.LocationProductAttributeModel]),
                CorporateCode = (String)GetValue(dr[FieldNames.LocationProductAttributeCorporateCode]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationProductAttributeDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationProductAttributeDateLastModified]),

                //CustomAttribute01 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute01]),
                //CustomAttribute02 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute02]),
                //CustomAttribute03 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute03]),
                //CustomAttribute04 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute04]),
                //CustomAttribute05 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute05]),
                //CustomAttribute06 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute06]),
                //CustomAttribute07 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute07]),
                //CustomAttribute08 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute08]),
                //CustomAttribute09 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute09]),
                //CustomAttribute10 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute10]),
                //CustomAttribute11 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute11]),
                //CustomAttribute12 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute12]),
                //CustomAttribute13 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute13]),
                //CustomAttribute14 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute14]),
                //CustomAttribute15 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute15]),
                //CustomAttribute16 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute16]),
                //CustomAttribute17 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute17]),
                //CustomAttribute18 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute18]),
                //CustomAttribute19 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute19]),
                //CustomAttribute20 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute20]),
                //CustomAttribute21 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute21]),
                //CustomAttribute22 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute22]),
                //CustomAttribute23 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute23]),
                //CustomAttribute24 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute24]),
                //CustomAttribute25 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute25]),
                //CustomAttribute26 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute26]),
                //CustomAttribute27 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute27]),
                //CustomAttribute28 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute28]),
                //CustomAttribute29 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute29]),
                //CustomAttribute30 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute30]),
                //CustomAttribute31 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute31]),
                //CustomAttribute32 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute32]),
                //CustomAttribute33 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute33]),
                //CustomAttribute34 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute34]),
                //CustomAttribute35 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute35]),
                //CustomAttribute36 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute36]),
                //CustomAttribute37 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute37]),
                //CustomAttribute38 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute38]),
                //CustomAttribute39 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute39]),
                //CustomAttribute40 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute40]),
                //CustomAttribute41 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute41]),
                //CustomAttribute42 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute42]),
                //CustomAttribute43 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute43]),
                //CustomAttribute44 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute44]),
                //CustomAttribute45 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute45]),
                //CustomAttribute46 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute46]),
                //CustomAttribute47 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute47]),
                //CustomAttribute48 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute48]),
                //CustomAttribute49 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute49]),
                //CustomAttribute50 = GetValue(dr[FieldNames.LocationProductAttributeCustomAttribute50])

            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all items for the specified entity
        /// </summary>
        public IEnumerable<LocationProductAttributeDto> FetchByEntityId(int entityId)
        {
            List<LocationProductAttributeDto> dtoList = new List<LocationProductAttributeDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationProductAttributeEntityId, SqlDbType.Int, entityId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// returns all location product combinations
        /// </summary>
        public List<LocationProductAttributeDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> combinationsList)
        {
            List<LocationProductAttributeDto> dtoList = new List<LocationProductAttributeDto>();
            try
            {
                // create the temp table
                CreateTempTable(_fetchTempTableSql);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _tempTableName + "]";
                    BulkCopyDataReader<Int16, Int32> dr = new BulkCopyDataReader<Int16, Int32>(combinationsList);
                    bulkCopy.WriteToServer(dr);
                }

                // perform the fetch
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeFetchByLocationIdProductIdCombinations))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropTempTable();
            }
            return dtoList;
        }

        /// <summary>
        /// Fetches the specified dto
        /// </summary>
        public LocationProductAttributeDto FetchByLocationIdProductId(Int16 locationId, Int32 productId)
        {
            LocationProductAttributeDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeFetchById))
                {
                    CreateParameter(command, FieldNames.LocationProductAttributeLocationId, SqlDbType.SmallInt, locationId);
                    CreateParameter(command, FieldNames.LocationProductAttributeProductId, SqlDbType.Int, productId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Fetches the specified dto
        /// </summary>
        public List<LocationProductAttributeDto> FetchByEntityIdLocationIdProductIds(Int32 entityId, Int16 locationId, IEnumerable<Int32> productIds)
        {
            List<LocationProductAttributeDto> dtoList = new List<LocationProductAttributeDto>();
            String tmpTableName = "#tmpProducts";
            try
            {
                DropProductTempTable();

                //create temp table
                CreateProductTempTable(_createIdTableSql, tmpTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + tmpTableName + "]";
                    ProductCodeBulkCopyDataReader dr = new ProductCodeBulkCopyDataReader(productIds);
                    bulkCopy.WriteToServer(dr);
                }

                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeFetchByEntityIdLocationIdProductIds))
                {
                    CreateParameter(command, FieldNames.LocationProductAttributeEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.LocationProductAttributeLocationId, SqlDbType.SmallInt, locationId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a dto into the database
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(LocationProductAttributeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeInsert))
                {
                    // Parameter to store returned new Id
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.LocationProductAttributeRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationProductAttributeDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationProductAttributeDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Add in the other parameters
                    CreateParameter(command, FieldNames.LocationProductAttributeProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.LocationProductAttributeLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.LocationProductAttributeEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LocationProductAttributeGTIN, SqlDbType.NVarChar, dto.GTIN);
                    CreateParameter(command, FieldNames.LocationProductAttributeDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.LocationProductAttributeHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.LocationProductAttributeWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.LocationProductAttributeDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.LocationProductAttributeCasePackUnits, SqlDbType.SmallInt, dto.CasePackUnits);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseHigh, SqlDbType.TinyInt, dto.CaseHigh);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseWide, SqlDbType.TinyInt, dto.CaseWide);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseDeep, SqlDbType.TinyInt, dto.CaseDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseHeight, SqlDbType.Real, dto.CaseHeight);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseWidth, SqlDbType.Real, dto.CaseWidth);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseDepth, SqlDbType.Real, dto.CaseDepth);
                    CreateParameter(command, FieldNames.LocationProductAttributeDaysOfSupply, SqlDbType.TinyInt, dto.DaysOfSupply);
                    CreateParameter(command, FieldNames.LocationProductAttributeMinDeep, SqlDbType.TinyInt, dto.MinDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeMaxDeep, SqlDbType.TinyInt, dto.MaxDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayPackUnits, SqlDbType.SmallInt, dto.TrayPackUnits);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayHigh, SqlDbType.TinyInt, dto.TrayHigh);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayWide, SqlDbType.TinyInt, dto.TrayWide);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayDeep, SqlDbType.TinyInt, dto.TrayDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayHeight, SqlDbType.Real, dto.TrayHeight);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayDepth, SqlDbType.Real, dto.TrayDepth);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayWidth, SqlDbType.Real, dto.TrayWidth);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayThickHeight, SqlDbType.Real, dto.TrayThickHeight);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayThickDepth, SqlDbType.Real, dto.TrayThickDepth);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayThickWidth, SqlDbType.Real, dto.TrayThickWidth);
                    CreateParameter(command, FieldNames.LocationProductAttributeStatusType, SqlDbType.TinyInt, dto.StatusType);
                    CreateParameter(command, FieldNames.LocationProductAttributeShelfLife, SqlDbType.SmallInt, dto.ShelfLife);
                    CreateParameter(command, FieldNames.LocationProductAttributeDeliveryFrequencyDays, SqlDbType.Real, dto.DeliveryFrequencyDays);
                    CreateParameter(command, FieldNames.LocationProductAttributeDeliveryMethod, SqlDbType.NVarChar, dto.DeliveryMethod);
                    CreateParameter(command, FieldNames.LocationProductAttributeVendorCode, SqlDbType.NVarChar, dto.VendorCode);
                    CreateParameter(command, FieldNames.LocationProductAttributeVendor, SqlDbType.NVarChar, dto.Vendor);
                    CreateParameter(command, FieldNames.LocationProductAttributeManufacturerCode, SqlDbType.NVarChar, dto.ManufacturerCode);
                    CreateParameter(command, FieldNames.LocationProductAttributeManufacturer, SqlDbType.NVarChar, dto.Manufacturer);
                    CreateParameter(command, FieldNames.LocationProductAttributeSize, SqlDbType.NVarChar, dto.Size);
                    CreateParameter(command, FieldNames.LocationProductAttributeUnitOfMeasure, SqlDbType.NVarChar, dto.UnitOfMeasure);
                    CreateParameter(command, FieldNames.LocationProductAttributeSellPrice, SqlDbType.Real, dto.SellPrice);
                    CreateParameter(command, FieldNames.LocationProductAttributeSellPackCount, SqlDbType.SmallInt, dto.SellPackCount);
                    CreateParameter(command, FieldNames.LocationProductAttributeSellPackDescription, SqlDbType.NVarChar, dto.SellPackDescription);
                    CreateParameter(command, FieldNames.LocationProductAttributeRecommendedRetailPrice, SqlDbType.Real, dto.RecommendedRetailPrice);
                    CreateParameter(command, FieldNames.LocationProductAttributeCostPrice, SqlDbType.Real, dto.CostPrice);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseCost, SqlDbType.Real, dto.CaseCost);
                    CreateParameter(command, FieldNames.LocationProductAttributeConsumerInformation, SqlDbType.NVarChar, dto.ConsumerInformation);
                    CreateParameter(command, FieldNames.LocationProductAttributePattern, SqlDbType.NVarChar, dto.Pattern);
                    CreateParameter(command, FieldNames.LocationProductAttributeModel, SqlDbType.NVarChar, dto.Model);
                    CreateParameter(command, FieldNames.LocationProductAttributeCorporateCode, SqlDbType.NVarChar, dto.CorporateCode);

                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates a dto into the database
        /// </summary>
        public void Update(LocationProductAttributeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeUpdate))
                {
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.LocationProductAttributeRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationProductAttributeDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationProductAttributeDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    CreateParameter(command, FieldNames.LocationProductAttributeProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.LocationProductAttributeLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.LocationProductAttributeEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LocationProductAttributeGTIN, SqlDbType.NVarChar, dto.GTIN);
                    CreateParameter(command, FieldNames.LocationProductAttributeDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.LocationProductAttributeHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.LocationProductAttributeWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.LocationProductAttributeDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.LocationProductAttributeCasePackUnits, SqlDbType.SmallInt, dto.CasePackUnits);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseHigh, SqlDbType.TinyInt, dto.CaseHigh);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseWide, SqlDbType.TinyInt, dto.CaseWide);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseDeep, SqlDbType.TinyInt, dto.CaseDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseHeight, SqlDbType.Real, dto.CaseHeight);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseWidth, SqlDbType.Real, dto.CaseWidth);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseDepth, SqlDbType.Real, dto.CaseDepth);
                    CreateParameter(command, FieldNames.LocationProductAttributeDaysOfSupply, SqlDbType.TinyInt, dto.DaysOfSupply);
                    CreateParameter(command, FieldNames.LocationProductAttributeMinDeep, SqlDbType.TinyInt, dto.MinDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeMaxDeep, SqlDbType.TinyInt, dto.MaxDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayPackUnits, SqlDbType.SmallInt, dto.TrayPackUnits);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayHigh, SqlDbType.TinyInt, dto.TrayHigh);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayWide, SqlDbType.TinyInt, dto.TrayWide);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayDeep, SqlDbType.TinyInt, dto.TrayDeep);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayHeight, SqlDbType.Real, dto.TrayHeight);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayDepth, SqlDbType.Real, dto.TrayDepth);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayWidth, SqlDbType.Real, dto.TrayWidth);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayThickHeight, SqlDbType.Real, dto.TrayThickHeight);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayThickDepth, SqlDbType.Real, dto.TrayThickDepth);
                    CreateParameter(command, FieldNames.LocationProductAttributeTrayThickWidth, SqlDbType.Real, dto.TrayThickWidth);
                    CreateParameter(command, FieldNames.LocationProductAttributeStatusType, SqlDbType.TinyInt, dto.StatusType);
                    CreateParameter(command, FieldNames.LocationProductAttributeShelfLife, SqlDbType.SmallInt, dto.ShelfLife);
                    CreateParameter(command, FieldNames.LocationProductAttributeDeliveryFrequencyDays, SqlDbType.Real, dto.DeliveryFrequencyDays);
                    CreateParameter(command, FieldNames.LocationProductAttributeDeliveryMethod, SqlDbType.NVarChar, dto.DeliveryMethod);
                    CreateParameter(command, FieldNames.LocationProductAttributeVendorCode, SqlDbType.NVarChar, dto.VendorCode);
                    CreateParameter(command, FieldNames.LocationProductAttributeVendor, SqlDbType.NVarChar, dto.Vendor);
                    CreateParameter(command, FieldNames.LocationProductAttributeManufacturerCode, SqlDbType.NVarChar, dto.ManufacturerCode);
                    CreateParameter(command, FieldNames.LocationProductAttributeManufacturer, SqlDbType.NVarChar, dto.Manufacturer);
                    CreateParameter(command, FieldNames.LocationProductAttributeSize, SqlDbType.NVarChar, dto.Size);
                    CreateParameter(command, FieldNames.LocationProductAttributeUnitOfMeasure, SqlDbType.NVarChar, dto.UnitOfMeasure);
                    CreateParameter(command, FieldNames.LocationProductAttributeSellPrice, SqlDbType.Real, dto.SellPrice);
                    CreateParameter(command, FieldNames.LocationProductAttributeSellPackCount, SqlDbType.SmallInt, dto.SellPackCount);
                    CreateParameter(command, FieldNames.LocationProductAttributeSellPackDescription, SqlDbType.NVarChar, dto.SellPackDescription);
                    CreateParameter(command, FieldNames.LocationProductAttributeRecommendedRetailPrice, SqlDbType.Real, dto.RecommendedRetailPrice);
                    CreateParameter(command, FieldNames.LocationProductAttributeCostPrice, SqlDbType.Real, dto.CostPrice);
                    CreateParameter(command, FieldNames.LocationProductAttributeCaseCost, SqlDbType.Real, dto.CaseCost);
                    CreateParameter(command, FieldNames.LocationProductAttributeConsumerInformation, SqlDbType.NVarChar, dto.ConsumerInformation);
                    CreateParameter(command, FieldNames.LocationProductAttributePattern, SqlDbType.NVarChar, dto.Pattern);
                    CreateParameter(command, FieldNames.LocationProductAttributeModel, SqlDbType.NVarChar, dto.Model);
                    CreateParameter(command, FieldNames.LocationProductAttributeCorporateCode, SqlDbType.NVarChar, dto.CorporateCode);
                   
                    // execute the insert statement
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified item from the database
        /// </summary>
        public void DeleteById(Int16 locationId, Int32 productId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeDeleteById))
                {
                    CreateParameter(command, FieldNames.LocationProductAttributeLocationId, SqlDbType.SmallInt, locationId);
                    CreateParameter(command, FieldNames.LocationProductAttributeProductId, SqlDbType.Int, productId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes all items for the specified entity
        /// </summary>
        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeDeleteByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationProductAttributeEntityId, SqlDbType.Int, entityId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert
        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<LocationProductAttributeDto> locationProductAttributeDtoList, LocationProductAttributeIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new LocationProductAttributeIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<LocationProductAttributeIdSearchCriteria, LocationProductAttributeDto> dtoList = new Dictionary<LocationProductAttributeIdSearchCriteria, LocationProductAttributeDto>();
            foreach (LocationProductAttributeDto dto in locationProductAttributeDtoList)
            {
                // create our criteria
                LocationProductAttributeIdSearchCriteria criteria = new LocationProductAttributeIdSearchCriteria()
                {
                    LocationCode = dto.LocationId.ToString(),
                    ProductGTIN = dto.ProductId.ToString()
                };

                // add to the dto list
                if (dtoList.ContainsKey(criteria))
                {
                    dtoList[criteria] = dto;
                }
                else
                {
                    dtoList.Add(criteria, dto);
                }
            }

            try
            {
                // create the temp table
                CreateTempTable(_upsertTempTableSql);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _tempTableName + "]";
                    LocationProductAttributeBulkCopyDataReader dr = new LocationProductAttributeBulkCopyDataReader(dtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String 
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch import
                Dictionary<Tuple<Int32, Int16>, RowVersion> rowVersions = new Dictionary<Tuple<Int32, Int16>, RowVersion>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductAttributeBulkUpsert))
                {
                    //Setup LocationProductAttribute SetProperties string parameter
                    CreateParameter(command, FieldNames.LocationProductAttributeSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Tuple<Int32, Int16> outputData =
                                new Tuple<Int32, Int16>
                                    (
                                        (Int32)GetValue(dr[FieldNames.LocationProductAttributeProductId]),
                                        (Int16)GetValue(dr[FieldNames.LocationProductAttributeLocationId])
                                    );
                            rowVersions.Add(outputData, new RowVersion(GetValue(dr[FieldNames.LocationProductAttributeRowVersion])));
                        }
                    }
                }
                //Assign each dto it's rowVersion from the dictionary
                foreach (LocationProductAttributeDto dto in locationProductAttributeDtoList)
                {
                    RowVersion rowVersion;
                    rowVersions.TryGetValue(new Tuple<Int32, Int16>(dto.ProductId, dto.LocationId), out rowVersion);

                    if (rowVersion != null)
                    {
                        dto.RowVersion = rowVersion;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropTempTable();
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        private void CreateTempTable(String tableSchema)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, tableSchema, _tempTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Creates a temporary table
        /// </summary>
        private void CreateProductTempTable(String createSql, String tempTableName)
        {
            // create the temp table
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, createSql, tempTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Drops a temporary import table
        /// </summary>
        private void DropTempTable()
        {
            try
            {
                using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropTempTableSql, _tempTableName), CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch { }
        }

        /// <summary>
        /// Drops the temporary table on the given connection
        /// </summary>
        /// <param name="connection">SQL connection to use</param>
        private void DropProductTempTable()
        {
            try
            {
                String dropTempTableSQL = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
                using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, dropTempTableSQL, "#tmpProducts"), CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch { }
        }

        /// <summary>
        /// Generates the property setter string from a dto
        /// </summary>
        private String GenerateSetPropertiesString(LocationProductAttributeIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            #region Standard Properties
            if (isSetDto.IsCaseCostSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCaseCost);
            }
            if (isSetDto.IsCaseDeepSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCaseDeep);
            }
            if (isSetDto.IsCaseDepthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCaseDepth);
            }
            if (isSetDto.IsCaseHeightSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCaseHeight);
            }
            if (isSetDto.IsCaseHighSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCaseHigh);
            }
            if (isSetDto.IsCasePackUnitsSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCasePackUnits);
            }
            if (isSetDto.IsCaseWideSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCaseWide);
            }
            if (isSetDto.IsCaseWidthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCaseWidth);
            }
            if (isSetDto.IsConsumerInformationSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeConsumerInformation);
            }
            if (isSetDto.IsCostPriceSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCostPrice);
            }
            if (isSetDto.IsDaysOfSupplySet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeDaysOfSupply);
            }
            if (isSetDto.IsDeliveryFrequencyDaysSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeDeliveryFrequencyDays);
            }
            if (isSetDto.IsDeliveryMethodSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeDeliveryMethod);
            }
            if (isSetDto.IsDepthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeDepth);
            }
            if (isSetDto.IsDescriptionSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeDescription);
            }
            if (isSetDto.IsGTINSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeGTIN);
            }
            if (isSetDto.IsHeightSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeHeight);
            }
            if (isSetDto.IsLocationIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeLocationId);
            }
            if (isSetDto.IsManufacturerCodeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeManufacturerCode);
            }
            if (isSetDto.IsManufacturerSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeManufacturer);
            }
            if (isSetDto.IsMaxDeepSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeMaxDeep);
            }
            if (isSetDto.IsMinDeepSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeMinDeep);
            }
            if (isSetDto.IsModelSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeModel);
            }
            if (isSetDto.IsPatternSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributePattern);
            }
            if (isSetDto.IsProductIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeProductId);
            }
            if (isSetDto.IsRecommendedRetailPriceSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeRecommendedRetailPrice);
            }
            if (isSetDto.IsSellPackCountSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeSellPackCount);
            }
            if (isSetDto.IsSellPackDescriptionSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeSellPackDescription);
            }
            if (isSetDto.IsSellPriceSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeSellPrice);
            }
            if (isSetDto.IsShelfLifeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeShelfLife);
            }
            if (isSetDto.IsSizeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeSize);
            }
            if (isSetDto.IsStatusTypeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeStatusType);
            }
            if (isSetDto.IsTrayDeepSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayDeep);
            }
            if (isSetDto.IsTrayDepthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayDepth);
            }
            if (isSetDto.IsTrayHeightSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayHeight);
            }
            if (isSetDto.IsTrayHighSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayHigh);
            }
            if (isSetDto.IsTrayPackUnitsSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayPackUnits);
            }
            if (isSetDto.IsTrayThickDepthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayThickDepth);
            }
            if (isSetDto.IsTrayThickHeightSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayThickHeight);
            }
            if (isSetDto.IsTrayThickWidthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayThickWidth);
            }
            if (isSetDto.IsTrayWideSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayWide);
            }
            if (isSetDto.IsTrayWidthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeTrayWidth);
            }
            if (isSetDto.IsUnitOfMeasureSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeUnitOfMeasure);
            }
            if (isSetDto.IsVendorCodeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeVendorCode);
            }
            if (isSetDto.IsVendorSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeVendor);
            }
            if (isSetDto.IsWidthSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeWidth);
            }
            if (isSetDto.IsCorporateCodeSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCorporateCode);
            }
            #endregion

            //#region Custom Attributes
            //if (isSetDto.IsCustomAttribute01Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute01);
            //}
            //if (isSetDto.IsCustomAttribute02Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute02);
            //}
            //if (isSetDto.IsCustomAttribute03Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute03);
            //}
            //if (isSetDto.IsCustomAttribute04Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute04);
            //}
            //if (isSetDto.IsCustomAttribute05Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute05);
            //}
            //if (isSetDto.IsCustomAttribute06Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute06);
            //}
            //if (isSetDto.IsCustomAttribute07Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute07);
            //}
            //if (isSetDto.IsCustomAttribute08Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute08);
            //}
            //if (isSetDto.IsCustomAttribute09Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute09);
            //}
            //if (isSetDto.IsCustomAttribute10Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute10);
            //}
            //if (isSetDto.IsCustomAttribute11Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute11);
            //}
            //if (isSetDto.IsCustomAttribute12Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute12);
            //}
            //if (isSetDto.IsCustomAttribute13Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute13);
            //}
            //if (isSetDto.IsCustomAttribute14Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute14);
            //}
            //if (isSetDto.IsCustomAttribute15Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute15);
            //}
            //if (isSetDto.IsCustomAttribute16Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute16);
            //}
            //if (isSetDto.IsCustomAttribute17Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute17);
            //}
            //if (isSetDto.IsCustomAttribute18Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute18);
            //}
            //if (isSetDto.IsCustomAttribute19Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute19);
            //}
            //if (isSetDto.IsCustomAttribute20Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute20);
            //}
            //if (isSetDto.IsCustomAttribute21Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute21);
            //}
            //if (isSetDto.IsCustomAttribute22Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute22);
            //}
            //if (isSetDto.IsCustomAttribute23Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute23);
            //}
            //if (isSetDto.IsCustomAttribute24Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute24);
            //}
            //if (isSetDto.IsCustomAttribute25Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute25);
            //}
            //if (isSetDto.IsCustomAttribute26Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute26);
            //}
            //if (isSetDto.IsCustomAttribute27Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute27);
            //}
            //if (isSetDto.IsCustomAttribute28Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute28);
            //}
            //if (isSetDto.IsCustomAttribute29Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute29);
            //}
            //if (isSetDto.IsCustomAttribute30Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute30);
            //}
            //if (isSetDto.IsCustomAttribute31Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute31);
            //}
            //if (isSetDto.IsCustomAttribute32Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute32);
            //}
            //if (isSetDto.IsCustomAttribute33Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute33);
            //}
            //if (isSetDto.IsCustomAttribute34Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute34);
            //}
            //if (isSetDto.IsCustomAttribute35Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute35);
            //}
            //if (isSetDto.IsCustomAttribute36Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute36);
            //}
            //if (isSetDto.IsCustomAttribute37Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute37);
            //}
            //if (isSetDto.IsCustomAttribute38Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute38);
            //}
            //if (isSetDto.IsCustomAttribute39Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute39);
            //}
            //if (isSetDto.IsCustomAttribute40Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute40);
            //}
            //if (isSetDto.IsCustomAttribute41Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute41);
            //}
            //if (isSetDto.IsCustomAttribute42Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute42);
            //}
            //if (isSetDto.IsCustomAttribute43Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute43);
            //}
            //if (isSetDto.IsCustomAttribute44Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute44);
            //}
            //if (isSetDto.IsCustomAttribute45Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute45);
            //}
            //if (isSetDto.IsCustomAttribute46Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute46);
            //}
            //if (isSetDto.IsCustomAttribute47Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute47);
            //}
            //if (isSetDto.IsCustomAttribute48Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute48);
            //}
            //if (isSetDto.IsCustomAttribute49Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute49);
            //}
            //if (isSetDto.IsCustomAttribute50Set)
            //{
            //    setProperties = String.Format("{0}#{1}", setProperties, FieldNames.LocationProductAttributeCustomAttribute50);
            //}

            //#endregion

            return setProperties;
        }
        #endregion
    }
}
