using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Data Access Layer class for retrieving records from EntityComparisonAttribute table <seealso cref="EntityComparisonAttributeDto"/> from the database. 
    /// </summary>
    public class EntityComparisonAttributeDal : Framework.Dal.Mssql.DalBase, IEntityComparisonAttributeDal
    {
        /// <summary>
        /// Fetches records from the EntityComparisonAttribute Table using the record id.
        /// </summary>
        /// <param name="entityId">Record id</param>
        /// <returns>List of Entity Comparison Attribute DTOs<seealso cref="EntityComparisonAttributeDto"/></returns>
        public IEnumerable<EntityComparisonAttributeDto> FetchByEntityId(Object entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityComparisonAttributeFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.EntityComparisonAttributeEntityId, SqlDbType.Int, (Int32)entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        var itemsToReturn = new List<EntityComparisonAttributeDto>();
                        while (dr.Read())
                            itemsToReturn.Add(EntityComparisonAttributeDto.CreateFromDataReader(dr));

                        return itemsToReturn;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return null;
        }

        /// <summary>
        /// Inserts a record in to the EntityComparisonAttribute table
        /// </summary>
        /// <param name="dto">Data to insert <seealso cref="EntityComparisonAttributeDto"/></param>
        public void Insert(EntityComparisonAttributeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityComparisonAttributeInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.EntityComparisonAttributeId, SqlDbType.Int, ParameterDirection.Output, dto.Id);
                    CreateOtherDTOParameters(dto, command);
                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        private void CreateOtherDTOParameters(EntityComparisonAttributeDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.EntityComparisonAttributeEntityId, SqlDbType.Int, dto.EntityId);
            CreateParameter(command, FieldNames.EntityComparisonAttributePropertyName, SqlDbType.NVarChar, dto.PropertyName);
            CreateParameter(command, FieldNames.EntityComparisonAttributePropertyDisplayName, SqlDbType.NVarChar, dto.PropertydisplayName);
            CreateParameter(command, FieldNames.EntityComparisonAttributeItemType, SqlDbType.TinyInt, dto.ItemType);
        }

        /// <summary>
        /// Updates the data into the EntityComparisonAttribute table
        /// </summary>
        /// <param name="dto">Data to update <seealso cref="EntityComparisonAttributeDto"/></param>
        public void Update(EntityComparisonAttributeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityComparisonAttributeUpdateById))
                {
                    var idParameter = CreateParameter(command, FieldNames.EntityComparisonAttributeId, SqlDbType.Int, dto.Id);
                    CreateOtherDTOParameters(dto, command);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes a record in the EntityComparisonAttribute table using it's id
        /// </summary>
        /// <param name="id">Id of the record to remove</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EntityComparisonAttributeDeleteById))
                {
                    CreateParameter(command, FieldNames.EntityComparisonAttributeId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
    }
}