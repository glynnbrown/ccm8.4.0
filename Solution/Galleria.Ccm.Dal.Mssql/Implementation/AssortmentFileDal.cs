﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentFileDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentFileDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentFileDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentFileDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentFileId]),
                AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentFileAssortmentId]),
                FileId = (Int32)GetValue(dr[FieldNames.AssortmentFileFileId]),
                FileName = (String)GetValue(dr[FieldNames.FileName]),
                SizeInBytes = (Int64)GetValue(dr[FieldNames.FileSizeInBytes]),
                SourceFilePath = (String)GetValue(dr[FieldNames.FileSourceFilePath]),
                UserName = (String)GetValue(dr[FieldNames.AssortmentFileUserName]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all assortment file dtos for the specified assortment
        /// </summary>
        /// <param name="assortmentId">The assortment id</param>
        /// <returns>All file dtos for the assortment</returns>
        public IEnumerable<AssortmentFileDto> FetchByAssortmentId(Int32 assortmentId)
        {
            List<AssortmentFileDto> dtoList = new List<AssortmentFileDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFileFetchByAssortmentId))
            {
                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentFileAssortmentId,
                    SqlDbType.Int,
                    assortmentId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment location id</param>
        /// <returns>The assortment location</returns>
        public AssortmentFileDto FetchById(Int32 id)
        {
            AssortmentFileDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFileFetchById))
            {
                // assortment location id
                CreateParameter(command,
                    FieldNames.AssortmentFileId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentFileDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFileInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentFileId,
                    SqlDbType.Int);

                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentFileAssortmentId,
                    SqlDbType.Int,
                    dto.AssortmentId);

                // file id
                CreateParameter(command,
                    FieldNames.AssortmentFileFileId,
                    SqlDbType.Int,
                    dto.FileId);

                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentFileDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFileUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentFileId,
                    SqlDbType.Int,
                    dto.Id);

                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentFileAssortmentId,
                    SqlDbType.Int,
                    dto.AssortmentId);

                // file id
                CreateParameter(command,
                    FieldNames.AssortmentFileFileId,
                    SqlDbType.Int,
                    dto.FileId);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentFileDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentFileId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}