﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class LocationSpaceProductGroupInfoDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationSpaceProductGroupInfoDal
    {
        #region DataTransferObject

        public LocationSpaceProductGroupInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationSpaceProductGroupInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupId]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.LocationSpaceProductGroupProductGroupId]),
                LocationId = (Int16)GetValue(dr[FieldNames.LocationId]),
                BayCount = (Single)GetValue(dr[FieldNames.LocationSpaceProductGroupBayCount]),
                ProductCount = (Int32?)GetValue(dr[FieldNames.LocationSpaceProductGroupProductCount]),
                AverageBayWidth = (Single)GetValue(dr[FieldNames.LocationSpaceProductGroupAverageBayWidth]),
                AisleName = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupAisleName]),
                ValleyName = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupValleyName]),
                ZoneName = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupZoneName]),
                CustomAttribute01 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute01]),
                CustomAttribute02 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute02]),
                CustomAttribute03 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute03]),
                CustomAttribute04 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute04]),
                CustomAttribute05 = (String)GetValue(dr[FieldNames.LocationSpaceProductGroupCustomAttribute01]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<LocationSpaceProductGroupInfoDto> FetchByProductGroupId(Int32 productGroupId)
        {
            List<LocationSpaceProductGroupInfoDto> dtoList = new List<LocationSpaceProductGroupInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceProductGroupInfoFetchByProductGroupId))
                {

                    //Product Group Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceProductGroupProductGroupId,
                        SqlDbType.Int,
                        productGroupId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

    }
}