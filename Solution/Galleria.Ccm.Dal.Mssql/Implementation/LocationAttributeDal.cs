﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Our location attribute dal
    /// </summary>
    public class LocationAttributeDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationAttributeDal
    {
        #region DataTransferObject
        /// <summary>
        /// Creates a new store attribute dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to create from</param>
        /// <returns>A new store attribute dto</returns>
        private LocationAttributeDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationAttributeDto
            {
                HasRegion = (Boolean)HasValue(dr[FieldNames.LocationRegion]),
                HasCounty = (Boolean)HasValue(dr[FieldNames.LocationCounty]),
                HasTVRegion = (Boolean)HasValue(dr[FieldNames.LocationTVRegion]),
                HasAddress1 = (Boolean)HasValue(dr[FieldNames.LocationAddress1]),
                HasAddress2 = (Boolean)HasValue(dr[FieldNames.LocationAddress2]),
                Has24Hours = (Boolean)HasAttributes(dr[FieldNames.LocationIs24Hours]),
                HasPetrolForecourt = (Boolean)HasAttributes(dr[FieldNames.LocationHasPetrolForecourt]),
                HasPetrolForecourtType = (Boolean)HasValue(dr[FieldNames.LocationPetrolForecourtType]),
                HasRestaurant = (Boolean)HasValue(dr[FieldNames.LocationRestaurant]),
                HasMezzFitted = (Boolean)HasAttributes(dr[FieldNames.LocationIsMezzFitted]),
                HasSizeGrossArea = (Boolean)HasValue(dr[FieldNames.LocationSizeGrossFloorArea]),
                HasSizeNetSalesArea = (Boolean)HasValue(dr[FieldNames.LocationSizeNetSalesArea]),
                HasSizeMezzSalesArea = (Boolean)HasValue(dr[FieldNames.LocationSizeMezzSalesArea]),
                HasCountry = (Boolean)HasValue(dr[FieldNames.LocationCountry]),
                HasManagerName = (Boolean)HasValue(dr[FieldNames.LocationManagerName]),
                HasRegionalManagerName = (Boolean)HasValue(dr[FieldNames.LocationRegionalManagerName]),
                HasDivisionalManagerName = (Boolean)HasValue(dr[FieldNames.LocationDivisionalManagerName]),
                HasAdvertisingZone = (Boolean)HasValue(dr[FieldNames.LocationAdvertisingZone]),
                HasDistributionCentre = (Boolean)HasValue(dr[FieldNames.LocationDistributionCentre]),
                HasNewsCube = (Boolean)HasAttributes(dr[FieldNames.LocationHasNewsCube]),
                HasAtmMachines = (Boolean)HasAttributes(dr[FieldNames.LocationHasAtmMachines]),
                HasNoOfCheckouts = (Boolean)HasValue(dr[FieldNames.LocationNoOfCheckouts]),
                HasCustomerWC = (Boolean)HasAttributes(dr[FieldNames.LocationHasCustomerWC]),
                HasBabyChanging = (Boolean)HasAttributes(dr[FieldNames.LocationHasBabyChanging]),
                HasInLocationBakery = (Boolean)HasAttributes(dr[FieldNames.LocationHasInStoreBakery]),
                HasHotFoodToGo = (Boolean)HasAttributes(dr[FieldNames.LocationHasHotFoodToGo]),
                HasRotisserie = (Boolean)HasAttributes(dr[FieldNames.LocationHasRotisserie]),
                HasFishmonger = (Boolean)HasAttributes(dr[FieldNames.LocationHasFishmonger]),
                HasButcher = (Boolean)HasAttributes(dr[FieldNames.LocationHasButcher]),
                HasPizza = (Boolean)HasAttributes(dr[FieldNames.LocationHasPizza]),
                HasDeli = (Boolean)HasAttributes(dr[FieldNames.LocationHasDeli]),
                HasSaladBar = (Boolean)HasAttributes(dr[FieldNames.LocationHasSaladBar]),
                HasOrganic = (Boolean)HasAttributes(dr[FieldNames.LocationHasOrganic]),
                HasGrocery = (Boolean)HasAttributes(dr[FieldNames.LocationHasGrocery]),
                HasMobilePhones = (Boolean)HasAttributes(dr[FieldNames.LocationHasMobilePhones]),
                HasDryCleaning = (Boolean)HasAttributes(dr[FieldNames.LocationHasDryCleaning]),
                HasHomeShoppingAvailable = (Boolean)HasAttributes(dr[FieldNames.LocationHasHomeShoppingAvailable]),
                HasOptician = (Boolean)HasAttributes(dr[FieldNames.LocationHasOptician]),
                HasPharmacy = (Boolean)HasAttributes(dr[FieldNames.LocationHasPharmacy]),
                HasTravel = (Boolean)HasAttributes(dr[FieldNames.LocationHasTravel]),
                HasPhotoDepartment = (Boolean)HasAttributes(dr[FieldNames.LocationHasPhotoDepartment]),
                HasCarServiceArea = (Boolean)HasAttributes(dr[FieldNames.LocationHasCarServiceArea]),
                HasGardenCentre = (Boolean)HasAttributes(dr[FieldNames.LocationHasGardenCentre]),
                HasClinic = (Boolean)HasAttributes(dr[FieldNames.LocationHasClinic]),
                HasAlcohol = (Boolean)HasAttributes(dr[FieldNames.LocationHasAlcohol]),
                HasFashion = (Boolean)HasAttributes(dr[FieldNames.LocationHasFashion]),
                HasCafe = (Boolean)HasAttributes(dr[FieldNames.LocationHasCafe]),
                HasRecycling = (Boolean)HasAttributes(dr[FieldNames.LocationHasRecycling]),
                HasPhotocopier = (Boolean)HasAttributes(dr[FieldNames.LocationHasPhotocopier]),
                HasLottery = (Boolean)HasAttributes(dr[FieldNames.LocationHasLottery]),
                HasPostOffice = (Boolean)HasAttributes(dr[FieldNames.LocationHasPostOffice]),
                HasMovieRental = (Boolean)HasAttributes(dr[FieldNames.LocationHasMovieRental]),
                HasCity = (Boolean)HasValue(dr[FieldNames.LocationCity]),
                HasOpenMonday = (Boolean)HasAttributes(dr[FieldNames.LocationIsOpenMonday]),
                HasOpenTuesday = (Boolean)HasAttributes(dr[FieldNames.LocationIsOpenTuesday]),
                HasOpenWednesday = (Boolean)HasAttributes(dr[FieldNames.LocationIsOpenWednesday]),
                HasOpenThursday = (Boolean)HasAttributes(dr[FieldNames.LocationIsOpenThursday]),
                HasOpenFriday = (Boolean)HasAttributes(dr[FieldNames.LocationIsOpenFriday]),
                HasOpenSaturday = (Boolean)HasAttributes(dr[FieldNames.LocationIsOpenSaturday]),
                HasOpenSunday = (Boolean)HasAttributes(dr[FieldNames.LocationIsOpenSunday]),
                HasJewellery = (Boolean)HasAttributes(dr[FieldNames.LocationHasJewellery])
            };
        }

        /// <summary>
        /// Determines if the database data contains a value
        /// </summary>
        /// <param name="value">The value to check</param>
        /// <returns>True if data available, else false</returns>
        private bool HasValue(object value)
        {
            if (value == DBNull.Value)
            {
                return false;
            }

            if (value.ToString().ToUpper() == "NULL")
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines if given value has attribute to perform split
        /// </summary>
        /// <param name="value">The value to check</param>
        /// <returns>True if attribute values available else false</returns>
        private bool HasAttributes(object value)
        {
            if ((Int32)value > 1)
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns all store attribute details within the solution
        /// </summary>
        /// <returns>A store attribute dto</returns>
        public LocationAttributeDto FetchByEntityId(Int32 entityId)
        {
            LocationAttributeDto dto = new LocationAttributeDto();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationAttributeFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationEntityId, SqlDbType.SmallInt, entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion
    }
}
