﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26270 : A. Kuszyk
//  Created
// V8-27647 : L.Luong
//  Added Inventory fields
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM802
// V8-28811 : L.Luong
//  Added MetaP1Percentage to MetaP20Percentage Meta Data Properties
#endregion
#region Version History: CCM 820
// V8-29998 : I.George
//	Added PlanogramPerformanceData_MetaP1Rank to PlanogramPerformanceData_MetaP20Rank fields
// V8-30992 : A.Probyn
//  Extended for new MinimumInventoryUnits
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramPerformanceDataDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramPerformanceDataDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogramPerformanceData";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[PlanogramPerformanceData_Id] [int] NOT NULL, " +
                "[PlanogramPerformance_Id] [int] NOT NULL, " +
                "[PlanogramProduct_Id] [int] NOT NULL, " +
                "[PlanogramPerformanceData_P1] [real] NULL, " +
                "[PlanogramPerformanceData_P2] [real] NULL, " +
                "[PlanogramPerformanceData_P3] [real] NULL, " +
                "[PlanogramPerformanceData_P4] [real] NULL, " +
                "[PlanogramPerformanceData_P5] [real] NULL, " +
                "[PlanogramPerformanceData_P6] [real] NULL, " +
                "[PlanogramPerformanceData_P7] [real] NULL, " +
                "[PlanogramPerformanceData_P8] [real] NULL, " +
                "[PlanogramPerformanceData_P9] [real] NULL, " +
                "[PlanogramPerformanceData_P10] [real] NULL, " +
                "[PlanogramPerformanceData_P11] [real] NULL, " +
                "[PlanogramPerformanceData_P12] [real] NULL, " +
                "[PlanogramPerformanceData_P13] [real] NULL, " +
                "[PlanogramPerformanceData_P14] [real] NULL, " +
                "[PlanogramPerformanceData_P15] [real] NULL, " +
                "[PlanogramPerformanceData_P16] [real] NULL, " +
                "[PlanogramPerformanceData_P17] [real] NULL, " +
                "[PlanogramPerformanceData_P18] [real] NULL, " +
                "[PlanogramPerformanceData_P19] [real] NULL, " +
                "[PlanogramPerformanceData_P20] [real] NULL, " +
                "[PlanogramPerformanceData_UnitsSoldPerDay] [real] NULL, " +
                "[PlanogramPerformanceData_AchievedCasePacks] [real] NULL, " +
                "[PlanogramPerformanceData_AchievedDos] [real] NULL, " +
                "[PlanogramPerformanceData_AchievedShelfLife] [real] NULL, " +
                "[PlanogramPerformanceData_AchievedDeliveries] [real] NULL, " +
                "[PlanogramPerformanceData_MinimumInventoryUnits] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP1Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP2Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP3Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP4Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP5Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP6Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP7Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP8Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP9Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP10Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP11Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP12Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP13Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP14Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP15Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP16Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP17Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP18Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP19Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP20Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaP1Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP2Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP3Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP4Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP5Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP6Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP7Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP8Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP9Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP10Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP11Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP12Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP13Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP14Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP15Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP16Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP17Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP18Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP19Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaP20Rank] [int] NULL, " +
                "[PlanogramPerformanceData_CP1] [real] NULL, " +
                "[PlanogramPerformanceData_CP2] [real] NULL, " +
                "[PlanogramPerformanceData_CP3] [real] NULL, " +
                "[PlanogramPerformanceData_CP4] [real] NULL, " +
                "[PlanogramPerformanceData_CP5] [real] NULL, " +
                "[PlanogramPerformanceData_CP6] [real] NULL, " +
                "[PlanogramPerformanceData_CP7] [real] NULL, " +
                "[PlanogramPerformanceData_CP8] [real] NULL, " +
                "[PlanogramPerformanceData_CP9] [real] NULL, " +
                "[PlanogramPerformanceData_CP10] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP1Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP2Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP3Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP4Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP5Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP6Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP7Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP8Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP9Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP10Percentage] [real] NULL, " +
                "[PlanogramPerformanceData_MetaCP1Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP2Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP3Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP4Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP5Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP6Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP7Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP8Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP9Rank] [int] NULL, " +
                "[PlanogramPerformanceData_MetaCP10Rank] [int] NULL " +
            ")";

        #endregion        

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramPerformanceDataBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<PlanogramPerformanceDataDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramPerformanceDataBulkCopyDataReader(IEnumerable<PlanogramPerformanceDataDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 99; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.PlanogramPerformanceId;
                        break;
                    case 2:
                        value = _enumerator.Current.PlanogramProductId;
                        break;
                    case 3:
                        value = _enumerator.Current.P1;
                        break;
                    case 4:
                        value = _enumerator.Current.P2;
                        break;
                    case 5:
                        value = _enumerator.Current.P3;
                        break;
                    case 6:
                        value = _enumerator.Current.P4;
                        break;
                    case 7:
                        value = _enumerator.Current.P5;
                        break;
                    case 8:
                        value = _enumerator.Current.P6;
                        break;
                    case 9:
                        value = _enumerator.Current.P7;
                        break;
                    case 10:
                        value = _enumerator.Current.P8;
                        break;
                    case 11:
                        value = _enumerator.Current.P9;
                        break;
                    case 12:
                        value = _enumerator.Current.P10;
                        break;
                    case 13:
                        value = _enumerator.Current.P11;
                        break;
                    case 14:
                        value = _enumerator.Current.P12;
                        break;
                    case 15:
                        value = _enumerator.Current.P13;
                        break;
                    case 16:
                        value = _enumerator.Current.P14;
                        break;
                    case 17:
                        value = _enumerator.Current.P15;
                        break;
                    case 18:
                        value = _enumerator.Current.P16;
                        break;
                    case 19:
                        value = _enumerator.Current.P17;
                        break;
                    case 20:
                        value = _enumerator.Current.P18;
                        break;
                    case 21:
                        value = _enumerator.Current.P19;
                        break;
                    case 22:
                        value = _enumerator.Current.P20;
                        break;
                    case 23:
                        value = _enumerator.Current.UnitsSoldPerDay;
                        break;
                    case 24:
                        value = _enumerator.Current.AchievedCasePacks;
                        break;
                    case 25:
                        value = _enumerator.Current.AchievedDos;
                        break;
                    case 26:
                        value = _enumerator.Current.AchievedShelfLife;
                        break;
                    case 27:
                        value = _enumerator.Current.AchievedDeliveries;
                        break;
                    case 28:
                        value = _enumerator.Current.MinimumInventoryUnits;
                        break;
                    case 29:
                        value = _enumerator.Current.MetaP1Percentage;
                        break;
                    case 30:
                        value = _enumerator.Current.MetaP2Percentage;
                        break;
                    case 31:
                        value = _enumerator.Current.MetaP3Percentage;
                        break;
                    case 32:
                        value = _enumerator.Current.MetaP4Percentage;
                        break;
                    case 33:
                        value = _enumerator.Current.MetaP5Percentage;
                        break;
                    case 34:
                        value = _enumerator.Current.MetaP6Percentage;
                        break;
                    case 35:
                        value = _enumerator.Current.MetaP7Percentage;
                        break;
                    case 36:
                        value = _enumerator.Current.MetaP8Percentage;
                        break;
                    case 37:
                        value = _enumerator.Current.MetaP9Percentage;
                        break;
                    case 38:
                        value = _enumerator.Current.MetaP10Percentage;
                        break;
                    case 39:
                        value = _enumerator.Current.MetaP11Percentage;
                        break;
                    case 40:
                        value = _enumerator.Current.MetaP12Percentage;
                        break;
                    case 41:
                        value = _enumerator.Current.MetaP13Percentage;
                        break;
                    case 42:
                        value = _enumerator.Current.MetaP14Percentage;
                        break;
                    case 43:
                        value = _enumerator.Current.MetaP15Percentage;
                        break;
                    case 44:
                        value = _enumerator.Current.MetaP16Percentage;
                        break;
                    case 45:
                        value = _enumerator.Current.MetaP17Percentage;
                        break;
                    case 46:
                        value = _enumerator.Current.MetaP18Percentage;
                        break;
                    case 47:
                        value = _enumerator.Current.MetaP19Percentage;
                        break;
                    case 48:
                        value = _enumerator.Current.MetaP20Percentage;
                        break;
                    case 49:
                        value = _enumerator.Current.MetaP1Rank;
                        break;
                    case 50:
                        value = _enumerator.Current.MetaP2Rank;
                        break;
                    case 51:
                        value = _enumerator.Current.MetaP3Rank;
                        break;
                    case 52:
                        value = _enumerator.Current.MetaP4Rank;
                        break;
                    case 53:
                        value = _enumerator.Current.MetaP5Rank;
                        break;
                    case 54:
                        value = _enumerator.Current.MetaP6Rank;
                        break;
                    case 55:
                        value = _enumerator.Current.MetaP7Rank;
                        break;
                    case 56:
                        value = _enumerator.Current.MetaP8Rank;
                        break;
                    case 57:
                        value = _enumerator.Current.MetaP9Rank;
                        break;
                    case 58:
                        value = _enumerator.Current.MetaP10Rank;
                        break;
                    case 59:
                        value = _enumerator.Current.MetaP11Rank;
                        break;
                    case 60:
                        value = _enumerator.Current.MetaP12Rank;
                        break;
                    case 61:
                        value = _enumerator.Current.MetaP13Rank;
                        break;
                    case 62:
                        value = _enumerator.Current.MetaP14Rank;
                        break;
                    case 63:
                        value = _enumerator.Current.MetaP15Rank;
                        break;
                    case 64:
                        value = _enumerator.Current.MetaP16Rank;
                        break;
                    case 65:
                        value = _enumerator.Current.MetaP17Rank;
                        break;
                    case 66:
                        value = _enumerator.Current.MetaP18Rank;
                        break;
                    case 67:
                        value = _enumerator.Current.MetaP19Rank;
                        break;
                    case 68:
                        value = _enumerator.Current.MetaP20Rank;
                        break;
                    case 69:
                        value = _enumerator.Current.CP1;
                        break;
                    case 70:
                        value = _enumerator.Current.CP2;
                        break;
                    case 71:
                        value = _enumerator.Current.CP3;
                        break;
                    case 72:
                        value = _enumerator.Current.CP4;
                        break;
                    case 73:
                        value = _enumerator.Current.CP5;
                        break;
                    case 74:
                        value = _enumerator.Current.CP6;
                        break;
                    case 75:
                        value = _enumerator.Current.CP7;
                        break;
                    case 76:
                        value = _enumerator.Current.CP8;
                        break;
                    case 77:
                        value = _enumerator.Current.CP9;
                        break;
                    case 78:
                        value = _enumerator.Current.CP10;
                        break;
                    case 79:
                        value = _enumerator.Current.MetaCP1Percentage;
                        break;
                    case 80:
                        value = _enumerator.Current.MetaCP2Percentage;
                        break;
                    case 81:
                        value = _enumerator.Current.MetaCP3Percentage;
                        break;
                    case 82:
                        value = _enumerator.Current.MetaCP4Percentage;
                        break;
                    case 83:
                        value = _enumerator.Current.MetaCP5Percentage;
                        break;
                    case 84:
                        value = _enumerator.Current.MetaCP6Percentage;
                        break;
                    case 85:
                        value = _enumerator.Current.MetaCP7Percentage;
                        break;
                    case 86:
                        value = _enumerator.Current.MetaCP8Percentage;
                        break;
                    case 87:
                        value = _enumerator.Current.MetaCP9Percentage;
                        break;
                    case 88:
                        value = _enumerator.Current.MetaCP10Percentage;
                        break;
                    case 89:
                        value = _enumerator.Current.MetaCP1Rank;
                        break;
                    case 90:
                        value = _enumerator.Current.MetaCP2Rank;
                        break;
                    case 91:
                        value = _enumerator.Current.MetaCP3Rank;
                        break;
                    case 92:
                        value = _enumerator.Current.MetaCP4Rank;
                        break;
                    case 93:
                        value = _enumerator.Current.MetaCP5Rank;
                        break;
                    case 94:
                        value = _enumerator.Current.MetaCP6Rank;
                        break;
                    case 95:
                        value = _enumerator.Current.MetaCP7Rank;
                        break;
                    case 96:
                        value = _enumerator.Current.MetaCP8Rank;
                        break;
                    case 97:
                        value = _enumerator.Current.MetaCP9Rank;
                        break;
                    case 98:
                        value = _enumerator.Current.MetaCP10Rank;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private PlanogramPerformanceDataDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramPerformanceDataDto()
            {
                Id = (Object)GetValue(dr[FieldNames.PlanogramPerformanceDataId]),
                PlanogramPerformanceId = (Object)GetValue(dr[FieldNames.PlanogramPerformanceDataPlanogramPerformanceId]),
                PlanogramProductId = (Object)GetValue(dr[FieldNames.PlanogramPerformanceDataPlanogramProductId]),
                P1 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP1]),
                P2 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP2]),
                P3 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP3]),
                P4 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP4]),
                P5 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP5]),
                P6 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP6]),
                P7 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP7]),
                P8 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP8]),
                P9 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP9]),
                P10 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP10]),
                P11 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP11]),
                P12 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP12]),
                P13 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP13]),
                P14 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP14]),
                P15 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP15]),
                P16 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP16]),
                P17 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP17]),
                P18 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP18]),
                P19 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP19]),
                P20 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataP20]),
                CP1 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP1]),
                CP2 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP2]),
                CP3 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP3]),
                CP4 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP4]),
                CP5 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP5]),
                CP6 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP6]),
                CP7 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP7]),
                CP8 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP8]),
                CP9 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP9]),
                CP10 = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataCP10]),
                UnitsSoldPerDay = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataUnitsSoldPerDay]),
                AchievedCasePacks = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataAchievedCasePacks]),
                AchievedDos = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataAchievedDos]),
                AchievedShelfLife = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataAchievedShelfLife]),
                AchievedDeliveries = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataAchievedDeliveries]),
                MinimumInventoryUnits = (Single?)GetValue(dr[FieldNames. PlanogramPerformanceDataMinimumInventoryUnits]),
                MetaP1Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP1Percentage]),
                MetaP2Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP2Percentage]),
                MetaP3Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP3Percentage]),
                MetaP4Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP4Percentage]),
                MetaP5Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP5Percentage]),
                MetaP6Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP6Percentage]),
                MetaP7Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP7Percentage]),
                MetaP8Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP8Percentage]),
                MetaP9Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP9Percentage]),
                MetaP10Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP10Percentage]),
                MetaP11Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP11Percentage]),
                MetaP12Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP12Percentage]),
                MetaP13Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP13Percentage]),
                MetaP14Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP14Percentage]),
                MetaP15Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP15Percentage]),
                MetaP16Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP16Percentage]),
                MetaP17Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP17Percentage]),
                MetaP18Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP18Percentage]),
                MetaP19Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP19Percentage]),
                MetaP20Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP20Percentage]),
                MetaCP1Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP1Percentage]),
                MetaCP2Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP2Percentage]),
                MetaCP3Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP3Percentage]),
                MetaCP4Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP4Percentage]),
                MetaCP5Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP5Percentage]),
                MetaCP6Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP6Percentage]),
                MetaCP7Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP7Percentage]),
                MetaCP8Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP8Percentage]),
                MetaCP9Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP9Percentage]),
                MetaCP10Percentage = (Single?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP10Percentage]),
                MetaP1Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP1Rank]),
                MetaP2Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP2Rank]),
                MetaP3Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP3Rank]),
                MetaP4Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP4Rank]),
                MetaP5Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP5Rank]),
                MetaP6Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP6Rank]),
                MetaP7Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP7Rank]),
                MetaP8Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP8Rank]),
                MetaP9Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP9Rank]),
                MetaP10Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP10Rank]),
                MetaP11Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP11Rank]),
                MetaP12Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP12Rank]),
                MetaP13Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP13Rank]),
                MetaP14Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP14Rank]),
                MetaP15Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP15Rank]),
                MetaP16Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP16Rank]),
                MetaP17Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP17Rank]),
                MetaP18Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP18Rank]),
                MetaP19Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP19Rank]),
                MetaP20Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaP20Rank]),
                MetaCP1Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP1Rank]),
                MetaCP2Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP2Rank]),
                MetaCP3Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP3Rank]),
                MetaCP4Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP4Rank]),
                MetaCP5Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP5Rank]),
                MetaCP6Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP6Rank]),
                MetaCP7Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP7Rank]),
                MetaCP8Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP8Rank]),
                MetaCP9Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP9Rank]),
                MetaCP10Rank = (Int32?)GetValue(dr[FieldNames.PlanogramPerformanceDataMetaCP10Rank])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramPerformanceDataDto> FetchByPlanogramPerformanceId(Object id)
        {
            List<PlanogramPerformanceDataDto> dtoList = new List<PlanogramPerformanceDataDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceDataFetchByPlanogramPerformanceId))
                {
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataPlanogramPerformanceId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramPerformanceDataDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceDataInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramPerformanceDataId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataPlanogramPerformanceId, SqlDbType.Int, dto.PlanogramPerformanceId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataPlanogramProductId, SqlDbType.Int, dto.PlanogramProductId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP1, SqlDbType.Real, dto.P1);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP2, SqlDbType.Real, dto.P2);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP3, SqlDbType.Real, dto.P3);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP4, SqlDbType.Real, dto.P4);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP5, SqlDbType.Real, dto.P5);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP6, SqlDbType.Real, dto.P6);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP7, SqlDbType.Real, dto.P7);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP8, SqlDbType.Real, dto.P8);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP9, SqlDbType.Real, dto.P9);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP10, SqlDbType.Real, dto.P10);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP11, SqlDbType.Real, dto.P11);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP12, SqlDbType.Real, dto.P12);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP13, SqlDbType.Real, dto.P13);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP14, SqlDbType.Real, dto.P14);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP15, SqlDbType.Real, dto.P15);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP16, SqlDbType.Real, dto.P16);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP17, SqlDbType.Real, dto.P17);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP18, SqlDbType.Real, dto.P18);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP19, SqlDbType.Real, dto.P19);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP20, SqlDbType.Real, dto.P20);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataUnitsSoldPerDay, SqlDbType.Real, dto.UnitsSoldPerDay);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedCasePacks, SqlDbType.Real, dto.AchievedCasePacks);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedDos, SqlDbType.Real, dto.AchievedDos);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedShelfLife, SqlDbType.Real, dto.AchievedShelfLife);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedDeliveries, SqlDbType.Real, dto.AchievedDeliveries);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMinimumInventoryUnits, SqlDbType.Real, dto.MinimumInventoryUnits);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP1Percentage, SqlDbType.Real, dto.MetaP1Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP2Percentage, SqlDbType.Real, dto.MetaP2Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP3Percentage, SqlDbType.Real, dto.MetaP3Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP4Percentage, SqlDbType.Real, dto.MetaP4Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP5Percentage, SqlDbType.Real, dto.MetaP5Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP6Percentage, SqlDbType.Real, dto.MetaP6Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP7Percentage, SqlDbType.Real, dto.MetaP7Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP8Percentage, SqlDbType.Real, dto.MetaP8Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP9Percentage, SqlDbType.Real, dto.MetaP9Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP10Percentage, SqlDbType.Real, dto.MetaP10Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP11Percentage, SqlDbType.Real, dto.MetaP11Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP12Percentage, SqlDbType.Real, dto.MetaP12Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP13Percentage, SqlDbType.Real, dto.MetaP13Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP14Percentage, SqlDbType.Real, dto.MetaP14Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP15Percentage, SqlDbType.Real, dto.MetaP15Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP16Percentage, SqlDbType.Real, dto.MetaP16Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP17Percentage, SqlDbType.Real, dto.MetaP17Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP18Percentage, SqlDbType.Real, dto.MetaP18Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP19Percentage, SqlDbType.Real, dto.MetaP19Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP20Percentage, SqlDbType.Real, dto.MetaP20Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP1Rank, SqlDbType.Int, dto.MetaP1Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP2Rank, SqlDbType.Int, dto.MetaP2Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP3Rank, SqlDbType.Int, dto.MetaP3Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP4Rank, SqlDbType.Int, dto.MetaP4Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP5Rank, SqlDbType.Int, dto.MetaP5Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP6Rank, SqlDbType.Int, dto.MetaP6Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP7Rank, SqlDbType.Int, dto.MetaP7Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP8Rank, SqlDbType.Int, dto.MetaP8Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP9Rank, SqlDbType.Int, dto.MetaP9Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP10Rank, SqlDbType.Int, dto.MetaP10Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP11Rank, SqlDbType.Int, dto.MetaP11Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP12Rank, SqlDbType.Int, dto.MetaP12Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP13Rank, SqlDbType.Int, dto.MetaP13Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP14Rank, SqlDbType.Int, dto.MetaP14Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP15Rank, SqlDbType.Int, dto.MetaP15Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP16Rank, SqlDbType.Int, dto.MetaP16Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP17Rank, SqlDbType.Int, dto.MetaP17Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP18Rank, SqlDbType.Int, dto.MetaP18Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP19Rank, SqlDbType.Int, dto.MetaP19Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP20Rank, SqlDbType.Int, dto.MetaP20Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP1, SqlDbType.Real, dto.CP1);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP2, SqlDbType.Real, dto.CP2);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP3, SqlDbType.Real, dto.CP3);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP4, SqlDbType.Real, dto.CP4);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP5, SqlDbType.Real, dto.CP5);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP6, SqlDbType.Real, dto.CP6);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP7, SqlDbType.Real, dto.CP7);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP8, SqlDbType.Real, dto.CP8);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP9, SqlDbType.Real, dto.CP9);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP10, SqlDbType.Real, dto.CP10);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP1Percentage, SqlDbType.Real, dto.MetaCP1Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP2Percentage, SqlDbType.Real, dto.MetaCP2Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP3Percentage, SqlDbType.Real, dto.MetaCP3Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP4Percentage, SqlDbType.Real, dto.MetaCP4Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP5Percentage, SqlDbType.Real, dto.MetaCP5Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP6Percentage, SqlDbType.Real, dto.MetaCP6Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP7Percentage, SqlDbType.Real, dto.MetaCP7Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP8Percentage, SqlDbType.Real, dto.MetaCP8Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP9Percentage, SqlDbType.Real, dto.MetaCP9Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP10Percentage, SqlDbType.Real, dto.MetaCP10Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP1Rank, SqlDbType.Int, dto.MetaCP1Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP2Rank, SqlDbType.Int, dto.MetaCP2Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP3Rank, SqlDbType.Int, dto.MetaCP3Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP4Rank, SqlDbType.Int, dto.MetaCP4Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP5Rank, SqlDbType.Int, dto.MetaCP5Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP6Rank, SqlDbType.Int, dto.MetaCP6Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP7Rank, SqlDbType.Int, dto.MetaCP7Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP8Rank, SqlDbType.Int, dto.MetaCP8Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP9Rank, SqlDbType.Int, dto.MetaCP9Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP10Rank, SqlDbType.Int, dto.MetaCP10Rank);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            HashSet<PlanogramPerformanceDataDto> dtosToSave = new HashSet<PlanogramPerformanceDataDto>(dtos, new PlanogramPerformanceDataDtoEqualityComparer());

            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, PlanogramPerformanceDataDto> index = new Dictionary<Object, PlanogramPerformanceDataDto>();
            foreach (PlanogramPerformanceDataDto dto in dtosToSave)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramPerformanceDataBulkCopyDataReader dr = new PlanogramPerformanceDataBulkCopyDataReader(dtosToSave);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceDataBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        public class PlanogramPerformanceDataDtoEqualityComparer : IEqualityComparer<PlanogramPerformanceDataDto>
        {
            public Boolean Equals(PlanogramPerformanceDataDto x, PlanogramPerformanceDataDto y)
            {
                return x.PlanogramProductId == y.PlanogramProductId;
            }

            public Int32 GetHashCode(PlanogramPerformanceDataDto obj)
            {
                return (int)obj.PlanogramProductId ^ 128;
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramPerformanceDataDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceDataUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataPlanogramPerformanceId, SqlDbType.Int, dto.PlanogramPerformanceId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataPlanogramProductId, SqlDbType.Int, dto.PlanogramProductId);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP1, SqlDbType.Real, dto.P1);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP2, SqlDbType.Real, dto.P2);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP3, SqlDbType.Real, dto.P3);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP4, SqlDbType.Real, dto.P4);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP5, SqlDbType.Real, dto.P5);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP6, SqlDbType.Real, dto.P6);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP7, SqlDbType.Real, dto.P7);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP8, SqlDbType.Real, dto.P8);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP9, SqlDbType.Real, dto.P9);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP10, SqlDbType.Real, dto.P10);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP11, SqlDbType.Real, dto.P11);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP12, SqlDbType.Real, dto.P12);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP13, SqlDbType.Real, dto.P13);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP14, SqlDbType.Real, dto.P14);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP15, SqlDbType.Real, dto.P15);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP16, SqlDbType.Real, dto.P16);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP17, SqlDbType.Real, dto.P17);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP18, SqlDbType.Real, dto.P18);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP19, SqlDbType.Real, dto.P19);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataP20, SqlDbType.Real, dto.P20);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataUnitsSoldPerDay, SqlDbType.Real, dto.UnitsSoldPerDay);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedCasePacks, SqlDbType.Real, dto.AchievedCasePacks);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedDos, SqlDbType.Real, dto.AchievedDos);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedShelfLife, SqlDbType.Real, dto.AchievedShelfLife);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataAchievedDeliveries, SqlDbType.Real, dto.AchievedDeliveries);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMinimumInventoryUnits, SqlDbType.Real, dto.MinimumInventoryUnits);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP1Percentage, SqlDbType.Real, dto.MetaP1Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP2Percentage, SqlDbType.Real, dto.MetaP2Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP3Percentage, SqlDbType.Real, dto.MetaP3Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP4Percentage, SqlDbType.Real, dto.MetaP4Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP5Percentage, SqlDbType.Real, dto.MetaP5Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP6Percentage, SqlDbType.Real, dto.MetaP6Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP7Percentage, SqlDbType.Real, dto.MetaP7Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP8Percentage, SqlDbType.Real, dto.MetaP8Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP9Percentage, SqlDbType.Real, dto.MetaP9Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP10Percentage, SqlDbType.Real, dto.MetaP10Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP11Percentage, SqlDbType.Real, dto.MetaP11Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP12Percentage, SqlDbType.Real, dto.MetaP12Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP13Percentage, SqlDbType.Real, dto.MetaP13Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP14Percentage, SqlDbType.Real, dto.MetaP14Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP15Percentage, SqlDbType.Real, dto.MetaP15Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP16Percentage, SqlDbType.Real, dto.MetaP16Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP17Percentage, SqlDbType.Real, dto.MetaP17Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP18Percentage, SqlDbType.Real, dto.MetaP18Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP19Percentage, SqlDbType.Real, dto.MetaP19Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP20Percentage, SqlDbType.Real, dto.MetaP20Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP1Rank, SqlDbType.Int, dto.MetaP1Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP2Rank, SqlDbType.Int, dto.MetaP2Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP3Rank, SqlDbType.Int, dto.MetaP3Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP4Rank, SqlDbType.Int, dto.MetaP4Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP5Rank, SqlDbType.Int, dto.MetaP5Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP6Rank, SqlDbType.Int, dto.MetaP6Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP7Rank, SqlDbType.Int, dto.MetaP7Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP8Rank, SqlDbType.Int, dto.MetaP8Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP9Rank, SqlDbType.Int, dto.MetaP9Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP10Rank, SqlDbType.Int, dto.MetaP10Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP11Rank, SqlDbType.Int, dto.MetaP11Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP12Rank, SqlDbType.Int, dto.MetaP12Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP13Rank, SqlDbType.Int, dto.MetaP13Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP14Rank, SqlDbType.Int, dto.MetaP14Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP15Rank, SqlDbType.Int, dto.MetaP15Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP16Rank, SqlDbType.Int, dto.MetaP16Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP17Rank, SqlDbType.Int, dto.MetaP17Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP18Rank, SqlDbType.Int, dto.MetaP18Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP19Rank, SqlDbType.Int, dto.MetaP19Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaP20Rank, SqlDbType.Int, dto.MetaP20Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP1, SqlDbType.Real, dto.CP1);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP2, SqlDbType.Real, dto.CP2);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP3, SqlDbType.Real, dto.CP3);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP4, SqlDbType.Real, dto.CP4);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP5, SqlDbType.Real, dto.CP5);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP6, SqlDbType.Real, dto.CP6);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP7, SqlDbType.Real, dto.CP7);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP8, SqlDbType.Real, dto.CP8);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP9, SqlDbType.Real, dto.CP9);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataCP10, SqlDbType.Real, dto.CP10);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP1Percentage, SqlDbType.Real, dto.MetaCP1Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP2Percentage, SqlDbType.Real, dto.MetaCP2Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP3Percentage, SqlDbType.Real, dto.MetaCP3Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP4Percentage, SqlDbType.Real, dto.MetaCP4Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP5Percentage, SqlDbType.Real, dto.MetaCP5Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP6Percentage, SqlDbType.Real, dto.MetaCP6Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP7Percentage, SqlDbType.Real, dto.MetaCP7Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP8Percentage, SqlDbType.Real, dto.MetaCP8Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP9Percentage, SqlDbType.Real, dto.MetaCP9Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP10Percentage, SqlDbType.Real, dto.MetaCP10Percentage);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP1Rank, SqlDbType.Int, dto.MetaCP1Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP2Rank, SqlDbType.Int, dto.MetaCP2Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP3Rank, SqlDbType.Int, dto.MetaCP3Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP4Rank, SqlDbType.Int, dto.MetaCP4Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP5Rank, SqlDbType.Int, dto.MetaCP5Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP6Rank, SqlDbType.Int, dto.MetaCP6Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP7Rank, SqlDbType.Int, dto.MetaCP7Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP8Rank, SqlDbType.Int, dto.MetaCP8Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP9Rank, SqlDbType.Int, dto.MetaCP9Rank);
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataMetaCP10Rank, SqlDbType.Int, dto.MetaCP10Rank);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramPerformanceDataBulkCopyDataReader dr = new PlanogramPerformanceDataBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceDataBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPerformanceDataDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramPerformanceDataId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramPerformanceDataDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            foreach (PlanogramPerformanceDataDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
