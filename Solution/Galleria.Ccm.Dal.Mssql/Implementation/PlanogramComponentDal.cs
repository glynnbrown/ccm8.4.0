﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version.
// V8-25787 : N.Foster
//  Added IsCarPark
// V8-27058 : A.Probyn
//  Removed MetaNumberOfShelfEdgeLabels
// V8-27468 : L.Ineson
//  Added IsMerchandisedTopDown
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: (CCM 8.2)
// V8-30873 : L.Ineson
//  Removed IsCarPark.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// The MSSQL DAL for the PlanogramComponent DTO.
    /// </summary>
    public class PlanogramComponentDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramComponentDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogramComponent";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
	            "[PlanogramComponent_Id] [int] NOT NULL, " +
	            "[Planogram_Id] [int] NOT NULL, " +
	            "[PlanogramComponent_Mesh3DId] [int] NULL, " +
	            "[PlanogramComponent_ImageIdFront] [int] NULL, " +
	            "[PlanogramComponent_ImageIdBack] [int] NULL, " +
	            "[PlanogramComponent_ImageIdTop] [int] NULL, " +
	            "[PlanogramComponent_ImageIdBottom] [int] NULL, " +
	            "[PlanogramComponent_ImageIdLeft] [int] NULL, " +
	            "[PlanogramComponent_ImageIdRight] [int] NULL, " +
                "[PlanogramComponent_Name] [nvarchar](50) COLLATE database_default NOT NULL, " +
	            "[PlanogramComponent_Height] [real] NOT NULL, " +
	            "[PlanogramComponent_Width] [real] NOT NULL, " +
	            "[PlanogramComponent_Depth] [real] NOT NULL, " +
	            "[PlanogramComponent_MetaNumberOfSubComponents] [smallint] NULL, " +
	            "[PlanogramComponent_MetaNumberOfMerchandisedSubComponents] [smallint] NULL, " +
	            "[PlanogramComponent_IsMoveable] [bit] NOT NULL, " +
	            "[PlanogramComponent_IsDisplayOnly] [bit] NOT NULL, " +
	            "[PlanogramComponent_CanAttachShelfEdgeLabel] [bit] NOT NULL, " +
                "[PlanogramComponent_RetailerReferenceCode] [nvarchar](50) COLLATE database_default NULL, " +
	            "[PlanogramComponent_BarCode] [nvarchar](13) NULL, " +
                "[PlanogramComponent_Manufacturer] [nvarchar](50) COLLATE database_default NULL, " +
                "[PlanogramComponent_ManufacturerPartName] [nvarchar](50) COLLATE database_default NULL, " +
                "[PlanogramComponent_ManufacturerPartNumber] [nvarchar](50) COLLATE database_default NULL, " +
                "[PlanogramComponent_SupplierName] [nvarchar](50) COLLATE database_default NULL, " +
                "[PlanogramComponent_SupplierPartNumber] [nvarchar](50) COLLATE database_default NULL, " +
	            "[PlanogramComponent_SupplierCostPrice] [real] NULL, " +
	            "[PlanogramComponent_SupplierDiscount] [real] NULL, " +
	            "[PlanogramComponent_SupplierLeadTime] [real] NULL, " +
	            "[PlanogramComponent_MinPurchaseQty] [int] NULL, " +
	            "[PlanogramComponent_WeightLimit] [real] NULL, " +
	            "[PlanogramComponent_Weight] [real] NULL, " +
	            "[PlanogramComponent_Volume] [real] NULL, " +
	            "[PlanogramComponent_Diameter] [real] NULL, " +
	            "[PlanogramComponent_Capacity] [smallint] NULL, " +
	            "[PlanogramComponent_ComponentType] [tinyint] NOT NULL, " +
                "[PlanogramComponent_IsMerchandisedTopDown] [bit] NOT NULL, " +
            ")";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramComponentBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<PlanogramComponentDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramComponentBulkCopyDataReader(IEnumerable<PlanogramComponentDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 36; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.PlanogramId;
                        break;
                    case 2:
                        value = _enumerator.Current.Mesh3DId;
                        break;
                    case 3:
                        value = _enumerator.Current.ImageIdFront;
                        break;
                    case 4:
                        value = _enumerator.Current.ImageIdBack;
                        break;
                    case 5:
                        value = _enumerator.Current.ImageIdTop;
                        break;
                    case 6:
                        value = _enumerator.Current.ImageIdBottom;
                        break;
                    case 7:
                        value = _enumerator.Current.ImageIdLeft;
                        break;
                    case 8:
                        value = _enumerator.Current.ImageIdRight;
                        break;
                    case 9:
                        value = _enumerator.Current.Name;
                        break;
                    case 10:
                        value = _enumerator.Current.Height;
                        break;
                    case 11:
                        value = _enumerator.Current.Width;
                        break;
                    case 12:
                        value = _enumerator.Current.Depth;
                        break;
                    case 13:
                        value = _enumerator.Current.MetaNumberOfSubComponents;
                        break;
                    case 14:
                        value = _enumerator.Current.MetaNumberOfMerchandisedSubComponents;
                        break;
                    case 15:
                        value = _enumerator.Current.IsMoveable;
                        break;
                    case 16:
                        value = _enumerator.Current.IsDisplayOnly;
                        break;
                    case 17:
                        value = _enumerator.Current.CanAttachShelfEdgeLabel;
                        break;
                    case 18:
                        value = _enumerator.Current.RetailerReferenceCode;
                        break;
                    case 19:
                        value = _enumerator.Current.BarCode;
                        break;
                    case 20:
                        value = _enumerator.Current.Manufacturer;
                        break;
                    case 21:
                        value = _enumerator.Current.ManufacturerPartName;
                        break;
                    case 22:
                        value = _enumerator.Current.ManufacturerPartNumber;
                        break;
                    case 23:
                        value = _enumerator.Current.SupplierName;
                        break;
                    case 24:
                        value = _enumerator.Current.SupplierPartNumber;
                        break;
                    case 25:
                        value = _enumerator.Current.SupplierCostPrice;
                        break;
                    case 26:
                        value = _enumerator.Current.SupplierDiscount;
                        break;
                    case 27:
                        value = _enumerator.Current.SupplierLeadTime;
                        break;
                    case 28:
                        value = _enumerator.Current.MinPurchaseQty;
                        break;
                    case 29:
                        value = _enumerator.Current.WeightLimit;
                        break;
                    case 30:
                        value = _enumerator.Current.Weight;
                        break;
                    case 31:
                        value = _enumerator.Current.Volume;
                        break;
                    case 32:
                        value = _enumerator.Current.Diameter;
                        break;
                    case 33:
                        value = _enumerator.Current.Capacity;
                        break;
                    case 34:
                        value = _enumerator.Current.ComponentType;
                        break;
                    case 35:
                        value = _enumerator.Current.IsMerchandisedTopDown;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramComponent DTO from the data reader.
        /// </summary>
        /// <param name="dr">The data reader to load data from.</param>
        /// <returns>A new PlanogramComponent DTO.</returns>
        public PlanogramComponentDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramComponentDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramComponentId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramComponentPlanogramId]),
                Mesh3DId = (Int32?)GetValue(dr[FieldNames.PlanogramComponentMesh3DId]),
                ImageIdFront = (Int32?)GetValue(dr[FieldNames.PlanogramComponentImageIdFront]),
                ImageIdBack = (Int32?)GetValue(dr[FieldNames.PlanogramComponentImageIdBack]),
                ImageIdTop = (Int32?)GetValue(dr[FieldNames.PlanogramComponentImageIdTop]),
                ImageIdBottom = (Int32?)GetValue(dr[FieldNames.PlanogramComponentImageIdBottom]),
                ImageIdLeft = (Int32?)GetValue(dr[FieldNames.PlanogramComponentImageIdLeft]),
                ImageIdRight = (Int32?)GetValue(dr[FieldNames.PlanogramComponentImageIdRight]),
                Name = (String)GetValue(dr[FieldNames.PlanogramComponentName]),
                Height = (Single)GetValue(dr[FieldNames.PlanogramComponentHeight]),
                Width = (Single)GetValue(dr[FieldNames.PlanogramComponentWidth]),
                Depth = (Single)GetValue(dr[FieldNames.PlanogramComponentDepth]),
                MetaNumberOfSubComponents = (Int16?)GetValue(dr[FieldNames.PlanogramComponentMetaNumberOfSubComponents]),
                MetaNumberOfMerchandisedSubComponents = (Int16?)GetValue(dr[FieldNames.PlanogramComponentMetaNumberOfMerchandisedSubComponents]),
                IsMoveable = (Boolean)GetValue(dr[FieldNames.PlanogramComponentIsMoveable]),
                IsDisplayOnly = (Boolean)GetValue(dr[FieldNames.PlanogramComponentIsDisplayOnly]),
                CanAttachShelfEdgeLabel = (Boolean)GetValue(dr[FieldNames.PlanogramComponentCanAttachShelfEdgeLabel]),
                RetailerReferenceCode = (String)GetValue(dr[FieldNames.PlanogramComponentRetailerReferenceCode]),
                BarCode = (String)GetValue(dr[FieldNames.PlanogramComponentBarCode]),
                Manufacturer = (String)GetValue(dr[FieldNames.PlanogramComponentManufacturer]),
                ManufacturerPartName = (String)GetValue(dr[FieldNames.PlanogramComponentManufacturerPartName]),
                ManufacturerPartNumber = (String)GetValue(dr[FieldNames.PlanogramComponentManufacturerPartNumber]),
                SupplierName = (String)GetValue(dr[FieldNames.PlanogramComponentSupplierName]),
                SupplierPartNumber = (String)GetValue(dr[FieldNames.PlanogramComponentSupplierPartNumber]),
                SupplierCostPrice = (Single?)GetValue(dr[FieldNames.PlanogramComponentSupplierCostPrice]),
                SupplierDiscount = (Single?)GetValue(dr[FieldNames.PlanogramComponentSupplierDiscount]),
                SupplierLeadTime = (Single?)GetValue(dr[FieldNames.PlanogramComponentSupplierLeadTime]),
                MinPurchaseQty = (Int32?)GetValue(dr[FieldNames.PlanogramComponentMinPurchaseQty]),
                WeightLimit = (Single?)GetValue(dr[FieldNames.PlanogramComponentWeightLimit]),
                Weight = (Single?)GetValue(dr[FieldNames.PlanogramComponentWeight]),
                Volume = (Single?)GetValue(dr[FieldNames.PlanogramComponentVolume]),
                Diameter = (Single?)GetValue(dr[FieldNames.PlanogramComponentDiameter]),
                Capacity = (Int16?)GetValue(dr[FieldNames.PlanogramComponentCapacity]),
                ComponentType = (Byte)GetValue(dr[FieldNames.PlanogramComponentComponentType]),
                IsMerchandisedTopDown = (Boolean)GetValue(dr[FieldNames.PlanogramComponentIsMerchandisedTopDown]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramComponent DTOs that have a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A list of PlanogramComponent DTOs.</returns>
        public IEnumerable<PlanogramComponentDto> FetchByPlanogramId(object planogramId)
        {
            var dtoList = new List<PlanogramComponentDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramComponentFetchByPlanogramId))
                {
                    CreateParameter(
                        command,
                        FieldNames.PlanogramComponentPlanogramId,
                        SqlDbType.Int,
                        (Int32)planogramId);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramComponentDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramComponentInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramComponentId, SqlDbType.Int);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramComponentDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, PlanogramComponentDto> index = new Dictionary<Object, PlanogramComponentDto>();
            foreach (PlanogramComponentDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramComponentBulkCopyDataReader dr = new PlanogramComponentBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComponentBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The PlanogramComponent DTO to update.</param>
        public void Update(PlanogramComponentDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramComponentUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramComponentId, SqlDbType.Int, dto.Id);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified dtos
        /// </summary>
        public void Update(IEnumerable<PlanogramComponentDto> dtos)
        {
            foreach (PlanogramComponentDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a PlanogramComponent by Id.
        /// </summary>
        /// <param name="id">The Id of the PlanogramComponent to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramComponentDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramComponentId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramComponentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramComponentDto> dtos)
        {
            foreach (PlanogramComponentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Adds parameters to the command for all DTO properties, apart from Id.
        /// </summary>
        /// <param name="dto">The DTo whose properties should be added.</param>
        /// <param name="command">The command to add parameters to.</param>
        private void CreateAllParametersButId(PlanogramComponentDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramComponentPlanogramId, SqlDbType.Int, dto.PlanogramId);
            CreateParameter(command, FieldNames.PlanogramComponentMesh3DId, SqlDbType.Int, dto.Mesh3DId);
            CreateParameter(command, FieldNames.PlanogramComponentImageIdFront, SqlDbType.Int, dto.ImageIdFront);
            CreateParameter(command, FieldNames.PlanogramComponentImageIdBack, SqlDbType.Int, dto.ImageIdBack);
            CreateParameter(command, FieldNames.PlanogramComponentImageIdTop, SqlDbType.Int, dto.ImageIdTop);
            CreateParameter(command, FieldNames.PlanogramComponentImageIdBottom, SqlDbType.Int, dto.ImageIdBottom);
            CreateParameter(command, FieldNames.PlanogramComponentImageIdLeft, SqlDbType.Int, dto.ImageIdLeft);
            CreateParameter(command, FieldNames.PlanogramComponentImageIdRight, SqlDbType.Int, dto.ImageIdRight);
            CreateParameter(command, FieldNames.PlanogramComponentName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramComponentHeight, SqlDbType.Real, dto.Height);
            CreateParameter(command, FieldNames.PlanogramComponentWidth, SqlDbType.Real, dto.Width);
            CreateParameter(command, FieldNames.PlanogramComponentDepth, SqlDbType.Real, dto.Depth);
            CreateParameter(command, FieldNames.PlanogramComponentMetaNumberOfSubComponents, SqlDbType.SmallInt, dto.MetaNumberOfSubComponents);
            CreateParameter(command, FieldNames.PlanogramComponentMetaNumberOfMerchandisedSubComponents, SqlDbType.SmallInt, dto.MetaNumberOfMerchandisedSubComponents);
            CreateParameter(command, FieldNames.PlanogramComponentIsMoveable, SqlDbType.Bit, dto.IsMoveable);
            CreateParameter(command, FieldNames.PlanogramComponentIsDisplayOnly, SqlDbType.Bit, dto.IsDisplayOnly);
            CreateParameter(command, FieldNames.PlanogramComponentCanAttachShelfEdgeLabel, SqlDbType.Bit, dto.CanAttachShelfEdgeLabel);
            CreateParameter(command, FieldNames.PlanogramComponentRetailerReferenceCode, SqlDbType.NVarChar, dto.RetailerReferenceCode);
            CreateParameter(command, FieldNames.PlanogramComponentBarCode, SqlDbType.NVarChar, dto.BarCode);
            CreateParameter(command, FieldNames.PlanogramComponentManufacturer, SqlDbType.NVarChar, dto.Manufacturer);
            CreateParameter(command, FieldNames.PlanogramComponentManufacturerPartName, SqlDbType.NVarChar, dto.ManufacturerPartName);
            CreateParameter(command, FieldNames.PlanogramComponentManufacturerPartNumber, SqlDbType.NVarChar, dto.ManufacturerPartNumber);
            CreateParameter(command, FieldNames.PlanogramComponentSupplierName, SqlDbType.NVarChar, dto.SupplierName);
            CreateParameter(command, FieldNames.PlanogramComponentSupplierPartNumber, SqlDbType.NVarChar, dto.SupplierPartNumber);
            CreateParameter(command, FieldNames.PlanogramComponentSupplierCostPrice, SqlDbType.Real, dto.SupplierCostPrice);
            CreateParameter(command, FieldNames.PlanogramComponentSupplierDiscount, SqlDbType.Real, dto.SupplierDiscount);
            CreateParameter(command, FieldNames.PlanogramComponentSupplierLeadTime, SqlDbType.Real, dto.SupplierLeadTime);
            CreateParameter(command, FieldNames.PlanogramComponentMinPurchaseQty, SqlDbType.Int, dto.MinPurchaseQty);
            CreateParameter(command, FieldNames.PlanogramComponentWeightLimit, SqlDbType.Real, dto.WeightLimit);
            CreateParameter(command, FieldNames.PlanogramComponentWeight, SqlDbType.Real, dto.Weight);
            CreateParameter(command, FieldNames.PlanogramComponentVolume, SqlDbType.Real, dto.Volume);
            CreateParameter(command, FieldNames.PlanogramComponentDiameter, SqlDbType.Real, dto.Diameter);
            CreateParameter(command, FieldNames.PlanogramComponentCapacity, SqlDbType.SmallInt, dto.Capacity);
            CreateParameter(command, FieldNames.PlanogramComponentComponentType, SqlDbType.TinyInt, dto.ComponentType);
            CreateParameter(command, FieldNames.PlanogramComponentIsMerchandisedTopDown, SqlDbType.Bit, dto.IsMerchandisedTopDown);
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}
