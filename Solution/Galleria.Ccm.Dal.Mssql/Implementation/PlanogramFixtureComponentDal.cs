﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS.
// V8-25881 : A.Probyn
//  Added MetaData properties    
// V8-27058 : A.Probyn
//  Updated MetaData properties 
// V8-27474 : A.Silva
//  Added ComponentSequenceNumber field.
#endregion

#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion

#region Version History: CCM802
// V8-28811 : L.Luong
//  Added MetaPercentageLinearSpaceFilled
#endregion

#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added NotchNumber property to allow saving to the database
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
#endregion

#region Version History: (CCM 8.3)
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramFixtureComponentDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramFixtureComponentDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogramFixtureComponent";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[PlanogramFixtureComponent_Id] [int] NOT NULL, " +
                "[PlanogramFixture_Id] [int] NOT NULL, " +
                "[PlanogramComponent_Id] [int] NOT NULL, " +
                "[PlanogramFixtureComponent_X] [real] NOT NULL, " +
                "[PlanogramFixtureComponent_Y] [real] NOT NULL, " +
                "[PlanogramFixtureComponent_Z] [real] NOT NULL, " +
                "[PlanogramFixtureComponent_Slope] [real] NOT NULL, " +
                "[PlanogramFixtureComponent_Angle] [real] NOT NULL, " +
                "[PlanogramFixtureComponent_Roll] [real] NOT NULL, " +
                "[PlanogramFixtureComponent_MetaTotalMerchandisableLinearSpace] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalMerchandisableAreaSpace] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalMerchandisableVolumetricSpace] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalLinearWhiteSpace] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalAreaWhiteSpace] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalVolumetricWhiteSpace] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaProductsPlaced] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaNewProducts] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaChangesFromPreviousCount] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaChangeFromPreviousStarRating] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaBlocksDropped] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaNotAchievedInventory] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalFacings] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaAverageFacings] [smallint] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalUnits] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaAverageUnits] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaMinDos] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaMaxDos] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaAverageDos] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaMinCases] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaAverageCases] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaMaxCases] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaSpaceToUnitsIndex] [smallint] NULL, " +
                "[PlanogramFixtureComponent_MetaIsOverMerchandisedWidth] [bit] NULL, " +
                "[PlanogramFixtureComponent_MetaIsOverMerchandisedHeight] [bit] NULL, " +
                "[PlanogramFixtureComponent_MetaIsOverMerchandisedDepth] [bit] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalComponentCollisions] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalPositionCollisions] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaTotalFrontFacings] [smallint] NULL, " +
                "[PlanogramFixtureComponent_MetaAverageFrontFacings] [real] NULL, " +
                "[PlanogramFixtureComponent_ComponentSequenceNumber] [smallint] NULL, " +
                "[PlanogramFixtureComponent_MetaPercentageLinearSpaceFilled] [real] NULL, " +
                "[PlanogramFixtureComponent_NotchNumber] [int] NULL, " +
                "[PlanogramFixtureComponent_MetaWorldX] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaWorldY] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaWorldZ] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaWorldAngle] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaWorldSlope] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaWorldRoll] [real] NULL, " +
                "[PlanogramFixtureComponent_MetaIsComponentSlopeWithNoRiser] [bit], " +
                "[PlanogramFixtureComponent_MetaIsOutsideOfFixtureArea] [bit] NULL" +
            ")";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramFixtureComponentBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<PlanogramFixtureComponentDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramFixtureComponentBulkCopyDataReader(IEnumerable<PlanogramFixtureComponentDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 49; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.PlanogramFixtureId;
                        break;
                    case 2:
                        value = _enumerator.Current.PlanogramComponentId;
                        break;
                    case 3:
                        value = _enumerator.Current.X;
                        break;
                    case 4:
                        value = _enumerator.Current.Y;
                        break;
                    case 5:
                        value = _enumerator.Current.Z;
                        break;
                    case 6:
                        value = _enumerator.Current.Slope;
                        break;
                    case 7:
                        value = _enumerator.Current.Angle;
                        break;
                    case 8:
                        value = _enumerator.Current.Roll;
                        break;
                    case 9:
                        value = _enumerator.Current.MetaTotalMerchandisableLinearSpace;
                        break;
                    case 10:
                        value = _enumerator.Current.MetaTotalMerchandisableAreaSpace;
                        break;
                    case 11:
                        value = _enumerator.Current.MetaTotalMerchandisableVolumetricSpace;
                        break;
                    case 12:
                        value = _enumerator.Current.MetaTotalLinearWhiteSpace;
                        break;
                    case 13:
                        value = _enumerator.Current.MetaTotalAreaWhiteSpace;
                        break;
                    case 14:
                        value = _enumerator.Current.MetaTotalVolumetricWhiteSpace;
                        break;
                    case 15:
                        value = _enumerator.Current.MetaProductsPlaced;
                        break;
                    case 16:
                        value = _enumerator.Current.MetaNewProducts;
                        break;
                    case 17:
                        value = _enumerator.Current.MetaChangesFromPreviousCount;
                        break;
                    case 18:
                        value = _enumerator.Current.MetaChangeFromPreviousStarRating;
                        break;
                    case 19:
                        value = _enumerator.Current.MetaBlocksDropped;
                        break;
                    case 20:
                        value = _enumerator.Current.MetaNotAchievedInventory;
                        break;
                    case 21:
                        value = _enumerator.Current.MetaTotalFacings;
                        break;
                    case 22:
                        value = _enumerator.Current.MetaAverageFacings;
                        break;
                    case 23:
                        value = _enumerator.Current.MetaTotalUnits;
                        break;
                    case 24:
                        value = _enumerator.Current.MetaAverageUnits;
                        break;
                    case 25:
                        value = _enumerator.Current.MetaMinDos;
                        break;
                    case 26:
                        value = _enumerator.Current.MetaMaxDos;
                        break;
                    case 27:
                        value = _enumerator.Current.MetaAverageDos;
                        break;
                    case 28:
                        value = _enumerator.Current.MetaMinCases;
                        break;
                    case 29:
                        value = _enumerator.Current.MetaAverageCases;
                        break;
                    case 30:
                        value = _enumerator.Current.MetaMaxCases;
                        break;
                    case 31:
                        value = _enumerator.Current.MetaSpaceToUnitsIndex;
                        break;
                    case 32:
                        value = _enumerator.Current.MetaIsOverMerchandisedWidth;
                        break;
                    case 33:
                        value = _enumerator.Current.MetaIsOverMerchandisedHeight;
                        break;
                    case 34:
                        value = _enumerator.Current.MetaIsOverMerchandisedDepth;
                        break;
                    case 35:
                        value = _enumerator.Current.MetaTotalComponentCollisions;
                        break;
                    case 36:
                        value = _enumerator.Current.MetaTotalPositionCollisions;
                        break;
                    case 37:
                        value = _enumerator.Current.MetaTotalFrontFacings;
                        break;
                    case 38:
                        value = _enumerator.Current.MetaAverageFrontFacings;
                        break;
                    case 39:
                        value = _enumerator.Current.ComponentSequenceNumber;
                        break;
                    case 40:
                        value = _enumerator.Current.MetaPercentageLinearSpaceFilled;
                        break;
                    case 41:
                        value = _enumerator.Current.NotchNumber;
                        break;
                    case 42:
                        value = _enumerator.Current.MetaWorldX;
                        break;
                    case 43:
                        value = _enumerator.Current.MetaWorldY;
                        break;
                    case 44:
                        value = _enumerator.Current.MetaWorldZ;
                        break;
                    case 45:
                        value = _enumerator.Current.MetaWorldAngle;
                        break;
                    case 46:
                        value = _enumerator.Current.MetaWorldSlope;
                        break;
                    case 47:
                        value = _enumerator.Current.MetaWorldRoll;
                        break;
                    case 48:
                        value = _enumerator.Current.MetaIsComponentSlopeWithNoRiser;
                        break;
                    case 49:
                        value = _enumerator.Current.MetaIsOutsideOfFixtureArea;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramFixtureComponentDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramFixtureComponentDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramFixtureComponentId]),
                PlanogramFixtureId = (Object)GetValue(dr[FieldNames.PlanogramFixtureComponentPlanogramFixtureId]),
                PlanogramComponentId = (Object)GetValue(dr[FieldNames.PlanogramFixtureComponentPlanogramComponentId]),
                ComponentSequenceNumber = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureComponentComponentSequenceNumber]),
                X = (Single)GetValue(dr[FieldNames.PlanogramFixtureComponentX]),
                Y = (Single)GetValue(dr[FieldNames.PlanogramFixtureComponentY]),
                Z = (Single)GetValue(dr[FieldNames.PlanogramFixtureComponentZ]),
                Slope = (Single)GetValue(dr[FieldNames.PlanogramFixtureComponentSlope]),
                Angle = (Single)GetValue(dr[FieldNames.PlanogramFixtureComponentAngle]),
                Roll = (Single)GetValue(dr[FieldNames.PlanogramFixtureComponentRoll]),
                MetaTotalMerchandisableLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace]),
                MetaTotalMerchandisableAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace]),
                MetaTotalMerchandisableVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace]),
                MetaTotalLinearWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalLinearWhiteSpace]),
                MetaTotalAreaWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalAreaWhiteSpace]),
                MetaTotalVolumetricWhiteSpace = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace]),
                MetaProductsPlaced = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaProductsPlaced]),
                MetaNewProducts = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaNewProducts]),
                MetaChangesFromPreviousCount = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaChangesFromPreviousCount]),
                MetaChangeFromPreviousStarRating = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaChangeFromPreviousStarRating]),
                MetaBlocksDropped = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaBlocksDropped]),
                MetaNotAchievedInventory = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaNotAchievedInventory]),
                MetaTotalFacings = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalFacings]),
                MetaAverageFacings = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaAverageFacings]),
                MetaTotalUnits = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalUnits]),
                MetaAverageUnits = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaAverageUnits]),
                MetaMinDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaMinDos]),
                MetaMaxDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaMaxDos]),
                MetaAverageDos = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaAverageDos]),
                MetaMinCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaMinCases]),
                MetaAverageCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaAverageCases]),
                MetaMaxCases = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaMaxCases]),
                MetaSpaceToUnitsIndex = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaSpaceToUnitsIndex]),
                MetaIsOverMerchandisedWidth = (Boolean?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedWidth]),
                MetaIsOverMerchandisedHeight = (Boolean?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedHeight]),
                MetaIsOverMerchandisedDepth = (Boolean?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedDepth]),
                MetaTotalComponentCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalComponentCollisions]),
                MetaTotalPositionCollisions = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalPositionCollisions]),
                MetaTotalFrontFacings = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaTotalFrontFacings]),
                MetaAverageFrontFacings = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaAverageFrontFacings]),
                MetaPercentageLinearSpaceFilled = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaPercentageLinearSpaceFilled]),
                NotchNumber = (Int32?)GetValue(dr[FieldNames.PlanogramFixtureComponentNotchNumber]),
                MetaWorldX = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaWorldX]),
                MetaWorldY = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaWorldY]),
                MetaWorldZ = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaWorldZ]),
                MetaWorldAngle = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaWorldAngle]),
                MetaWorldSlope = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaWorldSlope]),
                MetaWorldRoll = (Single?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaWorldRoll]),
                MetaIsOutsideOfFixtureArea = (Boolean?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaIsOutsideOfFixtureArea]),
                MetaIsComponentSlopeWithNoRiser = (Boolean?)GetValue(dr[FieldNames.PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser]),
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramFixtureComponentDto> FetchByPlanogramFixtureId(Object planogramFixtureId)
        {
            List<PlanogramFixtureComponentDto> dtoList = new List<PlanogramFixtureComponentDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureComponentFetchByPlanogramFixtureId))
                {
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentPlanogramFixtureId, SqlDbType.Int, planogramFixtureId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramFixtureComponentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureComponentInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramFixtureComponentId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentPlanogramFixtureId, SqlDbType.Int, dto.PlanogramFixtureId);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentPlanogramComponentId, SqlDbType.Int, dto.PlanogramComponentId);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentComponentSequenceNumber, SqlDbType.SmallInt, dto.ComponentSequenceNumber);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentZ, SqlDbType.Real, dto.Z);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentSlope, SqlDbType.Real, dto.Slope);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentAngle, SqlDbType.Real, dto.Angle);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentRoll, SqlDbType.Real, dto.Roll);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace, SqlDbType.Real, dto.MetaTotalMerchandisableLinearSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace, SqlDbType.Real, dto.MetaTotalMerchandisableAreaSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace, SqlDbType.Real, dto.MetaTotalMerchandisableVolumetricSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalLinearWhiteSpace, SqlDbType.Real, dto.MetaTotalLinearWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalAreaWhiteSpace, SqlDbType.Real, dto.MetaTotalAreaWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace, SqlDbType.Real, dto.MetaTotalVolumetricWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaProductsPlaced, SqlDbType.Int, dto.MetaProductsPlaced);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaNewProducts, SqlDbType.Int, dto.MetaNewProducts);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaChangesFromPreviousCount, SqlDbType.Int, dto.MetaChangesFromPreviousCount);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaChangeFromPreviousStarRating, SqlDbType.Int, dto.MetaChangeFromPreviousStarRating);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaBlocksDropped, SqlDbType.Int, dto.MetaBlocksDropped);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaNotAchievedInventory, SqlDbType.Int, dto.MetaNotAchievedInventory);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalFacings, SqlDbType.Int, dto.MetaTotalFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageFacings, SqlDbType.SmallInt, dto.MetaAverageFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalUnits, SqlDbType.Int, dto.MetaTotalUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageUnits, SqlDbType.Int, dto.MetaAverageUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMinDos, SqlDbType.Real, dto.MetaMinDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMaxDos, SqlDbType.Real, dto.MetaMaxDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageDos, SqlDbType.Real, dto.MetaAverageDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMinCases, SqlDbType.Real, dto.MetaMinCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageCases, SqlDbType.Real, dto.MetaAverageCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMaxCases, SqlDbType.Real, dto.MetaMaxCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaSpaceToUnitsIndex, SqlDbType.SmallInt, dto.MetaSpaceToUnitsIndex);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedWidth, SqlDbType.Bit, dto.MetaIsOverMerchandisedWidth);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedHeight, SqlDbType.Bit, dto.MetaIsOverMerchandisedHeight);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedDepth, SqlDbType.Bit, dto.MetaIsOverMerchandisedDepth);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalComponentCollisions, SqlDbType.Int, dto.MetaTotalComponentCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalPositionCollisions, SqlDbType.Int, dto.MetaTotalPositionCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalFrontFacings, SqlDbType.SmallInt, dto.MetaTotalFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageFrontFacings, SqlDbType.Real, dto.MetaAverageFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaPercentageLinearSpaceFilled, SqlDbType.Real, dto.MetaPercentageLinearSpaceFilled);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentNotchNumber, SqlDbType.Int, dto.NotchNumber);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldX, SqlDbType.Real, dto.MetaWorldX);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldY, SqlDbType.Real, dto.MetaWorldY);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldZ, SqlDbType.Real, dto.MetaWorldZ);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldAngle, SqlDbType.Real, dto.MetaWorldAngle);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldSlope, SqlDbType.Real, dto.MetaWorldSlope);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldRoll, SqlDbType.Real, dto.MetaWorldRoll);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOutsideOfFixtureArea, SqlDbType.Bit, dto.MetaIsOutsideOfFixtureArea);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser, SqlDbType.Bit, dto.MetaIsComponentSlopeWithNoRiser);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, PlanogramFixtureComponentDto> index = new Dictionary<Object, PlanogramFixtureComponentDto>();
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramFixtureComponentBulkCopyDataReader dr = new PlanogramFixtureComponentBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureComponentBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramFixtureComponentDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureComponentUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentPlanogramFixtureId, SqlDbType.Int, dto.PlanogramFixtureId);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentPlanogramComponentId, SqlDbType.Int, dto.PlanogramComponentId);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentComponentSequenceNumber, SqlDbType.SmallInt, dto.ComponentSequenceNumber);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentX, SqlDbType.Real, dto.X);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentY, SqlDbType.Real, dto.Y);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentZ, SqlDbType.Real, dto.Z);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentSlope, SqlDbType.Real, dto.Slope);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentAngle, SqlDbType.Real, dto.Angle);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentRoll, SqlDbType.Real, dto.Roll);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace, SqlDbType.Real, dto.MetaTotalMerchandisableLinearSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace, SqlDbType.Real, dto.MetaTotalMerchandisableAreaSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace, SqlDbType.Real, dto.MetaTotalMerchandisableVolumetricSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalLinearWhiteSpace, SqlDbType.Real, dto.MetaTotalLinearWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalAreaWhiteSpace, SqlDbType.Real, dto.MetaTotalAreaWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace, SqlDbType.Real, dto.MetaTotalVolumetricWhiteSpace);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaProductsPlaced, SqlDbType.Int, dto.MetaProductsPlaced);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaNewProducts, SqlDbType.Int, dto.MetaNewProducts);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaChangesFromPreviousCount, SqlDbType.Int, dto.MetaChangesFromPreviousCount);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaChangeFromPreviousStarRating, SqlDbType.Int, dto.MetaChangeFromPreviousStarRating);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaBlocksDropped, SqlDbType.Int, dto.MetaBlocksDropped);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaNotAchievedInventory, SqlDbType.Int, dto.MetaNotAchievedInventory);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalFacings, SqlDbType.Int, dto.MetaTotalFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageFacings, SqlDbType.SmallInt, dto.MetaAverageFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalUnits, SqlDbType.Int, dto.MetaTotalUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageUnits, SqlDbType.Int, dto.MetaAverageUnits);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMinDos, SqlDbType.Real, dto.MetaMinDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMaxDos, SqlDbType.Real, dto.MetaMaxDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageDos, SqlDbType.Real, dto.MetaAverageDos);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMinCases, SqlDbType.Real, dto.MetaMinCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageCases, SqlDbType.Real, dto.MetaAverageCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaMaxCases, SqlDbType.Real, dto.MetaMaxCases);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaSpaceToUnitsIndex, SqlDbType.SmallInt, dto.MetaSpaceToUnitsIndex);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedWidth, SqlDbType.Bit, dto.MetaIsOverMerchandisedWidth);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedHeight, SqlDbType.Bit, dto.MetaIsOverMerchandisedHeight);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOverMerchandisedDepth, SqlDbType.Bit, dto.MetaIsOverMerchandisedDepth);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalComponentCollisions, SqlDbType.Int, dto.MetaTotalComponentCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalPositionCollisions, SqlDbType.Int, dto.MetaTotalPositionCollisions);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaTotalFrontFacings, SqlDbType.SmallInt, dto.MetaTotalFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaAverageFrontFacings, SqlDbType.Real, dto.MetaAverageFrontFacings);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaPercentageLinearSpaceFilled, SqlDbType.Real, dto.MetaPercentageLinearSpaceFilled);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentNotchNumber, SqlDbType.Int, dto.NotchNumber);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldX, SqlDbType.Real, dto.MetaWorldX);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldY, SqlDbType.Real, dto.MetaWorldY);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldZ, SqlDbType.Real, dto.MetaWorldZ);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldAngle, SqlDbType.Real, dto.MetaWorldAngle);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldSlope, SqlDbType.Real, dto.MetaWorldSlope);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaWorldRoll, SqlDbType.Real, dto.MetaWorldRoll);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsOutsideOfFixtureArea, SqlDbType.Bit, dto.MetaIsOutsideOfFixtureArea);
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser, SqlDbType.Bit, dto.MetaIsComponentSlopeWithNoRiser);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureComponentDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramFixtureComponentId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramFixtureComponentDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                this.Delete(dto);
            }
        }


        #endregion

        #region Helper Methods
        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
