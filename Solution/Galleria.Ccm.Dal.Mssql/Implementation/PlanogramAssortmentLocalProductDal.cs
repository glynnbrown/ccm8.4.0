﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramAssortmentLocalProductDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAssortmentLocalProductDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAssortmentLocalProduct DTO from the given Data Reader.
        /// </summary>
        /// <param name="dr">The Data Reader from which to load data.</param>
        /// <returns>A new DTO.</returns>
        public PlanogramAssortmentLocalProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAssortmentLocalProductDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentLocalProductId]),
                PlanogramAssortmentId = (Int32)GetValue(dr[FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId]),
                ProductGtin = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocalProductProductGtin]),
                LocationCode = (String)GetValue(dr[FieldNames.PlanogramAssortmentLocalProductLocationCode])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramAssortmentLocalProduct DTOs with a matching Planogram Assortment Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramAssortmentLocalProduct DTOs.</returns>
        public IEnumerable<PlanogramAssortmentLocalProductDto> FetchByPlanogramAssortmentId(object id)
        {
            var dtoList = new List<PlanogramAssortmentLocalProductDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocalProductFetchByPlanogramAssortmentId))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId, SqlDbType.Int, (Int32)id);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramAssortmentLocalProductDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocalProductInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductLocationCode, SqlDbType.NVarChar, dto.LocationCode);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAssortmentLocalProductDto> dtos)
        {
            foreach (PlanogramAssortmentLocalProductDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAssortmentLocalProductDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocalProductUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductPlanogramAssortmentId, SqlDbType.Int, dto.PlanogramAssortmentId);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductLocationCode, SqlDbType.NVarChar, dto.LocationCode);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAssortmentLocalProductDto> dtos)
        {
            foreach (PlanogramAssortmentLocalProductDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAssortmentLocalProductDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAssortmentLocalProductId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAssortmentLocalProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAssortmentLocalProductDto> dtos)
        {
            foreach (PlanogramAssortmentLocalProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
