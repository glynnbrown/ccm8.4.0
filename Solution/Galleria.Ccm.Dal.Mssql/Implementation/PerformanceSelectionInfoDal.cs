﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//  Created
// CCM-27172 : I.George
//     Added FetchByEntityIdIncludingDeleted
#endregion

#region Version History: (CCM 8.3.0)
// V8-31830 : A.Probyn
//  Updated to include GFSPerformanceSourceId, SelectionType, TimeType, DynamicValue
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public sealed class PerformanceSelectionInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IPerformanceSelectionInfoDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private PerformanceSelectionInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PerformanceSelectionInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PerformanceSelectionId]),
                Name = (String)GetValue(dr[FieldNames.PerformanceSelectionName]),
                GFSPerformanceSourceId = (Int32)GetValue(dr[FieldNames.PerformanceSelectionGFSPerformanceSourceId]),
                SelectionType = (Byte)GetValue(dr[FieldNames.PerformanceSelectionSelectionType]),
                TimeType = (Byte)GetValue(dr[FieldNames.PerformanceSelectionTimeType]),
                DynamicValue = (Int32)GetValue(dr[FieldNames.PerformanceSelectionDynamicValue]),
            };
        }
        #endregion

        #region Fetch

        public IEnumerable<PerformanceSelectionInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<PerformanceSelectionInfoDto> dtoList = new List<PerformanceSelectionInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.PerformanceSelectionEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public IEnumerable<PerformanceSelectionInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            List<PerformanceSelectionInfoDto> dtoList = new List<PerformanceSelectionInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionInfoFetchByEntityIdIncludingDeleted))
                {
                    CreateParameter(command, FieldNames.PerformanceSelectionEntityId, SqlDbType.Int, entityId);
                        using(SqlDataReader dr = command.ExecuteReader())
                        {
                            while(dr.Read())
                            {
                                dtoList.Add(GetDataTransferObject(dr));
                            }
                        }
                }
            }
            catch(SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion
    }
}
