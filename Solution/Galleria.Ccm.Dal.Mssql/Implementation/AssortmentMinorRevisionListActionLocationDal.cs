﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentMinorRevisionListActionLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionListActionLocationDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionListActionLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionListActionLocationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionLocationId]),
                AssortmentMinorRevisionListActionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionLocationAssortmentMinorRevisionListActionId]),
                LocationCode = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionLocationLocationCode]),
                LocationName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionLocationLocationName]),
                LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionLocationLocationId]),
                Units = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionLocationUnits]),
                Facings = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionListActionLocationFacings])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionListActionLocationDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionListActionLocationDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionLocationFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment minor revision action id
        /// </summary>
        /// <param name="entityId">specified AssortmentMinorRevisionListAction id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionListActionLocationDto> FetchByAssortmentMinorRevisionListActionId(Int32 AssortmentMinorRevisionListActionId)
        {
            List<AssortmentMinorRevisionListActionLocationDto> dtoList = new List<AssortmentMinorRevisionListActionLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionLocationFetchByAssortmentMinorRevisionListActionId))
                {
                    //Assortment Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationAssortmentMinorRevisionListActionId, SqlDbType.Int, AssortmentMinorRevisionListActionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionListActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationAssortmentMinorRevisionListActionId, SqlDbType.Int, dto.AssortmentMinorRevisionListActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationUnits, SqlDbType.Int, dto.Units);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationFacings, SqlDbType.Int, dto.Facings);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionListActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionLocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationAssortmentMinorRevisionListActionId, SqlDbType.Int, dto.AssortmentMinorRevisionListActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationUnits, SqlDbType.Int, dto.Units);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationFacings, SqlDbType.Int, dto.Facings);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionListActionLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionListActionLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
