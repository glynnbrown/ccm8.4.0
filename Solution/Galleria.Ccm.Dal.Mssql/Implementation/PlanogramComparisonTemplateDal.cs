﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     MSSQL implementation of <see cref="PlanogramComparisonTemplateDal"/>.
    /// </summary>
    public class PlanogramComparisonTemplateDal : Framework.Dal.Mssql.DalBase, IPlanogramComparisonTemplateDal
    {
        #region DataTransferObject

        /// <summary>
        ///     Initialize a new <see cref="PlanogramComparisonTemplateDto"/> from the given <see cref="IDataRecord"/>.
        /// </summary>
        /// <param name="source">The <see cref="IDataRecord"/> that contains the data to be used in the new item.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateDto"/> with the data contained in the given <paramref name="source"/></returns>
        private static PlanogramComparisonTemplateDto GetDataTransferObject(IDataRecord source)
        {
            return new PlanogramComparisonTemplateDto
            {
                Id = (Int32)GetValue(source[FieldNames.PlanogramComparisonTemplateId]),
                RowVersion = new RowVersion(GetValue(source[FieldNames.PlanogramComparisonTemplateRowVersion])),
                EntityId = (Int32)GetValue(source[FieldNames.PlanogramComparisonTemplateEntityId]),
                DateCreated = (DateTime)GetValue(source[FieldNames.PlanogramComparisonTemplateDateCreated]),
                DateLastModified = (DateTime)GetValue(source[FieldNames.PlanogramComparisonTemplateDateLastModified]),
                Name = (String)GetValue(source[FieldNames.PlanogramComparisonTemplateName]),
                Description = (String)GetValue(source[FieldNames.PlanogramComparisonTemplateDescription]),
                IgnoreNonPlacedProducts = (Boolean)GetValue(source[FieldNames.PlanogramComparisonTemplateIgnoreNonPlacedProducts]),
                DataOrderType = (Byte)GetValue(source[FieldNames.PlanogramComparisonTemplateDataOrderType])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramComparisonTemplateDto"/> matching the given <paramref name="id"/> in the DAL.
        /// </summary>
        /// <param name="id">The ID used to locate the <c>dto</c> in the DAL.</param>
        /// <exception cref="DtoDoesNotExistException">Thrown if there is no corresponding <c>dto</c> to be fetched.</exception>
        public PlanogramComparisonTemplateDto FetchById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateFetchById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateId, SqlDbType.Int, id);

                    //  Read the data, and throw an exception if the dto is not found.
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        //  Return the fetched dto.
                        return GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            //  There was no dto, or an exception ocurred, return null.
            return null;
        }

        /// <summary>
        ///     Fetch the <see cref="PlanogramComparisonTemplateDto"/> matching the given <paramref name="entityId"/> and <paramref name="name"/> in the DAL.
        /// </summary>
        /// <param name="entityId">The ID of the Entity that the matching <c>dto</c> has to belong to in the DAL.</param>
        /// <param name="name">The name used when matching the <c>dto</c>.</param>
        /// <exception cref="DtoDoesNotExistException">Thrown if there is no corresponding <c>dto</c> to be fetched.</exception>
        public PlanogramComparisonTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateName, SqlDbType.NVarChar, name);

                    //  Read the data, and throw an exception if the dto is not found.
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        //  Return the fetched dto.
                        return GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            //  There was no dto, or an exception ocurred, return null.
            return null;
        }
       
        #endregion

        #region Insert

        /// <summary>
        ///     Insert the given <paramref name="dto"/> into the DAL.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonTemplateDto"/> instance containing the data to be inserted.</param>
        public void Insert(PlanogramComparisonTemplateDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateInsert))
                {
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramComparisonTemplateId, SqlDbType.Int);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PlanogramComparisonTemplateRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramComparisonTemplateDateCreated, SqlDbType.SmallDateTime);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command,
                                                                    FieldNames.PlanogramComparisonTemplateDateLastModified,
                                                                    SqlDbType.SmallDateTime);
                    CreateOtherParameters(dto, command);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)dateCreatedParameter.Value;
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(PlanogramComparisonTemplateDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateId, SqlDbType.Int, dto.Id);
                    SqlParameter rowVersionParameter =
                        CreateParameter(command,
                                        FieldNames.PlanogramComparisonTemplateRowVersion,
                                        SqlDbType.Timestamp,
                                        ParameterDirection.InputOutput,
                                        dto.RowVersion);
                    SqlParameter dateLastModifiedParameter =
                        CreateParameter(command, FieldNames.PlanogramComparisonTemplateDateLastModified, SqlDbType.SmallDateTime);

                    CreateOtherParameters(dto, command);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateLastModified = (DateTime)dateLastModifiedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateId, SqlDbType.Int, id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        
        #endregion

        #region Locking

        /// <summary>
        ///     Lock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be locked.</param>
        public void LockById(Object id)
        {
            //  MSSQL does not lock, do nothing.
        }

        /// <summary>
        ///     Unlock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be unlocked.</param>
        public void UnlockById(Object id)
        {
            //  MSSQL Mock does not unlock, do nothing.
        }

        #endregion

        #region Helper Methods

        private class OutputParameters
        {
            public SqlParameter IdParameter { get; set; }
            public SqlParameter RowVersionParameter { get; set; }
            public SqlParameter DateCreatedParameter { get; set; }
            public SqlParameter DateLastModifiedParameter { get; set; }
        }

        private void CreateOtherParameters(PlanogramComparisonTemplateDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateEntityId, SqlDbType.Int, dto.EntityId);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateDescription, SqlDbType.NVarChar, dto.Description);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateIgnoreNonPlacedProducts, SqlDbType.Bit, dto.IgnoreNonPlacedProducts);
            CreateParameter(command, FieldNames.PlanogramComparisonTemplateDataOrderType, SqlDbType.TinyInt, dto.DataOrderType);
        }

        #endregion
    }
}