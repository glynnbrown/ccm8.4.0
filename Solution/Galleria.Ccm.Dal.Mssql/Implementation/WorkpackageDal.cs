﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25546 : L.Ineson
//  Created
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link
//  Added Entity_Id link
// V8-27411 : M.Pettit
//  Added DateCompleted and UserId (owner) properties
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//      Extended for PlanogramLocationType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkpackageDal
    /// </summary>
    public sealed class WorkpackageDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackageDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public WorkpackageDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackageDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkpackageId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.WorkpackageRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.WorkpackageEntityId]),
                WorkflowId = (Int32)GetValue(dr[FieldNames.WorkpackageWorkflowId]),
                UserId = (Int32)GetValue(dr[FieldNames.WorkpackageUserId]),
                Name = (String)GetValue(dr[FieldNames.WorkpackageName]),
                WorkpackageType = (Byte)GetValue(dr[FieldNames.WorkpackageType]),
                PlanogramLocationType = (Byte)GetValue(dr[FieldNames.WorkpackagePlanogramLocationType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.WorkpackageDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.WorkpackageDateLastModified]),
                DateCompleted = (DateTime?)GetValue(dr[FieldNames.WorkpackageDateCompleted])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns the dto that matches the specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WorkpackageDto FetchById(Int32 id)
        {
            WorkpackageDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageFetchById))
                {
                    CreateParameter(command, FieldNames.WorkpackageId, SqlDbType.Int, id);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(WorkpackageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkpackageId, SqlDbType.Int);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.WorkpackageRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.WorkpackageDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.WorkpackageDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    CreateParameter(command, FieldNames.WorkpackageEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.WorkpackageWorkflowId, SqlDbType.Int, dto.WorkflowId);
                    CreateParameter(command, FieldNames.WorkpackageName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.WorkpackageType, SqlDbType.TinyInt, dto.WorkpackageType);
                    CreateParameter(command, FieldNames.WorkpackageUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramLocationType, SqlDbType.TinyInt, dto.PlanogramLocationType);
                    

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts a batch of dtos into the database
        /// </summary>
        /// <param name="dtos">The list of dtos to insert</param>
        public void Insert(IEnumerable<WorkpackageDto> dtos)
        {
            // Not required as we never commit more than one
            // workpackage at a time
            throw new NotImplementedException();
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the db
        /// </summary>
        /// <param name="dto"></param>
        public void Update(WorkpackageDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageUpdateById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackageId, SqlDbType.Int, dto.Id);
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.WorkpackageRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.WorkpackageDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);
                    CreateParameter(command, FieldNames.WorkpackageEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.WorkpackageWorkflowId, SqlDbType.Int, dto.WorkflowId);
                    CreateParameter(command, FieldNames.WorkpackageName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.WorkpackageType, SqlDbType.TinyInt, dto.WorkpackageType);
                    CreateParameter(command, FieldNames.WorkpackageUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramLocationType, SqlDbType.TinyInt, dto.PlanogramLocationType);
                    CreateParameter(command, FieldNames.WorkpackageDateCompleted, SqlDbType.DateTime, dto.DateCompleted);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates a batch of dtos in the database
        /// </summary>
        /// <param name="dtos">The list of dtos to update</param>
        public void Update(IEnumerable<WorkpackageDto> dtos)
        {
            // Not required as we never commit more than one
            // workpackage at a time
            throw new NotImplementedException();
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the record with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackageDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.WorkpackageId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item from the database
        /// </summary>
        public void Delete(WorkpackageDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes a batch of dtos from the database
        /// </summary>
        /// <param name="dtos">The list of dtos to delete</param>
        public void Delete(IEnumerable<WorkpackageDto> dtos)
        {
            // Not required as we never commit more than one
            // workpackage at a time
            throw new NotImplementedException();
        }
        #endregion
    }
}
