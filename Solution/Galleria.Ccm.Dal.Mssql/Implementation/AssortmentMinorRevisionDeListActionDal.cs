﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentMinorRevisionDeListActionDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionDeListActionDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionDeListActionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionDeListActionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionId]),
                AssortmentMinorRevisionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionAssortmentMinorRevisionId]),
                ProductGtin = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionProductGtin]),
                ProductName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionProductName]),
                ProductId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionProductId]),
                Priority = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionPriority]),
                Comments = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionDeListActionComments])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionDeListActionDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionDeListActionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment id
        /// </summary>
        /// <param name="entityId">specified Assortment id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionDeListActionDto> FetchByAssortmentMinorRevisionId(Int32 AssortmentMinorRevisionId)
        {
            List<AssortmentMinorRevisionDeListActionDto> dtoList = new List<AssortmentMinorRevisionDeListActionDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionFetchByAssortmentMinorRevisionId))
                {
                    //Assortment Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionAssortmentMinorRevisionId, SqlDbType.Int, AssortmentMinorRevisionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionDeListActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionComments, SqlDbType.NVarChar, dto.Comments);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionDeListActionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionAssortmentMinorRevisionId, SqlDbType.Int, dto.AssortmentMinorRevisionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionProductGtin, SqlDbType.NVarChar, dto.ProductGtin);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionProductName, SqlDbType.NVarChar, dto.ProductName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionPriority, SqlDbType.Int, dto.Priority);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionComments, SqlDbType.NVarChar, dto.Comments);
                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionDeListActionDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionDeListActionId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
