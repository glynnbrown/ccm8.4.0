﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-28996 : A.Silva
//		Created
#endregion

#region Version History : CCM830
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroupId.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramSequenceGroupProductDal : DalBase, IPlanogramSequenceGroupProductDal
    {
        #region Data TransferObject

        /// <summary>
        ///     Returns a <see cref="PlanogramSequenceGroupProductDto"/> from the values in <paramref name="dr"/>.
        /// </summary>
        /// <param name="dr">The <see cref="SqlDataReader"/> to load from.</param>
        /// <returns>A new <see cref="PlanogramSequenceGroupProductDto"/>.</returns>
        private static PlanogramSequenceGroupProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramSequenceGroupProductDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramSequenceGroupProductId]),
                PlanogramSequenceGroupId = (Int32)GetValue(dr[FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupId]),
                Gtin = (String)GetValue(dr[FieldNames.PlanogramSequenceGroupProductGtin]),
                SequenceNumber = (Int32)GetValue(dr[FieldNames.PlanogramSequenceGroupProductSequenceNumber]),
                PlanogramSequenceGroupSubGroupId = (Int32?)GetValue(dr[FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Gets a collection of <see cref="PlanogramSequenceGroupProductDto"/> matching the <paramref name="planogramSequenceId"/>.
        /// </summary>
        /// <param name="planogramSequenceId">Unique identifier for the Planogram Sequence Group that contains the Planogram Sequence Group Product to be fetched.</param>
        /// <returns>A new collection of new <see cref="PlanogramSequenceGroupProductDto"/> instances.</returns>
        public IEnumerable<PlanogramSequenceGroupProductDto> FetchByPlanogramSequenceGroupId(Object planogramSequenceId)
        {
            IList<PlanogramSequenceGroupProductDto> dtos = new List<PlanogramSequenceGroupProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupProductFetchByPlanogramSequenceGroupId))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupId, SqlDbType.Int, (Int32)planogramSequenceId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtos;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupProductDto"/> to insert.</param>
        public void Insert(PlanogramSequenceGroupProductDto dto)
        {
            try
            {
                SqlParameter idParameter;
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupProductInsert))
                {
                    idParameter = CreateParameter(command, FieldNames.PlanogramSequenceGroupProductId, SqlDbType.Int);
                    CreateDtoParameters(command, dto);

                    command.ExecuteNonQuery();
                }

                dto.Id = (Int32)idParameter.Value;
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Inserts the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupProductDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            foreach (PlanogramSequenceGroupProductDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupProductDto"/> to update.</param>
        public void Update(PlanogramSequenceGroupProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupProductUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupProductId, SqlDbType.Int, dto.Id);
                    CreateDtoParameters(command, dto);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Updates the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceGroupProductDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            foreach (PlanogramSequenceGroupProductDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the record with a matching <paramref name="id"/> from the database.
        /// </summary>
        /// <param name="id">The Id of the record to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupProductDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupProductId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Deletes the given <paramref name="dto"/> from the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupProductDto"/> to delete.</param>
        public void Delete(PlanogramSequenceGroupProductDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes the given <paramref name="dtos"/> from the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceGroupProductDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            foreach (PlanogramSequenceGroupProductDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion

        private void CreateDtoParameters(DalCommand command, PlanogramSequenceGroupProductDto dto)
        {
            CreateParameter(command, FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupId, SqlDbType.Int,
                dto.PlanogramSequenceGroupId);
            CreateParameter(command, FieldNames.PlanogramSequenceGroupProductGtin, SqlDbType.NVarChar, dto.Gtin);
            CreateParameter(command, FieldNames.PlanogramSequenceGroupProductSequenceNumber, SqlDbType.Int, dto.SequenceNumber);
            CreateParameter(command, FieldNames.PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId, SqlDbType.Int,
                dto.PlanogramSequenceGroupSubGroupId);
        }
    }
}
