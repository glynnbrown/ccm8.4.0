﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// AssortmentMinorRevisionReplaceActionLocation Dal
    /// </summary>
    public class AssortmentMinorRevisionReplaceActionLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionReplaceActionLocationDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionReplaceActionLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionReplaceActionLocationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionLocationId]),
                AssortmentMinorRevisionReplaceActionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionLocationAssortmentMinorRevisionReplaceActionId]),
                LocationCode = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationCode]),
                LocationName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationName]),
                LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationId]),
                Units = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionLocationUnits]),
                Facings = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionReplaceActionLocationFacings])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionReplaceActionLocationDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionReplaceActionLocationDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionLocationFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment minor revision action id
        /// </summary>
        /// <param name="entityId">specified AssortmentMinorRevisionReplaceAction id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionReplaceActionLocationDto> FetchByAssortmentMinorRevisionReplaceActionId(Int32 AssortmentMinorRevisionReplaceActionId)
        {
            List<AssortmentMinorRevisionReplaceActionLocationDto> dtoList = new List<AssortmentMinorRevisionReplaceActionLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionLocationFetchByAssortmentMinorRevisionReplaceActionId))
                {
                    //AssortmentMinorRevisionReplaceAction Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationAssortmentMinorRevisionReplaceActionId, SqlDbType.Int, AssortmentMinorRevisionReplaceActionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionReplaceActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationAssortmentMinorRevisionReplaceActionId, SqlDbType.Int, dto.AssortmentMinorRevisionReplaceActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationUnits, SqlDbType.Int, dto.Units);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationFacings, SqlDbType.Int, dto.Facings);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionReplaceActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionLocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationAssortmentMinorRevisionReplaceActionId, SqlDbType.Int, dto.AssortmentMinorRevisionReplaceActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationUnits, SqlDbType.Int, dto.Units);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationFacings, SqlDbType.Int, dto.Facings);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionReplaceActionLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionReplaceActionLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
