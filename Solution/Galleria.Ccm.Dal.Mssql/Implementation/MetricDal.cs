﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;


namespace Galleria.Ccm.Dal.Mssql.Implementation
{
   /// <summary>
   /// Metric Dal
   /// </summary>
    public class MetricDal : Galleria.Framework.Dal.Mssql.DalBase, IMetricDal
    {

        #region Data Transfer Object

        public static MetricDto GetDataTransferObjects(SqlDataReader dr)
        {
            return new MetricDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.MetricId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.MetricRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.MetricEntityId]),
                Name = (String)GetValue(dr[FieldNames.MetricName]),
                Description = (String)GetValue(dr[FieldNames.MetricDescription]),
                Direction = (Byte)GetValue(dr[FieldNames.MetricDirection]),
                Type = (Byte)GetValue(dr[FieldNames.MetricType]),
                SpecialType = (Byte)GetValue(dr[FieldNames.MetricSpecialType]),
                DataModelType = (Byte)GetValue(dr[FieldNames.MetricDataModelType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.MetricDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.MetricDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.MetricDateDeleted])

            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<MetricDto> FetchByEntityId(Int32 entityId)
        {
            List<MetricDto> dtoList = new List<MetricDto>();
            try {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.MetricEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObjects(dr));
                        }
                    }
                }
            }
            catch(SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(MetricDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricInsert))
                {
                    //id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.MetricId, SqlDbType.Int);

                    //Row Version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.MetricRowVersion, SqlDbType.Timestamp);

                    //Date Created 
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.MetricDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date Last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.MetricDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter datedeletedParameter = CreateParameter(command, FieldNames.MetricDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);

                    //Other properties
                    CreateParameter(command, FieldNames.MetricEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.MetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.MetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.MetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.MetricType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.MetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.MetricDataModelType, SqlDbType.TinyInt, dto.DataModelType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = datedeletedParameter.Value == DBNull.Value ? null : (DateTime?)datedeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Update
        public void Update(MetricDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricUpdate))
                {
                    //id
                    CreateParameter(command, FieldNames.MetricId, SqlDbType.Int, dto.Id);

                    //Row Version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.MetricRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //Date created
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.MetricDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.MetricDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date Deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.MetricDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //other properties
                    CreateParameter(command, FieldNames.MetricEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.MetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.MetricDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.MetricDirection, SqlDbType.TinyInt, dto.Direction);
                    CreateParameter(command, FieldNames.MetricType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.MetricSpecialType, SqlDbType.TinyInt, dto.SpecialType);
                    CreateParameter(command, FieldNames.MetricDataModelType, SqlDbType.TinyInt, dto.DataModelType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.MetricDeleteById))
                {
                    //id
                    CreateParameter(command, FieldNames.MetricId, SqlDbType.Int, id);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
