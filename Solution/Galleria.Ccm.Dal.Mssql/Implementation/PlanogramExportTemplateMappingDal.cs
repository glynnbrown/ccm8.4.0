﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
     /// <summary>
    /// PlanogramExportTemplateMapping Dal implementation
    /// </summary>
    public class PlanogramExportTemplateMappingDal : DalBase, IPlanogramExportTemplateMappingDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramExportTemplateMappingDto GetDataTransferObject(IDataRecord dr)
        {
            return new PlanogramExportTemplateMappingDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplateMappingId]),
                PlanogramExportTemplateId = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplateMappingPlanogramExportTemplateId]),
                FieldType = (Byte)GetValue(dr[FieldNames.PlanogramExportTemplateMappingFieldType]),
                Field = (String)GetValue(dr[FieldNames.PlanogramExportTemplateMappingField]),
                ExternalField = (String)GetValue(dr[FieldNames.PlanogramExportTemplateMappingExternalField]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramExportTemplateMappingDto"/> items corresponding to the given <paramref name="PlanogramExportTemplateId"/>.
        /// </summary>
        /// <param name="PlanogramExportTemplateId">The unique id value for the Planogram Import Template containing the groups to be retrieved.</param>
        /// <returns>An instance implementing <see cref="IEnumerable{PlanogramExportTemplateMappingDto}"/> with all the matching groups.</returns>
        public IEnumerable<PlanogramExportTemplateMappingDto> FetchByPlanogramExportTemplateId(Object PlanogramExportTemplateId)
        {
            var dtoList = new List<PlanogramExportTemplateMappingDto>();
            try
            {
                using (
                    var command = CreateCommand(ProcedureNames.PlanogramExportTemplateMappingFetchByPlanogramExportTemplateId)
                    )
                {
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingPlanogramExportTemplateId, SqlDbType.Int,
                        (Int32)PlanogramExportTemplateId);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the data contained in the given <paramref name="dto"/> into the dal.
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramExportTemplateMappingDto"/> containing the data to be persisted.</param>
        public void Insert(PlanogramExportTemplateMappingDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateMappingInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateMappingId,
                        SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingPlanogramExportTemplateId, SqlDbType.Int,
                        dto.PlanogramExportTemplateId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingFieldType, SqlDbType.TinyInt, dto.FieldType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingExternalField, SqlDbType.NVarChar,
                        dto.ExternalField);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the data contained in the given <paramref name="dto"/> in the dal
        /// </summary>
        /// <param name="dto">Instance of <see cref="PlanogramExportTemplateMappingDto"/> containing the data to be persisted.</param>
        public void Update(PlanogramExportTemplateMappingDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateMappingUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingPlanogramExportTemplateId, SqlDbType.Int,
                        dto.PlanogramExportTemplateId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingFieldType, SqlDbType.NVarChar, dto.FieldType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingField, SqlDbType.NVarChar,
                        dto.Field);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingExternalField, SqlDbType.NVarChar,
                        dto.ExternalField);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the Planogram Export Template Mapping matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Unique id value to determine with Planogram Export Template Mapping to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateMappingDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramExportTemplateMappingId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
