﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//   Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramValidationTemplate Dal
    /// </summary>
    public class PlanogramValidationTemplateDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramValidationTemplateDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramValidationTemplateDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramValidationTemplateDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramValidationTemplateId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramValidationTemplatePlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramValidationTemplateName]),
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Gets a list of PlanogramValidationTemplate DTOs with a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A List of PlanogramValidationTemplate DTOs.</returns>
        public PlanogramValidationTemplateDto FetchByPlanogramId(object planogramId)
        {
            PlanogramValidationTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateFetchByPlanogramId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PlanogramValidationTemplatePlanogramId, SqlDbType.Int, (Int32)planogramId);

                    // execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given DTO into the database.
        /// </summary>
        /// <param name="dto">The DTO to insert.</param>
        public void Insert(PlanogramValidationTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateInsert))
                {
                    // ID
                    var idParameter = CreateParameter(command, FieldNames.PlanogramValidationTemplateId, SqlDbType.Int);

                    // Other Parameters
                    CreateParameter(command, FieldNames.PlanogramValidationTemplatePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateName, SqlDbType.NVarChar, dto.Name);

                    // Excute
                    command.ExecuteNonQuery();

                    // Update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            foreach (PlanogramValidationTemplateDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramValidationTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateUpdateById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateId, SqlDbType.Int, dto.Id);

                    // Other parameter
                    CreateParameter(command, FieldNames.PlanogramValidationTemplatePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateName, SqlDbType.NVarChar, dto.Name);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            foreach (PlanogramValidationTemplateDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a DTO in the database from the given Id.
        /// </summary>
        /// <param name="id">The Id of the DTO to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationTemplateDeleteById))
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramValidationTemplateId, SqlDbType.Int, (Int32)id);

                    // Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramValidationTemplateDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            foreach (PlanogramValidationTemplateDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
