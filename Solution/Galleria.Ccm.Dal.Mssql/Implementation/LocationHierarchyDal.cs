﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// LocationHierarchy Dal
    /// </summary>
    public class LocationHierarchyDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationHierarchyDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static LocationHierarchyDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationHierarchyDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationHierarchyId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.LocationHierarchyRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationHierarchyEntityId]),
                Name = (String)GetValue(dr[FieldNames.LocationHierarchyName]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationHierarchyDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationHierarchyDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.LocationHierarchyDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public LocationHierarchyDto FetchById(Int32 id)
        {
            LocationHierarchyDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationHierarchyFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationHierarchyId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public LocationHierarchyDto FetchByEntityId(Int32 entityId)
        {
            LocationHierarchyDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationHierarchyFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.LocationHierarchyEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(LocationHierarchyDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationHierarchyInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationHierarchyId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.LocationHierarchyRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationHierarchyDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationHierarchyDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationHierarchyDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationHierarchyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LocationHierarchyName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(LocationHierarchyDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationHierarchyUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationHierarchyId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.LocationHierarchyRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationHierarchyDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationHierarchyDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationHierarchyDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationHierarchyEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LocationHierarchyName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationHierarchyDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationHierarchyId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}