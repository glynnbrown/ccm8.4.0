﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
// CCM-25827 : N.Haywood
//  Changed ParameterDirection on dates to output
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// LocationLevel Dal
    /// </summary>
    public class LocationLevelDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationLevelDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static LocationLevelDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationLevelDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationLevelId]),
                LocationHierarchyId = (Int32)GetValue(dr[FieldNames.LocationLevelLocationHierarchyId]),
                Name = (String)GetValue(dr[FieldNames.LocationLevelName]),
                ShapeNo = (Byte)GetValue(dr[FieldNames.LocationLevelShapeNo]),
                Colour = (Int32)GetValue(dr[FieldNames.LocationLevelColour]),
                ParentLevelId = (Int32?)GetValue(dr[FieldNames.LocationLevelParentLevelId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationLevelDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationLevelDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.LocationLevelDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public LocationLevelDto FetchById(Int32 id)
        {
            LocationLevelDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationLevelFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationLevelId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }



        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<LocationLevelDto> FetchByLocationHierarchyId(Int32 locationHierarchyId)
        {
            List<LocationLevelDto> dtoList = new List<LocationLevelDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationLevelFetchByLocationHierarchyId))
                {
                    CreateParameter(command, FieldNames.LocationLevelLocationHierarchyId, SqlDbType.Int, locationHierarchyId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(LocationLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationLevelInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.LocationLevelId, SqlDbType.Int);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationLevelDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationLevelDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationLevelDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationLevelLocationHierarchyId, SqlDbType.Int, dto.LocationHierarchyId);
                    CreateParameter(command, FieldNames.LocationLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LocationLevelShapeNo, SqlDbType.TinyInt, dto.ShapeNo);
                    CreateParameter(command, FieldNames.LocationLevelColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.LocationLevelParentLevelId, SqlDbType.Int, dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(LocationLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationLevelUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationLevelId, SqlDbType.Int, dto.Id);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationLevelDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationLevelDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.LocationLevelDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.LocationLevelLocationHierarchyId, SqlDbType.Int, dto.LocationHierarchyId);
                    CreateParameter(command, FieldNames.LocationLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LocationLevelShapeNo, SqlDbType.TinyInt, dto.ShapeNo);
                    CreateParameter(command, FieldNames.LocationLevelColour, SqlDbType.Int, dto.Colour);
                    CreateParameter(command, FieldNames.LocationLevelParentLevelId, SqlDbType.Int, dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationLevelDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LocationLevelId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}