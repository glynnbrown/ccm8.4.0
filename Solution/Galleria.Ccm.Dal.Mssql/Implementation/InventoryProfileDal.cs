﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26124 : I.George
//		Created (Auto-generated)
// V8-28013 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion

#region Version History: (CCM 820)
// CCM-30759 : J.Pickup
//		InventoryProfileType introduced.
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// InventoryProfile Dal
    /// </summary>
    public class InventoryProfileDal : Galleria.Framework.Dal.Mssql.DalBase, IInventoryProfileDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static InventoryProfileDto GetDataTransferObject(SqlDataReader dr)
        {
            return new InventoryProfileDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.InventoryProfileId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.InventoryProfileRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.InventoryProfileEntityId]),
                Name = (String)GetValue(dr[FieldNames.InventoryProfileName]),
                CasePack = (Single)GetValue(dr[FieldNames.InventoryProfileCasePack]),
                DaysOfSupply = (Single)GetValue(dr[FieldNames.InventoryProfileDaysOfSupply]),
                ShelfLife = (Single)GetValue(dr[FieldNames.InventoryProfileShelfLife]),
                ReplenishmentDays = (Single)GetValue(dr[FieldNames.InventoryProfileReplenishmentDays]),
                WasteHurdleUnits = (Single)GetValue(dr[FieldNames.InventoryProfileWasteHurdleUnits]),
                WasteHurdleCasePack = (Single)GetValue(dr[FieldNames.InventoryProfileWasteHurdleCasePack]),
                MinUnits = (Int32)GetValue(dr[FieldNames.InventoryProfileMinUnits]),
                MinFacings = (Int32)GetValue(dr[FieldNames.InventoryProfileMinFacings]),
                InventoryProfileType = (Byte)GetValue(dr[FieldNames.InventoryProfileType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.InventoryProfileDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.InventoryProfileDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.InventoryProfileDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public InventoryProfileDto FetchById(Int32 id)
        {
            InventoryProfileDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.InventoryProfileFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.InventoryProfileId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public InventoryProfileDto FetchByEntityIdName(Int32 entityId, String name)
        {
            InventoryProfileDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.InventoryProfileFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.InventoryProfileEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.InventoryProfileName, SqlDbType.NVarChar, name);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(InventoryProfileDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.InventoryProfileInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.InventoryProfileId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.InventoryProfileRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.InventoryProfileDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.InventoryProfileDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.InventoryProfileDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.InventoryProfileEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.InventoryProfileName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.InventoryProfileCasePack, SqlDbType.Real, dto.CasePack);
                    CreateParameter(command, FieldNames.InventoryProfileDaysOfSupply, SqlDbType.Real, dto.DaysOfSupply);
                    CreateParameter(command, FieldNames.InventoryProfileShelfLife, SqlDbType.Real, dto.ShelfLife);
                    CreateParameter(command, FieldNames.InventoryProfileReplenishmentDays, SqlDbType.Real, dto.ReplenishmentDays);
                    CreateParameter(command, FieldNames.InventoryProfileWasteHurdleUnits, SqlDbType.Real, dto.WasteHurdleUnits);
                    CreateParameter(command, FieldNames.InventoryProfileWasteHurdleCasePack, SqlDbType.Real, dto.WasteHurdleCasePack);
                    CreateParameter(command, FieldNames.InventoryProfileMinUnits, SqlDbType.Int, dto.MinUnits);
                    CreateParameter(command, FieldNames.InventoryProfileMinFacings, SqlDbType.Int, dto.MinFacings);
                    CreateParameter(command, FieldNames.InventoryProfileType, SqlDbType.TinyInt, dto.InventoryProfileType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(InventoryProfileDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.InventoryProfileUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.InventoryProfileId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.InventoryProfileRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.InventoryProfileDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.InventoryProfileDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.InventoryProfileDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.InventoryProfileEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.InventoryProfileName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.InventoryProfileCasePack, SqlDbType.Real, dto.CasePack);
                    CreateParameter(command, FieldNames.InventoryProfileDaysOfSupply, SqlDbType.Real, dto.DaysOfSupply);
                    CreateParameter(command, FieldNames.InventoryProfileShelfLife, SqlDbType.Real, dto.ShelfLife);
                    CreateParameter(command, FieldNames.InventoryProfileReplenishmentDays, SqlDbType.Real, dto.ReplenishmentDays);
                    CreateParameter(command, FieldNames.InventoryProfileWasteHurdleUnits, SqlDbType.Real, dto.WasteHurdleUnits);
                    CreateParameter(command, FieldNames.InventoryProfileWasteHurdleCasePack, SqlDbType.Real, dto.WasteHurdleCasePack);
                    CreateParameter(command, FieldNames.InventoryProfileMinUnits, SqlDbType.Int, dto.MinUnits);
                    CreateParameter(command, FieldNames.InventoryProfileMinFacings, SqlDbType.Int, dto.MinFacings);
                    CreateParameter(command, FieldNames.InventoryProfileType, SqlDbType.TinyInt, dto.InventoryProfileType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.InventoryProfileDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.InventoryProfileId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}
