﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27004 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     Mssql dal implementation of <see cref="PlanogramValidationMetricInfoDal"/>.
    /// </summary>
    public sealed class PlanogramValidationMetricInfoDal : DalBase, IPlanogramValidationMetricInfoDal
    {
        #region Constants

        private const String TempImportTable = "#tmpPlanogram";

        #endregion

        #region Data Transfer Object

        private static PlanogramValidationMetricInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramValidationMetricInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramValidationMetricInfoId]),
                PlanogramValidationGroupId = (Int32)GetValue(dr[FieldNames.PlanogramValidationMetricInfoPlanogramValidationGroupId]),
                Field = (String)GetValue(dr[FieldNames.PlanogramValidationMetricInfoField]),
                ResultType = (Byte)GetValue(dr[FieldNames.PlanogramValidationMetricInfoResultType])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetches a collection of <see cref="PlanogramValidationGroupInfoDto"/> instances matching the given <paramref name="planogramIds"/>.
        /// </summary>
        /// <param name="planogramIds">A collection of <see cref="Int32"/> values that must be matched by the returned dtos.</param>
        /// <returns>A new collection of <see cref="PlanogramValidationGroupInfoDto"/>.</returns>
        public IEnumerable<PlanogramValidationMetricInfoDto> FetchPlanogramValidationMetricsByPlanogramIds(
            IEnumerable<Int32> planogramIds)
        {
            var dtos = new List<PlanogramValidationMetricInfoDto>();

            CreateTempTable(TempImportTable);
            PopulateTempTable(TempImportTable, planogramIds);

            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramValidationMetricInfoFetchByPlanogramIds))
                using (var dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtos.Add(GetDataTransferObject(dr));
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            DropTempTable(TempImportTable);

            return dtos;
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Creates the temporary store table on the current connection.
        /// </summary>
        /// <param name="tempTableName">Name of the temporary table to be created.</param>
        private void CreateTempTable(String tempTableName)
        {
            const String createTempImportTableSql = "CREATE TABLE [{0}] (Planogram_Id INT)";
            var commandText = String.Format(CultureInfo.InvariantCulture, createTempImportTableSql,
                tempTableName);

            //  Create the temp table.
            using (var command = CreateCommand(commandText, CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        ///     Drops the temporary store table on the current connection.
        /// </summary>
        /// <param name="tempTableName">Name of the temporary table to be dropped.</param>
        private void DropTempTable(String tempTableName)
        {
            const String dropTempImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
            var commandText = String.Format(CultureInfo.InvariantCulture, dropTempImportTableSql,
                tempTableName);

            //  Drop the temp table.
            using (var command = CreateCommand(commandText, CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        private void PopulateTempTable(String detinationTableName, IEnumerable<Int32> values)
        {
            using (var bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = String.Format("[{0}]", detinationTableName);
                bulkCopy.WriteToServer(new PlanogramValidationInfoDal.BulkCopyIntDataReader(values));
            }
        }

        #endregion
    }
}
