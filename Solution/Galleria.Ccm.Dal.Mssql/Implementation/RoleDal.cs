﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//		Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class RoleDal : Galleria.Framework.Dal.Mssql.DalBase, IRoleDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        private RoleDto GetDataTransferObject(SqlDataReader dr)
        {
            return new RoleDto
            {
                Id = (Int32)GetValue(dr[FieldNames.RoleId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.RoleRowVersion])),
                Name = (String)GetValue(dr[FieldNames.RoleName]),
                Description = (String)GetValue(dr[FieldNames.RoleDescription]),
                IsAdministrator = (Boolean)GetValue(dr[FieldNames.RoleIsAdministrator]),
            };
        }

        #endregion

        #region Fetch

        public RoleDto FetchById(Int32 id)
        {
            RoleDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleFetchById))
                {
                    CreateParameter(command, FieldNames.RoleId, SqlDbType.Int, id);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<RoleDto> FetchByEntityId(Int32 entityId)
        {
            List<RoleDto> dtoList = new List<RoleDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.RoleEntity_EntityId, SqlDbType.Int, entityId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(RoleDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleInsert))
                {
                    //Parameters to store the returned Id and rowversion
                    SqlParameter idParameter = CreateParameter(command, FieldNames.RoleId, SqlDbType.Int);
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.RoleRowVersion, SqlDbType.Timestamp);

                    CreateParameter(command, FieldNames.RoleName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.RoleDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.RoleIsAdministrator, SqlDbType.Bit, dto.IsAdministrator);


                    //Execute the insert
                    command.ExecuteNonQuery();

                    //Update the dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(RoleDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleUpdate))
                {
                    SqlParameter versionParameter =
                        CreateParameter(command,
                        FieldNames.RoleRowVersion,
                        SqlDbType.Timestamp,
                        ParameterDirection.InputOutput,
                        dto.RowVersion);

                    CreateParameter(command, FieldNames.RoleId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.RoleName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.RoleDescription, SqlDbType.NVarChar, dto.Description);
                    CreateParameter(command, FieldNames.RoleIsAdministrator, SqlDbType.Bit, dto.IsAdministrator);

                    //Execute the insert
                    command.ExecuteNonQuery();

                    //Update the dto
                    dto.RowVersion = new RowVersion(versionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.RoleDelete))
                {
                    CreateParameter(command, FieldNames.RoleId, SqlDbType.Int, id);

                    // execute the insert statement
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

    }
}
