﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation

{
    public class AssortmentRegionDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentRegionDal
    {
        #region Data Access Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>A dto object</returns>
        public AssortmentRegionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentRegionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentRegionId]),
                Name = (String)GetValue(dr[FieldNames.AssortmentRegionName]),
                AssortmentId = (Int32)GetValue(dr[FieldNames.AssortmentRegionAssortmentId])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all region dtos for the specified assortment
        /// </summary>
        /// <param name="assortmentId">The assortment id</param>
        /// <returns>All region dtos for the assortment</returns>
        public IEnumerable<AssortmentRegionDto> FetchByAssortmentId(Int32 assortmentId)
        {
            List<AssortmentRegionDto> dtoList = new List<AssortmentRegionDto>();
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionFetchByAssortmentId))
            {
                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentRegionAssortmentId,
                    SqlDbType.Int,
                    assortmentId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the specified item from the database
        /// </summary>
        /// <param name="id">The assortment region id</param>
        /// <returns>The assortment region</returns>
        public AssortmentRegionDto FetchById(Int32 id)
        {
            AssortmentRegionDto dto = null;
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionFetchById))
            {
                // assortment region id
                CreateParameter(command,
                    FieldNames.AssortmentRegionId,
                    SqlDbType.Int,
                    id);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dto = GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
            return dto;
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified dto into the database
        /// </summary>
        /// <param name="dto">The dto to insert</param>
        public void Insert(AssortmentRegionDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionInsert))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentRegionId,
                    SqlDbType.Int);

                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentRegionAssortmentId,
                    SqlDbType.Int,
                    dto.AssortmentId);

                // name
                CreateParameter(command,
                    FieldNames.AssortmentRegionName,
                    SqlDbType.NVarChar,
                    dto.Name);
                // execute
                command.ExecuteNonQuery();

                // update the dto
                dto.Id = (Int32)idParameter.Value;
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(AssortmentRegionDto dto)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionUpdateById))
            {
                // id parameter
                SqlParameter idParameter = CreateParameter(command,
                    FieldNames.AssortmentRegionId,
                    SqlDbType.Int,
                    dto.Id);

                // name
                CreateParameter(command,
                    FieldNames.AssortmentRegionName,
                    SqlDbType.NVarChar,
                    dto.Name);

                // assortment id
                CreateParameter(command,
                    FieldNames.AssortmentRegionAssortmentId,
                    SqlDbType.Int,
                    dto.AssortmentId);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        /// <param name="id">The dto to delete</param>
        public void DeleteById(int id)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.AssortmentRegionDeleteById))
            {
                // id parameter
                CreateParameter(command,
                    FieldNames.AssortmentRegionId,
                    SqlDbType.Int,
                    id);

                // execute
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}