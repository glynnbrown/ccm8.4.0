﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.Silva ~ Changed type of validationTemplateId in FetchByValidationTemplateId to Object.
// V8-26812 : A.Silva ~ Added ValidationType field.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     ValidationTemplateGroup Dal
    /// </summary>
    public class ValidationTemplateGroupDal : DalBase, IValidationTemplateGroupDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static ValidationTemplateGroupDto GetDataTransferObject(IDataRecord dr)
        {
            return new ValidationTemplateGroupDto
            {
                Id = (Int32) GetValue(dr[FieldNames.ValidationTemplateGroupId]),
                ValidationTemplateId = (Int32) GetValue(dr[FieldNames.ValidationTemplateGroupValidationTemplateId]),
                Name = (String) GetValue(dr[FieldNames.ValidationTemplateGroupName]),
                Threshold1 = (Single) GetValue(dr[FieldNames.ValidationTemplateGroupThreshold1]),
                Threshold2 = (Single) GetValue(dr[FieldNames.ValidationTemplateGroupThreshold2]),
                ValidationType = (Byte) GetValue(dr[FieldNames.ValidationTemplateGroupValidationType])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="ValidationTemplateGroupDto"/> items corresponding to the given <paramref name="validationTemplateId"/>.
        /// </summary>
        /// <param name="validationTemplateId">The unique id value for the Validation Tempate containing the groups to be retrieved.</param>
        /// <returns>An instance implementing <see cref="IEnumerable{ValidationTemplateGroupDto}"/> with all the matching groups.</returns>
        public IEnumerable<ValidationTemplateGroupDto> FetchByValidationTemplateId(Object validationTemplateId)
        {
            var dtoList = new List<ValidationTemplateGroupDto>();
            try
            {
                using (
                    var command = CreateCommand(ProcedureNames.ValidationTemplateGroupFetchByValidationTemplateId)
                    )
                {
                    CreateParameter(command, FieldNames.ValidationTemplateGroupValidationTemplateId, SqlDbType.Int,
                        (Int32) validationTemplateId);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the data contained in the given <paramref name="dto"/> into the dal.
        /// </summary>
        /// <param name="dto">Instance of <see cref="ValidationTemplateGroupDto"/> containing the data to be persisted.</param>
        public void Insert(ValidationTemplateGroupDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateGroupInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.ValidationTemplateGroupId,
                        SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.ValidationTemplateGroupValidationTemplateId, SqlDbType.Int,
                        dto.ValidationTemplateId);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupValidationType, SqlDbType.TinyInt,
                        dto.ValidationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32) idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the data contained in the given <paramref name="dto"/> in the dal
        /// </summary>
        /// <param name="dto">Instance of <see cref="ValidationTemplateGroupDto"/> containing the data to be persisted.</param>
        public void Update(ValidationTemplateGroupDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateGroupUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ValidationTemplateGroupId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.ValidationTemplateGroupValidationTemplateId, SqlDbType.Int,
                        dto.ValidationTemplateId);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupThreshold1, SqlDbType.Real,
                        dto.Threshold1);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupThreshold2, SqlDbType.Real,
                        dto.Threshold2);
                    CreateParameter(command, FieldNames.ValidationTemplateGroupValidationType, SqlDbType.TinyInt,
                        dto.ValidationType);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the Validation Template Group matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Unique id value to determine with Validation Template Group to delete.</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ValidationTemplateGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}