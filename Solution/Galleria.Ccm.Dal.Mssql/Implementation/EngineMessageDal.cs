﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25787 : N.Foster
//  Created
// CCM-28512 : N.Foster
//  Changed the default query timeout for message operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using Galleria.Framework.Engine.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IEngineMessageDal
    /// </summary>
    public class EngineMessageDal : Galleria.Framework.Dal.Mssql.DalBase, IEngineMessageDal
    {
        #region Constants
        private const Int32 _commandTimeout = 5; // the command timeout in seconds
        private const String _importTableName = "#tmpMessageDependency";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL BEGIN DROP TABLE [{0}] END";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
            "[MessageDependency_ParentMessageId] [BIGINT] NOT NULL" +
            ")";
        #endregion

        #region DataTransferObject
        /// <summary>
        /// Returns a data reader from a data transfer object
        /// </summary>
        private static EngineMessageDto GetDataTransferObject(SqlDataReader dr)
        {
            return new EngineMessageDto()
            {
                Id = (Int64)GetValue(dr[FieldNames.EngineMessageId]),
                Priority = (Byte)GetValue(dr[FieldNames.EngineMessagePriority]),
                Data = (String)GetValue(dr[FieldNames.EngineMessageData]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.EngineMessageDateCreated]),
                DateScheduled = (DateTime)GetValue(dr[FieldNames.EngineMessageDateScheduled]),
                DateProcessed = (DateTime?)GetValue(dr[FieldNames.EngineMessageDateProcessed])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns the next message in the queue
        /// </summary>
        public EngineMessageDto FetchNext()
        {
            EngineMessageDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageFetchNext))
                {
                    command.CommandTimeout = _commandTimeout;
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dto;
        }

        /// <summary>
        /// Returns a count of the total number of items
        /// within the message queue
        /// </summary>
        public EngineMessageStatsDto FetchStats()
        {
            EngineMessageStatsDto dto = new EngineMessageStatsDto();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageFetchStats))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto.MessageCount = (Int32)GetValue(dr[FieldNames.EngineMessageCount]);
                            dto.MessageProcessCount = (Int32)GetValue(dr[FieldNames.EngineMessageProcessCount]);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dto;
        }

        #endregion

        #region Reap
        /// <summary>
        /// Reaps the next message in the queue
        /// </summary>
        public Boolean ReapNext(Int32 timeout)
        {
            Boolean messageReaped = false;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageReapNext))
                {
                    // set the command timeout
                    command.CommandTimeout = _commandTimeout;

                    // parameters
                    CreateParameter(command, FieldNames.EngineMessageTimeout, SqlDbType.Int, timeout);

                    // execute
                    messageReaped = (Int32)command.ExecuteScalar() > 0;
                }
            }
            catch (Exception ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return messageReaped;
        }

        /// <summary>
        /// Reaps the specified message
        /// </summary>
        public void ReapById(Int64 messageId, Byte messagePriority, Int32 messageDelay)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageReapById))
                {
                    // set the command timeout
                    command.CommandTimeout = _commandTimeout;

                    // parameters
                    CreateParameter(command, FieldNames.EngineMessageId, SqlDbType.BigInt, messageId);
                    CreateParameter(command, FieldNames.EngineMessagePriority, SqlDbType.TinyInt, messagePriority);
                    CreateParameter(command, FieldNames.EngineMessageDelay, SqlDbType.Int, messageDelay);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a message into the message queue
        /// </summary>
        public void Insert(EngineMessageDto dto, Int64 predecessorId, IEnumerable<Int64> parentIds)
        {
            try
            {
                // upload the dependencies if required
                if ((parentIds != null) && (parentIds.Any()))
                {
                    this.CreateTempImportTable();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                    {
                        bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                        bulkCopy.DestinationTableName = String.Format(CultureInfo.InvariantCulture, "[{0}]", _importTableName);
                        IDataReader dr = new BulkCopyDataReader<Int64>(parentIds);
                        bulkCopy.WriteToServer(dr);
                    }
                }

                // now call the insert stored procedure
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.EngineMessageId, SqlDbType.BigInt);
                    CreateParameter(command, FieldNames.EngineMessagePriority, SqlDbType.TinyInt, dto.Priority);
                    CreateParameter(command, FieldNames.EngineMessageData, SqlDbType.NVarChar, dto.Data);
                    CreateParameter(command, FieldNames.EngineMessageDateCreated, SqlDbType.SmallDateTime, dto.DateCreated);
                    CreateParameter(command, FieldNames.EngineMessageDateScheduled, SqlDbType.SmallDateTime, dto.DateScheduled);
                    CreateParameter(command, FieldNames.EngineMessagePredecessorId, SqlDbType.BigInt, predecessorId);

                    // execute
                    command.ExecuteNonQuery();

                    // update the dto
                    dto.Id = (Int64)idParameter.Value;
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            finally
            {
                this.DropTempImportTable();
            }
        }
        #endregion

        #region AddDependencies
        /// <summary>
        /// Adds dependencies to an existing message
        /// </summary>
        public void AddDependencies(Int64 id, IEnumerable<Int64> parentIds)
        {
            if ((parentIds == null) || (!parentIds.Any())) return;
            try
            {
                // upload dependencies
                this.CreateTempImportTable();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = String.Format(CultureInfo.InvariantCulture, "[{0}]", _importTableName);
                    IDataReader dr = new BulkCopyDataReader<Int64>(parentIds);
                    bulkCopy.WriteToServer(dr);
                }

                // now call the insert stored procedure
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageAddDependencies))
                {
                    // parameters
                    CreateParameter(command, FieldNames.EngineMessageId, SqlDbType.BigInt, id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            finally
            {
                this.DropTempImportTable();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified message from the queue
        /// </summary>
        public void Delete(EngineMessageDto messageDto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageDelete))
                {
                    // set the command timeout
                    command.CommandTimeout = _commandTimeout;

                    // parameter
                    CreateParameter(command, FieldNames.EngineMessageId, SqlDbType.BigInt, messageDto.Id);
                    CreateParameter(command, FieldNames.EngineMessagePriority, SqlDbType.TinyInt, messageDto.Priority);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion

        #region Exists
        /// <summary>
        /// Determines if a message exists within the queue
        /// based on the criteria
        /// </summary>
        public Boolean Exists(String criteria)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMessageExistsByCriteria))
                {
                    // parameters
                    CreateParameter(command, FieldNames.EngineMessageCriteria, SqlDbType.NVarChar, criteria);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            return true;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return false;
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Creates the temporary import table
        /// </summary>
        private void CreateTempImportTable()
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the temporary import table
        /// </summary>
        private void DropTempImportTable()
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}
