﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25787 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Added FetchAll()
#endregion
#region Version History: CCM820
// V8-30818 : N.Foster
//  Added FetchStats
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Engine.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class EngineInstanceDal : Galleria.Framework.Dal.Mssql.DalBase, IEngineInstanceDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public EngineInstanceDto GetDataTransferObject(SqlDataReader dr)
        {
            return new EngineInstanceDto()
            {
                Id = (Guid)GetValue(dr[FieldNames.EngineInstanceId]),
                ComputerName = (String)GetValue(dr[FieldNames.EngineInstanceComputerName]),
                WorkerCount = (Int32)GetValue(dr[FieldNames.EngineInstanceWorkerCount]),
                Lifespan = (Int32)GetValue(dr[FieldNames.EngineInstanceLifespan]),
                DateLastActive = (DateTime)GetValue(dr[FieldNames.EngineInstanceDateLastActive])
            };
        }
        #endregion

        #region Fetch

        public IEnumerable<EngineInstanceDto> FetchAll()
        {
            List<EngineInstanceDto> dtoList = new List<EngineInstanceDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineInstanceFetchAll))
                {
                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        public EngineInstanceStatsDto FetchStats()
        {
            EngineInstanceStatsDto dto = new EngineInstanceStatsDto();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineInstanceFetchStats))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto.InstanceCount = (Int32)GetValue(dr[FieldNames.EngineInstanceCount]);
                            dto.InstanceWorkerCount = (Int32)GetValue(dr[FieldNames.EngineInstanceWorkerCount]);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dto;
        }

        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto within the database
        /// </summary>
        public void Update(EngineInstanceDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineInstanceUpdateById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.EngineInstanceId, SqlDbType.UniqueIdentifier, dto.Id);
                    CreateParameter(command, FieldNames.EngineInstanceComputerName, SqlDbType.NVarChar, dto.ComputerName);
                    CreateParameter(command, FieldNames.EngineInstanceWorkerCount, SqlDbType.Int, dto.WorkerCount);
                    CreateParameter(command, FieldNames.EngineInstanceLifespan, SqlDbType.Int, dto.Lifespan);
                    CreateParameter(command, FieldNames.EngineInstanceDateLastActive, SqlDbType.SmallDateTime, dto.DateLastActive);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified instance from the database
        /// </summary>
        public void DeleteById(Guid id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineInstanceDeleteById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.EngineInstanceId, SqlDbType.UniqueIdentifier, id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        /// <summary>
        /// Deletes all inactive instances
        /// </summary>
        public void DeleteInactive()
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineInstanceDeleteInactive))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion
    }
}
