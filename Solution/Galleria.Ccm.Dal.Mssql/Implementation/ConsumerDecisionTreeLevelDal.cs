﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ConsumerDecisionTreeLevel Dal
    /// </summary>
    public class ConsumerDecisionTreeLevelDal : Galleria.Framework.Dal.Mssql.DalBase, IConsumerDecisionTreeLevelDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static ConsumerDecisionTreeLevelDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ConsumerDecisionTreeLevelDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeLevelId]),
                ConsumerDecisionTreeId = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeLevelConsumerDecisionTreeId]),
                Name = (String)GetValue(dr[FieldNames.ConsumerDecisionTreeLevelName]),
                ParentLevelId = (Int32?)GetValue(dr[FieldNames.ConsumerDecisionTreeLevelParentLevelId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeLevelDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeLevelDateLastModified])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ConsumerDecisionTreeLevelDto FetchById(Int32 id)
        {
            ConsumerDecisionTreeLevelDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeLevelFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<ConsumerDecisionTreeLevelDto> FetchByEntityId(Int32 entityId)
        {
            List<ConsumerDecisionTreeLevelDto> dtoList = new List<ConsumerDecisionTreeLevelDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeLevelFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<ConsumerDecisionTreeLevelDto> FetchByConsumerDecisionTreeId(Int32 consumerDecisionTreeId)
        {
            List<ConsumerDecisionTreeLevelDto> dtoList = new List<ConsumerDecisionTreeLevelDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeLevelFetchByConsumerDecisionTreeId))
                {
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelConsumerDecisionTreeId, SqlDbType.Int, consumerDecisionTreeId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ConsumerDecisionTreeLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeLevelInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelConsumerDecisionTreeId, SqlDbType.Int, dto.ConsumerDecisionTreeId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelParentLevelId, SqlDbType.Int, dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ConsumerDecisionTreeLevelDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeLevelUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelConsumerDecisionTreeId, SqlDbType.Int, dto.ConsumerDecisionTreeId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelParentLevelId, SqlDbType.Int, dto.ParentLevelId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeLevelDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeLevelId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}