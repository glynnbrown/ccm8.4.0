﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25608 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Removed value property
// V8-28232 : N.Foster
//  Add support for batch operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkpackagePlanogramParameterDal
    /// </summary>
    public sealed class WorkpackagePlanogramParameterDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackagePlanogramParameterDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a data reader from a data transfer object
        /// </summary>
        private static WorkpackagePlanogramParameterDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackagePlanogramParameterDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramParameterId]),
                WorkpackagePlanogramId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramParameterWorkpackagePlanogramId]),
                WorkflowTaskParameterId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramParameterWorflowTaskParameterId]),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Fetches all items for the specified workpackage planogram
        /// </summary>
        public IEnumerable<WorkpackagePlanogramParameterDto> FetchByWorkpackagePlanogramId(int workpackagePlanogramId)
        {
            List<WorkpackagePlanogramParameterDto> dtoList = new List<WorkpackagePlanogramParameterDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterFetchByWorkpackagePlanogramId))
                {
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterWorkpackagePlanogramId, SqlDbType.Int, workpackagePlanogramId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the specified item into the database
        /// </summary>
        public void Insert(WorkpackagePlanogramParameterDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterInsert))
                {
                    // parameters
                    SqlParameter idParameter = CreateParameter(command, FieldNames.WorkpackagePlanogramParameterId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterWorkpackagePlanogramId, SqlDbType.Int, dto.WorkpackagePlanogramId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterId, SqlDbType.Int, dto.WorkflowTaskParameterId);

                    // execute
                    command.ExecuteNonQuery();

                    // update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        /// <summary>
        /// Inserts a batch of dtos into the database
        /// </summary>
        /// <param name="dtos">The list of dtos to insert</param>
        public void Insert(IEnumerable<WorkpackagePlanogramParameterDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk insert for performance
            foreach (WorkpackagePlanogramParameterDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the specified item within the database
        /// </summary>
        public void Update(WorkpackagePlanogramParameterDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterUpdateById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterWorkpackagePlanogramId, SqlDbType.Int, dto.WorkpackagePlanogramId);
                    CreateParameter(command, FieldNames.WorkflowTaskParameterId, SqlDbType.Int, dto.WorkflowTaskParameterId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        /// <summary>
        /// Updates a batch of dtos in the database
        /// </summary>
        /// <param name="dtos">The list of dtos to update</param>
        public void Update(IEnumerable<WorkpackagePlanogramParameterDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk update for performance
            foreach (WorkpackagePlanogramParameterDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified item from the database
        /// </summary>
        public void DeleteById(Int32 workpackagePlanogramParameterId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramParameterDeleteById))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramParameterId, SqlDbType.Int, workpackagePlanogramParameterId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }

        /// <summary>
        /// Deletes the specifeid item from the database
        /// </summary>
        public void Delete(WorkpackagePlanogramParameterDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes a batch of dtos from the database
        /// </summary>
        /// <param name="dtos">The list of dtos to delete</param>
        public void Delete(IEnumerable<WorkpackagePlanogramParameterDto> dtos)
        {
            // NF - TODO - This needs to be changed to a bulk delete for performance
            foreach (WorkpackagePlanogramParameterDto dto in dtos)
            {
                this.DeleteById(dto.Id);
            }
        }
        #endregion
    }
}
