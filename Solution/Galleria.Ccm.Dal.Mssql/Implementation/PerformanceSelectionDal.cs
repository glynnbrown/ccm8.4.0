﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26159 : L.Ineson
//  Created (Auto-generated)
// V8-26773 : N.Foster
//  Added FetchByName
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PerformanceSelection Dal
    /// </summary>
    public class PerformanceSelectionDal : Galleria.Framework.Dal.Mssql.DalBase, IPerformanceSelectionDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PerformanceSelectionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PerformanceSelectionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PerformanceSelectionId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PerformanceSelectionRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.PerformanceSelectionEntityId]),
                Name = (String)GetValue(dr[FieldNames.PerformanceSelectionName]),
                GFSPerformanceSourceId = (Int32)GetValue(dr[FieldNames.PerformanceSelectionGFSPerformanceSourceId]),
                SelectionType = (Byte)GetValue(dr[FieldNames.PerformanceSelectionSelectionType]),
                TimeType = (Byte)GetValue(dr[FieldNames.PerformanceSelectionTimeType]),
                DynamicValue = (Int32)GetValue(dr[FieldNames.PerformanceSelectionDynamicValue]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.PerformanceSelectionDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.PerformanceSelectionDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.PerformanceSelectionDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PerformanceSelectionDto FetchById(Int32 id)
        {
            PerformanceSelectionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified name
        /// </summary>
        public PerformanceSelectionDto FetchByEntityIdName(Int32 entityId, String name)
        {
            PerformanceSelectionDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionFetchByEntityIdName))
                {
                    // parameters
                    CreateParameter(command, FieldNames.PerformanceSelectionEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.PerformanceSelectionName, SqlDbType.NVarChar, name);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PerformanceSelectionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PerformanceSelectionId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PerformanceSelectionRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PerformanceSelectionDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.PerformanceSelectionDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.PerformanceSelectionDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.PerformanceSelectionEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PerformanceSelectionName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PerformanceSelectionGFSPerformanceSourceId, SqlDbType.Int, dto.GFSPerformanceSourceId);
                    CreateParameter(command, FieldNames.PerformanceSelectionSelectionType, SqlDbType.TinyInt, dto.SelectionType);
                    CreateParameter(command, FieldNames.PerformanceSelectionTimeType, SqlDbType.TinyInt, dto.TimeType);
                    CreateParameter(command, FieldNames.PerformanceSelectionDynamicValue, SqlDbType.Int, dto.DynamicValue);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PerformanceSelectionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.PerformanceSelectionRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.PerformanceSelectionDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.PerformanceSelectionDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    SqlParameter dateDeletedParameter = CreateParameter(command, FieldNames.PerformanceSelectionDateDeleted, SqlDbType.DateTime, ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties 
                    CreateParameter(command, FieldNames.PerformanceSelectionEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PerformanceSelectionName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PerformanceSelectionGFSPerformanceSourceId, SqlDbType.Int, dto.GFSPerformanceSourceId);
                    CreateParameter(command, FieldNames.PerformanceSelectionSelectionType, SqlDbType.TinyInt, dto.SelectionType);
                    CreateParameter(command, FieldNames.PerformanceSelectionTimeType, SqlDbType.TinyInt, dto.TimeType);
                    CreateParameter(command, FieldNames.PerformanceSelectionDynamicValue, SqlDbType.Int, dto.DynamicValue);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value ? null : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}