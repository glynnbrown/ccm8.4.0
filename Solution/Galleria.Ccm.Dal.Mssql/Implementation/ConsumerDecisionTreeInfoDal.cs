﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-25455 : J.Pickup
//		Added FetchById
// V8-26520 : J.Pickup
//		Added FetchByProductGroupId
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ConsumerDecisionTreeInfo Dal
    /// </summary>
    public class ConsumerDecisionTreeInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IConsumerDecisionTreeInfoDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ConsumerDecisionTreeInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ConsumerDecisionTreeInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeId]),
                Name = (String)GetValue(dr[FieldNames.ConsumerDecisionTreeName]),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.ConsumerDecisionTreeUniqueContentReference]),
                ProductGroupId = (Int32?)GetValue(dr[FieldNames.ConsumerDecisionTreeProductGroupId]),
                EntityId = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeEntityId]),
                ProductGroupCode = (String)GetValue(dr[FieldNames.ProductGroupCode]),
                ProductGroupName = (String)GetValue(dr[FieldNames.ProductGroupName]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.ConsumerDecisionTreeParentUniqueContentReference])
            };
        }

        #endregion

        #region Nested Classes

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        private class BulkCopyIntDataReader : IDataReader
        {
            #region Fields
            IEnumerator<Int32> _enumerator; // an enumerator containing the values to write to the database
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyIntDataReader(IEnumerable<Int32> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = _enumerator.Current;
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns the dtos that match the specified entity id
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ConsumerDecisionTreeInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<ConsumerDecisionTreeInfoDto> dtoList = new List<ConsumerDecisionTreeInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeInfoFetchByEntityId))
                {
                    //EntityId
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        /// <summary>
        /// Returns the dtos that match the specified product group
        /// </summary>
        /// <param name="entityId">specified product group id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<ConsumerDecisionTreeInfoDto> FetchByProductGroupId(Int32 productGroupId)
        {
            List<ConsumerDecisionTreeInfoDto> dtoList = new List<ConsumerDecisionTreeInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeInfoFetchByProductGroupId))
                {
                    //ProductGroupId
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeProductGroupId, SqlDbType.Int, productGroupId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }


        public ConsumerDecisionTreeInfoDto FetchById(Int32 consumerDecisionTreeId)
        {
            ConsumerDecisionTreeInfoDto dto = null;
            try
            {
                //execute
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeInfoFetchById))
                {

                    //ConsumerDecisionTree
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeId, SqlDbType.Int, consumerDecisionTreeId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            //return items
            return dto;
        }
        
        #endregion
    }
}