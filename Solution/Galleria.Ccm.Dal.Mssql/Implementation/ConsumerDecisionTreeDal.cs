﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-25453 : A.Kuszyk
//      Added FetchByEntityIdConsumerDecisionTreeId.
// V8-27132 : A.Kuszyk
//      Added FetchByEntityIdName.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ConsumerDecisionTree Dal
    /// </summary>
    public class ConsumerDecisionTreeDal : Galleria.Framework.Dal.Mssql.DalBase, IConsumerDecisionTreeDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ConsumerDecisionTreeDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ConsumerDecisionTreeDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ConsumerDecisionTreeRowVersion])),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.ConsumerDecisionTreeUniqueContentReference]),
                Name = (String)GetValue(dr[FieldNames.ConsumerDecisionTreeName]),
                EntityId = (Int32)GetValue(dr[FieldNames.ConsumerDecisionTreeEntityId]),
                ProductGroupId = (Int32?)GetValue(dr[FieldNames.ConsumerDecisionTreeProductGroupId]),
                Type = (Byte)GetValue(dr[FieldNames.ConsumerDecisionTreeType]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ConsumerDecisionTreeDateLastModified]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.ConsumerDecisionTreeParentUniqueContentReference])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ConsumerDecisionTreeDto FetchById(Int32 id)
        {
            ConsumerDecisionTreeDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public ConsumerDecisionTreeDto FetchByEntityIdName(Int32 entityId, String name)
        {
            ConsumerDecisionTreeDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeName, SqlDbType.NVarChar, name);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<ConsumerDecisionTreeDto> FetchByEntityIdConsumerDecisionTreeIds(Int32 entityId, IEnumerable<Int32> consumerDecisionTreeIds)
        {
            List<ConsumerDecisionTreeDto> dtoList = new List<ConsumerDecisionTreeDto>();

            // create a temp table for consumer decision trees
            CreateConsumerDecisionTreeTempTable(consumerDecisionTreeIds);

            try
            {
                // execute
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeFetchByEntityIdConsumerDecisionTreeIds))
                {
                    // entity id
                    CreateParameter(command,
                        FieldNames.EntityId,
                        SqlDbType.Int,
                        entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop the temporary table
            DropConsumerDecisionTreeTempTable();

            // and return the items
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ConsumerDecisionTreeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeId, SqlDbType.Int);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeRowVersion, SqlDbType.Timestamp);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeUniqueContentReference, SqlDbType.UniqueIdentifier, dto.UniqueContentReference);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ConsumerDecisionTreeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeId, SqlDbType.Int, dto.Id);

                    //Row version
                    SqlParameter rowVersionParameter = CreateParameter(command, FieldNames.ConsumerDecisionTreeRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //Other properties 
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeUniqueContentReference, SqlDbType.UniqueIdentifier, dto.UniqueContentReference);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeProductGroupId, SqlDbType.Int, dto.ProductGroupId);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeParentUniqueContentReference, SqlDbType.UniqueIdentifier, dto.ParentUniqueContentReference);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ConsumerDecisionTreeDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ConsumerDecisionTreeId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateConsumerDecisionTreeTempTable(IEnumerable<Int32> consumerDecisionTreeIds)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpConsumerDecisionTrees (ConsumerDecisionTree_Id INT)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpConsumerDecisionTrees (ConsumerDecisionTree_Id) VALUES (@ConsumerDecisionTree_Id)", CommandType.Text))
            {
                // ConsumerDecisionTree Id Parameter
                SqlParameter consumerDecisionTreeIdParameter = CreateParameter(command,
                    "@ConsumerDecisionTree_Id",
                    SqlDbType.Int,
                    string.Empty);

                // insert the consumer decision tree ids
                foreach (Int32 consumerDecisionTreeId in consumerDecisionTreeIds)
                {
                    consumerDecisionTreeIdParameter.Value = consumerDecisionTreeId;
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Drops the store temporary table on the given connection
        /// </summary>
        /// <param name="connection">SQL connection to use</param>
        private void DropConsumerDecisionTreeTempTable()
        {
            using (DalCommand command = CreateCommand("DROP TABLE #tmpConsumerDecisionTrees", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}