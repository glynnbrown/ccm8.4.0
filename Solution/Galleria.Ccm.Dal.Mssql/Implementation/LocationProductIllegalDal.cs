﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History: (CCM 8.03)
// V8-29268 : L.Ineson
//  Removed date deleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class LocationProductIllegalDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationProductIllegalDal
    {
        #region Constants

        private const String _importTableName = "#tmpLocationProductIllegal";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
            "[Product_Id] [INT] NOT NULL," +
            "[Location_Id] [SMALLINT] NOT NULL," +
            "[Entity_Id] [INT] NOT NULL" +
            ")";

        private const String _createFetchCriteriaTableSql =
               "CREATE TABLE  [{0}](" +
               "[Location_Id] [SMALLINT] NOT NULL, " +
               "[Product_Id] [INT] NOT NULL" +
               ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        #endregion

        #region Nested Classes

        private class LocationProductIllegalBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<LocationProductIllegalDto> _enumerator; //The enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">the values to insert</param>
            public LocationProductIllegalBulkCopyDataReader(IEnumerable<LocationProductIllegalDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return 3; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            public object GetValue(Int32 i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.ProductId;
                        break;

                    case 1:
                        value = _enumerator.Current.LocationId;
                        break;

                    case 2:
                        value = _enumerator.Current.EntityId;
                        break;

                    default:
                        value = null;
                        break;
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }

            #endregion

            #region NotImplemented

            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }

            #endregion
        }


        private class LocationProductIllegalBulkCopyFetchCriteriaDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<Tuple<Int16, Int32>> _enumerator; //The enumerator containing fetch criteria
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">the values to insert</param>
            public LocationProductIllegalBulkCopyFetchCriteriaDataReader(IEnumerable<Tuple<Int16, Int32>> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return 2; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            public object GetValue(Int32 i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Item1; //location
                        break;

                    case 1:
                        value = _enumerator.Current.Item2; //product
                        break;

                    default:
                        value = null;
                        break;
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }

            #endregion

            #region NotImplemented

            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }

            #endregion
        }

        #endregion

        #region DataTransferObject

        private LocationProductIllegalDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationProductIllegalDto
            {
                ProductId = (Int32)GetValue(dr[FieldNames.LocationProductIllegalProductId]),
                LocationId = (Int16)GetValue(dr[FieldNames.LocationProductIllegalLocationId]),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationProductIllegalEntityId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.LocationProductIllegalRowVersion])),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LocationProductIllegalDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LocationProductIllegalDateLastModified]),
            };
        }

        #endregion

        #region Fetch

        public IEnumerable<LocationProductIllegalDto> FetchByEntityId(Int32 entityId)
        {
            List<LocationProductIllegalDto> dtoList = new List<LocationProductIllegalDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationProductIllegalEntityId, SqlDbType.Int, entityId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        public LocationProductIllegalDto FetchById(Int16 locationId, Int32 productId)
        {
            LocationProductIllegalDto dto = null;

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalFetchById))
                {
                    CreateParameter(command, FieldNames.LocationProductIllegalLocationId, SqlDbType.SmallInt, locationId);
                    CreateParameter(command, FieldNames.LocationProductIllegalProductId, SqlDbType.Int, productId);

                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        public List<LocationProductIllegalDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> locationProductIdCombinations)
        {
            List<LocationProductIllegalDto> dtoList = new List<LocationProductIllegalDto>();
            try
            {
                // create the temporary table
                CreateTempImportTable(_createFetchCriteriaTableSql, _importTableName);

                //perform batch insert of combinations to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationProductIllegalBulkCopyFetchCriteriaDataReader dr = new LocationProductIllegalBulkCopyFetchCriteriaDataReader(locationProductIdCombinations);
                    bulkCopy.WriteToServer(dr);
                }

                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalFetchByLocationIdProductIdCombinations))
                {
                    //execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropTempImportTable(_importTableName);
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(LocationProductIllegalDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalInsert))
                {
                    //Add parameters
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.LocationProductIllegalRowVersion, SqlDbType.Timestamp);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationProductIllegalDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationProductIllegalDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    CreateParameter(command, FieldNames.LocationProductIllegalLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.LocationProductIllegalProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.LocationProductIllegalEntityId, SqlDbType.Int, dto.EntityId);

                    command.ExecuteNonQuery();

                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        public void Update(LocationProductIllegalDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalUpdate))
                {
                    // Parameter 
                    SqlParameter versionParameter = CreateParameter(command, FieldNames.LocationProductIllegalRowVersion, SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);
                    SqlParameter dateCreatedParameter = CreateParameter(command, FieldNames.LocationProductIllegalDateCreated, SqlDbType.DateTime, ParameterDirection.Output, dto.DateCreated);
                    SqlParameter dateLastModifiedParameter = CreateParameter(command, FieldNames.LocationProductIllegalDateLastModified, SqlDbType.DateTime, ParameterDirection.Output, dto.DateLastModified);

                    CreateParameter(command, FieldNames.LocationProductIllegalLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.LocationProductIllegalProductId, SqlDbType.Int, dto.ProductId);
                    CreateParameter(command, FieldNames.LocationProductIllegalEntityId, SqlDbType.Int, dto.EntityId);

                    command.ExecuteNonQuery();

                    dto.RowVersion = new RowVersion(versionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        public void Upsert(IEnumerable<LocationProductIllegalDto> dtoList)
        {
            //to avoid duplicates, we create a dictionary of dtos
            //to ensure that only the last dto is actually inserted
            Dictionary<LocationProductIllegalIdSearchCriteria, LocationProductIllegalDto> existingDtoList =
                new Dictionary<LocationProductIllegalIdSearchCriteria, LocationProductIllegalDto>();
            foreach (LocationProductIllegalDto dto in dtoList)
            {
                //Create criteria
                LocationProductIllegalIdSearchCriteria criteria = new LocationProductIllegalIdSearchCriteria()
                {
                    LocationId = dto.LocationId,
                    ProductId = dto.ProductId
                };

                //add to the dto list
                if (existingDtoList.ContainsKey(criteria))
                {
                    existingDtoList[criteria] = dto;
                }
                else
                {
                    existingDtoList.Add(criteria, dto);
                }
            }

            try
            {
                CreateTempImportTable(_createImportTableSql, _importTableName);

                //perform batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    LocationProductIllegalBulkCopyDataReader dr = new LocationProductIllegalBulkCopyDataReader(existingDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //perform a batch import
                Dictionary<Tuple<Int32, Int16>, RowVersion> rowVersions = new Dictionary<Tuple<Int32, Int16>, RowVersion>();
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalBulkUpsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Tuple<Int32, Int16> outputData =
                                new Tuple<Int32, Int16>
                                    (
                                        (Int32)GetValue(dr[FieldNames.LocationProductIllegalProductId]),
                                        (Int16)GetValue(dr[FieldNames.LocationProductIllegalLocationId])
                                    );
                            rowVersions.Add(outputData, new RowVersion(GetValue(dr[FieldNames.LocationProductIllegalRowVersion])));
                        }
                    }
                }

                //Assign each dto it's rowVersion from the dictionary
                foreach (LocationProductIllegalDto dto in dtoList)
                {
                    RowVersion rowVersion;
                    rowVersions.TryGetValue(new Tuple<Int32, Int16>(dto.ProductId, dto.LocationId), out rowVersion);

                    if (rowVersion != null)
                    {
                        dto.RowVersion = rowVersion;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropTempImportTable(_importTableName);
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int16 locationId, Int32 productId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalDeleteById))
                {
                    CreateParameter(command, FieldNames.LocationProductIllegalLocationId, SqlDbType.SmallInt, locationId);
                    CreateParameter(command, FieldNames.LocationProductIllegalProductId, SqlDbType.Int, productId);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        public void DeleteByEntityId(Int32 entityId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationProductIllegalDeleteByEntityId))
                {
                    CreateParameter(command, FieldNames.LocationProductIllegalEntityId, SqlDbType.Int, entityId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        private void CreateTempImportTable(String sql, String tempTableName)
        {
            try
            {
                using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, sql, tempTableName), CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }


        /// <summary>
        /// Drops the sepecified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropTempImportTable(String tempTableName)
        {
            try
            {
                using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, tempTableName), CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch { }
        }
        #endregion
    }
}
