﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS. 
// V8-27474 : A.Silva
//  Added PositionSequenceNumber.
// V8-27823 : L.Ineson
//  Facings properties are now Int16
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM802
// V8-28766 : J.Pickup
//  IsManuallyPlaced property introduced
// V8-29028 : L.Ineson
// Added squeeze properties.
// V8-29054 : M.Pettit
//  Added more meta properties for validation storage
#endregion
#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
#endregion
#region Version History: CCM820
// V8-31164 : D.Pleasance
//  Added SequenceNumber \ SequenceColour
#endregion

#region Version History: CCM830
// V8-31947 : A.Silva
//  Added new meta data field, MetaComparisonStatus.
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName & MetaSequenceGroupName.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramPositionDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramPositionDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogramPosition";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
		        "[PlanogramPosition_Id] [INT] NOT NULL, " +
 		        "[Planogram_Id] [INT] NOT NULL, " +
 		        "[PlanogramFixtureItem_Id] [INT] NOT NULL, " +
 		        "[PlanogramFixtureAssembly_Id] [INT] NULL, " +
 		        "[PlanogramAssemblyComponent_Id] [INT] NULL, " +
 		        "[PlanogramFixtureComponent_Id] [INT] NULL, " +
 		        "[PlanogramSubComponent_Id] [INT] NOT NULL, " +
 		        "[PlanogramProduct_Id] [INT] NOT NULL, " +
 		        "[PlanogramPosition_X] [REAL] NOT NULL, " +
 		        "[PlanogramPosition_Y] [REAL] NOT NULL, " +
 		        "[PlanogramPosition_Z] [REAL] NOT NULL, " +
 		        "[PlanogramPosition_Slope] [REAL] NOT NULL, " +
 		        "[PlanogramPosition_Angle] [REAL] NOT NULL, " +
 		        "[PlanogramPosition_Roll] [REAL] NOT NULL, " +
 		        "[PlanogramPosition_FacingsHigh] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsWide] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsDeep] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_OrientationType] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_MerchandisingStyle] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsXHigh] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsXWide] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsXDeep] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_MerchandisingStyleX] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_OrientationTypeX] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_IsXPlacedLeft] [BIT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsYHigh] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsYWide] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsYDeep] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_MerchandisingStyleY] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_OrientationTypeY] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_IsYPlacedBottom] [BIT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsZHigh] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsZWide] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_FacingsZDeep] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_MerchandisingStyleZ] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_OrientationTypeZ] [TINYINT] NOT NULL, " +
 		        "[PlanogramPosition_IsZPlacedFront] [BIT] NOT NULL, " +
 		        "[PlanogramPosition_Sequence] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_SequenceX] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_SequenceY] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_SequenceZ] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_UnitsHigh] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_UnitsWide] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_UnitsDeep] [SMALLINT] NOT NULL, " +
 		        "[PlanogramPosition_TotalUnits] [INT] NOT NULL, " +
		        "[PlanogramPosition_MetaIsPositionCollisions] [BIT] NULL, " +
		        "[PlanogramPosition_MetaAchievedCases] [REAL] NULL, " +
		        "[PlanogramPosition_MetaFrontFacingsWide] [SMALLINT] NULL, " +
                "[PlanogramPosition_PositionSequenceNumber] [SMALLINT] NULL, " +
                "[PlanogramPosition_IsManuallyPlaced] [BIT] NOT NULL, " +
                "[PlanogramPosition_HorizontalSqueeze] [REAL] NOT NULL, " +
                "[PlanogramPosition_VerticalSqueeze] [REAL] NOT NULL, " +
                "[PlanogramPosition_DepthSqueeze] [REAL] NOT NULL, " +
                "[PlanogramPosition_HorizontalSqueezeX] [REAL] NOT NULL, " +
                "[PlanogramPosition_VerticalSqueezeX] [REAL] NOT NULL, " +
                "[PlanogramPosition_DepthSqueezeX] [REAL] NOT NULL, " +
                "[PlanogramPosition_HorizontalSqueezeY] [REAL] NOT NULL, " +
                "[PlanogramPosition_VerticalSqueezeY] [REAL] NOT NULL, " +
                "[PlanogramPosition_DepthSqueezeY] [REAL] NOT NULL, " +
                "[PlanogramPosition_HorizontalSqueezeZ] [REAL] NOT NULL, " +
                "[PlanogramPosition_VerticalSqueezeZ] [REAL] NOT NULL, " +
                "[PlanogramPosition_DepthSqueezeZ] [REAL] NOT NULL, " + 
                "[PlanogramPosition_MetaIsUnderMinDeep] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsOverMaxDeep] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsOverMaxRightCap] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsOverMaxStack] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsOverMaxTopCap] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsInvalidMerchandisingStyle] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsHangingTray] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsPegOverfilled] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsOutsideMerchandisingSpace] [BIT] NULL, " +
                "[PlanogramPosition_MetaIsOutsideOfBlockSpace] [BIT] NULL, " +
                "[PlanogramPosition_MetaHeight] [REAL] NULL, " +
                "[PlanogramPosition_MetaWidth] [REAL] NULL, " +
                "[PlanogramPosition_MetaDepth] [REAL] NULL, " +
                "[PlanogramPosition_MetaWorldX] [REAL] NULL, " +
                "[PlanogramPosition_MetaWorldY] [REAL] NULL, " +
                "[PlanogramPosition_MetaWorldZ] [REAL] NULL, " +
                "[PlanogramPosition_MetaTotalLinearSpace] [REAL] NULL, " +
                "[PlanogramPosition_MetaTotalAreaSpace] [REAL] NULL, " +
                "[PlanogramPosition_MetaTotalVolumetricSpace] [REAL] NULL, " +
                "[PlanogramPosition_MetaPlanogramLinearSpacePercentage] [REAL] NULL, " +
                "[PlanogramPosition_MetaPlanogramAreaSpacePercentage] [REAL] NULL, " +
                "[PlanogramPosition_MetaPlanogramVolumetricSpacePercentage] [REAL] NULL, " +
                "[PlanogramPosition_SequenceNumber] [INT] NULL, " +
                "[PlanogramPosition_SequenceColour] [INT] NULL, " +
                "[PlanogramPosition_MetaComparisonStatus] [TINYINT] NOT NULL, " +
                "[PlanogramPosition_MetaSequenceSubGroupName] [NVARCHAR](100) NULL," +
                "[PlanogramPosition_MetaSequenceGroupName] [NVARCHAR](50) NULL," +
                "[PlanogramPosition_MetaPegRowNumber] [INT] NULL," +
                "[PlanogramPosition_MetaPegColumnNumber] [INT] NULL" +
            ")";
               
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramPositionBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<PlanogramPositionDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramPositionBulkCopyDataReader(IEnumerable<PlanogramPositionDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 91; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.PlanogramId;
                        break;
                    case 2:
                        value = _enumerator.Current.PlanogramFixtureItemId;
                        break;
                    case 3:
                        value = _enumerator.Current.PlanogramFixtureAssemblyId;
                        break;
                    case 4:
                        value = _enumerator.Current.PlanogramAssemblyComponentId;
                        break;
                    case 5:
                        value = _enumerator.Current.PlanogramFixtureComponentId;
                        break;
                    case 6:
                        value = _enumerator.Current.PlanogramSubComponentId;
                        break;
                    case 7:
                        value = _enumerator.Current.PlanogramProductId;
                        break;
                    case 8:
                        value = _enumerator.Current.X;
                        break;
                    case 9:
                        value = _enumerator.Current.Y;
                        break;
                    case 10:
                        value = _enumerator.Current.Z;
                        break;
                    case 11:
                        value = _enumerator.Current.Slope;
                        break;
                    case 12:
                        value = _enumerator.Current.Angle;
                        break;
                    case 13:
                        value = _enumerator.Current.Roll;
                        break;
                    case 14:
                        value = _enumerator.Current.FacingsHigh;
                        break;
                    case 15:
                        value = _enumerator.Current.FacingsWide;
                        break;
                    case 16:
                        value = _enumerator.Current.FacingsDeep;
                        break;
                    case 17:
                        value = _enumerator.Current.OrientationType;
                        break;
                    case 18:
                        value = _enumerator.Current.MerchandisingStyle;
                        break;
                    case 19:
                        value = _enumerator.Current.FacingsXHigh;
                        break;
                    case 20:
                        value = _enumerator.Current.FacingsXWide;
                        break;
                    case 21:
                        value = _enumerator.Current.FacingsXDeep;
                        break;
                    case 22:
                        value = _enumerator.Current.MerchandisingStyleX;
                        break;
                    case 23:
                        value = _enumerator.Current.OrientationTypeX;
                        break;
                    case 24:
                        value = _enumerator.Current.IsXPlacedLeft;
                        break;
                    case 25:
                        value = _enumerator.Current.FacingsYHigh;
                        break;
                    case 26:
                        value = _enumerator.Current.FacingsYWide;
                        break;
                    case 27:
                        value = _enumerator.Current.FacingsYDeep;
                        break;
                    case 28:
                        value = _enumerator.Current.MerchandisingStyleZ;
                        break;
                    case 29:
                        value = _enumerator.Current.OrientationTypeY;
                        break;
                    case 30:
                        value = _enumerator.Current.IsYPlacedBottom;
                        break;
                    case 31:
                        value = _enumerator.Current.FacingsZHigh;
                        break;
                    case 32:
                        value = _enumerator.Current.FacingsZWide;
                        break;
                    case 33:
                        value = _enumerator.Current.FacingsZDeep;
                        break;
                    case 34:
                        value = _enumerator.Current.MerchandisingStyleZ;
                        break;
                    case 35:
                        value = _enumerator.Current.OrientationTypeZ;
                        break;
                    case 36:
                        value = _enumerator.Current.IsZPlacedFront;
                        break;
                    case 37:
                        value = _enumerator.Current.Sequence;
                        break;
                    case 38:
                        value = _enumerator.Current.SequenceX;
                        break;
                    case 39:
                        value = _enumerator.Current.SequenceY;
                        break;
                    case 40:
                        value = _enumerator.Current.SequenceZ;
                        break;
                    case 41:
                        value = _enumerator.Current.UnitsHigh;
                        break;
                    case 42:
                        value = _enumerator.Current.UnitsWide;
                        break;
                    case 43:
                        value = _enumerator.Current.UnitsDeep;
                        break;
                    case 44:
                        value = _enumerator.Current.TotalUnits;
                        break;
                    case 45:
                        value = _enumerator.Current.MetaIsPositionCollisions;
                        break;
                    case 46:
                        value = _enumerator.Current.MetaAchievedCases;
                        break;
                    case 47:
                        value = _enumerator.Current.MetaFrontFacingsWide;
                        break;
                    case 48:
                        value = _enumerator.Current.PositionSequenceNumber;
                        break;
                    case 49:
                        value = _enumerator.Current.IsManuallyPlaced;
                        break;
                    case 50:
                        value = _enumerator.Current.HorizontalSqueeze;
                        break;
                    case 51:
                        value = _enumerator.Current.VerticalSqueeze;
                        break;
                    case 52:
                        value = _enumerator.Current.DepthSqueeze;
                        break;
                    case 53:
                        value = _enumerator.Current.HorizontalSqueezeX;
                        break;
                    case 54:
                        value = _enumerator.Current.VerticalSqueezeX;
                        break;
                    case 55:
                        value = _enumerator.Current.DepthSqueezeX;
                        break;
                    case 56:
                        value = _enumerator.Current.HorizontalSqueezeY;
                        break;
                    case 57:
                        value = _enumerator.Current.VerticalSqueezeY;
                        break;
                    case 58:
                        value = _enumerator.Current.DepthSqueezeY;
                        break;
                    case 59:
                        value = _enumerator.Current.HorizontalSqueezeZ;
                        break;
                    case 60:
                        value = _enumerator.Current.VerticalSqueezeZ;
                        break;
                    case 61:
                        value = _enumerator.Current.DepthSqueezeZ;
                        break;
                    case 62:
                        value = _enumerator.Current.MetaIsUnderMinDeep;
                        break;
                    case 63:
                        value = _enumerator.Current.MetaIsOverMaxDeep;
                        break;
                    case 64:
                        value = _enumerator.Current.MetaIsOverMaxRightCap;
                        break;
                    case 65:
                        value = _enumerator.Current.MetaIsOverMaxStack;
                        break;
                    case 66:
                        value = _enumerator.Current.MetaIsOverMaxTopCap;
                        break;
                    case 67:
                        value = _enumerator.Current.MetaIsInvalidMerchandisingStyle;
                        break;
                    case 68:
                        value = _enumerator.Current.MetaIsHangingTray;
                        break;
                    case 69:
                        value = _enumerator.Current.MetaIsPegOverfilled;
                        break;
                    case 70:
                        value = _enumerator.Current.MetaIsOutsideMerchandisingSpace;
                        break;
                    case 71:
                        value = _enumerator.Current.MetaIsOutsideOfBlockSpace;
                        break;
                    case 72:
                        value = _enumerator.Current.MetaHeight;
                        break;
                    case 73:
                        value = _enumerator.Current.MetaWidth;
                        break;
                    case 74:
                        value = _enumerator.Current.MetaDepth;
                        break;
                    case 75:
                        value = _enumerator.Current.MetaWorldX;
                        break;
                    case 76:
                        value = _enumerator.Current.MetaWorldY;
                        break;
                    case 77:
                        value = _enumerator.Current.MetaWorldZ;
                        break;
                    case 78:
                        value = _enumerator.Current.MetaTotalLinearSpace;
                        break;
                    case 79:
                        value = _enumerator.Current.MetaTotalAreaSpace;
                        break;
                    case 80:
                        value = _enumerator.Current.MetaTotalVolumetricSpace;
                        break;
                    case 81:
                        value = _enumerator.Current.MetaPlanogramLinearSpacePercentage;
                        break;
                    case 82:
                        value = _enumerator.Current.MetaPlanogramAreaSpacePercentage;
                        break;
                    case 83:
                        value = _enumerator.Current.MetaPlanogramVolumetricSpacePercentage;
                        break;
                    case 84:
                        value = _enumerator.Current.SequenceNumber;
                        break;
                    case 85:
                        value = _enumerator.Current.SequenceColour;
                        break;
                    case 86:
                        value = _enumerator.Current.MetaComparisonStatus;
                        break;

                    case 87:
                        value = _enumerator.Current.MetaSequenceSubGroupName;
                        break;

                    case 88:
                        value = _enumerator.Current.MetaSequenceGroupName;
                        break;

                    case 89:
                        value = _enumerator.Current.MetaPegRowNumber;
                        break;

                    case 90:
                        value = _enumerator.Current.MetaPegColumnNumber;
                        break;

                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramPositionDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramPositionDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramPositionId]),
                PlanogramId = (Object)GetValue(dr[FieldNames.PlanogramPositionPlanogramId]),
                PlanogramFixtureItemId = (Object)GetValue(dr[FieldNames.PlanogramPositionPlanogramFixtureItemId]),
                PlanogramFixtureAssemblyId = (Object)GetValue(dr[FieldNames.PlanogramPositionPlanogramFixtureAssemblyId]),
                PlanogramAssemblyComponentId = (Object)GetValue(dr[FieldNames.PlanogramPositionPlanogramAssemblyComponentId]),
                PlanogramFixtureComponentId = (Object)GetValue(dr[FieldNames.PlanogramPositionPlanogramFixtureComponentId]),
                PlanogramSubComponentId = (Object)GetValue(dr[FieldNames.PlanogramPositionPlanogramSubComponentId]),
                PlanogramProductId = (Object)GetValue(dr[FieldNames.PlanogramPositionPlanogramProductId]),
                X = (Single)GetValue(dr[FieldNames.PlanogramPositionX]),
                Y = (Single)GetValue(dr[FieldNames.PlanogramPositionY]),
                Z = (Single)GetValue(dr[FieldNames.PlanogramPositionZ]),
                Slope = (Single)GetValue(dr[FieldNames.PlanogramPositionSlope]),
                Angle = (Single)GetValue(dr[FieldNames.PlanogramPositionAngle]),
                Roll = (Single)GetValue(dr[FieldNames.PlanogramPositionRoll]),
                FacingsHigh = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsHigh]),
                FacingsWide = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsWide]),
                FacingsDeep = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsDeep]),
                OrientationType = (Byte)GetValue(dr[FieldNames.PlanogramPositionOrientationType]),
                MerchandisingStyle = (Byte)GetValue(dr[FieldNames.PlanogramPositionMerchandisingStyle]),
                FacingsXHigh = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsXHigh]),
                FacingsXWide = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsXWide]),
                FacingsXDeep = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsXDeep]),
                MerchandisingStyleX = (Byte)GetValue(dr[FieldNames.PlanogramPositionMerchandisingStyleX]),
                OrientationTypeX = (Byte)GetValue(dr[FieldNames.PlanogramPositionOrientationTypeX]),
                IsXPlacedLeft = (Boolean)GetValue(dr[FieldNames.PlanogramPositionIsXPlacedLeft]),
                FacingsYHigh = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsYHigh]),
                FacingsYWide = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsYWide]),
                FacingsYDeep = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsYDeep]),
                MerchandisingStyleY = (Byte)GetValue(dr[FieldNames.PlanogramPositionMerchandisingStyleY]),
                OrientationTypeY = (Byte)GetValue(dr[FieldNames.PlanogramPositionOrientationTypeY]),
                IsYPlacedBottom = (Boolean)GetValue(dr[FieldNames.PlanogramPositionIsYPlacedBottom]),
                FacingsZHigh = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsZHigh]),
                FacingsZWide = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsZWide]),
                FacingsZDeep = (Int16)GetValue(dr[FieldNames.PlanogramPositionFacingsZDeep]),
                MerchandisingStyleZ = (Byte)GetValue(dr[FieldNames.PlanogramPositionMerchandisingStyleZ]),
                OrientationTypeZ = (Byte)GetValue(dr[FieldNames.PlanogramPositionOrientationTypeZ]),
                IsZPlacedFront = (Boolean)GetValue(dr[FieldNames.PlanogramPositionIsZPlacedFront]),
                Sequence = (Int16)GetValue(dr[FieldNames.PlanogramPositionSequence]),
                SequenceX = (Int16)GetValue(dr[FieldNames.PlanogramPositionSequenceX]),
                SequenceY = (Int16)GetValue(dr[FieldNames.PlanogramPositionSequenceY]),
                SequenceZ = (Int16)GetValue(dr[FieldNames.PlanogramPositionSequenceZ]),
                UnitsHigh = (Int16)GetValue(dr[FieldNames.PlanogramPositionUnitsHigh]),
                UnitsWide = (Int16)GetValue(dr[FieldNames.PlanogramPositionUnitsWide]),
                UnitsDeep = (Int16)GetValue(dr[FieldNames.PlanogramPositionUnitsDeep]),
                TotalUnits = (Int32)GetValue(dr[FieldNames.PlanogramPositionTotalUnits]),
                MetaIsPositionCollisions = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsPositionCollisions]),
                MetaAchievedCases = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaAchievedCases]),
                MetaFrontFacingsWide = (Int16?)GetValue(dr[FieldNames.PlanogramPositionMetaFrontFacingsWide]),
                PositionSequenceNumber = (Int16?)GetValue(dr[FieldNames.PlanogramPositionPositionSequenceNumber]),
                IsManuallyPlaced = (Boolean)GetValue(dr[FieldNames.PlanogramPositionIsManuallyPlaced]),
                HorizontalSqueeze = (Single)GetValue(dr[FieldNames.PlanogramPositionHorizontalSqueeze]),
                VerticalSqueeze = (Single)GetValue(dr[FieldNames.PlanogramPositionVerticalSqueeze]),
                DepthSqueeze = (Single)GetValue(dr[FieldNames.PlanogramPositionDepthSqueeze]),
                HorizontalSqueezeX = (Single)GetValue(dr[FieldNames.PlanogramPositionHorizontalSqueezeX]),
                VerticalSqueezeX = (Single)GetValue(dr[FieldNames.PlanogramPositionVerticalSqueezeX]),
                DepthSqueezeX = (Single)GetValue(dr[FieldNames.PlanogramPositionDepthSqueezeX]),
                HorizontalSqueezeY = (Single)GetValue(dr[FieldNames.PlanogramPositionHorizontalSqueezeY]),
                VerticalSqueezeY = (Single)GetValue(dr[FieldNames.PlanogramPositionVerticalSqueezeY]),
                DepthSqueezeY = (Single)GetValue(dr[FieldNames.PlanogramPositionDepthSqueezeY]),
                HorizontalSqueezeZ = (Single)GetValue(dr[FieldNames.PlanogramPositionHorizontalSqueezeZ]),
                VerticalSqueezeZ = (Single)GetValue(dr[FieldNames.PlanogramPositionVerticalSqueezeZ]),
                DepthSqueezeZ = (Single)GetValue(dr[FieldNames.PlanogramPositionDepthSqueezeZ]),
                MetaIsUnderMinDeep = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsUnderMinDeep]),
                MetaIsOverMaxDeep = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsOverMaxDeep]),
                MetaIsOverMaxRightCap = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsOverMaxRightCap]),
                MetaIsOverMaxStack = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsOverMaxStack]),
                MetaIsOverMaxTopCap = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsOverMaxTopCap]),
                MetaIsInvalidMerchandisingStyle = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsInvalidMerchandisingStyle]),
                MetaIsHangingTray = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsHangingTray]),
                MetaIsPegOverfilled = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsPegOverfilled]),
                MetaIsOutsideMerchandisingSpace = (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsOutsideMerchandisingSpace]),
                MetaIsOutsideOfBlockSpace= (Boolean?)GetValue(dr[FieldNames.PlanogramPositionMetaIsOutsideOfBlockSpace]),
                MetaHeight = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaHeight]),
                MetaWidth = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaWidth]),
                MetaDepth = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaDepth]),
                MetaWorldX = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaWorldX]),
                MetaWorldY = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaWorldY]),
                MetaWorldZ = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaWorldZ]),
                MetaTotalLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaTotalLinearSpace]),
                MetaTotalAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaTotalAreaSpace]),
                MetaTotalVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaTotalVolumetricSpace]),
                MetaPlanogramLinearSpacePercentage = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaPlanogramLinearSpacePercentage]),
                MetaPlanogramAreaSpacePercentage = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaPlanogramAreaSpacePercentage]),
                MetaPlanogramVolumetricSpacePercentage = (Single?)GetValue(dr[FieldNames.PlanogramPositionMetaPlanogramVolumetricSpacePercentage]),
                SequenceNumber = (Int32?)GetValue(dr[FieldNames.PlanogramPositionSequenceNumber]),
                SequenceColour = (Int32?)GetValue(dr[FieldNames.PlanogramPositionSequenceColour]),
                MetaComparisonStatus = (Byte)GetValue(dr[FieldNames.PlanogramPositionMetaComparisonStatus]),
                MetaSequenceSubGroupName = (String)GetValue(dr[FieldNames.PlanogramPositionMetaSequenceSubGroupName]),
                MetaSequenceGroupName = (String)GetValue(dr[FieldNames.PlanogramPositionMetaSequenceGroupName]),
                MetaPegRowNumber = (Int32?)GetValue(dr[FieldNames.PlanogramPositionMetaPegRowNumber]),
                MetaPegColumnNumber = (Int32?)GetValue(dr[FieldNames.PlanogramPositionMetaPegColumnNumber]),
            };

        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramPositionDto> FetchByPlanogramId(Object planogramId)
        {
            List<PlanogramPositionDto> dtoList = new List<PlanogramPositionDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPositionFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramPositionPlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramPositionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPositionInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramPositionId, SqlDbType.Int);
                    CreateOtherParameters(dto, command);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramPositionDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, PlanogramPositionDto> index = new Dictionary<Object, PlanogramPositionDto>();
            foreach (PlanogramPositionDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramPositionBulkCopyDataReader dr = new PlanogramPositionBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPositionBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramPositionDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPositionUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramPositionId, SqlDbType.Int, dto.Id);
                    CreateOtherParameters(dto, command);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramPositionDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramPositionBulkCopyDataReader dr = new PlanogramPositionBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPositionBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramPositionDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramPositionId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramPositionDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramPositionDto> dtos)
        {
            foreach (PlanogramPositionDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        ///     Creates the parameters other than Id and similar.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramPositionDto"/> containing the information to populate the parameters.</param>
        /// <param name="command">The <see cref="DalCommand"/> to which append the parameters.</param>
        private void CreateOtherParameters(PlanogramPositionDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramPositionPlanogramId, SqlDbType.Int, dto.PlanogramId);
            CreateParameter(command, FieldNames.PlanogramPositionPlanogramFixtureItemId, SqlDbType.Int, dto.PlanogramFixtureItemId);
            CreateParameter(command, FieldNames.PlanogramPositionPlanogramFixtureAssemblyId, SqlDbType.Int, dto.PlanogramFixtureAssemblyId);
            CreateParameter(command, FieldNames.PlanogramPositionPlanogramAssemblyComponentId, SqlDbType.Int, dto.PlanogramAssemblyComponentId);
            CreateParameter(command, FieldNames.PlanogramPositionPlanogramFixtureComponentId, SqlDbType.Int, dto.PlanogramFixtureComponentId);
            CreateParameter(command, FieldNames.PlanogramPositionPlanogramSubComponentId, SqlDbType.Int, dto.PlanogramSubComponentId);
            CreateParameter(command, FieldNames.PlanogramPositionPlanogramProductId, SqlDbType.Int, dto.PlanogramProductId);
            CreateParameter(command, FieldNames.PlanogramPositionX, SqlDbType.Real, dto.X);
            CreateParameter(command, FieldNames.PlanogramPositionY, SqlDbType.Real, dto.Y);
            CreateParameter(command, FieldNames.PlanogramPositionZ, SqlDbType.Real, dto.Z);
            CreateParameter(command, FieldNames.PlanogramPositionSlope, SqlDbType.Real, dto.Slope);
            CreateParameter(command, FieldNames.PlanogramPositionAngle, SqlDbType.Real, dto.Angle);
            CreateParameter(command, FieldNames.PlanogramPositionRoll, SqlDbType.Real, dto.Roll);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsHigh, SqlDbType.SmallInt, dto.FacingsHigh);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsWide, SqlDbType.SmallInt, dto.FacingsWide);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsDeep, SqlDbType.SmallInt, dto.FacingsDeep);
            CreateParameter(command, FieldNames.PlanogramPositionOrientationType, SqlDbType.TinyInt, dto.OrientationType);
            CreateParameter(command, FieldNames.PlanogramPositionMerchandisingStyle, SqlDbType.TinyInt, dto.MerchandisingStyle);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsXHigh, SqlDbType.SmallInt, dto.FacingsXHigh);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsXWide, SqlDbType.SmallInt, dto.FacingsXWide);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsXDeep, SqlDbType.SmallInt, dto.FacingsXDeep);
            CreateParameter(command, FieldNames.PlanogramPositionMerchandisingStyleX, SqlDbType.TinyInt, dto.MerchandisingStyleX);
            CreateParameter(command, FieldNames.PlanogramPositionOrientationTypeX, SqlDbType.TinyInt, dto.OrientationTypeX);
            CreateParameter(command, FieldNames.PlanogramPositionIsXPlacedLeft, SqlDbType.Bit, dto.IsXPlacedLeft);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsYHigh, SqlDbType.SmallInt, dto.FacingsYHigh);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsYWide, SqlDbType.SmallInt, dto.FacingsYWide);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsYDeep, SqlDbType.SmallInt, dto.FacingsYDeep);
            CreateParameter(command, FieldNames.PlanogramPositionMerchandisingStyleY, SqlDbType.TinyInt, dto.MerchandisingStyleY);
            CreateParameter(command, FieldNames.PlanogramPositionOrientationTypeY, SqlDbType.TinyInt, dto.OrientationTypeY);
            CreateParameter(command, FieldNames.PlanogramPositionIsYPlacedBottom, SqlDbType.Bit, dto.IsYPlacedBottom);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsZHigh, SqlDbType.SmallInt, dto.FacingsZHigh);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsZWide, SqlDbType.SmallInt, dto.FacingsZWide);
            CreateParameter(command, FieldNames.PlanogramPositionFacingsZDeep, SqlDbType.SmallInt, dto.FacingsZDeep);
            CreateParameter(command, FieldNames.PlanogramPositionMerchandisingStyleZ, SqlDbType.TinyInt, dto.MerchandisingStyleZ);
            CreateParameter(command, FieldNames.PlanogramPositionOrientationTypeZ, SqlDbType.TinyInt, dto.OrientationTypeZ);
            CreateParameter(command, FieldNames.PlanogramPositionIsZPlacedFront, SqlDbType.Bit, dto.IsZPlacedFront);
            CreateParameter(command, FieldNames.PlanogramPositionSequence, SqlDbType.SmallInt, dto.Sequence);
            CreateParameter(command, FieldNames.PlanogramPositionSequenceX, SqlDbType.SmallInt, dto.SequenceX);
            CreateParameter(command, FieldNames.PlanogramPositionSequenceY, SqlDbType.SmallInt, dto.SequenceY);
            CreateParameter(command, FieldNames.PlanogramPositionSequenceZ, SqlDbType.SmallInt, dto.SequenceZ);
            CreateParameter(command, FieldNames.PlanogramPositionUnitsHigh, SqlDbType.SmallInt, dto.UnitsHigh);
            CreateParameter(command, FieldNames.PlanogramPositionUnitsWide, SqlDbType.SmallInt, dto.UnitsWide);
            CreateParameter(command, FieldNames.PlanogramPositionUnitsDeep, SqlDbType.SmallInt, dto.UnitsDeep);
            CreateParameter(command, FieldNames.PlanogramPositionTotalUnits, SqlDbType.Int, dto.TotalUnits);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsPositionCollisions, SqlDbType.Bit, dto.MetaIsPositionCollisions);
            CreateParameter(command, FieldNames.PlanogramPositionMetaAchievedCases, SqlDbType.Real, dto.MetaAchievedCases);
            CreateParameter(command, FieldNames.PlanogramPositionMetaFrontFacingsWide, SqlDbType.SmallInt, dto.MetaFrontFacingsWide);
            CreateParameter(command, FieldNames.PlanogramPositionPositionSequenceNumber, SqlDbType.SmallInt, dto.PositionSequenceNumber);
            CreateParameter(command, FieldNames.PlanogramPositionIsManuallyPlaced, SqlDbType.Bit, dto.IsManuallyPlaced);
            CreateParameter(command, FieldNames.PlanogramPositionHorizontalSqueeze, SqlDbType.Real, dto.HorizontalSqueeze);
            CreateParameter(command, FieldNames.PlanogramPositionVerticalSqueeze, SqlDbType.Real, dto.VerticalSqueeze);
            CreateParameter(command, FieldNames.PlanogramPositionDepthSqueeze, SqlDbType.Real, dto.DepthSqueeze);
            CreateParameter(command, FieldNames.PlanogramPositionHorizontalSqueezeX, SqlDbType.Real, dto.HorizontalSqueezeX);
            CreateParameter(command, FieldNames.PlanogramPositionVerticalSqueezeX, SqlDbType.Real, dto.VerticalSqueezeX);
            CreateParameter(command, FieldNames.PlanogramPositionDepthSqueezeX, SqlDbType.Real, dto.DepthSqueezeX);
            CreateParameter(command, FieldNames.PlanogramPositionHorizontalSqueezeY, SqlDbType.Real, dto.HorizontalSqueezeY);
            CreateParameter(command, FieldNames.PlanogramPositionVerticalSqueezeY, SqlDbType.Real, dto.VerticalSqueezeY);
            CreateParameter(command, FieldNames.PlanogramPositionDepthSqueezeY, SqlDbType.Real, dto.DepthSqueezeY);
            CreateParameter(command, FieldNames.PlanogramPositionHorizontalSqueezeZ, SqlDbType.Real, dto.HorizontalSqueezeZ);
            CreateParameter(command, FieldNames.PlanogramPositionVerticalSqueezeZ, SqlDbType.Real, dto.VerticalSqueezeZ);
            CreateParameter(command, FieldNames.PlanogramPositionDepthSqueezeZ, SqlDbType.Real, dto.DepthSqueezeZ);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsUnderMinDeep, SqlDbType.Bit, dto.MetaIsUnderMinDeep);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsOverMaxDeep, SqlDbType.Bit, dto.MetaIsOverMaxDeep);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsOverMaxRightCap, SqlDbType.Bit, dto.MetaIsOverMaxRightCap);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsOverMaxStack, SqlDbType.Bit, dto.MetaIsOverMaxStack);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsOverMaxTopCap, SqlDbType.Bit, dto.MetaIsOverMaxTopCap);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsInvalidMerchandisingStyle, SqlDbType.Bit, dto.MetaIsInvalidMerchandisingStyle);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsHangingTray, SqlDbType.Bit, dto.MetaIsHangingTray);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsPegOverfilled, SqlDbType.Bit, dto.MetaIsPegOverfilled);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsOutsideMerchandisingSpace, SqlDbType.Bit, dto.MetaIsOutsideMerchandisingSpace);
            CreateParameter(command, FieldNames.PlanogramPositionMetaIsOutsideOfBlockSpace, SqlDbType.Bit, dto.MetaIsOutsideOfBlockSpace);
            CreateParameter(command, FieldNames.PlanogramPositionMetaHeight, SqlDbType.Real, dto.MetaHeight);
            CreateParameter(command, FieldNames.PlanogramPositionMetaWidth, SqlDbType.Real, dto.MetaWidth);
            CreateParameter(command, FieldNames.PlanogramPositionMetaDepth, SqlDbType.Real, dto.MetaDepth);
            CreateParameter(command, FieldNames.PlanogramPositionMetaWorldX, SqlDbType.Real, dto.MetaWorldX);
            CreateParameter(command, FieldNames.PlanogramPositionMetaWorldY, SqlDbType.Real, dto.MetaWorldY);
            CreateParameter(command, FieldNames.PlanogramPositionMetaWorldZ, SqlDbType.Real, dto.MetaWorldZ);
            CreateParameter(command, FieldNames.PlanogramPositionMetaTotalLinearSpace, SqlDbType.Real, dto.MetaTotalLinearSpace);
            CreateParameter(command, FieldNames.PlanogramPositionMetaTotalAreaSpace, SqlDbType.Real, dto.MetaTotalAreaSpace);
            CreateParameter(command, FieldNames.PlanogramPositionMetaTotalVolumetricSpace, SqlDbType.Real, dto.MetaTotalVolumetricSpace);
            CreateParameter(command, FieldNames.PlanogramPositionMetaPlanogramLinearSpacePercentage, SqlDbType.Real, dto.MetaPlanogramLinearSpacePercentage);
            CreateParameter(command, FieldNames.PlanogramPositionMetaPlanogramAreaSpacePercentage, SqlDbType.Real, dto.MetaPlanogramAreaSpacePercentage);
            CreateParameter(command, FieldNames.PlanogramPositionMetaPlanogramVolumetricSpacePercentage, SqlDbType.Real, dto.MetaPlanogramVolumetricSpacePercentage);
            CreateParameter(command, FieldNames.PlanogramPositionSequenceNumber, SqlDbType.Int, dto.SequenceNumber);
            CreateParameter(command, FieldNames.PlanogramPositionSequenceColour, SqlDbType.Int, dto.SequenceColour);
            CreateParameter(command, FieldNames.PlanogramPositionMetaComparisonStatus, SqlDbType.TinyInt, dto.MetaComparisonStatus);
            CreateParameter(command, FieldNames.PlanogramPositionMetaSequenceSubGroupName, SqlDbType.NVarChar, dto.MetaSequenceSubGroupName);
            CreateParameter(command, FieldNames.PlanogramPositionMetaSequenceGroupName, SqlDbType.NVarChar, dto.MetaSequenceGroupName);
            CreateParameter(command, FieldNames.PlanogramPositionMetaPegRowNumber, SqlDbType.Int, dto.MetaPegRowNumber);
            CreateParameter(command, FieldNames.PlanogramPositionMetaPegColumnNumber, SqlDbType.Int, dto.MetaPegColumnNumber);
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
