﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#region Version History: (CCM 8.3.0)
// V8-31877 : N.Haywood
//  Added Product Group Code
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ClusterLocation Dal
    /// </summary>
    public class ClusterLocationSearchCriteriaDal : Galleria.Framework.Dal.Mssql.DalBase, IClusterLocationSearchCriteriaDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ClusterLocationSearchCriteriaDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ClusterLocationSearchCriteriaDto()
            {
                ClusterSchemeName = (String)GetValue(dr[FieldNames.ClusterSchemeName]),
                ClusterName = (String)GetValue(dr[FieldNames.ClusterName]),
                LocationCode = (String)GetValue(dr[FieldNames.LocationCode]),
                ProductGroupCode = (String)GetValue(dr[FieldNames.ProductGroupCode])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<ClusterLocationSearchCriteriaDto> FetchByEntityId(int entityId)
        {
            List<ClusterLocationSearchCriteriaDto> dtoList = new List<ClusterLocationSearchCriteriaDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterLocationSearchCriteriaFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.ClusterSchemeEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        #endregion
    }
}
