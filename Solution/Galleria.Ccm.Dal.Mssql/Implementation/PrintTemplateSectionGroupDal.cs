﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Mssql.Schema;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MSSQL implementation of IPrintTemplateSectionGroupDal
    /// </summary>
    public sealed class PrintTemplateSectionGroupDal : Galleria.Framework.Dal.Mssql.DalBase, IPrintTemplateSectionGroupDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PrintTemplateSectionGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PrintTemplateSectionGroupDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PrintTemplateSectionGroupId]),
                PrintTemplateId = GetValue(dr[FieldNames.PrintTemplateSectionGroupPrintTemplateId]),
                Number = (Byte)GetValue(dr[FieldNames.PrintTemplateSectionGroupNumber]),
                Name = (String)GetValue(dr[FieldNames.PrintTemplateSectionGroupName]),
                BaysPerPage = (Byte)GetValue(dr[FieldNames.PrintTemplateSectionGroupBaysPerPage]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public PrintTemplateSectionGroupDto FetchById(Int32 id)
        {
            PrintTemplateSectionGroupDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionGroupFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }


        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PrintTemplateSectionGroupDto> FetchByPrintTemplateId(Object printTemplateId)
        {
            List<PrintTemplateSectionGroupDto> dtoList = new List<PrintTemplateSectionGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionGroupFetchByPrintTemplateId))
                {
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupPrintTemplateId, SqlDbType.Int, printTemplateId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PrintTemplateSectionGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionGroupInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PrintTemplateSectionGroupId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupPrintTemplateId, SqlDbType.Int, dto.PrintTemplateId);
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupNumber, SqlDbType.TinyInt, dto.Number);
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupBaysPerPage, SqlDbType.TinyInt, dto.BaysPerPage);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PrintTemplateSectionGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionGroupUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupPrintTemplateId, SqlDbType.Int, dto.PrintTemplateId);
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupNumber, SqlDbType.TinyInt, dto.Number);
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupBaysPerPage, SqlDbType.TinyInt, dto.BaysPerPage);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateSectionGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PrintTemplateSectionGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}