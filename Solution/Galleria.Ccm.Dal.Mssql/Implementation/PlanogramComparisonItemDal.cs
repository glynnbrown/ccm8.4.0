﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramComparisonItemDal : Framework.Dal.Mssql.DalBase, IPlanogramComparisonItemDal
    {
        #region Constants
        private const String ImportTableName = "#tmpPlanogramComparisonItem";
        private const String DropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String CreateImportTableSql =
            "CREATE TABLE [{0}](" +
            "[PlanogramComparisonItem_Id] [INT] NOT NULL," +
            "[PlanogramComparisonResult_Id] [INT] NOT NULL, " +
            "[PlanogramComparisonItem_ItemType] [NVARCHAR](100) COLLATE database_default NOT NULL, " +
            "[PlanogramComparisonItem_ItemId] [NVARCHAR](100) COLLATE database_default NULL, " +
            "[PlanogramComparisonItem_Status] [TINYINT] NOT NULL)";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramComparisonItemBulkCopyDataReader : IDataReader
        {
            #region Constants

            private const Int32 IdColumnNumber = 0;
            private const Int32 PlanogramComparisonResultIdColumnNumber = 1;
            private const Int32 ItemTypeColumnNumber = 2;
            private const Int32 ItemIdColumnNumber = 3;
            private const Int32 StatusColumnNumber = 4;
            private const Int32 ColumnCount = 5;

            #endregion

            #region Fields
            private readonly IEnumerator<PlanogramComparisonItemDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramComparisonItemBulkCopyDataReader(IEnumerable<PlanogramComparisonItemDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return ColumnCount; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public Object GetValue(Int32 i)
            {
                Object value;
                switch (i)
                {
                    case IdColumnNumber:
                        value = _enumerator.Current.Id;
                        break;
                    case PlanogramComparisonResultIdColumnNumber:
                        value = _enumerator.Current.PlanogramComparisonResultId;
                        break;
                    case ItemTypeColumnNumber:
                        value = _enumerator.Current.ItemType;
                        break;
                    case ItemIdColumnNumber:
                        value = _enumerator.Current.ItemId;
                        break;
                    case StatusColumnNumber:
                        value = _enumerator.Current.Status;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                return value ?? DBNull.Value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public Int32 Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public Boolean IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public Boolean NextResult()
            {
                throw new NotImplementedException();
            }

            public Int32 RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public Boolean GetBoolean(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Byte GetByte(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetBytes(Int32 i, Int64 fieldOffset, Byte[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public Char GetChar(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetChars(Int32 i, Int64 fieldoffset, Char[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetDataTypeName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Decimal GetDecimal(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Double GetDouble(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Single GetFloat(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int16 GetInt16(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetInt32(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetInt64(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetOrdinal(String name)
            {
                throw new NotImplementedException();
            }

            public String GetString(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetValues(Object[] values)
            {
                throw new NotImplementedException();
            }

            public Boolean IsDBNull(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Object this[String name]
            {
                get { throw new NotImplementedException(); }
            }

            public Object this[Int32 i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Create data transfer object with the data from the given <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="dr">The <see cref="SqlDataReader"/> instance from which to load data.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonItemDto"/> with the required values.</returns>
        public PlanogramComparisonItemDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramComparisonItemDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramComparisonItemId]),
                PlanogramComparisonResultId = (Int32)GetValue(dr[FieldNames.PlanogramComparisonResultId]),
                ItemType = (Byte)GetValue(dr[FieldNames.PlanogramComparisonItemItemType]),
                ItemId = (String)GetValue(dr[FieldNames.PlanogramComparisonItemItemId]),
                Status = (Byte)GetValue(dr[FieldNames.PlanogramComparisonItemStatus])
            };
        }
        #endregion
        
        #region Fetch

        /// <summary>
        ///     Create a collection of data transfer object with the data assigned to the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> containing the Id of the parent Planogram Comparison.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonItemDto"/> with the required values.</returns>
        public IEnumerable<PlanogramComparisonItemDto> FetchByPlanogramComparisonResultId(Object id)
        {
            var dtoList = new List<PlanogramComparisonItemDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonItemFetchByPlanogramComparisonResultId))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonResultId, SqlDbType.Int, (Int32) id);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the values in the given <paramref name="dto"/> into the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonItemDto"/> instance containing the values.</param>
        public void Insert(PlanogramComparisonItemDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonItemInsert))
                {
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramComparisonItemId, SqlDbType.Int);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemPlanogramComparisonResultId, SqlDbType.Int, dto.PlanogramComparisonResultId);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemItemType, SqlDbType.TinyInt, dto.ItemType);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemItemId, SqlDbType.NVarChar, dto.ItemId);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemStatus, SqlDbType.TinyInt, dto.Status);

                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Insert the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonItemDto"/> instances containing the values.</param>
        public void Insert(IEnumerable<PlanogramComparisonItemDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            var index = new Dictionary<Object, PlanogramComparisonItemDto>();
            IList<PlanogramComparisonItemDto> fieldDtos = dtos as IList<PlanogramComparisonItemDto> ?? dtos.ToList();
            foreach (PlanogramComparisonItemDto dto in fieldDtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(ImportTableName);

                // perform a batch insert to temp table
                using (var bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = CommandTimeout;
                    bulkCopy.DestinationTableName = String.Format("[{0}]", ImportTableName);
                    var dr = new PlanogramComparisonItemBulkCopyDataReader(fieldDtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonItemBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(ImportTableName);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the values in the given <paramref name="dto"/> into the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonItemDto"/> instance containing the values.</param>
        public void Update(PlanogramComparisonItemDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonItemUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonItemId, SqlDbType.Int, dto.Id);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemPlanogramComparisonResultId, SqlDbType.Int, dto.PlanogramComparisonResultId);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemItemType, SqlDbType.TinyInt, dto.ItemType);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemItemId, SqlDbType.NVarChar, dto.ItemId);
                    CreateParameter(command, FieldNames.PlanogramComparisonItemStatus, SqlDbType.TinyInt, dto.Status);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Update the values in the given <paramref name="dtos"/> into the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonItemDto"/> instances containing the values.</param>
        public void Update(IEnumerable<PlanogramComparisonItemDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(ImportTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = CommandTimeout;
                    bulkCopy.DestinationTableName = String.Format("[{0}]", ImportTableName);
                    var dr = new PlanogramComparisonItemBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonItemBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(ImportTableName);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the values assigned to the given <paramref name="id"/> from the data base.
        /// </summary>
        /// <param name="id">The <see cref="Object"/> contaning the ID assigned to the values.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonItemDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonItemId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Delete the values in the given <paramref name="dto"/> from the data base.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonItemDto"/> contaning the values.</param>
        public void Delete(PlanogramComparisonItemDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Delete the values in the given <paramref name="dtos"/> from the data base.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramComparisonItemDto"/> contaning the values.</param>
        public void Delete(IEnumerable<PlanogramComparisonItemDto> dtos)
        {
            foreach (PlanogramComparisonItemDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, CreateImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, DropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}