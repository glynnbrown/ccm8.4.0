﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS.      
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// A MSSQL DAL for the PlanogramAnnotation DTO.
    /// </summary>
    public class PlanogramAnnotationDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramAnnotationDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a new PlanogramAnnotation DTO from the data reader.
        /// </summary>
        /// <param name="dr">The data reader to load from.</param>
        /// <returns>A new PlanogramAnnotation DTO.</returns>
        public PlanogramAnnotationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramAnnotationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramAnnotationId]), 
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramAnnotationPlanogramId]), 
                PlanogramFixtureItemId = (Int32)GetValue(dr[FieldNames.PlanogramAnnotationPlanogramFixtureItemId]), 
                PlanogramFixtureAssemblyId = (Int32?)GetValue(dr[FieldNames.PlanogramAnnotationPlanogramFixtureAssemblyId]), 
                PlanogramFixtureComponentId = (Int32?)GetValue(dr[FieldNames.PlanogramAnnotationPlanogramFixtureComponentId]), 
                PlanogramAssemblyComponentId = (Int32?)GetValue(dr[FieldNames.PlanogramAnnotationPlanogramAssemblyComponentId]), 
                PlanogramSubComponentId = (Int32?)GetValue(dr[FieldNames.PlanogramAnnotationPlanogramSubComponentId]), 
                PlanogramPositionId = (Int32?)GetValue(dr[FieldNames.PlanogramAnnotationPlanogramPositionId]), 
                AnnotationType = (Byte)GetValue(dr[FieldNames.PlanogramAnnotationAnnotationType]), 
                Text = (String)GetValue(dr[FieldNames.PlanogramAnnotationText]), 
                X = (Single?)GetValue(dr[FieldNames.PlanogramAnnotationX]), 
                Y = (Single?)GetValue(dr[FieldNames.PlanogramAnnotationY]), 
                Z = (Single?)GetValue(dr[FieldNames.PlanogramAnnotationZ]), 
                Height = (Single)GetValue(dr[FieldNames.PlanogramAnnotationHeight]), 
                Width = (Single)GetValue(dr[FieldNames.PlanogramAnnotationWidth]), 
                Depth = (Single)GetValue(dr[FieldNames.PlanogramAnnotationDepth]), 
                BorderColour = (Int32)GetValue(dr[FieldNames.PlanogramAnnotationBorderColour]), 
                BorderThickness = (Single)GetValue(dr[FieldNames.PlanogramAnnotationBorderThickness]), 
                BackgroundColour = (Int32)GetValue(dr[FieldNames.PlanogramAnnotationBackgroundColour]), 
                FontColour = (Int32)GetValue(dr[FieldNames.PlanogramAnnotationFontColour]), 
                FontSize = (Single)GetValue(dr[FieldNames.PlanogramAnnotationFontSize]), 
                FontName = (String)GetValue(dr[FieldNames.PlanogramAnnotationFontName]), 
                CanReduceFontToFit = (Boolean)GetValue(dr[FieldNames.PlanogramAnnotationCanReduceFontToFit])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a list of PlanogramAnnotation DTOs that have a matching Planogram Id.
        /// </summary>
        /// <param name="planogramId">The Planogram Id to match.</param>
        /// <returns>A list of PlanogramAnnotation DTOs.</returns>
        public IEnumerable<PlanogramAnnotationDto> FetchByPlanogramId(object planogramId)
        {
            var dtoList = new List<PlanogramAnnotationDto>();
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAnnotationFetchByPlanogramId))
                {
                    CreateParameter(
                        command,
                        FieldNames.PlanogramAnnotationPlanogramId,
                        SqlDbType.Int,
                        planogramId);
                    
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Inserts the given PlanogramAnnotation DTO into the database.
        /// </summary>
        /// <param name="dto">The PlanogramAnnotation DTO to insert.</param>
        public void Insert(PlanogramAnnotationDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAnnotationInsert))
                {
                    var idParameter = CreateParameter(command, FieldNames.PlanogramAnnotationId, SqlDbType.Int);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the given DTO in the database.
        /// </summary>
        /// <param name="dto">The DTO to update.</param>
        public void Update(PlanogramAnnotationDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAnnotationUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramAnnotationId, SqlDbType.Int, dto.Id);
                    CreateAllParametersButId(dto, command);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the PlanogramAnnotation with the given Id.
        /// </summary>
        /// <param name="id">The Id of the PlanogramAnnotation to delete.</param>
        public void DeleteById(object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramAnnotationDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramAnnotationId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramAnnotationDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Creates parameters for the given command for each of the dto's properties, apart from it's Id.
        /// </summary>
        /// <param name="dto">The DTO whose properties should be used.</param>
        /// <param name="command">The command to add parameters to.</param>
        private void CreateAllParametersButId(PlanogramAnnotationDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramAnnotationPlanogramId, SqlDbType.Int, dto.PlanogramId);
            CreateParameter(command, FieldNames.PlanogramAnnotationPlanogramFixtureItemId, SqlDbType.Int, dto.PlanogramFixtureItemId);
            CreateParameter(command, FieldNames.PlanogramAnnotationPlanogramFixtureAssemblyId, SqlDbType.Int, dto.PlanogramFixtureAssemblyId);
            CreateParameter(command, FieldNames.PlanogramAnnotationPlanogramFixtureComponentId, SqlDbType.Int, dto.PlanogramFixtureComponentId);
            CreateParameter(command, FieldNames.PlanogramAnnotationPlanogramAssemblyComponentId, SqlDbType.Int, dto.PlanogramAssemblyComponentId);
            CreateParameter(command, FieldNames.PlanogramAnnotationPlanogramSubComponentId, SqlDbType.Int, dto.PlanogramSubComponentId);
            CreateParameter(command, FieldNames.PlanogramAnnotationPlanogramPositionId, SqlDbType.Int, dto.PlanogramPositionId);
            CreateParameter(command, FieldNames.PlanogramAnnotationAnnotationType, SqlDbType.TinyInt, dto.AnnotationType);
            CreateParameter(command, FieldNames.PlanogramAnnotationText, SqlDbType.NVarChar, dto.Text);
            CreateParameter(command, FieldNames.PlanogramAnnotationX, SqlDbType.Real, dto.X);
            CreateParameter(command, FieldNames.PlanogramAnnotationY, SqlDbType.Real, dto.Y);
            CreateParameter(command, FieldNames.PlanogramAnnotationZ, SqlDbType.Real, dto.Z);
            CreateParameter(command, FieldNames.PlanogramAnnotationHeight, SqlDbType.Real, dto.Height);
            CreateParameter(command, FieldNames.PlanogramAnnotationWidth, SqlDbType.Real, dto.Width);
            CreateParameter(command, FieldNames.PlanogramAnnotationDepth, SqlDbType.Real, dto.Depth);
            CreateParameter(command, FieldNames.PlanogramAnnotationBorderColour, SqlDbType.Int, dto.BorderColour);
            CreateParameter(command, FieldNames.PlanogramAnnotationBorderThickness, SqlDbType.Real, dto.BorderThickness);
            CreateParameter(command, FieldNames.PlanogramAnnotationBackgroundColour, SqlDbType.Int, dto.BackgroundColour);
            CreateParameter(command, FieldNames.PlanogramAnnotationFontColour, SqlDbType.Int, dto.FontColour);
            CreateParameter(command, FieldNames.PlanogramAnnotationFontSize, SqlDbType.Real, dto.FontSize);
            CreateParameter(command, FieldNames.PlanogramAnnotationFontName, SqlDbType.NVarChar, dto.FontName);
            CreateParameter(command, FieldNames.PlanogramAnnotationCanReduceFontToFit, SqlDbType.Bit, dto.CanReduceFontToFit);
        } 
        #endregion
    }
}
