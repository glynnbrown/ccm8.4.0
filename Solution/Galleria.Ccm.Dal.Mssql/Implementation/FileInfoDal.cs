﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation

{
    public class FileInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IFileInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A file info dto</returns>
        private FileInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new FileInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.FileId]),
                EntityId = (Int32)GetValue(dr[FieldNames.FileEntityId]),
                Name = (String)GetValue(dr[FieldNames.FileName]),
                SourceFilePath = (String)GetValue(dr[FieldNames.FileSourceFilePath]),
                SizeInBytes = (Int64)GetValue(dr[FieldNames.FileSizeInBytes]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.FileDateLastModified]),
                UserId = (Int32)GetValue(dr[FieldNames.FileUserId])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns all file infos in the database
        /// </summary>
        /// <returns>A list of file info dtos</returns>
        public FileInfoDto FetchById(Int32 fileId)
        {
            using (DalCommand command = CreateCommand(ProcedureNames.FileInfoFetchById))
            {
                // id
                CreateParameter(
                    command,
                    FieldNames.FileId,
                    SqlDbType.Int,
                    fileId);

                // execute
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        return GetDataTransferObject(dr);
                    }
                    else
                    {
                        throw new DtoDoesNotExistException();
                    }
                }
            }
        }
        #endregion
    }
}