﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ClusterLocation Dal
    /// </summary>
    public class ClusterLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IClusterLocationDal
    {
        #region Constants

        private const String _importTableName = "#tmpClusterLocation";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[Cluster_Id] [INT], " +
                "[Location_Id] [SMALLINT] " +
            ")";

        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class ClusterLocationBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<ClusterLocationDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ClusterLocationBulkCopyDataReader(IEnumerable<ClusterLocationDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 2; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.ClusterId;
                        break;
                    case 1:
                        value = _enumerator.Current.LocationId;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ClusterLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ClusterLocationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ClusterLocationId]),
                ClusterId = (Int32)GetValue(dr[FieldNames.ClusterLocationClusterId]),
                LocationId = (Int16)GetValue(dr[FieldNames.ClusterLocationLocationId]),
                Address1 = (String)GetValue(dr[FieldNames.LocationAddress1]),
                Address2 = (String)GetValue(dr[FieldNames.LocationAddress2]),
                City = (String)GetValue(dr[FieldNames.LocationCity]),
                Code = (String)GetValue(dr[FieldNames.LocationCode]),
                Country = (String)GetValue(dr[FieldNames.LocationCountry]),
                County = (String)GetValue(dr[FieldNames.LocationCounty]),
                Name = (String)GetValue(dr[FieldNames.LocationName]),
                PostalCode = (String)GetValue(dr[FieldNames.LocationPostalCode]),
                Region = (String)GetValue(dr[FieldNames.LocationRegion]),
                TVRegion = (String)GetValue(dr[FieldNames.LocationTVRegion]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ClusterLocationDto FetchById(Int32 id)
        {
            ClusterLocationDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterLocationFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ClusterLocationId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public IEnumerable<ClusterLocationDto> FetchByClusterId(int clusterId)
        {
            List<ClusterLocationDto> dtoList = new List<ClusterLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterLocationFetchByClusterId))
                {
                    // clusterId id
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.ClusterId,
                        SqlDbType.Int,
                        clusterId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(ClusterLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ClusterLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.ClusterLocationClusterId, SqlDbType.Int, dto.ClusterId);
                    CreateParameter(command, FieldNames.ClusterLocationLocationId, SqlDbType.Int, dto.LocationId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(ClusterLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterLocationUpdateById))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.ClusterLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.ClusterLocationClusterId, SqlDbType.Int, dto.ClusterId);
                    CreateParameter(command, FieldNames.ClusterLocationLocationId, SqlDbType.Int, dto.LocationId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Performs a combined update and insert into the database
        /// </summary>
        /// <param name="dtoList">The list of dtos to upsert</param>
        public void Upsert(IEnumerable<ClusterLocationDto> dtoList, ClusterLocationIsSetDto isSetDto)
        {
            //if isSetDto is passed through as null
            if (isSetDto == null)
            {
                isSetDto = new ClusterLocationIsSetDto(true);
            }

            // to avoid duplicates, we create a dictionary of dtos
            // to ensure that only the last dto is actually inserted
            Dictionary<ClusterLocationDtoKey, ClusterLocationDto> keyedDtoList = new Dictionary<ClusterLocationDtoKey, ClusterLocationDto>();
            foreach (ClusterLocationDto dto in dtoList)
            {
                // add to the dto list
                keyedDtoList[dto.DtoKey] = dto;
            }

            try
            {
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    ClusterLocationBulkCopyDataReader dr = new ClusterLocationBulkCopyDataReader(keyedDtoList.Values);
                    bulkCopy.WriteToServer(dr);
                }

                //Construct SetProperties String
                String setProperties = GenerateSetPropertiesString(isSetDto);

                // perform a batch upsert
                Dictionary<ClusterLocationDtoKey, Int32> ids = new Dictionary<ClusterLocationDtoKey, Int32>();
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterLocationBulkUpsert))
                {
                    //Setup SetProperties string parameter
                    CreateParameter(command, FieldNames.ClusterLocationSetProperties, SqlDbType.NVarChar, setProperties);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            Int32 outputData = (Int32)GetValue(dr[FieldNames.ClusterLocationId]);

                            ClusterLocationDtoKey key = new ClusterLocationDtoKey();
                            key.ClusterId = (Int32)GetValue(dr[FieldNames.ClusterLocationClusterId]);
                            key.LocationId = (Int16)GetValue(dr[FieldNames.ClusterLocationLocationId]);
                            ids[key] = outputData;
                        }
                    }
                }

                foreach (ClusterLocationDto dto in dtoList)
                {
                    Int32 outputData = 0;
                    ids.TryGetValue(dto.DtoKey, out outputData);

                    if (outputData != 0)
                    {
                        dto.Id = outputData;
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(string importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Generates a string containing the name of every DTO property for which the corresponding property value in 
        /// isSetDto is true.
        /// </summary>
        private string GenerateSetPropertiesString(ClusterLocationIsSetDto isSetDto)
        {
            String setProperties = String.Empty;

            if (isSetDto.IsClusterIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ClusterLocationClusterId);
            }
            if (isSetDto.IsLocationIdSet)
            {
                setProperties = String.Format("{0}#{1}", setProperties, FieldNames.ClusterLocationLocationId);
            }

            return setProperties;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ClusterLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}