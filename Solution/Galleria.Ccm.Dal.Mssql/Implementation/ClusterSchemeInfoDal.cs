﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (SA v1.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM v8.0.3)
// V8-29216 : D.Pleasance
//  Removed FetchByEntityIdIncludingDeleted
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion 
#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//  Added ProductGroupId And DateLastModified
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// ClusterSchemeInfo Dal
    /// </summary>
    public class ClusterSchemeInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IClusterSchemeInfoDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ClusterSchemeInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new ClusterSchemeInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.ClusterSchemeInfoId]),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.ClusterSchemeInfoUniqueContentReference]),
                Name = (String)GetValue(dr[FieldNames.ClusterSchemeInfoName]),
                IsDefault = (Boolean)GetValue(dr[FieldNames.ClusterSchemeInfoIsDefault]),
                ClusterCount = (Int32)GetValue(dr[FieldNames.ClusterSchemeInfoClusterCount]),
                EntityId = (Int32)GetValue(dr[FieldNames.ClusterSchemeInfoEntityId]),
                ProductGroupId = (Int32?)GetValue(dr[FieldNames.ClusterSchemeInfoProductGroupId]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.ClusterSchemeInfoDateLastModified]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.ClusterSchemeParentUniqueContentReference])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns all cluster scheme infos where entity id matches
        /// </summary>
        /// <param name="entityId">specified entity id</param>
        /// <returns>matching dto list</returns>
        public IEnumerable<ClusterSchemeInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<ClusterSchemeInfoDto> dtoList = new List<ClusterSchemeInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.ClusterSchemeInfoFetchByEntityId))
                {
                    //Id
                    CreateParameter(command, FieldNames.ClusterSchemeInfoEntityId, SqlDbType.Int, entityId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion
    }
}