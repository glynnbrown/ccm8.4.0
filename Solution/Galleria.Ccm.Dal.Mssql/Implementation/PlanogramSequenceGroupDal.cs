﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramSequenceGroupDal : DalBase, IPlanogramSequenceGroupDal
    {
        #region Data TransferObject

        /// <summary>
        ///     Returns a <see cref="PlanogramSequenceGroupDto"/> from the values in <paramref name="dr"/>.
        /// </summary>
        /// <param name="dr">The <see cref="SqlDataReader"/> to load from.</param>
        /// <returns>A new <see cref="PlanogramSequenceGroupDto"/>.</returns>
        private static PlanogramSequenceGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramSequenceGroupDto
            {
                Id = (Int32) GetValue(dr[FieldNames.PlanogramSequenceGroupId]),
                PlanogramSequenceId = (Int32) GetValue(dr[FieldNames.PlanogramSequenceGroupPlanogramSequenceId]),
                Name = (String) GetValue(dr[FieldNames.PlanogramSequenceGroupName]),
                Colour = (Int32) GetValue(dr[FieldNames.PlanogramSequenceGroupColour])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Gets a collection of <see cref="PlanogramSequenceGroupDto"/> matching the <paramref name="planogramSequenceId"/>.
        /// </summary>
        /// <param name="planogramSequenceId">Unique identifier for the Planogram Sequence that contains the Planogram Sequence Group to be fetched.</param>
        /// <returns>A new collection of new <see cref="PlanogramSequenceGroupDto"/> instances.</returns>
        public IEnumerable<PlanogramSequenceGroupDto> FetchByPlanogramSequenceId(Object planogramSequenceId)
        {
            IList<PlanogramSequenceGroupDto> dtos = new List<PlanogramSequenceGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupFetchByPlanogramSequenceId))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupPlanogramSequenceId, SqlDbType.Int, (Int32)planogramSequenceId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtos;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupDto"/> to insert.</param>
        public void Insert(PlanogramSequenceGroupDto dto)
        {
            try
            {
                SqlParameter idParameter;
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupInsert))
                {
                    idParameter = CreateParameter(command, FieldNames.PlanogramSequenceGroupId, SqlDbType.Int);
                    CreateDtoParameters(command, dto);

                    command.ExecuteNonQuery();
                }

                dto.Id = (Int32)idParameter.Value;
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Inserts the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The collection of <see cref="PlanogramSequenceGroupDto"/> to insert.</param>
        public void Insert(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the given <paramref name="dto"/> into the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupDto"/> to update.</param>
        public void Update(PlanogramSequenceGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupUpdateById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupId, SqlDbType.Int, dto.Id);
                    CreateDtoParameters(command, dto);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Updates the given <paramref name="dtos"/> into the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceGroupDto"/> to update.</param>
        public void Update(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupDto dto in dtos)
            {
                Update(dto);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the record with a matching <paramref name="id"/> from the database.
        /// </summary>
        /// <param name="id">The Id of the record to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramSequenceGroupDeleteById))
                {
                    CreateParameter(command, FieldNames.PlanogramSequenceGroupId, SqlDbType.Int, (Int32)id);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        ///     Deletes the given <paramref name="dto"/> from the database.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramSequenceGroupDto"/> to delete.</param>
        public void Delete(PlanogramSequenceGroupDto dto)
        {
            DeleteById(dto.Id);
        }

        /// <summary>
        ///     Deletes the given <paramref name="dtos"/> from the database.
        /// </summary>
        /// <param name="dtos">The <see cref="PlanogramSequenceGroupDto"/> to delete.</param>
        public void Delete(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            foreach (PlanogramSequenceGroupDto dto in dtos)
            {
                Delete(dto);
            }
        }

        #endregion

        private void CreateDtoParameters(DalCommand command, PlanogramSequenceGroupDto dto)
        {
            CreateParameter(command, FieldNames.PlanogramSequenceGroupPlanogramSequenceId, SqlDbType.Int,
                dto.PlanogramSequenceId);
            CreateParameter(command, FieldNames.PlanogramSequenceGroupName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramSequenceGroupColour, SqlDbType.Int, dto.Colour);
        }
    }
}
