﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#region Version History : (CCM CCM830)
// V8-31830 : A.Probyn
//  Fixed type bug on Id in the update.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal.Mssql;
using System.Data;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PerformanceSelectionMetricDal
    /// </summary>
    public class PerformanceSelectionMetricDal : Galleria.Framework.Dal.Mssql.DalBase, IPerformanceSelectionMetricDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr"> the data reader to load from</param>
        /// <returns> A new dto</returns>
        public static PerformanceSelectionMetricDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PerformanceSelectionMetricDto() 
            {
                Id = (Int32)GetValue(dr[FieldNames.PerformanceSelectionMetricId]),
                PerformanceSelectionId = (Int32)GetValue(dr[FieldNames.PerformanceSelectionMetricPerformanceSelectionId]),
                GFSPerformanceSourceMetricId = (Int32)GetValue(dr[FieldNames.PerformanceSelectionMetricGFSPerformanceSourceMetricId]),
                AggregationType = (Byte)GetValue(dr[FieldNames.PerformanceSelectionMetricAggregationType])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="performanceSelectionId">specified id</param>
        /// <returns> matching dto</returns>
        public IEnumerable<PerformanceSelectionMetricDto> FetchByPerformanceSelectionId(Int32 performanceSelectionId)
        {
            List<PerformanceSelectionMetricDto> dtoList = new List<PerformanceSelectionMetricDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionMetricFetchByPerformanceSelectionId))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricPerformanceSelectionId, SqlDbType.Int, performanceSelectionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts specified dto into the database
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PerformanceSelectionMetricDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionMetricInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PerformanceSelectionMetricId, SqlDbType.Int);

                    //other properties
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricPerformanceSelectionId, SqlDbType.Int, dto.PerformanceSelectionId);
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricGFSPerformanceSourceMetricId, SqlDbType.Int, dto.GFSPerformanceSourceMetricId);
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
    
        #endregion

        #region Update
        /// <summary>
        /// Updates dto that matches specified id in the db
        /// </summary>
        /// <param name="dto"> dto to update</param>
        public void Update(PerformanceSelectionMetricDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionMetricUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricId, SqlDbType.Int, dto.Id);

                    //other parameters
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricPerformanceSelectionId, SqlDbType.Int, dto.PerformanceSelectionId);
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricGFSPerformanceSourceMetricId, SqlDbType.Int, dto.GFSPerformanceSourceMetricId);
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricAggregationType, SqlDbType.TinyInt, dto.AggregationType);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id"> specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PerformanceSelectionMetricDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PerformanceSelectionMetricId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
