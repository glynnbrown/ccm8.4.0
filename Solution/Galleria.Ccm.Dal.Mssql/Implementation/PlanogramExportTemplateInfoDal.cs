﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramExportTemplateInfo Dal Implementation
    /// </summary>
    public class PlanogramExportTemplateInfoDal : DalBase, IPlanogramExportTemplateInfoDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramExportTemplateInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramExportTemplateInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplateId]),
                EntityId = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplateEntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramExportTemplateName])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramExportTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<PlanogramExportTemplateInfoDto> dtoList = new List<PlanogramExportTemplateInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramExportTemplateInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.PlanogramExportTemplateEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        #endregion
    }
}
