﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS.    
// V8-26041 : A.Kuszyk
//  Added additional Product fields from DTO.
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties
// V8-26777 : L.Ineson
//  Removed CanMerch properties
#endregion

#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
// V8-27636 : L.Ineson
//  Added MetaTotalUnits
#endregion

#region Version History: CCM802

// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-28811 : L.Luong
//  Added MetaPlanogramLinearSpacePercentage
//  Added MetaPlanogramAreaSpacePercentage
//  Added MetaPlanogramVolumetricSpacePercentage
// V8-29232 : A.Silva
//      Added PlanogramProductSequenceNumber.
// V8-29196 : D.Pleasance
//  Corrected issue with temp table creation and bulk insert
#endregion

#region Version History: CCM803
// V8-29506 : M.Shelley
//  Add new metadata properties for reporting :
//      MetaPositionCount, MetaTotalFacings, MetaTotalLinearSpace, MetaTotalAreaSpace, MetaTotalVolumetricSpace
// V8-28491 : L.Luong
//  Added MetaIsInMasterData
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
#endregion

#region Version History: CCM820
// V8-30728 : A.Kuszyk
//  Added ColourGroupValue field.
// V8-30763 :I.George
//	Added MetaCDTNode field to PlanogramProduct 
// V8-30705 : A.Probyn
//  Extended for new assortment rule related meta data properties
// V8-31164 : D.Pleasance
//  Removed SequenceNumber \ BlockingColour
#endregion

#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct and changed the FieldCount to 189 from 190
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31866 : D.Pleasance
//  Updated Planogram product attributes to match product.
// V8-31947 : A.Silva
//  Added MetaComparisonStatus field.
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32191 : A.Probyn
//  Corrected data reader case statement.
// V8-32396 : A.Probyn
//  Added PlanogramProduct_MetaIsDelistFamilyRuleBroken
// V8-32609 : L.Ineson
//  Removed old PegProngOffset
// V8-32814 : M.Pettit
//  Added MetaIsBuddied
// V8-32885 : A.Kuszyk
//  Corrected error in the order of fields in the temp table used for bulk inserts.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramProductDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramProductDal
    {
        #region Constants
        private const String _importTableName = "#tmpPlanogramProduct";
        private const String _dropImportTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";
        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
	            "[PlanogramProduct_Id] [INT] NOT NULL, " +
	            "[Planogram_Id] [INT] NOT NULL, " +
                "[PlanogramProduct_Gtin] [NVARCHAR](14) COLLATE database_default NOT NULL, " +
                "[PlanogramProduct_Name] [NVARCHAR](100) COLLATE database_default NOT NULL, " +
                "[PlanogramProduct_Brand] [NVARCHAR](100) COLLATE database_default NULL, " +
	            "[PlanogramProduct_Height] [REAL] NOT NULL, " +
	            "[PlanogramProduct_Width] [REAL] NOT NULL, " +
	            "[PlanogramProduct_Depth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_DisplayHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_DisplayWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_DisplayDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_AlternateHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_AlternateWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_AlternateDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PointOfPurchaseHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PointOfPurchaseWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PointOfPurchaseDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_NumberOfPegHoles] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_PegX] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PegX2] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PegX3] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PegY] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PegY2] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PegY3] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PegProngOffsetX] [REAL] NOT NULL, " +
                "[PlanogramProduct_PegProngOffsetY] [REAL] NOT NULL, " +
	            "[PlanogramProduct_PegDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_SqueezeHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_SqueezeWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_SqueezeDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_NestingHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_NestingWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_NestingDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_CasePackUnits] [SMALLINT] NOT NULL, " +
	            "[PlanogramProduct_CaseHigh] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_CaseWide] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_CaseDeep] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_CaseHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_CaseWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_CaseDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_MaxStack] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_MaxTopCap] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_MaxRightCap] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_MinDeep] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_MaxDeep] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_TrayPackUnits] [SMALLINT] NOT NULL, " +
	            "[PlanogramProduct_TrayHigh] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_TrayWide] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_TrayDeep] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_TrayHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_TrayWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_TrayDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_TrayThickHeight] [REAL] NOT NULL, " +
	            "[PlanogramProduct_TrayThickWidth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_TrayThickDepth] [REAL] NOT NULL, " +
	            "[PlanogramProduct_FrontOverhang] [REAL] NOT NULL, " +
	            "[PlanogramProduct_FingerSpaceAbove] [REAL] NOT NULL, " +
	            "[PlanogramProduct_FingerSpaceToTheSide] [REAL] NOT NULL, " +
	            "[PlanogramProduct_StatusType] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_OrientationType] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_MerchandisingStyle] [TINYINT] NOT NULL, " +
                "[PlanogramProduct_IsFrontOnly] [BIT] NOT NULL, " +
                "[PlanogramProduct_IsPlaceHolderProduct] [BIT] NOT NULL, " +
	            "[PlanogramProduct_IsActive] [BIT] NOT NULL, " +
	            "[PlanogramProduct_CanBreakTrayUp] [BIT] NOT NULL, " +
	            "[PlanogramProduct_CanBreakTrayDown] [BIT] NOT NULL, " +
	            "[PlanogramProduct_CanBreakTrayBack] [BIT] NOT NULL, " +
	            "[PlanogramProduct_CanBreakTrayTop] [BIT] NOT NULL, " +
	            "[PlanogramProduct_ForceMiddleCap] [BIT] NOT NULL, " +
	            "[PlanogramProduct_ForceBottomCap] [BIT] NOT NULL, " +
	            "[PlanogramProduct_PlanogramImageIdFront] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdBack] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdTop] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdBottom] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdLeft] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdRight] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdDisplayFront] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdDisplayBack] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdDisplayTop] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdDisplayBottom] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdDisplayLeft] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdDisplayRight] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdTrayFront] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdTrayBack] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdTrayTop] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdTrayBottom] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdTrayLeft] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdTrayRight] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdPointOfPurchaseFront] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdPointOfPurchaseBack] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdPointOfPurchaseTop] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdPointOfPurchaseBottom] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdPointOfPurchaseLeft] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdPointOfPurchaseRight] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdAlternateFront] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdAlternateBack] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdAlternateTop] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdAlternateBottom] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdAlternateLeft] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdAlternateRight] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdCaseFront] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdCaseBack] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdCaseTop] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdCaseBottom] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdCaseLeft] [INT] NULL, " +
	            "[PlanogramProduct_PlanogramImageIdCaseRight] [INT] NULL, " +
	            "[PlanogramProduct_ShapeType] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_FillPatternType] [TINYINT] NOT NULL, " +
	            "[PlanogramProduct_FillColour] [INT] NOT NULL, " +
                "[PlanogramProduct_Shape] [NVARCHAR](20) COLLATE database_default NULL, " +
                "[PlanogramProduct_PointOfPurchaseDescription] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_ShortDescription] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_Subcategory] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_CustomerStatus] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_Colour] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_Flavour] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_PackagingShape] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_PackagingType] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_CountryOfOrigin] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_CountryOfProcessing] [NVARCHAR](50) COLLATE database_default NULL, " +
	            "[PlanogramProduct_ShelfLife] [SMALLINT] NOT NULL, " +
	            "[PlanogramProduct_DeliveryFrequencyDays] [REAL] NULL, " +
                "[PlanogramProduct_DeliveryMethod] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_VendorCode] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_Vendor] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_ManufacturerCode] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_Manufacturer] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_Size] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_UnitOfMeasure] [NVARCHAR](50) COLLATE database_default NULL, " +
	            "[PlanogramProduct_DateIntroduced] [DATETIME] NULL, " +
	            "[PlanogramProduct_DateDiscontinued] [DATETIME] NULL, " +
	            "[PlanogramProduct_DateEffective] [DATETIME] NULL, " +
                "[PlanogramProduct_Health] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_CorporateCode] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_Barcode] [NVARCHAR](50) COLLATE database_default NULL, " +
	            "[PlanogramProduct_SellPrice] [REAL] NULL, " +
	            "[PlanogramProduct_SellPackCount] [SMALLINT] NULL, " +
                "[PlanogramProduct_SellPackDescription] [NVARCHAR](100) COLLATE database_default NULL, " +
	            "[PlanogramProduct_RecommendedRetailPrice] [REAL] NULL, " +
	            "[PlanogramProduct_ManufacturerRecommendedRetailPrice] [REAL] NULL, " +
	            "[PlanogramProduct_CostPrice] [REAL] NULL, " +
	            "[PlanogramProduct_CaseCost] [REAL] NULL, " +
	            "[PlanogramProduct_TaxRate] [REAL] NULL, " +
                "[PlanogramProduct_ConsumerInformation] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_Texture] [NVARCHAR](100) COLLATE database_default NULL, " +
	            "[PlanogramProduct_StyleNumber] [SMALLINT] NULL, " +
                "[PlanogramProduct_Pattern] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_Model] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_GarmentType] [NVARCHAR](100) COLLATE database_default NULL, " +
	            "[PlanogramProduct_IsPrivateLabel] [BIT] NOT NULL, " +
	            "[PlanogramProduct_IsNewProduct] [BIT] NOT NULL, " +
                "[PlanogramProduct_FinancialGroupCode] [NVARCHAR](50) COLLATE database_default NULL, " +
                "[PlanogramProduct_FinancialGroupName] [NVARCHAR](100) COLLATE database_default NULL, " +
                "[PlanogramProduct_MetaNotAchievedInventory] [BIT] NULL, " +
                "[PlanogramProduct_MetaTotalUnits] [INT] NULL, " +
                "[PlanogramProduct_MetaPlanogramLinearSpacePercentage] [REAL] NULL, " +
                "[PlanogramProduct_MetaPlanogramAreaSpacePercentage] [REAL] NULL, " +
                "[PlanogramProduct_MetaPlanogramVolumetricSpacePercentage] [REAL] NULL, " +
                "[PlanogramProduct_MetaPositionCount] [INT] NULL, " +
                "[PlanogramProduct_MetaTotalFacings] [INT] NULL, " +
                "[PlanogramProduct_MetaTotalLinearSpace] [REAL] NULL, " +
                "[PlanogramProduct_MetaTotalAreaSpace] [REAL] NULL, " +
                "[PlanogramProduct_MetaTotalVolumetricSpace] [REAL] NULL, " +
                "[PlanogramProduct_MetaIsInMasterData] [BIT] NULL, " +
                "[PlanogramProduct_MetaNotAchievedCases] [BIT] NULL, " +
                "[PlanogramProduct_MetaNotAchievedDOS] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsOverShelfLifePercent] [BIT] NULL, " +
                "[PlanogramProduct_MetaNotAchievedDeliveries] [BIT] NULL, " +
                "[PlanogramProduct_MetaTotalMainFacings] [INT] NULL, " +
                "[PlanogramProduct_MetaTotalXFacings] [INT] NULL, " +
                "[PlanogramProduct_MetaTotalYFacings] [INT] NULL, " +
                "[PlanogramProduct_MetaTotalZFacings] [INT] NULL, " +
                "[PlanogramProduct_MetaIsRangedInAssortment] [BIT] NULL, " +
                "[PlanogramProduct_ColourGroupValue] [NVARCHAR](255) NULL, " +
                "[PlanogramProduct_MetaCDTNode] [NVARCHAR](255) NULL," +
                "[PlanogramProduct_MetaIsProductRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsFamilyRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsInheritanceRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsLocalProductRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsDistributionRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsCoreRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsDelistProductRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsForceProductRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsPreserveProductRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsMinimumHurdleProductRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsMaximumProductFamilyRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsMinimumProductFamilyRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsDependencyFamilyRuleBroken] [BIT] NULL,  " +
                "[PlanogramProduct_MetaComparisonStatus] [TINYINT] NOT NULL, " +
                "[PlanogramProduct_MetaIsDelistFamilyRuleBroken] [BIT] NULL, " +
                "[PlanogramProduct_MetaIsBuddied] [BIT] NULL" +
            ")";


        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class PlanogramProductBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<PlanogramProductDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public PlanogramProductBulkCopyDataReader(IEnumerable<PlanogramProductDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 191; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.PlanogramId;
                        break;
                    case 2:
                        value = _enumerator.Current.Gtin;
                        break;
                    case 3:
                        value = _enumerator.Current.Name;
                        break;
                    case 4:
                        value = _enumerator.Current.Brand;
                        break;
                    case 5:
                        value = _enumerator.Current.Height;
                        break;
                    case 6:
                        value = _enumerator.Current.Width;
                        break;
                    case 7:
                        value = _enumerator.Current.Depth;
                        break;
                    case 8:
                        value = _enumerator.Current.DisplayHeight;
                        break;
                    case 9:
                        value = _enumerator.Current.DisplayWidth;
                        break;
                    case 10:
                        value = _enumerator.Current.DisplayDepth;
                        break;
                    case 11:
                        value = _enumerator.Current.AlternateHeight;
                        break;
                    case 12:
                        value = _enumerator.Current.AlternateWidth;
                        break;
                    case 13:
                        value = _enumerator.Current.AlternateDepth;
                        break;
                    case 14:
                        value = _enumerator.Current.PointOfPurchaseHeight;
                        break;
                    case 15:
                        value = _enumerator.Current.PointOfPurchaseWidth;
                        break;
                    case 16:
                        value = _enumerator.Current.PointOfPurchaseDepth;
                        break;
                    case 17:
                        value = _enumerator.Current.NumberOfPegHoles;
                        break;
                    case 18:
                        value = _enumerator.Current.PegX;
                        break;
                    case 19:
                        value = _enumerator.Current.PegX2;
                        break;
                    case 20:
                        value = _enumerator.Current.PegX3;
                        break;
                    case 21:
                        value = _enumerator.Current.PegY;
                        break;
                    case 22:
                        value = _enumerator.Current.PegY2;
                        break;
                    case 23:
                        value = _enumerator.Current.PegY3;
                        break;
                    case 24:
                        value = _enumerator.Current.PegProngOffsetX;
                        break;
                    case 25:
                        value = _enumerator.Current.PegProngOffsetY;
                        break;
                    case 26:
                        value = _enumerator.Current.PegDepth;
                        break;
                    case 27:
                        value = _enumerator.Current.SqueezeHeight;
                        break;
                    case 28:
                        value = _enumerator.Current.SqueezeWidth;
                        break;
                    case 29:
                        value = _enumerator.Current.SqueezeDepth;
                        break;
                    case 30:
                        value = _enumerator.Current.NestingHeight;
                        break;
                    case 31:
                        value = _enumerator.Current.NestingWidth;
                        break;
                    case 32:
                        value = _enumerator.Current.NestingDepth;
                        break;
                    case 33:
                        value = _enumerator.Current.CasePackUnits;
                        break;
                    case 34:
                        value = _enumerator.Current.CaseHigh;
                        break;
                    case 35:
                        value = _enumerator.Current.CaseWide;
                        break;
                    case 36:
                        value = _enumerator.Current.CaseDeep;
                        break;
                    case 37:
                        value = _enumerator.Current.CaseHeight;
                        break;
                    case 38:
                        value = _enumerator.Current.CaseWidth;
                        break;
                    case 39:
                        value = _enumerator.Current.CaseDepth;
                        break;
                    case 40:
                        value = _enumerator.Current.MaxStack;
                        break;
                    case 41:
                        value = _enumerator.Current.MaxTopCap;
                        break;
                    case 42:
                        value = _enumerator.Current.MaxRightCap;
                        break;
                    case 43:
                        value = _enumerator.Current.MinDeep;
                        break;
                    case 44:
                        value = _enumerator.Current.MaxDeep;
                        break;
                    case 45:
                        value = _enumerator.Current.TrayPackUnits;
                        break;
                    case 46:
                        value = _enumerator.Current.TrayHigh;
                        break;
                    case 47:
                        value = _enumerator.Current.TrayWide;
                        break;
                    case 48:
                        value = _enumerator.Current.TrayDeep;
                        break;
                    case 49:
                        value = _enumerator.Current.TrayHeight;
                        break;
                    case 50:
                        value = _enumerator.Current.TrayWidth;
                        break;
                    case 51:
                        value = _enumerator.Current.TrayDepth;
                        break;
                    case 52:
                        value = _enumerator.Current.TrayThickHeight;
                        break;
                    case 53:
                        value = _enumerator.Current.TrayThickWidth;
                        break;
                    case 54:
                        value = _enumerator.Current.TrayThickDepth;
                        break;
                    case 55:
                        value = _enumerator.Current.FrontOverhang;
                        break;
                    case 56:
                        value = _enumerator.Current.FingerSpaceAbove;
                        break;
                    case 57:
                        value = _enumerator.Current.FingerSpaceToTheSide;
                        break;
                    case 58:
                        value = _enumerator.Current.StatusType;
                        break;
                    case 59:
                        value = _enumerator.Current.OrientationType;
                        break;
                    case 60:
                        value = _enumerator.Current.MerchandisingStyle;
                        break;
                    case 61:
                        value = _enumerator.Current.IsFrontOnly;
                        break;
                    case 62:
                        value = _enumerator.Current.IsPlaceHolderProduct;
                        break;
                    case 63:
                        value = _enumerator.Current.IsActive;
                        break;
                    case 64:
                        value = _enumerator.Current.CanBreakTrayUp;
                        break;
                    case 65:
                        value = _enumerator.Current.CanBreakTrayDown;
                        break;
                    case 66:
                        value = _enumerator.Current.CanBreakTrayBack;
                        break;
                    case 67:
                        value = _enumerator.Current.CanBreakTrayTop;
                        break;
                    case 68:
                        value = _enumerator.Current.ForceMiddleCap;
                        break;
                    case 69:
                        value = _enumerator.Current.ForceBottomCap;
                        break;
                    case 70:
                        value = _enumerator.Current.PlanogramImageIdFront;
                        break;
                    case 71:
                        value = _enumerator.Current.PlanogramImageIdBack;
                        break;
                    case 72:
                        value = _enumerator.Current.PlanogramImageIdTop;
                        break;
                    case 73:
                        value = _enumerator.Current.PlanogramImageIdBottom;
                        break;
                    case 74:
                        value = _enumerator.Current.PlanogramImageIdLeft;
                        break;
                    case 75:
                        value = _enumerator.Current.PlanogramImageIdRight;
                        break;
                    case 76:
                        value = _enumerator.Current.PlanogramImageIdDisplayFront;
                        break;
                    case 77:
                        value = _enumerator.Current.PlanogramImageIdDisplayBack;
                        break;
                    case 78:
                        value = _enumerator.Current.PlanogramImageIdDisplayTop;
                        break;
                    case 79:
                        value = _enumerator.Current.PlanogramImageIdDisplayBottom;
                        break;
                    case 80:
                        value = _enumerator.Current.PlanogramImageIdDisplayLeft;
                        break;
                    case 81:
                        value = _enumerator.Current.PlanogramImageIdDisplayRight;
                        break;
                    case 82:
                        value = _enumerator.Current.PlanogramImageIdTrayFront;
                        break;
                    case 83:
                        value = _enumerator.Current.PlanogramImageIdTrayBack;
                        break;
                    case 84:
                        value = _enumerator.Current.PlanogramImageIdTrayTop;
                        break;
                    case 85:
                        value = _enumerator.Current.PlanogramImageIdTrayBottom;
                        break;
                    case 86:
                        value = _enumerator.Current.PlanogramImageIdTrayLeft;
                        break;
                    case 87:
                        value = _enumerator.Current.PlanogramImageIdTrayRight;
                        break;
                    case 88:
                        value = _enumerator.Current.PlanogramImageIdPointOfPurchaseFront;
                        break;
                    case 89:
                        value = _enumerator.Current.PlanogramImageIdPointOfPurchaseBack;
                        break;
                    case 90:
                        value = _enumerator.Current.PlanogramImageIdPointOfPurchaseTop;
                        break;
                    case 91:
                        value = _enumerator.Current.PlanogramImageIdPointOfPurchaseBottom;
                        break;
                    case 92:
                        value = _enumerator.Current.PlanogramImageIdPointOfPurchaseLeft;
                        break;
                    case 93:
                        value = _enumerator.Current.PlanogramImageIdPointOfPurchaseRight;
                        break;
                    case 94:
                        value = _enumerator.Current.PlanogramImageIdAlternateFront;
                        break;
                    case 95:
                        value = _enumerator.Current.PlanogramImageIdAlternateBack;
                        break;
                    case 96:
                        value = _enumerator.Current.PlanogramImageIdAlternateTop;
                        break;
                    case 97:
                        value = _enumerator.Current.PlanogramImageIdAlternateBottom;
                        break;
                    case 98:
                        value = _enumerator.Current.PlanogramImageIdAlternateLeft;
                        break;
                    case 99:
                        value = _enumerator.Current.PlanogramImageIdAlternateRight;
                        break;
                    case 100:
                        value = _enumerator.Current.PlanogramImageIdCaseFront;
                        break;
                    case 101:
                        value = _enumerator.Current.PlanogramImageIdCaseBack;
                        break;
                    case 102:
                        value = _enumerator.Current.PlanogramImageIdCaseTop;
                        break;
                    case 103:
                        value = _enumerator.Current.PlanogramImageIdCaseBottom;
                        break;
                    case 104:
                        value = _enumerator.Current.PlanogramImageIdCaseLeft;
                        break;
                    case 105:
                        value = _enumerator.Current.PlanogramImageIdCaseRight;
                        break;
                    case 106:
                        value = _enumerator.Current.ShapeType;
                        break;
                    case 107:
                        value = _enumerator.Current.FillPatternType;
                        break;
                    case 108:
                        value = _enumerator.Current.FillColour;
                        break;
                    case 109:
                        value = _enumerator.Current.Shape;
                        break;
                    case 110:
                        value = _enumerator.Current.PointOfPurchaseDescription;
                        break;
                    case 111:
                        value = _enumerator.Current.ShortDescription;
                        break;
                    case 112:
                        value = _enumerator.Current.Subcategory;
                        break;
                    case 113:
                        value = _enumerator.Current.CustomerStatus;
                        break;
                    case 114:
                        value = _enumerator.Current.Colour;
                        break;
                    case 115:
                        value = _enumerator.Current.Flavour;
                        break;
                    case 116:
                        value = _enumerator.Current.PackagingShape;
                        break;
                    case 117:
                        value = _enumerator.Current.PackagingType;
                        break;
                    case 118:
                        value = _enumerator.Current.CountryOfOrigin;
                        break;
                    case 119:
                        value = _enumerator.Current.CountryOfProcessing;
                        break;
                    case 120:
                        value = _enumerator.Current.ShelfLife;
                        break;
                    case 121:
                        value = _enumerator.Current.DeliveryFrequencyDays;
                        break;
                    case 122:
                        value = _enumerator.Current.DeliveryMethod;
                        break;
                    case 123:
                        value = _enumerator.Current.VendorCode;
                        break;
                    case 124:
                        value = _enumerator.Current.Vendor;
                        break;
                    case 125:
                        value = _enumerator.Current.ManufacturerCode;
                        break;
                    case 126:
                        value = _enumerator.Current.Manufacturer;
                        break;
                    case 127:
                        value = _enumerator.Current.Size;
                        break;
                    case 128:
                        value = _enumerator.Current.UnitOfMeasure;
                        break;
                    case 129:
                        value = _enumerator.Current.DateIntroduced;
                        break;
                    case 130:
                        value = _enumerator.Current.DateDiscontinued;
                        break;
                    case 131:
                        value = _enumerator.Current.DateEffective;
                        break;
                    case 132:
                        value = _enumerator.Current.Health;
                        break;
                    case 133:
                        value = _enumerator.Current.CorporateCode;
                        break;
                    case 134:
                        value = _enumerator.Current.Barcode;
                        break;
                    case 135:
                        value = _enumerator.Current.SellPrice;
                        break;
                    case 136:
                        value = _enumerator.Current.SellPackCount;
                        break;
                    case 137:
                        value = _enumerator.Current.SellPackDescription;
                        break;
                    case 138:
                        value = _enumerator.Current.RecommendedRetailPrice;
                        break;
                    case 139:
                        value = _enumerator.Current.ManufacturerRecommendedRetailPrice;
                        break;
                    case 140:
                        value = _enumerator.Current.CostPrice;
                        break;
                    case 141:
                        value = _enumerator.Current.CaseCost;
                        break;
                    case 142:
                        value = _enumerator.Current.TaxRate;
                        break;
                    case 143:
                        value = _enumerator.Current.ConsumerInformation;
                        break;
                    case 144:
                        value = _enumerator.Current.Texture;
                        break;
                    case 145:
                        value = _enumerator.Current.StyleNumber;
                        break;
                    case 146:
                        value = _enumerator.Current.Pattern;
                        break;
                    case 147:
                        value = _enumerator.Current.Model;
                        break;
                    case 148:
                        value = _enumerator.Current.GarmentType;
                        break;
                    case 149:
                        value = _enumerator.Current.IsPrivateLabel;
                        break;
                    case 150:
                        value = _enumerator.Current.IsNewProduct;
                        break;
                    case 151:
                        value = _enumerator.Current.FinancialGroupCode;
                        break;
                    case 152:
                        value = _enumerator.Current.FinancialGroupName;
                        break;
                    case 153:
                        value = _enumerator.Current.MetaNotAchievedInventory;
                        break;
                    case 154:
                        value = _enumerator.Current.MetaTotalUnits;
                        break;
                    case 155:
                        value = _enumerator.Current.MetaPlanogramLinearSpacePercentage;
                        break;
                    case 156:
                        value = _enumerator.Current.MetaPlanogramAreaSpacePercentage;
                        break;
                    case 157:
                        value = _enumerator.Current.MetaPlanogramVolumetricSpacePercentage;
                        break;
                    case 158:
                        value = _enumerator.Current.MetaPositionCount;
                        break;
                    case 159:
                        value = _enumerator.Current.MetaTotalFacings;
                        break;
                    case 160:
                        value = _enumerator.Current.MetaTotalLinearSpace;
                        break;
                    case 161:
                        value = _enumerator.Current.MetaTotalAreaSpace;
                        break;
                    case 162:
                        value = _enumerator.Current.MetaTotalVolumetricSpace;
                        break;
                    case 163:
                        value = _enumerator.Current.MetaIsInMasterData;
                        break;
                    case 164:
                        value = _enumerator.Current.MetaNotAchievedCases;
                        break;
                    case 165:
                        value = _enumerator.Current.MetaNotAchievedDOS;
                        break;
                    case 166:
                        value = _enumerator.Current.MetaIsOverShelfLifePercent;
                        break;
                    case 167:
                        value = _enumerator.Current.MetaNotAchievedDeliveries;
                        break;
                    case 168:
                        value = _enumerator.Current.MetaTotalMainFacings;
                        break;
                    case 169:
                        value = _enumerator.Current.MetaTotalXFacings;
                        break;
                    case 170:
                        value = _enumerator.Current.MetaTotalYFacings;
                        break;
                    case 171:
                        value = _enumerator.Current.MetaTotalZFacings;
                        break;
                    case 172:
                        value = _enumerator.Current.MetaIsRangedInAssortment;
                        break;
                    case 173:
                        value = _enumerator.Current.ColourGroupValue;
                        break;
                    case 174:
                        value = _enumerator.Current.MetaCDTNode;
                        break;
                    case 175:
                        value = _enumerator.Current.MetaIsProductRuleBroken;
                        break;
                    case 176:
                        value = _enumerator.Current.MetaIsFamilyRuleBroken;
                        break;
                    case 177:
                        value = _enumerator.Current.MetaIsInheritanceRuleBroken;
                        break;
                    case 178:
                        value = _enumerator.Current.MetaIsLocalProductRuleBroken;
                        break;
                    case 179:
                        value = _enumerator.Current.MetaIsDistributionRuleBroken;
                        break;
                    case 180:
                        value = _enumerator.Current.MetaIsCoreRuleBroken;
                        break;
                    case 181:
                        value = _enumerator.Current.MetaIsDelistProductRuleBroken;
                        break;
                    case 182:
                        value = _enumerator.Current.MetaIsForceProductRuleBroken;
                        break;
                    case 183:
                        value = _enumerator.Current.MetaIsPreserveProductRuleBroken;
                        break;
                    case 184:
                        value = _enumerator.Current.MetaIsMinimumHurdleProductRuleBroken;
                        break;
                    case 185:
                        value = _enumerator.Current.MetaIsMaximumProductFamilyRuleBroken;
                        break;
                    case 186:
                        value = _enumerator.Current.MetaIsMinimumProductFamilyRuleBroken;
                        break;
                    case 187:
                        value = _enumerator.Current.MetaIsDependencyFamilyRuleBroken;
                        break;
                    case 188:
                        value = _enumerator.Current.MetaComparisonStatus;
                        break;
                    case 189:
                        value = _enumerator.Current.MetaIsDelistFamilyRuleBroken;
                        break;
                    case 190:
                        value = _enumerator.Current.MetaIsBuddied;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramProductDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramProductDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramProductId]),
                PlanogramId = (Int32)GetValue(dr[FieldNames.PlanogramProductPlanogramId]),
                Gtin = (String)GetValue(dr[FieldNames.PlanogramProductGtin]),
                Name = (String)GetValue(dr[FieldNames.PlanogramProductName]),
                Brand = (String)GetValue(dr[FieldNames.PlanogramProductBrand]),
                Height = (Single)GetValue(dr[FieldNames.PlanogramProductHeight]),
                Width = (Single)GetValue(dr[FieldNames.PlanogramProductWidth]),
                Depth = (Single)GetValue(dr[FieldNames.PlanogramProductDepth]),
                DisplayHeight = (Single)GetValue(dr[FieldNames.PlanogramProductDisplayHeight]),
                DisplayWidth = (Single)GetValue(dr[FieldNames.PlanogramProductDisplayWidth]),
                DisplayDepth = (Single)GetValue(dr[FieldNames.PlanogramProductDisplayDepth]),
                AlternateHeight = (Single)GetValue(dr[FieldNames.PlanogramProductAlternateHeight]),
                AlternateWidth = (Single)GetValue(dr[FieldNames.PlanogramProductAlternateWidth]),
                AlternateDepth = (Single)GetValue(dr[FieldNames.PlanogramProductAlternateDepth]),
                PointOfPurchaseHeight = (Single)GetValue(dr[FieldNames.PlanogramProductPointOfPurchaseHeight]),
                PointOfPurchaseWidth = (Single)GetValue(dr[FieldNames.PlanogramProductPointOfPurchaseWidth]),
                PointOfPurchaseDepth = (Single)GetValue(dr[FieldNames.PlanogramProductPointOfPurchaseDepth]),
                NumberOfPegHoles = (Byte)GetValue(dr[FieldNames.PlanogramProductNumberOfPegHoles]),
                PegX = (Single)GetValue(dr[FieldNames.PlanogramProductPegX]),
                PegX2 = (Single)GetValue(dr[FieldNames.PlanogramProductPegX2]),
                PegX3 = (Single)GetValue(dr[FieldNames.PlanogramProductPegX3]),
                PegY = (Single)GetValue(dr[FieldNames.PlanogramProductPegY]),
                PegY2 = (Single)GetValue(dr[FieldNames.PlanogramProductPegY2]),
                PegY3 = (Single)GetValue(dr[FieldNames.PlanogramProductPegY3]),
                PegProngOffsetX = (Single) (dr[FieldNames.PlanogramProductPegProngOffsetX] is DBNull ? 0.0F : GetValue(dr[FieldNames.PlanogramProductPegProngOffsetX])),
                PegProngOffsetY = (Single) (dr[FieldNames.PlanogramProductPegProngOffsetY] is DBNull ? 0.0F : GetValue(dr[FieldNames.PlanogramProductPegProngOffsetY])),
                PegDepth = (Single)GetValue(dr[FieldNames.PlanogramProductPegDepth]),
                SqueezeHeight = (Single)GetValue(dr[FieldNames.PlanogramProductSqueezeHeight]),
                SqueezeWidth = (Single)GetValue(dr[FieldNames.PlanogramProductSqueezeWidth]),
                SqueezeDepth = (Single)GetValue(dr[FieldNames.PlanogramProductSqueezeDepth]),
                NestingHeight = (Single)GetValue(dr[FieldNames.PlanogramProductNestingHeight]),
                NestingWidth = (Single)GetValue(dr[FieldNames.PlanogramProductNestingWidth]),
                NestingDepth = (Single)GetValue(dr[FieldNames.PlanogramProductNestingDepth]),
                CasePackUnits = (Int16)GetValue(dr[FieldNames.PlanogramProductCasePackUnits]),
                CaseHigh = (Byte)GetValue(dr[FieldNames.PlanogramProductCaseHigh]),
                CaseWide = (Byte)GetValue(dr[FieldNames.PlanogramProductCaseWide]),
                CaseDeep = (Byte)GetValue(dr[FieldNames.PlanogramProductCaseDeep]),
                CaseHeight = (Single)GetValue(dr[FieldNames.PlanogramProductCaseHeight]),
                CaseWidth = (Single)GetValue(dr[FieldNames.PlanogramProductCaseWidth]),
                CaseDepth = (Single)GetValue(dr[FieldNames.PlanogramProductCaseDepth]),
                MaxStack = (Byte)GetValue(dr[FieldNames.PlanogramProductMaxStack]),
                MaxTopCap = (Byte)GetValue(dr[FieldNames.PlanogramProductMaxTopCap]),
                MaxRightCap = (Byte)GetValue(dr[FieldNames.PlanogramProductMaxRightCap]),
                MinDeep = (Byte)GetValue(dr[FieldNames.PlanogramProductMinDeep]),
                MaxDeep = (Byte)GetValue(dr[FieldNames.PlanogramProductMaxDeep]),
                TrayPackUnits = (Int16)GetValue(dr[FieldNames.PlanogramProductTrayPackUnits]),
                TrayHigh = (Byte)GetValue(dr[FieldNames.PlanogramProductTrayHigh]),
                TrayWide = (Byte)GetValue(dr[FieldNames.PlanogramProductTrayWide]),
                TrayDeep = (Byte)GetValue(dr[FieldNames.PlanogramProductTrayDeep]),
                TrayHeight = (Single)GetValue(dr[FieldNames.PlanogramProductTrayHeight]),
                TrayWidth = (Single)GetValue(dr[FieldNames.PlanogramProductTrayWidth]),
                TrayDepth = (Single)GetValue(dr[FieldNames.PlanogramProductTrayDepth]),
                TrayThickHeight = (Single)GetValue(dr[FieldNames.PlanogramProductTrayThickHeight]),
                TrayThickWidth = (Single)GetValue(dr[FieldNames.PlanogramProductTrayThickWidth]),
                TrayThickDepth = (Single)GetValue(dr[FieldNames.PlanogramProductTrayThickDepth]),
                FrontOverhang = (Single)GetValue(dr[FieldNames.PlanogramProductFrontOverhang]),
                FingerSpaceAbove = (Single)GetValue(dr[FieldNames.PlanogramProductFingerSpaceAbove]),
                FingerSpaceToTheSide = (Single)GetValue(dr[FieldNames.PlanogramProductFingerSpaceToTheSide]),
                StatusType = (Byte)GetValue(dr[FieldNames.PlanogramProductStatusType]),
                OrientationType = (Byte)GetValue(dr[FieldNames.PlanogramProductOrientationType]),
                MerchandisingStyle = (Byte)GetValue(dr[FieldNames.PlanogramProductMerchandisingStyle]),
                IsFrontOnly = (Boolean)GetValue(dr[FieldNames.PlanogramProductIsFrontOnly]),
                IsPlaceHolderProduct = (Boolean)GetValue(dr[FieldNames.PlanogramProductIsPlaceHolderProduct]),
                IsActive = (Boolean)GetValue(dr[FieldNames.PlanogramProductIsActive]),
                CanBreakTrayUp = (Boolean)GetValue(dr[FieldNames.PlanogramProductCanBreakTrayUp]),
                CanBreakTrayDown = (Boolean)GetValue(dr[FieldNames.PlanogramProductCanBreakTrayDown]),
                CanBreakTrayBack = (Boolean)GetValue(dr[FieldNames.PlanogramProductCanBreakTrayBack]),
                CanBreakTrayTop = (Boolean)GetValue(dr[FieldNames.PlanogramProductCanBreakTrayTop]),
                ForceMiddleCap = (Boolean)GetValue(dr[FieldNames.PlanogramProductForceMiddleCap]),
                ForceBottomCap = (Boolean)GetValue(dr[FieldNames.PlanogramProductForceBottomCap]),
                PlanogramImageIdFront = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdFront]),
                PlanogramImageIdBack = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdBack]),
                PlanogramImageIdTop = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdTop]),
                PlanogramImageIdBottom = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdBottom]),
                PlanogramImageIdLeft = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdLeft]),
                PlanogramImageIdRight = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdRight]),
                PlanogramImageIdDisplayFront = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdDisplayFront]),
                PlanogramImageIdDisplayBack = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdDisplayBack]),
                PlanogramImageIdDisplayTop = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdDisplayTop]),
                PlanogramImageIdDisplayBottom = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdDisplayBottom]),
                PlanogramImageIdDisplayLeft = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdDisplayLeft]),
                PlanogramImageIdDisplayRight = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdDisplayRight]),
                PlanogramImageIdTrayFront = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdTrayFront]),
                PlanogramImageIdTrayBack = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdTrayBack]),
                PlanogramImageIdTrayTop = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdTrayTop]),
                PlanogramImageIdTrayBottom = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdTrayBottom]),
                PlanogramImageIdTrayLeft = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdTrayLeft]),
                PlanogramImageIdTrayRight = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdTrayRight]),
                PlanogramImageIdPointOfPurchaseFront = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseFront]),
                PlanogramImageIdPointOfPurchaseBack = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBack]),
                PlanogramImageIdPointOfPurchaseTop = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseTop]),
                PlanogramImageIdPointOfPurchaseBottom = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBottom]),
                PlanogramImageIdPointOfPurchaseLeft = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseLeft]),
                PlanogramImageIdPointOfPurchaseRight = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseRight]),
                PlanogramImageIdAlternateFront = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdAlternateFront]),
                PlanogramImageIdAlternateBack = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdAlternateBack]),
                PlanogramImageIdAlternateTop = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdAlternateTop]),
                PlanogramImageIdAlternateBottom = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdAlternateBottom]),
                PlanogramImageIdAlternateLeft = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdAlternateLeft]),
                PlanogramImageIdAlternateRight = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdAlternateRight]),
                PlanogramImageIdCaseFront = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdCaseFront]),
                PlanogramImageIdCaseBack = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdCaseBack]),
                PlanogramImageIdCaseTop = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdCaseTop]),
                PlanogramImageIdCaseBottom = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdCaseBottom]),
                PlanogramImageIdCaseLeft = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdCaseLeft]),
                PlanogramImageIdCaseRight = (Int32?)GetValue(dr[FieldNames.PlanogramProductPlanogramImageIdCaseRight]),
                ShapeType = (Byte)GetValue(dr[FieldNames.PlanogramProductShapeType]),
                FillPatternType = (Byte)GetValue(dr[FieldNames.PlanogramProductFillPatternType]),
                FillColour = (Int32)GetValue(dr[FieldNames.PlanogramProductFillColour]),
                Shape = (String)GetValue(dr[FieldNames.PlanogramProductShape]),
                PointOfPurchaseDescription = (String)GetValue(dr[FieldNames.PlanogramProductPointOfPurchaseDescription]),
                ShortDescription = (String)GetValue(dr[FieldNames.PlanogramProductShortDescription]),
                Subcategory = (String)GetValue(dr[FieldNames.PlanogramProductSubcategory]),
                CustomerStatus = (String)GetValue(dr[FieldNames.PlanogramProductCustomerStatus]),
                Colour = (String)GetValue(dr[FieldNames.PlanogramProductColour]),
                Flavour = (String)GetValue(dr[FieldNames.PlanogramProductFlavour]),
                PackagingShape = (String)GetValue(dr[FieldNames.PlanogramProductPackagingShape]),
                PackagingType = (String)GetValue(dr[FieldNames.PlanogramProductPackagingType]),
                CountryOfOrigin = (String)GetValue(dr[FieldNames.PlanogramProductCountryOfOrigin]),
                CountryOfProcessing = (String)GetValue(dr[FieldNames.PlanogramProductCountryOfProcessing]),
                ShelfLife = (Int16)GetValue(dr[FieldNames.PlanogramProductShelfLife]),
                DeliveryFrequencyDays = (Single?)GetValue(dr[FieldNames.PlanogramProductDeliveryFrequencyDays]),
                DeliveryMethod = (String)GetValue(dr[FieldNames.PlanogramProductDeliveryMethod]),
                VendorCode = (String)GetValue(dr[FieldNames.PlanogramProductVendorCode]),
                Vendor = (String)GetValue(dr[FieldNames.PlanogramProductVendor]),
                ManufacturerCode = (String)GetValue(dr[FieldNames.PlanogramProductManufacturerCode]),
                Manufacturer = (String)GetValue(dr[FieldNames.PlanogramProductManufacturer]),
                Size = (String)GetValue(dr[FieldNames.PlanogramProductSize]),
                UnitOfMeasure = (String)GetValue(dr[FieldNames.PlanogramProductUnitOfMeasure]),
                DateIntroduced = (DateTime?)GetValue(dr[FieldNames.PlanogramProductDateIntroduced]),
                DateDiscontinued = (DateTime?)GetValue(dr[FieldNames.PlanogramProductDateDiscontinued]),
                DateEffective = (DateTime?)GetValue(dr[FieldNames.PlanogramProductDateEffective]),
                Health = (String)GetValue(dr[FieldNames.PlanogramProductHealth]),
                CorporateCode = (String)GetValue(dr[FieldNames.PlanogramProductCorporateCode]),
                Barcode = (String)GetValue(dr[FieldNames.PlanogramProductBarcode]),
                SellPrice = (Single?)GetValue(dr[FieldNames.PlanogramProductSellPrice]),
                SellPackCount = (Int16?)GetValue(dr[FieldNames.PlanogramProductSellPackCount]),
                SellPackDescription = (String)GetValue(dr[FieldNames.PlanogramProductSellPackDescription]),
                RecommendedRetailPrice = (Single?)GetValue(dr[FieldNames.PlanogramProductRecommendedRetailPrice]),
                ManufacturerRecommendedRetailPrice = (Single?)GetValue(dr[FieldNames.PlanogramProductManufacturerRecommendedRetailPrice]),
                CostPrice = (Single?)GetValue(dr[FieldNames.PlanogramProductCostPrice]),
                CaseCost = (Single?)GetValue(dr[FieldNames.PlanogramProductCaseCost]),
                TaxRate = (Single?)GetValue(dr[FieldNames.PlanogramProductTaxRate]),
                ConsumerInformation = (String)GetValue(dr[FieldNames.PlanogramProductConsumerInformation]),
                Texture = (String)GetValue(dr[FieldNames.PlanogramProductTexture]),
                StyleNumber = (Int16?)GetValue(dr[FieldNames.PlanogramProductStyleNumber]),
                Pattern = (String)GetValue(dr[FieldNames.PlanogramProductPattern]),
                Model = (String)GetValue(dr[FieldNames.PlanogramProductModel]),
                GarmentType = (String)GetValue(dr[FieldNames.PlanogramProductGarmentType]),
                IsPrivateLabel = (Boolean)GetValue(dr[FieldNames.PlanogramProductIsPrivateLabel]),
                IsNewProduct = (Boolean)GetValue(dr[FieldNames.PlanogramProductIsNewProduct]),
                FinancialGroupCode = (String)GetValue(dr[FieldNames.PlanogramProductFinancialGroupCode]),
                FinancialGroupName = (String)GetValue(dr[FieldNames.PlanogramProductFinancialGroupName]),
                MetaNotAchievedInventory = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaNotAchievedInventory]),
                MetaTotalUnits = (Int32?)GetValue(dr[FieldNames.PlanogramProductMetaTotalUnits]),
                MetaPlanogramLinearSpacePercentage = (Single?)GetValue(dr[FieldNames.PlanogramProductMetaPlanogramLinearSpacePercentage]),
                MetaPlanogramAreaSpacePercentage = (Single?)GetValue(dr[FieldNames.PlanogramProductMetaPlanogramAreaSpacePercentage]),
                MetaPlanogramVolumetricSpacePercentage = (Single?)GetValue(dr[FieldNames.PlanogramProductMetaPlanogramVolumetricSpacePercentage]),
                MetaPositionCount = (Int32?)GetValue(dr[FieldNames.PlanogramProductMetaPositionCount]),
                MetaTotalFacings = (Int32?)GetValue(dr[FieldNames.PlanogramProductMetaTotalFacings]),
                MetaTotalLinearSpace = (Single?)GetValue(dr[FieldNames.PlanogramProductMetaTotalLinearSpace]),
                MetaTotalAreaSpace = (Single?)GetValue(dr[FieldNames.PlanogramProductMetaTotalAreaSpace]),
                MetaTotalVolumetricSpace = (Single?)GetValue(dr[FieldNames.PlanogramProductMetaTotalVolumetricSpace]),
                MetaIsInMasterData = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsInMasterData]),
                MetaNotAchievedCases = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaNotAchievedCases]),
                MetaNotAchievedDOS = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaNotAchievedDOS]),
                MetaIsOverShelfLifePercent = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsOverShelfLifePercent]),
                MetaNotAchievedDeliveries = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaNotAchievedDeliveries]),
                MetaTotalMainFacings = (Int32?)GetValue(dr[FieldNames.PlanogramProductMetaTotalMainFacings]),
                MetaTotalXFacings = (Int32?)GetValue(dr[FieldNames.PlanogramProductMetaTotalXFacings]),
                MetaTotalYFacings = (Int32?)GetValue(dr[FieldNames.PlanogramProductMetaTotalYFacings]),
                MetaTotalZFacings = (Int32?)GetValue(dr[FieldNames.PlanogramProductMetaTotalZFacings]),
                MetaIsRangedInAssortment = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsRangedInAssortment]),
                ColourGroupValue = (String)GetValue(dr[FieldNames.PlanogramProductColourGroupValue]),
                MetaCDTNode = (String)GetValue(dr[FieldNames.PlanogramProductMetaCDTNode]),
                MetaIsProductRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsProductRuleBroken]),
                MetaIsFamilyRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsFamilyRuleBroken]),
                MetaIsInheritanceRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsInheritanceRuleBroken]),
                MetaIsLocalProductRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsLocalProductRuleBroken]),
                MetaIsDistributionRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsDistributionRuleBroken]),
                MetaIsCoreRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsCoreRuleBroken]),
                MetaIsDelistProductRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsDelistProductRuleBroken]),
                MetaIsForceProductRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsForceProductRuleBroken]),
                MetaIsPreserveProductRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsPreserveProductRuleBroken]),
                MetaIsMinimumHurdleProductRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsMinimumHurdleProductRuleBroken]),
                MetaIsMaximumProductFamilyRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsMaximumProductFamilyRuleBroken]),
                MetaIsMinimumProductFamilyRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsMinimumProductFamilyRuleBroken]),
                MetaIsDependencyFamilyRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsDependencyFamilyRuleBroken]),
                MetaIsDelistFamilyRuleBroken = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsDelistFamilyRuleBroken]),
                MetaComparisonStatus = (Byte)GetValue(dr[FieldNames.PlanogramProductMetaComparisonStatus]),
                MetaIsBuddied = (Boolean?)GetValue(dr[FieldNames.PlanogramProductMetaIsBuddied])
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramProductDto> FetchByPlanogramId(Object planogramId)
        {
            List<PlanogramProductDto> dtoList = new List<PlanogramProductDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProductFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramProductPlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProductInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramProductId, SqlDbType.Int);

                    //Other properties 
                    CreateOtherParameters(dto, command);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramProductDto> dtos)
        {
            // build a dictionary of dtos
            // indexed by their id as we will
            // need this information after the insert
            Dictionary<Object, PlanogramProductDto> index = new Dictionary<Object, PlanogramProductDto>();
            foreach (PlanogramProductDto dto in dtos)
            {
                dto.Id = IdentityHelper.GetNextInt32(); // allocate a new id to ensure its an Int32
                index.Add(dto.Id, dto);
            }

            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramProductBulkCopyDataReader dr = new PlanogramProductBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProductBulkInsert))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Object oldId = (Int32)GetValue(dr[0]);
                            Object newId = (Int32)GetValue(dr[1]);
                            index[oldId].Id = newId;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramProductDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProductUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramProductId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateOtherParameters(dto, command);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Update the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramProductDto> dtos)
        {
            try
            {
                // create the import table name
                CreateTempImportTable(_importTableName);

                // perform a batch insert to temp table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
                {
                    bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                    bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                    PlanogramProductBulkCopyDataReader dr = new PlanogramProductBulkCopyDataReader(dtos);
                    bulkCopy.WriteToServer(dr);
                }

                // perform a batch insert
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProductBulkUpdate))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            finally
            {
                DropImportTable(_importTableName);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramProductDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramProductId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramProductDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramProductDto> dtos)
        {
            foreach (PlanogramProductDto dto in dtos)
            {
                this.Delete(dto);
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates other parameters which are not the id or special types.
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="command"></param>
        private void CreateOtherParameters(PlanogramProductDto dto, DalCommand command)
        {
            CreateParameter(command, FieldNames.PlanogramProductPlanogramId, SqlDbType.Int, dto.PlanogramId);
            CreateParameter(command, FieldNames.PlanogramProductGtin, SqlDbType.NVarChar, dto.Gtin);
            CreateParameter(command, FieldNames.PlanogramProductName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramProductBrand, SqlDbType.NVarChar, dto.Brand);
            CreateParameter(command, FieldNames.PlanogramProductHeight, SqlDbType.Real, dto.Height);
            CreateParameter(command, FieldNames.PlanogramProductWidth, SqlDbType.Real, dto.Width);
            CreateParameter(command, FieldNames.PlanogramProductDepth, SqlDbType.Real, dto.Depth);
            CreateParameter(command, FieldNames.PlanogramProductDisplayHeight, SqlDbType.Real, dto.DisplayHeight);
            CreateParameter(command, FieldNames.PlanogramProductDisplayWidth, SqlDbType.Real, dto.DisplayWidth);
            CreateParameter(command, FieldNames.PlanogramProductDisplayDepth, SqlDbType.Real, dto.DisplayDepth);
            CreateParameter(command, FieldNames.PlanogramProductAlternateHeight, SqlDbType.Real, dto.AlternateHeight);
            CreateParameter(command, FieldNames.PlanogramProductAlternateWidth, SqlDbType.Real, dto.AlternateWidth);
            CreateParameter(command, FieldNames.PlanogramProductAlternateDepth, SqlDbType.Real, dto.AlternateDepth);
            CreateParameter(command, FieldNames.PlanogramProductPointOfPurchaseHeight, SqlDbType.Real, dto.PointOfPurchaseHeight);
            CreateParameter(command, FieldNames.PlanogramProductPointOfPurchaseWidth, SqlDbType.Real, dto.PointOfPurchaseWidth);
            CreateParameter(command, FieldNames.PlanogramProductPointOfPurchaseDepth, SqlDbType.Real, dto.PointOfPurchaseDepth);
            CreateParameter(command, FieldNames.PlanogramProductNumberOfPegHoles, SqlDbType.TinyInt, dto.NumberOfPegHoles);
            CreateParameter(command, FieldNames.PlanogramProductPegX, SqlDbType.Real, dto.PegX);
            CreateParameter(command, FieldNames.PlanogramProductPegX2, SqlDbType.Real, dto.PegX2);
            CreateParameter(command, FieldNames.PlanogramProductPegX3, SqlDbType.Real, dto.PegX3);
            CreateParameter(command, FieldNames.PlanogramProductPegY, SqlDbType.Real, dto.PegY);
            CreateParameter(command, FieldNames.PlanogramProductPegY2, SqlDbType.Real, dto.PegY2);
            CreateParameter(command, FieldNames.PlanogramProductPegY3, SqlDbType.Real, dto.PegY3);
            CreateParameter(command, FieldNames.PlanogramProductPegProngOffsetX, SqlDbType.Real, dto.PegProngOffsetX);
            CreateParameter(command, FieldNames.PlanogramProductPegProngOffsetY, SqlDbType.Real, dto.PegProngOffsetY);
            CreateParameter(command, FieldNames.PlanogramProductPegDepth, SqlDbType.Real, dto.PegDepth);
            CreateParameter(command, FieldNames.PlanogramProductSqueezeHeight, SqlDbType.Real, dto.SqueezeHeight);
            CreateParameter(command, FieldNames.PlanogramProductSqueezeWidth, SqlDbType.Real, dto.SqueezeWidth);
            CreateParameter(command, FieldNames.PlanogramProductSqueezeDepth, SqlDbType.Real, dto.SqueezeDepth);
            CreateParameter(command, FieldNames.PlanogramProductNestingHeight, SqlDbType.Real, dto.NestingHeight);
            CreateParameter(command, FieldNames.PlanogramProductNestingWidth, SqlDbType.Real, dto.NestingWidth);
            CreateParameter(command, FieldNames.PlanogramProductNestingDepth, SqlDbType.Real, dto.NestingDepth);
            CreateParameter(command, FieldNames.PlanogramProductCasePackUnits, SqlDbType.SmallInt, dto.CasePackUnits);
            CreateParameter(command, FieldNames.PlanogramProductCaseHigh, SqlDbType.TinyInt, dto.CaseHigh);
            CreateParameter(command, FieldNames.PlanogramProductCaseWide, SqlDbType.TinyInt, dto.CaseWide);
            CreateParameter(command, FieldNames.PlanogramProductCaseDeep, SqlDbType.TinyInt, dto.CaseDeep);
            CreateParameter(command, FieldNames.PlanogramProductCaseHeight, SqlDbType.Real, dto.CaseHeight);
            CreateParameter(command, FieldNames.PlanogramProductCaseWidth, SqlDbType.Real, dto.CaseWidth);
            CreateParameter(command, FieldNames.PlanogramProductCaseDepth, SqlDbType.Real, dto.CaseDepth);
            CreateParameter(command, FieldNames.PlanogramProductMaxStack, SqlDbType.TinyInt, dto.MaxStack);
            CreateParameter(command, FieldNames.PlanogramProductMaxTopCap, SqlDbType.TinyInt, dto.MaxTopCap);
            CreateParameter(command, FieldNames.PlanogramProductMaxRightCap, SqlDbType.TinyInt, dto.MaxRightCap);
            CreateParameter(command, FieldNames.PlanogramProductMinDeep, SqlDbType.TinyInt, dto.MinDeep);
            CreateParameter(command, FieldNames.PlanogramProductMaxDeep, SqlDbType.TinyInt, dto.MaxDeep);
            CreateParameter(command, FieldNames.PlanogramProductTrayPackUnits, SqlDbType.SmallInt, dto.TrayPackUnits);
            CreateParameter(command, FieldNames.PlanogramProductTrayHigh, SqlDbType.TinyInt, dto.TrayHigh);
            CreateParameter(command, FieldNames.PlanogramProductTrayWide, SqlDbType.TinyInt, dto.TrayWide);
            CreateParameter(command, FieldNames.PlanogramProductTrayDeep, SqlDbType.TinyInt, dto.TrayDeep);
            CreateParameter(command, FieldNames.PlanogramProductTrayHeight, SqlDbType.Real, dto.TrayHeight);
            CreateParameter(command, FieldNames.PlanogramProductTrayWidth, SqlDbType.Real, dto.TrayWidth);
            CreateParameter(command, FieldNames.PlanogramProductTrayDepth, SqlDbType.Real, dto.TrayDepth);
            CreateParameter(command, FieldNames.PlanogramProductTrayThickHeight, SqlDbType.Real, dto.TrayThickHeight);
            CreateParameter(command, FieldNames.PlanogramProductTrayThickWidth, SqlDbType.Real, dto.TrayThickWidth);
            CreateParameter(command, FieldNames.PlanogramProductTrayThickDepth, SqlDbType.Real, dto.TrayThickDepth);
            CreateParameter(command, FieldNames.PlanogramProductFrontOverhang, SqlDbType.Real, dto.FrontOverhang);
            CreateParameter(command, FieldNames.PlanogramProductFingerSpaceAbove, SqlDbType.Real, dto.FingerSpaceAbove);
            CreateParameter(command, FieldNames.PlanogramProductFingerSpaceToTheSide, SqlDbType.Real, dto.FingerSpaceToTheSide);
            CreateParameter(command, FieldNames.PlanogramProductStatusType, SqlDbType.TinyInt, dto.StatusType);
            CreateParameter(command, FieldNames.PlanogramProductOrientationType, SqlDbType.TinyInt, dto.OrientationType);
            CreateParameter(command, FieldNames.PlanogramProductMerchandisingStyle, SqlDbType.TinyInt, dto.MerchandisingStyle);
            CreateParameter(command, FieldNames.PlanogramProductIsFrontOnly, SqlDbType.Bit, dto.IsFrontOnly);
            CreateParameter(command, FieldNames.PlanogramProductIsPlaceHolderProduct, SqlDbType.Bit, dto.IsPlaceHolderProduct);
            CreateParameter(command, FieldNames.PlanogramProductIsActive, SqlDbType.Bit, dto.IsActive);
            CreateParameter(command, FieldNames.PlanogramProductCanBreakTrayUp, SqlDbType.Bit, dto.CanBreakTrayUp);
            CreateParameter(command, FieldNames.PlanogramProductCanBreakTrayDown, SqlDbType.Bit, dto.CanBreakTrayDown);
            CreateParameter(command, FieldNames.PlanogramProductCanBreakTrayBack, SqlDbType.Bit, dto.CanBreakTrayBack);
            CreateParameter(command, FieldNames.PlanogramProductCanBreakTrayTop, SqlDbType.Bit, dto.CanBreakTrayTop);
            CreateParameter(command, FieldNames.PlanogramProductForceMiddleCap, SqlDbType.Bit, dto.ForceMiddleCap);
            CreateParameter(command, FieldNames.PlanogramProductForceBottomCap, SqlDbType.Bit, dto.ForceBottomCap);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdFront, SqlDbType.Int, dto.PlanogramImageIdFront);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdBack, SqlDbType.Int, dto.PlanogramImageIdBack);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdTop, SqlDbType.Int, dto.PlanogramImageIdTop);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdBottom, SqlDbType.Int, dto.PlanogramImageIdBottom);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdLeft, SqlDbType.Int, dto.PlanogramImageIdLeft);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdRight, SqlDbType.Int, dto.PlanogramImageIdRight);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdDisplayFront, SqlDbType.Int, dto.PlanogramImageIdDisplayFront);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdDisplayBack, SqlDbType.Int, dto.PlanogramImageIdDisplayBack);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdDisplayTop, SqlDbType.Int, dto.PlanogramImageIdDisplayTop);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdDisplayBottom, SqlDbType.Int, dto.PlanogramImageIdDisplayBottom);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdDisplayLeft, SqlDbType.Int, dto.PlanogramImageIdDisplayLeft);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdDisplayRight, SqlDbType.Int, dto.PlanogramImageIdDisplayRight);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdTrayFront, SqlDbType.Int, dto.PlanogramImageIdTrayFront);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdTrayBack, SqlDbType.Int, dto.PlanogramImageIdTrayBack);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdTrayTop, SqlDbType.Int, dto.PlanogramImageIdTrayTop);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdTrayBottom, SqlDbType.Int, dto.PlanogramImageIdTrayBottom);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdTrayLeft, SqlDbType.Int, dto.PlanogramImageIdTrayLeft);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdTrayRight, SqlDbType.Int, dto.PlanogramImageIdTrayRight);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseFront, SqlDbType.Int, dto.PlanogramImageIdPointOfPurchaseFront);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBack, SqlDbType.Int, dto.PlanogramImageIdPointOfPurchaseBack);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseTop, SqlDbType.Int, dto.PlanogramImageIdPointOfPurchaseTop);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseBottom, SqlDbType.Int, dto.PlanogramImageIdPointOfPurchaseBottom);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseLeft, SqlDbType.Int, dto.PlanogramImageIdPointOfPurchaseLeft);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdPointOfPurchaseRight, SqlDbType.Int, dto.PlanogramImageIdPointOfPurchaseRight);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdAlternateFront, SqlDbType.Int, dto.PlanogramImageIdAlternateFront);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdAlternateBack, SqlDbType.Int, dto.PlanogramImageIdAlternateBack);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdAlternateTop, SqlDbType.Int, dto.PlanogramImageIdAlternateTop);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdAlternateBottom, SqlDbType.Int, dto.PlanogramImageIdAlternateBottom);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdAlternateLeft, SqlDbType.Int, dto.PlanogramImageIdAlternateLeft);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdAlternateRight, SqlDbType.Int, dto.PlanogramImageIdAlternateRight);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdCaseFront, SqlDbType.Int, dto.PlanogramImageIdCaseFront);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdCaseBack, SqlDbType.Int, dto.PlanogramImageIdCaseBack);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdCaseTop, SqlDbType.Int, dto.PlanogramImageIdCaseTop);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdCaseBottom, SqlDbType.Int, dto.PlanogramImageIdCaseBottom);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdCaseLeft, SqlDbType.Int, dto.PlanogramImageIdCaseLeft);
            CreateParameter(command, FieldNames.PlanogramProductPlanogramImageIdCaseRight, SqlDbType.Int, dto.PlanogramImageIdCaseRight);
            CreateParameter(command, FieldNames.PlanogramProductShapeType, SqlDbType.TinyInt, dto.ShapeType);
            CreateParameter(command, FieldNames.PlanogramProductFillPatternType, SqlDbType.TinyInt, dto.FillPatternType);
            CreateParameter(command, FieldNames.PlanogramProductFillColour, SqlDbType.Int, dto.FillColour);
            CreateParameter(command, FieldNames.PlanogramProductShape, SqlDbType.NVarChar, dto.Shape);
            CreateParameter(command, FieldNames.PlanogramProductPointOfPurchaseDescription, SqlDbType.NVarChar, dto.PointOfPurchaseDescription);
            CreateParameter(command, FieldNames.PlanogramProductShortDescription, SqlDbType.NVarChar, dto.ShortDescription);
            CreateParameter(command, FieldNames.PlanogramProductSubcategory, SqlDbType.NVarChar, dto.Subcategory);
            CreateParameter(command, FieldNames.PlanogramProductCustomerStatus, SqlDbType.NVarChar, dto.CustomerStatus);
            CreateParameter(command, FieldNames.PlanogramProductColour, SqlDbType.NVarChar, dto.Colour);
            CreateParameter(command, FieldNames.PlanogramProductFlavour, SqlDbType.NVarChar, dto.Flavour);
            CreateParameter(command, FieldNames.PlanogramProductPackagingShape, SqlDbType.NVarChar, dto.PackagingShape);
            CreateParameter(command, FieldNames.PlanogramProductPackagingType, SqlDbType.NVarChar, dto.PackagingType);
            CreateParameter(command, FieldNames.PlanogramProductCountryOfOrigin, SqlDbType.NVarChar, dto.CountryOfOrigin);
            CreateParameter(command, FieldNames.PlanogramProductCountryOfProcessing, SqlDbType.NVarChar, dto.CountryOfProcessing);
            CreateParameter(command, FieldNames.PlanogramProductShelfLife, SqlDbType.SmallInt, dto.ShelfLife);
            CreateParameter(command, FieldNames.PlanogramProductDeliveryFrequencyDays, SqlDbType.Real, dto.DeliveryFrequencyDays);
            CreateParameter(command, FieldNames.PlanogramProductDeliveryMethod, SqlDbType.NVarChar, dto.DeliveryMethod);
            CreateParameter(command, FieldNames.PlanogramProductVendorCode, SqlDbType.NVarChar, dto.VendorCode);
            CreateParameter(command, FieldNames.PlanogramProductVendor, SqlDbType.NVarChar, dto.Vendor);
            CreateParameter(command, FieldNames.PlanogramProductManufacturerCode, SqlDbType.NVarChar, dto.ManufacturerCode);
            CreateParameter(command, FieldNames.PlanogramProductManufacturer, SqlDbType.NVarChar, dto.Manufacturer);
            CreateParameter(command, FieldNames.PlanogramProductSize, SqlDbType.NVarChar, dto.Size);
            CreateParameter(command, FieldNames.PlanogramProductUnitOfMeasure, SqlDbType.NVarChar, dto.UnitOfMeasure);
            CreateParameter(command, FieldNames.PlanogramProductDateIntroduced, SqlDbType.DateTime, dto.DateIntroduced);
            CreateParameter(command, FieldNames.PlanogramProductDateDiscontinued, SqlDbType.DateTime, dto.DateDiscontinued);
            CreateParameter(command, FieldNames.PlanogramProductDateEffective, SqlDbType.DateTime, dto.DateEffective);
            CreateParameter(command, FieldNames.PlanogramProductHealth, SqlDbType.NVarChar, dto.Health);
            CreateParameter(command, FieldNames.PlanogramProductCorporateCode, SqlDbType.NVarChar, dto.CorporateCode);
            CreateParameter(command, FieldNames.PlanogramProductBarcode, SqlDbType.NVarChar, dto.Barcode);
            CreateParameter(command, FieldNames.PlanogramProductSellPrice, SqlDbType.Real, dto.SellPrice);
            CreateParameter(command, FieldNames.PlanogramProductSellPackCount, SqlDbType.SmallInt, dto.SellPackCount);
            CreateParameter(command, FieldNames.PlanogramProductSellPackDescription, SqlDbType.NVarChar, dto.SellPackDescription);
            CreateParameter(command, FieldNames.PlanogramProductRecommendedRetailPrice, SqlDbType.Real, dto.RecommendedRetailPrice);
            CreateParameter(command, FieldNames.PlanogramProductManufacturerRecommendedRetailPrice, SqlDbType.Real, dto.ManufacturerRecommendedRetailPrice);
            CreateParameter(command, FieldNames.PlanogramProductCostPrice, SqlDbType.Real, dto.CostPrice);
            CreateParameter(command, FieldNames.PlanogramProductCaseCost, SqlDbType.Real, dto.CaseCost);
            CreateParameter(command, FieldNames.PlanogramProductTaxRate, SqlDbType.Real, dto.TaxRate);
            CreateParameter(command, FieldNames.PlanogramProductConsumerInformation, SqlDbType.NVarChar, dto.ConsumerInformation);
            CreateParameter(command, FieldNames.PlanogramProductTexture, SqlDbType.NVarChar, dto.Texture);
            CreateParameter(command, FieldNames.PlanogramProductStyleNumber, SqlDbType.SmallInt, dto.StyleNumber);
            CreateParameter(command, FieldNames.PlanogramProductPattern, SqlDbType.NVarChar, dto.Pattern);
            CreateParameter(command, FieldNames.PlanogramProductModel, SqlDbType.NVarChar, dto.Model);
            CreateParameter(command, FieldNames.PlanogramProductGarmentType, SqlDbType.NVarChar, dto.GarmentType);
            CreateParameter(command, FieldNames.PlanogramProductIsPrivateLabel, SqlDbType.Bit, dto.IsPrivateLabel);
            CreateParameter(command, FieldNames.PlanogramProductIsNewProduct, SqlDbType.Bit, dto.IsNewProduct);
            CreateParameter(command, FieldNames.PlanogramProductFinancialGroupCode, SqlDbType.NVarChar, dto.FinancialGroupCode);
            CreateParameter(command, FieldNames.PlanogramProductFinancialGroupName, SqlDbType.NVarChar, dto.FinancialGroupName);
            CreateParameter(command, FieldNames.PlanogramProductMetaNotAchievedInventory, SqlDbType.Bit, dto.MetaNotAchievedInventory);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalUnits, SqlDbType.Int, dto.MetaTotalUnits);
            CreateParameter(command, FieldNames.PlanogramProductMetaPlanogramLinearSpacePercentage, SqlDbType.Real, dto.MetaPlanogramLinearSpacePercentage);
            CreateParameter(command, FieldNames.PlanogramProductMetaPlanogramAreaSpacePercentage, SqlDbType.Real, dto.MetaPlanogramAreaSpacePercentage);
            CreateParameter(command, FieldNames.PlanogramProductMetaPlanogramVolumetricSpacePercentage, SqlDbType.Real, dto.MetaPlanogramVolumetricSpacePercentage);
            CreateParameter(command, FieldNames.PlanogramProductMetaPositionCount, SqlDbType.Int, dto.MetaPositionCount);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalFacings, SqlDbType.Int, dto.MetaTotalFacings);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalLinearSpace, SqlDbType.Real, dto.MetaTotalLinearSpace);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalAreaSpace, SqlDbType.Real, dto.MetaTotalAreaSpace);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalVolumetricSpace, SqlDbType.Real, dto.MetaTotalVolumetricSpace);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsInMasterData, SqlDbType.Bit, dto.MetaIsInMasterData);
            CreateParameter(command, FieldNames.PlanogramProductMetaNotAchievedCases, SqlDbType.Bit, dto.MetaNotAchievedCases);
            CreateParameter(command, FieldNames.PlanogramProductMetaNotAchievedDOS, SqlDbType.Bit, dto.MetaNotAchievedDOS);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsOverShelfLifePercent, SqlDbType.Bit, dto.MetaIsOverShelfLifePercent);
            CreateParameter(command, FieldNames.PlanogramProductMetaNotAchievedDeliveries, SqlDbType.Bit, dto.MetaNotAchievedDeliveries);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalMainFacings, SqlDbType.Int, dto.MetaTotalMainFacings);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalXFacings, SqlDbType.Int, dto.MetaTotalXFacings);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalYFacings, SqlDbType.Int, dto.MetaTotalYFacings);
            CreateParameter(command, FieldNames.PlanogramProductMetaTotalZFacings, SqlDbType.Int, dto.MetaTotalZFacings);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsRangedInAssortment, SqlDbType.Bit, dto.MetaIsRangedInAssortment);
            CreateParameter(command, FieldNames.PlanogramProductColourGroupValue, SqlDbType.NVarChar, dto.ColourGroupValue);
            CreateParameter(command, FieldNames.PlanogramProductMetaCDTNode, SqlDbType.NVarChar, dto.MetaCDTNode);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsProductRuleBroken, SqlDbType.Bit, dto.MetaIsProductRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsFamilyRuleBroken, SqlDbType.Bit, dto.MetaIsFamilyRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsInheritanceRuleBroken, SqlDbType.Bit, dto.MetaIsInheritanceRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsLocalProductRuleBroken, SqlDbType.Bit, dto.MetaIsLocalProductRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsDistributionRuleBroken, SqlDbType.Bit, dto.MetaIsDistributionRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsCoreRuleBroken, SqlDbType.Bit, dto.MetaIsCoreRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsDelistProductRuleBroken, SqlDbType.Bit, dto.MetaIsDelistProductRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsForceProductRuleBroken, SqlDbType.Bit, dto.MetaIsForceProductRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsPreserveProductRuleBroken, SqlDbType.Bit, dto.MetaIsPreserveProductRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsMinimumHurdleProductRuleBroken, SqlDbType.Bit, dto.MetaIsMinimumHurdleProductRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsMaximumProductFamilyRuleBroken, SqlDbType.Bit, dto.MetaIsMaximumProductFamilyRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsMinimumProductFamilyRuleBroken, SqlDbType.Bit, dto.MetaIsMinimumProductFamilyRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsDependencyFamilyRuleBroken, SqlDbType.Bit, dto.MetaIsDependencyFamilyRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsDelistFamilyRuleBroken, SqlDbType.Bit, dto.MetaIsDelistFamilyRuleBroken);
            CreateParameter(command, FieldNames.PlanogramProductMetaComparisonStatus, SqlDbType.TinyInt, dto.MetaComparisonStatus);
            CreateParameter(command, FieldNames.PlanogramProductMetaIsBuddied, SqlDbType.Bit, dto.MetaIsBuddied);
        }

        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        /// <param name="importTableName">The temporary import table name</param>
        private void CreateTempImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _createImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Drops the specified table
        /// </summary>
        /// <param name="importTableName">The import table to drop</param>
        private void DropImportTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropImportTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
