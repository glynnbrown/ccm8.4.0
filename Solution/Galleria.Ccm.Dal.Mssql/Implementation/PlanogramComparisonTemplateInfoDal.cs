﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     MSSQL implementation of <see cref="PlanogramComparisonTemplateInfoDal"/>.
    /// </summary>
    public sealed class PlanogramComparisonTemplateInfoDal : DalBase, IPlanogramComparisonTemplateInfoDal
    {
        #region Constants

        private const String ImportTableName = "#tmpPlanogramComparisonTemplate";
        private const String CreateIdTableSql = "CREATE TABLE [{0}] (PlanogramComparisonTemplate_Id INT)";
        private const String DropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        #endregion

        #region Nested Classes

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        private class BulkCopyIntDataReader : IDataReader
        {
            #region Fields

            readonly IEnumerator<Object> _enumerator; // an enumerator containing the values to write to the database

            #endregion

            #region Constructors

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyIntDataReader(IEnumerable<Object> values)
            {
                _enumerator = values.GetEnumerator();
            }

            #endregion

            #region Properties

            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }

            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public Object GetValue(Int32 i)
            {
                return _enumerator.Current ?? DBNull.Value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public Int32 Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public Boolean IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public Boolean NextResult()
            {
                throw new NotImplementedException();
            }

            public Int32 RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public Boolean GetBoolean(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Byte GetByte(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetBytes(Int32 i, Int64 fieldOffset, Byte[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public Char GetChar(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetChars(Int32 i, Int64 fieldoffset, Char[] buffer, Int32 bufferoffset, Int32 length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetDataTypeName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Decimal GetDecimal(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Double GetDouble(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Single GetFloat(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int16 GetInt16(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetInt32(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int64 GetInt64(Int32 i)
            {
                throw new NotImplementedException();
            }

            public String GetName(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetOrdinal(String name)
            {
                throw new NotImplementedException();
            }

            public String GetString(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Int32 GetValues(Object[] values)
            {
                throw new NotImplementedException();
            }

            public Boolean IsDBNull(Int32 i)
            {
                throw new NotImplementedException();
            }

            public Object this[String name]
            {
                get { throw new NotImplementedException(); }
            }

            public Object this[Int32 i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Initialize a new <see cref="PlanogramComparisonTemplateInfoDto"/> from the given <see cref="IDataRecord"/>.
        /// </summary>
        /// <param name="source">The <see cref="IDataRecord"/> that contains the data to be used in the new item.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateInfoDto"/> with the data contained in the given <paramref name="source"/></returns>
        private static PlanogramComparisonTemplateInfoDto GetDataTransferObject(IDataRecord source)
        {
            return new PlanogramComparisonTemplateInfoDto
            {
                Id = GetValue(source[FieldNames.PlanogramComparisonTemplateId]),
                Name = (String)GetValue(source[FieldNames.PlanogramComparisonTemplateName]),
                Description = (String)GetValue(source[FieldNames.PlanogramComparisonTemplateDescription])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="ids" />.
        /// </summary>
        /// <param name="ids"><see cref="Object" /> containing the ids of matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="ids"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            var dtos = new List<PlanogramComparisonTemplateInfoDto>();

            //  Create a temporary table for the ids parameter.
            ExecuteNonQuery(CreateIdTableSql, ImportTableName);

            //  Perform a batch insert to the temp table.
            using (var bulkCopy = new SqlBulkCopy(DalContext.Connection, SqlBulkCopyOptions.Default, DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = CommandTimeout;
                bulkCopy.DestinationTableName = String.Format("[{0}]", ImportTableName);
                bulkCopy.WriteToServer(new BulkCopyIntDataReader(ids));
            }

            //  Execute the command.
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateInfoFetchByIds))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }

            //  Drop the temporary table for the ids parameter.
            ExecuteNonQuery(DropTableSql, ImportTableName);

            //  Return the list of items.
            return dtos;
        }

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            var dtos = new List<PlanogramComparisonTemplateInfoDto>();

            //  Execute the command.
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }

            //  Return the list of items.
            return dtos;
        }

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />, including the deleted ones.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            var dtos = new List<PlanogramComparisonTemplateInfoDto>();

            //  Execute the command.
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramComparisonTemplateInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.PlanogramComparisonTemplateEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }

            //  Return the list of items.
            return dtos;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Execute a Non Query command.
        /// </summary>
        /// <param name="nonQuerySql"></param>
        /// <param name="args">Optional args to be formated into the <paramref name="nonQuerySql"/>.</param>
        private void ExecuteNonQuery(String nonQuerySql, params Object[] args)
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, nonQuerySql, args), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}