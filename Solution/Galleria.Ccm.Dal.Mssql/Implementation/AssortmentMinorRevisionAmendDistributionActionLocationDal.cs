﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// AssortmentMinorRevisionAmendDistribtionActionLocation Dal
    /// </summary>
    public class AssortmentMinorRevisionAmendDistributionActionLocationDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionAmendDistributionActionLocationDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static AssortmentMinorRevisionAmendDistributionActionLocationDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionAmendDistributionActionLocationDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationId]),
                AssortmentMinorRevisionAmendDistributionActionId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationAssortmentMinorRevisionAmendDistributionActionId]),
                LocationCode = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationCode]),
                LocationName = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationName]),
                LocationId = (Int16?)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationId]),
                Units = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationUnits]),
                Facings = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationFacings])

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public AssortmentMinorRevisionAmendDistributionActionLocationDto FetchById(Int32 id)
        {
            AssortmentMinorRevisionAmendDistributionActionLocationDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionLocationFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the dtos that match the specified Assortment minor revision Amend Distribution action id
        /// </summary>
        /// <param name="entityId">specified AssortmentMinorRevisionAmendDistributionAction id</param>
        /// <returns>matching dtos</returns>
        public IEnumerable<AssortmentMinorRevisionAmendDistributionActionLocationDto> FetchByAssortmentMinorRevisionAmendDistributionActionId(Int32 AssortmentMinorRevisionAmendDistributionActionId)
        {
            List<AssortmentMinorRevisionAmendDistributionActionLocationDto> dtoList = new List<AssortmentMinorRevisionAmendDistributionActionLocationDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionLocationFetchByAssortmentMinorRevisionAmendDistributionActionId))
                {
                    //Assortment Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationAssortmentMinorRevisionAmendDistributionActionId, SqlDbType.Int, AssortmentMinorRevisionAmendDistributionActionId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(AssortmentMinorRevisionAmendDistributionActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionLocationInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationAssortmentMinorRevisionAmendDistributionActionId, SqlDbType.Int, dto.AssortmentMinorRevisionAmendDistributionActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationUnits, SqlDbType.Int, dto.Units);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationFacings, SqlDbType.Int, dto.Facings);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(AssortmentMinorRevisionAmendDistributionActionLocationDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionLocationUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationAssortmentMinorRevisionAmendDistributionActionId, SqlDbType.Int, dto.AssortmentMinorRevisionAmendDistributionActionId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationCode, SqlDbType.NVarChar, dto.LocationCode);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationName, SqlDbType.NVarChar, dto.LocationName);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationLocationId, SqlDbType.SmallInt, dto.LocationId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationUnits, SqlDbType.Int, dto.Units);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationFacings, SqlDbType.Int, dto.Facings);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Upsert

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Int32 id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionAmendDistributionActionLocationDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionAmendDistributionActionLocationId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion
    }
}