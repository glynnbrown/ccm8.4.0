﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25664 : A. Kuszyk
//      Created.    
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class UserPlanogramGroupDal : Galleria.Framework.Dal.Mssql.DalBase, IUserPlanogramGroupDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static UserPlanogramGroupDto GetDataTransferObject(SqlDataReader dr)
        {
            return new UserPlanogramGroupDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.UserPlanogramGroupId]),
                UserId = (Int32)GetValue(dr[FieldNames.UserPlanogramGroupUserId]),
                PlanogramGroupId = (Int32)GetValue(dr[FieldNames.UserPlanogramGroupPlanogramGroupId])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="userPlanogramGroupId">specified id</param>
        /// <returns>matching dto</returns>
        public UserPlanogramGroupDto FetchById(int userPlanogramGroupId)
        {
            UserPlanogramGroupDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserPlanogramGroupFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.UserPlanogramGroupId, SqlDbType.Int, userPlanogramGroupId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns a dto that matches the specified User Id
        /// </summary>
        /// <param name="userId">The Id of the user whose Planogram Groups should be fetched.</param>
        /// <returns>matching dto</returns>
        public IEnumerable<UserPlanogramGroupDto> FetchByUserId(int userId)
        {
            List<UserPlanogramGroupDto> dtoList = new List<UserPlanogramGroupDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserPlanogramGroupFetchByUserId))
                {
                    CreateParameter(command, FieldNames.UserPlanogramGroupUserId, SqlDbType.Int, userId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(UserPlanogramGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserPlanogramGroupInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.UserPlanogramGroupId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.UserPlanogramGroupUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.UserPlanogramGroupPlanogramGroupId, SqlDbType.Int, dto.PlanogramGroupId);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        } 
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(UserPlanogramGroupDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserPlanogramGroupUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.UserPlanogramGroupId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.UserPlanogramGroupUserId, SqlDbType.Int, dto.UserId);
                    CreateParameter(command, FieldNames.UserPlanogramGroupPlanogramGroupId, SqlDbType.Int, dto.PlanogramGroupId);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        } 
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(int id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.UserPlanogramGroupDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.UserPlanogramGroupId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        } 
        #endregion
    }
}
