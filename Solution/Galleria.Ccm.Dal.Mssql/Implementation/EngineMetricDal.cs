﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-27499 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Engine.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class EngineMetricDal : Galleria.Framework.Dal.Mssql.DalBase, IEngineMetricDal
    {
        #region Insert
        /// <summary>
        /// Inserts a metric into the database
        /// </summary>
        public void Insert(EngineMetricDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMetricInsert))
                {
                    // parameters
                    CreateParameter(command, FieldNames.EngineMetricType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.EngineMetricName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.EngineMetricTimestamp, SqlDbType.DateTime, dto.Timestamp);
                    CreateParameter(command, FieldNames.EngineMetricDuration, SqlDbType.BigInt, dto.Duration);
                    CreateParameter(command, FieldNames.EngineMetricCount, SqlDbType.Int, dto.Count);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes old metric information based on
        /// the timestamp value
        /// </summary>
        public Boolean DeleteByTimestamp(DateTime timestamp)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.EngineMetricDeleteByTimestamp))
                {
                    // parameters
                    CreateParameter(command, FieldNames.EngineMetricTimestamp, SqlDbType.DateTime, timestamp);

                    //execute
                    Int32 rowsAffected = command.ExecuteNonQuery();

                    // indicate if rows were deleted
                    return rowsAffected > 0;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return false;
        }
        #endregion
    }
}
