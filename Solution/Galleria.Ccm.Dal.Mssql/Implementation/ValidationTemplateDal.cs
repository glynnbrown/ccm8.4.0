﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.Silva ~ Added LockById and UnlockById. Used in the User Dal when accessing files.
// V8-27269 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed date deleted
#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     ValidationTemplate Dal
    /// </summary>
    public class ValidationTemplateDal : DalBase, IValidationTemplateDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static ValidationTemplateDto GetDataTransferObject(IDataRecord dr)
        {
            return new ValidationTemplateDto
            {
                Id = (Int32) GetValue(dr[FieldNames.ValidationTemplateId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.ValidationTemplateRowVersion])),
                EntityId = (Int32) GetValue(dr[FieldNames.ValidationTemplateEntityId]),
                Name = (String) GetValue(dr[FieldNames.ValidationTemplateName]),
                DateCreated = (DateTime) GetValue(dr[FieldNames.ValidationTemplateDateCreated]),
                DateLastModified = (DateTime) GetValue(dr[FieldNames.ValidationTemplateDateLastModified]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="ValidationTemplateDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="ValidationTemplateDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateDto" /> with the data for the provided <paramref name="id" />.</returns>
        public ValidationTemplateDto FetchById(Object id)
        {
            ValidationTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ValidationTemplateId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public ValidationTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            ValidationTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.ValidationTemplateEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.ValidationTemplateName, SqlDbType.NVarChar, name);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="ValidationTemplateDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(ValidationTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.ValidationTemplateId, SqlDbType.Int);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.ValidationTemplateRowVersion,
                        SqlDbType.Timestamp);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command,
                        FieldNames.ValidationTemplateDateCreated, SqlDbType.DateTime, ParameterDirection.Output,
                        dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command,
                        FieldNames.ValidationTemplateDateLastModified, SqlDbType.DateTime, ParameterDirection.Output,
                        dto.DateLastModified);


                    //Other properties 
                    CreateParameter(command, FieldNames.ValidationTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ValidationTemplateName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32) idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime) (dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime) (dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="ValidationTemplateDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(ValidationTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ValidationTemplateId, SqlDbType.Int, dto.Id);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.ValidationTemplateRowVersion,
                        SqlDbType.Timestamp, ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command,
                        FieldNames.ValidationTemplateDateCreated, SqlDbType.DateTime, ParameterDirection.Output,
                        dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command,
                        FieldNames.ValidationTemplateDateLastModified, SqlDbType.DateTime, ParameterDirection.Output,
                        dto.DateLastModified);


                    //Other properties 
                    CreateParameter(command, FieldNames.ValidationTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.ValidationTemplateName, SqlDbType.NVarChar, dto.Name);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime) (dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime) (dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.ValidationTemplateDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.ValidationTemplateId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a validation template for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <returns><c>True</c> if the validation template was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public bool LockById(Object id)
        {
            return true; // Not implemented as this dal does not use locking mechamisms.
        }

        /// <summary>
        ///     Unlocks a validation template after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }

        #endregion
    }
}