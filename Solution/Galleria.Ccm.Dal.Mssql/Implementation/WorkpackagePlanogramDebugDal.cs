﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class WorkpackagePlanogramDebugDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackagePlanogramDebugDal
    {
        #region Data Transfer Object
        /// <summary>
        /// Creates a dto from a data reader
        /// </summary>
        private WorkpackagePlanogramDebugDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackagePlanogramDebugDto()
            {
                SourcePlanogramId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramDebugSourcePlanogramId]),
                WorkflowTaskId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramDebugWorkflowTaskId]),
                DebugPlanogramId = (Int32)GetValue(dr[FieldNames.WorkpackagePlanogramDebugDebugPlanogramId])
            };
        }
        #endregion
        
        #region Fetch
        /// <summary>
        /// Returns all items for the specified planogram
        /// </summary>
        public IEnumerable<WorkpackagePlanogramDebugDto> FetchBySourcePlanogramId(Int32 sourcePlanogramId)
        {
            List<WorkpackagePlanogramDebugDto> dtoList = new List<WorkpackagePlanogramDebugDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramDebugFetchBySourcePlanogramId))
                {
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugSourcePlanogramId, SqlDbType.Int, sourcePlanogramId);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a new dto into the database
        /// </summary>
        public void Insert(WorkpackagePlanogramDebugDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramDebugInsert))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugSourcePlanogramId, SqlDbType.Int, dto.SourcePlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugWorkflowTaskId, SqlDbType.Int, dto.WorkflowTaskId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugDebugPlanogramId, SqlDbType.Int, dto.DebugPlanogramId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates an existing dto in the database
        /// </summary>
        public void Update(WorkpackagePlanogramDebugDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramDebugUpdateBySourcePlanogramIdWorkflowTaskId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugSourcePlanogramId, SqlDbType.Int, dto.SourcePlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugWorkflowTaskId, SqlDbType.Int, dto.WorkflowTaskId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugDebugPlanogramId, SqlDbType.Int, dto.DebugPlanogramId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified dto from the database
        /// </summary>
        public void DeleteBySourcePlanogramIdWorkflowTaskId(Int32 sourcePlanogramId, Int32 workflowTaskId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePlanogramDebugDeleteBySourcePlanogramIdWorkflowTaskId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugSourcePlanogramId, SqlDbType.Int, sourcePlanogramId);
                    CreateParameter(command, FieldNames.WorkpackagePlanogramDebugWorkflowTaskId, SqlDbType.Int, workflowTaskId);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion
    }
}
