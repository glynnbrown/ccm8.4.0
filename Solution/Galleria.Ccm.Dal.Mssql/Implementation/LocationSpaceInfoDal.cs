﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Location Space Info Dal
    /// </summary>
    public class LocationSpaceInfoDal : Galleria.Framework.Dal.Mssql.DalBase, ILocationSpaceInfoDal
    {
        #region Data Transfer Objects
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader</param>
        /// <returns>Returns a dto</returns>
        public LocationSpaceInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new LocationSpaceInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.LocationSpaceId]),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.LocationSpaceUniqueContentReference]),
                EntityId = (Int32)GetValue(dr[FieldNames.LocationSpaceEntityId]),
                LocationId = (Int16)GetValue(dr[FieldNames.LocationSpaceLocationId]),
                LocationCode = (String)GetValue(dr[FieldNames.LocationSpaceInfoLocationCode]),
                LocationName = (String)GetValue(dr[FieldNames.LocationSpaceInfoLocationName]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.LocationSpaceParentUniqueContentReference])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        /// <param name="id">The dto id</param>
        /// <returns>The specified dto</returns>
        public IEnumerable<LocationSpaceInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<LocationSpaceInfoDto> dtoList = new List<LocationSpaceInfoDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.LocationSpaceInfoFetchByEntityId))
                {

                    //Entity Id
                    CreateParameter(command,
                        FieldNames.LocationSpaceEntityId,
                        SqlDbType.Int,
                        entityId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion
    }
}
