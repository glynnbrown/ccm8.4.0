﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// V8-26520 : J.Pickup
//  Introduced Product Group Name
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class AssortmentMinorRevisionInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IAssortmentMinorRevisionInfoDal
    {
        #region DataTransferObject
        /// <summary>
        /// Returns a new dto from this data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private AssortmentMinorRevisionInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new AssortmentMinorRevisionInfoDto
            {
                Id = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionId]),
                UniqueContentReference = (Guid)GetValue(dr[FieldNames.AssortmentMinorRevisionUcr]),
                EntityId = (Int32)GetValue(dr[FieldNames.AssortmentMinorRevisionEntityId]),
                Name = (String)GetValue(dr[FieldNames.AssortmentMinorRevisionName]),
                ConsumerDecisionTreeId = (Int32?)GetValue(dr[FieldNames.AssortmentMinorRevisionConsumerDecisionTreeId]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.AssortmentMinorRevisionDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.AssortmentMinorRevisionDateLastModified]),
                ProductGroupId = (Int32)GetValue(dr[FieldNames.AssortmentProductGroupId]),
                ProductGroupName = (String)GetValue(dr[FieldNames.AssortmentProductGroupName]),
                ParentUniqueContentReference = (Guid?)GetValue(dr[FieldNames.AssortmentMinorRevisionParentUniqueContentReference])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public List<AssortmentMinorRevisionInfoDto> FetchByEntityId(int entityId)
        {
            List<AssortmentMinorRevisionInfoDto> dtoList = new List<AssortmentMinorRevisionInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionInfoFetchByEntityId))
                {
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }


        /// <summary>
        /// Returns all dtos for the given product group id
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <returns></returns>
        public List<AssortmentMinorRevisionInfoDto> FetchByProductGroupId(Int32 productGroupId)
        {
            List<AssortmentMinorRevisionInfoDto> dtoList = new List<AssortmentMinorRevisionInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionInfoFetchByProductGroupId))
                {
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionProductGroupId, SqlDbType.Int, productGroupId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }


        /// <summary>
        /// Returns all dtos for the given entity id and changed date
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public List<AssortmentMinorRevisionInfoDto> FetchByEntityIdChangeDate(int entityId, DateTime changeDate)
        {
            List<AssortmentMinorRevisionInfoDto> dtoList = new List<AssortmentMinorRevisionInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionInfoFetchByEntityIdChangeDate))
                {
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionInfoChangeDate, SqlDbType.SmallDateTime, changeDate);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }

                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }
        
        /// <summary>
        /// Returns the lastest dto for the given entityid and name
        /// </summary>
        /// <param name="entityId">The id of the entity </param>
        /// <param name="name">The name of the content item</param>
        /// <returns></returns>
        public AssortmentMinorRevisionInfoDto FetchByEntityIdName(Int32 entityId, String name)
        {
            AssortmentMinorRevisionInfoDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionInfoFetchByEntityIdName))
                {
                    // params
                    CreateParameter(command, FieldNames.EntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionName, SqlDbType.NVarChar, name);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        /// <summary>
        /// Returns the lastest dto for the given entityid and search criteira
        /// </summary>
        /// <param name="entityId">The id of the entity </param>
        /// <param name="name">The name of the content item</param>
        /// <returns></returns>
        public List<AssortmentMinorRevisionInfoDto> FetchByEntityIdAssortmentMinorRevisionSearchCriteria(Int32 entityId, String assortmentSearchCriteria)
        {
            List<AssortmentMinorRevisionInfoDto> dtoList = new List<AssortmentMinorRevisionInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionInfoFetchByEntityIdAssortmentMinorRevisionSearchCriteria))
                {
                    CreateParameter(command, FieldNames.AssortmentEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionName, SqlDbType.NVarChar, assortmentSearchCriteria);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

        /// <summary>
        /// Returns all dtos for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public List<AssortmentMinorRevisionInfoDto> FetchByEntityIdIncludingDeleted(int entityId)
        {
            List<AssortmentMinorRevisionInfoDto> dtoList = new List<AssortmentMinorRevisionInfoDto>();

            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.AssortmentMinorRevisionInfoFetchByEntityIdIncludingDeleted))
                {
                    CreateParameter(command, FieldNames.AssortmentMinorRevisionEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            return dtoList;
        }

       #endregion
    }
}
