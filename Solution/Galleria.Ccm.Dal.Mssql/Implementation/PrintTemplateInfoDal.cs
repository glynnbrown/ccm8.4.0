﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// MSSQL implementation of IPrintTemplateInfoDal
    /// </summary>
    public sealed class PrintTemplateInfoDal : Galleria.Framework.Dal.Mssql.DalBase, IPrintTemplateInfoDal
    {
        #region Constants

        private const String _importTableName = "#tmpPrintTemplate";
        private const String _createIdTableSql = "CREATE TABLE [{0}] (PrintTemplate_Id INT)";
        private const String _dropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        #endregion

        #region Nested Classes

        /// <summary>
        /// A simple data rd class used to bulk insert
        /// values into the database as fast as possible
        /// </summary>
        private class BulkCopyIntDataReader : IDataReader
        {
            #region Fields

            IEnumerator<Object> _enumerator; // an enumerator containing the values to write to the database

            #endregion

            #region Constructors

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to read from</param>
            public BulkCopyIntDataReader(IEnumerable<Object> values)
            {
                _enumerator = values.GetEnumerator();
            }

            #endregion

            #region Properties

            /// <summary>
            /// Returns the field count of the data rd
            /// </summary>
            public Int32 FieldCount
            {
                get { return 1; }
            }

            #endregion

            #region Methods
            /// <summary>
            /// Advances the data rd to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                object value = _enumerator.Current;
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PrintTemplateInfoDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PrintTemplateInfoDto()
            {
                Id = GetValue(dr[FieldNames.PrintTemplateId]),
                Name = (String)GetValue(dr[FieldNames.PrintTemplateName]),
                Description = (String)GetValue(dr[FieldNames.PrintTemplateDescription]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a list of dtos for the given ids
        /// </summary>
        public IEnumerable<PrintTemplateInfoDto> FetchByIds(IEnumerable<Object> printTemplateIds)
        {
            List<PrintTemplateInfoDto> dtoList = new List<PrintTemplateInfoDto>();

            //create temp table
            CreateTempTable(_createIdTableSql);

            // perform a batch insert to temp table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = this.CommandTimeout;
                bulkCopy.DestinationTableName = "[" + _importTableName + "]";
                bulkCopy.WriteToServer(new BulkCopyIntDataReader(printTemplateIds));
            }


            try
            {
                //execute
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateInfoFetchByIds))
                {

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            // and drop temp table
            DropTempTable();

            //return items
            return dtoList;
        }

        /// <summary>
        /// Returns a list of dtos for the given entity id
        /// </summary>
        public IEnumerable<PrintTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            List<PrintTemplateInfoDto> dtoList = new List<PrintTemplateInfoDto>();

            try
            {
                //execute
                using (DalCommand command = CreateCommand(ProcedureNames.PrintTemplateInfoByEntityId))
                {
                    CreateParameter(command, FieldNames.PrintTemplateEntityId, SqlDbType.Int, entityId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }

            //return items
            return dtoList;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the temporary table on the given connection
        /// </summary>
        private void CreateTempTable(String createSql)
        {
            // create the temp table
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, createSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Drops temporary table
        /// </summary>
        private void DropTempTable()
        {
            using (DalCommand command = CreateCommand(String.Format(CultureInfo.InvariantCulture, _dropTableSql, _importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
