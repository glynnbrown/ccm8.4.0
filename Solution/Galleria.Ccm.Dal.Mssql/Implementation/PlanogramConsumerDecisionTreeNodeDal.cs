﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-30763 :I.George
//  Added MetaData Fields
// V8-30737 : M.Shelley
//  Added new metadata fields for selected CDT nodes
#endregion
#region Version History : CCM830
// V8-32966 : A.Kuszyk
//  Fixed problem with single data being saved as an int.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     PlanogramConsumerDecisionTreeNode Dal
    /// </summary>
    public class PlanogramConsumerDecisionTreeNodeDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramConsumerDecisionTreeNodeDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramConsumerDecisionTreeNodeDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramConsumerDecisionTreeNodeDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeId]),
                PlanogramConsumerDecisionTreeId = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId]),
                PlanogramConsumerDecisionTreeLevelId = (Int32)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeName]),
                ParentNodeId = (Int32?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeParentNodeId]),
                MetaCountOfProducts = (Int32?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProducts]),
                MetaCountOfProductPlacedOnPlanogram = (Int32?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram]),
                MetaCountOfProductRecommendedInAssortment = (Int32?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment]),
                MetaLinearSpaceAllocatedToProductsOnPlanogram = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram]),
                MetaAreaSpaceAllocatedToProductsOnPlanogram = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram]),
                MetaVolumetricSpaceAllocatedToProductsOnPlanogram = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram]),
                P1 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Performance]),
                P2 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Performance]),
                P3 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Performance]),
                P4 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Performance]),
                P5 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Performance]),
                P6 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Performance]),
                P7 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Performance]),
                P8 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Performance]),
                P9 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Performance]),
                P10 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Performance]),
                P11 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Performance]),
                P12 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Performance]),
                P13 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Performance]),
                P14 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Performance]),
                P15 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Performance]),
                P16 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Performance]),
                P17 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Performance]),
                P18 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Performance]),
                P19 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Performance]),
                P20 = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Performance]),
                MetaP1Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Percentage]),
                MetaP2Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Percentage]),
                MetaP3Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Percentage]),
                MetaP4Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Percentage]),
                MetaP5Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Percentage]),
                MetaP6Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Percentage]),
                MetaP7Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Percentage]),
                MetaP8Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Percentage]),
                MetaP9Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Percentage]),
                MetaP10Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Percentage]),
                MetaP11Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Percentage]),
                MetaP12Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Percentage]),
                MetaP13Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Percentage]),
                MetaP14Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Percentage]),
                MetaP15Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Percentage]),
                MetaP16Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Percentage]),
                MetaP17Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Percentage]),
                MetaP18Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Percentage]),
                MetaP19Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage]),
                MetaP20Percentage = (Single?)GetValue(dr[FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Percentage]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a dto that matches the specified id
        /// </summary>
        /// <param name="ConsumerDecisionTreeId">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramConsumerDecisionTreeNodeDto> FetchByPlanogramConsumerDecisionTreeId(object planogramConsumerDecisionTreeId)
        {
            var dtoList = new List<PlanogramConsumerDecisionTreeNodeDto>();
            try
            {
                using (
                    DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeFetchByPlanogramConsumerDecisionTreeId)
                    )
                {
                    // Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId, SqlDbType.Int,
                        (Int32)planogramConsumerDecisionTreeId);

                    // Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }

        #endregion

        #region Insert

        private void CreateParameters(DalCommand command, PlanogramConsumerDecisionTreeNodeDto dto)
        {
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId, 
                SqlDbType.Int,
                dto.PlanogramConsumerDecisionTreeId);
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId, 
                SqlDbType.Int,
                dto.PlanogramConsumerDecisionTreeLevelId);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeName, SqlDbType.NVarChar, dto.Name);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeParentNodeId, SqlDbType.Int,dto.ParentNodeId);
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProducts, 
                SqlDbType.Int, 
                dto.MetaCountOfProducts);
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment, 
                SqlDbType.Int, 
                dto.MetaCountOfProductRecommendedInAssortment);
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram, 
                SqlDbType.Int, 
                dto.MetaCountOfProductPlacedOnPlanogram);
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram,
                SqlDbType.Real, 
                dto.MetaLinearSpaceAllocatedToProductsOnPlanogram);
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram,
                SqlDbType.Real, 
                dto.MetaAreaSpaceAllocatedToProductsOnPlanogram);
            CreateParameter(
                command, 
                FieldNames.PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram,
                SqlDbType.Real, 
                dto.MetaVolumetricSpaceAllocatedToProductsOnPlanogram);

            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Performance, SqlDbType.Real, dto.P1);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Performance, SqlDbType.Real, dto.P2);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Performance, SqlDbType.Real, dto.P3);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Performance, SqlDbType.Real, dto.P4);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Performance, SqlDbType.Real, dto.P5);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Performance, SqlDbType.Real, dto.P6);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Performance, SqlDbType.Real, dto.P7);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Performance, SqlDbType.Real, dto.P8);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Performance, SqlDbType.Real, dto.P9);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Performance, SqlDbType.Real, dto.P10);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Performance, SqlDbType.Real, dto.P11);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Performance, SqlDbType.Real, dto.P12);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Performance, SqlDbType.Real, dto.P13);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Performance, SqlDbType.Real, dto.P14);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Performance, SqlDbType.Real, dto.P15);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Performance, SqlDbType.Real, dto.P16);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Performance, SqlDbType.Real, dto.P17);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Performance, SqlDbType.Real, dto.P18);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Performance, SqlDbType.Real, dto.P19);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Performance, SqlDbType.Real, dto.P20);

            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP1Percentage, SqlDbType.Real, dto.MetaP1Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP2Percentage, SqlDbType.Real, dto.MetaP2Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP3Percentage, SqlDbType.Real, dto.MetaP3Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP4Percentage, SqlDbType.Real, dto.MetaP4Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP5Percentage, SqlDbType.Real, dto.MetaP5Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP6Percentage, SqlDbType.Real, dto.MetaP6Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP7Percentage, SqlDbType.Real, dto.MetaP7Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP8Percentage, SqlDbType.Real, dto.MetaP8Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP9Percentage, SqlDbType.Real, dto.MetaP9Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP10Percentage, SqlDbType.Real, dto.MetaP10Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP11Percentage, SqlDbType.Real, dto.MetaP11Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP12Percentage, SqlDbType.Real, dto.MetaP12Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP13Percentage, SqlDbType.Real, dto.MetaP13Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP14Percentage, SqlDbType.Real, dto.MetaP14Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP15Percentage, SqlDbType.Real, dto.MetaP15Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP16Percentage, SqlDbType.Real, dto.MetaP16Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP17Percentage, SqlDbType.Real, dto.MetaP17Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP18Percentage, SqlDbType.Real, dto.MetaP18Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP19Percentage, SqlDbType.Real, dto.MetaP19Percentage);
            CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeMetaP20Percentage, SqlDbType.Real, dto.MetaP20Percentage);
        }

        /// <summary>
        ///     Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeId,
                        SqlDbType.Int);
                    CreateParameters(command, dto);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeDto dto in dtos)
            {
                this.Insert(dto);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeId, SqlDbType.Int, dto.Id);
                    CreateParameters(command, dto);

                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }


        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramConsumerDecisionTreeNodeDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramConsumerDecisionTreeNodeId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            foreach (PlanogramConsumerDecisionTreeNodeDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
