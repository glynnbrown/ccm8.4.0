﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// PlanogramExportTemplate dal implementation
    /// </summary>
    public class PlanogramExportTemplateDal : DalBase, IPlanogramExportTemplateDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static PlanogramExportTemplateDto GetDataTransferObject(IDataRecord dr)
        {
            return new PlanogramExportTemplateDto
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplateId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.PlanogramExportTemplateRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.PlanogramExportTemplateEntityId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramExportTemplateName]),
                FileType = (Byte)GetValue(dr[FieldNames.PlanogramExportTemplateFileType]),
                FileVersion = (String)GetValue(dr[FieldNames.PlanogramExportTemplateFileVersion]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.PlanogramExportTemplateDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.PlanogramExportTemplateDateLastModified]),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="PlanogramExportTemplateDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="PlanogramExportTemplateDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="PlanogramExportTemplateDto" /> with the data for the provided <paramref name="id" />.</returns>
        public PlanogramExportTemplateDto FetchById(Object id)
        {
            PlanogramExportTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramExportTemplateId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public PlanogramExportTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            PlanogramExportTemplateDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.PlanogramExportTemplateEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateName, SqlDbType.NVarChar, name);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="PlanogramExportTemplateDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(PlanogramExportTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateId, SqlDbType.Int);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);


                    //Other properties
                    CreateParameter(command, FieldNames.PlanogramExportTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateFileType, SqlDbType.TinyInt, dto.FileType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateFileVersion, SqlDbType.NVarChar, dto.FileVersion);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="PlanogramExportTemplateDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(PlanogramExportTemplateDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramExportTemplateId, SqlDbType.Int, dto.Id);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateRowVersion, SqlDbType.Timestamp,
                        ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.PlanogramExportTemplateDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);


                    //Other properties
                    CreateParameter(command, FieldNames.PlanogramExportTemplateEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateFileType, SqlDbType.TinyInt, dto.FileType);
                    CreateParameter(command, FieldNames.PlanogramExportTemplateFileVersion, SqlDbType.NVarChar, dto.FileVersion);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.PlanogramExportTemplateDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramExportTemplateId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a PlanogramExportTemplate for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the PlanogramExportTemplate.</param>
        /// <returns><c>True</c> if the PlanogramExportTemplate was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void LockById(Object id)
        {
            // Not implemented as this dal does not use locking mechamisms.
        }

        /// <summary>
        ///     Unlocks a PlanogramExportTemplate after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the PlanogramExportTemplate.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }

        #endregion
    }
}
