﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25477 : A. Kuszyk
//  Initial version. Adapted from GFS.  
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties  
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    public class PlanogramFixtureDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanogramFixtureDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public static PlanogramFixtureDto GetDataTransferObject(SqlDataReader dr)
        {
            return new PlanogramFixtureDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanogramFixtureId]),
                PlanogramId = (Object)GetValue(dr[FieldNames.PlanogramFixturePlanogramId]),
                Name = (String)GetValue(dr[FieldNames.PlanogramFixtureName]),
                Height = (Single)GetValue(dr[FieldNames.PlanogramFixtureHeight]),
                Width = (Single)GetValue(dr[FieldNames.PlanogramFixtureWidth]),
                Depth = (Single)GetValue(dr[FieldNames.PlanogramFixtureDepth]),
                MetaNumberOfMerchandisedSubComponents = (Int16?)GetValue(dr[FieldNames.PlanogramFixtureMetaNumberOfMerchandisedSubComponents]),
                MetaTotalFixtureCost = (Single?)GetValue(dr[FieldNames.PlanogramFixtureMetaTotalFixtureCost])
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public IEnumerable<PlanogramFixtureDto> FetchByPlanogramId(Object planogramId)
        {
            List<PlanogramFixtureDto> dtoList = new List<PlanogramFixtureDto>();
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureFetchByPlanogramId))
                {
                    CreateParameter(command, FieldNames.PlanogramFixturePlanogramId, SqlDbType.Int, planogramId);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtoList;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Inserts a specified dto into the db
        /// </summary>
        /// <param name="dto">dto to insert</param>
        public void Insert(PlanogramFixtureDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureInsert))
                {
                    //Id
                    SqlParameter idParameter = CreateParameter(command, FieldNames.PlanogramFixtureId, SqlDbType.Int);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramFixturePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramFixtureName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramFixtureHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.PlanogramFixtureWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.PlanogramFixtureDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.PlanogramFixtureMetaNumberOfMerchandisedSubComponents, SqlDbType.SmallInt, dto.MetaNumberOfMerchandisedSubComponents);
                    CreateParameter(command, FieldNames.PlanogramFixtureMetaTotalFixtureCost, SqlDbType.Real, dto.MetaTotalFixtureCost);
                    
                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Inserts the specified items
        /// </summary>
        public void Insert(IEnumerable<PlanogramFixtureDto> dtos)
        {
            foreach (PlanogramFixtureDto dto in dtos)
            {
                this.Insert(dto);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update a specified dto in the db
        /// </summary>
        /// <param name="dto">dto to update</param>
        public void Update(PlanogramFixtureDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureUpdateById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramFixtureId, SqlDbType.Int, dto.Id);

                    //Other properties 
                    CreateParameter(command, FieldNames.PlanogramFixturePlanogramId, SqlDbType.Int, dto.PlanogramId);
                    CreateParameter(command, FieldNames.PlanogramFixtureName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.PlanogramFixtureHeight, SqlDbType.Real, dto.Height);
                    CreateParameter(command, FieldNames.PlanogramFixtureWidth, SqlDbType.Real, dto.Width);
                    CreateParameter(command, FieldNames.PlanogramFixtureDepth, SqlDbType.Real, dto.Depth);
                    CreateParameter(command, FieldNames.PlanogramFixtureMetaNumberOfMerchandisedSubComponents, SqlDbType.SmallInt, dto.MetaNumberOfMerchandisedSubComponents);
                    CreateParameter(command, FieldNames.PlanogramFixtureMetaTotalFixtureCost, SqlDbType.Real, dto.MetaTotalFixtureCost);
                    
                    //execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Updates the specified items
        /// </summary>
        public void Update(IEnumerable<PlanogramFixtureDto> dtos)
        {
            foreach (PlanogramFixtureDto dto in dtos)
            {
                this.Update(dto);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.PlanogramFixtureDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.PlanogramFixtureId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        /// <summary>
        /// Deletes the specified item
        /// </summary>
        public void Delete(PlanogramFixtureDto dto)
        {
            this.DeleteById(dto.Id);
        }

        /// <summary>
        /// Deletes the specified items
        /// </summary>
        public void Delete(IEnumerable<PlanogramFixtureDto> dtos)
        {
            foreach (PlanogramFixtureDto dto in dtos)
            {
                this.Delete(dto);
            }
        }

        #endregion
    }
}
