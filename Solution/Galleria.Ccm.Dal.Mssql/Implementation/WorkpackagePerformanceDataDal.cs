﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#region Version History: CCM803
// V8-29491 : D.Pleasance
//  removed Location code and added DataType \ DataId so that we can hold performance data at a variety of levels 
//      ((DataType=Enterprise, DataId=0), (DataType=Cluster, DataId=ClusterId), (DataType=Store, DataId=LocationId))
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    /// Mssql implementation of IWorkpackagePerformanceDataDal
    /// </summary>
    /// <remarks>
    /// This dal is a little different from the norm.
    /// As performance data is only needs to be held
    /// temporarily before being inserted into a planogram
    /// we hold this data as global temporary tables inside
    /// temp db. That means no stored procedures, so the 
    /// queries are coded inside this dal instead
    /// </remarks>
    public sealed class WorkpackagePerformanceDataDal : Galleria.Framework.Dal.Mssql.DalBase, IWorkpackagePerformanceDataDal
    {
        #region Constants
        private const Int32 _insertTimeout = 3600;
        private const Int32 _deleteTimeout = 3600;

        private const String _dropTableSql = "IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0}";

        private const String _tmpImport = "#tmpWorkpackagePerformanceData";
        private const String _createImportTableSql =
            "CREATE TABLE [" + _tmpImport + "] (" +
            "[Workpackage_Id] INT NOT NULL, " +
            "[PerformanceSelection_Id] INT NOT NULL, " +            
            "[Product_GTIN] NVARCHAR(14) COLLATE database_default NOT NULL, " +
            "[DataType] TINYINT NOT NULL, " +
            "[Data_Id] INT NOT NULL, " +
            "[WorkpackagePerformanceData_P1] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P2] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P3] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P4] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P5] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P6] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P7] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P8] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P9] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P10] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P11] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P12] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P13] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P14] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P15] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P16] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P17] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P18] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P19] [REAL] NULL, " +
            "[WorkpackagePerformanceData_P20] [REAL] NULL, " +
            ")";

        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class WorkpackagePerformanceDataBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<WorkpackagePerformanceDataDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public WorkpackagePerformanceDataBulkCopyDataReader(IEnumerable<WorkpackagePerformanceDataDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public Int32 FieldCount
            {
                get { return 25; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public Boolean Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public Object GetValue(int i)
            {
                Object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.WorkpackageId;
                        break;
                    case 1:
                        value = _enumerator.Current.PerformanceSelectionId;
                        break;
                    case 2:
                        value = _enumerator.Current.ProductGTIN;
                        break;
                    case 3:
                        value = _enumerator.Current.DataType;
                        break;
                    case 4:
                        value = _enumerator.Current.DataId;
                        break;
                    case 5:
                        value = _enumerator.Current.P1;
                        break;
                    case 6:
                        value = _enumerator.Current.P2;
                        break;
                    case 7:
                        value = _enumerator.Current.P3;
                        break;
                    case 8:
                        value = _enumerator.Current.P4;
                        break;
                    case 9:
                        value = _enumerator.Current.P5;
                        break;
                    case 10:
                        value = _enumerator.Current.P6;
                        break;
                    case 11:
                        value = _enumerator.Current.P7;
                        break;
                    case 12:
                        value = _enumerator.Current.P8;
                        break;
                    case 13:
                        value = _enumerator.Current.P9;
                        break;
                    case 14:
                        value = _enumerator.Current.P10;
                        break;
                    case 15:
                        value = _enumerator.Current.P11;
                        break;
                    case 16:
                        value = _enumerator.Current.P12;
                        break;
                    case 17:
                        value = _enumerator.Current.P13;
                        break;
                    case 18:
                        value = _enumerator.Current.P14;
                        break;
                    case 19:
                        value = _enumerator.Current.P15;
                        break;
                    case 20:
                        value = _enumerator.Current.P16;
                        break;
                    case 21:
                        value = _enumerator.Current.P17;
                        break;
                    case 22:
                        value = _enumerator.Current.P18;
                        break;
                    case 23:
                        value = _enumerator.Current.P19;
                        break;
                    case 24:
                        value = _enumerator.Current.P20;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }
        #endregion

        #region Data Transfer Object
        /// <summary>
        /// Creates a data transfer object from a data reader
        /// </summary>
        public static WorkpackagePerformanceDataDto GetDataTransferObject(SqlDataReader dr)
        {
            return new WorkpackagePerformanceDataDto()
            {
                WorkpackageId = (Int32)GetValue(dr[FieldNames.WorkpackagePerformanceDataWorkpackageId]),
                PerformanceSelectionId = (Int32)GetValue(dr[FieldNames.WorkpackagePerformanceDataPerformanceSelectionId]),                
                ProductGTIN = (String)GetValue(dr[FieldNames.WorkpackagePerformanceDataProductCode]),
                DataType = (Byte)GetValue(dr[FieldNames.WorkpackagePerformanceDataDataType]),
                DataId = (Int32?)GetValue(dr[FieldNames.WorkpackagePerformanceDataDataId]),
                P1 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP1]),
                P2 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP2]),
                P3 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP3]),
                P4 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP4]),
                P5 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP5]),
                P6 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP6]),
                P7 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP7]),
                P8 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP8]),
                P9 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP9]),
                P10 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP10]),
                P11 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP11]),
                P12 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP12]),
                P13 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP13]),
                P14 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP14]),
                P15 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP15]),
                P16 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP16]),
                P17 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP17]),
                P18 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP18]),
                P19 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP19]),
                P20 = (Single?)GetValue(dr[FieldNames.WorkpackagePerformanceDataP20])
            };
        }

        #endregion

        #region Fetch
        
        public IEnumerable<WorkpackagePerformanceDataDto> FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(Int32 workpackageId, Int32 performanceSelectionId, Byte dataType, Int32 dataId)
        {
            List<WorkpackagePerformanceDataDto> dtoList = new List<WorkpackagePerformanceDataDto>();
            try
            {
                // fetch the performance data
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePerformanceDataFetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId))
                {
                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePerformanceDataWorkpackageId, SqlDbType.Int, workpackageId);
                    CreateParameter(command, FieldNames.WorkpackagePerformanceDataPerformanceSelectionId, SqlDbType.Int, performanceSelectionId);
                    CreateParameter(command, FieldNames.WorkpackagePerformanceDataDataType, SqlDbType.TinyInt, dataType);
                    CreateParameter(command, FieldNames.WorkpackagePerformanceDataDataId, SqlDbType.Int, dataId);

                    // execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            return dtoList;
        }

        #endregion

        #region Upsert
        /// <summary>
        /// Upserts a set of data into the database
        /// </summary>
        public void BulkInsert(Int32 workpackageId, IEnumerable<WorkpackagePerformanceDataDto> dtoList)
        {
            try
            {
                // create an populate the import talbe
                this.CreateTempImportTable(dtoList);

                // call the bulk insert procedure
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePerformanceDataBulkInsert))
                {
                    command.CommandTimeout = _insertTimeout;
                    CreateParameter(command, FieldNames.WorkpackagePerformanceDataWorkpackageId, SqlDbType.Int, workpackageId);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
            finally
            {
                this.DropTable(_tmpImport);
            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// Deletes all performance data for the specified workpackage
        /// </summary>
        public void DeleteByWorkpackageId(Int32 workpackageId)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.WorkpackagePerformanceDataDeleteByWorkpackageId))
                {
                    command.CommandTimeout = _deleteTimeout;

                    // parameters
                    CreateParameter(command, FieldNames.WorkpackagePerformanceDataWorkpackageId, SqlDbType.Int, workpackageId);
                    
                    // execute
                    command.ExecuteNonQuery();
                }

            }
            catch (SqlException ex)
            {
                DalExceptionHelper.ThrowException(ex);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a temporary import table
        /// </summary>
        private void CreateTempImportTable(IEnumerable<WorkpackagePerformanceDataDto> dtoList)
        {
            // create the temp table
            using (DalCommand command = CreateCommand(_createImportTableSql, CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // bulk copy the data into the temp table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.DalContext.Connection, SqlBulkCopyOptions.Default, this.DalContext.Transaction))
            {
                bulkCopy.BulkCopyTimeout = _insertTimeout;
                bulkCopy.DestinationTableName = "[" + _tmpImport + "]";
                WorkpackagePerformanceDataBulkCopyDataReader dr = new WorkpackagePerformanceDataBulkCopyDataReader(dtoList);
                bulkCopy.WriteToServer(dr);
            }
        }
        
        /// <summary>
        /// Drops the specified table
        /// </summary>
        private void DropTable(String importTableName)
        {
            using (DalCommand command = CreateCommand(String.Format(_dropTableSql, importTableName), CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
