﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27940 : L.Luong
//  Created

#endregion

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     Label Dal
    /// </summary>
    public class LabelDal : DalBase, ILabelDal
    {
        #region Data Transfer Object

        /// <summary>
        ///     Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static LabelDto GetDataTransferObject(IDataRecord dr)
        {
            return new LabelDto
            {
                Id = (Int32)GetValue(dr[FieldNames.LabelId]),
                RowVersion = new RowVersion(GetValue(dr[FieldNames.LabelRowVersion])),
                EntityId = (Int32)GetValue(dr[FieldNames.LabelEntityId]),
                Name = (String)GetValue(dr[FieldNames.LabelName]),
                Type = (Byte)GetValue(dr[FieldNames.LabelType]),
                BackgroundColour = (Int32)GetValue(dr[FieldNames.LabelBackgroundColour]),
                BorderColour = (Int32)GetValue(dr[FieldNames.LabelBorderColour]),
                BorderThickness = (Int32)GetValue(dr[FieldNames.LabelBorderThickness]),
                Text = (String)GetValue(dr[FieldNames.LabelText]),
                TextColour = (Int32)GetValue(dr[FieldNames.LabelTextColour]),
                Font = (String)GetValue(dr[FieldNames.LabelFont]),
                FontSize = (Int32)GetValue(dr[FieldNames.LabelFontSize]),
                IsRotateToFitOn = (Boolean)GetValue(dr[FieldNames.LabelIsRotateToFitOn]),
                IsShrinkToFitOn = (Boolean)GetValue(dr[FieldNames.LabelIsShrinkToFitOn]),
                TextBoxFontBold = (Boolean)GetValue(dr[FieldNames.LabelTextBoxFontBold]),
                TextBoxFontItalic = (Boolean)GetValue(dr[FieldNames.LabelTextBoxFontItalic]),
                TextBoxFontUnderlined = (Boolean)GetValue(dr[FieldNames.LabelTextBoxFontUnderlined]),
                LabelHorizontalPlacement = (Byte)GetValue(dr[FieldNames.LabelLabelHorizontalPlacement]),
                LabelVerticalPlacement = (Byte)GetValue(dr[FieldNames.LabelLabelVerticalPlacement]),
                TextBoxTextAlignment = (Byte)GetValue(dr[FieldNames.LabelTextBoxTextAlignment]),
                TextDirection = (Byte)GetValue(dr[FieldNames.LabelTextDirection]),
                TextWrapping = (Byte)GetValue(dr[FieldNames.LabelTextWrapping]),
                BackgroundTransparency = (Single)GetValue(dr[FieldNames.LabelBackgroundTransparency]),
                ShowOverImages = (Boolean)GetValue(dr[FieldNames.LabelShowOverImages]),
                ShowLabelPerFacing = (Boolean)GetValue(dr[FieldNames.LabelShowLabelPerFacing]),
                DateCreated = (DateTime)GetValue(dr[FieldNames.LabelDateCreated]),
                DateLastModified = (DateTime)GetValue(dr[FieldNames.LabelDateLastModified]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.LabelDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="LabelDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="LabelDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="LabelDto" /> with the data for the provided <paramref name="id" />.</returns>
        public LabelDto FetchById(Object id)
        {
            LabelDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.LabelFetchById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LabelId, SqlDbType.Int, id);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        public LabelDto FetchByEntityIdName(Int32 entityId, String name)
        {
            LabelDto dto = null;
            try
            {
                using (var command = CreateCommand(ProcedureNames.LabelFetchByEntityIdName))
                {
                    CreateParameter(command, FieldNames.LabelEntityId, SqlDbType.Int, entityId);
                    CreateParameter(command, FieldNames.LabelName, SqlDbType.NVarChar, name);

                    //Execute
                    using (var dr = command.ExecuteReader())
                    {
                        if (!dr.Read()) throw new DtoDoesNotExistException();

                        dto = GetDataTransferObject(dr);
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="LabelDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(LabelDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.LabelInsert))
                {
                    //Id
                    var idParameter = CreateParameter(command, FieldNames.LabelId, SqlDbType.Int);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.LabelRowVersion, SqlDbType.Timestamp);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.LabelDateCreated, SqlDbType.DateTime, 
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.LabelDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    var dateDeletedParameter = CreateParameter(command, FieldNames.LabelDateDeleted, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties
                    CreateParameter(command, FieldNames.LabelEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LabelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LabelType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.LabelBackgroundColour, SqlDbType.Int, dto.BackgroundColour);
                    CreateParameter(command, FieldNames.LabelBorderColour, SqlDbType.Int, dto.BorderColour);
                    CreateParameter(command, FieldNames.LabelBorderThickness, SqlDbType.Int, dto.BorderThickness);
                    CreateParameter(command, FieldNames.LabelText, SqlDbType.NVarChar, dto.Text);
                    CreateParameter(command, FieldNames.LabelTextColour, SqlDbType.Int, dto.TextColour);
                    CreateParameter(command, FieldNames.LabelFont, SqlDbType.NVarChar, dto.Font);
                    CreateParameter(command, FieldNames.LabelFontSize, SqlDbType.Int, dto.FontSize);
                    CreateParameter(command, FieldNames.LabelIsRotateToFitOn, SqlDbType.Bit, dto.IsRotateToFitOn);
                    CreateParameter(command, FieldNames.LabelIsShrinkToFitOn, SqlDbType.Bit, dto.IsShrinkToFitOn);
                    CreateParameter(command, FieldNames.LabelTextBoxFontBold, SqlDbType.Bit, dto.TextBoxFontBold);
                    CreateParameter(command, FieldNames.LabelTextBoxFontItalic, SqlDbType.Bit, dto.TextBoxFontItalic);
                    CreateParameter(command, FieldNames.LabelTextBoxFontUnderlined, SqlDbType.Bit, dto.TextBoxFontUnderlined);
                    CreateParameter(command, FieldNames.LabelLabelHorizontalPlacement, SqlDbType.TinyInt, dto.LabelHorizontalPlacement);
                    CreateParameter(command, FieldNames.LabelLabelVerticalPlacement, SqlDbType.TinyInt, dto.LabelVerticalPlacement);
                    CreateParameter(command, FieldNames.LabelTextBoxTextAlignment, SqlDbType.TinyInt, dto.TextBoxTextAlignment);
                    CreateParameter(command, FieldNames.LabelTextDirection, SqlDbType.TinyInt, dto.TextDirection);
                    CreateParameter(command, FieldNames.LabelTextWrapping, SqlDbType.TinyInt, dto.TextWrapping);
                    CreateParameter(command, FieldNames.LabelBackgroundTransparency, SqlDbType.Real, dto.BackgroundTransparency);
                    CreateParameter(command, FieldNames.LabelShowOverImages, SqlDbType.Bit, dto.ShowOverImages);
                    CreateParameter(command, FieldNames.LabelShowLabelPerFacing, SqlDbType.Bit, dto.ShowLabelPerFacing);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.Id = (Int32)idParameter.Value;
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value
                        ? null
                        : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="LabelDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(LabelDto dto)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.LabelUpdate))
                {
                    //Id
                    CreateParameter(command, FieldNames.LabelId, SqlDbType.Int, dto.Id);

                    //Row version
                    var rowVersionParameter = CreateParameter(command, FieldNames.LabelRowVersion, SqlDbType.Timestamp, 
                        ParameterDirection.InputOutput, dto.RowVersion);

                    //DateCreated
                    var dateCreatedParameter = CreateParameter(command, FieldNames.LabelDateCreated, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateCreated);

                    //Date last modified
                    var dateLastModifiedParameter = CreateParameter(command, FieldNames.LabelDateLastModified, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateLastModified);

                    //Date deleted
                    var dateDeletedParameter = CreateParameter(command,  FieldNames.LabelDateDeleted, SqlDbType.DateTime,
                        ParameterDirection.Output, dto.DateDeleted);
                    dateDeletedParameter.IsNullable = true;

                    //Other properties
                    CreateParameter(command, FieldNames.LabelEntityId, SqlDbType.Int, dto.EntityId);
                    CreateParameter(command, FieldNames.LabelName, SqlDbType.NVarChar, dto.Name);
                    CreateParameter(command, FieldNames.LabelType, SqlDbType.TinyInt, dto.Type);
                    CreateParameter(command, FieldNames.LabelBackgroundColour, SqlDbType.Int, dto.BackgroundColour);
                    CreateParameter(command, FieldNames.LabelBorderColour, SqlDbType.Int, dto.BorderColour);
                    CreateParameter(command, FieldNames.LabelBorderThickness, SqlDbType.Int, dto.BorderThickness);
                    CreateParameter(command, FieldNames.LabelText, SqlDbType.NVarChar, dto.Text);
                    CreateParameter(command, FieldNames.LabelTextColour, SqlDbType.Int, dto.TextColour);
                    CreateParameter(command, FieldNames.LabelFont, SqlDbType.NVarChar, dto.Font);
                    CreateParameter(command, FieldNames.LabelFontSize, SqlDbType.Int, dto.FontSize);
                    CreateParameter(command, FieldNames.LabelIsRotateToFitOn, SqlDbType.Bit, dto.IsRotateToFitOn);
                    CreateParameter(command, FieldNames.LabelIsShrinkToFitOn, SqlDbType.Bit, dto.IsShrinkToFitOn);
                    CreateParameter(command, FieldNames.LabelTextBoxFontBold, SqlDbType.Bit, dto.TextBoxFontBold);
                    CreateParameter(command, FieldNames.LabelTextBoxFontItalic, SqlDbType.Bit, dto.TextBoxFontItalic);
                    CreateParameter(command, FieldNames.LabelTextBoxFontUnderlined, SqlDbType.Bit, dto.TextBoxFontUnderlined);
                    CreateParameter(command, FieldNames.LabelLabelHorizontalPlacement, SqlDbType.TinyInt, dto.LabelHorizontalPlacement);
                    CreateParameter(command, FieldNames.LabelLabelVerticalPlacement, SqlDbType.TinyInt, dto.LabelVerticalPlacement);
                    CreateParameter(command, FieldNames.LabelTextBoxTextAlignment, SqlDbType.TinyInt, dto.TextBoxTextAlignment);
                    CreateParameter(command, FieldNames.LabelTextDirection, SqlDbType.TinyInt, dto.TextDirection);
                    CreateParameter(command, FieldNames.LabelTextWrapping, SqlDbType.TinyInt, dto.TextWrapping);
                    CreateParameter(command, FieldNames.LabelBackgroundTransparency, SqlDbType.Real, dto.BackgroundTransparency);
                    CreateParameter(command, FieldNames.LabelShowOverImages, SqlDbType.Bit, dto.ShowOverImages);
                    CreateParameter(command, FieldNames.LabelShowLabelPerFacing, SqlDbType.Bit, dto.ShowLabelPerFacing);

                    //execute
                    command.ExecuteNonQuery();

                    //update dto
                    dto.RowVersion = new RowVersion(rowVersionParameter.Value);
                    dto.DateCreated = (DateTime)(dateCreatedParameter.Value);
                    dto.DateLastModified = (DateTime)(dateLastModifiedParameter.Value);
                    dto.DateDeleted = dateDeletedParameter.Value == DBNull.Value
                        ? null
                        : (DateTime?)dateDeletedParameter.Value;
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            try
            {
                using (var command = CreateCommand(ProcedureNames.LabelDeleteById))
                {
                    //Id
                    CreateParameter(command, FieldNames.LabelId, SqlDbType.Int, id);

                    //Execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a label for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the label.</param>
        /// <returns><c>True</c> if the label was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void LockById(Object id)
        {
            // Not implemented as this dal does not use locking mechamisms.
        }

        /// <summary>
        ///     Unlocks a label after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the label.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            // Not implemented as this dal does not use locking mechanisms.
        }

        #endregion
    }
}
