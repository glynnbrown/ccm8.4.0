﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27153 : A.Silva   ~ Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql.Schema;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using DalBase = Galleria.Framework.Dal.Mssql.DalBase;

namespace Galleria.Ccm.Dal.Mssql.Implementation
{
    /// <summary>
    ///     Dal implementation of <see cref="RenumberingStrategyInfoDal"/> for MS SQL.
    /// </summary>
    public class RenumberingStrategyInfoDal : DalBase, IRenumberingStrategyInfoDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        private static RenumberingStrategyInfoDto GetDataTransferObject(IDataRecord dr)
        {
            return new RenumberingStrategyInfoDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.RenumberingStrategyId]),
                EntityId = (Int32)GetValue(dr[FieldNames.RenumberingStrategyEntityId]),
                Name = (String)GetValue(dr[FieldNames.RenumberingStrategyName]),
                DateDeleted = (DateTime?)GetValue(dr[FieldNames.RenumberingStrategyDateDeleted])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns an enumeration of <see cref="RenumberingStrategyInfoDto" /> matching the provided <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> that will be matched for fetching.</param>
        /// <returns>An enumeration of instances of <see cref="RenumberingStrategyDto" /> wmatching the provided <paramref name="entityId" />.</returns>
        public IEnumerable<RenumberingStrategyInfoDto> FetchByEntityId(Int32 entityId)
        {
            return ExecuteProcedure(entityId, ProcedureNames.RenumberingStrategyFetchByEntityId);
        }

        /// <summary>
        ///     Returns an enumeration of <see cref="RenumberingStrategyInfoDto" /> matching the provided <paramref name="entityId" />, including deleted records.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> that will be matched for fetching.</param>
        /// <returns>An enumeration of instances of <see cref="RenumberingStrategyInfoDto" /> wmatching the provided <paramref name="entityId" />.</returns>
        public IEnumerable<RenumberingStrategyInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return ExecuteProcedure(entityId, ProcedureNames.RenumberingStrategyFetchByEntityIdIncludingDeleted);
        }

        private IEnumerable<RenumberingStrategyInfoDto> ExecuteProcedure(Int32 entityId, String procedureName)
        {
            var dtos = new List<RenumberingStrategyInfoDto>();

            try
            {
                using (var command = CreateCommand(procedureName))
                {
                    CreateParameter(command, FieldNames.RenumberingStrategyEntityId, SqlDbType.Int, entityId);

                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtos.Add(GetDataTransferObject(dr));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dtos;
        }

        #endregion
    }
}
