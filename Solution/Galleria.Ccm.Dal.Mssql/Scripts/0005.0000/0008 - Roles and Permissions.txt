﻿-- Title: CCM V8 v4.0 Roles and Permissions Scripts
-- Product: CCM v8.1.0
-- Company: Galleria RTS Ltd
-- Copyright: Copyright © Galleria RTS Ltd 2015


-- create the ccm role if required
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'CcmUser_DataWriter' AND type = 'R')
BEGIN
	CREATE ROLE [CcmUser_DataWriter] AUTHORIZATION [dbo]
END
GO

-- use a cursor to grant execute permissions to all stored procedures
DECLARE procedureCursor CURSOR FOR SELECT SPECIFIC_SCHEMA, SPECIFIC_NAME FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE='PROCEDURE'
DECLARE @Sql NVARCHAR(MAX)
DECLARE @Schema NVARCHAR(MAX)
DECLARE @Procedure NVARCHAR(MAX)

OPEN procedureCursor

FETCH NEXT FROM procedureCursor INTO @Schema, @Procedure
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @Sql = 'GRANT EXECUTE ON [' + @Schema + '].[' + @Procedure + '] TO [CcmUser_DataWriter]'
	EXEC SP_EXECUTESQL @SQL
	FETCH NEXT FROM procedureCursor INTO @Schema, @Procedure
END
CLOSE procedureCursor
DEALLOCATE procedureCursor
GO

-- Use a cursor to grant select permissions to all views
DECLARE viewCursor CURSOR FOR SELECT TABLE_SCHEMA, [TABLE_NAME] FROM INFORMATION_SCHEMA.VIEWS 
DECLARE @Sql NVARCHAR(MAX)
DECLARE @Schema NVARCHAR(MAX)
DECLARE @View NVARCHAR(MAX)

OPEN viewCursor

FETCH NEXT FROM viewCursor INTO @Schema, @View
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @Sql = 'GRANT SELECT ON [' + @Schema + '].[' + @View + '] TO [CcmUser_DataWriter]'
	EXEC SP_EXECUTESQL @SQL
	FETCH NEXT FROM viewCursor INTO @Schema, @View
END
CLOSE viewCursor
DEALLOCATE viewCursor
GO

-- Use a cursor to grant select permissions to all tables
DECLARE viewCursor CURSOR FOR SELECT TABLE_SCHEMA, [TABLE_NAME] FROM INFORMATION_SCHEMA.VIEWS 
DECLARE @Sql NVARCHAR(MAX)
DECLARE @Schema NVARCHAR(MAX)
DECLARE @View NVARCHAR(MAX)

OPEN viewCursor

FETCH NEXT FROM viewCursor INTO @Schema, @View
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @Sql = 'GRANT SELECT ON [' + @Schema + '].[' + @View + '] TO [CcmUser_DataWriter]'
	EXEC SP_EXECUTESQL @SQL
	FETCH NEXT FROM viewCursor INTO @Schema, @View
END
CLOSE viewCursor
DEALLOCATE viewCursor
GO


-- Use a cursor to grant select permissions to all tables
DECLARE tableCursor CURSOR FOR SELECT TABLE_SCHEMA, [TABLE_NAME] FROM INFORMATION_SCHEMA.TABLES 
DECLARE @Sql NVARCHAR(MAX)
DECLARE @Schema NVARCHAR(MAX)
DECLARE @Table NVARCHAR(MAX)

OPEN tableCursor

FETCH NEXT FROM tableCursor INTO @Schema, @Table
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @Sql = 'GRANT SELECT ON [' + @Schema + '].[' + @Table + '] TO [CcmUser_DataWriter]'
	EXEC SP_EXECUTESQL @SQL
	--PRINT @Sql
	FETCH NEXT FROM tableCursor INTO @Schema, @Table
END
CLOSE tableCursor
DEALLOCATE tableCursor
GO