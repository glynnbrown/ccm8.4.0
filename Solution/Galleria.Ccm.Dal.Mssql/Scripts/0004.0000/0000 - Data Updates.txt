﻿-- Title: CCM V8 v4.0 Data Update Scripts
-- Product: CCM v8.0.3
-- Company: Galleria RTS Ltd
-- Copyright: Copyright © Galleria RTS Ltd 2015

-- V8-29731 : N.Foster
--	Clear the message queue due to changes in message format

-- clear the message queue
DELETE [Engine].[Message];
DELETE [Engine].[MessageData];
DELETE [Engine].[MessageDependency];

-- delete any existing instances
DELETE [Engine].[Instance];

-- delete any existing planogram locks
DELETE [Content].[PlanogramLock]

-- reset the status of any workpackages
-- that are currently in progress, as we
-- have cleared the queue
UPDATE
	[Content].[WorkpackageProcessingStatus]
SET
	[WorkpackageProcessingStatus_Status] = 0
WHERE
	[WorkpackageProcessingStatus_Status] IN (1,2);

-- reset the status of any planograms
-- that are currently in progress, as we
-- have cleared the queue
UPDATE
	[Content].[PlanogramProcessingStatus]
SET
	[PlanogramProcessingStatus_AutomationStatus] = 0
WHERE
	[PlanogramProcessingStatus_AutomationStatus] IN (1,2);