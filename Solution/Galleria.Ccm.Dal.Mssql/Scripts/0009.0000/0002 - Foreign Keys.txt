﻿-- Title: CCM V8 v8.0 Foreign Keys Scripts
-- Product: CCM v8.3.1
-- Company: Galleria RTS Ltd
-- Copyright: Copyright © Galleria RTS Ltd 2016

/****** [FK_WorkpackagePlanogramProcessingStatus_Planogram] ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Content].[FK_WorkpackagePlanogramProcessingStatus_Planogram]') AND parent_object_id = OBJECT_ID(N'[Content].[WorkpackagePlanogramProcessingStatus]'))
BEGIN
	ALTER TABLE 
		[Content].[WorkpackagePlanogramProcessingStatus]  
	WITH CHECK ADD  CONSTRAINT 
		[FK_WorkpackagePlanogramProcessingStatus_Planogram] FOREIGN KEY([Planogram_Id])
	REFERENCES 
		[Content].[Planogram] ([Planogram_Id])
END

/****** [FK_WorkpackagePlanogramProcessingStatus_Workpackage] ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Content].[FK_WorkpackagePlanogramProcessingStatus_Workpackage]') AND parent_object_id = OBJECT_ID(N'[Content].[WorkpackagePlanogramProcessingStatus]'))
BEGIN
	ALTER TABLE 
		[Content].[WorkpackagePlanogramProcessingStatus]  
	WITH CHECK ADD  CONSTRAINT 
		[FK_WorkpackagePlanogramProcessingStatus_Workpackage] FOREIGN KEY([Workpackage_Id])
	REFERENCES 
		[Content].[Workpackage] ([Workpackage_Id])
END