﻿-- Title: CCM800 Scripts
-- Description: A generic script that applies page compression to all objects in the current database
-- Product: CCM v8
-- Company: Galleria RTS Ltd
-- Copyright: Copyright © Galleria RTS Ltd 2015

-- Version History: CCM830
--	CCM-31985 : Neil Foster
--		Created

/***** PARAMETERS *****/

:setvar DatabaseName	"CCM" -- the ccm database name

/***** STEP 1 : ENSURE WE ARE RUNNING AGAINST THE CORRECT DATABASE *****/

USE [$(DatabaseName)]
GO

/***** STEP 2 : APPLY COMPRESSION TO ALL INDEXES *****/

SET NOCOUNT ON;
DECLARE @sql NVARCHAR(MAX);

-- get details about the server
DECLARE @MssqlEdition NVARCHAR(MAX);
SET @MssqlEdition = CONVERT(NVARCHAR(MAX), SERVERPROPERTY ('edition'));

DECLARE @MssqlVersion NVARCHAR(MAX);
SET @MssqlVersion = CONVERT(NVARCHAR(MAX), SERVERPROPERTY('productversion'));

DECLARE @MssqlMajorVersion INT;
SET @MssqlMajorVersion = LEFT(@MssqlVersion, CHARINDEX('.', @MssqlVersion) - 1);

DECLARE @MssqlCompatibility INT;
SET @MssqlCompatibility = @MssqlMajorVersion * 10;

-- page compression only supported in developer/enterprise and sql2008 or later
IF ((@MssqlEdition LIKE '%DEVELOPER%') OR (@MssqlEdition LIKE '%ENTERPRISE%')) AND (@MssqlMajorVersion > 9)
BEGIN

	-- create a temp table to hold all the partitions
	IF OBJECT_ID('tempdb..#partitions') IS NOT NULL DROP TABLE #partitions;
	CREATE TABLE #partitions
	(
		[partition_id] INT IDENTITY(1,1),
		[partition_index] NVARCHAR(MAX),
		[partition_table] NVARCHAR(MAX),
		[partition_number] INT,
		[partition_count] INT
	);
	SET @sql = '
		INSERT INTO #partitions
		(
			partition_index,
			partition_table,
			partition_number,
			partition_count
		)
		SELECT
			QUOTENAME(i.[name]) AS [partition_index],
			QUOTENAME(SCHEMA_NAME(o.schema_id)) + ''.'' + QUOTENAME(OBJECT_NAME(o.object_id)) AS [partition_table],
			p.[partition_number] AS [partition_number],
			(
				SELECT
					MAX(q.[partition_number])
				FROM
					sys.partitions AS q
				WHERE
					q.index_id = p.index_id AND
					q.object_id = p.object_id
			) AS [partition_count]
		FROM
			sys.partitions AS p
			INNER JOIN sys.indexes AS i ON i.object_id = p.object_id AND i.index_id = p.index_id
			INNER JOIN sys.objects AS o ON o.object_id = i.object_id
		WHERE
			o.[type] = ''U'' AND
			i.[type] <> 0 AND
			p.[data_compression] = 0
		ORDER BY
			p.[partition_id] ASC;';
	EXEC sp_executesql @sql;

	-- enumerate all partitions
	DECLARE @partition_id INT;
	SELECT @partition_id = MIN(partition_id) FROM #partitions;
	WHILE @partition_id IS NOT NULL
	BEGIN
		DECLARE @partition_index NVARCHAR(MAX);
		DECLARE @partition_table NVARCHAR(MAX);
		DECLARE @partition_number INT;
		DECLARE @partition_count INT;

		-- get the partition details
		SELECT
			@partition_index = partition_index,
			@partition_table = partition_table,
			@partition_number = partition_number,
			@partition_count = partition_count
		FROM
			#partitions
		WHERE
			partition_id = @partition_id;

		-- rebuild the partition with compression
		IF @partition_number = @partition_count
		BEGIN
			SET @sql = 'ALTER INDEX ' + @partition_index + ' ON ' + @partition_table + ' REBUILD WITH (DATA_COMPRESSION = PAGE);';
		END
		ELSE
		BEGIN	
			SET @sql = 'ALTER INDEX ' + @partition_index + ' ON ' + @partition_table + ' REBUILD PARTITION = ' + CONVERT(NVARCHAR(MAX), @partition_number) + ' WITH (DATA_COMPRESSION = PAGE);';
		END
		PRINT '      ' + @sql;
		EXEC sp_executesql @sql;

		-- enumerate the next partition
		DELETE FROM #partitions WHERE partition_id = @partition_id;
		SELECT @partition_id = MIN(partition_id) FROM #partitions;

	END

	-- drop the partitions table if it exists
	IF OBJECT_ID('tempdb..#partitions') IS NOT NULL DROP TABLE #partitions;

END
GO

/***** STEP 3 : ADD A VALUE TO SYSTEM SETTINGS TO INDICATE THAT COMPRESSION HAS NOW BEEN APPLIED *****/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[System].[SystemSettings]') AND type in (N'U'))
BEGIN
	DECLARE @sql NVARCHAR(MAX);

	SET @sql = '
	IF NOT EXISTS (SELECT * FROM [System].[SystemSettings] WHERE [SystemSettings_Key] = ''DatabaseCompression'')
	BEGIN

		INSERT INTO [System].[SystemSettings]
		(
			[SystemSettings_Key],
			[SystemSettings_Value]
		)
		VALUES
		(
			''DatabaseCompression'',
			''PAGE''
		);

	END'
	EXEC sp_executesql @sql;

END
GO