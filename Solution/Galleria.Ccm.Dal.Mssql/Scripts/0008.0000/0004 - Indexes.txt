﻿-- Title: CCM V8 v8.0 Indexes Scripts
-- Product: CCM v8.3.0
-- Company: Galleria RTS Ltd
-- Copyright: Copyright © Galleria RTS Ltd 2015

-- V8-31550 : A.Probyn
--  Added new AssortmentLocationBuddy, AssortmentProductBuddy, PlanogramAssortmentLocationBuddy, PlanogramAssortmentProductBuddy table IX's
-- V8-31551 : A.Probyn
--  Added new AssortmentInventoryRule, PlanogramAssortmentInventoryRule table IX's
-- V8-31819 : A.Silva
--	Added new PlanogramComparison, PlanogramComparisonField, PlanogramComparisonResult, PlanogramComparisonItem and PlanogramComparisonFieldValue table IX's
-- V8-31970 : A.Silva
--	Removed duplicate and overlapping indexes.
--V8-32361 : L.Ineson
-- Added product library indexes
-- V8-32884 : A.Kuszyk
--	Added sequence sub group tables and metadata.

/****** [IX_AssortmentLocationBuddy_LocationId_AssortmentId] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentLocationBuddy]') AND name = N'IX_AssortmentLocationBuddy_LocationId_AssortmentId')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_AssortmentLocationBuddy_LocationId_AssortmentId] ON [MasterData].[AssortmentLocationBuddy] 
	( 
 		[Location_Id] ASC, 
 		[Assortment_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_AssortmentLocationBuddy_Location] ******/
-- NB This index is covered by [MasterData].[AssortmentLocationBuddy].[IX_AssortmentLocationBuddy_LocationId_AssortmentId] above.
--IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentLocationBuddy]') AND name = N'FK_AssortmentLocationBuddy_Location')
--BEGIN
--	CREATE NONCLUSTERED INDEX [FK_AssortmentLocationBuddy_Location] ON [MasterData].[AssortmentLocationBuddy] 
--	(
--		[Location_Id] ASC
--	) ON [PRIMARY]
--END

/****** [FK_AssortmentLocationBuddy_Assortment] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentLocationBuddy]') AND name = N'FK_AssortmentLocationBuddy_Assortment')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_AssortmentLocationBuddy_Assortment] ON [MasterData].[AssortmentLocationBuddy] 
	(
		[Assortment_Id] ASC
	) ON [PRIMARY]
END


/****** [IX_AssortmentProductBuddy_ProductId_AssortmentId] (Auto-generated) ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentProductBuddy]') AND name = N'IX_AssortmentProductBuddy_ProductId_AssortmentId')
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX [IX_AssortmentProductBuddy_ProductId_AssortmentId] ON [MasterData].[AssortmentProductBuddy] 
	( 
 		[Product_Id] ASC, 
 		[Assortment_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_AssortmentProductBuddy_Product] (Auto-generated) ******/
-- NB This index is covered by [MasterData].[AssortmentProductBuddy].[IX_AssortmentProductBuddy_ProductId_AssortmentId].
--IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentProductBuddy]') AND name = N'FK_AssortmentProductBuddy_Product')
--BEGIN
--	CREATE NONCLUSTERED INDEX [FK_AssortmentProductBuddy_Product] ON [MasterData].[AssortmentProductBuddy] 
--	(
--		[Product_Id] ASC
--	) ON [PRIMARY]
--END


/****** [FK_AssortmentProductBuddy_Assortment] (Auto-generated) ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentProductBuddy]') AND name = N'FK_AssortmentProductBuddy_Assortment')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_AssortmentProductBuddy_Assortment] ON [MasterData].[AssortmentProductBuddy] 
	(
		[Assortment_Id] ASC
	) ON [PRIMARY]
END


/****** [FK_AssortmentInventoryRule_Assortment] (Auto-generated) ******/
-- NB This index is covered by [MasterData].[AssortmentInventoryRule].[IX_AssortmentInventoryRule_AssortmentId_ProductId].
--IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentInventoryRule]') AND name = N'FK_AssortmentInventoryRule_Assortment')
--BEGIN
--	CREATE NONCLUSTERED INDEX [FK_AssortmentInventoryRule_Assortment] ON [MasterData].[AssortmentInventoryRule] 
--	(
--		[Assortment_Id] ASC
--	) ON [PRIMARY]
--END


/****** [IX_AssortmentInventoryRule_AssortmentId_ProductId] (Auto-generated) ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentInventoryRule]') AND name = N'IX_AssortmentInventoryRule_AssortmentId_ProductId')
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX [IX_AssortmentInventoryRule_AssortmentId_ProductId] ON [MasterData].[AssortmentInventoryRule] 
	( 
 		[Assortment_Id] ASC, 
 		[Product_Id] ASC
	) ON [PRIMARY]
END


/****** [FK_AssortmentInventoryRule_Product] (Auto-generated) ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[AssortmentInventoryRule]') AND name = N'FK_AssortmentInventoryRule_Product')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_AssortmentInventoryRule_Product] ON [MasterData].[AssortmentInventoryRule] 
	(
		[Product_Id] ASC
	) ON [PRIMARY]
END


/****** [FK_PlanogramAssortmentLocationBuddy_PlanogramAssortment_Location] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentLocationBuddy]') AND name = N'FK_PlanogramAssortmentLocationBuddy_PlanogramAssortment_Location')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramAssortmentLocationBuddy_PlanogramAssortment_Location] ON [Content].[PlanogramAssortmentLocationBuddy] 
	( 
 		[PlanogramAssortmentLocationBuddy_LocationCode] ASC, 
 		[PlanogramAssortment_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramAssortmentLocationBuddy_PlanogramAssortment] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentLocationBuddy]') AND name = N'FK_PlanogramAssortmentLocationBuddy_PlanogramAssortment')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramAssortmentLocationBuddy_PlanogramAssortment] ON [Content].[PlanogramAssortmentLocationBuddy] 
	(
		[PlanogramAssortment_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramAssortmentLocationBuddy_Location] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentLocalProduct].[FK_PlanogramAssortmentLocalProduct_PlanogramAssortment].
--IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentLocationBuddy]') AND name = N'FK_PlanogramAssortmentLocationBuddy_Location')
--BEGIN
--	CREATE NONCLUSTERED INDEX [FK_PlanogramAssortmentLocationBuddy_Location] ON [Content].[PlanogramAssortmentLocationBuddy] 
--	(
--		[PlanogramAssortmentLocationBuddy_LocationCode] ASC
--	) ON [PRIMARY]
--END

/****** [FK_PlanogramAssortmentProductBuddy_PlanogramAssortment] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentProductBuddy]') AND name = N'FK_PlanogramAssortmentProductBuddy_PlanogramAssortment')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramAssortmentProductBuddy_PlanogramAssortment] ON [Content].[PlanogramAssortmentProductBuddy] 
	(
		[PlanogramAssortment_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramAssortmentProductBuddy_Product] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentProductBuddy].[IX_PlanogramAssortmentProductBuddy_PlanogramAssortment_Product].
--IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentProductBuddy]') AND name = N'FK_PlanogramAssortmentProductBuddy_Product')
--BEGIN
--	CREATE NONCLUSTERED INDEX [FK_PlanogramAssortmentProductBuddy_Product] ON [Content].[PlanogramAssortmentProductBuddy] 
--	(
--		[PlanogramAssortmentProductBuddy_ProductGtin] ASC
--	) ON [PRIMARY]
--END

/****** [IX_PlanogramAssortmentProductBuddy_PlanogramAssortment_Product] (Auto-generated) ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentProductBuddy]') AND name = N'IX_PlanogramAssortmentProductBuddy_PlanogramAssortment_Product')
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PlanogramAssortmentProductBuddy_PlanogramAssortment_Product] ON [Content].[PlanogramAssortmentProductBuddy] 
	( 
 		[PlanogramAssortmentProductBuddy_ProductGtin] ASC, 
 		[PlanogramAssortment_Id] ASC
	) ON [PRIMARY]
END


/****** [FK_PlanogramAssortmentInventoryRule_PlanogramAssortment] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentInventoryRule]') AND name = N'FK_PlanogramAssortmentInventoryRule_PlanogramAssortment')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramAssortmentInventoryRule_PlanogramAssortment] ON [Content].[PlanogramAssortmentInventoryRule] 
	(
		[PlanogramAssortment_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramAssortmentInventoryRule_Product] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentInventoryRule].[IX_PlanogramAssortmentProductBuddy_PlanogramAssortment_Product] above.
--IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentInventoryRule]') AND name = N'FK_PlanogramAssortmentInventoryRule_Product')
--BEGIN
--	CREATE NONCLUSTERED INDEX [FK_PlanogramAssortmentInventoryRule_Product] ON [Content].[PlanogramAssortmentInventoryRule] 
--	(
--		[PlanogramAssortmentInventoryRule_ProductGtin] ASC
--	) ON [PRIMARY]
--END

/****** [IX_PlanogramAssortmentInventoryRule_PlanogramAssortment_Product] (Auto-generated) ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentInventoryRule]') AND name = N'IX_PlanogramAssortmentInventoryRule_PlanogramAssortment_Product')
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PlanogramAssortmentInventoryRule_PlanogramAssortment_Product] ON [Content].[PlanogramAssortmentInventoryRule] 
	( 
 		[PlanogramAssortmentInventoryRule_ProductGtin] ASC, 
 		[PlanogramAssortment_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramComparison_Planogram] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramComparison]') AND name = N'FK_PlanogramComparison_Planogram')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramComparison_Planogram] ON [Content].[PlanogramComparison] 
	(
		[Planogram_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramComparisonField_PlanogramComparison] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramComparisonField]') AND name = N'FK_PlanogramComparisonField_PlanogramComparison')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramComparisonField_PlanogramComparison] ON [Content].[PlanogramComparisonField] 
	(
		[PlanogramComparison_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramComparisonResult_PlanogramComparison] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramComparisonResult]') AND name = N'FK_PlanogramComparisonResult_PlanogramComparison')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramComparisonResult_PlanogramComparison] ON [Content].[PlanogramComparisonResult] 
	(
		[PlanogramComparison_Id] ASC
	) ON [PRIMARY]
END

/****** [IX_PlanogramComparisonResult_PlanogramComparison_PlanogramName] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramComparisonResult]') AND name = N'IX_PlanogramComparisonResult_PlanogramComparison_PlanogramName')
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PlanogramComparisonResult_PlanogramComparison_PlanogramName] ON [Content].[PlanogramComparisonResult] 
	( 
 		[PlanogramComparisonResult_PlanogramName] ASC, 
 		[PlanogramComparison_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramComparisonItem_PlanogramComparisonResult] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramComparisonItem]') AND name = N'FK_PlanogramComparisonItem_PlanogramComparisonResult')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramComparisonItem_PlanogramComparisonResult] ON [Content].[PlanogramComparisonItem] 
	(
		[PlanogramComparisonResult_Id] ASC
	) ON [PRIMARY]
END

/****** [IX_PlanogramComparisonItem_PlanogramComparisonResult_ItemId_ItemType] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramComparisonItem]') AND name = N'IX_PlanogramComparisonItem_PlanogramComparisonResult_ItemId_ItemType')
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PlanogramComparisonItem_PlanogramComparisonResult_ItemId_ItemType] ON [Content].[PlanogramComparisonItem] 
	( 
 		[PlanogramComparisonItem_ItemId] ASC, 
 		[PlanogramComparisonItem_ItemType] ASC, 
 		[PlanogramComparisonResult_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramComparisonFieldValue_PlanogramComparisonItem] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramComparisonFieldValue]') AND name = N'FK_PlanogramComparisonFieldValue_PlanogramComparisonItem')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramComparisonFieldValue_PlanogramComparisonItem] ON [Content].[PlanogramComparisonFieldValue] 
	(
		[PlanogramComparisonItem_Id] ASC
	) ON [PRIMARY]
END

/****** [IX_LocationHierarchy_EntityId] ******/
-- NB This index is duplicated by [MasterData].[LocationHierarchy].[FK_LocationHierarchy_Entity].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[LocationHierarchy]') AND name = N'IX_LocationHierarchy_EntityId')
BEGIN
	DROP INDEX IX_LocationHierarchy_EntityId ON MasterData.LocationHierarchy
END

/****** [IX_PlanogramHierarchy_EntityId] ******/
-- NB This index is duplicated by [MasterData].[PlanogramHierarchy].[FK_PlanogramHierarchy_Entity].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PlanogramHierarchy]') AND name = N'IX_PlanogramHierarchy_EntityId')
BEGIN
	DROP INDEX IX_PlanogramHierarchy_EntityId ON MasterData.PlanogramHierarchy
END

/****** [IX_PrintTemplate_Entity] ******/
-- NB This index is duplicated by [MasterData].[PrintTemplate].[FK_PrintTemplate_Entity].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PrintTemplate]') AND name = N'IX_PrintTemplate_Entity')
BEGIN
	DROP INDEX IX_PrintTemplate_Entity ON MasterData.PrintTemplate
END

/****** [IX_PrintTemplateComponent_PrintTemplateSection] ******/
-- NB This index is duplicated by [MasterData].[PrintTemplateComponent].[FK_PrintTemplateComponent_PrintTemplateSection].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PrintTemplateComponent]') AND name = N'IX_PrintTemplateComponent_PrintTemplateSection')
BEGIN
	DROP INDEX IX_PrintTemplateComponent_PrintTemplateSection ON MasterData.PrintTemplateComponent
END

/****** [IX_PrintTemplateComponentDetail_PrintTemplateComponent] ******/
-- NB This index is duplicated by [MasterData].[PrintTemplateComponentDetail].[FK_PrintTemplateComponentDetail_PrintTemplateComponent].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PrintTemplateComponentDetail]') AND name = N'IX_PrintTemplateComponentDetail_PrintTemplateComponent')
BEGIN
	DROP INDEX IX_PrintTemplateComponentDetail_PrintTemplateComponent ON MasterData.PrintTemplateComponentDetail
END

/****** [IX_PrintTemplateSectionGroup_PrintTemplate] ******/
-- NB This index is duplicated by [MasterData].[PrintTemplateSectionGroup].[FK_PrintTemplateSectionGroup_PrintTemplate].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PrintTemplateSectionGroup]') AND name = N'IX_PrintTemplateSectionGroup_PrintTemplate')
BEGIN
	DROP INDEX IX_PrintTemplateSectionGroup_PrintTemplate ON MasterData.PrintTemplateSectionGroup
END

/****** [IX_ProductHierarchy_EntityId] ******/
-- NB This index is duplicated by [MasterData].[ProductHierarchy].[FK_ProductHierarchy_Entity].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ProductHierarchy]') AND name = N'IX_ProductHierarchy_EntityId')
BEGIN
	DROP INDEX IX_ProductHierarchy_EntityId ON MasterData.ProductHierarchy
END

/****** [IX_SystemSettings_EntityId] ******/
-- NB This index is duplicated by [System].[SystemSettings].[FK_SystemSettings_Entity].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[System].[SystemSettings]') AND name = N'IX_SystemSettings_EntityId')
BEGIN
	DROP INDEX IX_SystemSettings_EntityId ON System.SystemSettings
END

/****** [FK_PlanogramAssortmentProduct_PlanogramAssortment] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentProduct].[IX_PlanogramAssortmentProduct_PlanogramAssortment_Gtin].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentProduct]') AND name = N'FK_PlanogramAssortmentProduct_PlanogramAssortment')
BEGIN
	DROP INDEX FK_PlanogramAssortmentProduct_PlanogramAssortment ON Content.PlanogramAssortmentProduct
END

/****** [FK_PlanogramAssortmentLocalProduct_PlanogramAssortment] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentLocalProduct].[FK_PlanogramAssortmentLocalProduct_PlanogramAssortment].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentLocalProduct]') AND name = N'FK_PlanogramAssortmentLocalProduct_PlanogramAssortment')
BEGIN
	DROP INDEX FK_PlanogramAssortmentLocalProduct_PlanogramAssortment ON Content.PlanogramAssortmentLocalProduct
END

/****** [FK_PlanogramAssortmentRegion_PlanogramAssortment] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentRegion].[IX_PlanogramAssortmentRegion_PlanogramAssortment_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentRegion]') AND name = N'FK_PlanogramAssortmentRegion_PlanogramAssortment')
BEGIN
	DROP INDEX FK_PlanogramAssortmentRegion_PlanogramAssortment ON Content.PlanogramAssortmentRegion
END

/****** [FK_PlanogramAssortmentRegionLocation_PlanogramAssortmentRegion] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentRegionLocation].[IX_PlanogramAssortmentRegionLocation_PlanogramAssortmentRegion_LocationCode].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentRegionLocation]') AND name = N'FK_PlanogramAssortmentRegionLocation_PlanogramAssortmentRegion')
BEGIN
	DROP INDEX FK_PlanogramAssortmentRegionLocation_PlanogramAssortmentRegion ON Content.PlanogramAssortmentRegionLocation
END

/****** [FK_PlanogramAssortmentRegionProduct_PlanogramAssortmentRegion] ******/
-- NB This index is covered by [Content].[PlanogramAssortmentRegionProduct].[IX_PlanogramAssortmentRegionProduct_PlanogramAssortmentRegion_PrimaryProductGtin].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramAssortmentRegionProduct]') AND name = N'FK_PlanogramAssortmentRegionProduct_PlanogramAssortmentRegion')
BEGIN
	DROP INDEX FK_PlanogramAssortmentRegionProduct_PlanogramAssortmentRegion ON Content.PlanogramAssortmentRegionProduct
END

/****** [FK_PlanogramConsumerDecisionTreeLevel_PlanogramConsumerDecisionTree] ******/
-- NB This index is covered by [Content].[PlanogramConsumerDecisionTreeLevel].[IX_PlanogramConsumerDecisionTreeLevel_PlanogramConsumerDecisionTreeId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramConsumerDecisionTreeLevel]') AND name = N'FK_PlanogramConsumerDecisionTreeLevel_PlanogramConsumerDecisionTree')
BEGIN
	DROP INDEX FK_PlanogramConsumerDecisionTreeLevel_PlanogramConsumerDecisionTree ON Content.PlanogramConsumerDecisionTreeLevel
END

/****** [FK_PlanogramConsumerDecisionTreeNode_PlanogramConsumerDecisionTree] ******/
-- NB This index is covered by [Content].[PlanogramConsumerDecisionTreeNode].[IX_PlanogramConsumerDecisionTreeNode_PlanogramConsumerDecisionTreeId_PlanogramConsumerDecisionTreeLevelId_Name_ParentNodeId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramConsumerDecisionTreeNode]') AND name = N'FK_PlanogramConsumerDecisionTreeNode_PlanogramConsumerDecisionTree')
BEGIN
	DROP INDEX FK_PlanogramConsumerDecisionTreeNode_PlanogramConsumerDecisionTree ON Content.PlanogramConsumerDecisionTreeNode
END

/****** [FK_PlanogramConsumerDecisionTreeNodeProduct_PlanogramConsumerDecisionTreeNode] ******/
-- NB This index is covered by [Content].[PlanogramConsumerDecisionTreeNodeProduct].[IX_PlanogramConsumerDecisionTreeNodeProduct_PlanogramConsumerDecisionTreeNodeId_PlanogramProductId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramConsumerDecisionTreeNodeProduct]') AND name = N'FK_PlanogramConsumerDecisionTreeNodeProduct_PlanogramConsumerDecisionTreeNode')
BEGIN
	DROP INDEX FK_PlanogramConsumerDecisionTreeNodeProduct_PlanogramConsumerDecisionTreeNode ON Content.PlanogramConsumerDecisionTreeNodeProduct
END

/****** [FK_PlanogramGroupPlanogram_PlanogramGroup] ******/
-- NB This index is covered by [Content].[PlanogramGroupPlanogram].[PK_PlanogramGroupPlanogram].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramGroupPlanogram]') AND name = N'FK_PlanogramGroupPlanogram_PlanogramGroup')
BEGIN
	DROP INDEX FK_PlanogramGroupPlanogram_PlanogramGroup ON Content.PlanogramGroupPlanogram
END

/****** [FK_PlanogramPerformanceMetric_PlanogramPerformance] ******/
-- NB This index is covered by [Content].[PlanogramPerformanceMetric].[IX_PlanogramPerformanceMetric_PlanogramPerformanceId_MetricId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramPerformanceMetric]') AND name = N'FK_PlanogramPerformanceMetric_PlanogramPerformance')
BEGIN
	DROP INDEX FK_PlanogramPerformanceMetric_PlanogramPerformance ON Content.PlanogramPerformanceMetric
END

/****** [FK_PlanogramRenumberingStrategy_Planogram] ******/
-- NB This index is covered by [Content].[PlanogramRenumberingStrategy].[IX_PlanogramRenumberingStrategy_PlanogramId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramRenumberingStrategy]') AND name = N'FK_PlanogramRenumberingStrategy_Planogram')
BEGIN
	DROP INDEX FK_PlanogramRenumberingStrategy_Planogram ON Content.PlanogramRenumberingStrategy
END

/****** [FK_PlanogramValidationTemplate_Planogram] ******/
-- NB This index is covered by [Content].[PlanogramValidationTemplate].[IX_PlanogramValidationTemplate_PlanogramId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramValidationTemplate]') AND name = N'FK_PlanogramValidationTemplate_Planogram')
BEGIN
	DROP INDEX FK_PlanogramValidationTemplate_Planogram ON Content.PlanogramValidationTemplate
END

/****** [FK_PlanogramValidationTemplateGroup_PlanogramValidationTemplate] ******/
-- NB This index is covered by [Content].[PlanogramValidationTemplateGroup].[IX_PlanogramValidationTemplateGroup_PlanogramValidationTemplateId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramValidationTemplateGroup]') AND name = N'FK_PlanogramValidationTemplateGroup_PlanogramValidationTemplate')
BEGIN
	DROP INDEX FK_PlanogramValidationTemplateGroup_PlanogramValidationTemplate ON Content.PlanogramValidationTemplateGroup
END

/****** [FK_PlanogramValidationTemplateGroupMetric_PlanogramValidationTemplateGroup] ******/
-- NB This index is covered by [Content].[PlanogramValidationTemplateGroupMetric].[IX_PlanogramValidationTemplateGroupMetric_PlanogramValidationTemplateGroupId_Field_AggregationType].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramValidationTemplateGroupMetric]') AND name = N'FK_PlanogramValidationTemplateGroupMetric_PlanogramValidationTemplateGroup')
BEGIN
	DROP INDEX FK_PlanogramValidationTemplateGroupMetric_PlanogramValidationTemplateGroup ON Content.PlanogramValidationTemplateGroupMetric
END

/****** [FK_Workflow_Entity] ******/
-- NB This index is covered by [Content].[Workflow].[IX_Workflow_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[Workflow]') AND name = N'FK_Workflow_Entity')
BEGIN
	DROP INDEX FK_Workflow_Entity ON Content.Workflow
END

/****** [FK_WorkpackagePlanogramDebug_SourcePlanogramId] ******/
-- NB This index is covered by [Content].[WorkpackagePlanogramDebug].[PK_WorkpackagePlanogramDebug].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[WorkpackagePlanogramDebug]') AND name = N'FK_WorkpackagePlanogramDebug_SourcePlanogramId')
BEGIN
	DROP INDEX FK_WorkpackagePlanogramDebug_SourcePlanogramId ON Content.WorkpackagePlanogramDebug
END

/****** [FK_Blocking_Entity] ******/
-- NB This index is covered by [MasterData].[Blocking].[IX_Blocking_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[Blocking]') AND name = N'FK_Blocking_Entity')
BEGIN
	DROP INDEX FK_Blocking_Entity ON MasterData.Blocking
END

/****** [FK_BlockingGroup_Blocking] ******/
-- NB This index is covered by [MasterData].[BlockingGroup].[IX_BlockingGroup_BlockingId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[BlockingGroup]') AND name = N'FK_BlockingGroup_Blocking')
BEGIN
	DROP INDEX FK_BlockingGroup_Blocking ON MasterData.BlockingGroup
END

/****** [FK_BlockingGroupProduct_BlockingGroup] ******/
-- NB This index is covered by [MasterData].[BlockingGroupProduct].[IX_BlockingGroupProduct_BlockingGroupId_ProductId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[BlockingGroupProduct]') AND name = N'FK_BlockingGroupProduct_BlockingGroup')
BEGIN
	DROP INDEX FK_BlockingGroupProduct_BlockingGroup ON MasterData.BlockingGroupProduct
END

/****** [FK_ClusterLocation_Cluster] ******/
-- NB This index is covered by [MasterData].[ClusterLocation].[IX_ClusterLocation_ClusterId_LocationId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ClusterLocation]') AND name = N'FK_ClusterLocation_Cluster')
BEGIN
	DROP INDEX FK_ClusterLocation_Cluster ON MasterData.ClusterLocation
END

/****** [FK_ConsumerDecisionTreeLevel_ConsumerDecisionTree] ******/
-- NB This index is covered by [MasterData].[ConsumerDecisionTreeLevel].[IX_ConsumerDecisionTreeLevel_ConsumerDecisionTreeId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ConsumerDecisionTreeLevel]') AND name = N'FK_ConsumerDecisionTreeLevel_ConsumerDecisionTree')
BEGIN
	DROP INDEX FK_ConsumerDecisionTreeLevel_ConsumerDecisionTree ON MasterData.ConsumerDecisionTreeLevel
END

/****** [FK_ConsumerDecisionTreeNode_ConsumerDecisionTree] ******/
-- NB This index is covered by [MasterData].[ConsumerDecisionTreeNode].[IX_ConsumerDecisionTreeNode_ConsumerDecisionTreeId_ConsumerDecisionTreeLevelId_Name_ParentNodeId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ConsumerDecisionTreeNode]') AND name = N'FK_ConsumerDecisionTreeNode_ConsumerDecisionTree')
BEGIN
	DROP INDEX FK_ConsumerDecisionTreeNode_ConsumerDecisionTree ON MasterData.ConsumerDecisionTreeNode
END

/****** [FK_ConsumerDecisionTreeNodeProduct_ConsumerDecisionTreeNode] ******/
-- NB This index is covered by [MasterData].[ConsumerDecisionTreeNodeProduct].[IX_ConsumerDecisionTreeNodeProduct_ConsumerDecisionTreeNodeId_ProductId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ConsumerDecisionTreeNodeProduct]') AND name = N'FK_ConsumerDecisionTreeNodeProduct_ConsumerDecisionTreeNode')
BEGIN
	DROP INDEX FK_ConsumerDecisionTreeNodeProduct_ConsumerDecisionTreeNode ON MasterData.ConsumerDecisionTreeNodeProduct
END

/****** [FK_InventoryProfile_Entity] ******/
-- NB This index is covered by [MasterData].[InventoryProfile].[IX_InventoryProfile_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[InventoryProfile]') AND name = N'FK_InventoryProfile_Entity')
BEGIN
	DROP INDEX FK_InventoryProfile_Entity ON MasterData.InventoryProfile
END

/****** [FK_Label_Entity] ******/
-- NB This index is covered by [MasterData].[Label].[IX_Label_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[Label]') AND name = N'FK_Label_Entity')
BEGIN
	DROP INDEX FK_Label_Entity ON MasterData.Label
END

/****** [FK_Location_Entity] ******/
-- NB This index is covered by [MasterData].[Location].[IX_Location_Code].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[Location]') AND name = N'FK_Location_Entity')
BEGIN
	DROP INDEX FK_Location_Entity ON MasterData.Location
END

/****** [FK_LocationGroup_LocationLevel] ******/
-- NB This index is covered by [MasterData].[LocationGroup].[IX_LocationGroup_LocationLevelId_Code].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[LocationGroup]') AND name = N'FK_LocationGroup_LocationLevel')
BEGIN
	DROP INDEX FK_LocationGroup_LocationLevel ON MasterData.LocationGroup
END

/****** [FK_LocationLevel_LocationHierarchy] ******/
-- NB This index is covered by [MasterData].[LocationLevel].[IX_LocationLevel_LocationHierarchyId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[LocationLevel]') AND name = N'FK_LocationLevel_LocationHierarchy')
BEGIN
	DROP INDEX FK_LocationLevel_LocationHierarchy ON MasterData.LocationLevel
END

/****** [FK_LocationSpace_Entity] ******/
-- NB This index is covered by [MasterData].[LocationSpace].[IX_LocationSpace_LocationId_EntityId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[LocationSpace]') AND name = N'FK_LocationSpace_Entity')
BEGIN
	DROP INDEX FK_LocationSpace_Entity ON MasterData.LocationSpace
END

/****** [FK_MetricProfile_Entity] ******/
-- NB This index is covered by [MasterData].[MetricProfile].[IX_MetricProfile_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[MetricProfile]') AND name = N'FK_MetricProfile_Entity')
BEGIN
	DROP INDEX FK_MetricProfile_Entity ON MasterData.MetricProfile
END

/****** [FK_PerformanceSelection_Entity] ******/
-- NB This index is covered by [MasterData].[PerformanceSelection].[IX_PerformanceSelection_EntityId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PerformanceSelection]') AND name = N'FK_PerformanceSelection_Entity')
BEGIN
	DROP INDEX FK_PerformanceSelection_Entity ON MasterData.PerformanceSelection
END

/****** [FK_PlanogramImportTemplate_Entity] ******/
-- NB This index is covered by [MasterData].[PlanogramImportTemplate].[IX_PlanogramImportTemplate_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PlanogramImportTemplate]') AND name = N'FK_PlanogramImportTemplate_Entity')
BEGIN
	DROP INDEX FK_PlanogramImportTemplate_Entity ON MasterData.PlanogramImportTemplate
END

/****** [FK_PlanogramImportTemplateMapping_PlanogramImportTemplate] ******/
-- NB This index is covered by [MasterData].[PlanogramImportTemplateMapping].[IX_PlanogramImportTemplateMapping_PlanogramImportTemplateId_Field_FieldType].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PlanogramImportTemplateMapping]') AND name = N'FK_PlanogramImportTemplateMapping_PlanogramImportTemplate')
BEGIN
	DROP INDEX FK_PlanogramImportTemplateMapping_PlanogramImportTemplate ON MasterData.PlanogramImportTemplateMapping
END

/****** [FK_PlanogramImportTemplatePerformanceMetric_PlanogramImportTemplate] ******/
-- NB This index is covered by [MasterData].[PlanogramImportTemplatePerformanceMetric].[IX_PlanogramImportTemplatePerformanceMetric_PlanogramImportTemplatePerformanceMetric_MetricId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PlanogramImportTemplatePerformanceMetric]') AND name = N'FK_PlanogramImportTemplatePerformanceMetric_PlanogramImportTemplate')
BEGIN
	DROP INDEX FK_PlanogramImportTemplatePerformanceMetric_PlanogramImportTemplate ON MasterData.PlanogramImportTemplatePerformanceMetric
END

/****** [FK_PlanogramNameTemplate_Entity] ******/
-- NB This index is covered by [MasterData].[PlanogramNameTemplate].[IX_PlanogramNameTemplate_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PlanogramNameTemplate]') AND name = N'FK_PlanogramNameTemplate_Entity')
BEGIN
	DROP INDEX FK_PlanogramNameTemplate_Entity ON MasterData.PlanogramNameTemplate
END

/****** [FK_PrintTemplate_Entity] ******/
-- NB This index is covered by [MasterData].[PrintTemplate].[IX_PrintTemplate_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[PrintTemplate]') AND name = N'FK_PrintTemplate_Entity')
BEGIN
	DROP INDEX FK_PrintTemplate_Entity ON MasterData.PrintTemplate
END

/****** [FK_ProductGroup_ProductLevel] ******/
-- NB This index is covered by [MasterData].[ProductGroup].[IX_ProductGroup_ProductLevelId_Code].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ProductGroup]') AND name = N'FK_ProductGroup_ProductLevel')
BEGIN
	DROP INDEX FK_ProductGroup_ProductLevel ON MasterData.ProductGroup
END

/****** [FK_ProductLevel_ProductHierarchy] ******/
-- NB This index is covered by [MasterData].[ProductLevel].[IX_ProductLevel_ProductHierarchyId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ProductLevel]') AND name = N'FK_ProductLevel_ProductHierarchy')
BEGIN
	DROP INDEX FK_ProductLevel_ProductHierarchy ON MasterData.ProductLevel
END

/****** [FK_ProductUniverseProduct_ProductUniverse] ******/
-- NB This index is covered by [MasterData].[ProductUniverseProduct].[IX_ProductUniverseProduct_ProductUniverseId_ProductId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ProductUniverseProduct]') AND name = N'FK_ProductUniverseProduct_ProductUniverse')
BEGIN
	DROP INDEX FK_ProductUniverseProduct_ProductUniverse ON MasterData.ProductUniverseProduct
END

/****** [FK_RenumberingStrategy_Entity] ******/
-- NB This index is covered by [MasterData].[RenumberingStrategy].[IX_RenumberingStrategy_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[RenumberingStrategy]') AND name = N'FK_RenumberingStrategy_Entity')
BEGIN
	DROP INDEX FK_RenumberingStrategy_Entity ON MasterData.RenumberingStrategy
END

/****** [FK_ValidationTemplate_Entity] ******/
-- NB This index is covered by [MasterData].[ValidationTemplate].[IX_ValidationTemplate_EntityId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ValidationTemplate]') AND name = N'FK_ValidationTemplate_Entity')
BEGIN
	DROP INDEX FK_ValidationTemplate_Entity ON MasterData.ValidationTemplate
END

/****** [FK_ValidationTemplateGroup_ValidationTemplate] ******/
-- NB This index is covered by [MasterData].[ValidationTemplateGroup].[IX_ValidationTemplateGroup_ValidationTemplateId_Name].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ValidationTemplateGroup]') AND name = N'FK_ValidationTemplateGroup_ValidationTemplate')
BEGIN
	DROP INDEX FK_ValidationTemplateGroup_ValidationTemplate ON MasterData.ValidationTemplateGroup
END

/****** [FK_ValidationTemplateGroupMetric_ValidationTemplateGroup] ******/
-- NB This index is covered by [MasterData].[ValidationTemplateGroupMetric].[IX_ValidationTemplateGroupMetric_ValidationTemplateGroupId_Field_AggregationType].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ValidationTemplateGroupMetric]') AND name = N'FK_ValidationTemplateGroupMetric_ValidationTemplateGroup')
BEGIN
	DROP INDEX FK_ValidationTemplateGroupMetric_ValidationTemplateGroup ON MasterData.ValidationTemplateGroupMetric
END

/****** [FK_RoleMember_User] ******/
-- NB This index is covered by [System].[RoleMember].[IX_RoleMember_UserRole].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[System].[RoleMember]') AND name = N'FK_RoleMember_User')
BEGIN
	DROP INDEX FK_RoleMember_User ON System.RoleMember
END

/****** [FK_RoleMember_User] ******/
-- NB This index is covered by [System].[UserPlanogramGroup].[IX_UserPlanogramGroup_UserId_PlanogramGroupId].
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[System].[UserPlanogramGroup]') AND name = N'FK_UserPlanogramGroup_User')
BEGIN
	DROP INDEX FK_UserPlanogramGroup_User ON System.UserPlanogramGroup
END

/****** [IX_ProductLibrary_Name_EntityId] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[MasterData].[ProductLibrary]') AND name = N'IX_ProductLibrary_Name_EntityId')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_ProductLibrary_Name_EntityId] ON [MasterData].[ProductLibrary] 
	( 
 		[ProductLibrary_Name] ASC, 
 		[Entity_Id] ASC
	) ON [PRIMARY]
END

/****** [FK_PlanogramSequenceGroupSubGroup_PlanogramSequenceGroup] ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Content].[PlanogramSequenceGroupSubGroup]') AND name = N'FK_PlanogramSequenceGroupSubGroup_PlanogramSequenceGroup')
BEGIN
	CREATE NONCLUSTERED INDEX [FK_PlanogramSequenceGroupSubGroup_PlanogramSequenceGroup] ON [Content].[PlanogramSequenceGroupSubGroup] 
	(
		[PlanogramSequenceGroup_Id] ASC
	) ON [PRIMARY]
END