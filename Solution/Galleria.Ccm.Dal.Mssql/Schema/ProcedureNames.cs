﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25476 : A. Kuszyk
//      Initial version. Adapted from GFS.
// CCM-25450 : L.Hodson
//	Added ProductGroup, ProductHierarchy and Product Level    
// CCM-25559 : L.Hodson
//  Added entity
// CCM-25587 : L.Hodson
//  Added PlanogramHierarchy and PlanogramGroup
// V8-25445 : L.Ineson
// Added LocationGroup, LocationHierarchy and Location Level
// V8-25619 : A.Kuszyk
//  Added Location, LocationInfo, ClusterScheme, Cluster, ClusterLocation, ClusterSchemeInfo, 
//  ProductInfo, ConsumerDecisionTree (and related procedures).
// V8-25546 : L.Ineson
//  Added Workflow, WorkflowTask, WorkflowTaskParameter,
//      Workpackage, PlanogramInfo
// v8-25444 : N.Haywood
//      Added LocationGroupInfoFetchByLocationGroupIds
// CCM-25444 : N.Haywood
//      Added FetchLocationGroupInfo
// CCM-25449 : N.Haywood
//  Added cluster locations and location attribute
// CCM-25242 : N.Haywood
//  Added fetch methods for product imports
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link and Added Entity_Id link to Workpackage
// CCM-25631 : N.Haywood
//  Added location space procedures
// V8-25453 : A.Kuszyk
//  Added Product FetchByEntityIdProductIds, CDT FetchByEntityIdConsumerDecisionTreeIds.
// CCM-25446 : N.Haywood
//  Added location product attributes
//CCM-25460 : L.Ineson
//  Added PlanogramInfoFetchByIds
// V8-25788 : M.Pettit
//  Added Report, ReportInfo, ReportComponent, ReportComponentDetail, ReportCondition,
//  Added ReportField, ReportFormulaField, ReportGroup, ReportGroupField, ReportParameter,
//  Added ReportPredicate,ReportSection procedures
// V8-25886 : L.Ineson
//  Added WorkflowTaskParameterValue & WorkpackagePlanogramParameterValue
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// V8-25454 : J.Pickup
//  Added Assortment, AssortmentFile, AssortmentLocalProduct, AssortmentLocation, AssortmentProduct, AssortmentRegion, 
//  AssortmentRegionLocation, AssortmentRegionProduct, Blob, File
// V8-25919 : L.Ineson
//  Added WorkflowTaskInfo
// V8-25556 : D.Pleasance
//  Added EntityFetchAll, LocationFetchByEntityIdIncludingDeleted, ProductFetchByEntityIdProductGtins
// V8-25664 : A.Kuszyk
//  Added User and UserPlanogramGroup.
// V8-25455 : J.Pickup
//  Added AssortmentMinorRevision, AssortmentMinorRevisionInfo, AssortmentMinorRevisionAmendDistributionAction, 
//  AssortmentMinorRevisionAmendDistributionActionLocation, AssortmentMinorRevisionDeListAction, 
//  AssortmentMinorRevisionDeListActionLocation, AssortmentMinorRevisionListAction, 
//  AssortmentMinorRevisionListActionLocation, AssortmentMinorRevisionReplaceAction, 
//  AssortmentMinorRevisionReplaceActionLocation, AssortmentInfo_FetchByEntityIdAssortmentIds
// V8-25881 : A.Probyn
//	Correct AssortmentMinorRevisionInfoFetchByEntityIdName  to include prefix. Unit test scripts were failing
// CCM-25879 : N.Haywood
//  Added LocationPlanogramAssignment and LocationPlanogramAssignmentLocation
// V8-26041 : A.Kuszyk
//  Added Compression, Image and ProductImage procedure names.
// V8-26147 : L.Ineson
// Added CustomAttributeData
// V8-26222 : L.Luong
//  Added UserInfo
// V8-26158 : I.George
// Added Metric Procedure Names
// V8-26270 : A.Kuszyk
//  Added PlanogramPerformance, PlanogramPerformanceMetric and PlanogramPerformanceData.
// V8-26277 : N.Foster
//  Removed references to reporting
// V8-26306 : L.Ineson
//  Added SystemSettings
// V8-26159 : L.Ineson
//  Added PerformanceSelection & PerformanceSelectionTimelineGroup
// V8-26426 : A.Kuszyk
//  Added PlanogramAssortment and related tables.
// V8-26474 : A.Silva ~ Added ValidationTemplate and related tables.
// V8-26123 : L.Ineson
//  Added MetricProfile
// V8-26573 : L.Luong
//  Added PlanogramValidationTemplate and related tables
// V8-26560 : A.Probyn
//  Added related procedure names for Sequence, SequenceInfo, SequenceProduct, SequenceProperty
//  Added ProductUniverseInfo_FetchById
// V8-26140 : I.George
// Added the ClusterSchemeInfoFetchByEntityIdIncludingDeleted
// V8-24761 : A.Silva ~ Added ValidationTemplateInfo FetchByEntityId and FetchByEntityIdIncludingDeleted.
// V8-26520 : J.Pickup
//  Added Content Lookup stored Procedures, PlanogramInfoFetchByCategoryCode, ProductUniverseInfo_FetchByProductGroupId, PlanogramInfo_FetchByNullCategoryCode
// V8-26140 : I.George
//  Added AssortmentInfoFetchByEntityIdIncludingDeleted
// V8-26773 : N.Foster
//  Added PerformanceSelection_FetchByName
// V8-26773 : N.Foster
//  Added WorkpackagePerformance, WorkpackagePerformanceData
// V8-26706 : L.Luong
//  Added PlanogramConsumerDecisionTree and related tables
// V8-26836 : L.Luong
//  Added PlanogramEventLog
// V8-26911 : L.Luong
//  Added EventLog and related tables
// V8-26953 : I.George
//  Added PerformanceSelectionMetric
// V8-27004 : A.Silva
//      Added PlanogramValidationInfo, PlanogramValidationGroupInfo and PlanogramValidationMetricInfo procedure names.
// CCM-26099 :I.George
//  Added FetchByMerchandisingGroupId procedure name
// V8-27132 : A.Kuszyk
//  Added ConsumerDecisionTree.FetchByEntityIdName.
// V8-27241 & 27269 : A.Kuszyk
//  Added FetchByEntityIdName to Blocking, Assortment and ValidationTemplate.
// V8-27153 : A.Silva
//  Added RenumberingStrategy related procedures.
//  Added PlanogramRenumberingStrategy related procedures.
// V8-27404 : L.Luong
//  Changed EventLogFetchByEventLogNameIdLevelIdEntityId to EventLogFetchByEventLogNameIdEntryTypeEntityId
// V8-27411 : M.Pettit
//  Added PlanogramLock table procedures
// V8-27562 : A.Silva
//  Added FetchByEntityIdName procedure name.
// V8-27411 : N.Foster
//  Added Engine Instance procedures
// V8-27647 : L.Luong
//  Added PlanogramInventory procedures
// V8-27918 : J.Pcikup
//  Added FetchByWorkpackageIdIncludingDeleted
// V8-27411 : M.Pettit
//  Added EngineInstanceFetchAll
// V8-27940 - L.Luong
//  Added Label procedures
// V8-27491 - J.Pickup
//  Added all Highlight related procedures i.e group, filter, characteristic, rule, and info.
// V8-27548 : A.Kuszyk
//  Added MetricProfile.FetchByEntityIdName.
// V8-27939 : L.Luong
//  Added PlanogramImportTemplate and related procedures
// V8-28234 : A.Probyn
//  Added DataInfoFetchByEntityId
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added CustomAttributeDataBulkInsert
// V8-28622 : D.Pleasance
//  Added PlanogramImportTemplatePerformanceMetric
// V8-28258 : N.Foster
//  Added Engine.Parameter stored procedures
// V8-28507 : D.Pleasance
//  Added PlanogramInfoFetchByWorkpackageIdPagingCriteria \ PlanogramInfoFetchByWorkpackageIdAutomationProcessingStatusPagingCriteria
// V8-28859 : N.Foster
//  Added Engine.Message_Exists procedure
#endregion
#region Version History: CCM802

// V8-29010 : D.Pleasance
//  Added PlanogramNameTemplate
// V8-28996 : A.Silva
//      Added PlanogramSequence, PlanogramSequenceGroup and PlanogramSequenceGroupProduct related procedures.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-29026 : D.Pleasance
//  Added LocationSpaceProductGroupInfo
// V8-29078 : A.Kuszyk
//  Added ProductGroupInfoFetchByProductGroupCodes.
// V8-29255 : L.Ineson
//  Removed Sequence tables
#endregion
#region Version History: CCM803
// V8-29134  : D.Pleasance
//  Renamed AssortmentFetchAllIncludingDeleted to AssortmentFetchAll as we now deleted outright
//  Removed AssortmentInfoFetchByEntityIdIncludingDeleted
// V8-29214 : D.Pleasance
//  Renamed AssortmentMinorRevisionFetchAllIncludingDeleted to AssortmentMinorRevisionFetchAll as we now deleted outright
//  Removed AssortmentMinorRevisionInfoFetchDeletedByEntityId
// V8-29216 : D.Pleasance
//  Removed FetchByEntityIdIncludingDeleted
// V8-29491 : D.Pleasance
//  Added WorkpackagePerformanceDataFetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId
// V8-29606 : N.Foster
//  Removed PlanogramInfo_FetchByMetadataCalculationRequired and PlanogramInfo_FetchByValidationCalculationRequired
//  Added Engine Maintenance stored procedures
// V8-29590 : A.Probyn
//  Added new EventLog_FetchByWorkpackageId
#endregion
#region Version History: CCM810
// V8-29746 : A.Probyn
//  Added ContentLookupFetchByPlanogramId
// V8-29861 : A.Probyn
//  Added new PlanogramEventLogInfoFetchByWorkpackageId
// V8-29986 : A.Probyn
//  Added new PlanogramInfoFetchNonDebugByCategoryCode
// V8-30213 : L.Luong
//  Added PlanogramInfoFetchNonDebugByLocationCode
#endregion
#region Version History: CCM811
// V8-30155 : L.Ineson
//  Added ContentLookupFetchByPlanogramIds
#endregion
#region Version History: CCM820
// V8-30738 : L.Ineson
//  Added Print Template procs.
// V8-30818 : N.Foster
//  Added additional engine metric procedures
// V8-30840 : A.Kuszyk
//  Added ProductUniverseFetchByEntityIdName.
// V8-30646 : M.Shelley
//  Added WorkpackageProcessingStatusSetPendingByWorkpackageId 
// V8-31479 : N.Foster
//  Added procedures to bulk insert/update planogram assortment products
#endregion
#region Version History: CCM830
// V8-31550 : A.Probyn
//  Added new PlanogramAssortmentProductBuddy, PlanogramAssortmentLocationBuddy, AssortmentLocationBuddy, AssortmentProductBuddy stored procs
// V8-31551 : A.Probyn
//  Added new PlanogramAssortmentInventoryRule, AssortmentInventoryRule  stored procs
// V8-31546 : M.Pettit
//  Added Planogram Export Template stored procedures
// V8-31819 : A.Silva
//  Added PlanogramComparison, PlanogramComparisonField, PlanogramComparisonResult, PlanogramComparisonItem, PlanogramComparisonFieldValue procedures.
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-31945 : A.Silva
//  Added PlanogramComparisonTemplate and PlanogramComparisonTemplateField stored procedures.
// V8-31831 : A.Probyn
//  Added PlanogramNameTemplateFetchByEntityIdName
// V8-32361 : L.Ineson
//  Added ProductLibrary procs
// V8-32356 : N.Haywood
//  Added ProductFetchByEntityIdMultipleSearchText
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroup names.
#endregion

#endregion

using System;

namespace Galleria.Ccm.Dal.Mssql.Schema
{
    internal static class ProcedureNames
    {
        #region Assortment

        public const String AssortmentFetchById = "MasterData.Assortment_FetchById";
        public const String AssortmentFetchByEntityIdName = "MasterData.Assortment_FetchByEntityIdName";
        public const String AssortmentInsert = "MasterData.Assortment_Insert";
        public const String AssortmentUpdateById = "MasterData.Assortment_UpdateById";
        public const String AssortmentDeleteById = "MasterData.Assortment_DeleteById";
        public const String AssortmentDeleteByEntityId = "MasterData.Assortment_DeleteByEntityId";
        public const String AssortmentFetchAll = "MasterData.Assortment_FetchAll";
        public const String Assortment_FetchByUniqueContentReference = "MasterData.Assortment_FetchByUniqueContentReference";

        #endregion

        #region AssortmentInfo

        public const String AssortmentInfoFetchByEntityId = "MasterData.AssortmentInfo_FetchByEntityId";
        public const String AssortmentInfoFetchByProductGroupId = "MasterData.AssortmentInfo_FetchByProductGroupId";
        public const String AssortmentInfoFetchByEntityIdAssortmentSearchCriteria = "MasterData.AssortmentInfo_FetchByEntityIdAssortmentSearchCriteria";
        public const String AssortmentInfoFetchByEntityIdAssortmentIds = "MasterData.AssortmentInfo_FetchByEntityIdAssortmentIds";

        #endregion

        #region AssortmentFile

        public const String AssortmentFileFetchByAssortmentId = "MasterData.AssortmentFile_FetchByAssortmentId";
        public const String AssortmentFileFetchById = "MasterData.AssortmentFile_FetchById";
        public const String AssortmentFileInsert = "MasterData.AssortmentFile_FetchById";
        public const String AssortmentFileUpdateById = "MasterData.AssortmentFile_UpdateById";
        public const String AssortmentFileDeleteById = "MasterData.AssortmentFile_DeleteById";

        #endregion

        #region AssortmentInventoryRule

        public const String AssortmentInventoryRuleFetchById = "MasterData.AssortmentInventoryRule_FetchById";
        public const String AssortmentInventoryRuleFetchByAssortmentId = "MasterData.AssortmentInventoryRule_FetchByAssortmentId";
        public const String AssortmentInventoryRuleInsert = "MasterData.AssortmentInventoryRule_Insert";
        public const String AssortmentInventoryRuleUpdateById = "MasterData.AssortmentInventoryRule_UpdateById";
        public const String AssortmentInventoryRuleDeleteById = "MasterData.AssortmentInventoryRule_DeleteById";

        #endregion

        #region AssortmentLocalProduct

        public const String AssortmentLocalProductFetchById = "MasterData.AssortmentLocalProduct_FetchById";
        public const String AssortmentLocalProductFetchByAssortmentId = "MasterData.AssortmentLocalProduct_FetchByAssortmentId";
        public const String AssortmentLocalProductInsert = "MasterData.AssortmentLocalProduct_Insert";
        public const String AssortmentLocalProductUpdateById = "MasterData.AssortmentLocalProduct_UpdateById";
        public const String AssortmentLocalProductDeleteById = "MasterData.AssortmentLocalProduct_DeleteById";

        #endregion

        #region AssortmentLocation

        public const String AssortmentLocationFetchById = "MasterData.AssortmentLocation_FetchById";
        public const String AssortmentLocationInsert = "MasterData.AssortmentLocation_Insert";
        public const String AssortmentLocationUpdateById = "MasterData.AssortmentLocation_UpdateById";
        public const String AssortmentLocationDeleteById = "MasterData.AssortmentLocation_DeleteById";
        public const String AssortmentLocationFetchByAssortmentId = "MasterData.AssortmentLocation_FetchByAssortmentId";

        #endregion

        #region AssortmentLocationBuddy

        public const String AssortmentLocationBuddyFetchById = "MasterData.AssortmentLocationBuddy_FetchById";
        public const String AssortmentLocationBuddyFetchByAssortmentId = "MasterData.AssortmentLocationBuddy_FetchByAssortmentId";
        public const String AssortmentLocationBuddyInsert = "MasterData.AssortmentLocationBuddy_Insert";
        public const String AssortmentLocationBuddyUpdateById = "MasterData.AssortmentLocationBuddy_UpdateById";
        public const String AssortmentLocationBuddyDeleteById = "MasterData.AssortmentLocationBuddy_DeleteById";

        #endregion

        #region AssortmentProduct

        public const String AssortmentProductFetchById = "MasterData.AssortmentProduct_FetchById";
        public const String AssortmentProductInsert = "MasterData.AssortmentProduct_Insert";
        public const String AssortmentProductUpdateById = "MasterData.AssortmentProduct_UpdateById";
        public const String AssortmentProductDeleteById = "MasterData.AssortmentProduct_DeleteById";
        public const String AssortmentProductFetchByAssortmentId = "MasterData.AssortmentProduct_FetchByAssortmentId";

        #endregion

        #region AssortmentProductBuddy

        public const String AssortmentProductBuddyFetchById = "MasterData.AssortmentProductBuddy_FetchById";
        public const String AssortmentProductBuddyFetchByAssortmentId = "MasterData.AssortmentProductBuddy_FetchByAssortmentId";
        public const String AssortmentProductBuddyInsert = "MasterData.AssortmentProductBuddy_Insert";
        public const String AssortmentProductBuddyUpdateById = "MasterData.AssortmentProductBuddy_UpdateById";
        public const String AssortmentProductBuddyDeleteById = "MasterData.AssortmentProductBuddy_DeleteById";

        #endregion

        #region AssortmentRegion

        public const String AssortmentRegionFetchById = "MasterData.AssortmentRegion_FetchById";
        public const String AssortmentRegionFetchByAssortmentId = "MasterData.AssortmentRegion_FetchByAssortmentId";
        public const String AssortmentRegionInsert = "MasterData.AssortmentRegion_Insert";
        public const String AssortmentRegionUpdateById = "MasterData.AssortmentRegion_UpdateById";
        public const String AssortmentRegionDeleteById = "MasterData.AssortmentRegion_DeleteById";

        #endregion

        #region AssortmentRegionLocation

        public const String AssortmentRegionLocationFetchById = "MasterData.AssortmentRegionLocation_FetchById";
        public const String AssortmentRegionLocationInsert = "MasterData.AssortmentRegionLocation_Insert";
        public const String AssortmentRegionLocationUpdateById = "MasterData.AssortmentRegionLocation_UpdateById";
        public const String AssortmentRegionLocationDeleteById = "MasterData.AssortmentRegionLocation_DeleteById";
        public const String AssortmentRegionLocationFetchByAssortmentRegionId = "MasterData.AssortmentRegionLocation_FetchByAssortmentRegionId";

        #endregion

        #region AssortmentRegionalProduct

        public const String AssortmentRegionalProductFetchById = "MasterData.AssortmentRegionalProduct_FetchById";
        public const String AssortmentRegionalProductInsert = "MasterData.AssortmentRegionalProduct_Insert";
        public const String AssortmentRegionalProductUpdateById = "MasterData.AssortmentRegionalProduct_UpdateById";
        public const String AssortmentRegionalProductDeleteById = "MasterData.AssortmentRegionalProduct_DeleteById";
        public const String AssortmentRegionalProductFetchByAssortmentRegionId = "MasterData.AssortmentRegionalProduct_FetchByAssortmentRegionId";


        #endregion

        #region AssortmentMinorRevision

        public const String AssortmentMinorRevisionFetchById = "MasterData.AssortmentMinorRevision_FetchById";
        public const String AssortmentMinorRevisionInsert = "MasterData.AssortmentMinorRevision_Insert";
        public const String AssortmentMinorRevisionUpdateById = "MasterData.AssortmentMinorRevision_UpdateById";
        public const String AssortmentMinorRevisionDeleteById = "MasterData.AssortmentMinorRevision_DeleteById";
        public const String AssortmentMinorRevisionFetchByEntityIdName = "MasterData.AssortmentMinorRevision_FetchByEntityIdName";
        public const String AssortmentMinorRevisionFetchAll = "MasterData.AssortmentMinorRevision_FetchAll";

        #endregion

        #region AssortmentMinorRevisionInfo

        public const String AssortmentMinorRevisionInfoFetchByEntityId = "MasterData.AssortmentMinorRevisionInfo_FetchByEntityId";
        public const String AssortmentMinorRevisionInfoFetchByProductGroupId = "MasterData.[AssortmentMinorRevisionInfo_FetchByProductGroupId]";
        public const String AssortmentMinorRevisionInfoFetchByEntityIdChangeDate = "MasterData.AssortmentMinorRevisionInfo_FetchByEntityIdChangeDate";
        public const String AssortmentMinorRevisionInfoFetchByEntityIdName = "MasterData.AssortmentMinorRevisionInfo_FetchByEntityIdName";
        public const String AssortmentMinorRevisionInfoFetchByEntityIdAssortmentMinorRevisionSearchCriteria = "MasterData.AssortmentMinorRevisionInfo_FetchByEntityIdAssortmentMinorRevisionSearchCriteria";
        public const String AssortmentMinorRevisionInfoFetchByEntityIdIncludingDeleted = "MasterData.AssortmentMinorRevisionInfo_FetchByEntityIdIncludingDeleted";

        #endregion

        #region AssortmentMinorRevisionAmendDistributionAction

        public const String AssortmentMinorRevisionAmendDistributionActionFetchById = "MasterData.AssortmentMinorRevisionAmendDistributionAction_FetchById";
        public const String AssortmentMinorRevisionAmendDistributionActionInsert = "MasterData.AssortmentMinorRevisionAmendDistributionAction_Insert";
        public const String AssortmentMinorRevisionAmendDistributionActionUpdateById = "MasterData.AssortmentMinorRevisionAmendDistributionAction_UpdateById";
        public const String AssortmentMinorRevisionAmendDistributionActionDeleteById = "MasterData.AssortmentMinorRevisionAmendDistributionAction_DeleteById";
        public const String AssortmentMinorRevisionAmendDistributionActionFetchByAssortmentMinorRevisionId = "MasterData.AssortmentMinorRevisionAmendDistributionAction_FetchByAssortmentMinorRevisionId";

        #endregion

        #region AssortmentMinorRevisionAmendDistributionActionLocation

        public const String AssortmentMinorRevisionAmendDistributionActionLocationFetchById = "MasterData.AssortmentMinorRevisionAmendDistributionActionLocation_FetchById";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationInsert = "MasterData.AssortmentMinorRevisionAmendDistributionActionLocation_Insert";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationUpdateById = "MasterData.AssortmentMinorRevisionAmendDistributionActionLocation_UpdateById";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationDeleteById = "MasterData.AssortmentMinorRevisionAmendDistributionActionLocation_DeleteById";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationFetchByAssortmentMinorRevisionAmendDistributionActionId = "MasterData.AssortmentMinorRevisionAmendDistributionActionLocation_FetchByAssortmentMinorRevisionAmendDistributionActionId";

        #endregion

        #region AssortmentMinorRevisionDeListAction

        public const String AssortmentMinorRevisionDeListActionFetchById = "MasterData.AssortmentMinorRevisionDeListAction_FetchById";
        public const String AssortmentMinorRevisionDeListActionInsert = "MasterData.AssortmentMinorRevisionDeListAction_Insert";
        public const String AssortmentMinorRevisionDeListActionUpdateById = "MasterData.AssortmentMinorRevisionDeListAction_UpdateById";
        public const String AssortmentMinorRevisionDeListActionDeleteById = "MasterData.AssortmentMinorRevisionDeListAction_DeleteById";
        public const String AssortmentMinorRevisionDeListActionFetchByAssortmentMinorRevisionId = "MasterData.AssortmentMinorRevisionDeListAction_FetchByAssortmentMinorRevisionId";

        #endregion

        #region AssortmentMinorRevisionDeListActionLocation

        public const String AssortmentMinorRevisionDeListActionLocationFetchById = "MasterData.AssortmentMinorRevisionDeListActionLocation_FetchById";
        public const String AssortmentMinorRevisionDeListActionLocationInsert = "MasterData.AssortmentMinorRevisionDeListActionLocation_Insert";
        public const String AssortmentMinorRevisionDeListActionLocationUpdateById = "MasterData.AssortmentMinorRevisionDeListActionLocation_UpdateById";
        public const String AssortmentMinorRevisionDeListActionLocationDeleteById = "MasterData.AssortmentMinorRevisionDeListActionLocation_DeleteById";
        public const String AssortmentMinorRevisionDeListActionLocationFetchByAssortmentMinorRevisionListActionId = "MasterData.AssortmentMinorRevisionDeListActionLocation_FetchByAssortmentMinorRevisionListActionId";

        #endregion

        #region AssortmentMinorRevisionListAction

        public const String AssortmentMinorRevisionListActionFetchById = "MasterData.AssortmentMinorRevisionListAction_FetchById";
        public const String AssortmentMinorRevisionListActionInsert = "MasterData.AssortmentMinorRevisionListAction_Insert";
        public const String AssortmentMinorRevisionListActionUpdateById = "MasterData.AssortmentMinorRevisionListAction_UpdateById";
        public const String AssortmentMinorRevisionListActionDeleteById = "MasterData.AssortmentMinorRevisionListAction_DeleteById";
        public const String AssortmentMinorRevisionListActionFetchByAssortmentMinorRevisionId = "MasterData.AssortmentMinorRevisionListAction_FetchByAssortmentMinorRevisionId";

        #endregion

        #region AssortmentMinorRevisionListActionLocation

        public const String AssortmentMinorRevisionListActionLocationFetchById = "MasterData.AssortmentMinorRevisionListActionLocation_FetchById";
        public const String AssortmentMinorRevisionListActionLocationInsert = "MasterData.AssortmentMinorRevisionListActionLocation_Insert";
        public const String AssortmentMinorRevisionListActionLocationUpdateById = "MasterData.AssortmentMinorRevisionListActionLocation_UpdateById";
        public const String AssortmentMinorRevisionListActionLocationDeleteById = "MasterData.AssortmentMinorRevisionListActionLocation_DeleteById";
        public const String AssortmentMinorRevisionListActionLocationFetchByAssortmentMinorRevisionListActionId = "MasterData.AssortmentMinorRevisionListActionLocation_FetchByAssortmentMinorRevisionListActionId";

        #endregion

        #region AssortmentMinorRevisionReplaceAction

        public const String AssortmentMinorRevisionReplaceActionFetchById = "MasterData.AssortmentMinorRevisionReplaceAction_FetchById";
        public const String AssortmentMinorRevisionReplaceActionInsert = "MasterData.AssortmentMinorRevisionReplaceAction_Insert";
        public const String AssortmentMinorRevisionReplaceActionUpdateById = "MasterData.AssortmentMinorRevisionReplaceAction_UpdateById";
        public const String AssortmentMinorRevisionReplaceActionDeleteById = "MasterData.AssortmentMinorRevisionReplaceAction_DeleteById";
        public const String AssortmentMinorRevisionReplaceActionFetchByAssortmentMinorRevisionId = "MasterData.AssortmentMinorRevisionReplaceAction_FetchByAssortmentMinorRevisionId";

        #endregion

        #region AssortmentMinorRevisionReplaceActionLocation

        public const String AssortmentMinorRevisionReplaceActionLocationFetchById = "MasterData.AssortmentMinorRevisionReplaceActionLocation_FetchById";
        public const String AssortmentMinorRevisionReplaceActionLocationInsert = "MasterData.AssortmentMinorRevisionReplaceActionLocation_Insert";
        public const String AssortmentMinorRevisionReplaceActionLocationUpdateById = "MasterData.AssortmentMinorRevisionReplaceActionLocation_UpdateById";
        public const String AssortmentMinorRevisionReplaceActionLocationDeleteById = "MasterData.AssortmentMinorRevisionReplaceActionLocation_DeleteById";
        public const String AssortmentMinorRevisionReplaceActionLocationFetchByAssortmentMinorRevisionReplaceActionId = "MasterData.AssortmentMinorRevisionReplaceActionLocation_FetchByAssortmentMinorRevisionReplaceActionId";

        #endregion

        #region Blob

        public const String BlobFetchById = "MasterData.Blob_FetchById";
        public const String BlobInsert = "MasterData.Blob_Insert";
        public const String BlobUpdateById = "MasterData.Blob_UpdateById";
        public const String BlobDeleteById = "MasterData.Blob_DeleteById";

        #endregion

        #region Blocking

        public const String BlockingFetchById = "MasterData.Blocking_FetchById";
        public const String BlockingFetchByEntityIdName = "MasterData.Blocking_FetchByEntityIdName";
        public const String BlockingInsert = "MasterData.Blocking_Insert";
        public const String BlockingUpdateById = "MasterData.Blocking_UpdateById";
        public const String BlockingDeleteById = "MasterData.Blocking_DeleteById";

        #endregion

        #region BlockingDivider

        public const String BlockingDividerFetchByBlockingId = "MasterData.BlockingDivider_FetchByBlockingId";
        public const String BlockingDividerInsert = "MasterData.BlockingDivider_Insert";
        public const String BlockingDividerUpdateById = "MasterData.BlockingDivider_UpdateById";
        public const String BlockingDividerDeleteById = "MasterData.BlockingDivider_DeleteById";

        #endregion

        #region BlockingGroup

        public const String BlockingGroupFetchByBlockingId = "MasterData.BlockingGroup_FetchByBlockingId";
        public const String BlockingGroupInsert = "MasterData.BlockingGroup_Insert";
        public const String BlockingGroupUpdateById = "MasterData.BlockingGroup_UpdateById";
        public const String BlockingGroupDeleteById = "MasterData.BlockingGroup_DeleteById";

        #endregion

        #region BlockingLocation

        public const String BlockingLocationFetchByBlockingId = "MasterData.BlockingLocation_FetchByBlockingId";
        public const String BlockingLocationInsert = "MasterData.BlockingLocation_Insert";
        public const String BlockingLocationUpdateById = "MasterData.BlockingLocation_UpdateById";
        public const String BlockingLocationDeleteById = "MasterData.BlockingLocation_DeleteById";

        #endregion

        #region BlockingInfo

        public const String BlockingInfoFetchByEntityId = "MasterData.BlockingInfo_FetchByEntityId";

        #endregion

        #region Cluster

        public const String ClusterFetchById = "MasterData.Cluster_FetchById";
        public const String ClusterFetchByClusterSchemeId = "MasterData.Cluster_FetchByClusterSchemeId";
        public const String ClusterInsert = "MasterData.Cluster_Insert";
        public const String ClusterUpdateById = "MasterData.Cluster_UpdateById";
        public const String ClusterBulkUpsert = "MasterData.Cluster_BulkUpsert";
        public const String ClusterDeleteById = "MasterData.Cluster_DeleteById";

        #endregion

        #region ClusterLocation

        public const String ClusterLocationFetchById = "MasterData.ClusterLocation_FetchById";
        public const String ClusterLocationFetchByClusterId = "MasterData.ClusterLocation_FetchByClusterId";
        public const String ClusterLocationInsert = "MasterData.ClusterLocation_Insert";
        public const String ClusterLocationUpdateById = "MasterData.ClusterLocation_UpdateById";
        public const String ClusterLocationBulkUpsert = "MasterData.ClusterLocation_BulkUpsert";
        public const String ClusterLocationDeleteById = "MasterData.ClusterLocation_DeleteById";

        #endregion

        #region ClusterLocationInfo

        public const String ClusterLocationSearchCriteriaFetchByEntityId = "MasterData.ClusterLocationSearchCriteria_FetchByEntityId";

        #endregion

        #region ClusterScheme

        public const String ClusterSchemeFetchById = "MasterData.ClusterScheme_FetchById";
        public const String ClusterSchemeFetchByEntityId = "MasterData.ClusterScheme_FetchByEntityId";
        public const String ClusterSchemeInsert = "MasterData.ClusterScheme_Insert";
        public const String ClusterSchemeUpdateById = "MasterData.ClusterScheme_UpdateById";
        public const String ClusterSchemeBulkUpsert = "MasterData.ClusterScheme_BulkUpsert";
        public const String ClusterSchemeDeleteById = "MasterData.ClusterScheme_DeleteById";
        public const String ClusterSchemeDeleteByEntityId = "MasterData.ClusterScheme_DeleteByEntityId";

        #endregion

        #region ClusterSchemeInfo

        public const String ClusterSchemeInfoFetchByEntityId = "MasterData.ClusterSchemeInfo_FetchByEntityId";

        #endregion

        #region Compression
        public const String CompressionDeleteById = "System.Compression_DeleteById";
        public const String CompressionFetchById = "System.Compression_FetchById";
        public const String CompressionFetchAll = "System.Compression_FetchAll";
        public const String CompressionFetchAllEnabled = "System.Compression_FetchAllEnabled";
        public const String CompressionUpdate = "System.Compression_UpdateById";
        public const String CompressionInsert = "System.Compression_Insert";
        #endregion

        #region ConsumerDecisionTree

        public const String ConsumerDecisionTreeFetchById = "MasterData.ConsumerDecisionTree_FetchById";
        public const String ConsumerDecisionTreeFetchByEntityIdName = "MasterData.ConsumerDecisionTree_FetchByEntityIdName";
        public const String ConsumerDecisionTreeFetchByEntityIdConsumerDecisionTreeIds = "MasterData.ConsumerDecisionTree_FetchByEntityIdConsumerDecisionTreeIds";
        public const String ConsumerDecisionTreeInsert = "MasterData.ConsumerDecisionTree_Insert";
        public const String ConsumerDecisionTreeUpdateById = "MasterData.ConsumerDecisionTree_UpdateById";
        public const String ConsumerDecisionTreeDeleteById = "MasterData.ConsumerDecisionTree_DeleteById";

        #endregion

        #region ConsumerDecisionTreeInfo

        public const String ConsumerDecisionTreeInfoFetchByEntityId = "MasterData.ConsumerDecisionTreeInfo_FetchByEntityId";
        public const String ConsumerDecisionTreeInfoFetchByProductGroupId = "MasterData.ConsumerDecisionTreeInfo_FetchByProductGroupId";
        public const String ConsumerDecisionTreeInfoFetchById = "MasterData.ConsumerDecisionTreeInfo_FetchById";
        public const String ConsumerDecisionTreeInfoFetchAllIncludingDeleted = "MasterData.ConsumerDecisionTreeInfo_FetchAllIncludingDeleted";

        #endregion

        #region ConsumerDecisionTreeNode

        public const String ConsumerDecisionTreeNodeFetchById = "MasterData.ConsumerDecisionTreeNode_FetchById";
        public const String ConsumerDecisionTreeNodeFetchByConsumerDecisionTreeId = "MasterData.ConsumerDecisionTreeNode_FetchByConsumerDecisionTreeId";
        public const String ConsumerDecisionTreeNodeFetchAll = "MasterData.ConsumerDecisionTreeNode_FetchAll";
        public const String ConsumerDecisionTreeNodeInsert = "MasterData.ConsumerDecisionTreeNode_Insert";
        public const String ConsumerDecisionTreeNodeUpdateById = "MasterData.ConsumerDecisionTreeNode_UpdateById";
        public const String ConsumerDecisionTreeNodeDeleteById = "MasterData.ConsumerDecisionTreeNode_DeleteById";

        #endregion

        #region ConsumerDecisionTreeLevel

        public const String ConsumerDecisionTreeLevelFetchById = "MasterData.ConsumerDecisionTreeLevel_FetchById";
        public const String ConsumerDecisionTreeLevelFetchByEntityId = "MasterData.ConsumerDecisionTreeLevel_FetchByEntityId";
        public const String ConsumerDecisionTreeLevelFetchByConsumerDecisionTreeId = "MasterData.ConsumerDecisionTreeLevel_FetchByConsumerDecisionTreeId";
        public const String ConsumerDecisionTreeLevelInsert = "MasterData.ConsumerDecisionTreeLevel_Insert";
        public const String ConsumerDecisionTreeLevelUpdateById = "MasterData.ConsumerDecisionTreeLevel_UpdateById";
        public const String ConsumerDecisionTreeLevelDeleteById = "MasterData.ConsumerDecisionTreeLevel_DeleteById";

        #endregion

        #region ConsumerDecisionTreeNodeProduct

        public const String ConsumerDecisionTreeNodeProductFetchById = "MasterData.ConsumerDecisionTreeNodeProduct_FetchById";
        public const String ConsumerDecisionTreeNodeProductFetchByConsumerDecisionTreeNodeId = "MasterData.ConsumerDecisionTreeNodeProduct_FetchByConsumerDecisionTreeNodeId";
        public const String ConsumerDecisionTreeNodeProductInsert = "MasterData.ConsumerDecisionTreeNodeProduct_Insert";
        public const String ConsumerDecisionTreeNodeProductUpdateById = "MasterData.ConsumerDecisionTreeNodeProduct_UpdateById";
        public const String ConsumerDecisionTreeNodeProductDeleteById = "MasterData.ConsumerDecisionTreeNodeProduct_DeleteById";

        #endregion

        #region CustomAttributeData

        public const String CustomAttributeDataFetchByParentTypeParentId = "MasterData.CustomAttributeData_FetchByParentTypeParentId";
        public const String CustomAttributeDataFetchByParentTypeParentIds = "MasterData.CustomAttributeData_FetchByParentTypeParentIds";
        public const String CustomAttributeDataInsert = "MasterData.CustomAttributeData_Insert";
        public const String CustomAttributeDataUpdateById = "MasterData.CustomAttributeData_UpdateById";
        public const String CustomAttributeDataBulkUpsert = "MasterData.CustomAttributeData_BulkUpsert";
        public const String CustomAttributeDataDeleteById = "MasterData.CustomAttributeData_DeleteById";
        public const String CustomAttributeDataBulkInsert = "MasterData.CustomAttributeData_BulkInsert";
        public const String CustomAttributeDataBulkUpdate = "MasterData.CustomAttributeData_BulkUpdate";

        #endregion

        #region ContentLookup

        public const String ContentLookupFetchById = "MasterData.ContentLookup_FetchById";
        public const String ContentLookupFetchByEntityId = "MasterData.ContentLookup_FetchByEntityId";
        public const String ContentLookupFetchByPlanogramId = "MasterData.ContentLookup_FetchByPlanogramId";
        public const String ContentLookupFetchByPlanogramIds = "MasterData.ContentLookup_FetchByPlanogramIds";
        public const String ContentLookupInsert = "MasterData.ContentLookup_Insert";
        public const String ContentLookupUpdateById = "MasterData.ContentLookup_UpdateById";
        public const String ContentLookupDeleteById = "MasterData.ContentLookup_DeleteById";

        #endregion

        #region Data Info

        public const String DataInfoFetchByEntityId = "MasterData.DataInfo_FetchByEntityId";

        #endregion

        #region EngineInstance

        public const String EngineInstanceUpdateById = "Engine.Instance_UpdateById";
        public const String EngineInstanceDeleteById = "Engine.Instance_DeleteById";
        public const String EngineInstanceDeleteInactive = "Engine.Instance_DeleteInactive";
        public const String EngineInstanceFetchAll = "Engine.Instance_FetchAll";
        public const String EngineInstanceFetchStats = "Engine.Instance_FetchStats";

        #endregion

        #region EngineMaintenance

        public const String EngineMaintenanceFetchPlanogramsByMetadataCalculationRequired = "Engine.Maintenance_FetchPlanogramsByMetadataCalculationRequired";
        public const String EngineMaintenanceFetchPlanogramsByValidationCalculationRequired = "Engine.Maintenance_FetchPlanogramsByValidationCalculationRequired";
        public const String EngineMaintenanceFetchPlanogramsByDeletionRequired = "Engine.Maintenance_FetchPlanogramsByDeletionRequired";

        #endregion

        #region EngineMessage

        public const String EngineMessageFetchNext = "Engine.Message_FetchNext";
        public const String EngineMessageFetchStats = "Engine.Message_FetchStats";
        public const String EngineMessageReapNext = "Engine.Message_ReapNext";
        public const String EngineMessageReapById = "Engine.Message_ReapById";
        public const String EngineMessageInsert = "Engine.Message_Insert";
        public const String EngineMessageAddDependencies = "Engine.Message_AddDependencies";
        public const String EngineMessageDelete = "Engine.Message_Delete";
        public const String EngineMessageExistsByCriteria = "Engine.Message_ExistsByCriteria";

        #endregion

        #region EngineMetric

        public const String EngineMetricInsert = "Engine.Metric_Insert";
        public const String EngineMetricDeleteByTimestamp = "Engine.Metric_DeleteByTimestamp";

        #endregion

        #region EngineParameter

        public const String EngineParameterFetchByName = "Engine.Parameter_FetchByName";
        public const String EngineParameterUpdateByName = "Engine.Parameter_UpdateByName";

        #endregion

        #region Entity

        public const String EntityFetchAll = "MasterData.Entity_FetchAll";
        public const String EntityFetchById = "MasterData.Entity_FetchById";
        public const String EntityFetchDeletedByName = "MasterData.Entity_FetchDeletedByName";
        public const String EntityInsert = "MasterData.Entity_Insert";
        public const String EntityUpdateById = "MasterData.Entity_UpdateById";
        public const String EntityDeleteById = "MasterData.Entity_DeleteById";

        #endregion

        #region EntityAttributeComparisonDetails

        public const String EntityComparisonAttributeFetchByPlanogramId = "MasterData.EntityProductAttributeComparisonAttribute_FetchByEntityId";
        public const String EntityComparisonAttributeInsert = "MasterData.EntityProductAttributeComparisonAttribute_Insert";
        public const String EntityComparisonAttributeUpdateById = "MasterData.EntityProductAttributeComparisonAttribute_UpdateById";
        public const String EntityComparisonAttributeDeleteById = "MasterData.EntityProductAttributeComparisonAttribute_DeleteById";


        #endregion

        #region EntityInfo

        public const String EntityInfoFetchAll = "MasterData.EntityInfo_FetchAll";

        #endregion

        #region EventLog

        public const String EventLogFetchAll = "System.EventLog_FetchAll";
        public const String EventLogFetchById = "System.EventLog_FetchById";
        public const String EventLogFetchAllOccuredAfterDateTime = "System.EventLog_FetchAllOccuredAfterDateTime";
        public const String EventLogFetchByEventLogNameId = "System.EventLog_FetchByEventLogNameId";
        public const String EventLogFetchByEventLogNameIdEntryTypeEntityId = "System.EventLog_FetchByEventLogNameIdEntryTypeEntityId";
        public const String EventLogFetchByMixedCriteria = "System.EventLog_FetchByMixedCriteria";
        public const String EventLogInsert = "System.EventLog_Insert";
        public const String EventLogInsertWithName = "System.EventLog_InsertWithName";
        public const String EventLogDeleteAll = "System.EventLog_DeleteAll";
        public const String EventLogFetchTopSpecifiedAmountIncludingDeleted = "System.EventLog_FetchTopSpecifiedAmountIncludingDeleted";
        public const String EventLogFetchByWorkpackageId = "System.EventLog_FetchByWorkpackageId";
        #endregion

        #region EventLogName

        public const String EventLogNameFetchAll = "System.EventLogName_FetchAll";
        public const String EventLogNameFetchById = "System.EventLogName_FetchById";
        public const String EventLogNameFetchByName = "System.EventLogName_FetchByName";
        public const String EventLogNameInsert = "System.EventLogName_Insert";

        #endregion

        #region File

        public const String FileFetchById = "MasterData.File_FetchById";
        public const String FileInsert = "MasterData.File_Insert";
        public const String FileUpdateById = "MasterData.File_UpdateById";
        public const String FileDeleteById = "MasterData.File_DeleteById";

        #endregion

        #region FileInfo

        public const String FileInfoFetchById = "MasterData.FileInfo_FetchById";


        #endregion

        #region Highlight

        public const String HighlightFetchById = "MasterData.Highlight_FetchById";
        public const String HighlightFetchByEntityIdName = "MasterData.Highlight_FetchByEntityIdName";
        public const String HighlightInsert = "MasterData.Highlight_Insert";
        public const String HighlightUpdate = "MasterData.Highlight_Update";
        public const String HighlightDeleteById = "MasterData.Highlight_DeleteById";

        #endregion

        #region HighlightInfo

        public const String HighlightInfoFetchByIds = "MasterData.HighlightInfo_FetchByIds";
        public const String HighlightInfoByEntityId = "MasterData.HighlightInfo_FetchByEntityId";
        public const String HighlightInfoByEntityIdIncludingDeleted = "MasterData.HighlightInfo_FetchByEntityIdIncludingDeleted";

        #endregion

        #region Highlight Group

        public const String HighlightGroupFetchByHighlightId = "MasterData.HighlightGroup_FetchByHighlightId";
        public const String HighlightGroupInsert = "MasterData.HighlightGroup_Insert";
        public const String HighlightGroupUpdate = "MasterData.HighlightGroup_UpdateById";
        public const String HighlightGroupDeleteById = "MasterData.HighlightGroup_DeleteById";

        #endregion

        #region Highlight Filter

        public const String HighlightFilterFetchById = "MasterData.HighlightFilter_FetchByHighlightId";
        public const String HighlightFilterInsert = "MasterData.HighlightFilter_Insert";
        public const String HighlightFilterUpdate = "MasterData.HighlightFilter_UpdateById";
        public const String HighlightFilterDeleteById = "MasterData.HighlightFilter_DeleteById";

        #endregion

        #region HighlightCharacteristic

        public const String HighlightCharacteristicFetchByHighlightCharacteristicId = "MasterData.HighlightCharacteristic_FetchByHighlightId";
        public const String HighlightCharacteristicInsert = "MasterData.HighlightCharacteristic_Insert";
        public const String HighlightCharacteristicUpdate = "MasterData.HighlightCharacteristic_UpdateById";
        public const String HighlightCharacteristicDeleteById = "MasterData.HighlightCharacteristic_DeleteById";

        #endregion

        #region HighlightCharacteristicRule

        public const String HighlightCharacteristicRuleFetchByHighlightCharacteristicId = "MasterData.HighlightCharacteristicRule_FetchByHighlightCharacteristicId";
        public const String HighlightCharacteristicRuleInsert = "MasterData.HighlightCharacteristicRule_Insert";
        public const String HighlightCharacteristicRuleUpdate = "MasterData.HighlightCharacteristicRule_UpdateById";
        public const String HighlightCharacteristicRuleDeleteById = "MasterData.HighlightCharacteristicRule_DeleteById";

        #endregion

        #region Image

        public const String ImageDeleteById = "MasterData.Image_DeleteById";
        public const String ImageFetchById = "MasterData.Image_FetchById";
        public const String ImageInsert = "MasterData.Image_Insert";
        public const String ImageUpdate = "MasterData.Image_UpdateById";

        #endregion

        #region InventoryProfile

        public const String InventoryProfileDeleteById = "MasterData.InventoryProfile_DeleteById";
        public const String InventoryProfileInsert = "MasterData.InventoryProfile_Insert";
        public const String InventoryProfileUpdateById = "MasterData.InventoryProfile_UpdateById";
        public const String InventoryProfileFetchById = "MasterData.InventoryProfile_FetchById";
        public const String InventoryProfileFetchByEntityIdName = "MasterData.InventoryProfile_FetchByEntityIdName";

        #endregion

        #region InventoryProfileInfo
        public const String InventoryProfileInfoFetchByEntityId = "MasterData.InventoryProfileInfo_FetchByEntityId";
        #endregion

        #region Label

        public const String LabelFetchById = "MasterData.Label_FetchById";
        public const String LabelFetchByEntityIdName = "MasterData.Label_FetchByEntityIdName";
        public const String LabelInsert = "MasterData.Label_Insert";
        public const String LabelUpdate = "MasterData.Label_Update";
        public const String LabelDeleteById = "MasterData.Label_DeleteById";

        #endregion

        #region LabelInfo

        public const String LabelInfoFetchByIds = "MasterData.LabelInfo_FetchByIds";
        public const String LabelInfoFetchByEntityId = "MasterData.LabelInfo_FetchByEntityId";
        public const String LabelInfoFetchByEntityIdIncludingDeleted = "MasterData.LabelInfo_FetchByEntityIdIncludingDeleted";

        #endregion

        #region Location

        public const String LocationDeleteById = "MasterData.Location_DeleteById";
        public const String LocationDeleteByEntityId = "MasterData.Location_DeleteByEntityId";
        public const String LocationFetchById = "MasterData.Location_FetchById";
        public const String LocationFetchByEntityId = "MasterData.Location_FetchByEntityId";
        public const String LocationFetchByEntityIdIncludingDeleted = "MasterData.Location_FetchByEntityIdIncludingDeleted";
        public const String LocationFetchByLocationGroupId = "MasterData.Location_FetchByLocationGroupId";
        public const String LocationInsert = "MasterData.Location_Insert";
        public const String LocationUpdateById = "MasterData.Location_UpdateById";
        public const String LocationBulkUpsert = "MasterData.Location_BulkUpsert";
        public const String LocationUpdateLocationGroupId = "MasterData.Location_UpdateLocationGroupId";


        #endregion

        #region LocationAttribute

        public const String LocationAttributeFetchByEntityId = "MasterData.LocationAttribute_FetchByEntityId";

        #endregion

        #region LocationGroup

        public const String LocationGroupDeleteById = "MasterData.LocationGroup_DeleteById";
        public const String LocationGroupFetchById = "MasterData.LocationGroup_FetchById";
        public const String LocationGroupFetchByLocationHierarchyId = "MasterData.LocationGroup_FetchByLocationHierarchyId";
        public const String LocationGroupFetchByLocationHierarchyIdIncludingDeleted = "MasterData.LocationGroup_FetchByLocationHierarchyIdIncludingDeleted";
        public const String LocationGroupInsert = "MasterData.LocationGroup_Insert";
        public const String LocationGroupUpdate = "MasterData.LocationGroup_UpdateById";
        public const String LocationGroupBulkUpsert = "MasterData.LocationGroup_BulkUpsert";

        #endregion

        #region LocationGroupInfo
        public const String LocationGroupInfoFetchDeletedByLocationHierarchyId = "MasterData.LocationGroupInfo_FetchDeletedByLocationHierarchyId";
        public const String LocationGroupInfoFetchByLocationGroupIds = "MasterData.LocationGroupInfo_FetchByLocationGroupIds";
        #endregion

        #region LocationGroupLocation
        public const String LocationGroupLocationFetchByLocationGroupId = "MasterData.LocationGroupLocation_FetchByLocationGroupId";
        #endregion

        #region LocationHierarchy

        public const String LocationHierarchyFetchById = "MasterData.LocationHierarchy_FetchById";
        public const String LocationHierarchyFetchByEntityId = "MasterData.LocationHierarchy_FetchByEntityId";
        public const String LocationHierarchyInsert = "MasterData.LocationHierarchy_Insert";
        public const String LocationHierarchyUpdateById = "MasterData.LocationHierarchy_UpdateById";
        public const String LocationHierarchyDeleteById = "MasterData.LocationHierarchy_DeleteById";

        #endregion

        #region LocationInfo

        public const String LocationInfoFetchByEntityId = "MasterData.LocationInfo_FetchByEntityId";
        public const String LocationInfoFetchByEntityIdLocationCodes = "MasterData.LocationInfo_FetchByEntityIdLocationCodes";
        public const String LocationInfoFetchByEntityIdIncludingDeleted = "MasterData.LocationInfo_FetchByEntityIdIncludingDeleted";
        public const String LocationInfoFetchAllIncludingDeleted = "MasterData.LocationInfo_FetchAllIncludingDeleted";
        public const String LocationInfoFetchByWorkpackageIdIncludingDeleted = "MasterData.LocationInfo_FetchByWorkpackageIdIncludingDeleted";
        public const String LocationInfoFetchByWorkpackagePlanogramIdIncludingDeleted = "MasterData.LocationInfo_FetchByWorkpackagePlanogramIdIncludingDeleted";

        #endregion

        #region LocationLevel

        public const String LocationLevelDeleteById = "MasterData.LocationLevel_DeleteById";
        public const String LocationLevelFetchById = "MasterData.LocationLevel_FetchById";
        public const String LocationLevelFetchByLocationHierarchyId = "MasterData.LocationLevel_FetchByLocationHierarchyId";
        public const String LocationLevelInsert = "MasterData.LocationLevel_Insert";
        public const String LocationLevelUpdate = "MasterData.LocationLevel_UpdateById";

        #endregion

        #region LocationPlanAssignment

        public const String LocationPlanAssignmentDeleteById = "Content.LocationPlanAssignment_DeleteById";
        public const String LocationPlanAssignmentFetchByPlanogramId = "Content.LocationPlanAssignment_FetchByPlanogramId";
        public const String LocationPlanAssignmentInsert = "Content.LocationPlanAssignment_Insert";
        public const String LocationPlanAssignmentUpdateById = "Content.LocationPlanAssignment_UpdateById";
        public const String LocationPlanAssignmentFetchById = "Content.LocationPlanAssignment_FetchById";
        public const String LocationPlanAssignmentFetchByProductGroupId = "Content.LocationPlanAssignment_FetchByProductGroupId";
        public const String LocationPlanAssignmentFetchByLocationId = "Content.LocationPlanAssignment_FetchByLocationId";

        #endregion

        #region LocationPlanAssignmentLocation

        public const String LocationPlanAssignmentLocationFetchByLocationPlanAssignmentId = "Content.LocationPlanAssignmentLocation_FetchByLocationPlanAssignmentId";
        public const String LocationPlanAssignmentLocationInsert = "Content.LocationPlanAssignmentLocation_Insert";
        public const String LocationPlanAssignmentLocationUpdate = "Content.LocationPlanAssignmentLocation_Update";
        public const String LocationPlanAssignmentLocationDeleteById = "Content.LocationPlanAssignmentLocation_DeleteById";

        #endregion

        #region LocationProductAttribute

        public const String LocationProductAttributeDeleteById = "MasterData.LocationProductAttribute_DeleteById";
        public const String LocationProductAttributeDeleteByEntityId = "MasterData.LocationProductAttribute_DeleteByEntityId";
        public const String LocationProductAttributeFetchById = "MasterData.LocationProductAttribute_FetchById";
        public const String LocationProductAttributeFetchByEntityIdLocationIdProductIds = "MasterData.LocationProductAttribute_FetchByEntityIdLocationIdProductIds";
        public const String LocationProductAttributeFetchByEntityId = "MasterData.LocationProductAttribute_FetchByEntityId";
        public const String LocationProductAttributeFetchByLocationIdProductIdCombinations = "MasterData.LocationProductAttribute_FetchByLocationIdProductIdCombinations";
        public const String LocationProductAttributeInsert = "MasterData.LocationProductAttribute_Insert";
        public const String LocationProductAttributeUpdate = "MasterData.LocationProductAttribute_UpdateById";
        public const String LocationProductAttributeBulkUpsert = "MasterData.LocationProductAttribute_BulkUpsert";

        #endregion

        #region LocationProductAttributeInfo

        public const String LocationProductAttributeInfoFetchByEntityId = "MasterData.LocationProductAttributeInfo_FetchByEntityId";
        public const String LocationProductAttributeInfoFetchByEntityIdSearchCriteria = "MasterData.LocationProductAttributeInfo_FetchByEntityIdSearchCriteria";
        #endregion

        #region LocationProductIllegal

        public const String LocationProductIllegalDeleteByEntityId = "MasterData.LocationProductIllegal_DeleteByEntityId";
        public const String LocationProductIllegalDeleteById = "MasterData.LocationProductIllegal_DeleteById";
        public const String LocationProductIllegalFetchByEntityId = "MasterData.LocationProductIllegal_FetchByEntityId";
        public const String LocationProductIllegalFetchById = "MasterData.LocationProductIllegal_FetchById";
        public const String LocationProductIllegalFetchByLocationIdProductIdCombinations = "MasterData.LocationProductIllegal_FetchByLocationIdProductIdCombinations";
        public const String LocationProductIllegalInsert = "MasterData.LocationProductIllegal_Insert";
        public const String LocationProductIllegalUpdate = "MasterData.LocationProductIllegal_UpdateById";
        public const String LocationProductIllegalBulkUpsert = "MasterData.LocationProductIllegal_BulkUpsert";

        #endregion

        #region LocationProductLegal

        public const String LocationProductLegalDeleteByEntityId = "MasterData.LocationProductLegal_DeleteByEntityId";
        public const String LocationProductLegalDeleteById = "MasterData.LocationProductLegal_DeleteById";
        public const String LocationProductLegalFetchByEntityId = "MasterData.LocationProductLegal_FetchByEntityId";
        public const String LocationProductLegalFetchById = "MasterData.LocationProductLegal_FetchById";
        public const String LocationProductLegalFetchByLocationIdProductIdCombinations = "MasterData.LocationProductLegal_FetchByLocationIdProductIdCombinations";
        public const String LocationProductLegalInsert = "MasterData.LocationProductLegal_Insert";
        public const String LocationProductLegalUpdate = "MasterData.LocationProductLegal_UpdateById";
        public const String LocationProductLegalBulkUpsert = "MasterData.LocationProductLegal_BulkUpsert";

        #endregion

        #region LocationProductIllegalInfo

        public const String LocationProductIllegalInfoFetchByEntityId = "MasterData.LocationProductIllegalInfo_FetchByEntityId";
        #endregion

        #region LocationSpace
        public const String LocationSpaceFetchById = "MasterData.LocationSpace_FetchById";
        public const String LocationSpaceInsert = "MasterData.LocationSpace_Insert";
        public const String LocationSpaceUpdateById = "MasterData.LocationSpace_UpdateById";
        public const String LocationSpaceDeleteById = "MasterData.LocationSpace_DeleteById";
        public const String LocationSpaceDeleteByEntityId = "MasterData.LocationSpace_DeleteByEntityId";
        public const String LocationSpaceBulkUpsert = "MasterData.LocationSpace_BulkUpsert";
        #endregion

        #region LocationSpaceSearchCriteria

        public const String LocationSpaceSearchCriteriaFetchByEntityId = "MasterData.LocationSpaceSearchCriteria_FetchByEntityId";

        #endregion

        #region LocationSpaceInfo
        public const String LocationSpaceInfoFetchByEntityId = "MasterData.LocationSpaceInfo_FetchByEntityId";
        #endregion

        #region LocationSpaceBay
        public const String LocationSpaceBayFetchByLocationSpaceProductGroupId = "MasterData.LocationSpaceBay_FetchByLocationSpaceProductGroupId";
        public const String LocationSpaceBayFetchById = "MasterData.LocationSpaceBay_FetchById";
        public const String LocationSpaceBayInsert = "MasterData.LocationSpaceBay_Insert";
        public const String LocationSpaceBayUpdateById = "MasterData.LocationSpaceBay_UpdateById";
        public const String LocationSpaceBayDeleteById = "MasterData.LocationSpaceBay_DeleteById";
        public const String LocationSpaceBayDeleteByEntityId = "MasterData.LocationSpaceBay_DeleteByEntityId";
        public const String LocationSpaceBayBulkUpsert = "MasterData.LocationSpaceBay_BulkUpsert";
        public const String LocationSpaceBayBulkInsert = "MasterData.LocationSpaceBay_BulkInsert";
        #endregion

        #region LocationSpaceBaySearchCriteria

        public const String LocationSpaceBaySearchCriteriaFetchByEntityId = "MasterData.LocationSpaceBaySearchCriteria_FetchByEntityId";

        #endregion

        #region LocationSpaceElement
        public const String LocationSpaceElementFetchByLocationSpaceBayId = "MasterData.LocationSpaceElement_FetchByLocationSpaceBayId";
        public const String LocationSpaceElementFetchById = "MasterData.LocationSpaceElement_FetchById";
        public const String LocationSpaceElementInsert = "MasterData.LocationSpaceElement_Insert";
        public const String LocationSpaceElementUpdateById = "MasterData.LocationSpaceElement_UpdateById";
        public const String LocationSpaceElementDeleteById = "MasterData.LocationSpaceElement_DeleteById";
        public const String LocationSpaceElementDeleteByEntityId = "MasterData.LocationSpaceElement_DeleteByEntityId";
        public const String LocationSpaceElementBulkUpsert = "MasterData.LocationSpaceElement_BulkUpsert";
        public const String LocationSpaceElementBulkInsert = "MasterData.LocationSpaceElement_BulkInsert";
        #endregion

        #region LocationSpaceElementSearchCriteria

        public const String LocationSpaceElementSearchCriteriaFetchByEntityId = "MasterData.LocationSpaceElementSearchCriteria_FetchByEntityId";

        #endregion

        #region LocationSpaceProductGroup
        public const String LocationSpaceProductGroupFetchByLocationSpaceId = "MasterData.LocationSpaceProductGroup_FetchByLocationSpaceId";
        public const String LocationSpaceProductGroupFetchById = "MasterData.LocationSpaceProductGroup_FetchById";
        public const String LocationSpaceProductGroupInsert = "MasterData.LocationSpaceProductGroup_Insert";
        public const String LocationSpaceProductGroupUpdateById = "MasterData.LocationSpaceProductGroup_UpdateById";
        public const String LocationSpaceProductGroupDeleteById = "MasterData.LocationSpaceProductGroup_DeleteById";
        public const String LocationSpaceProductGroupDeleteByLocationSpaceId = "MasterData.LocationSpaceProductGroup_DeleteByLocationSpaceId";
        public const String LocationSpaceProductGroupBulkUpsert = "MasterData.LocationSpaceProductGroup_BulkUpsert";
        public const String LocationSpaceProductGroupBulkInsert = "MasterData.LocationSpaceProductGroup_BulkInsert";
        #endregion

        #region LocationSpaceProductGroupInfo

        public const String LocationSpaceProductGroupInfoFetchByProductGroupId = "MasterData.LocationSpaceProductGroupInfo_FetchByProductGroupId";

        #endregion

        #region Metric

        public const String MetricFetchByEntityId = "MasterData.Metric_FetchByEntityId";
        public const String MetricInsert = "MasterData.Metric_Insert";
        public const String MetricUpdate = "MasterData.Metric_Update";
        public const String MetricDeleteById = "MasterData.Metric_DeleteById";

        #endregion

        #region MetricProfile

        public const String MetricProfileDeleteById = "MasterData.MetricProfile_DeleteById";
        public const String MetricProfileFetchById = "MasterData.MetricProfile_FetchById";
        public const String MetricProfileFetchByEntityIdName = "MasterData.MetricProfile_FetchByEntityIdName";
        public const String MetricProfileInsert = "MasterData.MetricProfile_Insert";
        public const String MetricProfileUpdateById = "MasterData.MetricProfile_UpdateById";

        #endregion

        #region MetricProfileInfo

        public const String MetricProfileInfoFetchByEntityId = "MasterData.MetricProfileInfo_FetchByEntityId";

        #endregion

        #region Package

        public const String PackageFetchById = "Content.Package_FetchById";
        public const String PackageInsert = "Content.Package_Insert";
        public const String PackageUpdateById = "Content.Package_UpdateById";
        public const String PackageDeleteById = "Content.Package_DeleteById";
        public const String FetchIdsExceptUcrs = "Content.Package_FetchIdsExceptUcrs";
        public const String FetchByUcrsDateLastModified = "Content.Package_FetchByUcrsDateLastModified";
        public const String FetchDeletedByUcrs = "Content.Package_FetchDeletedByUcrs";

        #endregion

        #region PerformanceSelection

        public const String PerformanceSelectionFetchById = "MasterData.PerformanceSelection_FetchById";
        public const String PerformanceSelectionFetchByEntityIdName = "MasterData.PerformanceSelection_FetchByEntityIdName";
        public const String PerformanceSelectionInsert = "MasterData.PerformanceSelection_Insert";
        public const String PerformanceSelectionUpdateById = "MasterData.PerformanceSelection_UpdateById";
        public const String PerformanceSelectionDeleteById = "MasterData.PerformanceSelection_DeleteById";

        #endregion

        #region PerformanceSelectionInfo
        public const String PerformanceSelectionInfoFetchByEntityId = "MasterData.PerformanceSelectionInfo_FetchByEntityId";
        public const String PerformanceSelectionInfoFetchByEntityIdIncludingDeleted = "MasterData.PerformanceSelectionInfo_FetchByEntityIdIncludingDeleted";
        #endregion

        #region PerformanceSelectionTimelineGroup

        public const String PerformanceSelectionTimelineGroupFetchByPerformanceSelectionId = "MasterData.PerformanceSelectionTimelineGroup_FetchByPerformanceSelectionId";
        public const String PerformanceSelectionTimelineGroupInsert = "MasterData.PerformanceSelectionTimelineGroup_Insert";
        public const String PerformanceSelectionTimelineGroupUpdateById = "MasterData.PerformanceSelectionTimelineGroup_UpdateById";
        public const String PerformanceSelectionTimelineGroupDeleteById = "MasterData.PerformanceSelectionTimelineGroup_DeleteById";

        #endregion

        #region PerformanceSelectionMetric

        public const String PerformanceSelectionMetricFetchByPerformanceSelectionId = "MasterData.PerformanceSelectionMetric_FetchByPerformanceSelectionId";
        public const String PerformanceSelectionMetricUpdateById = "MasterData.PerformanceSelectionMetric_UpdateById";
        public const String PerformanceSelectionMetricInsert = "MasterData.PerformanceSelectionMetric_Insert";
        public const String PerformanceSelectionMetricDeleteById = "MasterData.PerformanceSelectionMetric_DeleteById";

        #endregion

        #region PlanAssignmentStoreSpaceInfo

        public const String PlanAssignmentStoreSpaceInfoFetchByEntityIdProductGroupId = "Content.PlanAssignmentStoreSpaceInfo_FetchByEntityIdProductGroupId";
        public const String PlanAssignmentStoreSpaceInfoFetchByEntityIdLocationId = "Content.PlanAssignmentStoreSpaceInfo_FetchByEntityIdLocationId";

        #endregion

        #region Planogram

        public const String PlanogramFetchById = "Content.Planogram_FetchById";
        public const String PlanogramInsert = "Content.Planogram_Insert";
        public const String PlanogramUpdateById = "Content.Planogram_UpdateById";
        public const String PlanogramDeleteById = "Content.Planogram_DeleteById";
        public const String PlanogramUpdateAttributesById = "Content.Planogram_UpdateAttributesbyId";

        #endregion

        #region PlanogramAnnotation

        public const String PlanogramAnnotationFetchByPlanogramId = "Content.PlanogramAnnotation_FetchByPlanogramId";
        public const String PlanogramAnnotationInsert = "Content.PlanogramAnnotation_Insert";
        public const String PlanogramAnnotationUpdateById = "Content.PlanogramAnnotation_UpdateById";
        public const String PlanogramAnnotationDeleteById = "Content.PlanogramAnnotation_DeleteById";

        #endregion

        #region PlanogramAssemblyComponent

        public const String PlanogramAssemblyComponentFetchByPlanogramAssemblyId = "Content.PlanogramAssemblyComponent_FetchByPlanogramAssemblyId";
        public const String PlanogramAssemblyComponentInsert = "Content.PlanogramAssemblyComponent_Insert";
        public const String PlanogramAssemblyComponentUpdateById = "Content.PlanogramAssemblyComponent_UpdateById";
        public const String PlanogramAssemblyComponentDeleteById = "Content.PlanogramAssemblyComponent_DeleteById";

        #endregion

        #region PlanogramAssembly

        public const String PlanogramAssemblyFetchByPlanogramId = "Content.PlanogramAssembly_FetchByPlanogramId";
        public const String PlanogramAssemblyInsert = "Content.PlanogramAssembly_Insert";
        public const String PlanogramAssemblyUpdateById = "Content.PlanogramAssembly_UpdateById";
        public const String PlanogramAssemblyDeleteById = "Content.PlanogramAssembly_DeleteById";

        #endregion

        #region PlanogramAssortment

        public const String PlanogramAssortmentFetchByPlanogramId = "Content.PlanogramAssortment_FetchByPlanogramId";
        public const String PlanogramAssortmentInsert = "Content.PlanogramAssortment_Insert";
        public const String PlanogramAssortmentUpdateById = "Content.PlanogramAssortment_UpdateById";
        public const String PlanogramAssortmentDeleteById = "Content.PlanogramAssortment_DeleteById";

        #endregion

        #region PlanogramAssortmentInventoryRule

        public const String PlanogramAssortmentInventoryRuleFetchById = "Content.PlanogramAssortmentInventoryRule_FetchById";
        public const String PlanogramAssortmentInventoryRuleFetchByPlanogramAssortmentId = "Content.PlanogramAssortmentInventoryRule_FetchByPlanogramAssortmentId";
        public const String PlanogramAssortmentInventoryRuleInsert = "Content.PlanogramAssortmentInventoryRule_Insert";
        public const String PlanogramAssortmentInventoryRuleUpdateById = "Content.PlanogramAssortmentInventoryRule_UpdateById";
        public const String PlanogramAssortmentInventoryRuleDeleteById = "Content.PlanogramAssortmentInventoryRule_DeleteById";

        #endregion

        #region PlanogramAssortmentLocalProduct

        public const String PlanogramAssortmentLocalProductFetchByPlanogramAssortmentId = "Content.PlanogramAssortmentLocalProduct_FetchByPlanogramAssortmentId";
        public const String PlanogramAssortmentLocalProductInsert = "Content.PlanogramAssortmentLocalProduct_Insert";
        public const String PlanogramAssortmentLocalProductUpdateById = "Content.PlanogramAssortmentLocalProduct_UpdateById";
        public const String PlanogramAssortmentLocalProductDeleteById = "Content.PlanogramAssortmentLocalProduct_DeleteById";

        #endregion

        #region PlanogramAssortmentLocationBuddy

        public const String PlanogramAssortmentLocationBuddyFetchById = "Content.PlanogramAssortmentLocationBuddy_FetchById";
        public const String PlanogramAssortmentLocationBuddyFetchByPlanogramAssortmentId = "Content.PlanogramAssortmentLocationBuddy_FetchByPlanogramAssortmentId";
        public const String PlanogramAssortmentLocationBuddyInsert = "Content.PlanogramAssortmentLocationBuddy_Insert";
        public const String PlanogramAssortmentLocationBuddyUpdateById = "Content.PlanogramAssortmentLocationBuddy_UpdateById";
        public const String PlanogramAssortmentLocationBuddyDeleteById = "Content.PlanogramAssortmentLocationBuddy_DeleteById";

        #endregion

        #region PlanogramAssortmentProduct

        public const String PlanogramAssortmentProductFetchByPlanogramAssortmentId = "Content.PlanogramAssortmentProduct_FetchByPlanogramAssortmentId";
        public const String PlanogramAssortmentProductInsert = "Content.PlanogramAssortmentProduct_Insert";
        public const String PlanogramAssortmentProductUpdateById = "Content.PlanogramAssortmentProduct_UpdateById";
        public const String PlanogramAssortmentProductDeleteById = "Content.PlanogramAssortmentProduct_DeleteById";
        public const String PlanogramAssortmentProductBulkInsert = "Content.PlanogramAssortmentProduct_BulkInsert";
        public const String PlanogramAssortmentProductBulkUpdate = "Content.PlanogramAssortmentProduct_BulkUpdate";

        #endregion

        #region PlanogramAssortmentProductBuddy

        public const String PlanogramAssortmentProductBuddyFetchById = "Content.PlanogramAssortmentProductBuddy_FetchById";
        public const String PlanogramAssortmentProductBuddyFetchByPlanogramAssortmentId = "Content.PlanogramAssortmentProductBuddy_FetchByPlanogramAssortmentId";
        public const String PlanogramAssortmentProductBuddyInsert = "Content.PlanogramAssortmentProductBuddy_Insert";
        public const String PlanogramAssortmentProductBuddyUpdateById = "Content.PlanogramAssortmentProductBuddy_UpdateById";
        public const String PlanogramAssortmentProductBuddyDeleteById = "Content.PlanogramAssortmentProductBuddy_DeleteById";

        #endregion

        #region PlanogramAssortmentRegion

        public const String PlanogramAssortmentRegionFetchByPlanogramAssortmentId = "Content.PlanogramAssortmentRegion_FetchByPlanogramAssortmentId";
        public const String PlanogramAssortmentRegionInsert = "Content.PlanogramAssortmentRegion_Insert";
        public const String PlanogramAssortmentRegionUpdateById = "Content.PlanogramAssortmentRegion_UpdateById";
        public const String PlanogramAssortmentRegionDeleteById = "Content.PlanogramAssortmentRegion_DeleteById";

        #endregion

        #region PlanogramAssortmentRegionLocation

        public const String PlanogramAssortmentRegionLocationFetchByPlanogramAssortmentRegionId = "Content.PlanogramAssortmentRegionLocation_FetchByPlanogramAssortmentRegionId";
        public const String PlanogramAssortmentRegionLocationInsert = "Content.PlanogramAssortmentRegionLocation_Insert";
        public const String PlanogramAssortmentRegionLocationUpdateById = "Content.PlanogramAssortmentRegionLocation_UpdateById";
        public const String PlanogramAssortmentRegionLocationDeleteById = "Content.PlanogramAssortmentRegionLocation_DeleteById";

        #endregion

        #region PlanogramAssortmentRegionProduct

        public const String PlanogramAssortmentRegionProductFetchByPlanogramAssortmentRegionId = "Content.PlanogramAssortmentRegionProduct_FetchByPlanogramAssortmentRegionId";
        public const String PlanogramAssortmentRegionProductInsert = "Content.PlanogramAssortmentRegionProduct_Insert";
        public const String PlanogramAssortmentRegionProductUpdateById = "Content.PlanogramAssortmentRegionProduct_UpdateById";
        public const String PlanogramAssortmentRegionProductDeleteById = "Content.PlanogramAssortmentRegionProduct_DeleteById";

        #endregion

        #region PlanogramBlocking

        public const String PlanogramBlockingFetchByPlanogramId = "Content.PlanogramBlocking_FetchByPlanogramId";
        public const String PlanogramBlockingInsert = "Content.PlanogramBlocking_Insert";
        public const String PlanogramBlockingUpdateById = "Content.PlanogramBlocking_UpdateById";
        public const String PlanogramBlockingDeleteById = "Content.PlanogramBlocking_DeleteById";

        #endregion

        #region PlanogramBlockingDivider

        public const String PlanogramBlockingDividerFetchByPlanogramBlockingId = "Content.PlanogramBlockingDivider_FetchByPlanogramBlockingId";
        public const String PlanogramBlockingDividerInsert = "Content.PlanogramBlockingDivider_Insert";
        public const String PlanogramBlockingDividerUpdateById = "Content.PlanogramBlockingDivider_UpdateById";
        public const String PlanogramBlockingDividerDeleteById = "Content.PlanogramBlockingDivider_DeleteById";

        #endregion

        #region PlanogramBlockingGroup

        public const String PlanogramBlockingGroupFetchByPlanogramBlockingId = "Content.PlanogramBlockingGroup_FetchByPlanogramBlockingId";
        public const String PlanogramBlockingGroupInsert = "Content.PlanogramBlockingGroup_Insert";
        public const String PlanogramBlockingGroupUpdateById = "Content.PlanogramBlockingGroup_UpdateById";
        public const String PlanogramBlockingGroupDeleteById = "Content.PlanogramBlockingGroup_DeleteById";

        #endregion

        #region PlanogramBlockingLocation

        public const String PlanogramBlockingLocationFetchByPlanogramBlockingId = "Content.PlanogramBlockingLocation_FetchByPlanogramBlockingId";
        public const String PlanogramBlockingLocationInsert = "Content.PlanogramBlockingLocation_Insert";
        public const String PlanogramBlockingLocationUpdateById = "Content.PlanogramBlockingLocation_UpdateById";
        public const String PlanogramBlockingLocationDeleteById = "Content.PlanogramBlockingLocation_DeleteById";

        #endregion

        #region PlanogramComparisonTemplateInfo

        public const String PlanogramComparisonTemplateInfoFetchByIds = "MasterData.PlanogramComparisonTemplateInfo_FetchByIds";
        public const String PlanogramComparisonTemplateInfoFetchByEntityId = "MasterData.PlanogramComparisonTemplateInfo_FetchByEntityId";
        public const String PlanogramComparisonTemplateInfoFetchByEntityIdIncludingDeleted = "MasterData.PlanogramComparisonTemplateInfo_FetchByEntityIdIncludingDeleted";

        #endregion

        #region PlanogramComparisonTemplate

        public const String PlanogramComparisonTemplateFetchById = "MasterData.PlanogramComparisonTemplate_FetchById";
        public const String PlanogramComparisonTemplateFetchByEntityIdName = "MasterData.PlanogramComparisonTemplate_FetchByEntityIdName";
        public const String PlanogramComparisonTemplateInsert = "MasterData.PlanogramComparisonTemplate_Insert";
        public const String PlanogramComparisonTemplateUpdateById = "MasterData.PlanogramComparisonTemplate_UpdateById";
        public const String PlanogramComparisonTemplateDeleteById = "MasterData.PlanogramComparisonTemplate_DeleteById";

        #endregion

        #region PlanogramComparisonTemplateField

        public const String PlanogramComparisonTemplateFieldFetchById = "MasterData.PlanogramComparisonTemplateField_FetchById";
        public const String PlanogramComparisonTemplateFieldFetchByPlanogramComparisonTemplateId = "MasterData.PlanogramComparisonTemplateField_FetchByPlanogramComparisonTemplateId";
        public const String PlanogramComparisonTemplateFieldInsert = "MasterData.PlanogramComparisonTemplateField_Insert";
        public const String PlanogramComparisonTemplateFieldUpdateById = "MasterData.PlanogramComparisonTemplateField_UpdateById";
        public const String PlanogramComparisonTemplateFieldDeleteById = "MasterData.PlanogramComparisonTemplateField_DeleteById";

        #endregion

        #region PlanogramComparison

        public const String PlanogramComparisonFetchByPlanogramId = "Content.PlanogramComparison_FetchByPlanogramId";
        public const String PlanogramComparisonInsert = "Content.PlanogramComparison_Insert";
        public const String PlanogramComparisonUpdateById = "Content.PlanogramComparison_UpdateById";
        public const String PlanogramComparisonDeleteById = "Content.PlanogramComparison_DeleteById";

        #endregion

        #region PlanogramComparisonField

        public const String PlanogramComparisonFieldFetchByPlanogramComparisonId = "Content.PlanogramComparisonField_FetchByPlanogramComparisonId";
        public const String PlanogramComparisonFieldInsert = "Content.PlanogramComparisonField_Insert";
        public const String PlanogramComparisonFieldUpdateById = "Content.PlanogramComparisonField_UpdateById";
        public const String PlanogramComparisonFieldDeleteById = "Content.PlanogramComparisonField_DeleteById";
        public const String PlanogramComparisonFieldBulkInsert = "Content.PlanogramComparisonField_BulkInsert";
        public const String PlanogramComparisonFieldBulkUpdate = "Content.PlanogramComparisonField_BulkUpdate";

        #endregion

        #region PlanogramComparisonResult

        public const String PlanogramComparisonResultFetchByPlanogramComparisonId = "Content.PlanogramComparisonResult_FetchByPlanogramComparisonId";
        public const String PlanogramComparisonResultInsert = "Content.PlanogramComparisonResult_Insert";
        public const String PlanogramComparisonResultUpdateById = "Content.PlanogramComparisonResult_UpdateById";
        public const String PlanogramComparisonResultDeleteById = "Content.PlanogramComparisonResult_DeleteById";
        public const String PlanogramComparisonResultBulkInsert = "Content.PlanogramComparisonResult_BulkInsert";
        public const String PlanogramComparisonResultBulkUpdate = "Content.PlanogramComparisonResult_BulkUpdate";

        #endregion

        #region PlanogramComparisonItem

        public const String PlanogramComparisonItemFetchByPlanogramComparisonResultId = "Content.PlanogramComparisonItem_FetchByPlanogramComparisonResultId";
        public const String PlanogramComparisonItemInsert = "Content.PlanogramComparisonItem_Insert";
        public const String PlanogramComparisonItemUpdateById = "Content.PlanogramComparisonItem_UpdateById";
        public const String PlanogramComparisonItemDeleteById = "Content.PlanogramComparisonItem_DeleteById";
        public const String PlanogramComparisonItemBulkInsert = "Content.PlanogramComparisonItem_BulkInsert";
        public const String PlanogramComparisonItemBulkUpdate = "Content.PlanogramComparisonItem_BulkUpdate";

        #endregion

        #region PlanogramComparisonFieldValue

        public const String PlanogramComparisonFieldValueFetchByPlanogramComparisonItemId = "Content.PlanogramComparisonFieldValue_FetchByPlanogramComparisonItemId";
        public const String PlanogramComparisonFieldValueInsert = "Content.PlanogramComparisonFieldValue_Insert";
        public const String PlanogramComparisonFieldValueUpdateById = "Content.PlanogramComparisonFieldValue_UpdateById";
        public const String PlanogramComparisonFieldValueDeleteById = "Content.PlanogramComparisonFieldValue_DeleteById";
        public const String PlanogramComparisonFieldValueBulkInsert = "Content.PlanogramComparisonFieldValue_BulkInsert";
        public const String PlanogramComparisonFieldValueBulkUpdate = "Content.PlanogramComparisonFieldValue_BulkUpdate";

        #endregion

        #region PlanogramComponent

        public const String PlanogramComponentFetchByPlanogramId = "Content.PlanogramComponent_FetchByPlanogramId";
        public const String PlanogramComponentInsert = "Content.PlanogramComponent_Insert";
        public const String PlanogramComponentBulkInsert = "Content.PlanogramComponent_BulkInsert";
        public const String PlanogramComponentUpdateById = "Content.PlanogramComponent_UpdateById";
        public const String PlanogramComponentDeleteById = "Content.PlanogramComponent_DeleteById";

        #endregion

        #region PlanogramConsumerDecisionTree

        public const String PlanogramConsumerDecisionTreeFetchByPlanogramId = "Content.PlanogramConsumerDecisionTree_FetchByPlanogramId";
        public const String PlanogramConsumerDecisionTreeInsert = "Content.PlanogramConsumerDecisionTree_Insert";
        public const String PlanogramConsumerDecisionTreeUpdateById = "Content.PlanogramConsumerDecisionTree_UpdateById";
        public const String PlanogramConsumerDecisionTreeDeleteById = "Content.PlanogramConsumerDecisionTree_DeleteById";

        #endregion

        #region PlanogramConsumerDecisionTreeLevel

        public const String PlanogramConsumerDecisionTreeLevelFetchByPlanogramConsumerDecisionTreeId = "Content.PlanogramConsumerDecisionTreeLevel_FetchByPlanogramConsumerDecisionTreeId";
        public const String PlanogramConsumerDecisionTreeLevelInsert = "Content.PlanogramConsumerDecisionTreeLevel_Insert";
        public const String PlanogramConsumerDecisionTreeLevelUpdateById = "Content.PlanogramConsumerDecisionTreeLevel_UpdateById";
        public const String PlanogramConsumerDecisionTreeLevelDeleteById = "Content.PlanogramConsumerDecisionTreeLevel_DeleteById";

        #endregion

        #region PlanogramConsumerDecisionTreeNode

        public const String PlanogramConsumerDecisionTreeNodeFetchByPlanogramConsumerDecisionTreeId = "Content.PlanogramConsumerDecisionTreeNode_FetchByPlanogramConsumerDecisionTreeId";
        public const String PlanogramConsumerDecisionTreeNodeInsert = "Content.PlanogramConsumerDecisionTreeNode_Insert";
        public const String PlanogramConsumerDecisionTreeNodeUpdateById = "Content.PlanogramConsumerDecisionTreeNode_UpdateById";
        public const String PlanogramConsumerDecisionTreeNodeDeleteById = "Content.PlanogramConsumerDecisionTreeNode_DeleteById";

        #endregion

        #region PlanogramConsumerDecisionTreeNodeProduct

        public const String PlanogramConsumerDecisionTreeNodeProductFetchByPlanogramConsumerDecisionTreeNodeId = "Content.PlanogramConsumerDecisionTreeNodeProduct_FetchByPlanogramConsumerDecisionTreeNodeId";
        public const String PlanogramConsumerDecisionTreeNodeProductInsert = "Content.PlanogramConsumerDecisionTreeNodeProduct_Insert";
        public const String PlanogramConsumerDecisionTreeNodeProductUpdateById = "Content.PlanogramConsumerDecisionTreeNodeProduct_UpdateById";
        public const String PlanogramConsumerDecisionTreeNodeProductDeleteById = "Content.PlanogramConsumerDecisionTreeNodeProduct_DeleteById";

        #endregion

        #region PlanogramEventLog

        public const String PlanogramEventLogFetchByPlanogramId = "Content.PlanogramEventLog_FetchByPlanogramId";
        public const String PlanogramEventLogInsert = "Content.PlanogramEventLog_Insert";
        public const String PlanogramEventLogUpdateById = "Content.PlanogramEventLog_UpdateById";
        public const String PlanogramEventLogDeleteById = "Content.PlanogramEventLog_DeleteById";

        #endregion

        #region PlanogramEventLogInfo

        public const String PlanogramEventLogInfoFetchByWorkpackageId = "Content.PlanogramEventLogInfo_FetchByWorkpackageId";

        #endregion

        #region PlanogramExportTemplate

        public const String PlanogramExportTemplateFetchById = "MasterData.PlanogramExportTemplate_FetchById";
        public const String PlanogramExportTemplateFetchByEntityIdName = "MasterData.PlanogramExportTemplate_FetchByEntityIdName";
        public const String PlanogramExportTemplateInsert = "MasterData.PlanogramExportTemplate_Insert";
        public const String PlanogramExportTemplateUpdate = "MasterData.PlanogramExportTemplate_Update";
        public const String PlanogramExportTemplateDeleteById = "MasterData.PlanogramExportTemplate_DeleteById";

        #endregion

        #region PlanogramExportTemplateInfo

        public const String PlanogramExportTemplateInfoFetchByEntityId = "MasterData.PlanogramExportTemplateInfo_FetchByEntityId";

        #endregion

        #region PlanogramExportTemplateMapping

        public const String PlanogramExportTemplateMappingFetchByPlanogramExportTemplateId = "MasterData.PlanogramExportTemplateMapping_FetchByPlanogramExportTemplateId";
        public const String PlanogramExportTemplateMappingInsert = "MasterData.PlanogramExportTemplateMapping_Insert";
        public const String PlanogramExportTemplateMappingUpdate = "MasterData.PlanogramExportTemplateMapping_Update";
        public const String PlanogramExportTemplateMappingDeleteById = "MasterData.PlanogramExportTemplateMapping_DeleteById";

        #endregion

        #region PlanogramExportTemplatePerformanceMetric

        public const String PlanogramExportTemplatePerformanceMetricFetchByPlanogramExportTemplateId = "MasterData.PlanogramExportTemplatePerformanceMetric_FetchByPlanogramExportTemplateId";
        public const String PlanogramExportTemplatePerformanceMetricInsert = "MasterData.PlanogramExportTemplatePerformanceMetric_Insert";
        public const String PlanogramExportTemplatePerformanceMetricUpdate = "MasterData.PlanogramExportTemplatePerformanceMetric_Update";
        public const String PlanogramExportTemplatePerformanceMetricDeleteById = "MasterData.PlanogramExportTemplatePerformanceMetric_DeleteById";

        #endregion

        #region PlanogramFixtureAssembly

        public const String PlanogramFixtureAssemblyFetchByPlanogramFixtureId = "Content.PlanogramFixtureAssembly_FetchByPlanogramFixtureId";
        public const String PlanogramFixtureAssemblyInsert = "Content.PlanogramFixtureAssembly_Insert";
        public const String PlanogramFixtureAssemblyUpdateById = "Content.PlanogramFixtureAssembly_UpdateById";
        public const String PlanogramFixtureAssemblyDeleteById = "Content.PlanogramFixtureAssembly_DeleteById";

        #endregion

        #region PlanogramFixtureComponent

        public const String PlanogramFixtureComponentFetchByPlanogramFixtureId = "Content.PlanogramFixtureComponent_FetchByPlanogramFixtureId";
        public const String PlanogramFixtureComponentInsert = "Content.PlanogramFixtureComponent_Insert";
        public const String PlanogramFixtureComponentBulkInsert = "Content.PlanogramFixtureComponent_BulkInsert";
        public const String PlanogramFixtureComponentUpdateById = "Content.PlanogramFixtureComponent_UpdateById";
        public const String PlanogramFixtureComponentDeleteById = "Content.PlanogramFixtureComponent_DeleteById";

        #endregion

        #region ProductLibrary
        public const String ProductLibraryFetchById = "MasterData.ProductLibrary_FetchById";
        public const String ProductLibraryInsert = "MasterData.ProductLibrary_Insert";
        public const String ProductLibraryUpdateById = "MasterData.ProductLibrary_UpdateById";
        public const String ProductLibraryDeleteById = "MasterData.ProductLibrary_DeleteById";
        #endregion

        #region ProductLibraryColumnMapping
        public const String ProductLibraryColumnMappingFetchById = "MasterData.ProductLibraryColumnMapping_FetchById";
        public const String ProductLibraryColumnMappingFetchByProductLibraryId = "MasterData.ProductLibraryColumnMapping_FetchByProductLibraryId";
        public const String ProductLibraryColumnMappingInsert = "MasterData.ProductLibraryColumnMapping_Insert";
        public const String ProductLibraryColumnMappingUpdateById = "MasterData.ProductLibraryColumnMapping_UpdateById";
        public const String ProductLibraryColumnMappingDeleteById = "MasterData.ProductLibraryColumnMapping_DeleteById";
        #endregion

        #region ProductLibraryInfo
        public const String ProductLibraryInfoFetchByIds = "MasterData.ProductLibraryInfo_FetchByIds";
        public const String ProductLibraryInfoFetchByEntityId = "MasterData.ProductLibraryInfo_FetchByEntityId";
        #endregion

        #region PlanogramLock

        public const String PlanogramLockLockByPlanogramId = "Content.PlanogramLock_LockByPlanogramId";
        public const String PlanogramLockUnlockByPlanogramId = "Content.PlanogramLock_UnlockByPlanogramId";
        public const String PlanogramLockDeleteByLockTypeDateLocked = "Content.PlanogramLock_DeleteByLockTypeDateLocked";

        #endregion

        #region PlanogramRenumberingStrategy

        public const String PlanogramRenumberingStrategyFetchByPlanogramId = "Content.PlanogramRenumberingStrategy_FetchByPlanogramId";
        public const String PlanogramRenumberingStrategyInsert = "Content.PlanogramRenumberingStrategy_Insert";
        public const String PlanogramRenumberingStrategyUpdateById = "Content.PlanogramRenumberingStrategy_UpdateById";
        public const String PlanogramRenumberingStrategyDeleteById = "Content.PlanogramRenumberingStrategy_DeleteById";

        #endregion

        #region PlanogramSequence

        public const String PlanogramSequenceFetchByPlanogramId = "Content.PlanogramSequence_FetchByPlanogramId";
        public const String PlanogramSequenceInsert = "Content.PlanogramSequence_Insert";
        public const String PlanogramSequenceUpdateById = "Content.PlanogramSequence_UpdateById";
        public const String PlanogramSequenceDeleteById = "Content.PlanogramSequence_DeleteById";

        #endregion

        #region PlanogramSequenceGroup

        public const String PlanogramSequenceGroupFetchByPlanogramSequenceId = "Content.PlanogramSequenceGroup_FetchByPlanogramSequenceId";
        public const String PlanogramSequenceGroupInsert = "Content.PlanogramSequenceGroup_Insert";
        public const String PlanogramSequenceGroupUpdateById = "Content.PlanogramSequenceGroup_UpdateById";
        public const String PlanogramSequenceGroupDeleteById = "Content.PlanogramSequenceGroup_DeleteById";

        #endregion

        #region PlanogramSequence

        public const String PlanogramSequenceGroupProductFetchByPlanogramSequenceGroupId = "Content.PlanogramSequenceGroupProduct_FetchByPlanogramSequenceGroupId";
        public const String PlanogramSequenceGroupProductInsert = "Content.PlanogramSequenceGroupProduct_Insert";
        public const String PlanogramSequenceGroupProductUpdateById = "Content.PlanogramSequenceGroupProduct_UpdateById";
        public const String PlanogramSequenceGroupProductDeleteById = "Content.PlanogramSequenceGroupProduct_DeleteById";

        #endregion

        #region PlanogramSequenceGroupSubGroup

        public const String PlanogramSequenceGroupSubGroupFetchByPlanogramSequenceGroupId = "Content.PlanogramSequenceGroupSubGroup_FetchByPlanogramSequenceGroupId";
        public const String PlanogramSequenceGroupSubGroupInsert = "Content.PlanogramSequenceGroupSubGroup_Insert";
        public const String PlanogramSequenceGroupSubGroupUpdateById = "Content.PlanogramSequenceGroupSubGroup_UpdateById";
        public const String PlanogramSequenceGroupSubGroupDeleteById = "Content.PlanogramSequenceGroupSubGroup_DeleteById";

        #endregion

        #region ProductImage

        public const String ProductImageDeleteById = "MasterData.ProductImage_DeleteById";
        public const String ProductImageFetchByEntityId = "MasterData.ProductImage_FetchByEntityId";
        public const String ProductImageFetchByProductId = "MasterData.ProductImage_FetchByProductId";
        public const String ProductImageFetchByProductIdImageType = "MasterData.ProductImage_FetchByProductIdImageType";
        public const String ProductImageFetchByProductIdImageTypeFacingTypeCompressionId = "MasterData.ProductImage_FetchByProductIdImageTypeFacingTypeCompressionId";
        public const String ProductImageFetchById = "MasterData.ProductImage_FetchById";
        public const String ProductImageInsert = "MasterData.ProductImage_Insert";

        #endregion

        #region Product

        public const String ProductFetchById = "MasterData.Product_FetchById";
        public const String ProductFetchByEntityId = "MasterData.Product_FetchByEntityId";
        public const String ProductInsert = "MasterData.Product_Insert";
        public const String ProductUpdateById = "MasterData.Product_UpdateById";
        public const String ProductBulkUpsert = "MasterData.Product_BulkUpsert";
        public const String ProductDeleteById = "MasterData.Product_DeleteById";
        public const String ProductDeleteByEntityId = "MasterData.Product_DeleteByEntityId";
        public const String ProductFetchByProductGroupId = "MasterData.Product_FetchByProductGroupId";
        public const String ProductFetchByProductIds = "MasterData.Product_FetchByProductIds";
        public const String ProductFetchByProductUniverseId = "MasterData.Product_FetchByProductUniverseId";
        public const String ProductFetchByEntityIdProductGtins = "MasterData.Product_FetchByEntityIdProductGtins";
        public const String ProductFetchByEntityIdSearchText = "MasterData.Product_FetchByEntityIdSearchText";
        public const String ProductFetchByMerchandisingGroupId = "MasterData.Product_FetchByMerchandisingGroupId";
        public const String ProductFetchByEntityIdMultipleSearchText = "MasterData.Product_FetchByEntityIdMultipleSearchText";

        #endregion

        #region ProductAttributeComparisonResult

        public const String ProductAttributeComparisonResultFetchByPlanogramId = "Content.ProductComparisonAttributeResults_FetchByPlanogramId";
        public const String ProductAttributeComparisonResultFetchById = "Content.ProductComparisonAttributeResults_FetchById";
        public const String ProductAttributeComparisonResultInsert = "Content.ProductComparisonAttributeResults_Insert";
        public const String ProductAttributeComparisonResultUpdate = "Content.ProductComparisonAttributeResults_Update";
        public const String ProductAttributeComparisonResultDeleteById = "Content.ProductComparisonAttributeResults_DeleteById";
        public const String ProductAttributeComparisonResultDeleteByPlanogramId = "Content.ProductComparisonAttributeResults_DeleteByPlanogramId";
        public const String ProductAttributeComparisonResultBulkInsert = "Content.ProductComparisonAttributeResults_BulkInsert";
        public const String ProductAttributeComparisonResultBulkUpdate = "Content.ProductComparisonAttributeResults_BulkUpdate";

        #endregion

        #region ProductGroup

        public const String ProductGroupDeleteById = "MasterData.ProductGroup_DeleteById";
        public const String ProductGroupFetchById = "MasterData.ProductGroup_FetchById";
        public const String ProductGroupFetchByProductHierarchyId = "MasterData.ProductGroup_FetchByProductHierarchyId";
        public const String ProductGroupFetchByProductHierarchyIdIncludingDeleted = "MasterData.ProductGroup_FetchByProductHierarchyIdIncludingDeleted";
        public const String ProductGroupInsert = "MasterData.ProductGroup_Insert";
        public const String ProductGroupUpdate = "MasterData.ProductGroup_UpdateById";
        public const String ProductGroupBulkUpsert = "MasterData.ProductGroup_BulkUpsert";

        #endregion

        #region ProductGroupInfo
        public const String ProductGroupInfoFetchDeletedByProductHierarchyId = "MasterData.ProductGroupInfo_FetchDeletedByProductHierarchyId";
        public const String ProductGroupInfoFetchByProductGroupIds = "MasterData.ProductGroupInfo_FetchByProductGroupIds";
        public const String ProductGroupInfoFetchByProductGroupCodes = "MasterData.ProductGroupInfo_FetchByProductGroupCodes";

        #endregion

        #region ProductHierarchy

        public const String ProductHierarchyFetchById = "MasterData.ProductHierarchy_FetchById";
        public const String ProductHierarchyFetchByEntityId = "MasterData.ProductHierarchy_FetchByEntityId";
        public const String ProductHierarchyInsert = "MasterData.ProductHierarchy_Insert";
        public const String ProductHierarchyUpdateById = "MasterData.ProductHierarchy_UpdateById";
        public const String ProductHierarchyDeleteById = "MasterData.ProductHierarchy_DeleteById";

        #endregion

        #region ProductInfo

        public const String ProductInfoFetchByEntityId = "MasterData.ProductInfo_FetchByEntityId";
        public const String ProductInfoFetchByEntityIdIncludingDeleted = "MasterData.ProductInfo_FetchByEntityIdIncludingDeleted";
        public const String ProductInfoFetchByProductIds = "MasterData.ProductInfo_FetchByProductIds";
        public const String ProductInfoFetchByEntityIdProductGtins = "MasterData.ProductInfo_FetchByEntityIdProductGtins";
        public const String ProductInfoFetchByProductUniverseId = "MasterData.ProductInfo_FetchByProductUniverseId";
        public const String ProductInfoFetchByEntityIdSearchCriteria = "MasterData.ProductInfo_FetchByEntityIdSearchCriteria";
        public const String ProductInfoFetchByEntityIdProductIds = "MasterData.ProductInfo_FetchByEntityIdProductIds";
        public const String ProductInfoFetchByMerchandisingGroupId = "MasterData.ProductInfo_FetchByMerchandisingGroupId";
        public const String ProductInfoFetchAllIncludingDeleted = "MasterData.ProductInfo_FetchAllIncludingDeleted";
        public const String ProductInfoFetchProductInfoIllegalsByLocationCodeProductGroupCode = "MasterData.ProductInfo_FetchProductInfoIllegalsByLocationCodeProductGroupCode";
        public const String ProductInfoFetchProductInfoLegalsByLocationCodeProductGroupCode = "MasterData.ProductInfo_FetchProductInfoLegalsByLocationCodeProductGroupCode";

        #endregion

        #region ProductLevel

        public const String ProductLevelDeleteById = "MasterData.ProductLevel_DeleteById";
        public const String ProductLevelFetchById = "MasterData.ProductLevel_FetchById";
        public const String ProductLevelFetchByProductHierarchyId = "MasterData.ProductLevel_FetchByProductHierarchyId";
        public const String ProductLevelInsert = "MasterData.ProductLevel_Insert";
        public const String ProductLevelUpdate = "MasterData.ProductLevel_UpdateById";

        #endregion

        #region PlanogramFixture

        public const String PlanogramFixtureFetchByPlanogramId = "Content.PlanogramFixture_FetchByPlanogramId";
        public const String PlanogramFixtureInsert = "Content.PlanogramFixture_Insert";
        public const String PlanogramFixtureUpdateById = "Content.PlanogramFixture_UpdateById";
        public const String PlanogramFixtureDeleteById = "Content.PlanogramFixture_DeleteById";

        #endregion

        #region PlanogramFixtureItem

        public const String PlanogramFixtureItemFetchByPlanogramId = "Content.PlanogramFixtureItem_FetchByPlanogramId";
        public const String PlanogramFixtureItemInsert = "Content.PlanogramFixtureItem_Insert";
        public const String PlanogramFixtureItemUpdateById = "Content.PlanogramFixtureItem_UpdateById";
        public const String PlanogramFixtureItemDeleteById = "Content.PlanogramFixtureItem_DeleteById";

        #endregion

        #region PlanogramGroup

        public const String PlanogramGroupFetchById = "MasterData.PlanogramGroup_FetchById";
        public const String PlanogramGroupFetchByPlanogramHierarchyId = "MasterData.PlanogramGroup_FetchByPlanogramHierarchyId";
        public const String PlanogramGroupInsert = "MasterData.PlanogramGroup_Insert";
        public const String PlanogramGroupUpdateById = "MasterData.PlanogramGroup_UpdateById";
        public const String PlanogramGroupBulkUpsert = "MasterData.PlanogramGroup_BulkUpsert";
        public const String PlanogramGroupDeleteById = "MasterData.PlanogramGroup_DeleteById";

        #endregion

        #region PlanogramGroupPlanogram
        public const String PlanogramGroupPlanogramBulkUpsert = "Content.PlanogramGroupPlanogram_BulkUpsert";
        #endregion

        #region PlanogramHierarchy

        public const String PlanogramHierarchyFetchById = "MasterData.PlanogramHierarchy_FetchById";
        public const String PlanogramHierarchyFetchByEntityId = "MasterData.PlanogramHierarchy_FetchByEntityId";
        public const String PlanogramHierarchyInsert = "MasterData.PlanogramHierarchy_Insert";
        public const String PlanogramHierarchyUpdateById = "MasterData.PlanogramHierarchy_UpdateById";
        public const String PlanogramHierarchyDeleteById = "MasterData.PlanogramHierarchy_DeleteById";
        public const String PlanogramHierarchyFetchPlanogramGroupingsByEntityId = "MasterData.PlanogramHierarchy_FetchPlanogramGroupingsByEntityId";

        #endregion

        #region PlanogramInfo
        public const String PlanogramInfoFetchByWorkpackageId = "Content.PlanogramInfo_FetchByWorkpackageId";
        public const String PlanogramInfoFetchByWorkpackageIdPagingCriteria = "Content.PlanogramInfo_FetchByWorkpackageIdPagingCriteria";
        public const String PlanogramInfoFetchByWorkpackageIdAutomationProcessingStatus = "Content.PlanogramInfo_FetchByWorkpackageIdAutomationProcessingStatus";
        public const String PlanogramInfoFetchByWorkpackageIdAutomationProcessingStatusPagingCriteria = "Content.PlanogramInfo_FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria";
        public const String PlanogramInfoFetchByPlanogramGroupId = "Content.PlanogramInfo_FetchByPlanogramGroupId";
        public const String PlanogramInfoFetchByCategoryCode = "Content.PlanogramInfo_FetchByCategoryCode";
        public const String PlanogramInfoFetchNonDebugByCategoryCode = "Content.PlanogramInfo_FetchNonDebugByCategoryCode";
        public const String PlanogramInfoFetchNonDebugByLocationCode = "Content.PlanogramInfo_FetchNonDebugByLocationCode";
        public const String PlanogramInfoFetchByNullCategoryCode = "Content.PlanogramInfo_FetchByNullCategoryCode";
        public const String PlanogramInfoFetchBySearchCriteria = "Content.PlanogramInfo_FetchBySearchCriteria";
        public const String PlanogramInfoFetchByIds = "Content.PlanogramInfo_FetchByIds";
        #endregion

        #region PlanogramImage

        public const String PlanogramImageFetchByPlanogramId = "Content.PlanogramImage_FetchByPlanogramId";
        public const String PlanogramImageInsert = "Content.PlanogramImage_Insert";
        public const String PlanogramImageUpdateById = "Content.PlanogramImage_UpdateById";
        public const String PlanogramImageDeleteById = "Content.PlanogramImage_DeleteById";

        #endregion

        #region PlanogramMetadataImage

        public const String PlanogramMetadataImageFetchByPlanogramId = "Content.PlanogramMetadataImage_FetchByPlanogramId";
        public const String PlanogramMetadataImageInsert = "Content.PlanogramMetadataImage_Insert";
        public const String PlanogramMetadataImageUpdateById = "Content.PlanogramMetadataImage_UpdateById";
        public const String PlanogramMetadataImageDeleteById = "Content.PlanogramMetadataImage_DeleteById";

        #endregion

        #region PlanogramNameTemplate

        public const String PlanogramNameTemplateFetchById = "MasterData.PlanogramNameTemplate_FetchById";
        public const String PlanogramNameTemplateFetchByEntityIdName = "MasterData.PlanogramNameTemplate_FetchByEntityIdName";
        public const String PlanogramNameTemplateInsert = "MasterData.PlanogramNameTemplate_Insert";
        public const String PlanogramNameTemplateUpdate = "MasterData.PlanogramNameTemplate_Update";
        public const String PlanogramNameTemplateDeleteById = "MasterData.PlanogramNameTemplate_DeleteById";

        #endregion

        #region PlanogramNameTemplateInfo

        public const String PlanogramNameTemplateInfoFetchByEntityId = "MasterData.PlanogramNameTemplateInfo_FetchByEntityId";

        #endregion

        #region PlanogramImportTemplate

        public const String PlanogramImportTemplateFetchById = "MasterData.PlanogramImportTemplate_FetchById";
        public const String PlanogramImportTemplateFetchByEntityIdName = "MasterData.PlanogramImportTemplate_FetchByEntityIdName";
        public const String PlanogramImportTemplateInsert = "MasterData.PlanogramImportTemplate_Insert";
        public const String PlanogramImportTemplateUpdate = "MasterData.PlanogramImportTemplate_Update";
        public const String PlanogramImportTemplateDeleteById = "MasterData.PlanogramImportTemplate_DeleteById";

        #endregion

        #region PlanogramImportTemplateInfo

        public const String PlanogramImportTemplateInfoFetchByEntityId = "MasterData.PlanogramImportTemplateInfo_FetchByEntityId";

        #endregion

        #region PlanogramImportTemplateMapping

        public const String PlanogramImportTemplateMappingFetchByPlanogramImportTemplateId = "MasterData.PlanogramImportTemplateMapping_FetchByPlanogramImportTemplateId";
        public const String PlanogramImportTemplateMappingInsert = "MasterData.PlanogramImportTemplateMapping_Insert";
        public const String PlanogramImportTemplateMappingUpdate = "MasterData.PlanogramImportTemplateMapping_Update";
        public const String PlanogramImportTemplateMappingDeleteById = "MasterData.PlanogramImportTemplateMapping_DeleteById";

        #endregion

        #region PlanogramImportTemplatePerformanceMetric

        public const String PlanogramImportTemplatePerformanceMetricFetchByPlanogramImportTemplateId = "MasterData.PlanogramImportTemplatePerformanceMetric_FetchByPlanogramImportTemplateId";
        public const String PlanogramImportTemplatePerformanceMetricInsert = "MasterData.PlanogramImportTemplatePerformanceMetric_Insert";
        public const String PlanogramImportTemplatePerformanceMetricUpdate = "MasterData.PlanogramImportTemplatePerformanceMetric_Update";
        public const String PlanogramImportTemplatePerformanceMetricDeleteById = "MasterData.PlanogramImportTemplatePerformanceMetric_DeleteById";

        #endregion

        #region PlanogramInventory

        public const String PlanogramInventoryFetchByPlanogramId = "Content.PlanogramInventory_FetchByPlanogramId";
        public const String PlanogramInventoryInsert = "Content.PlanogramInventory_Insert";
        public const String PlanogramInventoryUpdateById = "Content.PlanogramInventory_UpdateById";
        public const String PlanogramInventoryDeleteById = "Content.PlanogramInventory_DeleteById";

        #endregion

        #region PlanogramPerformance

        public const String PlanogramPerformanceFetchByPlanogramId = "Content.PlanogramPerformance_FetchByPlanogramId";
        public const String PlanogramPerformanceInsert = "Content.PlanogramPerformance_Insert";
        public const String PlanogramPerformanceUpdateById = "Content.PlanogramPerformance_UpdateById";
        public const String PlanogramPerformanceDeleteById = "Content.PlanogramPerformance_DeleteById";

        #endregion

        #region PlanogramPerformanceData

        public const String PlanogramPerformanceDataFetchByPlanogramPerformanceId = "Content.PlanogramPerformanceData_FetchByPlanogramPerformanceId";
        public const String PlanogramPerformanceDataInsert = "Content.PlanogramPerformanceData_Insert";
        public const String PlanogramPerformanceDataBulkInsert = "Content.PlanogramPerformanceData_BulkInsert";
        public const String PlanogramPerformanceDataUpdateById = "Content.PlanogramPerformanceData_UpdateById";
        public const String PlanogramPerformanceDataBulkUpdate = "Content.PlanogramPerformanceData_BulkUpdate";
        public const String PlanogramPerformanceDataDeleteById = "Content.PlanogramPerformanceData_DeleteById";

        #endregion

        #region PlanogramPerformanceMetric

        public const String PlanogramPerformanceMetricFetchByPlanogramPerformanceId = "Content.PlanogramPerformanceMetric_FetchByPlanogramPerformanceId";
        public const String PlanogramPerformanceMetricInsert = "Content.PlanogramPerformanceMetric_Insert";
        public const String PlanogramPerformanceMetricUpdateById = "Content.PlanogramPerformanceMetric_UpdateById";
        public const String PlanogramPerformanceMetricDeleteById = "Content.PlanogramPerformanceMetric_DeleteById";

        #endregion

        #region PlanogramPosition

        public const String PlanogramPositionFetchByPlanogramId = "Content.PlanogramPosition_FetchByPlanogramId";
        public const String PlanogramPositionInsert = "Content.PlanogramPosition_Insert";
        public const String PlanogramPositionBulkInsert = "Content.PlanogramPosition_BulkInsert";
        public const String PlanogramPositionUpdateById = "Content.PlanogramPosition_UpdateById";
        public const String PlanogramPositionBulkUpdate = "Content.PlanogramPosition_BulkUpdate";
        public const String PlanogramPositionDeleteById = "Content.PlanogramPosition_DeleteById";

        #endregion

        #region PlanogramProcessingStatus

        public const String PlanogramProcessingStatusFetchByPlanogramId = "Content.PlanogramProcessingStatus_FetchByPlanogramId";
        public const String PlanogramProcessingStatusUpdateByPlanogramId = "Content.PlanogramProcessingStatus_UpdateByPlanogramId";
        public const String PlanogramProcessingStatusIncrementMetaDataStatusByPlanogramId = "Content.PlanogramProcessingStatus_IncrementMetaDataStatusByPlanogramId";
        public const String PlanogramProcessingStatusIncrementValidationStatusByPlanogramId = "Content.PlanogramProcessingStatus_IncrementValidationStatusByPlanogramId";
        #endregion

        #region PlanogramProduct

        public const String PlanogramProductFetchByPlanogramId = "Content.PlanogramProduct_FetchByPlanogramId";
        public const String PlanogramProductInsert = "Content.PlanogramProduct_Insert";
        public const String PlanogramProductBulkInsert = "Content.PlanogramProduct_BulkInsert";
        public const String PlanogramProductUpdateById = "Content.PlanogramProduct_UpdateById";
        public const String PlanogramProductBulkUpdate = "Content.PlanogramProduct_BulkUpdate";
        public const String PlanogramProductDeleteById = "Content.PlanogramProduct_DeleteById";

        #endregion

        #region PlanogramSubComponent

        public const String PlanogramSubComponentFetchByPlanogramComponentId = "Content.PlanogramSubComponent_FetchByPlanogramComponentId";
        public const String PlanogramSubComponentInsert = "Content.PlanogramSubComponent_Insert";
        public const String PlanogramSubComponentBulkInsert = "Content.PlanogramSubComponent_BulkInsert";
        public const String PlanogramSubComponentUpdateById = "Content.PlanogramSubComponent_UpdateById";
        public const String PlanogramSubComponentDeleteById = "Content.PlanogramSubComponent_DeleteById";

        #endregion

        #region PlanogramValidationInfo

        public const String PlanogramValidationGroupInfoFetchByPlanogramIds = "Content.PlanogramValidationGroupInfo_FetchByPlanogramIds";

        #endregion

        #region PlanogramValidationInfo

        public const String PlanogramValidationInfoFetchByPlanogramIds = "Content.PlanogramValidationInfo_FetchByPlanogramIds";

        #endregion

        #region PlanogramValidationInfo

        public const String PlanogramValidationMetricInfoFetchByPlanogramIds = "Content.PlanogramValidationMetricInfo_FetchByPlanogramIds";

        #endregion

        #region PlanogramValidationTemplate

        public const String PlanogramValidationTemplateFetchByPlanogramId = "Content.PlanogramValidationTemplate_FetchByPlanogramId";
        public const String PlanogramValidationTemplateInsert = "Content.PlanogramValidationTemplate_Insert";
        public const String PlanogramValidationTemplateUpdateById = "Content.PlanogramValidationTemplate_UpdateById";
        public const String PlanogramValidationTemplateDeleteById = "Content.PlanogramValidationTemplate_DeleteById";

        #endregion

        #region PlanogramValidationTemplateGroup

        public const String PlanogramValidationTemplateGroupFetchByPlanogramValidationTemplateId = "Content.PlanogramValidationTemplateGroup_FetchByPlanogramValidationTemplateId";
        public const String PlanogramValidationTemplateGroupInsert = "Content.PlanogramValidationTemplateGroup_Insert";
        public const String PlanogramValidationTemplateGroupUpdateById = "Content.PlanogramValidationTemplateGroup_UpdateById";
        public const String PlanogramValidationTemplateGroupDeleteById = "Content.PlanogramValidationTemplateGroup_DeleteById";

        #endregion

        #region PlanogramValidationTemplateGroupMetric

        public const String PlanogramValidationTemplateGroupMetricFetchByPlanogramValidationTemplateGroupId = "Content.PlanogramValidationTemplateGroupMetric_FetchByPlanogramValidationTemplateGroupId";
        public const String PlanogramValidationTemplateGroupMetricInsert = "Content.PlanogramValidationTemplateGroupMetric_Insert";
        public const String PlanogramValidationTemplateGroupMetricUpdateById = "Content.PlanogramValidationTemplateGroupMetric_UpdateById";
        public const String PlanogramValidationTemplateGroupMetricDeleteById = "Content.PlanogramValidationTemplateGroupMetric_DeleteById";

        #endregion

        #region PrintTemplate
        public const String PrintTemplateFetchById = "MasterData.PrintTemplate_FetchById";
        public const String PrintTemplateInsert = "MasterData.PrintTemplate_Insert";
        public const String PrintTemplateUpdateById = "MasterData.PrintTemplate_UpdateById";
        public const String PrintTemplateDeleteById = "MasterData.PrintTemplate_DeleteById";
        #endregion

        #region PrintTemplateComponent
        public const String PrintTemplateComponentFetchById = "MasterData.PrintTemplateComponent_FetchById";
        public const String PrintTemplateComponentFetchByPrintTemplateSectionId = "MasterData.PrintTemplateComponent_FetchByPrintTemplateSectionId";
        public const String PrintTemplateComponentInsert = "MasterData.PrintTemplateComponent_Insert";
        public const String PrintTemplateComponentUpdateById = "MasterData.PrintTemplateComponent_UpdateById";
        public const String PrintTemplateComponentDeleteById = "MasterData.PrintTemplateComponent_DeleteById";
        #endregion

        #region PrintTemplateComponentDetail
        public const String PrintTemplateComponentDetailFetchById = "MasterData.PrintTemplateComponentDetail_FetchById";
        public const String PrintTemplateComponentDetailFetchByPrintTemplateComponentId = "MasterData.PrintTemplateComponentDetail_FetchByPrintTemplateComponentId";
        public const String PrintTemplateComponentDetailInsert = "MasterData.PrintTemplateComponentDetail_Insert";
        public const String PrintTemplateComponentDetailUpdateById = "MasterData.PrintTemplateComponentDetail_UpdateById";
        public const String PrintTemplateComponentDetailDeleteById = "MasterData.PrintTemplateComponentDetail_DeleteById";
        public const String PrintTemplateComponentDetailDeleteByPrintTemplateComponentId = "MasterData.PrintTemplateComponentDetail_DeleteByPrintTemplateComponentId";
        #endregion

        #region PrintTemplateInfo
        public const String PrintTemplateInfoFetchByIds = "MasterData.PrintTemplateInfo_FetchByIds";
        public const String PrintTemplateInfoByEntityId = "MasterData.PrintTemplateInfo_FetchByEntityId";
        #endregion

        #region PrintTemplateSection
        public const String PrintTemplateSectionFetchById = "MasterData.PrintTemplateSection_FetchById";
        public const String PrintTemplateSectionFetchByPrintTemplateSectionGroupId = "MasterData.PrintTemplateSection_FetchByPrintTemplateSectionGroupId";
        public const String PrintTemplateSectionInsert = "MasterData.PrintTemplateSection_Insert";
        public const String PrintTemplateSectionUpdateById = "MasterData.PrintTemplateSection_UpdateById";
        public const String PrintTemplateSectionDeleteById = "MasterData.PrintTemplateSection_DeleteById";
        #endregion

        #region PrintTemplateSectionGroup
        public const String PrintTemplateSectionGroupFetchById = "MasterData.PrintTemplateSectionGroup_FetchById";
        public const String PrintTemplateSectionGroupFetchByPrintTemplateId = "MasterData.PrintTemplateSectionGroup_FetchByPrintTemplateId";
        public const String PrintTemplateSectionGroupInsert = "MasterData.PrintTemplateSectionGroup_Insert";
        public const String PrintTemplateSectionGroupUpdateById = "MasterData.PrintTemplateSectionGroup_UpdateById";
        public const String PrintTemplateSectionGroupDeleteById = "MasterData.PrintTemplateSectionGroup_DeleteById";
        #endregion

        #region ProductUniverse

        public const String ProductUniverseFetchById = "MasterData.ProductUniverse_FetchById";
        public const String ProductUniverseFetchByEntityIdName = "MasterData.ProductUniverse_FetchByEntityIdName";
        public const String ProductUniverseFetchByEntityId = "MasterData.ProductUniverse_FetchByEntityId";
        public const String ProductUniverseInsert = "MasterData.ProductUniverse_Insert";
        public const String ProductUniverseUpdateById = "MasterData.ProductUniverse_UpdateById";
        public const String ProductUniverseBulkUpsert = "MasterData.ProductUniverse_BulkUpsert";
        public const String ProductUniverseDeleteById = "MasterData.ProductUniverse_DeleteById";
        public const String ProductUniverseDeleteByEntityId = "MasterData.ProductUniverse_DeleteByEntityId";
        public const String ProductUniverseUpdateProductUniverses = "MasterData.ProductUniverse_UpdateProductUniverses";

        #endregion

        #region ProductUniverseInfo
        public const String ProductUniverseInfoFetchByEntityId = "MasterData.ProductUniverseInfo_FetchByEntityId";
        public const String ProductUniverseInfoFetchByProductUniverseIds = "MasterData.ProductUniverseInfo_FetchByProductUniverseIds";
        public const String ProductUniverseInfoFetchByProductGroupId = "MasterData.ProductUniverseInfo_FetchByProductGroupId";
        #endregion

        #region ProductUniverseProduct

        public const String ProductUniverseProductFetchByProductUniverseId = "MasterData.ProductUniverseProduct_FetchByProductUniverseId";
        public const String ProductUniverseProductFetchById = "MasterData.ProductUniverseProduct_FetchById";
        public const String ProductUniverseProductInsert = "MasterData.ProductUniverseProduct_Insert";
        public const String ProductUniverseProductUpdateById = "MasterData.ProductUniverseProduct_UpdateById";
        public const String ProductUniverseProductBulkUpsert = "MasterData.ProductUniverseProduct_BulkUpsert";
        public const String ProductUniverseProductDeleteById = "MasterData.ProductUniverseProduct_DeleteById";
        #endregion

        #region RenumberingStrategy

        public const String RenumberingStrategyFetchById = "MasterData.RenumberingStrategy_FetchById";
        public const String RenumberingStrategyFetchByEntityId = "MasterData.RenumberingStrategy_FetchByEntityId";
        public const String RenumberingStrategyFetchByEntityIdName = "MasterData.RenumberingStrategy_FetchByEntityIdName";
        public const String RenumberingStrategyFetchByEntityIdIncludingDeleted = "MasterData.RenumberingStrategy_FetchByEntityIdIncludingDeleted";
        public const String RenumberingStrategyInsert = "MasterData.RenumberingStrategy_Insert";
        public const String RenumberingStrategyUpdateById = "MasterData.RenumberingStrategy_UpdateById";
        public const String RenumberingStrategyDeleteById = "MasterData.RenumberingStrategy_DeleteById";

        #endregion

        #region Role

        public const String RoleDelete = "System.Role_DeleteById";
        public const String RoleFetchByEntityId = "System.Role_FetchByEntityId";
        public const String RoleFetchById = "System.Role_FetchById";
        public const String RoleUpdate = "System.Role_UpdateById";
        public const String RoleInsert = "System.Role_Insert";

        #endregion

        #region RoleInfo

        public const String RoleInfoFetchByEntityId = "System.RoleInfo_FetchByEntityId";
        public const String RoleInfoFetchAll = "System.RoleInfo_FetchAll";
        public const String RoleInfoFetchById = "System.RoleInfo_FetchById";
        #endregion

        #region RoleEntity

        public const String RoleEntityDelete = "System.RoleEntity_DeleteById";
        public const String RoleEntityFetchByEntityId = "System.RoleEntity_FetchByEntityId";
        public const String RoleEntityFetchByRoleId = "System.RoleEntity_FetchByRoleId";
        public const String RoleEntityFetchById = "System.RoleEntity_FetchById";
        public const String RoleEntityFetchAll = "System.RoleEntity_FetchAll";
        public const String RoleEntityInsert = "System.RoleEntity_Insert";

        #endregion

        #region RoleMember

        public const String RoleMemberDelete = "System.RoleMember_DeleteById";
        public const String RoleMemberFetchAll = "System.RoleMember_FetchAll ";
        public const String RoleMemberFetchByRoleId = "System.RoleMember_FetchByRoleId";
        public const String RoleMemberFetchByUserId = "System.RoleMember_FetchByUserId";
        public const String RoleMemberFetchById = "System.RoleMember_FetchById";
        public const String RoleMemberUpdate = "System.RoleMember_UpdateById";
        public const String RoleMemberInsert = "System.RoleMember_Insert";

        #endregion

        #region RolePermission

        public const String RolePermissionDelete = "System.RolePermission_DeleteById";
        public const String RolePermissionFetchAll = "System.RolePermission_FetchAll ";
        public const String RolePermissionFetchByRoleId = "System.RolePermission_FetchByRoleId";
        public const String RolePermissionFetchById = "System.RolePermission_FetchById";
        public const String RolePermissionUpdate = "System.RolePermission_UpdateById";
        public const String RolePermissionInsert = "System.RolePermission_Insert";

        #endregion

        #region SchemaVersion

        public const String SchemaVersionFetchAll = "System.SchemaVersion_FetchAll";

        #endregion

        #region SystemSettings
        public const String SystemSettingsFetchById = "System.SystemSettings_FetchById";
        public const String SystemSettingsFetchByEntityId = "System.SystemSettings_FetchByEntityId";
        public const String SystemSettingsFetchDefault = "System.SystemSettings_FetchDefault";
        public const String SystemSettingsInsert = "System.SystemSettings_Insert";
        public const String SystemSettingsUpdateById = "System.SystemSettings_UpdateById";
        public const String SystemSettingsDeleteById = "System.SystemSettings_DeleteById";
        #endregion

        #region User

        public const String UserFetchById = "System.User_FetchById";
        public const String UserFetchByUserName = "System.User_FetchByUserName";
        public const String UserInsert = "System.User_Insert";
        public const String UserUpdateById = "System.User_UpdateById";
        public const String UserDeleteById = "System.User_DeleteById";

        #endregion

        #region UserInfo

        public const String UserInfoFetchAll = "System.UserInfo_FetchAll";

        #endregion

        #region UserPlanogramGroup

        public const String UserPlanogramGroupFetchById = "System.UserPlanogramGroup_FetchById";
        public const String UserPlanogramGroupFetchByUserId = "System.UserPlanogramGroup_FetchByUserId";
        public const String UserPlanogramGroupInsert = "System.UserPlanogramGroup_Insert";
        public const String UserPlanogramGroupUpdateById = "System.UserPlanogramGroup_UpdateById";
        public const String UserPlanogramGroupDeleteById = "System.UserPlanogramGroup_DeleteById";

        #endregion

        #region ValidationTemplate

        public const String ValidationTemplateFetchById = "MasterData.ValidationTemplate_FetchById";
        public const String ValidationTemplateFetchByEntityIdName = "MasterData.ValidationTemplate_FetchByEntityIdName";
        public const String ValidationTemplateInsert = "MasterData.ValidationTemplate_Insert";
        public const String ValidationTemplateUpdateById = "MasterData.ValidationTemplate_UpdateById";
        public const String ValidationTemplateDeleteById = "MasterData.ValidationTemplate_DeleteById";

        #endregion

        #region ValidationTemplateInfo

        public const String ValidationTemplateInfoFetchByEntityId = "MasterData.ValidationTemplateInfo_FetchByEntityId";

        #endregion

        #region ValidationTemplateGroup

        public const String ValidationTemplateGroupFetchByValidationTemplateId = "MasterData.ValidationTemplateGroup_FetchByValidationTemplateId";
        public const String ValidationTemplateGroupInsert = "MasterData.ValidationTemplateGroup_Insert";
        public const String ValidationTemplateGroupUpdateById = "MasterData.ValidationTemplateGroup_UpdateById";
        public const String ValidationTemplateGroupDeleteById = "MasterData.ValidationTemplateGroup_DeleteById";

        #endregion

        #region ValidationTemplateGroupMetric

        public const String ValidationTemplateGroupMetricFetchByValidationTemplateGroupId = "MasterData.ValidationTemplateGroupMetric_FetchByValidationTemplateGroupId";
        public const String ValidationTemplateGroupMetricInsert = "MasterData.ValidationTemplateGroupMetric_Insert";
        public const String ValidationTemplateGroupMetricUpdateById = "MasterData.ValidationTemplateGroupMetric_UpdateById";
        public const String ValidationTemplateGroupMetricDeleteById = "MasterData.ValidationTemplateGroupMetric_DeleteById";

        #endregion

        #region Workflow

        public const String WorkflowFetchById = "Content.Workflow_FetchById";
        public const String WorkflowInsert = "Content.Workflow_Insert";
        public const String WorkflowUpdateById = "Content.Workflow_UpdateById";
        public const String WorkflowDeleteById = "Content.Workflow_DeleteById";

        #endregion

        #region WorkflowInfo

        public const String WorkflowInfoFetchByEntityId = "Content.WorkflowInfo_FetchByEntityId";
        public const String WorkflowInfoFetchByEntityIdIncludingDeleted = "Content.WorkflowInfo_FetchByEntityIdIncludingDeleted";
        public const String WorkflowInfoFetchById = "Content.WorkflowInfo_FetchById";

        #endregion

        #region WorkflowTask

        public const String WorkflowTaskFetchByWorkflowId = "Content.WorkflowTask_FetchByWorkflowId";
        public const String WorkflowTaskInsert = "Content.WorkflowTask_Insert";
        public const String WorkflowTaskUpdateById = "Content.WorkflowTask_UpdateById";
        public const String WorkflowTaskDeleteById = "Content.WorkflowTask_DeleteById";

        #endregion

        #region WorkflowTaskInfo

        public const String WorkflowTaskInfoFetchByWorkflowId = "Content.WorkflowTaskInfo_FetchByWorkflowId";
        public const String WorkflowTaskInfoFetchByWorkpackageId = "Content.WorkflowTaskInfo_FetchByWorkpackageId";

        #endregion

        #region WorkflowTaskParameter

        public const String WorkflowTaskParameterFetchByWorkflowTaskId = "Content.WorkflowTaskParameter_FetchByWorkflowTaskId";
        public const String WorkflowTaskParameterFetchByWorkflowId = "Content.WorkflowTaskParameter_FetchByWorkflowId";
        public const String WorkflowTaskParameterInsert = "Content.WorkflowTaskParameter_Insert";
        public const String WorkflowTaskParameterUpdateById = "Content.WorkflowTaskParameter_UpdateById";
        public const String WorkflowTaskParameterDeleteById = "Content.WorkflowTaskParameter_DeleteById";

        #endregion

        #region WorkflowTaskParameterValue

        public const String WorkflowTaskParameterValueFetchByWorkflowTaskParameterId = "Content.WorkflowTaskParameterValue_FetchByWorkflowTaskParameterId";
        public const String WorkflowTaskParameterValueInsert = "Content.WorkflowTaskParameterValue_Insert";
        public const String WorkflowTaskParameterValueUpdateById = "Content.WorkflowTaskParameterValue_UpdateById";
        public const String WorkflowTaskParameterValueDeleteById = "Content.WorkflowTaskParameterValue_DeleteById";

        #endregion

        #region Workpackage

        public const String WorkpackageFetchById = "Content.Workpackage_FetchById";
        public const String WorkpackageInsert = "Content.Workpackage_Insert";
        public const String WorkpackageUpdateById = "Content.Workpackage_UpdateById";
        public const String WorkpackageDeleteById = "Content.Workpackage_DeleteById";

        #endregion

        #region WorkpackageInfo

        public const String WorkpackageInfoFetchByEntityId = "Content.WorkpackageInfo_FetchByEntityId";
        public const String WorkpackageInfoFetchById = "Content.WorkpackageInfo_FetchById";

        #endregion

        #region WorkpackagePerformance

        public const String WorkpackagePerformanceInsert = "Content.WorkpackagePerformance_Insert";
        public const String WorkpackagePerformanceDeleteByWorkpackageId = "Content.WorkpackagePerformance_DeleteByWorkpackageId";

        #endregion

        #region WorkpackagePerformanceData

        public const String WorkpackagePerformanceDataFetchByWorkpackageIdPerformanceSelectionId = "Content.WorkpackagePerformanceData_FetchByWorkpackageIdPerformanceSelectionId";
        public const String WorkpackagePerformanceDataFetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId = "Content.WorkpackagePerformanceData_FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId";
        public const String WorkpackagePerformanceDataBulkInsert = "Content.WorkpackagePerformanceData_BulkInsert";
        public const String WorkpackagePerformanceDataDeleteByWorkpackageId = "Content.WorkpackagePerformanceData_DeleteByWorkpackageId";

        #endregion

        #region WorkpackagePlanogram
        public const String WorkpackagePlanogramFetchByWorkpackageId = "Content.WorkpackagePlanogram_FetchByWorkpackageId";
        public const String WorkpackagePlanogramFetchByWorkpackageIdDestinationPlanogramId = "Content.WorkpackagePlanogram_FetchByWorkpackageIdDestinationPlanogramId";
        public const String WorkpackagePlanogramInsert = "Content.WorkpackagePlanogram_Insert";
        public const String WorkpackagePlanogramUpdateById = "Content.WorkpackagePlanogram_UpdateById";
        public const String WorkpackagePlanogramDeleteById = "Content.WorkpackagePlanogram_DeleteById";
        #endregion

        #region WorkpackagePlanogramDebug
        public const String WorkpackagePlanogramDebugFetchBySourcePlanogramId = "Content.WorkpackagePlanogramDebug_FetchBySourcePlanogramId";
        public const String WorkpackagePlanogramDebugInsert = "Content.WorkpackagePlanogramDebug_Insert";
        public const String WorkpackagePlanogramDebugUpdateBySourcePlanogramIdWorkflowTaskId = "Content.WorkpackagePlanogramDebug_UpdateBySourcePlanogramIdWorkflowTaskId";
        public const String WorkpackagePlanogramDebugDeleteBySourcePlanogramIdWorkflowTaskId = "Content.WorkpackagePlanogramDebug_DeleteBySourcePlanogramIdWorkflowTaskId";
        #endregion

        #region WorkpackagePlanogramParameter
        public const String WorkpackagePlanogramParameterFetchByWorkpackagePlanogramId = "Content.WorkpackagePlanogramParameter_FetchByWorkpackagePlanogramId";
        public const String WorkpackagePlanogramParameterInsert = "Content.WorkpackagePlanogramParameter_Insert";
        public const String WorkpackagePlanogramParameterUpdateById = "Content.WorkpackagePlanogramParameter_UpdateById";
        public const String WorkpackagePlanogramParameterDeleteById = "Content.WorkpackagePlanogramParameter_DeleteById";
        #endregion

        #region WorkpackagePlanogramParameterValue
        public const String WorkpackagePlanogramParameterValueFetchByWorkpackagePlanogramParameterId = "Content.WorkpackagePlanogramParameterValue_FetchByWorkpackagePlanogramParameterId";
        public const String WorkpackagePlanogramParameterValueInsert = "Content.WorkpackagePlanogramParameterValue_Insert";
        public const String WorkpackagePlanogramParameterValueUpdateById = "Content.WorkpackagePlanogramParameterValue_UpdateById";
        public const String WorkpackagePlanogramParameterValueDeleteById = "Content.WorkpackagePlanogramParameterValue_DeleteById";
        #endregion

        #region WorkpackagePlanogramProcessingStatus
        public const String WorkpackagePlanogramProcessingStatusFetchByPlanogramIdWorkpackageId = "Content.WorkpackagePlanogramProcessingStatus_FetchByPlanogramIdWorkpackageId";
        public const String WorkpackagePlanogramProcessingStatusFetchByPlanogramId = "Content.WorkpackagePlanogramProcessingStatus_FetchByPlanogramId";
        public const String WorkpackagePlanogramProcessingStatusUpdateByPlanogramIdWorkpackageId = "Content.WorkpackagePlanogramProcessingStatus_UpdateByPlanogramIdWorkpackageId";
        public const String WorkpackagePlanogramProcessingStatusIncrementAutomationStatusByPlanogramIdWorkpackageId = "Content.WorkpackagePlanogramProcessingStatus_IncrementAutomationStatusByPlanogramIdWorkpackageId";
        #endregion

        #region WorkpackgageProcessingStatus

        public const String WorkpackageProcessingStatusFetchByWorkpackageId = "Content.WorkpackageProcessingStatus_FetchByWorkpackageId";
        public const String WorkpackageProcessingStatusInitializeByWorkpackageId = "Content.WorkpackageProcessingStatus_InitializeByWorkpackageId";
        public const String WorkpackageProcessingStatusUpdateByWorkpackageId = "Content.WorkpackageProcessingStatus_UpdateByWorkpackageId";
        public const String WorkpackageProcessingStatusIncrementByWorkpackageId = "Content.WorkpackageProcessingStatus_IncrementByWorkpackageId";
        public const String WorkpackageProcessingStatusSetPendingByWorkpackageId = "Content.WorkpackageProcessingStatus_SetPendingByWorkpackageId";

        #endregion
    }
}

