﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-25476 : A. Kuszyk
//  Initial version. Adapted from GFS.    
// CCM-25450 : L.Ineson
//	Added ProductGroup, ProductHierarchy and Product Level
// CCM-25559 : L.Ineson
//  Added entity
// CCM-25587 : L.Ineson
//  Added PlanogramHierarchy and PlanogramGroup
// V8-25479 : A.Kuszyk
//  Added DateCreated, DateModified and DateDeleted fields to Planogram.
// V8-25445 : L.Ineson
// Added LocationGroup, LocationHierarchy and Location Level
// V8-25619 : A.Kuszyk
//  Added Location, ClusterScheme, Cluster, ClusterLocation, ClusterSchemeInfo, ProductUniverse,
//  ProductUniverseInfo, ProductUniverseProduct, Product, ConsumerDecisionTree (including Node, Level
// and NodeProduct).
// V8-25546 : L.Ineson
//  Added Workflow, WorkflowTask, WorkflowTaskParameter,
//      Workpackage, PlanogramInfo
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link and Added Entity_Id link to Workpackage
// CCM-25631 : N.Haywood
//  Added location space procedures
// CCM-25446 : N.Haywood
//  Added location product attributes
// V8-25788 : M.Pettit
//  Added Report, ReportInfo, ReportComponent, ReportComponentDetail, ReportCondition,
//  Added ReportField, ReportFormulaField, ReportGroup, ReportGroupField, ReportParameter,
//  Added ReportPredicate,ReportSection fields
// V8-25868 : L.Ineson
//  Added Workflow Description
// V8-25886 : L.Ineson
//  Added WorkflowTaskParameterValue & WorkpackagePlanogramParameterValue
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// V8-25454 : J.Pickup
//  Added Assortment, AssortmentFile, AssortmentLocalProduct, AssortmentLocation, AssortmentProduct, AssortmentRegion, 
//  AssortmentRegionLocation, AssortmentRegionProduct, Blob, File, LocationInfoFetchByEntityIdIncludingDeleted
// V8-25919 : L.Ineson
//  Added WorkflowTaskInfo
// V8-25916 : N.Foster
//  Added PlanogramRowVersion
// V8-25556 : D.Pleasance
//  Added LocationUpdateDeleted
// V8-25664 : A.Kuszyk
//  Added User and UserPlanogramGroup.
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties for Planogram, PlanogramInfo, PlanogramFixtureItem, PlanogramAssemblyComponent, PlanogramFixtureAssembly, PlanogramFixtureComponent
// V8-25455 : J.Pickup
//  Added AssortmentMinorRevision, AssortmentMinorRevisionAmendDistributionAction, AssortmentMinorRevisionAmendDistributionActionLocation, 
//  AssortmentMinorRevisionDeListAction, AssortmentMinorRevisionDeListActionLocation, AssortmentMinorRevisionListAction, 
//  AssortmentMinorRevisionListActionLocation, AssortmentMinorRevisionReplaceAction, AssortmentMinorRevisionReplaceActionLocation
// V8-25787 : N.Foster
//  Added PlanogramComponentIsCarPark
// V8-26041 : A.Kuszyk
//  Changed Product_FillPattern to Product_FillPatternType.
//  Added additional Product fields to PlanogramProduct table.
// CCM-25879 : N.Haywood
//  Added LocationPlanogramAssignment and LocationPlanogramAssignmentLocation
// V8-26041 : A.Kuszyk
//  Added Compression, Image and ProductImage field names. Also added ShapeType to Product.
// V8-26147 : L.Ineson
//  Added CustomAttributeData
// V8-26222 : L.Luong
//  Modified User field names
// V8-26158 : I.George
// added Metric field names
// V8-26270 : A.Kuszyk
//  Added PlanogramPerformance, PlanogramPerformanceMetric and PlanogramPerformanceData.
// V8-26277 : N.Foster
//  Removed references to reporting
// V8-26306 : L.Ineson
//  Added SystemSettings
// V8-26159 : L.Ineosn
//  Added PerformanceSelection & PerformanceSelectionTimelineGroup
// V8-26426 : A.Kuszyk
//  Added PlanogramAssortment and related tables.
// V8-26474 : A.Silva 
//  Added ValidationTemplate and related tables.
// V8-26514 : A.Silva 
//  Added ValidationTemplate Score field names.
// V8-26123 : L.Ineson
//  Added MetricProfile
// V8-26573 : L.Luong
//  Added PlanogramValidationTemplate and related tables
// V8-26560 : A.Probyn
//  Added field names for Sequence, SequenceInfo, SequenceProduct, SequenceProperty
// V8-26520 : J.Pickup   
//  Added ContentLookup fieldnames
// V8-26709 : L.Ineson
//  Removed old planogram unit of measure property.
// V8-26704 : A.Kuszyk
//  Added AssortmentLocalProduct Product GTIN and Location Code fields.
//  Added AssortmentRegionLocation Location Code field.
// V8-26773 : N.Foster
//  Added WorkpackagePerformance, WorkpackagePerformanceData
// V8-26706 : L.Luong
//  Added PlanogramConsumerDecisionTree and related field names
// V8-26836 : L.Luong
//  Added PlanogramEventLog field names
// V8-26911 : L.Luong
//  Added EventLog and related field names
// V8-26799 : A.Kuszyk
//  Removed PlanogramAssortmentProduct LocationCode field.
// V8-26954 : A.Silva
//      Added ResultType field names to ValidationTemplateGroup and Metric.
// V8-26891 : L.Ineson
//  Added Blocking fields
// V8-26953 : I.George
//  Added PerformanceselectionMetric
// V8-26956 : L.Luong
//  Changed PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
// V8-27004 : A.Silva
//      Added PlanogramValidationInfo, PlanogramValidationGroupInfo and PlanogramValidationMetricInfo field names.
// V8-26812 : A.Silva
//      Added ValidationType fields to PlanogramValidationTemplateGroup, Metric, ValidationTemplateGroup and Metric.
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties for planogram related objects.
//  Added PlanogramDateMetadataCalculated
// V8-27132 : A.Kuszyk
//  Added Product Gtin to ConsumerDecisionTreeNode
// V8-27237: A.Silva
//      Added PlanogramDateValidationDataCalculated.
// V8-27264 : A.Kuszyk
//  Amended AssortmentProductIsPrimaryRegionalProduct to match database field name.
// V8-27153 : A.Silva
//      Added RenumberingStrategy related fields.
//      Added PlanogramRenumberingStrategy related fields.
// V8-27404 : L.Luong
//  Renamed LevelType to EntryType and added Content to PlanogramEventLog
//  Renamed LevelId to EntryType in EventLog
// V8-27424 : L.Luong
//  Added FinancialGroupCode and FinancialGroupName to Product
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber field to PlanogramFixtureComponent.
//      Added ComponentSequenceNumber field to PlanogramAssemblyComponent.
//      Added PositionSequenceNumber field to PlanogramPosition.
// V8-27476 : L.Luong
//  Added BlockPlacementXType and BlockPlacementYType for BlockingGroup and PlanogramBlockingGroup
// V8-27411 : M.Pettit
//  Added PlanogramLock fields
// V8-27078 : I.George
// Added ProductGroup Id to ClusterScheme fields
// V8-27605 : A.Silva
//      Added missing metadata fields names to PlanogramAssemblyComponent.
// V8-27411 : M.Pettit
//  Refactored PlanogramProcessingStatus column names
//  Added columns to PlanogramProcessingStatus
//  Added columns to PlanogramInfo
// V8-27647 : L.Luong
//  Added PlanogramInventory field names
// V8-27759 : A.Kuszyk
//  Added AffectedType and AffectedId fields to PlanogramEventLog.
// V8-27411 : M.Pettit
//  Added PlanogramInfoLockFullName field
//  Added WorkpackageDateCompleted and WorkpackageUserid to Workpackage
// V8-27940 : L.Luong
//  Added Label field names
// V8-27964 : A.Silva
//      Added DateLastMerchSynced to Entity.
// V8-27411 : M.Pettit
//  Added User_Id to Planogram, PlanoramInfo
// V8-27941 : J.Pickup
//  Added Highlight, HighlightCharachteristic, HighlightCharachteristicRule, HighlightGroup, HighlightFiter, HighlightInfo
// V8-27939 : L.Luong
//  Added PlanogramImportTemplate and related fields
// V8-24779 : D.Pleasance
//  Added Planogram.SourcePath
// V8-28059 : A.Kuszyk
//  Added WorkpackageSource, TaskSource and EventId fields to PlanogramEventLog.
// CCM-27891 : J.Pickup
//	Validation and renumbering added to contentlookup
// CCM-27599 : J.Pickup
//  Added Entity to PlanogramInfo fields
#endregion
#region Version History: CCM801
// V8-28519 : J.Pickup
//  Removed contentlookup_DateDeleted  
// V8-28622 : D.Pleasance
//  Added PlanogramImportTemplatePerformanceMetric
// V8-27636 : L.Ineson
//  Added MetaTotalUnits
// V8-28258 : N.Foster
//  Added EngineParameters
// V8-28507 : D.Pleasance
//  Added GenericPageNumber \ GenericPageSize
// V8-28859 : N.Foster
// Added Engine.Message_Exists procedure and fields
#endregion
#region Version History: CCM802

// V8-28987 : I.George
//  Added LocationCode, LocationName, ClusterSchemeName and ClusterName fields to Planogram Table
// V8-29010 : D.Pleasance
//  Added PlanogramNameTemplate
// V8-28996 : A.Silva
//      Added PlanogramSequence, PlanogramSequenceGroup and PlanogramSequenceGroupProduct fields.
// V8-29023 : M.Pettit
//  Added PlanogramSubComponent_MerchandisableDepth
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-28766 : J.Pickup
//  Introduced IsManuallyPlaced to planogram posistion. 
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type from 
//  BlockingGroup and PlanogramBlockingGroup.
// V8-28840 : L.Luong
//  Added EntityId to Planogram
// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-29028 : L.Ineson
//  Added position squeeze properties.
// V8-29054 : M.Pettit
//  Added planogram position meta validation fields
// V8-28811 : L.Luong
//  Added Meta Data Properties for PlanogramProduct, PlangramFixtureComponent, PlangramAssemblyComponent, PlangramPerformanceData
// V8-29232 : A.Silva
//      Added PlanogramProduct.PlanogramProductSequenceNumber.
// V8-29255 : L.Ineson
//  Removed Sequence tables
#endregion
#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added PlanogramFixtureComponentNotchNumber and PlanogramAssemblyComponentNotchNumber
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
// V8-29134 : D.Pleasance
//  Removed Assortment_DateDeleted column.
// V8-29214 : D.Pleasance
//  Removed AssortmentMinorRevisionDateDeleted column.
// V8-29216 : D.Pleasance
//  Removed ClusterScheme_DateDeleted
// V8-29223 : D.Pleasance
//  Removed LocationSpace_DateDeleted
// V8-29326 : M.Shelley
//  Added PlanogramInfoClusterSchemeName and PlanogramInfoClusterName
// V8-29135 : D.Pleasance
//  Added DataInfo
// V8-29491 : D.Pleasance
//  Added WorkpackagePerformanceDataDataType \ WorkpackagePerformanceDataDataId
// V8-29506 : M.Shelley
//  Add new metadata properties to PlanogramProduct for reporting:
//      MetaPositionCount, MetaTotalFacings, MetaTotalLinearSpace, MetaTotalAreaSpace, MetaTotalVolumetricSpace
// V8-28491 : L.Luong
//  Added PlanogramProductMetaIsInMasterData
// V8-29590 : A.Probyn
//  Added EventLog_WorkpackageName, EventLog_WorkpackageId
//  Added PlanogramEventLog_Score, 
//  Added Planogram_MetaNoOfErrors, Planogram_MetaNoOfWarnings, Planogram_MetaTotalErrorScore , Planogram_MetaHighestErrorScore 
//  Added PlanogramInfo_MetaNoOfErrors, PlanogramInfo_MetaNoOfWarnings, PlanogramInfo_MetaTotalErrorScore , PlanogramInfo_MetaHighestErrorScore
// V8-29596 : L.Luong
//  Added Criteria to PlanogramValidationTemplateGroupMetric and ValidationTemplateGroupMetric 
// V8-28242 : M.Shelley
//  Added to Planogram: Planogram_Status, Planogram_DateWIP, Planogram_DateApproved, Planogram_DateArchived
//  Added to LocationPlanAssignment: LocationPlanAssignmentDateCommunicated, LocationPlanAssignmentDateLive
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference to content that is synced from GFS
// V8-29682 : A.Probyn
//  Added PlanogramPerformanceMetric_AggregationType 
//  Added PlanogramImportTemplatePerformanceMetric_AggregationType 
// V8-29746 : A.Probyn
//  Added ContentLookup data Id field names
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//  Added WorkpackagePlanogramLocationType
// V8-29598 : L.Luong
//  Added Height to PlanAssignmentStoreSpaceInfo
//  Added ProductGroup and DateLastModified to ClusterSchemeInfo
// V8-29861 : A.Probyn
//  Added PlanogramEventLogInfo fields
// V8-29590 : A.Probyn
//  Added PlanogramMetaNoOfDebugEvents & PlanogramMetaNoOfInformationEvents
//  Added PlanogramInfoMetaNoOfDebugEvents & PlanogramInfoMetaNoOfInformationEvents
//  Added WorkpackageInfoPlanogramWarningCount, WorkpackageInfoPlanogramTotalErrorScore and WorkpackageInfoPlanogramHighestErrorScore
#endregion
#region Version History: CCM810
// V8-30359 : A.Probyn
//  Added new PlanogramInfoMostRecentLinkedWorkpackageName
#endregion
#region Version History: CCM820
// V8-30728 : A.Kuszyk
//  Added additional Planogram and Planogram Product fields.
// V8-30738 : L.Ineson
//  Added Print Template fields.
// CCM-30759 : J.Pickup
//	InventoryProfileType added to InventoryProfile.
// V8-29998 : I.George
//	Added PlanogramPerformanceData_MetaP1Rank to PlanogramPerformanceData_MetaP20Rank fields
// V8-30773 : J.Pickup
//	Added PlanogramInventory_InventoryMetricType to the field names schema.
// V8-30818 : N.Foster
//  Added additional engine metric fields
// V8-30762 : I.George
//  Added PlanogramProduct AssortmentMetaData field
// V8-30763 :I.George
//  Added ConsumerDecisionTreeNode MetaData Fields
// V8-30763 :I.George
//	Added MetaCDTNode field to PlanogramProduct 
// V8-30765 :I.George
//	Added PlanogramBlockingGroup Metadata Fields
// V8-30732 : M.Shelley
//  Added 2 new fields : LocationPlanAssignment_PublishType, LocationPlanAssignment_OutputDestination
// V8-30508 : M.Shelley
//  Added a new field : LocationPlanAssignment_PlanPublishStatus
// V8-30705 : A.Probyn
//  Added PlanogramAssortmentProductDelistedDueToAssortmentRule
//  Added new Assortment rule related meta data fields to Planogram, PlanogramProduct
// V8-30193 : L.Ineson
//  Added PlanogramRenumberingStrategyIsEnabled
// V8-30992 : A.Probyn
//  Added PlanogramPerformanceDataMinimumInventoryUnits
// V8-31164 : D.Pleasance
//  Added PlanogramPositionSequenceNumber \ PlanogramPositionSequenceColour
//  Removed PlanogramProductBlockingColour \ PlanogramProductSequenceNumber
#endregion
#region Version History: CCM830
// V8-31550 : A.Probyn
//  Added new PlanogramAssortmentProductBuddy, PlanogramAssortmentLocationBuddy, AssortmentLocationBuddy, AssortmentProductBuddy fields
// V8-31551 : A.Probyn
//  Added new PlanogramAssortmentInventoryRule, AssortmentInventoryRule  fields
// V8-31546 : M.Pettit
//  Added PlanogramExportTemplate,PlanogramExportTemplateMapping, PlanogramExportTemplatePerformanceMetric
// V8-31531 : A.Heathcote
//  Removed public const String Product IsTrayProduct & public const String PlanogramProduct IsTrayProduct  
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31819 : A.Silva
//  Added PlanogramComparison, PlanogramComparisonField, PlanogramComparisonResult, PlanogramComparisonItem, PlanogramComparisonFieldValue fields.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts to PlanogramComparison.
// V8-31947 : A.Silva
//  Added MetaComparisonStatus to PlanogramProduct and PlanogramPosition.
// V8-31963 : A.Silva
//  Added Display field to PlanogramComparisonField.
//V8-31831 : A.Probyn
//  Added PlanogramNameTemplatePlanogramAttribute
// V8-32005 : A.Silva
//  Added Name, Description and DataOrderType to PlanogramComparison.
// V8-31831 : A.Probyn
//  Added ContentLookupPlanogramNameTemplateName and ContentLookupPlanogramNameTemplateId
//  Removed PlanogramNameTemplateSeparatorType
// V8-31945 : A.Silva
//  Added PlanogramComparisonTemplate and PlanogramComparisonTemplateField fields.
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32357 : M.Brumby
//  [PCR01561] added workflowtask DisplayName and DisplayDescription
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroup fields.
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
// V8-32396 : A.Probyn
//  Added PlanogramProductMetaIsDelistFamilyRuleBroken & PlanogramMetaNumberOfDelistFamilyRulesBroken
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added PlanogramProductMetaIsBuddied
//  Added PlanogramMetaCountOfProductsBuddied
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName & MetaSequenceGroupName.
// V8-32810 : M.Pettit
//  Added ContentLookupPrintTemplateName
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
// V8-33003 : J.Pickup
//  PlanogramInfo_AssignedLocationCount Added.
#endregion
#region Version History: CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
// CCM-13408 : J.Mendes
//  Added LocationSpaceProductGroup fields to the Planogram Assignment
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Mssql.Schema
{
    internal static partial class FieldNames
    {
        #region Assortment

        public const String AssortmentId = "Assortment_Id";
        public const String AssortmentRowVersion = "Assortment_RowVersion";
        public const String AssortmentEntityId = "Entity_Id";
        public const String AssortmentProductGroupId = "ProductGroup_Id";
        public const String AssortmentConsumerDecisionTreeId = "ConsumerDecisionTree_Id";
        public const String AssortmentName = "Assortment_Name";
        public const String AssortmentUniqueContentReference = "Assortment_UniqueContentReference";
        public const String AssortmentDateCreated = "Assortment_DateCreated";
        public const String AssortmentDateLastModified = "Assortment_DateLastModified";
        public const String AssortmentSetProperties = "Assortment_SetProperties";
        public const String AssortmentProductGroupName = "ProductGroup_Name";
        public const String AssortmentProductGroupCode = "ProductGroup_Code";
        public const String AssortmentSearchCriteria = "AssortmentSearchCriteria";
        public const String AssortmentParentUniqueContentReference = "Assortment_ParentUniqueContentReference";


        #endregion

        #region AssortmentFile

        public const String AssortmentFileId = "AssortmentFile_Id";
        public const String AssortmentFileFileId = "File_Id";
        public const String AssortmentFileAssortmentId = "Assortment_Id";

        #endregion

        #region AssortmentInventoryRule

        public const String AssortmentInventoryRuleId = "AssortmentInventoryRule_Id";
        public const String AssortmentInventoryRuleProductId = "Product_Id";
        public const String AssortmentInventoryRuleProductGtin = "Product_Gtin";
        public const String AssortmentInventoryRuleCasePack = "AssortmentInventoryRule_CasePack";
        public const String AssortmentInventoryRuleDaysOfSupply = "AssortmentInventoryRule_DaysOfSupply";
        public const String AssortmentInventoryRuleShelfLife = "AssortmentInventoryRule_ShelfLife";
        public const String AssortmentInventoryRuleReplenishmentDays = "AssortmentInventoryRule_ReplenishmentDays";
        public const String AssortmentInventoryRuleWasteHurdleUnits = "AssortmentInventoryRule_WasteHurdleUnits";
        public const String AssortmentInventoryRuleWasteHurdleCasePack = "AssortmentInventoryRule_WasteHurdleCasePack";
        public const String AssortmentInventoryRuleMinUnits = "AssortmentInventoryRule_MinUnits";
        public const String AssortmentInventoryRuleMinFacings = "AssortmentInventoryRule_MinFacings";
        public const String AssortmentInventoryRuleAssortmentId = "Assortment_Id";


        #endregion

        #region AssortmentLocalProduct

        public const String AssortmentLocalProductId = "AssortmentLocalProduct_Id";
        public const String AssortmentLocalProductLocationId = "Location_Id";
        public const String AssortmentLocalProductProductId = "Product_Id";
        public const String AssortmentLocalProductAssortmentId = "Assortment_Id";
        public const String AssortmentLocalProductProductGTIN = "AssortmentLocalProduct_ProductGTIN";
        public const String AssortmentLocalProductLocationCode = "AssortmentLocalProduct_LocationCode";

        #endregion

        #region AssortmentLocation

        public const String AssortmentLocationId = "AssortmentLocation_Id";

        public const String AssortmentLocationLocationId = "Location_Id";
        public const String AssortmentLocationCode = "Location_Code";
        public const String AssortmentLocationAssortmentId = "Assortment_Id";
        public const String AssortmentLocationLocationName = "Location_Name";


        #endregion

        #region AssortmentLocationBuddy

        public const String AssortmentLocationBuddyId = "AssortmentLocationBuddy_Id";
        public const String AssortmentLocationBuddyLocationId = "Location_Id";
        public const String AssortmentLocationBuddyLocationCode = "Location_Code";
        public const String AssortmentLocationBuddyTreatmentType = "AssortmentLocationBuddy_TreatmentType";
        public const String AssortmentLocationBuddyS1LocationId = "AssortmentLocationBuddy_S1LocationId";
        public const String AssortmentLocationBuddyS1Percentage = "AssortmentLocationBuddy_S1Percentage";
        public const String AssortmentLocationBuddyS1LocationCode = "AssortmentLocationBuddy_S1LocationCode";
        public const String AssortmentLocationBuddyS2LocationId = "AssortmentLocationBuddy_S2LocationId";
        public const String AssortmentLocationBuddyS2Percentage = "AssortmentLocationBuddy_S2Percentage";
        public const String AssortmentLocationBuddyS2LocationCode = "AssortmentLocationBuddy_S2LocationCode";
        public const String AssortmentLocationBuddyS3LocationId = "AssortmentLocationBuddy_S3LocationId";
        public const String AssortmentLocationBuddyS3Percentage = "AssortmentLocationBuddy_S3Percentage";
        public const String AssortmentLocationBuddyS3LocationCode = "AssortmentLocationBuddy_S3LocationCode";
        public const String AssortmentLocationBuddyS4LocationId = "AssortmentLocationBuddy_S4LocationId";
        public const String AssortmentLocationBuddyS4Percentage = "AssortmentLocationBuddy_S4Percentage";
        public const String AssortmentLocationBuddyS4LocationCode = "AssortmentLocationBuddy_S4LocationCode";
        public const String AssortmentLocationBuddyS5LocationId = "AssortmentLocationBuddy_S5LocationId";
        public const String AssortmentLocationBuddyS5Percentage = "AssortmentLocationBuddy_S5Percentage";
        public const String AssortmentLocationBuddyS5LocationCode = "AssortmentLocationBuddy_S5LocationCode";
        public const String AssortmentLocationBuddyAssortmentId = "Assortment_Id";


        #endregion

        #region AssortmentProduct

        public const String AssortmentProductId = "AssortmentProduct_Id";
        public const String AssortmentProductRowVersion = "AssortmentProduct_RowVersion";
        public const String AssortmentProductProductId = "Product_Id";
        public const String AssortmentProductGTIN = "AssortmentProduct_GTIN";
        public const String AssortmentProductName = "AssortmentProduct_Name";
        public const String AssortmentProductIsRanged = "AssortmentProduct_IsRanged";
        public const String AssortmentProductRank = "AssortmentProduct_Rank";
        public const String AssortmentProductSegmentation = "AssortmentProduct_Segmentation";
        public const String AssortmentProductFacings = "AssortmentProduct_Facings";
        public const String AssortmentProductUnits = "AssortmentProduct_Units";
        public const String AssortmentProductProductTreatmentType = "AssortmentProduct_ProductTreatmentType";
        public const String AssortmentProductProductLocalizationType = "AssortmentProduct_ProductLocalizationType";
        public const String AssortmentProductComments = "AssortmentProduct_Comments";
        public const String AssortmentProductExactListFacings = "AssortmentProduct_ExactListFacings";
        public const String AssortmentProductExactListUnits = "AssortmentProduct_ExactListUnits";
        public const String AssortmentProductPreserveListFacings = "AssortmentProduct_PreserveListFacings";
        public const String AssortmentProductPreserveListUnits = "AssortmentProduct_PreserveListUnits";
        public const String AssortmentProductMaxListFacings = "AssortmentProduct_MaxListFacings";
        public const String AssortmentProductMaxListUnits = "AssortmentProduct_MaxListUnits";
        public const String AssortmentProductMinListFacings = "AssortmentProduct_MinListFacings";
        public const String AssortmentProductMinListUnits = "AssortmentProduct_MinListUnits";
        public const String AssortmentProductFamilyRuleName = "AssortmentProduct_FamilyRuleName";
        public const String AssortmentProductFamilyRuleType = "AssortmentProduct_FamilyRuleType";
        public const String AssortmentProductFamilyRuleValue = "AssortmentProduct_FamilyRuleValue";
        public const String AssortmentProductIsPrimaryRegionalProduct = "IsPrimaryProduct";
        public const String AssortmentProductDateCreated = "AssortmentProduct_DateCreated";
        public const String AssortmentProductDateLastModified = "AssortmentProduct_DateLastModified";
        public const String AssortmentProductDateDeleted = "AssortmentProduct_DateDeleted";
        public const String AssortmentProductAssortmentId = "Assortment_Id";

        #endregion

        #region AssortmentProductBuddy

        public const String AssortmentProductBuddyId = "AssortmentProductBuddy_Id";
        public const String AssortmentProductBuddyProductId = "Product_Id";
        public const String AssortmentProductBuddyProductGtin = "Product_Gtin";
        public const String AssortmentProductBuddySourceType = "AssortmentProductBuddy_SourceType";
        public const String AssortmentProductBuddyTreatmentType = "AssortmentProductBuddy_TreatmentType";
        public const String AssortmentProductBuddyTreatmentTypePercentage = "AssortmentProductBuddy_TreatmentTypePercentage";
        public const String AssortmentProductBuddyProductAttributeType = "AssortmentProductBuddy_ProductAttributeType";
        public const String AssortmentProductBuddyS1ProductId = "AssortmentProductBuddy_S1ProductId";
        public const String AssortmentProductBuddyS1Percentage = "AssortmentProductBuddy_S1Percentage";
        public const String AssortmentProductBuddyS1ProductGtin = "AssortmentProductBuddy_S1ProductGtin";
        public const String AssortmentProductBuddyS2ProductId = "AssortmentProductBuddy_S2ProductId";
        public const String AssortmentProductBuddyS2Percentage = "AssortmentProductBuddy_S2Percentage";
        public const String AssortmentProductBuddyS2ProductGtin = "AssortmentProductBuddy_S2ProductGtin";
        public const String AssortmentProductBuddyS3ProductId = "AssortmentProductBuddy_S3ProductId";
        public const String AssortmentProductBuddyS3Percentage = "AssortmentProductBuddy_S3Percentage";
        public const String AssortmentProductBuddyS3ProductGtin = "AssortmentProductBuddy_S3ProductGtin";
        public const String AssortmentProductBuddyS4ProductId = "AssortmentProductBuddy_S4ProductId";
        public const String AssortmentProductBuddyS4Percentage = "AssortmentProductBuddy_S4Percentage";
        public const String AssortmentProductBuddyS4ProductGtin = "AssortmentProductBuddy_S4ProductGtin";
        public const String AssortmentProductBuddyS5ProductId = "AssortmentProductBuddy_S5ProductId";
        public const String AssortmentProductBuddyS5Percentage = "AssortmentProductBuddy_S5Percentage";
        public const String AssortmentProductBuddyS5ProductGtin = "AssortmentProductBuddy_S5ProductGtin";
        public const String AssortmentProductBuddyAssortmentId = "Assortment_Id";


        #endregion

        #region AssortmentRegion

        public const String AssortmentRegionId = "AssortmentRegion_Id";
        public const String AssortmentRegionName = "AssortmentRegion_Name";
        public const String AssortmentRegionAssortmentId = "Assortment_Id";

        #endregion

        #region AssortmentRegionLocation

        public const String AssortmentRegionLocationId = "AssortmentRegionLocation_Id";
        public const String AssortmentRegionLocationLocationId = "Location_Id";
        public const String AssortmentRegionLocationRegionId = "AssortmentRegion_Id";
        public const String AssortmentRegionLocationLocationCode = "AssortmentRegionLocation_LocationCode";

        #endregion

        #region AssortmentRegionProduct

        public const String AssortmentRegionalProductId = "AssortmentRegionalProduct_Id";
        public const String AssortmentRegionalProductAssortmentRegionId = "AssortmentRegion_Id";
        public const String AssortmentRegionalProductPrimaryProductId = "AssortmentRegionalProduct_PrimaryProductId";
        public const String AssortmentRegionalProductPrimaryProductGTIN = "AssortmentRegionalProduct_PrimaryProductGTIN";
        public const String AssortmentRegionalProductPrimaryProductDescription = "AssortmentRegionalProduct_PrimaryProductDescription";
        public const String AssortmentRegionalProductRegionalProductId = "AssortmentRegionalProduct_RegionalProductId";
        public const String AssortmentRegionalProductRegionalProductGTIN = "AssortmentRegionalProduct_RegionalProductGTIN";
        public const String AssortmentRegionalProductRegionalProductDescription = "AssortmentRegionalProduct_RegionalProductDescription";

        #endregion

        #region AssortmentMinorRevision

        public const String AssortmentMinorRevisionId = "AssortmentMinorRevision_Id";
        public const String AssortmentMinorRevisionRowVersion = "AssortmentMinorRevision_RowVersion";
        public const String AssortmentMinorRevisionConsumerDecisionTreeId = "AssortmentMinorRevision_ConsumerDecisionTreeId";
        public const String AssortmentMinorRevisionUcr = "AssortmentMinorRevision_UniqueContentReference";
        public const String AssortmentMinorRevisionName = "AssortmentMinorRevision_Name";
        public const String AssortmentMinorRevisionEntityId = "Entity_Id";
        public const String AssortmentMinorRevisionDateCreated = "AssortmentMinorRevision_DateCreated";
        public const String AssortmentMinorRevisionDateLastModified = "AssortmentMinorRevision_DateLastModified";
        public const String AssortmentMinorRevisionSetProperties = "AssortmentMinorRevision_SetProperties";
        public const String AssortmentMinorRevisionInfoChangeDate = "AssortmentMinorRevision_ChangeDate";
        public const String AssortmentMinorRevisionProductGroupId = "ProductGroup_Id";
        public const String AssortmentMinorRevisionParentUniqueContentReference = "AssortmentMinorRevision_ParentUniqueContentReference";


        #endregion

        #region AssortmentMinorRevisionAmendDistributionAction

        public const String AssortmentMinorRevisionAmendDistributionActionId = "AssortmentMinorRevisionAmendDistributionAction_Id";
        public const String AssortmentMinorRevisionAmendDistributionActionAssortmentMinorRevisionId = "AssortmentMinorRevision_Id";
        public const String AssortmentMinorRevisionAmendDistributionActionProductId = "Product_Id";
        public const String AssortmentMinorRevisionAmendDistributionActionProductGtin = "AssortmentMinorRevisionAmendDistributionAction_ProductGtin";
        public const String AssortmentMinorRevisionAmendDistributionActionProductName = "AssortmentMinorRevisionAmendDistributionAction_ProductName";
        public const String AssortmentMinorRevisionAmendDistributionActionPriority = "AssortmentMinorRevisionAmendDistributionAction_Priority";
        public const String AssortmentMinorRevisionAmendDistributionActionComments = "AssortmentMinorRevisionAmendDistributionAction_Comments";
        public const String AssortmentMinorRevisionAmendDistributionActionSetProperties = "AssortmentMinorRevisionAmendDistributionAction_SetProperties";

        #endregion

        #region AssortmentMinorRevisionAmendDistributionActionLocation

        public const String AssortmentMinorRevisionAmendDistributionActionLocationId = "AssortmentMinorRevisionAmendDistributionActionLocation_Id";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationAssortmentMinorRevisionAmendDistributionActionId = "AssortmentMinorRevisionAmendDistributionAction_Id";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationLocationId = "Location_Id";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationLocationCode = "AssortmentMinorRevisionAmendDistributionActionLocation_LocationCode";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationLocationName = "AssortmentMinorRevisionAmendDistributionActionLocation_LocationName";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationUnits = "AssortmentMinorRevisionAmendDistributionActionLocation_Units";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationFacings = "AssortmentMinorRevisionAmendDistributionActionLocation_Facings";
        public const String AssortmentMinorRevisionAmendDistributionActionLocationSetProperties = "AssortmentMinorRevisionAmendDistributionActionLocation_SetProperties";

        #endregion

        #region AssortmentMinorRevisionDeListAction

        public const String AssortmentMinorRevisionDeListActionId = "AssortmentMinorRevisionDeListAction_Id";
        public const String AssortmentMinorRevisionDeListActionAssortmentMinorRevisionId = "AssortmentMinorRevision_Id";
        public const String AssortmentMinorRevisionDeListActionProductGtin = "AssortmentMinorRevisionDeListAction_ProductGtin";
        public const String AssortmentMinorRevisionDeListActionProductName = "AssortmentMinorRevisionDeListAction_ProductName";
        public const String AssortmentMinorRevisionDeListActionProductId = "Product_Id";
        public const String AssortmentMinorRevisionDeListActionPriority = "AssortmentMinorRevisionDeListAction_Priority";
        public const String AssortmentMinorRevisionDeListActionComments = "AssortmentMinorRevisionDeListAction_Comments";
        public const String AssortmentMinorRevisionDeListActionSetProperties = "AssortmentMinorRevisionDeListAction_SetProperties";

        #endregion

        #region AssortmentMinorRevisionDeListActionLocation

        public const String AssortmentMinorRevisionDeListActionLocationId = "AssortmentMinorRevisionDeListActionLocation_Id";
        public const String AssortmentMinorRevisionDeListActionLocationAssortmentMinorRevisionDeListActionId = "AssortmentMinorRevisionDeListAction_Id";
        public const String AssortmentMinorRevisionDeListActionLocationLocationId = "Location_Id";
        public const String AssortmentMinorRevisionDeListActionLocationLocationCode = "AssortmentMinorRevisionDeListActionLocation_LocationCode";
        public const String AssortmentMinorRevisionDeListActionLocationLocationName = "AssortmentMinorRevisionDeListActionLocation_LocationName";
        public const String AssortmentMinorRevisionDeListActionLocationSetProperties = "AssortmentMinorRevisionDeListActionLocation_SetProperties";

        #endregion

        #region AssortmentMinorRevisionListAction

        public const String AssortmentMinorRevisionListActionId = "AssortmentMinorRevisionListAction_Id";
        public const String AssortmentMinorRevisionListActionAssortmentMinorRevisionId = "AssortmentMinorRevision_Id";
        public const String AssortmentMinorRevisionListActionProductGtin = "AssortmentMinorRevisionListAction_ProductGtin";
        public const String AssortmentMinorRevisionListActionProductName = "AssortmentMinorRevisionListAction_ProductName";
        public const String AssortmentMinorRevisionListActionProductId = "Product_Id";
        public const String AssortmentMinorRevisionListActionAssortmentConsumerDecisionTreeNodeId = "AssortmentConsumerDecisionTreeNode_Id";
        public const String AssortmentMinorRevisionListActionPriority = "AssortmentMinorRevisionListAction_Priority";
        public const String AssortmentMinorRevisionListActionComments = "AssortmentMinorRevisionListAction_Comments";
        public const String AssortmentMinorRevisionListActionSetProperties = "AssortmentMinorRevisionListAction_SetProperties";

        #endregion

        #region AssortmentMinorRevisionListActionLocation

        public const String AssortmentMinorRevisionListActionLocationId = "AssortmentMinorRevisionListActionLocation_Id";
        public const String AssortmentMinorRevisionListActionLocationAssortmentMinorRevisionListActionId = "AssortmentMinorRevisionListAction_Id";
        public const String AssortmentMinorRevisionListActionLocationLocationId = "Location_Id";
        public const String AssortmentMinorRevisionListActionLocationLocationCode = "AssortmentMinorRevisionListActionLocation_LocationCode";
        public const String AssortmentMinorRevisionListActionLocationLocationName = "AssortmentMinorRevisionListActionLocation_LocationName";
        public const String AssortmentMinorRevisionListActionLocationUnits = "AssortmentMinorRevisionListActionLocation_Units";
        public const String AssortmentMinorRevisionListActionLocationFacings = "AssortmentMinorRevisionListActionLocation_Facings";
        public const String AssortmentMinorRevisionListActionLocationSetProperties = "AssortmentMinorRevisionListActionLocation_SetProperties";

        #endregion

        #region AssortmentMinorRevisionReplaceAction

        public const String AssortmentMinorRevisionReplaceActionId = "AssortmentMinorRevisionReplaceAction_Id";
        public const String AssortmentMinorRevisionReplaceActionAssortmentMinorRevisionId = "AssortmentMinorRevision_Id";
        public const String AssortmentMinorRevisionReplaceActionProductId = "Product_Id";
        public const String AssortmentMinorRevisionReplaceActionReplacementProductId = "AssortmentMinorRevisionReplaceAction_ReplacementProductId";
        public const String AssortmentMinorRevisionReplaceActionProductGtin = "AssortmentMinorRevisionReplaceAction_ProductGtin";
        public const String AssortmentMinorRevisionReplaceActionProductName = "AssortmentMinorRevisionReplaceAction_ProductName";
        public const String AssortmentMinorRevisionReplaceActionReplacementProductGtin = "AssortmentMinorRevisionReplaceAction_ReplacementProductGtin";
        public const String AssortmentMinorRevisionReplaceActionReplacementProductName = "AssortmentMinorRevisionReplaceAction_ReplacementProductName";
        public const String AssortmentMinorRevisionReplaceActionPriority = "AssortmentMinorRevisionReplaceAction_Priority";
        public const String AssortmentMinorRevisionReplaceActionComments = "AssortmentMinorRevisionReplaceAction_Comments";
        public const String AssortmentMinorRevisionReplaceActionSetProperties = "AssortmentMinorRevisionReplaceAction_SetProperties";

        #endregion

        #region AssortmentMinorRevisionReplaceActionLocation

        public const String AssortmentMinorRevisionReplaceActionLocationId = "AssortmentMinorRevisionReplaceActionLocation_Id";
        public const String AssortmentMinorRevisionReplaceActionLocationAssortmentMinorRevisionReplaceActionId = "AssortmentMinorRevisionReplaceAction_Id";
        public const String AssortmentMinorRevisionReplaceActionLocationLocationId = "Location_Id";
        public const String AssortmentMinorRevisionReplaceActionLocationLocationCode = "AssortmentMinorRevisionReplaceActionLocation_LocationCode";
        public const String AssortmentMinorRevisionReplaceActionLocationLocationName = "AssortmentMinorRevisionReplaceActionLocation_LocationName";
        public const String AssortmentMinorRevisionReplaceActionLocationUnits = "AssortmentMinorRevisionReplaceActionLocation_Units";
        public const String AssortmentMinorRevisionReplaceActionLocationFacings = "AssortmentMinorRevisionReplaceActionLocation_Facings";
        public const String AssortmentMinorRevisionReplaceActionLocationSetProperties = "AssortmentMinorRevisionReplaceActionLocation_SetProperties";

        #endregion

        #region Blob

        public const String BlobId = "Blob_Id";
        public const String BlobRowVersion = "Blob_RowVersion";
        public const String BlobData = "Blob_Data";
        public const String BlobDateCreated = "Blob_DateCreated";
        public const String BlobDateLastModified = "Blob_DateLastModified";
        public const String BlobDateDeleted = "Blob_DateDeleted";

        #endregion

        #region Blocking

        public const String BlockingId = "Blocking_Id";
        public const String BlockingRowVersion = "Blocking_RowVersion";
        public const String BlockingEntityId = "Entity_Id";
        public const String BlockingProductGroupId = "ProductGroup_Id";
        public const String BlockingName = "Blocking_Name";
        public const String BlockingDescription = "Blocking_Description";
        public const String BlockingHeight = "Blocking_Height";
        public const String BlockingWidth = "Blocking_Width";
        public const String BlockingPlanogramId = "Planogram_Id";
        public const String BlockingDateCreated = "Blocking_DateCreated";
        public const String BlockingDateLastModified = "Blocking_DateLastModified";
        public const String BlockingDateDeleted = "Blocking_DateDeleted";

        #endregion

        #region BlockingDivider

        public const String BlockingDividerId = "BlockingDivider_Id";
        public const String BlockingDividerBlockingId = "Blocking_Id";
        public const String BlockingDividerType = "BlockingDivider_Type";
        public const String BlockingDividerLevel = "BlockingDivider_Level";
        public const String BlockingDividerX = "BlockingDivider_X";
        public const String BlockingDividerY = "BlockingDivider_Y";
        public const String BlockingDividerLength = "BlockingDivider_Length";
        public const String BlockingDividerIsSnapped = "BlockingDivider_IsSnapped";

        #endregion

        #region BlockingGroup

        public const String BlockingGroupId = "BlockingGroup_Id";
        public const String BlockingGroupBlockingId = "Blocking_Id";
        public const String BlockingGroupName = "BlockingGroup_Name";
        public const String BlockingGroupColour = "BlockingGroup_Colour";
        public const String BlockingGroupFillPatternType = "BlockingGroup_FillPatternType";
        public const String BlockingGroupCanCompromiseSequence = "BlockingGroup_CanCompromiseSequence";
        public const String BlockingGroupIsRestrictedByComponentType = "BlockingGroup_IsRestrictedByComponentType";
        public const String BlockingGroupCanOptimise = "BlockingGroup_CanOptimise";
        public const String BlockingGroupCanMerge = "BlockingGroup_CanMerge";
        public const String BlockingGroupBlockPlacementPrimaryType = "BlockingGroup_BlockPlacementPrimaryType";
        public const String BlockingGroupBlockPlacementSecondaryType = "BlockingGroup_BlockPlacementSecondaryType";
        public const String BlockingGroupBlockPlacementTertiaryType = "BlockingGroup_BlockPlacementTertiaryType";
        public const String BlockingGroupTotalSpacePercentage = "BlockingGroup_TotalSpacePercentage";

        #endregion

        #region BlockingLocation

        public const String BlockingLocationId = "BlockingLocation_Id";
        public const String BlockingLocationBlockingId = "Blocking_Id";
        public const String BlockingLocationBlockingGroupId = "BlockingGroup_Id";
        public const String BlockingLocationBlockingDividerTopId = "BlockingLocation_BlockingDividerTopId";
        public const String BlockingLocationBlockingDividerBottomId = "BlockingLocation_BlockingDividerBottomId";
        public const String BlockingLocationBlockingDividerLeftId = "BlockingLocation_BlockingDividerLeftId";
        public const String BlockingLocationBlockingDividerRightId = "BlockingLocation_BlockingDividerRightId";
        public const String BlockingLocationSpacePercentage = "BlockingLocation_SpacePercentage";

        #endregion

        #region ContentLookup

        public const String ContentLookupId = "ContentLookup_Id";
        public const String ContentLookupUserId = "User_Id";
        public const String ContentLookupPlanogramName = "Planogram_Name";
        public const String ContentLookupProductUniverseName = "ProductUniverse_Name";
        public const String ContentLookupAssortmentName = "Assortment_Name";
        public const String ContentLookupBlockingName = "Blocking_Name";
        public const String ContentLookupSequenceName = "Sequence_Name";
        public const String ContentLookupMetricProfileName = "MetricProfile_Name";
        public const String ContentLookupInventoryProfileName = "InventoryProfile_Name";
        public const String ContentLookupConsumerDecisionTreeName = "ConsumerDecisionTree_Name";
        public const String ContentLookupMinorAssortmentName = "MinorAssortment_Name"; // obsolete - remove
        public const String ContentLookupAssortmentMinorRevisionName = "AssortmentMinorRevision_Name";
        public const String ContentLookupPerformanceSelectionName = "PerformanceSelection_Name";
        public const String ContentLookupClusterSchemeName = "ClusterScheme_Name";
        public const String ContentLookupClusterName = "Cluster_Name";
        public const String ContentLookupRenumberingStrategyName = "RenumberingStrategy_Name";
        public const String ContentLookupValidationTemplateName = "ValidationTemplate_Name";
        public const String ContentLookupPlanogramId = "Planogram_Id";
        public const String ContentLookupProductUniverseId = "ProductUniverse_Id";
        public const String ContentLookupAssortmentId = "Assortment_Id";
        public const String ContentLookupBlockingId = "Blocking_Id";
        public const String ContentLookupSequenceId = "Sequence_Id";
        public const String ContentLookupMetricProfileId = "MetricProfile_Id";
        public const String ContentLookupInventoryProfileId = "InventoryProfile_Id";
        public const String ContentLookupConsumerDecisionTreeId = "ConsumerDecisionTree_Id";
        public const String ContentLookupAssortmentMinorRevisionId = "AssortmentMinorRevision_Id";
        public const String ContentLookupPerformanceSelectionId = "PerformanceSelection_Id";
        public const String ContentLookupClusterSchemeId = "ClusterScheme_Id";
        public const String ContentLookupClusterId = "Cluster_Id";
        public const String ContentLookupRenumberingStrategyId = "RenumberingStrategy_Id";
        public const String ContentLookupValidationTemplateId = "ValidationTemplate_Id";
        public const String ContentLookupPlanogramNameTemplateName = "PlanogramNameTemplate_Name";
        public const String ContentLookupPlanogramNameTemplateId = "PlanogramNameTemplate_Id";
        public const String ContentLookupPrintTemplateName = "PrintTemplate_Name";
        public const String ContentLookupPrintTemplateId = "PrintTemplate_Id";
        public const String ContentLookupRowVersion = "ContentLookup_RowVersion";
        public const String ContentLookupDateCreated = "ContentLookup_DateCreated";
        public const String ContentLookupDateLastModified = "ContentLookup_DateLastModified";
        public const String ContentLookupEntityId = "Entity_Id";

        #endregion

        #region Cluster

        public const String ClusterId = "Cluster_Id";
        public const String ClusterName = "Cluster_Name";
        public const String ClusterClusterSchemeId = "ClusterScheme_Id";
        public const String ClusterSetProperties = "Cluster_SetProperties";

        #endregion

        #region ClusterLocation

        public const String ClusterLocationId = "ClusterLocation_Id";
        public const String ClusterLocationClusterId = "Cluster_Id";
        public const String ClusterLocationLocationId = "Location_Id";
        public const String ClusterLocationSetProperties = "ClusterLocation_SetProperties";

        #endregion

        #region ClusterScheme

        public const String ClusterSchemeId = "ClusterScheme_Id";
        public const String ClusterSchemeRowVersion = "ClusterScheme_RowVersion";
        public const String ClusterSchemeUniqueContentReference = "ClusterScheme_UniqueContentReference";
        public const String ClusterSchemeName = "ClusterScheme_Name";
        public const String ClusterSchemeIsDefault = "ClusterScheme_IsDefault";
        public const String ClusterSchemeEntityId = "Entity_Id";
        public const String ClusterSchemeProductGroupId = "ProductGroup_Id";
        public const String ClusterSchemeDateCreated = "ClusterScheme_DateCreated";
        public const String ClusterSchemeDateLastModified = "ClusterScheme_DateLastModified";
        public const String ClusterSchemeSetProperties = "ClusterScheme_SetProperties";
        public const String ClusterSchemeParentUniqueContentReference = "ClusterScheme_ParentUniqueContentReference";


        #endregion

        #region ClusterSchemeInfo
        public const String ClusterSchemeInfoId = "ClusterScheme_Id";
        public const String ClusterSchemeInfoUniqueContentReference = "ClusterScheme_UniqueContentReference";
        public const String ClusterSchemeInfoName = "ClusterScheme_Name";
        public const String ClusterSchemeInfoIsDefault = "ClusterScheme_IsDefault";
        public const String ClusterSchemeInfoClusterCount = "Cluster_Count";
        public const String ClusterSchemeInfoEntityId = "Entity_Id";
        public const String ClusterSchemeInfoProductGroupId = "ProductGroup_Id";
        public const String ClusterSchemeInfoDateLastModified = "ClusterScheme_DateLastModified";
        #endregion

        #region Compression

        public const String CompressionId = "Compression_Id";
        public const String CompressionRowVersion = "Compression_RowVersion";
        public const String CompressionWidth = "Compression_Width";
        public const String CompressionHeight = "Compression_Height";
        public const String CompressionColourDepth = "Compression_ColourDepth";
        public const String CompressionMaintainAspectRatio = "Compression_MaintainAspectRatio";
        public const String CompressionEnabled = "Compression_Enabled";
        public const String CompressionName = "Compression_Name";

        #endregion

        #region ConsumerDecisionTree

        public const String ConsumerDecisionTreeId = "ConsumerDecisionTree_Id";
        public const String ConsumerDecisionTreeRowVersion = "ConsumerDecisionTree_RowVersion";
        public const String ConsumerDecisionTreeUniqueContentReference = "ConsumerDecisionTree_UniqueContentReference";
        public const String ConsumerDecisionTreeName = "ConsumerDecisionTree_Name";
        public const String ConsumerDecisionTreeEntityId = "Entity_Id";
        public const String ConsumerDecisionTreeProductGroupId = "ProductGroup_Id";
        public const String ConsumerDecisionTreeType = "ConsumerDecisionTree_Type";
        public const String ConsumerDecisionTreeDateCreated = "ConsumerDecisionTree_DateCreated";
        public const String ConsumerDecisionTreeDateLastModified = "ConsumerDecisionTree_DateLastModified";
        public const String ConsumerDecisionTreeParentUniqueContentReference = "ConsumerDecisionTree_ParentUniqueContentReference";

        #endregion

        #region ConsumerDecisionTreeNode

        public const String ConsumerDecisionTreeNodeId = "ConsumerDecisionTreeNode_Id";
        public const String ConsumerDecisionTreeNodeGFSId = "ConsumerDecisionTreeNode_GFSId";
        public const String ConsumerDecisionTreeNodeConsumerDecisionTreeId = "ConsumerDecisionTree_Id";
        public const String ConsumerDecisionTreeNodeConsumerDecisionTreeLevelId = "ConsumerDecisionTreeLevel_Id";
        public const String ConsumerDecisionTreeNodeName = "ConsumerDecisionTreeNode_Name";
        public const String ConsumerDecisionTreeNodeParentNodeId = "ConsumerDecisionTreeNode_ParentNodeId";
        public const String ConsumerDecisionTreeNodeDateCreated = "ConsumerDecisionTreeNode_DateCreated";
        public const String ConsumerDecisionTreeNodeDateLastModified = "ConsumerDecisionTreeNode_DateLastModified";
        public const String ConsumerDecisionTreeNodeSetProperties = "ConsumerDecisionTreeNode_SetProperties";

        #endregion

        #region ConsumerDecisionTreeLevel

        public const String ConsumerDecisionTreeLevelId = "ConsumerDecisionTreeLevel_Id";
        public const String ConsumerDecisionTreeLevelConsumerDecisionTreeId = "ConsumerDecisionTree_Id";
        public const String ConsumerDecisionTreeLevelName = "ConsumerDecisionTreeLevel_Name";
        public const String ConsumerDecisionTreeLevelParentLevelId = "ConsumerDecisionTreeLevel_ParentLevelId";
        public const String ConsumerDecisionTreeLevelDateCreated = "ConsumerDecisionTreeLevel_DateCreated";
        public const String ConsumerDecisionTreeLevelDateLastModified = "ConsumerDecisionTreeLevel_DateLastModified";
        public const String ConsumerDecisionTreeLevelSetProperties = "ConsumerDecisionTreeLevel_SetProperties";

        #endregion

        #region ConsumerDecisionTreeNodeProduct

        public const String ConsumerDecisionTreeNodeProductId = "ConsumerDecisionTreeNodeProduct_Id";
        public const String ConsumerDecisionTreeNodeProductConsumerDecisionTreeNodeId = "ConsumerDecisionTreeNode_Id";
        public const String ConsumerDecisionTreeNodeProductProductId = "Product_Id";
        public const String ConsumerDecisionTreeNodeProductProductGtin = "Product_Gtin";
        public const String ConsumerDecisionTreeNodeProductDateCreated = "ConsumerDecisionTreeNodeProduct_DateCreated";
        public const String ConsumerDecisionTreeNodeProductDateLastModified = "ConsumerDecisionTreeNodeProduct_DateLastModified";

        #endregion

        #region CustomAttributeData

        public const String CustomAttributeDataId = "CustomAttributeData_Id";
        public const String CustomAttributeDataParentId = "CustomAttributeData_ParentId";
        public const String CustomAttributeDataParentType = "CustomAttributeData_ParentType";
        public const String CustomAttributeDataText1 = "CustomAttributeData_Text1";
        public const String CustomAttributeDataText2 = "CustomAttributeData_Text2";
        public const String CustomAttributeDataText3 = "CustomAttributeData_Text3";
        public const String CustomAttributeDataText4 = "CustomAttributeData_Text4";
        public const String CustomAttributeDataText5 = "CustomAttributeData_Text5";
        public const String CustomAttributeDataText6 = "CustomAttributeData_Text6";
        public const String CustomAttributeDataText7 = "CustomAttributeData_Text7";
        public const String CustomAttributeDataText8 = "CustomAttributeData_Text8";
        public const String CustomAttributeDataText9 = "CustomAttributeData_Text9";
        public const String CustomAttributeDataText10 = "CustomAttributeData_Text10";
        public const String CustomAttributeDataText11 = "CustomAttributeData_Text11";
        public const String CustomAttributeDataText12 = "CustomAttributeData_Text12";
        public const String CustomAttributeDataText13 = "CustomAttributeData_Text13";
        public const String CustomAttributeDataText14 = "CustomAttributeData_Text14";
        public const String CustomAttributeDataText15 = "CustomAttributeData_Text15";
        public const String CustomAttributeDataText16 = "CustomAttributeData_Text16";
        public const String CustomAttributeDataText17 = "CustomAttributeData_Text17";
        public const String CustomAttributeDataText18 = "CustomAttributeData_Text18";
        public const String CustomAttributeDataText19 = "CustomAttributeData_Text19";
        public const String CustomAttributeDataText20 = "CustomAttributeData_Text20";
        public const String CustomAttributeDataText21 = "CustomAttributeData_Text21";
        public const String CustomAttributeDataText22 = "CustomAttributeData_Text22";
        public const String CustomAttributeDataText23 = "CustomAttributeData_Text23";
        public const String CustomAttributeDataText24 = "CustomAttributeData_Text24";
        public const String CustomAttributeDataText25 = "CustomAttributeData_Text25";
        public const String CustomAttributeDataText26 = "CustomAttributeData_Text26";
        public const String CustomAttributeDataText27 = "CustomAttributeData_Text27";
        public const String CustomAttributeDataText28 = "CustomAttributeData_Text28";
        public const String CustomAttributeDataText29 = "CustomAttributeData_Text29";
        public const String CustomAttributeDataText30 = "CustomAttributeData_Text30";
        public const String CustomAttributeDataText31 = "CustomAttributeData_Text31";
        public const String CustomAttributeDataText32 = "CustomAttributeData_Text32";
        public const String CustomAttributeDataText33 = "CustomAttributeData_Text33";
        public const String CustomAttributeDataText34 = "CustomAttributeData_Text34";
        public const String CustomAttributeDataText35 = "CustomAttributeData_Text35";
        public const String CustomAttributeDataText36 = "CustomAttributeData_Text36";
        public const String CustomAttributeDataText37 = "CustomAttributeData_Text37";
        public const String CustomAttributeDataText38 = "CustomAttributeData_Text38";
        public const String CustomAttributeDataText39 = "CustomAttributeData_Text39";
        public const String CustomAttributeDataText40 = "CustomAttributeData_Text40";
        public const String CustomAttributeDataText41 = "CustomAttributeData_Text41";
        public const String CustomAttributeDataText42 = "CustomAttributeData_Text42";
        public const String CustomAttributeDataText43 = "CustomAttributeData_Text43";
        public const String CustomAttributeDataText44 = "CustomAttributeData_Text44";
        public const String CustomAttributeDataText45 = "CustomAttributeData_Text45";
        public const String CustomAttributeDataText46 = "CustomAttributeData_Text46";
        public const String CustomAttributeDataText47 = "CustomAttributeData_Text47";
        public const String CustomAttributeDataText48 = "CustomAttributeData_Text48";
        public const String CustomAttributeDataText49 = "CustomAttributeData_Text49";
        public const String CustomAttributeDataText50 = "CustomAttributeData_Text50";
        public const String CustomAttributeDataValue1 = "CustomAttributeData_Value1";
        public const String CustomAttributeDataValue2 = "CustomAttributeData_Value2";
        public const String CustomAttributeDataValue3 = "CustomAttributeData_Value3";
        public const String CustomAttributeDataValue4 = "CustomAttributeData_Value4";
        public const String CustomAttributeDataValue5 = "CustomAttributeData_Value5";
        public const String CustomAttributeDataValue6 = "CustomAttributeData_Value6";
        public const String CustomAttributeDataValue7 = "CustomAttributeData_Value7";
        public const String CustomAttributeDataValue8 = "CustomAttributeData_Value8";
        public const String CustomAttributeDataValue9 = "CustomAttributeData_Value9";
        public const String CustomAttributeDataValue10 = "CustomAttributeData_Value10";
        public const String CustomAttributeDataValue11 = "CustomAttributeData_Value11";
        public const String CustomAttributeDataValue12 = "CustomAttributeData_Value12";
        public const String CustomAttributeDataValue13 = "CustomAttributeData_Value13";
        public const String CustomAttributeDataValue14 = "CustomAttributeData_Value14";
        public const String CustomAttributeDataValue15 = "CustomAttributeData_Value15";
        public const String CustomAttributeDataValue16 = "CustomAttributeData_Value16";
        public const String CustomAttributeDataValue17 = "CustomAttributeData_Value17";
        public const String CustomAttributeDataValue18 = "CustomAttributeData_Value18";
        public const String CustomAttributeDataValue19 = "CustomAttributeData_Value19";
        public const String CustomAttributeDataValue20 = "CustomAttributeData_Value20";
        public const String CustomAttributeDataValue21 = "CustomAttributeData_Value21";
        public const String CustomAttributeDataValue22 = "CustomAttributeData_Value22";
        public const String CustomAttributeDataValue23 = "CustomAttributeData_Value23";
        public const String CustomAttributeDataValue24 = "CustomAttributeData_Value24";
        public const String CustomAttributeDataValue25 = "CustomAttributeData_Value25";
        public const String CustomAttributeDataValue26 = "CustomAttributeData_Value26";
        public const String CustomAttributeDataValue27 = "CustomAttributeData_Value27";
        public const String CustomAttributeDataValue28 = "CustomAttributeData_Value28";
        public const String CustomAttributeDataValue29 = "CustomAttributeData_Value29";
        public const String CustomAttributeDataValue30 = "CustomAttributeData_Value30";
        public const String CustomAttributeDataValue31 = "CustomAttributeData_Value31";
        public const String CustomAttributeDataValue32 = "CustomAttributeData_Value32";
        public const String CustomAttributeDataValue33 = "CustomAttributeData_Value33";
        public const String CustomAttributeDataValue34 = "CustomAttributeData_Value34";
        public const String CustomAttributeDataValue35 = "CustomAttributeData_Value35";
        public const String CustomAttributeDataValue36 = "CustomAttributeData_Value36";
        public const String CustomAttributeDataValue37 = "CustomAttributeData_Value37";
        public const String CustomAttributeDataValue38 = "CustomAttributeData_Value38";
        public const String CustomAttributeDataValue39 = "CustomAttributeData_Value39";
        public const String CustomAttributeDataValue40 = "CustomAttributeData_Value40";
        public const String CustomAttributeDataValue41 = "CustomAttributeData_Value41";
        public const String CustomAttributeDataValue42 = "CustomAttributeData_Value42";
        public const String CustomAttributeDataValue43 = "CustomAttributeData_Value43";
        public const String CustomAttributeDataValue44 = "CustomAttributeData_Value44";
        public const String CustomAttributeDataValue45 = "CustomAttributeData_Value45";
        public const String CustomAttributeDataValue46 = "CustomAttributeData_Value46";
        public const String CustomAttributeDataValue47 = "CustomAttributeData_Value47";
        public const String CustomAttributeDataValue48 = "CustomAttributeData_Value48";
        public const String CustomAttributeDataValue49 = "CustomAttributeData_Value49";
        public const String CustomAttributeDataValue50 = "CustomAttributeData_Value50";
        public const String CustomAttributeDataFlag1 = "CustomAttributeData_Flag1";
        public const String CustomAttributeDataFlag2 = "CustomAttributeData_Flag2";
        public const String CustomAttributeDataFlag3 = "CustomAttributeData_Flag3";
        public const String CustomAttributeDataFlag4 = "CustomAttributeData_Flag4";
        public const String CustomAttributeDataFlag5 = "CustomAttributeData_Flag5";
        public const String CustomAttributeDataFlag6 = "CustomAttributeData_Flag6";
        public const String CustomAttributeDataFlag7 = "CustomAttributeData_Flag7";
        public const String CustomAttributeDataFlag8 = "CustomAttributeData_Flag8";
        public const String CustomAttributeDataFlag9 = "CustomAttributeData_Flag9";
        public const String CustomAttributeDataFlag10 = "CustomAttributeData_Flag10";
        public const String CustomAttributeDataDate1 = "CustomAttributeData_Date1";
        public const String CustomAttributeDataDate2 = "CustomAttributeData_Date2";
        public const String CustomAttributeDataDate3 = "CustomAttributeData_Date3";
        public const String CustomAttributeDataDate4 = "CustomAttributeData_Date4";
        public const String CustomAttributeDataDate5 = "CustomAttributeData_Date5";
        public const String CustomAttributeDataDate6 = "CustomAttributeData_Date6";
        public const String CustomAttributeDataDate7 = "CustomAttributeData_Date7";
        public const String CustomAttributeDataDate8 = "CustomAttributeData_Date8";
        public const String CustomAttributeDataDate9 = "CustomAttributeData_Date9";
        public const String CustomAttributeDataDate10 = "CustomAttributeData_Date10";
        public const String CustomAttributeDataNote1 = "CustomAttributeData_Note1";
        public const String CustomAttributeDataNote2 = "CustomAttributeData_Note2";
        public const String CustomAttributeDataNote3 = "CustomAttributeData_Note3";
        public const String CustomAttributeDataNote4 = "CustomAttributeData_Note4";
        public const String CustomAttributeDataNote5 = "CustomAttributeData_Note5";
        public const String CustomAttributeDataSetProperties = "CustomAttributeData_SetProperties";

        #endregion

        #region Data Info

        public const String DataInfoHasProductHierarchy = "DataInfo_HasProductHierarchy";
        public const String DataInfoHasLocationHierarchy = "DataInfo_HasLocationHierarchy";
        public const String DataInfoHasLocation = "DataInfo_HasLocation";
        public const String DataInfoHasProduct = "DataInfo_HasProduct";
        public const String DataInfoHasAssortment = "DataInfo_HasAssortment";
        public const String DataInfoHasLocationSpace = "DataInfo_HasLocationSpace";
        public const String DataInfoHasLocationSpaceBay = "DataInfo_HasLocationSpaceBay";
        public const String DataInfoHasLocationSpaceElement = "DataInfo_HasLocationSpaceElement";
        public const String DataInfoHasClusterScheme = "DataInfo_HasClusterScheme";
        public const String DataInfoHasLocationProductAttribute = "DataInfo_HasLocationProductAttribute";
        public const String DataInfoHasLocationProductIllegal = "DataInfo_HasLocationProductIllegal";
        public const String DataInfoHasLocationProductLegal = "DataInfo_HasLocationProductLegal";

        #endregion

        #region EngineInstance

        public const String EngineInstanceId = "Instance_Id";
        public const String EngineInstanceComputerName = "Instance_ComputerName";
        public const String EngineInstanceWorkerCount = "Instance_WorkerCount";
        public const String EngineInstanceLifespan = "Instance_Lifespan";
        public const String EngineInstanceDateLastActive = "Instance_DateLastActive";
        public const String EngineInstanceCount = "Instance_Count";

        #endregion

        #region EngineMessage

        public const String EngineMessageId = "Message_Id";
        public const String EngineMessagePriority = "Message_Priority";
        public const String EngineMessageData = "Message_Data";
        public const String EngineMessageDateCreated = "Message_DateCreated";
        public const String EngineMessageDateScheduled = "Message_DateScheduled";
        public const String EngineMessageDateProcessed = "Message_DateProcessed";
        public const String EngineMessageTimeout = "Message_Timeout";
        public const String EngineMessageCount = "Message_Count";
        public const String EngineMessageProcessCount = "Message_ProcessCount";
        public const String EngineMessagePredecessorId = "Message_PredecessorId";
        public const String EngineMessageDelay = "Message_Delay";
        public const String EngineMessageCriteria = "Message_Criteria";

        #endregion

        #region EngineMetric

        public const String EngineMetricType = "Metric_Type";
        public const String EngineMetricName = "Metric_Name";
        public const String EngineMetricTimestamp = "Metric_Timestamp";
        public const String EngineMetricDuration = "Metric_Duration";
        public const String EngineMetricCount = "Metric_Count";

        #endregion

        #region EngineParameter

        public const String EngineParameterRowVersion = "Parameter_RowVersion";
        public const String EngineParameterName = "Parameter_Name";
        public const String EngineParameterValue = "Parameter_Value";

        #endregion

        #region Entity

        public const String EntityId = "Entity_Id";
        public const String EntityRowVersion = "Entity_RowVersion";
        public const String EntityName = "Entity_Name";
        public const String EntityDescription = "Entity_Description";
        public const String EntityGFSEntityId = "Entity_GFSId";
        public const String EntityLengthUnitsOfMeasure = "Entity_LengthUnitsOfMeasure";
        public const String EntityAngleUnitsOfMeasure = "Entity_AngleOfUnitsMeasure";
        public const String EntityAreaUnitsOfMeasure = "Entity_AreaUnitsOfMeasure";
        public const String EntityVolumeUnitsOfMeasure = "Entity_VolumeUnitsOfMeasure";
        public const String EntityWeightUnitsOfMeasure = "Entity_WeightUnitsOfMeasure";
        public const String EntityCurrencyUnitsOfMeasure = "Entity_CurrencyUnitsOfMeasure";
        public const String EntityDateCreated = "Entity_DateCreated";
        public const String EntityDateLastModified = "Entity_DateLastModified";
        public const String EntityDateDeleted = "Entity_DateDeleted";
        public const String EntityDateLastMerchSynced = "Entity_DateLastMerchSynced";

        #endregion

        #region EntityProductAttributeComparisonAttribute

        public const String EntityComparisonAttributeId = "EntityProductAttributeComparisonAttribute_Id";
        public const String EntityComparisonAttributeEntityId = "Entity_Id";
        public const String EntityComparisonAttributePropertyName = "EntityProductAttributeComparisonAttribute_PropertyName";
        public const String EntityComparisonAttributePropertyDisplayName = "EntityProductAttributeComparisonAttribute_PropertyDisplayName";
        public const String EntityComparisonAttributeItemType = "EntityProductAttributeComparisonAttribute_ItemType";

        #endregion

        #region ProductComparisonAttributeResults

        public const String ProductComparisonAttributeResultsId = "ProductComparisonAttributeResults_Id";
        public const String ProductComparisonAttributeResultsPlanogramId = "ProductComparisonAttributeResults_PlanogramId";
        public const String ProductComparisonAttributeResultsProductGtin = "ProductComparisonAttributeResults_ProductGTIN";
        public const String ProductComparisonAttributeResultsComparedProductAttribute = "ProductComparisonAttributeResults_ComparedProductAttribute";
        public const String ProductComparisonAttributeResultsMasterDataValue = "ProductComparisonAttributeResults_MasterDataValue";
        public const String ProductComparisonAttributeResultsProductValue = "ProductComparisonAttributeResults_ProductValue";


        #endregion

        #region EventLog

        public const String EventLogId = "EventLog_Id";
        public const String EventLogEntityId = "Entity_Id";
        public const String EventLogEventLogNameId = "EventLogName_Id";
        public const String EventLogSource = "EventLog_Source";
        public const String EventLogEventId = "EventLog_Event_Id";
        public const String EventLogEntryType = "EventLog_EntryType";
        public const String EventLogUserId = "EventLog_User_Id";
        public const String EventLogDateTime = "EventLog_DateTime";
        public const String EventLogProcess = "EventLog_Process";
        public const String EventLogComputerName = "EventLog_ComputerName";
        public const String EventLogURLHelperLink = "EventLog_URLHelperLink";
        public const String EventLogDescription = "EventLog_Description";
        public const String EventLogContent = "EventLog_Content";
        public const String EventLogGibraltarSessionId = "EventLog_GibraltarSessionId";
        public const String EventLogStartDate = "StartDateTime";
        public const String EventLogEndDate = "EndDateTime";
        public const String EventLogWorkpackageName = "EventLog_WorkpackageName";
        public const String EventLogWorkpackageId = "EventLog_WorkpackageId";

        #endregion

        #region EventLogName

        public const String EventLogNameId = "EventLogName_Id";
        public const String EventLogNameName = "EventLogName_Name";

        #endregion

        #region File

        public const String FileId = "File_Id";
        public const String FileRowVersion = "File_RowVersion";
        public const String FileEntityId = "Entity_Id";
        public const String FileBlobId = "Blob_Id";
        public const String FileUserId = "User_Id";
        public const String FileName = "File_Name";
        public const String FileSourceFilePath = "File_SourceFilePath";
        public const String FileSizeInBytes = "File_SizeInBytes";
        public const String FileData = "File_Data";
        public const String FileDateCreated = "File_DateCreated";
        public const String FileDateLastModified = "File_DateLastModified";
        public const String FileDateDeleted = "File_DateDeleted";
        public const String AssortmentFileUserName = "AssortmentFile_UserName";

        #endregion

        #region Generic

        public const String GenericPageNumber = "PageNumber";
        public const String GenericPageSize = "PageSize";

        #endregion

        #region  Highlight

        public const String HighlightId = "Highlight_Id";
        public const String HighlightRowVersion = "Highlight_RowVersion";
        public const String HighlightEntityId = "Entity_Id";
        public const String HighlightName = "Highlight_Name";
        public const String HighlightType = "Highlight_Type";
        public const String HighlightMethodType = "Highlight_MethodType";
        public const String HighlightIsFilterEnabled = "Highlight_IsFilterEnabled";
        public const String HighlightIsPreFilter = "Highlight_IsPreFilter";
        public const String HighlightIsAndFilter = "Highlight_IsAndFilter";
        public const String HighlightField1 = "Highlight_Field1";
        public const String HighlightField2 = "Highlight_Field2";
        public const String HighlightQuadrantXType = "Highlight_QuadrantXType";
        public const String HighlightQuadrantYType = "Highlight_QuadrantYType";
        public const String HighlightQuadrantXConstant = "Highlight_QuadrantXConstant";
        public const String HighlightQuadrantYConstant = "Highlight_QuadrantYConstant";
        public const String HighlightDateCreated = "Highlight_DateCreated";
        public const String HighlightDateLastModified = "Highlight_DateLastModified";
        public const String HighlightDateDeleted = "Highlight_DateDeleted";

        #endregion

        #region  HighlightInfo

        public const String HighlightInfoId = "HighlightInfo_HighlightId";
        public const String HighlightInfoName = "HighlightInfo_HighlightName";

        #endregion

        #region  HighlightCharacteristic

        public const String HighlightCharacteristicId = "HighlightCharacteristic_Id";
        public const String HighlightCharacteristicHighlightId = "Highlight_Id";
        public const String HighlightCharacteristicIsAndFilter = "HighlightCharacteristic_IsAndFilter";
        public const String HighlightCharacteristicName = "HighlightCharacteristic_Name";

        #endregion

        #region  HighlightCharacteristicRule

        public const String HighlightCharacteristicRuleId = "HighlightCharacteristicRule_Id";
        public const String HighlightCharacteristicRuleCharacteristicId = "HighlightCharacteristic_Id";
        public const String HighlightCharacteristicRuleName = "HighlightCharacteristicRule_Name";
        public const String HighlightCharacteristicRuleField = "HighlightCharacteristicRule_Field";
        public const String HighlightCharacteristicRuleValue = "HighlightCharacteristicRule_Value";
        public const String HighlightCharacteristicRuleType = "HighlightCharacteristicRule_Type";

        #endregion

        #region  HighlightFilter

        public const String HighlightFilterId = "HighlightFilter_Id";
        public const String HighlightFilterHighlightId = "Highlight_Id";
        public const String HighlightFilterName = "HighlightFilter_Name";
        public const String HighlightFilterField = "HighlightFilter_Field";
        public const String HighlightFilterValue = "HighlightFilter_Value";
        public const String HighlightFilterType = "HighlightFilter_Type";

        #endregion

        #region  HighlightGroup

        public const String HighlightGroupId = "HighlightGroup_Id";
        public const String HighlightGroupHighlightId = "Highlight_Id";
        public const String HighlightGroupOrder = "HighlightGroup_Order";
        public const String HighlightGroupName = "HighlightGroup_Name";
        public const String HighlightGroupDisplayName = "HighlightGroup_DisplayName";
        public const String HighlightGroupFillColour = "HighlightGroup_FillColour";
        public const String HighlightGroupFillPatternType = "HighlightGroup_FillPatternType";

        #endregion

        #region Image

        public const String ImageId = "Image_Id";
        public const String ImageRowVersion = "Image_RowVersion";
        public const String ImageEntityId = "Entity_Id";
        public const String ImageOriginalFileId = "File_Id";
        public const String ImageBlobId = "Blob_Id";
        public const String ImageCompressionId = "Compression_Id";
        public const String ImageHeight = "Image_Height";
        public const String ImageWidth = "Image_Width";
        public const String ImageSizeInBytes = "Image_SizeInBytes";
        public const String ImageDateCreated = "Image_DateCreated";
        public const String ImageDateLastModified = "Image_DateLastModified";
        public const String ImageDateDeleted = "Image_DateDeleted";

        #endregion

        #region InventoryProfile

        public const String InventoryProfileId = "InventoryProfile_Id";
        public const String InventoryProfileRowVersion = "InventoryProfile_RowVersion";
        public const String InventoryProfileEntityId = "Entity_Id";
        public const String InventoryProfileName = "InventoryProfile_Name";
        public const String InventoryProfileCasePack = "InventoryProfile_CasePack";
        public const String InventoryProfileDaysOfSupply = "InventoryProfile_DaysOfSupply";
        public const String InventoryProfileShelfLife = "InventoryProfile_ShelfLife";
        public const String InventoryProfileReplenishmentDays = "InventoryProfile_ReplenishmentDays";
        public const String InventoryProfileWasteHurdleUnits = "InventoryProfile_WasteHurdleUnits";
        public const String InventoryProfileWasteHurdleCasePack = "InventoryProfile_WasteHurdleCasePack";
        public const String InventoryProfileMinUnits = "InventoryProfile_MinUnits";
        public const String InventoryProfileMinFacings = "InventoryProfile_MinFacings";
        public const String InventoryProfileType = "InventoryProfile_InventoryProfileType";
        public const String InventoryProfileDateCreated = "InventoryProfile_DateCreated";
        public const String InventoryProfileDateLastModified = "InventoryProfile_DateLastModified";
        public const String InventoryProfileDateDeleted = "InventoryProfile_DateDeleted";
        public const String InventoryProfileSetProperties = "InventoryProfile_SetProperties";

        #endregion

        #region Label

        public const String LabelId = "Label_Id";
        public const String LabelRowVersion = "Label_RowVersion";
        public const String LabelEntityId = "Entity_Id";
        public const String LabelName = "Label_Name";
        public const String LabelType = "Label_Type";
        public const String LabelBackgroundColour = "Label_BackgroundColour";
        public const String LabelBorderColour = "Label_BorderColour";
        public const String LabelBorderThickness = "Label_BorderThickness";
        public const String LabelText = "Label_Text";
        public const String LabelTextColour = "Label_TextColour";
        public const String LabelFont = "Label_Font";
        public const String LabelFontSize = "Label_FontSize";
        public const String LabelIsRotateToFitOn = "Label_IsRotateToFitOn";
        public const String LabelIsShrinkToFitOn = "Label_IsShrinkToFitOn";
        public const String LabelTextBoxFontBold = "Label_TextBoxFontBold";
        public const String LabelTextBoxFontItalic = "Label_TextBoxFontItalic";
        public const String LabelTextBoxFontUnderlined = "Label_TextBoxFontUnderlined";
        public const String LabelLabelHorizontalPlacement = "Label_LabelHorizontalPlacement";
        public const String LabelLabelVerticalPlacement = "Label_LabelVerticalPlacement";
        public const String LabelTextBoxTextAlignment = "Label_TextBoxTextAlignment";
        public const String LabelTextDirection = "Label_TextDirection";
        public const String LabelTextWrapping = "Label_TextWrapping";
        public const String LabelBackgroundTransparency = "Label_BackgroundTransparency";
        public const String LabelShowOverImages = "Label_ShowOverImages";
        public const String LabelShowLabelPerFacing = "Label_ShowLabelPerFacing";
        public const String LabelDateCreated = "Label_DateCreated";
        public const String LabelDateLastModified = "Label_DateLastModified";
        public const String LabelDateDeleted = "Label_DateDeleted";

        #endregion

        #region Location

        public const String LocationId = "Location_Id";
        public const String LocationRowVersion = "Location_RowVersion";
        public const String LocationEntityId = "Entity_Id";
        public const String LocationLocationGroupId = "LocationGroup_Id";
        public const String LocationCode = "Location_Code";
        public const String LocationName = "Location_Name";
        public const String LocationRegion = "Location_Region";
        public const String LocationTVRegion = "Location_TVRegion";
        public const String LocationLocation = "Location_Location";
        public const String LocationDefaultClusterAttribute = "Location_DefaultClusterAttribute";
        public const String LocationAddress1 = "Location_Address1";
        public const String LocationAddress2 = "Location_Address2";
        public const String LocationCity = "Location_City";
        public const String LocationCounty = "Location_County";
        public const String LocationState = "Location_State";
        public const String LocationPostalCode = "Location_PostalCode";
        public const String LocationCountry = "Location_Country";
        public const String LocationLongitude = "Location_Longitude";
        public const String LocationLatitude = "Location_Latitude";
        public const String LocationDateOpen = "Location_DateOpen";
        public const String LocationDateLastRefitted = "Location_DateLastRefitted";
        public const String LocationDateClosed = "Location_DateClosed";
        public const String LocationCarParkSpaces = "Location_CarParkSpaces";
        public const String LocationCarParkManagement = "Location_CarParkManagement";
        public const String LocationPetrolForecourtType = "Location_PetrolForecourtType";
        public const String LocationRestaurant = "Location_Restaurant";
        public const String LocationSizeGrossFloorArea = "Location_SizeGrossFloorArea";
        public const String LocationSizeNetSalesArea = "Location_SizeNetSalesArea";
        public const String LocationSizeMezzSalesArea = "Location_SizeMezzSalesArea";
        public const String LocationTelephoneCountryCode = "Location_TelephoneCountryCode";
        public const String LocationTelephoneAreaCode = "Location_TelephoneAreaCode";
        public const String LocationTelephoneNumber = "Location_TelephoneNumber";
        public const String LocationFaxCountryCode = "Location_FaxCountryCode";
        public const String LocationFaxAreaCode = "Location_FaxAreaCode";
        public const String LocationFaxNumber = "Location_FaxNumber";
        public const String LocationOpeningHours = "Location_OpeningHours";
        public const String LocationAverageOpeningHours = "Location_AverageOpeningHours";
        public const String LocationManagerName = "Location_ManagerName";
        public const String LocationManagerEmail = "Location_ManagerEmail";
        public const String LocationRegionalManagerName = "Location_RegionalManagerName";
        public const String LocationRegionalManagerEmail = "Location_RegionalManagerEmail";
        public const String LocationDivisionalManagerName = "Location_DivisionalManagerName";
        public const String LocationDivisionalManagerEmail = "Location_DivisionalManagerEmail";
        public const String LocationAdvertisingZone = "Location_AdvertisingZone";
        public const String LocationDistributionCentre = "Location_DistributionCentre";
        public const String LocationNoOfCheckouts = "Location_NoOfCheckouts";
        public const String LocationIsMezzFitted = "Location_IsMezzFitted";
        public const String LocationIsFreehold = "Location_IsFreehold";
        public const String LocationIs24Hours = "Location_Is24Hours";
        public const String LocationIsOpenMonday = "Location_IsOpenMonday";
        public const String LocationIsOpenTuesday = "Location_IsOpenTuesday";
        public const String LocationIsOpenWednesday = "Location_IsOpenWednesday";
        public const String LocationIsOpenThursday = "Location_IsOpenThursday";
        public const String LocationIsOpenFriday = "Location_IsOpenFriday";
        public const String LocationIsOpenSaturday = "Location_IsOpenSaturday";
        public const String LocationIsOpenSunday = "Location_IsOpenSunday";
        public const String LocationHasPetrolForecourt = "Location_HasPetrolForecourt";
        public const String LocationHasNewsCube = "Location_HasNewsCube";
        public const String LocationHasAtmMachines = "Location_HasAtmMachines";
        public const String LocationHasCustomerWC = "Location_HasCustomerWC";
        public const String LocationHasBabyChanging = "Location_HasBabyChanging";
        public const String LocationHasInStoreBakery = "Location_HasInStoreBakery";
        public const String LocationHasHotFoodToGo = "Location_HasHotFoodToGo";
        public const String LocationHasRotisserie = "Location_HasRotisserie";
        public const String LocationHasFishmonger = "Location_HasFishmonger";
        public const String LocationHasButcher = "Location_HasButcher";
        public const String LocationHasPizza = "Location_HasPizza";
        public const String LocationHasDeli = "Location_HasDeli";
        public const String LocationHasSaladBar = "Location_HasSaladBar";
        public const String LocationHasOrganic = "Location_HasOrganic";
        public const String LocationHasGrocery = "Location_HasGrocery";
        public const String LocationHasMobilePhones = "Location_HasMobilePhones";
        public const String LocationHasDryCleaning = "Location_HasDryCleaning";
        public const String LocationHasHomeShoppingAvailable = "Location_HasHomeShoppingAvailable";
        public const String LocationHasOptician = "Location_HasOptician";
        public const String LocationHasPharmacy = "Location_HasPharmacy";
        public const String LocationHasTravel = "Location_HasTravel";
        public const String LocationHasPhotoDepartment = "Location_HasPhotoDepartment";
        public const String LocationHasCarServiceArea = "Location_HasCarServiceArea";
        public const String LocationHasGardenCentre = "Location_HasGardenCentre";
        public const String LocationHasClinic = "Location_HasClinic";
        public const String LocationHasAlcohol = "Location_HasAlcohol";
        public const String LocationHasFashion = "Location_HasFashion";
        public const String LocationHasCafe = "Location_HasCafe";
        public const String LocationHasRecycling = "Location_HasRecycling";
        public const String LocationHasPhotocopier = "Location_HasPhotocopier";
        public const String LocationHasLottery = "Location_HasLottery";
        public const String LocationHasPostOffice = "Location_HasPostOffice";
        public const String LocationHasMovieRental = "Location_HasMovieRental";
        public const String LocationHasJewellery = "Location_HasJewellery";
        public const String LocationLocationType = "Location_LocationType";
        public const String LocationDateCreated = "Location_DateCreated";
        public const String LocationDateLastModified = "Location_DateLastModified";
        public const String LocationDateDeleted = "Location_DateDeleted";
        public const String LocationSetProperties = "Location_SetProperties";
        public const String LocationUpdateDeleted = "Location_UpdateDeleted";

        #endregion

        #region LocationGroup

        public const String LocationGroupId = "LocationGroup_Id";
        public const String LocationGroupLocationHierarchyId = "LocationHierarchy_Id";
        public const String LocationGroupLocationLevelId = "LocationLevel_Id";
        public const String LocationGroupName = "LocationGroup_Name";
        public const String LocationGroupCode = "LocationGroup_Code";
        public const String LocationGroupParentGroupId = "LocationGroup_ParentGroupId";
        public const String LocationGroupDateCreated = "LocationGroup_DateCreated";
        public const String LocationGroupDateLastModified = "LocationGroup_DateLastModified";
        public const String LocationGroupDateDeleted = "LocationGroup_DateDeleted";
        public const String LocationGroupAssignedLocationsCount = "LocationGroup_AssignedLocationsCount";
        public const String LocationGroupSetProperties = "LocationGroup_SetProperties";

        #endregion

        #region LocationHierarchy

        public const String LocationHierarchyId = "LocationHierarchy_Id";
        public const String LocationHierarchyRowVersion = "LocationHierarchy_RowVersion";
        public const String LocationHierarchyEntityId = "Entity_Id";
        public const String LocationHierarchyName = "LocationHierarchy_Name";
        public const String LocationHierarchyDateCreated = "LocationHierarchy_DateCreated";
        public const String LocationHierarchyDateLastModified = "LocationHierarchy_DateLastModified";
        public const String LocationHierarchyDateDeleted = "LocationHierarchy_DateDeleted";

        #endregion

        #region LocationLevel

        public const String LocationLevelId = "LocationLevel_Id";
        public const String LocationLevelLocationHierarchyId = "LocationHierarchy_Id";
        public const String LocationLevelName = "LocationLevel_Name";
        public const String LocationLevelShapeNo = "LocationLevel_ShapeNo";
        public const String LocationLevelColour = "LocationLevel_Colour";
        public const String LocationLevelParentLevelId = "LocationLevel_ParentLevelId";
        public const String LocationLevelDateCreated = "LocationLevel_DateCreated";
        public const String LocationLevelDateLastModified = "LocationLevel_DateLastModified";
        public const String LocationLevelDateDeleted = "LocationLevel_DateDeleted";

        #endregion

        #region LocationPlanAssignment

        public const String LocationPlanAssignmentId = "LocationPlanAssignment_Id";
        public const String LocationPlanAssignmentPlanId = "Planogram_Id";
        public const String LocationPlanAssignmentProductGroupId = "ProductGroup_Id";
        public const String LocationPlanAssignmentLocationId = "Location_Id";
        public const String LocationPlanAssignmentAssignedByUserId = "LocationPlanAssignment_AssignedByUserId";
        public const String LocationPlanAssignmentPublishUserId = "LocationPlanAssignment_PublishUserId";
        public const String LocationPlanAssignmentDateAssigned = "LocationPlanAssignment_DateAssigned";
        public const String LocationPlanAssignmentDatePublished = "LocationPlanAssignment_DatePublished";
        public const String LocationPlanAssignmentDateCommunicated = "LocationPlanAssignment_DateCommunicated";
        public const String LocationPlanAssignmentDateLive = "LocationPlanAssignment_DateLive";
        public const String LocationPlanAssignmentPublishType = "LocationPlanAssignment_PublishType";
        public const String LocationPlanAssignmentOutputDestination = "LocationPlanAssignment_OutputDestination";
        public const String LocationPlanAssignmentPublishStatus = "LocationPlanAssignment_PlanPublishStatus";

        #endregion

        #region LocationPlanAssignmentLocation

        public const String LocationPlanAssignmentLocationLocationId = "Location_Id";
        public const String LocationPlanAssignmentLocationLocationPlanAssignmentId = "LocationPlanAssignment_Id";
        public const String LocationPlanAssignmentLocationDateCreated = "LocationPlanAssignmentLocation_DateCreated";
        public const String LocationPlanAssignmentLocationDateLastModified = "LocationPlanAssignmentLocation_DateLastModified";
        public const String LocationPlanAssignmentLocationDateDeleted = "LocationPlanAssignmentLocation_DateDeleted";

        #endregion

        #region LocationProductAttribute

        public const String LocationProductAttributeProductId = "Product_Id";
        public const String LocationProductAttributeLocationId = "Location_Id";
        public const String LocationProductAttributeEntityId = "Entity_Id";
        public const String LocationProductAttributeRowVersion = "LocationProductAttribute_RowVersion";
        public const String LocationProductAttributeGTIN = "LocationProductAttribute_GTIN";
        public const String LocationProductAttributeDescription = "LocationProductAttribute_ProductDescription";
        public const String LocationProductAttributeHeight = "LocationProductAttribute_Height";
        public const String LocationProductAttributeWidth = "LocationProductAttribute_Width";
        public const String LocationProductAttributeDepth = "LocationProductAttribute_Depth";
        public const String LocationProductAttributeCasePackUnits = "LocationProductAttribute_CasePackUnits";
        public const String LocationProductAttributeCaseHigh = "LocationProductAttribute_CaseHigh";
        public const String LocationProductAttributeCaseWide = "LocationProductAttribute_CaseWide";
        public const String LocationProductAttributeCaseDeep = "LocationProductAttribute_CaseDeep";
        public const String LocationProductAttributeCaseHeight = "LocationProductAttribute_CaseHeight";
        public const String LocationProductAttributeCaseDepth = "LocationProductAttribute_CaseDepth";
        public const String LocationProductAttributeCaseWidth = "LocationProductAttribute_CaseWidth";
        public const String LocationProductAttributeDaysOfSupply = "LocationProductAttribute_DaysOfSupply";
        public const String LocationProductAttributeMinDeep = "LocationProductAttribute_MinDeep";
        public const String LocationProductAttributeMaxDeep = "LocationProductAttribute_MaxDeep";
        public const String LocationProductAttributeTrayPackUnits = "LocationProductAttribute_TrayPackUnits";
        public const String LocationProductAttributeTrayHigh = "LocationProductAttribute_TrayHigh";
        public const String LocationProductAttributeTrayWide = "LocationProductAttribute_TrayWide";
        public const String LocationProductAttributeTrayDeep = "LocationProductAttribute_TrayDeep";
        public const String LocationProductAttributeTrayHeight = "LocationProductAttribute_TrayHeight";
        public const String LocationProductAttributeTrayDepth = "LocationProductAttribute_TrayDepth";
        public const String LocationProductAttributeTrayWidth = "LocationProductAttribute_TrayWidth";
        public const String LocationProductAttributeTrayThickHeight = "LocationProductAttribute_TrayThickHeight";
        public const String LocationProductAttributeTrayThickDepth = "LocationProductAttribute_TrayThickDepth";
        public const String LocationProductAttributeTrayThickWidth = "LocationProductAttribute_TrayThickWidth";
        public const String LocationProductAttributeStatusType = "LocationProductAttribute_StatusType";
        public const String LocationProductAttributeShelfLife = "LocationProductAttribute_ShelfLife";
        public const String LocationProductAttributeDeliveryFrequencyDays = "LocationProductAttribute_DeliveryFrequencyDays";
        public const String LocationProductAttributeDeliveryMethod = "LocationProductAttribute_DeliveryMethod";
        public const String LocationProductAttributeVendorCode = "LocationProductAttribute_VendorCode";
        public const String LocationProductAttributeVendor = "LocationProductAttribute_Vendor";
        public const String LocationProductAttributeManufacturerCode = "LocationProductAttribute_ManufacturerCode";
        public const String LocationProductAttributeManufacturer = "LocationProductAttribute_Manufacturer";
        public const String LocationProductAttributeSize = "LocationProductAttribute_Size";
        public const String LocationProductAttributeUnitOfMeasure = "LocationProductAttribute_UnitOfMeasure";
        public const String LocationProductAttributeSellPrice = "LocationProductAttribute_SellPrice";
        public const String LocationProductAttributeSellPackCount = "LocationProductAttribute_SellPackCount";
        public const String LocationProductAttributeSellPackDescription = "LocationProductAttribute_SellPackDescription";
        public const String LocationProductAttributeRecommendedRetailPrice = "LocationProductAttribute_RecommendedRetailPrice";
        public const String LocationProductAttributeCostPrice = "LocationProductAttribute_CostPrice";
        public const String LocationProductAttributeCaseCost = "LocationProductAttribute_CaseCost";
        public const String LocationProductAttributeConsumerInformation = "LocationProductAttribute_ConsumerInformation";
        public const String LocationProductAttributePattern = "LocationProductAttribute_Pattern";
        public const String LocationProductAttributeModel = "LocationProductAttribute_Model";
        public const String LocationProductAttributeCorporateCode = "LocationProductAttribute_CorporateCode";
        public const String LocationProductAttributeDateCreated = "LocationProductAttribute_DateCreated";
        public const String LocationProductAttributeDateLastModified = "LocationProductAttribute_DateLastModified";

        //public const String LocationProductAttributeCustomAttribute01 = "LocationProductAttribute_CustomAttribute01";
        //public const String LocationProductAttributeCustomAttribute02 = "LocationProductAttribute_CustomAttribute02";
        //public const String LocationProductAttributeCustomAttribute03 = "LocationProductAttribute_CustomAttribute03";
        //public const String LocationProductAttributeCustomAttribute04 = "LocationProductAttribute_CustomAttribute04";
        //public const String LocationProductAttributeCustomAttribute05 = "LocationProductAttribute_CustomAttribute05";
        //public const String LocationProductAttributeCustomAttribute06 = "LocationProductAttribute_CustomAttribute06";
        //public const String LocationProductAttributeCustomAttribute07 = "LocationProductAttribute_CustomAttribute07";
        //public const String LocationProductAttributeCustomAttribute08 = "LocationProductAttribute_CustomAttribute08";
        //public const String LocationProductAttributeCustomAttribute09 = "LocationProductAttribute_CustomAttribute09";
        //public const String LocationProductAttributeCustomAttribute10 = "LocationProductAttribute_CustomAttribute10";
        //public const String LocationProductAttributeCustomAttribute11 = "LocationProductAttribute_CustomAttribute11";
        //public const String LocationProductAttributeCustomAttribute12 = "LocationProductAttribute_CustomAttribute12";
        //public const String LocationProductAttributeCustomAttribute13 = "LocationProductAttribute_CustomAttribute13";
        //public const String LocationProductAttributeCustomAttribute14 = "LocationProductAttribute_CustomAttribute14";
        //public const String LocationProductAttributeCustomAttribute15 = "LocationProductAttribute_CustomAttribute15";
        //public const String LocationProductAttributeCustomAttribute16 = "LocationProductAttribute_CustomAttribute16";
        //public const String LocationProductAttributeCustomAttribute17 = "LocationProductAttribute_CustomAttribute17";
        //public const String LocationProductAttributeCustomAttribute18 = "LocationProductAttribute_CustomAttribute18";
        //public const String LocationProductAttributeCustomAttribute19 = "LocationProductAttribute_CustomAttribute19";
        //public const String LocationProductAttributeCustomAttribute20 = "LocationProductAttribute_CustomAttribute20";
        //public const String LocationProductAttributeCustomAttribute21 = "LocationProductAttribute_CustomAttribute21";
        //public const String LocationProductAttributeCustomAttribute22 = "LocationProductAttribute_CustomAttribute22";
        //public const String LocationProductAttributeCustomAttribute23 = "LocationProductAttribute_CustomAttribute23";
        //public const String LocationProductAttributeCustomAttribute24 = "LocationProductAttribute_CustomAttribute24";
        //public const String LocationProductAttributeCustomAttribute25 = "LocationProductAttribute_CustomAttribute25";
        //public const String LocationProductAttributeCustomAttribute26 = "LocationProductAttribute_CustomAttribute26";
        //public const String LocationProductAttributeCustomAttribute27 = "LocationProductAttribute_CustomAttribute27";
        //public const String LocationProductAttributeCustomAttribute28 = "LocationProductAttribute_CustomAttribute28";
        //public const String LocationProductAttributeCustomAttribute29 = "LocationProductAttribute_CustomAttribute29";
        //public const String LocationProductAttributeCustomAttribute30 = "LocationProductAttribute_CustomAttribute30";
        //public const String LocationProductAttributeCustomAttribute31 = "LocationProductAttribute_CustomAttribute31";
        //public const String LocationProductAttributeCustomAttribute32 = "LocationProductAttribute_CustomAttribute32";
        //public const String LocationProductAttributeCustomAttribute33 = "LocationProductAttribute_CustomAttribute33";
        //public const String LocationProductAttributeCustomAttribute34 = "LocationProductAttribute_CustomAttribute34";
        //public const String LocationProductAttributeCustomAttribute35 = "LocationProductAttribute_CustomAttribute35";
        //public const String LocationProductAttributeCustomAttribute36 = "LocationProductAttribute_CustomAttribute36";
        //public const String LocationProductAttributeCustomAttribute37 = "LocationProductAttribute_CustomAttribute37";
        //public const String LocationProductAttributeCustomAttribute38 = "LocationProductAttribute_CustomAttribute38";
        //public const String LocationProductAttributeCustomAttribute39 = "LocationProductAttribute_CustomAttribute39";
        //public const String LocationProductAttributeCustomAttribute40 = "LocationProductAttribute_CustomAttribute40";
        //public const String LocationProductAttributeCustomAttribute41 = "LocationProductAttribute_CustomAttribute41";
        //public const String LocationProductAttributeCustomAttribute42 = "LocationProductAttribute_CustomAttribute42";
        //public const String LocationProductAttributeCustomAttribute43 = "LocationProductAttribute_CustomAttribute43";
        //public const String LocationProductAttributeCustomAttribute44 = "LocationProductAttribute_CustomAttribute44";
        //public const String LocationProductAttributeCustomAttribute45 = "LocationProductAttribute_CustomAttribute45";
        //public const String LocationProductAttributeCustomAttribute46 = "LocationProductAttribute_CustomAttribute46";
        //public const String LocationProductAttributeCustomAttribute47 = "LocationProductAttribute_CustomAttribute47";
        //public const String LocationProductAttributeCustomAttribute48 = "LocationProductAttribute_CustomAttribute48";
        //public const String LocationProductAttributeCustomAttribute49 = "LocationProductAttribute_CustomAttribute49";
        //public const String LocationProductAttributeCustomAttribute50 = "LocationProductAttribute_CustomAttribute50";
        public const String LocationProductAttributeSetProperties = "LocationProductAttribute_SetProperties";

        #endregion

        #region LocationProductAttributeInfo

        public const String LocationProductAttributeInfoProductId = "Product_Id";
        public const String LocationProductAttributeInfoLocationId = "Location_Id";
        public const String LocationProductAttributeInfoEntityId = "Entity_Id";
        public const String LocationProductAttributeInfoGTIN = "LocationProductAttribute_GTIN";
        public const String LocationProductAttributeInfoLocationCode = "Location_Code";
        public const String LocationProductAttributeInfoDaysOfSupply = "LocationProductAttribute_DaysOfSupply";
        public const String LocationProductAttributeInfoDeliveryFrequencyDays = "LocationProductAttribute_DeliveryFrequencyDays";
        public const String LocationProductAttributeInfoShelfLife = "LocationProductAttribute_ShelfLife";
        public const String LocationProductAttributeInfoDateCreated = "LocationProductAttribute_DateCreated";
        public const String LocationProductAttributeInfoDateLastModified = "LocationProductAttribute_DateLastModified";

        #endregion

        #region LocationProductIllegal

        public const String LocationProductIllegalProductId = "Product_Id";
        public const String LocationProductIllegalLocationId = "Location_Id";
        public const String LocationProductIllegalEntityId = "Entity_Id";
        public const String LocationProductIllegalRowVersion = "LocationProductIllegal_RowVersion";
        public const String LocationProductIllegalDateCreated = "LocationProductIllegal_DateCreated";
        public const String LocationProductIllegalDateLastModified = "LocationProductIllegal_DateLastModified";

        #endregion

        #region LocationProductLegal

        public const String LocationProductLegalProductId = "Product_Id";
        public const String LocationProductLegalLocationId = "Location_Id";
        public const String LocationProductLegalEntityId = "Entity_Id";
        public const String LocationProductLegalRowVersion = "LocationProductLegal_RowVersion";
        public const String LocationProductLegalDateCreated = "LocationProductLegal_DateCreated";
        public const String LocationProductLegalDateLastModified = "LocationProductLegal_DateLastModified";

        #endregion

        #region LocationSpace

        public const String LocationSpaceId = "LocationSpace_Id";
        public const String LocationSpaceRowVersion = "LocationSpace_RowVersion";
        public const String LocationSpaceUniqueContentReference = "LocationSpace_UniqueContentReference";
        public const String LocationSpaceLocationId = "Location_Id";
        public const String LocationSpaceEntityId = "Entity_Id";
        public const String LocationSpaceDateCreated = "LocationSpace_DateCreated";
        public const String LocationSpaceDateLastModified = "LocationSpace_DateLastModified";
        public const String LocationSpaceParentUniqueContentReference = "LocationSpace_ParentUniqueContentReference";

        #endregion

        #region LocationSpaceInfo

        public const String LocationSpaceInfoLocationCode = "Location_Code";
        public const String LocationSpaceInfoLocationName = "Location_Name";

        #endregion

        #region LocationSpaceBay

        public const String LocationSpaceBayId = "LocationSpaceBay_Id";
        public const String LocationSpaceBayOrder = "LocationSpaceBay_Order";
        public const String LocationSpaceBayHeight = "LocationSpaceBay_Height";
        public const String LocationSpaceBayWidth = "LocationSpaceBay_Width";
        public const String LocationSpaceBayDepth = "LocationSpaceBay_Depth";
        public const String LocationSpaceBayBaseHeight = "LocationSpaceBay_BaseHeight";
        public const String LocationSpaceBayBaseWidth = "LocationSpaceBay_BaseWidth";
        public const String LocationSpaceBayBaseDepth = "LocationSpaceBay_BaseDepth";
        public const String LocationSpaceBayLocationSpaceProductGroupId = "LocationSpaceProductGroup_Id";
        public const String LocationSpaceBayBayType = "LocationSpaceBay_BayType";
        public const String LocationSpaceBayBayTypePostFix = "LocationSpaceBay_BayTypePostFix";
        public const String LocationSpaceBayBayTypeCalculation = "LocationSpaceBay_BayTypeCalculation";
        public const String LocationSpaceBayNotchPitch = "LocationSpaceBay_NotchPitch";
        public const String LocationSpaceBayFixtureName = "LocationSpaceBay_FixtureName";
        public const String LocationSpaceBayFixtureType = "LocationSpaceBay_FixtureType";
        public const String LocationSpaceBayIsPromotional = "LocationSpaceBay_IsPromotional";
        public const String LocationSpaceBayIsAisleStart = "LocationSpaceBay_IsAisleStart";
        public const String LocationSpaceBayIsAisleEnd = "LocationSpaceBay_IsAisleEnd";
        public const String LocationSpaceBayIsAisleRight = "LocationSpaceBay_IsAisleRight";
        public const String LocationSpaceBayIsAisleLeft = "LocationSpaceBay_IsAisleLeft";
        public const String LocationSpaceBayManufacturerName = "LocationSpaceBay_ManufacturerName";
        public const String LocationSpaceBayBarcode = "LocationSpaceBay_Barcode";
        public const String LocationSpaceBayHasPower = "LocationSpaceBay_HasPower";
        public const String LocationSpaceBayAssetNumber = "LocationSpaceBay_AssetNumber";
        public const String LocationSpaceBayTemperature = "LocationSpaceBay_Temperature";
        public const String LocationSpaceBayColour = "LocationSpaceBay_Colour";
        public const String LocationSpaceBayBayLocationRef = "LocationSpaceBay_BayLocationRef";
        public const String LocationSpaceBayBayFloorDrawingNo = "LocationSpaceBay_BayFloorDrawingNo";
        public const String LocationSpaceBayNotchStartY = "LocationSpaceBay_NotchStartY";
        public const String LocationSpaceBayFixtureShape = "LocationSpaceBay_FixtureShape";

        #endregion

        #region LocationSpaceProductGroup

        public const String LocationSpaceProductGroupId = "LocationSpaceProductGroup_Id";
        public const String LocationSpaceProductGroupProductGroupId = "ProductGroup_Id";
        public const String LocationSpaceProductGroupLocationSpaceId = "LocationSpace_Id";
        public const String LocationSpaceProductGroupBayCount = "LocationSpaceProductGroup_BayCount";
        public const String LocationSpaceProductGroupProductCount = "LocationSpaceProductGroup_ProductCount";
        public const String LocationSpaceProductGroupAverageBayWidth = "LocationSpaceProductGroup_AverageBayWidth";
        public const String LocationSpaceProductGroupAisleName = "LocationSpaceProductGroup_AisleName";
        public const String LocationSpaceProductGroupValleyName = "LocationSpaceProductGroup_ValleyName";
        public const String LocationSpaceProductGroupZoneName = "LocationSpaceProductGroup_ZoneName";
        public const String LocationSpaceProductGroupCustomAttribute01 = "LocationSpaceProductGroup_CustomAttribute01";
        public const String LocationSpaceProductGroupCustomAttribute02 = "LocationSpaceProductGroup_CustomAttribute02";
        public const String LocationSpaceProductGroupCustomAttribute03 = "LocationSpaceProductGroup_CustomAttribute03";
        public const String LocationSpaceProductGroupCustomAttribute04 = "LocationSpaceProductGroup_CustomAttribute04";
        public const String LocationSpaceProductGroupCustomAttribute05 = "LocationSpaceProductGroup_CustomAttribute05";

        #endregion

        #region LocationSpaceElement

        public const String LocationSpaceElementId = "LocationSpaceElement_Id";
        public const String LocationSpaceElementLocationSpaceBayId = "LocationSpaceBay_Id";
        public const String LocationSpaceElementOrder = "LocationSpaceElement_Order";
        public const String LocationSpaceElementX = "LocationSpaceElement_X";
        public const String LocationSpaceElementY = "LocationSpaceElement_Y";
        public const String LocationSpaceElementZ = "LocationSpaceElement_Z";
        public const String LocationSpaceElementMerchandisableHeight = "LocationSpaceElement_MerchandisableHeight";
        public const String LocationSpaceElementHeight = "LocationSpaceElement_Height";
        public const String LocationSpaceElementWidth = "LocationSpaceElement_Width";
        public const String LocationSpaceElementDepth = "LocationSpaceElement_Depth";
        public const String LocationSpaceElementFaceThickness = "LocationSpaceElement_FaceThickness";
        public const String LocationSpaceElementNotchNumber = "LocationSpaceElement_NotchNumber";

        #endregion

        #region Metric

        public const String MetricId = "Metric_Id";
        public const String MetricRowVersion = "Metric_RowVersion";
        public const String MetricEntityId = "Entity_Id";
        public const String MetricName = "Metric_Name";
        public const String MetricDescription = "Metric_Description";
        public const String MetricType = "Metric_Type";
        public const String MetricDirection = "Metric_Direction";
        public const String MetricSpecialType = "Metric_SpecialType";
        public const String MetricDataModelType = "Metric_DataModelType";
        public const String MetricDateCreated = "Metric_DateCreated";
        public const String MetricDateLastModified = "Metric_DateLastModified";
        public const String MetricDateDeleted = "Metric_DateDeleted";

        #endregion

        #region MetricProfile

        public const String MetricProfileId = "MetricProfile_Id";
        public const String MetricProfileRowVersion = "MetricProfile_RowVersion";
        public const String MetricProfileEntityId = "Entity_Id";
        public const String MetricProfileName = "MetricProfile_Name";
        public const String MetricProfileMetric1MetricId = "MetricProfile_Metric1MetricId";
        public const String MetricProfileMetric1Ratio = "MetricProfile_Metric1Ratio";
        public const String MetricProfileMetric2MetricId = "MetricProfile_Metric2MetricId";
        public const String MetricProfileMetric2Ratio = "MetricProfile_Metric2Ratio";
        public const String MetricProfileMetric3MetricId = "MetricProfile_Metric3MetricId";
        public const String MetricProfileMetric3Ratio = "MetricProfile_Metric3Ratio";
        public const String MetricProfileMetric4MetricId = "MetricProfile_Metric4MetricId";
        public const String MetricProfileMetric4Ratio = "MetricProfile_Metric4Ratio";
        public const String MetricProfileMetric5MetricId = "MetricProfile_Metric5MetricId";
        public const String MetricProfileMetric5Ratio = "MetricProfile_Metric5Ratio";
        public const String MetricProfileMetric6MetricId = "MetricProfile_Metric6MetricId";
        public const String MetricProfileMetric6Ratio = "MetricProfile_Metric6Ratio";
        public const String MetricProfileMetric7MetricId = "MetricProfile_Metric7MetricId";
        public const String MetricProfileMetric7Ratio = "MetricProfile_Metric7Ratio";
        public const String MetricProfileMetric8MetricId = "MetricProfile_Metric8MetricId";
        public const String MetricProfileMetric8Ratio = "MetricProfile_Metric8Ratio";
        public const String MetricProfileMetric9MetricId = "MetricProfile_Metric9MetricId";
        public const String MetricProfileMetric9Ratio = "MetricProfile_Metric9Ratio";
        public const String MetricProfileMetric10MetricId = "MetricProfile_Metric10MetricId";
        public const String MetricProfileMetric10Ratio = "MetricProfile_Metric10Ratio";
        public const String MetricProfileMetric11MetricId = "MetricProfile_Metric11MetricId";
        public const String MetricProfileMetric11Ratio = "MetricProfile_Metric11Ratio";
        public const String MetricProfileMetric12MetricId = "MetricProfile_Metric12MetricId";
        public const String MetricProfileMetric12Ratio = "MetricProfile_Metric12Ratio";
        public const String MetricProfileMetric13MetricId = "MetricProfile_Metric13MetricId";
        public const String MetricProfileMetric13Ratio = "MetricProfile_Metric13Ratio";
        public const String MetricProfileMetric14MetricId = "MetricProfile_Metric14MetricId";
        public const String MetricProfileMetric14Ratio = "MetricProfile_Metric14Ratio";
        public const String MetricProfileMetric15MetricId = "MetricProfile_Metric15MetricId";
        public const String MetricProfileMetric15Ratio = "MetricProfile_Metric15Ratio";
        public const String MetricProfileMetric16MetricId = "MetricProfile_Metric16MetricId";
        public const String MetricProfileMetric16Ratio = "MetricProfile_Metric16Ratio";
        public const String MetricProfileMetric17MetricId = "MetricProfile_Metric17MetricId";
        public const String MetricProfileMetric17Ratio = "MetricProfile_Metric17Ratio";
        public const String MetricProfileMetric18MetricId = "MetricProfile_Metric18MetricId";
        public const String MetricProfileMetric18Ratio = "MetricProfile_Metric18Ratio";
        public const String MetricProfileMetric19MetricId = "MetricProfile_Metric19MetricId";
        public const String MetricProfileMetric19Ratio = "MetricProfile_Metric19Ratio";
        public const String MetricProfileMetric20MetricId = "MetricProfile_Metric20MetricId";
        public const String MetricProfileMetric20Ratio = "MetricProfile_Metric20Ratio";
        public const String MetricProfileDateCreated = "MetricProfile_DateCreated";
        public const String MetricProfileDateLastModified = "MetricProfile_DateLastModified";
        public const String MetricProfileDateDeleted = "MetricProfile_DateDeleted";
        public const String MetricProfileSetProperties = "MetricProfile_SetProperties";

        #endregion

        #region PerformanceSelection
        public const String PerformanceSelectionId = "PerformanceSelection_Id";
        public const String PerformanceSelectionRowVersion = "PerformanceSelection_RowVersion";
        public const String PerformanceSelectionEntityId = "Entity_Id";
        public const String PerformanceSelectionName = "PerformanceSelection_Name";
        public const String PerformanceSelectionGFSPerformanceSourceId = "PerformanceSelection_GFSPerformanceSourceId";
        public const String PerformanceSelectionTimeType = "PerformanceSelection_TimeType";
        public const String PerformanceSelectionSelectionType = "PerformanceSelection_SelectionType";
        public const String PerformanceSelectionDynamicValue = "PerformanceSelection_DynamicValue";
        public const String PerformanceSelectionDateCreated = "PerformanceSelection_DateCreated";
        public const String PerformanceSelectionDateLastModified = "PerformanceSelection_DateLastModified";
        public const String PerformanceSelectionDateDeleted = "PerformanceSelection_DateDeleted";
        #endregion

        #region PerformanceSelectionTimelineGroup
        public const String PerformanceSelectionTimelineGroupId = "PerformanceSelectionTimelineGroup_Id";
        public const String PerformanceSelectionTimelineGroupPerformanceSelectionId = "PerformanceSelection_Id";
        public const String PerformanceSelectionTimelineGroupCode = "PerformanceSelectionTimelineGroup_Code";
        #endregion

        #region PerformanceSelectionMetric

        public const String PerformanceSelectionMetricId = "PerformanceSelectionMetric_Id";
        public const String PerformanceSelectionMetricPerformanceSelectionId = "PerformanceSelection_Id";
        public const String PerformanceSelectionMetricGFSPerformanceSourceMetricId = "PerformanceSelectionMetric_GFSPerformanceSourceMetricId";
        public const String PerformanceSelectionMetricAggregationType = "PerformanceSelectionMetric_AggregationType";
        #endregion

        #region PlanAssignmentStoreSpaceInfo

        public const String PlanAssignmentStoreSpaceInfoId = "PlanAssignmentStoreSpaceInfo_Id";
        public const String PlanAssignmentStoreSpaceInfoLocationId = "Location_Id";
        public const String PlanAssignmentStoreSpaceInfoLocationCode = "PlanAssignmentStoreSpaceInfo_LocationCode";
        public const String PlanAssignmentStoreSpaceInfoLocationName = "PlanAssignmentStoreSpaceInfo_LocationName";
        public const String PlanAssignmentStoreSpaceInfoLocationAddress = "PlanAssignmentStoreSpaceInfo_LocationAddress";
        public const String PlanAssignmentStoreSpaceInfoProductGroupId = "PlanAssignmentStoreSpaceInfo_ProductGroupId";
        public const String PlanAssignmentStoreSpaceInfoProductGroupName = "PlanAssignmentStoreSpaceInfo_ProductGroupName";
        public const String PlanAssignmentStoreSpaceInfoProductGroupCode = "PlanAssignmentStoreSpaceInfo_ProductGroupCode";
        public const String PlanAssignmentStoreSpaceInfoBayDimensions = "PlanAssignmentStoreSpaceInfo_BayDimensions";
        public const String PlanAssignmentStoreSpaceInfo_BayTotalWidth = "PlanAssignmentStoreSpaceInfo_BayTotalWidth";
        public const String PlanAssignmentStoreSpaceInfoBayHeight = "PlanAssignmentStoreSpaceInfo_BayHeight";
        public const String PlanAssignmentStoreSpaceInfoBayCount = "PlanAssignmentStoreSpaceInfo_BayCount";
        public const String PlanAssignmentStoreSpaceInfoClusterSchemeName = "PlanAssignmentStoreSpaceInfo_ClusterSchemeName";
        public const String PlanAssignmentStoreSpaceInfoClusterName = "PlanAssignmentStoreSpaceInfo_ClusterName";
        public const String PlanAssignmentStoreSpaceInfoProductCount = "PlanAssignmentStoreSpaceInfo_ProductCount";
        public const String PlanAssignmentStoreSpaceInfoAverageBayWidth = "PlanAssignmentStoreSpaceInfo_AverageBayWidth";
        public const String PlanAssignmentStoreSpaceInfoAisleName = "PlanAssignmentStoreSpaceInfo_AisleName";
        public const String PlanAssignmentStoreSpaceInfoValleyName = "PlanAssignmentStoreSpaceInfo_ValleyName";
        public const String PlanAssignmentStoreSpaceInfoZoneName = "PlanAssignmentStoreSpaceInfo_ZoneName";
        public const String PlanAssignmentStoreSpaceInfoCustomAttribute01 = "PlanAssignmentStoreSpaceInfo_CustomAttribute01";
        public const String PlanAssignmentStoreSpaceInfoCustomAttribute02 = "PlanAssignmentStoreSpaceInfo_CustomAttribute02";
        public const String PlanAssignmentStoreSpaceInfoCustomAttribute03 = "PlanAssignmentStoreSpaceInfo_CustomAttribute03";
        public const String PlanAssignmentStoreSpaceInfoCustomAttribute04 = "PlanAssignmentStoreSpaceInfo_CustomAttribute04";
        public const String PlanAssignmentStoreSpaceInfoCustomAttribute05 = "PlanAssignmentStoreSpaceInfo_CustomAttribute05";
        #endregion

        #region Planogram


        public const String PlanogramId = "Planogram_Id";
        public const String PlanogramRowVersion = "Planogram_RowVersion";
        public const String PlanogramUniqueContentReference = "Planogram_UniqueContentReference";
        public const String PlanogramName = "Planogram_Name";
        public const String PlanogramHeight = "Planogram_Height";
        public const String PlanogramWidth = "Planogram_Width";
        public const String PlanogramDepth = "Planogram_Depth";
        public const String PlanogramUserName = "Planogram_UserName";
        public const String PlanogramLengthUnitsOfMeasure = "Planogram_LengthUnitsOfMeasure";
        public const String PlanogramAreaUnitsOfMeasure = "Planogram_AreaUnitsOfMeasure";
        public const String PlanogramVolumeUnitsOfMeasure = "Planogram_VolumeUnitsOfMeasure";
        public const String PlanogramWeightUnitsOfMeasure = "Planogram_WeightUnitsOfMeasure";
        public const String PlanogramAngleUnitsOfMeasure = "Planogram_AngleUnitsOfMeasure";
        public const String PlanogramCurrencyUnitsOfMeasure = "Planogram_CurrencyUnitsOfMeasure";
        public const String PlanogramProductPlacementX = "Planogram_ProductPlacementX";
        public const String PlanogramProductPlacementY = "Planogram_ProductPlacementY";
        public const String PlanogramProductPlacementZ = "Planogram_ProductPlacementZ";
        public const String PlanogramDateCreated = "Planogram_DateCreated";
        public const String PlanogramDateLastModified = "Planogram_DateLastModified";
        public const String PlanogramDateDeleted = "Planogram_DateDeleted";
        public const String PlanogramDateMetadataCalculated = "Planogram_DateMetadataCalculated";
        public const String PlanogramDateValidationDataCalculated = "Planogram_DateValidationDataCalculated";
        public const String PlanogramCategoryCode = "Planogram_CategoryCode";
        public const String PlanogramCategoryName = "Planogram_CategoryName";
        public const String PlanogramSourcePath = "Planogram_SourcePath";
        public const String PlanogramMetaBayCount = "Planogram_MetaBayCount";
        public const String PlanogramMetaUniqueProductCount = "Planogram_MetaUniqueProductCount";
        public const String PlanogramMetaComponentCount = "Planogram_MetaComponentCount";
        public const String PlanogramMetaTotalMerchandisableLinearSpace = "Planogram_MetaTotalMerchandisableLinearSpace";
        public const String PlanogramMetaTotalMerchandisableAreaSpace = "Planogram_MetaTotalMerchandisableAreaSpace";
        public const String PlanogramMetaTotalMerchandisableVolumetricSpace = "Planogram_MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramMetaTotalLinearWhiteSpace = "Planogram_MetaTotalLinearWhiteSpace";
        public const String PlanogramMetaTotalAreaWhiteSpace = "Planogram_MetaTotalAreaWhiteSpace";
        public const String PlanogramMetaTotalVolumetricWhiteSpace = "Planogram_MetaTotalVolumetricWhiteSpace";
        public const String PlanogramMetaProductsPlaced = "Planogram_MetaProductsPlaced";
        public const String PlanogramMetaProductsUnplaced = "Planogram_MetaProductsUnplaced";
        public const String PlanogramMetaNewProducts = "Planogram_MetaNewProducts";
        public const String PlanogramMetaChangesFromPreviousCount = "Planogram_MetaChangesFromPreviousCount";
        public const String PlanogramMetaChangeFromPreviousStarRating = "Planogram_MetaChangeFromPreviousStarRating";
        public const String PlanogramMetaBlocksDropped = "Planogram_MetaBlocksDropped";
        public const String PlanogramMetaNotAchievedInventory = "Planogram_MetaNotAchievedInventory";
        public const String PlanogramMetaTotalFacings = "Planogram_MetaTotalFacings";
        public const String PlanogramMetaAverageFacings = "Planogram_MetaAverageFacings";
        public const String PlanogramMetaTotalUnits = "Planogram_MetaTotalUnits";
        public const String PlanogramMetaAverageUnits = "Planogram_MetaAverageUnits";
        public const String PlanogramMetaMinDos = "Planogram_MetaMinDos";
        public const String PlanogramMetaMaxDos = "Planogram_MetaMaxDos";
        public const String PlanogramMetaAverageDos = "Planogram_MetaAverageDos";
        public const String PlanogramMetaMinCases = "Planogram_MetaMinCases";
        public const String PlanogramMetaAverageCases = "Planogram_MetaAverageCases";
        public const String PlanogramMetaMaxCases = "Planogram_MetaMaxCases";
        public const String PlanogramMetaSpaceToUnitsIndex = "Planogram_MetaSpaceToUnitsIndex";
        public const String PlanogramMetaTotalComponentsOverMerchandisedWidth = "Planogram_MetaTotalComponentsOverMerchandisedWidth";
        public const String PlanogramMetaHasComponentsOutsideOfFixtureArea = "Planogram_MetaHasComponentsOutsideOfFixtureArea";
        public const String PlanogramMetaTotalComponentsOverMerchandisedHeight = "Planogram_MetaTotalComponentsOverMerchandisedHeight";
        public const String PlanogramMetaTotalComponentsOverMerchandisedDepth = "Planogram_MetaTotalComponentsOverMerchandisedDepth";
        public const String PlanogramMetaTotalComponentCollisions = "Planogram_MetaTotalComponentCollisions";
        public const String PlanogramMetaTotalPositionCollisions = "Planogram_MetaTotalPositionCollisions";
        public const String PlanogramMetaTotalFrontFacings = "Planogram_MetaTotalFrontFacings";
        public const String PlanogramMetaAverageFrontFacings = "Planogram_MetaAverageFrontFacings";
        public const String PlanogramMetaNoOfErrors = "Planogram_MetaNoOfErrors";
        public const String PlanogramMetaNoOfWarnings = "Planogram_MetaNoOfWarnings";
        public const String PlanogramMetaNoOfDebugEvents = "Planogram_MetaNoOfDebugEvents";
        public const String PlanogramMetaNoOfInformationEvents = "Planogram_MetaNoOfInformationEvents";
        public const String PlanogramMetaTotalErrorScore = "Planogram_MetaTotalErrorScore";
        public const String PlanogramMetaHighestErrorScore = "Planogram_MetaHighestErrorScore";
        public const String PlanogramUserLastModified = "Planogram_UserLastModified";
        public const String PlanogramLocationCode = "Planogram_LocationCode";
        public const String PlanogramLocationName = "Planogram_LocationName";
        public const String PlanogramClusterSchemeName = "Planogram_ClusterSchemeName";
        public const String PlanogramClusterName = "Planogram_ClusterName";
        public const String PlanogramEntityId = "Entity_Id";
        public const String PlanogramStatus = "Planogram_Status";
        public const String PlanogramDateWip = "Planogram_DateWIP";
        public const String PlanogramDateApproved = "Planogram_DateApproved";
        public const String PlanogramDateArchived = "Planogram_DateArchived";
        public const String PlanogramMetaCountOfProductNotAchievedCases = "Planogram_MetaCountOfProductNotAchievedCases";
        public const String PlanogramMetaCountOfProductNotAchievedDOS = "Planogram_MetaCountOfProductNotAchievedDOS";
        public const String PlanogramMetaCountOfProductOverShelfLifePercent = "Planogram_MetaCountOfProductOverShelfLifePercent";
        public const String PlanogramMetaCountOfProductNotAchievedDeliveries = "Planogram_MetaCountOfProductNotAchievedDeliveries";
        public const String PlanogramMetaPercentOfProductNotAchievedCases = "Planogram_MetaPercentOfProductNotAchievedCases";
        public const String PlanogramMetaPercentOfProductNotAchievedDOS = "Planogram_MetaPercentOfProductNotAchievedDOS";
        public const String PlanogramMetaPercentOfProductOverShelfLifePercent = "Planogram_MetaPercentOfProductOverShelfLifePercent";
        public const String PlanogramMetaPercentOfProductNotAchievedDeliveries = "Planogram_MetaPercentOfProductNotAchievedDeliveries";
        public const String PlanogramMetaCountOfPositionsOutsideOfBlockSpace = "Planogram_MetaCountOfPositionsOutsideOfBlockSpace";
        public const String PlanogramMetaPercentOfPositionsOutsideOfBlockSpace = "Planogram_MetaPercentOfPositionsOutsideOfBlockSpace";
        public const String PlanogramMetaPercentOfPlacedProductsRecommendedInAssortment = "Planogram_MetaPercentOfPlacedProductsRecommendedInAssortment";
        public const String PlanogramMetaCountOfPlacedProductsRecommendedInAssortment = "Planogram_MetaCountOfPlacedProductsRecommendedInAssortment";
        public const String PlanogramMetaCountOfPlacedProductsNotRecommendedInAssortment = "Planogram_MetaCountOfPlacedProductsNotRecommendedInAssortment";
        public const String PlanogramMetaCountOfRecommendedProductsInAssortment = "Planogram_MetaCountOfRecommendedProductsInAssortment";
        public const String PlanogramPlanogramType = "Planogram_Type";
        public const String PlanogramMetaPercentOfProductNotAchievedInventory = "Planogram_MetaPercentOfProductNotAchievedInventory";
        public const String PlanogramSetProperties = "Planogram_SetProperties";
        public const String PlanogramHighlightSequenceStrategy = "Planogram_HighlightSequenceStrategy";
        public const String PlanogramMetaNumberOfAssortmentRulesBroken = "Planogram_MetaNumberOfAssortmentRulesBroken";
        public const String PlanogramMetaNumberOfProductRulesBroken = "Planogram_MetaNumberOfProductRulesBroken";
        public const String PlanogramMetaNumberOfFamilyRulesBroken = "Planogram_MetaNumberOfFamilyRulesBroken";
        public const String PlanogramMetaNumberOfInheritanceRulesBroken = "Planogram_MetaNumberOfInheritanceRulesBroken";
        public const String PlanogramMetaNumberOfLocalProductRulesBroken = "Planogram_MetaNumberOfLocalProductRulesBroken";
        public const String PlanogramMetaNumberOfDistributionRulesBroken = "Planogram_MetaNumberOfDistributionRulesBroken";
        public const String PlanogramMetaNumberOfCoreRulesBroken = "Planogram_MetaNumberOfCoreRulesBroken";
        public const String PlanogramMetaPercentageOfAssortmentRulesBroken = "Planogram_MetaPercentageOfAssortmentRulesBroken";
        public const String PlanogramMetaPercentageOfProductRulesBroken = "Planogram_MetaPercentageOfProductRulesBroken";
        public const String PlanogramMetaPercentageOfFamilyRulesBroken = "Planogram_MetaPercentageOfFamilyRulesBroken";
        public const String PlanogramMetaPercentageOfInheritanceRulesBroken = "Planogram_MetaPercentageOfInheritanceRulesBroken";
        public const String PlanogramMetaPercentageOfLocalProductRulesBroken = "Planogram_MetaPercentageOfLocalProductRulesBroken";
        public const String PlanogramMetaPercentageOfDistributionRulesBroken = "Planogram_MetaPercentageOfDistributionRulesBroken";
        public const String PlanogramMetaPercentageOfCoreRulesBroken = "Planogram_MetaPercentageOfCoreRulesBroken";
        public const String PlanogramMetaNumberOfDelistProductRulesBroken = "Planogram_MetaNumberOfDelistProductRulesBroken";
        public const String PlanogramMetaNumberOfForceProductRulesBroken = "Planogram_MetaNumberOfForceProductRulesBroken";
        public const String PlanogramMetaNumberOfPreserveProductRulesBroken = "Planogram_MetaNumberOfPreserveProductRulesBroken";
        public const String PlanogramMetaNumberOfMinimumHurdleProductRulesBroken = "Planogram_MetaNumberOfMinimumHurdleProductRulesBroken";
        public const String PlanogramMetaNumberOfMaximumProductFamilyRulesBroken = "Planogram_MetaNumberOfMaximumProductFamilyRulesBroken";
        public const String PlanogramMetaNumberOfMinimumProductFamilyRulesBroken = "Planogram_MetaNumberOfMinimumProductFamilyRulesBroken";
        public const String PlanogramMetaNumberOfDependencyFamilyRulesBroken = "Planogram_MetaNumberOfDependencyFamilyRulesBroken";
        public const String PlanogramMetaNumberOfDelistFamilyRulesBroken = "Planogram_MetaNumberOfDelistFamilyRulesBroken";
        public const String PlanogramMetaCountOfProductsBuddied = "Planogram_MetaCountOfProductsBuddied";
        #endregion

        #region PlanogramAnnotation

        public const String PlanogramAnnotationId = "PlanogramAnnotation_Id";
        public const String PlanogramAnnotationPlanogramId = "Planogram_Id";
        public const String PlanogramAnnotationPlanogramFixtureItemId = "PlanogramFixtureItem_Id";
        public const String PlanogramAnnotationPlanogramFixtureAssemblyId = "PlanogramFixtureAssembly_Id";
        public const String PlanogramAnnotationPlanogramFixtureComponentId = "PlanogramFixtureComponent_Id";
        public const String PlanogramAnnotationPlanogramAssemblyComponentId = "PlanogramAssemblyComponent_Id";
        public const String PlanogramAnnotationPlanogramSubComponentId = "PlanogramSubComponent_Id";
        public const String PlanogramAnnotationPlanogramPositionId = "PlanogramPosition_Id";
        public const String PlanogramAnnotationAnnotationType = "PlanogramAnnotation_AnnotationType";
        public const String PlanogramAnnotationText = "PlanogramAnnotation_Text";
        public const String PlanogramAnnotationX = "PlanogramAnnotation_X";
        public const String PlanogramAnnotationY = "PlanogramAnnotation_Y";
        public const String PlanogramAnnotationZ = "PlanogramAnnotation_Z";
        public const String PlanogramAnnotationHeight = "PlanogramAnnotation_Height";
        public const String PlanogramAnnotationWidth = "PlanogramAnnotation_Width";
        public const String PlanogramAnnotationDepth = "PlanogramAnnotation_Depth";
        public const String PlanogramAnnotationBorderColour = "PlanogramAnnotation_BorderColour";
        public const String PlanogramAnnotationBorderThickness = "PlanogramAnnotation_BorderThickness";
        public const String PlanogramAnnotationBackgroundColour = "PlanogramAnnotation_BackgroundColour";
        public const String PlanogramAnnotationFontColour = "PlanogramAnnotation_FontColour";
        public const String PlanogramAnnotationFontSize = "PlanogramAnnotation_FontSize";
        public const String PlanogramAnnotationFontName = "PlanogramAnnotation_FontName";
        public const String PlanogramAnnotationCanReduceFontToFit = "PlanogramAnnotation_CanReduceFontToFit";

        #endregion

        #region PlanogramAssemblyComponent

        public const String PlanogramAssemblyComponentId = "PlanogramAssemblyComponent_Id";
        public const String PlanogramAssemblyComponentPlanogramAssemblyId = "PlanogramAssembly_Id";
        public const String PlanogramAssemblyComponentPlanogramComponentId = "PlanogramComponent_Id";
        public const String PlanogramAssemblyComponentComponentSequenceNumber = "PlanogramAssemblyComponent_ComponentSequenceNumber";
        public const String PlanogramAssemblyComponentX = "PlanogramAssemblyComponent_X";
        public const String PlanogramAssemblyComponentY = "PlanogramAssemblyComponent_Y";
        public const String PlanogramAssemblyComponentZ = "PlanogramAssemblyComponent_Z";
        public const String PlanogramAssemblyComponentSlope = "PlanogramAssemblyComponent_Slope";
        public const String PlanogramAssemblyComponentAngle = "PlanogramAssemblyComponent_Angle";
        public const String PlanogramAssemblyComponentRoll = "PlanogramAssemblyComponent_Roll";
        public const String PlanogramAssemblyComponentMetaTotalMerchandisableLinearSpace = "PlanogramAssemblyComponent_MetaTotalMerchandisableLinearSpace";
        public const String PlanogramAssemblyComponentMetaTotalMerchandisableAreaSpace = "PlanogramAssemblyComponent_MetaTotalMerchandisableAreaSpace";
        public const String PlanogramAssemblyComponentMetaTotalMerchandisableVolumetricSpace = "PlanogramAssemblyComponent_MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramAssemblyComponentMetaTotalLinearWhiteSpace = "PlanogramAssemblyComponent_MetaTotalLinearWhiteSpace";
        public const String PlanogramAssemblyComponentMetaTotalAreaWhiteSpace = "PlanogramAssemblyComponent_MetaTotalAreaWhiteSpace";
        public const String PlanogramAssemblyComponentMetaTotalVolumetricWhiteSpace = "PlanogramAssemblyComponent_MetaTotalVolumetricWhiteSpace";
        public const String PlanogramAssemblyComponentMetaProductsPlaced = "PlanogramAssemblyComponent_MetaProductsPlaced";
        public const String PlanogramAssemblyComponentMetaNewProducts = "PlanogramAssemblyComponent_MetaNewProducts";
        public const String PlanogramAssemblyComponentMetaChangesFromPreviousCount = "PlanogramAssemblyComponent_MetaChangesFromPreviousCount";
        public const String PlanogramAssemblyComponentMetaChangeFromPreviousStarRating = "PlanogramAssemblyComponent_MetaChangeFromPreviousStarRating";
        public const String PlanogramAssemblyComponentMetaBlocksDropped = "PlanogramAssemblyComponent_MetaBlocksDropped";
        public const String PlanogramAssemblyComponentMetaNotAchievedInventory = "PlanogramAssemblyComponent_MetaNotAchievedInventory";
        public const String PlanogramAssemblyComponentMetaTotalFacings = "PlanogramAssemblyComponent_MetaTotalFacings";
        public const String PlanogramAssemblyComponentMetaAverageFacings = "PlanogramAssemblyComponent_MetaAverageFacings";
        public const String PlanogramAssemblyComponentMetaTotalUnits = "PlanogramAssemblyComponent_MetaTotalUnits";
        public const String PlanogramAssemblyComponentMetaAverageUnits = "PlanogramAssemblyComponent_MetaAverageUnits";
        public const String PlanogramAssemblyComponentMetaMinDos = "PlanogramAssemblyComponent_MetaMinDos";
        public const String PlanogramAssemblyComponentMetaMaxDos = "PlanogramAssemblyComponent_MetaMaxDos";
        public const String PlanogramAssemblyComponentMetaAverageDos = "PlanogramAssemblyComponent_MetaAverageDos";
        public const String PlanogramAssemblyComponentMetaMinCases = "PlanogramAssemblyComponent_MetaMinCases";
        public const String PlanogramAssemblyComponentMetaAverageCases = "PlanogramAssemblyComponent_MetaAverageCases";
        public const String PlanogramAssemblyComponentMetaMaxCases = "PlanogramAssemblyComponent_MetaMaxCases";
        public const String PlanogramAssemblyComponentMetaSpaceToUnitsIndex = "PlanogramAssemblyComponent_MetaSpaceToUnitsIndex";
        public const String PlanogramAssemblyComponentMetaTotalFrontFacings = "PlanogramAssemblyComponent_MetaTotalFrontFacings";
        public const String PlanogramAssemblyComponentMetaAverageFrontFacings = "PlanogramAssemblyComponent_MetaAverageFrontFacings";
        public const String PlanogramAssemblyComponentMetaIsComponentSlopeWithNoRiser = "PlanogramAssemblyComponent_MetaIsComponentSlopeWithNoRiser";
        public const String PlanogramAssemblyComponentMetaIsOverMerchandisedDepth = "PlanogramAssemblyComponent_MetaIsOverMerchandisedDepth";
        public const String PlanogramAssemblyComponentMetaIsOverMerchandisedHeight = "PlanogramAssemblyComponent_MetaIsOverMerchandisedHeight";
        public const String PlanogramAssemblyComponentMetaIsOverMerchandisedWidth = "PlanogramAssemblyComponent_MetaIsOverMerchandisedWidth";
        public const String PlanogramAssemblyComponentMetaTotalPositionCollisions = "PlanogramAssemblyComponent_MetaTotalPositionCollisions";
        public const String PlanogramAssemblyComponentMetaTotalComponentCollisions = "PlanogramAssemblyComponent_MetaTotalComponentCollisions";
        public const String PlanogramAssemblyComponentMetaPercentageLinearSpaceFilled = "PlanogramAssemblyComponent_MetaPercentageLinearSpaceFilled";
        public const String PlanogramAssemblyComponentNotchNumber = "PlanogramAssemblyComponent_NotchNumber";
        public const String PlanogramAssemblyComponentMetaWorldX = "PlanogramAssemblyComponent_MetaWorldX";
        public const String PlanogramAssemblyComponentMetaWorldY = "PlanogramAssemblyComponent_MetaWorldY";
        public const String PlanogramAssemblyComponentMetaWorldZ = "PlanogramAssemblyComponent_MetaWorldZ";
        public const String PlanogramAssemblyComponentMetaWorldAngle = "PlanogramAssemblyComponent_MetaWorldAngle";
        public const String PlanogramAssemblyComponentMetaWorldSlope = "PlanogramAssemblyComponent_MetaWorldSlope";
        public const String PlanogramAssemblyComponentMetaWorldRoll = "PlanogramAssemblyComponent_MetaWorldRoll";
        public const String PlanogramAssemblyComponentMetaIsOutsideOfFixtureArea = "PlanogramAssemblyComponent_MetaIsOutsideOfFixtureArea";

        #endregion

        #region PlanogramAssembly

        public const String PlanogramAssemblyId = "PlanogramAssembly_Id";
        public const String PlanogramAssemblyPlanogramId = "Planogram_Id";
        public const String PlanogramAssemblyName = "PlanogramAssembly_Name";
        public const String PlanogramAssemblyHeight = "PlanogramAssembly_Height";
        public const String PlanogramAssemblyWidth = "PlanogramAssembly_Width";
        public const String PlanogramAssemblyDepth = "PlanogramAssembly_Depth";
        public const String PlanogramAssemblyNumberOfComponents = "PlanogramAssembly_NumberOfComponents";
        public const String PlanogramAssemblyTotalComponentCost = "PlanogramAssembly_TotalComponentCost";

        #endregion

        #region PlanogramBlocking

        public const String PlanogramBlockingId = "PlanogramBlocking_Id";
        public const String PlanogramBlockingPlanogramId = "Planogram_Id";
        public const String PlanogramBlockingType = "PlanogramBlocking_Type";

        #endregion

        #region PlanogramBlockingDivider

        public const String PlanogramBlockingDividerId = "PlanogramBlockingDivider_Id";
        public const String PlanogramBlockingDividerPlanogramBlockingId = "PlanogramBlocking_Id";
        public const String PlanogramBlockingDividerType = "PlanogramBlockingDivider_Type";
        public const String PlanogramBlockingDividerLevel = "PlanogramBlockingDivider_Level";
        public const String PlanogramBlockingDividerX = "PlanogramBlockingDivider_X";
        public const String PlanogramBlockingDividerY = "PlanogramBlockingDivider_Y";
        public const String PlanogramBlockingDividerLength = "PlanogramBlockingDivider_Length";
        public const String PlanogramBlockingDividerIsSnapped = "PlanogramBlockingDivider_IsSnapped";
        public const String PlanogramBlockingDividerIsLimited = "PlanogramBlockingDivider_IsLimited";
        public const String PlanogramBlockingDividerLimitedPercentage = "PlanogramBlockingDivider_LimitedPercentage";

        #endregion

        #region PlanogramBlockingGroup

        public const String PlanogramBlockingGroupId = "PlanogramBlockingGroup_Id";
        public const String PlanogramBlockingGroupPlanogramBlockingId = "PlanogramBlocking_Id";
        public const String PlanogramBlockingGroupName = "PlanogramBlockingGroup_Name";
        public const String PlanogramBlockingGroupColour = "PlanogramBlockingGroup_Colour";
        public const String PlanogramBlockingGroupFillPatternType = "PlanogramBlockingGroup_FillPatternType";
        public const String PlanogramBlockingGroupCanCompromiseSequence = "PlanogramBlockingGroup_CanCompromiseSequence";
        public const String PlanogramBlockingGroupIsRestrictedByComponentType = "PlanogramBlockingGroup_IsRestrictedByComponentType";
        public const String PlanogramBlockingGroupCanMerge = "PlanogramBlockingGroup_CanMerge";
        public const String PlanogramBlockingGroupIsLimited = "PlanogramBlockingGroup_IsLimited";
        public const String PlanogramBlockingGroupLimitedPercentage = "PlanogramBlockingGroup_LimitedPercentage";
        public const String PlanogramBlockingGroupBlockPlacementPrimaryType = "PlanogramBlockingGroup_BlockPlacementPrimaryType";
        public const String PlanogramBlockingGroupBlockPlacementSecondaryType = "PlanogramBlockingGroup_BlockPlacementSecondaryType";
        public const String PlanogramBlockingGroupBlockPlacementTertiaryType = "PlanogramBlockingGroup_BlockPlacementTertiaryType";
        public const String PlanogramBlockingGroupTotalSpacePercentage = "PlanogramBlockingGroup_TotalSpacePercentage";
        public const String PlanogramBlockingGroupMetaCountOfProducts = "PlanogramBlockingGroup_MetaCountOfProducts";
        public const String PlanogramBlockingGroupMetaCountOfProductRecommendedOnPlanogram = "PlanogramBlockingGroup_MetaCountOfProductRecommendedOnPlanogram";
        public const String PlanogramBlockingGroupMetaCountOfProductPlacedOnPlanogram = "PlanogramBlockingGroup_MetaCountOfProductPlacedOnPlanogram";
        public const String PlanogramBlockingGroupMetaOriginalPercentAllocated = "PlanogramBlockingGroup_MetaOriginalPercentAllocated";
        public const String PlanogramBlockingGroupMetaPerformancePercentAllocated = "PlanogramBlockingGroup_MetaPerformancePercentAllocated";
        public const String PlanogramBlockingGroupMetaFinalPercentAllocated = "PlanogramBlockingGroup_MetaFinalPercentAllocated";
        public const String PlanogramBlockingGroupMetaLinearProductPlacementPercent = "PlanogramBlockingGroup_MetaLinearProductPlacementPercent";
        public const String PlanogramBlockingGroupMetaAreaProductPlacementPercent = "PlanogramBlockingGroup_MetaAreaProductPlacementPercent";
        public const String PlanogramBlockingGroupMetaVolumetricProductPlacementPercent = "PlanogramBlockingGroup_MetaVolumetricProductPlacementPercent";

        public const String PlanogramBlockingGroupMetaP1Performance = "PlanogramBlockingGroup_MetaP1";
        public const String PlanogramBlockingGroupMetaP2Performance = "PlanogramBlockingGroup_MetaP2";
        public const String PlanogramBlockingGroupMetaP3Performance = "PlanogramBlockingGroup_MetaP3";
        public const String PlanogramBlockingGroupMetaP4Performance = "PlanogramBlockingGroup_MetaP4";
        public const String PlanogramBlockingGroupMetaP5Performance = "PlanogramBlockingGroup_MetaP5";
        public const String PlanogramBlockingGroupMetaP6Performance = "PlanogramBlockingGroup_MetaP6";
        public const String PlanogramBlockingGroupMetaP7Performance = "PlanogramBlockingGroup_MetaP7";
        public const String PlanogramBlockingGroupMetaP8Performance = "PlanogramBlockingGroup_MetaP8";
        public const String PlanogramBlockingGroupMetaP9Performance = "PlanogramBlockingGroup_MetaP9";
        public const String PlanogramBlockingGroupMetaP10Performance = "PlanogramBlockingGroup_MetaP10";
        public const String PlanogramBlockingGroupMetaP11Performance = "PlanogramBlockingGroup_MetaP11";
        public const String PlanogramBlockingGroupMetaP12Performance = "PlanogramBlockingGroup_MetaP12";
        public const String PlanogramBlockingGroupMetaP13Performance = "PlanogramBlockingGroup_MetaP13";
        public const String PlanogramBlockingGroupMetaP14Performance = "PlanogramBlockingGroup_MetaP14";
        public const String PlanogramBlockingGroupMetaP15Performance = "PlanogramBlockingGroup_MetaP15";
        public const String PlanogramBlockingGroupMetaP16Performance = "PlanogramBlockingGroup_MetaP16";
        public const String PlanogramBlockingGroupMetaP17Performance = "PlanogramBlockingGroup_MetaP17";
        public const String PlanogramBlockingGroupMetaP18Performance = "PlanogramBlockingGroup_MetaP18";
        public const String PlanogramBlockingGroupMetaP19Performance = "PlanogramBlockingGroup_MetaP19";
        public const String PlanogramBlockingGroupMetaP20Performance = "PlanogramBlockingGroup_MetaP20";

        public const String PlanogramBlockingGroupMetaP1Percentage = "PlanogramBlockingGroup_MetaP1Percentage";
        public const String PlanogramBlockingGroupMetaP2Percentage = "PlanogramBlockingGroup_MetaP2Percentage";
        public const String PlanogramBlockingGroupMetaP3Percentage = "PlanogramBlockingGroup_MetaP3Percentage";
        public const String PlanogramBlockingGroupMetaP4Percentage = "PlanogramBlockingGroup_MetaP4Percentage";
        public const String PlanogramBlockingGroupMetaP5Percentage = "PlanogramBlockingGroup_MetaP5Percentage";
        public const String PlanogramBlockingGroupMetaP6Percentage = "PlanogramBlockingGroup_MetaP6Percentage";
        public const String PlanogramBlockingGroupMetaP7Percentage = "PlanogramBlockingGroup_MetaP7Percentage";
        public const String PlanogramBlockingGroupMetaP8Percentage = "PlanogramBlockingGroup_MetaP8Percentage";
        public const String PlanogramBlockingGroupMetaP9Percentage = "PlanogramBlockingGroup_MetaP9Percentage";
        public const String PlanogramBlockingGroupMetaP10Percentage = "PlanogramBlockingGroup_MetaP10Percentage";
        public const String PlanogramBlockingGroupMetaP11Percentage = "PlanogramBlockingGroup_MetaP11Percentage";
        public const String PlanogramBlockingGroupMetaP12Percentage = "PlanogramBlockingGroup_MetaP12Percentage";
        public const String PlanogramBlockingGroupMetaP13Percentage = "PlanogramBlockingGroup_MetaP13Percentage";
        public const String PlanogramBlockingGroupMetaP14Percentage = "PlanogramBlockingGroup_MetaP14Percentage";
        public const String PlanogramBlockingGroupMetaP15Percentage = "PlanogramBlockingGroup_MetaP15Percentage";
        public const String PlanogramBlockingGroupMetaP16Percentage = "PlanogramBlockingGroup_MetaP16Percentage";
        public const String PlanogramBlockingGroupMetaP17Percentage = "PlanogramBlockingGroup_MetaP17Percentage";
        public const String PlanogramBlockingGroupMetaP18Percentage = "PlanogramBlockingGroup_MetaP18Percentage";
        public const String PlanogramBlockingGroupMetaP19Percentage = "PlanogramBlockingGroup_MetaP19Percentage";
        public const String PlanogramBlockingGroupMetaP20Percentage = "PlanogramBlockingGroup_MetaP20Percentage";

        #endregion

        #region PlanogramBlockingLocation

        public const String PlanogramBlockingLocationId = "PlanogramBlockingLocation_Id";
        public const String PlanogramBlockingLocationPlanogramBlockingId = "PlanogramBlocking_Id";
        public const String PlanogramBlockingLocationPlanogramBlockingGroupId = "PlanogramBlockingGroup_Id";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerTopId = "PlanogramBlockingLocation_PlanogramBlockingDividerTopId";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerBottomId = "PlanogramBlockingLocation_PlanogramBlockingDividerBottomId";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerLeftId = "PlanogramBlockingLocation_PlanogramBlockingDividerLeftId";
        public const String PlanogramBlockingLocationPlanogramBlockingDividerRightId = "PlanogramBlockingLocation_PlanogramBlockingDividerRightId";
        public const String PlanogramBlockingLocationSpacePercentage = "PlanogramBlockingLocation_SpacePercentage";

        #endregion

        #region PlanogramComponent

        public const String PlanogramComponentId = "PlanogramComponent_Id";
        public const String PlanogramComponentPlanogramId = "Planogram_Id";
        public const String PlanogramComponentMesh3DId = "PlanogramComponent_Mesh3DId";
        public const String PlanogramComponentImageIdFront = "PlanogramComponent_ImageIdFront";
        public const String PlanogramComponentImageIdBack = "PlanogramComponent_ImageIdBack";
        public const String PlanogramComponentImageIdTop = "PlanogramComponent_ImageIdTop";
        public const String PlanogramComponentImageIdBottom = "PlanogramComponent_ImageIdBottom";
        public const String PlanogramComponentImageIdLeft = "PlanogramComponent_ImageIdLeft";
        public const String PlanogramComponentImageIdRight = "PlanogramComponent_ImageIdRight";
        public const String PlanogramComponentName = "PlanogramComponent_Name";
        public const String PlanogramComponentHeight = "PlanogramComponent_Height";
        public const String PlanogramComponentWidth = "PlanogramComponent_Width";
        public const String PlanogramComponentDepth = "PlanogramComponent_Depth";
        public const String PlanogramComponentMetaNumberOfSubComponents = "PlanogramComponent_MetaNumberOfSubComponents";
        public const String PlanogramComponentMetaNumberOfMerchandisedSubComponents = "PlanogramComponent_MetaNumberOfMerchandisedSubComponents";
        public const String PlanogramComponentIsMoveable = "PlanogramComponent_IsMoveable";
        public const String PlanogramComponentIsDisplayOnly = "PlanogramComponent_IsDisplayOnly";
        public const String PlanogramComponentCanAttachShelfEdgeLabel = "PlanogramComponent_CanAttachShelfEdgeLabel";
        public const String PlanogramComponentRetailerReferenceCode = "PlanogramComponent_RetailerReferenceCode";
        public const String PlanogramComponentBarCode = "PlanogramComponent_BarCode";
        public const String PlanogramComponentManufacturer = "PlanogramComponent_Manufacturer";
        public const String PlanogramComponentManufacturerPartName = "PlanogramComponent_ManufacturerPartName";
        public const String PlanogramComponentManufacturerPartNumber = "PlanogramComponent_ManufacturerPartNumber";
        public const String PlanogramComponentSupplierName = "PlanogramComponent_SupplierName";
        public const String PlanogramComponentSupplierPartNumber = "PlanogramComponent_SupplierPartNumber";
        public const String PlanogramComponentSupplierCostPrice = "PlanogramComponent_SupplierCostPrice";
        public const String PlanogramComponentSupplierDiscount = "PlanogramComponent_SupplierDiscount";
        public const String PlanogramComponentSupplierLeadTime = "PlanogramComponent_SupplierLeadTime";
        public const String PlanogramComponentMinPurchaseQty = "PlanogramComponent_MinPurchaseQty";
        public const String PlanogramComponentWeightLimit = "PlanogramComponent_WeightLimit";
        public const String PlanogramComponentWeight = "PlanogramComponent_Weight";
        public const String PlanogramComponentVolume = "PlanogramComponent_Volume";
        public const String PlanogramComponentDiameter = "PlanogramComponent_Diameter";
        public const String PlanogramComponentCapacity = "PlanogramComponent_Capacity";
        public const String PlanogramComponentComponentType = "PlanogramComponent_ComponentType";
        public const String PlanogramComponentIsMerchandisedTopDown = "PlanogramComponent_IsMerchandisedTopDown";

        #endregion

        #region PlanogramRenumberingStrategy

        public const String PlanogramRenumberingStrategyId = "PlanogramRenumberingStrategy_Id";
        public const String PlanogramRenumberingStrategyPlanogramId = "Planogram_Id";
        public const String PlanogramRenumberingStrategyName = "PlanogramRenumberingStrategy_Name";
        public const String PlanogramRenumberingStrategyBayComponentXRenumberStrategyPriority = "PlanogramRenumberingStrategy_BayComponentXRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyBayComponentYRenumberStrategyPriority = "PlanogramRenumberingStrategy_BayComponentYRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyBayComponentZRenumberStrategyPriority = "PlanogramRenumberingStrategy_BayComponentZRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyPositionXRenumberStrategyPriority = "PlanogramRenumberingStrategy_PositionXRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyPositionYRenumberStrategyPriority = "PlanogramRenumberingStrategy_PositionYRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyPositionZRenumberStrategyPriority = "PlanogramRenumberingStrategy_PositionZRenumberStrategyPriority";
        public const String PlanogramRenumberingStrategyBayComponentXRenumberStrategy = "PlanogramRenumberingStrategy_BayComponentXRenumberStrategy";
        public const String PlanogramRenumberingStrategyBayComponentYRenumberStrategy = "PlanogramRenumberingStrategy_BayComponentYRenumberStrategy";
        public const String PlanogramRenumberingStrategyBayComponentZRenumberStrategy = "PlanogramRenumberingStrategy_BayComponentZRenumberStrategy";
        public const String PlanogramRenumberingStrategyPositionXRenumberStrategy = "PlanogramRenumberingStrategy_PositionXRenumberStrategy";
        public const String PlanogramRenumberingStrategyPositionYRenumberStrategy = "PlanogramRenumberingStrategy_PositionYRenumberStrategy";
        public const String PlanogramRenumberingStrategyPositionZRenumberStrategy = "PlanogramRenumberingStrategy_PositionZRenumberStrategy";
        public const String PlanogramRenumberingStrategyRestartComponentRenumberingPerBay = "PlanogramRenumberingStrategy_RestartComponentRenumberingPerBay";
        public const String PlanogramRenumberingStrategyIgnoreNonMerchandisingComponents = "PlanogramRenumberingStrategy_IgnoreNonMerchandisingComponents";
        public const String PlanogramRenumberingStrategyRestartPositionRenumberingPerComponent = "PlanogramRenumberingStrategy_RestartPositionRenumberingPerComponent";
        public const String PlanogramRenumberingStrategyUniqueNumberMultiPositionProductsPerComponent = "PlanogramRenumberingStrategy_UniqueNumberMultiPositionProductsPerComponent";
        public const String PlanogramRenumberingStrategyExceptAdjacentPositions = "PlanogramRenumberingStrategy_ExceptAdjacentPositions";
        public const String PlanogramRenumberingStrategyIsEnabled = "PlanogramRenumberingStrategy_IsEnabled";
        public const String PlanogramRenumberingStrategyRestartPositionRenumberingPerBay = "PlanogramRenumberingStrategy_RestartPositionRenumberingPerBay";
        public const String PlanogramRenumberingStrategyRestartComponentRenumberingPerComponentType = "PlanogramRenumberingStrategy_RestartComponentRenumberingPerComponentType";

        #endregion

        #region Product

        public const String ProductId = "Product_Id";
        public const String ProductRowVersion = "Product_RowVersion";
        public const String ProductEntityId = "Entity_Id";
        public const String ProductReplacementProductId = "Product_ReplacementProductId";
        public const String ProductGtin = "Product_Gtin";
        public const String ProductName = "Product_Name";
        public const String ProductHeight = "Product_Height";
        public const String ProductWidth = "Product_Width";
        public const String ProductDepth = "Product_Depth";
        public const String ProductDisplayHeight = "Product_DisplayHeight";
        public const String ProductDisplayWidth = "Product_DisplayWidth";
        public const String ProductDisplayDepth = "Product_DisplayDepth";
        public const String ProductAlternateHeight = "Product_AlternateHeight";
        public const String ProductAlternateWidth = "Product_AlternateWidth";
        public const String ProductAlternateDepth = "Product_AlternateDepth";
        public const String ProductPointOfPurchaseHeight = "Product_PointOfPurchaseHeight";
        public const String ProductPointOfPurchaseWidth = "Product_PointOfPurchaseWidth";
        public const String ProductPointOfPurchaseDepth = "Product_PointOfPurchaseDepth";
        public const String ProductNumberOfPegHoles = "Product_NumberOfPegHoles";
        public const String ProductPegX = "Product_PegX";
        public const String ProductPegX2 = "Product_PegX2";
        public const String ProductPegX3 = "Product_PegX3";
        public const String ProductPegY = "Product_PegY";
        public const String ProductPegY2 = "Product_PegY2";
        public const String ProductPegY3 = "Product_PegY3";
        public const String ProductPegProngOffset = "Product_PegProngOffset";
        public const String ProductPegProngOffsetX = "Product_PegProngOffsetX";
        public const String ProductPegProngOffsetY = "Product_PegProngOffsetY";
        public const String ProductPegDepth = "Product_PegDepth";
        public const String ProductSqueezeHeight = "Product_SqueezeHeight";
        public const String ProductSqueezeWidth = "Product_SqueezeWidth";
        public const String ProductSqueezeDepth = "Product_SqueezeDepth";
        public const String ProductNestingHeight = "Product_NestingHeight";
        public const String ProductNestingWidth = "Product_NestingWidth";
        public const String ProductNestingDepth = "Product_NestingDepth";
        public const String ProductCasePackUnits = "Product_CasePackUnits";
        public const String ProductCaseHigh = "Product_CaseHigh";
        public const String ProductCaseWide = "Product_CaseWide";
        public const String ProductCaseDeep = "Product_CaseDeep";
        public const String ProductCaseHeight = "Product_CaseHeight";
        public const String ProductCaseWidth = "Product_CaseWidth";
        public const String ProductCaseDepth = "Product_CaseDepth";
        public const String ProductMaxStack = "Product_MaxStack";
        public const String ProductMaxTopCap = "Product_MaxTopCap";
        public const String ProductMaxRightCap = "Product_MaxRightCap";
        public const String ProductMinDeep = "Product_MinDeep";
        public const String ProductMaxDeep = "Product_MaxDeep";
        public const String ProductTrayPackUnits = "Product_TrayPackUnits";
        public const String ProductTrayHigh = "Product_TrayHigh";
        public const String ProductTrayWide = "Product_TrayWide";
        public const String ProductTrayDeep = "Product_TrayDeep";
        public const String ProductTrayHeight = "Product_TrayHeight";
        public const String ProductTrayWidth = "Product_TrayWidth";
        public const String ProductTrayDepth = "Product_TrayDepth";
        public const String ProductTrayThickHeight = "Product_TrayThickHeight";
        public const String ProductTrayThickWidth = "Product_TrayThickWidth";
        public const String ProductTrayThickDepth = "Product_TrayThickDepth";
        public const String ProductFrontOverhang = "Product_FrontOverhang";
        public const String ProductFingerSpaceAbove = "Product_FingerSpaceAbove";
        public const String ProductFingerSpaceToTheSide = "Product_FingerSpaceToTheSide";
        public const String ProductStatusType = "Product_StatusType";
        public const String ProductOrientationType = "Product_OrientationType";
        public const String ProductMerchandisingStyle = "Product_MerchandisingStyle";
        public const String ProductIsFrontOnly = "Product_IsFrontOnly";
        public const String ProductIsPlaceHolderProduct = "Product_IsPlaceHolderProduct";
        public const String ProductIsActive = "Product_IsActive";
        public const String ProductCanBreakTrayUp = "Product_CanBreakTrayUp";
        public const String ProductCanBreakTrayDown = "Product_CanBreakTrayDown";
        public const String ProductCanBreakTrayBack = "Product_CanBreakTrayBack";
        public const String ProductCanBreakTrayTop = "Product_CanBreakTrayTop";
        public const String ProductForceMiddleCap = "Product_ForceMiddleCap";
        public const String ProductForceBottomCap = "Product_ForceBottomCap";
        public const String ProductPlacementStyle = "Product_PlacementStyle";
        public const String ProductShape = "Product_Shape";
        public const String ProductShapeType = "Product_ShapeType";
        public const String ProductFillColour = "Product_FillColour";
        public const String ProductFillPatternType = "Product_FillPatternType";
        public const String ProductPointOfPurchaseDescription = "Product_PointOfPurchaseDescription";
        public const String ProductShortDescription = "Product_ShortDescription";
        public const String ProductSubcategory = "Product_Subcategory";
        public const String ProductCustomerStatus = "Product_CustomerStatus";
        public const String ProductColour = "Product_Colour";
        public const String ProductFlavour = "Product_Flavour";
        public const String ProductPackagingShape = "Product_PackagingShape";
        public const String ProductPackagingType = "Product_PackagingType";
        public const String ProductCountryOfOrigin = "Product_CountryOfOrigin";
        public const String ProductCountryOfProcessing = "Product_CountryOfProcessing";
        public const String ProductShelfLife = "Product_ShelfLife";
        public const String ProductDeliveryFrequencyDays = "Product_DeliveryFrequencyDays";
        public const String ProductDeliveryMethod = "Product_DeliveryMethod";
        public const String ProductBrand = "Product_Brand";
        public const String ProductVendorCode = "Product_VendorCode";
        public const String ProductVendor = "Product_Vendor";
        public const String ProductManufacturerCode = "Product_ManufacturerCode";
        public const String ProductManufacturer = "Product_Manufacturer";
        public const String ProductSize = "Product_Size";
        public const String ProductUnitOfMeasure = "Product_UnitOfMeasure";
        public const String ProductDateIntroduced = "Product_DateIntroduced";
        public const String ProductDateDiscontinued = "Product_DateDiscontinued";
        public const String ProductDateEffective = "Product_DateEffective";
        public const String ProductHealth = "Product_Health";
        public const String ProductCorporateCode = "Product_CorporateCode";
        public const String ProductBarcode = "Product_Barcode";
        public const String ProductSellPrice = "Product_SellPrice";
        public const String ProductSellPackCount = "Product_SellPackCount";
        public const String ProductSellPackDescription = "Product_SellPackDescription";
        public const String ProductRecommendedRetailPrice = "Product_RecommendedRetailPrice";
        public const String ProductManufacturerRecommendedRetailPrice = "Product_ManufacturerRecommendedRetailPrice";
        public const String ProductCostPrice = "Product_CostPrice";
        public const String ProductCaseCost = "Product_CaseCost";
        public const String ProductTaxRate = "Product_TaxRate";
        public const String ProductConsumerInformation = "Product_ConsumerInformation";
        public const String ProductTexture = "Product_Texture";
        public const String ProductStyleNumber = "Product_StyleNumber";
        public const String ProductPattern = "Product_Pattern";
        public const String ProductModel = "Product_Model";
        public const String ProductGarmentType = "Product_GarmentType";
        public const String ProductIsPrivateLabel = "Product_IsPrivateLabel";
        public const String ProductIsNew = "Product_IsNew";
        public const String ProductFinancialGroupCode = "Product_FinancialGroupCode";
        public const String ProductFinancialGroupName = "Product_FinancialGroupName";
        public const String ProductDateCreated = "Product_DateCreated";
        public const String ProductDateLastModified = "Product_DateLastModified";
        public const String ProductDateDeleted = "Product_DateDeleted";
        public const String ProductSetProperties = "Product_SetProperties";
        public const String ProductSearchText = "Product_SearchText";

        #endregion

        #region ProductGroup

        public const String ProductGroupId = "ProductGroup_Id";
        public const String ProductGroupProductHierarchyId = "ProductHierarchy_Id";
        public const String ProductGroupProductLevelId = "ProductLevel_Id";
        public const String ProductGroupName = "ProductGroup_Name";
        public const String ProductGroupCode = "ProductGroup_Code";
        public const String ProductGroupParentGroupId = "ProductGroup_ParentGroupId";
        public const String ProductGroupDateCreated = "ProductGroup_DateCreated";
        public const String ProductGroupDateLastModified = "ProductGroup_DateLastModified";
        public const String ProductGroupDateDeleted = "ProductGroup_DateDeleted";
        public const String ProductGroupSetProperties = "ProductGroup_SetProperties";

        #endregion

        #region ProductHierarchy

        public const String ProductHierarchyId = "ProductHierarchy_Id";
        public const String ProductHierarchyRowVersion = "ProductHierarchy_RowVersion";
        public const String ProductHierarchyEntityId = "Entity_Id";
        public const String ProductHierarchyName = "ProductHierarchy_Name";
        public const String ProductHierarchyDateCreated = "ProductHierarchy_DateCreated";
        public const String ProductHierarchyDateLastModified = "ProductHierarchy_DateLastModified";
        public const String ProductHierarchyDateDeleted = "ProductHierarchy_DateDeleted";

        #endregion

        #region ProductLevel

        public const String ProductLevelId = "ProductLevel_Id";
        public const String ProductLevelProductHierarchyId = "ProductHierarchy_Id";
        public const String ProductLevelName = "ProductLevel_Name";
        public const String ProductLevelShapeNo = "ProductLevel_ShapeNo";
        public const String ProductLevelColour = "ProductLevel_Colour";
        public const String ProductLevelParentLevelId = "ProductLevel_ParentLevelId";
        public const String ProductLevelDateCreated = "ProductLevel_DateCreated";
        public const String ProductLevelDateLastModified = "ProductLevel_DateLastModified";
        public const String ProductLevelDateDeleted = "ProductLevel_DateDeleted";

        #endregion

        #region ProductLibrary
        public const String ProductLibraryId = "ProductLibrary_Id";
        public const String ProductLibraryRowVersion = "ProductLibrary_RowVersion";
        public const String ProductLibraryName = "ProductLibrary_Name";
        public const String ProductLibraryEntityId = "Entity_Id";
        public const String ProductLibraryFileType = "ProductLibrary_FileType";
        public const String ProductLibraryExcelWorkbookSheet = "ProductLibrary_ExcelWorkbookSheet";
        public const String ProductLibraryStartAtRow = "ProductLibrary_StartAtRow";
        public const String ProductLibraryIsFirstRowHeaders = "ProductLibrary_IsFirstRowHeaders";
        public const String ProductLibraryTextDataFormat = "ProductLibrary_TextDataFormat";
        public const String ProductLibraryTextDelimiterType = "ProductLibrary_TextDelimiterType";
        public const String ProductLibraryDateCreated = "ProductLibrary_DateCreated";
        public const String ProductLibraryDateLastModified = "ProductLibrary_DateLastModified";
        #endregion

        #region ProductLibraryColumnMapping
        public const String ProductLibraryColumnMappingId = "ProductLibraryColumnMapping_Id";
        public const String ProductLibraryColumnMappingProductLibraryId = "ProductLibrary_Id";
        public const String ProductLibraryColumnMappingSource = "ProductLibraryColumnMapping_Source";
        public const String ProductLibraryColumnMappingDestination = "ProductLibraryColumnMapping_Destination";
        public const String ProductLibraryColumnMappingIsMandatory = "ProductLibraryColumnMapping_IsMandatory";
        public const String ProductLibraryColumnMappingType = "ProductLibraryColumnMapping_Type";
        public const String ProductLibraryColumnMappingMin = "ProductLibraryColumnMapping_Min";
        public const String ProductLibraryColumnMappingMax = "ProductLibraryColumnMapping_Max";
        public const String ProductLibraryColumnMappingCanDefault = "ProductLibraryColumnMapping_CanDefault";
        public const String ProductLibraryColumnMappingDefault = "ProductLibraryColumnMapping_Default";
        public const String ProductLibraryColumnMappingTextFixedWidthColumnStart = "ProductLibraryColumnMapping_TextFixedWidthColumnStart";
        public const String ProductLibraryColumnMappingTextFixedWidthColumnEnd = "ProductLibraryColumnMapping_TextFixedWidthColumnEnd";
        #endregion

        #region PlanogramAssortment

        public const String PlanogramAssortmentId = "PlanogramAssortment_Id";
        public const String PlanogramAssortmentPlanogramId = "Planogram_Id";
        public const String PlanogramAssortmentName = "PlanogramAssortment_Name";

        #endregion

        #region PlanogramAssortmentInventoryRule

        public const String PlanogramAssortmentInventoryRuleId = "PlanogramAssortmentInventoryRule_Id";
        public const String PlanogramAssortmentInventoryRuleProductGtin = "PlanogramAssortmentInventoryRule_ProductGtin";
        public const String PlanogramAssortmentInventoryRuleCasePack = "PlanogramAssortmentInventoryRule_CasePack";
        public const String PlanogramAssortmentInventoryRuleDaysOfSupply = "PlanogramAssortmentInventoryRule_DaysOfSupply";
        public const String PlanogramAssortmentInventoryRuleShelfLife = "PlanogramAssortmentInventoryRule_ShelfLife";
        public const String PlanogramAssortmentInventoryRuleReplenishmentDays = "PlanogramAssortmentInventoryRule_ReplenishmentDays";
        public const String PlanogramAssortmentInventoryRuleWasteHurdleUnits = "PlanogramAssortmentInventoryRule_WasteHurdleUnits";
        public const String PlanogramAssortmentInventoryRuleWasteHurdleCasePack = "PlanogramAssortmentInventoryRule_WasteHurdleCasePack";
        public const String PlanogramAssortmentInventoryRuleMinUnits = "PlanogramAssortmentInventoryRule_MinUnits";
        public const String PlanogramAssortmentInventoryRuleMinFacings = "PlanogramAssortmentInventoryRule_MinFacings";
        public const String PlanogramAssortmentInventoryRulePlanogramAssortmentId = "PlanogramAssortment_Id";

        #endregion

        #region PlanogramAssortmentLocalProduct

        public const String PlanogramAssortmentLocalProductId = "PlanogramAssortmentLocalProduct_Id";
        public const String PlanogramAssortmentLocalProductPlanogramAssortmentId = "PlanogramAssortment_Id";
        public const String PlanogramAssortmentLocalProductProductGtin = "PlanogramAssortmentLocalProduct_ProductGtin";
        public const String PlanogramAssortmentLocalProductLocationCode = "PlanogramAssortmentLocalProduct_LocationCode";

        #endregion

        #region PlanogramAssortmentLocationBuddy

        public const String PlanogramAssortmentLocationBuddyId = "PlanogramAssortmentLocationBuddy_Id";
        public const String PlanogramAssortmentLocationBuddyLocationCode = "PlanogramAssortmentLocationBuddy_LocationCode";
        public const String PlanogramAssortmentLocationBuddyTreatmentType = "PlanogramAssortmentLocationBuddy_TreatmentType";
        public const String PlanogramAssortmentLocationBuddyS1LocationCode = "PlanogramAssortmentLocationBuddy_S1LocationCode";
        public const String PlanogramAssortmentLocationBuddyS1Percentage = "PlanogramAssortmentLocationBuddy_S1Percentage";
        public const String PlanogramAssortmentLocationBuddyS2LocationCode = "PlanogramAssortmentLocationBuddy_S2LocationCode";
        public const String PlanogramAssortmentLocationBuddyS2Percentage = "PlanogramAssortmentLocationBuddy_S2Percentage";
        public const String PlanogramAssortmentLocationBuddyS3LocationCode = "PlanogramAssortmentLocationBuddy_S3LocationCode";
        public const String PlanogramAssortmentLocationBuddyS3Percentage = "PlanogramAssortmentLocationBuddy_S3Percentage";
        public const String PlanogramAssortmentLocationBuddyS4LocationCode = "PlanogramAssortmentLocationBuddy_S4LocationCode";
        public const String PlanogramAssortmentLocationBuddyS4Percentage = "PlanogramAssortmentLocationBuddy_S4Percentage";
        public const String PlanogramAssortmentLocationBuddyS5LocationCode = "PlanogramAssortmentLocationBuddy_S5LocationCode";
        public const String PlanogramAssortmentLocationBuddyS5Percentage = "PlanogramAssortmentLocationBuddy_S5Percentage";
        public const String PlanogramAssortmentLocationBuddyPlanogramAssortmentId = "PlanogramAssortment_Id";

        #endregion

        #region PlanogramAssortmentProduct

        public const String PlanogramAssortmentProductId = "PlanogramAssortmentProduct_Id";
        public const String PlanogramAssortmentProductPlanogramAssortmentId = "PlanogramAssortment_Id";
        public const String PlanogramAssortmentProductGtin = "PlanogramAssortmentProduct_Gtin";
        public const String PlanogramAssortmentProductName = "PlanogramAssortmentProduct_Name";
        public const String PlanogramAssortmentProductIsRanged = "PlanogramAssortmentProduct_IsRanged";
        public const String PlanogramAssortmentProductRank = "PlanogramAssortmentProduct_Rank";
        public const String PlanogramAssortmentProductSegmentation = "PlanogramAssortmentProduct_Segmentation";
        public const String PlanogramAssortmentProductFacings = "PlanogramAssortmentProduct_Facings";
        public const String PlanogramAssortmentProductUnits = "PlanogramAssortmentProduct_Units";
        public const String PlanogramAssortmentProductProductTreatmentType = "PlanogramAssortmentProduct_ProductTreatmentType";
        public const String PlanogramAssortmentProductProductLocalizationType = "PlanogramAssortmentProduct_ProductLocalizationType";
        public const String PlanogramAssortmentProductComments = "PlanogramAssortmentProduct_Comments";
        public const String PlanogramAssortmentProductExactListFacings = "PlanogramAssortmentProduct_ExactListFacings";
        public const String PlanogramAssortmentProductExactListUnits = "PlanogramAssortmentProduct_ExactListUnits";
        public const String PlanogramAssortmentProductPreserveListFacings = "PlanogramAssortmentProduct_PreserveListFacings";
        public const String PlanogramAssortmentProductPreserveListUnits = "PlanogramAssortmentProduct_PreserveListUnits";
        public const String PlanogramAssortmentProductMaxListFacings = "PlanogramAssortmentProduct_MaxListFacings";
        public const String PlanogramAssortmentProductMaxListUnits = "PlanogramAssortmentProduct_MaxListUnits";
        public const String PlanogramAssortmentProductMinListFacings = "PlanogramAssortmentProduct_MinListFacings";
        public const String PlanogramAssortmentProductMinListUnits = "PlanogramAssortmentProduct_MinListUnits";
        public const String PlanogramAssortmentProductFamilyRuleName = "PlanogramAssortmentProduct_FamilyRuleName";
        public const String PlanogramAssortmentProductFamilyRuleType = "PlanogramAssortmentProduct_FamilyRuleType";
        public const String PlanogramAssortmentProductFamilyRuleValue = "PlanogramAssortmentProduct_FamilyRuleValue";
        public const String PlanogramAssortmentProductIsPrimaryRegionalProduct = "PlanogramAssortmentProduct_IsPrimaryRegionalProduct";
        public const String PlanogramAssortmentProductDelistedDueToAssortmentRule = "PlanogramAssortmentProduct_DelistedDueToAssortmentRule";
        public const String PlanogramAssortmentProductMetaIsProductPlacedOnPlanogram = "PlanogramAssortmentProduct_MetaIsProductPlacedOnPlanogram";

        #endregion

        #region PlanogramAssortmentProductBuddy

        public const String PlanogramAssortmentProductBuddyId = "PlanogramAssortmentProductBuddy_Id";
        public const String PlanogramAssortmentProductBuddyProductGtin = "PlanogramAssortmentProductBuddy_ProductGtin";
        public const String PlanogramAssortmentProductBuddySourceType = "PlanogramAssortmentProductBuddy_SourceType";
        public const String PlanogramAssortmentProductBuddyTreatmentType = "PlanogramAssortmentProductBuddy_TreatmentType";
        public const String PlanogramAssortmentProductBuddyTreatmentTypePercentage = "PlanogramAssortmentProductBuddy_TreatmentTypePercentage";
        public const String PlanogramAssortmentProductBuddyProductAttributeType = "PlanogramAssortmentProductBuddy_ProductAttributeType";
        public const String PlanogramAssortmentProductBuddyS1ProductGtin = "PlanogramAssortmentProductBuddy_S1ProductGtin";
        public const String PlanogramAssortmentProductBuddyS1Percentage = "PlanogramAssortmentProductBuddy_S1Percentage";
        public const String PlanogramAssortmentProductBuddyS2ProductGtin = "PlanogramAssortmentProductBuddy_S2ProductGtin";
        public const String PlanogramAssortmentProductBuddyS2Percentage = "PlanogramAssortmentProductBuddy_S2Percentage";
        public const String PlanogramAssortmentProductBuddyS3ProductGtin = "PlanogramAssortmentProductBuddy_S3ProductGtin";
        public const String PlanogramAssortmentProductBuddyS3Percentage = "PlanogramAssortmentProductBuddy_S3Percentage";
        public const String PlanogramAssortmentProductBuddyS4ProductGtin = "PlanogramAssortmentProductBuddy_S4ProductGtin";
        public const String PlanogramAssortmentProductBuddyS4Percentage = "PlanogramAssortmentProductBuddy_S4Percentage";
        public const String PlanogramAssortmentProductBuddyS5ProductGtin = "PlanogramAssortmentProductBuddy_S5ProductGtin";
        public const String PlanogramAssortmentProductBuddyS5Percentage = "PlanogramAssortmentProductBuddy_S5Percentage";
        public const String PlanogramAssortmentProductBuddyPlanogramAssortmentId = "PlanogramAssortment_Id";

        #endregion

        #region PlanogramAssortmentRegion

        public const String PlanogramAssortmentRegionId = "PlanogramAssortmentRegion_Id";
        public const String PlanogramAssortmentRegionPlanogramAssortmentId = "PlanogramAssortment_Id";
        public const String PlanogramAssortmentRegionName = "PlanogramAssortmentRegion_Name";

        #endregion

        #region PlanogramAssortmentRegionLocation

        public const String PlanogramAssortmentRegionLocationId = "PlanogramAssortmentRegionLocation_Id";
        public const String PlanogramAssortmentRegionLocationPlanogramAssortmentRegionId = "PlanogramAssortmentRegion_Id";
        public const String PlanogramAssortmentRegionLocationLocationCode = "PlanogramAssortmentRegionLocation_LocationCode";

        #endregion

        #region PlanogramAssortmentRegionProduct

        public const String PlanogramAssortmentRegionProductId = "PlanogramAssortmentRegionProduct_Id";
        public const String PlanogramAssortmentRegionProductPlanogramAssortmentRegionId = "PlanogramAssortmentRegion_Id";
        public const String PlanogramAssortmentRegionProductPrimaryProductGtin = "PlanogramAssortmentRegionProduct_PrimaryProductGtin";
        public const String PlanogramAssortmentRegionProductRegionalProductGtin = "PlanogramAssortmentRegionProduct_RegionalProductGtin";

        #endregion

        #region PlanogramComparisonTemplate

        public const String PlanogramComparisonTemplateId = "PlanogramComparisonTemplate_Id";
        public const String PlanogramComparisonTemplateRowVersion = "PlanogramComparisonTemplate_RowVersion";
        public const String PlanogramComparisonTemplateEntityId = "Entity_Id";
        public const String PlanogramComparisonTemplateDateCreated = "PlanogramComparisonTemplate_DateCreated";
        public const String PlanogramComparisonTemplateDateLastModified = "PlanogramComparisonTemplate_DateLastModified";
        public const String PlanogramComparisonTemplateName = "PlanogramComparisonTemplate_Name";
        public const String PlanogramComparisonTemplateDescription = "PlanogramComparisonTemplate_Description";
        public const String PlanogramComparisonTemplateIgnoreNonPlacedProducts = "PlanogramComparisonTemplate_IgnoreNonPlacedProducts";
        public const String PlanogramComparisonTemplateDataOrderType = "PlanogramComparisonTemplate_DataOrderType";

        #endregion

        #region PlanogramComparisonTemplateField

        public const String PlanogramComparisonTemplateFieldId = "PlanogramComparisonTemplateField_Id";
        public const String PlanogramComparisonTemplateFieldPlanogramComparisonTemplateId = "PlanogramComparisonTemplate_Id";
        public const String PlanogramComparisonTemplateFieldItemType = "PlanogramComparisonTemplateField_ItemType";
        public const String PlanogramComparisonTemplateFieldFieldPlaceholder = "PlanogramComparisonTemplateField_FieldPlaceholder";
        public const String PlanogramComparisonTemplateFieldDisplayName = "PlanogramComparisonTemplateField_DisplayName";
        public const String PlanogramComparisonTemplateFieldNumber = "PlanogramComparisonTemplateField_Number";
        public const String PlanogramComparisonTemplateFieldDisplay = "PlanogramComparisonTemplateField_Display";

        #endregion

        #region PlanogramComparison

        public const String PlanogramComparisonId = "PlanogramComparison_Id";
        public const String PlanogramComparisonPlanogramId = "Planogram_Id";
        public const String PlanogramComparisonName = "PlanogramComparison_Name";
        public const String PlanogramComparisonDescription = "PlanogramComparison_Description";
        public const String PlanogramComparisonDateLastCompared = "PlanogramComparison_DateLastCompared";
        public const String PlanogramComparisonIgnoreNonPlacedProducts = "PlanogramComparison_IgnoreNonPlacedProducts";
        public const String PlanogramComparisonDataOrderType = "PlanogramComparison_DataOrderType";

        #endregion

        #region PlanogramComparisonField

        public const String PlanogramComparisonFieldId = "PlanogramComparisonField_Id";
        public const String PlanogramComparisonFieldPlanogramComparisonId = "PlanogramComparison_Id";
        public const String PlanogramComparisonFieldItemType = "PlanogramComparisonField_ItemType";
        public const String PlanogramComparisonFieldDisplayName = "PlanogramComparisonField_DisplayName";
        public const String PlanogramComparisonFieldNumber = "PlanogramComparisonField_Number";
        public const String PlanogramComparisonFieldFieldPlaceholder = "PlanogramComparisonField_FieldPlaceholder";
        public const String PlanogramComparisonFieldDisplay = "PlanogramComparisonField_Display";

        #endregion

        #region PlanogramComparisonResult

        public const String PlanogramComparisonResultId = "PlanogramComparisonResult_Id";
        public const String PlanogramComparisonResultPlanogramComparisonId = "PlanogramComparison_Id";
        public const String PlanogramComparisonResultPlanogramName = "PlanogramComparisonResult_PlanogramName";
        public const String PlanogramComparisonResultStatus = "PlanogramComparisonResult_Status";

        #endregion

        #region PlanogramComparisonItem

        public const String PlanogramComparisonItemId = "PlanogramComparisonItem_Id";
        public const String PlanogramComparisonItemPlanogramComparisonResultId = "PlanogramComparisonResult_Id";
        public const String PlanogramComparisonItemItemType = "PlanogramComparisonItem_ItemType";
        public const String PlanogramComparisonItemItemId = "PlanogramComparisonItem_ItemId";
        public const String PlanogramComparisonItemStatus = "PlanogramComparisonItem_Status";

        #endregion

        #region PlanogramComparisonFieldValue

        public const String PlanogramComparisonFieldValueId = "PlanogramComparisonFieldValue_Id";
        public const String PlanogramComparisonFieldValuePlanogramComparisonItemId = "PlanogramComparisonItem_Id";
        public const String PlanogramComparisonFieldValueFieldPlaceholder = "PlanogramComparisonFieldValue_FieldPlaceholder";
        public const String PlanogramComparisonFieldValueValue = "PlanogramComparisonFieldValue_Value";

        #endregion

        #region PlanogramConsumerDecisionTree

        public const String PlanogramConsumerDecisionTreeId = "PlanogramConsumerDecisionTree_Id";
        public const String PlanogramConsumerDecisionTreePlanogramId = "Planogram_Id";
        public const String PlanogramConsumerDecisionTreeName = "PlanogramConsumerDecisionTree_Name";

        #endregion

        #region PlanogramConsumerDecisionTreeLevel

        public const String PlanogramConsumerDecisionTreeLevelId = "PlanogramConsumerDecisionTreeLevel_Id";
        public const String PlanogramConsumerDecisionTreeLevelPlanogramConsumerDecisionTreeId = "PlanogramConsumerDecisionTree_Id";
        public const String PlanogramConsumerDecisionTreeLevelParentLevelId = "PlanogramConsumerDecisionTreeLevel_ParentLevelId";
        public const String PlanogramConsumerDecisionTreeLevelName = "PlanogramConsumerDecisionTreeLevel_Name";

        #endregion

        #region PlanogramConsumerDecisionTreeNode

        public const String PlanogramConsumerDecisionTreeNodeId = "PlanogramConsumerDecisionTreeNode_Id";
        public const String PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeId = "PlanogramConsumerDecisionTree_Id";
        public const String PlanogramConsumerDecisionTreeNodePlanogramConsumerDecisionTreeLevelId = "PlanogramConsumerDecisionTreeLevel_Id";
        public const String PlanogramConsumerDecisionTreeNodeParentNodeId = "PlanogramConsumerDecisionTreeNode_ParentNodeId";
        public const String PlanogramConsumerDecisionTreeNodeName = "PlanogramConsumerDecisionTreeNode_Name";
        public const String PlanogramConsumerDecisionTreeNodeMetaCountOfProducts = "PlanogramConsumerDecisionTreeNode_MetaCountOfProducts";
        public const String PlanogramConsumerDecisionTreeNodeMetaCountOfProductRecommendedInAssortment = "PlanogramConsumerDecisionTreeNode_MetaCountOfProductRecommendedInAssortment";
        public const String PlanogramConsumerDecisionTreeNodeMetaCountOfProductPlacedOnPlanogram = "PlanogramConsumerDecisionTreeNode_MetaCountOfProductPlacedOnPlanogram";
        public const String PlanogramConsumerDecisionTreeNodeMetaLinearSpaceAllocatedToProductsOnPlanogram = "PlanogramConsumerDecisionTreeNode_MetaLinearSpaceAllocatedToProductsOnPlanogram";
        public const String PlanogramConsumerDecisionTreeNodeMetaAreaSpaceAllocatedToProductsOnPlanogram = "PlanogramConsumerDecisionTreeNode_MetaAreaSpaceAllocatedToProductsOnPlanogram";
        public const String PlanogramConsumerDecisionTreeNodeMetaVolumetricSpaceAllocatedToProductsOnPlanogram = 
            "PlanogramConsumerDecisionTreeNode_MetaVolumetricSpaceAllocatedToProductsOnPlanogram";

        public const String PlanogramConsumerDecisionTreeNodeMetaP1Performance = "PlanogramConsumerDecisionTreeNode_MetaP1";
        public const String PlanogramConsumerDecisionTreeNodeMetaP2Performance = "PlanogramConsumerDecisionTreeNode_MetaP2";
        public const String PlanogramConsumerDecisionTreeNodeMetaP3Performance = "PlanogramConsumerDecisionTreeNode_MetaP3";
        public const String PlanogramConsumerDecisionTreeNodeMetaP4Performance = "PlanogramConsumerDecisionTreeNode_MetaP4";
        public const String PlanogramConsumerDecisionTreeNodeMetaP5Performance = "PlanogramConsumerDecisionTreeNode_MetaP5";
        public const String PlanogramConsumerDecisionTreeNodeMetaP6Performance = "PlanogramConsumerDecisionTreeNode_MetaP6";
        public const String PlanogramConsumerDecisionTreeNodeMetaP7Performance = "PlanogramConsumerDecisionTreeNode_MetaP7";
        public const String PlanogramConsumerDecisionTreeNodeMetaP8Performance = "PlanogramConsumerDecisionTreeNode_MetaP8";
        public const String PlanogramConsumerDecisionTreeNodeMetaP9Performance = "PlanogramConsumerDecisionTreeNode_MetaP9";
        public const String PlanogramConsumerDecisionTreeNodeMetaP10Performance = "PlanogramConsumerDecisionTreeNode_MetaP10";
        public const String PlanogramConsumerDecisionTreeNodeMetaP11Performance = "PlanogramConsumerDecisionTreeNode_MetaP11";
        public const String PlanogramConsumerDecisionTreeNodeMetaP12Performance = "PlanogramConsumerDecisionTreeNode_MetaP12";
        public const String PlanogramConsumerDecisionTreeNodeMetaP13Performance = "PlanogramConsumerDecisionTreeNode_MetaP13";
        public const String PlanogramConsumerDecisionTreeNodeMetaP14Performance = "PlanogramConsumerDecisionTreeNode_MetaP14";
        public const String PlanogramConsumerDecisionTreeNodeMetaP15Performance = "PlanogramConsumerDecisionTreeNode_MetaP15";
        public const String PlanogramConsumerDecisionTreeNodeMetaP16Performance = "PlanogramConsumerDecisionTreeNode_MetaP16";
        public const String PlanogramConsumerDecisionTreeNodeMetaP17Performance = "PlanogramConsumerDecisionTreeNode_MetaP17";
        public const String PlanogramConsumerDecisionTreeNodeMetaP18Performance = "PlanogramConsumerDecisionTreeNode_MetaP18";
        public const String PlanogramConsumerDecisionTreeNodeMetaP19Performance = "PlanogramConsumerDecisionTreeNode_MetaP19";
        public const String PlanogramConsumerDecisionTreeNodeMetaP20Performance = "PlanogramConsumerDecisionTreeNode_MetaP20";

        public const String PlanogramConsumerDecisionTreeNodeMetaP1Percentage = "PlanogramConsumerDecisionTreeNode_MetaP1Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP2Percentage = "PlanogramConsumerDecisionTreeNode_MetaP2Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP3Percentage = "PlanogramConsumerDecisionTreeNode_MetaP3Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP4Percentage = "PlanogramConsumerDecisionTreeNode_MetaP4Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP5Percentage = "PlanogramConsumerDecisionTreeNode_MetaP5Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP6Percentage = "PlanogramConsumerDecisionTreeNode_MetaP6Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP7Percentage = "PlanogramConsumerDecisionTreeNode_MetaP7Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP8Percentage = "PlanogramConsumerDecisionTreeNode_MetaP8Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP9Percentage = "PlanogramConsumerDecisionTreeNode_MetaP9Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP10Percentage = "PlanogramConsumerDecisionTreeNode_MetaP10Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP11Percentage = "PlanogramConsumerDecisionTreeNode_MetaP11Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP12Percentage = "PlanogramConsumerDecisionTreeNode_MetaP12Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP13Percentage = "PlanogramConsumerDecisionTreeNode_MetaP13Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP14Percentage = "PlanogramConsumerDecisionTreeNode_MetaP14Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP15Percentage = "PlanogramConsumerDecisionTreeNode_MetaP15Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP16Percentage = "PlanogramConsumerDecisionTreeNode_MetaP16Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP17Percentage = "PlanogramConsumerDecisionTreeNode_MetaP17Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP18Percentage = "PlanogramConsumerDecisionTreeNode_MetaP18Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP19Percentage = "PlanogramConsumerDecisionTreeNode_MetaP19Percentage";
        public const String PlanogramConsumerDecisionTreeNodeMetaP20Percentage = "PlanogramConsumerDecisionTreeNode_MetaP20Percentage";

        #endregion

        #region PlanogramConsumerDecisionTreeNodeProduct

        public const String PlanogramConsumerDecisionTreeNodeProductId = "PlanogramConsumerDecisionTreeNodeProduct_Id";
        public const String PlanogramConsumerDecisionTreeNodeProductPlanogramConsumerDecisionTreeNodeId = "PlanogramConsumerDecisionTreeNode_Id";
        public const String PlanogramConsumerDecisionTreeNodeProductPlanogramProductId = "PlanogramProduct_Id";

        #endregion

        #region PlanogramEventLog

        public const String PlanogramEventLogId = "PlanogramEventLog_Id";
        public const String PlanogramEventLogPlanogramId = "Planogram_Id";
        public const String PlanogramEventLogEventType = "PlanogramEventLog_EventType";
        public const String PlanogramEventLogEntryType = "PlanogramEventLog_EntryType";
        public const String PlanogramEventLogAffectedType = "PlanogramEventLog_AffectedType";
        public const String PlanogramEventLogAffectedId = "PlanogramEventLog_AffectedId";
        public const String PlanogramEventLogWorkpackageSource = "PlanogramEventLog_WorkpackageSource";
        public const String PlanogramEventLogTaskSource = "PlanogramEventLog_TaskSource";
        public const String PlanogramEventLogEventId = "PlanogramEventLog_EventId";
        public const String PlanogramEventLogDateTime = "PlanogramEventLog_DateTime";
        public const String PlanogramEventLogDescription = "PlanogramEventLog_Description";
        public const String PlanogramEventLogContent = "PlanogramEventLog_Content";
        public const String PlanogramEventLogScore = "PlanogramEventLog_Score";

        #endregion

        #region PlanogramEventLogInfo

        public const String PlanogramEventLogInfoId = "PlanogramEventLog_Id";
        public const String PlanogramEventLogInfoPlanogramId = "Planogram_Id";
        public const String PlanogramEventLogInfoEventType = "PlanogramEventLog_EventType";
        public const String PlanogramEventLogInfoEntryType = "PlanogramEventLog_EntryType";
        public const String PlanogramEventLogInfoAffectedType = "PlanogramEventLog_AffectedType";
        public const String PlanogramEventLogInfoAffectedId = "PlanogramEventLog_AffectedId";
        public const String PlanogramEventLogInfoWorkpackageSource = "PlanogramEventLog_WorkpackageSource";
        public const String PlanogramEventLogInfoTaskSource = "PlanogramEventLog_TaskSource";
        public const String PlanogramEventLogInfoEventId = "PlanogramEventLog_EventId";
        public const String PlanogramEventLogInfoDateTime = "PlanogramEventLog_DateTime";
        public const String PlanogramEventLogInfoDescription = "PlanogramEventLog_Description";
        public const String PlanogramEventLogInfoContent = "PlanogramEventLog_Content";
        public const String PlanogramEventLogInfoScore = "PlanogramEventLog_Score";
        public const String PlanogramEventLogInfoWorkpackageId = "Workpackage_Id";
        public const String PlanogramEventLogInfoExecutionOrder = "PlanogramEventLogInfo_ExecutionOrder";
        public const String PlanogramEventLogInfoPlanogramName = "Planogram_Name";

        #endregion

        #region PlanogramExportTemplate

        public const String PlanogramExportTemplateId = "PlanogramExportTemplate_Id";
        public const String PlanogramExportTemplateRowVersion = "PlanogramExportTemplate_RowVersion";
        public const String PlanogramExportTemplateEntityId = "Entity_Id";
        public const String PlanogramExportTemplateName = "PlanogramExportTemplate_Name";
        public const String PlanogramExportTemplateFileType = "PlanogramExportTemplate_FileType";
        public const String PlanogramExportTemplateFileVersion = "PlanogramExportTemplate_FileVersion";
        public const String PlanogramExportTemplateDateCreated = "PlanogramExportTemplate_DateCreated";
        public const String PlanogramExportTemplateDateLastModified = "PlanogramExportTemplate_DateLastModified";

        #endregion

        #region PlanogramExportTemplateMapping

        public const String PlanogramExportTemplateMappingId = "PlanogramExportTemplateMapping_Id";
        public const String PlanogramExportTemplateMappingPlanogramExportTemplateId = "PlanogramExportTemplate_Id";
        public const String PlanogramExportTemplateMappingFieldType = "PlanogramExportTemplateMapping_FieldType";
        public const String PlanogramExportTemplateMappingField = "PlanogramExportTemplateMapping_Field";
        public const String PlanogramExportTemplateMappingExternalField = "PlanogramExportTemplateMapping_ExternalField";

        #endregion

        #region PlanogramExportTemplatePerformanceMetric

        public const String PlanogramExportTemplatePerformanceMetricId = "PlanogramExportTemplatePerformanceMetric_Id";
        public const String PlanogramExportTemplatePerformanceMetricPlanogramExportTemplateId = "PlanogramExportTemplate_Id";
        public const String PlanogramExportTemplatePerformanceMetricName = "PlanogramExportTemplatePerformanceMetric_Name";
        public const String PlanogramExportTemplatePerformanceMetricDescription = "PlanogramExportTemplatePerformanceMetric_Description";
        public const String PlanogramExportTemplatePerformanceMetricDirection = "PlanogramExportTemplatePerformanceMetric_Direction";
        public const String PlanogramExportTemplatePerformanceMetricSpecialType = "PlanogramExportTemplatePerformanceMetric_SpecialType";
        public const String PlanogramExportTemplatePerformanceMetricMetricType = "PlanogramExportTemplatePerformanceMetric_MetricType";
        public const String PlanogramExportTemplatePerformanceMetricMetricId = "PlanogramExportTemplatePerformanceMetric_MetricId";
        public const String PlanogramExportTemplatePerformanceMetricExternalField = "PlanogramExportTemplatePerformanceMetric_ExternalField";
        public const String PlanogramExportTemplatePerformanceMetricAggregationType = "PlanogramExportTemplatePerformanceMetric_AggregationType";
        #endregion

        #region PlanogramFixtureAssembly

        public const String PlanogramFixtureAssemblyId = "PlanogramFixtureAssembly_Id";
        public const String PlanogramFixtureAssemblyPlanogramFixtureId = "PlanogramFixture_Id";
        public const String PlanogramFixtureAssemblyPlanogramAssemblyId = "PlanogramAssembly_Id";
        public const String PlanogramFixtureAssemblyX = "PlanogramFixtureAssembly_X";
        public const String PlanogramFixtureAssemblyY = "PlanogramFixtureAssembly_Y";
        public const String PlanogramFixtureAssemblyZ = "PlanogramFixtureAssembly_Z";
        public const String PlanogramFixtureAssemblySlope = "PlanogramFixtureAssembly_Slope";
        public const String PlanogramFixtureAssemblyAngle = "PlanogramFixtureAssembly_Angle";
        public const String PlanogramFixtureAssemblyRoll = "PlanogramFixtureAssembly_Roll";
        public const String PlanogramFixtureAssemblyMetaComponentCount = "PlanogramFixtureAssembly_MetaComponentCount";
        public const String PlanogramFixtureAssemblyMetaTotalMerchandisableLinearSpace = "PlanogramFixtureAssembly_MetaTotalMerchandisableLinearSpace";
        public const String PlanogramFixtureAssemblyMetaTotalMerchandisableAreaSpace = "PlanogramFixtureAssembly_MetaTotalMerchandisableAreaSpace";
        public const String PlanogramFixtureAssemblyMetaTotalMerchandisableVolumetricSpace = "PlanogramFixtureAssembly_MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramFixtureAssemblyMetaTotalLinearWhiteSpace = "PlanogramFixtureAssembly_MetaTotalLinearWhiteSpace";
        public const String PlanogramFixtureAssemblyMetaTotalAreaWhiteSpace = "PlanogramFixtureAssembly_MetaTotalAreaWhiteSpace";
        public const String PlanogramFixtureAssemblyMetaTotalVolumetricWhiteSpace = "PlanogramFixtureAssembly_MetaTotalVolumetricWhiteSpace";
        public const String PlanogramFixtureAssemblyMetaProductsPlaced = "PlanogramFixtureAssembly_MetaProductsPlaced";
        public const String PlanogramFixtureAssemblyMetaNewProducts = "PlanogramFixtureAssembly_MetaNewProducts";
        public const String PlanogramFixtureAssemblyMetaChangesFromPreviousCount = "PlanogramFixtureAssembly_MetaChangesFromPreviousCount";
        public const String PlanogramFixtureAssemblyMetaChangeFromPreviousStarRating = "PlanogramFixtureAssembly_MetaChangeFromPreviousStarRating";
        public const String PlanogramFixtureAssemblyMetaBlocksDropped = "PlanogramFixtureAssembly_MetaBlocksDropped";
        public const String PlanogramFixtureAssemblyMetaNotAchievedInventory = "PlanogramFixtureAssembly_MetaNotAchievedInventory";
        public const String PlanogramFixtureAssemblyMetaTotalFacings = "PlanogramFixtureAssembly_MetaTotalFacings";
        public const String PlanogramFixtureAssemblyMetaAverageFacings = "PlanogramFixtureAssembly_MetaAverageFacings";
        public const String PlanogramFixtureAssemblyMetaTotalUnits = "PlanogramFixtureAssembly_MetaTotalUnits";
        public const String PlanogramFixtureAssemblyMetaAverageUnits = "PlanogramFixtureAssembly_MetaAverageUnits";
        public const String PlanogramFixtureAssemblyMetaMinDos = "PlanogramFixtureAssembly_MetaMinDos";
        public const String PlanogramFixtureAssemblyMetaMaxDos = "PlanogramFixtureAssembly_MetaMaxDos";
        public const String PlanogramFixtureAssemblyMetaAverageDos = "PlanogramFixtureAssembly_MetaAverageDos";
        public const String PlanogramFixtureAssemblyMetaMinCases = "PlanogramFixtureAssembly_MetaMinCases";
        public const String PlanogramFixtureAssemblyMetaAverageCases = "PlanogramFixtureAssembly_MetaAverageCases";
        public const String PlanogramFixtureAssemblyMetaMaxCases = "PlanogramFixtureAssembly_MetaMaxCases";
        public const String PlanogramFixtureAssemblyMetaSpaceToUnitsIndex = "PlanogramFixtureAssembly_MetaSpaceToUnitsIndex";
        public const String PlanogramFixtureAssemblyMetaTotalFrontFacings = "PlanogramFixtureAssembly_MetaTotalFrontFacings";
        public const String PlanogramFixtureAssemblyMetaAverageFrontFacings = "PlanogramFixtureAssembly_MetaAverageFrontFacings";


        #endregion

        #region PlanogramFixtureComponent

        public const String PlanogramFixtureComponentId = "PlanogramFixtureComponent_Id";
        public const String PlanogramFixtureComponentPlanogramFixtureId = "PlanogramFixture_Id";
        public const String PlanogramFixtureComponentPlanogramComponentId = "PlanogramComponent_Id";
        public const String PlanogramFixtureComponentComponentSequenceNumber = "PlanogramFixtureComponent_ComponentSequenceNumber";
        public const String PlanogramFixtureComponentX = "PlanogramFixtureComponent_X";
        public const String PlanogramFixtureComponentY = "PlanogramFixtureComponent_Y";
        public const String PlanogramFixtureComponentZ = "PlanogramFixtureComponent_Z";
        public const String PlanogramFixtureComponentSlope = "PlanogramFixtureComponent_Slope";
        public const String PlanogramFixtureComponentAngle = "PlanogramFixtureComponent_Angle";
        public const String PlanogramFixtureComponentRoll = "PlanogramFixtureComponent_Roll";
        public const String PlanogramFixtureComponentMetaTotalMerchandisableLinearSpace = "PlanogramFixtureComponent_MetaTotalMerchandisableLinearSpace";
        public const String PlanogramFixtureComponentMetaTotalMerchandisableAreaSpace = "PlanogramFixtureComponent_MetaTotalMerchandisableAreaSpace";
        public const String PlanogramFixtureComponentMetaTotalMerchandisableVolumetricSpace = "PlanogramFixtureComponent_MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramFixtureComponentMetaTotalLinearWhiteSpace = "PlanogramFixtureComponent_MetaTotalLinearWhiteSpace";
        public const String PlanogramFixtureComponentMetaTotalAreaWhiteSpace = "PlanogramFixtureComponent_MetaTotalAreaWhiteSpace";
        public const String PlanogramFixtureComponentMetaTotalVolumetricWhiteSpace = "PlanogramFixtureComponent_MetaTotalVolumetricWhiteSpace";
        public const String PlanogramFixtureComponentMetaProductsPlaced = "PlanogramFixtureComponent_MetaProductsPlaced";
        public const String PlanogramFixtureComponentMetaNewProducts = "PlanogramFixtureComponent_MetaNewProducts";
        public const String PlanogramFixtureComponentMetaChangesFromPreviousCount = "PlanogramFixtureComponent_MetaChangesFromPreviousCount";
        public const String PlanogramFixtureComponentMetaChangeFromPreviousStarRating = "PlanogramFixtureComponent_MetaChangeFromPreviousStarRating";
        public const String PlanogramFixtureComponentMetaBlocksDropped = "PlanogramFixtureComponent_MetaBlocksDropped";
        public const String PlanogramFixtureComponentMetaNotAchievedInventory = "PlanogramFixtureComponent_MetaNotAchievedInventory";
        public const String PlanogramFixtureComponentMetaTotalFacings = "PlanogramFixtureComponent_MetaTotalFacings";
        public const String PlanogramFixtureComponentMetaAverageFacings = "PlanogramFixtureComponent_MetaAverageFacings";
        public const String PlanogramFixtureComponentMetaTotalUnits = "PlanogramFixtureComponent_MetaTotalUnits";
        public const String PlanogramFixtureComponentMetaAverageUnits = "PlanogramFixtureComponent_MetaAverageUnits";
        public const String PlanogramFixtureComponentMetaMinDos = "PlanogramFixtureComponent_MetaMinDos";
        public const String PlanogramFixtureComponentMetaMaxDos = "PlanogramFixtureComponent_MetaMaxDos";
        public const String PlanogramFixtureComponentMetaAverageDos = "PlanogramFixtureComponent_MetaAverageDos";
        public const String PlanogramFixtureComponentMetaMinCases = "PlanogramFixtureComponent_MetaMinCases";
        public const String PlanogramFixtureComponentMetaAverageCases = "PlanogramFixtureComponent_MetaAverageCases";
        public const String PlanogramFixtureComponentMetaMaxCases = "PlanogramFixtureComponent_MetaMaxCases";
        public const String PlanogramFixtureComponentMetaSpaceToUnitsIndex = "PlanogramFixtureComponent_MetaSpaceToUnitsIndex";
        public const String PlanogramFixtureComponentMetaIsOverMerchandisedWidth = "PlanogramFixtureComponent_MetaIsOverMerchandisedWidth";
        public const String PlanogramFixtureComponentMetaIsOverMerchandisedHeight = "PlanogramFixtureComponent_MetaIsOverMerchandisedHeight";
        public const String PlanogramFixtureComponentMetaIsOverMerchandisedDepth = "PlanogramFixtureComponent_MetaIsOverMerchandisedDepth";
        public const String PlanogramFixtureComponentMetaTotalComponentCollisions = "PlanogramFixtureComponent_MetaTotalComponentCollisions";
        public const String PlanogramFixtureComponentMetaTotalPositionCollisions = "PlanogramFixtureComponent_MetaTotalPositionCollisions";
        public const String PlanogramFixtureComponentMetaIsComponentSlopeWithNoRiser = "PlanogramFixtureComponent_MetaIsComponentSlopeWithNoRiser";
        public const String PlanogramFixtureComponentMetaTotalFrontFacings = "PlanogramFixtureComponent_MetaTotalFrontFacings";
        public const String PlanogramFixtureComponentMetaAverageFrontFacings = "PlanogramFixtureComponent_MetaAverageFrontFacings";
        public const String PlanogramFixtureComponentMetaPercentageLinearSpaceFilled = "PlanogramFixtureComponent_MetaPercentageLinearSpaceFilled";
        public const String PlanogramFixtureComponentNotchNumber = "PlanogramFixtureComponent_NotchNumber";
        public const String PlanogramFixtureComponentMetaWorldX = "PlanogramFixtureComponent_MetaWorldX";
        public const String PlanogramFixtureComponentMetaWorldY = "PlanogramFixtureComponent_MetaWorldY";
        public const String PlanogramFixtureComponentMetaWorldZ = "PlanogramFixtureComponent_MetaWorldZ";
        public const String PlanogramFixtureComponentMetaWorldAngle = "PlanogramFixtureComponent_MetaWorldAngle";
        public const String PlanogramFixtureComponentMetaWorldSlope = "PlanogramFixtureComponent_MetaWorldSlope";
        public const String PlanogramFixtureComponentMetaWorldRoll = "PlanogramFixtureComponent_MetaWorldRoll";
        public const String PlanogramFixtureComponentMetaIsOutsideOfFixtureArea = "PlanogramFixtureComponent_MetaIsOutsideOfFixtureArea";

        #endregion

        #region PlanogramFixture

        public const String PlanogramFixtureId = "PlanogramFixture_Id";
        public const String PlanogramFixturePlanogramId = "Planogram_Id";
        public const String PlanogramFixtureName = "PlanogramFixture_Name";
        public const String PlanogramFixtureHeight = "PlanogramFixture_Height";
        public const String PlanogramFixtureWidth = "PlanogramFixture_Width";
        public const String PlanogramFixtureDepth = "PlanogramFixture_Depth";
        public const String PlanogramFixtureMetaNumberOfMerchandisedSubComponents = "PlanogramFixture_MetaNumberOfMerchandisedSubComponents";
        public const String PlanogramFixtureMetaTotalFixtureCost = "PlanogramFixture_MetaTotalFixtureCost";

        #endregion

        #region PlanogramFixtureItem

        public const String PlanogramFixtureItemId = "PlanogramFixtureItem_Id";
        public const String PlanogramFixtureItemRowVersion = "PlanogramFixtureItem_RowVersion";
        public const String PlanogramFixtureItemPlanogramId = "Planogram_Id";
        public const String PlanogramFixtureItemPlanogramFixtureId = "PlanogramFixture_Id";
        public const String PlanogramFixtureItemX = "PlanogramFixtureItem_X";
        public const String PlanogramFixtureItemY = "PlanogramFixtureItem_Y";
        public const String PlanogramFixtureItemZ = "PlanogramFixtureItem_Z";
        public const String PlanogramFixtureItemSlope = "PlanogramFixtureItem_Slope";
        public const String PlanogramFixtureItemAngle = "PlanogramFixtureItem_Angle";
        public const String PlanogramFixtureItemRoll = "PlanogramFixtureItem_Roll";
        public const String PlanogramFixtureItemBaySequenceNumber = "PlanogramFixtureItem_BaySequenceNumber";
        public const String PlanogramFixtureItemMetaComponentCount = "PlanogramFixtureItem_MetaComponentCount";
        public const String PlanogramFixtureItemMetaTotalMerchandisableLinearSpace = "PlanogramFixtureItem_MetaTotalMerchandisableLinearSpace";
        public const String PlanogramFixtureItemMetaTotalMerchandisableAreaSpace = "PlanogramFixtureItem_MetaTotalMerchandisableAreaSpace";
        public const String PlanogramFixtureItemMetaTotalMerchandisableVolumetricSpace = "PlanogramFixtureItem_MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramFixtureItemMetaTotalLinearWhiteSpace = "PlanogramFixtureItem_MetaTotalLinearWhiteSpace";
        public const String PlanogramFixtureItemMetaTotalAreaWhiteSpace = "PlanogramFixtureItem_MetaTotalAreaWhiteSpace";
        public const String PlanogramFixtureItemMetaTotalVolumetricWhiteSpace = "PlanogramFixtureItem_MetaTotalVolumetricWhiteSpace";
        public const String PlanogramFixtureItemMetaProductsPlaced = "PlanogramFixtureItem_MetaProductsPlaced";
        public const String PlanogramFixtureItemMetaNewProducts = "PlanogramFixtureItem_MetaNewProducts";
        public const String PlanogramFixtureItemMetaChangesFromPreviousCount = "PlanogramFixtureItem_MetaChangesFromPreviousCount";
        public const String PlanogramFixtureItemMetaChangeFromPreviousStarRating = "PlanogramFixtureItem_MetaChangeFromPreviousStarRating";
        public const String PlanogramFixtureItemMetaBlocksDropped = "PlanogramFixtureItem_MetaBlocksDropped";
        public const String PlanogramFixtureItemMetaNotAchievedInventory = "PlanogramFixtureItem_MetaNotAchievedInventory";
        public const String PlanogramFixtureItemMetaTotalFacings = "PlanogramFixtureItem_MetaTotalFacings";
        public const String PlanogramFixtureItemMetaAverageFacings = "PlanogramFixtureItem_MetaAverageFacings";
        public const String PlanogramFixtureItemMetaTotalUnits = "PlanogramFixtureItem_MetaTotalUnits";
        public const String PlanogramFixtureItemMetaAverageUnits = "PlanogramFixtureItem_MetaAverageUnits";
        public const String PlanogramFixtureItemMetaMinDos = "PlanogramFixtureItem_MetaMinDos";
        public const String PlanogramFixtureItemMetaMaxDos = "PlanogramFixtureItem_MetaMaxDos";
        public const String PlanogramFixtureItemMetaAverageDos = "PlanogramFixtureItem_MetaAverageDos";
        public const String PlanogramFixtureItemMetaMinCases = "PlanogramFixtureItem_MetaMinCases";
        public const String PlanogramFixtureItemMetaAverageCases = "PlanogramFixtureItem_MetaAverageCases";
        public const String PlanogramFixtureItemMetaMaxCases = "PlanogramFixtureItem_MetaMaxCases";
        public const String PlanogramFixtureItemMetaSpaceToUnitsIndex = "PlanogramFixtureItem_MetaSpaceToUnitsIndex";
        public const String PlanogramFixtureItemMetaTotalComponentsOverMerchandisedWidth = "PlanogramFixtureItem_MetaTotalComponentsOverMerchandisedWidth";
        public const String PlanogramFixtureItemMetaTotalComponentsOverMerchandisedHeight = "PlanogramFixtureItem_MetaTotalComponentsOverMerchandisedHeight";
        public const String PlanogramFixtureItemMetaTotalComponentsOverMerchandisedDepth = "PlanogramFixtureItem_MetaTotalComponentsOverMerchandisedDepth";
        public const String PlanogramFixtureItemMetaTotalComponentCollisions = "PlanogramFixtureItem_MetaTotalComponentCollisions";
        public const String PlanogramFixtureItemMetaTotalPositionCollisions = "PlanogramFixtureItem_MetaTotalPositionCollisions";
        public const String PlanogramFixtureItemMetaTotalFrontFacings = "PlanogramFixtureItem_MetaTotalFrontFacings";
        public const String PlanogramFixtureItemMetaAverageFrontFacings = "PlanogramFixtureItem_MetaAverageFrontFacings";

        #endregion

        #region PlanogramGroup

        public const String PlanogramGroupId = "PlanogramGroup_Id";
        public const String PlanogramGroupPlanogramHierarchyId = "PlanogramHierarchy_Id";
        public const String PlanogramGroupName = "PlanogramGroup_Name";
        public const String PlanogramGroupParentGroupId = "PlanogramGroup_ParentGroupId";
        public const String PlanogramGroupDateCreated = "PlanogramGroup_DateCreated";
        public const String PlanogramGroupDateLastModified = "PlanogramGroup_DateLastModified";
        public const String PlanogramGroupDateDeleted = "PlanogramGroup_DateDeleted";
        public const String PlanogramGroupSetProperties = "PlanogramGroup_SetProperties";
        public const String PlanogramGroupCode = "PlanogramGroup_Code";

        #endregion

        #region PlanogramHierarchy

        public const String PlanogramHierarchyId = "PlanogramHierarchy_Id";
        public const String PlanogramHierarchyRowVersion = "PlanogramHierarchy_RowVersion";
        public const String PlanogramHierarchyEntityId = "Entity_Id";
        public const String PlanogramHierarchyName = "PlanogramHierarchy_Name";
        public const String PlanogramHierarchyDateCreated = "PlanogramHierarchy_DateCreated";
        public const String PlanogramHierarchyDateLastModified = "PlanogramHierarchy_DateLastModified";
        public const String PlanogramHierarchyDateDeleted = "PlanogramHierarchy_DateDeleted";
        public const String PlanogramHierarchySetProperties = "PlanogramHierarchy_SetProperties";

        #endregion

        #region PlanogramImage

        public const String PlanogramImageId = "PlanogramImage_Id";
        public const String PlanogramImagePlanogramId = "Planogram_Id";
        public const String PlanogramImageFileName = "PlanogramImage_FileName";
        public const String PlanogramImageDescription = "PlanogramImage_Description";
        public const String PlanogramImageImageData = "PlanogramImage_ImageData";

        #endregion

        #region PlanogramImportTemplate

        public const String PlanogramImportTemplateId = "PlanogramImportTemplate_Id";
        public const String PlanogramImportTemplateRowVersion = "PlanogramImportTemplate_RowVersion";
        public const String PlanogramImportTemplateEntityId = "Entity_Id";
        public const String PlanogramImportTemplateName = "PlanogramImportTemplate_Name";
        public const String PlanogramImportTemplateFileType = "PlanogramImportTemplate_FileType";
        public const String PlanogramImportTemplateFileVersion = "PlanogramImportTemplate_FileVersion";
        public const String PlanogramImportTemplateDateCreated = "PlanogramImportTemplate_DateCreated";
        public const String PlanogramImportTemplateDateLastModified = "PlanogramImportTemplate_DateLastModified";
        public const String PlanogramImportTemplateAllowAdvancedBaySplitting = "PlanogramImportTemplate_AllowAdvancedBaySplitting";
        public const String PlanogramImportTemplateCanSplitComponents = "PlanogramImportTemplate_CanSplitComponents";
        public const String PlanogramImportTemplateSplitBaysByComponentStart = "PlanogramImportTemplate_SplitBaysByComponentStart";
        public const String PlanogramImportTemplateSplitBaysByComponentEnd = "PlanogramImportTemplate_SplitBaysByComponentEnd";
        public const String PlanogramImportTemplateSplitByFixedSize = "PlanogramImportTemplate_SplitByFixedSize";
        public const String PlanogramImportTemplateSplitByRecurringPattern = "PlanogramImportTemplate_SplitByRecurringPattern";
        public const String PlanogramImportTemplateSplitByBayCount = "PlanogramImportTemplate_SplitByBayCount";
        public const String PlanogramImportTemplateSplitFixedSize = "PlanogramImportTemplate_SplitFixedSize";
        public const String PlanogramImportTemplateSplitRecurringPattern = "PlanogramImportTemplate_SplitRecurringPattern";
        public const String PlanogramImportTemplateSplitBayCount = "PlanogramImportTemplate_SplitBayCount";

        #endregion

        #region PlanogramImportTemplateMapping

        public const String PlanogramImportTemplateMappingId = "PlanogramImportTemplateMapping_Id";
        public const String PlanogramImportTemplateMappingPlanogramImportTemplateId = "PlanogramImportTemplate_Id";
        public const String PlanogramImportTemplateMappingFieldType = "PlanogramImportTemplateMapping_FieldType";
        public const String PlanogramImportTemplateMappingField = "PlanogramImportTemplateMapping_Field";
        public const String PlanogramImportTemplateMappingExternalField = "PlanogramImportTemplateMapping_ExternalField";

        #endregion

        #region PlanogramImportTemplatePerformanceMetric

        public const String PlanogramImportTemplatePerformanceMetricId = "PlanogramImportTemplatePerformanceMetric_Id";
        public const String PlanogramImportTemplatePerformanceMetricPlanogramImportTemplateId = "PlanogramImportTemplate_Id";
        public const String PlanogramImportTemplatePerformanceMetricName = "PlanogramImportTemplatePerformanceMetric_Name";
        public const String PlanogramImportTemplatePerformanceMetricDescription = "PlanogramImportTemplatePerformanceMetric_Description";
        public const String PlanogramImportTemplatePerformanceMetricDirection = "PlanogramImportTemplatePerformanceMetric_Direction";
        public const String PlanogramImportTemplatePerformanceMetricSpecialType = "PlanogramImportTemplatePerformanceMetric_SpecialType";
        public const String PlanogramImportTemplatePerformanceMetricMetricType = "PlanogramImportTemplatePerformanceMetric_MetricType";
        public const String PlanogramImportTemplatePerformanceMetricMetricId = "PlanogramImportTemplatePerformanceMetric_MetricId";
        public const String PlanogramImportTemplatePerformanceMetricExternalField = "PlanogramImportTemplatePerformanceMetric_ExternalField";
        public const String PlanogramImportTemplatePerformanceMetricAggregationType = "PlanogramImportTemplatePerformanceMetric_AggregationType";
        #endregion

        #region PlanogramInventory

        public const String PlanogramInventoryId = "PlanogramInventory_Id";
        public const String PlanogramInventoryPlanogramId = "Planogram_Id";
        public const String PlanogramInventoryDaysOfPerformance = "PlanogramInventory_DaysOfPerformance";
        public const String PlanogramInventoryMinCasePacks = "PlanogramInventory_MinCasePacks";
        public const String PlanogramInventoryMinDos = "PlanogramInventory_MinDos";
        public const String PlanogramInventoryMinShelfLife = "PlanogramInventory_MinShelfLife";
        public const String PlanogramInventoryMinDeliveries = "PlanogramInventory_MinDeliveries";
        public const String PlanogramInventoryIsCasePacksValidated = "PlanogramInventory_IsCasePacksValidated";
        public const String PlanogramInventoryIsDosValidated = "PlanogramInventory_IsDosValidated";
        public const String PlanogramInventoryIsShelfLifeValidated = "PlanogramInventory_IsShelfLifeValidated";
        public const String PlanogramInventoryIsDeliveriesValidated = "PlanogramInventory_IsDeliveriesValidated";
        public const String PlanogramInventoryMetricType = "PlanogramInventory_InventoryMetricType";

        #endregion

        #region PlanogramMetadataImage

        public const String PlanogramMetadataImageId = "PlanogramMetadataImage_Id";
        public const String PlanogramMetadataImagePlanogramId = "Planogram_Id";
        public const String PlanogramMetadataImageFileName = "PlanogramMetadataImage_FileName";
        public const String PlanogramMetadataImageDescription = "PlanogramMetadataImage_Description";
        public const String PlanogramMetadataImageImageData = "PlanogramMetadataImage_ImageData";

        #endregion

        #region PlanogramNameTemplate

        public const String PlanogramNameTemplateId = "PlanogramNameTemplate_Id";
        public const String PlanogramNameTemplateRowVersion = "PlanogramNameTemplate_RowVersion";
        public const String PlanogramNameTemplateName = "PlanogramNameTemplate_Name";
        public const String PlanogramNameTemplatePlanogramAttribute = "PlanogramNameTemplate_PlanogramAttribute";
        public const String PlanogramNameTemplateBuilderFormula = "PlanogramNameTemplate_BuilderFormula";
        public const String PlanogramNameTemplateDateCreated = "PlanogramNameTemplate_DateCreated";
        public const String PlanogramNameTemplateDateLastModified = "PlanogramNameTemplate_DateLastModified";

        #endregion

        #region PlanogramInfo

        public const String PlanogramInfoId = "Planogram_Id";
        public const String PlanogramInfoRowVersion = "Planogram_RowVersion";
        public const String PlanogramInfoName = "Planogram_Name";
        public const String PlanogramInfoUserName = "Planogram_UserName";
        public const String PlanogramInfoStatus = "Planogram_Status";
        public const String PlanogramInfoPlanogramGroupId = "PlanogramGroup_Id";
        public const String PlanogramInfoWorkpackageId = "Workpackage_Id";
        public const String PlanogramInfoEntityId = "Entity_Id";
        public const String PlanogramInfoHeight = "Planogram_Height";
        public const String PlanogramInfoWidth = "Planogram_Width";
        public const String PlanogramInfoDepth = "Planogram_Depth";
        public const String PlanogramInfoDateCreated = "Planogram_DateCreated";
        public const String PlanogramInfoDateLastModified = "Planogram_DateLastModified";
        public const String PlanogramInfoDateDeleted = "Planogram_DateDeleted";
        public const String PlanogramInfoCategoryCode = "Planogram_CategoryCode";
        public const String PlanogramInfoCategoryName = "Planogram_CategoryName";
        public const String PlanogramInfoMetaBayCount = "Planogram_MetaBayCount";
        public const String PlanogramInfoMetaUniqueProductCount = "Planogram_MetaUniqueProductCount";
        public const String PlanogramInfoMetaTotalPositionCount = "Planogram_MetaTotalPositionCount";
        public const String PlanogramInfoMetaComponentCount = "Planogram_MetaComponentCount";
        public const String PlanogramInfoMetaTotalMerchandisableLinearSpace = "Planogram_MetaTotalMerchandisableLinearSpace";
        public const String PlanogramInfoMetaTotalMerchandisableAreaSpace = "Planogram_MetaTotalMerchandisableAreaSpace";
        public const String PlanogramInfoMetaTotalMerchandisableVolumetricSpace = "Planogram_MetaTotalMerchandisableVolumetricSpace";
        public const String PlanogramInfoMetaTotalLinearWhiteSpace = "Planogram_MetaTotalLinearWhiteSpace";
        public const String PlanogramInfoMetaTotalAreaWhiteSpace = "Planogram_MetaTotalAreaWhiteSpace";
        public const String PlanogramInfoMetaTotalVolumetricWhiteSpace = "Planogram_MetaTotalVolumetricWhiteSpace";
        public const String PlanogramInfoMetaProductsPlaced = "Planogram_MetaProductsPlaced";
        public const String PlanogramInfoMetaProductsUnplaced = "Planogram_MetaProductsUnplaced";
        public const String PlanogramInfoMetaNewProducts = "Planogram_MetaNewProducts";
        public const String PlanogramInfoMetaChangesFromPreviousCount = "Planogram_MetaChangesFromPreviousCount";
        public const String PlanogramInfoMetaChangeFromPreviousStarRating = "Planogram_MetaChangeFromPreviousStarRating";
        public const String PlanogramInfoMetaBlocksDropped = "Planogram_MetaBlocksDropped";
        public const String PlanogramInfoMetaNotAchievedInventory = "Planogram_MetaNotAchievedInventory";
        public const String PlanogramInfoMetaTotalFacings = "Planogram_MetaTotalFacings";
        public const String PlanogramInfoMetaAverageFacings = "Planogram_MetaAverageFacings";
        public const String PlanogramInfoMetaTotalUnits = "Planogram_MetaTotalUnits";
        public const String PlanogramInfoMetaAverageUnits = "Planogram_MetaAverageUnits";
        public const String PlanogramInfoMetaMinDos = "Planogram_MetaMinDos";
        public const String PlanogramInfoMetaMaxDos = "Planogram_MetaMaxDos";
        public const String PlanogramInfoMetaAverageDos = "Planogram_MetaAverageDos";
        public const String PlanogramInfoMetaMinCases = "Planogram_MetaMinCases";
        public const String PlanogramInfoMetaAverageCases = "Planogram_MetaAverageCases";
        public const String PlanogramInfoMetaMaxCases = "Planogram_MetaMaxCases";
        public const String PlanogramInfoMetaSpaceToUnitsIndex = "Planogram_MetaSpaceToUnitsIndex";
        public const String PlanogramInfoPlanogramGroupName = "PlanogramGroup_Name";
        public const String PlanogramInfoMetaTotalComponentsOverMerchandisedWidth = "Planogram_MetaTotalComponentsOverMerchandisedWidth";
        public const String PlanogramInfoMetaTotalComponentsOverMerchandisedHeight = "Planogram_MetaTotalComponentsOverMerchandisedHeight";
        public const String PlanogramInfoMetaTotalComponentsOverMerchandisedDepth = "Planogram_MetaTotalComponentsOverMerchandisedDepth";
        public const String PlanogramInfoMetaTotalComponentCollisions = "Planogram_MetaTotalComponentCollisions";
        public const String PlanogramInfoMetaTotalPositionCollisions = "Planogram_MetaTotalPositionCollisions";
        public const String PlanogramInfoMetaTotalFrontFacings = "Planogram_MetaTotalFrontFacings";
        public const String PlanogramInfoMetaAverageFrontFacings = "Planogram_MetaAverageFrontFacings";
        public const String PlanogramInfoDateMetadataCalculated = "Planogram_DateMetadataCalculated";
        public const String PlanogramInfoDateValidationDataCalculated = "Planogram_DateValidationDataCalculated";
        public const String PlanogramInfoIsLocked = "PlanogramLock_IsLocked";
        public const String PlanogramInfoLockType = "PlanogramLock_Type";
        public const String PlanogramInfoLockUserId = "User_Id";
        public const String PlanogramInfoLockUserName = "User_UserName";
        public const String PlanogramInfoLockFullName = "User_FullName";
        public const String PlanogramInfoLockDate = "PlanogramLock_DateLocked";
        public const String PlanogramInfoAutomationProcessingStatus = "WorkpackagePlanogramProcessingStatus_AutomationStatus";
        public const String PlanogramInfoAutomationProcessingStatusDescription = "WorkpackagePlanogramProcessingStatus_AutomationStatusDescription";
        public const String PlanogramInfoAutomationProgressMax = "WorkpackagePlanogramProcessingStatus_AutomationProgressMax";
        public const String PlanogramInfoAutomationProgressCurrent = "WorkpackagePlanogramProcessingStatus_AutomationProgressCurrent";
        public const String PlanogramInfoAutomationDateStarted = "WorkpackagePlanogramProcessingStatus_AutomationDateStarted";
        public const String PlanogramInfoAutomationDateUpdated = "WorkpackagePlanogramProcessingStatus_AutomationDateUpdated";
        public const String PlanogramInfoMetaDataProcessingStatus = "PlanogramProcessingStatus_MetaDataStatus";
        public const String PlanogramInfoMetaDataProcessingStatusDescription = "PlanogramProcessingStatus_MetaDataStatusDescription";
        public const String PlanogramInfoMetaDataProgressMax = "PlanogramProcessingStatus_MetaDataProgressMax";
        public const String PlanogramInfoMetaDataProgressCurrent = "PlanogramProcessingStatus_MetaDataProgressCurrent";
        public const String PlanogramInfoMetaDataDateStarted = "PlanogramProcessingStatus_MetaDataDateStarted";
        public const String PlanogramInfoMetaDataDateUpdated = "PlanogramProcessingStatus_MetaDataDateUpdated";
        public const String PlanogramInfoValidationProcessingStatus = "PlanogramProcessingStatus_ValidationStatus";
        public const String PlanogramInfoValidationProcessingStatusDescription = "PlanogramProcessingStatus_ValidationStatusDescription";
        public const String PlanogramInfoValidationProgressMax = "PlanogramProcessingStatus_ValidationProgressMax";
        public const String PlanogramInfoValidationProgressCurrent = "PlanogramProcessingStatus_ValidationProgressCurrent";
        public const String PlanogramInfoValidationDateStarted = "PlanogramProcessingStatus_ValidationDateStarted";
        public const String PlanogramInfoValidationDateUpdated = "PlanogramProcessingStatus_ValidationDateUpdated";
        public const String PlanogramInfoValidationIsOutOfDate = "PlanogramInfo_ValidationIsOutOfDate";
        public const String PlanogramInfoEntityID = "Entity_Id";
        public const String PlanogramInfoClusterSchemeName = "Planogram_ClusterSchemeName";
        public const String PlanogramInfoClusterName = "Planogram_ClusterName";
        public const String PlanogramInfoLocationCode = "Planogram_LocationCode";
        public const String PlanogramInfoLocationName = "Planogram_LocationName";
        public const String PlanogramInfoMetaNoOfErrors = "Planogram_MetaNoOfErrors";
        public const String PlanogramInfoMetaNoOfWarnings = "Planogram_MetaNoOfWarnings";
        public const String PlanogramInfoMetaTotalErrorScore = "Planogram_MetaTotalErrorScore";
        public const String PlanogramInfoMetaHighestErrorScore = "Planogram_MetaHighestErrorScore";
        public const String PlanogramInfoMetaNoOfDebugEvents = "Planogram_MetaNoOfDebugEvents";
        public const String PlanogramInfoMetaNoOfInformationEvents = "Planogram_MetaNoOfInformationEvents";
        public const String PlanogramInfoMostRecentLinkedWorkpackageName = "PlanogramInfo_MostRecentLinkedWorkpackageName";
        public const String PlanogramInfoAssignedLocationCount = "PlanogramInfo_AssignedLocationCount";

        #endregion

        #region PlanogramLock

        public const String PlanogramLockPlanogramId = "Planogram_Id";
        public const String PlanogramLockType = "PlanogramLock_Type";
        public const String PlanogramLockComputerName = "PlanogramLock_ComputerName";
        public const String PlanogramLockDateLocked = "PlanogramLock_DateLocked";
        public const String PlanogramLockUserId = "User_Id";

        #endregion

        #region PlanogramPerformance

        public const String PlanogramPerformanceId = "PlanogramPerformance_Id";
        public const String PlanogramPerformancePlanogramId = "Planogram_Id";
        public const String PlanogramPerformanceName = "PlanogramPerformance_Name";
        public const String PlanogramPerformanceDescription = "PlanogramPerformance_Description";

        #endregion

        #region PlanogramPerformanceData

        public const String PlanogramPerformanceDataId = "PlanogramPerformanceData_Id";
        public const String PlanogramPerformanceDataPlanogramPerformanceId = "PlanogramPerformance_Id";
        public const String PlanogramPerformanceDataPlanogramProductId = "PlanogramProduct_Id";
        public const String PlanogramPerformanceDataP1 = "PlanogramPerformanceData_P1";
        public const String PlanogramPerformanceDataP2 = "PlanogramPerformanceData_P2";
        public const String PlanogramPerformanceDataP3 = "PlanogramPerformanceData_P3";
        public const String PlanogramPerformanceDataP4 = "PlanogramPerformanceData_P4";
        public const String PlanogramPerformanceDataP5 = "PlanogramPerformanceData_P5";
        public const String PlanogramPerformanceDataP6 = "PlanogramPerformanceData_P6";
        public const String PlanogramPerformanceDataP7 = "PlanogramPerformanceData_P7";
        public const String PlanogramPerformanceDataP8 = "PlanogramPerformanceData_P8";
        public const String PlanogramPerformanceDataP9 = "PlanogramPerformanceData_P9";
        public const String PlanogramPerformanceDataP10 = "PlanogramPerformanceData_P10";
        public const String PlanogramPerformanceDataP11 = "PlanogramPerformanceData_P11";
        public const String PlanogramPerformanceDataP12 = "PlanogramPerformanceData_P12";
        public const String PlanogramPerformanceDataP13 = "PlanogramPerformanceData_P13";
        public const String PlanogramPerformanceDataP14 = "PlanogramPerformanceData_P14";
        public const String PlanogramPerformanceDataP15 = "PlanogramPerformanceData_P15";
        public const String PlanogramPerformanceDataP16 = "PlanogramPerformanceData_P16";
        public const String PlanogramPerformanceDataP17 = "PlanogramPerformanceData_P17";
        public const String PlanogramPerformanceDataP18 = "PlanogramPerformanceData_P18";
        public const String PlanogramPerformanceDataP19 = "PlanogramPerformanceData_P19";
        public const String PlanogramPerformanceDataP20 = "PlanogramPerformanceData_P20";
        public const String PlanogramPerformanceDataCP1 = "PlanogramPerformanceData_CP1";
        public const String PlanogramPerformanceDataCP2 = "PlanogramPerformanceData_CP2";
        public const String PlanogramPerformanceDataCP3 = "PlanogramPerformanceData_CP3";
        public const String PlanogramPerformanceDataCP4 = "PlanogramPerformanceData_CP4";
        public const String PlanogramPerformanceDataCP5 = "PlanogramPerformanceData_CP5";
        public const String PlanogramPerformanceDataCP6 = "PlanogramPerformanceData_CP6";
        public const String PlanogramPerformanceDataCP7 = "PlanogramPerformanceData_CP7";
        public const String PlanogramPerformanceDataCP8 = "PlanogramPerformanceData_CP8";
        public const String PlanogramPerformanceDataCP9 = "PlanogramPerformanceData_CP9";
        public const String PlanogramPerformanceDataCP10 = "PlanogramPerformanceData_CP10";
        public const String PlanogramPerformanceDataUnitsSoldPerDay = "PlanogramPerformanceData_UnitsSoldPerDay";
        public const String PlanogramPerformanceDataAchievedCasePacks = "PlanogramPerformanceData_AchievedCasePacks";
        public const String PlanogramPerformanceDataAchievedDos = "PlanogramPerformanceData_AchievedDos";
        public const String PlanogramPerformanceDataAchievedShelfLife = "PlanogramPerformanceData_AchievedShelfLife";
        public const String PlanogramPerformanceDataAchievedDeliveries = "PlanogramPerformanceData_AchievedDeliveries";
        public const String PlanogramPerformanceDataMinimumInventoryUnits = "PlanogramPerformanceData_MinimumInventoryUnits";
        public const String PlanogramPerformanceDataMetaP1Percentage = "PlanogramPerformanceData_MetaP1Percentage";
        public const String PlanogramPerformanceDataMetaP2Percentage = "PlanogramPerformanceData_MetaP2Percentage";
        public const String PlanogramPerformanceDataMetaP3Percentage = "PlanogramPerformanceData_MetaP3Percentage";
        public const String PlanogramPerformanceDataMetaP4Percentage = "PlanogramPerformanceData_MetaP4Percentage";
        public const String PlanogramPerformanceDataMetaP5Percentage = "PlanogramPerformanceData_MetaP5Percentage";
        public const String PlanogramPerformanceDataMetaP6Percentage = "PlanogramPerformanceData_MetaP6Percentage";
        public const String PlanogramPerformanceDataMetaP7Percentage = "PlanogramPerformanceData_MetaP7Percentage";
        public const String PlanogramPerformanceDataMetaP8Percentage = "PlanogramPerformanceData_MetaP8Percentage";
        public const String PlanogramPerformanceDataMetaP9Percentage = "PlanogramPerformanceData_MetaP9Percentage";
        public const String PlanogramPerformanceDataMetaP10Percentage = "PlanogramPerformanceData_MetaP10Percentage";
        public const String PlanogramPerformanceDataMetaP11Percentage = "PlanogramPerformanceData_MetaP11Percentage";
        public const String PlanogramPerformanceDataMetaP12Percentage = "PlanogramPerformanceData_MetaP12Percentage";
        public const String PlanogramPerformanceDataMetaP13Percentage = "PlanogramPerformanceData_MetaP13Percentage";
        public const String PlanogramPerformanceDataMetaP14Percentage = "PlanogramPerformanceData_MetaP14Percentage";
        public const String PlanogramPerformanceDataMetaP15Percentage = "PlanogramPerformanceData_MetaP15Percentage";
        public const String PlanogramPerformanceDataMetaP16Percentage = "PlanogramPerformanceData_MetaP16Percentage";
        public const String PlanogramPerformanceDataMetaP17Percentage = "PlanogramPerformanceData_MetaP17Percentage";
        public const String PlanogramPerformanceDataMetaP18Percentage = "PlanogramPerformanceData_MetaP18Percentage";
        public const String PlanogramPerformanceDataMetaP19Percentage = "PlanogramPerformanceData_MetaP19Percentage";
        public const String PlanogramPerformanceDataMetaP20Percentage = "PlanogramPerformanceData_MetaP20Percentage";
        public const String PlanogramPerformanceDataMetaCP1Percentage = "PlanogramPerformanceData_MetaCP1Percentage";
        public const String PlanogramPerformanceDataMetaCP2Percentage = "PlanogramPerformanceData_MetaCP2Percentage";
        public const String PlanogramPerformanceDataMetaCP3Percentage = "PlanogramPerformanceData_MetaCP3Percentage";
        public const String PlanogramPerformanceDataMetaCP4Percentage = "PlanogramPerformanceData_MetaCP4Percentage";
        public const String PlanogramPerformanceDataMetaCP5Percentage = "PlanogramPerformanceData_MetaCP5Percentage";
        public const String PlanogramPerformanceDataMetaCP6Percentage = "PlanogramPerformanceData_MetaCP6Percentage";
        public const String PlanogramPerformanceDataMetaCP7Percentage = "PlanogramPerformanceData_MetaCP7Percentage";
        public const String PlanogramPerformanceDataMetaCP8Percentage = "PlanogramPerformanceData_MetaCP8Percentage";
        public const String PlanogramPerformanceDataMetaCP9Percentage = "PlanogramPerformanceData_MetaCP9Percentage";
        public const String PlanogramPerformanceDataMetaCP10Percentage = "PlanogramPerformanceData_MetaCP10Percentage";
        public const String PlanogramPerformanceDataMetaP1Rank = "PlanogramPerformanceData_MetaP1Rank";
        public const String PlanogramPerformanceDataMetaP2Rank = "PlanogramPerformanceData_MetaP2Rank";
        public const String PlanogramPerformanceDataMetaP3Rank = "PlanogramPerformanceData_MetaP3Rank";
        public const String PlanogramPerformanceDataMetaP4Rank = "PlanogramPerformanceData_MetaP4Rank";
        public const String PlanogramPerformanceDataMetaP5Rank = "PlanogramPerformanceData_MetaP5Rank";
        public const String PlanogramPerformanceDataMetaP6Rank = "PlanogramPerformanceData_MetaP6Rank";
        public const String PlanogramPerformanceDataMetaP7Rank = "PlanogramPerformanceData_MetaP7Rank";
        public const String PlanogramPerformanceDataMetaP8Rank = "PlanogramPerformanceData_MetaP8Rank";
        public const String PlanogramPerformanceDataMetaP9Rank = "PlanogramPerformanceData_MetaP9Rank";
        public const String PlanogramPerformanceDataMetaP10Rank = "PlanogramPerformanceData_MetaP10Rank";
        public const String PlanogramPerformanceDataMetaP11Rank = "PlanogramPerformanceData_MetaP11Rank";
        public const String PlanogramPerformanceDataMetaP12Rank = "PlanogramPerformanceData_MetaP12Rank";
        public const String PlanogramPerformanceDataMetaP13Rank = "PlanogramPerformanceData_MetaP13Rank";
        public const String PlanogramPerformanceDataMetaP14Rank = "PlanogramPerformanceData_MetaP14Rank";
        public const String PlanogramPerformanceDataMetaP15Rank = "PlanogramPerformanceData_MetaP15Rank";
        public const String PlanogramPerformanceDataMetaP16Rank = "PlanogramPerformanceData_MetaP16Rank";
        public const String PlanogramPerformanceDataMetaP17Rank = "PlanogramPerformanceData_MetaP17Rank";
        public const String PlanogramPerformanceDataMetaP18Rank = "PlanogramPerformanceData_MetaP18Rank";
        public const String PlanogramPerformanceDataMetaP19Rank = "PlanogramPerformanceData_MetaP19Rank";
        public const String PlanogramPerformanceDataMetaP20Rank = "PlanogramPerformanceData_MetaP20Rank";
        public const String PlanogramPerformanceDataMetaCP1Rank = "PlanogramPerformanceData_MetaCP1Rank";
        public const String PlanogramPerformanceDataMetaCP2Rank = "PlanogramPerformanceData_MetaCP2Rank";
        public const String PlanogramPerformanceDataMetaCP3Rank = "PlanogramPerformanceData_MetaCP3Rank";
        public const String PlanogramPerformanceDataMetaCP4Rank = "PlanogramPerformanceData_MetaCP4Rank";
        public const String PlanogramPerformanceDataMetaCP5Rank = "PlanogramPerformanceData_MetaCP5Rank";
        public const String PlanogramPerformanceDataMetaCP6Rank = "PlanogramPerformanceData_MetaCP6Rank";
        public const String PlanogramPerformanceDataMetaCP7Rank = "PlanogramPerformanceData_MetaCP7Rank";
        public const String PlanogramPerformanceDataMetaCP8Rank = "PlanogramPerformanceData_MetaCP8Rank";
        public const String PlanogramPerformanceDataMetaCP9Rank = "PlanogramPerformanceData_MetaCP9Rank";
        public const String PlanogramPerformanceDataMetaCP10Rank = "PlanogramPerformanceData_MetaCP10Rank";

        #endregion

        #region PlanogramPerformanceMetric

        public const String PlanogramPerformanceMetricId = "PlanogramPerformanceMetric_Id";
        public const String PlanogramPerformanceMetricPlanogramPerformanceId = "PlanogramPerformance_Id";
        public const String PlanogramPerformanceMetricName = "PlanogramPerformanceMetric_Name";
        public const String PlanogramPerformanceMetricDescription = "PlanogramPerformanceMetric_Description";
        public const String PlanogramPerformanceMetricDirection = "PlanogramPerformanceMetric_Direction";
        public const String PlanogramPerformanceMetricSpecialType = "PlanogramPerformanceMetric_SpecialType";
        public const String PlanogramPerformanceMetricMetricType = "PlanogramPerformanceMetric_MetricType";
        public const String PlanogramPerformanceMetricMetricId = "PlanogramPerformanceMetric_MetricId";
        public const String PlanogramPerformanceMetricAggregationType = "PlanogramPerformanceMetric_AggregationType";

        #endregion

        #region PlanogramPosition

        public const String PlanogramPositionId = "PlanogramPosition_Id";
        public const String PlanogramPositionPlanogramId = "Planogram_Id";
        public const String PlanogramPositionPlanogramFixtureItemId = "PlanogramFixtureItem_Id";
        public const String PlanogramPositionPlanogramFixtureAssemblyId = "PlanogramFixtureAssembly_Id";
        public const String PlanogramPositionPlanogramAssemblyComponentId = "PlanogramAssemblyComponent_Id";
        public const String PlanogramPositionPlanogramFixtureComponentId = "PlanogramFixtureComponent_Id";
        public const String PlanogramPositionPlanogramSubComponentId = "PlanogramSubComponent_Id";
        public const String PlanogramPositionPlanogramProductId = "PlanogramProduct_Id";
        public const String PlanogramPositionPositionSequenceNumber = "PlanogramPosition_PositionSequenceNumber";
        public const String PlanogramPositionX = "PlanogramPosition_X";
        public const String PlanogramPositionY = "PlanogramPosition_Y";
        public const String PlanogramPositionZ = "PlanogramPosition_Z";
        public const String PlanogramPositionSlope = "PlanogramPosition_Slope";
        public const String PlanogramPositionAngle = "PlanogramPosition_Angle";
        public const String PlanogramPositionRoll = "PlanogramPosition_Roll";
        public const String PlanogramPositionFacingsHigh = "PlanogramPosition_FacingsHigh";
        public const String PlanogramPositionFacingsWide = "PlanogramPosition_FacingsWide";
        public const String PlanogramPositionFacingsDeep = "PlanogramPosition_FacingsDeep";
        public const String PlanogramPositionOrientationType = "PlanogramPosition_OrientationType";
        public const String PlanogramPositionMerchandisingStyle = "PlanogramPosition_MerchandisingStyle";
        public const String PlanogramPositionFacingsXHigh = "PlanogramPosition_FacingsXHigh";
        public const String PlanogramPositionFacingsXWide = "PlanogramPosition_FacingsXWide";
        public const String PlanogramPositionFacingsXDeep = "PlanogramPosition_FacingsXDeep";
        public const String PlanogramPositionMerchandisingStyleX = "PlanogramPosition_MerchandisingStyleX";
        public const String PlanogramPositionOrientationTypeX = "PlanogramPosition_OrientationTypeX";
        public const String PlanogramPositionIsXPlacedLeft = "PlanogramPosition_IsXPlacedLeft";
        public const String PlanogramPositionFacingsYHigh = "PlanogramPosition_FacingsYHigh";
        public const String PlanogramPositionFacingsYWide = "PlanogramPosition_FacingsYWide";
        public const String PlanogramPositionFacingsYDeep = "PlanogramPosition_FacingsYDeep";
        public const String PlanogramPositionMerchandisingStyleY = "PlanogramPosition_MerchandisingStyleY";
        public const String PlanogramPositionOrientationTypeY = "PlanogramPosition_OrientationTypeY";
        public const String PlanogramPositionIsYPlacedBottom = "PlanogramPosition_IsYPlacedBottom";
        public const String PlanogramPositionFacingsZHigh = "PlanogramPosition_FacingsZHigh";
        public const String PlanogramPositionFacingsZWide = "PlanogramPosition_FacingsZWide";
        public const String PlanogramPositionFacingsZDeep = "PlanogramPosition_FacingsZDeep";
        public const String PlanogramPositionMerchandisingStyleZ = "PlanogramPosition_MerchandisingStyleZ";
        public const String PlanogramPositionOrientationTypeZ = "PlanogramPosition_OrientationTypeZ";
        public const String PlanogramPositionIsZPlacedFront = "PlanogramPosition_IsZPlacedFront";
        public const String PlanogramPositionSequence = "PlanogramPosition_Sequence";
        public const String PlanogramPositionSequenceX = "PlanogramPosition_SequenceX";
        public const String PlanogramPositionSequenceY = "PlanogramPosition_SequenceY";
        public const String PlanogramPositionSequenceZ = "PlanogramPosition_SequenceZ";
        public const String PlanogramPositionUnitsHigh = "PlanogramPosition_UnitsHigh";
        public const String PlanogramPositionUnitsWide = "PlanogramPosition_UnitsWide";
        public const String PlanogramPositionUnitsDeep = "PlanogramPosition_UnitsDeep";
        public const String PlanogramPositionTotalUnits = "PlanogramPosition_TotalUnits";
        public const String PlanogramPositionMetaIsPositionCollisions = "PlanogramPosition_MetaIsPositionCollisions";
        public const String PlanogramPositionMetaAchievedCases = "PlanogramPosition_MetaAchievedCases";
        public const String PlanogramPositionMetaFrontFacingsWide = "PlanogramPosition_MetaFrontFacingsWide";
        public const String PlanogramPositionIsManuallyPlaced = "PlanogramPosition_IsManuallyPlaced";
        public const String PlanogramPositionHorizontalSqueeze = "PlanogramPosition_HorizontalSqueeze";
        public const String PlanogramPositionVerticalSqueeze = "PlanogramPosition_VerticalSqueeze";
        public const String PlanogramPositionDepthSqueeze = "PlanogramPosition_DepthSqueeze";
        public const String PlanogramPositionHorizontalSqueezeX = "PlanogramPosition_HorizontalSqueezeX";
        public const String PlanogramPositionVerticalSqueezeX = "PlanogramPosition_VerticalSqueezeX";
        public const String PlanogramPositionDepthSqueezeX = "PlanogramPosition_DepthSqueezeX";
        public const String PlanogramPositionHorizontalSqueezeY = "PlanogramPosition_HorizontalSqueezeY";
        public const String PlanogramPositionVerticalSqueezeY = "PlanogramPosition_VerticalSqueezeY";
        public const String PlanogramPositionDepthSqueezeY = "PlanogramPosition_DepthSqueezeY";
        public const String PlanogramPositionHorizontalSqueezeZ = "PlanogramPosition_HorizontalSqueezeZ";
        public const String PlanogramPositionVerticalSqueezeZ = "PlanogramPosition_VerticalSqueezeZ";
        public const String PlanogramPositionDepthSqueezeZ = "PlanogramPosition_DepthSqueezeZ";
        public const String PlanogramPositionMetaIsUnderMinDeep = "PlanogramPosition_MetaIsUnderMinDeep";
        public const String PlanogramPositionMetaIsOverMaxDeep = "PlanogramPosition_MetaIsOverMaxDeep";
        public const String PlanogramPositionMetaIsOverMaxRightCap = "PlanogramPosition_MetaIsOverMaxRightCap";
        public const String PlanogramPositionMetaIsOverMaxStack = "PlanogramPosition_MetaIsOverMaxStack";
        public const String PlanogramPositionMetaIsOverMaxTopCap = "PlanogramPosition_MetaIsOverMaxTopCap";
        public const String PlanogramPositionMetaIsInvalidMerchandisingStyle = "PlanogramPosition_MetaIsInvalidMerchandisingStyle";
        public const String PlanogramPositionMetaIsHangingTray = "PlanogramPosition_MetaIsHangingTray";
        public const String PlanogramPositionMetaIsPegOverfilled = "PlanogramPosition_MetaIsPegOverfilled";
        public const String PlanogramPositionMetaIsOutsideMerchandisingSpace = "PlanogramPosition_MetaIsOutsideMerchandisingSpace";
        public const String PlanogramPositionMetaIsOutsideOfBlockSpace = "PlanogramPosition_MetaIsOutsideOfBlockSpace";
        public const String PlanogramPositionMetaHeight = "PlanogramPosition_MetaHeight";
        public const String PlanogramPositionMetaWidth = "PlanogramPosition_MetaWidth";
        public const String PlanogramPositionMetaDepth = "PlanogramPosition_MetaDepth";
        public const String PlanogramPositionMetaWorldX = "PlanogramPosition_MetaWorldX";
        public const String PlanogramPositionMetaWorldY = "PlanogramPosition_MetaWorldY";
        public const String PlanogramPositionMetaWorldZ = "PlanogramPosition_MetaWorldZ";
        public const String PlanogramPositionMetaTotalLinearSpace = "PlanogramPosition_MetaTotalLinearSpace";
        public const String PlanogramPositionMetaTotalAreaSpace = "PlanogramPosition_MetaTotalAreaSpace";
        public const String PlanogramPositionMetaTotalVolumetricSpace = "PlanogramPosition_MetaTotalVolumetricSpace";
        public const String PlanogramPositionMetaPlanogramLinearSpacePercentage = "PlanogramPosition_MetaPlanogramLinearSpacePercentage";
        public const String PlanogramPositionMetaPlanogramAreaSpacePercentage = "PlanogramPosition_MetaPlanogramAreaSpacePercentage";
        public const String PlanogramPositionMetaPlanogramVolumetricSpacePercentage = "PlanogramPosition_MetaPlanogramVolumetricSpacePercentage";
        public const String PlanogramPositionSequenceNumber = "PlanogramPosition_SequenceNumber";
        public const String PlanogramPositionSequenceColour = "PlanogramPosition_SequenceColour";
        public const String PlanogramPositionMetaComparisonStatus = "PlanogramPosition_MetaComparisonStatus";
        public const String PlanogramPositionMetaSequenceSubGroupName = "PlanogramPosition_MetaSequenceSubGroupName";
        public const String PlanogramPositionMetaSequenceGroupName = "PlanogramPosition_MetaSequenceGroupName";
        public const String PlanogramPositionMetaPegRowNumber = "PlanogramPosition_MetaPegRowNumber";
        public const String PlanogramPositionMetaPegColumnNumber = "PlanogramPosition_MetaPegColumnNumber";

        #endregion

        #region PlanogramProcessingStatus

        public const String PlanogramProcessingStatusPlanogramId = "Planogram_Id";
        public const String PlanogramProcessingStatusRowVersion = "PlanogramProcessingStatus_RowVersion";
        public const String PlanogramProcessingStatusMetaDataStatus = "PlanogramProcessingStatus_MetaDataStatus";
        public const String PlanogramProcessingStatusMetaDataStatusDescription = "PlanogramProcessingStatus_MetaDataStatusDescription";
        public const String PlanogramProcessingStatusMetaDataProgressMax = "PlanogramProcessingStatus_MetaDataProgressMax";
        public const String PlanogramProcessingStatusMetaDataProgressCurrent = "PlanogramProcessingStatus_MetaDataProgressCurrent";
        public const String PlanogramProcessingStatusMetaDataDateStarted = "PlanogramProcessingStatus_MetaDataDateStarted";
        public const String PlanogramProcessingStatusMetaDataDateUpdated = "PlanogramProcessingStatus_MetaDataDateUpdated";
        public const String PlanogramProcessingStatusValidationStatus = "PlanogramProcessingStatus_ValidationStatus";
        public const String PlanogramProcessingStatusValidationStatusDescription = "PlanogramProcessingStatus_ValidationStatusDescription";
        public const String PlanogramProcessingStatusValidationProgressMax = "PlanogramProcessingStatus_ValidationProgressMax";
        public const String PlanogramProcessingStatusValidationProgressCurrent = "PlanogramProcessingStatus_ValidationProgressCurrent";
        public const String PlanogramProcessingStatusValidationDateStarted = "PlanogramProcessingStatus_ValidationDateStarted";
        public const String PlanogramProcessingStatusValidationDateUpdated = "PlanogramProcessingStatus_ValidationDateUpdated";

        #endregion

        #region PlanogramProduct

        public const String PlanogramProductId = "PlanogramProduct_Id";
        public const String PlanogramProductPlanogramId = "Planogram_Id";
        public const String PlanogramProductGtin = "PlanogramProduct_Gtin";
        public const String PlanogramProductName = "PlanogramProduct_Name";
        public const String PlanogramProductBrand = "PlanogramProduct_Brand";
        public const String PlanogramProductHeight = "PlanogramProduct_Height";
        public const String PlanogramProductWidth = "PlanogramProduct_Width";
        public const String PlanogramProductDepth = "PlanogramProduct_Depth";
        public const String PlanogramProductDisplayHeight = "PlanogramProduct_DisplayHeight";
        public const String PlanogramProductDisplayWidth = "PlanogramProduct_DisplayWidth";
        public const String PlanogramProductDisplayDepth = "PlanogramProduct_DisplayDepth";
        public const String PlanogramProductAlternateHeight = "PlanogramProduct_AlternateHeight";
        public const String PlanogramProductAlternateWidth = "PlanogramProduct_AlternateWidth";
        public const String PlanogramProductAlternateDepth = "PlanogramProduct_AlternateDepth";
        public const String PlanogramProductPointOfPurchaseHeight = "PlanogramProduct_PointOfPurchaseHeight";
        public const String PlanogramProductPointOfPurchaseWidth = "PlanogramProduct_PointOfPurchaseWidth";
        public const String PlanogramProductPointOfPurchaseDepth = "PlanogramProduct_PointOfPurchaseDepth";
        public const String PlanogramProductNumberOfPegHoles = "PlanogramProduct_NumberOfPegHoles";
        public const String PlanogramProductPegX = "PlanogramProduct_PegX";
        public const String PlanogramProductPegX2 = "PlanogramProduct_PegX2";
        public const String PlanogramProductPegX3 = "PlanogramProduct_PegX3";
        public const String PlanogramProductPegY = "PlanogramProduct_PegY";
        public const String PlanogramProductPegY2 = "PlanogramProduct_PegY2";
        public const String PlanogramProductPegY3 = "PlanogramProduct_PegY3";
        public const String PlanogramProductPegProngOffsetX = "PlanogramProduct_PegProngOffsetX";
        public const String PlanogramProductPegProngOffsetY = "PlanogramProduct_PegProngOffsetY";
        public const String PlanogramProductPegDepth = "PlanogramProduct_PegDepth";
        public const String PlanogramProductSqueezeHeight = "PlanogramProduct_SqueezeHeight";
        public const String PlanogramProductSqueezeWidth = "PlanogramProduct_SqueezeWidth";
        public const String PlanogramProductSqueezeDepth = "PlanogramProduct_SqueezeDepth";
        public const String PlanogramProductNestingHeight = "PlanogramProduct_NestingHeight";
        public const String PlanogramProductNestingWidth = "PlanogramProduct_NestingWidth";
        public const String PlanogramProductNestingDepth = "PlanogramProduct_NestingDepth";
        public const String PlanogramProductCasePackUnits = "PlanogramProduct_CasePackUnits";
        public const String PlanogramProductCaseHigh = "PlanogramProduct_CaseHigh";
        public const String PlanogramProductCaseWide = "PlanogramProduct_CaseWide";
        public const String PlanogramProductCaseDeep = "PlanogramProduct_CaseDeep";
        public const String PlanogramProductCaseHeight = "PlanogramProduct_CaseHeight";
        public const String PlanogramProductCaseWidth = "PlanogramProduct_CaseWidth";
        public const String PlanogramProductCaseDepth = "PlanogramProduct_CaseDepth";
        public const String PlanogramProductMaxStack = "PlanogramProduct_MaxStack";
        public const String PlanogramProductMaxTopCap = "PlanogramProduct_MaxTopCap";
        public const String PlanogramProductMaxRightCap = "PlanogramProduct_MaxRightCap";
        public const String PlanogramProductMinDeep = "PlanogramProduct_MinDeep";
        public const String PlanogramProductMaxDeep = "PlanogramProduct_MaxDeep";
        public const String PlanogramProductTrayPackUnits = "PlanogramProduct_TrayPackUnits";
        public const String PlanogramProductTrayHigh = "PlanogramProduct_TrayHigh";
        public const String PlanogramProductTrayWide = "PlanogramProduct_TrayWide";
        public const String PlanogramProductTrayDeep = "PlanogramProduct_TrayDeep";
        public const String PlanogramProductTrayHeight = "PlanogramProduct_TrayHeight";
        public const String PlanogramProductTrayWidth = "PlanogramProduct_TrayWidth";
        public const String PlanogramProductTrayDepth = "PlanogramProduct_TrayDepth";
        public const String PlanogramProductTrayThickHeight = "PlanogramProduct_TrayThickHeight";
        public const String PlanogramProductTrayThickWidth = "PlanogramProduct_TrayThickWidth";
        public const String PlanogramProductTrayThickDepth = "PlanogramProduct_TrayThickDepth";
        public const String PlanogramProductFrontOverhang = "PlanogramProduct_FrontOverhang";
        public const String PlanogramProductFingerSpaceAbove = "PlanogramProduct_FingerSpaceAbove";
        public const String PlanogramProductFingerSpaceToTheSide = "PlanogramProduct_FingerSpaceToTheSide";
        public const String PlanogramProductStatusType = "PlanogramProduct_StatusType";
        public const String PlanogramProductOrientationType = "PlanogramProduct_OrientationType";
        public const String PlanogramProductMerchandisingStyle = "PlanogramProduct_MerchandisingStyle";
        public const String PlanogramProductIsFrontOnly = "PlanogramProduct_IsFrontOnly";
        public const String PlanogramProductIsPlaceHolderProduct = "PlanogramProduct_IsPlaceHolderProduct";
        public const String PlanogramProductIsActive = "PlanogramProduct_IsActive";
        public const String PlanogramProductCanBreakTrayUp = "PlanogramProduct_CanBreakTrayUp";
        public const String PlanogramProductCanBreakTrayDown = "PlanogramProduct_CanBreakTrayDown";
        public const String PlanogramProductCanBreakTrayBack = "PlanogramProduct_CanBreakTrayBack";
        public const String PlanogramProductCanBreakTrayTop = "PlanogramProduct_CanBreakTrayTop";
        public const String PlanogramProductForceMiddleCap = "PlanogramProduct_ForceMiddleCap";
        public const String PlanogramProductForceBottomCap = "PlanogramProduct_ForceBottomCap";
        public const String PlanogramProductPlacementStyle = "PlanogramProduct_PlacementStyle";
        public const String PlanogramProductPlanogramImageIdFront = "PlanogramProduct_PlanogramImageIdFront";
        public const String PlanogramProductPlanogramImageIdBack = "PlanogramProduct_PlanogramImageIdBack";
        public const String PlanogramProductPlanogramImageIdTop = "PlanogramProduct_PlanogramImageIdTop";
        public const String PlanogramProductPlanogramImageIdBottom = "PlanogramProduct_PlanogramImageIdBottom";
        public const String PlanogramProductPlanogramImageIdLeft = "PlanogramProduct_PlanogramImageIdLeft";
        public const String PlanogramProductPlanogramImageIdRight = "PlanogramProduct_PlanogramImageIdRight";
        public const String PlanogramProductPlanogramImageIdDisplayFront = "PlanogramProduct_PlanogramImageIdDisplayFront";
        public const String PlanogramProductPlanogramImageIdDisplayBack = "PlanogramProduct_PlanogramImageIdDisplayBack";
        public const String PlanogramProductPlanogramImageIdDisplayTop = "PlanogramProduct_PlanogramImageIdDisplayTop";
        public const String PlanogramProductPlanogramImageIdDisplayBottom = "PlanogramProduct_PlanogramImageIdDisplayBottom";
        public const String PlanogramProductPlanogramImageIdDisplayLeft = "PlanogramProduct_PlanogramImageIdDisplayLeft";
        public const String PlanogramProductPlanogramImageIdDisplayRight = "PlanogramProduct_PlanogramImageIdDisplayRight";
        public const String PlanogramProductPlanogramImageIdTrayFront = "PlanogramProduct_PlanogramImageIdTrayFront";
        public const String PlanogramProductPlanogramImageIdTrayBack = "PlanogramProduct_PlanogramImageIdTrayBack";
        public const String PlanogramProductPlanogramImageIdTrayTop = "PlanogramProduct_PlanogramImageIdTrayTop";
        public const String PlanogramProductPlanogramImageIdTrayBottom = "PlanogramProduct_PlanogramImageIdTrayBottom";
        public const String PlanogramProductPlanogramImageIdTrayLeft = "PlanogramProduct_PlanogramImageIdTrayLeft";
        public const String PlanogramProductPlanogramImageIdTrayRight = "PlanogramProduct_PlanogramImageIdTrayRight";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseFront = "PlanogramProduct_PlanogramImageIdPointOfPurchaseFront";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseBack = "PlanogramProduct_PlanogramImageIdPointOfPurchaseBack";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseTop = "PlanogramProduct_PlanogramImageIdPointOfPurchaseTop";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseBottom = "PlanogramProduct_PlanogramImageIdPointOfPurchaseBottom";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseLeft = "PlanogramProduct_PlanogramImageIdPointOfPurchaseLeft";
        public const String PlanogramProductPlanogramImageIdPointOfPurchaseRight = "PlanogramProduct_PlanogramImageIdPointOfPurchaseRight";
        public const String PlanogramProductPlanogramImageIdAlternateFront = "PlanogramProduct_PlanogramImageIdAlternateFront";
        public const String PlanogramProductPlanogramImageIdAlternateBack = "PlanogramProduct_PlanogramImageIdAlternateBack";
        public const String PlanogramProductPlanogramImageIdAlternateTop = "PlanogramProduct_PlanogramImageIdAlternateTop";
        public const String PlanogramProductPlanogramImageIdAlternateBottom = "PlanogramProduct_PlanogramImageIdAlternateBottom";
        public const String PlanogramProductPlanogramImageIdAlternateLeft = "PlanogramProduct_PlanogramImageIdAlternateLeft";
        public const String PlanogramProductPlanogramImageIdAlternateRight = "PlanogramProduct_PlanogramImageIdAlternateRight";
        public const String PlanogramProductPlanogramImageIdCaseFront = "PlanogramProduct_PlanogramImageIdCaseFront";
        public const String PlanogramProductPlanogramImageIdCaseBack = "PlanogramProduct_PlanogramImageIdCaseBack";
        public const String PlanogramProductPlanogramImageIdCaseTop = "PlanogramProduct_PlanogramImageIdCaseTop";
        public const String PlanogramProductPlanogramImageIdCaseBottom = "PlanogramProduct_PlanogramImageIdCaseBottom";
        public const String PlanogramProductPlanogramImageIdCaseLeft = "PlanogramProduct_PlanogramImageIdCaseLeft";
        public const String PlanogramProductPlanogramImageIdCaseRight = "PlanogramProduct_PlanogramImageIdCaseRight";
        public const String PlanogramProductShapeType = "PlanogramProduct_ShapeType";
        public const String PlanogramProductFillPatternType = "PlanogramProduct_FillPatternType";
        public const String PlanogramProductFillColour = "PlanogramProduct_FillColour";
        public const String PlanogramProductShape = "PlanogramProduct_Shape";
        public const String PlanogramProductPointOfPurchaseDescription = "PlanogramProduct_PointOfPurchaseDescription";
        public const String PlanogramProductShortDescription = "PlanogramProduct_ShortDescription";
        public const String PlanogramProductSubcategory = "PlanogramProduct_Subcategory";
        public const String PlanogramProductCustomerStatus = "PlanogramProduct_CustomerStatus";
        public const String PlanogramProductColour = "PlanogramProduct_Colour";
        public const String PlanogramProductFlavour = "PlanogramProduct_Flavour";
        public const String PlanogramProductPackagingShape = "PlanogramProduct_PackagingShape";
        public const String PlanogramProductPackagingType = "PlanogramProduct_PackagingType";
        public const String PlanogramProductCountryOfOrigin = "PlanogramProduct_CountryOfOrigin";
        public const String PlanogramProductCountryOfProcessing = "PlanogramProduct_CountryOfProcessing";
        public const String PlanogramProductShelfLife = "PlanogramProduct_ShelfLife";
        public const String PlanogramProductDeliveryFrequencyDays = "PlanogramProduct_DeliveryFrequencyDays";
        public const String PlanogramProductDeliveryMethod = "PlanogramProduct_DeliveryMethod";
        public const String PlanogramProductVendorCode = "PlanogramProduct_VendorCode";
        public const String PlanogramProductVendor = "PlanogramProduct_Vendor";
        public const String PlanogramProductManufacturerCode = "PlanogramProduct_ManufacturerCode";
        public const String PlanogramProductManufacturer = "PlanogramProduct_Manufacturer";
        public const String PlanogramProductSize = "PlanogramProduct_Size";
        public const String PlanogramProductUnitOfMeasure = "PlanogramProduct_UnitOfMeasure";
        public const String PlanogramProductDateIntroduced = "PlanogramProduct_DateIntroduced";
        public const String PlanogramProductDateDiscontinued = "PlanogramProduct_DateDiscontinued";
        public const String PlanogramProductDateEffective = "PlanogramProduct_DateEffective";
        public const String PlanogramProductHealth = "PlanogramProduct_Health";
        public const String PlanogramProductCorporateCode = "PlanogramProduct_CorporateCode";
        public const String PlanogramProductBarcode = "PlanogramProduct_Barcode";
        public const String PlanogramProductSellPrice = "PlanogramProduct_SellPrice";
        public const String PlanogramProductSellPackCount = "PlanogramProduct_SellPackCount";
        public const String PlanogramProductSellPackDescription = "PlanogramProduct_SellPackDescription";
        public const String PlanogramProductRecommendedRetailPrice = "PlanogramProduct_RecommendedRetailPrice";
        public const String PlanogramProductManufacturerRecommendedRetailPrice = "PlanogramProduct_ManufacturerRecommendedRetailPrice";
        public const String PlanogramProductCostPrice = "PlanogramProduct_CostPrice";
        public const String PlanogramProductCaseCost = "PlanogramProduct_CaseCost";
        public const String PlanogramProductTaxRate = "PlanogramProduct_TaxRate";
        public const String PlanogramProductConsumerInformation = "PlanogramProduct_ConsumerInformation";
        public const String PlanogramProductTexture = "PlanogramProduct_Texture";
        public const String PlanogramProductStyleNumber = "PlanogramProduct_StyleNumber";
        public const String PlanogramProductPattern = "PlanogramProduct_Pattern";
        public const String PlanogramProductModel = "PlanogramProduct_Model";
        public const String PlanogramProductGarmentType = "PlanogramProduct_GarmentType";
        public const String PlanogramProductIsPrivateLabel = "PlanogramProduct_IsPrivateLabel";
        public const String PlanogramProductIsNewProduct = "PlanogramProduct_IsNewProduct";
        public const String PlanogramProductFinancialGroupCode = "PlanogramProduct_FinancialGroupCode";
        public const String PlanogramProductFinancialGroupName = "PlanogramProduct_FinancialGroupName";
        public const String PlanogramProductMetaNotAchievedInventory = "PlanogramProduct_MetaNotAchievedInventory";
        public const String PlanogramProductMetaTotalUnits = "PlanogramProduct_MetaTotalUnits";
        public const String PlanogramProductMetaPlanogramLinearSpacePercentage = "PlanogramProduct_MetaPlanogramLinearSpacePercentage";
        public const String PlanogramProductMetaPlanogramAreaSpacePercentage = "PlanogramProduct_MetaPlanogramAreaSpacePercentage";
        public const String PlanogramProductMetaPlanogramVolumetricSpacePercentage = "PlanogramProduct_MetaPlanogramVolumetricSpacePercentage";
        public const String PlanogramProductMetaPositionCount = "PlanogramProduct_MetaPositionCount";
        public const String PlanogramProductMetaTotalFacings = "PlanogramProduct_MetaTotalFacings";
        public const String PlanogramProductMetaTotalLinearSpace = "PlanogramProduct_MetaTotalLinearSpace";
        public const String PlanogramProductMetaTotalAreaSpace = "PlanogramProduct_MetaTotalAreaSpace";
        public const String PlanogramProductMetaTotalVolumetricSpace = "PlanogramProduct_MetaTotalVolumetricSpace";
        public const String PlanogramProductMetaIsInMasterData = "PlanogramProduct_MetaIsInMasterData";
        public const String PlanogramProductMetaNotAchievedCases = "PlanogramProduct_MetaNotAchievedCases";
        public const String PlanogramProductMetaNotAchievedDOS = "PlanogramProduct_MetaNotAchievedDOS";
        public const String PlanogramProductMetaIsOverShelfLifePercent = "PlanogramProduct_MetaIsOverShelfLifePercent";
        public const String PlanogramProductMetaNotAchievedDeliveries = "PlanogramProduct_MetaNotAchievedDeliveries";
        public const String PlanogramProductMetaTotalMainFacings = "PlanogramProduct_MetaTotalMainFacings";
        public const String PlanogramProductMetaTotalXFacings = "PlanogramProduct_MetaTotalXFacings";
        public const String PlanogramProductMetaTotalYFacings = "PlanogramProduct_MetaTotalYFacings";
        public const String PlanogramProductMetaTotalZFacings = "PlanogramProduct_MetaTotalZFacings";
        public const String PlanogramProductMetaIsRangedInAssortment = "PlanogramProduct_MetaIsRangedInAssortment";
        public const String PlanogramProductColourGroupValue = "PlanogramProduct_ColourGroupValue";
        public const String PlanogramProductMetaCDTNode = "PlanogramProduct_MetaCDTNode";
        public const String PlanogramProductMetaIsProductRuleBroken = "PlanogramProduct_MetaIsProductRuleBroken";
        public const String PlanogramProductMetaIsFamilyRuleBroken = "PlanogramProduct_MetaIsFamilyRuleBroken";
        public const String PlanogramProductMetaIsInheritanceRuleBroken = "PlanogramProduct_MetaIsInheritanceRuleBroken";
        public const String PlanogramProductMetaIsLocalProductRuleBroken = "PlanogramProduct_MetaIsLocalProductRuleBroken";
        public const String PlanogramProductMetaIsDistributionRuleBroken = "PlanogramProduct_MetaIsDistributionRuleBroken";
        public const String PlanogramProductMetaIsCoreRuleBroken = "PlanogramProduct_MetaIsCoreRuleBroken";
        public const String PlanogramProductMetaIsDelistProductRuleBroken = "PlanogramProduct_MetaIsDelistProductRuleBroken";
        public const String PlanogramProductMetaIsForceProductRuleBroken = "PlanogramProduct_MetaIsForceProductRuleBroken";
        public const String PlanogramProductMetaIsPreserveProductRuleBroken = "PlanogramProduct_MetaIsPreserveProductRuleBroken";
        public const String PlanogramProductMetaIsMinimumHurdleProductRuleBroken = "PlanogramProduct_MetaIsMinimumHurdleProductRuleBroken";
        public const String PlanogramProductMetaIsMaximumProductFamilyRuleBroken = "PlanogramProduct_MetaIsMaximumProductFamilyRuleBroken";
        public const String PlanogramProductMetaIsMinimumProductFamilyRuleBroken = "PlanogramProduct_MetaIsMinimumProductFamilyRuleBroken";
        public const String PlanogramProductMetaIsDependencyFamilyRuleBroken = "PlanogramProduct_MetaIsDependencyFamilyRuleBroken";
        public const String PlanogramProductMetaIsDelistFamilyRuleBroken = "PlanogramProduct_MetaIsDelistFamilyRuleBroken";
        public const String PlanogramProductMetaIsBuddied = "PlanogramProduct_MetaIsBuddied";
        public const String PlanogramProductMetaComparisonStatus = "PlanogramProduct_MetaComparisonStatus";

        #endregion

        #region PlanogramSequence

        public const String PlanogramSequenceId = "PlanogramSequence_Id";
        public const String PlanogramSequencePlanogramId = "Planogram_Id";

        #endregion

        #region PlanogramSequenceGroup

        public const String PlanogramSequenceGroupId = "PlanogramSequenceGroup_Id";
        public const String PlanogramSequenceGroupPlanogramSequenceId = "PlanogramSequence_Id";
        public const String PlanogramSequenceGroupName = "PlanogramSequenceGroup_Name";
        public const String PlanogramSequenceGroupColour = "PlanogramSequenceGroup_Colour";

        #endregion

        #region PlanogramSequenceGroupProduct

        public const String PlanogramSequenceGroupProductId = "PlanogramSequenceGroupProduct_Id";
        public const String PlanogramSequenceGroupProductPlanogramSequenceGroupId = "PlanogramSequenceGroup_Id";
        public const String PlanogramSequenceGroupProductGtin = "PlanogramSequenceGroupProduct_Gtin";
        public const String PlanogramSequenceGroupProductSequenceNumber = "PlanogramSequenceGroupProduct_SequenceNumber";
        public const String PlanogramSequenceGroupProductPlanogramSequenceGroupSubGroupId = "PlanogramSequenceGroupSubGroup_Id";

        #endregion

        #region PlanogramSequenceGroupSubGroup

        public const String PlanogramSequenceGroupSubGroupId = "PlanogramSequenceGroupSubGroup_Id";
        public const String PlanogramSequenceGroupSubGroupName = "PlanogramSequenceGroupSubGroup_Name";
        public const String PlanogramSequenceGroupSubGroupAlignment = "PlanogramSequenceGroupSubGroup_Alignment";
        public const String PlanogramSequenceGroupSubGroupPlanogramSequenceGroupId = "PlanogramSequenceGroup_Id";

        #endregion

        #region PlanogramSubComponent

        public const String PlanogramSubComponentId = "PlanogramSubComponent_Id";
        public const String PlanogramSubComponentPlanogramComponentId = "PlanogramComponent_Id";
        public const String PlanogramSubComponentMesh3DId = "PlanogramSubComponent_Mesh3DId";
        public const String PlanogramSubComponentImageIdFront = "PlanogramSubComponent_ImageIdFront";
        public const String PlanogramSubComponentImageIdBack = "PlanogramSubComponent_ImageIdBack";
        public const String PlanogramSubComponentImageIdTop = "PlanogramSubComponent_ImageIdTop";
        public const String PlanogramSubComponentImageIdBottom = "PlanogramSubComponent_ImageIdBottom";
        public const String PlanogramSubComponentImageIdLeft = "PlanogramSubComponent_ImageIdLeft";
        public const String PlanogramSubComponentImageIdRight = "PlanogramSubComponent_ImageIdRight";
        public const String PlanogramSubComponentName = "PlanogramSubComponent_Name";
        public const String PlanogramSubComponentHeight = "PlanogramSubComponent_Height";
        public const String PlanogramSubComponentWidth = "PlanogramSubComponent_Width";
        public const String PlanogramSubComponentDepth = "PlanogramSubComponent_Depth";
        public const String PlanogramSubComponentX = "PlanogramSubComponent_X";
        public const String PlanogramSubComponentY = "PlanogramSubComponent_Y";
        public const String PlanogramSubComponentZ = "PlanogramSubComponent_Z";
        public const String PlanogramSubComponentSlope = "PlanogramSubComponent_Slope";
        public const String PlanogramSubComponentAngle = "PlanogramSubComponent_Angle";
        public const String PlanogramSubComponentRoll = "PlanogramSubComponent_Roll";
        public const String PlanogramSubComponentShapeType = "PlanogramSubComponent_ShapeType";
        public const String PlanogramSubComponentMerchandisableHeight = "PlanogramSubComponent_MerchandisableHeight";
        public const String PlanogramSubComponentMerchandisableDepth = "PlanogramSubComponent_MerchandisableDepth";
        public const String PlanogramSubComponentIsVisible = "PlanogramSubComponent_IsVisible";
        public const String PlanogramSubComponentHasCollisionDetection = "PlanogramSubComponent_HasCollisionDetection";
        public const String PlanogramSubComponentFillPatternTypeFront = "PlanogramSubComponent_FillPatternTypeFront";
        public const String PlanogramSubComponentFillPatternTypeBack = "PlanogramSubComponent_FillPatternTypeBack";
        public const String PlanogramSubComponentFillPatternTypeTop = "PlanogramSubComponent_FillPatternTypeTop";
        public const String PlanogramSubComponentFillPatternTypeBottom = "PlanogramSubComponent_FillPatternTypeBottom";
        public const String PlanogramSubComponentFillPatternTypeLeft = "PlanogramSubComponent_FillPatternTypeLeft";
        public const String PlanogramSubComponentFillPatternTypeRight = "PlanogramSubComponent_FillPatternTypeRight";
        public const String PlanogramSubComponentFillColourFront = "PlanogramSubComponent_FillColourFront";
        public const String PlanogramSubComponentFillColourBack = "PlanogramSubComponent_FillColourBack";
        public const String PlanogramSubComponentFillColourTop = "PlanogramSubComponent_FillColourTop";
        public const String PlanogramSubComponentFillColourBottom = "PlanogramSubComponent_FillColourBottom";
        public const String PlanogramSubComponentFillColourLeft = "PlanogramSubComponent_FillColourLeft";
        public const String PlanogramSubComponentFillColourRight = "PlanogramSubComponent_FillColourRight";
        public const String PlanogramSubComponentLineColour = "PlanogramSubComponent_LineColour";
        public const String PlanogramSubComponentTransparencyPercentFront = "PlanogramSubComponent_TransparencyPercentFront";
        public const String PlanogramSubComponentTransparencyPercentBack = "PlanogramSubComponent_TransparencyPercentBack";
        public const String PlanogramSubComponentTransparencyPercentTop = "PlanogramSubComponent_TransparencyPercentTop";
        public const String PlanogramSubComponentTransparencyPercentBottom = "PlanogramSubComponent_TransparencyPercentBottom";
        public const String PlanogramSubComponentTransparencyPercentLeft = "PlanogramSubComponent_TransparencyPercentLeft";
        public const String PlanogramSubComponentTransparencyPercentRight = "PlanogramSubComponent_TransparencyPercentRight";
        public const String PlanogramSubComponentFaceThicknessFront = "PlanogramSubComponent_FaceThicknessFront";
        public const String PlanogramSubComponentFaceThicknessBack = "PlanogramSubComponent_FaceThicknessBack";
        public const String PlanogramSubComponentFaceThicknessTop = "PlanogramSubComponent_FaceThicknessTop";
        public const String PlanogramSubComponentFaceThicknessBottom = "PlanogramSubComponent_FaceThicknessBottom";
        public const String PlanogramSubComponentFaceThicknessLeft = "PlanogramSubComponent_FaceThicknessLeft";
        public const String PlanogramSubComponentFaceThicknessRight = "PlanogramSubComponent_FaceThicknessRight";
        public const String PlanogramSubComponentRiserHeight = "PlanogramSubComponent_RiserHeight";
        public const String PlanogramSubComponentRiserThickness = "PlanogramSubComponent_RiserThickness";
        public const String PlanogramSubComponentIsRiserPlacedOnFront = "PlanogramSubComponent_IsRiserPlacedOnFront";
        public const String PlanogramSubComponentIsRiserPlacedOnBack = "PlanogramSubComponent_IsRiserPlacedOnBack";
        public const String PlanogramSubComponentIsRiserPlacedOnLeft = "PlanogramSubComponent_IsRiserPlacedOnLeft";
        public const String PlanogramSubComponentIsRiserPlacedOnRight = "PlanogramSubComponent_IsRiserPlacedOnRight";
        public const String PlanogramSubComponentRiserFillPatternType = "PlanogramSubComponent_RiserFillPatternType";
        public const String PlanogramSubComponentRiserColour = "PlanogramSubComponent_RiserColour";
        public const String PlanogramSubComponentRiserTransparencyPercent = "PlanogramSubComponent_RiserTransparencyPercent";
        public const String PlanogramSubComponentNotchStartX = "PlanogramSubComponent_NotchStartX";
        public const String PlanogramSubComponentNotchSpacingX = "PlanogramSubComponent_NotchSpacingX";
        public const String PlanogramSubComponentNotchStartY = "PlanogramSubComponent_NotchStartY";
        public const String PlanogramSubComponentNotchSpacingY = "PlanogramSubComponent_NotchSpacingY";
        public const String PlanogramSubComponentNotchHeight = "PlanogramSubComponent_NotchHeight";
        public const String PlanogramSubComponentNotchWidth = "PlanogramSubComponent_NotchWidth";
        public const String PlanogramSubComponentIsNotchPlacedOnFront = "PlanogramSubComponent_IsNotchPlacedOnFront";
        public const String PlanogramSubComponentIsNotchPlacedOnBack = "PlanogramSubComponent_IsNotchPlacedOnBack";
        public const String PlanogramSubComponentIsNotchPlacedOnLeft = "PlanogramSubComponent_IsNotchPlacedOnLeft";
        public const String PlanogramSubComponentIsNotchPlacedOnRight = "PlanogramSubComponent_IsNotchPlacedOnRight";
        public const String PlanogramSubComponentNotchStyleType = "PlanogramSubComponent_NotchStyleType";
        public const String PlanogramSubComponentDividerObstructionHeight = "PlanogramSubComponent_DividerObstructionHeight";
        public const String PlanogramSubComponentDividerObstructionWidth = "PlanogramSubComponent_DividerObstructionWidth";
        public const String PlanogramSubComponentDividerObstructionDepth = "PlanogramSubComponent_DividerObstructionDepth";
        public const String PlanogramSubComponentDividerObstructionStartX = "PlanogramSubComponent_DividerObstructionStartX";
        public const String PlanogramSubComponentDividerObstructionSpacingX = "PlanogramSubComponent_DividerObstructionSpacingX";
        public const String PlanogramSubComponentDividerObstructionStartY = "PlanogramSubComponent_DividerObstructionStartY";
        public const String PlanogramSubComponentDividerObstructionSpacingY = "PlanogramSubComponent_DividerObstructionSpacingY";
        public const String PlanogramSubComponentDividerObstructionStartZ = "PlanogramSubComponent_DividerObstructionStartZ";
        public const String PlanogramSubComponentDividerObstructionSpacingZ = "PlanogramSubComponent_DividerObstructionSpacingZ";
        public const String PlanogramSubComponentMerchConstraintRow1StartX = "PlanogramSubComponent_MerchConstraintRow1StartX";
        public const String PlanogramSubComponentMerchConstraintRow1SpacingX = "PlanogramSubComponent_MerchConstraintRow1SpacingX";
        public const String PlanogramSubComponentMerchConstraintRow1StartY = "PlanogramSubComponent_MerchConstraintRow1StartY";
        public const String PlanogramSubComponentMerchConstraintRow1SpacingY = "PlanogramSubComponent_MerchConstraintRow1SpacingY";
        public const String PlanogramSubComponentMerchConstraintRow1Height = "PlanogramSubComponent_MerchConstraintRow1Height";
        public const String PlanogramSubComponentMerchConstraintRow1Width = "PlanogramSubComponent_MerchConstraintRow1Width";
        public const String PlanogramSubComponentMerchConstraintRow2StartX = "PlanogramSubComponent_MerchConstraintRow2StartX";
        public const String PlanogramSubComponentMerchConstraintRow2SpacingX = "PlanogramSubComponent_MerchConstraintRow2SpacingX";
        public const String PlanogramSubComponentMerchConstraintRow2StartY = "PlanogramSubComponent_MerchConstraintRow2StartY";
        public const String PlanogramSubComponentMerchConstraintRow2SpacingY = "PlanogramSubComponent_MerchConstraintRow2SpacingY";
        public const String PlanogramSubComponentMerchConstraintRow2Height = "PlanogramSubComponent_MerchConstraintRow2Height";
        public const String PlanogramSubComponentMerchConstraintRow2Width = "PlanogramSubComponent_MerchConstraintRow2Width";
        public const String PlanogramSubComponentLineThickness = "PlanogramSubComponent_LineThickness";
        public const String PlanogramSubComponentMerchandisingType = "PlanogramSubComponent_MerchandisingType";
        public const String PlanogramSubComponentCombineType = "PlanogramSubComponent_CombineType";
        public const String PlanogramSubComponentIsProductOverlapAllowed = "PlanogramSubComponent_IsProductOverlapAllowed";
        public const String PlanogramSubComponentIsProductSqueezeAllowed = "PlanogramSubComponent_IsProductSqueezeAllowed";
        public const String PlanogramSubComponentMerchandisingStrategyX = "PlanogramSubComponent_MerchandisingStrategyX";
        public const String PlanogramSubComponentMerchandisingStrategyY = "PlanogramSubComponent_MerchandisingStrategyY";
        public const String PlanogramSubComponentMerchandisingStrategyZ = "PlanogramSubComponent_MerchandisingStrategyZ";
        public const String PlanogramSubComponentLeftOverhang = "PlanogramSubComponent_LeftOverhang";
        public const String PlanogramSubComponentRightOverhang = "PlanogramSubComponent_RightOverhang";
        public const String PlanogramSubComponentFrontOverhang = "PlanogramSubComponent_FrontOverhang";
        public const String PlanogramSubComponentBackOverhang = "PlanogramSubComponent_BackOverhang";
        public const String PlanogramSubComponentTopOverhang = "PlanogramSubComponent_TopOverhang";
        public const String PlanogramSubComponentBottomOverhang = "PlanogramSubComponent_BottomOverhang";
        public const String PlanogramSubComponentExtendedData = "PlanogramSubComponent_ExtendedData";
        public const String PlanogramSubComponentIsDividerObstructionAtStart = "PlanogramSubComponent_IsDividerObstructionAtStart";
        public const String PlanogramSubComponentIsDividerObstructionAtEnd = "PlanogramSubComponent_IsDividerObstructionAtEnd";
        public const String PlanogramSubComponentIsDividerObstructionByFacing = "PlanogramSubComponent_IsDividerObstructionByFacing";
        public const String PlanogramSubComponentDividerObstructionFillPattern = "PlanogramSubComponent_DividerObstructionFillPattern";
        public const String PlanogramSubComponentDividerObstructionFillColour = "PlanogramSubComponent_DividerObstructionFillColour";

        #endregion

        #region PlanogramValidationGroupInfo

        public const String PlanogramValidationGroupInfoId = "PlanogramValidationTemplateGroup_Id";
        public const String PlanogramValidationGroupInfoPlanogramValidationId = "PlanogramValidationTemplate_Id";
        public const String PlanogramValidationGroupInfoName = "PlanogramValidationTemplateGroup_Name";
        public const String PlanogramValidationGroupInfoResultType = "PlanogramValidationTemplateGroup_ResultType";

        #endregion

        #region PlanogramValidationInfo

        public const String PlanogramValidationInfoId = "PlanogramValidationTemplate_Id";
        public const String PlanogramValidationInfoPlanogramId = "Planogram_Id";
        public const String PlanogramValidationInfoName = "PlanogramValidationTemplate_Name";

        #endregion

        #region PlanogramValidationMetricInfo

        public const String PlanogramValidationMetricInfoId = "PlanogramValidationTemplateGroupMetric_Id";
        public const String PlanogramValidationMetricInfoPlanogramValidationGroupId = "PlanogramValidationTemplateGroup_Id";
        public const String PlanogramValidationMetricInfoField = "PlanogramValidationTemplateGroupMetric_Field";
        public const String PlanogramValidationMetricInfoResultType = "PlanogramValidationTemplateGroupMetric_ResultType";

        #endregion

        #region PlanogramValidationTemplate

        public const String PlanogramValidationTemplateId = "PlanogramValidationTemplate_Id";
        public const String PlanogramValidationTemplateName = "PlanogramValidationTemplate_Name";
        public const String PlanogramValidationTemplatePlanogramId = "Planogram_Id";

        #endregion

        #region PlanogramValidationTemplateGroup

        public const String PlanogramValidationTemplateGroupId = "PlanogramValidationTemplateGroup_Id";
        public const String PlanogramValidationTemplateGroupPlanogramValidationTemplateId = "PlanogramValidationTemplate_Id";
        public const String PlanogramValidationTemplateGroupName = "PlanogramValidationTemplateGroup_Name";
        public const String PlanogramValidationTemplateGroupThreshold1 = "PlanogramValidationTemplateGroup_Threshold1";
        public const String PlanogramValidationTemplateGroupThreshold2 = "PlanogramValidationTemplateGroup_Threshold2";
        public const String PlanogramValidationTemplateGroupResultType = "PlanogramValidationTemplateGroup_ResultType";
        public const String PlanogramValidationTemplateGroupValidationType = "PlanogramValidationTemplateGroup_ValidationType";

        #endregion

        #region PlanogramValidationTemplateGroupMetric

        public const String PlanogramValidationTemplateGroupMetricId = "PlanogramValidationTemplateGroupMetric_Id";
        public const String PlanogramValidationTemplateGroupMetricPlanogramValidationTemplateGroupId = "PlanogramValidationTemplateGroup_Id";
        public const String PlanogramValidationTemplateGroupMetricField = "PlanogramValidationTemplateGroupMetric_Field";
        public const String PlanogramValidationTemplateGroupMetricThreshold1 = "PlanogramValidationTemplateGroupMetric_Threshold1";
        public const String PlanogramValidationTemplateGroupMetricThreshold2 = "PlanogramValidationTemplateGroupMetric_Threshold2";
        public const String PlanogramValidationTemplateGroupMetricScore1 = "PlanogramValidationTemplateGroupMetric_Score1";
        public const String PlanogramValidationTemplateGroupMetricScore2 = "PlanogramValidationTemplateGroupMetric_Score2";
        public const String PlanogramValidationTemplateGroupMetricScore3 = "PlanogramValidationTemplateGroupMetric_Score3";
        public const String PlanogramValidationTemplateGroupMetricAggregationType = "PlanogramValidationTemplateGroupMetric_AggregationType";
        public const String PlanogramValidationTemplateGroupMetricResultType = "PlanogramValidationTemplateGroupMetric_ResultType";
        public const String PlanogramValidationTemplateGroupMetricValidationType = "PlanogramValidationTemplateGroupMetric_ValidationType";
        public const String PlanogramValidationTemplateGroupMetricCriteria = "PlanogramValidationTemplateGroupMetric_Criteria";

        #endregion

        #region PrintTemplate
        public const String PrintTemplateId = "PrintTemplate_Id";
        public const String PrintTemplateRowVersion = "PrintTemplate_RowVersion";
        public const String PrintTemplateEntityId = "Entity_Id";
        public const String PrintTemplateName = "PrintTemplate_Name";
        public const String PrintTemplateDescription = "PrintTemplate_Description";
        public const String PrintTemplatePaperSize = "PrintTemplate_PaperSize";
        public const String PrintTemplateIsPlanMirrored = "PrintTemplate_IsPlanMirrored";
        public const String PrintTemplateDateCreated = "PrintTemplate_DateCreated";
        public const String PrintTemplateDateLastModified = "PrintTemplate_DateLastModified";
        #endregion

        #region PrintTemplateComponent
        public const String PrintTemplateComponentId = "PrintTemplateComponent_Id";
        public const String PrintTemplateComponentPrintTemplateSectionId = "PrintTemplateSection_Id";
        public const String PrintTemplateComponentType = "PrintTemplateComponent_Type";
        public const String PrintTemplateComponentX = "PrintTemplateComponent_X";
        public const String PrintTemplateComponentY = "PrintTemplateComponent_Y";
        public const String PrintTemplateComponentHeight = "PrintTemplateComponent_Height";
        public const String PrintTemplateComponentWidth = "PrintTemplateComponent_Width";
        #endregion

        #region PrintTemplateComponentDetail
        public const String PrintTemplateComponentDetailId = "PrintTemplateComponentDetail_Id";
        public const String PrintTemplateComponentDetailPrintTemplateComponentId = "PrintTemplateComponent_Id";
        public const String PrintTemplateComponentDetailKey = "PrintTemplateComponentDetail_Key";
        public const String PrintTemplateComponentDetailValue = "PrintTemplateComponentDetail_Value";
        #endregion

        #region PrintTemplateSection
        public const String PrintTemplateSectionId = "PrintTemplateSection_Id";
        public const String PrintTemplateSectionPrintTemplateSectionGroupId = "PrintTemplateSectionGroup_Id";
        public const String PrintTemplateSectionNumber = "PrintTemplateSection_Number";
        public const String PrintTemplateSectionOrientation = "PrintTemplateSection_Orientation";
        #endregion

        #region PrintTemplateSectionGroup
        public const String PrintTemplateSectionGroupId = "PrintTemplateSectionGroup_Id";
        public const String PrintTemplateSectionGroupPrintTemplateId = "PrintTemplate_Id";
        public const String PrintTemplateSectionGroupNumber = "PrintTemplateSectionGroup_Number";
        public const String PrintTemplateSectionGroupName = "PrintTemplateSectionGroup_Name";
        public const String PrintTemplateSectionGroupBaysPerPage = "PrintTemplateSectionGroup_BaysPerPage";
        #endregion

        #region ProductImage

        public const String ProductImageId = "ProductImage_Id";
        public const String ProductImageRowVersion = "ProductImage_RowVersion";
        public const String ProductImageEntityId = "Entity_Id";
        public const String ProductImageProductId = "Product_Id";
        public const String ProductImageImageType = "ProductImage_ImageType";
        public const String ProductImageFacingType = "ProductImage_FacingType";
        public const String ProductImageImageId = "Image_Id";
        public const String ProductImageDateCreated = "ProductImage_DateCreated";
        public const String ProductImageDateLastModified = "ProductImage_DateLastModified";

        #endregion

        #region ProductUniverse

        public const String ProductUniverseId = "ProductUniverse_Id";
        public const String ProductUniverseRowVersion = "ProductUniverse_RowVersion";
        public const String ProductUniverseUniqueContentReference = "ProductUniverse_UniqueContentReference";
        public const String ProductUniverseName = "ProductUniverse_Name";
        public const String ProductUniverseProductGroupId = "ProductGroup_Id";
        public const String ProductUniverseIsMaster = "ProductUniverse_IsMaster";
        public const String ProductUniverseEntityId = "Entity_Id";
        public const String ProductUniverseDateCreated = "ProductUniverse_DateCreated";
        public const String ProductUniverseDateLastModified = "ProductUniverse_DateLastModified";
        public const String ProductUniverseSetProperties = "ProductUniverse_SetProperties";
        public const String ProductUniverseParentUniqueContentReference = "ProductUniverse_ParentUniqueContentReference";

        #endregion

        #region ProductUniverseInfo

        public const String ProductUniverseInfoProductGroupCode = "ProductGroup_Code";
        public const String ProductUniverseInfoProductGroupName = "ProductGroup_Name";
        public const String ProductUniverseInfoProductCount = "Product_Count";

        #endregion

        #region ProductUniverseProduct

        public const String ProductUniverseProductId = "ProductUniverseProduct_Id";
        public const String ProductUniverseProductGtin = "Product_Gtin";
        public const String ProductUniverseProductName = "Product_Name";
        public const String ProductUniverseProductProductId = "Product_Id";
        public const String ProductUniverseProductProductUniverseId = "ProductUniverse_Id";
        public const String ProductUniverseProductSetProperties = "ProductUniverseProduct_SetProperties";

        #endregion

        #region RenumberingStrategy

        public const String RenumberingStrategyId = "RenumberingStrategy_Id";
        public const String RenumberingStrategyRowVersion = "RenumberingStrategy_RowVersion";
        public const String RenumberingStrategyEntityId = "Entity_Id";
        public const String RenumberingStrategyName = "RenumberingStrategy_Name";
        public const String RenumberingStrategyDateCreated = "RenumberingStrategy_DateCreated";
        public const String RenumberingStrategyDateLastModified = "RenumberingStrategy_DateLastModified";
        public const String RenumberingStrategyDateDeleted = "RenumberingStrategy_DateDeleted";
        public const String RenumberingStrategyBayComponentXRenumberStrategyPriority =
            "RenumberingStrategy_BayComponentXRenumberStrategyPriority";
        public const String RenumberingStrategyBayComponentYRenumberStrategyPriority =
            "RenumberingStrategy_BayComponentYRenumberStrategyPriority";
        public const String RenumberingStrategyBayComponentZRenumberStrategyPriority =
            "RenumberingStrategy_BayComponentZRenumberStrategyPriority";
        public const String RenumberingStrategyPositionXRenumberStrategyPriority =
            "RenumberingStrategy_PositionXRenumberStrategyPriority";
        public const String RenumberingStrategyPositionYRenumberStrategyPriority =
            "RenumberingStrategy_PositionYRenumberStrategyPriority";
        public const String RenumberingStrategyPositionZRenumberStrategyPriority =
            "RenumberingStrategy_PositionZRenumberStrategyPriority";
        public const String RenumberingStrategyBayComponentXRenumberStrategy =
            "RenumberingStrategy_BayComponentXRenumberStrategy";
        public const String RenumberingStrategyBayComponentYRenumberStrategy =
            "RenumberingStrategy_BayComponentYRenumberStrategy";
        public const String RenumberingStrategyBayComponentZRenumberStrategy =
            "RenumberingStrategy_BayComponentZRenumberStrategy";
        public const String RenumberingStrategyPositionXRenumberStrategy =
            "RenumberingStrategy_PositionXRenumberStrategy";
        public const String RenumberingStrategyPositionYRenumberStrategy =
            "RenumberingStrategy_PositionYRenumberStrategy";
        public const String RenumberingStrategyPositionZRenumberStrategy =
            "RenumberingStrategy_PositionZRenumberStrategy";

        public const String RenumberingStrategyRestartComponentRenumberingPerBay =
            "RenumberingStrategy_RestartComponentRenumberingPerBay";

        public const String RenumberingStrategyIgnoreNonMerchandisingComponents =
            "RenumberingStrategy_IgnoreNonMerchandisingComponents";

        public const String RenumberingStrategyRestartPositionRenumberingPerComponent =
            "RenumberingStrategy_RestartPositionRenumberingPerComponent";

        public const String RenumberingStrategyUniqueNumberMultiPositionProductsPerComponent =
            "RenumberingStrategy_UniqueNumberMultiPositionProductsPerComponent";

        public const String RenumberingStrategyExceptAdjacentPositions = "RenumberingStrategy_ExceptAdjacentPositions";

        public const String RenumberingStrategyRestartPositionRenumberingPerBay =
            "RenumberingStrategy_RestartPositionRenumberingPerBay";

        public const String RenumberingStrategyRestartComponentRenumberingPerComponentType =
            "RenumberingStrategy_RestartComponentRenumberingPerComponentType";

        #endregion

        #region Role

        public const String RoleId = "Role_Id";
        public const String RoleRowVersion = "Role_RowVersion";
        public const String RoleName = "Role_Name";
        public const String RoleDescription = "Role_Description";
        public const String RoleIsAdministrator = "Role_IsAdministrator";

        #endregion

        #region RoleInfo

        public const String RoleInfoId = "Role_Id";
        public const String RoleInfoRowVersion = "Role_RowVersion";
        public const String RoleInfoName = "Role_Name";
        public const String RoleInfoDescription = "Role_Description";

        #endregion

        #region RoleEntity

        public const String RoleEntityId = "RoleEntity_Id";
        public const String RoleEntityRoleId = "Role_Id";
        public const String RoleEntity_EntityId = "Entity_Id";

        #endregion

        #region RoleMember

        public const String RoleMemberId = "RoleMember_Id";
        public const String RoleMemberRowVersion = "RoleMember_RowVersion";
        public const String RoleMemberUserId = "User_Id";
        public const String RoleMemberRoleId = "Role_Id";

        #endregion

        #region RolePermission

        public const String RolePermissionId = "RolePermission_Id";
        public const String RolePermissionRoleId = "Role_Id";
        public const String RolePermissionPermissionType = "RolePermission_PermissionType";

        #endregion

        #region SchemaVersion

        public const String SchemaVersionId = "SchemaVersion_Id";
        public const String SchemaVersionType = "SchemaVersion_Type";
        public const String SchemaVersionMajorVersion = "SchemaVersion_MajorVersion";
        public const String SchemaVersionMinorVersion = "SchemaVersion_MinorVersion";

        #endregion

        #region SystemSettings

        public const String SystemSettingsId = "SystemSettings_Id";
        public const String SystemSettingsKey = "SystemSettings_Key";
        public const String SystemSettingsValue = "SystemSettings_Value";
        public const String SystemSettingsUserId = "User_Id";
        public const String SystemSettingsEntityId = "Entity_Id";

        #endregion

        #region User

        public const String UserId = "User_Id";
        public const String UserRowVersion = "User_RowVersion";
        public const String UserUserName = "User_UserName";
        public const String UserFirstName = "User_FirstName";
        public const String UserLastName = "User_LastName";
        public const String UserDescription = "User_Description";
        public const String UserOffice = "User_Office";
        public const String UserTelephoneNumber = "User_TelephoneNumber";
        public const String UserEMailAddress = "User_EMailAddress";
        public const String UserTitle = "User_Title";
        public const String UserDepartment = "User_Department";
        public const String UserCompany = "User_Company";
        public const String UserManager = "User_Manager";
        public const String UserDateDeleted = "User_DateDeleted";

        #endregion

        #region UserPlanogramGroup

        public const String UserPlanogramGroupId = "UserPlanogramGroup_Id";
        public const String UserPlanogramGroupUserId = "User_Id";
        public const String UserPlanogramGroupPlanogramGroupId = "PlanogramGroup_Id";

        #endregion

        #region ValidationTemplate

        public const String ValidationTemplateRowVersion = "ValidationTemplate_RowVersion";
        public const String ValidationTemplateId = "ValidationTemplate_Id";
        public const String ValidationTemplateEntityId = "Entity_Id";
        public const String ValidationTemplateName = "ValidationTemplate_Name";
        public const String ValidationTemplateDateCreated = "ValidationTemplate_DateCreated";
        public const String ValidationTemplateDateLastModified = "ValidationTemplate_DateLastModified";

        #endregion

        #region ValidationTemplateGroup

        public const String ValidationTemplateGroupId = "ValidationTemplateGroup_Id";
        public const String ValidationTemplateGroupValidationTemplateId = "ValidationTemplate_Id";
        public const String ValidationTemplateGroupName = "ValidationTemplateGroup_Name";
        public const String ValidationTemplateGroupThreshold1 = "ValidationTemplateGroup_Threshold1";
        public const String ValidationTemplateGroupThreshold2 = "ValidationTemplateGroup_Threshold2";
        public const String ValidationTemplateGroupValidationType = "ValidationTemplateGroup_ValidationType";

        #endregion

        #region ValidationTemplateGroupMetric

        public const String ValidationTemplateGroupMetricId = "ValidationTemplateGroupMetric_Id";
        public const String ValidationTemplateGroupMetricValidationTemplateGroupId = "ValidationTemplateGroup_Id";
        public const String ValidationTemplateGroupMetricField = "ValidationTemplateGroupMetric_Field";
        public const String ValidationTemplateGroupMetricThreshold1 = "ValidationTemplateGroupMetric_Threshold1";
        public const String ValidationTemplateGroupMetricThreshold2 = "ValidationTemplateGroupMetric_Threshold2";
        public const String ValidationTemplateGroupMetricScore1 = "ValidationTemplateGroupMetric_Score1";
        public const String ValidationTemplateGroupMetricScore2 = "ValidationTemplateGroupMetric_Score2";
        public const String ValidationTemplateGroupMetricScore3 = "ValidationTemplateGroupMetric_Score3";
        public const String ValidationTemplateGroupMetricAggregationType = "ValidationTemplateGroupMetric_AggregationType";
        public const String ValidationTemplateGroupMetricValidationType = "ValidationTemplateGroupMetric_ValidationType";
        public const String ValidationTemplateGroupMetricCriteria = "ValidationTemplateGroupMetric_Criteria";

        #endregion

        #region Workflow

        public const String WorkflowId = "Workflow_Id";
        public const String WorkflowRowVersion = "Workflow_RowVersion";
        public const String WorkflowEntityId = "Entity_Id";
        public const String WorkflowName = "Workflow_Name";
        public const String WorkflowDescription = "Workflow_Description";
        public const String WorkflowDateCreated = "Workflow_DateCreated";
        public const String WorkflowDateLastModified = "Workflow_DateLastModified";
        public const String WorkflowDateDeleted = "Workflow_DateDeleted";

        #endregion

        #region WorkflowTask

        public const String WorkflowTaskId = "WorkflowTask_Id";
        public const String WorkflowTaskWorkflowId = "Workflow_Id";
        public const String WorkflowTaskSequenceId = "WorkflowTask_SequenceId";
        public const String WorkflowTaskTaskType = "WorkflowTask_TaskType";
        public const String WorkflowTaskDisplayName = "WorkflowTask_DisplayName";
        public const String WorkflowTaskDisplayDescription = "WorkflowTask_DisplayDescription";
        

        #endregion

        #region WorkflowTaskInfo

        public const String WorkflowTaskInfoId = "WorkflowTask_Id";
        public const String WorkflowTaskInfoSequenceId = "WorkflowTask_SequenceId";
        public const String WorkflowTaskInfoTaskType = "WorkflowTask_TaskType";
        public const String WorkflowTaskInfoWorkflowId = "Workflow_Id";
        public const String WorkflowTaskInfoWorkpackageId = "Workpackage_Id";
        public const String WorkflowTaskInfoDisplayName = "WorkflowTask_DisplayName";
        public const String WorkflowTaskInfoDisplayDescription = "WorkflowTask_DisplayDescription";

        #endregion

        #region WorkflowTaskParameter

        public const String WorkflowTaskParameterId = "WorkflowTaskParameter_Id";
        public const String WorkflowTaskParameterWorkflowTaskId = "WorkflowTask_Id";
        public const String WorkflowTaskParameterParameterId = "WorkflowTaskParameter_ParameterId";
        public const String WorkflowTaskParameterIsHidden = "WorkflowTaskParameter_IsHidden";
        public const String WorkflowTaskParameterIsReadOnly = "WorkflowTaskParameter_IsReadOnly";

        #endregion

        #region WorkflowTaskParameterValue

        public const String WorkflowTaskParameterValueId = "WorkflowTaskParameterValue_Id";
        public const String WorkflowTaskParameterValueWorkflowTaskParameterId = "WorkflowTaskParameter_Id";
        public const String WorkflowTaskParameterValueValue1 = "WorkflowTaskParameterValue_Value1";
        public const String WorkflowTaskParameterValueValue2 = "WorkflowTaskParameterValue_Value2";
        public const String WorkflowTaskParameterValueValue3 = "WorkflowTaskParameterValue_Value3";

        #endregion

        #region Workpackage

        public const String WorkpackageId = "Workpackage_Id";
        public const String WorkpackageRowVersion = "Workpackage_RowVersion";
        public const String WorkpackageEntityId = "Entity_Id";
        public const String WorkpackageWorkflowId = "Workflow_Id";
        public const String WorkpackageName = "Workpackage_Name";
        public const String WorkpackageType = "Workpackage_Type";
        public const String WorkpackageUserId = "User_Id";
        public const String WorkpackagePlanogramLocationType = "Workpackage_PlanogramLocationType";
        public const String WorkpackageDateCreated = "Workpackage_DateCreated";
        public const String WorkpackageDateLastModified = "Workpackage_DateLastModified";
        public const String WorkpackageDateCompleted = "Workpackage_DateCompleted";

        #endregion

        #region WorkpackageInfo

        public const String WorkpackageInfoPlanogramCount = "WorkpackageInfo_PlanogramCount";
        public const String WorkpackageInfoPlanogramPendingCount = "WorkpackageInfo_PlanogramPendingCount";
        public const String WorkpackageInfoPlanogramQueuedCount = "WorkpackageInfo_PlanogramQueuedCount";
        public const String WorkpackageInfoPlanogramProcessingCount = "WorkpackageInfo_PlanogramProcessingCount";
        public const String WorkpackageInfoPlanogramCompleteCount = "WorkpackageInfo_PlanogramCompleteCount";
        public const String WorkpackageInfoPlanogramFailedCount = "WorkpackageInfo_PlanogramFailedCount";
        public const String WorkpackageInfoUserFullName = "WorkpackageInfo_UserFullName";
        public const String WorkpackageInfoPlanogramWarningCount = "WorkpackageInfo_PlanogramWarningCount";
        public const String WorkpackageInfoPlanogramTotalErrorScore = "WorkpackageInfo_PlanogramTotalErrorScore";
        public const String WorkpackageInfoPlanogramHighestErrorScore = "WorkpackageInfo_PlanogramHighestErrorScore";
        public const String WorkpackageInfoWorkflowName = "Workflow_Name";

        #endregion

        #region WorkpackagePerformance

        public const String WorkpackagePerformanceWorkpackageId = "Workpackage_Id";
        public const String WorkpackagePerformanceKey = "WorkpackagePerformance_Key";

        #endregion

        #region WorkpackagePerformanceData

        public const String WorkpackagePerformanceDataWorkpackageId = "Workpackage_Id";
        public const String WorkpackagePerformanceDataPerformanceSelectionId = "PerformanceSelection_Id";
        public const String WorkpackagePerformanceDataLocationCode = "Location_Code"; // TODO REMOVE!
        public const String WorkpackagePerformanceDataProductCode = "Product_GTIN";
        public const String WorkpackagePerformanceDataDataType = "DataType";
        public const String WorkpackagePerformanceDataDataId = "Data_Id";
        public const String WorkpackagePerformanceDataP1 = "WorkpackagePerformanceData_P1";
        public const String WorkpackagePerformanceDataP2 = "WorkpackagePerformanceData_P2";
        public const String WorkpackagePerformanceDataP3 = "WorkpackagePerformanceData_P3";
        public const String WorkpackagePerformanceDataP4 = "WorkpackagePerformanceData_P4";
        public const String WorkpackagePerformanceDataP5 = "WorkpackagePerformanceData_P5";
        public const String WorkpackagePerformanceDataP6 = "WorkpackagePerformanceData_P6";
        public const String WorkpackagePerformanceDataP7 = "WorkpackagePerformanceData_P7";
        public const String WorkpackagePerformanceDataP8 = "WorkpackagePerformanceData_P8";
        public const String WorkpackagePerformanceDataP9 = "WorkpackagePerformanceData_P9";
        public const String WorkpackagePerformanceDataP10 = "WorkpackagePerformanceData_P10";
        public const String WorkpackagePerformanceDataP11 = "WorkpackagePerformanceData_P11";
        public const String WorkpackagePerformanceDataP12 = "WorkpackagePerformanceData_P12";
        public const String WorkpackagePerformanceDataP13 = "WorkpackagePerformanceData_P13";
        public const String WorkpackagePerformanceDataP14 = "WorkpackagePerformanceData_P14";
        public const String WorkpackagePerformanceDataP15 = "WorkpackagePerformanceData_P15";
        public const String WorkpackagePerformanceDataP16 = "WorkpackagePerformanceData_P16";
        public const String WorkpackagePerformanceDataP17 = "WorkpackagePerformanceData_P17";
        public const String WorkpackagePerformanceDataP18 = "WorkpackagePerformanceData_P18";
        public const String WorkpackagePerformanceDataP19 = "WorkpackagePerformanceData_P19";
        public const String WorkpackagePerformanceDataP20 = "WorkpackagePerformanceData_P20";

        #endregion

        #region WorkpackagePlanogram

        public const String WorkpackagePlanogramId = "WorkpackagePlanogram_Id";
        public const String WorkpackagePlanogramWorkpackageId = "Workpackage_Id";
        public const String WorkpackagePlanogramSourcePlanogramId = "WorkpackagePlanogram_SourcePlanogramId";
        public const String WorkpackagePlanogramDestinationPlanogramId = "WorkpackagePlanogram_DestinationPlanogramId";
        public const String WorkpackagePlanogramPlanogramName = "Planogram_Name";
        public const String WorkpackagePlanogramPlanogramUserName = "Planogram_UserName";
        public const String WorkpackagePlanogramPlanogramAutomationProcessingStatus = "WorkpackagePlanogramProcessingStatus_AutomationStatus";

        #endregion

        #region WorkpackagePlanogramDebug

        public const String WorkpackagePlanogramDebugSourcePlanogramId = "WorkpackagePlanogramDebug_SourcePlanogramId";
        public const String WorkpackagePlanogramDebugWorkflowTaskId = "WorkflowTask_Id";
        public const String WorkpackagePlanogramDebugDebugPlanogramId = "WorkpackagePlanogramDebug_DebugPlanogramId";

        #endregion

        #region WorkpackagePlanogramParameter

        public const String WorkpackagePlanogramParameterId = "WorkpackagePlanogramParameter_Id";
        public const String WorkpackagePlanogramParameterWorkpackagePlanogramId = "WorkpackagePlanogram_Id";
        public const String WorkpackagePlanogramParameterWorflowTaskParameterId = "WorkflowTaskParameter_Id";

        #endregion

        #region WorkpackagePlanogramParameterValue

        public const String WorkpackagePlanogramParameterValueId = "WorkpackagePlanogramParameterValue_Id";
        public const String WorkpackagePlanogramParameterValueWorkpackagePlanogramParameterId = "WorkpackagePlanogramParameter_Id";
        public const String WorkpackagePlanogramParameterValueValue1 = "WorkpackagePlanogramParameterValue_Value1";
        public const String WorkpackagePlanogramParameterValueValue2 = "WorkpackagePlanogramParameterValue_Value2";
        public const String WorkpackagePlanogramParameterValueValue3 = "WorkpackagePlanogramParameterValue_Value3";

        #endregion

        #region WorkpackagePlanogramProcessingStatus
        public const String WorkpackagePlanogramProcessingStatusWorkpackageId = "Workpackage_Id";
        public const String WorkpackagePlanogramProcessingStatusPlanogramId = "Planogram_Id";
        public const String WorkpackagePlanogramProcessingStatusRowVersion = "WorkpackagePlanogramProcessingStatus_RowVersion";
        public const String WorkpackagePlanogramProcessingStatusAutomationStatus = "WorkpackagePlanogramProcessingStatus_AutomationStatus";
        public const String WorkpackagePlanogramProcessingStatusAutomationStatusDescription = "WorkpackagePlanogramProcessingStatus_AutomationStatusDescription";
        public const String WorkpackagePlanogramProcessingStatusAutomationProgressMax = "WorkpackagePlanogramProcessingStatus_AutomationProgressMax";
        public const String WorkpackagePlanogramProcessingStatusAutomationProgressCurrent = "WorkpackagePlanogramProcessingStatus_AutomationProgressCurrent";
        public const String WorkpackagePlanogramProcessingStatusAutomationDateStarted = "WorkpackagePlanogramProcessingStatus_AutomationDateStarted";
        public const String WorkpackagePlanogramProcessingStatusAutomationDateUpdated = "WorkpackagePlanogramProcessingStatus_AutomationDateUpdated";
        #endregion

        #region WorkpackageProcessingStatus

        public const String WorkpackageProcessingStatusWorkpackageId = "Workpackage_Id";
        public const String WorkpackageProcessingStatusRowVersion = "WorkpackageProcessingStatus_RowVersion";
        public const String WorkpackageProcessingStatusStatus = "WorkpackageProcessingStatus_Status";
        public const String WorkpackageProcessingStatusStatusDescription = "WorkpackageProcessingStatus_StatusDescription";
        public const String WorkpackageProcessingstatusProgressMax = "WorkpackageProcessingStatus_ProgressMax";
        public const String WorkpackageProcessingStatusProgressCurrent = "WorkpackageProcessingStatus_ProgressCurrent";
        public const String WorkpackageProcessingStatusDateStarted = "WorkpackageProcessingStatus_DateStarted";
        public const String WorkpackageProcessingStatusDateUpdated = "WorkpackageProcessingStatus_DateUpdated";

        #endregion
    }
}