﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM V8.0)
//// V8-25476 : A. Kuszyk
////      Initial version. Adapted from GFS.    
//// V8-25788 : M.Pettit
////  Added Report, ReportInfo, ReportComponent, ReportComponentDetail, ReportCondition,
////  Added ReportField, ReportFormulaField, ReportGroup, ReportGroupField, ReportParameter,
////  Added ReportPredicate,ReportSection tables
//// V8-25454 : J.Pickup
////  Added Assortment, AssortmentFile, AssortmentLocalProduct, AssortmentLocation, AssortmentProduct, AssortmentRegion, 
////  AssortmentRegionLocation, AssortmentRegionProduct, Blob, File
//// V8-25455 : J.Pickup
////  Added AssortmentMinorRevision, AssortmentMinorRevisionAmendDistributionAction, AssortmentMinorRevisionAmendDistributionActionLocation, 
////  AssortmentMinorRevisionAmendDistributionActionLocation, AssortmentMinorRevisionDeListAction, AssortmentMinorRevisionDeListActionLocation,
////  AssortmentMinorRevisionReplaceAction, AssortmentMinorRevisionReplaceActionLocation.
//// V8 -26158 : I.George
//// Added the Metric table Name
//// V8-26277 : N.Foster
////  Removed references to reporting
//// V8-26474 : A.Silva ~ Added the ValidationTemplate and related tables.
//// V8-26573 : L.Luong
////  Added PlanogramValidationTemplate and related tables
//// V8-26560 : A.Probyn
////  Added table names for Sequence, SequenceInfo, SequenceProduct, SequenceProperty
//// V8-26520 : J.Pickup
////  Added table name for ContentLookup and tidied up the code.
//// V8-26773 : N.Foster
////  Added WorkpackagePerformance, WorkpackagePerformanceData
//// V8-27411 : M.Pettit
////  Added PlanogramLock table
//// V8-27491 : J.Pickup
////  Added Highlight tables.
//// V8-27939 : L.Luong
////  Added PlanogramImportTemplate tables
//#endregion
//#endregion

//using System;

//namespace Galleria.Ccm.Dal.Mssql.Schema
//{
//    internal static class TableNames
//    {
//        public const String Assortment = "MasterData.Assortment";
//        public const String AssortmentFile = "MasterData.AssortmentFile";
//        public const String AssortmentLocalProduct = "MasterData.AssortmentLocalProduct";
//        public const String AssortmentLocation = "MasterData.AssortmentLocation";
//        public const String AssortmentProduct = "MasterData.AssortmentProduct";
//        public const String AssortmentRegion = "MasterData.AssortmentRegion";
//        public const String AssortmentRegionLocation = "MasterData.AssortmentRegionLocation";
//        public const String AssortmentRegionProduct = "MasterData.AssortmentRegionProduct";
//        public const String AssortmentMinorRevision = "MasterData.AssortmentMinorRevision";
//        public const String AssortmentMinorRevisionAmendDistributionAction = "MasterData.AssortmentMinorRevisionAmendDistributionAction";
//        public const String AssortmentMinorRevisionAmendDistributionActionLocation = "MasterData.AssortmentMinorRevisionAmendDistributionActionLocation";
//        public const String AssortmentMinorRevisionDeListAction = "MasterData.AssortmentMinorRevisionDeListAction";
//        public const String AssortmentMinorRevisionDeListActionLocation = "MasterData.AssortmentMinorRevisionDeListActionLocation";
//        public const String AssortmentMinorRevisionListAction = "MasterData.AssortmentMinorRevisionListAction";
//        public const String AssortmentMinorRevisionListActionLocation = "MasterData.AssortmentMinorRevisionListActionLocation";
//        public const String AssortmentMinorRevisionReplaceAction = "MasterData.AssortmentMinorRevisionReplaceAction";
//        public const String AssortmentMinorRevisionReplaceActionLocation = "MasterData.AssortmentMinorRevisionReplaceActionLocation";
//        public const String Highlight = "MasterData.Highlight";
//        public const String HighlightCharacteristic = "MasterData.HighlightCharacteristic";
//        public const String HighlightCharacteristicRule = "MasterData.HighlightCharacteristicRule";
//        public const String HighlightGroup = "MasterData.HighlightGroup";
//        public const String HighlightFilter = "MasterData.HighlightFilter";
//        public const String Blob = "MasterData.Blob";
//        public const String ContentLookup = "MasterData.ContentLookup";
//        public const String File = "MasterData.File";
//        public const String InventoryProfile = "MasterData.InventoryProfile";
//        public const String Metric = "MasterData.Metric";
//        public const String Label = "MasterData.Label";
//        public const String PlanogramImportTemplate = "MasterData.PlanogramImportTemplate";
//        public const String PlanogramImportTemplateMapping = "MasterData.PlanogramImportTemplateMapping";
//        public const String PlanogramLock = "Content.PlanogramLock";
//        public const String PlanogramProcessingStatus = "Content.PlanogramProcessingStatus";
//        public const String PlanogramValidationTemplate = "Content.PlanogramValidationTemplate";
//        public const String PlanogramValidationTemplateGroup = "Content.PlanogramValidationTemplateGroup";
//        public const String PlanogramValidationTemplateGroupMetric = "Content.PlanogramValidationTemplateGroupMetric";
//        public const String SchemaVersion = "System.SchemaVersion";
//        public const String ValidationTemplate = "MasterData.ValidationTemplate";
//        public const String ValidationTemplateGroup = "MasterData.ValidationTemplateGroup";
//        public const String ValidationTemplateGroupMetric = "MasterData.ValidationTemplateGroupMetric";
//        public const String WorkpackagePerformance = "Content.WorkpackagePerformance";
//        public const String WorkpackagePerformanceData = "Content.WorkpacakgePerformanceData";
//        public const String WorkpackageProcessingStatus = "Content.WorkpackageProcessingStatus";
//        public const String PerformanceSelectionMetric = "MasterData.PerformanceSelectionMetric";
//    }
//}
