﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25476 : A. Kuszyk
//  Initial version. Adapted from GFS.   
// V8-26156 : N.Foster
//  Converted to use framework versions
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal.Configuration;
using System.Reflection;
using System.IO;
using Galleria.Framework.Dal;
using Galleria.Ccm.Security;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Dal.Mssql
{
    /// <summary>
    /// Dal factory class
    /// </summary>
    public class DalFactory : Galleria.Framework.Dal.Mssql.DalFactoryBase
    {
        #region Properties
        /// <summary>
        /// Returns the database type name
        /// </summary>
        protected override String DatabaseTypeName
        {
            get { return "Customer Centric Merchandising"; }
        }
        #endregion

        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion

        #region Upgrade Reports

        /// <summary>
        /// Upgrade the databse by importing a new report definition from the given file
        /// </summary>
        /// <param name="context"></param>
        /// <param name="resourcePrefix"></param>
        /// <param name="resourceName">The filename of the file to import</param>
        public override void ImportReports(Galleria.Framework.Dal.Mssql.DalContext context, String path)
        {
            //Remember old default Dal name
            String previousDalName = DalContainer.DalName;

            //Set current DalFactory as default
            // register the dal factory as the default dal
            // in the dal container, under a temporary name
            String dalName = Guid.NewGuid().ToString();
            DalContainer.RegisterFactory(dalName, this);
            DalContainer.DalName = dalName;

            try
            {
                // now perform authentication
                if (!DomainPrincipal.Authenticate()) throw new DomainAuthenticationException();

                // Call process in Reporting to load reports from xml files into database
                IProcess importDefaultReportsDataProcess =
                    new Galleria.Reporting.Processes.DatabaseMaintenance.ImportDefaultReportsProcess(path);

                // execute this process
                importDefaultReportsDataProcess.Execute();
            }
            catch
            {
                throw;
            }
            finally
            {
                // revert the dal container
                DalContainer.RemoveFactory(dalName);
                DalContainer.DalName = previousDalName;
            }
        }

        #endregion
    }
}