﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-27258 : L.Ineson
//   Created
// V8-27411 : M.Pettit
//  Process fetch now requires userId and locktype
#endregion
#region Version History: (CCM810)
// V8-28662 : L.Ineson
//  Changes to facilitate showing labels and highlights.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Processes.RenderPlanogramImage
{
    public partial class Process
    {
        #region Methods
        /// <summary>
        /// Called when this process is executed
        /// </summary>
        protected override void OnExecute()
        {
            // fetch the planogram if required
            if (this.Planogram == null)
            {
                Package planPackage = Package.FetchById(this.PlanogramId);
                this.Planogram = planPackage.Planograms[0];
            }

            IPlanogramImageSettings imageSettings = this.ImageSettings;

            // we always render as design view for the momement but later on we
            // may want to add different camera types.
            const Boolean isDesignViewCamera = true;

            //Get the rendered dimensions of the planogram.
            Single planHeight = this.Planogram.Height;
            Single planWidth = this.Planogram.Width;

            if (isDesignViewCamera)
            {
                //we will be rendering the plan as design view
                //(bays shown front on next to one another) 
                // so get the dimensions for that.
                GetPlanDesignSize(this.Planogram, /*fixturesOnly*/!imageSettings.Positions, out planHeight, out planWidth);
            }

            //determine the height and width to produce the image as.
            if (this.IsProportional)
            {
                if (this.ImageHeight == null && this.ImageWidth == null)
                {
                    // no dimensions set so set the height to a sensible max
                    this.ImageHeight = 1000;
                }

                if (this.ImageHeight != null && this.ImageWidth == null)
                {
                    //ratio the width according to the specified height
                    this.ImageWidth = (Int32)(this.ImageHeight.Value * (planWidth / planHeight));
                }
                else if (this.ImageHeight == null && this.ImageWidth != null)
                {
                    //ratio the height according to the specified width
                    this.ImageHeight = (Int32)(this.ImageWidth.Value * (planHeight / planWidth));
                }
            }


            //return out if the dimensions are not acceptable.
            if (Single.IsNaN(this.ImageHeight.Value) || this.ImageHeight <= 0
                || Single.IsNaN(this.ImageWidth.Value) || this.ImageWidth <= 0)
            {
                return;
            }

            // get the image renderer.
            IPlanogramImageRenderer imageRenderer = PlanogramImagesHelper.GetPlanogramImageRenderer();
            if (imageRenderer == null) throw new ArgumentNullException("Please register a IPlanogramImageRenderer class to use");

            // enable software rendering as this render
            // could be occurring within a background process
            imageRenderer.IsSoftwareRender = this.IsSoftwareRender;

            // apply render settings
            imageRenderer.ImageHeight = this.ImageHeight.Value;
            imageRenderer.ImageWidth = this.ImageWidth.Value;
            imageRenderer.FitUnproportionately = !this.IsProportional;
            imageRenderer.ImageSettings = imageSettings;
            imageRenderer.PlanogramModelDataList = PlanogramModelDataList;
            // and perform the render
            this.ImageData = imageRenderer.CreateImageData(this.Planogram, this.BayRangeStart, this.BayRangeEnd);
        }

        /// <summary>
        /// Returns the size of the planogram when placed in design view.
        /// </summary>
        /// <param name="fixtureOnly"></param>
        /// <param name="planHeight"></param>
        /// <param name="planWidth"></param>
        private static void GetPlanDesignSize(Planogram plan, Boolean fixtureOnly, out Single planHeight, out Single planWidth)
        {
            Single height = 0;
            Single width = 0;

            List<PlanogramFixture> fixtures = plan.FixtureItems.Select(f => f.GetPlanogramFixture()).ToList();

            width = fixtures.Sum(f => f.Width);
            height = (fixtures.Count != 0) ? fixtures.Max(f => f.Height) : 0;

            if (!fixtureOnly)
            {
                Dictionary<PlanogramFixtureItem, Single> designPlacements = new Dictionary<PlanogramFixtureItem, Single>();
                Single lastFixtureX = 0;
                foreach (PlanogramFixtureItem item in plan.FixtureItems.OrderBy(f => f.BaySequenceNumber))
                {
                    designPlacements[item] = lastFixtureX;
                    lastFixtureX = lastFixtureX + item.GetPlanogramFixture().Width;
                }

                Single planMinX = 0;
                Single planMinY = 0;
                Single planMaxX = width;
                Single planMaxY = height;

                foreach (PlanogramSubComponentPlacement subPlacement in plan.GetPlanogramSubComponentPlacements().ToArray())
                {
                    //check if any positions exceed the bounds
                    foreach (PlanogramPosition pos in subPlacement.GetPlanogramPositions())
                    {
                        PlanogramPositionDetails details = PlanogramPositionDetails.NewPlanogramPositionDetails(pos, pos.GetPlanogramProduct(), subPlacement);
                        RectValue posBounds = new RectValue(pos.X, pos.Y, pos.Z, details.TotalSize.Width, details.TotalSize.Height, details.TotalSize.Depth);

                        //PointValue posLocalCoordinates = new PointValue(pos.X, pos.Y, pos.Z);


                        MatrixValue relativeTransform = MatrixValue.Identity;

                        //append the design matrix for the fixture item
                        PlanogramFixtureItem fixtureItem = subPlacement.FixtureItem;
                        if (fixtureItem != null)
                        {
                            relativeTransform = GetPlanogramRelativeDesignTransform(designPlacements[fixtureItem], subPlacement);
                        }


                        //relativeTransform.Append(MatrixValue.CreateTransformationMatrix(designPlacements[fixtureItem], 0, 0, 0, 0, 0));

                        //if (subPlacement.FixtureAssembly != null)
                        //{
                        //    subPlacement.FixtureAssembly.AppendTranform(ref relativeTransform);
                        //    subPlacement.AssemblyComponent.AppendTranform(ref relativeTransform);
                        //}
                        //else
                        //{
                        //    subPlacement.FixtureComponent.AppendTranform(ref relativeTransform);
                        //}
                        //subPlacement.SubComponent.AppendTranform(ref relativeTransform);

                        //transform the bounds
                        posBounds = relativeTransform.TransformBounds(posBounds);


                        //check against the planogram values.
                        planMinX = Math.Min(posBounds.X, planMinX);
                        planMinY = Math.Min(posBounds.Y, planMinY);
                        planMaxX = Math.Max(posBounds.X + posBounds.Width, planMaxX);
                        planMaxY = Math.Max(posBounds.Y + posBounds.Height, planMaxY);
                    }
                }

                width = planMaxX - planMinX;
                height = planMaxY - planMinY;
            }


            //set the output values.
            planHeight = height;
            planWidth = width;
        }

        /// <summary>
        /// Returns the matrix tranform for this item relative to the planogram.
        /// </summary>
        /// <param name="includeSubComponent">if false, parent transform will be returned</param>
        private static MatrixValue GetPlanogramRelativeDesignTransform(Single fixtureItemX, PlanogramSubComponentPlacement subPlacement)
        {
            //return the full planogram relative tranform for this item.
            // nb - order is important here, must build upwards.
            MatrixValue relativeTransform = MatrixValue.Identity;

            //SubComponent
            PlanogramSubComponent sub = subPlacement.SubComponent;
            if (sub != null)
            {
                relativeTransform.Append(
                   MatrixValue.CreateTransformationMatrix(
                       sub.X, sub.Y, sub.Z, sub.Angle, sub.Slope, sub.Roll));
            }

            if (subPlacement.FixtureComponent != null)
            {
                //Fixture Component
                PlanogramFixtureComponent fc = subPlacement.FixtureComponent;

                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fc.X, fc.Y, fc.Z, fc.Angle, fc.Slope, fc.Roll));

            }
            else if (subPlacement.AssemblyComponent != null)
            {
                //Assembly Component
                PlanogramAssemblyComponent ac = subPlacement.AssemblyComponent;

                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        ac.X, ac.Y, ac.Z, ac.Angle, ac.Slope, ac.Roll));

                //Assembly
                if (subPlacement.FixtureAssembly != null)
                {
                    PlanogramFixtureAssembly fa = subPlacement.FixtureAssembly;

                    relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fa.X, fa.Y, fa.Z, fa.Angle, fa.Slope, fa.Roll));
                }
            }

            //Fixture - design placement
            relativeTransform.Append(MatrixValue.CreateTransformationMatrix(fixtureItemX, 0, 0, 0, 0, 0));


            return relativeTransform;
        }

        #endregion
    }
}
