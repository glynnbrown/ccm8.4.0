﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-27258 : L.Ineson
//   Created
// V8-27411 : M.Pettit
//  Process fetch now requires userId and locktype
#endregion
#region Version History: (CCM CCM820)
// V8-31105 : L.Ineson
//  Added IsProportional property.
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Processes;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Rendering;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Processes.RenderPlanogramImage
{
    public partial class Process : ProcessBase<Process>
    {
        #region Properties

        #region Planogram

        /// <summary>
        /// Planogram property definition
        /// </summary>
        public static PropertyInfo<Planogram> PlanogramProperty =
            RegisterProperty<Planogram>(p => p.Planogram);

        /// <summary>
        /// Gets the planogram used to render the image.
        /// </summary>
        public Planogram Planogram
        {
            get { return this.ReadProperty<Planogram>(PlanogramProperty); }
            private set { this.LoadProperty<Planogram>(PlanogramProperty, value); }
        }

        #endregion

        #region PlanogramId

        /// <summary>
        /// Planogram Id property definition
        /// </summary>
        public static PropertyInfo<Int32> PlanogramIdProperty =
            RegisterProperty<Int32>(p => p.PlanogramId);

        /// <summary>
        /// Gets the id of the planogram to render the image for.
        /// </summary>
        public Int32 PlanogramId
        {
            get { return this.ReadProperty<Int32>(PlanogramIdProperty); }
            private set { this.LoadProperty<Int32>(PlanogramIdProperty, value); }
        }

        #endregion

        #region UserId

        /// <summary>
        /// User Id property definition
        /// </summary>
        public static PropertyInfo<Int32> UserIdProperty =
            RegisterProperty<Int32>(p => p.UserId);

        /// <summary>
        /// Gets the id of the user requesting the process. Required for locking parent package
        /// </summary>
        public Int32 UserId
        {
            get { return this.ReadProperty<Int32>(UserIdProperty); }
            private set { this.LoadProperty<Int32>(UserIdProperty, value); }
        }

        #endregion

        #region ImageHeight

        /// <summary>
        /// ImageHeight property definition.
        /// </summary>
        public static PropertyInfo<Int32?> ImageHeightProperty =
            RegisterProperty<Int32?>(p => p.ImageHeight);

        /// <summary>
        /// Get/Sets the height of the created image.
        /// If null this will be calculated according to the value of
        /// ImageWidth and the planogram dimensions.
        /// </summary>
        public Int32? ImageHeight
        {
            get { return this.ReadProperty<Int32?>(ImageHeightProperty); }
            set { this.LoadProperty<Int32?>(ImageHeightProperty, value); }
        }

        #endregion

        #region ImageWidth

        /// <summary>
        /// Image width property definition
        /// </summary>
        public static PropertyInfo<Int32?> ImageWidthProperty =
            RegisterProperty<Int32?>(p => p.ImageWidth);

        /// <summary>
        /// Gets/Sets the width of the created image
        /// If null this will be calculated according to the value of 
        /// ImageHeight and the planogram dimensions.
        /// </summary>
        public Int32? ImageWidth
        {
            get { return this.ReadProperty<Int32?>(ImageWidthProperty); }
            set { this.LoadProperty<Int32?>(ImageWidthProperty, value); }
        }

        #endregion

        #region IsProportional

        /// <summary>
        /// IsProportional property definition.
        /// </summary>
        public static PropertyInfo<Boolean> IsProportionalProperty =
            RegisterProperty<Boolean>(p => p.IsProportional);

        /// <summary>
        /// Gets/Sets whether the image should be scaled proportionally.
        /// </summary>
        public Boolean IsProportional
        {
            get { return this.ReadProperty<Boolean>(IsProportionalProperty); }
            set { this.LoadProperty<Boolean>(IsProportionalProperty, value); }
        }

        #endregion

        #region IsSoftwareRender

        /// <summary>
        /// IsSoftwareRender property definition.
        /// </summary>
        public static PropertyInfo<Boolean> IsSoftwareRenderProperty =
            RegisterProperty<Boolean>(p => p.IsSoftwareRender);

        /// <summary>
        /// Returns true if the image will be produced using the software renderer.
        /// NB- As a general rule of thumb this should be true if calling from a service
        /// or false if calling from a ui client.
        /// </summary>
        public Boolean IsSoftwareRender
        {
            get { return this.ReadProperty<Boolean>(IsSoftwareRenderProperty); }
            private set { this.LoadProperty<Boolean>(IsSoftwareRenderProperty, value); }
        }

        #endregion

        #region Image Settings

        /// <summary>
        /// ImageSettings property definition.
        /// </summary>
        public static PropertyInfo<IPlanogramImageSettings> ImageSettingsProperty =
            RegisterProperty<IPlanogramImageSettings>(p => p.ImageSettings);

        /// <summary>
        /// Gets/Sets the image settings to use
        /// </summary>
        public IPlanogramImageSettings ImageSettings
        {
            get { return this.ReadProperty<IPlanogramImageSettings>(ImageSettingsProperty); }
            set { this.LoadProperty<IPlanogramImageSettings>(ImageSettingsProperty, value); }
        }

        #endregion

        #region ImageData

        /// <summary>
        /// ImageData property definition
        /// </summary>
        public static PropertyInfo<Byte[]> ImageDataProperty =
            RegisterProperty<Byte[]>(p => p.ImageData);
        /// <summary>
        /// Returns the created image data
        /// </summary>
        public Byte[] ImageData
        {
            get { return this.ReadProperty<Byte[]>(ImageDataProperty); }
            private set { this.LoadProperty<Byte[]>(ImageDataProperty, value); }
        }

        #endregion

        #region Bay Range Start

        /// <summary>
        /// BayRangeStart property definition
        /// </summary>
        public static PropertyInfo<Int32> BayRangeStartProperty =
            RegisterProperty<Int32>(p => p.BayRangeStart);

        /// <summary>
        /// Gets/Sets the bay number to start from. 
        /// </summary>
        public Int32 BayRangeStart
        {
            get { return this.ReadProperty<Int32>(BayRangeStartProperty); }
            set { this.LoadProperty<Int32>(BayRangeStartProperty, value); }
        }

        #endregion

        #region Bay Range End

        /// <summary>
        /// BayRangeEnd property definition
        /// </summary>
        public static PropertyInfo<Int32> BayRangeEndProperty =
            RegisterProperty<Int32>(p => p.BayRangeEnd);

        /// <summary>
        /// Gets/Sets the bay number to end on. 
        /// </summary>
        public Int32 BayRangeEnd
        {
            get { return this.ReadProperty<Int32>(BayRangeEndProperty); }
            set { this.LoadProperty<Int32>(BayRangeEndProperty, value); }
        }


        #endregion

        #region PlanogramModelDataList

        /// <summary>
        /// PlanogramModelDataList property definition
        /// </summary>
        public static PropertyInfo<List<Plan3DData>> PlanogramModelDataListProperty =
            RegisterProperty<List<Plan3DData>>(p => p.PlanogramModelDataList);

        /// <summary>
        /// Gets/Sets the PlanogramModelData. 
        /// </summary>
        public List<Plan3DData> PlanogramModelDataList
        {
            get { return this.ReadProperty<List<Plan3DData>>(PlanogramModelDataListProperty); }
            set { this.LoadProperty<List<Plan3DData>>(PlanogramModelDataListProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        //TODO:
        ///// <summary>
        ///// Defines the authorization rules for this type
        ///// </summary>
        //private static void AddObjectAuthorizationRules()
        //{
        //    BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
        //    BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
        //    BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
        //    BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        //}
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="planogramId">the id of the planogram to create the image for.</param>
        /// <param name="isSoftwareRender">if true the image will be created using the software renderer. Must be true if calling from a service.</param>
        public Process(Int32 planogramId, Boolean isSoftwareRender, Int32 userId)
        {
            this.PlanogramId = planogramId;
            this.UserId = userId;
            this.IsSoftwareRender = isSoftwareRender;

            this.ImageHeight = 300;
            this.ImageWidth = null;
            this.IsProportional = true;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan">the planogram to create the image for.</param>
        /// <param name="isSoftwareRender">if true the image will be created using the software renderer. Must be true if calling from a service.</param>
        public Process(Planogram plan, Boolean isSoftwareRender, Int32 userId, IPlanogramImageSettings imageSettings)
        {
            this.Planogram = plan;
            this.PlanogramId = Convert.ToInt32(plan.Id);
            this.UserId = userId;
            this.IsSoftwareRender = isSoftwareRender;
            this.ImageSettings = imageSettings;


            this.ImageHeight = 300;
            this.ImageWidth = null;
            this.IsProportional = true;
        }


        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan">the planogram to create the image for.</param>
        /// <param name="isSoftwareRender">if true the image will be created using the software renderer. Must be true if calling from a service.</param>
        public Process(Planogram plan, Boolean isSoftwareRender, IPlanogramImageSettings imageSettings)
        {
            this.Planogram = plan;
            this.PlanogramId = Convert.ToInt32(plan.Id);
            this.IsSoftwareRender = isSoftwareRender;
            this.ImageSettings = imageSettings;

            this.ImageHeight = 300;
            this.ImageWidth = null;
            this.IsProportional = true;
        }
        #endregion

   }
}
