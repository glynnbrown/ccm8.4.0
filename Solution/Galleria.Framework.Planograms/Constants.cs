﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-27667 : A.Kuszyk
//  Added Single Precision constant.
#endregion

#region Version History: CCM830
// CCM-18449 : M.Pettit
//  Added Performance Metrics constants
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms
{
    public static class Constants
    {
        #region Math Constants

        // Math contants
        public const Int32 SinglePrecision = 3;

        #endregion

        #region Blocking Constants

        /// <summary>
        /// If a vertical divider is closer than this value to a fixture boundary, it will be
        /// snapped irrespective of its IsSnapped value.
        /// </summary>
        public const Single PercentageMarginForVerticalSnapping = 0.050f;

        /// <summary>
        /// If set to true, when dividers are moved their surround dividers will be stretched to maintain
        /// the aesthetics of the blocking plan. When set to false, dividers are simply moved leaving their
        /// neighbours in place.
        /// </summary>
        public const Boolean StretchLocationsWhenMovingDividers = true;

        /// <summary>
        /// The minimum distance that must be maintained between dividers.
        /// </summary>
        public const Single MinDistanceToNeighbour = 0.01f;

        /// <summary>
        /// The minimum amount that two items can overlap by in order for them to be
        /// considered to be in collision.
        /// </summary>
        public const Single MinPercentageToConsiderOverlap = 0.01f;

        #endregion

        #region Performance

        /// <summary>
        /// The maximum number of allowed metrics, including calculated metrics in a single performance source 
        /// This should equal the maximum allowed standard + calc metrics
        /// (Performance Sources and their metrics are set up in GFS)
        /// </summary>
        public const Int32 MaximumMetricsPerPerformanceSource = 30;

        /// <summary>
        /// The maximum number of allowed standard global performance metrics in a single performance source
        /// (Performance Sources and their metrics are set up in GFS)
        /// </summary>
        public const Int32 MaximumStandardGlobalMetricsPerPerformanceSource = 20;

        /// <summary>
        /// The maximum number of allowed calculated performance metrics in a single performance source
        /// (Performance Sources and their metrics are set up in GFS)
        /// </summary>
        public const Int32 MaximumCalculatedMetricsPerPerformanceSource = 10;
        #endregion
    }
}
