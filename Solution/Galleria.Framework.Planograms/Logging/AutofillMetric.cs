﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM803
//// V8-30103 : J.Pickup 
////  Created
//#endregion
//#endregion

//using System;
//using System.Diagnostics;
//using Gibraltar.Agent.Metrics;

//namespace Galleria.Framework.Planograms.Logging
//{
//    /// <summary>
//    /// A metric used to track the performance of the autofill calculation.
//    /// </summary>
//    [EventMetric("Galleria", "Galleria.Framework.Planograms", "Autofill", Caption = "Autofill", Description = "Performance information for Autofill calculations.")]
//    public sealed class AutofillMetric : IDisposable
//    {
//        #region Fields
//        private Stopwatch _stopWatch = new Stopwatch(); // a stopwatch used to record the duration of the process
//        private string _description; // the ui code description
//        #endregion

//        #region Constructors
//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public AutofillMetric(String description)
//        {
//            _description = description;
//#if DEBUG
//            _stopWatch.Start();
//#endif
//        }
//        #endregion

//        #region Properties
//        /// <summary>
//        /// The metric name
//        /// </summary>
//        [EventMetricValue("description", SummaryFunction.Count, null, Caption = "Code Description", Description = "The name of the UI code that has been executed")]
//        public string Description
//        {
//            get { return _description; }
//        }

//        /// <summary>
//        /// The operation duration
//        /// </summary>
//        [EventMetricValue("duration", SummaryFunction.Sum, "ms", Caption = "Duration", Description = "Duration of the executed operation", IsDefaultValue = true)]
//        public TimeSpan Duration
//        {
//            get { return _stopWatch.Elapsed; }
//        }
//        #endregion

//        #region Methods
//        /// <summary>
//        /// Called when this instance is being disposed
//        /// </summary>
//        public void Dispose()
//        {
//#if DEBUG
//            _stopWatch.Stop();
//            EventMetric.Write(this);
//            //Debug.WriteLine("{0} : {1}", this.Description, this.Duration);
//#endif
//        }
//        #endregion
//    }
//}
