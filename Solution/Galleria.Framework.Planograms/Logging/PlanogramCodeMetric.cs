﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM803
//// V8-29291 : L.Ineson
////  Created
//#endregion
//#region Version History: CCM820
//// V8-30778 : N.Foster
////  Tweaked metric to use less resources in release builds
//#endregion
//#endregion

//using System;
//using System.Diagnostics;
//using Gibraltar.Agent.Metrics;
//using System.Reflection;

//namespace Galleria.Framework.Planograms.Logging
//{
//    /// <summary>
//    /// A metric used to track UI code
//    /// </summary>
//    [EventMetric("Galleria", "Galleria.Framework", "Planograms", Caption = "Planograms", Description = "Execution information for planogram code")]
//    public sealed class PlanogramCodeMetric : IDisposable
//    {
//        #region Fields
//#if DEBUG
//        private Stopwatch _stopWatch = new Stopwatch(); // a stopwatch used to record the duration of the process
//        private string _description; // the code description
//#endif
//        #endregion

//        #region Constructors
//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public PlanogramCodeMetric()
//        {
//#if DEBUG
//            StackFrame frame = new StackFrame(1);
//            MethodBase method = frame.GetMethod();
//            _description = String.Format("{0}.{1}", method.DeclaringType.ToString(), method.Name);
//            _stopWatch.Start();
//#endif
//        }
//        #endregion

//        #region Properties
//#if DEBUG
//        /// <summary>
//        /// The metric name
//        /// </summary>
//        [EventMetricValue("description", SummaryFunction.Count, null, Caption = "Code Description", Description = "The name of the code that has been executed")]
//        public string Description
//        {
//            get { return _description; }
//        }

//        /// <summary>
//        /// The operation duration
//        /// </summary>
//        [EventMetricValue("duration", SummaryFunction.Sum, "ms", Caption = "Duration", Description = "Duration of the executed operation", IsDefaultValue = true)]
//        public TimeSpan Duration
//        {
//            get { return _stopWatch.Elapsed; }
//        }
//#endif
//        #endregion

//        #region Methods
//        /// <summary>
//        /// Called when this instance is being disposed
//        /// </summary>
//        public void Dispose()
//        {
//#if DEBUG
//            _stopWatch.Stop();
//            EventMetric.Write(this);
//            Debug.WriteLine("{0} : {1}", this.Description, this.Duration);
//#endif
//        }
//        #endregion
//    }
//}
