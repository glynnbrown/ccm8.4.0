#region Header Information

// Copyright � Galleria RTS Ltd 2015

#endregion

using System;

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Logging
{
    /// <summary>
    ///     To enable the execution of any of these Debug Methods the Preprocessor Symbol associated must be enabled in the project properties, 
    ///     under Build/Conditional Compilation Symbols. Make sure any changes to these are reverted before commiting so that the symbol does not stay
    ///     active for everyone.
    ///     Note that DEBUGSNAPSHOT can be a very expensive one as it will create planogram snapshots when invoked.
    /// </summary>
    public static class DebugLayout
    {
        public static void DebugLogLayoutPass(String groupName, ICollection<PlanogramMerchandisingBlockPlacement> placementBlocks)
        {
            Debug.WriteLine($"----{Environment.NewLine}Debugging LayOutPlanogram: Laying out blocks...");
            Debug.WriteLine($"Current Block {groupName} contains {placementBlocks.Count} placement blocks.");
            foreach (IGrouping<String, PlanogramMerchandisingBlockPlacement> block in placementBlocks.GroupBy(placement => placement.Parent.SequenceGroup.Name))
            {
                foreach (PlanogramMerchandisingBlockPlacement placement in block)
                {
                    Debug.WriteLine($"Block {block.Key} {placement.SequenceNumber} --> ({placement.OriginalBounds.X}, {placement.OriginalBounds.Y}, {placement.OriginalBounds.Z})-({placement.OriginalBounds.X + placement.OriginalBounds.Width}, {placement.OriginalBounds.Y + placement.OriginalBounds.Height}, {placement.OriginalBounds.Z + placement.OriginalBounds.Depth})");
                }
            }
            Debug.WriteLine($"----{Environment.NewLine}Debugging LayOutBlock: Placing products...");
        }

        //[Conditional("DEBUGLAYOUT")]
        //internal static void OutputPlacement(
        //IEnumerable<PlanogramPositionPlacement> placements,
        //Int32 placementSequence,
        //    Dictionary<String, Int32> rankByGtinLookup)
        //{
        //    IEnumerable<String> positionPlacementsData =
        //        placements.Select(p =>
        //        {
        //            Int32 rank;
        //            if (!rankByGtinLookup.TryGetValue(p.Product.Gtin, out rank)) rank = 0;
        //            return $"{p.Product.Gtin} (S {p.Position.SequenceNumber}, R {rank})";
        //        });
        //    String placementData = String.Join(", ", positionPlacementsData);
        //    Debug.WriteLine($"Placement {placementSequence}: {placementData}");
        //}

        public static void DebugLogPositionPlacements(
            PlanogramMerchandisingBlockPlacement blockPlacement,
            Dictionary<String, Int32> rankByGtinLookup)
        {
            WidthHeightDepthValue blockPlacementSize = blockPlacement.GetSizeReducedByFacingSpacePercentage();
            List<String> placementData = new List<String>();
            String separator = String.Empty;
            IEnumerable<List<PlanogramPositionPlacement>> zGroupedPositions =
                blockPlacement.PositionPlacements.GroupBy(placement => placement.Position.SequenceZ)
                              .OrderBy(grouping => grouping.Key)
                              .Select(grouping => grouping.ToList());
            foreach (List<PlanogramPositionPlacement> positionsWithSameZ in zGroupedPositions)
            {
                IEnumerable<List<PlanogramPositionPlacement>> yGroupedPositions =
                    positionsWithSameZ.GroupBy(placement => placement.Position.SequenceY)
                                      .OrderBy(grouping => grouping.Key)
                                      .Select(grouping => grouping.ToList());
                foreach (List<PlanogramPositionPlacement> positionsWithSameZAndY in yGroupedPositions)
                {
                    String lineData = String.Empty;
                    foreach (PlanogramPositionPlacement p in positionsWithSameZAndY.OrderBy(placement => placement.Position.SequenceX))
                    {
                        Int32 rank;
                        if (!rankByGtinLookup.TryGetValue(p.Product.Gtin, out rank)) rank = 0;
                        String currentData =
                            $"{p.Product.Gtin} (S {p.Position.SequenceNumber}, R {rank}) [{p.Position.SequenceX}, {p.Position.SequenceY}, {p.Position.SequenceZ}]";
                        lineData = $"{lineData}{separator}{currentData}";
                        if (String.IsNullOrWhiteSpace(separator)) separator = ", ";
                    }
                    placementData.Add(lineData);
                    separator = String.Empty;
                }
                placementData.Add("---");
            }
            Debug.WriteLine($"Placement {blockPlacement.SequenceNumber}:");
            foreach (String line in placementData)
            {
                Debug.WriteLine(line);
            }
            Debug.WriteLine($"Allocated coordinates: ({blockPlacement.Coordinates.X}, {blockPlacement.Coordinates.Y}, {blockPlacement.Coordinates.Z}) - ({blockPlacement.Coordinates.X + blockPlacementSize.Width}, {blockPlacement.Coordinates.Y + blockPlacementSize.Height}, {blockPlacement.Coordinates.Z + blockPlacementSize.Depth})");
        }

        public static void DebugLogFirstProductInBlogPlacementDecision(
            PlanogramPositionAnchorDirection anchorDirection,
            String placedGtin,
            String anchorGtin)
        {
            Debug.Print($"Add First Product in Block maintaining presentation --> Placed {placedGtin} {anchorDirection} of {anchorGtin}");
        }

        public static void DebugLogBlockSpaceCompactResult(Single previousValue, Single newValue)
        {
            Debug.WriteLine($"---{Environment.NewLine}Debugging ReduceBlockSpace: Reduce block space to better distribute the products...");
            Debug.WriteLine($"Last reduction ({previousValue}) succeeded, Try reducing {newValue}.");
        }

        [Conditional("DEBUGSNAPSHOT")]
        public static void DebugTakeSnapshot(Planogram source, String fileName = null)
        {
#if DEBUG
            //source.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            //source.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            //source.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            if (String.IsNullOrWhiteSpace(fileName)) fileName = source.Name;
            fileName = Path.ChangeExtension(fileName, ".pog");
            String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), source.Parent.Name, fileName);
            String directoryName = Path.GetDirectoryName(path);
            if (directoryName == null)
            {
                Debug.Print($"Could not take snapshot for {path}");
                return;
            }
            if (!Directory.Exists(directoryName)) Directory.CreateDirectory(directoryName);

            Int32 copyCount = 1;
            String name = Path.GetFileNameWithoutExtension(path);
            while (File.Exists(path))
            {
                copyCount++;
                path = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    source.Parent.Name,
                    $"{Path.GetFileNameWithoutExtension(name)}({copyCount}).pog");
            }
            var sourceClone = source.Parent.Clone();
            sourceClone.Planograms[0].Name += copyCount;
            sourceClone.ApplyEdit();
            sourceClone.SaveAs(1, path);
#endif
        }
    }
}