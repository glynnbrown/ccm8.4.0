﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Galleria.Framework.Planograms.UnitTests")]
[assembly: InternalsVisibleTo("Galleria.Ccm.PlanogramTranslator")]
[assembly: InternalsVisibleTo("Galleria.Ccm.UnitTests")]
