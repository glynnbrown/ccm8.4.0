﻿// Copyright © Galleria RTS Ltd 2016

using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Interface defining the expected members of a
    /// viewmodel class which output the child changed event.
    /// </summary>
    public interface INotifyViewModelChildChanged
    {
        event EventHandler<ViewModelChildChangedEventArgs> ChildChanged;
    }


    /// <summary>
    /// Event Args for the ViewModel ChildChanged event
    /// </summary>
    public class ViewModelChildChangedEventArgs : EventArgs
    {
        #region Properties

        public Object ChildObject { get; private set; }
        public Object OriginalSource { get; private set; }
        public PropertyChangedEventArgs PropertyChangedArgs { get; private set; }
        public NotifyCollectionChangedEventArgs CollectionChangedArgs { get; private set; }

        #endregion

        #region Constructors

        public ViewModelChildChangedEventArgs(Object childObject, PropertyChangedEventArgs propertyArgs)
        {
            this.ChildObject = childObject;
            this.PropertyChangedArgs = propertyArgs;
        }

        public ViewModelChildChangedEventArgs(Object childObject, Object originalSource, NotifyCollectionChangedEventArgs collectionChangedArgs)
        {
            this.ChildObject = childObject;
            this.OriginalSource = originalSource;
            this.CollectionChangedArgs = collectionChangedArgs;
        }

        #endregion

    }
}
