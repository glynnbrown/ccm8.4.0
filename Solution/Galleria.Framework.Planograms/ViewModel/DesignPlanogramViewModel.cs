﻿// Copyright © Galleria RTS Ltd 2016

using Galleria.Framework.Planograms.Rendering;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Simple implemenation of a planogram viewmodel design view.
    /// </summary>
    public class DesignPlanogramViewModel : IPlanRenderable, IDisposable
    {
        #region Fields
        private PlanogramViewModelBase _planogram;
        private readonly ObservableCollection<DesignFixtureViewModel> _flattenedFixtures = new ObservableCollection<DesignFixtureViewModel>();
        private ReadOnlyObservableCollection<DesignFixtureViewModel> _flattenedFixturesRO;
        #endregion

        #region Properties

        public Single Height
        {
            get { return _planogram.Height; }
        }

        public Single Width
        {
            get { return _planogram.Width; }
        }

        public Single Depth
        {
            get { return _planogram.Depth; }
        }

        /// <summary>
        /// Returns a readonly collection of the fixtures of this planogram flattened out.
        /// </summary>
        public ReadOnlyObservableCollection<DesignFixtureViewModel> FlattenedFixtures
        {
            get
            {
                if (_flattenedFixturesRO == null)
                {
                    _flattenedFixturesRO = new ReadOnlyObservableCollection<DesignFixtureViewModel>(_flattenedFixtures);
                }
                return _flattenedFixturesRO;
            }
        }

        #endregion

        #region Constructor

        public DesignPlanogramViewModel(PlanogramViewModelBase plan)
        {
            _planogram = plan;
            plan.PropertyChanged += Plan_PropertyChanged;
            plan.ChildChanged += Plan_ChildChanged;
            ((INotifyCollectionChanged)plan.FixtureViews).CollectionChanged += Plan_FixturesCollectionChanged;

            //add in existing fixtures
            foreach (var child in plan.FixtureViews)
            {
                _flattenedFixtures.Add(new DesignFixtureViewModel(child));
            }
            UpdateFlattenedFixtures();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the source planogram.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plan_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Height":
                case "Width":
                case "Depth":
                    OnPropertyChanged(e.PropertyName);
                    break;
            }
        }

        /// <summary>
        /// Called whenever a child property changes on the source planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plan_ChildChanged(object sender, ViewModelChildChangedEventArgs e)
        {
            if (e.ChildObject is DesignFixtureViewModel)
            {
                UpdateFlattenedFixtures();
            }
        }

        /// <summary>
        /// Called whenever the planogram fixtures collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plan_FixturesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramFixtureViewModelBase o in e.NewItems)
                    {
                        _flattenedFixtures.Add(new DesignFixtureViewModel(o));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        List<PlanogramFixtureViewModelBase> oldItems = e.OldItems.Cast<PlanogramFixtureViewModelBase>().ToList();

                        foreach (DesignFixtureViewModel flattenedView in _flattenedFixtures.ToList())
                        {
                            if (oldItems.Contains(flattenedView.Fixture))
                            {
                                _flattenedFixtures.Remove(flattenedView);
                                flattenedView.Dispose();
                            }
                        }

                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        foreach (var f in _flattenedFixtures)
                        {
                            f.Dispose();
                        }
                        _flattenedFixtures.Clear();

                        foreach (PlanogramFixtureViewModelBase child in (IEnumerable)sender)
                        {
                            _flattenedFixtures.Add(new DesignFixtureViewModel(child));
                        }

                    }
                    break;
            }

            UpdateFlattenedFixtures();
        }

        #endregion

        #region Methods

        private void UpdateFlattenedFixtures()
        {
            Single currX = 0;
            foreach (var view in EnumerateAllFixtures())
            {
                view.X = currX;
                currX += view.Width;
            }
        }

        /// <summary>
        /// Enumerates through all fixtures in this plan in sequence order.
        /// </summary>
        /// <returns></returns>        
        public IEnumerable<DesignFixtureViewModel> EnumerateAllFixtures()
        {
            //If our numbering strategy isn't left to right then we need to get the fixtures in reverse
            //order. As the numbering strategy should not effect the order of the bays.
            if (this._planogram != null &&
                this._planogram.Model != null &&
                this._planogram.Model.RenumberingStrategy != null &&
                this._planogram.Model.RenumberingStrategy.BayComponentXRenumberStrategy == Model.PlanogramRenumberingStrategyXRenumberType.RightToLeft)
            {
                foreach (var fixture in _flattenedFixtures.OrderByDescending(f => f.Fixture.BaySequenceNumber))
                {
                    yield return fixture;
                }
            }
            else
            {
                foreach (var fixture in _flattenedFixtures.OrderBy(f => f.Fixture.BaySequenceNumber))
                {
                    yield return fixture;
                }
            }
        }
        #endregion

        #region IPlanogram Members

        event NotifyCollectionChangedEventHandler IPlanRenderable.FixturesCollectionChanged
        {
            add { _flattenedFixtures.CollectionChanged += value; }
            remove { _flattenedFixtures.CollectionChanged -= value; }
        }

        IEnumerable<IPlanFixtureRenderable> IPlanRenderable.Fixtures
        {
            get { return this.FlattenedFixtures; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;


        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _planogram.PropertyChanged -= Plan_PropertyChanged;
                _planogram.ChildChanged -= Plan_ChildChanged;
                ((INotifyCollectionChanged)_planogram.FixtureViews).CollectionChanged -= Plan_FixturesCollectionChanged;

                foreach (var f in _flattenedFixtures)
                {
                    f.Dispose();
                }

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion
    }
}
