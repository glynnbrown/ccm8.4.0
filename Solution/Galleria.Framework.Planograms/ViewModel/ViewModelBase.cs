﻿// Copyright © Galleria RTS Ltd 2016

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// A base for viewmodel classes which provides advanced event capabilities.
    /// </summary>
    public abstract class ViewModelBase :
        INotifyPropertyChanging,
        INotifyPropertyChanged,
        INotifyViewModelChildChanging,
        INotifyViewModelChildChanged,
        IUpdateable,
        IDataErrorInfo,
        IDisposable
    {
        #region Fields
        private List<Object> _hookedChildObjects = new List<Object>();
        private Boolean _isUpdating; //flag to indicate that this is updating.
        private Int32 _updateLevelCount = 0; //the update level count.
        private ViewModelBase _parent;
        private Boolean _isDisposed;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the parent object that this is currently hooked into.
        /// </summary>
        protected ViewModelBase Parent
        {
            get { return _parent; }
            private set
            {
                if (_parent != value)
                {
                    ViewModelBase oldValue = _parent;
                    _parent = value;

                    OnParentChanged(oldValue, value);
                }
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewModelBase() { }

        #endregion

        #region Interface Implementations

        #region INotifyPropertyChanging

        /// <summary>
        /// Event to notify when a property on this viewmodel is about to change.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        private void RaisePropertyChanging(PropertyChangingEventArgs e)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, e);
            }
        }

        protected virtual void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            RaisePropertyChanging(e);
        }

        protected void OnPropertyChanging(String propertyName)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        #endregion

        #region INotifyPropertyChanged

        /// <summary>
        /// Event to notify when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e);
        }

        protected void OnPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region INotifyViewModelChildChanging

        private event EventHandler<ViewModelChildChangingEventArgs> _childChangingHandlers;

        /// <summary>
        /// Raised whenever a child is about to change.
        /// </summary>
        public event EventHandler<ViewModelChildChangingEventArgs> ChildChanging
        {
            add
            {
                _childChangingHandlers = (EventHandler<ViewModelChildChangingEventArgs>)
                  System.Delegate.Combine(_childChangingHandlers, value);
            }
            remove
            {
                _childChangingHandlers = (EventHandler<ViewModelChildChangingEventArgs>)
                  System.Delegate.Remove(_childChangingHandlers, value);
            }
        }

        private void RaiseChildChanging(ViewModelChildChangingEventArgs args)
        {
            if (_childChangingHandlers != null)
            {
                _childChangingHandlers.Invoke(this, args);
            }
        }

        protected virtual void OnChildChanging(ViewModelChildChangingEventArgs e)
        {
            RaiseChildChanging(e);
        }

        private void OnChildChanging(Object child, PropertyChangingEventArgs propertyChangingArgs)
        {
            ViewModelChildChangingEventArgs args = new ViewModelChildChangingEventArgs(child, propertyChangingArgs);
            OnChildChanging(args);
        }

        #endregion

        #region INotifyViewModelChildChanged

        private event EventHandler<ViewModelChildChangedEventArgs> _childChangedHandlers;

        /// <summary>
        /// Raised whenever a child is changed.
        /// </summary>
        public event EventHandler<ViewModelChildChangedEventArgs> ChildChanged
        {
            add
            {
                _childChangedHandlers = (EventHandler<ViewModelChildChangedEventArgs>)
                  System.Delegate.Combine(_childChangedHandlers, value);
            }
            remove
            {
                _childChangedHandlers = (EventHandler<ViewModelChildChangedEventArgs>)
                  System.Delegate.Remove(_childChangedHandlers, value);
            }
        }

        private void RaiseChildChanged(ViewModelChildChangedEventArgs args)
        {
            if (_childChangedHandlers != null)
            {
                _childChangedHandlers.Invoke(this, args);
            }
        }

        protected virtual void OnChildChanged(ViewModelChildChangedEventArgs e)
        {
            RaiseChildChanged(e);
        }

        private void OnChildChanged(Object child, PropertyChangedEventArgs propertyChangedArgs)
        {
            ViewModelChildChangedEventArgs args = new ViewModelChildChangedEventArgs(child, propertyChangedArgs);
            OnChildChanged(args);
        }

        private void OnChildChanged(Object child, Object originalSource, NotifyCollectionChangedEventArgs collectionChangedArgs)
        {
            ViewModelChildChangedEventArgs args = new ViewModelChildChangedEventArgs(child, originalSource, collectionChangedArgs);
            OnChildChanged(args);
        }

        #endregion

        #region IUpdatable

        /// <summary>
        /// Returns true if this view is currently updating.
        /// </summary>
        public Boolean IsUpdating
        {
            get { return _isUpdating; }
            private set
            {
                if (_isUpdating != value)
                {
                    _isUpdating = value;
                    OnPropertyChanged("IsUpdating");
                }
            }
        }

        /// <summary>
        /// Notifies this object that multiple properties are about to be updated
        /// UI should not be updated until EndUpdate has been called.
        /// </summary>
        /// <returns>true if the call started a new update, or false if
        /// this was already updating</returns>
        public virtual void BeginUpdate()
        {
            //increase the update level count.
            _updateLevelCount++;

            //flag this viewmodel as in the process of updating.
            IsUpdating = true;

            //ripple down.
            foreach (Object child in _hookedChildObjects)
            {
                IUpdateable updateableChild = child as IUpdateable;
                if (updateableChild != null)
                {
                    updateableChild.BeginUpdate();
                }
            }
        }

        /// <summary>
        /// Ends the ongoing update state.
        /// </summary>
        public virtual void EndUpdate()
        {
            if (IsUpdating)
            {
                //ripple down.
                foreach (Object child in _hookedChildObjects)
                {
                    IUpdateable updateableChild = child as IUpdateable;
                    if (updateableChild != null)
                    {
                        updateableChild.EndUpdate();
                    }
                }

                //decrease the update level count.
                _updateLevelCount--;

                if (_updateLevelCount == 0)
                {
                    IsUpdating = false;
                }
            }
        }

        #endregion

        #region IDataErrorInfo

        public virtual String Error
        {
            get { return ""; }
        }

        public String this[String columnName]
        {
            get
            {
                return GetError(columnName);
            }
        }

        /// <summary>
        /// Returns the IDataErrorInfo error message for the given columname.
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected abstract String GetError(String columnName);

        #endregion

        #region IDisposable

        protected Boolean IsDisposed
        {
            get { return _isDisposed; }
            private set { _isDisposed = value; }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                OnDisposing(disposing);
                IsDisposed = true;
            }
        }

        protected virtual void OnDisposing(Boolean disposing)
        {
            RemoveAllChildEventHooks();
            this.Parent = null;
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on a child object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.PropertyName))
            {
                OnChildChanged(sender, e);
            }
        }

        /// <summary>
        /// Called whenever a child collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnChildChanged(this, sender, e);
        }

        /// <summary>
        /// Called whenever a child raises an INotifyViewModelChildChanged event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_ChildChanged(object sender, ViewModelChildChangedEventArgs e)
        {
            OnChildChanged(e);
        }

        /// <summary>
        /// Called whenever a child raises an INotifyChildChanging event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_ChildChanging(object sender, ViewModelChildChangingEventArgs e)
        {
            OnChildChanging(e);
        }

        /// <summary>
        /// Called whenever a property is about to change on a child object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.PropertyName))
            {
                OnChildChanging(sender, e);
            }
        }

        /// <summary>
        /// Collection Changed event handler.
        /// </summary>
        /// <typeparam name="CHILDTYPE"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChildViewModelBaseList_CollectionChanged<CHILDTYPE>(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Object o in e.NewItems)
                    {
                        AddChildEventHooks(o);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (Object o in e.OldItems)
                    {
                        RemoveChildEventHooks(o);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {

                        RemoveAllChildEventHooks(typeof(CHILDTYPE));

                        foreach (Object child in (IEnumerable)sender)
                        {
                            AddChildEventHooks(child);
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds event hooks to the given child
        /// </summary>
        /// <param name="child"></param>
        protected void AddChildEventHooks(Object child)
        {
            if (!_hookedChildObjects.Contains(child))
            {
                ViewModelBase childExtendedViewModelObject = child as ViewModelBase;
                if (childExtendedViewModelObject != null)
                {
                    childExtendedViewModelObject.Parent = this;
                }

                INotifyPropertyChanged propertyChangedChild = child as INotifyPropertyChanged;
                if (propertyChangedChild != null)
                {
                    propertyChangedChild.PropertyChanged += Child_PropertyChanged;
                }

                INotifyCollectionChanged collectionChangedChild = child as INotifyCollectionChanged;
                if (collectionChangedChild != null)
                {
                    collectionChangedChild.CollectionChanged += Child_CollectionChanged;
                }


                INotifyViewModelChildChanged childChangeNotifyChild = child as INotifyViewModelChildChanged;
                if (childChangeNotifyChild != null)
                {
                    childChangeNotifyChild.ChildChanged += Child_ChildChanged;
                }

                INotifyPropertyChanging propertyChangingChild = child as INotifyPropertyChanging;
                if (propertyChangingChild != null)
                {
                    propertyChangingChild.PropertyChanging += Child_PropertyChanging;
                }

                INotifyViewModelChildChanging childChangingNotifyChild = child as INotifyViewModelChildChanging;
                if (childChangingNotifyChild != null)
                {
                    childChangingNotifyChild.ChildChanging += Child_ChildChanging;
                }

                //set the child to updating state if required.
                if (IsUpdating)
                {
                    IUpdateable updateableChild = child as IUpdateable;
                    if (updateableChild != null)
                    {
                        updateableChild.BeginUpdate();
                    }
                }


                _hookedChildObjects.Add(child);
            }
        }

        /// <summary>
        /// Removes event hooks from the given child.
        /// </summary>
        /// <param name="child"></param>
        protected void RemoveChildEventHooks(Object child, Boolean disposeChild = true)
        {
            ViewModelBase childExtendedViewModelObject = child as ViewModelBase;
            if (childExtendedViewModelObject != null)
            {
                childExtendedViewModelObject.Parent = null;
            }

            INotifyPropertyChanged propertyChangedChild = child as INotifyPropertyChanged;
            if (propertyChangedChild != null)
            {
                propertyChangedChild.PropertyChanged -= Child_PropertyChanged;
            }

            INotifyCollectionChanged collectionChangedChild = child as INotifyCollectionChanged;
            if (collectionChangedChild != null)
            {
                collectionChangedChild.CollectionChanged -= Child_CollectionChanged;
            }

            INotifyViewModelChildChanged childChangeNotifyChild = child as INotifyViewModelChildChanged;
            if (childChangeNotifyChild != null)
            {
                childChangeNotifyChild.ChildChanged -= Child_ChildChanged;
            }

            INotifyPropertyChanging propertyChangingChild = child as INotifyPropertyChanging;
            if (propertyChangingChild != null)
            {
                propertyChangingChild.PropertyChanging -= Child_PropertyChanging;
            }

            INotifyViewModelChildChanging childChangingNotifyChild = child as INotifyViewModelChildChanging;
            if (childChangingNotifyChild != null)
            {
                childChangingNotifyChild.ChildChanging -= Child_ChildChanging;
            }

            IUpdateable updateableChild = child as IUpdateable;
            if (updateableChild != null && updateableChild.IsUpdating)
            {
                updateableChild.EndUpdate();
            }

            if (disposeChild)
            {
                IDisposable disposableChild = child as IDisposable;
                if (disposableChild != null)
                {
                    disposableChild.Dispose();
                }
            }

            _hookedChildObjects.Remove(child);
        }

        /// <summary>
        /// Removes hooks from all child objects of a specific type.
        /// </summary>
        protected void RemoveAllChildEventHooks(Type childType, Boolean disposeChildren = true)
        {
            foreach (Object child in _hookedChildObjects.ToList())
            {
                if (child.GetType() == childType)
                {
                    RemoveChildEventHooks(child, disposeChildren);
                }
            }
        }

        /// <summary>
        /// Removes hooks from all child objects.
        /// </summary>
        protected void RemoveAllChildEventHooks()
        {
            foreach (Object child in _hookedChildObjects.ToList())
            {
                RemoveChildEventHooks(child);
            }
        }

        /// <summary>
        /// Called whenever the parent changes.
        /// </summary>
        /// <param name="oldValue">the old parent</param>
        /// <param name="newValue">the new parent</param>
        protected virtual void OnParentChanged(Object oldValue, Object newValue)
        {
            //override as required.
        }

        #endregion

    }
}
