﻿// Copyright © Galleria RTS Ltd 2015
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Provides a base class for a viewmodel of a PlanogramAnnotation
    /// </summary>
    public abstract class PlanogramAnnotationViewModelBase : ViewModelBase, IPlanAnnotationRenderable
    {
        #region Fields
        private readonly PlanogramAnnotation _annotation;
        private PointValue _worldCoordinates = new PointValue();
        #endregion

        #region Properties

        /// <summary>
        /// Returns the source PlanogramAnnotation.
        /// </summary>
        public PlanogramAnnotation Model
        {
            get { return _annotation; }
        }

        #region Model Properties

        public PlanogramAnnotationType AnnotationType
        {
            get { return _annotation.AnnotationType; }
            set { _annotation.AnnotationType = value; }
        }

        public String Text
        {
            get { return _annotation.Text; }
            set { _annotation.Text = value; }
        }

        public Single? X
        {
            get { return _annotation.X; }
            set { _annotation.X = value; }
        }
        public Single? Y
        {
            get { return _annotation.Y; }
            set { _annotation.Y = value; }
        }
        public Single? Z
        {
            get { return _annotation.Z; }
            set { _annotation.Z = value; }
        }

        public Single Height
        {
            get { return _annotation.Height; }
            set { _annotation.Height = value; }
        }
        public Single Width
        {
            get { return _annotation.Width; }
            set { _annotation.Width = value; }
        }
        public Single Depth
        {
            get { return _annotation.Depth; }
            set { _annotation.Depth = value; }
        }

        public Int32 BackgroundColour
        {
            get { return _annotation.BackgroundColour; }
            set { _annotation.BackgroundColour = value; }
        }

        public Int32 BorderColour
        {
            get { return _annotation.BorderColour; }
            set { _annotation.BorderColour = value; }
        }
        public Single BorderThickness
        {
            get { return _annotation.BorderThickness; }
            set { _annotation.BorderThickness = value; }
        }

        public Int32 FontColour
        {
            get { return _annotation.FontColour; }
            set { _annotation.FontColour = value; }
        }
        public String FontName
        {
            get { return _annotation.FontName; }
            set { _annotation.FontName = value; }
        }
        public Single FontSize
        {
            get { return _annotation.FontSize; }
            set { _annotation.FontSize = value; }
        }


        public Boolean CanReduceFontToFit
        {
            get { return _annotation.CanReduceFontToFit; }
            set { _annotation.CanReduceFontToFit = value; }
        }



        #endregion

        #region WorldX

        /// <summary>
        /// Gets/Sets the X coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldX
        {
            get { return _worldCoordinates.X; }
            set { SetWorldCoordinates(value, WorldY, WorldZ); }
        }

        #endregion

        #region WorldY

        /// <summary>
        /// Gets/Sets the Y coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldY
        {
            get { return _worldCoordinates.Y; }
            set { SetWorldCoordinates(WorldX, value, WorldZ); }
        }

        #endregion

        #region WorldZ

        /// <summary>
        /// Gets/Sets the Z coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldZ
        {
            get { return _worldCoordinates.Z; }
            set { SetWorldCoordinates(WorldX, WorldY, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="annotation"></param>
        public PlanogramAnnotationViewModelBase(PlanogramAnnotation annotation)
        {
            _annotation = annotation;
            annotation.PropertyChanging += Model_PropertyChanging;
            annotation.PropertyChanged += Model_PropertyChanged;

            UpdateWorldCoordinates();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(e.PropertyName);
        }

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnModelPropertyChanging(String propertyName)
        {
            if (typeof(PlanogramAnnotationViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the source planogram annotation model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Called whenever a property changes on the source planogram annotation model.
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnModelPropertyChanged(String propertyName)
        {
            if (typeof(PlanogramAnnotationViewModelBase).GetProperty(propertyName) != null)
            {
                //relay out.
                OnPropertyChanged(propertyName);
            }

            switch (propertyName)
            {
                case "X":
                case "Y":
                case "Z":
                    UpdateWorldCoordinates();
                    break;
            }
        }

        /// <summary>
        /// Called whenever the attached parent changes.
        /// </summary>
        protected override void OnParentChanged(object oldValue, object newValue)
        {
            base.OnParentChanged(oldValue, newValue);

            INotifyPropertyChanged oldParent = oldValue as INotifyPropertyChanged;
            if (oldParent != null)
            {
                oldParent.PropertyChanged -= Parent_PropertyChanged;
            }

            INotifyPropertyChanged newParent = newValue as INotifyPropertyChanged;
            if (newParent != null)
            {
                newParent.PropertyChanged += Parent_PropertyChanged;
            }

        }

        /// <summary>
        /// Called whenever a property changes on the attached parent.
        /// </summary>
        private void Parent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.IsDisposed) return;

            switch (e.PropertyName)
            {
                case "WorldX":
                case "WorldY":
                case "WorldZ":
                case "WorldAngle":
                case "WorldSlope":
                case "WorldRoll":
                case "MetaWorldX":
                case "MetaWorldY":
                case "MetaWorldZ":
                case "MetaWorldAngle":
                case "MetaWorldSlope":
                case "MetaWorldRoll":
                    UpdateWorldCoordinates();
                    break;

                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    if (sender is PlanogramFixtureViewModelBase)
                    {
                        UpdateWorldCoordinates();
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called for IDataErrorInfo
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            return ((IDataErrorInfo)Model)[columnName];
        }


        #region World Space Methods

        /// <summary>
        /// Updates the values of the world coordinates
        /// </summary>
        private void UpdateWorldCoordinates()
        {
            //get the world coordinates and rotation
            _worldCoordinates = this.Model.GetPlanogramRelativeCoordinates();

            //nb - rotation is currently not in use.

            //fire off property changed events
            OnPropertyChanged("WorldX");
            OnPropertyChanged("WorldY");
            OnPropertyChanged("WorldZ");
        }


        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(Single newWorldX, Single newWorldY, Single newWorldZ)
        {
            SetWorldCoordinates(new PointValue(newWorldX, newWorldY, newWorldZ));
        }
        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(PointValue newCoordinates)
        {
            this.Model.SetCoordinatesFromPlanogramRelative(newCoordinates);
        }

        #endregion

        /// <summary>
        /// Returns the list of plan item fields for this plan item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields()
        {
            foreach (var field in PlanogramAnnotation.EnumerateDisplayableFieldInfos())
            {
                yield return field;
            }
        }

        #endregion

        #region IPlanogramAnnotationRenderable Members

        Single IPlanAnnotationRenderable.X { get { return (_annotation.X.HasValue) ? _annotation.X.Value : 0; } }
        Single IPlanAnnotationRenderable.Y { get { return (_annotation.Y.HasValue) ? _annotation.Y.Value : 0; } }
        Single IPlanAnnotationRenderable.Z { get { return (_annotation.Z.HasValue) ? _annotation.Z.Value : 0; } }


        #endregion

        #region IDisposable Members

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void OnDisposing(Boolean disposing)
        {
            base.OnDisposing(disposing);

            _annotation.PropertyChanging -= Model_PropertyChanging;
            _annotation.PropertyChanged -= Model_PropertyChanged;
        }

        #endregion

    }
}
