﻿#region header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.2)
// CCM-18938 : M.Pettit
//  Added PegX2,PegY2, PegX3, PegY3 properties
#endregion
#endregion

using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;


namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Viewmodel base for rendering a PlanogramPosition model object
    /// </summary>
    public abstract class PlanogramPositionViewModelBase : ViewModelBase, IPlanPositionRenderable, IPlanogramSubComponentSequencedItem
    {
        #region Fields
        private PlanogramSubComponentViewModelBase _parentSubComponentView;

        private readonly PlanogramPosition _model;
        private PlanogramProduct _referencedProduct;

        private Boolean _isUpdatingCalculatedInfo;

        private Single _height;
        private Single _width;
        private Single _depth;


        private Boolean _isMerchandisedAsTrays;
        private PlanogramProductOrientationType _unitOrientationType;
        private Single _unitHeight;
        private Single _unitWidth;
        private Single _unitDepth;
        private Single _blockMainStartX;
        private Single _blockMainStartY;
        private Single _blockMainStartZ;

        private Boolean _isXMerchandisedAsTrays;
        private PlanogramProductOrientationType _unitXOrientationType;
        private Single _unitXHeight;
        private Single _unitXWidth;
        private Single _unitXDepth;
        private Single _blockXStartX;
        private Single _blockXStartY;
        private Single _blockXStartZ;

        private Boolean _isYMerchandisedAsTrays;
        private PlanogramProductOrientationType _unitYOrientationType;
        private Single _unitYHeight;
        private Single _unitYWidth;
        private Single _unitYDepth;
        private Single _blockYStartX;
        private Single _blockYStartY;
        private Single _blockYStartZ;


        private Boolean _isZMerchandisedAsTrays;
        private PlanogramProductOrientationType _unitZOrientationType;
        private Single _unitZHeight;
        private Single _unitZWidth;
        private Single _unitZDepth;
        private Single _blockZStartX;
        private Single _blockZStartY;
        private Single _blockZStartZ;

        private Single _facingSpaceX;
        private Single _facingSpaceY;
        private Single _facingSpaceZ;

        private PlanogramProductMerchandisingStyle _appliedMerchandisingStyle;
        private PlanogramProductMerchandisingStyle _appliedMerchandisingStyleX;
        private PlanogramProductMerchandisingStyle _appliedMerchandisingStyleY;
        private PlanogramProductMerchandisingStyle _appliedMerchandisingStyleZ;

        private PlanogramProductOrientationType _appliedOrientationType;
        private PlanogramProductOrientationType _appliedOrientationTypeX;
        private PlanogramProductOrientationType _appliedOrientationTypeY;
        private PlanogramProductOrientationType _appliedOrientationTypeZ;

        private Single _pegX;
        private Single _pegY;
        private Single _pegX2;
        private Single _pegY2;
        private Single _pegX3;
        private Single _pegY3;
        private PointValue _worldCoordinates = new PointValue();

        private Boolean _suppressModelPropertyChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the position model object
        /// </summary>
        public PlanogramPosition Model
        {
            get { return _model; }
        }

        /// <summary>
        /// Gets the referenced planogram product model.
        /// </summary>
        protected internal PlanogramProduct ReferencedProduct
        {
            get { return _referencedProduct; }
            private set
            {
                if (_referencedProduct != value)
                {
                    PlanogramProduct oldValue = _referencedProduct;
                    _referencedProduct = value;
                    OnReferencedProductChanged(oldValue, _referencedProduct);
                }
            }
        }

        #region Calculated Properties

        /// <summary>
        /// Returns true if the position products are on pegs.
        /// </summary>
        public Boolean IsOnPegs
        {
            get
            {
                return (ParentSubComponentView.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang);
            }
        }

        /// <summary>
        /// Return true if this position is on a top down component
        /// </summary>
        public Boolean IsOnTopDownComponent
        {
            get { return ParentSubComponentView.ParentComponentView.IsMerchandisedTopDown; }
        }

        /// <summary>
        /// Returns the overall height of this position.
        /// </summary>
        public Single Height
        {
            get { return _height; }
            private set
            {
                if (_height != value)
                {
                    _height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        /// <summary>
        /// Returns the overall width of this position.
        /// </summary>
        public Single Width
        {
            get { return _width; }
            private set
            {
                if (_width != value)
                {
                    _width = value;
                    OnPropertyChanged("Width");
                }
            }
        }

        /// <summary>
        /// Returns the overall depth of this position.
        /// </summary>
        public Single Depth
        {
            get { return _depth; }
            private set
            {
                if (_depth != value)
                {
                    _depth = value;
                    OnPropertyChanged("Depth");
                }
            }
        }

        public Boolean IsMerchandisedAsTrays
        {
            get { return _isMerchandisedAsTrays; }
            private set
            {
                if (_isMerchandisedAsTrays != value)
                {
                    _isMerchandisedAsTrays = value;
                    OnPropertyChanged("IsMerchandisedAsTrays");
                }
            }
        }

        public PlanogramProductOrientationType UnitOrientationType
        {
            get { return _unitOrientationType; }
            private set
            {
                if (_unitOrientationType != value)
                {
                    _unitOrientationType = value;
                    OnPropertyChanged("UnitOrientationType");
                }
            }
        }

        public Single UnitHeight
        {
            get { return _unitHeight; }
            private set
            {
                if (_unitHeight != value)
                {
                    _unitHeight = value;
                    OnPropertyChanged("UnitHeight");
                }
            }
        }

        public Single UnitWidth
        {
            get { return _unitWidth; }
            private set
            {
                if (_unitWidth != value)
                {
                    _unitWidth = value;
                    OnPropertyChanged("UnitWidth");
                }
            }
        }

        public Single UnitDepth
        {
            get { return _unitDepth; }
            private set
            {
                if (_unitDepth != value)
                {
                    _unitDepth = value;
                    OnPropertyChanged("UnitDepth");
                }
            }
        }

        public Single BlockMainStartX
        {
            get { return _blockMainStartX; }
            private set
            {
                if (_blockMainStartX != value)
                {
                    _blockMainStartX = value;
                    OnPropertyChanged("BlockMainStartX");
                }
            }
        }

        public Single BlockMainStartY
        {
            get { return _blockMainStartY; }
            private set
            {
                if (_blockMainStartY != value)
                {
                    _blockMainStartY = value;
                    OnPropertyChanged("BlockMainStartY");
                }
            }

        }

        public Single BlockMainStartZ
        {
            get { return _blockMainStartZ; }
            private set
            {
                if (_blockMainStartZ != value)
                {
                    _blockMainStartZ = value;
                    OnPropertyChanged("BlockMainStartZ");
                }
            }
        }

        public Boolean IsXMerchandisedAsTrays
        {
            get { return _isXMerchandisedAsTrays; }
            private set
            {
                if (_isXMerchandisedAsTrays != value)
                {
                    _isXMerchandisedAsTrays = value;
                    OnPropertyChanged("IsXMerchandisedAsTrays");
                }
            }
        }

        public PlanogramProductOrientationType UnitXOrientationType
        {
            get { return _unitXOrientationType; }
            private set
            {
                if (_unitXOrientationType != value)
                {
                    _unitXOrientationType = value;
                    OnPropertyChanged("UnitXOrientationType");
                }
            }
        }

        public Single UnitXHeight
        {
            get { return _unitXHeight; }
            private set
            {
                if (_unitXHeight != value)
                {
                    _unitXHeight = value;
                    OnPropertyChanged("UnitXHeight");
                }
            }
        }

        public Single UnitXWidth
        {
            get { return _unitXWidth; }
            private set
            {
                if (_unitXWidth != value)
                {
                    _unitXWidth = value;
                    OnPropertyChanged("UnitXWidth");
                }
            }
        }

        public Single UnitXDepth
        {
            get { return _unitXDepth; }
            private set
            {
                if (_unitXDepth != value)
                {
                    _unitXDepth = value;
                    OnPropertyChanged("UnitXDepth");
                }
            }
        }

        public Single BlockXStartX
        {
            get { return _blockXStartX; }
            private set
            {
                if (_blockXStartX != value)
                {
                    _blockXStartX = value;
                    OnPropertyChanged("BlockXStartX");
                }
            }
        }

        public Single BlockXStartY
        {
            get { return _blockXStartY; }
            private set
            {
                if (_blockXStartY != value)
                {
                    _blockXStartY = value;
                    OnPropertyChanged("BlockXStartY");
                }
            }
        }

        public Single BlockXStartZ
        {
            get { return _blockXStartZ; }
            private set
            {
                if (_blockXStartZ != value)
                {
                    _blockXStartZ = value;
                    OnPropertyChanged("BlockXStartZ");
                }
            }
        }

        public Boolean IsYMerchandisedAsTrays
        {
            get { return _isYMerchandisedAsTrays; }
            private set
            {
                if (_isYMerchandisedAsTrays != value)
                {
                    _isYMerchandisedAsTrays = value;
                    OnPropertyChanged("IsYMerchandisedAsTrays");
                }
            }
        }

        public PlanogramProductOrientationType UnitYOrientationType
        {
            get { return _unitYOrientationType; }
            private set
            {
                if (_unitYOrientationType != value)
                {
                    _unitYOrientationType = value;
                    OnPropertyChanged("UnitYOrientationType");
                }
            }
        }

        public Single UnitYHeight
        {
            get { return _unitYHeight; }
            private set
            {
                if (_unitYHeight != value)
                {
                    _unitYHeight = value;
                    OnPropertyChanged("UnitYHeight");
                }
            }
        }

        public Single UnitYWidth
        {
            get { return _unitYWidth; }
            private set
            {
                if (_unitYWidth != value)
                {
                    _unitYWidth = value;
                    OnPropertyChanged("UnitYWidth");
                }
            }
        }

        public Single UnitYDepth
        {
            get { return _unitYDepth; }
            private set
            {
                if (_unitYDepth != value)
                {
                    _unitYDepth = value;
                    OnPropertyChanged("UnitYDepth");
                }
            }
        }

        public Single BlockYStartX
        {
            get { return _blockYStartX; }
            private set
            {
                if (_blockYStartX != value)
                {
                    _blockYStartX = value;
                    OnPropertyChanged("BlockYStartX");
                }
            }
        }

        public Single BlockYStartY
        {
            get { return _blockYStartY; }
            private set
            {
                if (_blockYStartY != value)
                {
                    _blockYStartY = value;
                    OnPropertyChanged("BlockYStartY");
                }
            }
        }

        public Single BlockYStartZ
        {
            get { return _blockYStartZ; }
            private set
            {
                if (_blockYStartZ != value)
                {
                    _blockYStartZ = value;
                    OnPropertyChanged("BlockYStartZ");
                }
            }
        }

        public Boolean IsZMerchandisedAsTrays
        {
            get { return _isZMerchandisedAsTrays; }
            private set
            {
                if (_isZMerchandisedAsTrays != value)
                {
                    _isZMerchandisedAsTrays = value;
                    OnPropertyChanged("IsZMerchandisedAsTrays");
                }
            }
        }

        public PlanogramProductOrientationType UnitZOrientationType
        {
            get { return _unitZOrientationType; }
            private set
            {
                if (_unitZOrientationType != value)
                {
                    _unitZOrientationType = value;
                    OnPropertyChanged("UnitZOrientationType");
                }
            }
        }

        public Single UnitZHeight
        {
            get { return _unitZHeight; }
            private set
            {
                if (_unitZHeight != value)
                {
                    _unitZHeight = value;
                    OnPropertyChanged("UnitZHeight");
                }
            }
        }

        public Single UnitZWidth
        {
            get { return _unitZWidth; }
            private set
            {
                if (_unitZWidth != value)
                {
                    _unitZWidth = value;
                    OnPropertyChanged("UnitZWidth");
                }
            }
        }

        public Single UnitZDepth
        {
            get { return _unitZDepth; }
            private set
            {
                if (_unitZDepth != value)
                {
                    _unitZDepth = value;
                    OnPropertyChanged("UnitZDepth");
                }
            }
        }

        public Single BlockZStartX
        {
            get { return _blockZStartX; }
            private set
            {
                if (_blockZStartX != value)
                {
                    _blockZStartX = value;
                    OnPropertyChanged("BlockZStartX");
                }
            }
        }

        public Single BlockZStartY
        {
            get { return _blockZStartY; }
            private set
            {
                if (_blockZStartY != value)
                {
                    _blockZStartY = value;
                    OnPropertyChanged("BlockZStartY");
                }
            }
        }

        public Single BlockZStartZ
        {
            get { return _blockZStartZ; }
            private set
            {
                if (_blockZStartZ != value)
                {
                    _blockZStartZ = value;
                    OnPropertyChanged("BlockZStartZ");
                }
            }
        }

        public Single FacingSpaceX
        {
            get { return _facingSpaceX; }
            private set
            {
                if (_facingSpaceX != value)
                {
                    _facingSpaceX = value;
                    OnPropertyChanged("FacingSpaceX");
                }
            }
        }

        public Single FacingSpaceY
        {
            get { return _facingSpaceY; }
            private set
            {
                if (_facingSpaceY != value)
                {
                    _facingSpaceY = value;
                    OnPropertyChanged("FacingSpaceY");
                }
            }
        }

        public Single FacingSpaceZ
        {
            get { return _facingSpaceZ; }
            private set
            {
                if (_facingSpaceZ != value)
                {
                    _facingSpaceZ = value;
                    OnPropertyChanged("FacingSpaceZ");
                }
            }
        }


        public PlanogramProductMerchandisingStyle AppliedMerchandisingStyle
        {
            get { return _appliedMerchandisingStyle; }
            private set
            {
                if (_appliedMerchandisingStyle != value)
                {
                    _appliedMerchandisingStyle = value;
                }
                OnPropertyChanged("AppliedMerchandisingStyle");
            }
        }

        public PlanogramProductMerchandisingStyle AppliedMerchandisingStyleX
        {
            get { return _appliedMerchandisingStyleX; }
            private set
            {
                if (_appliedMerchandisingStyleX != value)
                {
                    _appliedMerchandisingStyleX = value;
                }
                OnPropertyChanged("AppliedMerchandisingStyleX");
            }
        }

        public PlanogramProductMerchandisingStyle AppliedMerchandisingStyleY
        {
            get { return _appliedMerchandisingStyleY; }
            private set
            {
                if (_appliedMerchandisingStyleY != value)
                {
                    _appliedMerchandisingStyleY = value;
                }
                OnPropertyChanged("AppliedMerchandisingStyleY");
            }
        }

        public PlanogramProductMerchandisingStyle AppliedMerchandisingStyleZ
        {
            get { return _appliedMerchandisingStyleZ; }
            private set
            {
                if (_appliedMerchandisingStyleZ != value)
                {
                    _appliedMerchandisingStyleZ = value;
                }
                OnPropertyChanged("AppliedMerchandisingStyleZ");
            }
        }

        public PlanogramProductOrientationType AppliedOrientationType
        {
            get { return _appliedOrientationType; }
            private set
            {
                if (_appliedOrientationType != value)
                {
                    _appliedOrientationType = value;
                }
                OnPropertyChanged("AppliedOrientationType");
            }
        }

        public PlanogramProductOrientationType AppliedOrientationTypeX
        {
            get { return _appliedOrientationTypeX; }
            private set
            {
                if (_appliedOrientationTypeX != value)
                {
                    _appliedOrientationTypeX = value;
                }
                OnPropertyChanged("AppliedOrientationTypeX");
            }
        }

        public PlanogramProductOrientationType AppliedOrientationTypeY
        {
            get { return _appliedOrientationTypeY; }
            private set
            {
                if (_appliedOrientationTypeY != value)
                {
                    _appliedOrientationTypeY = value;
                }
                OnPropertyChanged("AppliedOrientationTypeY");
            }
        }

        public PlanogramProductOrientationType AppliedOrientationTypeZ
        {
            get { return _appliedOrientationTypeZ; }
            private set
            {
                if (_appliedOrientationTypeZ != value)
                {
                    _appliedOrientationTypeZ = value;
                }
                OnPropertyChanged("AppliedOrientationTypeZ");
            }
        }

        /// <summary>
        /// Gets the pegx value to use for this position
        /// </summary>
        public Single PegX
        {
            get { return _pegX; }
            private set
            {
                if (_pegX != value)
                {
                    _pegX = value;
                    OnPropertyChanged("PegX");
                }
            }
        }

        /// <summary>
        /// Gets the pegx value to use for this position
        /// </summary>
        public Single PegY
        {
            get { return _pegY; }
            private set
            {
                if (_pegY != value)
                {
                    _pegY = value;
                    OnPropertyChanged("PegY");
                }
            }
        }

        /// <summary>
        /// Gets the pegx value to use for this position
        /// </summary>
        public Single PegX2
        {
            get { return _pegX2; }
            private set
            {
                if (_pegX2 != value)
                {
                    _pegX2 = value;
                    OnPropertyChanged("PegX2");
                }
            }
        }

        /// <summary>
        /// Gets the pegx value to use for this position
        /// </summary>
        public Single PegY2
        {
            get { return _pegY2; }
            private set
            {
                if (_pegY2 != value)
                {
                    _pegY2 = value;
                    OnPropertyChanged("PegY2");
                }
            }
        }

        /// <summary>
        /// Gets the pegx value to use for this position
        /// </summary>
        public Single PegX3
        {
            get { return _pegX3; }
            private set
            {
                if (_pegX3 != value)
                {
                    _pegX3 = value;
                    OnPropertyChanged("PegX3");
                }
            }
        }

        /// <summary>
        /// Gets the pegx value to use for this position
        /// </summary>
        public Single PegY3
        {
            get { return _pegY3; }
            private set
            {
                if (_pegY3 != value)
                {
                    _pegY3 = value;
                    OnPropertyChanged("PegY3");
                }
            }
        }


        #endregion

        #region Model Properties

        public Single X
        {
            get { return _model.X; }
            set
            {
                Debug.Assert(value != Single.NaN, "Pos X set as NAN");
                if (value != Single.NaN
                    && value != _model.X)
                {
                    _model.X = value;
                }
            }
        }
        public Single Y
        {
            get { return _model.Y; }
            set
            {
                Debug.Assert(value != Single.NaN, "Pos Y set as NAN");
                if (value != Single.NaN
                    && value != _model.Y)
                {
                    _model.Y = value;
                }
            }
        }
        public Single Z
        {
            get { return _model.Z; }
            set
            {
                Debug.Assert(value != Single.NaN, "Pos Z set as NAN");
                if (value != Single.NaN
                    && value != _model.Z)
                {
                    _model.Z = value;
                }
            }
        }

        //NB- Rotation values are currently not in use.
        //public Single Angle
        //{
        //    get { return _model.Angle; }
        //    set { _model.Angle = value; }
        //}
        //public Single Slope
        //{
        //    get { return _model.Slope; }
        //    set { _model.Slope = value; }
        //}
        //public Single Roll
        //{
        //    get { return _model.Roll; }
        //    set { _model.Roll = value; }
        //}

        public Int16 FacingsHigh
        {
            get { return Model.FacingsHigh; }
            set
            {
                Model.FacingsHigh = (value > 0) ? value : (Byte)1;
                OnFacingsChanged(AxisType.Y);
            }
        }
        public Int16 FacingsWide
        {
            get { return Model.FacingsWide; }
            set
            {
                Model.FacingsWide = (value > 0) ? value : (Byte)1;
                OnFacingsChanged(AxisType.X);
            }
        }
        public Int16 FacingsDeep
        {
            get { return Model.FacingsDeep; }
            set
            {
                Model.FacingsDeep = (value > 0) ? value : (Byte)1;
                OnFacingsChanged(AxisType.Z);
            }
        }
        public PlanogramPositionOrientationType OrientationType
        {
            get { return Model.OrientationType; }
            set { Model.OrientationType = value; }
        }
        public PlanogramPositionMerchandisingStyle MerchandisingStyle
        {
            get { return _model.MerchandisingStyle; }
            set { _model.MerchandisingStyle = value; }
        }

        public Int16 FacingsXHigh
        {
            get { return Model.FacingsXHigh; }
            set { Model.FacingsXHigh = value; }
        }
        public Int16 FacingsXWide
        {
            get { return Model.FacingsXWide; }
            set { Model.FacingsXWide = value; }
        }
        public Int16 FacingsXDeep
        {
            get { return Model.FacingsXDeep; }
            set { Model.FacingsXDeep = value; }
        }
        public PlanogramPositionOrientationType OrientationTypeX
        {
            get { return Model.OrientationTypeX; }
            set { Model.OrientationTypeX = value; }
        }
        public PlanogramPositionMerchandisingStyle MerchandisingStyleX
        {
            get { return Model.MerchandisingStyleX; }
            set { Model.MerchandisingStyleX = value; }
        }
        public Boolean IsXPlacedLeft
        {
            get { return Model.IsXPlacedLeft; }
            set { Model.IsXPlacedLeft = value; }
        }

        public Int16 FacingsYHigh
        {
            get { return Model.FacingsYHigh; }
            set { Model.FacingsYHigh = value; }
        }
        public Int16 FacingsYWide
        {
            get { return Model.FacingsYWide; }
            set { Model.FacingsYWide = value; }
        }
        public Int16 FacingsYDeep
        {
            get { return Model.FacingsYDeep; }
            set { Model.FacingsYDeep = value; }
        }
        public PlanogramPositionOrientationType OrientationTypeY
        {
            get { return Model.OrientationTypeY; }
            set { Model.OrientationTypeY = value; }
        }
        public PlanogramPositionMerchandisingStyle MerchandisingStyleY
        {
            get { return Model.MerchandisingStyleY; }
            set { Model.MerchandisingStyleY = value; }
        }
        public Boolean IsYPlacedBottom
        {
            get { return Model.IsYPlacedBottom; }
            set { Model.IsYPlacedBottom = value; }
        }

        public Int16 FacingsZHigh
        {
            get { return Model.FacingsZHigh; }
            set { Model.FacingsZHigh = value; }
        }
        public Int16 FacingsZWide
        {
            get { return Model.FacingsZWide; }
            set { Model.FacingsZWide = value; }
        }
        public Int16 FacingsZDeep
        {
            get { return Model.FacingsZDeep; }
            set { Model.FacingsZDeep = value; }
        }
        public PlanogramPositionOrientationType OrientationTypeZ
        {
            get { return Model.OrientationTypeZ; }
            set { Model.OrientationTypeZ = value; }
        }
        public PlanogramPositionMerchandisingStyle MerchandisingStyleZ
        {
            get { return Model.MerchandisingStyleZ; }
            set { Model.MerchandisingStyleZ = value; }
        }
        public Boolean IsZPlacedFront
        {
            get { return Model.IsZPlacedFront; }
            set { Model.IsZPlacedFront = value; }
        }


        public Int16 Sequence
        {
            get { return Model.Sequence; }
            set
            {
                if (Model.Sequence != value)
                {
                    Model.Sequence = value;
                }
            }
        }
        public Int16 SequenceX
        {
            get { return Model.SequenceX; }
            set { Model.SequenceX = value; }
        }
        public Int16 SequenceY
        {
            get { return Model.SequenceY; }
            set { Model.SequenceY = value; }
        }
        public Int16 SequenceZ
        {
            get { return Model.SequenceZ; }
            set { Model.SequenceZ = value; }
        }

        public Int16 UnitsHigh
        {
            get { return _model.UnitsHigh; }
            private set
            {
                if (_model.UnitsHigh != value)
                {
                    _model.UnitsHigh = value;
                }
            }
        }
        public Int16 UnitsWide
        {
            get { return _model.UnitsWide; }
            private set
            {
                if (_model.UnitsWide != value)
                {
                    _model.UnitsWide = value;
                }
            }
        }
        public Int16 UnitsDeep
        {
            get { return _model.UnitsDeep; }
            private set
            {
                if (_model.UnitsDeep != value)
                {
                    _model.UnitsDeep = value;
                }
            }
        }
        public Int32 TotalUnits
        {
            get { return _model.TotalUnits; }
            private set
            {
                if (_model.TotalUnits != value)
                {
                    _model.TotalUnits = value;
                }
            }
        }

        public Int16? PositionSequenceNumber
        {
            get
            {
                return Model.PositionSequenceNumber;
            }
            set
            {
                Model.PositionSequenceNumber = value;
            }
        }

        public Single HorizontalSqueeze
        {
            get { return Model.HorizontalSqueeze; }
        }
        public Single VerticalSqueeze
        {
            get { return Model.VerticalSqueeze; }
        }
        public Single DepthSqueeze
        {
            get { return Model.DepthSqueeze; }
        }

        public Single HorizontalSqueezeX
        {
            get { return Model.HorizontalSqueezeX; }
        }
        public Single VerticalSqueezeX
        {
            get { return Model.VerticalSqueezeX; }
        }
        public Single DepthSqueezeX
        {
            get { return Model.DepthSqueezeX; }
        }

        public Single HorizontalSqueezeY
        {
            get { return Model.HorizontalSqueezeY; }
        }
        public Single VerticalSqueezeY
        {
            get { return Model.VerticalSqueezeY; }
        }
        public Single DepthSqueezeY
        {
            get { return Model.DepthSqueezeY; }
        }

        public Single HorizontalSqueezeZ
        {
            get { return Model.HorizontalSqueezeZ; }
        }
        public Single VerticalSqueezeZ
        {
            get { return Model.VerticalSqueezeZ; }
        }
        public Single DepthSqueezeZ
        {
            get { return Model.DepthSqueezeZ; }
        }

        public Boolean IsManuallyPlaced
        {
            get { return Model.IsManuallyPlaced; }
            set { Model.IsManuallyPlaced = value; }
        }

        public Int32? SequenceNumber
        {
            get { return Model.SequenceNumber; }
        }

        public Int32? SequenceColour
        {
            get { return Model.SequenceColour; }
        }

        #endregion

        #region Meta Properties

        /// <summary>
        /// Returns the MetaAchievedCases for each position.
        /// </summary>
        public Single? MetaAchievedCases
        {
            get { return _model.MetaAchievedCases; }
        }

        /// <summary>
        /// Returns the MetaFrontFacingsWide for each position.
        /// </summary>
        public Int16? MetaFrontFacingsWide
        {
            get { return _model.MetaFrontFacingsWide; }
        }

        /// <summary>
        /// Returns the MetaIsPositionCollisions for each position.
        /// </summary>
        public Boolean? MetaIsPositionCollisions
        {
            get { return _model.MetaIsPositionCollisions; }
        }

        /// <summary>
        /// Returns the MetaIsUnderMinDeep for each position.
        /// </summary>
        public Boolean? MetaIsUnderMinDeep
        {
            get { return _model.MetaIsUnderMinDeep; }
        }

        /// <summary>
        /// Returns the MetaIsOverMaxDeep for each position.
        /// </summary>
        public Boolean? MetaIsOverMaxDeep
        {
            get { return _model.MetaIsOverMaxDeep; }
        }

        /// <summary>
        /// Returns the MetaIsOverMaxRightCap for each position.
        /// </summary>
        public Boolean? MetaIsOverMaxRightCap
        {
            get { return _model.MetaIsOverMaxRightCap; }
        }

        /// <summary>
        /// Returns the MetaIsOverMaxStack for each position.
        /// </summary>
        public Boolean? MetaIsOverMaxStack
        {
            get { return _model.MetaIsOverMaxStack; }
        }

        /// <summary>
        /// Returns the MetaIsOverMaxTopCap for each position.
        /// </summary>
        public Boolean? MetaIsOverMaxTopCap
        {
            get { return _model.MetaIsOverMaxTopCap; }
        }

        /// <summary>
        /// Returns the MetaIsInvalidMerchandisingStyle for each position.
        /// </summary>
        public Boolean? MetaIsInvalidMerchandisingStyle
        {
            get { return _model.MetaIsInvalidMerchandisingStyle; }
        }

        /// <summary>
        /// Returns the MetaIsHangingTray for each position.
        /// </summary>
        public Boolean? MetaIsHangingTray
        {
            get { return _model.MetaIsHangingTray; }
        }

        /// <summary>
        /// Returns the MetaIsPegOverfilled for each position.
        /// </summary>
        public Boolean? MetaIsPegOverfilled
        {
            get { return _model.MetaIsPegOverfilled; }
        }

        /// <summary>
        /// Returns the MetaIsOutsideMerchandisingSpacee for each position.
        /// </summary>
        public Boolean? MetaIsOutsideMerchandisingSpace
        {
            get { return _model.MetaIsOutsideMerchandisingSpace; }
        }

        /// <summary>
        /// Returns the MetaIsOutsideOfBlockSpace for each position.
        /// </summary>
        public Boolean? MetaIsOutsideOfBlockSpace
        {
            get { return _model.MetaIsOutsideOfBlockSpace; }
        }

        public Single? MetaHeight
        {
            get { return _model.MetaHeight; }
        }

        public Single? MetaWidth
        {
            get { return _model.MetaWidth; }
        }

        public Single? MetaDepth
        {
            get { return _model.MetaDepth; }
        }

        /// <summary>
        /// Gets/Sets the X coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldX
        {
            get { return _worldCoordinates.X; }
            set { SetWorldCoordinates(value, MetaWorldY, MetaWorldZ); }
        }

        /// <summary>
        /// Gets/Sets the Y coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldY
        {
            get { return _worldCoordinates.Y; }
            set { SetWorldCoordinates(MetaWorldX, value, MetaWorldZ); }
        }

        /// <summary>
        /// Gets/Sets the Z coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldZ
        {
            get { return _worldCoordinates.Z; }
            set { SetWorldCoordinates(MetaWorldX, MetaWorldY, value); }
        }

        public Single? MetaTotalLinearSpace
        {
            get { return _model.MetaTotalLinearSpace; }
        }

        public Single? MetaTotalAreaSpace
        {
            get { return _model.MetaTotalAreaSpace; }
        }

        public Single? MetaTotalVolumetricSpace
        {
            get { return _model.MetaTotalVolumetricSpace; }
        }

        public Single? MetaPlanogramLinearSpacePercentage
        {
            get { return _model.MetaPlanogramLinearSpacePercentage; }
        }

        public Single? MetaPlanogramAreaSpacePercentage
        {
            get { return _model.MetaPlanogramAreaSpacePercentage; }
        }

        public Single? MetaPlanogramVolumetricSpacePercentage
        {
            get { return _model.MetaPlanogramVolumetricSpacePercentage; }
        }

        public String MetaSequenceSubGroupName
        {
            get { return _model.MetaSequenceSubGroupName; }
        }

        public PlanogramItemComparisonStatusType MetaComparisonStatus
        {
            get { return _model.MetaComparisonStatus; }
        }

        public Int32? MetaPegRowNumber
        {
            get { return _model.MetaPegRowNumber; }
        }

        public Int32? MetaPegColumnNumber
        {
            get { return _model.MetaPegColumnNumber; }
        }

        #endregion

        #region ParentSubComponentView
        /// <summary>
        /// Returns the parent subcomponent viewmodel.
        /// </summary>
        protected internal PlanogramSubComponentViewModelBase ParentSubComponentView
        {
            get { return _parentSubComponentView; }
        }
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="position"></param>
        public PlanogramPositionViewModelBase(PlanogramSubComponentViewModelBase parentSubComponentView, PlanogramPosition position)
        {
            _parentSubComponentView = parentSubComponentView;

            _model = position;
            _model.PropertyChanging += Model_PropertyChanging;
            _model.PropertyChanged += Model_PropertyChanged;

            //get the linked prouct
            this.ReferencedProduct = _model.GetPlanogramProduct();

            UpdateCalculatedInfo();
            UpdateWorldCoordinates();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(e.PropertyName);
        }
        protected virtual void OnModelPropertyChanging(String propertyName)
        {
            if (typeof(PlanogramPositionViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(e.PropertyName);
        }
        protected virtual void OnModelPropertyChanged(String propertyName)
        {
            if (_suppressModelPropertyChanged) return;

            _suppressModelPropertyChanged = true;

            if (typeof(PlanogramPositionViewModelBase).GetProperty(propertyName) != null)
            {
                //relay out.
                OnPropertyChanged(propertyName);

                //[V8-29291] Don't update calculated info for meta property changes
                if (!propertyName.StartsWith("Meta"))
                {
                    UpdateCalculatedInfo();
                }
            }


            switch (propertyName)
            {
                case "PlanogramProductId":
                    {
                        //update the planogram product reference
                        this.ReferencedProduct = _model.GetPlanogramProduct();
                        if (this.ReferencedProduct != null)
                        {
                            UpdateCalculatedInfo();
                        }
                    }
                    break;

                case "MerchandisingStyle":
                case "MerchandisingStyleX":
                case "MerchandisingStyleY":
                case "MerchandisingStyleZ":
                    RefreshImages();
                    break;

                case "X":
                case "Y":
                case "Z":
                    UpdateWorldCoordinates();
                    break;
            }

            _suppressModelPropertyChanged = false;

        }


        /// <summary>
        /// Called whenever the parent changes.
        /// </summary>
        protected override void OnParentChanged(object oldValue, object newValue)
        {
            base.OnParentChanged(oldValue, newValue);

            //Hook into subcomponent property changed:

            PlanogramSubComponentViewModelBase oldParent = oldValue as PlanogramSubComponentViewModelBase;
            if (oldParent != null)
            {
                oldParent.PropertyChanged -= Parent_PropertyChanged;
            }


            PlanogramSubComponentViewModelBase newParent = newValue as PlanogramSubComponentViewModelBase;
            if (newParent != null)
            {
                newParent.PropertyChanged += Parent_PropertyChanged;
            }
        }

        /// <summary>
        /// Called whenever a property changes on the parent subcomponent.
        /// </summary>
        private void Parent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.IsDisposed) return;
            OnPlanogramSubComponentPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Called whenever a property changes on the parent subcomponent.
        /// </summary>
        protected virtual void OnPlanogramSubComponentPropertyChanged(String propertyName)
        {
            switch (propertyName)
            {
                case "WorldX":
                case "WorldY":
                case "WorldZ":
                case "MetaWorldX":
                case "MetaWorldY":
                case "MetaWorldZ":
                case "WorldAngle":
                case "WorldSlope":
                case "WorldRoll":
                case "MetaWorldAngle":
                case "MetaWorldSlope":
                case "MetaWorldRoll":
                    UpdateWorldCoordinates();
                    break;

                case "IsDividerObstructionByFacing":
                case "DividerObstructionDepth":
                case "DividerObstructionHeight":
                case "DividerObstructionWidth":
                case "MerchandisingType":
                    UpdateCalculatedInfo();
                    break;

                case "MerchConstraintRow1Height":
                case "MerchConstraintRow1SpacingX":
                case "MerchConstraintRow1SpacingY":
                case "MerchConstraintRow1StartX":
                case "MerchConstraintRow1StartY":
                case "MerchConstraintRow1Width":
                case "MerchConstraintRow2Height":
                case "MerchConstraintRow2SpacingX":
                case "MerchConstraintRow2SpacingY":
                case "MerchConstraintRow2StartX":
                case "MerchConstraintRow2StartY":
                case "MerchConstraintRow2Width":
                    if (ParentSubComponentView.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                    {
                        UpdateCalculatedInfo();
                    }
                    break;
            }
        }



        /// <summary>
        /// Called whenever the referenced product changes.
        /// </summary>
        private void OnReferencedProductChanged(PlanogramProduct oldValue, PlanogramProduct newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= ReferencedProduct_PropertyChanged;
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += ReferencedProduct_PropertyChanged;
            }
        }

        /// <summary>
        /// Called whenever a property changes on the referenced product.
        /// </summary>
        private void ReferencedProduct_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.IsDisposed) return;

            //confirm this is still valid.
            if (!Object.Equals(this.Model.PlanogramProductId, ((PlanogramProduct)sender).Id))
            {
                this.ReferencedProduct = this.Model.GetPlanogramProduct();
                return;
            }

            //handle the change
            OnPlanogramProductPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Called whenever a property changes on the referenced product.
        /// </summary>
        protected virtual void OnPlanogramProductPropertyChanged(String propertyName)
        {
            if (propertyName.Contains("Image") && propertyName != "IsLoadingImages")
            {
                RefreshImages();
            }
            else if (propertyName == "MerchandisingStyle")
            {
                UpdateCalculatedInfo();
                RefreshImages();
            }
            else if (propertyName != "IsUpdating"
                && propertyName != "CustomAttributes"
                && propertyName != "CustomAttributesAsync"
                && !propertyName.StartsWith("Meta"))
            {
                UpdateCalculatedInfo();
            }
        }



        /// <summary>
        /// Updates capping when a facings value changes
        /// </summary>
        private void OnFacingsChanged(AxisType axis)
        {
            PlanogramPosition position = this.Model;
            PlanogramProduct product = this.ReferencedProduct;

            if (position == null || product == null) return;
            if (this.ParentSubComponentView == null) return;

            PlanogramSubComponentPlacement subPlacement = this.ParentSubComponentView.ModelPlacement;
            if (subPlacement == null) return;

            Planogram planogram = position.Parent;
            if (planogram == null) return;


            switch (axis)
            {
                case AxisType.X:
                    {
                        //only process if fill wide is off
                        if (planogram.ProductPlacementX == PlanogramProductPlacementXType.FillWide
                            && !position.IsManuallyPlaced) return;

                        //call method to update the capping
                        PlanogramPositionDetails details = position.GetPositionDetails(subPlacement);
                        position.UpdateCapsToMainBlockWidth(product, subPlacement, ref details);
                    }
                    break;

                case AxisType.Y:
                    {
                        //only process if fill high is off
                        if (planogram.ProductPlacementY == PlanogramProductPlacementYType.FillHigh
                            && !position.IsManuallyPlaced) return;


                        //call method to update capping.
                        PlanogramPositionDetails details = position.GetPositionDetails(subPlacement);
                        position.UpdateCapsToMainBlockHeight(product, subPlacement, ref details);
                    }
                    break;

                case AxisType.Z:
                    {
                        //only process if fill deep is off
                        if (planogram.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep
                            && !position.IsManuallyPlaced) return;


                        //call method to update capping.
                        PlanogramPositionDetails details = position.GetPositionDetails(subPlacement);
                        position.UpdateCapsToMainBlockDepth(product, subPlacement, ref details);
                    }
                    break;
            }

            position.CleanUpUnusedCapValues();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the <see cref="PlanogramSequenceGroupSubGroup"/> that this position belongs to, or null
        /// if it isn't a member of any sub-group.
        /// </summary>
        /// <returns></returns>
        public PlanogramSequenceGroupSubGroup GetSequenceSubGroup()
        {
            if (this.ParentPlanogramView == null) return null;
            if (!this.SequenceColour.HasValue) return null;
            PlanogramSequenceGroup sequenceGroup = ParentPlanogramView.Model.Sequence.Groups
                .FirstOrDefault(g => g.Colour.Equals(SequenceColour.Value));
            if (sequenceGroup == null) return null;
            PlanogramProduct product = Model.GetPlanogramProduct();
            if (product == null) return null;
            return sequenceGroup.SubGroups.FirstOrDefault(g => g.EnumerateProducts().Select(p => p.Gtin).Contains(product.Gtin));
        }

        /// <summary>
        /// Updates the height, width and depth values for this position.
        /// </summary>
        private void UpdateCalculatedInfo()
        {
            if (_isUpdatingCalculatedInfo) return;

            _isUpdatingCalculatedInfo = true;

            PlanogramProduct product = this.ReferencedProduct;

            //check we have the correct product model.
            if (product == null
                || !Object.Equals(product.Id, this.Model.PlanogramProductId)
                || product.IsDeleted)
            {
                this.ReferencedProduct = Model.GetPlanogramProduct();
                product = this.ReferencedProduct;
            }
            if (product == null) return;



            BeginUpdate();


            //recalculate all render data for the position.
            PlanogramPositionDetails info = this.Model.GetPositionDetails(product, _parentSubComponentView.ModelPlacement);

            //general
            this.Height = info.TotalSize.Height;
            this.Width = info.TotalSize.Width;
            this.Depth = info.TotalSize.Depth;

            this.FacingSpaceX = info.FacingSize.Width;
            this.FacingSpaceY = info.FacingSize.Height;
            this.FacingSpaceZ = info.FacingSize.Depth;

            this.UnitsHigh = info.TotalUnits.High;
            this.UnitsWide = info.TotalUnits.Wide;
            this.UnitsDeep = info.TotalUnits.Deep;
            this.TotalUnits = info.TotalUnitCount;



            //Main block
            this.IsMerchandisedAsTrays = (info.MainMerchStyle == PlanogramProductMerchandisingStyle.Tray);
            this.UnitOrientationType = info.MainOrientationType;
            this.UnitHeight = info.MainUnitSize.Height;
            this.UnitWidth = info.MainUnitSize.Width;
            this.UnitDepth = info.MainUnitSize.Depth;
            this.BlockMainStartX = info.MainCoordinates.X;
            this.BlockMainStartY = info.MainCoordinates.Y;
            this.BlockMainStartZ = info.MainCoordinates.Z;



            //X block
            this.IsXMerchandisedAsTrays = (info.XMerchStyle == PlanogramProductMerchandisingStyle.Tray);
            this.UnitXOrientationType = info.XOrientationType;
            this.UnitXHeight = info.XUnitSize.Height;
            this.UnitXWidth = info.XUnitSize.Width;
            this.UnitXDepth = info.XUnitSize.Depth;
            this.BlockXStartX = info.XCoordinates.X;
            this.BlockXStartY = info.XCoordinates.Y;
            this.BlockXStartZ = info.XCoordinates.Z;


            //y block
            this.IsYMerchandisedAsTrays = (info.YMerchStyle == PlanogramProductMerchandisingStyle.Tray);
            this.UnitYOrientationType = info.YOrientationType;
            this.UnitYHeight = info.YUnitSize.Height;
            this.UnitYWidth = info.YUnitSize.Width;
            this.UnitYDepth = info.YUnitSize.Depth;
            this.BlockYStartX = info.YCoordinates.X;
            this.BlockYStartY = info.YCoordinates.Y;
            this.BlockYStartZ = info.YCoordinates.Z;

            //Z block
            this.IsZMerchandisedAsTrays = (info.ZMerchStyle == PlanogramProductMerchandisingStyle.Tray);
            this.UnitZOrientationType = info.ZOrientationType;
            this.UnitZHeight = info.ZUnitSize.Height;
            this.UnitZWidth = info.ZUnitSize.Width;
            this.UnitZDepth = info.ZUnitSize.Depth;
            this.BlockZStartX = info.ZCoordinates.X;
            this.BlockZStartY = info.ZCoordinates.Y;
            this.BlockZStartZ = info.ZCoordinates.Z;


            //applied merch styles
            this.AppliedMerchandisingStyle = this.Model.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Main);
            this.AppliedMerchandisingStyleX = this.Model.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.X);
            this.AppliedMerchandisingStyleY = this.Model.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Y);
            this.AppliedMerchandisingStyleZ = this.Model.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Z);

            //applied orientation types
            this.AppliedOrientationType = this.Model.GetAppliedOrientationType(product, PlanogramPositionBlockType.Main);
            this.AppliedOrientationTypeX = this.Model.GetAppliedOrientationType(product, PlanogramPositionBlockType.X);
            this.AppliedOrientationTypeY = this.Model.GetAppliedOrientationType(product, PlanogramPositionBlockType.Y);
            this.AppliedOrientationTypeZ = this.Model.GetAppliedOrientationType(product, PlanogramPositionBlockType.Z);


            //Peg
            Single pegX, pegY, pegX2, pegY2, pegX3, pegY3;
            PlanogramPositionDetails.GetProductPegXPegY(product, info, this.Model.Parent, out pegX, out pegY, out pegX2, out pegY2, out pegX3, out pegY3);
            this.PegX = pegX;
            this.PegY = pegY;
            this.PegX2 = pegX2;
            this.PegY2 = pegY2;
            this.PegX3 = pegX3;
            this.PegY3 = pegY3;

            EndUpdate();
            _isUpdatingCalculatedInfo = false;
        }

        /// <summary>
        /// For IDataErrorInfo
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            return ((IDataErrorInfo)this.Model)[columnName];
        }

        /// <summary>
        /// Returns the label text to be displayed based on the 
        /// given format string
        /// </summary>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public abstract String GetLabelText(String formatString);

        /// <summary>
        /// Returns the value of the given field for this item.
        /// </summary>
        public virtual Object GetFieldValue(ObjectFieldInfo fieldInfo)
        {
            return fieldInfo.GetValue(this);
        }

        #region World Space Methods

        /// <summary>
        /// Updates the values of the world coordinates
        /// </summary>
        private void UpdateWorldCoordinates()
        {
            PointValue oldCoords = _worldCoordinates;

            //get the world coordinates and rotation
            _worldCoordinates =
                    this.Model.GetPlanogramRelativeCoordinates(
                        this.ParentSubComponentView.ModelPlacement);

            //nb - rotation is currently not in use.


            //fire off property changed events
            if (oldCoords.X != _worldCoordinates.X) OnPropertyChanged(PlanogramPosition.MetaWorldXProperty.Name);
            if (oldCoords.Y != _worldCoordinates.Y) OnPropertyChanged(PlanogramPosition.MetaWorldYProperty.Name);
            if (oldCoords.Z != _worldCoordinates.Z) OnPropertyChanged(PlanogramPosition.MetaWorldZProperty.Name);
        }

        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(Single newWorldX, Single newWorldY, Single newWorldZ)
        {
            SetWorldCoordinates(new PointValue(newWorldX, newWorldY, newWorldZ));
        }

        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(PointValue newCoordinates)
        {
            this.Model.SetCoordinatesFromPlanogramRelative(
                   newCoordinates, this.ParentSubComponentView.ModelPlacement);
        }

        #endregion


        /// <summary>
        /// Returns the list of plan item fields for this plan item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields()
        {
            foreach (var field in PlanogramPosition.EnumerateDisplayableFieldInfos())
            {
                yield return field;
            }
        }

        /// <summary>
        /// Fires of property changed notifications for all image rendering properties.
        /// </summary>
        protected internal void RefreshImages()
        {
            if (this.IsDisposed) return;

            BeginUpdate();

            //Main:
            OnPropertyChanged("FrontImageData");
            OnPropertyChanged("BackImageData");
            OnPropertyChanged("TopImageData");
            OnPropertyChanged("BottomImageData");
            OnPropertyChanged("LeftImageData");
            OnPropertyChanged("RightImageData");

            //X
            // -only fire if the block is active
            if (this.FacingsXHigh > 0 && this.FacingsXWide > 0 && this.FacingsXDeep > 0)
            {
                OnPropertyChanged("FrontXImageData");
                OnPropertyChanged("BackXImageData");
                OnPropertyChanged("TopXImageData");
                OnPropertyChanged("BottomXImageData");
                OnPropertyChanged("LeftXImageData");
                OnPropertyChanged("RightXImageData");
            }

            //Y
            if (this.FacingsYHigh > 0 && this.FacingsYWide > 0 && this.FacingsYDeep > 0)
            {
                OnPropertyChanged("FrontYImageData");
                OnPropertyChanged("BackYImageData");
                OnPropertyChanged("TopYImageData");
                OnPropertyChanged("BottomYImageData");
                OnPropertyChanged("LeftYImageData");
                OnPropertyChanged("RightYImageData");
            }

            //Z
            if (this.FacingsZHigh > 0 && this.FacingsZWide > 0 && this.FacingsZDeep > 0)
            {
                OnPropertyChanged("FrontZImageData");
                OnPropertyChanged("BackZImageData");
                OnPropertyChanged("TopZImageData");
                OnPropertyChanged("BottomZImageData");
                OnPropertyChanged("LeftZImageData");
                OnPropertyChanged("RightZImageData");
            }

            EndUpdate();
        }

        /// <summary>
        /// Returns the view of the associated product
        /// </summary>
        /// <returns></returns>
        protected PlanogramProductViewModelBase GetProductView()
        {
           return ParentPlanogramView.FindProductView(this.Model.PlanogramProductId);
        }

        #endregion

        #region IPlanPositionRenderable Members

        public IPlanProductRenderable Product
        {
            get { throw new NotImplementedException(); }
        }

        public IPlanSubComponentRenderable ParentSubComponent
        {
            get { return _parentSubComponentView; }
        }

        protected PlanogramViewModelBase ParentPlanogramView
        {
            get
            {
                if (this.ParentSubComponentView == null) return null;
                if (this.ParentSubComponentView.ParentComponentView == null) return null;
                return this.ParentSubComponentView.ParentComponentView.ParentPlanogramView;
            }
        }

        private Boolean IsImageLoadAsync()
        {
            PlanogramViewModelBase parentPlanogramView = this.ParentPlanogramView;
            if (parentPlanogramView == null) return false;

            return parentPlanogramView.IsImageLoadAsync;
        }

        //Always return out 0 for these as they should not be populated.
        //Orientation type is used to set product rotation. These will rotate the entire 
        // position model.
        Single IPlanPositionRenderable.Slope { get { return 0; } }
        Single IPlanPositionRenderable.Angle { get { return 0; } }
        Single IPlanPositionRenderable.Roll { get { return 0; } }


        Object IPlanPositionRenderable.Id { get { return Model.Id; } }


        Byte[] IPlanPositionRenderable.FrontImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Main, PlanogramProductFaceType.Front, "FrontImageData"); }
        }
        Byte[] IPlanPositionRenderable.BackImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Main, PlanogramProductFaceType.Back, "BackImageData"); }
        }
        Byte[] IPlanPositionRenderable.TopImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Main, PlanogramProductFaceType.Top, "TopImageData"); }
        }
        Byte[] IPlanPositionRenderable.BottomImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Main, PlanogramProductFaceType.Bottom, "BottomImageData"); }
        }
        Byte[] IPlanPositionRenderable.LeftImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Main, PlanogramProductFaceType.Left, "LeftImageData"); }
        }
        Byte[] IPlanPositionRenderable.RightImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Main, PlanogramProductFaceType.Right, "RightImageData"); }
        }

        Byte[] IPlanPositionRenderable.FrontXImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.X, PlanogramProductFaceType.Front, "FrontXImageData"); }
        }
        Byte[] IPlanPositionRenderable.BackXImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.X, PlanogramProductFaceType.Back, "BackXImageData"); }
        }
        Byte[] IPlanPositionRenderable.TopXImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.X, PlanogramProductFaceType.Top, "TopXImageData"); }
        }
        Byte[] IPlanPositionRenderable.BottomXImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.X, PlanogramProductFaceType.Bottom, "BottomXImageData"); }
        }
        Byte[] IPlanPositionRenderable.LeftXImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.X, PlanogramProductFaceType.Left, "LeftXImageData"); }
        }
        Byte[] IPlanPositionRenderable.RightXImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.X, PlanogramProductFaceType.Right, "RightXImageData"); }
        }

        Byte[] IPlanPositionRenderable.FrontYImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Y, PlanogramProductFaceType.Front, "FrontYImageData"); }
        }
        Byte[] IPlanPositionRenderable.BackYImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Y, PlanogramProductFaceType.Back, "BackYImageData"); }
        }
        Byte[] IPlanPositionRenderable.TopYImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Y, PlanogramProductFaceType.Top, "TopYImageData"); }
        }
        Byte[] IPlanPositionRenderable.BottomYImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Y, PlanogramProductFaceType.Bottom, "BottomYImageData"); }
        }
        Byte[] IPlanPositionRenderable.LeftYImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Y, PlanogramProductFaceType.Left, "LeftYImageData"); }
        }
        Byte[] IPlanPositionRenderable.RightYImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Y, PlanogramProductFaceType.Right, "RightYImageData"); }
        }

        Byte[] IPlanPositionRenderable.FrontZImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Z, PlanogramProductFaceType.Front, "FrontZImageData"); }
        }
        Byte[] IPlanPositionRenderable.BackZImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Z, PlanogramProductFaceType.Back, "BackZImageData"); }
        }
        Byte[] IPlanPositionRenderable.TopZImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Z, PlanogramProductFaceType.Top, "TopZImageData"); }
        }
        Byte[] IPlanPositionRenderable.BottomZImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Z, PlanogramProductFaceType.Bottom, "BottomZImageData"); }
        }
        Byte[] IPlanPositionRenderable.LeftZImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Z, PlanogramProductFaceType.Left, "LeftZImageData"); }
        }
        Byte[] IPlanPositionRenderable.RightZImageData
        {
            get { return GetImageData(PlanogramPositionBlockType.Z, PlanogramProductFaceType.Right, "RightZImageData"); }
        }


        private Byte[] GetImageData(PlanogramPositionBlockType block, PlanogramProductFaceType face, String propertyName)
        {
            PlanogramImageType typ = PlanogramImageType.None;
            return GetImageData(block, face, propertyName, out typ);
        }
        private Byte[] GetImageData(PlanogramPositionBlockType block, PlanogramProductFaceType face, String propertyName, out PlanogramImageType imageType)
        {
            PlanogramImage img;
            imageType = PlanogramImageType.None;

            if (IsImageLoadAsync())
            {
                img = this.Model.GetImageAsync(this.ReferencedProduct,
                   block, face, OnFetchImageAsyncCompleted, propertyName, out imageType);
            }
            else
            {
                img = this.Model.GetImage(this.ReferencedProduct, block, face, out imageType);
            }
            return (img != null) ? img.ImageData : null;
        }
        private void OnFetchImageAsyncCompleted(Object userState)
        {
            String propertyName = userState as String;
            if (String.IsNullOrEmpty(propertyName)) return;

            //Application.Current.Dispatcher.Invoke(new Action(() => OnPropertyChanged(propertyName)));
            //OnPropertyChanged(propertyName);
            FirePropertyChangedOnAppDispatcher(propertyName);
        }

        /// <summary>
        /// Really shoddy thing to do, but force the property change to fire on the ui thread.
        /// </summary>
        protected abstract void FirePropertyChangedOnAppDispatcher(String propertyName);
        

        Boolean IPlanPositionRenderable.IsTrayImage(PlanogramPositionBlockType blockType, PlanogramProductFaceType faceType)
        {
            String propertyName = null;

            switch (blockType)
            {
                case PlanogramPositionBlockType.Main:
                    if (!this.IsMerchandisedAsTrays) return false;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: propertyName = "FrontImageData"; break;
                        case PlanogramProductFaceType.Back: propertyName = "BackImageData"; break;
                        case PlanogramProductFaceType.Top: propertyName = "TopImageData"; break;
                        case PlanogramProductFaceType.Bottom: propertyName = "BottomImageData"; break;
                        case PlanogramProductFaceType.Left: propertyName = "LeftImageData"; break;
                        case PlanogramProductFaceType.Right: propertyName = "RightImageData"; break;
                    }
                    break;

                case PlanogramPositionBlockType.X:
                    if (!this.IsXMerchandisedAsTrays) return false;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: propertyName = "FrontXImageData"; break;
                        case PlanogramProductFaceType.Back: propertyName = "BackXImageData"; break;
                        case PlanogramProductFaceType.Top: propertyName = "TopXImageData"; break;
                        case PlanogramProductFaceType.Bottom: propertyName = "BottomXImageData"; break;
                        case PlanogramProductFaceType.Left: propertyName = "LeftXImageData"; break;
                        case PlanogramProductFaceType.Right: propertyName = "RightXImageData"; break;
                    }
                    break;

                case PlanogramPositionBlockType.Y:
                    if (!this.IsYMerchandisedAsTrays) return false;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: propertyName = "FrontYImageData"; break;
                        case PlanogramProductFaceType.Back: propertyName = "BackYImageData"; break;
                        case PlanogramProductFaceType.Top: propertyName = "TopYImageData"; break;
                        case PlanogramProductFaceType.Bottom: propertyName = "BottomYImageData"; break;
                        case PlanogramProductFaceType.Left: propertyName = "LeftYImageData"; break;
                        case PlanogramProductFaceType.Right: propertyName = "RightYImageData"; break;
                    }
                    break;

                case PlanogramPositionBlockType.Z:
                    if (!this.IsZMerchandisedAsTrays) return false;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: propertyName = "FrontZImageData"; break;
                        case PlanogramProductFaceType.Back: propertyName = "BackZImageData"; break;
                        case PlanogramProductFaceType.Top: propertyName = "TopZImageData"; break;
                        case PlanogramProductFaceType.Bottom: propertyName = "BottomZImageData"; break;
                        case PlanogramProductFaceType.Left: propertyName = "LeftZImageData"; break;
                        case PlanogramProductFaceType.Right: propertyName = "RightZImageData"; break;
                    }
                    break;
            }

            if (propertyName == null) return false;


            PlanogramImageType imageType = PlanogramImageType.None;
            GetImageData(blockType, faceType, propertyName, out imageType);
            return imageType == PlanogramImageType.Tray;
        }

        

        #endregion

        #region IDisposable Members

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void OnDisposing(Boolean disposing)
        {
            base.OnDisposing(disposing);

            this.ReferencedProduct = null;
            _model.PropertyChanging -= Model_PropertyChanging;
            _model.PropertyChanged -= Model_PropertyChanged;
        }

        #endregion

        #region IPlanogramSubComponentSequencedItem members
        public PlanogramSubComponentPlacement GetParentSubComponentPlacement()
        {
            return ParentSubComponentView == null ? null : ParentSubComponentView.ModelPlacement;
        }

        object IPlanogramSubComponentSequencedItem.Id
        {
            get { return _model.Id; }
        }
        #endregion
    }
}
