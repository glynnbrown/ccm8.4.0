﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Base implementation of a PlanogramSubComponent viewmodel used for rendering
    /// </summary>
    public abstract class PlanogramSubComponentViewModelBase : ViewModelBase, IPlanSubComponentRenderable
    {
        #region Fields

        private PlanogramComponentViewModelBase _parentComponentView;

        private Boolean _isInitialized;

        private readonly PlanogramSubComponent _subComponent;
        private readonly PlanogramSubComponentPlacement _subComponentPlacement;

        private readonly ObservableCollection<PlanogramPositionViewModelBase> _positions = new ObservableCollection<PlanogramPositionViewModelBase>();
        private readonly ObservableCollection<PlanogramAnnotationViewModelBase> _annotations = new ObservableCollection<PlanogramAnnotationViewModelBase>();
        private readonly ObservableCollection<PlanogramSubComponentDivider> _dividers = new ObservableCollection<PlanogramSubComponentDivider>();


        private PointValue _worldCoordinates = new PointValue();
        private RotationValue _worldRotation = new RotationValue();

        private PlanogramImage _frontImage;
        private PlanogramImage _backImage;
        private PlanogramImage _topImage;
        private PlanogramImage _bottomImage;
        private PlanogramImage _leftImage;
        private PlanogramImage _rightImage;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the source PlanogramSubComponent model
        /// </summary>
        public PlanogramSubComponent Model
        {
            get { return _subComponent; }
        }

        /// <summary>
        /// Gets the source sub component placement
        /// </summary>
        public PlanogramSubComponentPlacement ModelPlacement
        {
            get { return _subComponentPlacement; }
        }

        #region Image Properties

        /// <summary>
        /// Gets/Sets the front face image
        /// </summary>
        public PlanogramImage FrontImage
        {
            get
            {
                if (_frontImage == null)
                {
                    _frontImage = GetImageLazy(this.Model.ImageIdFront);
                }
                return _frontImage;
            }
            set
            {
                _subComponent.ImageIdFront = (value != null) ? value.Id : null;
                _frontImage = null;
            }
        }

        /// <summary>
        /// Gets/Sets the back face image
        /// </summary>
        public PlanogramImage BackImage
        {
            get
            {
                if (_backImage == null)
                {
                    _backImage = GetImageLazy(this.Model.ImageIdBack);
                }
                return _backImage;
            }
            set
            {
                _subComponent.ImageIdBack = (value != null) ? value.Id : null;
                _backImage = null;
            }
        }

        /// <summary>
        /// Gets/Sets the top face image
        /// </summary>
        public PlanogramImage TopImage
        {
            get
            {
                if (_topImage == null)
                {
                    _topImage = GetImageLazy(this.Model.ImageIdTop);
                }
                return _topImage;
            }
            set
            {
                _subComponent.ImageIdTop = (value != null) ? value.Id : null;
                _topImage = null;
            }
        }

        /// <summary>
        /// Gets/Sets the bottom face image
        /// </summary>
        public PlanogramImage BottomImage
        {
            get
            {
                if (_bottomImage == null)
                {
                    _bottomImage = GetImageLazy(this.Model.ImageIdBottom);
                }
                return _bottomImage;
            }
            set
            {
                _subComponent.ImageIdBottom = (value != null) ? value.Id : null;
                _bottomImage = null;
            }
        }

        /// <summary>
        /// Gets/Sets the left face image
        /// </summary>
        public PlanogramImage LeftImage
        {
            get
            {
                if (_leftImage == null)
                {
                    _leftImage = GetImageLazy(this.Model.ImageIdLeft);
                }
                return _leftImage;
            }
            set
            {
                _subComponent.ImageIdLeft = (value != null) ? value.Id : null;
                _leftImage = null;
            }
        }

        /// <summary>
        /// Gets/Sets the right face image
        /// </summary>
        public PlanogramImage RightImage
        {
            get
            {
                if (_rightImage == null)
                {
                    _rightImage = GetImageLazy(this.Model.ImageIdRight);
                }
                return _rightImage;
            }
            set
            {
                _subComponent.ImageIdRight = (value != null) ? value.Id : null;
                _rightImage = null;
            }
        }

        #endregion

        #region Model properties

        public String Name
        {
            get { return _subComponent.Name; }
            set
            {
                _subComponent.Name = value;
            }
        }

        public Single X
        {
            get { return _subComponent.X; }
            set
            {
                _subComponent.X = value;
            }
        }
        public Single Y
        {
            get { return _subComponent.Y; }
            set
            {
                _subComponent.Y = value;
            }
        }
        public Single Z
        {
            get { return _subComponent.Z; }
            set
            {
                _subComponent.Z = value;
            }
        }

        public Single Slope
        {
            get { return _subComponent.Slope; }
            set
            {
                _subComponent.Slope = value;
            }
        }
        public Single Angle
        {
            get { return _subComponent.Angle; }
            set
            {
                _subComponent.Angle = value;
            }
        }
        public Single Roll
        {
            get { return _subComponent.Roll; }
            set
            {
                _subComponent.Roll = value;
            }
        }

        public Single Height
        {
            get { return _subComponent.Height; }
            set
            {
                _subComponent.Height = value;
            }
        }
        public Single Width
        {
            get { return _subComponent.Width; }
            set
            {
                _subComponent.Width = value;
            }
        }
        public Single Depth
        {
            get { return _subComponent.Depth; }
            set
            {
                _subComponent.Depth = value;
            }
        }

        public PlanogramSubComponentShapeType ShapeType
        {
            get { return _subComponent.ShapeType; }
            set { _subComponent.ShapeType = value; }
        }

        public Int32 FillColourFront
        {
            get { return _subComponent.FillColourFront; }
            set { _subComponent.FillColourFront = value; }
        }
        public Int32 FillColourBack
        {
            get
            {
                if (_subComponent.FillColourBack != 0)
                {
                    return _subComponent.FillColourBack;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourBack = value; }
        }
        public Int32 FillColourTop
        {
            get
            {
                if (_subComponent.FillColourTop != 0)
                {
                    return _subComponent.FillColourTop;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourTop = value; }
        }
        public Int32 FillColourBottom
        {
            get
            {
                if (_subComponent.FillColourBottom != 0)
                {
                    return _subComponent.FillColourBottom;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourBottom = value; }
        }
        public Int32 FillColourLeft
        {
            get
            {
                if (_subComponent.FillColourLeft != 0)
                {
                    return _subComponent.FillColourLeft;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourLeft = value; }
        }
        public Int32 FillColourRight
        {
            get
            {
                if (_subComponent.FillColourRight != 0)
                {
                    return _subComponent.FillColourRight;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourRight = value; }
        }

        public Int32 LineColour
        {
            get { return _subComponent.LineColour; }
            set { _subComponent.LineColour = value; }
        }
        public Single LineThickness
        {
            get { return _subComponent.LineThickness; }
            set { _subComponent.LineThickness = value; }
        }

        public Int32 TransparencyPercentFront
        {
            get { return _subComponent.TransparencyPercentFront; }
            set { _subComponent.TransparencyPercentFront = value; }
        }
        public Int32 TransparencyPercentBack
        {
            get { return _subComponent.TransparencyPercentBack; }
            set { _subComponent.TransparencyPercentBack = value; }
        }
        public Int32 TransparencyPercentTop
        {
            get { return _subComponent.TransparencyPercentTop; }
            set { _subComponent.TransparencyPercentTop = value; }
        }
        public Int32 TransparencyPercentBottom
        {
            get { return _subComponent.TransparencyPercentBottom; }
            set { _subComponent.TransparencyPercentBottom = value; }
        }
        public Int32 TransparencyPercentLeft
        {
            get { return _subComponent.TransparencyPercentLeft; }
            set { _subComponent.TransparencyPercentLeft = value; }
        }
        public Int32 TransparencyPercentRight
        {
            get { return _subComponent.TransparencyPercentRight; }
            set { _subComponent.TransparencyPercentRight = value; }
        }

        public PlanogramSubComponentFillPatternType FillPatternTypeFront
        {
            get { return _subComponent.FillPatternTypeFront; }
            set { _subComponent.FillPatternTypeFront = value; }
        }
        public PlanogramSubComponentFillPatternType FillPatternTypeBack
        {
            get { return _subComponent.FillPatternTypeBack; }
            set { _subComponent.FillPatternTypeBack = value; }
        }
        public PlanogramSubComponentFillPatternType FillPatternTypeTop
        {
            get { return _subComponent.FillPatternTypeTop; }
            set { _subComponent.FillPatternTypeTop = value; }
        }
        public PlanogramSubComponentFillPatternType FillPatternTypeBottom
        {
            get { return _subComponent.FillPatternTypeBottom; }
            set { _subComponent.FillPatternTypeBottom = value; }
        }
        public PlanogramSubComponentFillPatternType FillPatternTypeLeft
        {
            get { return _subComponent.FillPatternTypeLeft; }
            set { _subComponent.FillPatternTypeLeft = value; }
        }
        public PlanogramSubComponentFillPatternType FillPatternTypeRight
        {
            get { return _subComponent.FillPatternTypeRight; }
            set { _subComponent.FillPatternTypeRight = value; }
        }

        public Single FaceThicknessFront
        {
            get { return _subComponent.FaceThicknessFront; }
            set { _subComponent.FaceThicknessFront = value; }
        }
        public Single FaceThicknessBack
        {
            get { return _subComponent.FaceThicknessBack; }
            set { _subComponent.FaceThicknessBack = value; }
        }
        public Single FaceThicknessTop
        {
            get { return _subComponent.FaceThicknessTop; }
            set { _subComponent.FaceThicknessTop = value; }
        }
        public Single FaceThicknessBottom
        {
            get { return _subComponent.FaceThicknessBottom; }
            set { _subComponent.FaceThicknessBottom = value; }
        }
        public Single FaceThicknessLeft
        {
            get { return _subComponent.FaceThicknessLeft; }
            set { _subComponent.FaceThicknessLeft = value; }
        }
        public Single FaceThicknessRight
        {
            get { return _subComponent.FaceThicknessRight; }
            set { _subComponent.FaceThicknessRight = value; }
        }

        public Single RiserHeight
        {
            get { return _subComponent.RiserHeight; }
            set
            {
                _subComponent.RiserHeight = value;
            }
        }
        public Single RiserThickness
        {
            get { return _subComponent.RiserThickness; }
            set { _subComponent.RiserThickness = value; }
        }
        public Int32 RiserColour
        {
            get { return _subComponent.RiserColour; }
            set { _subComponent.RiserColour = value; }
        }
        public Int32 RiserTransparencyPercent
        {
            get { return _subComponent.RiserTransparencyPercent; }
            set { _subComponent.RiserTransparencyPercent = value; }
        }
        public Boolean IsRiserPlacedOnFront
        {
            get { return _subComponent.IsRiserPlacedOnFront; }
            set
            {
                _subComponent.IsRiserPlacedOnFront = value;
            }
        }
        public Boolean IsRiserPlacedOnBack
        {
            get { return _subComponent.IsRiserPlacedOnBack; }
            set
            {
                _subComponent.IsRiserPlacedOnBack = value;
            }
        }
        public Boolean IsRiserPlacedOnLeft
        {
            get { return _subComponent.IsRiserPlacedOnLeft; }
            set
            {
                _subComponent.IsRiserPlacedOnLeft = value;
            }
        }
        public Boolean IsRiserPlacedOnRight
        {
            get { return _subComponent.IsRiserPlacedOnRight; }
            set
            {
                _subComponent.IsRiserPlacedOnRight = value;
            }
        }

        public Single NotchStartX
        {
            get { return _subComponent.NotchStartX; }
            set { _subComponent.NotchStartX = value; }
        }
        public Single NotchSpacingX
        {
            get { return _subComponent.NotchSpacingX; }
            set { _subComponent.NotchSpacingX = value; }
        }
        public Single NotchStartY
        {
            get { return _subComponent.NotchStartY; }
            set { _subComponent.NotchStartY = value; }
        }
        public Single NotchSpacingY
        {
            get { return _subComponent.NotchSpacingY; }
            set { _subComponent.NotchSpacingY = value; }
        }
        public Single NotchHeight
        {
            get { return _subComponent.NotchHeight; }
            set { _subComponent.NotchHeight = value; }
        }
        public Single NotchWidth
        {
            get { return _subComponent.NotchWidth; }
            set { _subComponent.NotchWidth = value; }
        }
        public Boolean IsNotchPlacedOnFront
        {
            get { return _subComponent.IsNotchPlacedOnFront; }
            set { _subComponent.IsNotchPlacedOnFront = value; }
        }
        public Boolean IsNotchPlacedOnBack
        {
            get { return _subComponent.IsNotchPlacedOnBack; }
            set { _subComponent.IsNotchPlacedOnBack = value; }
        }
        public Boolean IsNotchPlacedOnLeft
        {
            get { return _subComponent.IsNotchPlacedOnLeft; }
            set { _subComponent.IsNotchPlacedOnLeft = value; }
        }
        public Boolean IsNotchPlacedOnRight
        {
            get { return _subComponent.IsNotchPlacedOnRight; }
            set { _subComponent.IsNotchPlacedOnRight = value; }
        }
        public PlanogramSubComponentNotchStyleType NotchStyleType
        {
            get { return _subComponent.NotchStyleType; }
            set { _subComponent.NotchStyleType = value; }
        }

        public Single MerchConstraintRow1StartX
        {
            get { return _subComponent.MerchConstraintRow1StartX; }
            set { _subComponent.MerchConstraintRow1StartX = value; }
        }
        public Single MerchConstraintRow1SpacingX
        {
            get { return _subComponent.MerchConstraintRow1SpacingX; }
            set { _subComponent.MerchConstraintRow1SpacingX = value; }
        }
        public Single MerchConstraintRow1StartY
        {
            get { return _subComponent.MerchConstraintRow1StartY; }
            set { _subComponent.MerchConstraintRow1StartY = value; }
        }
        public Single MerchConstraintRow1SpacingY
        {
            get { return _subComponent.MerchConstraintRow1SpacingY; }
            set { _subComponent.MerchConstraintRow1SpacingY = value; }
        }
        public Single MerchConstraintRow1Height
        {
            get { return _subComponent.MerchConstraintRow1Height; }
            set { _subComponent.MerchConstraintRow1Height = value; }
        }
        public Single MerchConstraintRow1Width
        {
            get { return _subComponent.MerchConstraintRow1Width; }
            set { _subComponent.MerchConstraintRow1Width = value; }
        }
        public Single MerchConstraintRow2StartX
        {
            get { return _subComponent.MerchConstraintRow2StartX; }
            set { _subComponent.MerchConstraintRow2StartX = value; }
        }
        public Single MerchConstraintRow2SpacingX
        {
            get { return _subComponent.MerchConstraintRow2SpacingX; }
            set { _subComponent.MerchConstraintRow2SpacingX = value; }
        }
        public Single MerchConstraintRow2StartY
        {
            get { return _subComponent.MerchConstraintRow2StartY; }
            set { _subComponent.MerchConstraintRow2StartY = value; }
        }
        public Single MerchConstraintRow2SpacingY
        {
            get { return _subComponent.MerchConstraintRow2SpacingY; }
            set { _subComponent.MerchConstraintRow2SpacingY = value; }
        }
        public Single MerchConstraintRow2Height
        {
            get { return _subComponent.MerchConstraintRow2Height; }
            set { _subComponent.MerchConstraintRow2Height = value; }
        }
        public Single MerchConstraintRow2Width
        {
            get { return _subComponent.MerchConstraintRow2Width; }
            set { _subComponent.MerchConstraintRow2Width = value; }
        }

        public Single DividerObstructionHeight
        {
            get { return _subComponent.DividerObstructionHeight; }
            set { _subComponent.DividerObstructionHeight = value; }
        }
        public Single DividerObstructionWidth
        {
            get { return _subComponent.DividerObstructionWidth; }
            set { _subComponent.DividerObstructionWidth = value; }
        }
        public Single DividerObstructionDepth
        {
            get { return _subComponent.DividerObstructionDepth; }
            set { _subComponent.DividerObstructionDepth = value; }
        }
        public Single DividerObstructionStartX
        {
            get { return _subComponent.DividerObstructionStartX; }
            set { _subComponent.DividerObstructionStartX = value; }
        }
        public Single DividerObstructionSpacingX
        {
            get { return _subComponent.DividerObstructionSpacingX; }
            set { _subComponent.DividerObstructionSpacingX = value; }
        }
        public Single DividerObstructionStartY
        {
            get { return _subComponent.DividerObstructionStartY; }
            set { _subComponent.DividerObstructionStartY = value; }
        }
        public Single DividerObstructionSpacingY
        {
            get { return _subComponent.DividerObstructionSpacingY; }
            set { _subComponent.DividerObstructionSpacingY = value; }
        }
        public Single DividerObstructionStartZ
        {
            get { return _subComponent.DividerObstructionStartZ; }
            set { _subComponent.DividerObstructionStartZ = value; }
        }
        public Single DividerObstructionSpacingZ
        {
            get { return _subComponent.DividerObstructionSpacingZ; }
            set { _subComponent.DividerObstructionSpacingZ = value; }
        }
        public Boolean IsDividerObstructionAtStart
        {
            get { return _subComponent.IsDividerObstructionAtStart; }
            set { _subComponent.IsDividerObstructionAtStart = value; }
        }
        public Boolean IsDividerObstructionAtEnd
        {
            get { return _subComponent.IsDividerObstructionAtEnd; }
            set { _subComponent.IsDividerObstructionAtEnd = value; }
        }
        public Boolean IsDividerObstructionByFacing
        {
            get { return _subComponent.IsDividerObstructionByFacing; }
            set { _subComponent.IsDividerObstructionByFacing = value; }
        }
        public Int32 DividerObstructionFillColour
        {
            get { return _subComponent.DividerObstructionFillColour; }
            set { _subComponent.DividerObstructionFillColour = value; }
        }
        public PlanogramSubComponentFillPatternType DividerObstructionFillPattern
        {
            get { return _subComponent.DividerObstructionFillPattern; }
            set { _subComponent.DividerObstructionFillPattern = value; }
        }

        public Single MerchandisableHeight
        {
            get { return _subComponent.MerchandisableHeight; }
            set { _subComponent.MerchandisableHeight = value; }
        }

        public Single MerchandisableDepth
        {
            get { return _subComponent.MerchandisableDepth; }
            set { _subComponent.MerchandisableDepth = value; }
        }

        public PlanogramSubComponentCombineType CombineType
        {
            get { return _subComponent.CombineType; }
            set { _subComponent.CombineType = value; }
        }
        public PlanogramSubComponentXMerchStrategyType MerchandisingStrategyX
        {
            get { return _subComponent.MerchandisingStrategyX; }
            set { _subComponent.MerchandisingStrategyX = value; }
        }
        public PlanogramSubComponentYMerchStrategyType MerchandisingStrategyY
        {
            get { return _subComponent.MerchandisingStrategyY; }
            set { _subComponent.MerchandisingStrategyY = value; }
        }
        public PlanogramSubComponentZMerchStrategyType MerchandisingStrategyZ
        {
            get { return _subComponent.MerchandisingStrategyZ; }
            set { _subComponent.MerchandisingStrategyZ = value; }
        }
        public PlanogramSubComponentMerchandisingType MerchandisingType
        {
            get { return _subComponent.MerchandisingType; }
            set { _subComponent.MerchandisingType = value; }
        }
        public Boolean IsProductOverlapAllowed
        {
            get { return _subComponent.IsProductOverlapAllowed; }
            set { _subComponent.IsProductOverlapAllowed = value; }
        }
        public Boolean IsProductSqueezeAllowed
        {
            get { return _subComponent.IsProductSqueezeAllowed; }
            set { _subComponent.IsProductSqueezeAllowed = value; }
        }

        public Single LeftOverhang
        {
            get { return _subComponent.LeftOverhang; }
            set { _subComponent.LeftOverhang = value; }
        }
        public Single RightOverhang
        {
            get { return _subComponent.RightOverhang; }
            set { _subComponent.RightOverhang = value; }
        }
        public Single FrontOverhang
        {
            get { return _subComponent.FrontOverhang; }
            set { _subComponent.FrontOverhang = value; }
        }
        public Single BackOverhang
        {
            get { return _subComponent.BackOverhang; }
            set { _subComponent.BackOverhang = value; }
        }
        public Single TopOverhang
        {
            get { return _subComponent.TopOverhang; }
            set { _subComponent.TopOverhang = value; }
        }
        public Single BottomOverhang
        {
            get { return _subComponent.BottomOverhang; }
            set { _subComponent.BottomOverhang = value; }
        }

        #endregion


        #region WorldX

        /// <summary>
        /// Gets/Sets the X coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldX
        {
            get { return _worldCoordinates.X; }
            set { SetWorldCoordinates(value, WorldY, WorldZ); }
        }

        #endregion

        #region WorldY

        /// <summary>
        /// Gets/Sets the Y coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldY
        {
            get { return _worldCoordinates.Y; }
            set { SetWorldCoordinates(WorldX, value, WorldZ); }
        }

        #endregion

        #region WorldZ

        /// <summary>
        /// Gets/Sets the Z coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldZ
        {
            get { return _worldCoordinates.Z; }
            set { SetWorldCoordinates(WorldX, WorldY, value); }
        }

        #endregion

        #region WorldAngle

        /// <summary>
        /// Gets/Sets the rotation angle value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldAngle
        {
            get { return _worldRotation.Angle; }
            set { SetWorldRotation(value, WorldSlope, WorldRoll); }
        }

        #endregion

        #region WorldSlope

        /// <summary>
        /// Gets/Sets the rotation slope value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldSlope
        {
            get { return _worldRotation.Slope; }
            set { SetWorldRotation(WorldAngle, value, WorldRoll); }
        }

        #endregion

        #region WorldRoll

        /// <summary>
        /// Gets/Sets the rotation roll value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldRoll
        {
            get { return _worldRotation.Roll; }
            set { SetWorldRotation(WorldAngle, WorldSlope, value); }
        }

        #endregion

        /// <summary>
        /// Returns true if this is merchandisable
        /// </summary>
        public Boolean IsMerchandisable
        {
            get { return this.MerchandisingType != PlanogramSubComponentMerchandisingType.None; }
        }

        /// <summary>
        /// Returns the collection of positions held by this subcomponent.
        /// </summary>
        protected internal ObservableCollection<PlanogramPositionViewModelBase> PositionViews
        {
            get { return _positions; }
        }

        /// <summary>
        /// Returns the collection of annotations held by this subcomponent.
        /// </summary>
        protected ObservableCollection<PlanogramAnnotationViewModelBase> AnnotationViews
        {
            get { return _annotations; }
        }

        /// <summary>
        /// Returns the collection of dividers placed on this subcomponent.
        /// </summary>
        protected ObservableCollection<PlanogramSubComponentDivider> DividerViews
        {
            get { return _dividers; }
        }

        /// <summary>
        /// Returns the parent component viewmodel.
        /// </summary>
        protected internal PlanogramComponentViewModelBase ParentComponentView
        {
            get { return _parentComponentView; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="subComponent"></param>
        public PlanogramSubComponentViewModelBase(PlanogramComponentViewModelBase parentComponentView, PlanogramSubComponent subComponent)
        {
            _parentComponentView = parentComponentView;

            _subComponent = subComponent;
            subComponent.PropertyChanging += Model_PropertyChanging;
            subComponent.PropertyChanged += Model_PropertyChanged;

            // create a sub component placement
            if (parentComponentView.FixtureComponentModel != null)
            {
                _subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                    _subComponent,
                    _parentComponentView.ParentFixtureView.FixtureItemModel,
                    _parentComponentView.FixtureComponentModel);
            }
            else
            {
                _subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                    _subComponent,
                    _parentComponentView.ParentFixtureView.FixtureItemModel,
                    _parentComponentView.ParentAssemblyView.FixtureAssemblyModel,
                    _parentComponentView.AssemblyComponentModel);
            }

            UpdateWorldCoordinatesAndRotation();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(e.PropertyName);
        }
        protected virtual void OnModelPropertyChanging(String propertyName)
        {
            if (typeof(PlanogramSubComponentViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property has changed on the base model objects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(e.PropertyName);
        }
        protected virtual void OnModelPropertyChanged(String propertyName)
        {
            if (typeof(PlanogramSubComponentViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }

            switch (propertyName)
            {
                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    UpdateWorldCoordinatesAndRotation();
                    break;


                case "ImageIdFront":
                    OnPropertyChanged("FrontImage");
                    OnPropertyChanged("FrontImageData");
                    break;

                case "ImageIdBack":
                    OnPropertyChanged("BackImage");
                    OnPropertyChanged("BackImageData");
                    break;

                case "ImageIdTop":
                    OnPropertyChanged("TopImage");
                    OnPropertyChanged("TopImageData");
                    break;

                case "ImageIdBottom":
                    OnPropertyChanged("BottomImage");
                    OnPropertyChanged("BottomImageData");
                    break;

                case "ImageIdLeft":
                    OnPropertyChanged("LeftImage");
                    OnPropertyChanged("LeftImageData");
                    break;

                case "ImageIdRight":
                    OnPropertyChanged("RightImage");
                    OnPropertyChanged("RightImageData");
                    break;

                case "IsDividerObstructionByFacing":
                    OnPropertyChanged("DividerObstructionSpacingX");
                    OnPropertyChanged("DividerObstructionSpacingY");
                    OnPropertyChanged("DividerObstructionSpacingZ");
                    break;
            }


        }

        /// <summary>
        /// Called whenever the positions collection changes on the parent planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_PositionsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (PlanogramPosition pos in e.ChangedItems)
                        {
                            if (this.ModelPlacement.IsPositionAssociated(pos))
                            {
                                PlanogramPositionViewModelBase view = CreatePositionView(pos);
                                if (view != null)
                                {
                                    _positions.Add(view);
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (PlanogramPosition pos in e.ChangedItems)
                        {
                            PlanogramPositionViewModelBase view = _positions.FirstOrDefault(p => p.Model == pos);
                            if (view != null)
                            {
                                _positions.Remove(view);
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_positions.Count > 0) _positions.Clear();

                        //Add all related positions
                        PlanogramPositionList positions = sender as PlanogramPositionList;
                        if (positions == null && sender is Planogram)
                        {
                            positions = ((Planogram)sender).Positions;
                        }


                        foreach (PlanogramPosition pos in positions)
                        {
                            if (this.ModelPlacement.IsPositionAssociated(pos))
                            {
                                PlanogramPositionViewModelBase view = CreatePositionView(pos);
                                if (view != null)
                                {
                                    _positions.Add(view);
                                }
                            }
                        }

                    }
                    break;
            }

            //if we have no positions left make sure that we have no dividers either.
            if (_positions.Count == 0 && _dividers.Count > 0) _dividers.Clear();

        }

        /// <summary>
        /// Called whenever the annotations collection changes on the parent planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_AnnotationsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (PlanogramAnnotation anno in e.ChangedItems)
                        {
                            if (this.ModelPlacement.IsAnnotationAssociated(anno))
                            {
                                PlanogramAnnotationViewModelBase view = CreateAnnotationView(anno);
                                if (view != null)
                                {
                                    _annotations.Add(view);
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (PlanogramAnnotation pos in e.ChangedItems)
                        {
                            PlanogramAnnotationViewModelBase view = _annotations.FirstOrDefault(p => p.Model == pos);
                            if (view != null)
                            {
                                _annotations.Remove(view);
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_annotations.Count > 0) _annotations.Clear();

                        //Add all related positions
                        foreach (PlanogramAnnotation anno in ((Planogram)sender).Annotations)
                        {
                            if (this.ModelPlacement.IsAnnotationAssociated(anno))
                            {
                                PlanogramAnnotationViewModelBase view = CreateAnnotationView(anno);
                                if (view != null)
                                {
                                    _annotations.Add(view);
                                }
                            }
                        }

                    }
                    break;
            }
        }


        /// <summary>
        /// Called when the parent changes.
        /// </summary>
        protected override void OnParentChanged(object oldValue, object newValue)
        {
            base.OnParentChanged(oldValue, newValue);

            PlanogramComponentViewModelBase oldParent = oldValue as PlanogramComponentViewModelBase;
            if (oldParent != null)
            {
                oldParent.PropertyChanged -= Parent_PropertyChanged;
            }

            PlanogramComponentViewModelBase newParent = newValue as PlanogramComponentViewModelBase;
            if (newParent != null)
            {
                newParent.PropertyChanged += Parent_PropertyChanged;
            }

        }

        /// <summary>
        /// Called whenever a property changes on the parent
        /// </summary>
        private void Parent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.IsDisposed) return;

            switch (e.PropertyName)
            {
                case "WorldX":
                case "WorldY":
                case "WorldZ":
                case "WorldAngle":
                case "WorldSlope":
                case "WorldRoll":
                case "MetaWorldX":
                case "MetaWorldY":
                case "MetaWorldZ":
                case "MetaWorldAngle":
                case "MetaWorldSlope":
                case "MetaWorldRoll":
                    UpdateWorldCoordinatesAndRotation();
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the collections of child viewmodels.
        /// </summary>
        protected void InitializeChildViews()
        {
            if (!_isInitialized)
            {
                //subscribe child collections
                AddChildEventHooks(_positions);
                _positions.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramPositionViewModelBase>;
                AddChildEventHooks(_annotations);
                _annotations.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramAnnotationViewModelBase>;

                Planogram parentPlanogram = Model.Parent.Parent;

                //Add all related positions
                Planogram_PositionsBulkCollectionChanged(parentPlanogram, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));

                //Add related annotations
                Planogram_AnnotationsBulkCollectionChanged(parentPlanogram, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));


                parentPlanogram.Positions.BulkCollectionChanged += Planogram_PositionsBulkCollectionChanged;
                parentPlanogram.Annotations.BulkCollectionChanged += Planogram_AnnotationsBulkCollectionChanged;

                _isInitialized = true;
            }
        }

        /// <summary>
        /// Returns the IDataErrorInfo error for the given columnname.
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            return ((IDataErrorInfo)Model)[columnName];
        }

        #region Image Methods

        /// <summary>
        /// Returns the planogram image with the given id
        /// or kicks off the async load.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected virtual PlanogramImage GetImageLazy(Object id)
        {
            PlanogramImage img = null;

            if (id != null)
            {
                PlanogramComponent parentComponent = Model.Parent;
                if (parentComponent != null)
                {
                    Planogram parentPlan = parentComponent.Parent;
                    if (parentPlan != null)
                    {
                        PlanogramImageList imageList = parentPlan.Images;
                        if (imageList != null)
                        {
                            img = imageList.FindById(id);
                        }
                    }
                }
            }

            return img;
        }

        /// <summary>
        /// Forces the subcomponent images to be refreshed. 
        /// </summary>
        protected internal void RefreshImages()
        {
            // null off existing
            _frontImage = null;
            _backImage = null;
            _topImage = null;
            _bottomImage = null;
            _leftImage = null;
            _rightImage = null;

            //refire property changed
            BeginUpdate();

            OnPropertyChanged("FrontImage");
            OnPropertyChanged("FrontImageData");

            OnPropertyChanged("BackImage");
            OnPropertyChanged("BackImageData");

            OnPropertyChanged("TopImage");
            OnPropertyChanged("TopImageData");

            OnPropertyChanged("BottomImage");
            OnPropertyChanged("BottomImageData");

            OnPropertyChanged("LeftImage");
            OnPropertyChanged("LeftImageData");

            OnPropertyChanged("RightImage");
            OnPropertyChanged("RightImageData");


            EndUpdate();
        }

        #endregion

        #region Child Methods

        /// <summary>
        /// Creates a new view of the given subcomponent model.
        /// </summary>
        /// <param name="subComponent"></param>
        /// <returns></returns>
        protected abstract PlanogramPositionViewModelBase CreatePositionView(PlanogramPosition position);

        /// <summary>
        /// Returns a view of the given annotation.
        /// </summary>
        protected abstract PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation annotation);

        /// <summary>
        /// Notifies this vm that a position in the object graph tree has changed its parent.
        /// </summary>
        internal void NotifyPlanogramPositionParentChanged(PlanogramPosition position)
        {
            Boolean isChild = this.ModelPlacement.IsPositionAssociated(position);
            var positionView = this.PositionViews.FirstOrDefault(p => p.Model == position);

            //IF the position is not a child but it has a view
            //this remove it.
            if (!isChild && positionView != null)
            {
                _positions.Remove(positionView);
            }

            // OR if the position should be a child but has no view
            // then add one.
            else if (isChild && positionView == null)
            {
                PlanogramPositionViewModelBase view = CreatePositionView(position);
                if (view != null)
                {
                    _positions.Add(view);
                }
            }

            //if we have no positions left make sure that we have no dividers either.
            //if (_positions.Count == 0 && _dividers.Count > 0) _dividers.Clear();
        }

        /// <summary>
        /// Notifies this vm that an annotation in the object graph tree has changed its parent.
        /// </summary>
        internal void NotifyPlanogramAnnotationParentChanged(PlanogramAnnotation annotation)
        {
            Boolean isChild = this.ModelPlacement.IsAnnotationAssociated(annotation);
            var annotationView = this.AnnotationViews.FirstOrDefault(p => p.Model == annotation);

            //IF the annotation is not a child but it has a view
            //this remove it.
            if (!isChild && annotationView != null)
            {
                _annotations.Remove(annotationView);
            }

            // OR if the annotation should be a child but has no view
            // then add one.
            else if (isChild && annotationView == null)
            {
                PlanogramAnnotationViewModelBase view = CreateAnnotationView(annotation);
                if (view != null)
                {
                    _annotations.Add(view);
                }
            }
        }

        #endregion

        #region World Space Methods

        /// <summary>
        /// Updates the values of the world coordinates
        /// </summary>
        private void UpdateWorldCoordinatesAndRotation()
        {
            //get the world coordinates and rotation
            PointValue oldCoordinates = _worldCoordinates;
            RotationValue oldRotation = _worldRotation;

            _worldCoordinates =
                   this.ModelPlacement.GetPlanogramRelativeCoordinates();

            _worldRotation =
                this.ModelPlacement.GetPlanogramRelativeRotation();


            //fire off property changed events
            if (oldCoordinates.X != _worldCoordinates.X) OnPropertyChanged("WorldX");
            if (oldCoordinates.Y != _worldCoordinates.Y) OnPropertyChanged("WorldY");
            if (oldCoordinates.Z != _worldCoordinates.Z) OnPropertyChanged("WorldZ");
            if (oldRotation.Angle != _worldRotation.Angle) OnPropertyChanged("WorldAngle");
            if (oldRotation.Slope != _worldRotation.Slope) OnPropertyChanged("WorldSlope");
            if (oldRotation.Roll != _worldRotation.Roll) OnPropertyChanged("WorldRoll");
        }


        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(Single newWorldX, Single newWorldY, Single newWorldZ)
        {
            SetWorldCoordinates(new PointValue(newWorldX, newWorldY, newWorldZ));
        }
        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(PointValue newCoordinates)
        {
            this.ModelPlacement.SetCoordinatesFromPlanogramRelative(newCoordinates);
        }


        /// <summary>
        /// Sets the rotation for this item to the given world rotation.
        /// </summary>
        public void SetWorldRotation(Single newWorldAngle, Single newWorldSlope, Single newWorldRoll)
        {
            SetWorldRotation(new RotationValue(newWorldAngle, newWorldSlope, newWorldRoll));
        }
        /// <summary>
        /// Sets the rotation for this item to the given world rotation.
        /// </summary>
        public void SetWorldRotation(RotationValue newRotation)
        {
            this.ModelPlacement.SetRotationFromPlanogramRelative(newRotation);
        }


        public RectValue GetWorldSpaceBounds()
        {
            return this.ModelPlacement.GetPlanogramRelativeBoundingBox();
        }

        public PointValue ToWorld(PointValue localPoint)
        {
            return PlanogramSubComponentPlacement.ToWorld(localPoint, this.ModelPlacement);
        }

        public PointValue ToLocal(PointValue worldPoint)
        {
            return PlanogramSubComponentPlacement.ToLocal(worldPoint, this.ModelPlacement);
        }

        #endregion

        /// <summary>
        /// Returns the value of the given field for this item.
        /// </summary>
        public virtual Object GetFieldValue(ObjectFieldInfo fieldInfo)
        {
            return fieldInfo.GetValue(this);
        }

        /// <summary>
        /// Enumerates through displayable fields for this item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields()
        {
            foreach (var fieldInfo in PlanogramSubComponent.EnumerateDisplayableFieldInfos())
            {
                yield return fieldInfo;
            }
        }

        #endregion

        #region IPlanSubComponentRenderable Members

        event NotifyCollectionChangedEventHandler IPlanSubComponentRenderable.PositionsCollectionChanged
        {
            add { _positions.CollectionChanged += value; }
            remove { _positions.CollectionChanged -= value; }
        }

        IEnumerable<IPlanPositionRenderable> IPlanSubComponentRenderable.Positions
        {
            get { return _positions; }
        }

        event NotifyCollectionChangedEventHandler IPlanSubComponentRenderable.AnnotationsCollectionChanged
        {
            add { _annotations.CollectionChanged += value; }
            remove { _annotations.CollectionChanged -= value; }
        }

        IEnumerable<IPlanAnnotationRenderable> IPlanSubComponentRenderable.Annotations
        {
            get { return _annotations; }
        }

        event NotifyCollectionChangedEventHandler IPlanSubComponentRenderable.DividersCollectionChanged
        {
            add { _dividers.CollectionChanged += value; }
            remove { _dividers.CollectionChanged -= value; }
        }

        IEnumerable<ISubComponentDividerRenderable> IPlanSubComponentRenderable.Dividers
        {
            get { return _dividers; }
        }


        Boolean IPlanSubComponentRenderable.IsChest
        {
            get
            {
                if (MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                {
                    if (FaceThicknessTop == 0
                        && FaceThicknessBottom > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        Byte[] IPlanSubComponentRenderable.FrontImageData
        {
            get
            {
                PlanogramImage img = this.FrontImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IPlanSubComponentRenderable.BackImageData
        {
            get
            {
                PlanogramImage img = this.BackImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IPlanSubComponentRenderable.TopImageData
        {
            get
            {
                PlanogramImage img = this.TopImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IPlanSubComponentRenderable.BottomImageData
        {
            get
            {
                PlanogramImage img = this.BottomImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IPlanSubComponentRenderable.LeftImageData
        {
            get
            {
                PlanogramImage img = this.LeftImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IPlanSubComponentRenderable.RightImageData
        {
            get
            {
                PlanogramImage img = this.RightImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        PlanogramPegHoles IPlanSubComponentRenderable.GetPegHoles()
        {
            return Model.GetPegHoles();
        }

        List<RectValue> IPlanSubComponentRenderable.GetDividerLines()
        {
            List<RectValue> dividerLines = new List<RectValue>();

            //if no dimentions are specified then just return out.
            if (this.DividerObstructionHeight <= 0 && this.DividerObstructionWidth <= 0 && this.DividerObstructionDepth <= 0)
            {
                return dividerLines;
            }

            Boolean hasFaceThick = this.Model.HasFaceThickness();
            Single faceThickFront = (hasFaceThick) ? this.FaceThicknessFront : 0;
            Single faceThickBack = (hasFaceThick) ? this.FaceThicknessBack : 0;
            Single faceThickLeft = (hasFaceThick) ? this.FaceThicknessLeft : 0;
            Single faceThickRight = (hasFaceThick) ? this.FaceThicknessRight : 0;
            Single faceThickTop = (hasFaceThick) ? this.FaceThicknessTop : 0;
            Single faceThickBottom = (hasFaceThick) ? this.FaceThicknessBottom : 0;


            //get the combined  merch group that this belongs to.
            List<PlanogramSubComponentPlacement> combinedList = this.ModelPlacement.GetCombinedWithList();
            PlanogramSubComponent firstSub = combinedList.First().SubComponent;

            // the first sube be the one used to render the divider lines
            // so if this is not the first sub then return a blank list 
            if (firstSub != this.Model) return dividerLines;


            Single subX = 0;
            for (Int32 i = 0; i < combinedList.Count; i++)
            {
                if (combinedList[i] == this.ModelPlacement) break;
                subX += combinedList[i].SubComponent.Width;
            }


            Single lineWidth = firstSub.DividerObstructionWidth;
            Single lineDepth = 0.01F;


            //X Divider lines
            //nb - ignore spacing if divider by facing is set.
            Single xSpacing = (!firstSub.IsDividerObstructionByFacing) ? firstSub.DividerObstructionSpacingX : 0;
            if (xSpacing > 0 && xSpacing > lineWidth)
            {
                Single axisWidth = combinedList.Sum(c => c.SubComponent.Width);

                Single lineY = this.Height;
                if (faceThickBottom > 0) lineY = faceThickBottom;


                if (this.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked
                    || this.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Right)
                {
                    Single xStart = axisWidth - faceThickRight - firstSub.DividerObstructionStartX - lineWidth;

                    //place from right
                    for (Single curX = xStart; curX > 0; curX = curX - xSpacing)
                    {
                        if (curX >= subX)
                        {
                            //front
                            dividerLines.Add(new RectValue(curX, 0, this.Depth, lineWidth, lineY, lineDepth));
                            //top
                            dividerLines.Add(new RectValue(curX, lineY, 0, lineWidth, lineDepth, this.Depth));
                        }
                    }

                }
                else
                {
                    //ignore start setting if strategy is even
                    Single xStart =
                         (this.MerchandisingStrategyX != PlanogramSubComponentXMerchStrategyType.Even) ?
                        faceThickLeft + firstSub.DividerObstructionStartX : faceThickLeft;

                    //place from left
                    for (Single curX = xStart; curX < (axisWidth - faceThickRight); curX += xSpacing)
                    {
                        if (curX >= subX)
                        {
                            //front
                            dividerLines.Add(new RectValue(curX, 0, this.Depth, lineWidth, lineY, lineDepth));
                            //top
                            dividerLines.Add(new RectValue(curX, lineY, 0, lineWidth, lineDepth, this.Depth));
                        }
                    }
                }
            }


            //Y Divider lines
            Single ySpacing = firstSub.DividerObstructionSpacingY;
            if (ySpacing > 0 && ySpacing > lineWidth)
            {
                if (this.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked
                    || this.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Top)
                {
                    //place from top
                    Single yStart = this.Height - faceThickTop - firstSub.DividerObstructionStartY - lineWidth;
                    for (Single curY = yStart; curY > faceThickBottom; curY = curY - ySpacing)
                    {
                        //front
                        dividerLines.Add(new RectValue(0, curY, this.Depth, this.Width, lineWidth, lineDepth));
                    }

                }
                else
                {
                    //place from bottom
                    //ignore start setting if strategy is even
                    Single yStart =
                        (this.MerchandisingStrategyY != PlanogramSubComponentYMerchStrategyType.Even) ?
                        faceThickBottom + firstSub.DividerObstructionStartY : faceThickBottom;

                    for (Single curY = yStart; curY < (this.Height - faceThickTop); curY += ySpacing)
                    {
                        //front
                        dividerLines.Add(new RectValue(0, curY, this.Depth, this.Width, lineWidth, lineDepth));
                    }
                }
            }


            //Z Divider lines
            Single zSpacing = firstSub.DividerObstructionSpacingZ;

            if (zSpacing > 0 && zSpacing > lineWidth)
            {
                Single lineY = this.Height;
                if (faceThickBottom > 0) lineY = faceThickBottom;


                if (this.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked
                    || this.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Front)
                {
                    //place from front
                    Single zStart = this.Depth - faceThickFront - firstSub.DividerObstructionStartZ - lineWidth;
                    for (Single curZ = zStart; curZ > faceThickBack; curZ -= zSpacing)
                    {
                        //left
                        dividerLines.Add(new RectValue(0, 0, curZ, lineDepth, lineY, lineWidth));

                        // right
                        dividerLines.Add(new RectValue(this.Width, 0, curZ, lineDepth, lineY, lineWidth));

                        //top
                        dividerLines.Add(new RectValue(0, lineY, curZ, this.Width, lineDepth, lineWidth));
                    }
                }
                else
                {
                    //place from back
                    //ignore start setting if strategy is even
                    Single zStart =
                         (this.MerchandisingStrategyZ != PlanogramSubComponentZMerchStrategyType.Even) ?
                         faceThickBack + firstSub.DividerObstructionStartZ : faceThickBack;
                    for (Single curZ = zStart; curZ < (this.Depth - faceThickFront); curZ += zSpacing)
                    {
                        //left
                        dividerLines.Add(new RectValue(0, 0, curZ, lineDepth, lineY, lineWidth));

                        // right
                        dividerLines.Add(new RectValue(this.Width, 0, curZ, lineDepth, lineY, lineWidth));

                        //top
                        dividerLines.Add(new RectValue(0, lineY, curZ, this.Width, lineDepth, lineWidth));
                    }
                }

            }


            return dividerLines;
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void OnDisposing(Boolean disposing)
        {
            _positions.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramPositionViewModelBase>;
            _annotations.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramAnnotationViewModelBase>;

            PlanogramComponent parentComponent = Model.Parent;
            if (parentComponent != null)
            {
                Planogram parentPlan = parentComponent.Parent;
                if (parentPlan != null)
                {
                    parentPlan.Positions.BulkCollectionChanged -= Planogram_PositionsBulkCollectionChanged;
                    parentPlan.Annotations.BulkCollectionChanged -= Planogram_AnnotationsBulkCollectionChanged;
                }
            }

            base.OnDisposing(disposing);

            _subComponent.PropertyChanging -= Model_PropertyChanging;
            _subComponent.PropertyChanged -= Model_PropertyChanged;

        }

        #endregion
    }
}
