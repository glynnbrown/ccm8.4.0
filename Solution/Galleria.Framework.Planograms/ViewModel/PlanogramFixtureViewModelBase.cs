﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Provides a base class for a viewmodel of a planogram fixture.
    /// </summary>
    public abstract class PlanogramFixtureViewModelBase : ViewModelBase, IPlanFixtureRenderable
    {
        #region Fields

        private Boolean _isInitialized;
        private Single _oldWidth;

        private readonly PlanogramViewModelBase _parentPlangoramView;

        private readonly PlanogramFixtureItem _fixtureItem;
        private readonly PlanogramFixture _fixture;

        private readonly ObservableCollection<PlanogramAssemblyViewModelBase> _assemblies = new ObservableCollection<PlanogramAssemblyViewModelBase>();
        private readonly ObservableCollection<PlanogramComponentViewModelBase> _components = new ObservableCollection<PlanogramComponentViewModelBase>();
        private readonly ObservableCollection<PlanogramAnnotationViewModelBase> _annotations = new ObservableCollection<PlanogramAnnotationViewModelBase>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the source fixture model object
        /// </summary>
        public PlanogramFixture FixtureModel
        {
            get { return _fixture; }
        }

        /// <summary>
        /// Returns the source fixture item model object
        /// </summary>
        public PlanogramFixtureItem FixtureItemModel
        {
            get { return _fixtureItem; }
        }

        #region Model Properties

        public Single X
        {
            get { return _fixtureItem.X; }
            set { _fixtureItem.X = value; }
        }

        public Single Y
        {
            get { return _fixtureItem.Y; }
            set { _fixtureItem.Y = value; }
        }

        public Single Z
        {
            get { return _fixtureItem.Z; }
            set { _fixtureItem.Z = value; }
        }

        public Single Slope
        {
            get { return _fixtureItem.Slope; }
            set { _fixtureItem.Slope = value; }
        }

        public Single Angle
        {
            get { return _fixtureItem.Angle; }
            set { _fixtureItem.Angle = value; }
        }

        public Single Roll
        {
            get { return _fixtureItem.Roll; }
            set { _fixtureItem.Roll = value; }
        }

        /// <summary>
        /// Gets/Sets the bay sequence number.
        /// </summary>
        public Int16 BaySequenceNumber
        {
            get { return _fixtureItem.BaySequenceNumber; }
            set { _fixtureItem.BaySequenceNumber = value; }
        }

        public String Name
        {
            get { return _fixture.Name; }
            set
            {
                _fixture.Name = value;
            }
        }

        public Single Height
        {
            get { return _fixture.Height; }
            set
            {
                if (value != _fixture.Height)
                {
                    if (value != 0)
                    {
                        _fixture.Height = value;
                    }
                }
            }
        }

        public Single Width
        {
            get { return _fixture.Width; }
            set
            {
                if (value != _fixture.Width)
                {
                    if (value != 0)
                    {
                        // V8-29527 : Save the old width value to be able to work out which components need
                        // to be expanded / contracted when the fixture width changes.
                        _oldWidth = _fixture.Width;

                        _fixture.Width = value;
                    }
                }
            }
        }

        // Used to return the "old" width before a fixture width was changed
        public Single PreviousWidth
        {
            get { return _oldWidth; }
        }

        public Single Depth
        {
            get { return _fixture.Depth; }
            set
            {
                if (value != _fixture.Depth)
                {
                    if (value != 0)
                    {
                        _fixture.Depth = value;
                    }
                }
            }
        }

        #endregion

        #region Meta Data Properties

        public Int16? MetaNumberOfMerchandisedSubComponents
        {
            get { return _fixture.MetaNumberOfMerchandisedSubComponents; }
        }

        public Single? MetaTotalFixtureCost
        {
            get { return _fixture.MetaTotalFixtureCost; }
        }

        public Int32? MetaComponentCount
        {
            get { return _fixtureItem.MetaComponentCount; }
        }

        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return _fixtureItem.MetaTotalMerchandisableLinearSpace; }
        }

        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return _fixtureItem.MetaTotalMerchandisableAreaSpace; }
        }

        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return _fixtureItem.MetaTotalMerchandisableVolumetricSpace; }
        }

        public Single? MetaTotalLinearWhiteSpace
        {
            get { return _fixtureItem.MetaTotalLinearWhiteSpace; }
        }

        public Single? MetaTotalAreaWhiteSpace
        {
            get { return _fixtureItem.MetaTotalAreaWhiteSpace; }
        }

        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return _fixtureItem.MetaTotalVolumetricWhiteSpace; }
        }

        public Int32? MetaProductsPlaced
        {
            get { return _fixtureItem.MetaProductsPlaced; }
        }

        public Int32? MetaNewProducts
        {
            get { return _fixtureItem.MetaNewProducts; }
        }

        public Int32? MetaChangesFromPreviousCount
        {
            get { return _fixtureItem.MetaChangesFromPreviousCount; }
        }

        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return _fixtureItem.MetaChangeFromPreviousStarRating; }
        }

        public Int32? MetaBlocksDropped
        {
            get { return _fixtureItem.MetaBlocksDropped; }
        }

        public Int32? MetaNotAchievedInventory
        {
            get { return _fixtureItem.MetaNotAchievedInventory; }
        }

        public Int32? MetaTotalFacings
        {
            get { return _fixtureItem.MetaTotalFacings; }
        }

        public Int16? MetaAverageFacings
        {
            get { return _fixtureItem.MetaAverageFacings; }
        }

        public Int32? MetaTotalUnits
        {
            get { return _fixtureItem.MetaTotalUnits; }
        }

        public Int32? MetaAverageUnits
        {
            get { return _fixtureItem.MetaAverageUnits; }
        }

        public Single? MetaMinDos
        {
            get { return _fixtureItem.MetaMinDos; }
        }

        public Single? MetaMaxDos
        {
            get { return _fixtureItem.MetaMaxDos; }
        }

        public Single? MetaAverageDos
        {
            get { return _fixtureItem.MetaAverageDos; }
        }

        public Single? MetaMinCases
        {
            get { return _fixtureItem.MetaMinCases; }
        }

        public Single? MetaAverageCases
        {
            get { return _fixtureItem.MetaAverageCases; }
        }

        public Single? MetaMaxCases
        {
            get { return _fixtureItem.MetaMaxCases; }
        }

        public Int16? MetaSpaceToUnitsIndex
        {
            get { return _fixtureItem.MetaSpaceToUnitsIndex; }
        }

        public Int32? MetaTotalComponentCollisions
        {
            get { return _fixtureItem.MetaTotalComponentCollisions; }
        }

        public Int32? MetaTotalComponentsOverMerchandisedDepth
        {
            get { return _fixtureItem.MetaTotalComponentsOverMerchandisedDepth; }
        }

        public Int32? MetaTotalComponentsOverMerchandisedHeight
        {
            get { return _fixtureItem.MetaTotalComponentsOverMerchandisedHeight; }
        }

        public Int32? MetaTotalComponentsOverMerchandisedWidth
        {
            get { return _fixtureItem.MetaTotalComponentsOverMerchandisedWidth; }
        }

        public Int32? MetaTotalPositionCollisions
        {
            get { return _fixtureItem.MetaTotalPositionCollisions; }
        }

        public Int16? MetaTotalFrontFacings
        {
            get { return _fixtureItem.MetaTotalFrontFacings; }
        }

        public Single? MetaAverageFrontFacings
        {
            get { return _fixtureItem.MetaAverageFrontFacings; }
        }

        #endregion

        /// <summary>
        /// Returns direct assembly children of this fixture.
        /// </summary>
        protected internal ObservableCollection<PlanogramAssemblyViewModelBase> AssemblyViews
        {
            get { return _assemblies; }
        }

        /// <summary>
        /// Returns direct component children of this fixture.
        /// </summary>
        protected internal ObservableCollection<PlanogramComponentViewModelBase> ComponentViews
        {
            get { return _components; }
        }

        /// <summary>
        /// Returns the collection of child annotation views.
        /// </summary>
        protected internal ObservableCollection<PlanogramAnnotationViewModelBase> AnnotationViews
        {
            get { return _annotations; }
        }

        /// <summary>
        /// Returns the parent planogram viewmodel.
        /// </summary>
        protected internal PlanogramViewModelBase ParentPlanogramView
        {
            get { return _parentPlangoramView; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentPlanogramView"></param>
        /// <param name="fixtureItem"></param>
        public PlanogramFixtureViewModelBase(PlanogramViewModelBase parentPlanogramView, PlanogramFixtureItem fixtureItem, PlanogramFixture fixture)
        {
            _parentPlangoramView = parentPlanogramView;

            _fixtureItem = fixtureItem;
            fixtureItem.PropertyChanging += Model_PropertyChanging;
            fixtureItem.PropertyChanged += Model_PropertyChanged;

            Debug.Assert(Object.Equals(fixture.Id, fixtureItem.PlanogramFixtureId), "Incorrect fixture");

            _fixture = fixture;
            _fixture.PropertyChanging += Model_PropertyChanging;
            _fixture.PropertyChanged += Model_PropertyChanged;

            _oldWidth = _fixture.Width;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(sender, e.PropertyName);
        }
        protected virtual void OnModelPropertyChanging(Object sender, String propertyName)
        {
            if (typeof(PlanogramFixtureViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the planogram fixture.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(sender, e.PropertyName);
        }
        protected virtual void OnModelPropertyChanged(Object sender, String propertyName)
        {
            if (typeof(PlanogramFixtureViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }
        }

        /// <summary>
        /// Called when the PlanogramFixture.Assemblies collection changes.
        /// </summary>
        private void PlanogramFixture_AssembliesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            OnPlanogramFixtureAssembliesBulkCollectionChanged(e);
        }
        protected virtual void OnPlanogramFixtureAssembliesBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramFixtureAssembly fixtureAssembly in e.ChangedItems)
                    {
                        PlanogramAssemblyViewModelBase assemblyView = CreateAssemblyView(fixtureAssembly);
                        if (assemblyView != null)
                        {
                            _assemblies.Add(assemblyView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramFixtureAssembly fixtureAssembly in e.ChangedItems)
                    {
                        PlanogramAssemblyViewModelBase assemblyView =
                            _assemblies.FirstOrDefault(a => a.FixtureAssemblyModel == fixtureAssembly);
                        if (assemblyView != null)
                        {
                            _assemblies.Remove(assemblyView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        _assemblies.Clear();

                        foreach (var fixtureAssembly in this.FixtureModel.Assemblies)
                        {
                            PlanogramAssemblyViewModelBase assemblyView = CreateAssemblyView(fixtureAssembly);
                            if (assemblyView != null)
                            {
                                _assemblies.Add(assemblyView);
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called when the PlanogramFixture.Components collection changes.
        /// </summary>
        private void PlanogramFixture_ComponentsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            OnPlanogramFixtureComponentsBulkCollectionChanged(e);
        }
        protected virtual void OnPlanogramFixtureComponentsBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramFixtureComponent fixtureComponent in e.ChangedItems)
                    {
                        PlanogramComponentViewModelBase componentView = CreateComponentView(fixtureComponent);
                        if (componentView != null)
                        {
                            _components.Add(componentView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramFixtureComponent fixtureComponent in e.ChangedItems)
                    {
                        PlanogramComponentViewModelBase componentView =
                            _components.FirstOrDefault(a => a.FixtureComponentModel == fixtureComponent);
                        if (componentView != null)
                        {
                            _components.Remove(componentView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        _components.Clear();

                        foreach (var fixtureComponent in this.FixtureModel.Components)
                        {
                            PlanogramComponentViewModelBase componentView = CreateComponentView(fixtureComponent);
                            if (componentView != null)
                            {
                                _components.Add(componentView);
                            }
                        }

                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the annotations collection changes on the parent planogram
        /// </summary>
        private void Planogram_AnnotationsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (e.ChangedItems == null) return;

            List<PlanogramAnnotation> changedItems = new List<PlanogramAnnotation>();
            foreach (PlanogramAnnotation anno in e.ChangedItems)
            {
                if (IsAnnotationAssociated(anno))
                {
                    changedItems.Add(anno);
                }
            }

            if (changedItems.Count > 0)
            {
                OnLinkedAnnotationsChanged(e.Action, changedItems);
            }

        }
        protected virtual void OnLinkedAnnotationsChanged(NotifyCollectionChangedAction action, IEnumerable<PlanogramAnnotation> changedItems)
        {
            switch (action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (PlanogramAnnotation anno in changedItems)
                        {
                            PlanogramAnnotationViewModelBase annoView = CreateAnnotationView(anno);
                            if (annoView != null) _annotations.Add(annoView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (PlanogramAnnotation anno in changedItems)
                        {
                            PlanogramAnnotationViewModelBase annoView =
                                _annotations.Cast<PlanogramAnnotationViewModelBase>()
                                .FirstOrDefault(a => a.Model == anno);

                            if (annoView != null) _annotations.Remove(annoView);
                        }

                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_annotations.Count > 0) _annotations.Clear();

                        Planogram plan = this.FixtureItemModel.Parent;
                        if (plan != null)
                        {
                            foreach (PlanogramAnnotation anno in plan.Annotations)
                            {
                                if (!IsAnnotationAssociated(anno)) continue;

                                PlanogramAnnotationViewModelBase annoView = CreateAnnotationView(anno);
                                if (annoView != null) _annotations.Add(annoView);
                            }
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the child view collections.
        /// </summary>
        protected void InitializeChildViews()
        {
            if (!_isInitialized)
            {
                //subscribe child collections
                AddChildEventHooks(_assemblies);
                _assemblies.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramAssemblyViewModelBase>;
                AddChildEventHooks(_components);
                _components.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramComponentViewModelBase>;
                AddChildEventHooks(_annotations);
                _annotations.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramAnnotationViewModelBase>;


                _fixture.Assemblies.BulkCollectionChanged += PlanogramFixture_AssembliesBulkCollectionChanged;
                _fixture.Components.BulkCollectionChanged += PlanogramFixture_ComponentsBulkCollectionChanged;

                Planogram parentPlanogram = this.FixtureItemModel.Parent;
                if (parentPlanogram != null)
                {
                    parentPlanogram.Annotations.BulkCollectionChanged += Planogram_AnnotationsBulkCollectionChanged;
                }

                //add child views
                OnPlanogramFixtureAssembliesBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
                OnPlanogramFixtureComponentsBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
                OnLinkedAnnotationsChanged(NotifyCollectionChangedAction.Reset, null);

                _isInitialized = true;
            }
        }

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// For IDataErrorInfo
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            String error = ((IDataErrorInfo)this.FixtureModel)[columnName];
            if (!String.IsNullOrEmpty(error))
            {
                return error;
            }

            error = ((IDataErrorInfo)this.FixtureItemModel)[columnName];

            return error;
        }

        /// <summary>
        /// Creates a new viewmodel of the given fixture assembly
        /// </summary>
        /// <param name="fixtureAssembly"></param>
        /// <returns></returns>
        protected abstract PlanogramAssemblyViewModelBase CreateAssemblyView(PlanogramFixtureAssembly fixtureAssembly);

        /// <summary>
        /// Creates a new viewmodel of the given fixture component
        /// </summary>
        /// <param name="fixtureComponent"></param>
        /// <returns></returns>
        protected abstract PlanogramComponentViewModelBase CreateComponentView(PlanogramFixtureComponent fixtureComponent);

        /// <summary>
        /// Creates a new viewmodel of the given annotation.
        /// </summary>
        /// <param name="fixtureAnnotation"></param>
        /// <returns></returns>
        protected abstract PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation fixtureAnnotation);

        /// <summary>
        /// Returns true if the given annotation is associated with this fixture.
        /// </summary>
        private Boolean IsAnnotationAssociated(PlanogramAnnotation annotation)
        {
            if (this.FixtureItemModel == null) return false;

            if (!Object.Equals(annotation.PlanogramFixtureItemId, this.FixtureItemModel.Id)) return false;
            if (!Object.Equals(annotation.PlanogramFixtureAssemblyId, null)) return false;
            //if(!Object.Equals(annotation.PlanogramAssemblyComponentId, null)) return false;
            if (!Object.Equals(annotation.PlanogramFixtureComponentId, null)) return false;
            //if(!Object.Equals(annotation.PlanogramSubComponentId, null)) return false;
            //if(!Object.Equals(annotation.PlanogramPositionId, null)) return false;

            return true;
        }

        /// <summary>
        /// Notifies this vm that an annotation in the object graph tree has changed its parent.
        /// </summary>
        internal void NotifyPlanogramAnnotationParentChanged(PlanogramAnnotation annotation)
        {
            Boolean isChild = IsAnnotationAssociated(annotation);
            var annotationView = this.AnnotationViews.FirstOrDefault(p => p.Model == annotation);

            //IF the annotation is not a child but it has a view
            //this remove it.
            if (!isChild && annotationView != null)
            {
                _annotations.Remove(annotationView);
            }

            // OR if the annotation should be a child but has no view
            // then add one.
            else if (isChild && annotationView == null)
            {
                PlanogramAnnotationViewModelBase view = CreateAnnotationView(annotation);
                if (view != null)
                {
                    _annotations.Add(view);
                }
            }
        }


        /// <summary>
        /// Returns the world space bounds of this fixture.
        /// </summary>
        public RectValue GetWorldSpaceBounds()
        {
            return this.FixtureItemModel.GetPlanogramRelativeBoundingBox(this.FixtureModel);
        }

        public PointValue ToLocal(PointValue worldPoint)
        {
            return PlanogramFixtureItem.ToLocal(worldPoint, this.FixtureItemModel);
        }

        public PointValue ToWorld(PointValue localPoint)
        {
            return PlanogramFixtureItem.ToWorld(localPoint, this.FixtureItemModel);
        }


        /// <summary>
        /// Returns the value of the given field for this item.
        /// </summary>
        public virtual Object GetFieldValue(ObjectFieldInfo fieldInfo)
        {
            return fieldInfo.GetValue(this);
        }

        /// <summary>
        /// Enumerates through displayable fields for this item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields()
        {
            foreach (var fieldInfo in PlanogramFixture.EnumerateDisplayableFieldInfos(/*incMetadata*/true))
            {
                yield return fieldInfo;
            }

            foreach (var field in PlanogramFixtureItem.EnumerateDisplayableFieldInfos(/*incMetadata*/true))
            {
                yield return field;
            }
        }

        #endregion

        #region IPlanFixtureRenderable Members

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.AssembliesCollectionChanged
        {
            add { _assemblies.CollectionChanged += value; }
            remove { _assemblies.CollectionChanged -= value; }
        }

        IEnumerable<IPlanAssemblyRenderable> IPlanFixtureRenderable.Assemblies
        {
            get { return _assemblies; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.ComponentsCollectionChanged
        {
            add { _components.CollectionChanged += value; }
            remove { _components.CollectionChanged -= value; }
        }

        IEnumerable<IPlanComponentRenderable> IPlanFixtureRenderable.Components
        {
            get { return _components; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.AnnotationsCollectionChanged
        {
            add { _annotations.CollectionChanged += value; }
            remove { _annotations.CollectionChanged -= value; }
        }

        IEnumerable<IPlanAnnotationRenderable> IPlanFixtureRenderable.Annotations
        {
            get { return _annotations; }
        }

        Object IPlanFixtureRenderable.PlanogramFixtureItemId
        {
            get { return this.FixtureItemModel.Id; }
        }

        #endregion

        #region IDisposable Members

        protected override void OnDisposing(Boolean disposing)
        {
            _assemblies.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramAssemblyViewModelBase>;
            _components.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramComponentViewModelBase>;
            _annotations.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramAnnotationViewModelBase>;

            base.OnDisposing(disposing);

            Planogram parentPlanogram = _fixtureItem.Parent;
            if (parentPlanogram != null)
            {
                parentPlanogram.Annotations.BulkCollectionChanged -= Planogram_AnnotationsBulkCollectionChanged;
            }

            _fixtureItem.PropertyChanging -= Model_PropertyChanging;
            _fixtureItem.PropertyChanged -= Model_PropertyChanged;
            _fixture.PropertyChanging -= Model_PropertyChanging;
            _fixture.PropertyChanged -= Model_PropertyChanged;
            _fixture.Assemblies.BulkCollectionChanged -= PlanogramFixture_AssembliesBulkCollectionChanged;
            _fixture.Components.BulkCollectionChanged -= PlanogramFixture_ComponentsBulkCollectionChanged;
        }

        #endregion

    }
}
