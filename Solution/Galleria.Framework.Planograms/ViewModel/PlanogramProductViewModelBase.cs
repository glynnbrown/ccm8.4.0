﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Provides a base class for a viewmodel of a PlanogramProduct.
    /// </summary>
    public abstract class PlanogramProductViewModelBase : ViewModelBase, IPlanProductRenderable
    {
        #region Fields

        private readonly PlanogramViewModelBase _parentPlanogram;
        private readonly PlanogramProduct _planogramProduct;
        private Boolean _isLoadingImages;
        private PlanogramPerformanceData _performanceData;
        private PlanogramAssortmentProduct _assortmentProduct;

        /// <summary>
        ///     The lookup dictionary that indicates which image is stored outside the planogram and a description.
        /// </summary>
        private readonly Dictionary<String, String> _externalImageDescriptions = new Dictionary<String, String>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the source PlanogramProduct object.
        /// </summary>
        public PlanogramProduct Model
        {
            get { return _planogramProduct; }
        }

        /// <summary>
        /// Returns the custom attribute data model for this product.
        /// </summary>
        public CustomAttributeData CustomAttributes
        {
            get { return Model.CustomAttributes; }
        }

        /// <summary>
        /// Returns the performance data model for this product.
        /// </summary>
        public PlanogramPerformanceData PerformanceData
        {
            get { return _performanceData; }
        }

        /// <summary>
        /// Returns the assortment product for this product.
        /// </summary>
        public PlanogramAssortmentProduct AssortmentProduct
        {
            get
            {
                if (_assortmentProduct == null
                    || _assortmentProduct.IsDeleted
                    || _assortmentProduct.Gtin != this.Gtin)
                {
                    _assortmentProduct = Model.GetPlanogramAssortmentProduct();
                }
                return _assortmentProduct;
            }
        }

        #region Model Properties

        public String Gtin
        {
            get { return _planogramProduct.Gtin; }
            set { _planogramProduct.Gtin = value; }
        }
        public String Name
        {
            get { return _planogramProduct.Name; }
            set { _planogramProduct.Name = value; }
        }

        public String Brand
        {
            get { return _planogramProduct.Brand; }
            set { _planogramProduct.Brand = value; }
        }

        public Single Height
        {
            get { return _planogramProduct.Height; }
            set { _planogramProduct.Height = value; }
        }
        public Single Width
        {
            get { return _planogramProduct.Width; }
            set { _planogramProduct.Width = value; }
        }
        public Single Depth
        {
            get { return _planogramProduct.Depth; }
            set { _planogramProduct.Depth = value; }
        }

        public Single DisplayHeight
        {
            get { return _planogramProduct.DisplayHeight; }
            set { _planogramProduct.DisplayHeight = value; }
        }
        public Single DisplayWidth
        {
            get { return _planogramProduct.DisplayWidth; }
            set { _planogramProduct.DisplayWidth = value; }
        }
        public Single DisplayDepth
        {
            get { return _planogramProduct.DisplayDepth; }
            set { _planogramProduct.DisplayDepth = value; }
        }

        public Single AlternateHeight
        {
            get { return _planogramProduct.AlternateHeight; }
            set { _planogramProduct.AlternateHeight = value; }
        }
        public Single AlternateWidth
        {
            get { return _planogramProduct.AlternateWidth; }
            set { _planogramProduct.AlternateWidth = value; }
        }
        public Single AlternateDepth
        {
            get { return _planogramProduct.AlternateDepth; }
            set { _planogramProduct.AlternateDepth = value; }
        }

        public Single PointOfPurchaseHeight
        {
            get { return _planogramProduct.PointOfPurchaseHeight; }
            set { _planogramProduct.PointOfPurchaseHeight = value; }
        }
        public Single PointOfPurchaseWidth
        {
            get { return _planogramProduct.PointOfPurchaseWidth; }
            set { _planogramProduct.PointOfPurchaseWidth = value; }
        }
        public Single PointOfPurchaseDepth
        {
            get { return _planogramProduct.PointOfPurchaseDepth; }
            set { _planogramProduct.PointOfPurchaseDepth = value; }
        }

        public Byte NumberOfPegHoles
        {
            get { return _planogramProduct.NumberOfPegHoles; }
            set { _planogramProduct.NumberOfPegHoles = value; }
        }
        public Single PegX
        {
            get { return _planogramProduct.PegX; }
            set { _planogramProduct.PegX = value; }
        }
        public Single PegX2
        {
            get { return _planogramProduct.PegX2; }
            set { _planogramProduct.PegX2 = value; }
        }
        public Single PegX3
        {
            get { return _planogramProduct.PegX3; }
            set { _planogramProduct.PegX3 = value; }
        }
        public Single PegY
        {
            get { return _planogramProduct.PegY; }
            set { _planogramProduct.PegY = value; }
        }
        public Single PegY2
        {
            get { return _planogramProduct.PegY2; }
            set { _planogramProduct.PegY2 = value; }
        }
        public Single PegY3
        {
            get { return _planogramProduct.PegY3; }
            set { _planogramProduct.PegY3 = value; }
        }
        public Single PegProngOffsetX
        {
            get { return _planogramProduct.PegProngOffsetX; }
            set { _planogramProduct.PegProngOffsetX = value; }
        }
        public Single PegProngOffsetY
        {
            get { return _planogramProduct.PegProngOffsetY; }
            set { _planogramProduct.PegProngOffsetY = value; }
        }
        public Single PegDepth
        {
            get { return _planogramProduct.PegDepth; }
            set { _planogramProduct.PegDepth = value; }
        }

        public Single SqueezeHeight
        {
            get { return _planogramProduct.SqueezeHeight; }
            set
            {
                //force value to be between 0 and 1.
                Single newValue = Math.Min(1, value);
                newValue = Math.Max(0, newValue);

                _planogramProduct.SqueezeHeight = value;
            }
        }
        public Single SqueezeHeightActual
        {
            get { return _planogramProduct.SqueezeHeightActual; }
            set { _planogramProduct.SqueezeHeightActual = value; }
        }
        public Single SqueezeWidth
        {
            get { return _planogramProduct.SqueezeWidth; }
            set
            {
                //force value to be between 0 and 1.
                Single newValue = Math.Min(1, value);
                newValue = Math.Max(0, newValue);

                _planogramProduct.SqueezeWidth = newValue;
            }
        }
        public Single SqueezeWidthActual
        {
            get { return _planogramProduct.SqueezeWidthActual; }
            set { _planogramProduct.SqueezeWidthActual = value; }
        }
        public Single SqueezeDepth
        {
            get { return _planogramProduct.SqueezeDepth; }
            set
            {
                //force value to be between 0 and 1.
                Single newValue = Math.Min(1, value);
                newValue = Math.Max(0, newValue);

                _planogramProduct.SqueezeDepth = newValue;
            }
        }
        public Single SqueezeDepthActual
        {
            get { return _planogramProduct.SqueezeDepthActual; }
            set { _planogramProduct.SqueezeDepthActual = value; }
        }

        public Single NestingHeight
        {
            get { return _planogramProduct.NestingHeight; }
            set { _planogramProduct.NestingHeight = value; }
        }
        public Single NestingWidth
        {
            get { return _planogramProduct.NestingWidth; }
            set { _planogramProduct.NestingWidth = value; }
        }
        public Single NestingDepth
        {
            get { return _planogramProduct.NestingDepth; }
            set { _planogramProduct.NestingDepth = value; }
        }

        public Int16 CasePackUnits
        {
            get { return _planogramProduct.CasePackUnits; }
            set { _planogramProduct.CasePackUnits = value; }
        }
        public Byte CaseHigh
        {
            get { return _planogramProduct.CaseHigh; }
            set { _planogramProduct.CaseHigh = value; }
        }
        public Byte CaseWide
        {
            get { return _planogramProduct.CaseWide; }
            set { _planogramProduct.CaseWide = value; }
        }
        public Byte CaseDeep
        {
            get { return _planogramProduct.CaseDeep; }
            set { _planogramProduct.CaseDeep = value; }
        }
        public Single CaseHeight
        {
            get { return _planogramProduct.CaseHeight; }
            set { _planogramProduct.CaseHeight = value; }
        }
        public Single CaseWidth
        {
            get { return _planogramProduct.CaseWidth; }
            set { _planogramProduct.CaseWidth = value; }
        }
        public Single CaseDepth
        {
            get { return _planogramProduct.CaseDepth; }
            set { _planogramProduct.CaseDepth = value; }
        }

        public Byte MaxStack
        {
            get { return _planogramProduct.MaxStack; }
            set { _planogramProduct.MaxStack = value; }
        }
        public Byte MaxTopCap
        {
            get { return _planogramProduct.MaxTopCap; }
            set { _planogramProduct.MaxTopCap = value; }
        }
        public Byte MaxRightCap
        {
            get { return _planogramProduct.MaxRightCap; }
            set { _planogramProduct.MaxRightCap = value; }
        }
        public Byte MinDeep
        {
            get { return _planogramProduct.MinDeep; }
            set { _planogramProduct.MinDeep = value; }
        }
        public Byte MaxDeep
        {
            get { return _planogramProduct.MaxDeep; }
            set { _planogramProduct.MaxDeep = value; }
        }

        public Int16 TrayPackUnits
        {
            get { return _planogramProduct.TrayPackUnits; }
            set { _planogramProduct.TrayPackUnits = value; }
        }
        public Byte TrayHigh
        {
            get { return _planogramProduct.TrayHigh; }
            set { _planogramProduct.TrayHigh = value; }
        }
        public Byte TrayWide
        {
            get { return _planogramProduct.TrayWide; }
            set { _planogramProduct.TrayWide = value; }
        }
        public Byte TrayDeep
        {
            get { return _planogramProduct.TrayDeep; }
            set { _planogramProduct.TrayDeep = value; }
        }
        public Single TrayHeight
        {
            get { return _planogramProduct.TrayHeight; }
            set { _planogramProduct.TrayHeight = value; }
        }
        public Single TrayWidth
        {
            get { return _planogramProduct.TrayWidth; }
            set { _planogramProduct.TrayWidth = value; }
        }
        public Single TrayDepth
        {
            get { return _planogramProduct.TrayDepth; }
            set { _planogramProduct.TrayDepth = value; }
        }
        public Single TrayThickHeight
        {
            get { return _planogramProduct.TrayThickHeight; }
            set { _planogramProduct.TrayThickHeight = value; }
        }
        public Single TrayThickWidth
        {
            get { return _planogramProduct.TrayThickWidth; }
            set { _planogramProduct.TrayThickWidth = value; }
        }
        public Single TrayThickDepth
        {
            get { return _planogramProduct.TrayThickDepth; }
            set { _planogramProduct.TrayThickDepth = value; }
        }

        public Single FrontOverhang
        {
            get { return _planogramProduct.FrontOverhang; }
            set { _planogramProduct.FrontOverhang = value; }
        }
        public Single FingerSpaceAbove
        {
            get { return _planogramProduct.FingerSpaceAbove; }
            set { _planogramProduct.FingerSpaceAbove = value; }
        }
        public Single FingerSpaceToTheSide
        {
            get { return _planogramProduct.FingerSpaceToTheSide; }
            set { _planogramProduct.FingerSpaceToTheSide = value; }
        }

        public PlanogramProductStatusType StatusType
        {
            get { return _planogramProduct.StatusType; }
            set { _planogramProduct.StatusType = value; }
        }
        public PlanogramProductOrientationType OrientationType
        {
            get { return _planogramProduct.OrientationType; }
            set { _planogramProduct.OrientationType = value; }
        }
        public PlanogramProductMerchandisingStyle MerchandisingStyle
        {
            get { return _planogramProduct.MerchandisingStyle; }
            set { _planogramProduct.MerchandisingStyle = value; }
        }

        public Boolean IsFrontOnly
        {
            get { return _planogramProduct.IsFrontOnly; }
            set { _planogramProduct.IsFrontOnly = value; }
        }

        public Boolean IsPlaceHolderProduct
        {
            get { return _planogramProduct.IsPlaceHolderProduct; }
            set { _planogramProduct.IsPlaceHolderProduct = value; }
        }
        public Boolean IsActive
        {
            get { return _planogramProduct.IsActive; }
            set { _planogramProduct.IsActive = value; }
        }

        public Boolean CanBreakTrayUp
        {
            get { return _planogramProduct.CanBreakTrayUp; }
            set { _planogramProduct.CanBreakTrayUp = value; }
        }
        public Boolean CanBreakTrayDown
        {
            get { return _planogramProduct.CanBreakTrayDown; }
            set { _planogramProduct.CanBreakTrayDown = value; }
        }
        public Boolean CanBreakTrayBack
        {
            get { return _planogramProduct.CanBreakTrayBack; }
            set { _planogramProduct.CanBreakTrayBack = value; }
        }
        public Boolean CanBreakTrayTop
        {
            get { return _planogramProduct.CanBreakTrayTop; }
            set { _planogramProduct.CanBreakTrayTop = value; }
        }

        public Boolean ForceMiddleCap
        {
            get { return _planogramProduct.ForceMiddleCap; }
            set { _planogramProduct.ForceMiddleCap = value; }
        }
        public Boolean ForceBottomCap
        {
            get { return _planogramProduct.ForceBottomCap; }
            set { _planogramProduct.ForceBottomCap = value; }
        }


        public PlanogramProductShapeType ShapeType
        {
            get { return _planogramProduct.ShapeType; }
            set { _planogramProduct.ShapeType = value; }
        }

        public PlanogramProductFillPatternType FillPatternType
        {
            get { return _planogramProduct.FillPatternType; }
            set { _planogramProduct.FillPatternType = value; }
        }

        public Int32 FillColour
        {
            get { return _planogramProduct.FillColour; }
            set { _planogramProduct.FillColour = value; }
        }

        public String ColourGroupValue
        {
            get { return _planogramProduct.ColourGroupValue; }
        }

        public String Shape
        {
            get { return _planogramProduct.Shape; }
            set { _planogramProduct.Shape = value; }
        }

        public String PointOfPurchaseDescription
        {
            get { return _planogramProduct.PointOfPurchaseDescription; }
            set { _planogramProduct.PointOfPurchaseDescription = value; }
        }

        public String ShortDescription
        {
            get { return _planogramProduct.ShortDescription; }
            set { _planogramProduct.ShortDescription = value; }
        }

        public String Subcategory
        {
            get { return _planogramProduct.Subcategory; }
            set { _planogramProduct.Subcategory = value; }
        }

        public String CustomerStatus
        {
            get { return _planogramProduct.CustomerStatus; }
            set { _planogramProduct.CustomerStatus = value; }
        }

        public String Colour
        {
            get { return _planogramProduct.Colour; }
            set { _planogramProduct.Colour = value; }
        }

        public String Flavour
        {
            get { return _planogramProduct.Flavour; }
            set { _planogramProduct.Flavour = value; }
        }

        public String PackagingShape
        {
            get { return _planogramProduct.PackagingShape; }
            set { _planogramProduct.PackagingShape = value; }
        }

        public String PackagingType
        {
            get { return _planogramProduct.PackagingType; }
            set { _planogramProduct.PackagingType = value; }
        }

        public String CountryOfOrigin
        {
            get { return _planogramProduct.CountryOfOrigin; }
            set { _planogramProduct.CountryOfOrigin = value; }
        }

        public String CountryOfProcessing
        {
            get { return _planogramProduct.CountryOfProcessing; }
            set { _planogramProduct.CountryOfProcessing = value; }
        }

        public Int16 ShelfLife
        {
            get { return _planogramProduct.ShelfLife; }
            set { _planogramProduct.ShelfLife = value; }
        }

        public Single? DeliveryFrequencyDays
        {
            get { return _planogramProduct.DeliveryFrequencyDays; }
            set { _planogramProduct.DeliveryFrequencyDays = value; }
        }

        public String DeliveryMethod
        {
            get { return _planogramProduct.DeliveryMethod; }
            set { _planogramProduct.DeliveryMethod = value; }
        }

        public String VendorCode
        {
            get { return _planogramProduct.VendorCode; }
            set { _planogramProduct.VendorCode = value; }
        }

        public String Vendor
        {
            get { return _planogramProduct.Vendor; }
            set { _planogramProduct.Vendor = value; }
        }

        public String ManufacturerCode
        {
            get { return _planogramProduct.ManufacturerCode; }
            set { _planogramProduct.ManufacturerCode = value; }
        }

        public String Manufacturer
        {
            get { return _planogramProduct.Manufacturer; }
            set { _planogramProduct.Manufacturer = value; }
        }

        public String Size
        {
            get { return _planogramProduct.Size; }
            set { _planogramProduct.Size = value; }
        }

        public String UnitOfMeasure
        {
            get { return _planogramProduct.UnitOfMeasure; }
            set { _planogramProduct.UnitOfMeasure = value; }
        }

        public DateTime? DateIntroduced
        {
            get { return _planogramProduct.DateIntroduced; }
            set { _planogramProduct.DateIntroduced = value; }
        }

        public DateTime? DateDiscontinued
        {
            get { return _planogramProduct.DateDiscontinued; }
            set { _planogramProduct.DateDiscontinued = value; }
        }

        public DateTime? DateEffective
        {
            get { return _planogramProduct.DateEffective; }
            set { _planogramProduct.DateEffective = value; }
        }

        public String Health
        {
            get { return _planogramProduct.Health; }
            set { _planogramProduct.Health = value; }
        }

        public String CorporateCode
        {
            get { return _planogramProduct.CorporateCode; }
            set { _planogramProduct.CorporateCode = value; }
        }

        public String Barcode
        {
            get { return _planogramProduct.Barcode; }
            set { _planogramProduct.Barcode = value; }
        }

        public Single? SellPrice
        {
            get { return _planogramProduct.SellPrice; }
            set { _planogramProduct.SellPrice = value; }
        }

        public Int16? SellPackCount
        {
            get { return _planogramProduct.SellPackCount; }
            set { _planogramProduct.SellPackCount = value; }
        }

        public String SellPackDescription
        {
            get { return _planogramProduct.SellPackDescription; }
            set { _planogramProduct.SellPackDescription = value; }
        }

        public Single? RecommendedRetailPrice
        {
            get { return _planogramProduct.RecommendedRetailPrice; }
            set { _planogramProduct.RecommendedRetailPrice = value; }
        }

        public Single? ManufacturerRecommendedRetailPrice
        {
            get { return _planogramProduct.ManufacturerRecommendedRetailPrice; }
            set { _planogramProduct.ManufacturerRecommendedRetailPrice = value; }
        }

        public Single? CostPrice
        {
            get { return _planogramProduct.CostPrice; }
            set { _planogramProduct.CostPrice = value; }
        }

        public Single? CaseCost
        {
            get { return _planogramProduct.CaseCost; }
            set { _planogramProduct.CaseCost = value; }
        }

        public Single? TaxRate
        {
            get { return _planogramProduct.TaxRate; }
            set { _planogramProduct.TaxRate = value; }
        }

        public String ConsumerInformation
        {
            get { return _planogramProduct.ConsumerInformation; }
            set { _planogramProduct.ConsumerInformation = value; }
        }

        public String Texture
        {
            get { return _planogramProduct.Texture; }
            set { _planogramProduct.Texture = value; }
        }

        public Int16? StyleNumber
        {
            get { return _planogramProduct.StyleNumber; }
            set { _planogramProduct.StyleNumber = value; }
        }

        public String Pattern
        {
            get { return _planogramProduct.Pattern; }
            set { _planogramProduct.Pattern = value; }
        }

        public String ModelString
        {
            get { return _planogramProduct.Model; }
            set { _planogramProduct.Model = value; }
        }

        public String GarmentType
        {
            get { return _planogramProduct.GarmentType; }
            set { _planogramProduct.GarmentType = value; }
        }

        public Boolean IsPrivateLabel
        {
            get { return _planogramProduct.IsPrivateLabel; }
            set { _planogramProduct.IsPrivateLabel = value; }
        }

        public Boolean IsNewProduct
        {
            get { return _planogramProduct.IsNewProduct; }
            set { _planogramProduct.IsNewProduct = value; }
        }

        public String FinancialGroupCode
        {
            get { return _planogramProduct.FinancialGroupCode; }
            set { _planogramProduct.FinancialGroupCode = value; }
        }

        public String FinancialGroupName
        {
            get { return _planogramProduct.FinancialGroupName; }
            set { _planogramProduct.FinancialGroupName = value; }
        }

        //public String CdtNodeName
        //{
        //    get { return Model.CdtNodeName; }
        //}

        #endregion

        #region Image Properties

        /// <summary>
        /// Returns true if the images for this product are being loaded.
        /// </summary>
        public Boolean IsLoadingImages
        {
            get { return _isLoadingImages; }
            private set
            {
                if (_isLoadingImages != value)
                {
                    _isLoadingImages = value;
                    OnPropertyChanged("IsLoadingImages");
                }
            }
        }

        /// <summary>
        ///     Gets the lookup dictionary of images stored outside the planogram.
        /// </summary>
        public Dictionary<String, String> ExternalImageDescriptions
        {
            get { return _externalImageDescriptions; }
        }

        #region Unit

        /// <summary>
        /// Gets/Sets the front product image
        /// </summary>
        public PlanogramImage FrontImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdFront, PlanogramImageFacingType.Front, PlanogramImageType.None); }
            set { this.Model.PlanogramImageIdFront = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the back product image
        /// </summary>
        public PlanogramImage BackImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdBack, PlanogramImageFacingType.Back, PlanogramImageType.None); }
            set { this.Model.PlanogramImageIdBack = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the top product image
        /// </summary>
        public PlanogramImage TopImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdTop, PlanogramImageFacingType.Top, PlanogramImageType.None); }
            set { this.Model.PlanogramImageIdTop = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the bottom product image
        /// </summary>
        public PlanogramImage BottomImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdBottom, PlanogramImageFacingType.Bottom, PlanogramImageType.None); }
            set { this.Model.PlanogramImageIdBottom = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the left product image
        /// </summary>
        public PlanogramImage LeftImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdLeft, PlanogramImageFacingType.Left, PlanogramImageType.None); }
            set { this.Model.PlanogramImageIdLeft = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the right product image.
        /// </summary>
        public PlanogramImage RightImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdRight, PlanogramImageFacingType.Right, PlanogramImageType.None); }
            set { this.Model.PlanogramImageIdRight = (value != null) ? value.Id : null; }
        }

        #endregion

        #region Display

        /// <summary>
        /// Gets/Sets the front product image
        /// </summary>
        public PlanogramImage FrontDisplayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdDisplayFront, PlanogramImageFacingType.Front, PlanogramImageType.Display); }
            set { this.Model.PlanogramImageIdDisplayFront = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the back product image
        /// </summary>
        public PlanogramImage BackDisplayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdDisplayBack, PlanogramImageFacingType.Back, PlanogramImageType.Display); }
            set { this.Model.PlanogramImageIdDisplayBack = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the top product image
        /// </summary>
        public PlanogramImage TopDisplayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdDisplayTop, PlanogramImageFacingType.Top, PlanogramImageType.Display); }
            set { this.Model.PlanogramImageIdDisplayTop = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the bottom product image
        /// </summary>
        public PlanogramImage BottomDisplayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdDisplayBottom, PlanogramImageFacingType.Bottom, PlanogramImageType.Display); }
            set { this.Model.PlanogramImageIdDisplayBottom = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the left product image
        /// </summary>
        public PlanogramImage LeftDisplayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdDisplayLeft, PlanogramImageFacingType.Left, PlanogramImageType.Display); }
            set { this.Model.PlanogramImageIdDisplayLeft = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the right product image.
        /// </summary>
        public PlanogramImage RightDisplayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdDisplayRight, PlanogramImageFacingType.Right, PlanogramImageType.Display); }
            set { this.Model.PlanogramImageIdDisplayRight = (value != null) ? value.Id : null; }
        }

        #endregion

        #region Tray

        /// <summary>
        /// Gets/Sets the front product image
        /// </summary>
        public PlanogramImage FrontTrayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdTrayFront, PlanogramImageFacingType.Front, PlanogramImageType.Tray); }
            set { this.Model.PlanogramImageIdTrayFront = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the back product image
        /// </summary>
        public PlanogramImage BackTrayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdTrayBack, PlanogramImageFacingType.Back, PlanogramImageType.Tray); }
            set { this.Model.PlanogramImageIdTrayBack = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the top product image
        /// </summary>
        public PlanogramImage TopTrayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdTrayTop, PlanogramImageFacingType.Top, PlanogramImageType.Tray); }
            set { this.Model.PlanogramImageIdTrayTop = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the bottom product image
        /// </summary>
        public PlanogramImage BottomTrayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdTrayBottom, PlanogramImageFacingType.Bottom, PlanogramImageType.Tray); }
            set { this.Model.PlanogramImageIdTrayBottom = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the left product image
        /// </summary>
        public PlanogramImage LeftTrayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdTrayLeft, PlanogramImageFacingType.Left, PlanogramImageType.Tray); }
            set { this.Model.PlanogramImageIdTrayLeft = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the right product image.
        /// </summary>
        public PlanogramImage RightTrayImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdTrayRight, PlanogramImageFacingType.Right, PlanogramImageType.Tray); }
            set { this.Model.PlanogramImageIdTrayRight = (value != null) ? value.Id : null; }
        }

        #endregion

        #region PointOfPurchase

        /// <summary>
        /// Gets/Sets the front product image
        /// </summary>
        public PlanogramImage FrontPointOfPurchaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdPointOfPurchaseFront, PlanogramImageFacingType.Front, PlanogramImageType.PointOfPurchase); }
            set { this.Model.PlanogramImageIdPointOfPurchaseFront = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the back product image
        /// </summary>
        public PlanogramImage BackPointOfPurchaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdPointOfPurchaseBack, PlanogramImageFacingType.Back, PlanogramImageType.PointOfPurchase); }
            set { this.Model.PlanogramImageIdPointOfPurchaseBack = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the top product image
        /// </summary>
        public PlanogramImage TopPointOfPurchaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdPointOfPurchaseTop, PlanogramImageFacingType.Top, PlanogramImageType.PointOfPurchase); }
            set { this.Model.PlanogramImageIdPointOfPurchaseTop = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the bottom product image
        /// </summary>
        public PlanogramImage BottomPointOfPurchaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdPointOfPurchaseBottom, PlanogramImageFacingType.Bottom, PlanogramImageType.PointOfPurchase); }
            set { this.Model.PlanogramImageIdPointOfPurchaseBottom = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the left product image
        /// </summary>
        public PlanogramImage LeftPointOfPurchaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdPointOfPurchaseLeft, PlanogramImageFacingType.Left, PlanogramImageType.PointOfPurchase); }
            set { this.Model.PlanogramImageIdPointOfPurchaseLeft = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the right product image.
        /// </summary>
        public PlanogramImage RightPointOfPurchaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdPointOfPurchaseRight, PlanogramImageFacingType.Right, PlanogramImageType.PointOfPurchase); }
            set { this.Model.PlanogramImageIdPointOfPurchaseRight = (value != null) ? value.Id : null; }
        }

        #endregion

        #region Alternate

        /// <summary>
        /// Gets/Sets the front product image
        /// </summary>
        public PlanogramImage FrontAlternateImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdAlternateFront, PlanogramImageFacingType.Front, PlanogramImageType.Alternate); }
            set { this.Model.PlanogramImageIdAlternateFront = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the back product image
        /// </summary>
        public PlanogramImage BackAlternateImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdAlternateBack, PlanogramImageFacingType.Back, PlanogramImageType.Alternate); }
            set { this.Model.PlanogramImageIdAlternateBack = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the top product image
        /// </summary>
        public PlanogramImage TopAlternateImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdAlternateTop, PlanogramImageFacingType.Top, PlanogramImageType.Alternate); }
            set { this.Model.PlanogramImageIdAlternateTop = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the bottom product image
        /// </summary>
        public PlanogramImage BottomAlternateImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdAlternateBottom, PlanogramImageFacingType.Bottom, PlanogramImageType.Alternate); }
            set { this.Model.PlanogramImageIdAlternateBottom = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the left product image
        /// </summary>
        public PlanogramImage LeftAlternateImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdAlternateLeft, PlanogramImageFacingType.Left, PlanogramImageType.Alternate); }
            set { this.Model.PlanogramImageIdAlternateLeft = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the right product image.
        /// </summary>
        public PlanogramImage RightAlternateImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdAlternateRight, PlanogramImageFacingType.Right, PlanogramImageType.Alternate); }
            set { this.Model.PlanogramImageIdAlternateRight = (value != null) ? value.Id : null; }
        }

        #endregion

        #region Case

        /// <summary>
        /// Gets/Sets the front product image
        /// </summary>
        public PlanogramImage FrontCaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdCaseFront, PlanogramImageFacingType.Front, PlanogramImageType.Case); }
            set { this.Model.PlanogramImageIdCaseFront = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the back product image
        /// </summary>
        public PlanogramImage BackCaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdCaseBack, PlanogramImageFacingType.Back, PlanogramImageType.Case); }
            set { this.Model.PlanogramImageIdCaseBack = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the top product image
        /// </summary>
        public PlanogramImage TopCaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdCaseTop, PlanogramImageFacingType.Top, PlanogramImageType.Case); }
            set { this.Model.PlanogramImageIdCaseTop = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the bottom product image
        /// </summary>
        public PlanogramImage BottomCaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdCaseBottom, PlanogramImageFacingType.Bottom, PlanogramImageType.Case); }
            set { this.Model.PlanogramImageIdCaseBottom = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the left product image
        /// </summary>
        public PlanogramImage LeftCaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdCaseLeft, PlanogramImageFacingType.Left, PlanogramImageType.Case); }
            set { this.Model.PlanogramImageIdCaseLeft = (value != null) ? value.Id : null; }
        }

        /// <summary>
        /// Gets/Sets the right product image.
        /// </summary>
        public PlanogramImage RightCaseImage
        {
            get { return this.GetImageLazy(this.Model.PlanogramImageIdCaseRight, PlanogramImageFacingType.Right, PlanogramImageType.Case); }
            set { this.Model.PlanogramImageIdCaseRight = (value != null) ? value.Id : null; }
        }

        #endregion

        #endregion

        #region Calculated Properties

        /// <summary>
        /// Returns the number of units for this product accross all positions.
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return _planogramProduct.MetaTotalUnits; }
            //private set
            //{
            //    if (_metaTotalUnits != value)
            //    {
            //        _metaTotalUnits = value;
            //        OnPropertyChanged("MetaTotalUnits");
            //    }
            //}
        }

        /// <summary>
        /// Returns the not achieved inventory accross all positions.
        /// </summary>
        public Boolean? MetaNotAchievedInventory
        {
            get { return _planogramProduct.MetaNotAchievedInventory; }
        }

        /// <summary>
        /// Returns the Linear Space Percentage accross all positions.
        /// </summary>
        public Single? MetaPlanogramLinearSpacePercentage
        {
            get { return _planogramProduct.MetaPlanogramLinearSpacePercentage; }
        }

        /// <summary>
        /// Returns the Area Space Percentage accross all positions.
        /// </summary>
        public Single? MetaPlanogramAreaSpacePercentage
        {
            get { return _planogramProduct.MetaPlanogramAreaSpacePercentage; }
        }

        /// <summary>
        /// Returns the Volumetric Space Percentage accross all positions.
        /// </summary>
        public Single? MetaPlanogramVolumetricSpacePercentage
        {
            get { return _planogramProduct.MetaPlanogramVolumetricSpacePercentage; }
        }

        /// <summary>
        /// Returns if product is in master data.
        /// </summary>
        public Boolean? MetaIsInMasterData
        {
            get { return _planogramProduct.MetaIsInMasterData; }
        }

        public Int32? MetaTotalMainFacings
        {
            get { return _planogramProduct.MetaTotalMainFacings; }
        }

        public Int32? MetaTotalXFacings
        {
            get { return _planogramProduct.MetaTotalXFacings; }
        }

        public Int32? MetaTotalYFacings
        {
            get { return _planogramProduct.MetaTotalYFacings; }
        }

        public Int32? MetaTotalZFacings
        {
            get { return _planogramProduct.MetaTotalZFacings; }
        }

        public Boolean? MetaIsRangedInAssortment
        {
            get { return _planogramProduct.MetaIsRangedInAssortment; }
        }

        public Boolean? MetaNotAchievedCases
        {
            get { return _planogramProduct.MetaNotAchievedCases; }
        }

        public Boolean? MetaNotAchievedDOS
        {
            get { return _planogramProduct.MetaNotAchievedDOS; }
        }

        public Boolean? MetaIsOverShelfLifePercent
        {
            get { return _planogramProduct.MetaIsOverShelfLifePercent; }
        }

        public Boolean? MetaNotAchievedDeliveries
        {
            get { return _planogramProduct.MetaNotAchievedDeliveries; }
        }

        public Int32? MetaPositionCount
        {
            get { return Model.MetaPositionCount; }
        }

        public Int32? MetaTotalFacings
        {
            get { return Model.MetaTotalFacings; }
        }

        public Single? MetaTotalLinearSpace
        {
            get { return Model.MetaTotalLinearSpace; }
        }

        public Single? MetaTotalAreaSpace
        {
            get { return Model.MetaTotalAreaSpace; }
        }

        public Single? MetaTotalVolumetricSpace
        {
            get { return Model.MetaTotalVolumetricSpace; }
        }

        public String MetaCDTNode
        {
            get { return Model.MetaCDTNode; }
        }

        public Boolean? MetaIsProductRuleBroken
        {
            get { return Model.MetaIsProductRuleBroken; }
        }

        public Boolean? MetaIsFamilyRuleBroken
        {
            get { return Model.MetaIsFamilyRuleBroken; }
        }

        public Boolean? MetaIsInheritanceRuleBroken
        {
            get { return Model.MetaIsInheritanceRuleBroken; }
        }

        public Boolean? MetaIsLocalProductRuleBroken
        {
            get { return Model.MetaIsLocalProductRuleBroken; }
        }

        public Boolean? MetaIsDistributionRuleBroken
        {
            get { return Model.MetaIsDistributionRuleBroken; }
        }

        public Boolean? MetaIsCoreRuleBroken
        {
            get { return Model.MetaIsCoreRuleBroken; }
        }

        public Boolean? MetaIsDelistProductRuleBroken
        {
            get { return Model.MetaIsDelistProductRuleBroken; }
        }

        public Boolean? MetaIsForceProductRuleBroken
        {
            get { return Model.MetaIsForceProductRuleBroken; }
        }

        public Boolean? MetaIsPreserveProductRuleBroken
        {
            get { return Model.MetaIsPreserveProductRuleBroken; }
        }

        public Boolean? MetaIsMinimumHurdleProductRuleBroken
        {
            get { return Model.MetaIsMinimumHurdleProductRuleBroken; }
        }

        public Boolean? MetaIsMaximumProductFamilyRuleBroken
        {
            get { return Model.MetaIsMaximumProductFamilyRuleBroken; }
        }

        public Boolean? MetaIsMinimumProductFamilyRuleBroken
        {
            get { return Model.MetaIsMinimumProductFamilyRuleBroken; }
        }

        public Boolean? MetaIsDependencyFamilyRuleBroken
        {
            get { return Model.MetaIsDependencyFamilyRuleBroken; }
        }

        public Boolean? MetaIsDelistFamilyRuleBroken
        {
            get { return Model.MetaIsDelistFamilyRuleBroken; }
        }

        public Boolean? MetaIsBuddied
        {
            get { return Model.MetaIsBuddied; }
        }

        public PlanogramItemComparisonStatusType MetaComparisonStatus
        {
            get { return Model.MetaComparisonStatus; }
        }

        #endregion

        /// <summary>
        /// Returns the parent plan view.
        /// </summary>
        protected PlanogramViewModelBase ParentPlangoram
        {
            get { return _parentPlanogram; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="product"></param>
        public PlanogramProductViewModelBase(PlanogramProduct product)
        {
            _planogramProduct = product;

            _performanceData = product.GetPlanogramPerformanceData();

            //if no performance data was found then add one.
            if (_performanceData == null && product.Parent != null)
            {
                _performanceData = PlanogramPerformanceData.NewPlanogramPerformanceData(Model);
                product.Parent.Performance.PerformanceData.Add(_performanceData);
            }

            product.PropertyChanging += Model_PropertyChanging;
            product.PropertyChanged += Model_PropertyChanged;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="product"></param>
        public PlanogramProductViewModelBase(PlanogramProduct product, PlanogramViewModelBase parentPlanogramView)
        {
            _planogramProduct = product;
            _parentPlanogram = parentPlanogramView;

            _performanceData = product.GetPlanogramPerformanceData();

            //if no performance data was found then add one.
            if (_performanceData == null && product.Parent != null)
            {
                PlanogramPerformanceDataList planPerformance = product.Parent.Performance.PerformanceData;

                _performanceData = PlanogramPerformanceData.NewPlanogramPerformanceData(Model);
                planPerformance.RaiseListChangedEvents = false;
                planPerformance.Add(_performanceData);
                planPerformance.RaiseListChangedEvents = true;
            }

            product.PropertyChanging += Model_PropertyChanging;
            product.PropertyChanged += Model_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(e.PropertyName);
        }
        protected virtual void OnModelPropertyChanging(String propertyName)
        {
            if (typeof(PlanogramProductViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(e.PropertyName);
        }
        protected virtual void OnModelPropertyChanged(String propertyName)
        {
            if (typeof(PlanogramProductViewModelBase).GetProperty(propertyName) != null)
            {
                //Pass through
                OnPropertyChanged(propertyName);
            }

            switch (propertyName)
            {
                case "Gtin":
                    OnPropertyChanged("AssortmentProduct");
                    break;

                #region Unit Images
                case "PlanogramImageIdFront":
                    OnPropertyChanged("FrontImage");
                    break;

                case "PlanogramImageIdBack":
                    OnPropertyChanged("BackImage");
                    break;

                case "PlanogramImageIdTop":
                    OnPropertyChanged("TopImage");
                    break;

                case "PlanogramImageIdBottom":
                    OnPropertyChanged("BottomImage");
                    break;

                case "PlanogramImageIdLeft":
                    OnPropertyChanged("LeftImage");
                    break;

                case "PlanogramImageIdRight":
                    OnPropertyChanged("RightImage");
                    break;
                #endregion

                #region Display Images
                case "PlanogramImageIdDisplayFront":
                    OnPropertyChanged("FrontDisplayImage");
                    break;

                case "PlanogramImageIdDisplayBack":
                    OnPropertyChanged("BackDisplayImage");
                    break;

                case "PlanogramImageIdDisplayTop":
                    OnPropertyChanged("TopDisplayImage");
                    break;

                case "PlanogramImageIdDisplayBottom":
                    OnPropertyChanged("BottomDisplayImage");
                    break;

                case "PlanogramImageIdDisplayLeft":
                    OnPropertyChanged("LeftDisplayImage");
                    break;

                case "PlanogramImageIdDisplayRight":
                    OnPropertyChanged("RightDisplayImage");
                    break;

                #endregion

                #region Tray Images
                case "PlanogramImageIdTrayFront":
                    OnPropertyChanged("FrontTrayImage");
                    break;

                case "PlanogramImageIdTrayBack":
                    OnPropertyChanged("BackTrayImage");
                    break;

                case "PlanogramImageIdTrayTop":
                    OnPropertyChanged("TopTrayImage");
                    break;

                case "PlanogramImageIdTrayBottom":
                    OnPropertyChanged("BottomTrayImage");
                    OnPropertyChanged("BottomTrayImageData");
                    break;

                case "PlanogramImageIdTrayLeft":
                    OnPropertyChanged("LeftTrayImage");
                    break;

                case "PlanogramImageIdTrayRight":
                    OnPropertyChanged("RightTrayImage");
                    break;

                #endregion

                #region PointOfPurchase Images
                case "PlanogramImageIdPointOfPurchaseFront":
                    OnPropertyChanged("FrontPointOfPurchaseImage");
                    break;

                case "PlanogramImageIdPointOfPurchaseBack":
                    OnPropertyChanged("BackPointOfPurchaseImage");
                    break;

                case "PlanogramImageIdPointOfPurchaseTop":
                    OnPropertyChanged("TopPointOfPurchaseImage");
                    break;

                case "PlanogramImageIdPointOfPurchaseBottom":
                    OnPropertyChanged("BottomPointOfPurchaseImage");
                    break;

                case "PlanogramImageIdPointOfPurchaseLeft":
                    OnPropertyChanged("LeftPointOfPurchaseImage");
                    break;

                case "PlanogramImageIdPointOfPurchaseRight":
                    OnPropertyChanged("RightPointOfPurchaseImage");
                    break;

                #endregion

                #region Alternate Images
                case "PlanogramImageIdAlternateFront":
                    OnPropertyChanged("FrontAlternateImage");
                    break;

                case "PlanogramImageIdAlternateBack":
                    OnPropertyChanged("BackAlternateImage");
                    break;

                case "PlanogramImageIdAlternateTop":
                    OnPropertyChanged("TopAlternateImage");
                    break;

                case "PlanogramImageIdAlternateBottom":
                    OnPropertyChanged("BottomAlternateImage");
                    break;

                case "PlanogramImageIdAlternateLeft":
                    OnPropertyChanged("LeftAlternateImage");
                    break;

                case "PlanogramImageIdAlternateRight":
                    OnPropertyChanged("RightAlternateImage");
                    break;

                #endregion

                #region Case Images
                case "PlanogramImageIdCaseFront":
                    OnPropertyChanged("FrontCaseImage");
                    break;

                case "PlanogramImageIdCaseBack":
                    OnPropertyChanged("BackCaseImage");
                    break;

                case "PlanogramImageIdCaseTop":
                    OnPropertyChanged("TopCaseImage");
                    break;

                case "PlanogramImageIdCaseBottom":
                    OnPropertyChanged("BottomCaseImage");
                    break;

                case "PlanogramImageIdCaseLeft":
                    OnPropertyChanged("LeftCaseImage");
                    break;

                case "PlanogramImageIdCaseRight":
                    OnPropertyChanged("RightCaseImage");
                    break;

                    #endregion
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("{0} {1}", this.Gtin, this.Name);
        }


        /// <summary>
        /// Gets the image with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <returns></returns>
        protected virtual PlanogramImage GetImageLazy(Object id, PlanogramImageFacingType facingType, PlanogramImageType imageType)
        {
            Planogram planogram = this.Model.Parent;
            if (planogram == null) return null;

            Boolean isLoadingImages;
            PlanogramImage img = planogram.GetImageLazy(out isLoadingImages, this.Model, id, facingType, imageType);
            this.IsLoadingImages = isLoadingImages;

            UpdateExternalImages(id, img, facingType, imageType);

            return img;
        }

        /// <summary>
        ///     Updates the lookup dictionary of external images.
        /// </summary>
        /// <param name="imageId"></param>
        /// <param name="image"></param>
        /// <param name="facingType">The image facing type.</param>
        /// <param name="imageType">The image type.</param>
        private void UpdateExternalImages(Object imageId, PlanogramImage image, PlanogramImageFacingType facingType, PlanogramImageType imageType)
        {
            String externalImageDescription = GetExternalImageDescription(image, imageId);
            String imagePropertyName = GetImagePropertyName(facingType, imageType);
            Boolean isStoredInPlanogram = String.IsNullOrEmpty(externalImageDescription);
            Boolean imageWasStoredInPlanogram = !_externalImageDescriptions.ContainsKey(imagePropertyName);

            if (isStoredInPlanogram)
            {
                if (imageWasStoredInPlanogram) return;

                // Remove the image if it is now stored in the planogram.
                _externalImageDescriptions.Remove(imagePropertyName);
            }
            else
            {
                if (!imageWasStoredInPlanogram) return;

                // Add the image if it is now external.
                _externalImageDescriptions.Add(imagePropertyName, externalImageDescription);
            }

            //  Notifiy the dictionary has changed.
            OnPropertyChanged("ExternalImageDescriptions");
        }

        /// <summary>
        /// Forces the product images to be refreshed. 
        /// </summary>
        internal void RefreshImages()
        {
            if (this.IsDisposed) return;

            //refire property changed
            BeginUpdate();

            OnPropertyChanged("FrontImage");
            OnPropertyChanged("BackImage");
            OnPropertyChanged("TopImage");
            OnPropertyChanged("BottomImage");
            OnPropertyChanged("LeftImage");
            OnPropertyChanged("RightImage");

            OnPropertyChanged("FrontDisplayImage");
            OnPropertyChanged("BackDisplayImage");
            OnPropertyChanged("TopDisplayImage");
            OnPropertyChanged("BottomDisplayImage");
            OnPropertyChanged("LeftDisplayImage");
            OnPropertyChanged("RightDisplayImage");

            OnPropertyChanged("FrontTrayImage");
            OnPropertyChanged("BackTrayImage");
            OnPropertyChanged("TopTrayImage");
            OnPropertyChanged("BottomTrayImage");
            OnPropertyChanged("LeftTrayImage");
            OnPropertyChanged("RightTrayImage");

            OnPropertyChanged("FrontPointOfPurchaseImage");
            OnPropertyChanged("BackPointOfPurchaseImage");
            OnPropertyChanged("TopPointOfPurchaseImage");
            OnPropertyChanged("BottomPointOfPurchaseImage");
            OnPropertyChanged("LeftPointOfPurchaseImage");
            OnPropertyChanged("RightPointOfPurchaseImage");

            OnPropertyChanged("FrontAlternateImage");
            OnPropertyChanged("BackAlternateImage");
            OnPropertyChanged("TopAlternateImage");
            OnPropertyChanged("BottomAlternateImage");
            OnPropertyChanged("LeftAlternateImage");
            OnPropertyChanged("RightAlternateImage");

            OnPropertyChanged("FrontCaseImage");
            OnPropertyChanged("BackCaseImage");
            OnPropertyChanged("TopCaseImage");
            OnPropertyChanged("BottomCaseImage");
            OnPropertyChanged("LeftCaseImage");
            OnPropertyChanged("RightCaseImage");

            EndUpdate();
        }

        /// <summary>
        /// For IDataErrorInfo
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            return ((IDataErrorInfo)Model)[columnName];
        }


        /// <summary>
        /// Enumerates through displayable fields for this item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields(Planogram context = null)
        {
            //cycle through plan product fields
            foreach (var field in PlanogramProduct.EnumerateDisplayableFieldInfos(
                /*incMeta*/true, /*incCustom*/true, /*incPerformance*/true, ((context != null) ? context.Performance : null)))
            {
                yield return field;
            }
        }

        #endregion

        #region Private Static Helpers

        /// <summary>
        ///     Gets the image property name that would be applicable to the given <paramref name="facingType"/> and <paramref name="imageType"/>.
        /// </summary>
        /// <param name="facingType">The <see cref="PlanogramImageFacingType"/> that the returned name must be for.</param>
        /// <param name="imageType">The <see cref="PlanogramImageType"/> that the returned name must be for.</param>
        /// <returns></returns>
        private static String GetImagePropertyName(PlanogramImageFacingType facingType, PlanogramImageType imageType)
        {
            return String.Format("{0}{1}Image", facingType, imageType == PlanogramImageType.None
                                                                ? (Object)String.Empty
                                                                : imageType);
        }

        /// <summary>
        ///     Gets the external image description is there is any.
        /// </summary>
        /// <param name="image">The <see cref="PlanogramImage"/> from wich to extract the description <b>if</b> it is external.</param>
        /// <param name="imageId">
        ///     Image Id. If not null the image is not external and will have no external image
        ///     description.
        /// </param>
        /// <returns></returns>
        private static String GetExternalImageDescription(PlanogramImage image, Object imageId)
        {
            return imageId == null &&
                   image != null &&
                   image.ImageData != null
                       ? image.Description
                       : null;
        }

        #endregion

        #region IPlanProductRenderable Members

        Single IPlanProductRenderable.TrayHeight
        {
            get { return this.Model.GetUnorientatedTraySize().Height; }
        }
        Single IPlanProductRenderable.TrayWidth
        {
            get { return this.Model.GetUnorientatedTraySize().Width; }
        }
        Single IPlanProductRenderable.TrayDepth
        {
            get { return this.Model.GetUnorientatedTraySize().Depth; }
        }

        Single IPlanProductRenderable.PegDefaultHeight
        {
            get
            {
                if (this.ParentPlangoram == null) return 1;
                switch (this.ParentPlangoram.LengthUnitsOfMeasure)
                {
                    default:
                    case PlanogramLengthUnitOfMeasureType.Centimeters: return 0.8F;
                    case PlanogramLengthUnitOfMeasureType.Inches: return 0.25F;
                }

            }
        }

        Single IPlanProductRenderable.PegDefaultWidth
        {
            get
            {
                if (this.ParentPlangoram == null) return 1;
                switch (this.ParentPlangoram.LengthUnitsOfMeasure)
                {
                    default:
                    case PlanogramLengthUnitOfMeasureType.Centimeters: return 0.8F;
                    case PlanogramLengthUnitOfMeasureType.Inches: return 0.25F;
                }

            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void OnDisposing(Boolean disposing)
        {
            base.OnDisposing(disposing);

            _planogramProduct.PropertyChanging -= Model_PropertyChanging;
            _planogramProduct.PropertyChanged -= Model_PropertyChanged;

        }

        #endregion
    }
}
