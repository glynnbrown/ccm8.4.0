﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Model;
using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Represents a <see cref="PlanogramBlockingGroup"/> with additional UI specific functionality.
    /// </summary>
    public abstract class PlanogramBlockingGroupViewModelBase : ViewModelBase
    {
        #region Fields

        /// <summary>
        /// The <see cref="PlanogramBlockingGroup"/> that this view model represents.
        /// </summary>
        private PlanogramBlockingGroup _model;

        #endregion

        #region Properties

        #region Model Properties
        /// <summary>
        /// The <see cref="PlanogramBlockingGroup"/> that this view model represents.
        /// </summary>
        public PlanogramBlockingGroup Model
        {
            get { return _model; }
        }

        /// <summary>
        /// The name of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public String Name
        {
            get
            {
                return _model.Name;
            }
            set
            {
                _model.Name = value;
            }
        }

        /// <summary>
        /// The colour of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public Int32 Colour
        {
            get
            {
                return _model.Colour;
            }
            set
            {
                _model.Colour = value;
            }
        }

        /// <summary>
        /// The fill pattern of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public PlanogramBlockingFillPatternType FillPatternType
        {
            get
            {
                return _model.FillPatternType;
            }
            set
            {
                _model.FillPatternType = value;
            }
        }

        /// <summary>
        /// The primary placement type of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementPrimaryType
        {
            get
            {
                return _model.BlockPlacementPrimaryType;
            }
            set
            {
                _model.BlockPlacementPrimaryType = value;
            }
        }

        /// <summary>
        /// The secondary placement type of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementSecondaryType
        {
            get
            {
                return _model.BlockPlacementSecondaryType;
            }
            set
            {
                _model.BlockPlacementSecondaryType = value;
            }
        }

        /// <summary>
        /// The tertiary placement type of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementTertiaryType
        {
            get
            {
                return _model.BlockPlacementTertiaryType;
            }
            set
            {
                _model.BlockPlacementTertiaryType = value;
            }
        }

        /// <summary>
        /// Gets the Total Space Percentage of this <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public Single TotalSpacePercentage
        {
            get { return _model.TotalSpacePercentage; }
        }

        /// <summary>
        /// The CanMerge of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public Boolean CanMerge
        {
            get
            {
                return _model.CanMerge;
            }
            set
            {
                _model.CanMerge = value;
            }
        }

        /// <summary>
        /// The CanMerge of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public Boolean IsLimited
        {
            get
            {
                return _model.IsLimited;
            }
            set
            {
                _model.IsLimited = value;
            }
        }


        /// <summary>
        /// The LimitedPercentage of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public Single LimitedPercentage
        {
            get
            {
                return _model.LimitedPercentage;
            }
            set
            {
                _model.LimitedPercentage = value;
            }
        }

        /// <summary>
        /// The BlockSpaceLimitedPercentage of the <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        public Single BlockSpaceLimitedPercentage
        {
            get
            {
                return _model.BlockSpaceLimitedPercentage;
            }
            set
            {
                _model.BlockSpaceLimitedPercentage = value;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramBlockingGroupViewModelBase(PlanogramBlockingGroup blockingGroup)
        {
            _model = blockingGroup;
            _model.PropertyChanged += Model_PropertyChanged;
        }

        #endregion

        #region Event Handlers
        /// <summary>
        /// Called whenever a property changes on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(sender, e.PropertyName);
        }

        protected virtual void OnModelPropertyChanged(Object sender, String propertyName)
        {
            if (typeof(PlanogramBlockingGroupViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }
        }

        #endregion

        #region Methods

        protected override string GetError(string columnName)
        {
            return String.Empty;
        }

        #endregion

        #region IDisposable Members

        protected override void OnDisposing(Boolean disposing)
        {
            base.OnDisposing(disposing);

            _model.PropertyChanged -= Model_PropertyChanged;
        }

        #endregion
    }
}
