﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Provides a base class for a viewmodel of a PlanogramAssembly
    /// </summary>
    public abstract class PlanogramAssemblyViewModelBase : ViewModelBase, IPlanAssemblyRenderable
    {
        #region Fields

        private Boolean _isInitialized;

        private PlanogramFixtureViewModelBase _parentFixtureView;

        private PlanogramFixtureAssembly _fixtureAssembly;
        private readonly PlanogramAssembly _assembly;

        private readonly ObservableCollection<PlanogramComponentViewModelBase> _components = new ObservableCollection<PlanogramComponentViewModelBase>();

        private PointValue _worldCoordinates = new PointValue();
        private RotationValue _worldRotation = new RotationValue();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the fixture assembly model
        /// </summary>
        public PlanogramFixtureAssembly FixtureAssemblyModel
        {
            get { return _fixtureAssembly; }
        }

        /// <summary>
        /// Gets the assembly model
        /// </summary>
        public PlanogramAssembly AssemblyModel
        {
            get { return _assembly; }
        }

        /// <summary>
        /// Gets the collection of viewmodels for child components.
        /// </summary>
        protected internal ObservableCollection<PlanogramComponentViewModelBase> ComponentViews
        {
            get { return _components; }
        }

        /// <summary>
        /// Gets the parent fixture viewmodel.
        /// </summary>
        protected internal PlanogramFixtureViewModelBase ParentFixtureView
        {
            get { return _parentFixtureView; }
        }

        #region Model Properties

        public Single X
        {
            get { return _fixtureAssembly.X; }
            set { _fixtureAssembly.X = value; }
        }

        public Single Y
        {
            get { return _fixtureAssembly.Y; }
            set { _fixtureAssembly.Y = value; }
        }

        public Single Z
        {
            get { return _fixtureAssembly.Z; }
            set { _fixtureAssembly.Z = value; }
        }

        public Single Slope
        {
            get { return _fixtureAssembly.Slope; }
            set { _fixtureAssembly.Slope = value; }
        }

        public Single Angle
        {
            get { return _fixtureAssembly.Angle; }
            set { _fixtureAssembly.Angle = value; }
        }

        public Single Roll
        {
            get { return _fixtureAssembly.Roll; }
            set { _fixtureAssembly.Roll = value; }
        }

        public String Name
        {
            get { return _assembly.Name; }
            set
            {
                _assembly.Name = value;
            }
        }

        public Single Height
        {
            get { return _assembly.Height; }
            set { _assembly.Height = value; }
        }

        public Single Width
        {
            get { return _assembly.Width; }
            set { _assembly.Width = value; }
        }

        public Single Depth
        {
            get { return _assembly.Depth; }
            set { _assembly.Depth = value; }
        }

        #endregion

        #region Meta Data Properties

        public Int32? MetaComponentCount
        {
            get { return _fixtureAssembly.MetaComponentCount; }
        }

        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return _fixtureAssembly.MetaTotalMerchandisableLinearSpace; }
        }

        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return _fixtureAssembly.MetaTotalMerchandisableAreaSpace; }
        }

        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return _fixtureAssembly.MetaTotalMerchandisableVolumetricSpace; }
        }

        public Single? MetaTotalLinearWhiteSpace
        {
            get { return _fixtureAssembly.MetaTotalLinearWhiteSpace; }
        }

        public Single? MetaTotalAreaWhiteSpace
        {
            get { return _fixtureAssembly.MetaTotalAreaWhiteSpace; }
        }

        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return _fixtureAssembly.MetaTotalVolumetricWhiteSpace; }
        }

        public Int32? MetaProductsPlaced
        {
            get { return _fixtureAssembly.MetaProductsPlaced; }
        }

        public Int32? MetaNewProducts
        {
            get { return _fixtureAssembly.MetaNewProducts; }
        }

        public Int32? MetaChangesFromPreviousCount
        {
            get { return _fixtureAssembly.MetaChangesFromPreviousCount; }
        }

        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return _fixtureAssembly.MetaChangeFromPreviousStarRating; }
        }

        public Int32? MetaBlocksDropped
        {
            get { return _fixtureAssembly.MetaBlocksDropped; }
        }

        public Int32? MetaNotAchievedInventory
        {
            get { return _fixtureAssembly.MetaNotAchievedInventory; }
        }

        public Int32? MetaTotalFacings
        {
            get { return _fixtureAssembly.MetaTotalFacings; }
        }

        public Int16? MetaAverageFacings
        {
            get { return _fixtureAssembly.MetaAverageFacings; }
        }

        public Int32? MetaTotalUnits
        {
            get { return _fixtureAssembly.MetaTotalUnits; }
        }

        public Int32? MetaAverageUnits
        {
            get { return _fixtureAssembly.MetaAverageUnits; }
        }

        public Single? MetaMinDos
        {
            get { return _fixtureAssembly.MetaMinDos; }
        }

        public Single? MetaMaxDos
        {
            get { return _fixtureAssembly.MetaMaxDos; }
        }

        public Single? MetaAverageDos
        {
            get { return _fixtureAssembly.MetaAverageDos; }
        }

        public Single? MetaMinCases
        {
            get { return _fixtureAssembly.MetaMinCases; }
        }

        public Single? MetaAverageCases
        {
            get { return _fixtureAssembly.MetaAverageCases; }
        }

        public Single? MetaMaxCases
        {
            get { return _fixtureAssembly.MetaMaxCases; }
        }

        public Int16? MetaSpaceToUnitsIndex
        {
            get { return _fixtureAssembly.MetaSpaceToUnitsIndex; }
        }

        public Int16? MetaTotalFrontFacings
        {
            get { return _fixtureAssembly.MetaTotalFrontFacings; }
        }

        public Single? MetaAverageFrontFacings
        {
            get { return _fixtureAssembly.MetaAverageFrontFacings; }
        }

        #endregion

        #region WorldX

        /// <summary>
        /// Gets/Sets the X coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldX
        {
            get { return _worldCoordinates.X; }
            set { SetWorldCoordinates(value, WorldY, WorldZ); }
        }

        #endregion

        #region WorldY

        /// <summary>
        /// Gets/Sets the Y coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldY
        {
            get { return _worldCoordinates.Y; }
            set { SetWorldCoordinates(WorldX, value, WorldZ); }
        }

        #endregion

        #region WorldZ

        /// <summary>
        /// Gets/Sets the Z coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldZ
        {
            get { return _worldCoordinates.Z; }
            set { SetWorldCoordinates(WorldX, WorldY, value); }
        }

        #endregion

        #region WorldAngle

        /// <summary>
        /// Gets/Sets the rotation angle value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldAngle
        {
            get { return _worldRotation.Angle; }
            set { SetWorldRotation(value, WorldSlope, WorldRoll); }
        }

        #endregion

        #region WorldSlope

        /// <summary>
        /// Gets/Sets the rotation slope value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldSlope
        {
            get { return _worldRotation.Slope; }
            set { SetWorldRotation(WorldAngle, value, WorldRoll); }
        }

        #endregion

        #region WorldRoll

        /// <summary>
        /// Gets/Sets the rotation roll value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single WorldRoll
        {
            get { return _worldRotation.Roll; }
            set { SetWorldRotation(WorldAngle, WorldSlope, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="fixtureAssembly"></param>
        public PlanogramAssemblyViewModelBase(PlanogramFixtureViewModelBase parentFixtureView,
            PlanogramFixtureAssembly fixtureAssembly, PlanogramAssembly assembly)
        {
            _parentFixtureView = parentFixtureView;

            _fixtureAssembly = fixtureAssembly;
            fixtureAssembly.PropertyChanging += Model_PropertyChanging;
            fixtureAssembly.PropertyChanged += Model_PropertyChanged;

            Debug.Assert(Object.Equals(assembly.Id, fixtureAssembly.PlanogramAssemblyId), "Incorrect assembly");

            _assembly = assembly;
            _assembly.PropertyChanging += Model_PropertyChanging;
            _assembly.PropertyChanged += Model_PropertyChanged;
            _assembly.Components.BulkCollectionChanged += PlanogramAssembly_ComponentsBulkCollectionChanged;

            UpdateWorldCoordinatesAndRotation();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(sender, e.PropertyName);
        }
        protected virtual void OnModelPropertyChanging(Object sender, String propertyName)
        {
            if (typeof(PlanogramAssemblyViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the source models
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(sender, e.PropertyName);
        }
        protected virtual void OnModelPropertyChanged(Object sender, String propertyName)
        {
            if (typeof(PlanogramAssemblyViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }

            switch (propertyName)
            {
                case "X":
                case "Y":
                case "Z":
                case "Angle":
                case "Slope":
                case "Roll":
                    UpdateWorldCoordinatesAndRotation();
                    break;
            }
        }

        /// <summary>
        /// Called when the PlanogramAssembly.Components collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramAssembly_ComponentsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            OnPlanogramAssemblyComponentsBulkCollectionChanged(e);
        }
        protected virtual void OnPlanogramAssemblyComponentsBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramAssemblyComponent assemblyComponent in e.ChangedItems)
                    {
                        PlanogramComponentViewModelBase componentView = CreateComponentView(assemblyComponent);
                        if (componentView != null)
                        {
                            _components.Add(componentView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramAssemblyComponent assemblyComponent in e.ChangedItems)
                    {
                        PlanogramComponentViewModelBase componentView =
                            _components.FirstOrDefault(a => a.AssemblyComponentModel == assemblyComponent);
                        if (componentView != null)
                        {
                            _components.Remove(componentView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        _components.Clear();

                        foreach (PlanogramAssemblyComponent assemblyComponent in this.AssemblyModel.Components)
                        {
                            PlanogramComponentViewModelBase componentView = CreateComponentView(assemblyComponent);
                            if (componentView != null)
                            {
                                _components.Add(componentView);
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the parent vm changes.
        /// </summary>
        protected override void OnParentChanged(object oldValue, object newValue)
        {
            base.OnParentChanged(oldValue, newValue);

            PlanogramFixtureViewModelBase oldParent = oldValue as PlanogramFixtureViewModelBase;
            if (oldParent != null)
            {
                oldParent.PropertyChanged -= Parent_PropertyChanged;
            }

            PlanogramFixtureViewModelBase newParent = newValue as PlanogramFixtureViewModelBase;
            if (newParent != null)
            {
                newParent.PropertyChanged += Parent_PropertyChanged;
            }
        }

        /// <summary>
        /// Called whenever a property changes on the parent vm.
        /// </summary>
        private void Parent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.IsDisposed) return;

            switch (e.PropertyName)
            {
                case "WorldX":
                case "WorldY":
                case "WorldZ":
                case "WorldAngle":
                case "WorldSlope":
                case "WorldRoll":
                    UpdateWorldCoordinatesAndRotation();
                    break;

                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    if (sender is PlanogramFixtureViewModelBase)
                    {
                        UpdateWorldCoordinatesAndRotation();
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the collection of child viewmodels.
        /// </summary>
        protected void InitializeChildViews()
        {
            if (!_isInitialized)
            {
                //subscribe child collections
                AddChildEventHooks(_components);
                _components.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramComponentViewModelBase>;

                OnPlanogramAssemblyComponentsBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));

                _isInitialized = true;
            }
        }

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// For IDataErrorInfo
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            String error = ((IDataErrorInfo)this.AssemblyModel)[columnName];
            if (!String.IsNullOrEmpty(error))
            {
                return error;
            }

            return ((IDataErrorInfo)this.FixtureAssemblyModel)[columnName];
        }

        /// <summary>
        /// Creates a new viewmodel of the given assembly component
        /// </summary>
        /// <param name="assemblyComponent"></param>
        /// <returns></returns>
        protected abstract PlanogramComponentViewModelBase CreateComponentView(PlanogramAssemblyComponent assemblyComponent);

        #region World Space Methods

        /// <summary>
        /// Updates the values of the world coordinates
        /// </summary>
        private void UpdateWorldCoordinatesAndRotation()
        {
            //get the world coordinates and rotation
            _worldCoordinates =
                    this.FixtureAssemblyModel.GetPlanogramRelativeCoordinates(
                        this.ParentFixtureView.FixtureItemModel);

            _worldRotation =
                this.FixtureAssemblyModel.GetPlanogramRelativeRotation(
                    this.ParentFixtureView.FixtureItemModel);

            //fire off property changed events
            OnPropertyChanged("WorldX");
            OnPropertyChanged("WorldY");
            OnPropertyChanged("WorldZ");
            OnPropertyChanged("WorldAngle");
            OnPropertyChanged("WorldSlope");
            OnPropertyChanged("WorldRoll");
        }


        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(Single newWorldX, Single newWorldY, Single newWorldZ)
        {
            SetWorldCoordinates(new PointValue(newWorldX, newWorldY, newWorldZ));
        }
        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(PointValue newCoordinates)
        {
            RectValue thisBounds = new RectValue(
                    newCoordinates.X, newCoordinates.Y, newCoordinates.Z,
                    this.Width, this.Height, this.Depth);

            PlanogramFixtureItem parentFixtureItem = this.ParentFixtureView.FixtureItemModel;
            PlanogramFixture parentFixture = this.ParentFixtureView.FixtureModel;

            //check whether the new coords are still within the bounds of the parent bay.
            RectValue parentBayBounds = new RectValue(
                parentFixtureItem.X, parentFixtureItem.Y, parentFixtureItem.Z,
                parentFixture.Width, parentFixture.Height, parentFixture.Depth);

            if (!thisBounds.CollidesWith(parentBayBounds))
            {
                Planogram parentPlan = this.AssemblyModel.Parent;

                foreach (PlanogramFixtureItem fixtureItem in
                    parentPlan.FixtureItems.OrderBy(f => f.BaySequenceNumber))
                {
                    PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();

                    RectValue bayBounds = new RectValue(fixtureItem.X, fixtureItem.Y, fixtureItem.Z,
                    fixture.Width, fixture.Height, fixture.Depth);

                    if (thisBounds.CollidesWith(bayBounds))
                    {
                        parentFixtureItem = fixtureItem;
                        parentFixture = fixture;
                    }
                }
            }


            if (parentFixture == this.ParentFixtureView.FixtureModel)
            {
                //fixture did not change so just set the value.
                this.FixtureAssemblyModel.SetCoordinatesFromPlanogramRelative(
                   newCoordinates,
                   this.ParentFixtureView.FixtureItemModel);
            }
            else
            {
                //change the parent bay
                PlanogramFixtureAssembly newFixtureAssembly =
                this.FixtureAssemblyModel.Move<PlanogramFixtureAssemblyList>(this.ParentFixtureView.FixtureModel.Assemblies, parentFixture.Assemblies);

                //set the coordinates against the new bay
                newFixtureAssembly.SetCoordinatesFromPlanogramRelative(newCoordinates, parentFixtureItem);

                //[V8-28618] swap out the model in this view. This is because the item may be open in an edit window 
                // if the user then carries on adjusting properties it fails because this model is deleted.
                // we dont need to fire off any property changes however as the model is exactly the same and there are no
                // child object properties.
                _fixtureAssembly = newFixtureAssembly;
            }
        }


        /// <summary>
        /// Sets the rotation for this item to the given world rotation.
        /// </summary>
        public void SetWorldRotation(Single newWorldAngle, Single newWorldSlope, Single newWorldRoll)
        {
            SetWorldRotation(new RotationValue(newWorldAngle, newWorldSlope, newWorldRoll));
        }
        /// <summary>
        /// Sets the rotation for this item to the given world rotation.
        /// </summary>
        public void SetWorldRotation(RotationValue newRotation)
        {

            this.FixtureAssemblyModel.SetRotationFromPlanogramRelative(
                   newRotation,
                   this.ParentFixtureView.FixtureItemModel);
        }

        /// <summary>
        /// Returns the bounding box for this component
        /// relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public RectValue GetWorldSpaceBounds()
        {
            return this.FixtureAssemblyModel.GetPlanogramRelativeBoundingBox(
                    this.AssemblyModel, this.ParentFixtureView.FixtureItemModel);

        }

        public PointValue ToLocal(PointValue worldPoint)
        {
            return PlanogramFixtureAssembly.ToLocal(
                worldPoint,
                 this.ParentFixtureView.FixtureItemModel,
                 this.FixtureAssemblyModel);

        }

        public PointValue ToWorld(PointValue localPoint)
        {
            return PlanogramFixtureAssembly.ToWorld(
               localPoint,
                this.ParentFixtureView.FixtureItemModel,
                this.FixtureAssemblyModel);
        }

        #endregion

        #region Field Infos

        /// <summary>
        /// Returns the value of the given field for this item.
        /// </summary>
        public virtual Object GetFieldValue(ObjectFieldInfo fieldInfo)
        {
            return fieldInfo.GetValue(this);
        }

        /// <summary>
        /// Returns the list of plan item fields for this plan item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields()
        {
            foreach (var field in PlanogramFixtureAssembly.EnumerateDisplayableFieldInfos(true))
            {
                yield return field;
            }

            foreach (var field in PlanogramAssembly.EnumerateDisplayableFieldInfos(true))
            {
                yield return field;
            }
        }

        #endregion

        #endregion

        #region IPlanAssemblyRenderable Members

        event NotifyCollectionChangedEventHandler IPlanAssemblyRenderable.ComponentsCollectionChanged
        {
            add { _components.CollectionChanged += value; }
            remove { _components.CollectionChanged -= value; }
        }

        IEnumerable<IPlanComponentRenderable> IPlanAssemblyRenderable.Components
        {
            get { return this.ComponentViews; }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void OnDisposing(Boolean disposing)
        {
            _components.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramComponentViewModelBase>;

            base.OnDisposing(disposing);

            _fixtureAssembly.PropertyChanging -= Model_PropertyChanging;
            _fixtureAssembly.PropertyChanged -= Model_PropertyChanged;
            _assembly.PropertyChanging -= Model_PropertyChanging;
            _assembly.PropertyChanged -= Model_PropertyChanged;
            _assembly.Components.BulkCollectionChanged -= PlanogramAssembly_ComponentsBulkCollectionChanged;

        }

        #endregion

    }
}
