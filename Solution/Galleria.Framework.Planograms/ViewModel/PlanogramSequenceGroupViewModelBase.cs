﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Model;
using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// The base class for view models representing <see cref="PlanogramSequenceGroup"/>s.
    /// </summary>
    public class PlanogramSequenceGroupViewModelBase : ViewModelBase
    {
        #region Fields

        /// <summary>
        /// The <see cref="PlanogramSequenceGroup"/> that this view model represents.
        /// </summary>
        private PlanogramSequenceGroup _model;

        #endregion

        #region Properties

        /// <summary>
        /// The <see cref="PlanogramSequenceGroup"/> that this view model represents.
        /// </summary>
        public PlanogramSequenceGroup Model
        {
            get { return _model; }
        }

        /// <summary>
        /// The colour of the <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        public Int32 Colour
        {
            get { return _model.Colour; }
            set { _model.Colour = value; }
        }

        /// <summary>
        /// The Name of the <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        public String Name
        {
            get { return _model.Name; }
            set { _model.Name = value; }
        }

        /// <summary>
        /// Gets the <see cref="PlanogramSequenceGroupProduct"/>s of the <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        public PlanogramSequenceGroupProductList Products
        {
            get { return _model.Products; }
        }

        #endregion

        #region Constructor
        public PlanogramSequenceGroupViewModelBase(PlanogramSequenceGroup sequenceGroup)
        {
            _model = sequenceGroup;
            _model.PropertyChanged += Model_PropertyChanged;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called whenever a property changes on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(sender, e.PropertyName);
        }

        protected virtual void OnModelPropertyChanged(Object sender, String propertyName)
        {
            if (typeof(PlanogramSequenceGroupViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }
        }

        #endregion

        #region Methods

        protected override string GetError(string columnName)
        {
            return String.Empty;
        }

        #endregion

        #region IDisposable Members

        protected override void OnDisposing(Boolean disposing)
        {
            base.OnDisposing(disposing);

            _model.PropertyChanged -= Model_PropertyChanged;
        }

        #endregion
    }
}
