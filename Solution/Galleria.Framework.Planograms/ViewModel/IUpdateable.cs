﻿// Copyright © Galleria RTS Ltd 2016
using System;


namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Interface defining members of an object which
    /// indicates when multiple values are changing.
    /// This is to improve rendering/update performance.
    /// </summary>
    public interface IUpdateable
    {
        Boolean IsUpdating { get; }

        void BeginUpdate();
        void EndUpdate();
    }
}
