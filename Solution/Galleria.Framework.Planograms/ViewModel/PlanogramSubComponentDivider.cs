﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Rendering;
using System;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Implementation of ISubComponentDividerRenderable
    /// TODO: Find a better way of doing this.
    /// </summary>
    public sealed class PlanogramSubComponentDivider : ISubComponentDividerRenderable
    {
        public Single X { get; private set; }

        public Single Y { get; private set; }

        public Single Z { get; private set; }

        public Single Height { get; private set; }

        public Single Width { get; private set; }

        public Single Depth { get; private set; }


        public PlanogramSubComponentDivider(PlanogramDividerPlacement dividerPlacement, Single xOffset)
        {
            this.X = dividerPlacement.X + xOffset;
            this.Y = dividerPlacement.Y;
            this.Z = dividerPlacement.Z;
            this.Width = dividerPlacement.Width;
            this.Height = dividerPlacement.Height;
            this.Depth = dividerPlacement.Depth;
        }
    }
}
