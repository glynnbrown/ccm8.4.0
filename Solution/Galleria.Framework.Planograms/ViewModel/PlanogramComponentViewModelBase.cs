﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Viewmodel base for a viewmodel used to render a PlanogramComponent model object.
    /// </summary>
    public abstract class PlanogramComponentViewModelBase : ViewModelBase, IPlanComponentRenderable
    {
        #region Fields

        private Boolean _isInitialized;
        private Boolean _isInvalid;

        private PlanogramFixtureViewModelBase _parentFixtureView;
        private PlanogramAssemblyViewModelBase _parentAssemblyView;

        private PlanogramFixtureComponent _fixtureComponent;
        private readonly PlanogramAssemblyComponent _assemblyComponent;
        private readonly PlanogramComponent _component;


        private readonly ObservableCollection<PlanogramSubComponentViewModelBase> _subComponents = new ObservableCollection<PlanogramSubComponentViewModelBase>();
        private readonly ObservableCollection<PlanogramAnnotationViewModelBase> _annotations = new ObservableCollection<PlanogramAnnotationViewModelBase>();

        private PointValue _worldCoordinates = new PointValue();
        private RotationValue _worldRotation = new RotationValue();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the source PlanogramFixtureComponent model object
        /// </summary>
        public PlanogramFixtureComponent FixtureComponentModel
        {
            get { return _fixtureComponent; }
        }

        /// <summary>
        /// Gets the source PlanogramAssemblyComponent model object
        /// </summary>
        public PlanogramAssemblyComponent AssemblyComponentModel
        {
            get { return _assemblyComponent; }
        }

        /// <summary>
        /// Gets the source PlanogramComponent model object
        /// </summary>
        public PlanogramComponent ComponentModel
        {
            get { return _component; }
        }

        /// <summary>
        /// Returns the custom attribute data model for this product.
        /// </summary>
        public CustomAttributeData CustomAttributes
        {
            get { return ComponentModel.CustomAttributes; }
        }

        #region Model Properties

        public String Name
        {
            get { return _component.Name; }
            set { _component.Name = value; }
        }

        public Single X
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.X;
                }
                else
                {
                    return _fixtureComponent.X;
                }
            }
            set
            {
                if (_assemblyComponent != null)
                {
                    _assemblyComponent.X = value;
                }
                else
                {
                    _fixtureComponent.X = value;
                }
            }
        }

        public Single Y
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.Y;
                }
                else
                {
                    return _fixtureComponent.Y;
                }
            }
            set
            {
                if (_assemblyComponent != null)
                {
                    _assemblyComponent.Y = value;
                }
                else
                {
                    _fixtureComponent.Y = value;
                }
            }
        }

        public Single Z
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.Z;
                }
                else
                {
                    return _fixtureComponent.Z;
                }

            }
            set
            {
                if (_assemblyComponent != null)
                {
                    _assemblyComponent.Z = value;
                }
                else
                {
                    _fixtureComponent.Z = value;
                }
            }
        }

        public Single Slope
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.Slope;
                }
                else
                {
                    return _fixtureComponent.Slope;
                }
            }
            set
            {
                if (_assemblyComponent != null)
                {
                    _assemblyComponent.Slope = value;
                }
                else
                {
                    _fixtureComponent.Slope = value;
                }
            }
        }

        public Single Angle
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.Angle;
                }
                else
                {
                    return _fixtureComponent.Angle;
                }
            }
            set
            {
                if (_assemblyComponent != null)
                {
                    _assemblyComponent.Angle = value;
                }
                else
                {
                    _fixtureComponent.Angle = value;
                }
            }
        }

        public Single Roll
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.Roll;
                }
                else
                {
                    return _fixtureComponent.Roll;
                }
            }
            set
            {
                if (_assemblyComponent != null)
                {
                    _assemblyComponent.Roll = value;
                }
                else
                {
                    _fixtureComponent.Roll = value;
                }
            }
        }

        public PlanogramComponentType ComponentType
        {
            get { return _component.ComponentType; }
            protected set { _component.ComponentType = value; } //should not be settable in the ui.
        }

        public virtual Single Height
        {
            get { return _component.Height; }
            set { _component.Height = value; }
        }

        public virtual Single Width
        {
            get { return _component.Width; }
            set { _component.Width = value; }
        }

        public virtual Single Depth
        {
            get { return _component.Depth; }
            set { _component.Depth = value; }
        }

        public Boolean IsMoveable
        {
            get { return _component.IsMoveable; }
            set { _component.IsMoveable = value; }
        }

        public Boolean IsDisplayOnly
        {
            get { return _component.IsDisplayOnly; }
            set { _component.IsDisplayOnly = value; }
        }

        public Boolean CanAttachShelfEdgeLabel
        {
            get { return _component.CanAttachShelfEdgeLabel; }
            set { _component.CanAttachShelfEdgeLabel = value; }
        }

        public String RetailerReferenceCode
        {
            get { return _component.RetailerReferenceCode; }
            set { _component.RetailerReferenceCode = value; }
        }

        public String BarCode
        {
            get { return _component.BarCode; }
            set { _component.BarCode = value; }
        }

        public String Manufacturer
        {
            get { return _component.Manufacturer; }
            set { _component.Manufacturer = value; }
        }

        public String ManufacturerPartName
        {
            get { return _component.ManufacturerPartName; }
            set { _component.ManufacturerPartName = value; }
        }

        public String ManufacturerPartNumber
        {
            get { return _component.ManufacturerPartNumber; }
            set { _component.ManufacturerPartNumber = value; }
        }

        public String SupplierName
        {
            get { return _component.SupplierName; }
            set { _component.SupplierName = value; }
        }

        public String SupplierPartNumber
        {
            get { return _component.SupplierPartNumber; }
            set { _component.SupplierPartNumber = value; }
        }

        public Single? SupplierCostPrice
        {
            get { return _component.SupplierCostPrice; }
            set { _component.SupplierCostPrice = value; }
        }

        public Single? SupplierDiscount
        {
            get { return _component.SupplierDiscount; }
            set { _component.SupplierDiscount = value; }
        }

        public Single? SupplierLeadTime
        {
            get { return _component.SupplierLeadTime; }
            set { _component.SupplierLeadTime = value; }
        }

        public Int32? MinPurchaseQty
        {
            get { return _component.MinPurchaseQty; }
            set { _component.MinPurchaseQty = value; }
        }

        public Single? WeightLimit
        {
            get { return _component.WeightLimit; }
            set { _component.WeightLimit = value; }
        }

        public Single? Weight
        {
            get { return _component.Weight; }
            set { _component.Weight = value; }
        }

        public Single? Volume
        {
            get { return _component.Volume; }
            set { _component.Volume = value; }
        }

        public Single? Diameter
        {
            get { return _component.Diameter; }
            set { _component.Diameter = value; }
        }

        public Int16? Capacity
        {
            get { return _component.Capacity; }
            set { _component.Capacity = value; }
        }

        public Boolean IsMerchandisedTopDown
        {
            get { return _component.IsMerchandisedTopDown; }
            set { _component.IsMerchandisedTopDown = value; }
        }

        public Int16? ComponentSequenceNumber
        {
            get
            {
                if (this.FixtureComponentModel != null) return this.FixtureComponentModel.ComponentSequenceNumber;
                else if (this.AssemblyComponentModel != null) return this.AssemblyComponentModel.ComponentSequenceNumber;
                else return null;
            }
            set
            {
                if (this.FixtureComponentModel != null)
                {
                    this.FixtureComponentModel.ComponentSequenceNumber = value;
                }
                else if (this.AssemblyComponentModel != null)
                {
                    this.AssemblyComponentModel.ComponentSequenceNumber = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the notch number this component should
        /// snap to.
        /// </summary>
        public Int32? NotchNumber
        {
            get
            {
                if (this.FixtureComponentModel != null) return this.FixtureComponentModel.NotchNumber;
                else if (this.AssemblyComponentModel != null) return this.AssemblyComponentModel.NotchNumber;
                else return null;
            }
            set
            {
                if (value.HasValue)
                {
                    if (this.FixtureComponentModel != null)
                    {
                        this.FixtureComponentModel.SetNotchNumber(value.Value,
                            this.ComponentModel, this.ParentFixtureView.FixtureItemModel);
                    }
                    else if (this.AssemblyComponentModel != null)
                    {
                        this.AssemblyComponentModel.SetNotchNumber(value.Value,
                            this.ComponentModel, this.ParentAssemblyView.FixtureAssemblyModel, this.ParentFixtureView.FixtureItemModel);
                    }
                }
            }
        }

        #endregion

        #region Meta Data Properties

        public Int16? MetaNumberOfSubComponents
        {
            get { return _component.MetaNumberOfSubComponents; }
        }

        public Int16? MetaNumberOfMerchandisedSubComponents
        {
            get { return _component.MetaNumberOfMerchandisedSubComponents; }
        }

        public Single? MetaTotalMerchandisableLinearSpace
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalMerchandisableLinearSpace;
                }
                else
                {
                    return _fixtureComponent.MetaTotalMerchandisableLinearSpace;
                }
            }
        }

        public Single? MetaTotalMerchandisableAreaSpace
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalMerchandisableAreaSpace;
                }
                else
                {
                    return _fixtureComponent.MetaTotalMerchandisableAreaSpace;
                }
            }
        }

        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalMerchandisableVolumetricSpace;
                }
                else
                {
                    return _fixtureComponent.MetaTotalMerchandisableVolumetricSpace;
                }
            }
        }

        public Single? MetaTotalLinearWhiteSpace
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalLinearWhiteSpace;
                }
                else
                {
                    return _fixtureComponent.MetaTotalLinearWhiteSpace;
                }
            }
        }

        public Single? MetaTotalAreaWhiteSpace
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalAreaWhiteSpace;
                }
                else
                {
                    return _fixtureComponent.MetaTotalAreaWhiteSpace;
                }
            }
        }

        public Single? MetaTotalVolumetricWhiteSpace
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalVolumetricWhiteSpace;
                }
                else
                {
                    return _fixtureComponent.MetaTotalVolumetricWhiteSpace;
                }
            }
        }

        public Int32? MetaProductsPlaced
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaProductsPlaced;
                }
                else
                {
                    return _fixtureComponent.MetaProductsPlaced;
                }
            }
        }

        public Int32? MetaNewProducts
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaNewProducts;
                }
                else
                {
                    return _fixtureComponent.MetaNewProducts;
                }
            }
        }

        public Int32? MetaChangesFromPreviousCount
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaChangesFromPreviousCount;
                }
                else
                {
                    return _fixtureComponent.MetaChangesFromPreviousCount;
                }
            }
        }

        public Int32? MetaChangeFromPreviousStarRating
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaChangeFromPreviousStarRating;
                }
                else
                {
                    return _fixtureComponent.MetaChangeFromPreviousStarRating;
                }
            }
        }

        public Int32? MetaBlocksDropped
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaBlocksDropped;
                }
                else
                {
                    return _fixtureComponent.MetaBlocksDropped;
                }
            }
        }

        public Int32? MetaNotAchievedInventory
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaNotAchievedInventory;
                }
                else
                {
                    return _fixtureComponent.MetaNotAchievedInventory;
                }
            }
        }

        public Int32? MetaTotalFacings
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalFacings;
                }
                else
                {
                    return _fixtureComponent.MetaTotalFacings;
                }
            }
        }

        public Int16? MetaAverageFacings
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaAverageFacings;
                }
                else
                {
                    return _fixtureComponent.MetaAverageFacings;
                }
            }
        }

        public Int32? MetaTotalUnits
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalUnits;
                }
                else
                {
                    return _fixtureComponent.MetaTotalUnits;
                }
            }
        }

        public Int32? MetaAverageUnits
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaAverageUnits;
                }
                else
                {
                    return _fixtureComponent.MetaAverageUnits;
                }
            }
        }

        public Single? MetaMinDos
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaMinDos;
                }
                else
                {
                    return _fixtureComponent.MetaMinDos;
                }
            }
        }

        public Single? MetaMaxDos
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaMaxDos;
                }
                else
                {
                    return _fixtureComponent.MetaMaxDos;
                }
            }
        }

        public Single? MetaAverageDos
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaAverageDos;
                }
                else
                {
                    return _fixtureComponent.MetaAverageDos;
                }
            }
        }

        public Single? MetaMinCases
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaMinCases;
                }
                else
                {
                    return _fixtureComponent.MetaMinCases;
                }
            }
        }

        public Single? MetaAverageCases
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaAverageCases;
                }
                else
                {
                    return _fixtureComponent.MetaAverageCases;
                }
            }
        }

        public Single? MetaMaxCases
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaMaxCases;
                }
                else
                {
                    return _fixtureComponent.MetaMaxCases;
                }
            }
        }

        public Int16? MetaSpaceToUnitsIndex
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaSpaceToUnitsIndex;
                }
                else
                {
                    return _fixtureComponent.MetaSpaceToUnitsIndex;
                }
            }
        }

        public Boolean? MetaIsComponentSlopeWithNoRiser
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaIsComponentSlopeWithNoRiser;
                }
                else
                {
                    return _fixtureComponent.MetaIsComponentSlopeWithNoRiser;
                }
            }
        }

        public Boolean? MetaIsOverMerchandisedDepth
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaIsOverMerchandisedDepth;
                }
                else
                {
                    return _fixtureComponent.MetaIsOverMerchandisedDepth;
                }
            }
        }

        public Boolean? MetaIsOverMerchandisedHeight
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaIsOverMerchandisedHeight;
                }
                else
                {
                    return _fixtureComponent.MetaIsOverMerchandisedHeight;
                }
            }
        }

        public Boolean? MetaIsOverMerchandisedWidth
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaIsOverMerchandisedWidth;
                }
                else
                {
                    return _fixtureComponent.MetaIsOverMerchandisedWidth;
                }
            }
        }

        public Int32? MetaTotalComponentCollisions
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalComponentCollisions;
                }
                else
                {
                    return _fixtureComponent.MetaTotalComponentCollisions;
                }
            }
        }

        public Int32? MetaTotalPositionCollisions
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalPositionCollisions;
                }
                else
                {
                    return _fixtureComponent.MetaTotalPositionCollisions;
                }
            }
        }

        public Int16? MetaTotalFrontFacings
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaTotalFrontFacings;
                }
                else
                {
                    return _fixtureComponent.MetaTotalFrontFacings;
                }
            }
        }

        public Single? MetaAverageFrontFacings
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaAverageFrontFacings;
                }
                else
                {
                    return _fixtureComponent.MetaAverageFrontFacings;
                }
            }
        }

        public Single? MetaPercentageLinearSpaceFilled
        {
            get
            {
                if (_assemblyComponent != null)
                {
                    return _assemblyComponent.MetaPercentageLinearSpaceFilled;
                }
                else
                {
                    return _fixtureComponent.MetaPercentageLinearSpaceFilled;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the X coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldX
        {
            get { return _worldCoordinates.X; }
            set { SetWorldCoordinates(value, MetaWorldY, MetaWorldZ); }
        }

        /// <summary>
        /// Gets/Sets the Y coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldY
        {
            get { return _worldCoordinates.Y; }
            set { SetWorldCoordinates(MetaWorldX, value, MetaWorldZ); }
        }

        /// <summary>
        /// Gets/Sets the Z coordinate for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldZ
        {
            get { return _worldCoordinates.Z; }
            set { SetWorldCoordinates(MetaWorldX, MetaWorldY, value); }
        }

        /// <summary>
        /// Gets/Sets the rotation angle value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldAngle
        {
            get { return _worldRotation.Angle; }
            set { SetWorldRotation(value, MetaWorldSlope, MetaWorldRoll); }
        }

        /// <summary>
        /// Gets/Sets the rotation slope value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldSlope
        {
            get { return _worldRotation.Slope; }
            set { SetWorldRotation(MetaWorldAngle, value, MetaWorldRoll); }
        }

        /// <summary>
        /// Gets/Sets the rotation roll value for this item
        /// relative to the entire plan.
        /// </summary>
        public Single MetaWorldRoll
        {
            get { return _worldRotation.Roll; }
            set { SetWorldRotation(MetaWorldAngle, MetaWorldSlope, value); }
        }


        #endregion

        /// <summary>
        /// Returns the collection of views of child subcomponents
        /// </summary>
        protected internal ObservableCollection<PlanogramSubComponentViewModelBase> SubComponentViews
        {
            get { return _subComponents; }
        }

        /// <summary>
        /// Returns the collection of annotations held by this component.
        /// </summary>
        protected ObservableCollection<PlanogramAnnotationViewModelBase> AnnotationViews
        {
            get { return _annotations; }
        }

        /// <summary>
        /// Returns the parent fixture viewmodel.
        /// </summary>
        protected internal PlanogramFixtureViewModelBase ParentFixtureView
        {
            get
            {
                if (_parentAssemblyView != null)
                {
                    return _parentAssemblyView.ParentFixtureView;
                }
                return _parentFixtureView;
            }
        }

        /// <summary>
        /// Returns the parent assembly viewmodel.
        /// May be null if this component does not belong to an assembly.
        /// </summary>
        protected internal PlanogramAssemblyViewModelBase ParentAssemblyView
        {
            get { return _parentAssemblyView; }
        }

        internal PlanogramViewModelBase ParentPlanogramView
        {
            get { return ParentFixtureView.ParentPlanogramView; }
        }

        /// <summary>
        /// Returns true if this viewmodel is invalid.
        /// </summary>
        public Boolean IsInvalid
        {
            get
            {
                if (!_isInvalid)
                {
                    if ((this.FixtureComponentModel != null && this.FixtureComponentModel.IsDeleted)
                        || (this.AssemblyComponentModel != null && this.AssemblyComponentModel.IsDeleted))
                        _isInvalid = true;
                }


                return _isInvalid;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance for the given fixture component.
        /// </summary>
        /// <param name="fixtureComponent"></param>
        public PlanogramComponentViewModelBase(PlanogramFixtureViewModelBase parentFixtureView,
            PlanogramFixtureComponent fixtureComponent, PlanogramComponent component)
        {
            _parentFixtureView = parentFixtureView;

            _fixtureComponent = fixtureComponent;
            fixtureComponent.PropertyChanging += Model_PropertyChanging;
            fixtureComponent.PropertyChanged += Model_PropertyChanged;

            Debug.Assert(Object.Equals(component.Id, fixtureComponent.PlanogramComponentId), "Incorrect component");

            _component = component;
            _component.PropertyChanging += Model_PropertyChanging;
            _component.PropertyChanged += Model_PropertyChanged;
            _component.SubComponents.BulkCollectionChanged += PlanogramComponent_SubComponentsBulkCollectionChanged;

            UpdateWorldCoordinatesAndRotation();
        }

        /// <summary>
        /// Creates a new instance for the given assembly component.
        /// </summary>
        /// <param name="assemblyComponent"></param>
        public PlanogramComponentViewModelBase(PlanogramAssemblyViewModelBase parentAssemblyView,
            PlanogramAssemblyComponent assemblyComponent, PlanogramComponent component)
        {
            _parentAssemblyView = parentAssemblyView;

            _assemblyComponent = assemblyComponent;
            assemblyComponent.PropertyChanging += Model_PropertyChanging;
            assemblyComponent.PropertyChanged += Model_PropertyChanged;

            Debug.Assert(Object.Equals(component.Id, assemblyComponent.PlanogramComponentId), "Incorrect component");

            _component = component;
            _component.PropertyChanging += Model_PropertyChanging;
            _component.PropertyChanged += Model_PropertyChanged;
            _component.SubComponents.BulkCollectionChanged += PlanogramComponent_SubComponentsBulkCollectionChanged;

            UpdateWorldCoordinatesAndRotation();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(sender, e.PropertyName);
        }
        protected virtual void OnModelPropertyChanging(Object sender, String propertyName)
        {
            if (typeof(PlanogramComponentViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(sender, e.PropertyName);
        }
        protected virtual void OnModelPropertyChanged(Object sender, String propertyName)
        {
            if (typeof(PlanogramComponentViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }

            switch (propertyName)
            {
                case "X":
                case "Y":
                case "Z":
                case "Angle":
                case "Slope":
                case "Roll":
                    UpdateWorldCoordinatesAndRotation();
                    break;
            }
        }

        /// <summary>
        /// Called when the PlanogramComponent.SubComponents collection changes.
        /// </summary>
        private void PlanogramComponent_SubComponentsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            OnComponentModelSubComponentsBulkCollectionChanged(e);
        }

        /// <summary>
        /// Called when the PlanogramComponent.SubComponents collection changes.
        /// </summary>
        protected virtual void OnComponentModelSubComponentsBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramSubComponent subComponent in e.ChangedItems)
                    {
                        PlanogramSubComponentViewModelBase view = CreateSubComponentView(subComponent);
                        if (view != null)
                        {
                            _subComponents.Add(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramSubComponent subComponent in e.ChangedItems)
                    {
                        PlanogramSubComponentViewModelBase view =
                            _subComponents.FirstOrDefault(a => a.Model == subComponent);
                        if (view != null)
                        {
                            _subComponents.Remove(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_subComponents.Count > 0) _subComponents.Clear();

                        foreach (PlanogramSubComponent subComponent in this.ComponentModel.SubComponents)
                        {
                            PlanogramSubComponentViewModelBase view = CreateSubComponentView(subComponent);
                            if (view != null)
                            {
                                _subComponents.Add(view);
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the parent changes.
        /// </summary>
        protected override void OnParentChanged(object oldValue, object newValue)
        {
            base.OnParentChanged(oldValue, newValue);

            INotifyPropertyChanged oldParent = oldValue as INotifyPropertyChanged;
            if (oldParent != null)
            {
                oldParent.PropertyChanged -= Parent_PropertyChanged;
            }


            INotifyPropertyChanged newParent = newValue as INotifyPropertyChanged;
            if (newParent != null)
            {
                newParent.PropertyChanged += Parent_PropertyChanged;
            }
        }

        /// <summary>
        /// Called whenever a property on the parent changes.
        /// </summary>
        private void Parent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.IsDisposed) return;

            switch (e.PropertyName)
            {
                case "WorldX":
                case "WorldY":
                case "WorldZ":
                case "WorldAngle":
                case "WorldSlope":
                case "WorldRoll":
                case "MetaWorldX":
                case "MetaWorldY":
                case "MetaWorldZ":
                case "MetaWorldAngle":
                case "MetaWorldSlope":
                case "MetaWorldRoll":
                    UpdateWorldCoordinatesAndRotation();
                    break;

                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    if (sender is PlanogramFixtureViewModelBase)
                    {
                        UpdateWorldCoordinatesAndRotation();
                    }
                    break;

            }
        }

        /// <summary>
        /// Called whenever the annotations collection changes on the parent planogram
        /// </summary>
        private void Planogram_AnnotationsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (PlanogramAnnotation anno in e.ChangedItems)
                        {
                            if (IsAnnotationAssociated(anno))
                            {
                                PlanogramAnnotationViewModelBase view = CreateAnnotationView(anno);
                                if (view != null)
                                {
                                    _annotations.Add(view);
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (PlanogramAnnotation pos in e.ChangedItems)
                        {
                            PlanogramAnnotationViewModelBase view = _annotations.FirstOrDefault(p => p.Model == pos);
                            if (view != null)
                            {
                                _annotations.Remove(view);
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_annotations.Count > 0) _annotations.Clear();

                        //Add all related positions
                        foreach (PlanogramAnnotation anno in ((Planogram)sender).Annotations)
                        {
                            if (IsAnnotationAssociated(anno))
                            {
                                PlanogramAnnotationViewModelBase view = CreateAnnotationView(anno);
                                if (view != null)
                                {
                                    _annotations.Add(view);
                                }
                            }
                        }

                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes child view collections
        /// </summary>
        protected void InitializeChildViews()
        {
            if (_isInitialized) return;

            //subscribe child collections
            AddChildEventHooks(_subComponents);
            _subComponents.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramSubComponentViewModelBase>;
            AddChildEventHooks(_annotations);
            _annotations.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramAnnotationViewModelBase>;

            //add related subcomponents.
            OnComponentModelSubComponentsBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));

            //Add related annotations
            Planogram parentPlanogram = this.ComponentModel.Parent;
            Planogram_AnnotationsBulkCollectionChanged(parentPlanogram, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
            parentPlanogram.Annotations.BulkCollectionChanged += Planogram_AnnotationsBulkCollectionChanged;

            _isInitialized = true;

        }

        /// <summary>
        /// Creates a new view of the given subcomponent model.
        /// </summary>
        /// <param name="subComponent"></param>
        /// <returns></returns>
        protected abstract PlanogramSubComponentViewModelBase CreateSubComponentView(PlanogramSubComponent subComponent);

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Returns the IDataErrorInfo error for the given columnname.
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            String error = ((IDataErrorInfo)ComponentModel)[columnName];

            if (String.IsNullOrEmpty(error) && AssemblyComponentModel != null)
            {
                error = ((IDataErrorInfo)AssemblyComponentModel)[columnName];
            }

            if (String.IsNullOrEmpty(error) && FixtureComponentModel != null)
            {
                error = ((IDataErrorInfo)FixtureComponentModel)[columnName];
            }

            return error;
        }

        /// <summary>
        /// Returns the label text to display from the given format string.
        /// </summary>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public virtual String GetLabelText(String formatString)
        {
            //don't display a label if this is a backboard or base.
            if (this.ComponentType == PlanogramComponentType.Backboard
                || this.ComponentType == PlanogramComponentType.Base)
            {
                return null;
            }

            if (this.FixtureComponentModel != null)
            {
                return PlanogramFieldHelper.ResolveLabel(
                    formatString,
                    PlanogramFieldHelper.EnumerateAllFields(this.ParentPlanogramView.Model),
                    PlanogramItemPlacement.NewPlanogramItemPlacement(
                    component: this.ComponentModel,
                    fixtureComponent: this.FixtureComponentModel,
                    fixture: this.ParentFixtureView.FixtureModel,
                    fixtureItem: this.ParentFixtureView.FixtureItemModel,
                    planogram: this.ParentPlanogramView.Model));

            }
            else if (this.AssemblyComponentModel != null)
            {
                return PlanogramFieldHelper.ResolveLabel(
                    formatString,
                    PlanogramFieldHelper.EnumerateAllFields(this.ParentPlanogramView.Model),
                    PlanogramItemPlacement.NewPlanogramItemPlacement(
                    component: this.ComponentModel,
                    assemblyComponent: this.AssemblyComponentModel,
                    assembly: this.ParentAssemblyView.AssemblyModel,
                    fixtureAssembly: this.ParentAssemblyView.FixtureAssemblyModel,
                    fixture: this.ParentFixtureView.FixtureModel,
                    fixtureItem: this.ParentFixtureView.FixtureItemModel,
                    planogram: this.ParentPlanogramView.Model));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the value of the given field for this item.
        /// </summary>
        public virtual Object GetFieldValue(ObjectFieldInfo fieldInfo)
        {
            return fieldInfo.GetValue(this);
        }

        #region World Space Methods

        /// <summary>
        /// Updates the values of the world coordinates
        /// </summary>
        private void UpdateWorldCoordinatesAndRotation()
        {
            PointValue oldCoords = _worldCoordinates;
            RotationValue oldRotation = _worldRotation;


            //get the world coordinates and rotation
            if (this.FixtureComponentModel != null)
            {
                _worldCoordinates =
                    this.FixtureComponentModel.GetPlanogramRelativeCoordinates(
                        this.ParentFixtureView.FixtureItemModel);

                _worldRotation =
                    this.FixtureComponentModel.GetPlanogramRelativeRotation(
                        this.ParentFixtureView.FixtureItemModel);

                //trigger a notch number update:
                Int32? notchNumber = this.FixtureComponentModel.GetNotchNumber(this.ComponentModel, this.ParentFixtureView.FixtureItemModel);
            }
            else
            {
                _worldCoordinates =
                    this.AssemblyComponentModel.GetPlanogramRelativeCoordinates(
                        this.ParentFixtureView.FixtureItemModel, this.ParentAssemblyView.FixtureAssemblyModel);

                _worldRotation =
                    this.AssemblyComponentModel.GetPlanogramRelativeRotation(
                        this.ParentFixtureView.FixtureItemModel, this.ParentAssemblyView.FixtureAssemblyModel);

                //trigger a notch number update:
                Int32? notchNumber = this.AssemblyComponentModel.GetNotchNumber(
                        this.ComponentModel, this.ParentAssemblyView.FixtureAssemblyModel, this.ParentFixtureView.FixtureItemModel);
            }

            //fire off property changed events
            if (oldCoords.X != _worldCoordinates.X) OnPropertyChanged(PlanogramFixtureComponent.MetaWorldXProperty.Name);
            if (oldCoords.Y != _worldCoordinates.Y) OnPropertyChanged(PlanogramFixtureComponent.MetaWorldYProperty.Name);
            if (oldCoords.Z != _worldCoordinates.Z) OnPropertyChanged(PlanogramFixtureComponent.MetaWorldZProperty.Name);
            if (oldRotation.Angle != _worldRotation.Angle) OnPropertyChanged(PlanogramFixtureComponent.MetaWorldAngleProperty.Name);
            if (oldRotation.Slope != _worldRotation.Slope) OnPropertyChanged(PlanogramFixtureComponent.MetaWorldSlopeProperty.Name);
            if (oldRotation.Roll != _worldRotation.Roll) OnPropertyChanged(PlanogramFixtureComponent.MetaWorldRollProperty.Name);
            OnPropertyChanged(PlanogramFixtureComponent.NotchNumberProperty.Name);
        }


        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(Single newWorldX, Single newWorldY, Single newWorldZ)
        {
            SetWorldCoordinates(new PointValue(newWorldX, newWorldY, newWorldZ));
        }
        /// <summary>
        /// Sets the coordinates for this item to the given world position.
        /// </summary>
        public void SetWorldCoordinates(PointValue newCoordinates)
        {
            if (FixtureComponentModel != null)
            {
                RectValue thisBounds = new RectValue(
                    newCoordinates.X, newCoordinates.Y, newCoordinates.Z,
                    this.Width, this.Height, this.Depth);

                PlanogramFixtureItem parentFixtureItem = this.ParentFixtureView.FixtureItemModel;
                PlanogramFixture parentFixture = this.ParentFixtureView.FixtureModel;


                //check whether the new coords are still within the bounds of the parent bay.
                RectValue parentBayBounds = new RectValue(
                    parentFixtureItem.X, parentFixtureItem.Y, parentFixtureItem.Z,
                    parentFixture.Width, parentFixture.Height, parentFixture.Depth);

                if (!thisBounds.CollidesWith(parentBayBounds))
                {
                    Planogram parentPlan = this.ComponentModel.Parent;

                    foreach (PlanogramFixtureItem fixtureItem in
                        parentPlan.FixtureItems.OrderBy(f => f.BaySequenceNumber))
                    {
                        PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();

                        RectValue bayBounds = new RectValue(fixtureItem.X, fixtureItem.Y, fixtureItem.Z,
                        fixture.Width, fixture.Height, fixture.Depth);

                        if (thisBounds.CollidesWith(bayBounds))
                        {
                            parentFixtureItem = fixtureItem;
                            parentFixture = fixture;
                        }
                    }
                }


                if (parentFixture == this.ParentFixtureView.FixtureModel)
                {
                    //fixture did not change so just set the value.
                    FixtureComponentModel.SetCoordinatesFromPlanogramRelative(
                    newCoordinates,
                    this.ParentFixtureView.FixtureItemModel);
                }
                else
                {
                    //As we are going to be moving things around,
                    //start a new update on the parent plan view
                    PlanogramViewModelBase parentPlanView = this.ParentPlanogramView;
                    Boolean updateStarted = !parentPlanView.IsUpdating;
                    if (updateStarted) parentPlanView.BeginUpdate();

                    //get positions to move
                    var positionsToMove =
                        this.FixtureComponentModel.GetPlanogramSubComponentPlacements()
                        .SelectMany(s => s.GetPlanogramPositions()).ToList();

                    //change the parent bay
                    PlanogramFixtureComponent newFixtureComponent = this.FixtureComponentModel;
                    try
                    {
                        newFixtureComponent =
                        this.FixtureComponentModel.Move<PlanogramFixtureComponentList>(this.ParentFixtureView.FixtureModel.Components, parentFixture.Components);
                    }
                    catch (InvalidOperationException) { }

                    //set the coordinates against the new bay
                    newFixtureComponent.SetCoordinatesFromPlanogramRelative(newCoordinates, parentFixtureItem);

                    //[V8-28618] swap out the model in this view. This is because the item may be open in an edit window 
                    // if the user then carries on adjusting properties it fails because this model is deleted.
                    // we dont need to fire off any property changes however as the model is exactly the same and there are no
                    // child object properties.
                    _fixtureComponent = newFixtureComponent;
                    _isInvalid = true;

                    //move over the positions
                    foreach (PlanogramPosition pos in positionsToMove)
                    {
                        pos.PlanogramFixtureItemId = parentFixtureItem.Id;
                    }

                    //If we started an update, end it now.
                    if (updateStarted) parentPlanView.EndUpdate();
                }

            }
            else
            {
                AssemblyComponentModel.SetCoordinatesFromPlanogramRelative(
                    newCoordinates,
                    this.ParentFixtureView.FixtureItemModel,
                    this.ParentAssemblyView.FixtureAssemblyModel);
            }
        }


        /// <summary>
        /// Sets the rotation for this item to the given world rotation.
        /// </summary>
        public void SetWorldRotation(Single newWorldAngle, Single newWorldSlope, Single newWorldRoll)
        {
            SetWorldRotation(new RotationValue(newWorldAngle, newWorldSlope, newWorldRoll));
        }
        /// <summary>
        /// Sets the rotation for this item to the given world rotation.
        /// </summary>
        public void SetWorldRotation(RotationValue newRotation)
        {
            if (FixtureComponentModel != null)
            {
                FixtureComponentModel.SetRotationFromPlanogramRelative(
                    newRotation,
                    this.ParentFixtureView.FixtureItemModel);
            }
            else
            {
                AssemblyComponentModel.SetRotationFromPlanogramRelative(
                    newRotation,
                    this.ParentFixtureView.FixtureItemModel,
                    this.ParentAssemblyView.FixtureAssemblyModel);
            }
        }

        /// <summary>
        /// Returns the bounding box for this component
        /// relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public RectValue GetWorldSpaceBounds()
        {
            if (this.AssemblyComponentModel != null)
            {
                return this.AssemblyComponentModel.GetPlanogramRelativeBoundingBox(
                    this.ComponentModel, this.ParentFixtureView.FixtureItemModel, this.ParentAssemblyView.FixtureAssemblyModel);
            }
            else
            {
                return this.FixtureComponentModel.GetPlanogramRelativeBoundingBox(
                    this.ComponentModel, this.ParentFixtureView.FixtureItemModel);
            }
        }

        public PointValue ToLocal(PointValue worldPoint)
        {
            if (this.AssemblyComponentModel != null)
            {
                return PlanogramAssemblyComponent.ToLocal(
                    worldPoint,
                    this.ParentFixtureView.FixtureItemModel,
                    this.ParentAssemblyView.FixtureAssemblyModel,
                    this.AssemblyComponentModel);
            }
            else
            {
                return PlanogramFixtureComponent.ToLocal(
                    worldPoint,
                    this.ParentFixtureView.FixtureItemModel,
                    this.FixtureComponentModel);
            }
        }

        public PointValue ToWorld(PointValue localPoint)
        {
            if (this.AssemblyComponentModel != null)
            {
                return PlanogramAssemblyComponent.ToWorld(
                    localPoint,
                    this.ParentFixtureView.FixtureItemModel,
                    this.ParentAssemblyView.FixtureAssemblyModel,
                    this.AssemblyComponentModel);
            }
            else
            {
                return PlanogramFixtureComponent.ToWorld(
                    localPoint,
                    this.ParentFixtureView.FixtureItemModel,
                    this.FixtureComponentModel);
            }
        }


        #endregion

        /// <summary>
        /// Returns the list of plan item fields for this plan item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields()
        {
            //Component fields
            foreach (var field in PlanogramComponent.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                yield return field;
            }

            //PlanogramFixtureComponent fields.
            foreach (var field in PlanogramFixtureComponent.EnumerateDisplayableFieldInfos())
            {
                yield return field;
            }
        }

        #region Annotations

        /// <summary>
        /// Returns a view of the given annotation.
        /// </summary>
        protected abstract PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation annotation);

        /// <summary>
        /// Notifies this vm that an annotation in the object graph tree has changed its parent.
        /// </summary>
        internal void NotifyPlanogramAnnotationParentChanged(PlanogramAnnotation annotation)
        {
            Boolean isChild = IsAnnotationAssociated(annotation);
            var annotationView = this.AnnotationViews.FirstOrDefault(p => p.Model == annotation);

            //IF the annotation is not a child but it has a view
            //this remove it.
            if (!isChild && annotationView != null)
            {
                _annotations.Remove(annotationView);
            }

            // OR if the annotation should be a child but has no view
            // then add one.
            else if (isChild && annotationView == null)
            {
                PlanogramAnnotationViewModelBase view = CreateAnnotationView(annotation);
                if (view != null)
                {
                    _annotations.Add(view);
                }
            }
        }

        /// <summary>
        /// Returns true if the given annotation is related to this component.
        /// </summary>
        protected Boolean IsAnnotationAssociated(PlanogramAnnotation annotation)
        {
            PlanogramFixtureItem fixtureItem = this.ParentFixtureView.FixtureItemModel;

            if (this.FixtureComponentModel != null)
            {
                //this.FixtureComponentModel.IsAnnotationAssociated(annotation);

                // fixture item
                if ((fixtureItem == null) && (annotation.PlanogramFixtureItemId != null)) return false;
                if ((fixtureItem != null) && (annotation.PlanogramFixtureItemId == null)) return false;
                if ((fixtureItem != null) && (!fixtureItem.Id.Equals(annotation.PlanogramFixtureItemId))) return false;

                // fixture assembly
                if (annotation.PlanogramFixtureAssemblyId != null) return false;

                // assembly component
                if (annotation.PlanogramAssemblyComponentId != null) return false;

                // fixture component
                if ((this.FixtureComponentModel == null) && (annotation.PlanogramFixtureComponentId != null)) return false;
                if ((this.FixtureComponentModel != null) && (annotation.PlanogramFixtureComponentId == null)) return false;
                if ((this.FixtureComponentModel != null) && (!this.FixtureComponentModel.Id.Equals(annotation.PlanogramFixtureComponentId)))
                    return false;

                // sub component
                if (annotation.PlanogramSubComponentId != null) return false;

                //position
                if (annotation.PlanogramPositionId != null) return false;

                // if we got this far, return true
                return true;


            }
            else
            {
                PlanogramFixtureAssembly fixtureAssembly = this.ParentAssemblyView.FixtureAssemblyModel;

                //this.AssemblyComponentModel.IsAnnotationAssociated(annotation);

                // fixture item
                if ((fixtureItem == null) && (annotation.PlanogramFixtureItemId != null)) return false;
                if ((fixtureItem != null) && (annotation.PlanogramFixtureItemId == null)) return false;
                if ((fixtureItem != null) && (!fixtureItem.Id.Equals(annotation.PlanogramFixtureItemId))) return false;

                // fixture assembly
                if ((fixtureAssembly == null) && (annotation.PlanogramFixtureAssemblyId != null)) return false;
                if ((fixtureAssembly != null) && (annotation.PlanogramFixtureAssemblyId == null)) return false;
                if ((fixtureAssembly != null) && (!fixtureAssembly.Id.Equals(annotation.PlanogramFixtureAssemblyId)))
                    return false;

                // assembly component
                if ((this.AssemblyComponentModel == null) && (annotation.PlanogramAssemblyComponentId != null)) return false;
                if ((this.AssemblyComponentModel != null) && (annotation.PlanogramAssemblyComponentId == null)) return false;
                if ((this.AssemblyComponentModel != null) && (!this.AssemblyComponentModel.Id.Equals(annotation.PlanogramAssemblyComponentId)))
                    return false;

                // fixture component
                if (annotation.PlanogramFixtureComponentId != null) return false;

                // sub component
                if (annotation.PlanogramSubComponentId != null) return false;

                //position
                if (annotation.PlanogramPositionId != null) return false;

                // if we got this far, return true
                return true;
            }
        }

        /// <summary>
        /// Associates the give annotation with this component.
        /// </summary>
        protected void AssociateAnnotation(PlanogramAnnotation annotation)
        {
            //link to this
            if (this.FixtureComponentModel != null)
            {
                PlanogramFixtureItem fixtureItem = this.ParentFixtureView.FixtureItemModel;
                PlanogramFixtureComponent fixtureComponent = this.FixtureComponentModel;

                //this.FixtureComponentModel.AssociateAnnotation(annotation);
                annotation.PlanogramPositionId = null;
                annotation.PlanogramSubComponentId = null;
                annotation.PlanogramFixtureComponentId = fixtureComponent.Id;
                annotation.PlanogramAssemblyComponentId = null;
                annotation.PlanogramFixtureAssemblyId = null;
                annotation.PlanogramFixtureItemId = fixtureItem.Id;
            }
            else
            {
                PlanogramFixtureItem fixtureItem = this.ParentFixtureView.FixtureItemModel;
                PlanogramFixtureAssembly fixtureAssembly = this.ParentAssemblyView.FixtureAssemblyModel;
                PlanogramAssemblyComponent assemblyComponent = this.AssemblyComponentModel;

                //this.AssemblyComponentModel.AssociateAnnotation(annotation);
                annotation.PlanogramPositionId = null;
                annotation.PlanogramSubComponentId = null;
                annotation.PlanogramFixtureComponentId = null;
                annotation.PlanogramAssemblyComponentId = assemblyComponent.Id;
                annotation.PlanogramFixtureAssemblyId = fixtureAssembly.Id;
                annotation.PlanogramFixtureItemId = fixtureItem.Id;
            }
        }

        #endregion

        public IEnumerable<PlanogramSubComponentViewModelBase> EnumerateSubComponentViews()
        {
            foreach (var subView in this.SubComponentViews) yield return subView;
        }

        #endregion

        #region IPlanComponentRenderable Members

        event NotifyCollectionChangedEventHandler IPlanComponentRenderable.SubComponentsCollectionChanged
        {
            add { _subComponents.CollectionChanged += value; }
            remove { _subComponents.CollectionChanged -= value; }
        }

        IEnumerable<IPlanSubComponentRenderable> IPlanComponentRenderable.SubComponents
        {
            get { return this.SubComponentViews; }
        }

        event NotifyCollectionChangedEventHandler IPlanComponentRenderable.AnnotationsCollectionChanged
        {
            add { _annotations.CollectionChanged += value; }
            remove { _annotations.CollectionChanged -= value; }
        }

        IEnumerable<IPlanAnnotationRenderable> IPlanComponentRenderable.Annotations
        {
            get { return _annotations; }
        }

        Object IPlanComponentRenderable.Id
        {
            get
            {
                if (this.ParentFixtureView == null) return null;

                if (this.FixtureComponentModel != null)
                {
                    return new PlanogramComponentPlacementId(
                        this.FixtureComponentModel,
                        this.ParentFixtureView.FixtureItemModel,
                        this.ParentPlanogramView.Model);
                }
                else
                {
                    if (this.ParentAssemblyView == null) return null;

                    return new PlanogramComponentPlacementId(
                        this.AssemblyComponentModel,
                        this.ParentAssemblyView.FixtureAssemblyModel,
                        this.ParentFixtureView.FixtureItemModel,
                        this.ParentPlanogramView.Model);
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void OnDisposing(Boolean disposing)
        {
            _subComponents.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramSubComponentViewModelBase>;
            _annotations.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramAnnotationViewModelBase>;

            Planogram parentPlan = this.ComponentModel.Parent;
            if (parentPlan != null)
            {
                parentPlan.Annotations.BulkCollectionChanged -= Planogram_AnnotationsBulkCollectionChanged;
            }


            base.OnDisposing(disposing);

            _component.PropertyChanging -= Model_PropertyChanging;
            _component.PropertyChanged -= Model_PropertyChanged;
            _component.SubComponents.BulkCollectionChanged -= PlanogramComponent_SubComponentsBulkCollectionChanged;

            if (_assemblyComponent != null)
            {
                _assemblyComponent.PropertyChanging -= Model_PropertyChanging;
                _assemblyComponent.PropertyChanged -= Model_PropertyChanged;
            }

            if (_fixtureComponent != null)
            {
                _fixtureComponent.PropertyChanging -= Model_PropertyChanging;
                _fixtureComponent.PropertyChanged -= Model_PropertyChanged;
            }
        }

        #endregion

    }
}
