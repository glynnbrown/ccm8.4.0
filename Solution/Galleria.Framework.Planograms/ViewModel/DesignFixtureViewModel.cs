﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Simple implemenation of a fixture for design view.
    /// </summary>
    public class DesignFixtureViewModel : IPlanFixtureRenderable, IDisposable
    {
        #region Fields

        private PlanogramFixtureViewModelBase _fixtureView;
        private Single _x;

        #endregion

        #region Properties

        public PlanogramFixtureViewModelBase Fixture
        {
            get { return _fixtureView; }
        }

        public Single X
        {
            get { return _x; }
            set
            {
                _x = value;
                OnPropertyChanged("X");
            }

        }

        #endregion

        #region Constructor

        public DesignFixtureViewModel(PlanogramFixtureViewModelBase fixtureView)
        {
            _fixtureView = fixtureView;
            fixtureView.PropertyChanged += FixtureView_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        private void FixtureView_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region IPlanogramFixture Members

        Object IPlanFixtureRenderable.PlanogramFixtureItemId
        {
            get { return Fixture.FixtureItemModel.Id; }
        }

        Single IPlanFixtureRenderable.Y
        {
            get { return 0; }
        }
        Single IPlanFixtureRenderable.Z
        {
            get { return 0; }
        }

        Single IPlanFixtureRenderable.Angle
        {
            get { return 0; }
        }
        Single IPlanFixtureRenderable.Slope
        {
            get { return 0; }
        }
        Single IPlanFixtureRenderable.Roll
        {
            get { return 0; }
        }

        public Single Height
        {
            get { return _fixtureView.Height; }
        }
        public Single Width
        {
            get { return _fixtureView.Width; }
        }
        public Single Depth
        {
            get { return _fixtureView.Depth; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.AssembliesCollectionChanged
        {
            add { ((IPlanFixtureRenderable)_fixtureView).AssembliesCollectionChanged += value; }
            remove { ((IPlanFixtureRenderable)_fixtureView).AssembliesCollectionChanged -= value; }
        }

        IEnumerable<IPlanAssemblyRenderable> IPlanFixtureRenderable.Assemblies
        {
            get { return _fixtureView.AssemblyViews; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.ComponentsCollectionChanged
        {
            add { ((IPlanFixtureRenderable)_fixtureView).ComponentsCollectionChanged += value; }
            remove { ((IPlanFixtureRenderable)_fixtureView).ComponentsCollectionChanged -= value; }
        }


        IEnumerable<IPlanComponentRenderable> IPlanFixtureRenderable.Components
        {
            get { return _fixtureView.ComponentViews; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.AnnotationsCollectionChanged
        {
            add { ((IPlanFixtureRenderable)_fixtureView).AnnotationsCollectionChanged += value; }
            remove { ((IPlanFixtureRenderable)_fixtureView).AnnotationsCollectionChanged -= value; }
        }

        IEnumerable<IPlanAnnotationRenderable> IPlanFixtureRenderable.Annotations
        {
            get { return _fixtureView.AnnotationViews; }
        }


        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _fixtureView.PropertyChanged -= FixtureView_PropertyChanged;

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion


    }
}
