﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// A base class for implementations of view models representing a <see cref="PlanogramBlocking"/> model.
    /// </summary>
    public abstract class PlanogramBlockingViewModelBase : ViewModelBase
    {
        #region Fields

        /// <summary>
        /// The <see cref="PlanogramBlocking"/> that this view model represents.
        /// </summary>
        private PlanogramBlocking _model;

        /// <summary>
        /// The view models of the <see cref="PlanogramBlockingGroup"/>s in the <see cref="Model"/>.
        /// </summary>
        private ObservableCollection<PlanogramBlockingGroupViewModelBase> _groups;

        #endregion

        #region Properties

        /// <summary>
        /// The Id of this <see cref="PlanogramBlocking"/>.
        /// </summary>
        public Object Id
        {
            get { return _model.Id; }
        }

        /// <summary>
        /// The <see cref="PlanogramBlocking"/> that this view model represents.
        /// </summary>
        public PlanogramBlocking Model
        {
            get { return _model; }
        }

        /// <summary>
        /// Indicates what type (Initial, Performance Applied, Final) this <see cref="PlanogramBlocking"/> is.
        /// </summary>
        public PlanogramBlockingType Type
        {
            get { return _model.Type; }
            set { _model.Type = value; }
        }

        /// <summary>
        /// The collection of <see cref="PlanogramBlockingDivider"/>s on the <see cref="Model"/>.
        /// </summary>
        public PlanogramBlockingDividerList Dividers
        {
            get { return _model.Dividers; }
        }

        /// <summary>
        /// The collection of <see cref="PlanogramBlockingLocation"/>s on the <see cref="Model"/>.
        /// </summary>
        public PlanogramBlockingLocationList Locations
        {
            get { return _model.Locations; }
        }

        /// <summary>
        /// The view models of the <see cref="PlanogramBlockingGroup"/>s in the <see cref="Model"/>.
        /// </summary>
        protected ObservableCollection<PlanogramBlockingGroupViewModelBase> Groups
        {
            get { return _groups; }
        }

        #endregion

        #region Constructor

        public PlanogramBlockingViewModelBase(PlanogramBlocking blocking)
        {
            _model = blocking;
            _model.PropertyChanged += Model_PropertyChanged;
            _groups = new ObservableCollection<PlanogramBlockingGroupViewModelBase>(blocking.Groups.Select(g => CreateBlockingGroupView(g)));
            _model.Groups.BulkCollectionChanged += Model_GroupsBulkCollectionChanged;
        }


        #endregion

        #region Event Handlers
        /// <summary>
        /// Called whenever a property changes on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(sender, e.PropertyName);
        }

        /// <summary>
        /// Handles property changes in the <see cref="Model"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="propertyName"></param>
        protected virtual void OnModelPropertyChanged(Object sender, String propertyName)
        {
            if (typeof(PlanogramBlockingGroupViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }
        }

        #region Blocking Groups Bulk Collection Changed
        /// <summary>
        /// Called when the Groups collection changes on the <see cref="Model"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_GroupsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            OnModelGroupsBulkCollectionChanged(e);
        }

        /// <summary>
        /// Called when the Groups collection changes on the <see cref="Model"/>.
        /// </summary>
        protected virtual void OnModelGroupsBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramBlockingGroup group in e.ChangedItems)
                    {

                        PlanogramBlockingGroupViewModelBase view = CreateBlockingGroupView(group);
                        if (view != null)
                        {
                            _groups.Add(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramBlockingGroup group in e.ChangedItems)
                    {
                        PlanogramBlockingGroupViewModelBase view = _groups.FirstOrDefault(a => a.Model == group);
                        if (view != null)
                        {
                            _groups.Remove(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_groups.Count > 0) _groups.Clear();

                        foreach (PlanogramBlockingGroup group in _model.Groups)
                        {
                            PlanogramBlockingGroupViewModelBase view = CreateBlockingGroupView(group);
                            if (view != null)
                            {
                                _groups.Add(view);
                            }
                        }
                    }
                    break;
            }
        }
        #endregion

        #endregion

        #region Methods

        protected override string GetError(string columnName)
        {
            return String.Empty;
        }

        /// <summary>
        /// Overriden by derived classes to instantiate implementations of <see cref="PlanogramBlockingGroupViewModelBase"/> objects.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <returns></returns>
        protected abstract PlanogramBlockingGroupViewModelBase CreateBlockingGroupView(PlanogramBlockingGroup blockingGroup);

        #endregion

        #region IDisposable Members

        protected override void OnDisposing(Boolean disposing)
        {
            base.OnDisposing(disposing);
            _model.Groups.BulkCollectionChanged -= Model_GroupsBulkCollectionChanged;
            _model.PropertyChanged -= Model_PropertyChanged;
        }

        #endregion
    }
}
