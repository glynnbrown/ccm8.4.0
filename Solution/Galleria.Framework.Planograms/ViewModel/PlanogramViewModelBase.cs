﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Provides a base class for a viewmodel of a Planogram
    /// </summary>
    public abstract class PlanogramViewModelBase : ViewModelBase, IPlanRenderable
    {
        #region Fields

        private Planogram _model;
        private readonly ObservableCollection<PlanogramFixtureViewModelBase> _fixtures = new ObservableCollection<PlanogramFixtureViewModelBase>();
        private readonly ObservableCollection<PlanogramProductViewModelBase> _products = new ObservableCollection<PlanogramProductViewModelBase>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the source model object.
        /// </summary>
        public Planogram Model
        {
            get { return _model; }
            protected set { _model = value; }
        }

        public CustomAttributeData CustomAttributes
        {
            get { return _model.CustomAttributes; }
        }

        public PlanogramInventory Inventory
        {
            get { return _model.Inventory; }
        }

        #region Model Properties

        public String Name
        {
            get { return _model.Name; }
            set
            {
                _model.Name = value;
            }
        }

        public Single Height
        {
            get { return _model.Height; }
            private set
            {
                _model.Height = value;
            }
        }

        public Single Width
        {
            get { return _model.Width; }
            private set
            {
                _model.Width = value;
            }
        }

        public Single Depth
        {
            get { return _model.Depth; }
            private set
            {
                _model.Depth = value;
            }
        }

        public PlanogramLengthUnitOfMeasureType LengthUnitsOfMeasure
        {
            get { return Model.LengthUnitsOfMeasure; }
            set { Model.LengthUnitsOfMeasure = value; }
        }

        public PlanogramAreaUnitOfMeasureType AreaUnitsOfMeasure
        {
            get { return Model.AreaUnitsOfMeasure; }
            set { Model.AreaUnitsOfMeasure = value; }
        }

        public PlanogramWeightUnitOfMeasureType WeightUnitsOfMeasure
        {
            get { return Model.WeightUnitsOfMeasure; }
            set { Model.WeightUnitsOfMeasure = value; }
        }

        public PlanogramVolumeUnitOfMeasureType VolumeUnitsOfMeasure
        {
            get { return Model.VolumeUnitsOfMeasure; }
            set { Model.VolumeUnitsOfMeasure = value; }
        }

        public PlanogramCurrencyUnitOfMeasureType CurrencyUnitsOfMeasure
        {
            get { return Model.CurrencyUnitsOfMeasure; }
            set { Model.CurrencyUnitsOfMeasure = value; }
        }

        public String CategoryCode
        {
            get { return Model.CategoryCode; }
            set { Model.CategoryCode = value; }
        }

        public String CategoryName
        {
            get { return Model.CategoryName; }
            set { Model.CategoryName = value; }
        }

        public String LocationCode
        {
            get { return Model.LocationCode; }
            set { Model.LocationCode = value; }
        }

        public String LocationName
        {
            get { return Model.LocationName; }
            set { Model.LocationName = value; }
        }

        public String ClusterSchemeName
        {
            get { return Model.ClusterSchemeName; }
            set { Model.ClusterSchemeName = value; }
        }

        public String ClusterName
        {
            get { return Model.ClusterName; }
            set { Model.ClusterName = value; }
        }

        public PlanogramStatusType Status
        {
            get { return Model.Status; }
            set { Model.Status = value; }
        }

        public DateTime? DateWip
        {
            get { return Model.DateWip; }
            set { Model.DateWip = value; }
        }

        public DateTime? DateApproved
        {
            get { return Model.DateApproved; }
            set { Model.DateApproved = value; }
        }

        public DateTime? DateArchived
        {
            get { return Model.DateArchived; }
            set { Model.DateArchived = value; }
        }

        public PlanogramType PlanogramType
        {
            get { return Model.PlanogramType; }
            set { Model.PlanogramType = value; }
        }

        public PlanogramProductPlacementXType ProductPlacementX
        {
            get { return Model.ProductPlacementX; }
            set { Model.ProductPlacementX = value; }
        }

        public PlanogramProductPlacementYType ProductPlacementY
        {
            get { return Model.ProductPlacementY; }
            set { Model.ProductPlacementY = value; }
        }

        public PlanogramProductPlacementZType ProductPlacementZ
        {
            get { return Model.ProductPlacementZ; }
            set { Model.ProductPlacementZ = value; }
        }

        public String HighlightSequenceStrategy
        {
            get { return Model.HighlightSequenceStrategy; }
            set { Model.HighlightSequenceStrategy = value; }
        }

        #endregion

        #region Meta Data Properties

        public Int32? MetaBayCount
        {
            get { return _model.MetaBayCount; }
        }

        public Int32? MetaUniqueProductCount
        {
            get { return _model.MetaUniqueProductCount; }
        }

        public Int32? MetaComponentCount
        {
            get { return _model.MetaComponentCount; }
        }

        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return _model.MetaTotalMerchandisableLinearSpace; }
        }

        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return _model.MetaTotalMerchandisableAreaSpace; }
        }

        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return _model.MetaTotalMerchandisableVolumetricSpace; }
        }

        public Single? MetaTotalLinearWhiteSpace
        {
            get { return _model.MetaTotalLinearWhiteSpace; }
        }

        public Single? MetaTotalAreaWhiteSpace
        {
            get { return _model.MetaTotalAreaWhiteSpace; }
        }

        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return _model.MetaTotalVolumetricWhiteSpace; }
        }

        public Int32? MetaProductsPlaced
        {
            get { return _model.MetaProductsPlaced; }
        }

        public Int32? MetaProductsUnplaced
        {
            get { return _model.MetaProductsUnplaced; }
        }

        public Int32? MetaNewProducts
        {
            get { return _model.MetaNewProducts; }
        }

        public Int32? MetaChangesFromPreviousCount
        {
            get { return _model.MetaChangesFromPreviousCount; }
        }

        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return _model.MetaChangeFromPreviousStarRating; }
        }

        public Int32? MetaBlocksDropped
        {
            get { return _model.MetaBlocksDropped; }
        }

        public Int32? MetaNotAchievedInventory
        {
            get { return _model.MetaNotAchievedInventory; }
        }

        public Int32? MetaTotalFacings
        {
            get { return _model.MetaTotalFacings; }
        }

        public Single? MetaAverageFacings
        {
            get { return _model.MetaAverageFacings; }
        }

        public Int32? MetaTotalUnits
        {
            get { return _model.MetaTotalUnits; }
        }

        public Single? MetaAverageUnits
        {
            get { return _model.MetaAverageUnits; }
        }

        public Single? MetaMinDos
        {
            get { return _model.MetaMinDos; }
        }

        public Single? MetaMaxDos
        {
            get { return _model.MetaMaxDos; }
        }

        public Single? MetaAverageDos
        {
            get { return _model.MetaAverageDos; }
        }

        public Single? MetaMinCases
        {
            get { return _model.MetaMinCases; }
        }

        public Single? MetaAverageCases
        {
            get { return _model.MetaAverageCases; }
        }

        public Single? MetaMaxCases
        {
            get { return _model.MetaMaxCases; }
        }

        public Int32? MetaTotalComponentCollisions
        {
            get { return _model.MetaTotalComponentCollisions; }
        }

        public Int32? MetaTotalComponentsOverMerchandisedDepth
        {
            get { return _model.MetaTotalComponentsOverMerchandisedDepth; }
        }

        public Int32? MetaTotalComponentsOverMerchandisedHeight
        {
            get { return _model.MetaTotalComponentsOverMerchandisedHeight; }
        }

        public Int32? MetaTotalComponentsOverMerchandisedWidth
        {
            get { return _model.MetaTotalComponentsOverMerchandisedWidth; }
        }

        public Boolean? MetaHasComponentsOutsideOfFixtureArea
        {
            get { return _model.MetaHasComponentsOutsideOfFixtureArea; }
        }

        public Int32? MetaTotalPositionCollisions
        {
            get { return _model.MetaTotalPositionCollisions; }
        }

        public Int16? MetaTotalFrontFacings
        {
            get { return _model.MetaTotalFrontFacings; }
        }

        public Single? MetaAverageFrontFacings
        {
            get { return _model.MetaAverageFrontFacings; }
        }

        public Int32? MetaCountOfProductNotAchievedCases
        {
            get { return _model.MetaCountOfProductNotAchievedCases; }
        }

        public Int32? MetaCountOfProductNotAchievedDOS
        {
            get { return _model.MetaCountOfProductNotAchievedDOS; }
        }

        public Int32? MetaCountOfProductOverShelfLifePercent
        {
            get { return _model.MetaCountOfProductOverShelfLifePercent; }
        }

        public Int32? MetaCountOfProductNotAchievedDeliveries
        {
            get { return _model.MetaCountOfProductNotAchievedDeliveries; }
        }


        public Single? MetaPercentOfProductNotAchievedCases
        {
            get { return _model.MetaPercentOfProductNotAchievedCases; }
        }

        public Single? MetaPercentOfProductNotAchievedDOS
        {
            get { return _model.MetaPercentOfProductNotAchievedDOS; }
        }

        public Single? MetaPercentOfProductOverShelfLifePercent
        {
            get { return _model.MetaPercentOfProductOverShelfLifePercent; }
        }

        public Single? MetaPercentOfProductNotAchievedDeliveries
        {
            get { return _model.MetaPercentOfProductNotAchievedDeliveries; }
        }


        public Int32? MetaCountOfPositionsOutsideOfBlockSpace
        {
            get { return _model.MetaCountOfPositionsOutsideOfBlockSpace; }
        }

        public Single? MetaPercentOfPositionsOutsideOfBlockSpace
        {
            get { return _model.MetaPercentOfPositionsOutsideOfBlockSpace; }
        }

        public Single? MetaPercentOfPlacedProductsRecommendedInAssortment
        {
            get { return _model.MetaPercentOfPlacedProductsRecommendedInAssortment; }
        }

        public Int32? MetaCountOfPlacedProductsRecommendedInAssortment
        {
            get { return _model.MetaCountOfPlacedProductsRecommendedInAssortment; }
        }

        public Int32? MetaCountOfPlacedProductsNotRecommendedInAssortment
        {
            get { return _model.MetaCountOfPlacedProductsNotRecommendedInAssortment; }
        }

        public Int32? MetaCountOfRecommendedProductsInAssortment
        {
            get { return _model.MetaCountOfRecommendedProductsInAssortment; }
        }

        public Single? MetaPercentOfProductNotAchievedInventory
        {
            get { return _model.MetaPercentOfProductNotAchievedInventory; }
        }

        public Int16? MetaNumberOfAssortmentRulesBroken
        {
            get { return _model.MetaNumberOfAssortmentRulesBroken; }
        }

        public Int16? MetaNumberOfProductRulesBroken
        {
            get { return _model.MetaNumberOfProductRulesBroken; }
        }

        public Int16? MetaNumberOfFamilyRulesBroken
        {
            get { return _model.MetaNumberOfFamilyRulesBroken; }
        }

        public Int16? MetaNumberOfInheritanceRulesBroken
        {
            get { return _model.MetaNumberOfInheritanceRulesBroken; }
        }

        public Int16? MetaNumberOfLocalProductRulesBroken
        {
            get { return _model.MetaNumberOfLocalProductRulesBroken; }
        }

        public Int16? MetaNumberOfDistributionRulesBroken
        {
            get { return _model.MetaNumberOfDistributionRulesBroken; }
        }

        public Int16? MetaNumberOfCoreRulesBroken
        {
            get { return _model.MetaNumberOfCoreRulesBroken; }
        }

        public Single? MetaPercentageOfAssortmentRulesBroken
        {
            get { return _model.MetaPercentageOfAssortmentRulesBroken; }
        }

        public Single? MetaPercentageOfProductRulesBroken
        {
            get { return _model.MetaPercentageOfProductRulesBroken; }
        }

        public Single? MetaPercentageOfFamilyRulesBroken
        {
            get { return _model.MetaPercentageOfFamilyRulesBroken; }
        }

        public Single? MetaPercentageOfInheritanceRulesBroken
        {
            get { return _model.MetaPercentageOfInheritanceRulesBroken; }
        }

        public Single? MetaPercentageOfLocalProductRulesBroken
        {
            get { return _model.MetaPercentageOfLocalProductRulesBroken; }
        }

        public Single? MetaPercentageOfDistributionRulesBroken
        {
            get { return _model.MetaPercentageOfDistributionRulesBroken; }
        }

        public Single? MetaPercentageOfCoreRulesBroken
        {
            get { return _model.MetaPercentageOfCoreRulesBroken; }
        }

        public Int16? MetaNumberOfDelistProductRulesBroken
        {
            get { return _model.MetaNumberOfDelistProductRulesBroken; }
        }

        public Int16? MetaNumberOfForceProductRulesBroken
        {
            get { return _model.MetaNumberOfForceProductRulesBroken; }
        }

        public Int16? MetaNumberOfPreserveProductRulesBroken
        {
            get { return _model.MetaNumberOfPreserveProductRulesBroken; }
        }

        public Int16? MetaNumberOfMinimumHurdleProductRulesBroken
        {
            get { return _model.MetaNumberOfMinimumHurdleProductRulesBroken; }
        }

        public Int16? MetaNumberOfMaximumProductFamilyRulesBroken
        {
            get { return _model.MetaNumberOfMaximumProductFamilyRulesBroken; }
        }

        public Int16? MetaNumberOfMinimumProductFamilyRulesBroken
        {
            get { return _model.MetaNumberOfMinimumProductFamilyRulesBroken; }
        }

        public Int16? MetaNumberOfDependencyFamilyRulesBroken
        {
            get { return _model.MetaNumberOfDependencyFamilyRulesBroken; }
        }

        public Int16? MetaNumberOfDelistFamilyRulesBroken
        {
            get { return _model.MetaNumberOfDelistFamilyRulesBroken; }
        }

        public Int16? MetaCountOfProductsBuddied
        {
            get { return _model.MetaCountOfProductsBuddied; }
        }

        #endregion

        /// <summary>
        /// Returns the collection of planogram fixture views
        /// </summary>
        protected internal ObservableCollection<PlanogramFixtureViewModelBase> FixtureViews
        {
            get { return _fixtures; }
        }

        /// <summary>
        /// Returns the collection of planogram product views.
        /// </summary>
        protected internal ObservableCollection<PlanogramProductViewModelBase> ProductViews
        {
            get { return _products; }
        }

        /// <summary>
        /// Gets/Sets whether images should be loaded aynchronously.
        /// </summary>
        public Boolean IsImageLoadAsync { get; set; }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PlanogramViewModelBase()
        {
            this.IsImageLoadAsync = true;

            //subscribe child collections
            AddChildEventHooks(_fixtures);
            _fixtures.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramFixtureViewModelBase>;

            AddChildEventHooks(_products);
            _products.CollectionChanged += base.ChildViewModelBaseList_CollectionChanged<PlanogramProductViewModelBase>;
        }

        #endregion

        #region Event Handlers


        /// <summary>
        /// Handles a PropertyChanging event raised by the model.
        /// </summary>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            OnModelPropertyChanging(sender, e.PropertyName);
        }

        /// <summary>
        /// Carries out actions when a model property is changing.
        /// </summary>
        protected virtual void OnModelPropertyChanging(Object sender, String propertyName)
        {
            if (typeof(PlanogramViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }


        /// <summary>
        /// Handles a PropertyChanged event raised by the Model.
        /// </summary>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPropertyChanged(sender, e.PropertyName);
        }

        /// <summary>
        /// Carries out actions when a model property changes.
        /// </summary>
        protected virtual void OnModelPropertyChanged(Object sender, String propertyName)
        {
            if (typeof(PlanogramViewModelBase).GetProperty(propertyName) != null)
            {
                OnPropertyChanged(propertyName);
            }

            //Planogram Images collection change:
            //  - we only need to do this if there are actually some
            //  images in the collection otherwise it is pointless.
            if (propertyName == Planogram.ImagesProperty.Name
                && this.Model.ImagesAsync != null
                && this.Model.ImagesAsync.Any())
            {
                //notify all product views to refresh their images
                foreach (var productView in ProductViews)
                {
                    //dont bother if we have no images for the product.
                    if (!productView.Model.EnumerateImageIds().Any()) continue;

                    productView.RefreshImages();
                }

                //subcomponents
                foreach (var subView in EnumerateSubComponents())
                {
                    if (subView.Model.EnumerateImageIds().Any())
                    {
                        subView.RefreshImages();
                    }

                    foreach (var posView in subView.PositionViews)
                    {
                        if (!posView.ReferencedProduct.EnumerateImageIds().Any()) continue;
                        posView.RefreshImages();
                    }
                }
            }
        }


        /// <summary>
        /// Handles a ChildChanging event raised by the Model.
        /// </summary>
        private void Model_ChildChanging(object sender, ChildChangingEventArgs e)
        {
            OnModelChildChanging(e);
        }

        /// <summary>
        /// Carries out actions when a model child is changing.
        /// </summary>
        protected virtual void OnModelChildChanging(ChildChangingEventArgs e)
        {
            //override as required.
        }


        /// <summary>
        /// Called whenever a child of the model changes
        /// </summary>
        private void Model_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            OnModelChildChanged(e);
        }

        /// <summary>
        /// Called whenever a child of the model changes
        /// </summary>
        protected virtual void OnModelChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            if (this.IsDisposed) return; //don't process if we are disposed

            if (e.PropertyChangedArgs != null)
            {
                if (e.PropertyChangedArgs.PropertyName.StartsWith("Meta")
                    || e.PropertyChangedArgs.PropertyName.EndsWith("Async"))
                    return;

                if (e.ChildObject is PlanogramPosition)
                {
                    // - Check if a position has moved.
                    PlanogramPosition position = e.ChildObject as PlanogramPosition;

                    if (e.PropertyChangedArgs.PropertyName == PlanogramPosition.PlanogramSubComponentIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramPosition.PlanogramFixtureItemIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramPosition.PlanogramFixtureComponentIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramPosition.PlanogramFixtureAssemblyIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramPosition.PlanogramAssemblyComponentIdProperty.Name)
                    {
                        //notify all subcomponents
                        foreach (var subView in EnumerateSubComponents())
                        {
                            subView.NotifyPlanogramPositionParentChanged(position);
                        }
                    }


                }
                else if (e.ChildObject is PlanogramAnnotation)
                {
                    //- Check is an annotation has moved
                    PlanogramAnnotation annotation = e.ChildObject as PlanogramAnnotation;

                    if (e.PropertyChangedArgs.PropertyName == PlanogramAnnotation.PlanogramSubComponentIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramAnnotation.PlanogramFixtureItemIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramAnnotation.PlanogramFixtureComponentIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramAnnotation.PlanogramFixtureAssemblyIdProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramAnnotation.PlanogramAssemblyComponentIdProperty.Name)
                    {
                        //notify all fixtures, components and subcomponents.
                        foreach (var fixtureView in FixtureViews)
                        {
                            fixtureView.NotifyPlanogramAnnotationParentChanged(annotation);
                        }
                        foreach (var component in EnumerateComponents())
                        {
                            component.NotifyPlanogramAnnotationParentChanged(annotation);

                            //notify all subcomponents
                            foreach (var subView in EnumerateSubComponents())
                            {
                                subView.NotifyPlanogramAnnotationParentChanged(annotation);
                            }
                        }
                    }

                }
                else if (e.ChildObject is PlanogramFixture)
                {
                    //- Check is an fixture has moved
                    PlanogramFixture fixture = e.ChildObject as PlanogramFixture;

                    if (e.PropertyChangedArgs.PropertyName == PlanogramFixture.WidthProperty.Name)
                    {
                        IncrementFixtureXPositions(fixture);
                    }
                }
            }
        }

        /// <summary>
        /// Called whenever the fixture items collection changes on the planogram model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_FixtureItemsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            OnPlanogramFixtureItemsBulkCollectionChanged(e);
        }

        /// <summary>
        /// Called whenever the fixture items collection changes on the planogram model.
        /// </summary>
        protected virtual void OnPlanogramFixtureItemsBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramFixtureItem fixtureItem in e.ChangedItems)
                    {
                        PlanogramFixtureViewModelBase view = CreateFixtureView(fixtureItem);
                        if (view != null)
                        {
                            _fixtures.Add(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramFixtureItem fixtureItem in e.ChangedItems)
                    {
                        PlanogramFixtureViewModelBase fixtureView =
                            _fixtures.FirstOrDefault(a => a.FixtureItemModel == fixtureItem);
                        if (fixtureView != null)
                        {
                            _fixtures.Remove(fixtureView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _fixtures.Clear();
                    if (Model != null)
                    {
                        foreach (PlanogramFixtureItem fixtureItem in Model.FixtureItems)
                        {
                            PlanogramFixtureViewModelBase view = CreateFixtureView(fixtureItem);
                            if (view != null)
                            {
                                _fixtures.Add(view);
                            }
                        }
                    }
                    break;
            }
        }


        /// <summary>
        /// Called whenever the products collection changes on the planogram model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_ProductsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            OnPlanogramProductsBulkCollectionChanged(e);
        }

        /// <summary>
        /// Called whenever the products collection changes on the planogram model.
        /// </summary>
        protected virtual void OnPlanogramProductsBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramProduct product in e.ChangedItems)
                    {
                        PlanogramProductViewModelBase view = CreateProductView(product);
                        if (view != null)
                        {
                            _products.Add(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramProduct product in e.ChangedItems)
                    {
                        PlanogramProductViewModelBase productView = _products.FirstOrDefault(p => p.Model == product);
                        if (productView != null)
                        {
                            _products.Remove(productView);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _products.Clear();
                    if (Model != null)
                    {
                        foreach (PlanogramProduct product in Model.Products)
                        {
                            PlanogramProductViewModelBase view = CreateProductView(product);
                            if (view != null)
                            {
                                _products.Add(view);
                            }
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the new model.
        /// </summary>
        protected virtual void SetModel(Planogram newModel, ViewStateObjectModelChangeReason reason)
        {
            Planogram oldModel = _model;

            if (oldModel != null)
            {
                OnRemoveEventHooks(oldModel);
            }

            _model = newModel;
            OnPropertyChanged("Model");

            if (newModel != null)
            {
                OnAddEventHooks(newModel);
            }

            OnPlanogramProductsBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
            OnPlanogramFixtureItemsBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
        }

        /// <summary>
        /// Attaches event handlers to the given model
        /// </summary>
        /// <param name="model"></param>
        public virtual void OnAddEventHooks(Planogram model)
        {
            model.PropertyChanging += Model_PropertyChanging;
            model.PropertyChanged += Model_PropertyChanged;
            model.ChildChanging += Model_ChildChanging;
            model.ChildChanged += Model_ChildChanged;

            model.Products.BulkCollectionChanged += Planogram_ProductsBulkCollectionChanged;
            model.FixtureItems.BulkCollectionChanged += Planogram_FixtureItemsBulkCollectionChanged;
        }

        /// <summary>
        /// Dettaches event handlers from the given model.
        /// </summary>
        /// <param name="model"></param>
        public virtual void OnRemoveEventHooks(Planogram model)
        {
            model.PropertyChanging -= Model_PropertyChanging;
            model.PropertyChanged -= Model_PropertyChanged;
            model.ChildChanging -= Model_ChildChanging;
            model.ChildChanged -= Model_ChildChanged;

            model.Products.BulkCollectionChanged -= Planogram_ProductsBulkCollectionChanged;
            model.FixtureItems.BulkCollectionChanged -= Planogram_FixtureItemsBulkCollectionChanged;
        }


        /// <summary>
        /// Creates a new viewmodel for the given product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        protected abstract PlanogramProductViewModelBase CreateProductView(PlanogramProduct product);

        /// <summary>
        /// Creates a new viewmodel for the given fixture
        /// </summary>
        /// <param name="fixture"></param>
        /// <returns></returns>
        protected abstract PlanogramFixtureViewModelBase CreateFixtureView(PlanogramFixtureItem fixture);

        /// <summary>
        /// For IDataErrorInfo
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected override String GetError(String columnName)
        {
            return ((IDataErrorInfo)this.Model)[columnName];
        }

        /// <summary>
        /// Find the viewmodel of the product with the given id.
        /// </summary>
        /// <param name="productModelId"></param>
        /// <returns></returns>
        protected internal PlanogramProductViewModelBase FindProductView(Object productModelId)
        {
            return ProductViews.FirstOrDefault(p => Object.Equals(p.Model.Id, productModelId));
        }

        /// <summary>
        /// Returns the value of the given field for this item.
        /// </summary>
        public virtual Object GetFieldValue(ObjectFieldInfo fieldInfo)
        {
            return fieldInfo.GetValue(this);
        }

        /// <summary>
        /// Enumerates through all displayable field infos.
        /// </summary>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFields()
        {
            foreach (ObjectFieldInfo info in Planogram.EnumerateDisplayableFieldInfos(/*incMetadata*/true))
            {
                yield return info;
            }
        }

        /// <summary>
        /// Enumerates through all child subcomponents.
        /// </summary>
        private IEnumerable<PlanogramComponentViewModelBase> EnumerateComponents()
        {
            foreach (var fixtureView in FixtureViews)
            {
                foreach (var assemblyView in fixtureView.AssemblyViews)
                {
                    foreach (var componentView in assemblyView.ComponentViews)
                    {
                        yield return componentView;
                    }
                }

                foreach (var componentView in fixtureView.ComponentViews)
                {
                    yield return componentView;
                }
            }
        }

        /// <summary>
        /// Enumerates through all child subcomponents.
        /// </summary>
        private IEnumerable<PlanogramSubComponentViewModelBase> EnumerateSubComponents()
        {
            foreach (var fixtureView in FixtureViews)
            {
                foreach (var assemblyView in fixtureView.AssemblyViews)
                {
                    foreach (var componentView in assemblyView.ComponentViews)
                    {
                        foreach (var subView in componentView.SubComponentViews)
                        {
                            yield return subView;
                        }
                    }
                }

                foreach (var componentView in fixtureView.ComponentViews)
                {
                    foreach (var subView in componentView.SubComponentViews)
                    {
                        yield return subView;
                    }
                }
            }
        }

        /// <summary>
        /// Increments the fixture x positions based upon fixture width changing.
        /// </summary>
        /// <param name="fixture">The fixture that is changing width</param>
        private void IncrementFixtureXPositions(PlanogramFixture fixture)
        {
            PlanogramFixtureViewModelBase fixtureModelBase = FixtureViews.FirstOrDefault(p => p.FixtureModel == fixture);
            if (fixtureModelBase != null)
            {
                Single xPosition = fixtureModelBase.X + fixtureModelBase.Width;

                // increment each fixture that proceeds the given fixture
                foreach (PlanogramFixtureViewModelBase fixtures in FixtureViews.Where(p => p.X > fixtureModelBase.X)
                                                                                .OrderBy(p => p.X))
                {
                    fixtures.X = xPosition;
                    xPosition = fixtures.X + fixtures.Width;
                }
            }
        }

        #endregion

        #region IPlanogram Members

        event NotifyCollectionChangedEventHandler IPlanRenderable.FixturesCollectionChanged
        {
            add { _fixtures.CollectionChanged += value; }
            remove { _fixtures.CollectionChanged -= value; }
        }

        IEnumerable<IPlanFixtureRenderable> IPlanRenderable.Fixtures
        {
            get { return this.FixtureViews; }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void OnDisposing(Boolean disposing)
        {
            _fixtures.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramFixtureViewModelBase>;
            _products.CollectionChanged -= base.ChildViewModelBaseList_CollectionChanged<PlanogramProductViewModelBase>;


            base.OnDisposing(disposing);

            OnRemoveEventHooks(this.Model);
        }

        #endregion
    }
}
