﻿// Copyright © Galleria RTS Ltd 2016

using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.ViewModel
{
    /// <summary>
    /// Defines the expected members of an object which notifies
    /// when one of its children is about to change
    /// </summary>
    public interface INotifyViewModelChildChanging
    {
        event EventHandler<ViewModelChildChangingEventArgs> ChildChanging;
    }

    /// <summary>
    /// Event args for the INotifyChildChanging ChildChanging event.
    /// </summary>
    public class ViewModelChildChangingEventArgs : EventArgs
    {
        #region Properties

        /// <summary>
        /// The object source about to change
        /// </summary>
        public Object ChildObject { get; private set; }

        /// <summary>
        /// The property changing event args
        /// </summary>
        public PropertyChangingEventArgs PropertyChangingArgs { get; private set; }

        #endregion

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="child"></param>
        /// <param name="args"></param>
        public ViewModelChildChangingEventArgs(Object child, PropertyChangingEventArgs args)
        {
            this.ChildObject = child;
            this.PropertyChangingArgs = args;
        }
    }
}
