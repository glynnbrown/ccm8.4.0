﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: v8.0

// V8-27474 : A.Silva
//      Created
// V8-27570 : A.Silva
//      Moved IPlanogramComponent out to its own file.
// V8-32593 : A.Silva
//  Exposed Id so it can be access through the interface.

#endregion

#region Version History : CCM830

// V8-32522 : A.Silva
//  Exposed IsEntirelyOutsideParentFixture().

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    ///     Interface that defines 
    /// </summary>
    public interface IPlanogramFixtureComponent
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the Id for this instance.
        /// </summary>
        Object Id { get; set; }

        /// <summary>
        ///		Gets or sets the sequence number (order) for this instance.
        /// </summary>
        Int16? ComponentSequenceNumber { get; set; }

        /// <summary>
        ///     Defines the X coordinate for the <see cref="IPlanogramFixtureComponent"/>, relative to its container.
        /// </summary>
        Single X { get; set; }

        /// <summary>
        ///     Defines the Y coordinate for the <see cref="IPlanogramFixtureComponent"/>, relative to its container.
        /// </summary>
        Single Y { get; set; }

        /// <summary>
        ///     Defines the Z coordinate for the <see cref="IPlanogramFixtureComponent"/>, relative to its container.
        /// </summary>
        Single Z { get; set; }

        /// <summary>
        ///     Defines the Slope for the <see cref="IPlanogramFixtureComponent"/>.
        /// </summary>
        Single Slope { get; set; }

        /// <summary>
        ///     Defines the Angle for the <see cref="IPlanogramFixtureComponent"/>.
        /// </summary>
        Single Angle { get; set; }

        /// <summary>
        ///     Defines the Roll for the <see cref="IPlanogramFixtureComponent"/>.
        /// </summary>
        Single Roll { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Returns all sub component placements associated to this assembly component.
        /// </summary>
        IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements();

        /// <summary>
        ///     Returns the planogram component.
        /// </summary>
        PlanogramComponent GetPlanogramComponent();

        /// <summary>
        /// Checks to see if a component is within it's parent fixture area.
        /// </summary>
        /// <returns>Returns true if the current component is entirely outside the fixture area, else false. Null when not on a fixture.</returns>
        Boolean? IsEntirelyOutsideParentFixture();

        #endregion
    }
}