﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

#region Version History: CCM801

// V8-28878 : A.Silva
//      Amended name of RestartComponentRenumberingPerBayProperty to make it consistent with the database field name.

#endregion

using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    ///     Declares the expected members for a type that defines a Planogram Renumbering Strategy.
    /// </summary>
    public interface IPlanogramRenumberingStrategy
    {
        /// <summary>
        ///     Gets or sets the value for the <see cref="Name" /> property.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentXRenumberStrategyPriority" /> property.
        /// </summary>
        Byte BayComponentXRenumberStrategyPriority { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentYRenumberStrategyPriority" /> property.
        /// </summary>
        Byte BayComponentYRenumberStrategyPriority { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentZRenumberStrategyPriority" /> property.
        /// </summary>
        Byte BayComponentZRenumberStrategyPriority { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionXRenumberStrategyPriority" /> property.
        /// </summary>
        Byte PositionXRenumberStrategyPriority { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionYRenumberStrategyPriority" /> property.
        /// </summary>
        Byte PositionYRenumberStrategyPriority { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionZRenumberStrategyPriority" /> property.
        /// </summary>
        Byte PositionZRenumberStrategyPriority { get; set; }

        /// <summary>
        ///     Gets or sets the type of X (horizontal) renumbering strategy to apply to bays and components.
        /// </summary>
        PlanogramRenumberingStrategyXRenumberType BayComponentXRenumberStrategy { get; set; }

        /// <summary>
        ///     Gets or sets the type of Y (vertical) renumbering strategy to apply to bays and components.
        /// </summary>
        PlanogramRenumberingStrategyYRenumberType BayComponentYRenumberStrategy { get; set; }

        /// <summary>
        ///     Gets or sets the type of Z (depth) renumbering strategy to apply to bays and components.
        /// </summary>
        PlanogramRenumberingStrategyZRenumberType BayComponentZRenumberStrategy { get; set; }

        /// <summary>
        ///     Gets or sets the type of X (horizontal) renumbering strategy to apply to positions.
        /// </summary>
        PlanogramRenumberingStrategyXRenumberType PositionXRenumberStrategy { get; set; }

        /// <summary>
        ///     Gets or sets the type of Y (vertical) renumbering strategy to apply to positions.
        /// </summary>
        PlanogramRenumberingStrategyYRenumberType PositionYRenumberStrategy { get; set; }

        /// <summary>
        ///     Gets or sets the type of Z (depth) renumbering strategy to apply to positions.
        /// </summary>
        PlanogramRenumberingStrategyZRenumberType PositionZRenumberStrategy { get; set; }

        /// <summary>
        ///     Gets or sets whether component sequence renumbering restarts from #1 for each bay.
        /// </summary>
        Boolean RestartComponentRenumberingPerBay { get; set; }

        /// <summary>
        ///     Gets or sets whether component sequence renumbering ignores non merchandising ones.
        /// </summary>
        Boolean IgnoreNonMerchandisingComponents { get; set; }

        /// <summary>
        ///     Gets or sets whether position sequence renumbering restarts from #1 for each component.
        /// </summary>
        Boolean RestartPositionRenumberingPerComponent { get; set; }

        /// <summary>
        ///     Gets or sets whether different positions for the same product get unique sequence numbers.
        /// </summary>
        Boolean UniqueNumberMultiPositionProductsPerComponent { get; set; }

        /// <summary>
        ///     Gets or sets whether different positions for the same product, but adjacent, get the same sequence number when
        ///     unique sequence numbers per position is in place.
        /// </summary>
        Boolean ExceptAdjacentPositions { get; set; }


        /// <summary>
        ///     Gets or sets whether position sequence renumbering restarts from #1 for each bay.
        /// </summary>
        Boolean RestartPositionRenumberingPerBay { get; set; }

        /// <summary>
        ///     Gets or sets whether component sequence renumbering restarts from #1 for each bay.
        /// </summary>
        Boolean RestartComponentRenumberingPerComponentType { get; set; }
    }
}
