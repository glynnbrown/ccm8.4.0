﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-26704 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Interface that defines the info properties on a Product model.
    /// </summary>
    public interface IPlanogramProductInfo
    {
        String Gtin { get; }
        String Name { get; }
    }
}
