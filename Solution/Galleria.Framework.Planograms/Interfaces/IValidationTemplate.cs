﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: v8.00

// V8-26721 : A.Silva ~ Created
// V8-26815 : A.Silva ~ Added LoadFrom method.
// V8-26817 : A.Silva ~ Updated descriptions for the methods in the interface.

#endregion

#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Removed LoadFrom as the ccm.model object shouldn't load this way.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Core;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    ///     Represents the information needed to validate a planogram.
    /// </summary>
    public interface IValidationTemplate : IEditableBusinessObject
    {
        /// <summary>
        ///     Gets or sets the name of the template.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        ///     Gets the groups included in this template.
        /// </summary>
        IEnumerable<IValidationTemplateGroup> Groups { get; }

        /// <summary>
        ///     Adds a new instance, implementing the <see cref="IValidationTemplateGroup"/> interface, to the groups collection.
        /// </summary>
        /// <param name="groupName">the name of the new group.</param>
        /// <returns>The newly created and added instance of <see cref="IValidationTemplateGroup"/>.</returns>
        IValidationTemplateGroup AddNewGroup(String name);

        /// <summary>
        ///     Removes the given <paramref name="templateGroup"/> from the collection of groups in the template.
        /// </summary>
        /// <param name="templateGroup">Instance that will be removed from the groups in this instance.</param>
        void RemoveGroup(IValidationTemplateGroup templateGroup);

    }
}