﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26041 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramProductImageHelper
    {
        Byte[] GetImageData(PlanogramImageType imageType, PlanogramImageFacingType facingType);
    }
}
