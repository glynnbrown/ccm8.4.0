﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: v8.00

// V8-26721 : A.Silva ~ Created
// V8-26815 : A.Silva ~ Added Threshold1 and 2 properties to be exposed in the Interface.
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Core;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    ///     Represents a group containing validation metrics that are related somehow to one another.
    /// </summary>
    public interface IValidationTemplateGroup : IEditableBusinessObject
    {
        /// <summary>
        ///     Gets the collection of <see cref="IValidationTemplateGroupMetric" /> items assigned to this instance.
        /// </summary>
        IList<IValidationTemplateGroupMetric> Metrics { get; }

        /// <summary>
        ///     Gets or sets the string that users use to identify this instance.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        ///     Gets or sets the value that will be used as a valid threshold.
        /// </summary>
        Single Threshold1 { get; set; }

        /// <summary>
        ///     Gets or sets the value that will be used as a warning threshold.
        /// </summary>
        Single Threshold2 { get; set; }

        /// <summary>
        ///     Adds a new metric to the internal collection, and returns it.
        /// </summary>
        /// <returns>
        ///     A new instance implementing <see cref="IValidationTemplateGroupMetric" /> that was just added to the
        ///     collection.
        /// </returns>
        IValidationTemplateGroupMetric AddNewMetric();

        /// <summary>
        ///     Removes an existing metric from the internal collection.
        /// </summary>
        /// <param name="metric">
        ///     Instance implementing <see cref="IValidationTemplateGroupMetric" /> to be removed from the
        ///     internal collection.
        /// </param>
        void RemoveMetric(IValidationTemplateGroupMetric metric);

        /// <summary>
        ///     Gets or sets the type of validation calculation to use with this instance.
        /// </summary>
        PlanogramValidationType ValidationType { get; set; }
    }
}