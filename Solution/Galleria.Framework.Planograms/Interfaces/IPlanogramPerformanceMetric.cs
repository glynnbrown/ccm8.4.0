﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created.
#endregion
#endregion

using System;
using Galleria.Framework.Enums;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Interface that defines a planogram performance metric
    /// </summary>
    public interface IPlanogramPerformanceMetric
    {
        #region Properties
        String Name { get; }
        String Description { get; }
        MetricDirectionType Direction { get; }
        MetricSpecialType SpecialType { get; }
        MetricType Type { get; }
        Byte MetricId { get; }
        #endregion
    }
}
