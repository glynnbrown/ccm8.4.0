﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    ///     Defines the common properties and functionality expected of any class
    ///     that is able to provide configuration values to perform a planogram comparison.
    /// </summary>
    public interface IPlanogramComparisonSettings
    {
        #region Properties

        /// <summary>
        ///     Get/Set the name used to identify this comparison settings to the user.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        ///     Get/Set the description used for this comparison settings.
        /// </summary>
        String Description { get; set; }

        /// <summary>
        ///     Get/Set the last date compared. Not used for this model, just here to coply with the interface.
        /// </summary>
        DateTime? DateLastCompared { get; set; }

        /// <summary>
        ///     Get/Set whether non placed products will be considered missing in this comparison settings.
        /// </summary>
        Boolean IgnoreNonPlacedProducts { get; set; }

        /// <summary>
        ///     Get/Set the data order type used to group results in this comparison settings.
        /// </summary>
        DataOrderType DataOrderType { get; set; }

        /// <summary>
        ///     Get the list of <see cref="PlanogramComparisonField" /> items used to compare different planograms.
        /// </summary>
        IEnumerable<IPlanogramComparisonSettingsField> ComparisonFields { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Loads the data contained in the <paramref name="source" /> instance implementing
        ///     <see cref="IPlanogramComparisonSettings" /> into the current instance.
        /// </summary>
        /// <param name="source">
        ///     Instance containing the data to load, and that implements
        ///     <see cref="IPlanogramComparisonSettings" />.
        /// </param>
        void Load(IPlanogramComparisonSettings source);

        /// <summary>
        ///     Resets the internal collection, clearing all existing comparison fields in it.
        /// </summary>
        void ResetComparisonFields();

        /// <summary>
        ///     Add the fields in the enumeration to the current comparison fields in this instance.
        /// </summary>
        /// <param name="sourceItems">The collection of <see cref="IPlanogramComparisonSettingsField"/> to add.</param>
        void AddComparisonFields(IEnumerable<IPlanogramComparisonSettingsField> sourceItems);

        #endregion

        /// <summary>
        ///     Add a collection of <see cref="IPlanogramComparisonSettingsField" /> derived from an enumeration of
        ///     <see cref="ObjectFieldInfo" />.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType" /> that this range should be targeting.</param>
        /// <param name="fieldInfos">The enumeration of <see cref="ObjectFieldInfo" /> from which to derive the new items.</param>
        void AddComparisonFields(PlanogramItemType itemType, Dictionary<ObjectFieldInfo, Boolean> fieldInfos);

        void AddComparisonFields(PlanogramItemType itemType, IEnumerable<IPlanogramComparisonSettingsField> sourceItems);
    }

    /// <summary>
    ///     Defines the common properties and functionality expected of any class
    ///     that is able to define a comparison field to use when performing a planogram comparison.
    /// </summary>
    public interface IPlanogramComparisonSettingsField
    {
        #region Properties

        /// <summary>
        ///     The <see cref="PlanogramItemType" /> that this instance's field is.
        /// </summary>
        PlanogramItemType ItemType { get; set; }

        /// <summary>
        ///     The internal field place holder used to evaluate the value in a given plan item.
        /// </summary>
        String FieldPlaceholder { get; set; }

        /// <summary>
        ///     The display name presented to the user on the UI.
        /// </summary>
        String DisplayName { get; set; }

        /// <summary>
        ///     The order of this field in the collection of fields (to order columns in a datagrid and such).
        /// </summary>
        Int16 Number { get; set; }

        /// <summary>
        ///     Whether this field should be displayed on the UI, when such thing is optional.
        /// </summary>
        Boolean Display { get; set; }

        #endregion
    }
}