﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-26421 : N.Foster
//  Created
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// An interface that defines settings that are
    /// required when manipulating planograms
    /// </summary>
    public interface IPlanogramSettings
    {
        #region Properties
        /// <summary>
        /// Returns the default fixture height
        /// </summary>
        Single FixtureHeight { get; }

        /// <summary>
        /// Returns the default fixture width
        /// </summary>
        Single FixtureWidth { get; }

        /// <summary>
        /// Returns the default fixture depth
        /// </summary>
        Single FixtureDepth { get; }

        /// <summary>
        /// Returns the default base height
        /// </summary>
        Single BaseHeight { get; }

        /// <summary>
        /// Returns the default backboard depth
        /// </summary>
        Single BackboardDepth { get; }

        /// <summary>
        /// Returns the default bar height
        /// </summary>
        Single BarHeight { get; }

        /// <summary>
        /// Returns the default bar width
        /// </summary>
        Single BarWidth { get; }

        /// <summary>
        /// Returns the default bar depth
        /// </summary>
        Single BarDepth { get; }

        /// <summary>
        /// Returns the default bar fill colour
        /// </summary>
        Int32 BarFillColour { get; }

        /// <summary>
        /// Returns the default chest height
        /// </summary>
        Single ChestHeight { get; }

        /// <summary>
        /// Returns the default chest width
        /// </summary>
        Single ChestWidth { get; }

        /// <summary>
        /// Returns the default chest depth
        /// </summary>
        Single ChestDepth { get; }

        /// <summary>
        /// Returns the default chest fill colour
        /// </summary>
        Int32 ChestFillColour { get; }

        /// <summary>
        /// Returns the default clip strip height
        /// </summary>
        Single ClipStripHeight { get; }

        /// <summary>
        /// Returns the default clip strip width
        /// </summary>
        Single ClipStripWidth { get; }

        /// <summary>
        /// Returns the default clip strip depth
        /// </summary>
        Single ClipStripDepth { get; }

        /// <summary>
        /// Returns the default clip strip fill colour
        /// </summary>
        Int32 ClipStripFillColour { get; }

        /// <summary>
        /// Returns the default height
        /// </summary>
        Single ShelfHeight { get; }

        /// <summary>
        /// Returns the default width
        /// </summary>
        Single ShelfWidth { get; }

        /// <summary>
        /// Returns the default depth
        /// </summary>
        Single ShelfDepth { get; }

        /// <summary>
        /// Returns the default shelf fill colour
        /// </summary>
        Int32 ShelfFillColour { get; }

        /// <summary>
        /// Returns the default pegboard height
        /// </summary>
        Single PegboardHeight { get; }

        /// <summary>
        /// Returns the default pegboard width
        /// </summary>
        Single PegboardWidth { get; }

        /// <summary>
        /// Returns the default pegboard depth
        /// </summary>
        Single PegboardDepth { get; }

        /// <summary>
        /// Returns the default pegboard fill colour
        /// </summary>
        Int32 PegboardFillColour { get; }

        /// <summary>
        /// Returns the default rod height
        /// </summary>
        Single RodHeight { get; }

        /// <summary>
        /// Returns the default rod width
        /// </summary>
        Single RodWidth { get; }

        /// <summary>
        /// Returns the default rod depth
        /// </summary>
        Single RodDepth { get; }

        /// <summary>
        /// Returns the default rod fill colour
        /// </summary>
        Int32 RodFillColour { get; }

        /// <summary>
        /// Returns the default pallet height
        /// </summary>
        Single PalletHeight { get; }

        /// <summary>
        /// Returns the default pallet width
        /// </summary>
        Single PalletWidth { get; }

        /// <summary>
        /// Returns the default pallet depth
        /// </summary>
        Single PalletDepth { get; }

        /// <summary>
        /// Returns the default pallet fill colour
        /// </summary>
        Int32 PalletFillColour { get; }

        /// <summary>
        /// Returns the default Slotwall height
        /// </summary>
        Single SlotwallHeight { get; }

        /// <summary>
        /// Returns the default Slotwall width
        /// </summary>
        Single SlotwallWidth { get; }

        /// <summary>
        /// Returns the default Slotwall depth
        /// </summary>
        Single SlotwallDepth { get; }

        /// <summary>
        /// Returns the default Slotwall fill colour
        /// </summary>
        Int32 SlotwallFillColour { get; }

        /// <summary>
        /// Returns the default textbox font
        /// </summary>
        String TextBoxFont { get; }

        /// <summary>
        /// Returns the default textbox font size
        /// </summary>
        Single TextBoxFontSize { get; }

        /// <summary>
        /// Returns true of the textbox setting for reduce to fit is on.
        /// </summary>
        Boolean TextBoxCanReduceToFit { get; }

        /// <summary>
        /// The default textbox font colour.
        /// </summary>
        Int32 TextBoxFontColour { get; }

        /// <summary>
        /// The default textbox background colour
        /// </summary>
        Int32 TextBoxBackgroundColour { get; }

        /// <summary>
        /// The default textbox border colour
        /// </summary>
        Int32 TextBoxBorderColour { get; }

        /// <summary>
        /// The default textbox border thickness
        /// </summary>
        Single TextBoxBorderThickness { get; }

        #endregion
    }
}
