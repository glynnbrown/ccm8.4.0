﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31550 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramAssortmentProductBuddy
    {
        String ProductGtin { get; }
        PlanogramAssortmentProductBuddySourceType SourceType { get; }
        PlanogramAssortmentProductBuddyTreatmentType TreatmentType { get; }
        Single TreatmentTypePercentage { get; }
        PlanogramAssortmentProductBuddyProductAttributeType ProductAttributeType { get; }
        String S1ProductGtin { get; }
        Single? S1Percentage { get; }
        String S2ProductGtin { get; }
        Single? S2Percentage { get; }
        String S3ProductGtin { get; }
        Single? S3Percentage { get; }
        String S4ProductGtin { get; }
        Single? S4Percentage { get; }
        String S5ProductGtin { get; }
        Single? S5Percentage { get; }
    }
}
