﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion

using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramBlockingLocation : INotifyPropertyChanged
    {
        Object PlanogramBlockingGroupId { get; set; }
        Object PlanogramBlockingDividerTopId { get; set; }
        Object PlanogramBlockingDividerBottomId { get; set; }
        Object PlanogramBlockingDividerLeftId { get; set; }
        Object PlanogramBlockingDividerRightId { get; set; }

        Single X { get; }
        Single Y { get; }
        Single Width { get; }
        Single Height { get; }

        /// <summary>
        /// Returns the actual blocking group.
        /// </summary>
        /// <returns></returns>
        IPlanogramBlockingGroup GetBlockingGroup();
        
        /// <summary>
        /// Returns true if this location is associated with the given divider
        /// </summary>
        /// <param name="divider"></param>
        /// <returns></returns>
        Boolean IsAssociated(IPlanogramBlockingDivider divider);

        /// <summary>
        /// Returns true if this location can be combined into
        /// the one given.
        /// </summary>
        Boolean CanCombineWith(IPlanogramBlockingLocation location);

        /// <summary>
        /// Combines this location into the one given.
        /// </summary>
        /// <param name="location"></param>
        void CombineWith(IPlanogramBlockingLocation location);
    }
}
