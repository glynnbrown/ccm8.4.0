﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM803
// V8-28491 : L.Luong
//  Created
#endregion
#region Version History: (CCM v8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramMetadataHelper
    {
        Boolean IsProductGtinInMasterData(String gtin);

        IPlanogramImageSettings GetPlanogramImageSettings(Planogram plan);
        Boolean IsProductIllegal(Planogram plan, String gtin);
        Boolean IsProductNotLegal(Planogram plan, String gtin);
    }
}
