﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26888 L.Ineson ~ Created
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Defines the expected members of a class 
    /// used to create a planogram label.
    /// </summary>
    public interface IPlanogramLabel
    {
        Int32 BackgroundColour { get;  }
        Int32 BorderColour { get;  }
        Int32 BorderThickness { get;  }
        String Text { get; }
        Int32 TextColour { get; }
        String Font { get; }
        Int32 FontSize { get; }
        Boolean IsRotateToFitOn { get; }
        Boolean IsShrinkToFitOn { get; }
        Boolean TextBoxFontBold { get; }
        Boolean TextBoxFontItalic { get; }
        Boolean TextBoxFontUnderlined { get; }
        PlanogramLabelHorizontalAlignment LabelHorizontalPlacement { get; }
        PlanogramLabelVerticalAlignment LabelVerticalPlacement { get; }
        PlanogramLabelTextAlignment TextBoxTextAlignment { get; }
        PlanogramLabelTextDirection TextDirection { get; }
        PlanogramLabelTextWrapping TextWrapping { get; }
        Single BackgroundTransparency { get; }
        Boolean ShowOverImages { get; }

    }

    #region Supporting Enums

    /// <summary>
    /// Denotes the faces that a label may attach to.
    /// </summary>
    public enum PlanogramLabelAttachFace
    {
        Front = 0,
        Top = 1
    }

    /// <summary>
    /// Denotes the possible text wrapping of a label.
    /// </summary>
    public enum PlanogramLabelTextWrapping
    {
        Word = 0,
        Letter = 1,
        None = 2
    }

    /// <summary>
    /// Denotes the horizontal alignments of a label
    /// </summary>
    public enum PlanogramLabelHorizontalAlignment
    {
        Left = 0,
        Center = 1,
        Right = 2
    }

    /// <summary>
    /// Denotes the possible vertical alignments of a label
    /// </summary>
    public enum PlanogramLabelVerticalAlignment
    {
        Top = 0,
        Center = 1,
        Bottom = 2
    }

    /// <summary>
    /// Denotes the possible text directions of a label
    /// </summary>
    public enum PlanogramLabelTextDirection
    {
        Vertical = 0,
        Horizontal = 1,
    }

    /// <summary>
    /// Denotes the possible text alignments of a label.
    /// </summary>
    public enum PlanogramLabelTextAlignment
    {
        Left = 0,
        Right = 1,
        Center = 2,
        Justified = 3
    }

    #endregion
}
