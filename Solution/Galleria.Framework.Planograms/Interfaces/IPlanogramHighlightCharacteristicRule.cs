﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.10)
// V8-28661 L.Ineson 
//  Created
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramHighlightCharacteristicRule
    {
        String Field { get; }
        PlanogramHighlightCharacteristicRuleType Type { get; }
        String Value { get; }
    }

    public enum PlanogramHighlightCharacteristicRuleType
    {
        Equals,
        NotEquals,
        LessThan,
        LessThanOrEqualTo,
        MoreThan,
        MoreThanOrEqualTo,
        Contains,
    }
}
