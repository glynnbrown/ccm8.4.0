﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Interface that defines planogram performance
    /// </summary>
    public interface IPlanogramPerformance
    {
        #region Properties
        String Name { get; }
        String Description { get; }
        IEnumerable<IPlanogramPerformanceMetric> Metrics { get; }

        #endregion
    }
}
