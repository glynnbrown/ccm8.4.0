﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26704 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramAssortmentRegionProduct
    {
        String PrimaryProductGtin { get; }
        String RegionalProductGtin { get; }
    }
}
