﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30795 : A.Kuszyk
//  Created
// V8-31004 : D.Pleasance
//  Added Id property to hold reference of the sequenced sub component item
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Defines the members required for an object sequenced on a <see cref="PlanogramSubComponentPlacement"/>.
    /// </summary>
    public interface IPlanogramSubComponentSequencedItem
    {
        /// <summary>
        /// The id of the sequenced sub component item
        /// </summary>
        Object Id { get; }

        /// <summary>
        /// The sequence value of the item in the X direction.
        /// </summary>
        Int16 SequenceX { get; }

        /// <summary>
        /// The sequence value of the item in the Y direction.
        /// </summary>
        Int16 SequenceY { get; }

        /// <summary>
        /// The sequence value of the item in the Z direction.
        /// </summary>
        Int16 SequenceZ { get; }

        /// <summary>
        /// Gets the <see cref="PlanogramSubComponentPlacement"/> that this item is sequenced on.
        /// </summary>
        /// <returns></returns>
        PlanogramSubComponentPlacement GetParentSubComponentPlacement();
    }
}
