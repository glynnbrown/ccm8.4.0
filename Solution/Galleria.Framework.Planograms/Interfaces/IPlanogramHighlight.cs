﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.10)
// V8-28661 L.Ineson 
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Interface denoting the expected members of a planogram highlight
    /// </summary>
    public interface IPlanogramHighlight
    {
        String Name { get; }
        PlanogramHighlightMethodType MethodType { get;  }
        Boolean IsFilterEnabled { get; }
        Boolean IsPreFilter { get; }
        Boolean IsAndFilter { get; }
        String Field1 { get; }
        String Field2 { get; }
        PlanogramHighlightQuadrantType QuadrantXType { get; }
        Single QuadrantXConstant { get; }
        PlanogramHighlightQuadrantType QuadrantYType { get; }
        Single QuadrantYConstant { get; }

        IEnumerable<IPlanogramHighlightGroup> Groups { get; }
        IEnumerable<IPlanogramHighlightFilter> Filters { get; }
        IEnumerable<IPlanogramHighlightCharacteristic> Characteristics { get; }

        void ClearGroups();
    }


    public enum PlanogramHighlightMethodType
    {
        Group,
        Quadrant,
        ColourScale,
        Characteristic
    }

    public enum PlanogramHighlightQuadrantType
    {
        Mean,
        Median,
        MidRange,
        Constant
    }

}
