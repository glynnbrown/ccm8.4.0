﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: v8.00

// V8-26721 : A.Silva ~ Created
// V8-26815 : A.Silva ~ Added Field, AggregationType, Threshold1 and 2, Score1, 2 and 3 properties to be exposed in the Interface.
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#region Version History: CCM803

// V8-29596 : L.Luong
//  Added Criteria

#endregion

#endregion

using System;
using Csla.Core;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    ///     Represents a single metric that gives a score when evaluated.
    /// </summary>
    public interface IValidationTemplateGroupMetric : IEditableBusinessObject
    {
        /// <summary>
        ///     Gets or sets the type of aggregation that will be used when there are multiple values for the <see cref="Field" />.
        /// </summary>
        PlanogramValidationAggregationType AggregationType { get; set; }

        /// <summary>
        ///     Gets or sets the string that defines the field or formula used for this metric's calculation.
        /// </summary>
        String Field { get; set; }

        /// <summary>
        ///     Gets the parent for this instance.
        /// </summary>
        IValidationTemplateGroup Parent { get; }

        /// <summary>
        ///     Gets or sets the value that will be assigned as score if the metric's value is valid.
        /// </summary>
        Single Score1 { get; set; }

        /// <summary>
        ///     Gets or sets the value that will be assigned as score if the metric's value is a warning.
        /// </summary>
        Single Score2 { get; set; }

        /// <summary>
        ///     Gets or sets the value that will be assigned as score if the metric's value is a failure.
        /// </summary>
        Single Score3 { get; set; }

        /// <summary>
        ///     Gets or sets the value that will be used as a valid threshold.
        /// </summary>
        Single Threshold1 { get; set; }

        /// <summary>
        ///     Gets or sets the value that will be used as a warning threshold.
        /// </summary>
        Single Threshold2 { get; set; }

        /// <summary>
        ///     Gets or sets the type of validation calculation to use with this instance.
        /// </summary>
        PlanogramValidationType ValidationType { get; set; }

        /// <summary>
        ///     Gets or sets the criteria to use with this instance.
        /// </summary>
        String Criteria { get; set; }
    }
}