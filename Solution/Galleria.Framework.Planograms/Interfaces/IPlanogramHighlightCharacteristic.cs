﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.10)
// V8-28661 L.Ineson 
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PlanogramHighlightCharacteristic 
    /// </summary>
    public interface IPlanogramHighlightCharacteristic
    {
        String Name { get; }
        Boolean IsAndFilter { get; }
        IEnumerable<IPlanogramHighlightCharacteristicRule> Rules { get; }
    }
}
