﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31550 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramAssortmentLocationBuddy
    {
        String LocationCode { get; }
        PlanogramAssortmentLocationBuddyTreatmentType TreatmentType { get; }
        String S1LocationCode { get; }
        Single? S1Percentage { get; }
        String S2LocationCode { get; }
        Single? S2Percentage { get; }
        String S3LocationCode { get; }
        Single? S3Percentage { get; }
        String S4LocationCode { get; }
        Single? S4Percentage { get; }
        String S5LocationCode { get; }
        Single? S5Percentage { get; }
    }
}
