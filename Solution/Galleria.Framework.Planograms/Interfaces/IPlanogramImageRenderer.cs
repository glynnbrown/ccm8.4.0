﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26888 L.Ineson 
//  Created
#endregion
#region Version History: (CCM 8.3)
// V8-32636 : A.Probyn
//  Added Annotations
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Rendering;

namespace Galleria.Framework.Planograms.Interfaces
{
    #region IPlanogramImageRenderer

    /// <summary>
    /// Denotes the expected members of an image helper
    /// </summary>
    public interface IPlanogramImageRenderer : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets/Sets the height of the produced image
        /// </summary>
        Double ImageHeight {get; set;}

        /// <summary>
        /// Gets/Sets the width of the produced image
        /// </summary>
        Double ImageWidth {get; set;}

        /// <summary>
        /// Gets/Sets whether a software render should be used.
        /// </summary>
        Boolean IsSoftwareRender {get; set;}

        /// <summary>
        /// Get/Sets whether the image should be scaled
        /// to fit the height and width fully. If off 
        /// then it will be scaled proportionately to the maximum.
        /// </summary>
        Boolean FitUnproportionately { get; set; }


        /// <summary>
        /// Gets/Sets the image settings to use.
        /// </summary>
        IPlanogramImageSettings ImageSettings { get; set; }

        /// <summary>
        /// Gets/Sets PlanogramModelData
        /// </summary>
        List<Plan3DData> PlanogramModelDataList { get; set; }
        #endregion
      
        #region Methods

        /// <summary>
        /// Returns the image data for the given plan
        /// based on the current settings.
        /// </summary>
        Byte[] CreateImageData(Planogram plan);

        /// <summary>
        /// Returns the image data for the given plan
        /// based on the current settings.
        /// </summary>
        Byte[] CreateImageData(Planogram plan, Int32 bayRangeStart, Int32 bayRangeEnd);

        #endregion
    }

    #endregion

    #region IPlanogramImageSettings

    /// <summary>
    /// Denotes the expected members of a class containing settings
    /// required to render a planogram image.
    /// </summary>
    public interface IPlanogramImageSettings
    {
        PlanogramImageViewType ViewType { get; }

        Boolean ChestWalls { get; }
        Boolean RotateTopDownComponents { get; }
        Boolean Risers { get; }
        Boolean FixtureImages { get; }
        Boolean FixtureFillPatterns { get; }
        Boolean Notches { get; }
        Boolean PegHoles { get; }
        Boolean DividerLines { get; }
        Boolean Positions { get; }
        Boolean PositionUnits { get; }
        Boolean ProductImages { get; }
        Boolean ProductShapes { get; }
        Boolean ProductFillColours { get; }
        Boolean ProductFillPatterns { get; }
        Boolean Pegs { get; }
        Boolean Dividers { get; }
        Boolean TextBoxes { get; }
        Boolean Annotations { get; }

        IPlanogramLabel ProductLabel { get; }
        IPlanogramLabel FixtureLabel { get; }

        PlanogramPositionHighlightColours PositionHighlights { get; }
        PlanogramPositionLabelTexts PositionLabelText { get; }
        PlanogramFixtureLabelTexts FixtureLabelText { get; }

        VectorValue? CameraLookDirection { get; }
        PointValue? CameraPosition { get; }
    }

    #endregion

    #region PlanogramPositionLabelTexts

    /// <summary>
    /// Used to hold information about label text to be displayed.
    /// </summary>
    public sealed class PlanogramPositionLabelTexts : IEnumerable<KeyValuePair<Object, String>>
    {
        #region Fields
        private Dictionary<Object, String> _refDict;
        #endregion

        #region Properties

        public String this[Object positionId]
        {
            get { return _refDict[positionId]; }
        }

        #endregion

        #region Constructor

        public PlanogramPositionLabelTexts(Dictionary<Object, String> positionToText)
        {
            _refDict = positionToText.ToDictionary(k => k.Key, v => v.Value);
        }

        public PlanogramPositionLabelTexts(String labelTextFormat, Planogram planogram)
        {
            var dict = new Dictionary<Object, String>(planogram.Positions.Count);
            var allFieldInfos = PlanogramFieldHelper.EnumerateAllFields(planogram).ToList();

            foreach (PlanogramPosition position in planogram.Positions)
            {
                dict[position.Id] = PlanogramFieldHelper.ResolvePositionLabel(labelTextFormat, position, allFieldInfos);
            }
            _refDict = dict;
        }

        #endregion

        #region Methods

        public Boolean TryGetValue(Object positionId, out String value)
        {
            return _refDict.TryGetValue(positionId, out value);
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator<KeyValuePair<Object, String>> GetEnumerator()
        {
            return _refDict.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _refDict.GetEnumerator();
        }

        #endregion
    }

    #endregion

    #region PlanogramFixtureLabelTexts

    /// <summary>
    /// Used to hold information about label text to be displayed.
    /// </summary>
    public sealed class PlanogramFixtureLabelTexts : IEnumerable<KeyValuePair<Object, String>>
    {
        #region Fields
        private Dictionary<Object, String> _refDict;
        #endregion

        #region Properties

        public String this[Object componentPlacementId]
        {
            get { return _refDict[componentPlacementId]; }
        }

        #endregion

        #region Constructor

        public PlanogramFixtureLabelTexts(Dictionary<Object, String> placementidToText)
        {
            _refDict = placementidToText.ToDictionary(k => k.Key, v => v.Value);
        }

        public PlanogramFixtureLabelTexts(String labelTextFormat, Planogram planogram)
        {
            var dict = new Dictionary<Object, String>(planogram.Positions.Count);

            var allFieldInfos = PlanogramFieldHelper.EnumerateAllFields(planogram).ToList();

            foreach (PlanogramSubComponentPlacement subPlacement in planogram.GetPlanogramSubComponentPlacements())
            {
                PlanogramComponentPlacementId componentPlacementId = new PlanogramComponentPlacementId(subPlacement);
                String value;

                if (!dict.ContainsKey(componentPlacementId))
                {
                    //don't display a label if this is a backboard or base.
                    if (subPlacement.Component.ComponentType == PlanogramComponentType.Backboard
                        || subPlacement.Component.ComponentType == PlanogramComponentType.Base)
                    {
                        value = null;
                    }
                    else
                    {
                        value = PlanogramFieldHelper.ResolveComponentLabel(labelTextFormat, subPlacement, allFieldInfos);
                    }


                    dict[componentPlacementId] = value;
                }

            }

            _refDict = dict;
        }

        #endregion

        #region Methods

        public Boolean TryGetValue(Object positionId, out String value)
        {
            return _refDict.TryGetValue(positionId, out value);
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator<KeyValuePair<Object, String>> GetEnumerator()
        {
            return _refDict.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _refDict.GetEnumerator();
        }

        #endregion
    }

    #endregion

    #region PlanogramPositionHighlightColours

    /// <summary>
    /// Used to hold information about highlights to be displayed.
    /// </summary>
    public sealed class PlanogramPositionHighlightColours : IEnumerable<KeyValuePair<Object, Int32>>
    {
        #region Fields
        private Dictionary<Object, Int32> _refDict;
        #endregion

        #region Properties

        public Int32 this[Object positionId]
        {
            get { return _refDict[positionId]; }
        }

        #endregion

        #region Constructor

        public PlanogramPositionHighlightColours(Dictionary<Object, Int32> positionToColour)
        {
            _refDict = positionToColour.ToDictionary(k => k.Key, v => v.Value);
        }

        public PlanogramPositionHighlightColours(PlanogramHighlightResult highlightResult)
        {
            Dictionary<Object, Int32> colourDict = new Dictionary<Object, Int32>();
            foreach (PlanogramHighlightResultGroup highlightGroup in highlightResult.Groups)
            {
                Int32 highlightGroupColour = highlightGroup.Colour;
                foreach (PlanogramPosition item in highlightGroup.Items)
                {
                    colourDict[item.Id] = highlightGroupColour;
                }
            }
            _refDict = colourDict;
        }

        #endregion

        #region Methods

        public Boolean TryGetValue(Object positionId, out Int32 value)
        {
            return _refDict.TryGetValue(positionId, out value);
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator<KeyValuePair<Object, Int32>> GetEnumerator()
        {
            return _refDict.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _refDict.GetEnumerator();
        }

        #endregion
    }

    #endregion

    #region PlanogramImageViewType

    /// <summary>
    /// Denotes the different view types for
    /// generating planogram images.
    /// </summary>
    public enum PlanogramImageViewType
    {
        Design,
        Perspective,
        Front,
        Back,
        Left,
        Right,
        Top,
        Bottom
    }

    #endregion
}
