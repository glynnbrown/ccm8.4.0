#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28676 : A.Silva
//      Created.

#endregion

#region Version History: CCM802
// V8-29018 : D.Pleasance
//      Added IsRunning property
#endregion

#region Version History: CCM830
// V8-32953 : L.Ineson
// GetImageAsync now accepts a userstate
#endregion

#endregion

using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    ///     Defines a methods to retrive real images and a way to configure how to do so.
    /// </summary>
    public interface IRealImageProvider : IDisposable
    {
        /// <summary>
        ///     Gets an image using the configuration.
        /// </summary>
        /// <param name="imageTarget"></param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <param name="imageUpdateCallBack">The action to invoke on return</param>
        /// <param name="userState">The user state parameter to pass to the callback action.</param>
        PlanogramImage GetImageAsync(Object imageTarget, PlanogramImageFacingType facingType, PlanogramImageType imageType,
            Action<Object> imageUpdateCallBack, Object userState);
        
        PlanogramImage GetImage(Object imageTarget, PlanogramImageFacingType facingType, PlanogramImageType imageType);

        /// <summary>
        ///     Loads new settings into the provider.
        /// </summary>
        /// <param name="settings"></param>
        void Reload(IRealImageProviderSettings settings);

        //void StartFetchAsync();
        void PauseFetchAsync();
        void ContinueFetchAsync();
        Boolean IsRunning { get; }
        void StopFetchAsync();

        void PreloadPlanogram(Planogram plan, Boolean commonFacesOnly);
    }
}