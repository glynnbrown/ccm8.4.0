﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-25787 : N.Foster
//  Created
// V8-26041 : A.Kuszyk
//  Added additional Product properties.
//  Removed image properties - these aren't necessary as images are implemented differently
//  Products when compared with PlanogramProducts.
// CCM-26150 : L.Ineson
//	Added CustomAttributes property
// V8-27606 : L.Ineson
//  Added FinancialGroupCode and FinancialGroupName
// V8-26777 : L.Ineson
//  Removed CanMerch properties
#endregion

#region Version History: CCM 830
// V8-31531
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Intefact that defines the properties
    /// that are available on a planogram product
    /// </summary>
    public interface IPlanogramProduct
    {
        #region Properties
        Object Id { get; }
        String Gtin { get; set; }
        String Name { get; set; }
        String Brand { get; set; }
        Single Height { get; set; }
        Single Width { get; set; }
        Single Depth { get; set; }
        Single DisplayHeight { get; set; }
        Single DisplayWidth { get; set; }
        Single DisplayDepth { get; set; }
        Single AlternateHeight { get; set; }
        Single AlternateWidth { get; set; }
        Single AlternateDepth { get; set; }
        Single PointOfPurchaseHeight { get; set; }
        Single PointOfPurchaseWidth { get; set; }
        Single PointOfPurchaseDepth { get; set; }
        Byte NumberOfPegHoles { get; set; }
        Single PegX { get; set; }
        Single PegX2 { get; set; }
        Single PegX3 { get; set; }
        Single PegY { get; set; }
        Single PegY2 { get; set; }
        Single PegY3 { get; set; }
        Single PegProngOffsetX { get; set; }
        Single PegProngOffsetY { get; set; }
        Single PegDepth { get; set; }
        Single SqueezeHeight { get; set; }
        Single SqueezeWidth { get; set; }
        Single SqueezeDepth { get; set; }
        Single NestingHeight { get; set; }
        Single NestingWidth { get; set; }
        Single NestingDepth { get; set; }
        Int16 CasePackUnits { get; set; }
        Byte CaseHigh { get; set; }
        Byte CaseWide { get; set; }
        Byte CaseDeep { get; set; }
        Single CaseHeight { get; set; }
        Single CaseWidth { get; set; }
        Single CaseDepth { get; set; }
        Byte MaxStack { get; set; }
        Byte MaxTopCap { get; set; }
        Byte MaxRightCap { get; set; }
        Byte MinDeep { get; set; }
        Byte MaxDeep { get; set; }
        Int16 TrayPackUnits { get; set; }
        Byte TrayHigh { get; set; }
        Byte TrayWide { get; set; }
        Byte TrayDeep { get; set; }
        Single TrayHeight { get; set; }
        Single TrayWidth { get; set; }
        Single TrayDepth { get; set; }
        Single TrayThickHeight { get; set; }
        Single TrayThickWidth { get; set; }
        Single TrayThickDepth { get; set; }
        Single FrontOverhang { get; set; }
        Single FingerSpaceAbove { get; set; }
        Single FingerSpaceToTheSide { get; set; }
        PlanogramProductStatusType StatusType { get; set; }
        PlanogramProductOrientationType OrientationType { get; set; }
        PlanogramProductMerchandisingStyle MerchandisingStyle { get; set; }
        Boolean IsFrontOnly { get; set; }
        Boolean IsPlaceHolderProduct { get; set; }
        Boolean IsActive { get; set; }
        Boolean CanBreakTrayUp { get; set; }
        Boolean CanBreakTrayDown { get; set; }
        Boolean CanBreakTrayBack { get; set; }
        Boolean CanBreakTrayTop { get; set; }
        Boolean ForceMiddleCap { get; set; }
        Boolean ForceBottomCap { get; set; }
        PlanogramProductShapeType ShapeType { get; set; }
        PlanogramProductFillPatternType FillPatternType { get; set; }
        Int32 FillColour { get; set; }

        // Additional properties added from CCM Product Model
        String Shape { get; set; }
        String PointOfPurchaseDescription { get; set; }
        String ShortDescription { get; set; }
        String Subcategory { get; set; }
        String CustomerStatus { get; set; }
        String Colour { get; set; }
        String Flavour { get; set; }
        String PackagingShape { get; set; }
        String PackagingType { get; set; }
        String CountryOfOrigin { get; set; }
        String CountryOfProcessing { get; set; }
        Int16 ShelfLife { get; set; }
        Single? DeliveryFrequencyDays { get; set; }
        String DeliveryMethod { get; set; }
        String VendorCode { get; set; }
        String Vendor { get; set; }
        String ManufacturerCode { get; set; }
        String Manufacturer { get; set; }
        String Size { get; set; }
        String UnitOfMeasure { get; set; }
        DateTime? DateIntroduced { get; set; }
        DateTime? DateDiscontinued { get; set; }
        DateTime? DateEffective { get; set; }
        String Health { get; set; }
        String CorporateCode { get; set; }
        String Barcode { get; set; }
        Single? SellPrice { get; set; }
        Int16? SellPackCount { get; set; }
        String SellPackDescription { get; set; }
        Single? RecommendedRetailPrice { get; set; }
        Single? ManufacturerRecommendedRetailPrice { get; set; }
        Single? CostPrice { get; set; }
        Single? CaseCost { get; set; }
        Single? TaxRate { get; set; }
        String ConsumerInformation { get; set; }
        String Texture { get; set; }
        Int16? StyleNumber { get; set; }
        String Pattern { get; set; }
        String Model { get; set; }
        String GarmentType { get; set; }
        Boolean IsPrivateLabel { get; set; }
        Boolean IsNewProduct { get; set; }
        String FinancialGroupCode { get; set; }
        String FinancialGroupName { get; set; }

        ICustomAttributeData CustomAttributes { get; }

        #endregion
    }
}
