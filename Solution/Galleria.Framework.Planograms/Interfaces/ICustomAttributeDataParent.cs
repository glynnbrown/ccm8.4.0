﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM801
// V8-28722 : N.Foster
//  Created
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface ICustomAttributeDataParent
    {
        #region Properties

        /// <summary>
        /// The parent id
        /// </summary>
        Object Id { get; }

        /// <summary>
        /// The custom attribute data parent type
        /// </summary>
        Byte ParentType { get; }

        #endregion
    }
}
