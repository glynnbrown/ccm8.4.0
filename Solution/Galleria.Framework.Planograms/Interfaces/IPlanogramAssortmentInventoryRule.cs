﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31551 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramAssortmentInventoryRule
    {
        String ProductGtin { get; }
        Single CasePack { get; }
        Single DaysOfSupply { get; }
        Single ShelfLife { get; }
        Single ReplenishmentDays { get; }
        Single WasteHurdleUnits { get; }
        Single WasteHurdleCasePack { get; }
        Int32 MinUnits { get; }
        Int32 MinFacings { get; }
    }
}
