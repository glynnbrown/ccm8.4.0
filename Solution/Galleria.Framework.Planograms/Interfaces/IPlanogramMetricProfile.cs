﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-29597 : N.Foster
//  Created
#endregion

#region Version History: CCM830
// V8-13996 : J.Pickup
// Introduced Name - taskrefactor required it.
#endregion
#endregion

using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Defines a metric profile used by a planogram
    /// </summary>
    public interface IPlanogramMetricProfile
    {
        #region Properties

        String Name { get; set; }
        Single? Metric1Ratio { get; set; }
        Single? Metric2Ratio { get; set; }
        Single? Metric3Ratio { get; set; }
        Single? Metric4Ratio { get; set; }
        Single? Metric5Ratio { get; set; }
        Single? Metric6Ratio { get; set; }
        Single? Metric7Ratio { get; set; }
        Single? Metric8Ratio { get; set; }
        Single? Metric9Ratio { get; set; }
        Single? Metric10Ratio { get; set; }
        Single? Metric11Ratio { get; set; }
        Single? Metric12Ratio { get; set; }
        Single? Metric13Ratio { get; set; }
        Single? Metric14Ratio { get; set; }
        Single? Metric15Ratio { get; set; }
        Single? Metric16Ratio { get; set; }
        Single? Metric17Ratio { get; set; }
        Single? Metric18Ratio { get; set; }
        Single? Metric19Ratio { get; set; }
        Single? Metric20Ratio { get; set; }

        #endregion

        #region Methods

        IEnumerable<Tuple<PlanogramPerformanceMetric, Single>> EnumerateAppliedMetricsAndRatios();

        #endregion
    }
}
