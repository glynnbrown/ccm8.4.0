﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26147 : L.Ineson
//  Created
#endregion
#region Version History : CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Intefact that defines the properties
    /// that are available on CustomAttributeData
    /// </summary>
    public interface ICustomAttributeData
    {
        #region Properties
         String Text1 { get; set; }
         String Text2 { get; set; }
         String Text3 { get; set; }
         String Text4 { get; set; }
         String Text5 { get; set; }
         String Text6 { get; set; }
         String Text7 { get; set; }
         String Text8 { get; set; }
         String Text9 { get; set; }
         String Text10 { get; set; }
         String Text11 { get; set; }
         String Text12 { get; set; }
         String Text13 { get; set; }
         String Text14 { get; set; }
         String Text15 { get; set; }
         String Text16 { get; set; }
         String Text17 { get; set; }
         String Text18 { get; set; }
         String Text19 { get; set; }
         String Text20 { get; set; }
         String Text21 { get; set; }
         String Text22 { get; set; }
         String Text23 { get; set; }
         String Text24 { get; set; }
         String Text25 { get; set; }
         String Text26 { get; set; }
         String Text27 { get; set; }
         String Text28 { get; set; }
         String Text29 { get; set; }
         String Text30 { get; set; }
         String Text31 { get; set; }
         String Text32 { get; set; }
         String Text33 { get; set; }
         String Text34 { get; set; }
         String Text35 { get; set; }
         String Text36 { get; set; }
         String Text37 { get; set; }
         String Text38 { get; set; }
         String Text39 { get; set; }
         String Text40 { get; set; }
         String Text41 { get; set; }
         String Text42 { get; set; }
         String Text43 { get; set; }
         String Text44 { get; set; }
         String Text45 { get; set; }
         String Text46 { get; set; }
         String Text47 { get; set; }
         String Text48 { get; set; }
         String Text49 { get; set; }
         String Text50 { get; set; }
         Single Value1 { get; set; }
         Single Value2 { get; set; }
         Single Value3 { get; set; }
         Single Value4 { get; set; }
         Single Value5 { get; set; }
         Single Value6 { get; set; }
         Single Value7 { get; set; }
         Single Value8 { get; set; }
         Single Value9 { get; set; }
         Single Value10 { get; set; }
         Single Value11 { get; set; }
         Single Value12 { get; set; }
         Single Value13 { get; set; }
         Single Value14 { get; set; }
         Single Value15 { get; set; }
         Single Value16 { get; set; }
         Single Value17 { get; set; }
         Single Value18 { get; set; }
         Single Value19 { get; set; }
         Single Value20 { get; set; }
         Single Value21 { get; set; }
         Single Value22 { get; set; }
         Single Value23 { get; set; }
         Single Value24 { get; set; }
         Single Value25 { get; set; }
         Single Value26 { get; set; }
         Single Value27 { get; set; }
         Single Value28 { get; set; }
         Single Value29 { get; set; }
         Single Value30 { get; set; }
         Single Value31 { get; set; }
         Single Value32 { get; set; }
         Single Value33 { get; set; }
         Single Value34 { get; set; }
         Single Value35 { get; set; }
         Single Value36 { get; set; }
         Single Value37 { get; set; }
         Single Value38 { get; set; }
         Single Value39 { get; set; }
         Single Value40 { get; set; }
         Single Value41 { get; set; }
         Single Value42 { get; set; }
         Single Value43 { get; set; }
         Single Value44 { get; set; }
         Single Value45 { get; set; }
         Single Value46 { get; set; }
         Single Value47 { get; set; }
         Single Value48 { get; set; }
         Single Value49 { get; set; }
         Single Value50 { get; set; }
         Boolean Flag1 { get; set; }
         Boolean Flag2 { get; set; }
         Boolean Flag3 { get; set; }
         Boolean Flag4 { get; set; }
         Boolean Flag5 { get; set; }
         Boolean Flag6 { get; set; }
         Boolean Flag7 { get; set; }
         Boolean Flag8 { get; set; }
         Boolean Flag9 { get; set; }
         Boolean Flag10 { get; set; }
         DateTime? Date1 { get; set; }
         DateTime? Date2 { get; set; }
         DateTime? Date3 { get; set; }
         DateTime? Date4 { get; set; }
         DateTime? Date5 { get; set; }
         DateTime? Date6 { get; set; }
         DateTime? Date7 { get; set; }
         DateTime? Date8 { get; set; }
         DateTime? Date9 { get; set; }
         DateTime? Date10 { get; set; }
        String Note1 { get; set; }
        String Note2 { get; set; }
        String Note3 { get; set; }
        String Note4 { get; set; }
        String Note5 { get; set; }
        #endregion
    }
}
