﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created
// V8-27476 : L.Luong
//      Added BlockPlacementX and BlockPlacementY 
#endregion

#region Version History : CCM 802
// V8-28989 : A.Kuszyk
//  Added TotalSpacePercentage property.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion

#region Version History : CCM 830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
#endregion
#endregion

using System;
using System.ComponentModel;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramBlockingGroup : INotifyPropertyChanged
    {
        Object Id { get; }
        String Name { get; set; }
        Int32 Colour { get; set; }
        PlanogramBlockingFillPatternType FillPatternType { get; set; }
        Boolean CanCompromiseSequence { get; set; }
        Boolean IsRestrictedByComponentType { get; set; }
        Boolean CanMerge { get; set; }
        Boolean IsLimited { get; set; }
        Single LimitedPercentage { get; set; }
        PlanogramBlockingGroupPlacementType BlockPlacementPrimaryType { get; }
        PlanogramBlockingGroupPlacementType BlockPlacementSecondaryType { get; }
        PlanogramBlockingGroupPlacementType BlockPlacementTertiaryType { get; }
        Single TotalSpacePercentage { get; }
        IEnumerable<IPlanogramBlockingLocation> GetBlockingLocations();
        void UpdateAssociatedLocations(IEnumerable<IPlanogramBlockingLocation> locations);
    }

    

    
}
