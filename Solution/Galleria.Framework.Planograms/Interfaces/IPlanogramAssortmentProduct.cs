﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-26704 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM 830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Interface that defines the properties on a PlanogramAssortmentProduct
    /// </summary>
    public interface IPlanogramAssortmentProduct
    {
        String Gtin { get; set; }
        String Name { get; set; }
        Boolean IsRanged { get; set; }
        Int16 Rank { get; set; }
        String Segmentation { get; set; }
        Byte Facings { get; set; }
        Int16 Units { get; set; }
        PlanogramAssortmentProductTreatmentType ProductTreatmentType { get; set; }
        PlanogramAssortmentProductLocalizationType ProductLocalizationType { get; set; }
        String Comments { get; set; }
        Byte? ExactListFacings { get; set; }
        Int16? ExactListUnits { get; set; }
        Byte? PreserveListFacings { get; set; }
        Int16? PreserveListUnits { get; set; }
        Byte? MaxListFacings { get; set; }
        Int16? MaxListUnits { get; set; }
        Byte? MinListFacings { get; set; }
        Int16? MinListUnits { get; set; }
        String FamilyRuleName { get; set; }
        PlanogramAssortmentProductFamilyRuleType FamilyRuleType { get; set; }
        Byte? FamilyRuleValue { get; set; }
        Boolean IsPrimaryRegionalProduct { get; set; }
    }
}
