﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#region Version History: (CCM 830)
// V8-32985 : D.Pleasance
//		Added IsLimited \ LimitedPercentage
#endregion
#endregion

using System;
using System.ComponentModel;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Defines the expected members of an object representing 
    /// a planogram blocking divider
    /// </summary>
    public interface IPlanogramBlockingDivider : INotifyPropertyChanged
    {
        /// <summary>
        /// The object id
        /// </summary>
        Object Id { get; }

        /// <summary>
        /// The type of divider this is - vert or horiz
        /// </summary>
        PlanogramBlockingDividerType Type { get; }
        
        /// <summary>
        /// The level number at which the divider sits
        /// </summary>
        Byte Level { get; }

        /// <summary>
        /// The x position of the divider
        /// This is a percentage value between 0 and 1.
        /// </summary>
        Single X { get; }

        /// <summary>
        /// The y position of the divider
        /// This is a percentage value between 0 and 1.
        /// </summary>
        Single Y { get; }

        /// <summary>
        /// The length of the divider
        /// This is a percentage value between 0 and 1.
        /// </summary>
        Single Length { get; }

        /// <summary>
        /// Gets/Sets the divider should snap.
        /// </summary>
        Boolean IsSnapped { get; }

        /// <summary>
        /// Determines if the divider is limited
        /// </summary>
        Boolean IsLimited { get; set; }

        /// <summary>
        /// Percentage of space that the divider is limited by
        /// </summary>
        Single LimitedPercentage { get; set; }

        /// <summary>
        /// Method to move this divider to the given value.
        /// The new value is a percentage value between 0 and 1.
        /// </summary>
        void MoveTo(Single newValue);
    }
}
