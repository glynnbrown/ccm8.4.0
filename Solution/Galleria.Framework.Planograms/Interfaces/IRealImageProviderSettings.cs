#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28676 : A.Silva
//      Created.
// V8-28679 : A.Silva
//      Added ProductImageTransparency.

#endregion

#endregion

using System;
using System.Reflection;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IRealImageProviderSettings
    {
        /// <summary>
        ///		Gets or sets the currently set image source type.
        /// </summary>
        RealImageProviderType ProductImageSource { get; set; }

        /// <summary>
        ///		Gets or sets the value for the Product Images folder.
        /// </summary>
        String ProductImageLocation { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageAttribute"/> property.
        /// </summary>
        String ProductImageAttribute { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageIgnoreLeadingZeros"/> property.
        /// </summary>
        Boolean ProductImageIgnoreLeadingZeros { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageMinFieldLength"/> property.
        /// </summary>
        Byte ProductImageMinFieldLength { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixFront"/> property.
        /// </summary>
        String ProductImageFacingPostfixFront { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixLeft"/> property.
        /// </summary>
        String ProductImageFacingPostfixLeft { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixTop"/> property.
        /// </summary>
        String ProductImageFacingPostfixTop { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixBack"/> property.
        /// </summary>
        String ProductImageFacingPostfixBack { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixRight"/> property.
        /// </summary>
        String ProductImageFacingPostfixRight { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixBottom"/> property.
        /// </summary>
        String ProductImageFacingPostfixBottom { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageCompression"/> property.
        /// </summary>
        RealImageCompressionType ProductImageCompression { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageColourDepth"/> property.
        /// </summary>
        RealImageColourDepthType ProductImageColourDepth { get; set; }

        /// <summary>
        ///     Gets or sets whether to apply transparency to the images loaded from folders.
        /// </summary>
        Boolean ProductImageTransparency { get; set; }
    }

    public class RealImageProviderSettings : IRealImageProviderSettings 
    {
        #region Properties

        /// <summary>
        ///		Gets or sets the currently set image source type.
        /// </summary>
        public RealImageProviderType ProductImageSource { get; set; }

        /// <summary>
        ///		Gets or sets the value for the Product Images folder.
        /// </summary>
        public string ProductImageLocation { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageAttribute"/> property.
        /// </summary>
        public string ProductImageAttribute { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageIgnoreLeadingZeros"/> property.
        /// </summary>
        public bool ProductImageIgnoreLeadingZeros { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageMinFieldLength"/> property.
        /// </summary>
        public byte ProductImageMinFieldLength { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageFacingPostfixFront"/> property.
        /// </summary>
        public string ProductImageFacingPostfixFront { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageFacingPostfixLeft"/> property.
        /// </summary>
        public string ProductImageFacingPostfixLeft { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageFacingPostfixTop"/> property.
        /// </summary>
        public string ProductImageFacingPostfixTop { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageFacingPostfixBack"/> property.
        /// </summary>
        public string ProductImageFacingPostfixBack { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageFacingPostfixRight"/> property.
        /// </summary>
        public string ProductImageFacingPostfixRight { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageFacingPostfixBottom"/> property.
        /// </summary>
        public string ProductImageFacingPostfixBottom { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageCompression"/> property.
        /// </summary>
        public RealImageCompressionType ProductImageCompression { get; set; }

        /// <summary>
        ///		Gets or sets the value for the <see cref="IRealImageProviderSettings.ProductImageColourDepth"/> property.
        /// </summary>
        public RealImageColourDepthType ProductImageColourDepth { get; set; }

        /// <summary>
        ///     Gets or sets whether to apply transparency to the images loaded from folders.
        /// </summary>
        public bool ProductImageTransparency { get; set; }

        #endregion

        #region Constructor

        public RealImageProviderSettings(IRealImageProviderSettings settings)
        {
            Type type = typeof (IRealImageProviderSettings);
            foreach (PropertyInfo propertyInfo in type.GetProperties())
            {
                propertyInfo.SetValue(this, propertyInfo.GetValue(settings, null), null);
            }
        }

        #endregion
    }
}