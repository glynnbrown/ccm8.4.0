﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.10)
// V8-28661 L.Ineson 
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a planogram highlight filter
    /// </summary>
    public interface IPlanogramHighlightFilter
    {
        String Field { get; }
        PlanogramHighlightFilterType Type { get; }
        String Value { get; }
    }

    public enum PlanogramHighlightFilterType
    {
        Equals,
        NotEquals,
        LessThan,
        LessThanOrEqualTo,
        MoreThan,
        MoreThanOrEqualTo,
        Contains,
    }

    public static class PlanogramHighlightFilterTypeHelper
    {
        public static readonly Dictionary<PlanogramHighlightFilterType, String> FriendlyNames =
            new Dictionary<PlanogramHighlightFilterType, String>()
            {
                {PlanogramHighlightFilterType.Equals, "=" },
                {PlanogramHighlightFilterType.NotEquals, "<>" },
                {PlanogramHighlightFilterType.LessThan, "<" },
                {PlanogramHighlightFilterType.LessThanOrEqualTo, "<=" },
                {PlanogramHighlightFilterType.MoreThan, ">" },
                {PlanogramHighlightFilterType.MoreThanOrEqualTo, ">=" },
                {PlanogramHighlightFilterType.Contains, "Contains"/*Message.HighlightType_Contains*/ },
            };
    }
}
