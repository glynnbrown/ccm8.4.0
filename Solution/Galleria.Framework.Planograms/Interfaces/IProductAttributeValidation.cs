using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Interface used to model if a class knows how to handle product attribute validation warnings
    /// </summary>
    public interface IProductAttributeValidationWarning
    {
        /// <summary>
        /// Method to clear product validation warnings
        /// </summary>
        void ClearProductAttributeValidationWarnings();

       /// <summary>
       /// Method to register a Product Attribute Validation Warning
       /// </summary>
       /// <param name="gtin">Product GTIN</param>
       /// <param name="attributeCompared">Attribute compared</param>
       /// <param name="expectedMasterDataValue">Expected value in the master data</param>
       /// <param name="actualProductValue">Value from the product attribute</param>
        void AddProductAttributeValidationWarning(string gtin, string attributeCompared, string expectedMasterDataValue, string actualProductValue);

        /// <summary>
        /// Gets a list of products that needs to be processed.
        /// </summary>
        /// <returns>List of products <see cref="PlanogramProduct"/></returns>
        IEnumerable<PlanogramProduct> GetProductsToValidateAttributesFor();
    }
}