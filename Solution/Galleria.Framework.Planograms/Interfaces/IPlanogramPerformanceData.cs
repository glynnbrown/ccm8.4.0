﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM802
// CCM-29030 : J.Pickup
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Inteface that defines the properties
    /// that are available on ProductLibraryProductPerformanceData
    /// </summary>
    public interface IPlanogramPerformanceData
    {
        #region Properties

         Single? P1 { get; set; }
         Single? P2 { get; set; }
         Single? P3 { get; set; }
         Single? P4 { get; set; }
         Single? P5 { get; set; }
         Single? P6 { get; set; }
         Single? P7 { get; set; }
         Single? P8 { get; set; }
         Single? P9 { get; set; }
         Single? P10 { get; set; }
         Single? P11 { get; set; }
         Single? P12 { get; set; }
         Single? P13 { get; set; }
         Single? P14 { get; set; }
         Single? P15 { get; set; }
         Single? P16 { get; set; }
         Single? P17 { get; set; }
         Single? P18 { get; set; }
         Single? P19 { get; set; }
         Single? P20 { get; set; }
         Single? UnitsSoldPerDay { get; set; }
         Single? AchievedCasePacks { get; set; }
         Single? AchievedDos { get; set; }
         Single? AchievedShelfLife { get; set; }
         Single? AchievedDeliveries { get; set; } 

        #endregion
    }
}