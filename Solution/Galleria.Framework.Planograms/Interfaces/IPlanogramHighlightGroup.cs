﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.10)
// V8-28661 L.Ineson 
//  Created
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a planogram highlight group.
    /// </summary>
    public interface IPlanogramHighlightGroup
    {
        Int32 Order { get; }
        String Name { get; }
        String DisplayName { get; }
        Int32 FillColour { get; }
        PlanogramHighlightFillPatternType FillPatternType { get; }
    }

    /// <summary>
    /// Fill Pattern Type Enum
    /// </summary>
    public enum PlanogramHighlightFillPatternType
    {
        Solid = 0,
        Border = 1,
        DiagonalUp = 2,
        DiagonalDown = 3,
        Crosshatch = 4,
        Horizontal = 5,
        Vertical = 6,
        Dotted = 7
    }
}
