﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27132 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramConsumerDecisionTree
    {
        String Name { get; }
        IPlanogramConsumerDecisionTreeLevel RootLevel { get; }
        IPlanogramConsumerDecisionTreeNode RootNode { get; }
    }
}
