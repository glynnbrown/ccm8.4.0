﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26704 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM830
// V8-31550 : A.Probyn
//  Added ProductBuddies, LocationBuddies.
// V8-31551 : A.Probyn
//  Added InventoryRules.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Interfaces
{
    public interface IPlanogramAssortment
    {
        String Name { get; }
        IEnumerable<IPlanogramAssortmentProduct> Products { get; }
        IEnumerable<IPlanogramAssortmentLocalProduct> LocalProducts { get; }
        IEnumerable<IPlanogramAssortmentRegion> Regions { get; }
        IEnumerable<IPlanogramAssortmentInventoryRule> InventoryRules { get; }
        IEnumerable<IPlanogramAssortmentProductBuddy> ProductBuddies { get; }
        IEnumerable<IPlanogramAssortmentLocationBuddy> LocationBuddies { get; }
    }
}
