﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion

using System.Collections.Generic;
using System.ComponentModel;
using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Interfaces
{
    /// <summary>
    /// Denotes the expected members of an object defining
    /// planogram blocking.
    /// </summary>
    public interface IPlanogramBlocking: INotifyPropertyChanged
    {
        /// <summary>
        /// Collection of dividers
        /// </summary>
        IEnumerable<IPlanogramBlockingDivider> Dividers { get; }

        /// <summary>
        /// Collection of groups
        /// </summary>
        IEnumerable<IPlanogramBlockingGroup> Groups { get; }

        /// <summary>
        /// Collection of locations
        /// </summary>
        IEnumerable<IPlanogramBlockingLocation> Locations { get;  }

        /// <summary>
        /// Adds a new divider at the given position
        /// </summary>
        IPlanogramBlockingDivider AddNewDivider(Single x, Single y, PlanogramBlockingDividerType type);

        /// <summary>
        /// Removes the given divider
        /// </summary>
        void RemoveDivider(IPlanogramBlockingDivider divider);

    }

}
