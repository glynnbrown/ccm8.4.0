﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
namespace Galleria.Framework.Planograms.External.ApolloStructure
{
    /// <summary>
    /// Enum type names taken from Apollo book (Administraitors guid 6.1)
    /// </summary>
    public enum ApolloColourType
    {
        White = 0,
        Blue = 1,
        Green = 2,
        Cyan = 3,
        Red = 4,
        Violet = 5,
        Yellow = 6,
        Black = 7,
        LightBlue = 8,
        LightGreen = 9,
        LightCyan = 10,
        LightRed = 11,
        LightViolet = 12,
        LightYellow = 13,
        Grey = 14
    }


    public static class ApolloColourTypeHelper
    {
        public static Byte GetRed(ApolloColourType type)
        {
            switch (type)
            {
                case ApolloColourType.White: return 255;
                case ApolloColourType.Blue: return 0;
                case ApolloColourType.Green: return 0;
                case ApolloColourType.Cyan: return 0;
                case ApolloColourType.Red: return 255;
                case ApolloColourType.Violet: return 255;
                case ApolloColourType.Yellow: return 255;
                case ApolloColourType.Black: return 0;
                case ApolloColourType.LightBlue: return 128;
                case ApolloColourType.LightGreen: return 128;
                case ApolloColourType.LightCyan: return 128;
                case ApolloColourType.LightRed: return 255;
                case ApolloColourType.LightViolet: return 255;
                case ApolloColourType.LightYellow: return 255;
                case ApolloColourType.Grey: return 128;
                default: return 0;
            }
        }

        public static Byte GetGreen(ApolloColourType type)
        {
            switch (type)
            {
                case ApolloColourType.White: return 255;
                case ApolloColourType.Blue: return 0;
                case ApolloColourType.Green: return 255;
                case ApolloColourType.Cyan: return 255;
                case ApolloColourType.Red: return 0;
                case ApolloColourType.Violet: return 0;
                case ApolloColourType.Yellow: return 255;
                case ApolloColourType.Black: return 0;
                case ApolloColourType.LightBlue: return 128;
                case ApolloColourType.LightGreen: return 255;
                case ApolloColourType.LightCyan: return 255;
                case ApolloColourType.LightRed: return 128;
                case ApolloColourType.LightViolet: return 128;
                case ApolloColourType.LightYellow: return 255;
                case ApolloColourType.Grey: return 128;
                default: return 0;
            }
        }

        public static Byte GetBlue(ApolloColourType type)
        {
            switch (type)
            {
                case ApolloColourType.White: return 255;
                case ApolloColourType.Blue: return 255;
                case ApolloColourType.Green: return 0;
                case ApolloColourType.Cyan: return 255;
                case ApolloColourType.Red: return 0;
                case ApolloColourType.Violet: return 255;
                case ApolloColourType.Yellow: return 0;
                case ApolloColourType.Black: return 0;
                case ApolloColourType.LightBlue: return 255;
                case ApolloColourType.LightGreen: return 128;
                case ApolloColourType.LightCyan: return 255;
                case ApolloColourType.LightRed: return 128;
                case ApolloColourType.LightViolet: return 255;
                case ApolloColourType.LightYellow: return 128;
                case ApolloColourType.Grey: return 128;
                default: return 0;
            }
        }
    }
}
