﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.External.ApolloStructure
{
    public enum ApolloMerchMethodType
    {
        Hand = 100,
        TrayPack = 200,
        Divider = 300,
        Basket = 400,
        RectangleLog = 500,
        PyramidLog = 510,
        HalfPyramidLog = 520,
        Dump = 600,
        Case = 700,
        MultiHand = 800
    }
}
