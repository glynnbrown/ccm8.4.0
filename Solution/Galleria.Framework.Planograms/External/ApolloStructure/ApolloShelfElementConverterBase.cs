﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31547 : M.Pettit
//  Created
// V8-32375 : M.Pettit
//  Chest merch height now converts to SE_MAXMERCHDEPTH
// V8-32409 : M.Pettit
//  Peg/Slotwall/Crossbar merch depth is now exported to the SE_MAXMERCHDEPTH insead of SE_MERCHDEPTH
//  Merch depth is now correctly supported for hanging element types (auto-calc fronts is turned off if a merch depth is applied)
// V8-32388 : M.Pettit
//  Now supports Assembly component exports
// V8-32450 : M.Pettit
//  Chest improvements
// V8-32606 : M.Pettit
//  Forced value for SH_SHELFHEIGHT for shelves based on available space
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.External.ApolloStructure
{
    /// <summary>
    /// This class mages the conversion from a passed CCM sub-component to a default Apollo Shelf element. 
    /// This class is abstract must be overrriden by each Apollo shelf type. This abstract base applies default values which 
    /// are then overriden where necesary by the inherited class.
    /// </summary>
    public abstract class ApolloShelfDataElementConverterBase
    {
        #region Fields
        private PlanogramFixtureItemDto _fixtureItemDto;
        private PlanogramFixtureDto _fixtureDto;

        private PlanogramFixtureComponentDto _fixtureComponentDto;

        private PlanogramFixtureAssemblyDto _fixtureAssemblyDto;
        private PlanogramAssemblyComponentDto _assemblyComponentDto;

        private PlanogramComponentDto _componentDto;
        private PlanogramSubComponentDto _subComponentDto;

        private CustomAttributeDataDto _customAttributeDataDto;
        private ApolloDataSetSectionShelvesShelfShelfData _shelf;
        private IEnumerable<PlanogramFieldMappingDto> _fieldMappings;
        private Int32 _id = 0;
        private PlanogramSubComponentDto _backboardSubComponent; //the parent fixture's backbaord component - used for notch data
        private Byte _shelfType;
        private Boolean _isAssemblyChild = false;

        //Used for logging purposes
        private Object _planogramId;
        private List<PlanogramEventLogDto> _planogramEventLogDtoList = new List<PlanogramEventLogDto>();

        //user preferences
        private Boolean _retainSourcePlanLookAndFeel = true;
        #endregion

        #region Properties
        /// <summary>
        /// The parent FixtureItemDto of the subComponentDto to convert
        /// </summary>
        protected PlanogramFixtureItemDto FixtureItemDto
        {
            get { return _fixtureItemDto; }
            set { _fixtureItemDto = value; }
        }

        /// <summary>
        /// The parent FixtureDto of the subComponentDto to convert
        /// </summary>
        protected PlanogramFixtureDto FixtureDto
        {
            get { return _fixtureDto; }
            set { _fixtureDto = value; }
        }

        /// <summary>
        /// The parent FixtureComponentDto of the subComponentDto to convert. 
        /// If an AssemblyComponent, this will be null
        /// </summary>
        protected PlanogramFixtureComponentDto FixtureComponentDto
        {
            get { return _fixtureComponentDto; }
            set { _fixtureComponentDto = value; }
        }

        /// <summary>
        /// The parent PlanogramFixtureAssemblyDto of the componentDto to convert
        /// </summary>
        protected PlanogramFixtureAssemblyDto FixtureAssemblyDto
        {
            get { return _fixtureAssemblyDto; }
            set { _fixtureAssemblyDto = value; }
        }

        /// <summary>
        /// The parent PlanogramAssemblyComponentDto of the subComponentDto to convert
        /// </summary>
        protected PlanogramAssemblyComponentDto AssemblyComponentDto
        {
            get { return _assemblyComponentDto; }
            set { _assemblyComponentDto = value; }
        }

        /// <summary>
        /// The parent ComponentDto of the subComponentDto to convert
        /// </summary>
        protected PlanogramComponentDto ComponentDto
        {
            get { return _componentDto; }
            set { _componentDto = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        protected PlanogramSubComponentDto SubComponentDto
        {
            get { return _subComponentDto; }
            set { _subComponentDto = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        protected CustomAttributeDataDto CustomAttributeDataDto
        {
            get { return _customAttributeDataDto; }
        }

        protected IEnumerable<PlanogramFieldMappingDto> FieldMappingDtos
        {
            get { return _fieldMappings; }
        }

        /// <summary>
        /// The output Shelf Data Object
        /// </summary>
        protected ApolloDataSetSectionShelvesShelfShelfData Shelf
        {
            get { return _shelf; }
            set { _shelf = value; }
        }

        /// <summary>
        /// The id of the created shelf data item
        /// </summary>
        protected Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// The parent fixture's backbaord component - this is used to determine whether
        /// the shelf element created can snap to notch and the location of the notches
        /// </summary>
        protected PlanogramSubComponentDto BackboardSubComponent
        {
            get { return _backboardSubComponent; }
        }

        /// <summary>
        /// Determines whether the shelf element created is allowed 
        /// to snap to notch on the parent fixture
        /// </summary>
        protected Byte ShelfType
        {
            get { return _shelfType; }
        }

        /// <summary>
        /// Determines whether the shelf element created is created from a standard
        /// fixture or an assembly component
        /// </summary>
        protected Boolean IsAssemblyChild
        {
            get { return _isAssemblyChild; }
        }

        /// <summary>
        /// The id of the planogram being exported. Used for logging purposes
        /// </summary>
        protected Object PlanogramId
        {
            get { return _planogramId; }
        }

        /// <summary>
        /// The list of event logs. Any events added in this class will be added to this list
        /// </summary>
        protected List<PlanogramEventLogDto> PlanogramEventLogDtoList
        {
            get { return _planogramEventLogDtoList; }
        }

        /// <summary>
        /// Determines whether the exported plan only exports values that are directly compatible with the export type (Apollo etc)
        /// or if additional efforts should be made to retain the look and feel of the source plan. 
        /// For example if users check this, a position in a V8 planogram with right caps will ensure that the exported Apollo plan 
        /// will have 2 positions, one for the front facings and one for the right caps. If they uncheck it, since Apollo does not 
        /// support right caps, only one position will exist on the exported pan with no right cappings applied.
        /// </summary>
        protected Boolean RetainSourcePlanLookAndFeel
        {
            get { return _retainSourcePlanLookAndFeel; }
        }
        #endregion

        protected ApolloShelfDataElementConverterBase()
        {
            _shelf = new ApolloDataSetSectionShelvesShelfShelfData();

            InitialiseValues();
        }

        public void Initialise(Byte shelfType, PlanogramFixtureItemDto fixtureItemDto, PlanogramFixtureDto fixtureDto, PlanogramFixtureAssemblyDto fixtureAssemblyDto, PlanogramAssemblyComponentDto assemblyComponentDto, PlanogramFixtureComponentDto fixtureComponentDto, PlanogramComponentDto componentDto, PlanogramSubComponentDto subComponentDto, CustomAttributeDataDto customAttrData, IEnumerable<PlanogramFieldMappingDto> fieldMappings, Int32 shelfId, PlanogramSubComponentDto backboardSubComponent, Boolean retainSourcePlanLookAndFeel)
        {
            this.Id = shelfId; 
            
            _fixtureItemDto = fixtureItemDto;
            _fixtureDto = fixtureDto;
            _fixtureComponentDto = fixtureComponentDto;
            _fixtureAssemblyDto = fixtureAssemblyDto;
            _assemblyComponentDto = assemblyComponentDto;
            _componentDto = componentDto;
            _subComponentDto = subComponentDto;
            _customAttributeDataDto = customAttrData;
            _fieldMappings = fieldMappings;
            _backboardSubComponent = backboardSubComponent;
            _shelfType = shelfType;
            _isAssemblyChild = (_fixtureComponentDto == null); //is the element we are creating a child of an assembly

            _retainSourcePlanLookAndFeel = retainSourcePlanLookAndFeel;

            _shelf.SH_SHELF = this.Id.ToString();
            _shelf.SH_SHELFTYPE = this.ShelfType.ToString();
        }

        public void InitialiseLogging(Object planogramId, List<PlanogramEventLogDto> planogramEventLogDtoList)
        {
            _planogramId = planogramId;
            _planogramEventLogDtoList = planogramEventLogDtoList;
        }

        internal void LogExportWarning(String description, String content)
        {
            SpacePlanningExportHelper.CreateEventLogEntry(this.PlanogramId, this.PlanogramEventLogDtoList, description, content, PlanogramEventLogEntryType.Warning, PlanogramEventLogAffectedType.Components);
        }

        public virtual ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            ConvertDefaults();

            return null;
        }

        /// <summary>
        /// Sets all values in the Apollo Shelf object to empty values (zero or empty string)
        /// </summary>
        internal void InitialiseValues()
        {
            this.Shelf.SH_SHELFHEIGHT = "0.0";
            this.Shelf.SH_SHELFWIDTH = "0.0";
            this.Shelf.SH_SHELFDEPTH = "0.0";
            this.Shelf.SH_XPOS = "0.0";
            this.Shelf.SH_YPOS = "0.0";
            this.Shelf.SH_ZPOS = "0.0";
            this.Shelf.SH_THICKNESS = "0.0";
            this.Shelf.SH_SLOPE = "0.0";
            this.Shelf.SH_XINC = "0.0";
            this.Shelf.SH_YINC = "0.0";
            this.Shelf.SH_DIVINC = "0.0";
            this.Shelf.SH_DIVTHICK = "0.0";
            this.Shelf.SH_BRACKETINCREMENT = "0.0";
            this.Shelf.SH_BRACKETHEIGHT = "0.0";
            this.Shelf.SH_BRACKETDEPTH = "0.0";
            this.Shelf.SH_BRACKETTHICKNESS = "0.0";
            this.Shelf.SH_BRACKETSLOPE = "0.0";
            this.Shelf.SH_XPEGEND = "0.0";
            this.Shelf.SH_YPEGEND = "0.0";
            this.Shelf.SH_YPEGSTART = "0.0";
            this.Shelf.SH_XPEGSTART = "0.0";
            this.Shelf.SH_NOTCHNUMBER = "0";

            this.Shelf.SH_USERDESC1 = String.Empty;
            this.Shelf.SH_USERDESC2 = String.Empty;
            this.Shelf.SH_USERDESC3 = String.Empty;
            this.Shelf.SH_USERDESC4 = String.Empty;
            this.Shelf.SH_USERNUM1 = "0";
            this.Shelf.SH_USERNUM2 = "0";
            this.Shelf.SH_USERNUM3 = "0";

            this.Shelf.SH_USEDLINEAR = "0.0";
            this.Shelf.SH_USEDSQUARE = "0.0";
            this.Shelf.SH_USEDCUBIC = "0.0";
            this.Shelf.SH_AVAILABLELINEAR = "0.0";
            this.Shelf.SH_AVAILABLESQUARE = "0.0";
            this.Shelf.SH_AVAILABLECUBIC = "0.0";

            this.Shelf.SH_SALES = "0.0";
            this.Shelf.SH_PROFIT = "0.0";
            this.Shelf.SH_DPP = "0.0";
            this.Shelf.SH_MOVEMENT = "0.0";
            this.Shelf.SH_ROII = "0.0";
            this.Shelf.SH_INVENTORY = "0.0";
            this.Shelf.SH_CUBICTHROUGHPUT = "0.0";
            this.Shelf.SH_LOWDAYS = "0.0";
            this.Shelf.SH_LOWCASES = "0.0";
            this.Shelf.SH_LOWFACINGS = "0.0";
            this.Shelf.SH_HIDAYS = "0.0";
            this.Shelf.SH_HICASES = "0.0";
            this.Shelf.SH_HIFACINGS = "0.0";
            this.Shelf.SH_AVERAGECASES = "0.0";

            this.Shelf.SH_AVERAGEFACINGS = "1.0";
            this.Shelf.SH_NUMSKUS = "0";
            this.Shelf.SH_CAPACITY = "0";
            this.Shelf.SH_INVATRETAIL = "0.0";
            this.Shelf.SH_CASES = "0.0";
            this.Shelf.SH_FACINGS = "0.0";

            this.Shelf.SH_MERCHHEIGHT = "0.0";
            this.Shelf.SH_MERCHDEPTH = "0.0";

            this.Shelf.SH_GRILLTHICKNESS = "0.0";
            this.Shelf.SH_GRILLHEIGHT = "0.0";

            this.Shelf.SH_FITSTATUS = "0";

            this.Shelf.SH_FOREGROUND = String.Empty;
            this.Shelf.SH_EDGE = String.Empty;
            this.Shelf.SH_BACKGROUND = String.Empty;

            this.Shelf.SH_RESERVED1 = "0";  //Reserved for peg data
            this.Shelf.SH_RESERVED2 = "0";
            this.Shelf.SH_RESERVED3 = "0";
            this.Shelf.SH_RESERVED4 = "0";
            this.Shelf.SH_RESERVED5 = "0"; //Reserved for Advanced Fixture Id - needs to be the parent bounding box or its own id if not complex
            this.Shelf.SH_RESERVED6 = "0";
            this.Shelf.SH_DISPLAYNUM = "0";

            this.Shelf.SH_AUTOCALCFRONTS = "0"; 
            this.Shelf.SH_SPREADMODE = "0";
            this.Shelf.SH_MAXMERCHDEPTH = "0";

            #region X04_SHDESCX?? fields
            //_shelf.X04_SHDESCX01 = String.Empty;
            //_shelf.X04_SHDESCX02 = String.Empty;
            //_shelf.X04_SHDESCX03 = String.Empty;
            //_shelf.X04_SHDESCX04 = String.Empty;
            //_shelf.X04_SHDESCX05 = String.Empty;
            //_shelf.X04_SHDESCX06 = String.Empty;
            //_shelf.X04_SHDESCX07 = String.Empty;
            //_shelf.X04_SHDESCX08 = String.Empty;
            //_shelf.X04_SHDESCX09 = String.Empty;
            //_shelf.X04_SHDESCX10 = String.Empty;
            //_shelf.X04_SHDESCX11 = String.Empty;
            //_shelf.X04_SHDESCX12 = String.Empty;
            //_shelf.X04_SHDESCX13 = String.Empty;
            //_shelf.X04_SHDESCX14 = String.Empty;
            //_shelf.X04_SHDESCX15 = String.Empty;
            //_shelf.X04_SHDESCX16 = String.Empty;
            //_shelf.X04_SHDESCX17 = String.Empty;
            //_shelf.X04_SHDESCX18 = String.Empty;
            //_shelf.X04_SHDESCX19 = String.Empty;
            //_shelf.X04_SHDESCX20 = String.Empty;
            //_shelf.X04_SHDESCX21 = String.Empty;
            //_shelf.X04_SHDESCX22 = String.Empty;
            //_shelf.X04_SHDESCX23 = String.Empty;
            //_shelf.X04_SHDESCX24 = String.Empty;
            //_shelf.X04_SHDESCX25 = String.Empty;
            //_shelf.X04_SHDESCX26 = String.Empty;
            //_shelf.X04_SHDESCX27 = String.Empty;
            //_shelf.X04_SHDESCX28 = String.Empty;
            //_shelf.X04_SHDESCX29 = String.Empty;
            //_shelf.X04_SHDESCX30 = String.Empty;
            //_shelf.X04_SHDESCX31 = String.Empty;
            //_shelf.X04_SHDESCX32 = String.Empty;
            //_shelf.X04_SHDESCX33 = String.Empty;
            //_shelf.X04_SHDESCX34 = String.Empty;
            //_shelf.X04_SHDESCX35 = String.Empty;
            //_shelf.X04_SHDESCX36 = String.Empty;
            //_shelf.X04_SHDESCX37 = String.Empty;
            //_shelf.X04_SHDESCX38 = String.Empty;
            //_shelf.X04_SHDESCX39 = String.Empty;
            //_shelf.X04_SHDESCX40 = String.Empty;
            //_shelf.X04_SHDESCX41 = String.Empty;
            //_shelf.X04_SHDESCX42 = String.Empty;
            //_shelf.X04_SHDESCX43 = String.Empty;
            //_shelf.X04_SHDESCX44 = String.Empty;
            //_shelf.X04_SHDESCX45 = String.Empty;
            //_shelf.X04_SHDESCX46 = String.Empty;
            //_shelf.X04_SHDESCX47 = String.Empty;
            //_shelf.X04_SHDESCX48 = String.Empty;
            //_shelf.X04_SHDESCX49 = String.Empty;
            //_shelf.X04_SHDESCX50 = String.Empty;
            #endregion

            #region X04_SHMEASX?? fields
            _shelf.X04_SHMEASX01 = "0.0";
            _shelf.X04_SHMEASX02 = "0.0";
            _shelf.X04_SHMEASX03 = "0.0";
            _shelf.X04_SHMEASX04 = "0.0";
            _shelf.X04_SHMEASX05 = "0.0";
            _shelf.X04_SHMEASX06 = "0.0";
            _shelf.X04_SHMEASX07 = "0.0";
            _shelf.X04_SHMEASX08 = "0.0";
            _shelf.X04_SHMEASX09 = "0.0";
            _shelf.X04_SHMEASX10 = "0.0";
            _shelf.X04_SHMEASX11 = "0.0";
            _shelf.X04_SHMEASX12 = "0.0";
            _shelf.X04_SHMEASX13 = "0.0";
            _shelf.X04_SHMEASX14 = "0.0";
            _shelf.X04_SHMEASX15 = "0.0";
            _shelf.X04_SHMEASX16 = "0.0";
            _shelf.X04_SHMEASX17 = "0.0";
            _shelf.X04_SHMEASX18 = "0.0";
            _shelf.X04_SHMEASX19 = "0.0";
            _shelf.X04_SHMEASX20 = "0.0";
            _shelf.X04_SHMEASX21 = "0.0";
            _shelf.X04_SHMEASX22 = "0.0";
            _shelf.X04_SHMEASX23 = "0.0";
            _shelf.X04_SHMEASX24 = "0.0";
            _shelf.X04_SHMEASX25 = "0.0";
            _shelf.X04_SHMEASX26 = "0.0";
            _shelf.X04_SHMEASX27 = "0.0";
            _shelf.X04_SHMEASX28 = "0.0";
            _shelf.X04_SHMEASX29 = "0.0";
            _shelf.X04_SHMEASX30 = "0.0";
            _shelf.X04_SHMEASX31 = "0.0";
            _shelf.X04_SHMEASX32 = "0.0";
            _shelf.X04_SHMEASX33 = "0.0";
            _shelf.X04_SHMEASX34 = "0.0";
            _shelf.X04_SHMEASX35 = "0.0";
            _shelf.X04_SHMEASX36 = "0.0";
            _shelf.X04_SHMEASX37 = "0.0";
            _shelf.X04_SHMEASX38 = "0.0";
            _shelf.X04_SHMEASX39 = "0.0";
            _shelf.X04_SHMEASX40 = "0.0";
            _shelf.X04_SHMEASX41 = "0.0";
            _shelf.X04_SHMEASX42 = "0.0";
            _shelf.X04_SHMEASX43 = "0.0";
            _shelf.X04_SHMEASX44 = "0.0";
            _shelf.X04_SHMEASX45 = "0.0";
            _shelf.X04_SHMEASX46 = "0.0";
            _shelf.X04_SHMEASX47 = "0.0";
            _shelf.X04_SHMEASX48 = "0.0";
            _shelf.X04_SHMEASX49 = "0.0";
            _shelf.X04_SHMEASX50 = "0.0";
            #endregion

            #region X04_SHMEASRESERVE?? fields
            _shelf.X04_SHMEASRESERVE20 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE20, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE19 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE19, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE18 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE18, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE17 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE17, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE16 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE16, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE15 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE15, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE14 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE14, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE13 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE13, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE12 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE12, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE11 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE11, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE10 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE10, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE09 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE09, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE08 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE08, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE07 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE07, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE06 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE06, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE05 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE05, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE04 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE04, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE03 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE03, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE02 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE02, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE01 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE01, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            #endregion

            #region X04_SHDESCRESERVE?? fields
            //_shelf.X04_SHDESCRESERVE01 = String.Empty;
            //_shelf.X04_SHDESCRESERVE02 = String.Empty;
            //_shelf.X04_SHDESCRESERVE03 = String.Empty;
            //_shelf.X04_SHDESCRESERVE04 = String.Empty;
            //_shelf.X04_SHDESCRESERVE05 = String.Empty;
            //_shelf.X04_SHDESCRESERVE06 = String.Empty;
            //_shelf.X04_SHDESCRESERVE07 = String.Empty;
            //_shelf.X04_SHDESCRESERVE08 = String.Empty;
            #endregion
        }

        /// <summary>
        /// Applies default values from the passed sub-component to the ApolloDataSet Shelf object
        /// </summary>
        internal virtual void ConvertDefaults()
        {
            this.Shelf.SH_DISPLAYNUM = this.Id.ToString(); // matches the shelf id by default;

            this.Shelf.SH_SHELFHEIGHT = _subComponentDto.Height.ToString();
            this.Shelf.SH_SHELFWIDTH = _subComponentDto.Width.ToString();
            this.Shelf.SH_SHELFDEPTH = _subComponentDto.Depth.ToString();


            if (!_isAssemblyChild)
            {
                this.Shelf.SH_XPOS = (_fixtureItemDto.X + _fixtureComponentDto.X + _subComponentDto.X).ToString();
                this.Shelf.SH_YPOS = (_fixtureComponentDto.Y + _subComponentDto.Y).ToString();
                this.Shelf.SH_ZPOS = (_fixtureComponentDto.Z + _subComponentDto.Z).ToString(); 
            }
            else
            {
                this.Shelf.SH_XPOS = (_fixtureItemDto.X + _fixtureAssemblyDto.X + _assemblyComponentDto.X + _subComponentDto.X).ToString();
                this.Shelf.SH_YPOS = (_fixtureAssemblyDto.Y + _assemblyComponentDto.Y + _subComponentDto.Y).ToString();
                this.Shelf.SH_ZPOS = (_fixtureAssemblyDto.Z + _assemblyComponentDto.Z + _subComponentDto.Z).ToString();
            }

            //Merch hieght seems to be optional
            this.Shelf.SH_MERCHHEIGHT = _subComponentDto.MerchandisableHeight.ToString();
            //Merch depth seems to always be populated
            this.Shelf.SH_MERCHDEPTH = (_subComponentDto.MerchandisableDepth > 0 ? _subComponentDto.MerchandisableDepth : _subComponentDto.Depth).ToString();

            //Apollo Slopes only allowed to be +89 -> -89
            Single slope = 0;
            Single ccmSlope = 0;
            if (!_isAssemblyChild)
            {
                ccmSlope = ApolloExportHelper.ConvertRadiansToDegrees(_subComponentDto.Slope + _fixtureComponentDto.Slope);
            }
            else
            {
                ccmSlope = ApolloExportHelper.ConvertRadiansToDegrees(_subComponentDto.Slope + _fixtureAssemblyDto.Slope + _assemblyComponentDto.Slope);
            }
            if (ccmSlope >= 90 && ccmSlope <= 270)
            {
                //slope is not compatible with Apollo so mirror value around y-axis and inform user
                if (ccmSlope >= 180 && ccmSlope <= 270) slope = (0 - (ccmSlope - 180));
                if (ccmSlope >= 90 && ccmSlope < 180) slope = (90 - (ccmSlope - 90));
                slope = (slope < 90) ? slope : 89;
                slope = (slope < -90) ? slope : -89;
                LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_ShelfSlope_Description, String.Format(Message.ApolloExportLog_MerchandisingWarning_ShelfSlope_Invalid_Content, this.ComponentDto.Name));
            }
            if (ccmSlope > 270) slope = ccmSlope - 360;

            if (ccmSlope < 90) slope = ccmSlope;
            this.Shelf.SH_SLOPE = slope.ToString();


            //Check for invalid shelf angles - they are only allowed for hanging bars
            if (this.ShelfType != (Byte)ApolloShelfType.HangingBar)
            {
                if (this.SubComponentDto.Angle != 0)
                {
                    LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_ShelfAngle_Description, String.Format(Message.ApolloExportLog_MerchandisingWarning_ShelfAngle_Invalid_Content, this.ComponentDto.Name));
                }
            }
            
            this.Shelf.SH_RESERVED2 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentRESERVED2, _componentDto, _customAttributeDataDto, _fieldMappings, "0");
            this.Shelf.SH_RESERVED3 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentRESERVED3, _componentDto, _customAttributeDataDto, _fieldMappings, "0");
            this.Shelf.SH_RESERVED4 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentRESERVED4, _componentDto, _customAttributeDataDto, _fieldMappings, this.Id.ToString());
            this.Shelf.SH_RESERVED5 = this.Id.ToString(); // or should it be _shelf.SH_SHELF; //Reserved for Advanced Fixture Id
            
            this.Shelf.SH_USERDESC1 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentUSERDESC1, _componentDto, _customAttributeDataDto, _fieldMappings, "0");
            this.Shelf.SH_USERDESC2 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentUSERDESC2, _componentDto, _customAttributeDataDto, _fieldMappings, "0");
            this.Shelf.SH_USERDESC3 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentUSERDESC3, _componentDto, _customAttributeDataDto, _fieldMappings, "0");
            this.Shelf.SH_USERDESC4 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentUSERDESC4, _componentDto, _customAttributeDataDto, _fieldMappings, "0");
            this.Shelf.SH_USERNUM1 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentUSERNUM1, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            this.Shelf.SH_USERNUM2 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentUSERNUM2, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            this.Shelf.SH_USERNUM3 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentUSERNUM3, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");

            #region Notch information
            Boolean snapShelf = false;
            //Test Item Collisions off, all others on = 64
            //SnapToLeftUpgright off only = 2
            //SnapToRightUpright off only = 4
            //SnapToNotch off only = 32
            const Int32 snapToLeftUpgrightBitValue = 2;
            const Int32 snapToRightUpgrightBitValue = 4;
            const Int32 autoCalcFrontsBitValue = 24;
            const Int32 snapToNotchBitValue = 32;
            const Int32 textItemCollisionsBitValue = 64;
            //initially turn off just these values
            Int32 notchWordValue = snapToLeftUpgrightBitValue + snapToRightUpgrightBitValue + textItemCollisionsBitValue;

            //Apollo Notch numbers are measured to the bottom of the component - v8 is to the top
            Int32 notchNumber = 0;
            if (!_isAssemblyChild)
            {
                notchNumber = (Int32)(_fixtureComponentDto.NotchNumber != null ? _fixtureComponentDto.NotchNumber : 0);
            }
            else
            {
                notchNumber = (Int32)(_assemblyComponentDto.NotchNumber != null ? _assemblyComponentDto.NotchNumber : 0);
            }
            Boolean bottomAdjustedNotchIsBelowFirstNotch = false;
            if (notchNumber > 0 && _backboardSubComponent != null && _subComponentDto.Height > _backboardSubComponent.NotchSpacingY)
            {
                //get y position of notch
                Single notchY = _backboardSubComponent.NotchStartY + (_backboardSubComponent.NotchSpacingY * (notchNumber - 1));
                //Find how many notches between bottom of component and top
                Int32 notchOffset =  (Int32)Math.Round(_subComponentDto.Height / _backboardSubComponent.NotchSpacingY, 0, MidpointRounding.AwayFromZero);
                notchNumber -= notchOffset;
                bottomAdjustedNotchIsBelowFirstNotch = notchNumber < 0;
                notchNumber = bottomAdjustedNotchIsBelowFirstNotch ? 0 : notchNumber;

                snapShelf = _backboardSubComponent.IsNotchPlacedOnFront && !bottomAdjustedNotchIsBelowFirstNotch && notchNumber > 0;
            }
            //Set the notch value for normal components
            this.Shelf.SH_NOTCHNUMBER = ApolloExportHelper.ParseNumericValue(notchNumber, false, "0");

            //if:
            //1. the parent backboard has notches placed on front and this subcomponent has a notch value, we should snap it 
            //2. BUT if the user wants to retain the source plan element locations, we should not snap it :)
            if (this.RetainSourcePlanLookAndFeel)
            {
                //The exported planogram shelf elements should be in the same location as the source planogram
                //so turn off the notch snapping
                notchWordValue += snapToNotchBitValue;
                if (snapShelf)
                {
                    //log to inform user that shelf snapping is ignored as this may move elements unexpectedly
                    LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_SnapToNotch_Description, 
                        String.Format(Message.ApolloExportLog_MerchandisingWarning_SnapToNotch_SnapOff_Content, this.ComponentDto.Name));
                }
            }
            else
            {
                if (!snapShelf)
                {
                    //backboard is not set to snap to notch so turn off
                    notchWordValue += snapToNotchBitValue;
                }
            }
            //For some hanging types, turn off autocalc fronts to preserve the correct number of facings
            //Don't know which ones yet :)
            if (this.ShelfType == (Byte)ApolloShelfType.HangingBar && 
                this.SubComponentDto.MerchandisableDepth > 0 &&
                this.SubComponentDto.MerchandisableDepth < this.SubComponentDto.Depth)
            {
                //also turn off autoCalcFronts to retain correct facings
                notchWordValue = notchWordValue + autoCalcFrontsBitValue;
            }
            else if (this.ShelfType == (Byte)ApolloShelfType.Coffin)
            {
                //also turn off autoCalcFronts to retain correct facings
                notchWordValue = notchWordValue + autoCalcFrontsBitValue;
            }

            this.Shelf.SH_RESERVED6 = notchWordValue.ToString();
            #endregion

            #region X04_SHDESCX?? fields
            //_shelf.X04_SHDESCX01 = String.Empty;
            //_shelf.X04_SHDESCX02 = String.Empty;
            //_shelf.X04_SHDESCX03 = String.Empty;
            //_shelf.X04_SHDESCX04 = String.Empty;
            //_shelf.X04_SHDESCX05 = String.Empty;
            //_shelf.X04_SHDESCX06 = String.Empty;
            //_shelf.X04_SHDESCX07 = String.Empty;
            //_shelf.X04_SHDESCX08 = String.Empty;
            //_shelf.X04_SHDESCX09 = String.Empty;
            //_shelf.X04_SHDESCX10 = String.Empty;
            //_shelf.X04_SHDESCX11 = String.Empty;
            //_shelf.X04_SHDESCX12 = String.Empty;
            //_shelf.X04_SHDESCX13 = String.Empty;
            //_shelf.X04_SHDESCX14 = String.Empty;
            //_shelf.X04_SHDESCX15 = String.Empty;
            //_shelf.X04_SHDESCX16 = String.Empty;
            //_shelf.X04_SHDESCX17 = String.Empty;
            //_shelf.X04_SHDESCX18 = String.Empty;
            //_shelf.X04_SHDESCX19 = String.Empty;
            //_shelf.X04_SHDESCX20 = String.Empty;
            //_shelf.X04_SHDESCX21 = String.Empty;
            //_shelf.X04_SHDESCX22 = String.Empty;
            //_shelf.X04_SHDESCX23 = String.Empty;
            //_shelf.X04_SHDESCX24 = String.Empty;
            //_shelf.X04_SHDESCX25 = String.Empty;
            //_shelf.X04_SHDESCX26 = String.Empty;
            //_shelf.X04_SHDESCX27 = String.Empty;
            //_shelf.X04_SHDESCX28 = String.Empty;
            //_shelf.X04_SHDESCX29 = String.Empty;
            //_shelf.X04_SHDESCX30 = String.Empty;
            //_shelf.X04_SHDESCX31 = String.Empty;
            //_shelf.X04_SHDESCX32 = String.Empty;
            //_shelf.X04_SHDESCX33 = String.Empty;
            //_shelf.X04_SHDESCX34 = String.Empty;
            //_shelf.X04_SHDESCX35 = String.Empty;
            //_shelf.X04_SHDESCX36 = String.Empty;
            //_shelf.X04_SHDESCX37 = String.Empty;
            //_shelf.X04_SHDESCX38 = String.Empty;
            //_shelf.X04_SHDESCX39 = String.Empty;
            //_shelf.X04_SHDESCX40 = String.Empty;
            //_shelf.X04_SHDESCX41 = String.Empty;
            //_shelf.X04_SHDESCX42 = String.Empty;
            //_shelf.X04_SHDESCX43 = String.Empty;
            //_shelf.X04_SHDESCX44 = String.Empty;
            //_shelf.X04_SHDESCX45 = String.Empty;
            //_shelf.X04_SHDESCX46 = String.Empty;
            //_shelf.X04_SHDESCX47 = String.Empty;
            //_shelf.X04_SHDESCX48 = String.Empty;
            //_shelf.X04_SHDESCX49 = String.Empty;
            //_shelf.X04_SHDESCX50 = String.Empty;
            #endregion

            #region X04_SHMEASX?? fields
            _shelf.X04_SHMEASX01 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX01, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX02 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX02, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX03 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX03, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX04 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX04, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX05 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX05, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX06 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX06, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX07 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX07, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX08 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX08, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX09 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX09, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX10 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX10, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX11 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX11, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX12 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX12, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX13 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX13, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX14 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX14, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX15 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX15, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX16 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX16, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX17 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX17, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX18 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX18, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX19 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX19, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX20 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX20, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX21 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX21, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX22 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX22, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX23 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX23, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX24 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX24, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX25 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX25, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX26 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX26, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX27 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX27, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX28 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX28, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX29 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX29, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX30 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX30, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX31 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX31, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX32 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX32, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX33 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX33, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX34 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX34, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX35 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX35, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX36 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX36, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX37 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX37, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX38 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX38, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX39 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX39, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX40 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX40, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX41 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX41, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX42 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX42, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX43 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX43, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX44 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX44, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX45 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX45, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX46 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX46, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX47 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX47, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX48 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX48, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX49 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX49, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASX50 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASX50, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            #endregion

            #region X04_SHMEASRESERVE?? fields
            _shelf.X04_SHMEASRESERVE20 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE20, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE19 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE19, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE18 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE18, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE17 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE17, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE16 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE16, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE15 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE15, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE14 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE14, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE13 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE13, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE12 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE12, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE11 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE11, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE10 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE10, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE09 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE09, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE08 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE08, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE07 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE07, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE06 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE06, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE05 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE05, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE04 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE04, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE03 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE03, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE02 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE02, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0");
            _shelf.X04_SHMEASRESERVE01 = ApolloExportHelper.GetFieldMappedValue<PlanogramComponentDto>(ApolloExportHelper.ComponentX04_SHMEASRESERVE01, _componentDto, _customAttributeDataDto, _fieldMappings, "0.0"); 
            #endregion

            #region X04_SHDESCRESERVE?? fields
            //_shelf.X04_SHDESCRESERVE01 = String.Empty;
            //_shelf.X04_SHDESCRESERVE02 = String.Empty;
            //_shelf.X04_SHDESCRESERVE03 = String.Empty;
            //_shelf.X04_SHDESCRESERVE04 = String.Empty;
            //_shelf.X04_SHDESCRESERVE05 = String.Empty;
            //_shelf.X04_SHDESCRESERVE06 = String.Empty;
            //_shelf.X04_SHDESCRESERVE07 = String.Empty;
            //_shelf.X04_SHDESCRESERVE08 = String.Empty;
            #endregion
        }
    }

    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for Shelf components
    /// </summary>
    public class ApolloShelfDataElementConverterShelf : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();
                        
            //Implement shelf-specific overrides here
            Single yPos = 0;
            if (!this.IsAssemblyChild)
            {
                yPos = this.FixtureComponentDto.Y + this.SubComponentDto.Y + this.SubComponentDto.Height;
            }
            else
            {
                yPos = this.FixtureAssemblyDto.Y + this.AssemblyComponentDto.Y + this.SubComponentDto.Y + this.SubComponentDto.Height;
            }
            this.Shelf.SH_YPOS = yPos.ToString();

            Boolean merchandisingStrategyXSupported = false;
            this.Shelf.SH_SPREADMODE = ((Byte)ApolloExportHelper.GetApolloShelfSpreadMode(this.SubComponentDto, this.RetainSourcePlanLookAndFeel, this.ShelfType, out merchandisingStrategyXSupported)).ToString();
            
            #region Log SpreadMode
            //Notes: Shelves that contain left or right caps require extra positions to be added as Apollo does
            //not support left/right caps for positions. If the Merch strategey is Even this will result in a gap existing between
            //the main block position and the left or right cap. So if we are in RetainSOurcePlanPresentation mode, we should switch
            //to manual Spreadmode
            if (this.RetainSourcePlanLookAndFeel)
            {
                if (!merchandisingStrategyXSupported)
                {
                    //log to inform user that merch strategy ignored to retain look and feel
                    base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                        String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyX_RetainLookAndFeelContent, this.ComponentDto.Name));
                }
            }
            else 
            if (!merchandisingStrategyXSupported)
            {
                //log to inform user that merch strategy was not supported
                base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyX_NotSupportedContent, this.ComponentDto.Name));
            }
            #endregion

            //Shelf Height is meaqsured as the distance from the shelf to the next shelf
            if (this.SubComponentDto.MerchandisableHeight > 0)
            {
                this.Shelf.SH_SHELFHEIGHT = this.SubComponentDto.MerchandisableHeight.ToString();
            }
            else
            {
                Single distanceShelfToTopOfFixture = this.FixtureDto.Height;
                if (this.IsAssemblyChild)
                {
                    distanceShelfToTopOfFixture -= this.FixtureAssemblyDto.Y + this.AssemblyComponentDto.Y + this.SubComponentDto.Y;
                }
                else
                {
                    distanceShelfToTopOfFixture = this.FixtureComponentDto.Y + this.SubComponentDto.Y;
                }
                distanceShelfToTopOfFixture -= this.SubComponentDto.Height;
                this.Shelf.SH_SHELFHEIGHT = distanceShelfToTopOfFixture.ToString();
            }
            
            this.Shelf.SH_THICKNESS = this.SubComponentDto.Height.ToString();

            if (this.SubComponentDto.IsRiserPlacedOnFront && this.SubComponentDto.RiserHeight > 0)
            {
                this.Shelf.SH_GRILLTHICKNESS = this.SubComponentDto.RiserThickness.ToString();
                this.Shelf.SH_GRILLHEIGHT = this.SubComponentDto.RiserHeight.ToString();
            }
            
            this.Shelf.SH_DIVINC = this.SubComponentDto.DividerObstructionStartX.ToString();
            this.Shelf.SH_DIVTHICK = this.SubComponentDto.DividerObstructionWidth.ToString();

            //Log issues here:

            //Inform user that left/back/right risers are not suported in Apollo
            if ((this.SubComponentDto.IsRiserPlacedOnBack || this.SubComponentDto.IsRiserPlacedOnLeft || this.SubComponentDto.IsRiserPlacedOnRight) && this.SubComponentDto.RiserHeight > 0)
            {
                LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Riser_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_Riser_Invalid_Content, this.ComponentDto.Name));
            }

            if (this.SubComponentDto.MerchandisingStrategyZ != (Byte)PlanogramSubComponentZMerchStrategyType.Front)
            {
                // Apollo only supports front merchandsed positions on shelves
                LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyZ_NotSupportedContent, this.ComponentDto.Name));
            }
            
            return this.Shelf;
        }
    }


    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for FreeHand Surface components
    /// </summary>
    public class ApolloShelfDataElementConverterFreeHandSurface : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();

            //Implement shelf-specific overrides here

            Single yPos = 0;
            if (!this.IsAssemblyChild)
            {
                yPos = this.FixtureComponentDto.Y + this.SubComponentDto.Y + this.SubComponentDto.Height;
            }
            else
            {
                yPos = this.FixtureAssemblyDto.Y + this.AssemblyComponentDto.Y + this.SubComponentDto.Y + this.SubComponentDto.Height;
            }
            this.Shelf.SH_YPOS = yPos.ToString();

            this.Shelf.SH_SHELFHEIGHT = this.SubComponentDto.MerchandisableHeight.ToString();
            this.Shelf.SH_THICKNESS = "0.0";

            if (this.SubComponentDto.IsRiserPlacedOnFront && this.SubComponentDto.RiserHeight > 0)
            {
                this.Shelf.SH_GRILLTHICKNESS = this.SubComponentDto.RiserThickness.ToString();
                this.Shelf.SH_GRILLHEIGHT = this.SubComponentDto.RiserHeight.ToString();
            }

            this.Shelf.SH_DIVINC = this.SubComponentDto.DividerObstructionStartX.ToString();
            this.Shelf.SH_DIVTHICK = this.SubComponentDto.DividerObstructionWidth.ToString();

            return this.Shelf;
        }
    }


    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for Peg components
    /// </summary>
    public class ApolloShelfDataElementConverterPeg : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();

            //Implement peg-specific overrides here
            this.Shelf.SH_THICKNESS = this.SubComponentDto.Depth.ToString();
            this.Shelf.SH_SHELFDEPTH = this.FixtureDto.Depth.ToString();
            this.Shelf.SH_XPEGSTART = this.SubComponentDto.MerchConstraintRow1StartX.ToString();
            this.Shelf.SH_YPEGEND = this.SubComponentDto.MerchConstraintRow1StartY.ToString();
            this.Shelf.SH_XINC = this.SubComponentDto.MerchConstraintRow1SpacingX.ToString();
            this.Shelf.SH_DIVINC = this.SubComponentDto.MerchConstraintRow1SpacingX.ToString();
            this.Shelf.SH_YINC = this.SubComponentDto.MerchConstraintRow1SpacingY.ToString();
            Single merchDepth = this.SubComponentDto.MerchandisableDepth > 0 ? this.SubComponentDto.MerchandisableDepth : this.FixtureDto.Depth;
            this.Shelf.SH_MAXMERCHDEPTH = merchDepth.ToString();
            
            return this.Shelf;
        }
    }


    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for Coffin components
    /// </summary>
    public class ApolloShelfDataElementConverterCoffin : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();

            //Implement chest-specific overrides here

            //Note: Apollo doe not draw chests correctly. Positions in chests are rendered thorugh any base thickness,
            //resulting in more products being placed high than are allowed in V8. For now, turn off the chest wall
            //thickness and resize the chest to be the size of the inner walls, with a thickness of zero.
            #region Alternate Code - allowed chests to have thickness
            //this.Shelf.SH_SHELFHEIGHT = this.SubComponentDto.Depth.ToString();
            //this.Shelf.SH_SHELFDEPTH = this.SubComponentDto.Height.ToString();

            //Apollo only has 1 thickness value for all faces. Only apply the thickness to the coffin if all ccm face values are the same,
            //otherwise we nmay have collisions
            //if (this.SubComponentDto.FaceThicknessBottom == this.SubComponentDto.FaceThicknessFront &&
            //    this.SubComponentDto.FaceThicknessBottom == this.SubComponentDto.FaceThicknessBack &&
            //    this.SubComponentDto.FaceThicknessBottom == this.SubComponentDto.FaceThicknessLeft &&
            //    this.SubComponentDto.FaceThicknessBottom == this.SubComponentDto.FaceThicknessRight)
            //{
            //    this.Shelf.SH_THICKNESS = this.SubComponentDto.FaceThicknessBottom.ToString();
            //}
            //else
            //{
            //    this.Shelf.SH_THICKNESS = "0.0";
            //}
            #endregion

            //Apollo does not get position y positions correct inside chests, so set all chest wall thicknesses to 0 and adjust its dimensions accordingly
            this.Shelf.SH_SHELFHEIGHT = (this.SubComponentDto.Depth - this.SubComponentDto.FaceThicknessBack - this.SubComponentDto.FaceThicknessFront).ToString();
            this.Shelf.SH_SHELFWIDTH = (this.SubComponentDto.Width - this.SubComponentDto.FaceThicknessLeft - this.SubComponentDto.FaceThicknessRight).ToString();
            this.Shelf.SH_SHELFDEPTH = (this.SubComponentDto.Height - this.SubComponentDto.FaceThicknessBottom - this.SubComponentDto.FaceThicknessTop).ToString();

            if (!this.IsAssemblyChild)
            {
                this.Shelf.SH_XPOS = (this.FixtureItemDto.X + this.FixtureComponentDto.X + this.SubComponentDto.X + this.SubComponentDto.FaceThicknessLeft).ToString();
                this.Shelf.SH_YPOS = (this.FixtureComponentDto.Y + this.SubComponentDto.Y + this.SubComponentDto.FaceThicknessBottom).ToString();
                this.Shelf.SH_ZPOS = (this.FixtureComponentDto.Z + this.SubComponentDto.Z + this.SubComponentDto.FaceThicknessBack).ToString();
            }
            else
            {
                this.Shelf.SH_XPOS = (this.FixtureItemDto.X + this.FixtureAssemblyDto.X + this.AssemblyComponentDto.X + this.SubComponentDto.X + this.SubComponentDto.FaceThicknessLeft).ToString();
                this.Shelf.SH_YPOS = (this.FixtureAssemblyDto.Y + this.AssemblyComponentDto.Y + this.SubComponentDto.Y + this.SubComponentDto.FaceThicknessBottom).ToString();
                this.Shelf.SH_ZPOS = (this.FixtureAssemblyDto.Z + this.AssemblyComponentDto.Z + this.SubComponentDto.Z + this.SubComponentDto.FaceThicknessBack).ToString();
            }
           
            this.Shelf.SH_THICKNESS = "0.0";
            this.Shelf.SH_MAXMERCHDEPTH = this.SubComponentDto.MerchandisableHeight.ToString();

            #region Logging
            //log to inform user that chest thcknesses are rendered incorrectly in Apollo (positions are allowed to sit through the chest base) and will be ignored
            if (this.SubComponentDto.FaceThicknessBottom > 0 ||
                this.SubComponentDto.FaceThicknessBack > 0 || this.SubComponentDto.FaceThicknessFront > 0 ||
                this.SubComponentDto.FaceThicknessLeft > 0 || this.SubComponentDto.FaceThicknessRight > 0)
            {
                base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Coffin_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_CoffinThickness_Content, this.ComponentDto.Name));
            }

            //if subcomponent has a merch strategy and it is not a shelf, it will be ignored.
            //Shelf strategies are not supported by Apollo for non-shelf components
            if (this.SubComponentDto.MerchandisingStrategyX != (Byte)PlanogramSubComponentXMerchStrategyType.Manual)
            {
                //log to inform user that merch strategy not supported
                base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyX_NotSuported_Content, this.ComponentDto.Name));
            }

            //V8 supports positions sticking out tof the top of a chest. Apollo - guess What.. Doesn't.
            if (this.SubComponentDto.MerchandisableHeight > this.SubComponentDto.Height)
            {
                //log to inform user that merch strategy not supported
                base.LogExportWarning(Message.ApolloExportLog_ChestMerchandisingHeightNotSupported__Description,
                    String.Format(Message.ApolloExportLog_ChestMerchandisingHeightNotSupported_Content, this.ComponentDto.Name));
            }
            #endregion

            return this.Shelf;
        }
    }


    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for Hanging Bar components
    /// </summary>
    public class ApolloShelfDataElementConverterHangingBar : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();

            //Implement Hanging bar-specific overrides here
            this.Shelf.SH_SHELFWIDTH = this.SubComponentDto.Depth.ToString();
            this.Shelf.SH_SHELFDEPTH = this.SubComponentDto.Width.ToString();
            this.Shelf.SH_THICKNESS = this.SubComponentDto.Height.ToString();


            //Angle only valid for hanging bars
            Single angle = 0;
            if (!this.IsAssemblyChild)
            {
                angle = ApolloExportHelper.ConvertRadiansToDegrees(this.FixtureItemDto.Angle + this.FixtureComponentDto.Angle + this.SubComponentDto.Angle) + 90;
            }
            else
            {
                angle = ApolloExportHelper.ConvertRadiansToDegrees(this.FixtureAssemblyDto.Angle + this.AssemblyComponentDto.Angle + this.FixtureComponentDto.Angle + this.SubComponentDto.Angle) + 90;
            }

            while (angle >= 360) angle = angle - 360;
            this.Shelf.SH_RESERVED1 = angle.ToString(); //shelf angle

            this.Shelf.SH_XPEGSTART = this.SubComponentDto.MerchConstraintRow1StartX.ToString();
            this.Shelf.SH_YPEGSTART = this.SubComponentDto.MerchConstraintRow1StartY.ToString();
            this.Shelf.SH_XINC = this.SubComponentDto.MerchConstraintRow1SpacingX.ToString();
            this.Shelf.SH_YINC = this.SubComponentDto.MerchConstraintRow1SpacingY.ToString();
            Single merchDepth = this.SubComponentDto.MerchandisableDepth > 0 ? this.SubComponentDto.MerchandisableDepth : this.FixtureDto.Depth;
            this.Shelf.SH_MAXMERCHDEPTH = merchDepth.ToString();


            this.Shelf.SH_XPEGEND = "0.1";  //Knob offset Left
            this.Shelf.SH_XPEGSTART = "0.1"; //Knob offset Right

            //if subcomponent has a merch strategy and it is not a shelf, it will be ignored.
            //Shelf strategies are not supported by Apollo for non-shelf components
            if (this.SubComponentDto.MerchandisingStrategyX != (Byte)PlanogramSubComponentXMerchStrategyType.Manual)
            {
                //log to inform user that merch strategy not supported
                base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyX_NotSuported_Content, this.ComponentDto.Name));
            }

            return this.Shelf;
        }
    }


    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for Crossbar components
    /// </summary>
    public class ApolloShelfDataElementConverterCrossbar : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();

            //Implement crossbar-specific overrides here
            this.Shelf.SH_THICKNESS = this.SubComponentDto.Depth.ToString();
            this.Shelf.SH_SHELFDEPTH = this.FixtureDto.Depth.ToString();
            this.Shelf.SH_XPEGSTART = this.SubComponentDto.MerchConstraintRow1StartX.ToString();
            this.Shelf.SH_YPEGEND = this.SubComponentDto.MerchConstraintRow1StartY.ToString();
            this.Shelf.SH_XINC = this.SubComponentDto.MerchConstraintRow1SpacingX.ToString();
            this.Shelf.SH_YINC = (this.SubComponentDto.Height / 2).ToString();
            Single merchDepth = this.SubComponentDto.MerchandisableDepth > 0 ? this.SubComponentDto.MerchandisableDepth : this.FixtureDto.Depth;
            this.Shelf.SH_MAXMERCHDEPTH = merchDepth.ToString();

            //if subcomponent has a merch strategy and it is not a shelf, it will be ignored.
            //Shelf strategies are not supported by Apollo for non-shelf components
            if (this.SubComponentDto.MerchandisingStrategyX != (Byte)PlanogramSubComponentXMerchStrategyType.Manual)
            {
                //log to inform user that merch strategy not supported
                base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyX_NotSuported_Content, this.ComponentDto.Name));
            }

            return this.Shelf;
        }
    }


    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for Slotwall components
    /// </summary>
    public class ApolloShelfDataElementConverterSlotwall : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();

            //Implement slotwall-specific overrides here
            this.Shelf.SH_THICKNESS = this.SubComponentDto.Depth.ToString();
            this.Shelf.SH_SHELFDEPTH = this.FixtureDto.Depth.ToString();
            this.Shelf.SH_YPEGEND = this.SubComponentDto.MerchConstraintRow1StartY.ToString();
            this.Shelf.SH_YINC = this.SubComponentDto.MerchConstraintRow1SpacingY.ToString();
            Single merchDepth = this.SubComponentDto.MerchandisableDepth > 0 ? this.SubComponentDto.MerchandisableDepth : this.FixtureDto.Depth;
            this.Shelf.SH_MAXMERCHDEPTH = merchDepth.ToString();

            //if subcomponent has a merch strategy and it is not a shelf, it will be ignored.
            //Shelf strategies are not supported by Apollo for non-shelf components
            if (this.SubComponentDto.MerchandisingStrategyX != (Byte)PlanogramSubComponentXMerchStrategyType.Manual)
            {
                //log to inform user that merch strategy not supported
                base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyX_NotSuported_Content, this.ComponentDto.Name));
            }
                    
            return this.Shelf;
        }
    }


    /// <summary>
    /// Implementation of a ApolloShelfDataElementConverterBase for Box components
    /// </summary>
    public class ApolloShelfDataElementConverterBox : ApolloShelfDataElementConverterBase
    {
        public override ApolloDataSetSectionShelvesShelfShelfData Convert()
        {
            base.Convert();

            //Implement box-specific overrides here

            //if subcomponent has a merch strategy and it is not a shelf, it will be ignored.
            //Shelf strategies are not supported by Apollo for non-shelf components
            if (this.SubComponentDto.MerchandisingStrategyX != (Byte)PlanogramSubComponentXMerchStrategyType.Manual)
            {
                //log to inform user that merch strategy not supported
                base.LogExportWarning(Message.ApolloExportLog_MerchandisingWarning_Strategy_Description,
                    String.Format(Message.ApolloExportLog_MerchandisingWarning_StrategyX_NotSuported_Content, this.ComponentDto.Name));
            }

            return this.Shelf;
        }

        public ApolloDataSetSectionShelvesShelfShelfData CreateBoundingBoxData(List<ApolloDataSetSectionShelvesShelf> elementsToContain, Int32 shelfId)
        {
            Single boxMinX = Single.MaxValue;
            Single boxMinY = Single.MaxValue;
            Single boxMinZ = Single.MaxValue;
            Single boxMaxX = 0;
            Single boxMaxY = 0;
            Single boxMaxZ = 0;

            //Load empty values
            base.InitialiseValues();

            //Calculate dimensions
            foreach (ApolloDataSetSectionShelvesShelf element in elementsToContain)
            {
                Boolean convertSuccess = true;

                Single minX = (Single)SpacePlanningImportHelper.ParseSingle(element.ShelfData[0].SH_XPOS, false, out convertSuccess);
                Single minY = (Single)SpacePlanningImportHelper.ParseSingle(element.ShelfData[0].SH_YPOS, false, out convertSuccess);
                Single minZ = (Single)SpacePlanningImportHelper.ParseSingle(element.ShelfData[0].SH_ZPOS, false, out convertSuccess);
                Single height = (Single)SpacePlanningImportHelper.ParseSingle(element.ShelfData[0].SH_SHELFHEIGHT, false, out convertSuccess);
                Single width = (Single)SpacePlanningImportHelper.ParseSingle(element.ShelfData[0].SH_SHELFWIDTH, false, out convertSuccess);
                Single depth = (Single)SpacePlanningImportHelper.ParseSingle(element.ShelfData[0].SH_SHELFDEPTH, false, out convertSuccess);
                Single maxX = minX + width;
                Single maxY = minY + height;
                Single maxZ = minZ + depth;

                boxMinX = Math.Min(boxMinX, minX);
                boxMinY = Math.Min(boxMinY, minY);
                boxMinZ = Math.Min(boxMinZ, minZ);

                boxMaxX = Math.Max(boxMaxX, maxX);
                boxMaxY = Math.Max(boxMaxY, maxY);
                boxMaxZ = Math.Max(boxMaxZ, maxZ);
            }

            //populate data element
            this.Shelf.SH_XPOS = boxMinX.ToString();
            this.Shelf.SH_YPOS = boxMinY.ToString();
            this.Shelf.SH_ZPOS = boxMinZ.ToString();
            this.Shelf.SH_SHELFHEIGHT = (boxMaxY - boxMinY).ToString();
            this.Shelf.SH_SHELFWIDTH = (boxMaxX - boxMinX).ToString();
            this.Shelf.SH_SHELFDEPTH = (boxMaxZ - boxMinZ).ToString();

            this.Shelf.SH_SHELFTYPE = "10";
            this.Shelf.SH_SHELF = shelfId.ToString();
            this.Shelf.SH_RESERVED5 = shelfId.ToString();
            
            return this.Shelf;
        }
    }
}
