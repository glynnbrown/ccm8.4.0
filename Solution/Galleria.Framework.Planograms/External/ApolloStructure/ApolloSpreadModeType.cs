﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32283 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.External.ApolloStructure
{
    //The 'Crunch' modes supported by Apollo elements
    //May be overriden by the Section Crunch Mode
    public enum ApolloSpreadModeType : byte
    {
        Right = 1,
        Left = 2,

        Even = 4,
        Manual = 5
    }
}
