﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.External.ApolloStructure
{
    /// <summary>
    /// An appolo shelf is not always a shelf!
    /// Enum type names taken from Apollo book (Administraitors guid 6.1)
    /// </summary>
    public enum ApolloShelfType : byte
    {
        Shelf = 0,
        Pegboard = 1,
        Coffin = 2,
        Sltowall = 3, //Slot wall (typo in book)
        Crossbar = 4,

        Box = 6, //Added
        HangingBar = 7, //Added 
        /// <summary>
        /// Possibly an invisible merchandising area
        /// </summary>
        FreeHandSurface = 8,//Added
        BoundingBox = 10, //Added, not sure
        Basket = 14, //Added
    }
}
