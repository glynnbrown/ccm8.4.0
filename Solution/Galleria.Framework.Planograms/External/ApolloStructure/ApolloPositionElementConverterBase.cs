﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31547 : M.Pettit
//  Created
// V8-32375 : M.pettit
//  Re-worked merch Style to take from product if position set to default
// V8-32375 : M.Pettit
//  Orientations now take account of position defaults using product values
// V8-32409 : M.Pettit
//  Peg/Slotwall/Crossbar position peg lengths are now set to the pegdepth of the parent source product.
//  Improvements made to hanging bar positions
// V8-32388 : M.Pettit
//  Now supports Assembly component exports
// V8-32448 : M.Pettit
//  Left/Right caps now determine their own merch style (hand/tray etc)
// V8-32456 : M.Pettit
//  For slotwalls and pegboards, Apollo facings wide = facings wide * facings high. Obviously.
// V8-32448 : M.Pettit
//  Log message added when plan contains units top capped onto tray positions.
// V8-32465 : M.Pettit
//  Log message added for positions using alternate dimensions
//V8-32450 : M.Pettit
//  Chest Z, Y positions were incorrect
// V8-32465 : M.Pettit
//  Log message added for positions using alternate dimensions
// V8-32448 : M.Pettit
//  Log message added when plan contains units top capped onto case positions.
// V8-32601 : M.Pettit
//  XENDPOS was incorrect for shelves
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.External.ApolloStructure
{
    public abstract class ApolloPositionElementConverterBase
    {
        #region Fields
        private PlanogramFixtureItemDto _fixtureItem;
        private PlanogramFixtureDto _fixture;

        private PlanogramFixtureAssemblyDto _fixtureAssemblyDto;
        private PlanogramAssemblyComponentDto _assemblyComponentDto;

        private PlanogramFixtureComponentDto _fixtureComponent;
        private PlanogramComponentDto _component;
        private PlanogramSubComponentDto _subComponent;
        private CustomAttributeDataDto _customAttrData;
        private PlanogramProductDto _productDto;
        private PlanogramPositionDto _positionDto;

        private ApolloDataSetSectionShelvesShelfShelfData _shelf;
        private PositionsPosition _position;
      
        private IEnumerable<PlanogramFieldMappingDto> _fieldMappings;
        private Int32 _id = 0;

        private ApolloPositionElementPositionType _positionType;

        private Boolean _isAssemblyChild = false;

        //Used for logging purposes
        private Object _planogramId;
        private List<PlanogramEventLogDto> _planogramEventLogDtoList = new List<PlanogramEventLogDto>();

        //user preferences
        private Boolean _retainSourcePlanLookAndFeel = true;
        #endregion

        #region Properties
        /// <summary>
        /// The source dto to convert
        /// </summary>
        public PlanogramFixtureItemDto FixtureItem
        {
            get { return _fixtureItem; }
            set { _fixtureItem = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        public PlanogramFixtureDto Fixture
        {
            get { return _fixture; }
            set { _fixture = value; }
        }

        /// <summary>
        /// The parent PlanogramFixtureAssemblyDto of the componentDto to convert
        /// </summary>
        protected PlanogramFixtureAssemblyDto FixtureAssemblyDto
        {
            get { return _fixtureAssemblyDto; }
            set { _fixtureAssemblyDto = value; }
        }

        /// <summary>
        /// The parent PlanogramAssemblyComponentDto of the subComponentDto to convert
        /// </summary>
        protected PlanogramAssemblyComponentDto AssemblyComponentDto
        {
            get { return _assemblyComponentDto; }
            set { _assemblyComponentDto = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        public PlanogramFixtureComponentDto FixtureComponent
        {
            get { return _fixtureComponent; }
            set { _fixtureComponent = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        public PlanogramComponentDto Component
        {
            get { return _component; }
            set { _component = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        public PlanogramSubComponentDto SubComponent
        {
            get { return _subComponent; }
            set { _subComponent = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        public CustomAttributeDataDto CustomAttributeDataDto
        {
            get { return _customAttrData; }
            set { _customAttrData = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        public PlanogramProductDto ProductDto
        {
            get { return _productDto; }
            set { _productDto = value; }
        }

        /// <summary>
        /// The source dto to convert
        /// </summary>
        public PlanogramPositionDto PositionDto
        {
            get { return _positionDto; }
            set { _positionDto = value; }
        }

        public IEnumerable<PlanogramFieldMappingDto> FieldMappings
        {
            get { return _fieldMappings; }
            set { _fieldMappings = value; }
        }

        /// <summary>
        /// The output Position's parent Shelf Data Object
        /// </summary>
        public ApolloDataSetSectionShelvesShelfShelfData Shelf
        {
            get { return _shelf; }
            set { _shelf = value; }
        }

        /// <summary>
        /// The output Position Data Object
        /// </summary>
        public PositionsPosition Position
        {
            get { return _position; }
            set { _position = value; }
        }

        /// <summary>
        /// Determines whether the shelf element created is created from a standard
        /// fixture or an assembly component
        /// </summary>
        protected Boolean IsAssemblyChild
        {
            get { return _isAssemblyChild; }
        }

        /// <summary>
        /// The id of the created shelf data item
        /// </summary>
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        //the type of position to create from the source positionDto. Since Apollo does not support left/right caps
        //we need to extract them into individual positions
        public ApolloPositionElementPositionType PositionType
        {
            get { return _positionType; }
            set { _positionType = value; }
        }

        /// <summary>
        /// The id of the planogram being exported. Used for logging purposes
        /// </summary>
        protected Object PlanogramId
        {
            get { return _planogramId; }
        }

        /// <summary>
        /// The list of event logs. Any events added in this class will be added to this list
        /// </summary>
        protected List<PlanogramEventLogDto> PlanogramEventLogDtoList
        {
            get { return _planogramEventLogDtoList; }
        }

        private Boolean RetainSourcePlanLookAndFeel
        {
            get { return _retainSourcePlanLookAndFeel; }
            set { _retainSourcePlanLookAndFeel = value; }
        }
        #endregion

        protected ApolloPositionElementConverterBase()
        {
            _position = new PositionsPosition();

            InitialiseValues();
        }

        public void Initialise(PlanogramFixtureItemDto fixtureItem, PlanogramFixtureDto fixture, PlanogramFixtureAssemblyDto fixtureAssembly, PlanogramAssemblyComponentDto assemblyComponent, PlanogramFixtureComponentDto fixtureComponent, PlanogramComponentDto component, PlanogramSubComponentDto subComponent,
            CustomAttributeDataDto customAttrData, PlanogramProductDto product, PlanogramPositionDto position, IEnumerable<PlanogramFieldMappingDto> fieldMappings, Int32 positionId, Boolean retainSourcePlanLookAndFeel)
        {
            this.FixtureItem = fixtureItem;
            this.Fixture = fixture;
            this.FixtureComponent = fixtureComponent;
            this.FixtureAssemblyDto = fixtureAssembly;
            this.AssemblyComponentDto = assemblyComponent;
            this.Component = component;
            this.SubComponent = subComponent;
            this.CustomAttributeDataDto = customAttrData;
            this.ProductDto = product;
            this.PositionDto = position;
            this.FieldMappings = fieldMappings;
            this.Id = positionId;

            _isAssemblyChild = (fixtureComponent == null); //is the element we are creating a child of an assembly

            this.RetainSourcePlanLookAndFeel = retainSourcePlanLookAndFeel;

            _position.PO_POSITION = _id.ToString();
        }

        public void InitialiseLogging(Object planogramId, List<PlanogramEventLogDto> planogramEventLogDtoList)
        {
            _planogramId = planogramId;
            _planogramEventLogDtoList = planogramEventLogDtoList;
        }

        internal void LogExportWarning(String description, String content)
        {
            SpacePlanningExportHelper.CreateEventLogEntry(
                this.PlanogramId, this.PlanogramEventLogDtoList, 
                description, content, 
                PlanogramEventLogEntryType.Warning, PlanogramEventLogAffectedType.Positions);
        }

        /// <summary>
        /// Logs an error message to the parent export context for displaying to users
        /// </summary>
        /// <param name="ex"></param>
        internal void LogExportError(Exception ex)
        {
            SpacePlanningExportHelper.CreateEventLogEntry(
                this.PlanogramId, this.PlanogramEventLogDtoList,
                Message.ApolloExportLog_Position_Error_Description,
                String.Format(Message.ApolloExportLog_Position_Error_Content, this.ProductDto.Gtin, this.ProductDto.Name, ex.Message),
                PlanogramEventLogEntryType.Error, PlanogramEventLogAffectedType.Positions);
        }

        internal void InitialiseValues()
        {
            _position.PO_POSITION = "0";
            _position.PO_UPC = String.Empty;
            _position.PO_XPOS = "0.0";
            _position.PO_YPOS = "0.0";
            _position.PO_ZPOS = "0.0";
            _position.PO_X_ENDPOS = "0.0";
            _position.PO_Y_ENDPOS = "0.0";
            _position.PO_Z_ENDPOS = "0.0";
            _position.PO_XFACINGS = "0";
            _position.PO_YFACINGS = "0";
            _position.PO_ZFACINGS = "0";
            _position.PO_XDIM = "0.0";
            _position.PO_YDIM = "0.0";
            _position.PO_ZDIM = "0.0";
            _position.PO_CAPACITY = "0";
            _position.PO_SALES = "0.0";
            _position.PO_YPEGHOLEPOS = "0.0";
            _position.PO_XGAP = "0.0";
            _position.PO_YGAP = "0.0";
            _position.PO_ZGAP = "0.0";

            _position.PO_MERCHANDISINGTYPE = "100";
            _position.PO_ORIENTATION = "130";

            _position.PO_LAYOVERS = "0";
            _position.PO_LAYOVERSDEEP = "0";
            _position.PO_PEGLENGTH = "0.0";
            _position.PO_PEGTYPE = "0";
            _position.PO_XPEGNUM = "0.0";
            _position.PO_YPEGNUM = "0.0";
            _position.PO_XPEGHOLEPOS = "0.0";
            _position.PO_LINEAR = "0.0";
            _position.PO_SQUARE = "0.0";
            _position.PO_CUBIC = "0.0";
            _position.PO_CASES = "0.0";
            _position.PO_DAYSOFSUPPLY = "0.0";
            _position.PO_INVENTORY = "0.0";
            _position.PO_TURNS = "0.0";
            _position.PO_INVENTORYATRETAIL = "0.0";
            _position.PO_PROFIT = "0.0";
            _position.PO_ADJUSTEDMOVEMENT = "0.0";
            _position.PO_NORMALSTOCK = "0.0";
            _position.PO_SAFETYSTOCK = "0.0";
            _position.PO_PCTCUBICPERSHF = "0.0";
            _position.PO_PCTCUBICPERSECT = "0.0";
            _position.PO_PCTSQUAREPERSHF = "0.0";
            _position.PO_PCTSQUAREPERSECT = "0.0";
            _position.PO_PCTLINEARPERSHF = "0.0";
            _position.PO_PCTLINEARPERSECT = "0.0";
            _position.PO_INVRETPERCUBIC = "0.0";
            _position.PO_INVRETPERSQUARE = "0.0";
            _position.PO_INVRETPERLINEAR = "0.0";
            _position.PO_INVCOSTPERCUBIC = "0.0";
            _position.PO_INVCOSTPERSQUARE = "0.0";
            _position.PO_INVCOSTPERLINEAR = "0.0";
            _position.PO_CAPACITYPERCUBIC = "0.0";
            _position.PO_CAPACITYPERSQUARE = "0.0";
            _position.PO_CAPACITYPERLINEAR = "0.0";
            _position.PO_ROII = "0.0";
            _position.PO_WEEKSOFSUPPLY = "0.0";
            _position.PO_BACKROOMSTOCK = "0.0";
            _position.PO_PCTINVCOSTPERSHF = "0.0";
            _position.PO_PCTINVCOSTPERSECT = "0.0";
            _position.PO_PCTCAPACITYPERSHF = "0.0";
            _position.PO_PCTCAPACITYPERSECT = "0.0";
            _position.PO_DPP = "0.0";
            _position.PO_MOVEMENTPERSQUARE = "0.0";
            _position.PO_MULTICAPACITY = "0";
            _position.PO_SUGGESTEDFACINGS = "0";
            _position.PO_MOVEMENTPERLINEAR = "0.0";
            _position.PO_DPPPERUNIT = "0.0";
            _position.PO_DPPPERCUBIC = "0.0";
            _position.PO_DPPPERSQUARE = "0.0";
            _position.PO_DPPPERLINEAR = "0.0";
            _position.PO_SALESPERCUBIC = "0.0";
            _position.PO_SALESPERSQUARE = "0.0";
            _position.PO_SALESPERLINEAR = "0.0";
            _position.PO_DPPPERCENT = "0.0";
            _position.PO_PCTPROFITPERSHF = "0.0";
            _position.PO_PCTPROFITPERSECT = "0.0";
            _position.PO_PCTMOVEMENTPERSHF = "0.0";
            _position.PO_PCTMOVEMENTPERSECT = "0.0";
            _position.PO_PCTDPPPERSHF = "0.0";
            _position.PO_PCTDPPPERSECT = "0.0";
            _position.PO_PCTSALESPERSHF = "0.0";
            _position.PO_PCTSALESPERSECT = "0.0";
            _position.PO_PROFITPERUNIT = "0.0";
            _position.PO_PROFITPERCUBIC = "0.0";
            _position.PO_PROFITPERSQUARE = "0.0";
            _position.PO_PROFITPERLINEAR = "0.0";
            _position.PO_MOVEMENTPERCUBIC = "0.0";
            _position.PO_MULTIORIENT = "0";
            _position.PO_EDGE = String.Empty;
            _position.PO_FITSTATUS = "0";
            _position.PO_LOSTSALES = "0.0";
            _position.PO_DISTMOVEMENT = "0.0";
            _position.PO_ACTUALMARGIN = "0.0";
            _position.PO_CONTRIBTOMARGIN = "0.0";
            _position.PO_SALESPERFACING = "0.0";
            _position.PO_RESERVED1 = "0.0";
            _position.PO_RESERVED2 = "0.0";
            _position.PO_TAGNUMBER = "0.0";
            _position.PO_SALESGROWTH = "0";
            _position.PO_DECOTEXT = String.Empty;
            _position.PO_GMROI = "0.0";
            _position.PO_RESERVED5 = "0";

            #region Unused Fields
            //_position.PO_RECCAPSHELF = "0.0";
            //_position.PO_RECCAPSECTION = "0.0";
            //_position.PO_RECFRONTSDEEP = "0.0";
            //_position.PO_INVVAR>7.0</PO_INVVAR>
            //_position.PO_RECROII = "0.0";
            //_position.PO_RECWOS = "0.0";
            //_position.PO_RECTURNS = "0.0";
            //_position.PO_RECINVRTL = "0.0";
            //_position.PO_RECINVCST = "0.0";
            //_position.PO_RECDOS = "0.0";
            //_position.PO_RECBKRMSTOCK = "0.0";
            //_position.PO_RECCASES = "0.0";
            //_position.PO_RECCAPPOS = "0.0";
            #endregion
            #region X05_POSMEASX?? Fields
            //_position.X05_POSMEASX01 = "0.0";
            //_position.X05_POSMEASX02 = "0.0";
            //_position.X05_POSMEASX03 = "0.0";
            //_position.X05_POSMEASX04 = "0.0";
            //_position.X05_POSMEASX05 = "0.0";
            //_position.X05_POSMEASX06 = "0.0";
            //_position.X05_POSMEASX07 = "0.0";
            //_position.X05_POSMEASX08 = "0.0";
            //_position.X05_POSMEASX09 = "0.0";
            //_position.X05_POSMEASX10 = "0.0";
            //_position.X05_POSMEASX11 = "0.0";
            //_position.X05_POSMEASX12 = "0.0";
            //_position.X05_POSMEASX13 = "0.0";
            //_position.X05_POSMEASX14 = "0.0";
            //_position.X05_POSMEASX15 = "0.0";
            //_position.X05_POSMEASX16 = "0.0";
            //_position.X05_POSMEASX17 = "0.0";
            //_position.X05_POSMEASX18 = "0.0";
            //_position.X05_POSMEASX19 = "0.0";
            //_position.X05_POSMEASX20 = "0.0";
            //_position.X05_POSMEASX21 = "0.0";
            //_position.X05_POSMEASX22 = "0.0";
            //_position.X05_POSMEASX23 = "0.0";
            //_position.X05_POSMEASX24 = "0.0";
            //_position.X05_POSMEASX25 = "0.0";
            //_position.X05_POSMEASX26 = "0.0";
            //_position.X05_POSMEASX27 = "0.0";
            //_position.X05_POSMEASX28 = "0.0";
            //_position.X05_POSMEASX29 = "0.0";
            //_position.X05_POSMEASX30 = "0.0";
            #endregion
            #region X05_POSDESCX?? Fields
            //_position.X05_POSDESCX01 = "0.0";
            //_position.X05_POSDESCX02 = "0.0";
            //_position.X05_POSDESCX03 = "0.0";
            //_position.X05_POSDESCX04 = "0.0";
            //_position.X05_POSDESCX05 = "0.0";
            //_position.X05_POSDESCX06 = "0.0";
            //_position.X05_POSDESCX07 = "0.0";
            //_position.X05_POSDESCX08 = "0.0";
            //_position.X05_POSDESCX09 = "0.0";
            //_position.X05_POSDESCX10 = "0.0";
            #endregion
            #region X05_POSMEASRESERVE?? Fields
            //_position.X05_POSMEASRESERVE20 = "0.0";
            //_position.X05_POSMEASRESERVE19 = "0.0";
            //_position.X05_POSMEASRESERVE18 = "0.0";
            //_position.X05_POSMEASRESERVE17 = "0.0";
            //_position.X05_POSMEASRESERVE16 = "0.0";
            //_position.X05_POSMEASRESERVE15 = "0.0";
            //_position.X05_POSMEASRESERVE14 = "0.0";
            //_position.X05_POSMEASRESERVE13 = "0.0";
            //_position.X05_POSMEASRESERVE12 = "0.0";
            //_position.X05_POSMEASRESERVE11 = "0.0";
            //_position.X05_POSMEASRESERVE10 = "0.0";
            //_position.X05_POSMEASRESERVE09 = "0.0";
            //_position.X05_POSMEASRESERVE08 = "0.0";
            //_position.X05_POSMEASRESERVE07 = "0.0";
            //_position.X05_POSMEASRESERVE06 = "0.0";
            //_position.X05_POSMEASRESERVE05 = "0.0";
            //_position.X05_POSMEASRESERVE04 = "0.0";
            //_position.X05_POSMEASRESERVE03 = "0.0";
            //_position.X05_POSMEASRESERVE02 = "0.0";
            //_position.X05_POSMEASRESERVE01 = "0.0";
            //_position.X05_POSDESCRESERVE01 = String.Empty;
            //_position.X05_POSDESCRESERVE02 = String.Empty;
            //_position.X05_POSDESCRESERVE03 = String.Empty;
            #endregion
        }

        public virtual PositionsPosition Convert()
        {
            ConvertDefaults();

            return null;
        }

        internal virtual void ConvertDefaults()
        {
            _position.PO_POSITION = _id.ToString();
            _position.PO_UPC = _productDto.Gtin;

            ApolloMerchMethodType merchMethod = ApolloMerchMethodType.Hand;
            ApolloOrientationType orientation = ApolloOrientationType.FrontEnd;
            Single xPos = 0;

            switch (this.PositionType)
            {
                case ApolloPositionElementPositionType.LeftCap:
                    _position.PO_XPOS = _positionDto.X.ToString();
                    _position.PO_X_ENDPOS = (_positionDto.X + PositionPlacementExportHelper.GetLeftCapBlockWidth(_positionDto, _productDto)).ToString();
                    _position.PO_YPOS = (_positionDto.Y).ToString();
                    _position.PO_Y_ENDPOS = PositionPlacementExportHelper.GetLeftCapBlockHeight(_positionDto, _productDto).ToString();
                    _position.PO_Z_ENDPOS = _positionDto.Z.ToString();
                    _position.PO_ZPOS = (_positionDto.Z + PositionPlacementExportHelper.GetLeftCapBlockDepth(_positionDto, _productDto)).ToString();
                    _position.PO_XFACINGS = _positionDto.FacingsXWide.ToString();
                    _position.PO_YFACINGS = _positionDto.FacingsXHigh.ToString();
                    _position.PO_ZFACINGS = _positionDto.FacingsXDeep.ToString();
                    _position.PO_XDIM = PositionPlacementExportHelper.GetLeftRightCapUnitWidth(_positionDto, _productDto).ToString();
                    _position.PO_YDIM = PositionPlacementExportHelper.GetLeftRightCapUnitHeight(_positionDto, _productDto).ToString();
                    _position.PO_ZDIM = PositionPlacementExportHelper.GetLeftRightCapUnitDepth(_positionDto, _productDto).ToString();
                    _position.PO_CAPACITY = (_positionDto.FacingsXWide * _positionDto.FacingsXHigh * _positionDto.FacingsXDeep).ToString();
                    _position.PO_LINEAR = PositionPlacementExportHelper.GetLeftCapBlockWidth(_positionDto, _productDto).ToString();

                    orientation = ApolloExportHelper.GetApolloPositionOrientation((PlanogramPositionOrientationType)_positionDto.OrientationTypeX, (PlanogramProductOrientationType)_productDto.OrientationType);
                    _position.PO_ORIENTATION = ((Int32)orientation).ToString();

                    _position.PO_XGAP = ApolloExportHelper.GetPositionXGap(_positionDto, _productDto, orientation).ToString();
                    _position.PO_YGAP = ApolloExportHelper.GetPositionYGap(_positionDto, _productDto, orientation).ToString();

                    merchMethod = ApolloExportHelper.GetApolloLeftRightCapMerchandisingType(_positionDto, _productDto);
                    _position.PO_MERCHANDISINGTYPE = ((Int32)merchMethod).ToString();
                    break;

                case ApolloPositionElementPositionType.Main:
                    if (ApolloExportHelper.PositionHasSideCappings(_positionDto) && _positionDto.IsXPlacedLeft)
                    {
                        //Add offset equal to width of left capping
                        xPos = _positionDto.X + PositionPlacementExportHelper.GetLeftCapBlockWidth(_positionDto, _productDto);
                    }
                    else
                    {
                        //positionDto has no left cap so just use the dto's x-position
                        xPos = _positionDto.X;
                    }
                    _position.PO_XPOS = xPos.ToString();
                    _position.PO_X_ENDPOS = (xPos + PositionPlacementExportHelper.GetMainBlockWidth(_positionDto, _productDto)).ToString();
                    _position.PO_YPOS = (_positionDto.Y).ToString(); //For shelves, y = y - subComponent.Height
                    _position.PO_Y_ENDPOS = PositionPlacementExportHelper.GetMainBlockHeight(_positionDto, _productDto).ToString();
                    //Apollo Z pos is front to back i.e. start pos is the front of the position, endpos is the back
                    _position.PO_Z_ENDPOS = _positionDto.Z.ToString();
                    _position.PO_ZPOS = (_positionDto.Z + PositionPlacementExportHelper.GetMainBlockDepth(_positionDto, _productDto)).ToString();
                    _position.PO_XFACINGS = _positionDto.FacingsWide.ToString();
                    _position.PO_YFACINGS = _positionDto.FacingsHigh.ToString();
                    _position.PO_ZFACINGS = _positionDto.FacingsDeep.ToString();
                    _position.PO_XDIM = PositionPlacementExportHelper.GetMainBlockUnitWidth(_positionDto, _productDto).ToString();
                    _position.PO_YDIM = PositionPlacementExportHelper.GetMainBlockUnitHeight(_positionDto, _productDto).ToString();
                    _position.PO_ZDIM = PositionPlacementExportHelper.GetMainBlockUnitDepth(_positionDto, _productDto).ToString();
                    if (_positionDto.FacingsYHigh > 0 && _positionDto.FacingsYDeep > 0)
                    {
                        _position.PO_LAYOVERS = _positionDto.FacingsYHigh.ToString();
                        _position.PO_LAYOVERSDEEP = _positionDto.FacingsYDeep.ToString();
                    }
                    _position.PO_CAPACITY = (_positionDto.FacingsWide * _positionDto.FacingsHigh * _positionDto.FacingsDeep).ToString();
                    _position.PO_LINEAR = PositionPlacementExportHelper.GetMainBlockWidth(_positionDto, _productDto).ToString();

                    orientation = ApolloExportHelper.GetApolloPositionOrientation((PlanogramPositionOrientationType)_positionDto.OrientationType, (PlanogramProductOrientationType)_productDto.OrientationType);
                    _position.PO_ORIENTATION = ((Int32)orientation).ToString();

                    _position.PO_XGAP = ApolloExportHelper.GetPositionXGap(_positionDto, _productDto, orientation).ToString();
                    _position.PO_YGAP = ApolloExportHelper.GetPositionYGap(_positionDto, _productDto, orientation).ToString();

                    merchMethod = ApolloExportHelper.GetApolloMainBlockMerchandisingType(_positionDto, _productDto);
                    _position.PO_MERCHANDISINGTYPE = ((Int32)merchMethod).ToString();
                    break;

                case ApolloPositionElementPositionType.RightCap:
                    xPos = _positionDto.X + PositionPlacementExportHelper.GetMainBlockWidth(_positionDto, _productDto);
                    _position.PO_XPOS = xPos.ToString();
                    _position.PO_X_ENDPOS = xPos + PositionPlacementExportHelper.GetRightCapBlockWidth(_positionDto, _productDto).ToString();
                    _position.PO_YPOS = (_positionDto.Y).ToString();
                    _position.PO_Y_ENDPOS = PositionPlacementExportHelper.GetRightCapBlockHeight(_positionDto, _productDto).ToString();
                    _position.PO_Z_ENDPOS = _positionDto.Z.ToString();
                    _position.PO_ZPOS = (_positionDto.Z + PositionPlacementExportHelper.GetRightCapBlockDepth(_positionDto, _productDto)).ToString();
                    _position.PO_XFACINGS = _positionDto.FacingsXWide.ToString();
                    _position.PO_YFACINGS = _positionDto.FacingsXHigh.ToString();
                    _position.PO_ZFACINGS = _positionDto.FacingsXDeep.ToString();
                    _position.PO_XDIM = PositionPlacementExportHelper.GetLeftRightCapUnitWidth(_positionDto, _productDto).ToString();
                    _position.PO_YDIM = PositionPlacementExportHelper.GetLeftRightCapUnitHeight(_positionDto, _productDto).ToString();
                    _position.PO_ZDIM = PositionPlacementExportHelper.GetLeftRightCapUnitDepth(_positionDto, _productDto).ToString();
                    _position.PO_CAPACITY = (_positionDto.FacingsXWide * _positionDto.FacingsXHigh * _positionDto.FacingsXDeep).ToString();
                    _position.PO_LINEAR = PositionPlacementExportHelper.GetRightCapBlockWidth(_positionDto, _productDto).ToString();
                    
                    orientation = ApolloExportHelper.GetApolloPositionOrientation((PlanogramPositionOrientationType)_positionDto.OrientationTypeX, (PlanogramProductOrientationType)_productDto.OrientationType);
                    _position.PO_ORIENTATION = ((Int32)orientation).ToString();

                    _position.PO_XGAP = ApolloExportHelper.GetPositionXGap(_positionDto, _productDto, orientation).ToString();
                    _position.PO_YGAP = ApolloExportHelper.GetPositionYGap(_positionDto, _productDto, orientation).ToString();

                    merchMethod = ApolloExportHelper.GetApolloLeftRightCapMerchandisingType(_positionDto, _productDto);
                    _position.PO_MERCHANDISINGTYPE = ((Int32)merchMethod).ToString();
                    break;
            }

            
            _position.PO_SQUARE = (System.Convert.ToSingle(_position.PO_LINEAR) * System.Convert.ToSingle(_position.PO_ZPOS)).ToString();
            _position.PO_CUBIC = (System.Convert.ToSingle(_position.PO_LINEAR) * System.Convert.ToSingle(_position.PO_ZPOS) * System.Convert.ToSingle(_position.PO_Y_ENDPOS)).ToString();
            
            #region Log Issues
            if (_positionDto.FacingsHigh > ProductDto.MaxStack)
            {
                //If manually placed in V8, users can override the max stack value. Apollo will not allow this
                LogExportWarning(Message.ApolloExportLog_PositionWarning_FacingsHighGreaterThanMaxStack_Description,
                    String.Format(Message.ApolloExportLog_PositionWarning_FacingsHighGreaterThanMaxStack_Content, ProductDto.Gtin, ProductDto.Name));
            }

            if (_productDto.FingerSpaceToTheSide > 0 && _productDto.SqueezeWidth > 0)
            {
                //V8 products that have both squeeze and finger space will ignore the finger space
                LogExportWarning(Message.ApolloExportLog_PositionWarning_FingerSpaceAndSqueeze_Description,
                    String.Format(Message.ApolloExportLog_PositionWarning_FingerSpaceAndSqueeze_Content, ProductDto.Gtin, ProductDto.Name));
            }
            if (this.PositionType == ApolloPositionElementPositionType.Main && _positionDto.FacingsYHigh > 0)
            {
                //We have top caps. Check the orientation of the V8 plan caps is valid for the main facings.
                //Apollo only has limited support for top caps for a position
                if (!ApolloExportHelper.TopCapOrientationIsValid(PositionDto, ProductDto, orientation))
                {
                    LogExportWarning(Message.ApolloExportLog_PositionWarning_TopCapOrientation_Description,
                    String.Format(Message.ApolloExportLog_PositionWarning_TopCapOrientation_Content, ProductDto.Gtin, ProductDto.Name));
                }
            }

            if (_positionDto.FacingsZHigh > 0 && _positionDto.FacingsZWide > 0 && _positionDto.FacingsZDeep > 0)
            {
                //position has back/front caps. Apollo will not support this this.
                LogExportWarning(Message.ApolloExportLog_PositionWarning_BackCaps_Description,
                    String.Format(Message.ApolloExportLog_PositionWarning_BackCapsDetected_Content, ProductDto.Gtin, ProductDto.Name));
            }

            Boolean isPositionMerchStyle = _positionDto.MerchandisingStyle > 0;
            Byte merchStyle = _positionDto.MerchandisingStyle > 0 ? _positionDto.MerchandisingStyle : _productDto.MerchandisingStyle;
            if (isPositionMerchStyle)
            {
                if ((PlanogramPositionMerchandisingStyle)merchStyle == PlanogramPositionMerchandisingStyle.Display)
                {
                    if (_productDto.DisplayHeight > 0 || _productDto.DisplayWidth > 0 || _productDto.DisplayDepth > 0)
                    {
                        //position uses display dimensions. Apollo will not support this.
                        LogExportWarning(Message.ApolloExportLog_PositionWarning_DisplayDimension_Description,
                            String.Format(Message.ApolloExportLog_PositionWarning_DisplayDimensionsDetected_Content, ProductDto.Gtin, ProductDto.Name));
                    }
                }
                if ((PlanogramPositionMerchandisingStyle)merchStyle == PlanogramPositionMerchandisingStyle.PointOfPurchase)
                {
                    //position uses point of purchase dimensions. Apollo will not support this.
                    LogExportWarning(Message.ApolloExportLog_PositionWarning_PointOfPurchase_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_PointOfPurchaseDimensionsDetected_Content, ProductDto.Gtin, ProductDto.Name));
                }
                if ((PlanogramPositionMerchandisingStyle)merchStyle == PlanogramPositionMerchandisingStyle.Alternate)
                {
                    //position uses point of purchase dimensions. Apollo will not support this.
                    LogExportWarning(Message.ApolloExportLog_PositionWarning_Alternate_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_AlternateDimensionsDetected_Content, ProductDto.Gtin, ProductDto.Name));
                }
            }
            else
            {
                if ((PlanogramProductMerchandisingStyle)merchStyle == PlanogramProductMerchandisingStyle.Display)
                {
                    if (_productDto.DisplayHeight > 0 || _productDto.DisplayWidth > 0 || _productDto.DisplayDepth > 0)
                    {
                        //position uses display dimensions. Apollo will not support this.
                        LogExportWarning(Message.ApolloExportLog_PositionWarning_DisplayDimension_Description,
                            String.Format(Message.ApolloExportLog_PositionWarning_DisplayDimensionsDetected_Content, ProductDto.Gtin, ProductDto.Name));
                    }
                }
                if ((PlanogramProductMerchandisingStyle)merchStyle == PlanogramProductMerchandisingStyle.PointOfPurchase)
                {
                    //position uses point of purchase dimensions. Apollo will not support this.
                    LogExportWarning(Message.ApolloExportLog_PositionWarning_PointOfPurchase_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_PointOfPurchaseDimensionsDetected_Content, ProductDto.Gtin, ProductDto.Name));
                }
                if ((PlanogramProductMerchandisingStyle)merchStyle == PlanogramProductMerchandisingStyle.Alternate)
                {
                    //position uses point of purchase dimensions. Apollo will not support this.
                    LogExportWarning(Message.ApolloExportLog_PositionWarning_Alternate_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_AlternateDimensionsDetected_Content, ProductDto.Gtin, ProductDto.Name));
                }
            }

            if (this.PositionType == ApolloPositionElementPositionType.Main)
            {
                if (merchMethod == ApolloMerchMethodType.TrayPack)
                {
                    if (_positionDto.FacingsYHigh > 0 && _positionDto.FacingsYWide > 0 && _positionDto.FacingsYDeep > 0)
                    {
                        //top caps for tray products are capped as trays so warn user
                        LogExportWarning(Message.ApolloExportLog_PositionWarning_TrayTopCaps_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_TrayTopCapsDetected_Content, ProductDto.Gtin, ProductDto.Name));

                    }
                }
                if (merchMethod == ApolloMerchMethodType.Case)
                {
                    if (_positionDto.FacingsYHigh > 0 && _positionDto.FacingsYWide > 0 && _positionDto.FacingsYDeep > 0)
                    {
                        //top caps for case products are capped as cases so warn user
                        LogExportWarning(Message.ApolloExportLog_PositionWarning_CaseTopCaps_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_CaseTopCapsDetected_Content, ProductDto.Gtin, ProductDto.Name));

                    }
                }
            }

            #endregion
        }
    }


    public class ApolloPositionElementConverterShelf : ApolloPositionElementConverterBase
    {
        public ApolloPositionElementConverterShelf(ApolloPositionElementPositionType apolloPositionElementPositionType)
        {
            this.PositionType = apolloPositionElementPositionType;
        }

        public override PositionsPosition Convert()
        {
            try
            {
                base.Convert();

                //Implement shelf-specific overrides here
                this.Position.PO_YPOS = (this.PositionDto.Y - this.SubComponent.Height).ToString(); //For shelves, y = y - subComponent.Height
                       
                return this.Position;
            }
            catch (Exception ex)
            {
                LogExportError(ex);
                return null;
            }
        }
    }


    public class ApolloPositionElementConverterFreeHandSurface : ApolloPositionElementConverterBase
    {
        public ApolloPositionElementConverterFreeHandSurface(ApolloPositionElementPositionType apolloPositionElementPositionType)
        {
            this.PositionType = apolloPositionElementPositionType;
        }

        public override PositionsPosition Convert()
        {
            try
            {
                base.Convert();

                //Implement freehand surface-specific overrides here

                return this.Position;
            }
            catch (Exception ex)
            {
                LogExportError(ex);
                return null;
            }
        }
    }


    public class ApolloPositionElementConverterPeg : ApolloPositionElementConverterBase
    {
        public ApolloPositionElementConverterPeg(ApolloPositionElementPositionType apolloPositionElementPositionType)
        {
            this.PositionType = apolloPositionElementPositionType;
        }

        public override PositionsPosition Convert()
        {
            try
            {
                base.Convert();

                //Implement peg-specific overrides here
                switch (this.PositionType)
                {
                    case ApolloPositionElementPositionType.LeftCap:
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsXWide * this.PositionDto.FacingsXHigh).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetLeftCapBlockDepth(this.PositionDto, this.ProductDto)).ToString();

                        break;

                    case ApolloPositionElementPositionType.Main:
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsWide * this.PositionDto.FacingsHigh).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetMainBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        break;

                    case ApolloPositionElementPositionType.RightCap:
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsXWide * this.PositionDto.FacingsXHigh).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetRightCapBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        break;
                }
                 
                this.Position.PO_Z_ENDPOS = this.PositionDto.Z.ToString();
                this.Position.PO_PEGLENGTH = this.ProductDto.PegDepth.ToString();
                //no layovers allowed
                this.Position.PO_LAYOVERS = "0";
                this.Position.PO_LAYOVERSDEEP = "0";

                return this.Position;
            }
            catch (Exception ex)
            {
                LogExportError(ex);
                return null;
            }
        }
    }


    public class ApolloPositionElementConverterCoffin : ApolloPositionElementConverterBase
    {
        public ApolloPositionElementConverterCoffin(ApolloPositionElementPositionType apolloPositionElementPositionType)
        {
            this.PositionType = apolloPositionElementPositionType;
        }

        public override PositionsPosition Convert()
        {
            try
            {
                base.Convert();
                        
                //Implement coffin-specific overrides here
                ApolloOrientationType orientation;

                switch (this.PositionType)
                {                    
                    case ApolloPositionElementPositionType.LeftCap:
                        this.Position.PO_XPOS = (this.PositionDto.X - this.SubComponent.FaceThicknessLeft).ToString();
                        this.Position.PO_X_ENDPOS = (PositionPlacementExportHelper.GetLeftCapBlockWidth(this.PositionDto, this.ProductDto)).ToString();
                        //Apollo YPos values are CCM z dimensions for coffins:
                        //  YPOS = front of merch area of subcomponent to the front of the position
                        //  Y_ENDPOS = front of merch area of subcomponent to rear of position
                        this.Position.PO_Y_ENDPOS = (this.SubComponent.Depth - this.SubComponent.FaceThicknessFront - this.PositionDto.Z).ToString();
                        this.Position.PO_YPOS = (this.SubComponent.Depth - this.SubComponent.FaceThicknessFront - this.PositionDto.Z - PositionPlacementExportHelper.GetMainBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        //Apollo ZPos values are CCM Y dimensions for coffins
                        this.Position.PO_Z_ENDPOS = (this.PositionDto.Y - this.SubComponent.FaceThicknessBottom).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Y - this.SubComponent.FaceThicknessBottom + PositionPlacementExportHelper.GetLeftCapBlockHeight(this.PositionDto, this.ProductDto)).ToString();
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsXWide * this.PositionDto.FacingsXDeep).ToString();
                        this.Position.PO_YFACINGS = this.PositionDto.FacingsXDeep.ToString();
                        this.Position.PO_ZFACINGS = this.PositionDto.FacingsXHigh.ToString();
                        this.Position.PO_YDIM = PositionPlacementExportHelper.GetLeftRightCapUnitDepth(this.PositionDto, this.ProductDto).ToString();
                        this.Position.PO_ZDIM = PositionPlacementExportHelper.GetLeftRightCapUnitHeight(this.PositionDto, this.ProductDto).ToString();
                        this.Position.PO_XGAP = "0";
                        orientation = ApolloExportHelper.GetApolloCoffinPositionOrientation((PlanogramPositionOrientationType)this.PositionDto.OrientationTypeX);
                        this.Position.PO_ORIENTATION = ((Int32)orientation).ToString();
                        break;

                    case ApolloPositionElementPositionType.Main:
                        if (ApolloExportHelper.PositionHasSideCappings(this.PositionDto) && this.PositionDto.IsXPlacedLeft)
                        {
                        //Add offset equal to width of left capping
                            this.Position.PO_XPOS = (this.PositionDto.X - this.SubComponent.FaceThicknessLeft + PositionPlacementExportHelper.GetLeftCapBlockWidth(this.PositionDto, this.ProductDto)).ToString();
                        }
                        else
                        {
                            //positionDto has no left cap so just use the dto's x-position
                            this.Position.PO_XPOS = (this.PositionDto.X - this.SubComponent.FaceThicknessLeft).ToString();
                        }
                        this.Position.PO_X_ENDPOS = PositionPlacementExportHelper.GetMainBlockWidth(this.PositionDto, this.ProductDto).ToString();

                        //Apollo YPos values are CCM z dimensions for coffins:
                        //  YPOS = front of merch area of subcomponent to the front of the position
                        //  Y_ENDPOS = front of merch area of subcomponent to rear of position
                        this.Position.PO_Y_ENDPOS = (this.SubComponent.Depth - this.SubComponent.FaceThicknessFront - this.PositionDto.Z).ToString();
                        this.Position.PO_YPOS = (this.SubComponent.Depth - this.SubComponent.FaceThicknessFront - this.PositionDto.Z - PositionPlacementExportHelper.GetMainBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        //Apollo ZPos values are CCM Y dimensions for coffins
                        this.Position.PO_Z_ENDPOS = (this.PositionDto.Y - this.SubComponent.FaceThicknessBottom).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Y - this.SubComponent.FaceThicknessBottom + PositionPlacementExportHelper.GetMainBlockHeight(this.PositionDto, this.ProductDto)).ToString();
                        //For chests, x facings = total number of positions visible from above
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsWide * this.PositionDto.FacingsDeep).ToString();
                        this.Position.PO_YFACINGS = this.PositionDto.FacingsDeep.ToString();
                        this.Position.PO_ZFACINGS = this.PositionDto.FacingsHigh.ToString();
                        this.Position.PO_YDIM = PositionPlacementExportHelper.GetMainBlockUnitDepth(this.PositionDto, this.ProductDto).ToString();
                        this.Position.PO_ZDIM = PositionPlacementExportHelper.GetMainBlockUnitHeight(this.PositionDto, this.ProductDto).ToString();

                        orientation = ApolloExportHelper.GetApolloCoffinPositionOrientation((PlanogramPositionOrientationType)this.PositionDto.OrientationType);
                        this.Position.PO_ORIENTATION = ((Int32)orientation).ToString();
                        this.Position.PO_XGAP = ApolloExportHelper.GetPositionXGap(this.PositionDto, this.ProductDto, orientation).ToString();
                        break;

                    case ApolloPositionElementPositionType.RightCap:
                        this.Position.PO_XPOS = (this.PositionDto.X - this.SubComponent.FaceThicknessLeft + PositionPlacementExportHelper.GetMainBlockWidth(this.PositionDto, this.ProductDto)).ToString();
                        this.Position.PO_X_ENDPOS = (PositionPlacementExportHelper.GetRightCapBlockWidth(this.PositionDto, this.ProductDto)).ToString();
                        //Apollo YPos values are CCM z dimensions for coffins:
                        //  YPOS = front of merch area of subcomponent to the front of the position
                        //  Y_ENDPOS = front of merch area of subcomponent to rear of position
                        this.Position.PO_Y_ENDPOS = (this.SubComponent.Depth - this.SubComponent.FaceThicknessFront - this.PositionDto.Z).ToString();
                        this.Position.PO_YPOS = (this.SubComponent.Depth - this.SubComponent.FaceThicknessFront - this.PositionDto.Z - PositionPlacementExportHelper.GetMainBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        //Apollo ZPos values are CCM Y dimensions for coffins
                        this.Position.PO_Z_ENDPOS = (this.PositionDto.Y - this.SubComponent.FaceThicknessBottom).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Y - this.SubComponent.FaceThicknessBottom + PositionPlacementExportHelper.GetRightCapBlockHeight(this.PositionDto, this.ProductDto)).ToString();
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsXWide * this.PositionDto.FacingsXDeep).ToString();
                        this.Position.PO_YFACINGS = this.PositionDto.FacingsXDeep.ToString();
                        this.Position.PO_ZFACINGS = this.PositionDto.FacingsXHigh.ToString();
                        this.Position.PO_YDIM = PositionPlacementExportHelper.GetLeftRightCapUnitDepth(this.PositionDto, this.ProductDto).ToString();
                        this.Position.PO_ZDIM = PositionPlacementExportHelper.GetLeftRightCapUnitHeight(this.PositionDto, this.ProductDto).ToString();

                        this.Position.PO_XGAP = this.ProductDto.FingerSpaceToTheSide.ToString();
                        orientation = ApolloExportHelper.GetApolloCoffinPositionOrientation((PlanogramPositionOrientationType)this.PositionDto.OrientationTypeX);
                        this.Position.PO_ORIENTATION = ((Int32)orientation).ToString();
                        break;
                }

                this.Position.PO_LAYOVERS = "0";
                this.Position.PO_LAYOVERSDEEP = "0";

                #region Log Issues
                if (this.PositionDto.FacingsYHigh > 0 && this.PositionDto.FacingsYWide > 0 && this.PositionDto.FacingsYDeep > 0)
                {
                    //If manually placed in V8, users can override the max stack value. Apollo will not allow this
                    LogExportWarning(Message.ApolloExportLog_PositionWarning_TopCapDetected_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_TopCapDetected_Content, ProductDto.Gtin, ProductDto.Name));
                }

                Single blockHeight = PositionPlacementExportHelper.GetMainBlockHeight(this.PositionDto, this.ProductDto);
                Single chestHeight = this.SubComponent.Height - this.SubComponent.FaceThicknessBottom - this.SubComponent.FaceThicknessTop;
                if (blockHeight > chestHeight)
                {
                    //chest position merchandises above the walls of the chest
                    LogExportWarning(Message.ApolloExportLog_PositionWarning_PositionHeightGreaterThanChestHeight_Description,
                        String.Format(Message.ApolloExportLog_PositionWarning_PositionHeightGreaterThanChestHeight_Content, ProductDto.Gtin, ProductDto.Name));
                }
            
                #endregion

                return this.Position;
            }
            catch (Exception ex)
            {
                LogExportError(ex);
                return null;
            }
        }
    }


    public class ApolloPositionElementConverterHangingBar : ApolloPositionElementConverterBase
    {
        public ApolloPositionElementConverterHangingBar(ApolloPositionElementPositionType apolloPositionElementPositionType)
        {
            this.PositionType = apolloPositionElementPositionType;
        }

        public override PositionsPosition Convert()
        {
            try
            {
                base.Convert();

                Single unitPositionWidth = 0;

                //Implement hanging bar-specific overrides here
                switch (this.PositionType)
                {
                    case ApolloPositionElementPositionType.LeftCap:
                        //Left and Right caps not supported in Apollo
                        //position uses point of purchase dimensions. Apollo will not support this.
                        LogExportWarning(Message.ApolloExportLog_PositionWarning_LeftRightCapsOnHangingBars_Description,
                            String.Format(Message.ApolloExportLog_PositionWarning_LeftRightCapsOnHangingBars_Content, ProductDto.Gtin, ProductDto.Name));
                        this.Position = null;
                        return this.Position;

                    case ApolloPositionElementPositionType.Main:
                        this.Position.PO_XPOS = "0.0";
                        this.Position.PO_X_ENDPOS = PositionPlacementExportHelper.GetMainBlockDepth(this.PositionDto, this.ProductDto).ToString();
                        //Apollo Z pos for hanging bars, since they are rotated by 90 degrees, is the
                        // position depth /2 ( not product depth /2)
                        unitPositionWidth = PositionPlacementExportHelper.GetMainBlockUnitWidth(this.PositionDto, this.ProductDto);
                        this.Position.PO_ZPOS = (unitPositionWidth / 2).ToString();
                        this.Position.PO_Z_ENDPOS = (0 - (unitPositionWidth / 2)).ToString();

                        this.Position.PO_YPOS = (this.SubComponent.Height - PositionPlacementExportHelper.GetMainBlockUnitHeight(this.PositionDto, this.ProductDto)).ToString();
                        this.Position.PO_Y_ENDPOS = this.SubComponent.Height.ToString();
                        
                        this.Position.PO_XGAP = this.ProductDto.FingerSpaceToTheSide.ToString();
                        break;

                    case ApolloPositionElementPositionType.RightCap:
                        //Left and Right caps not supported in Apollo
                        LogExportWarning(Message.ApolloExportLog_PositionWarning_LeftRightCapsOnHangingBars_Description,
                            String.Format(Message.ApolloExportLog_PositionWarning_LeftRightCapsOnHangingBars_Content, ProductDto.Gtin, ProductDto.Name));
                        this.Position = null;
                        return this.Position;
                }

                //this.Position.PO_RESERVED5 = "1"; //Turns off hangers
                this.Position.PO_PEGLENGTH = "0.0";
                this.Position.PO_XPEGHOLEPOS = this.ProductDto.PegX.ToString();
                //no layovers allowed
                this.Position.PO_LAYOVERS = "0";
                this.Position.PO_LAYOVERSDEEP = "0";


                return this.Position;
            }
            catch (Exception ex)
            {
                LogExportError(ex);
                return null;
            }
        }
    }


    public class ApolloPositionElementConverterCrossbar : ApolloPositionElementConverterBase
    {
        public ApolloPositionElementConverterCrossbar(ApolloPositionElementPositionType apolloPositionElementPositionType)
        {
            this.PositionType = apolloPositionElementPositionType;
        }

        public override PositionsPosition Convert()
        {
            try
            {
                base.Convert();

                //Implement crossbar-specific overrides here
                switch (this.PositionType)
                {
                    case ApolloPositionElementPositionType.LeftCap:
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetLeftCapBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        break;

                    case ApolloPositionElementPositionType.Main:
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetMainBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        break;

                    case ApolloPositionElementPositionType.RightCap:
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetRightCapBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        break;
                }
                this.Position.PO_Z_ENDPOS = this.PositionDto.Z.ToString();
                this.Position.PO_PEGLENGTH = this.ProductDto.PegDepth.ToString();
                //no layovers allowed
                this.Position.PO_LAYOVERS = "0";
                this.Position.PO_LAYOVERSDEEP = "0";
            
                return this.Position;
            }
            catch (Exception ex)
            {
                LogExportError(ex);
                return null;
            }
        }
    }


    public class ApolloPositionElementConverterSlotwall : ApolloPositionElementConverterBase
    {
        public ApolloPositionElementConverterSlotwall(ApolloPositionElementPositionType apolloPositionElementPositionType)
        {
            this.PositionType = apolloPositionElementPositionType;
        }

        public override PositionsPosition Convert()
        {
            try
            {
                base.Convert();

                //Implement slotwall-specific overrides here
                switch (this.PositionType)
                {
                    case ApolloPositionElementPositionType.LeftCap:
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsXWide * this.PositionDto.FacingsXHigh).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetLeftCapBlockDepth(this.PositionDto, this.ProductDto)).ToString();

                        break;

                    case ApolloPositionElementPositionType.Main:
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsWide * this.PositionDto.FacingsHigh).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetMainBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        break;

                    case ApolloPositionElementPositionType.RightCap:
                        this.Position.PO_XFACINGS = (this.PositionDto.FacingsXWide * this.PositionDto.FacingsXHigh).ToString();
                        this.Position.PO_ZPOS = (this.PositionDto.Z + PositionPlacementExportHelper.GetRightCapBlockDepth(this.PositionDto, this.ProductDto)).ToString();
                        break;
                }
                this.Position.PO_Z_ENDPOS = this.PositionDto.Z.ToString();
                this.Position.PO_PEGLENGTH = this.ProductDto.PegDepth.ToString();
                //no layovers allowed
                this.Position.PO_LAYOVERS = "0";
                this.Position.PO_LAYOVERSDEEP = "0";

                return this.Position;
            }
            catch (Exception ex)
            {
                LogExportError(ex); 
                return null;
            }
        }
    }


    //No need for Box type as boxes are not merchndisable


    //Determines the type of Apollo position we are creating from the Source CCM V8 PositionDto object
    public enum ApolloPositionElementPositionType
    {
        Main = 0,
        LeftCap = 1,
        RightCap = 2
    }
}
