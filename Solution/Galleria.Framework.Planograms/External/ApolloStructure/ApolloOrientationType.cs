﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.External.ApolloStructure
{
    /// <summary>
    /// Enum type names taken from Apollo book (Administraitors guid 6.1)
    /// </summary>
    public enum ApolloOrientationType
    {
        FrontSide = 120,
        FrontEnd = 130,
        SideFront = 210,
        SideEnd = 230,
        EndFront = 310,
        EndSide = 320
    }
}
