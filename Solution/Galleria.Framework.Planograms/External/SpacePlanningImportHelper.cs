﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820

// V8-30754 : A.Silva
//  Created.
// V8-31073 : A.Silva
//  Added nested types MetricData and MappingData to help pass data around when setting the mappings.
// V8-31152 : M.Brumby
//  Added generic value set.
// V8-31214 : A.Silva
//  Removed calculation of tray thickness if the number of units in the tray is not known.
// V8-31145 : A.Silva
//  Added method to get a colours better contrast alternative colour.
// V8-31153 : A.Silva
//  Amended DivideBy to avoid an issue with floating pointing errors requiring rounding and not having it applied.
// V8-31260 : A.Silva
//  New ammending of DivideBy to ensur correct rounding.
// V8-31285 : M.Brumby
//  Better Gtin handling
// V8-30809 : A.Silva
//  Added ParseValue to encapsulate the type based parsing.
// V8-31351 : A.Silva
//  Changed the default constants for measures to properties that can return values depending on the measure system type.
// V8-31402 : M.Brumby
//  Made parsing invariant as it was messing up Lithuanian imports
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Resources.Language;
using System.Reflection;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Enums;

namespace Galleria.Framework.Planograms.External
{
    public static class SpacePlanningImportHelper
    {
        #region Constants

        public const Int32 DefaultShelfFillColour = -16777216;
        public const Int32 DefaultPegFillColour = -3308225;
        public const Int32 DefaultChestFillColour = -10185235;
        public const Int32 DefaultBarFillColour = -11179217;
        public const Int32 DefaultBackboardFillColour = -2894893;
        public const Int32 DefaultRodFillColour = -5171;
        public const Int32 DefaultBackboardLineColour = -16777216;
        public const Int32 BaseDefaultFillColour = -5658199;
        public const Int32 BaseDefaultLineColour = -16777216;
        public const Int32 DefaultLineColour = -16777216;
        public const Byte DefaultFillPatternType = (Byte) PlanogramSubComponentFillPatternType.Solid;
        public const Int32 DefaultRiserColour = -1;

        #endregion

        #region Fields

        private static List<Tuple<PlanogramProductOrientationType, PlanogramPositionOrientationType>> _orientationTypePairs;

        #endregion

        #region Properties

        private static List<Tuple<PlanogramProductOrientationType, PlanogramPositionOrientationType>> OrientationTypePairs
        {
            get
            {
                return _orientationTypePairs ?? (_orientationTypePairs =
                                                 new List<Tuple<PlanogramProductOrientationType, PlanogramPositionOrientationType>>
                                                 {
                                                     PairOrientationTypes(PlanogramProductOrientationType.Front0,
                                                                          PlanogramPositionOrientationType.Front0),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Front90,
                                                                          PlanogramPositionOrientationType.Front90),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Front180,
                                                                          PlanogramPositionOrientationType.Front180),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Front270,
                                                                          PlanogramPositionOrientationType.Front270),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Top0, PlanogramPositionOrientationType.Top0),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Top90,
                                                                          PlanogramPositionOrientationType.Top90),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Top180,
                                                                          PlanogramPositionOrientationType.Top180),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Top270,
                                                                          PlanogramPositionOrientationType.Top270),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Right0,
                                                                          PlanogramPositionOrientationType.Right0),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Right90,
                                                                          PlanogramPositionOrientationType.Right90),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Right180,
                                                                          PlanogramPositionOrientationType.Right180),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Right270,
                                                                          PlanogramPositionOrientationType.Right270),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Left0,
                                                                          PlanogramPositionOrientationType.Left0),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Left90,
                                                                          PlanogramPositionOrientationType.Left90),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Left180,
                                                                          PlanogramPositionOrientationType.Left180),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Left270,
                                                                          PlanogramPositionOrientationType.Left270),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Back0,
                                                                          PlanogramPositionOrientationType.Back0),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Back90,
                                                                          PlanogramPositionOrientationType.Back90),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Back180,
                                                                          PlanogramPositionOrientationType.Back180),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Back270,
                                                                          PlanogramPositionOrientationType.Back270),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Bottom0,
                                                                          PlanogramPositionOrientationType.Bottom0),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Bottom90,
                                                                          PlanogramPositionOrientationType.Bottom90),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Bottom180,
                                                                          PlanogramPositionOrientationType.Bottom180),
                                                     PairOrientationTypes(PlanogramProductOrientationType.Bottom270,
                                                                          PlanogramPositionOrientationType.Bottom270)
                                                 });
            }
        }

        public static Single GetDefaultNotchHeight(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.39F;
                default:
                    return 1.0F;
            }
        }

        public static Single GetDefaultNotchSpacingX(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.79F;
                default:
                    return 2.0F;
            }
        }

        public static Single GetDefaultJdaMerchConstraintRow1SpacingY(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.39F;
                default:
                    return 1.0F;
            }
        }

        public static Single GetDefaultBackboardDepth(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.39F;
                default:
                    return 1.0F;
            }
        }

        public static Single GetDefaultMerchConstraintRow1Width(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.39F;
                default:
                    return 1.0F;
            }
        }

        public static Single GetDefaultMerchConstraintRow1Height(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.39F;
                default:
                    return 1.0F;
            }
        }

        public static Single GetDefaultLineThickness(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.20F;
                default:
                    return 0.5F;
            }
        }

        #region Default Measures Properties

        public static Single GetDefaultRiserThickness(PlanogramLengthUnitOfMeasureType measureType)
        {
            switch (measureType)
            {
                case PlanogramLengthUnitOfMeasureType.Inches:
                    return 0.39F;
                default:
                    return 1.0F;
            }
        }

        #endregion

        #endregion

        #region String parsing helpers

        /// <summary>
        /// Checks if the supplied type is nullable
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Boolean INullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        /// <summary>
        /// Converts the provided stringValue into the same type of the targetProperty and sets
        /// the converted value in the target. If the value fails to convert then it will be
        /// set to the type default or if nullable it will be null. If the type of the property
        /// is not a recognised type then no value will be set.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="targetProperty"></param>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static Boolean SetValue(Object target, PropertyInfo targetProperty, String stringValue)
        {
            Boolean success = false;
            Type propertyType = targetProperty.PropertyType;
            Boolean isNullable = INullableType(propertyType);
            if (isNullable)
            {
                propertyType = Nullable.GetUnderlyingType(propertyType);
            }

            if (propertyType== typeof(Int16)){
                targetProperty.SetValue(target, ParseInt16(stringValue, isNullable, out success), null);
            }else if (propertyType== typeof(Int32)){
                targetProperty.SetValue(target, ParseInt32(stringValue, isNullable, out success), null);
            }else if (propertyType== typeof(Single)){
                targetProperty.SetValue(target, ParseSingle(stringValue, isNullable, out success), null);
            }else if (propertyType== typeof(Double)){
                targetProperty.SetValue(target, ParseDouble(stringValue, isNullable, out success), null);
            }else if (propertyType== typeof(Byte)){
                targetProperty.SetValue(target, ParseByte(stringValue, isNullable, out success), null);
            }else if (propertyType== typeof(DateTime)){
                targetProperty.SetValue(target, ParseDateTime(stringValue, isNullable, out success), null);
            }else if (propertyType== typeof(Boolean)){
                targetProperty.SetValue(target, ParseBoolean(stringValue, isNullable, out success), null);
            }else if (propertyType == typeof(String)){
                targetProperty.SetValue(target, stringValue, null);
                success = true;
            }
            return success;
        }

        public static Object ParseSingle(String stringValue, Boolean isNullable, out Boolean success)
        {
            success = true;
            //  Try parse the value.
            Single value;
            if (Single.TryParse(stringValue, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out value)) return value;

            success = false;
            return isNullable ? (Single?)null : default(Single);
        }

        public static Object ParseDouble(String stringValue, Boolean isNullable, out Boolean success)
        {
            success = true;
            //  Try parse the value.
            Double value;
            if (Double.TryParse(stringValue, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out value)) return value;

            success = false;
            return isNullable ? (Double?)null : default(Double);
        }

        public static Object ParseInt32(String stringValue, Boolean isNullable, out Boolean success)
        {
            success = true;
            //  Try parse the value.
            Int32 value;
            if (Int32.TryParse(stringValue, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out value)) return value;

            success = false;
            return isNullable ? (Int32?)null : default(Int32);
        }

        public static Object ParseInt16(String stringValue, Boolean isNullable, out Boolean success)
        {
            success = true;
            //  Try parse the value.
            Int16 value;
            if (Int16.TryParse(stringValue, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out value)) return value;

            success = false;
            return isNullable ? (Int16?)null : default(Int16);
        }

        public static Object ParseByte(String stringValue, Boolean isNullable, out Boolean success)
        {
            success = true;
            //  Try parse the value.
            Single value;
            if (Single.TryParse(stringValue, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out value))
            {
                if (value >= Byte.MinValue &&
                    value <= Byte.MaxValue)
                    return (Byte)value;
            }
            success = false;
            return isNullable ? (Byte?)null : default(Byte);
        }

        public static Object ParseDateTime(String stringValue, Boolean isNullable, out Boolean success)
        {
            success = true;
            Int32 serialDate;
            if (Int32.TryParse(stringValue, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out serialDate))
            {
                if (serialDate == 0) return isNullable ? (DateTime?)null : DateTime.MinValue;
                if (serialDate < 2958465 && serialDate > -693594) 
                    return new DateTime(1899, 12, 31).AddDays(serialDate);
            }
            success = false;
            return isNullable ? (DateTime?)null : DateTime.MinValue;
        }

        public static Object ParseBoolean(String stringValue, Boolean isNullable, out Boolean success)
        {
            success = true;
            //  Try parse the value.
            Int32 value;
            if (Int32.TryParse(stringValue, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out value)) return value != 0;

            success = false;
            return isNullable ? (Boolean?)null : default(Boolean);
        }

        #endregion

        #region Decoding helpers

        public static Single ConvertToRadians(Single degrees)
        {
            if (degrees.EqualTo(0)) return 0;
            if (degrees.GreaterThan(360) ||
                degrees.LessThan(-360)) degrees %= 360;
            if (degrees.LessThan(0)) degrees += 360;
            return Convert.ToSingle(degrees * (Math.PI / 180f), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a ProSpace colour to a CCM colour
        /// </summary>
        /// <param name="value">The colour value</param>
        /// <returns>CCM Colour</returns>
        public static Int32 DecodeColour(Single value)
        {
            var intValue = (Int32)value;
            const Int32 cTransparentColour = 16777215;
            if (intValue == -1)
            {
                return -1; //cTransparentColour; 
            }

            var b = (Byte)((intValue >> 0x10) & 0xff);
            var g = (Byte)((intValue >> 8) & 0xff);
            var r = (Byte)(intValue & 0xff);
            var a = (Byte)((intValue >> 0x18) & 0xff);

            if (a == 0) { a = 255; }

            UInt32 uint32Color = (((UInt32)a) << 24) |
                                 (((UInt32)r) << 16) |
                                 (((UInt32)g) << 8) |
                                 b;

            return (Int32)uint32Color;
        }

        /// <summary>
        ///     Decode the tray thickness based on the actual tray size and the size that the products would have on their own.
        /// </summary>
        /// <param name="dto">The DTO containing the data needed for the calculation, and also the recipient of the result.</param>
        /// <remarks>
        ///     Thickness is not allowed to be negative, so if the tray size is not larger than the product size, it will be
        ///     set to zero.
        /// </remarks>
        public static void DecodeTrayThickness(PlanogramProductDto dto)
        {
            Byte trayHigh = dto.TrayHigh;
            if (trayHigh == 0) return;
            Byte trayWide = dto.TrayWide;
            if (trayWide == 0) return;
            Byte trayDeep = dto.TrayDeep;
            if (trayDeep == 0) return;

            Single productHeight = trayHigh * dto.Height;
            Single trayHeight = dto.TrayHeight;
            if (trayHeight > productHeight)
                dto.TrayThickHeight = trayHeight - productHeight;

            //  Width is distributed between left and right tray walls if there is any thickness.
            Single productWidth = trayWide * dto.Width;
            Single trayWidth = dto.TrayWidth;
            if (trayWidth > productWidth)
                dto.TrayThickWidth = (trayWidth - productWidth) / 2;

            //  Depth is distributed between front and back tray walls if there is any thickness.
            Single productDepth = trayDeep * dto.Depth;
            Single trayDepth = dto.TrayDepth;
            if (trayDepth > productDepth)
                dto.TrayThickDepth = (trayDepth - productDepth) / 2;
        }

        #endregion

        #region Default value helpers

        /// <summary>
        ///     Set the default values for the CanBreakTray properties in the DTO.
        /// </summary>
        /// <param name="dto">The DTO to set the values in.</param>
        /// <remarks>
        ///     By default all CanBreakTray properties are set to true. This avoids issues with missing import data making
        ///     extra calcualtions necessary to determine if trays were broken.
        /// </remarks>
        public static void SetDefaultCanBreakTrayValues(PlanogramProductDto dto)
        {
            dto.CanBreakTrayUp = true;
            dto.CanBreakTrayDown = true;
            dto.CanBreakTrayBack = true;
            dto.CanBreakTrayTop = true;
        }

        #endregion

        /// <summary>
        ///     Extension method to obtain the <see cref="PlanogramProductOrientationType" /> equivalent of the given
        ///     <paramref name="positionOrientationType" />.
        /// </summary>
        /// <param name="positionOrientationType">
        ///     The <see cref="PlanogramProductOrientationType" /> to obtain the <see cref="PlanogramPositionOrientationType" />
        ///     equivalent of.
        /// </param>
        /// <returns>Equivalent value in the <see cref="PlanogramPositionOrientationType" /> enumeration.</returns>
        public static PlanogramProductOrientationType ToProductOrientationType(this PlanogramPositionOrientationType positionOrientationType)
        {
            Tuple<PlanogramProductOrientationType, PlanogramPositionOrientationType> match = OrientationTypePairs.FirstOrDefault(tuple => Equals(tuple.Item2, positionOrientationType));
            return match != null ? match.Item1 : PlanogramProductOrientationType.Front0;
        }

        public static WidthHeightDepthValue MultiplyBy(this WidthHeightDepthValue factorA, WideHighDeepValue factorB)
        {
            return new WidthHeightDepthValue(factorA.Width * factorB.Wide, factorA.Height * factorB.High, factorA.Depth * factorB.Deep);
        }

        public static WideHighDeepValue DivideBy(this WidthHeightDepthValue dividend, WidthHeightDepthValue divisor)
        {

            var wide = (Int16)(!dividend.Width.EqualTo(0) && !divisor.Width.EqualTo(0) ? Convert.ToInt16(Math.Floor(Math.Round(
                    dividend.Width / divisor.Width,
                    MathHelper.DefaultPrecision,
                    MidpointRounding.AwayFromZero))) : 0);
            var high = (Int16)(!dividend.Height.EqualTo(0) && !divisor.Height.EqualTo(0) ? Convert.ToInt16(Math.Floor(Math.Round(
                    dividend.Height / divisor.Height,
                    MathHelper.DefaultPrecision,
                    MidpointRounding.AwayFromZero))) : 0);
            var deep = (Int16)(!dividend.Depth.EqualTo(0) && !divisor.Depth.EqualTo(0) ? Convert.ToInt16(Math.Floor(Math.Round(
                    dividend.Depth / divisor.Depth,
                    MathHelper.DefaultPrecision,
                    MidpointRounding.AwayFromZero))) : 0);
            return new WideHighDeepValue(wide, high, deep);
        }



        /// <summary>
        /// Checks to see if the gtin set in the supplied product dto is contained in the supplied list of 
        /// product gtins. If it is then a new Gtin is set to the product dto.
        /// </summary>
        /// <param name="planogramEventLogDtoList"></param>
        /// <param name="planogramProductDto"></param>
        /// <param name="productGtins"></param>
        /// <param name="logDuplicate"></param>
        private static void CheckForDuplicateGtins(List<PlanogramEventLogDto> planogramEventLogDtoList, PlanogramProductDto planogramProductDto, List<String> productGtins, Boolean logDuplicate = true)
        {
            // Validate for duplicate product gtins
            if (productGtins.Contains(planogramProductDto.Gtin))
            {
                Int32 uniqueKey = 1;
                String newKey = String.Format("{0}{1}", '~', uniqueKey);
                String newGtin = planogramProductDto.Gtin;
                if (newGtin.Length + newKey.Length > 14)
                {
                    newGtin = newGtin.Substring(0, newGtin.Length - (newGtin.Length + newKey.Length - 14));
                }
                newGtin = String.Format("{0}{1}", newGtin, newKey);

                while (productGtins.Contains(newGtin))
                {
                    uniqueKey++;
                    newKey = String.Format("{0}{1}", '~', uniqueKey);
                    newGtin = planogramProductDto.Gtin;
                    if (newGtin.Length + newKey.Length > 14)
                    {
                        newGtin = newGtin.Substring(0, newGtin.Length - (newGtin.Length + newKey.Length - 14));
                    }
                    newGtin = String.Format("{0}{1}", newGtin, newKey);
                }

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList,
                    Message.PlanogramProduct_Gtin_Warning_Duplicate, String.Format(Message.PlanogramProduct_Gtin_Warning_Duplicate_Description,
                    newGtin, planogramProductDto.Name, planogramProductDto.Gtin), PlanogramEventLogEntryType.Warning,
                    PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);

                planogramProductDto.Gtin = newGtin;
            }

            productGtins.Add(planogramProductDto.Gtin);
        }

        /// <summary>
        /// Validates the business rule data for the planogram product object
        /// </summary>
        /// <param name="planogramProductDto"></param>
        public static void ValidatePlanogramProductBusinessRules(List<PlanogramEventLogDto> planogramEventLogDtoList, PlanogramProductDto planogramProductDto, List<String> productGtins = null)
        {
            #region GTIN Business Rule
            Boolean newGtinCreated = false;
            if (String.IsNullOrWhiteSpace(planogramProductDto.Gtin))
            {
                newGtinCreated = true;
                planogramProductDto.Gtin = String.Format("{0}{1}", '~', 0).PadLeft(14, '0');
                if (productGtins != null)
                {
                    CheckForDuplicateGtins(planogramEventLogDtoList, planogramProductDto, productGtins, false);
                }
                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList,
                   Message.PlanogramProduct_Gtin_Warning_GtinCreated, String.Format(Message.PlanogramProduct_Gtin_Warning_GtinCreated_Description,
                   planogramProductDto.Gtin, planogramProductDto.Name), PlanogramEventLogEntryType.Warning,
                   PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }

            if (planogramProductDto.Gtin.Length > 14)
            {
                String oldValue = planogramProductDto.Gtin;
                planogramProductDto.Gtin = planogramProductDto.Gtin.Substring(0, 14);

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_Gtin_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_Gtin_Error_Description, planogramProductDto.Gtin), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }

            //Ignore if we've created our own Gtin.
            if (!newGtinCreated)
            {
              

                //duplicate check. 
                if (productGtins != null)
                {
                    CheckForDuplicateGtins(planogramEventLogDtoList, planogramProductDto, productGtins);
                }
            }
            #endregion

            #region Product Name Business Rule
            if (planogramProductDto.Name != null && planogramProductDto.Name.Length > 100)
            {
                String oldValue = planogramProductDto.Name;
                planogramProductDto.Name = planogramProductDto.Name.Substring(0, 100);

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_Name_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_Name_Error_Description, planogramProductDto.Name), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }
            if (planogramProductDto.Name.Length == 0 && planogramProductDto.Id != null)
            {
                planogramProductDto.Name = planogramProductDto.Id.ToString();
            }
            #endregion

            #region Brand Business Rule
            if (planogramProductDto.Brand != null && planogramProductDto.Brand.Length > 20)
            {
                String oldValue = planogramProductDto.Brand;
                planogramProductDto.Brand = planogramProductDto.Brand.Substring(0, 20);

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_Brand_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_Brand_Error_Description, planogramProductDto.Brand), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }
            #endregion

            #region Financial Group Code Business Rule
            if (planogramProductDto.FinancialGroupCode != null && planogramProductDto.FinancialGroupCode.Length > 50)
            {
                String oldValue = planogramProductDto.FinancialGroupCode;
                planogramProductDto.FinancialGroupCode = planogramProductDto.FinancialGroupCode.Substring(0, 50);

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_FinancialGroupCode_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_FinancialGroupCode_Error_Description, planogramProductDto.FinancialGroupCode), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }
            #endregion

            #region Financial Group Name Business Rule
            if (planogramProductDto.FinancialGroupName != null && planogramProductDto.FinancialGroupName.Length > 100)
            {
                String oldValue = planogramProductDto.FinancialGroupName;
                planogramProductDto.FinancialGroupName = planogramProductDto.FinancialGroupName.Substring(0, 100);

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_FinancialGroupName_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_FinancialGroupName_Error_Description, planogramProductDto.FinancialGroupName), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }
            #endregion

            #region Nesting Height Business Rule
            if (planogramProductDto.NestingHeight > planogramProductDto.Height)
            {
                Single oldValue = planogramProductDto.NestingHeight;
                planogramProductDto.NestingHeight = planogramProductDto.Height;

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_NestingHeight_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_NestingHeight_Error_Description, planogramProductDto.NestingHeight), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }
            #endregion

            #region Nesting Width Business Rule
            if (planogramProductDto.NestingWidth > planogramProductDto.Width)
            {
                Single oldValue = planogramProductDto.NestingWidth;
                planogramProductDto.NestingWidth = planogramProductDto.Width;

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_NestingWidth_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_NestingWidth_Error_Description, planogramProductDto.NestingWidth), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }
            #endregion

            #region Nesting Depth Business Rule
            if (planogramProductDto.NestingDepth > planogramProductDto.Depth)
            {
                Single oldValue = planogramProductDto.NestingDepth;
                planogramProductDto.NestingDepth = planogramProductDto.Depth;

                CreateErrorEventLogEntry(planogramProductDto.PlanogramId, planogramEventLogDtoList, String.Format(Message.PlanogramProduct_NestingDepth_Error, oldValue),
                                            String.Format(Message.PlanogramProduct_NestingDepth_Error_Description, planogramProductDto.NestingDepth), PlanogramEventLogEntryType.Warning,
                                            PlanogramEventLogEventType.Import, PlanogramEventLogAffectedType.Products);
            }
            #endregion
        }
       
        public static void CreateErrorEventLogEntry(Object planogramId, List<PlanogramEventLogDto> planogramEventLogDtoList, String description, String content, PlanogramEventLogEntryType entryType, PlanogramEventLogEventType eventType, PlanogramEventLogAffectedType affectedType, Object affectedId = null)
        {
            PlanogramEventLogDto dto = planogramEventLogDtoList.FirstOrDefault(p => p.Description == description && p.PlanogramId.Equals(planogramId));

            if (dto == null)
            {
                planogramEventLogDtoList.Add(new PlanogramEventLogDto()
                {
                    Id = IdentityHelper.GetNextInt32(),
                    PlanogramId = planogramId,
                    EntryType = (Byte)entryType,
                    EventType = (Byte)eventType,
                    AffectedType = (Byte)affectedType,
                    AffectedId = affectedId ?? 1,
                    Description = description,
                    Content = content,
                    DateTime = DateTime.UtcNow
                });
            }
            else
            {
                if (dto.Content.Contains(content)) return;
                dto.Content = String.Format("{0}{1}{2}", dto.Content, Environment.NewLine, content);
            }
        }

        #region private helper methods

        private static Tuple<PlanogramProductOrientationType, PlanogramPositionOrientationType> PairOrientationTypes(
            PlanogramProductOrientationType productOrientationType,
            PlanogramPositionOrientationType positionOrientationType)
        {
            return new Tuple<PlanogramProductOrientationType, PlanogramPositionOrientationType>(productOrientationType, positionOrientationType);
        }

        #endregion

        /// <summary>
        /// Creates a clone of an object
        /// </summary>
        /// <typeparam name="T">The type being cloned</typeparam>
        /// <param name="obj">the instance to clone</param>
        /// <returns>a clone of the provided instance</returns>
        public static T Clone<T>(T obj)
        {
            if (obj == null)
            {
                return default(T);
            }
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public class MetricData
        {
            #region Properties

            public String Name { get; private set; }

            public String Description { get; private set; }

            public String ExternalField { get; private set; }

            public Byte MetricId { get; private set; }

            public MetricType MetricType { get; private set; }

            public MetricSpecialType SpecialType { get; private set; }

            #endregion

            #region Constructor

            public MetricData(String name,
                              String description,
                              String externalField,
                              Byte metricId,
                              MetricType metricType = MetricType.Decimal,
                              MetricSpecialType specialType = MetricSpecialType.None)
            {
                Name = name;
                Description = description;
                ExternalField = externalField;
                MetricId = metricId;
                MetricType = metricType;
                SpecialType = specialType;
            }

            #endregion
        }

        public class MappingData
        {
            public String TargetName { get; private set; }
            public String SourceName { get; private set; }

            public MappingData(String targetName, String sourceName, Boolean isCustomAttribute = false)
            {
                TargetName = !isCustomAttribute
                                 ? targetName
                                 : String.Format("{0}.{1}", PlanogramProduct.CustomAttributesProperty.Name, targetName);
                SourceName = sourceName;
            }
        }

        /// <summary>
        ///     Get the colour that will offer an acceptable contrast to the one provided.
        /// </summary>
        /// <param name="source">The numeric representation in the format aRGB of the colour to get the contrast colour for.</param>
        /// <returns>A numeric representation in the format aRGB of a colour that will offer an acceptable contrast for the one provided.</returns>
        /// <remarks>For light colours, black will be returned, for dark colours white will be returned.</remarks>
        public static Int32 GetContrastColour(Int32 source)
        {
            var b = (Byte)((source & 0xff));
            var g = (Byte)((source & 0xff) >> 8);
            var r = (Byte)(source & 0xff >> 16);
            var a = (Byte)((source & 0xff) >> 24);

            b = Invert(b);
            g = Invert(g);
            r = Invert(r);
            if (a == 0) { a = 255; }

            UInt32 uint32Color = (((UInt32)a) << 24) |
                     (((UInt32)r) << 16) |
                     (((UInt32)g) << 8) |
                     b;

            return (Int32)uint32Color;
        }

        private static Byte Invert(Byte value)
        {
            //  Just return white for dark values, and black for light values.
            return (Byte) (value > 128 ? 0 : 255);
        }

        public static Object ParseValue(String stringValue, Type propertyType, out Boolean success)
        {
            success = false;
            Object value = null;
            Type type = Nullable.GetUnderlyingType(propertyType);
            Boolean isNullable = type != null;
            if (!isNullable) type = propertyType;

            if (type == typeof (String))
            {
                value = stringValue;
                success = true;
            }
            else if (type == typeof (Single))
                value = ParseSingle(stringValue, isNullable, out success);
            else if (type == typeof (Double))
                value = ParseDouble(stringValue, isNullable, out success);
            else if (type == typeof (Int32))
                value = ParseInt32(stringValue, isNullable, out success);
            else if (type == typeof (Int16))
                value = ParseInt16(stringValue, isNullable, out success);
            else if (type == typeof (Boolean))
                value = ParseBoolean(stringValue, isNullable, out success);
            else if (type == typeof (Byte))
                value = ParseByte(stringValue, isNullable, out success);
            else if (type == typeof (DateTime))
                value = ParseDateTime(stringValue, isNullable, out success);

            return value;
        }
    }
}