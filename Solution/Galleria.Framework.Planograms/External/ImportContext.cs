﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820

// V8-31152 : M.Brumby
//  Created
// V8-31175 : A.Silva
//  Removed PlanName property.
//  Added default, empty constructor.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using System.Collections.ObjectModel;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.External
{
    public class ImportContext
    {
        #region Fields
         
        private IPlanogramSettings _planSettings; //holds default plan settings
        private ReadOnlyCollection<PlanogramFieldMappingDto> _fieldMappings; //holds the customisable field mappings to be applied
        private ReadOnlyCollection<PlanogramMetricMappingDto> _metricMappings; //holds the customisable field mappings to be applied
        private ReadOnlyCollection<PlanogramFieldMappingDto> _planogramFieldMappings = null;
        private ReadOnlyCollection<PlanogramFieldMappingDto> _fixtureFieldMappings = null;
        private ReadOnlyCollection<PlanogramFieldMappingDto> _componentFieldMappings = null;
        private ReadOnlyCollection<PlanogramFieldMappingDto> _productFieldMappings = null;
        #endregion

        #region Properties

        public IPlanogramSettings PlanSettings
        {
            get
            {
                return _planSettings;
            }
            set
            {
                _planSettings = value;
            }
        }

        /// <summary>
        /// Returns the collection of customisable field mappings to apply.
        /// </summary>
        /// <remarks>Type: Planogram = 0, Fixture = 1, Component = 2, Product = 3</remarks>
        public ReadOnlyCollection<PlanogramFieldMappingDto> FieldMappings
        {
            get { return _fieldMappings; }
            set
            {
                _fieldMappings = value;
                UpdateMappings();
            }
        }

        public ReadOnlyCollection<PlanogramFieldMappingDto> PlanogramMappings
        {
            get
            {
                return _planogramFieldMappings;
            }
        }
         public ReadOnlyCollection<PlanogramFieldMappingDto> FixtureMappings
        {
            get
            {
                return _fixtureFieldMappings;
            }
        }
         public ReadOnlyCollection<PlanogramFieldMappingDto> ComponentMappings
        {
            get
            {
                return _componentFieldMappings;
            }
        }
         public ReadOnlyCollection<PlanogramFieldMappingDto> ProductMappings
        {
            get
            {
                return _productFieldMappings;
            }
        }
        
        /// <summary>
        /// Returns the collection of customisable metric mappings to apply.
        /// </summary>
        /// <remarks></remarks>
        public ReadOnlyCollection<PlanogramMetricMappingDto> MetricMappings
        {
            get { return _metricMappings; }
            set
            {
                _metricMappings = value;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initialize an empty, default instance of <see cref="ImportContext"/>.
        /// </summary>
        public ImportContext() : this(PlanogramSettings.NewPlanogramSettings(), new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>()){}

        public ImportContext(IPlanogramSettings planSettings, 
            IEnumerable<PlanogramFieldMappingDto> fieldMappings,
            IEnumerable<PlanogramMetricMappingDto> metricMappings)
        {
            PlanSettings = planSettings;
            FieldMappings = fieldMappings.ToList().AsReadOnly();
            MetricMappings = metricMappings.ToList().AsReadOnly();
        }
        #endregion

        #region Methods

        public void UpdateMappings()
        {
            _planogramFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Planogram).ToList().AsReadOnly();
            _fixtureFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Fixture).ToList().AsReadOnly();
            _componentFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Component).ToList().AsReadOnly();
            _productFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Product).ToList().AsReadOnly();
        }
        #endregion
    }
}