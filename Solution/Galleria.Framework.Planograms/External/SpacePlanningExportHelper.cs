﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31546 : M.Pettit
//  Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Resources.Language;
using System.Reflection;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Framework.Planograms.External
{
    public static class SpacePlanningExportHelper
    {
        /// <summary>
        /// Returns the file path and name of the location to which a file copy should be saved
        /// </summary>
        /// <param name="filePath">The original file</param>
        /// <returns>The location of the backup file</returns>
        private static String GetBackupFilePath(String filePath)
        {
            return String.Format("{0}$", filePath);
        }

        /// <summary>
        /// Creates a backup copy of the old existing file. If an error occurs while exporting the
        /// plnaogram to the new file, this copy should be re-instantiated
        /// </summary>
        /// <param name="fileToCopyPath">The original file to copy</param>
        public static void CreateExportFileBackup(String fileToCopyPath)
        {
            if (System.IO.File.Exists(fileToCopyPath))
            {
                String fileBackupPath = GetBackupFilePath(fileToCopyPath);
                    
                //delete any previous temp file (should have been cleaned up by previous process anyway)
                if (System.IO.File.Exists(fileBackupPath)) System.IO.File.Delete(fileBackupPath);
                //create temp copy of old file to restore in case of error
                System.IO.File.Move(fileToCopyPath, fileBackupPath);
            }
        }

        /// <summary>
        /// Creates a backup copy of the existing file. If an error occurs while exporting the
        /// plnaogram tothe new file, this copy should be re-instantiated
        /// </summary>
        /// <param name="filePath">The path and name of the file whose backup copy is to be deleted</param>
        public static void DeleteExportFileBackup(String filePath)
        {
            String fileBackupPath = GetBackupFilePath(filePath);
            
            if (System.IO.File.Exists(fileBackupPath))
            {
                //delete the temp file
                System.IO.File.Delete(fileBackupPath);
            }
        }

        /// <summary>
        /// Restores the backup copy of the old existing file to its original location
        /// </summary>
        /// <param name="filePath">The path and name of the file whose backup copy is to be restored</param>
        public static void RestoreExportFileBackup(String filePath)
        {
            String fileBackupPath = GetBackupFilePath(filePath);
            
            if (System.IO.File.Exists(fileBackupPath))
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }

                System.IO.File.Move(fileBackupPath, filePath);
            }
        }

        private static Dictionary<Type, Tuple<String, PlanogramEventLogAffectedType>> _typeErrorHelperLookup = new Dictionary<Type, Tuple<String, PlanogramEventLogAffectedType>>()
        {
           { typeof(PackageDto), new Tuple<String, PlanogramEventLogAffectedType>("Package",  PlanogramEventLogAffectedType.Planogram)},
           { typeof(PlanogramDto), new Tuple<String, PlanogramEventLogAffectedType>("Planogram",  PlanogramEventLogAffectedType.Planogram)},
           { typeof(PlanogramFixtureDto), new Tuple<String, PlanogramEventLogAffectedType>("Fixture",  PlanogramEventLogAffectedType.Planogram)},
           { typeof(PlanogramFixtureItemDto), new Tuple<String, PlanogramEventLogAffectedType>("Fixture Item",  PlanogramEventLogAffectedType.Planogram)},
           { typeof(PlanogramFixtureComponentDto), new Tuple<String, PlanogramEventLogAffectedType>("Fixture Component",  PlanogramEventLogAffectedType.Components)},
           { typeof(PlanogramComponentDto), new Tuple<String, PlanogramEventLogAffectedType>("Component",  PlanogramEventLogAffectedType.Components)},
           { typeof(PlanogramSubComponentDto), new Tuple<String, PlanogramEventLogAffectedType>("Subcomponent",  PlanogramEventLogAffectedType.Components)},
           { typeof(PlanogramAssemblyDto), new Tuple<String, PlanogramEventLogAffectedType>("Assembly",  PlanogramEventLogAffectedType.Components)},
           { typeof(PlanogramAssemblyComponentDto), new Tuple<String, PlanogramEventLogAffectedType>("Fixture Assembly",  PlanogramEventLogAffectedType.Components)},
           { typeof(PlanogramProductDto), new Tuple<String, PlanogramEventLogAffectedType>("Product",  PlanogramEventLogAffectedType.Products)},
           { typeof(PlanogramPositionDto), new Tuple<String, PlanogramEventLogAffectedType>("Position",  PlanogramEventLogAffectedType.Positions)},
           { typeof(PlanogramAnnotationDto), new Tuple<String, PlanogramEventLogAffectedType>("Annotation",  PlanogramEventLogAffectedType.Planogram)},
           { typeof(PlanogramPerformanceDto), new Tuple<String, PlanogramEventLogAffectedType>("Performance",  PlanogramEventLogAffectedType.Performance)},
           { typeof(PlanogramPerformanceDataDto), new Tuple<String, PlanogramEventLogAffectedType>("Performance Data",  PlanogramEventLogAffectedType.Performance)},
           { typeof(PlanogramPerformanceMetricDto), new Tuple<String, PlanogramEventLogAffectedType>("Performance Metric",  PlanogramEventLogAffectedType.Performance)},
           { typeof(CustomAttributeDataDto), new Tuple<String, PlanogramEventLogAffectedType>("Custom Attribute",  PlanogramEventLogAffectedType.Planogram)},

        };

       
        public static void CreateErrorEventLogEntry<T>(Object planogramId, List<PlanogramEventLogDto> planogramEventLogDtoList, String content, Object affectedId = null)
        {
            Tuple<String, PlanogramEventLogAffectedType> loopUpResult;
            PlanogramEventLogAffectedType affectedType = PlanogramEventLogAffectedType.Planogram;
            String description = String.Format("Error loading {0} record", (typeof(T)).Name);
            if (_typeErrorHelperLookup.TryGetValue(typeof(T), out loopUpResult))
            {
                description = String.Format("Error loading {0} record", loopUpResult.Item1);
                affectedType = loopUpResult.Item2;
            }


            CreateEventLogEntry(planogramId, planogramEventLogDtoList, description, content, PlanogramEventLogEntryType.Error, PlanogramEventLogEventType.Export, affectedType, affectedId);
        }
        public static void CreateErrorEventLogEntry(Object planogramId, List<PlanogramEventLogDto> planogramEventLogDtoList, String description, String content, PlanogramEventLogAffectedType affectedType, Object affectedId = null)
        {
            CreateEventLogEntry(planogramId, planogramEventLogDtoList, description, content, PlanogramEventLogEntryType.Error, PlanogramEventLogEventType.Export, affectedType, affectedId);
        }

        public static void CreateEventLogEntry(Object planogramId, List<PlanogramEventLogDto> planogramEventLogDtoList, String description, String content, PlanogramEventLogEntryType entryType, PlanogramEventLogAffectedType affectedType, Object affectedId = null)
        {
            CreateEventLogEntry(planogramId, planogramEventLogDtoList, description, content, entryType, PlanogramEventLogEventType.Export, affectedType, affectedId);
        }
        public static void CreateEventLogEntry(Object planogramId, List<PlanogramEventLogDto> planogramEventLogDtoList, String description, String content, PlanogramEventLogEntryType entryType, PlanogramEventLogEventType eventType, PlanogramEventLogAffectedType affectedType, Object affectedId = null)
        {
            PlanogramEventLogDto dto = planogramEventLogDtoList.FirstOrDefault(p => p.Description == description && p.PlanogramId.Equals(planogramId));

            if (dto == null)
            {
                planogramEventLogDtoList.Add(new PlanogramEventLogDto()
                {
                    Id = IdentityHelper.GetNextInt32(),
                    PlanogramId = planogramId,
                    EntryType = (Byte)entryType,
                    EventType = (Byte)eventType,
                    AffectedType = (Byte)affectedType,
                    AffectedId = affectedId ?? 1,
                    Description = description,
                    Content = content,
                    DateTime = DateTime.UtcNow,
                    TaskSource = PlanogramEventLogEventTypeHelper.FriendlyNames[eventType]
                });
            }
            else
            {
                if (dto.Content.Contains(content)) return;
                dto.Content = String.Format("{0}{1}{2}", dto.Content, Environment.NewLine, content);
            }
        }
    }
}
