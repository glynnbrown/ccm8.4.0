﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
// V8-31547 : M.Pettit
//  Added options to alter plan creation based on user settings (include perfformance data, retain look and feel etc)
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using System.Collections.ObjectModel;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.External
{
    [Serializable]
    public class ExportContext
    {
        #region Fields

        private IPlanogramSettings _planSettings; //holds default plan settings
        private ReadOnlyCollection<PlanogramFieldMappingDto> _fieldMappings; //holds the customisable field mappings to be applied
        private ReadOnlyCollection<PlanogramMetricMappingDto> _metricMappings; //holds the customisable field mappings to be applied
        private ReadOnlyCollection<PlanogramFieldMappingDto> _planogramFieldMappings = null;
        private ReadOnlyCollection<PlanogramFieldMappingDto> _fixtureFieldMappings = null;
        private ReadOnlyCollection<PlanogramFieldMappingDto> _componentFieldMappings = null;
        private ReadOnlyCollection<PlanogramFieldMappingDto> _productFieldMappings = null;

        private Boolean _includePerformanceData = true;
        private Boolean _retainSourcePlanLookAndFeel = true;
        private Boolean _allowComplexPositions = true;
        private Boolean _viewErrorLogsOnCompletion = true;

        private List<PlanogramEventLogDto> _exportEvents = new List<PlanogramEventLogDto>();
        #endregion

        
        #region Properties

        public IPlanogramSettings PlanSettings
        {
            get
            {
                return _planSettings;
            }
            set
            {
                _planSettings = value;
            }
        }

        /// <summary>
        /// Returns the collection of customisable field mappings to apply.
        /// </summary>
        /// <remarks>Type: Planogram = 0, Fixture = 1, Component = 2, Product = 3</remarks>
        public ReadOnlyCollection<PlanogramFieldMappingDto> FieldMappings
        {
            get { return _fieldMappings; }
            set
            {
                _fieldMappings = value;
                UpdateMappings();
            }
        }

        public ReadOnlyCollection<PlanogramFieldMappingDto> PlanogramMappings
        {
            get
            {
                return _planogramFieldMappings;
            }
        }
        public ReadOnlyCollection<PlanogramFieldMappingDto> FixtureMappings
        {
            get
            {
                return _fixtureFieldMappings;
            }
        }
        public ReadOnlyCollection<PlanogramFieldMappingDto> ComponentMappings
        {
            get
            {
                return _componentFieldMappings;
            }
        }
        public ReadOnlyCollection<PlanogramFieldMappingDto> ProductMappings
        {
            get
            {
                return _productFieldMappings;
            }
        }
        
        /// <summary>
        /// Returns the collection of customisable metric mappings to apply.
        /// </summary>
        /// <remarks></remarks>
        public ReadOnlyCollection<PlanogramMetricMappingDto> MetricMappings
        {
            get { return _metricMappings; }
            set
            {
                _metricMappings = value;
            }
        }

        public Boolean IncludePerformanceData
        {
            get { return _includePerformanceData; }
            set { _includePerformanceData = value; }
        }

        public Boolean RetainSourcePlanLookAndFeel
        {
            get { return _retainSourcePlanLookAndFeel; }
            set { _retainSourcePlanLookAndFeel = value; }
        }

        public Boolean AllowComplexPositions
        {
            get { return _allowComplexPositions; }
            set { _allowComplexPositions = value; }
        }

        public Boolean ViewErrorLogsOnCompletion
        {
            get { return _viewErrorLogsOnCompletion; }
            set { _viewErrorLogsOnCompletion = value; }
        }

        /// <summary>
        /// A list of any events that occured during the export
        /// </summary>
        public List<PlanogramEventLogDto> ExportEvents
        {
            get
            {
                return _exportEvents;
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        ///     Initialize an empty, default instance of <see cref="ImportContext"/>.
        /// </summary>
        public ExportContext() : 
            this(PlanogramSettings.NewPlanogramSettings(), new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>(), true, true, true, true)
        { }

        public ExportContext(IPlanogramSettings planSettings, 
            IEnumerable<PlanogramFieldMappingDto> fieldMappings,
            IEnumerable<PlanogramMetricMappingDto> metricMappings,
            Boolean includePerformanceData, 
            Boolean retainSourcePlanLookAndFeel, 
            Boolean allowComplexPositions,
            Boolean viewErrorLogsOnCompletion)
        {
            PlanSettings = planSettings;
            FieldMappings = fieldMappings.ToList().AsReadOnly();
            MetricMappings = metricMappings.ToList().AsReadOnly();
            IncludePerformanceData = includePerformanceData;
            RetainSourcePlanLookAndFeel = retainSourcePlanLookAndFeel;
            AllowComplexPositions = allowComplexPositions;
            ViewErrorLogsOnCompletion = viewErrorLogsOnCompletion;
        }
        #endregion

        #region Methods

        public void UpdateMappings()
        {
            _planogramFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Planogram).ToList().AsReadOnly();
            _fixtureFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Fixture).ToList().AsReadOnly();
            _componentFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Component).ToList().AsReadOnly();
            _productFieldMappings = FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Product).ToList().AsReadOnly();
        }
        #endregion
    }
}
