﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion

#region Version History: (CCM 8.3.0)
// V8-31546 : J.Pickup
//  Created EncodeColour & ConvertToRadians & EncodeOrientation && EncodeMerchandisingStyle etc
// V8-32141 : M.Pettit
//  Updated DefaultPogeft
// V8-32469  : J.Pickup
//  Changed EncodeMerchandisingStyle to export point of purchase as unit.
// V8-32705  : J.Pickup
//  More methods V8-GetMerchStyleFriendly && IsASpacemanType etc
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Resources.Language;
using System.Globalization;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.External
{
    #region Version History: CCM830
    // V8-31548 : J.Pickup
    //  Work to facilitate exporting - created.
    #endregion

    using System.Collections.Generic;
    using System;
    using Dal.DataTransferObjects;
    using System.Diagnostics;

    /// <summary>
    /// Indicates the spaceman orientation type.
    /// </summary>
    public enum SpacemanOrientationtype
    {
        Front0 = 0,
        Front90 = 1,
        Front180 = 2,
        Front270 = 3,
        Top0 = 4,
        Top90 = 5,
        Top180 = 6,
        Top270 = 7,
        Left0 = 8,
        Left90 = 9,
        Left180 = 10,
        Left270 = 11,
        Back0 = 12,
        Back90 = 13,
        Back180 = 14,
        Back270 = 15,
        Bottom0 = 16,
        Bottom90 = 17,
        Bottom180 = 18,
        Bottom270 = 19,
        Right0 = 20,
        Right90 = 21,
        Right180 = 22,
        Right270 = 23,
    }

    /// <summary>
    /// Indicates the spaceman x merch strategy type.
    /// </summary>
    public enum SpacemanMerchStrategyXType
    {
        Left = 0,
        Right = 1,
        Even = 3,
        Facings = 2 //Doesn't exist in v8. 
    }

    public enum SpacemanMerchStyle
    {
        Units = 0,
        Trays = 1,
        Cases = 2,
        Displays = 3
    }

    public static class SpacemanExportHelper
    {
        /// <summary>
        ///     Default name for the Spaceman V9 template file. [Spaceman V9 Default.pogeft]
        /// </summary>
        public static String DefaultPogeft { get { return "Nielsen Spaceman V9 Default.pogeft"; } }
        public static List<String> AvailableVersions
        {
            get { return new List<String> { "9" }; }
        }

        public static IEnumerable<String> SupportedVersions
        {
            get
            {
                yield return "9.0";
            }
        }

        public static UInt32 EncodeColour(Int32 value)
        {

            var r = (Byte)((value >> 0x10) & 0xff);
            var g = (Byte)((value >> 8) & 0xff);
            var b = (Byte)(value & 0xff);
            var a = (Byte)((value >> 0x18) & 0xff);

            if (a == 0)
            {
                r = 255;
                b = 255;
                g = 255;
                a = 255;
            }

            UInt32 uint32Color =
                                 (((UInt32)b) << 16) |
                                 (((UInt32)g) << 8) |
                                 r;

            return uint32Color;
        }

        public static Single ConvertFromRadians(Single radians)
        {
            Single radian = radians;
            IFormatProvider prov = CultureInfo.InvariantCulture;


            if (radian > 360 || radians < -360)
            {
                radian = radian % 360;
            }

            if (radian < 0)
            {
                radian = 360 + radian;
            }

            Single myDegrees = System.Convert.ToSingle(radian * (180 / Math.PI));

            //V8 stores positive radians only, so reduce where necessary. 
            if (myDegrees > 180)
            {
                myDegrees -= 360;
            }

            return myDegrees;
        }

        public static Single ConvertCCMAngleToSpacemanAngle(Single radiansToconvert)
        {
            return -ConvertFromRadians(radiansToconvert);
        }

        public static SpacemanOrientationtype EncodeOrientation(PlanogramPositionOrientationType V8Orientation, PlanogramProductOrientationType v8ProductOrientation)
        {
            switch (V8Orientation)
            {
                case PlanogramPositionOrientationType.Default:
                    return EncodeProductOrientationType(v8ProductOrientation);
                case PlanogramPositionOrientationType.Front0:
                    return SpacemanOrientationtype.Front0;
                case PlanogramPositionOrientationType.Front90:
                    return SpacemanOrientationtype.Front90;
                case PlanogramPositionOrientationType.Top0:
                    return SpacemanOrientationtype.Top0;
                case PlanogramPositionOrientationType.Top90:
                    return SpacemanOrientationtype.Top90;
                case PlanogramPositionOrientationType.Right0:
                    return SpacemanOrientationtype.Right0;
                case PlanogramPositionOrientationType.Right90:
                    return SpacemanOrientationtype.Right90;
                case PlanogramPositionOrientationType.Left0:
                    return SpacemanOrientationtype.Left0;
                case PlanogramPositionOrientationType.Left90:
                    return SpacemanOrientationtype.Left90;
                case PlanogramPositionOrientationType.Back0:
                    return SpacemanOrientationtype.Back0;
                case PlanogramPositionOrientationType.Back90:
                    return SpacemanOrientationtype.Back90;
                case PlanogramPositionOrientationType.Bottom0:
                    return SpacemanOrientationtype.Bottom0;
                case PlanogramPositionOrientationType.Bottom90:
                    return SpacemanOrientationtype.Bottom90;
                case PlanogramPositionOrientationType.Front180:
                    return SpacemanOrientationtype.Front180;
                case PlanogramPositionOrientationType.Front270:
                    return SpacemanOrientationtype.Front270;
                case PlanogramPositionOrientationType.Top180:
                    return SpacemanOrientationtype.Top180;
                case PlanogramPositionOrientationType.Top270:
                    return SpacemanOrientationtype.Top270;
                case PlanogramPositionOrientationType.Right180:
                    return SpacemanOrientationtype.Right180;
                case PlanogramPositionOrientationType.Right270:
                    return SpacemanOrientationtype.Right270;
                case PlanogramPositionOrientationType.Left180:
                    return SpacemanOrientationtype.Left180;
                case PlanogramPositionOrientationType.Left270:
                    return SpacemanOrientationtype.Left270;
                case PlanogramPositionOrientationType.Back180:
                    return SpacemanOrientationtype.Back180;
                case PlanogramPositionOrientationType.Back270:
                    return SpacemanOrientationtype.Back270;
                case PlanogramPositionOrientationType.Bottom180:
                    return SpacemanOrientationtype.Bottom180;
                case PlanogramPositionOrientationType.Bottom270:
                    return SpacemanOrientationtype.Bottom270;
                default:
                    //Should never ever happen. Unless we make rotations even more crazy than they already are.
                    return 0;
            }
        }


        public static SpacemanOrientationtype EncodeProductOrientationType(PlanogramProductOrientationType v8ProductOrientation)
        {
            switch (v8ProductOrientation)
            {
                case PlanogramProductOrientationType.Front0:
                    return SpacemanOrientationtype.Front0;
                case PlanogramProductOrientationType.Front90:
                    return SpacemanOrientationtype.Front90;
                case PlanogramProductOrientationType.Top0:
                    return SpacemanOrientationtype.Top0;
                case PlanogramProductOrientationType.Top90:
                    return SpacemanOrientationtype.Top90;
                case PlanogramProductOrientationType.Right0:
                    return SpacemanOrientationtype.Right0;
                case PlanogramProductOrientationType.Right90:
                    return SpacemanOrientationtype.Right90;
                case PlanogramProductOrientationType.Left0:
                    return SpacemanOrientationtype.Left0;
                case PlanogramProductOrientationType.Left90:
                    return SpacemanOrientationtype.Left90;
                case PlanogramProductOrientationType.Back0:
                    return SpacemanOrientationtype.Back0;
                case PlanogramProductOrientationType.Back90:
                    return SpacemanOrientationtype.Back90;
                case PlanogramProductOrientationType.Bottom0:
                    return SpacemanOrientationtype.Bottom0;
                case PlanogramProductOrientationType.Bottom90:
                    return SpacemanOrientationtype.Bottom90;
                case PlanogramProductOrientationType.Front180:
                    return SpacemanOrientationtype.Front180;
                case PlanogramProductOrientationType.Front270:
                    return SpacemanOrientationtype.Front270;
                case PlanogramProductOrientationType.Top180:
                    return SpacemanOrientationtype.Top180;
                case PlanogramProductOrientationType.Top270:
                    return SpacemanOrientationtype.Top270;
                case PlanogramProductOrientationType.Right180:
                    return SpacemanOrientationtype.Right180;
                case PlanogramProductOrientationType.Right270:
                    return SpacemanOrientationtype.Right270;
                case PlanogramProductOrientationType.Left180:
                    return SpacemanOrientationtype.Left180;
                case PlanogramProductOrientationType.Left270:
                    return SpacemanOrientationtype.Left270;
                case PlanogramProductOrientationType.Back180:
                    return SpacemanOrientationtype.Back180;
                case PlanogramProductOrientationType.Back270:
                    return SpacemanOrientationtype.Back270;
                case PlanogramProductOrientationType.Bottom180:
                    return SpacemanOrientationtype.Bottom180;
                case PlanogramProductOrientationType.Bottom270:
                    return SpacemanOrientationtype.Bottom270;
                default:
                    //Should never ever happen. Unless we make rotations even more crazy than they already are.
                    return 0;
            }
        }

        static public SpacemanMerchStrategyXType EncodeMerchandisingStrategyX(PlanogramSubComponentXMerchStrategyType type)
        {
            switch (type)
            {
                case PlanogramSubComponentXMerchStrategyType.Manual:
                case PlanogramSubComponentXMerchStrategyType.Left:
                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    return SpacemanMerchStrategyXType.Left;

                case PlanogramSubComponentXMerchStrategyType.Right:
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    return SpacemanMerchStrategyXType.Right;

                case PlanogramSubComponentXMerchStrategyType.Even:
                    return SpacemanMerchStrategyXType.Even;
                default:
                    Debug.Assert(false, "Unknown V8 merch strategy.");
                    return SpacemanMerchStrategyXType.Left;
            }
        }

        static public SpacemanMerchStyle EncodeMerchandisingStyle(PlanogramPositionMerchandisingStyle type, PlanogramProductMerchandisingStyle v8MerchStyle)
        {
            switch (type)
            {
                case PlanogramPositionMerchandisingStyle.Default:
                    return EncodeProductMerchandisingStyle(v8MerchStyle);

                case PlanogramPositionMerchandisingStyle.Unit:
                    return SpacemanMerchStyle.Units;

                case PlanogramPositionMerchandisingStyle.Tray:
                    return SpacemanMerchStyle.Trays;

                case PlanogramPositionMerchandisingStyle.Case:
                    return SpacemanMerchStyle.Cases;

                //Spaceman doesn't have point of purchase or alternate
                case PlanogramPositionMerchandisingStyle.Alternate:
                case PlanogramPositionMerchandisingStyle.PointOfPurchase:
                    return SpacemanMerchStyle.Units;

                case PlanogramPositionMerchandisingStyle.Display:
                    return SpacemanMerchStyle.Displays;

                default:
                    Debug.Assert(false, "PlanogramPositionMerchandisingStyle unknown.");
                    return SpacemanMerchStyle.Units;
            }
        }

        static public SpacemanMerchStyle EncodeProductMerchandisingStyle(PlanogramProductMerchandisingStyle v8MerchStyle)
        {
            switch (v8MerchStyle)
            {
                case PlanogramProductMerchandisingStyle.Unit:
                    return SpacemanMerchStyle.Units;

                case PlanogramProductMerchandisingStyle.Tray:
                    return SpacemanMerchStyle.Trays;

                case PlanogramProductMerchandisingStyle.Case:
                    return SpacemanMerchStyle.Cases;

                //Spaceman doesn't have point of purchase or alternate
                case PlanogramProductMerchandisingStyle.Alternate:
                case PlanogramProductMerchandisingStyle.PointOfPurchase:
                    return SpacemanMerchStyle.Units;

                case PlanogramProductMerchandisingStyle.Display:
                    return SpacemanMerchStyle.Displays;
                default:
                    Debug.Assert(false, "PlanogramPositionMerchandisingStyle unknown.");
                    return SpacemanMerchStyle.Units;
            }
        }

        static public SpacemanMerchStyle EncodeMerchandisingStyle(PlanogramPositionMerchandisingStyle type)
        {
            switch (type)
            {
                case PlanogramPositionMerchandisingStyle.Alternate:
                case PlanogramPositionMerchandisingStyle.PointOfPurchase:
                case PlanogramPositionMerchandisingStyle.Default:
                case PlanogramPositionMerchandisingStyle.Unit:
                    return SpacemanMerchStyle.Units;

                case PlanogramPositionMerchandisingStyle.Tray:
                    return SpacemanMerchStyle.Trays;

                case PlanogramPositionMerchandisingStyle.Case:
                    return SpacemanMerchStyle.Cases;

                case PlanogramPositionMerchandisingStyle.Display:
                    return SpacemanMerchStyle.Displays;
                default:
                    Debug.Assert(false, "PlanogramPositionMerchandisingStyle unknown.");
                    return SpacemanMerchStyle.Units;
            }
        }

        /// <summary>
        /// Checks to see if spaceman will allow the given merchstylebased on known limitations created by other properties.
        /// </summary>
        /// <param name="posMerchStyle"></param>
        /// <param name="posDto"></param>
        /// <param name="prodDto"></param>
        /// <remarks>Spaceman only allows merch style if has dimensions provided.</remarks>
        public static Boolean MerchStyleIsAllowedInSpaceman(PlanogramPositionDto posDto, PlanogramProductDto prodDto)
        {
            PlanogramPositionMerchandisingStyle posMerchStyle = (PlanogramPositionMerchandisingStyle)posDto.MerchandisingStyle;

            switch (posMerchStyle)
            {
                case PlanogramPositionMerchandisingStyle.Default:
                    //Check the product.
                    switch (prodDto.MerchandisingStyle)
	                {
                       case (Byte)PlanogramProductMerchandisingStyle.Case:
                            if (prodDto.TrayDepth == 0 || prodDto.TrayHeight == 0 || prodDto.TrayWidth == 0)
                            {
                                return false;
                            }
                            break;

                       case (Byte)PlanogramProductMerchandisingStyle.Tray:
                            if (prodDto.CaseDepth == 0 || prodDto.CaseHeight == 0 || prodDto.CaseWidth == 0)
                            {
                                return true;
                            }
                            break;

                       case (Byte)PlanogramProductMerchandisingStyle.Display:
                            if (prodDto.DisplayDepth == 0 || prodDto.DisplayHeight == 0 || prodDto.DisplayWidth == 0)
                            {
                                return false;
                            }
                            break;

                        case (Byte)PlanogramProductMerchandisingStyle.Unit:
                        case (Byte)PlanogramProductMerchandisingStyle.Alternate:
                        case (Byte)PlanogramProductMerchandisingStyle.PointOfPurchase:
                            return true;

		                default:
                            return true;
	                }
                    break;
                
                case PlanogramPositionMerchandisingStyle.Tray:
                    if (prodDto.TrayDepth == 0 || prodDto.TrayHeight == 0 || prodDto.TrayWidth == 0)
                    {
                        return false;
                    }
                    break;

                case PlanogramPositionMerchandisingStyle.Case:
                    if (prodDto.CaseDepth == 0 || prodDto.CaseHeight == 0 || prodDto.CaseWidth == 0)
                    {
                        return false;
                    }
                    break;

                case PlanogramPositionMerchandisingStyle.Display:
                    if (prodDto.DisplayDepth == 0 || prodDto.DisplayHeight == 0 || prodDto.DisplayWidth == 0)
                    {
                        return false;
                    }
                    break;

                case PlanogramPositionMerchandisingStyle.Unit:
                case PlanogramPositionMerchandisingStyle.Alternate:
                case PlanogramPositionMerchandisingStyle.PointOfPurchase:
                    //always passes as will be a unit
                    return true;

                default:
                    return true;
            }

            return true;
        }


        /// <summary>
        /// Gets the friedly for the appropriate merch style
        /// </summary>
        /// <param name="planogramPositionDto"></param>
        /// <param name="planogramProductDto"></param>
        /// <param name="merchStyleFriendly"></param>
        public static void GetMerchStyleFriendly(PlanogramPositionDto planogramPositionDto, PlanogramProductDto planogramProductDto, out String merchStyleFriendly)
        {
            if ((PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyle != PlanogramPositionMerchandisingStyle.Default)
            {
                merchStyleFriendly = PlanogramPositionMerchandisingStyleHelper.FriendlyNames[(PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyle];
            }
            else
            {
                merchStyleFriendly = PlanogramProductMerchandisingStyleHelper.FriendlyNames[(PlanogramProductMerchandisingStyle)planogramProductDto.MerchandisingStyle];
            }
        }

        public static Boolean IsNotASpacemanType(PlanogramPositionDto planogramPositionDto, PlanogramProductDto planogramProductDto)
        {
            return (PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyle == PlanogramPositionMerchandisingStyle.PointOfPurchase
                    || (PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default && (PlanogramProductMerchandisingStyle)planogramProductDto.MerchandisingStyle == PlanogramProductMerchandisingStyle.PointOfPurchase
                    || (PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Alternate
                    || (PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default && (PlanogramProductMerchandisingStyle)planogramProductDto.MerchandisingStyle == PlanogramProductMerchandisingStyle.Alternate;
        }


    }
}
