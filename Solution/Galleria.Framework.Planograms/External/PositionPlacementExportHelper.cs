﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32266 : M.Pettit
//  Created
// V8-32375 : M.Pettit
//  Orientations now take account of position defaults using product values
// V8-32601 : M.Pettit
//  Block dimensions now take account of tray/case pack merchandising
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.External
{
    /// <summary>
    /// Provides position-related calculations using passed PositionDto and ProductDto Data
    /// Transfer Objects. Differs from the PlanogramPositionDetails / PlanogramPositionPlacement 
    /// classes in that it deliberately does not take finger space into account as Apollo requires
    /// its data this way.
    /// </summary>
    public static class PositionPlacementExportHelper
    {       
        /// <summary>
        /// Gets the width of the main block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetMainBlockWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            //if (IsMainBlockMerchandisedInCases(positionDto, productDto))
            //{
            //    return positionDto.FacingsWide * GetMainBlockOrientatedCaseWidth(positionDto, productDto);
            //}
            //if (IsMainBlockMerchandisedInTrays(positionDto, productDto))
            //{
            //    return positionDto.FacingsWide * GetMainBlockOrientatedTrayWidth(positionDto, productDto);
            //}
            return positionDto.FacingsWide * GetMainBlockUnitWidth(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the height of the main block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetMainBlockHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            //if (IsMainBlockMerchandisedInCases(positionDto, productDto))
            //{
            //    return positionDto.FacingsHigh * GetMainBlockOrientatedCaseHeight(positionDto, productDto);
            //}
            //if (IsMainBlockMerchandisedInTrays(positionDto, productDto))
            //{
            //    return positionDto.FacingsHigh * GetMainBlockOrientatedTrayHeight(positionDto, productDto);
            //}
            return positionDto.FacingsHigh * GetMainBlockUnitHeight(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the depth of the main block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetMainBlockDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            //if (IsMainBlockMerchandisedInCases(positionDto, productDto))
            //{
            //    return positionDto.FacingsDeep * GetMainBlockOrientatedCaseDepth(positionDto, productDto);
            //}
            //if (IsMainBlockMerchandisedInTrays(positionDto, productDto))
            //{
            //    return positionDto.FacingsDeep * GetMainBlockOrientatedTrayDepth(positionDto, productDto);
            //}
            return positionDto.FacingsDeep * GetMainBlockUnitDepth(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the width of the left cap block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetLeftCapBlockWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            //if (IsLeftRightCapMerchandisedInCases(positionDto, productDto))
            //{
            //    return positionDto.FacingsXWide * GetLeftRightCapOrientatedCaseWidth(positionDto, productDto);
            //}
            //if (IsLeftRightCapMerchandisedInTrays(positionDto, productDto))
            //{
            //    return positionDto.FacingsXWide * GetLeftRightCapOrientatedTrayWidth(positionDto, productDto);
            //}
            return positionDto.FacingsXWide * GetLeftRightCapUnitWidth(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the height of the left cap block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetLeftCapBlockHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            //if (IsLeftRightCapMerchandisedInCases(positionDto, productDto))
            //{
            //    return positionDto.FacingsXHigh * GetLeftRightCapOrientatedCaseHeight(positionDto, productDto);
            //}
            //if (IsLeftRightCapMerchandisedInTrays(positionDto, productDto))
            //{
            //    return positionDto.FacingsXHigh * GetLeftRightCapOrientatedTrayHeight(positionDto, productDto);
            //} 
            return positionDto.FacingsXHigh * GetLeftRightCapUnitHeight(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the depth of the left cap block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetLeftCapBlockDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            //if (IsLeftRightCapMerchandisedInCases(positionDto, productDto))
            //{
            //    return positionDto.FacingsXDeep * GetLeftRightCapOrientatedCaseDepth(positionDto, productDto);
            //}
            //if (IsLeftRightCapMerchandisedInTrays(positionDto, productDto))
            //{
            //    return positionDto.FacingsXDeep * GetLeftRightCapOrientatedTrayDepth(positionDto, productDto);
            //} 
            return positionDto.FacingsXDeep * GetLeftRightCapUnitDepth(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the width of the right cap block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetRightCapBlockWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            return GetLeftCapBlockWidth(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the height of the right cap block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetRightCapBlockHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            return GetLeftCapBlockHeight(positionDto, productDto); 
        }

        /// <summary>
        /// Gets the depth of the right cap block for a position.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The converted value</returns>
        public static Single GetRightCapBlockDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            return GetLeftCapBlockDepth(positionDto, productDto); 
        }

        #region Unit Dimensions By Block/Cap Type

        /// <summary>
        /// Gets the width of a single position unit (1 product or if in a tray, 1 tray, or 1 case) based on the passed orientation.
        /// Note:   This value is used for the Apollo position PO_XDIM value
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The position unit width</returns>
        public static Single GetMainBlockUnitWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (IsMainBlockMerchandisedInCases(positionDto, productDto))
            {
                return GetMainBlockOrientatedCaseWidth(positionDto, productDto);
            }
            if (IsMainBlockMerchandisedInTrays(positionDto, productDto))
            {
                return GetMainBlockOrientatedTrayWidth(positionDto, productDto);
            } 

            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueeze;
                        return productDto.Width;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueeze;
                        return productDto.Height;

                    default:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Depth * positionDto.HorizontalSqueeze;
                        return productDto.Width;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueeze;
                        return productDto.Width;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueeze;
                        return productDto.Height;

                    default:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Depth * positionDto.HorizontalSqueeze;
                        return productDto.Width;
                }
            }
        }

        /// <summary>
        /// Gets the height of a single position unit (1 product or if in a tray, 1 tray, or 1 case) based on the passed orientation.
        /// Note:   This value is used for the Apollo position PO_YDIM value for normal elements, 
        ///         or for PO_ZDIM for positions placed in coffins or on hanging bars (for these element types height and depth are switched)
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The position height</returns>
        public static Single GetMainBlockUnitHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (IsMainBlockMerchandisedInCases(positionDto, productDto))
            {
                return GetMainBlockOrientatedCaseHeight(positionDto, productDto);
            }
            if (IsMainBlockMerchandisedInTrays(positionDto, productDto))
            {
                return GetMainBlockOrientatedTrayHeight(positionDto, productDto);
            } 
            
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueeze;
                        return productDto.Height;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueeze;
                        return productDto.Width;

                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;

                    default:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Depth * positionDto.VerticalSqueeze;
                        return productDto.Height;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueeze;
                        return productDto.Height;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueeze;
                        return productDto.Width;

                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;

                    default:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Depth * positionDto.VerticalSqueeze;
                        return productDto.Height;
                }
            }
        }

        /// <summary>
        /// Gets the depth of a single position unit (1 product or if in a tray, 1 tray, or 1 case) based on the passed orientation.
        /// Note:   This value is used for the Apollo position PO_ZDIM value for normal elements, 
        ///         or for PO_YDIM for positions placed in coffins or on hanging bars (for these element types height and depth are switched)
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The position depth</returns>
        public static Single GetMainBlockUnitDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (IsMainBlockMerchandisedInCases(positionDto, productDto))
            {
                return GetMainBlockOrientatedCaseDepth(positionDto, productDto);
            }
            if (IsMainBlockMerchandisedInTrays(positionDto, productDto))
            {
                return GetMainBlockOrientatedTrayDepth(positionDto, productDto);
            } 
            
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueeze;
                        return productDto.Width;

                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueeze;
                        return productDto.Height;

                    default:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;
                };
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Back270:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        if (MainBlockPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueeze;
                        return productDto.Width;

                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        if (MainBlockPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueeze;
                        return productDto.Height;

                    default:
                        if (MainBlockPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueeze;
                        return productDto.Depth;
                };
            }
        }

        /// <summary>
        /// Gets the width of a single position unit (1 product or if in a tray, 1 tray, or 1 case) based on the passed orientation.
        /// Note:   This value is used for the Apollo position PO_XDIM value
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The position width</returns>
        public static Single GetLeftRightCapUnitWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (IsLeftRightCapMerchandisedInCases(positionDto, productDto))
            {
                return GetLeftRightCapOrientatedCaseWidth(positionDto, productDto);
            }
            if (IsLeftRightCapMerchandisedInTrays(positionDto, productDto))
            {
                return GetLeftRightCapOrientatedTrayWidth(positionDto, productDto);
            } 
            
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueezeX;
                        return productDto.Width;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueezeX;
                        return productDto.Height;

                    default:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Depth * positionDto.HorizontalSqueezeX;
                        return productDto.Width;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueezeX;
                        return productDto.Width;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueezeX;
                        return productDto.Height;

                    default:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Depth * positionDto.HorizontalSqueezeX;
                        return productDto.Width;
                }
            }
        }

        /// <summary>
        /// Gets the height of a single position unit (1 product or if in a tray, 1 tray, or 1 case) based on the passed orientation.
        /// Note:   This value is used for the Apollo position PO_YDIM value for normal elements, 
        ///         or for PO_ZDIM for positions placed in coffins or on hanging bars (for these element types height and depth are switched)
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The position height</returns>
        public static Single GetLeftRightCapUnitHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (IsLeftRightCapMerchandisedInCases(positionDto, productDto))
            {
                return GetLeftRightCapOrientatedCaseHeight(positionDto, productDto);
            }
            if (IsLeftRightCapMerchandisedInTrays(positionDto, productDto))
            {
                return GetLeftRightCapOrientatedTrayHeight(positionDto, productDto);
            } 
            
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueezeX;
                        return productDto.Height;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueezeX;
                        return productDto.Width;

                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;

                    default:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Depth * positionDto.VerticalSqueezeX;
                        return productDto.Height;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueezeX;
                        return productDto.Height;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueezeX;
                        return productDto.Width;

                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;

                    default:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Depth * positionDto.VerticalSqueezeX;
                        return productDto.Height;
                }
            }
        }

        /// <summary>
        /// Gets the depth of a single position unit (1 product or if in a tray, 1 tray, or 1 case) based on the passed orientation.
        /// Note:   This value is used for the Apollo position PO_ZDIM value for normal elements, 
        ///         or for PO_YDIM for positions placed in coffins or on hanging bars (for these element types height and depth are switched)
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>The position depth</returns>
        public static Single GetLeftRightCapUnitDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (IsLeftRightCapMerchandisedInCases(positionDto, productDto))
            {
                return GetLeftRightCapOrientatedCaseDepth(positionDto, productDto);
            }
            if (IsLeftRightCapMerchandisedInTrays(positionDto, productDto))
            {
                return GetLeftRightCapOrientatedTrayDepth(positionDto, productDto);
            } 
            
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueezeX;
                        return productDto.Width;

                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueezeX;
                        return productDto.Height;

                    default:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;
                };
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Back270:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        if (LeftRightCapPositionHasSqueezeWidth(positionDto)) return productDto.Width * positionDto.HorizontalSqueezeX;
                        return productDto.Width;

                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        if (LeftRightCapPositionHasSqueezeHeight(positionDto)) return productDto.Height * positionDto.VerticalSqueezeX;
                        return productDto.Height;

                    default:
                        if (LeftRightCapPositionHasSqueezeDepth(positionDto)) return productDto.Depth * positionDto.DepthSqueezeX;
                        return productDto.Depth;
                };
            }
        }

        #endregion

        #region Main Block Squeeze Check
        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean MainBlockPositionHasSqueezeHeight(PlanogramPositionDto dto)
        {
            if (dto.VerticalSqueeze > 0 && dto.VerticalSqueeze < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean MainBlockPositionHasSqueezeWidth(PlanogramPositionDto dto)
        {
            if (dto.HorizontalSqueeze > 0 && dto.HorizontalSqueeze < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean MainBlockPositionHasSqueezeDepth(PlanogramPositionDto dto)
        {
            if (dto.DepthSqueeze > 0 && dto.DepthSqueeze < 1) return true;
            return false;
        }
        #endregion

        #region Left/Right Cap Block position Squeeze Tests
        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean LeftRightCapPositionHasSqueezeHeight(PlanogramPositionDto dto)
        {
            if (dto.VerticalSqueezeX > 0 && dto.VerticalSqueezeX < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean LeftRightCapPositionHasSqueezeWidth(PlanogramPositionDto dto)
        {
            if (dto.HorizontalSqueezeX > 0 && dto.HorizontalSqueezeX < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean LeftRightCapPositionHasSqueezeDepth(PlanogramPositionDto dto)
        {
            if (dto.DepthSqueezeX > 0 && dto.DepthSqueezeX < 1) return true;
            return false;
        }
        #endregion

        #region Top/Bottom Cap Block position Squeeze Tests
        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean TopBottomCapPositionHasSqueezeHeight(PlanogramPositionDto dto)
        {
            if (dto.VerticalSqueezeY > 0 && dto.VerticalSqueezeY < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean TopBottomCapPositionHasSqueezeWidth(PlanogramPositionDto dto)
        {
            if (dto.HorizontalSqueezeY > 0 && dto.HorizontalSqueezeY < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean TopBottomCapPositionHasSqueezeDepth(PlanogramPositionDto dto)
        {
            if (dto.DepthSqueezeY > 0 && dto.DepthSqueezeY < 1) return true;
            return false;
        }
        #endregion

        #region Front/Back Cap Block position Squeeze Tests
        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean FrontBackCapPositionHasSqueezeHeight(PlanogramPositionDto dto)
        {
            if (dto.VerticalSqueezeZ > 0 && dto.VerticalSqueezeZ < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean FrontBackCapPositionHasSqueezeWidth(PlanogramPositionDto dto)
        {
            if (dto.HorizontalSqueezeZ > 0 && dto.HorizontalSqueezeZ < 1) return true;
            return false;
        }

        /// <summary>
        /// Determines whether a position has a particular squeeze value set
        /// </summary>
        /// <param name="dto">The position to test</param>
        /// <returns>True if the position has the particular squeeze value</returns>
        private static Boolean FrontBackCapPositionHasSqueezeDepth(PlanogramPositionDto dto)
        {
            if (dto.DepthSqueezeZ > 0 && dto.DepthSqueezeZ < 1) return true;
            return false;
        }
        #endregion

        #region Case or Tray Merchandising
        /// <summary>
        /// Determines whether the position is merchandised in a tray pack
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>True if merchandised in trays, else False</returns>
        private static Boolean IsMainBlockMerchandisedInTrays(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (positionDto.MerchandisingStyle > 0)
            {
                return positionDto.MerchandisingStyle == (Byte)PlanogramPositionMerchandisingStyle.Tray;
            }
            else
            {
                return productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Tray;
            }
        }

        /// <summary>
        /// Determines whether the position is merchandised in a tray pack
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>True if merchandised in trays, else False</returns>
        private static Boolean IsLeftRightCapMerchandisedInTrays(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (positionDto.MerchandisingStyleX > 0)
            {
                return positionDto.MerchandisingStyleX == (Byte)PlanogramPositionMerchandisingStyle.Tray;
            }
            else
            {
                return productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Tray;
            }
        }

        /// <summary>
        /// Determines whether the position is merchandised in a case
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>True if merchandised in cases, else False</returns>
        private static Boolean IsMainBlockMerchandisedInCases(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (positionDto.MerchandisingStyle > 0)
            {
                return positionDto.MerchandisingStyle == (Byte)PlanogramPositionMerchandisingStyle.Case;
            }
            else
            {
                return productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Case;
            }
        }


        /// <summary>
        /// Determines whether the position is merchandised in a case
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <returns>True if merchandised in cases, else False</returns>
        private static Boolean IsLeftRightCapMerchandisedInCases(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (positionDto.MerchandisingStyleX > 0)
            {
                return positionDto.MerchandisingStyleX == (Byte)PlanogramPositionMerchandisingStyle.Case;
            }
            else
            {
                return productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Case;
            }
        }
        #endregion

        #region Case Dimensions
        /// <summary>
        /// Gets the physical height of a single case based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The height of the single case position</returns>
        private static Single GetMainBlockOrientatedCaseHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                        return productDto.CaseHeight;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.CaseDepth;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                        return productDto.CaseHeight;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.CaseDepth;
                }
            }
            return productDto.CaseHeight;
        }

        /// <summary>
        /// Gets the physical width of a single case based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The width of the single case position</returns>
        private static Single GetMainBlockOrientatedCaseWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.CaseHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.CaseHeight;
                }
            }
            return productDto.CaseWidth;
        }

        /// <summary>
        /// Gets the physical depth of a single case based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The depth of the single case position</returns>
        private static Single GetMainBlockOrientatedCaseDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.CaseHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Back270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.CaseHeight;
                }
            }
            return productDto.CaseDepth;
        }

        /// <summary>
        /// Gets the physical height of a single case based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The height of the single case position</returns>
        private static Single GetLeftRightCapOrientatedCaseHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                        return productDto.CaseHeight;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.CaseDepth;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                        return productDto.CaseHeight;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.CaseDepth;
                }
            }
            return productDto.CaseHeight;
        }

        /// <summary>
        /// Gets the physical width of a single case based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The width of the single case position</returns>
        private static Single GetLeftRightCapOrientatedCaseWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.CaseHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.CaseHeight;
                }
            }
            return productDto.CaseWidth;
        }

        /// <summary>
        /// Gets the physical depth of a single case based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The depth of the single case position</returns>
        private static Single GetLeftRightCapOrientatedCaseDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.CaseHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Back270:
                        return productDto.CaseDepth;

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.CaseWidth;

                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.CaseHeight;
                }
            }
            return productDto.CaseDepth;
        }
        #endregion


        #region Tray Dimensions
        /// <summary>
        /// Gets the physical height of a single tray based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The height of the single tray position</returns>
        private static Single GetMainBlockOrientatedTrayHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
                }
            }
            return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
        }

        /// <summary>
        /// Gets the physical width of a single tray based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The width of the single tray position</returns>
        private static Single GetMainBlockOrientatedTrayWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);
        }

        /// <summary>
        /// Gets the physical depth of a single tray based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The depth of the single tray position</returns>
        private static Single GetMainBlockOrientatedTrayDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationType)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Back270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
        }

        /// <summary>
        /// Gets the physical height of a single tray based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The height of the single tray position</returns>
        private static Single GetLeftRightCapOrientatedTrayHeight(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
                }
            }
            return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
        }

        /// <summary>
        /// Gets the physical width of a single tray based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The width of the single tray position</returns>
        private static Single GetLeftRightCapOrientatedTrayWidth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back270:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);
        }

        /// <summary>
        /// Gets the physical depth of a single tray based on its orientation
        /// </summary>
        /// <param name="positionDto"></param>
        /// <param name="productDto"></param>
        /// <returns>The depth of the single tray position</returns>
        private static Single GetLeftRightCapOrientatedTrayDepth(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if ((PlanogramPositionOrientationType)positionDto.OrientationTypeX != PlanogramPositionOrientationType.Default)
            {
                switch (positionDto.OrientationTypeX)
                {
                    case (Byte)PlanogramPositionOrientationType.Front0:
                    case (Byte)PlanogramPositionOrientationType.Front90:
                    case (Byte)PlanogramPositionOrientationType.Front180:
                    case (Byte)PlanogramPositionOrientationType.Front270:
                    case (Byte)PlanogramPositionOrientationType.Back0:
                    case (Byte)PlanogramPositionOrientationType.Back90:
                    case (Byte)PlanogramPositionOrientationType.Back180:
                    case (Byte)PlanogramPositionOrientationType.Back270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramPositionOrientationType.Left0:
                    case (Byte)PlanogramPositionOrientationType.Left90:
                    case (Byte)PlanogramPositionOrientationType.Left180:
                    case (Byte)PlanogramPositionOrientationType.Left270:
                    case (Byte)PlanogramPositionOrientationType.Right0:
                    case (Byte)PlanogramPositionOrientationType.Right90:
                    case (Byte)PlanogramPositionOrientationType.Right180:
                    case (Byte)PlanogramPositionOrientationType.Right270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramPositionOrientationType.Top0:
                    case (Byte)PlanogramPositionOrientationType.Top180:
                    case (Byte)PlanogramPositionOrientationType.Bottom0:
                    case (Byte)PlanogramPositionOrientationType.Bottom180:
                    case (Byte)PlanogramPositionOrientationType.Top90:
                    case (Byte)PlanogramPositionOrientationType.Top270:
                    case (Byte)PlanogramPositionOrientationType.Bottom90:
                    case (Byte)PlanogramPositionOrientationType.Bottom270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            else
            {
                switch (productDto.OrientationType)
                {
                    case (Byte)PlanogramProductOrientationType.Front0:
                    case (Byte)PlanogramProductOrientationType.Front90:
                    case (Byte)PlanogramProductOrientationType.Front180:
                    case (Byte)PlanogramProductOrientationType.Front270:
                    case (Byte)PlanogramProductOrientationType.Back0:
                    case (Byte)PlanogramProductOrientationType.Back90:
                    case (Byte)PlanogramProductOrientationType.Back180:
                    case (Byte)PlanogramProductOrientationType.Back270:
                        return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);

                    case (Byte)PlanogramProductOrientationType.Left0:
                    case (Byte)PlanogramProductOrientationType.Left90:
                    case (Byte)PlanogramProductOrientationType.Left180:
                    case (Byte)PlanogramProductOrientationType.Left270:
                    case (Byte)PlanogramProductOrientationType.Right0:
                    case (Byte)PlanogramProductOrientationType.Right90:
                    case (Byte)PlanogramProductOrientationType.Right180:
                    case (Byte)PlanogramProductOrientationType.Right270:
                        return productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);

                    case (Byte)PlanogramProductOrientationType.Top0:
                    case (Byte)PlanogramProductOrientationType.Top180:
                    case (Byte)PlanogramProductOrientationType.Bottom0:
                    case (Byte)PlanogramProductOrientationType.Bottom180:
                    case (Byte)PlanogramProductOrientationType.Top90:
                    case (Byte)PlanogramProductOrientationType.Top270:
                    case (Byte)PlanogramProductOrientationType.Bottom90:
                    case (Byte)PlanogramProductOrientationType.Bottom270:
                        return productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
            }
            return productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
        }
        #endregion
    }
}
