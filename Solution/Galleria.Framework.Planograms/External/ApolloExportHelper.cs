﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31547 : M.Pettit
//  Created
// V8-32141 : M.Pettit
//  Updated DefaultPogeft
// V8-31548 : M.Pettit
//  Moved standard position placement methods to new class
// V8-32321 : M.Pettit
//  Amended default mapping for SE_NAME to be Planogram Name, not CustomAttributeData.Text2 or FixtureName
// V8-32390 : M.Pettit
//  Amended default mapping so that V8 Planogram Date01,02 fields  are now unmapped
// V8-32375 : M.pettit
//  Re-worked merch Style to take from product if position set to default
// V8-32375 : M.Pettit
//  Orientations now take account of position defaults using product values
// V8-32620 : M.Pettit
//  Case calculations (GetApolloProductCaseHeight/Width/Depth) now take account of multi-sited positions
// V8-32738 : M.Pettit
//  Added missing SECTIONDESCX01 - SECTIONDESCX49 fields
// CCM-18559 : M.Pettit
//  Shelves with CanCombine set must be given a spreadmode of Manual to retain any -ve xposition position locations
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System.Reflection;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using System.Globalization;
using Galleria.Framework.Planograms.External.ApolloStructure;

namespace Galleria.Framework.Planograms.External
{
    public static class ApolloExportHelper
    {
        #region Fields
        private static object[] parameters = new object[1]; // to cut down on garbage
        private static Dictionary<Type, Dictionary<String, ObjectFieldInfo>> _typeInfoLookup = new Dictionary<Type, Dictionary<String, ObjectFieldInfo>>();
        private static List<String> _availableVersions;
        private static Dictionary<String, ObjectFieldInfo> _planogramFieldInfos;
        private static Dictionary<String, ObjectFieldInfo> _componentFieldInfos;
        private static Dictionary<String, ObjectFieldInfo> _fixtureFieldInfos;
        private static Dictionary<String, ObjectFieldInfo> _productFieldInfos;
        private static Dictionary<String, ObjectFieldInfo> _performanceFieldInfos;
        private static String SourceFormat { get { return "{0}|{1}"; } }
        
        private static Dictionary<Type, String> _typeFriendlyNames = new Dictionary<Type, String>()
            {
                {typeof(ApolloDataSetInfo), Message.ApolloImport_Info},
                {typeof(ApolloDataSetStore),  Message.ApolloImport_Store},
                {typeof(ApolloDataSetRetailer), Message.ApolloImport_Retailer},
                {typeof(ApolloDataSetSectionSectionData), Message.ApolloImport_Section},
                {typeof(ApolloDataSetSectionShelvesShelfShelfData),Message.ApolloImport_Component},      
                {typeof(ApolloDataSetProductsProduct),Message.ApolloImport_Product},
                {typeof(ApolloDataSetSectionSectionProductsSectionProduct), Message.ApolloImport_SectionProduct},
                {typeof(PositionsPosition), Message.ApolloImport_Position},
            };


        private readonly static Dictionary<Type, Dictionary<String, PropertyInfo>> _typePropertyLookups = new Dictionary<Type, Dictionary<String, PropertyInfo>>()
            {                
                {typeof(ApolloDataSet), typeof(ApolloDataSet).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetInfo), typeof(ApolloDataSetInfo).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetStore), typeof(ApolloDataSetStore).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetRetailer),typeof(ApolloDataSetRetailer).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetSectionSectionData), typeof(ApolloDataSetSectionSectionData).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetSectionShelvesShelfShelfData),typeof(ApolloDataSetSectionShelvesShelfShelfData).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetProductsProduct),typeof(ApolloDataSetProductsProduct).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetSectionSectionProductsSectionProduct), typeof(ApolloDataSetSectionSectionProductsSectionProduct).GetProperties().ToDictionary(p => p.Name)},                                
                {typeof(ApolloDataSetSectionShelvesShelf),typeof(ApolloDataSetSectionShelvesShelf).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetSectionShelves),typeof(ApolloDataSetSectionShelves).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetSection),typeof(ApolloDataSetSection).GetProperties().ToDictionary(p => p.Name)},
                {typeof(ApolloDataSetProducts),typeof(ApolloDataSetProducts).GetProperties().ToDictionary(p => p.Name)},
                {typeof(Positions),typeof(Positions).GetProperties().ToDictionary(p => p.Name)},
                {typeof(PositionsPosition),typeof(PositionsPosition).GetProperties().ToDictionary(p => p.Name)},
            };
        private readonly static Dictionary<Type, Dictionary<String, MethodInfo>> _typeSetterLookups = new Dictionary<Type, Dictionary<String, MethodInfo>>()
            {                
                {typeof(ApolloDataSet), GetSetterDic<ApolloDataSet>()},
                {typeof(ApolloDataSetInfo), GetSetterDic<ApolloDataSetInfo>()},
                {typeof(ApolloDataSetStore), GetSetterDic<ApolloDataSetStore>()},
                {typeof(ApolloDataSetRetailer),GetSetterDic<ApolloDataSetRetailer>()},
                {typeof(ApolloDataSetSectionSectionData), GetSetterDic<ApolloDataSetSectionSectionData>()},
                {typeof(ApolloDataSetSectionShelvesShelfShelfData),GetSetterDic<ApolloDataSetSectionShelvesShelfShelfData>()},
                {typeof(ApolloDataSetProductsProduct), GetSetterDic<ApolloDataSetProductsProduct>()},
                {typeof(ApolloDataSetSectionSectionProductsSectionProduct),  GetSetterDic<ApolloDataSetSectionSectionProductsSectionProduct>()},                               
                {typeof(ApolloDataSetSectionShelvesShelf), GetSetterDic<ApolloDataSetSectionShelvesShelf>()},
                {typeof(ApolloDataSetSectionShelves), GetSetterDic<ApolloDataSetSectionShelves>()},
                {typeof(ApolloDataSetSection), GetSetterDic<ApolloDataSetSection>()},
                {typeof(ApolloDataSetProducts), GetSetterDic<ApolloDataSetProducts>()},
                {typeof(Positions), GetSetterDic<Positions>()},
                {typeof(PositionsPosition), GetSetterDic<PositionsPosition>()},
            };

        private readonly static Dictionary<Type, ConstructorInfo> _typeConstructorLookups = new Dictionary<Type, ConstructorInfo>()
            {                
                {typeof(ApolloDataSet), typeof(ApolloDataSet).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetInfo), typeof(ApolloDataSetInfo).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetStore), typeof(ApolloDataSetStore).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetRetailer),typeof(ApolloDataSetRetailer).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetSectionSectionData), typeof(ApolloDataSetSectionSectionData).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetSectionShelvesShelfShelfData),typeof(ApolloDataSetSectionShelvesShelfShelfData).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetProductsProduct),typeof(ApolloDataSetProductsProduct).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetSectionSectionProductsSectionProduct), typeof(ApolloDataSetSectionSectionProductsSectionProduct).GetConstructor(System.Type.EmptyTypes)},                                
                {typeof(ApolloDataSetSectionShelvesShelf),typeof(ApolloDataSetSectionShelvesShelf).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetSectionShelves),typeof(ApolloDataSetSectionShelves).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetSection),typeof(ApolloDataSetSection).GetConstructor(System.Type.EmptyTypes)},
                {typeof(ApolloDataSetProducts),typeof(ApolloDataSetProducts).GetConstructor(System.Type.EmptyTypes)},
                {typeof(Positions),typeof(Positions).GetConstructor(System.Type.EmptyTypes)},
                {typeof(PositionsPosition),typeof(PositionsPosition).GetConstructor(System.Type.EmptyTypes)},
            };

       #endregion

        #region PropertyNames

        #region ApolloDataSetSectionSectionData
        private static String _sectionName = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_NAME");
        private static String _sectionALLOWFLOAT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_ALLOWFLOAT");
        private static String _sectionALLOWOVERHANG = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_ALLOWOVERHANG");
        private static String _sectionALLOWOVERLAP = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_ALLOWOVERLAP");
        private static String _sectionARBITRARY = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_ARBITRARY");
        private static String _sectionAVAILABLECUBIC = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_AVAILABLECUBIC");
        private static String _sectionAVAILABLELINEAR = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_AVAILABLELINEAR");
        private static String _sectionAVAILABLESQUARE = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_AVAILABLESQUARE");
        private static String _sectionAVERAGECASES = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_AVERAGECASES");
        private static String _sectionAVERAGEFACINGS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_AVERAGEFACINGS");
        private static String _sectionBACKGROUND = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_BACKGROUND");
        private static String _sectionCAPACITY = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_CAPACITY");
        private static String _sectionCASES = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_CASES");
        private static String _sectionCREATEDATE = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_CREATEDATE");
        private static String _sectionCRUNCH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_CRUNCH");
        private static String _sectionCUBICMULTIPLIER = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_CUBICMULTIPLIER");
        private static String _sectionCUBICTHROUGHPUT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_CUBICTHROUGHPUT");
        private static String _sectionDAYSINWEEK = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DAYSINWEEK");
        private static String _sectionDECOHEIGHT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DECOHEIGHT");
        private static String _sectionDECOTEXT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DECOTEXT");
        private static String _sectionDECOWIDTH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DECOWIDTH");
        private static String _sectionDECOXPOS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DECOXPOS");
        private static String _sectionDECOYPOS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DECOYPOS");
        private static String _sectionDEPTH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DEPTH");
        private static String _sectionDPP = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_DPP");
        private static String _sectionFACINGS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_FACINGS");
        private static String _sectionFIRSTNOTCH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_FIRSTNOTCH");
        private static String _sectionFOREGROUND = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_FOREGROUND");
        private static String _sectionHEIGHT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_HEIGHT");
        private static String _sectionHICASES = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_HICASES");
        private static String _sectionHIDAYS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_HIDAYS");
        private static String _sectionHIFACINGS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_HIFACINGS");
        private static String _sectionINCREMENT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_INCREMENT");
        private static String _sectionINVATRETAIL = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_INVATRETAIL");
        private static String _sectionINVENTORY = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_INVENTORY");
        private static String _sectionLINEARMULTIPLIER = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_LINEARMULTIPLIER");
        private static String _sectionLOGOPATH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_LOGOPATH");
        private static String _sectionLOWCASES = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_LOWCASES");
        private static String _sectionLOWDAYS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_LOWDAYS");
        private static String _sectionLOWFACINGS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_LOWFACINGS");
        private static String _sectionMERCH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_MERCH");
        private static String _sectionMESSAGELINE1 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_MESSAGELINE1");
        private static String _sectionMESSAGELINE2 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_MESSAGELINE2");
        private static String _sectionMESSAGELINE3 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_MESSAGELINE3");
        private static String _sectionMESSAGELINE4 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_MESSAGELINE4");
        private static String _sectionMESSAGELINE5 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_MESSAGELINE5");
        private static String _sectionMOVEMENT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_MOVEMENT");
        private static String _sectionNOTCH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_NOTCH");
        private static String _sectionNUMSHELVES = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_NUMSHELVES");
        private static String _sectionNUMSKUS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_NUMSKUS");
        private static String _sectionPLOTDESC1 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_PLOTDESC1");
        private static String _sectionPLOTDESC2 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_PLOTDESC2");
        private static String _sectionPLOTDESC3 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_PLOTDESC3");
        private static String _sectionPROFIT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_PROFIT");
        private static String _sectionRESERVED1 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_RESERVED1");
        private static String _sectionRESERVED2 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_RESERVED2");
        private static String _sectionRESERVED3 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_RESERVED3");
        private static String _sectionRESERVED4 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_RESERVED4");
        private static String _sectionRESERVED5 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_RESERVED5");
        private static String _sectionRESERVED6 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_RESERVED6");
        private static String _sectionROII = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_ROII");
        private static String _sectionSALES = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_SALES");
        private static String _sectionSECTION = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_SECTION");
        private static String _sectionSECTIONDESC = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_SECTIONDESC");
        private static String _sectionSECTIONFLAGSBIT2 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_SECTIONFLAGSBIT2");
        private static String _sectionSECTIONID = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_SECTIONID");
        private static String _sectionSHOWDECO = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_SHOWDECO");
        private static String _sectionSQUAREMULTIPLIER = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_SQUAREMULTIPLIER");
        private static String _sectionSTAGGEREDSHELVES = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_STAGGEREDSHELVES");
        private static String _sectionSTANDARDLENGTH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_STANDARDLENGTH");
        private static String _sectionTRAFFIC = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_TRAFFIC");
        private static String _sectionUPDATEDATE = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_UPDATEDATE");
        private static String _sectionUPDATETIME = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_UPDATETIME");
        private static String _sectionUPRIGHT = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_UPRIGHT");
        private static String _sectionUPRIGHTBACKGROUND = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_UPRIGHTBACKGROUND");
        private static String _sectionUPRIGHTFOREGROUND = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_UPRIGHTFOREGROUND");
        private static String _sectionUSEDCUBIC = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_USEDCUBIC");
        private static String _sectionUSEDLINEAR = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_USEDLINEAR");
        private static String _sectionUSEDSQUARE = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_USEDSQUARE");
        private static String _sectionUSER1 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_USER1");
        private static String _sectionUSER2 = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_USER2");
        private static String _sectionVERSION = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_VERSION");
        private static String _sectionVERSIONDESC = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_VERSIONDESC");
        private static String _sectionWIDTH = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_WIDTH");
        private static String _sectionXPOS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_XPOS");
        private static String _sectionYPOS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_YPOS");
        private static String _sectionZPOS = CombinePropertyName<ApolloDataSetSectionSectionData>("SE_ZPOS");
        private static String _sectionX03_SEDESCRESERVE02 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCRESERVE02");
        private static String _sectionX03_SEDESCRESERVE03 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCRESERVE03");
        private static String _sectionX03_SEDESCRESERVE04 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCRESERVE04");
        private static String _sectionX03_SEDESCRESERVE05 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCRESERVE05");
        private static String _sectionX03_SEDESCRESERVE07 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCRESERVE07");
        private static String _sectionX03_SEDESCX01 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX01");
        private static String _sectionX03_SEDESCX02 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX02");
        private static String _sectionX03_SEDESCX03 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX03");
        private static String _sectionX03_SEDESCX04 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX04");
        private static String _sectionX03_SEDESCX05 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX05");
        private static String _sectionX03_SEDESCX06 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX06");
        private static String _sectionX03_SEDESCX07 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX07");
        private static String _sectionX03_SEDESCX08 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX08");
        private static String _sectionX03_SEDESCX09 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX09");
        private static String _sectionX03_SEDESCX10 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX10");
        private static String _sectionX03_SEDESCX11 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX11");
        private static String _sectionX03_SEDESCX12 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX12");
        private static String _sectionX03_SEDESCX13 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX13");
        private static String _sectionX03_SEDESCX14 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX14");
        private static String _sectionX03_SEDESCX15 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX15");
        private static String _sectionX03_SEDESCX16 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX16");
        private static String _sectionX03_SEDESCX17 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX17");
        private static String _sectionX03_SEDESCX18 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX18");
        private static String _sectionX03_SEDESCX19 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX19");
        private static String _sectionX03_SEDESCX20 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX20");
        private static String _sectionX03_SEDESCX21 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX21");
        private static String _sectionX03_SEDESCX22 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX22");
        private static String _sectionX03_SEDESCX23 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX23");
        private static String _sectionX03_SEDESCX24 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX24");
        private static String _sectionX03_SEDESCX25 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX25");
        private static String _sectionX03_SEDESCX26 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX26");
        private static String _sectionX03_SEDESCX27 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX27");
        private static String _sectionX03_SEDESCX28 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX28");
        private static String _sectionX03_SEDESCX29 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX29");
        private static String _sectionX03_SEDESCX30 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX30");
        private static String _sectionX03_SEDESCX31 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX31");
        private static String _sectionX03_SEDESCX32 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX32");
        private static String _sectionX03_SEDESCX33 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX33");
        private static String _sectionX03_SEDESCX34 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX34");
        private static String _sectionX03_SEDESCX35 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX35");
        private static String _sectionX03_SEDESCX36 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX36");
        private static String _sectionX03_SEDESCX37 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX37");
        private static String _sectionX03_SEDESCX38 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX38");
        private static String _sectionX03_SEDESCX39 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX39");
        private static String _sectionX03_SEDESCX40 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX40");
        private static String _sectionX03_SEDESCX41 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX41");
        private static String _sectionX03_SEDESCX42 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX42");
        private static String _sectionX03_SEDESCX43 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX43");
        private static String _sectionX03_SEDESCX44 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX44");
        private static String _sectionX03_SEDESCX45 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX45");
        private static String _sectionX03_SEDESCX46 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX46");
        private static String _sectionX03_SEDESCX47 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX47");
        private static String _sectionX03_SEDESCX48 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX48");
        private static String _sectionX03_SEDESCX49 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX49");
        private static String _sectionX03_SEDESCX50 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEDESCX50");
        private static String _sectionX03_SEMEASRESERVE01 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE01");
        private static String _sectionX03_SEMEASRESERVE02 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE02");
        private static String _sectionX03_SEMEASRESERVE03 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE03");
        private static String _sectionX03_SEMEASRESERVE04 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE04");
        private static String _sectionX03_SEMEASRESERVE05 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE05");
        private static String _sectionX03_SEMEASRESERVE06 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE06");
        private static String _sectionX03_SEMEASRESERVE07 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE07");
        private static String _sectionX03_SEMEASRESERVE08 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE08");
        private static String _sectionX03_SEMEASRESERVE09 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE09");
        private static String _sectionX03_SEMEASRESERVE10 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE10");
        private static String _sectionX03_SEMEASRESERVE11 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE11");
        private static String _sectionX03_SEMEASRESERVE12 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE12");
        private static String _sectionX03_SEMEASRESERVE13 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE13");
        private static String _sectionX03_SEMEASRESERVE14 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE14");
        private static String _sectionX03_SEMEASRESERVE15 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE15");
        private static String _sectionX03_SEMEASRESERVE16 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE16");
        private static String _sectionX03_SEMEASRESERVE17 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE17");
        private static String _sectionX03_SEMEASRESERVE18 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE18");
        private static String _sectionX03_SEMEASRESERVE19 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE19");
        private static String _sectionX03_SEMEASRESERVE20 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASRESERVE20");
        private static String _sectionX03_SEMEASX01 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX01");
        private static String _sectionX03_SEMEASX02 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX02");
        private static String _sectionX03_SEMEASX03 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX03");
        private static String _sectionX03_SEMEASX04 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX04");
        private static String _sectionX03_SEMEASX05 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX05");
        private static String _sectionX03_SEMEASX06 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX06");
        private static String _sectionX03_SEMEASX07 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX07");
        private static String _sectionX03_SEMEASX08 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX08");
        private static String _sectionX03_SEMEASX09 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX09");
        private static String _sectionX03_SEMEASX10 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX10");
        private static String _sectionX03_SEMEASX11 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX11");
        private static String _sectionX03_SEMEASX12 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX12");
        private static String _sectionX03_SEMEASX13 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX13");
        private static String _sectionX03_SEMEASX14 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX14");
        private static String _sectionX03_SEMEASX15 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX15");
        private static String _sectionX03_SEMEASX16 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX16");
        private static String _sectionX03_SEMEASX17 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX17");
        private static String _sectionX03_SEMEASX18 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX18");
        private static String _sectionX03_SEMEASX19 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX19");
        private static String _sectionX03_SEMEASX20 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX20");
        private static String _sectionX03_SEMEASX21 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX21");
        private static String _sectionX03_SEMEASX22 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX22");
        private static String _sectionX03_SEMEASX23 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX23");
        private static String _sectionX03_SEMEASX24 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX24");
        private static String _sectionX03_SEMEASX25 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX25");
        private static String _sectionX03_SEMEASX26 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX26");
        private static String _sectionX03_SEMEASX27 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX27");
        private static String _sectionX03_SEMEASX28 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX28");
        private static String _sectionX03_SEMEASX29 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX29");
        private static String _sectionX03_SEMEASX30 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX30");
        private static String _sectionX03_SEMEASX31 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX31");
        private static String _sectionX03_SEMEASX32 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX32");
        private static String _sectionX03_SEMEASX33 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX33");
        private static String _sectionX03_SEMEASX34 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX34");
        private static String _sectionX03_SEMEASX35 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX35");
        private static String _sectionX03_SEMEASX36 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX36");
        private static String _sectionX03_SEMEASX37 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX37");
        private static String _sectionX03_SEMEASX38 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX38");
        private static String _sectionX03_SEMEASX39 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX39");
        private static String _sectionX03_SEMEASX40 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX40");
        private static String _sectionX03_SEMEASX41 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX41");
        private static String _sectionX03_SEMEASX42 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX42");
        private static String _sectionX03_SEMEASX43 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX43");
        private static String _sectionX03_SEMEASX44 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX44");
        private static String _sectionX03_SEMEASX45 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX45");
        private static String _sectionX03_SEMEASX46 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX46");
        private static String _sectionX03_SEMEASX47 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX47");
        private static String _sectionX03_SEMEASX48 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX48");
        private static String _sectionX03_SEMEASX49 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX49");
        private static String _sectionX03_SEMEASX50 = CombinePropertyName<ApolloDataSetSectionSectionData>("X03_SEMEASX50");
        #endregion ApolloDataSetSectionSectionData

        #region ApolloDataSetStore
        private static String _storeACV = CombinePropertyName<ApolloDataSetStore>("ST_ACV");
        private static String _storeADDRESS1 = CombinePropertyName<ApolloDataSetStore>("ST_ADDRESS1");
        private static String _storeADDRESS2 = CombinePropertyName<ApolloDataSetStore>("ST_ADDRESS2");
        private static String _storeCITY = CombinePropertyName<ApolloDataSetStore>("ST_CITY");
        private static String _storeCLUSTER = CombinePropertyName<ApolloDataSetStore>("ST_CLUSTER");
        private static String _storeCONTACT = CombinePropertyName<ApolloDataSetStore>("ST_CONTACT");
        private static String _storeDEPTH = CombinePropertyName<ApolloDataSetStore>("ST_DEPTH");
        private static String _storeDISTRICT = CombinePropertyName<ApolloDataSetStore>("ST_DISTRICT");
        private static String _storeHEIGHT = CombinePropertyName<ApolloDataSetStore>("ST_HEIGHT");
        private static String _storeNAME = CombinePropertyName<ApolloDataSetStore>("ST_NAME");
        private static String _storePHONE = CombinePropertyName<ApolloDataSetStore>("ST_PHONE");
        private static String _storeSTATE = CombinePropertyName<ApolloDataSetStore>("ST_STATE");
        private static String _storeSTORE = CombinePropertyName<ApolloDataSetStore>("ST_STORE");
        private static String _storeUSER1 = CombinePropertyName<ApolloDataSetStore>("ST_USER1");
        private static String _storeUSER2 = CombinePropertyName<ApolloDataSetStore>("ST_USER2");
        private static String _storeUSER3 = CombinePropertyName<ApolloDataSetStore>("ST_USER3");
        private static String _storeUSER4 = CombinePropertyName<ApolloDataSetStore>("ST_USER4");
        private static String _storeWIDTH = CombinePropertyName<ApolloDataSetStore>("ST_WIDTH");
        private static String _storeZIP = CombinePropertyName<ApolloDataSetStore>("ST_ZIP");
        #endregion ApolloDataSetStore

        #region ApolloDataSetRetailer

        private static String _retailerADDRESS1 = CombinePropertyName<ApolloDataSetRetailer>("RE_ADDRESS1");
        private static String _retailerADDRESS2 = CombinePropertyName<ApolloDataSetRetailer>("RE_ADDRESS2");
        private static String _retailerCITY = CombinePropertyName<ApolloDataSetRetailer>("RE_CITY");
        private static String _retailerCONTACT = CombinePropertyName<ApolloDataSetRetailer>("RE_CONTACT");
        private static String _retailerNAME = CombinePropertyName<ApolloDataSetRetailer>("RE_NAME");
        private static String _retailerNUMSTORES = CombinePropertyName<ApolloDataSetRetailer>("RE_NUMSTORES");
        private static String _retailerPHONE = CombinePropertyName<ApolloDataSetRetailer>("RE_PHONE");
        private static String _retailerRETAILER = CombinePropertyName<ApolloDataSetRetailer>("RE_RETAILER");
        private static String _retailerSTATE = CombinePropertyName<ApolloDataSetRetailer>("RE_STATE");
        private static String _retailerZIP = CombinePropertyName<ApolloDataSetRetailer>("RE_ZIP");

        #endregion ApolloDataSetRetailer

        #region ApolloDataSetSectionShelvesShelfShelfData Fields
        private static String _componentName = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SHELF");
        private static String _componentAUTOCALCFRONTS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_AUTOCALCFRONTS");
        private static String _componentAVAILABLECUBIC = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_AVAILABLECUBIC");
        private static String _componentAVAILABLELINEAR = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_AVAILABLELINEAR");
        private static String _componentAVAILABLESQUARE = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_AVAILABLESQUARE");
        private static String _componentAVERAGECASES = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_AVERAGECASES");
        private static String _componentAVERAGEFACINGS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_AVERAGEFACINGS");
        private static String _componentBACKGROUND = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_BACKGROUND");
        private static String _componentBRACKETDEPTH = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_BRACKETDEPTH");
        private static String _componentBRACKETHEIGHT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_BRACKETHEIGHT");
        private static String _componentBRACKETINCREMENT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_BRACKETINCREMENT");
        private static String _componentBRACKETSLOPE = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_BRACKETSLOPE");
        private static String _componentBRACKETTHICKNESS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_BRACKETTHICKNESS");
        private static String _componentCAPACITY = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_CAPACITY");
        private static String _componentCASES = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_CASES");
        private static String _componentCUBICTHROUGHPUT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_CUBICTHROUGHPUT");
        private static String _componentDISPLAYNUM = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_DISPLAYNUM");
        private static String _componentDIVINC = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_DIVINC");
        private static String _componentDIVTHICK = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_DIVTHICK");
        private static String _componentDPP = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_DPP");
        private static String _componentEDGE = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_EDGE");
        private static String _componentFACINGS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_FACINGS");
        private static String _componentFITSTATUS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_FITSTATUS");
        private static String _componentFOREGROUND = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_FOREGROUND");
        private static String _componentGRILLHEIGHT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_GRILLHEIGHT");
        private static String _componentGRILLTHICKNESS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_GRILLTHICKNESS");
        private static String _componentHICASES = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_HICASES");
        private static String _componentHIDAYS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_HIDAYS");
        private static String _componentHIFACINGS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_HIFACINGS");
        private static String _componentINVATRETAIL = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_INVATRETAIL");
        private static String _componentINVENTORY = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_INVENTORY");
        private static String _componentLOWCASES = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_LOWCASES");
        private static String _componentLOWDAYS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_LOWDAYS");
        private static String _componentLOWFACINGS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_LOWFACINGS");
        private static String _componentMAXMERCHDEPTH = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_MAXMERCHDEPTH");
        private static String _componentMERCHDEPTH = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_MERCHDEPTH");
        private static String _componentMERCHHEIGHT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_MERCHHEIGHT");
        private static String _componentMOVEMENT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_MOVEMENT");
        private static String _componentNOTCHNUMBER = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_NOTCHNUMBER");
        private static String _componentNUMSKUS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_NUMSKUS");
        private static String _componentPROFIT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_PROFIT");
        private static String _componentRESERVED1 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_RESERVED1");
        private static String _componentRESERVED2 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_RESERVED2");
        private static String _componentRESERVED3 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_RESERVED3");
        private static String _componentRESERVED4 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_RESERVED4");
        private static String _componentRESERVED5 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_RESERVED5");
        private static String _componentRESERVED6 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_RESERVED6");
        private static String _componentROII = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_ROII");
        private static String _componentSALES = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SALES");
        private static String _componentSHELFDEPTH = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SHELFDEPTH");
        private static String _componentSHELFHEIGHT = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SHELFHEIGHT");
        private static String _componentSHELFTYPE = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SHELFTYPE");
        private static String _componentSHELFWIDTH = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SHELFWIDTH");
        private static String _componentSLOPE = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SLOPE");
        private static String _componentSPREADMODE = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_SPREADMODE");
        private static String _componentTHICKNESS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_THICKNESS");
        private static String _componentUSEDCUBIC = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USEDCUBIC");
        private static String _componentUSEDLINEAR = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USEDLINEAR");
        private static String _componentUSEDSQUARE = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USEDSQUARE");
        private static String _componentUSERDESC1 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USERDESC1");
        private static String _componentUSERDESC2 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USERDESC2");
        private static String _componentUSERDESC3 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USERDESC3");
        private static String _componentUSERDESC4 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USERDESC4");
        private static String _componentUSERNUM1 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USERNUM1");
        private static String _componentUSERNUM2 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USERNUM2");
        private static String _componentUSERNUM3 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_USERNUM3");
        private static String _componentXINC = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_XINC");
        private static String _componentXPEGEND = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_XPEGEND");
        private static String _componentXPEGSTART = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_XPEGSTART");
        private static String _componentXPOS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_XPOS");
        private static String _componentYINC = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_YINC");
        private static String _componentYPEGEND = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_YPEGEND");
        private static String _componentYPEGSTART = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_YPEGSTART");
        private static String _componentYPOS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_YPOS");
        private static String _componentZPOS = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("SH_ZPOS");
        private static String _componentX04_SHDESCX50 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHDESCX50");
        private static String _componentX04_SHMEASRESERVE01 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE01");
        private static String _componentX04_SHMEASRESERVE02 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE02");
        private static String _componentX04_SHMEASRESERVE03 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE03");
        private static String _componentX04_SHMEASRESERVE04 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE04");
        private static String _componentX04_SHMEASRESERVE05 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE05");
        private static String _componentX04_SHMEASRESERVE06 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE06");
        private static String _componentX04_SHMEASRESERVE07 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE07");
        private static String _componentX04_SHMEASRESERVE08 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE08");
        private static String _componentX04_SHMEASRESERVE09 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE09");
        private static String _componentX04_SHMEASRESERVE10 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE10");
        private static String _componentX04_SHMEASRESERVE11 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE11");
        private static String _componentX04_SHMEASRESERVE12 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE12");
        private static String _componentX04_SHMEASRESERVE13 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE13");
        private static String _componentX04_SHMEASRESERVE14 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE14");
        private static String _componentX04_SHMEASRESERVE15 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE15");
        private static String _componentX04_SHMEASRESERVE16 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE16");
        private static String _componentX04_SHMEASRESERVE17 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE17");
        private static String _componentX04_SHMEASRESERVE18 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE18");
        private static String _componentX04_SHMEASRESERVE19 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE19");
        private static String _componentX04_SHMEASRESERVE20 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASRESERVE20");
        private static String _componentX04_SHMEASX01 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX01");
        private static String _componentX04_SHMEASX02 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX02");
        private static String _componentX04_SHMEASX03 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX03");
        private static String _componentX04_SHMEASX04 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX04");
        private static String _componentX04_SHMEASX05 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX05");
        private static String _componentX04_SHMEASX06 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX06");
        private static String _componentX04_SHMEASX07 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX07");
        private static String _componentX04_SHMEASX08 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX08");
        private static String _componentX04_SHMEASX09 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX09");
        private static String _componentX04_SHMEASX10 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX10");
        private static String _componentX04_SHMEASX11 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX11");
        private static String _componentX04_SHMEASX12 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX12");
        private static String _componentX04_SHMEASX13 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX13");
        private static String _componentX04_SHMEASX14 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX14");
        private static String _componentX04_SHMEASX15 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX15");
        private static String _componentX04_SHMEASX16 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX16");
        private static String _componentX04_SHMEASX17 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX17");
        private static String _componentX04_SHMEASX18 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX18");
        private static String _componentX04_SHMEASX19 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX19");
        private static String _componentX04_SHMEASX20 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX20");
        private static String _componentX04_SHMEASX21 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX21");
        private static String _componentX04_SHMEASX22 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX22");
        private static String _componentX04_SHMEASX23 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX23");
        private static String _componentX04_SHMEASX24 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX24");
        private static String _componentX04_SHMEASX25 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX25");
        private static String _componentX04_SHMEASX26 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX26");
        private static String _componentX04_SHMEASX27 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX27");
        private static String _componentX04_SHMEASX28 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX28");
        private static String _componentX04_SHMEASX29 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX29");
        private static String _componentX04_SHMEASX30 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX30");
        private static String _componentX04_SHMEASX31 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX31");
        private static String _componentX04_SHMEASX32 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX32");
        private static String _componentX04_SHMEASX33 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX33");
        private static String _componentX04_SHMEASX34 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX34");
        private static String _componentX04_SHMEASX35 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX35");
        private static String _componentX04_SHMEASX36 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX36");
        private static String _componentX04_SHMEASX37 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX37");
        private static String _componentX04_SHMEASX38 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX38");
        private static String _componentX04_SHMEASX39 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX39");
        private static String _componentX04_SHMEASX40 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX40");
        private static String _componentX04_SHMEASX41 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX41");
        private static String _componentX04_SHMEASX42 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX42");
        private static String _componentX04_SHMEASX43 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX43");
        private static String _componentX04_SHMEASX44 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX44");
        private static String _componentX04_SHMEASX45 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX45");
        private static String _componentX04_SHMEASX46 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX46");
        private static String _componentX04_SHMEASX47 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX47");
        private static String _componentX04_SHMEASX48 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX48");
        private static String _componentX04_SHMEASX49 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX49");
        private static String _componentX04_SHMEASX50 = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>("X04_SHMEASX50");
        #endregion ApolloDataSetSectionShelvesShelfShelfData Fields

        #region ApolloDataSetProductsProduct Fields
        private static String _productUPC = CombinePropertyName<ApolloDataSetProductsProduct>("PR_UPC");
        private static String _productSTOCKCODE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_STOCKCODE");
        private static String _productLONGDESC = CombinePropertyName<ApolloDataSetProductsProduct>("PR_LONGDESC");
        private static String _productBRAND = CombinePropertyName<ApolloDataSetProductsProduct>("PR_BRAND");
        private static String _productSIZE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SIZE");
        private static String _productUOM = CombinePropertyName<ApolloDataSetProductsProduct>("PR_UOM");
        private static String _productSCREENDESC = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SCREENDESC");
        private static String _productCHAR = CombinePropertyName<ApolloDataSetProductsProduct>("PR_CHAR");
        private static String _productCOLOR = CombinePropertyName<ApolloDataSetProductsProduct>("PR_COLOR");
        private static String _productPLOTDESC1 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_PLOTDESC1");
        private static String _productPLOTDESC2 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_PLOTDESC2");
        private static String _productSYMBOL = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SYMBOL");
        private static String _productCATEGORY = CombinePropertyName<ApolloDataSetProductsProduct>("PR_CATEGORY");
        private static String _productSUBCATEGORY = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SUBCATEGORY");
        private static String _productCOMPONENT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_COMPONENT");
        private static String _productMANUFACTURER = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MANUFACTURER");
        private static String _productPACKAGE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_PACKAGE");
        private static String _productDISTRIBUTION = CombinePropertyName<ApolloDataSetProductsProduct>("PR_DISTRIBUTION");
        private static String _productMULTIPACK = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MULTIPACK");
        private static String _productCASEPACK = CombinePropertyName<ApolloDataSetProductsProduct>("PR_CASEPACK");
        private static String _productMOVEMENTADJ = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MOVEMENTADJ");
        private static String _productPRODHEIGHT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_PRODHEIGHT");
        private static String _productPRODWIDTH = CombinePropertyName<ApolloDataSetProductsProduct>("PR_PRODWIDTH");
        private static String _productPRODDEPTH = CombinePropertyName<ApolloDataSetProductsProduct>("PR_PRODDEPTH");
        private static String _productCASEHEIGHT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_CASEHEIGHT");
        private static String _productCASEWIDTH = CombinePropertyName<ApolloDataSetProductsProduct>("PR_CASEWIDTH");
        private static String _productCASEDEPTH = CombinePropertyName<ApolloDataSetProductsProduct>("PR_CASEDEPTH");
        private static String _productCASEWEIGHT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_CASEWEIGHT");
        private static String _productNESTING = CombinePropertyName<ApolloDataSetProductsProduct>("PR_NESTING");
        private static String _productOVERHANG = CombinePropertyName<ApolloDataSetProductsProduct>("PR_OVERHANG");
        private static String _productXPEGHOLE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_XPEGHOLE");
        private static String _productYPEGHOLE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_YPEGHOLE");
        private static String _productFINGER = CombinePropertyName<ApolloDataSetProductsProduct>("PR_FINGER");
        private static String _productMAXFRONTS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXFRONTS");
        private static String _productMAXLAYOVERS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXLAYOVERS");
        private static String _productADJTOCASECOST = CombinePropertyName<ApolloDataSetProductsProduct>("PR_ADJTOCASECOST");
        private static String _productARRIVEAS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_ARRIVEAS");
        private static String _productAVRNOCASESDSDSH = CombinePropertyName<ApolloDataSetProductsProduct>("PR_AVRNOCASESDSDSH");
        private static String _productEXPECTEDMOVEMENT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_EXPECTEDMOVEMENT");
        private static String _productMAXCASES = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXCASES");
        private static String _productMAXDAYS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXDAYS");
        private static String _productMAXFACING = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXFACING");
        private static String _productMAXFRONTSDEEP = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXFRONTSDEEP");
        private static String _productMAXHEIGHT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXHEIGHT");
        private static String _productMAXUNITS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MAXUNITS");
        private static String _productMINCASES = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MINCASES");
        private static String _productMINDAYS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MINDAYS");
        private static String _productMINFACING = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MINFACING");
        private static String _productMINUNITS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_MINUNITS");
        private static String _productNETDAYSCREDIT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_NETDAYSCREDIT");
        private static String _productNOCASESPALLET = CombinePropertyName<ApolloDataSetProductsProduct>("PR_NOCASESPALLET");
        private static String _productNOINNERPACKS = CombinePropertyName<ApolloDataSetProductsProduct>("PR_NOINNERPACKS");
        private static String _productNOTRAYSCASE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_NOTRAYSCASE");
        private static String _productPATTERN = CombinePropertyName<ApolloDataSetProductsProduct>("PR_PATTERN");
        private static String _productRACKOPENHT = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RACKOPENHT");
        private static String _productREPLENISHMETHOD = CombinePropertyName<ApolloDataSetProductsProduct>("PR_REPLENISHMETHOD");
        private static String _productRESERVED1 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RESERVED1");
        private static String _productRESERVED2 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RESERVED2");
        private static String _productRESERVED3 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RESERVED3");
        private static String _productRESERVED4 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RESERVED4");
        private static String _productRESERVED5 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RESERVED5");
        private static String _productRESERVED6 = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RESERVED6");
        private static String _productRESTOCK = CombinePropertyName<ApolloDataSetProductsProduct>("PR_RESTOCK");
        private static String _productSECTIONID = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SECTIONID");
        private static String _productSECTIONSHARE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SECTIONSHARE");
        private static String _productSERVICE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SERVICE");
        private static String _productSHARE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_SHARE");
        private static String _productSTOREPRICEMETHOD = CombinePropertyName<ApolloDataSetProductsProduct>("PR_STOREPRICEMETHOD");
        private static String _productSTORERECUNLOAD = CombinePropertyName<ApolloDataSetProductsProduct>("PR_STORERECUNLOAD");
        private static String _productUSERDESCA = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCA");
        private static String _productUSERDESCB = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCB");
        private static String _productUSERDESCC = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCC");
        private static String _productUSERDESCD = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCD");
        private static String _productUSERDESCE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCE");
        private static String _productUSERDESCF = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCF");
        private static String _productUSERDESCG = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCG");
        private static String _productUSERDESCH = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCH");
        private static String _productUSERDESCI = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCI");
        private static String _productUSERDESCJ = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERDESCJ");
        private static String _productUSERMEASUREA = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREA");
        private static String _productUSERMEASUREB = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREB");
        private static String _productUSERMEASUREC = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREC");
        private static String _productUSERMEASURED = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASURED");
        private static String _productUSERMEASUREE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREE");
        private static String _productUSERMEASUREF = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREF");
        private static String _productUSERMEASUREG = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREG");
        private static String _productUSERMEASUREH = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREH");
        private static String _productUSERMEASUREI = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREI");
        private static String _productUSERMEASUREJ = CombinePropertyName<ApolloDataSetProductsProduct>("PR_USERMEASUREJ");
        private static String _productVARIANCE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_VARIANCE");
        private static String _productWARERECEIVE = CombinePropertyName<ApolloDataSetProductsProduct>("PR_WARERECEIVE");
        private static String _productX06_DESCX01 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX01");
        private static String _productX06_DESCX02 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX02");
        private static String _productX06_DESCX03 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX03");
        private static String _productX06_DESCX04 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX04");
        private static String _productX06_DESCX05 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX05");
        private static String _productX06_DESCX06 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX06");
        private static String _productX06_DESCX07 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX07");
        private static String _productX06_DESCX08 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX08");
        private static String _productX06_DESCX09 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX09");
        private static String _productX06_DESCX10 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_DESCX10");
        private static String _productX06_MEASRESERVE01 = CombinePropertyName<ApolloDataSetProductsProduct>("X06_MEASRESERVE01");


        #endregion ApolloDataSetProductsProduct Fields

        #region ApolloDataSetSectionSectionProductsSectionProduct
        private static String _sectionProductRealMovement = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_REALMOVEMENT");
        private static String _sectionProductCost = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_COST");
        private static String _sectionProductCaseCost = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_CASECOST");
        private static String _sectionProductVAT = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_VAT");
        private static String _sectionProductRETAIL = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_RETAIL");
        private static String _sectionProductDPCA = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_DPCA");
        private static String _sectionProductPAYDISCOUNT = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_PAYDISCOUNT");
        private static String _sectionProductSELLINGPRICE = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_SELLINGPRICE");
        private static String _sectionProductMARGIN = CombinePropertyName<ApolloDataSetSectionSectionProductsSectionProduct>("PR_MARGIN");
        #endregion ApolloDataSetSectionSectionProductsSectionProduct

        #region PositionsPositon fields
        private static String _positionSales = CombinePropertyName<PositionsPosition>("PO_SALES");
        private static String _positionACTUALMARGIN = CombinePropertyName<PositionsPosition>("PO_ACTUALMARGIN");
        private static String _positionADJUSTEDMOVEMENT = CombinePropertyName<PositionsPosition>("PO_ADJUSTEDMOVEMENT");
        private static String _positionBACKROOMSTOCK = CombinePropertyName<PositionsPosition>("PO_BACKROOMSTOCK");
        private static String _positionCAPACITY = CombinePropertyName<PositionsPosition>("PO_CAPACITY");
        private static String _positionCAPACITYPERCUBIC = CombinePropertyName<PositionsPosition>("PO_CAPACITYPERCUBIC");
        private static String _positionCAPACITYPERLINEAR = CombinePropertyName<PositionsPosition>("PO_CAPACITYPERLINEAR");
        private static String _positionCAPACITYPERSQUARE = CombinePropertyName<PositionsPosition>("PO_CAPACITYPERSQUARE");
        private static String _positionCASES = CombinePropertyName<PositionsPosition>("PO_CASES");
        private static String _positionCONTRIBTOMARGIN = CombinePropertyName<PositionsPosition>("PO_CONTRIBTOMARGIN");
        private static String _positionCUBIC = CombinePropertyName<PositionsPosition>("PO_CUBIC");
        private static String _positionDAYSOFSUPPLY = CombinePropertyName<PositionsPosition>("PO_DAYSOFSUPPLY");
        private static String _positionDECOTEXT = CombinePropertyName<PositionsPosition>("PO_DECOTEXT");
        private static String _positionDISTMOVEMENT = CombinePropertyName<PositionsPosition>("PO_DISTMOVEMENT");
        private static String _positionDPP = CombinePropertyName<PositionsPosition>("PO_DPP");
        private static String _positionDPPPERCENT = CombinePropertyName<PositionsPosition>("PO_DPPPERCENT");
        private static String _positionDPPPERCUBIC = CombinePropertyName<PositionsPosition>("PO_DPPPERCUBIC");
        private static String _positionDPPPERLINEAR = CombinePropertyName<PositionsPosition>("PO_DPPPERLINEAR");
        private static String _positionDPPPERSQUARE = CombinePropertyName<PositionsPosition>("PO_DPPPERSQUARE");
        private static String _positionDPPPERUNIT = CombinePropertyName<PositionsPosition>("PO_DPPPERUNIT");
        private static String _positionEDGE = CombinePropertyName<PositionsPosition>("PO_EDGE");
        private static String _positionFITSTATUS = CombinePropertyName<PositionsPosition>("PO_FITSTATUS");
        private static String _positionGMROI = CombinePropertyName<PositionsPosition>("PO_GMROI");
        private static String _positionINVCOSTPERCUBIC = CombinePropertyName<PositionsPosition>("PO_INVCOSTPERCUBIC");
        private static String _positionINVCOSTPERLINEAR = CombinePropertyName<PositionsPosition>("PO_INVCOSTPERLINEAR");
        private static String _positionINVCOSTPERSQUARE = CombinePropertyName<PositionsPosition>("PO_INVCOSTPERSQUARE");
        private static String _positionINVENTORY = CombinePropertyName<PositionsPosition>("PO_INVENTORY");
        private static String _positionINVENTORYATRETAIL = CombinePropertyName<PositionsPosition>("PO_INVENTORYATRETAIL");
        private static String _positionINVRETPERCUBIC = CombinePropertyName<PositionsPosition>("PO_INVRETPERCUBIC");
        private static String _positionINVRETPERLINEAR = CombinePropertyName<PositionsPosition>("PO_INVRETPERLINEAR");
        private static String _positionINVRETPERSQUARE = CombinePropertyName<PositionsPosition>("PO_INVRETPERSQUARE");
        private static String _positionLAYOVERS = CombinePropertyName<PositionsPosition>("PO_LAYOVERS");
        private static String _positionLAYOVERSDEEP = CombinePropertyName<PositionsPosition>("PO_LAYOVERSDEEP");
        private static String _positionLINEAR = CombinePropertyName<PositionsPosition>("PO_LINEAR");
        private static String _positionLOSTSALES = CombinePropertyName<PositionsPosition>("PO_LOSTSALES");
        private static String _positionMERCHANDISINGTYPE = CombinePropertyName<PositionsPosition>("PO_MERCHANDISINGTYPE");
        private static String _positionMOVEMENTPERCUBIC = CombinePropertyName<PositionsPosition>("PO_MOVEMENTPERCUBIC");
        private static String _positionMOVEMENTPERLINEAR = CombinePropertyName<PositionsPosition>("PO_MOVEMENTPERLINEAR");
        private static String _positionMOVEMENTPERSQUARE = CombinePropertyName<PositionsPosition>("PO_MOVEMENTPERSQUARE");
        private static String _positionMULTICAPACITY = CombinePropertyName<PositionsPosition>("PO_MULTICAPACITY");
        private static String _positionMULTIORIENT = CombinePropertyName<PositionsPosition>("PO_MULTIORIENT");
        private static String _positionNORMALSTOCK = CombinePropertyName<PositionsPosition>("PO_NORMALSTOCK");
        private static String _positionORIENTATION = CombinePropertyName<PositionsPosition>("PO_ORIENTATION");
        private static String _positionPCTCAPACITYPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTCAPACITYPERSECT");
        private static String _positionPCTCAPACITYPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTCAPACITYPERSHF");
        private static String _positionPCTCUBICPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTCUBICPERSECT");
        private static String _positionPCTCUBICPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTCUBICPERSHF");
        private static String _positionPCTDPPPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTDPPPERSECT");
        private static String _positionPCTDPPPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTDPPPERSHF");
        private static String _positionPCTINVCOSTPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTINVCOSTPERSECT");
        private static String _positionPCTINVCOSTPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTINVCOSTPERSHF");
        private static String _positionPCTLINEARPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTLINEARPERSECT");
        private static String _positionPCTLINEARPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTLINEARPERSHF");
        private static String _positionPCTMOVEMENTPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTMOVEMENTPERSECT");
        private static String _positionPCTMOVEMENTPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTMOVEMENTPERSHF");
        private static String _positionPCTPROFITPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTPROFITPERSECT");
        private static String _positionPCTPROFITPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTPROFITPERSHF");
        private static String _positionPCTSALESPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTSALESPERSECT");
        private static String _positionPCTSALESPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTSALESPERSHF");
        private static String _positionPCTSQUAREPERSECT = CombinePropertyName<PositionsPosition>("PO_PCTSQUAREPERSECT");
        private static String _positionPCTSQUAREPERSHF = CombinePropertyName<PositionsPosition>("PO_PCTSQUAREPERSHF");
        private static String _positionPEGLENGTH = CombinePropertyName<PositionsPosition>("PO_PEGLENGTH");
        private static String _positionPEGTYPE = CombinePropertyName<PositionsPosition>("PO_PEGTYPE");
        private static String _positionPOSITION = CombinePropertyName<PositionsPosition>("PO_POSITION");
        private static String _positionPROFIT = CombinePropertyName<PositionsPosition>("PO_PROFIT");
        private static String _positionPROFITPERCUBIC = CombinePropertyName<PositionsPosition>("PO_PROFITPERCUBIC");
        private static String _positionPROFITPERLINEAR = CombinePropertyName<PositionsPosition>("PO_PROFITPERLINEAR");
        private static String _positionPROFITPERSQUARE = CombinePropertyName<PositionsPosition>("PO_PROFITPERSQUARE");
        private static String _positionPROFITPERUNIT = CombinePropertyName<PositionsPosition>("PO_PROFITPERUNIT");
        private static String _positionRESERVED1 = CombinePropertyName<PositionsPosition>("PO_RESERVED1");
        private static String _positionRESERVED2 = CombinePropertyName<PositionsPosition>("PO_RESERVED2");
        private static String _positionRESERVED5 = CombinePropertyName<PositionsPosition>("PO_RESERVED5");
        private static String _positionROII = CombinePropertyName<PositionsPosition>("PO_ROII");
        private static String _positionSAFETYSTOCK = CombinePropertyName<PositionsPosition>("PO_SAFETYSTOCK");
        private static String _positionSALESGROWTH = CombinePropertyName<PositionsPosition>("PO_SALESGROWTH");
        private static String _positionSALESPERCUBIC = CombinePropertyName<PositionsPosition>("PO_SALESPERCUBIC");
        private static String _positionSALESPERFACING = CombinePropertyName<PositionsPosition>("PO_SALESPERFACING");
        private static String _positionSALESPERLINEAR = CombinePropertyName<PositionsPosition>("PO_SALESPERLINEAR");
        private static String _positionSALESPERSQUARE = CombinePropertyName<PositionsPosition>("PO_SALESPERSQUARE");
        private static String _positionSQUARE = CombinePropertyName<PositionsPosition>("PO_SQUARE");
        private static String _positionSUGGESTEDFACINGS = CombinePropertyName<PositionsPosition>("PO_SUGGESTEDFACINGS");
        private static String _positionTAGNUMBER = CombinePropertyName<PositionsPosition>("PO_TAGNUMBER");
        private static String _positionTURNS = CombinePropertyName<PositionsPosition>("PO_TURNS");
        private static String _positionUPC = CombinePropertyName<PositionsPosition>("PO_UPC");
        private static String _positionWEEKSOFSUPPLY = CombinePropertyName<PositionsPosition>("PO_WEEKSOFSUPPLY");
        private static String _positionX_ENDPOS = CombinePropertyName<PositionsPosition>("PO_X_ENDPOS");
        private static String _positionXDIM = CombinePropertyName<PositionsPosition>("PO_XDIM");
        private static String _positionXFACINGS = CombinePropertyName<PositionsPosition>("PO_XFACINGS");
        private static String _positionXGAP = CombinePropertyName<PositionsPosition>("PO_XGAP");
        private static String _positionXPEGHOLEPOS = CombinePropertyName<PositionsPosition>("PO_XPEGHOLEPOS");
        private static String _positionXPEGNUM = CombinePropertyName<PositionsPosition>("PO_XPEGNUM");
        private static String _positionXPOS = CombinePropertyName<PositionsPosition>("PO_XPOS");
        private static String _positionY_ENDPOS = CombinePropertyName<PositionsPosition>("PO_Y_ENDPOS");
        private static String _positionYDIM = CombinePropertyName<PositionsPosition>("PO_YDIM");
        private static String _positionYFACINGS = CombinePropertyName<PositionsPosition>("PO_YFACINGS");
        private static String _positionYGAP = CombinePropertyName<PositionsPosition>("PO_YGAP");
        private static String _positionYPEGHOLEPOS = CombinePropertyName<PositionsPosition>("PO_YPEGHOLEPOS");
        private static String _positionYPEGNUM = CombinePropertyName<PositionsPosition>("PO_YPEGNUM");
        private static String _positionYPOS = CombinePropertyName<PositionsPosition>("PO_YPOS");
        private static String _positionZ_ENDPOS = CombinePropertyName<PositionsPosition>("PO_Z_ENDPOS");
        private static String _positionZDIM = CombinePropertyName<PositionsPosition>("PO_ZDIM");
        private static String _positionZFACINGS = CombinePropertyName<PositionsPosition>("PO_ZFACINGS");
        private static String _positionZGAP = CombinePropertyName<PositionsPosition>("PO_ZGAP");
        private static String _positionZPOS = CombinePropertyName<PositionsPosition>("PO_ZPOS");
        private static String _positionX05_POSMEASX04 = CombinePropertyName<PositionsPosition>("X05_POSMEASX04");
        private static String _positionX05_POSMEASX05 = CombinePropertyName<PositionsPosition>("X05_POSMEASX05");
        #endregion PositionsPosition fields

        #region Section
        public static String SectionName { get { return _sectionName; } }
        public static String SectionALLOWFLOAT { get { return _sectionALLOWFLOAT; } }
        public static String SectionALLOWOVERHANG { get { return _sectionALLOWOVERHANG; } }
        public static String SectionALLOWOVERLAP { get { return _sectionALLOWOVERLAP; } }
        public static String SectionARBITRARY { get { return _sectionARBITRARY; } }
        public static String SectionAVAILABLECUBIC { get { return _sectionAVAILABLECUBIC; } }
        public static String SectionAVAILABLELINEAR { get { return _sectionAVAILABLELINEAR; } }
        public static String SectionAVAILABLESQUARE { get { return _sectionAVAILABLESQUARE; } }
        public static String SectionAVERAGECASES { get { return _sectionAVERAGECASES; } }
        public static String SectionAVERAGEFACINGS { get { return _sectionAVERAGEFACINGS; } }
        public static String SectionBACKGROUND { get { return _sectionBACKGROUND; } }
        public static String SectionCAPACITY { get { return _sectionCAPACITY; } }
        public static String SectionCASES { get { return _sectionCASES; } }
        public static String SectionCREATEDATE { get { return _sectionCREATEDATE; } }
        public static String SectionCRUNCH { get { return _sectionCRUNCH; } }
        public static String SectionCUBICMULTIPLIER { get { return _sectionCUBICMULTIPLIER; } }
        public static String SectionCUBICTHROUGHPUT { get { return _sectionCUBICTHROUGHPUT; } }
        public static String SectionDAYSINWEEK { get { return _sectionDAYSINWEEK; } }
        public static String SectionDECOHEIGHT { get { return _sectionDECOHEIGHT; } }
        public static String SectionDECOTEXT { get { return _sectionDECOTEXT; } }
        public static String SectionDECOWIDTH { get { return _sectionDECOWIDTH; } }
        public static String SectionDECOXPOS { get { return _sectionDECOXPOS; } }
        public static String SectionDECOYPOS { get { return _sectionDECOYPOS; } }
        public static String SectionDEPTH { get { return _sectionDEPTH; } }
        public static String SectionDPP { get { return _sectionDPP; } }
        public static String SectionFACINGS { get { return _sectionFACINGS; } }
        public static String SectionFIRSTNOTCH { get { return _sectionFIRSTNOTCH; } }
        public static String SectionFOREGROUND { get { return _sectionFOREGROUND; } }
        public static String SectionHEIGHT { get { return _sectionHEIGHT; } }
        public static String SectionHICASES { get { return _sectionHICASES; } }
        public static String SectionHIDAYS { get { return _sectionHIDAYS; } }
        public static String SectionHIFACINGS { get { return _sectionHIFACINGS; } }
        public static String SectionINCREMENT { get { return _sectionINCREMENT; } }
        public static String SectionINVATRETAIL { get { return _sectionINVATRETAIL; } }
        public static String SectionINVENTORY { get { return _sectionINVENTORY; } }
        public static String SectionLINEARMULTIPLIER { get { return _sectionLINEARMULTIPLIER; } }
        public static String SectionLOGOPATH { get { return _sectionLOGOPATH; } }
        public static String SectionLOWCASES { get { return _sectionLOWCASES; } }
        public static String SectionLOWDAYS { get { return _sectionLOWDAYS; } }
        public static String SectionLOWFACINGS { get { return _sectionLOWFACINGS; } }
        public static String SectionMERCH { get { return _sectionMERCH; } }
        public static String SectionMESSAGELINE1 { get { return _sectionMESSAGELINE1; } }
        public static String SectionMESSAGELINE2 { get { return _sectionMESSAGELINE2; } }
        public static String SectionMESSAGELINE3 { get { return _sectionMESSAGELINE3; } }
        public static String SectionMESSAGELINE4 { get { return _sectionMESSAGELINE4; } }
        public static String SectionMESSAGELINE5 { get { return _sectionMESSAGELINE5; } }
        public static String SectionMOVEMENT { get { return _sectionMOVEMENT; } }
        public static String SectionNOTCH { get { return _sectionNOTCH; } }
        public static String SectionNUMSHELVES { get { return _sectionNUMSHELVES; } }
        public static String SectionNUMSKUS { get { return _sectionNUMSKUS; } }
        public static String SectionPLOTDESC1 { get { return _sectionPLOTDESC1; } }
        public static String SectionPLOTDESC2 { get { return _sectionPLOTDESC2; } }
        public static String SectionPLOTDESC3 { get { return _sectionPLOTDESC3; } }
        public static String SectionPROFIT { get { return _sectionPROFIT; } }
        public static String SectionRESERVED1 { get { return _sectionRESERVED1; } }
        public static String SectionRESERVED2 { get { return _sectionRESERVED2; } }
        public static String SectionRESERVED3 { get { return _sectionRESERVED3; } }
        public static String SectionRESERVED4 { get { return _sectionRESERVED4; } }
        public static String SectionRESERVED5 { get { return _sectionRESERVED5; } }
        public static String SectionRESERVED6 { get { return _sectionRESERVED6; } }
        public static String SectionROII { get { return _sectionROII; } }
        public static String SectionSALES { get { return _sectionSALES; } }
        public static String SectionSECTION { get { return _sectionSECTION; } }
        public static String SectionSECTIONDESC { get { return _sectionSECTIONDESC; } }
        public static String SectionSECTIONFLAGSBIT2 { get { return _sectionSECTIONFLAGSBIT2; } }
        public static String SectionSECTIONID { get { return _sectionSECTIONID; } }
        public static String SectionSHOWDECO { get { return _sectionSHOWDECO; } }
        public static String SectionSQUAREMULTIPLIER { get { return _sectionSQUAREMULTIPLIER; } }
        public static String SectionSTAGGEREDSHELVES { get { return _sectionSTAGGEREDSHELVES; } }
        public static String SectionSTANDARDLENGTH { get { return _sectionSTANDARDLENGTH; } }
        public static String SectionTRAFFIC { get { return _sectionTRAFFIC; } }
        public static String SectionUPDATEDATE { get { return _sectionUPDATEDATE; } }
        public static String SectionUPDATETIME { get { return _sectionUPDATETIME; } }
        public static String SectionUPRIGHT { get { return _sectionUPRIGHT; } }
        public static String SectionUPRIGHTBACKGROUND { get { return _sectionUPRIGHTBACKGROUND; } }
        public static String SectionUPRIGHTFOREGROUND { get { return _sectionUPRIGHTFOREGROUND; } }
        public static String SectionUSEDCUBIC { get { return _sectionUSEDCUBIC; } }
        public static String SectionUSEDLINEAR { get { return _sectionUSEDLINEAR; } }
        public static String SectionUSEDSQUARE { get { return _sectionUSEDSQUARE; } }
        public static String SectionUSER1 { get { return _sectionUSER1; } }
        public static String SectionUSER2 { get { return _sectionUSER2; } }
        public static String SectionVERSION { get { return _sectionVERSION; } }
        public static String SectionVERSIONDESC { get { return _sectionVERSIONDESC; } }
        public static String SectionWIDTH { get { return _sectionWIDTH; } }
        public static String SectionXPOS { get { return _sectionXPOS; } }
        public static String SectionYPOS { get { return _sectionYPOS; } }
        public static String SectionZPOS { get { return _sectionZPOS; } }
        public static String SectionX03_SEDESCRESERVE02 { get { return _sectionX03_SEDESCRESERVE02; } }
        public static String SectionX03_SEDESCRESERVE03 { get { return _sectionX03_SEDESCRESERVE03; } }
        public static String SectionX03_SEDESCRESERVE04 { get { return _sectionX03_SEDESCRESERVE04; } }
        public static String SectionX03_SEDESCRESERVE05 { get { return _sectionX03_SEDESCRESERVE05; } }
        public static String SectionX03_SEDESCRESERVE07 { get { return _sectionX03_SEDESCRESERVE07; } }
        public static String SectionX03_SEDESCX01 { get { return _sectionX03_SEDESCX01; } }
        public static String SectionX03_SEDESCX02 { get { return _sectionX03_SEDESCX02; } }
        public static String SectionX03_SEDESCX03 { get { return _sectionX03_SEDESCX03; } }
        public static String SectionX03_SEDESCX04 { get { return _sectionX03_SEDESCX04; } }
        public static String SectionX03_SEDESCX05 { get { return _sectionX03_SEDESCX05; } }
        public static String SectionX03_SEDESCX06 { get { return _sectionX03_SEDESCX06; } }
        public static String SectionX03_SEDESCX07 { get { return _sectionX03_SEDESCX07; } }
        public static String SectionX03_SEDESCX08 { get { return _sectionX03_SEDESCX08; } }
        public static String SectionX03_SEDESCX09 { get { return _sectionX03_SEDESCX09; } }
        public static String SectionX03_SEDESCX10 { get { return _sectionX03_SEDESCX10; } }
        public static String SectionX03_SEDESCX11 { get { return _sectionX03_SEDESCX11; } }
        public static String SectionX03_SEDESCX12 { get { return _sectionX03_SEDESCX12; } }
        public static String SectionX03_SEDESCX13 { get { return _sectionX03_SEDESCX13; } }
        public static String SectionX03_SEDESCX14 { get { return _sectionX03_SEDESCX14; } }
        public static String SectionX03_SEDESCX15 { get { return _sectionX03_SEDESCX15; } }
        public static String SectionX03_SEDESCX16 { get { return _sectionX03_SEDESCX16; } }
        public static String SectionX03_SEDESCX17 { get { return _sectionX03_SEDESCX17; } }
        public static String SectionX03_SEDESCX18 { get { return _sectionX03_SEDESCX18; } }
        public static String SectionX03_SEDESCX19 { get { return _sectionX03_SEDESCX19; } }
        public static String SectionX03_SEDESCX20 { get { return _sectionX03_SEDESCX20; } }
        public static String SectionX03_SEDESCX21 { get { return _sectionX03_SEDESCX21; } }
        public static String SectionX03_SEDESCX22 { get { return _sectionX03_SEDESCX22; } }
        public static String SectionX03_SEDESCX23 { get { return _sectionX03_SEDESCX23; } }
        public static String SectionX03_SEDESCX24 { get { return _sectionX03_SEDESCX24; } }
        public static String SectionX03_SEDESCX25 { get { return _sectionX03_SEDESCX25; } }
        public static String SectionX03_SEDESCX26 { get { return _sectionX03_SEDESCX26; } }
        public static String SectionX03_SEDESCX27 { get { return _sectionX03_SEDESCX27; } }
        public static String SectionX03_SEDESCX28 { get { return _sectionX03_SEDESCX28; } }
        public static String SectionX03_SEDESCX29 { get { return _sectionX03_SEDESCX29; } }
        public static String SectionX03_SEDESCX30 { get { return _sectionX03_SEDESCX30; } }
        public static String SectionX03_SEDESCX31 { get { return _sectionX03_SEDESCX31; } }
        public static String SectionX03_SEDESCX32 { get { return _sectionX03_SEDESCX32; } }
        public static String SectionX03_SEDESCX33 { get { return _sectionX03_SEDESCX33; } }
        public static String SectionX03_SEDESCX34 { get { return _sectionX03_SEDESCX34; } }
        public static String SectionX03_SEDESCX35 { get { return _sectionX03_SEDESCX35; } }
        public static String SectionX03_SEDESCX36 { get { return _sectionX03_SEDESCX36; } }
        public static String SectionX03_SEDESCX37 { get { return _sectionX03_SEDESCX37; } }
        public static String SectionX03_SEDESCX38 { get { return _sectionX03_SEDESCX38; } }
        public static String SectionX03_SEDESCX39 { get { return _sectionX03_SEDESCX39; } }
        public static String SectionX03_SEDESCX40 { get { return _sectionX03_SEDESCX40; } }
        public static String SectionX03_SEDESCX41 { get { return _sectionX03_SEDESCX41; } }
        public static String SectionX03_SEDESCX42 { get { return _sectionX03_SEDESCX42; } }
        public static String SectionX03_SEDESCX43 { get { return _sectionX03_SEDESCX43; } }
        public static String SectionX03_SEDESCX44 { get { return _sectionX03_SEDESCX44; } }
        public static String SectionX03_SEDESCX45 { get { return _sectionX03_SEDESCX45; } }
        public static String SectionX03_SEDESCX46 { get { return _sectionX03_SEDESCX46; } }
        public static String SectionX03_SEDESCX47 { get { return _sectionX03_SEDESCX47; } }
        public static String SectionX03_SEDESCX48 { get { return _sectionX03_SEDESCX48; } }
        public static String SectionX03_SEDESCX49 { get { return _sectionX03_SEDESCX49; } }
        public static String SectionX03_SEDESCX50 { get { return _sectionX03_SEDESCX50; } }
        public static String SectionX03_SEMEASRESERVE01 { get { return _sectionX03_SEMEASRESERVE01; } }
        public static String SectionX03_SEMEASRESERVE02 { get { return _sectionX03_SEMEASRESERVE02; } }
        public static String SectionX03_SEMEASRESERVE03 { get { return _sectionX03_SEMEASRESERVE03; } }
        public static String SectionX03_SEMEASRESERVE04 { get { return _sectionX03_SEMEASRESERVE04; } }
        public static String SectionX03_SEMEASRESERVE05 { get { return _sectionX03_SEMEASRESERVE05; } }
        public static String SectionX03_SEMEASRESERVE06 { get { return _sectionX03_SEMEASRESERVE06; } }
        public static String SectionX03_SEMEASRESERVE07 { get { return _sectionX03_SEMEASRESERVE07; } }
        public static String SectionX03_SEMEASRESERVE08 { get { return _sectionX03_SEMEASRESERVE08; } }
        public static String SectionX03_SEMEASRESERVE09 { get { return _sectionX03_SEMEASRESERVE09; } }
        public static String SectionX03_SEMEASRESERVE10 { get { return _sectionX03_SEMEASRESERVE10; } }
        public static String SectionX03_SEMEASRESERVE11 { get { return _sectionX03_SEMEASRESERVE11; } }
        public static String SectionX03_SEMEASRESERVE12 { get { return _sectionX03_SEMEASRESERVE12; } }
        public static String SectionX03_SEMEASRESERVE13 { get { return _sectionX03_SEMEASRESERVE13; } }
        public static String SectionX03_SEMEASRESERVE14 { get { return _sectionX03_SEMEASRESERVE14; } }
        public static String SectionX03_SEMEASRESERVE15 { get { return _sectionX03_SEMEASRESERVE15; } }
        public static String SectionX03_SEMEASRESERVE16 { get { return _sectionX03_SEMEASRESERVE16; } }
        public static String SectionX03_SEMEASRESERVE17 { get { return _sectionX03_SEMEASRESERVE17; } }
        public static String SectionX03_SEMEASRESERVE18 { get { return _sectionX03_SEMEASRESERVE18; } }
        public static String SectionX03_SEMEASRESERVE19 { get { return _sectionX03_SEMEASRESERVE19; } }
        public static String SectionX03_SEMEASRESERVE20 { get { return _sectionX03_SEMEASRESERVE20; } }
        public static String SectionX03_SEMEASX01 { get { return _sectionX03_SEMEASX01; } }
        public static String SectionX03_SEMEASX02 { get { return _sectionX03_SEMEASX02; } }
        public static String SectionX03_SEMEASX03 { get { return _sectionX03_SEMEASX03; } }
        public static String SectionX03_SEMEASX04 { get { return _sectionX03_SEMEASX04; } }
        public static String SectionX03_SEMEASX05 { get { return _sectionX03_SEMEASX05; } }
        public static String SectionX03_SEMEASX06 { get { return _sectionX03_SEMEASX06; } }
        public static String SectionX03_SEMEASX07 { get { return _sectionX03_SEMEASX07; } }
        public static String SectionX03_SEMEASX08 { get { return _sectionX03_SEMEASX08; } }
        public static String SectionX03_SEMEASX09 { get { return _sectionX03_SEMEASX09; } }
        public static String SectionX03_SEMEASX10 { get { return _sectionX03_SEMEASX10; } }
        public static String SectionX03_SEMEASX11 { get { return _sectionX03_SEMEASX11; } }
        public static String SectionX03_SEMEASX12 { get { return _sectionX03_SEMEASX12; } }
        public static String SectionX03_SEMEASX13 { get { return _sectionX03_SEMEASX13; } }
        public static String SectionX03_SEMEASX14 { get { return _sectionX03_SEMEASX14; } }
        public static String SectionX03_SEMEASX15 { get { return _sectionX03_SEMEASX15; } }
        public static String SectionX03_SEMEASX16 { get { return _sectionX03_SEMEASX16; } }
        public static String SectionX03_SEMEASX17 { get { return _sectionX03_SEMEASX17; } }
        public static String SectionX03_SEMEASX18 { get { return _sectionX03_SEMEASX18; } }
        public static String SectionX03_SEMEASX19 { get { return _sectionX03_SEMEASX19; } }
        public static String SectionX03_SEMEASX20 { get { return _sectionX03_SEMEASX20; } }
        public static String SectionX03_SEMEASX21 { get { return _sectionX03_SEMEASX21; } }
        public static String SectionX03_SEMEASX22 { get { return _sectionX03_SEMEASX22; } }
        public static String SectionX03_SEMEASX23 { get { return _sectionX03_SEMEASX23; } }
        public static String SectionX03_SEMEASX24 { get { return _sectionX03_SEMEASX24; } }
        public static String SectionX03_SEMEASX25 { get { return _sectionX03_SEMEASX25; } }
        public static String SectionX03_SEMEASX26 { get { return _sectionX03_SEMEASX26; } }
        public static String SectionX03_SEMEASX27 { get { return _sectionX03_SEMEASX27; } }
        public static String SectionX03_SEMEASX28 { get { return _sectionX03_SEMEASX28; } }
        public static String SectionX03_SEMEASX29 { get { return _sectionX03_SEMEASX29; } }
        public static String SectionX03_SEMEASX30 { get { return _sectionX03_SEMEASX30; } }
        public static String SectionX03_SEMEASX31 { get { return _sectionX03_SEMEASX31; } }
        public static String SectionX03_SEMEASX32 { get { return _sectionX03_SEMEASX32; } }
        public static String SectionX03_SEMEASX33 { get { return _sectionX03_SEMEASX33; } }
        public static String SectionX03_SEMEASX34 { get { return _sectionX03_SEMEASX34; } }
        public static String SectionX03_SEMEASX35 { get { return _sectionX03_SEMEASX35; } }
        public static String SectionX03_SEMEASX36 { get { return _sectionX03_SEMEASX36; } }
        public static String SectionX03_SEMEASX37 { get { return _sectionX03_SEMEASX37; } }
        public static String SectionX03_SEMEASX38 { get { return _sectionX03_SEMEASX38; } }
        public static String SectionX03_SEMEASX39 { get { return _sectionX03_SEMEASX39; } }
        public static String SectionX03_SEMEASX40 { get { return _sectionX03_SEMEASX40; } }
        public static String SectionX03_SEMEASX41 { get { return _sectionX03_SEMEASX41; } }
        public static String SectionX03_SEMEASX42 { get { return _sectionX03_SEMEASX42; } }
        public static String SectionX03_SEMEASX43 { get { return _sectionX03_SEMEASX43; } }
        public static String SectionX03_SEMEASX44 { get { return _sectionX03_SEMEASX44; } }
        public static String SectionX03_SEMEASX45 { get { return _sectionX03_SEMEASX45; } }
        public static String SectionX03_SEMEASX46 { get { return _sectionX03_SEMEASX46; } }
        public static String SectionX03_SEMEASX47 { get { return _sectionX03_SEMEASX47; } }
        public static String SectionX03_SEMEASX48 { get { return _sectionX03_SEMEASX48; } }
        public static String SectionX03_SEMEASX49 { get { return _sectionX03_SEMEASX49; } }
        public static String SectionX03_SEMEASX50 { get { return _sectionX03_SEMEASX50; } }
        #endregion Section

        #region Store

        public static String StoreACV { get { return _storeACV; } }
        public static String StoreADDRESS1 { get { return _storeADDRESS1; } }
        public static String StoreADDRESS2 { get { return _storeADDRESS2; } }
        public static String StoreCITY { get { return _storeCITY; } }
        public static String StoreCLUSTER { get { return _storeCLUSTER; } }
        public static String StoreCONTACT { get { return _storeCONTACT; } }
        public static String StoreDEPTH { get { return _storeDEPTH; } }
        public static String StoreDISTRICT { get { return _storeDISTRICT; } }
        public static String StoreHEIGHT { get { return _storeHEIGHT; } }
        public static String StoreNAME { get { return _storeNAME; } }
        public static String StorePHONE { get { return _storePHONE; } }
        public static String StoreSTATE { get { return _storeSTATE; } }
        public static String StoreSTORE { get { return _storeSTORE; } }
        public static String StoreUSER1 { get { return _storeUSER1; } }
        public static String StoreUSER2 { get { return _storeUSER2; } }
        public static String StoreUSER3 { get { return _storeUSER3; } }
        public static String StoreUSER4 { get { return _storeUSER4; } }
        public static String StoreWIDTH { get { return _storeWIDTH; } }
        public static String StoreZIP { get { return _storeZIP; } }

        #endregion

        #region Retailer

        public static String RetailerADDRESS1 { get { return _retailerADDRESS1; } }
        public static String RetailerADDRESS2 { get { return _retailerADDRESS2; } }
        public static String RetailerCITY { get { return _retailerCITY; } }
        public static String RetailerCONTACT { get { return _retailerCONTACT; } }
        public static String RetailerNAME { get { return _retailerNAME; } }
        public static String RetailerNUMSTORES { get { return _retailerNUMSTORES; } }
        public static String RetailerPHONE { get { return _retailerPHONE; } }
        public static String RetailerRETAILER { get { return _retailerRETAILER; } }
        public static String RetailerSTATE { get { return _retailerSTATE; } }
        public static String RetailerZIP { get { return _retailerZIP; } }

        #endregion Retailer

        #region Component
        public static String ComponentName { get { return _componentName; } }
        public static String ComponentAUTOCALCFRONTS { get { return _componentAUTOCALCFRONTS; } }
        public static String ComponentAVAILABLECUBIC { get { return _componentAVAILABLECUBIC; } }
        public static String ComponentAVAILABLELINEAR { get { return _componentAVAILABLELINEAR; } }
        public static String ComponentAVAILABLESQUARE { get { return _componentAVAILABLESQUARE; } }
        public static String ComponentAVERAGECASES { get { return _componentAVERAGECASES; } }
        public static String ComponentAVERAGEFACINGS { get { return _componentAVERAGEFACINGS; } }
        public static String ComponentBACKGROUND { get { return _componentBACKGROUND; } }
        public static String ComponentBRACKETDEPTH { get { return _componentBRACKETDEPTH; } }
        public static String ComponentBRACKETHEIGHT { get { return _componentBRACKETHEIGHT; } }
        public static String ComponentBRACKETINCREMENT { get { return _componentBRACKETINCREMENT; } }
        public static String ComponentBRACKETSLOPE { get { return _componentBRACKETSLOPE; } }
        public static String ComponentBRACKETTHICKNESS { get { return _componentBRACKETTHICKNESS; } }
        public static String ComponentCAPACITY { get { return _componentCAPACITY; } }
        public static String ComponentCASES { get { return _componentCASES; } }
        public static String ComponentCUBICTHROUGHPUT { get { return _componentCUBICTHROUGHPUT; } }
        public static String ComponentDISPLAYNUM { get { return _componentDISPLAYNUM; } }
        public static String ComponentDIVINC { get { return _componentDIVINC; } }
        public static String ComponentDIVTHICK { get { return _componentDIVTHICK; } }
        public static String ComponentDPP { get { return _componentDPP; } }
        public static String ComponentEDGE { get { return _componentEDGE; } }
        public static String ComponentFACINGS { get { return _componentFACINGS; } }
        public static String ComponentFITSTATUS { get { return _componentFITSTATUS; } }
        public static String ComponentFOREGROUND { get { return _componentFOREGROUND; } }
        public static String ComponentGRILLHEIGHT { get { return _componentGRILLHEIGHT; } }
        public static String ComponentGRILLTHICKNESS { get { return _componentGRILLTHICKNESS; } }
        public static String ComponentHICASES { get { return _componentHICASES; } }
        public static String ComponentHIDAYS { get { return _componentHIDAYS; } }
        public static String ComponentHIFACINGS { get { return _componentHIFACINGS; } }
        public static String ComponentINVATRETAIL { get { return _componentINVATRETAIL; } }
        public static String ComponentINVENTORY { get { return _componentINVENTORY; } }
        public static String ComponentLOWCASES { get { return _componentLOWCASES; } }
        public static String ComponentLOWDAYS { get { return _componentLOWDAYS; } }
        public static String ComponentLOWFACINGS { get { return _componentLOWFACINGS; } }
        public static String ComponentMAXMERCHDEPTH { get { return _componentMAXMERCHDEPTH; } }
        public static String ComponentMERCHDEPTH { get { return _componentMERCHDEPTH; } }
        public static String ComponentMERCHHEIGHT { get { return _componentMERCHHEIGHT; } }
        public static String ComponentMOVEMENT { get { return _componentMOVEMENT; } }
        public static String ComponentNOTCHNUMBER { get { return _componentNOTCHNUMBER; } }
        public static String ComponentNUMSKUS { get { return _componentNUMSKUS; } }
        public static String ComponentPROFIT { get { return _componentPROFIT; } }
        public static String ComponentRESERVED1 { get { return _componentRESERVED1; } }
        public static String ComponentRESERVED2 { get { return _componentRESERVED2; } }
        public static String ComponentRESERVED3 { get { return _componentRESERVED3; } }
        public static String ComponentRESERVED4 { get { return _componentRESERVED4; } }
        public static String ComponentRESERVED5 { get { return _componentRESERVED5; } }
        public static String ComponentRESERVED6 { get { return _componentRESERVED6; } }
        public static String ComponentROII { get { return _componentROII; } }
        public static String ComponentSALES { get { return _componentSALES; } }
        public static String ComponentSHELFDEPTH { get { return _componentSHELFDEPTH; } }
        public static String ComponentSHELFHEIGHT { get { return _componentSHELFHEIGHT; } }
        public static String ComponentSHELFTYPE { get { return _componentSHELFTYPE; } }
        public static String ComponentSHELFWIDTH { get { return _componentSHELFWIDTH; } }
        public static String ComponentSLOPE { get { return _componentSLOPE; } }
        public static String ComponentSPREADMODE { get { return _componentSPREADMODE; } }
        public static String ComponentTHICKNESS { get { return _componentTHICKNESS; } }
        public static String ComponentUSEDCUBIC { get { return _componentUSEDCUBIC; } }
        public static String ComponentUSEDLINEAR { get { return _componentUSEDLINEAR; } }
        public static String ComponentUSEDSQUARE { get { return _componentUSEDSQUARE; } }
        public static String ComponentUSERDESC1 { get { return _componentUSERDESC1; } }
        public static String ComponentUSERDESC2 { get { return _componentUSERDESC2; } }
        public static String ComponentUSERDESC3 { get { return _componentUSERDESC3; } }
        public static String ComponentUSERDESC4 { get { return _componentUSERDESC4; } }
        public static String ComponentUSERNUM1 { get { return _componentUSERNUM1; } }
        public static String ComponentUSERNUM2 { get { return _componentUSERNUM2; } }
        public static String ComponentUSERNUM3 { get { return _componentUSERNUM3; } }
        public static String ComponentXINC { get { return _componentXINC; } }
        public static String ComponentXPEGEND { get { return _componentXPEGEND; } }
        public static String ComponentXPEGSTART { get { return _componentXPEGSTART; } }
        public static String ComponentXPOS { get { return _componentXPOS; } }
        public static String ComponentYINC { get { return _componentYINC; } }
        public static String ComponentYPEGEND { get { return _componentYPEGEND; } }
        public static String ComponentYPEGSTART { get { return _componentYPEGSTART; } }
        public static String ComponentYPOS { get { return _componentYPOS; } }
        public static String ComponentZPOS { get { return _componentZPOS; } }
        public static String ComponentX04_SHDESCX50 { get { return _componentX04_SHDESCX50; } }
        public static String ComponentX04_SHMEASRESERVE01 { get { return _componentX04_SHMEASRESERVE01; } }
        public static String ComponentX04_SHMEASRESERVE02 { get { return _componentX04_SHMEASRESERVE02; } }
        public static String ComponentX04_SHMEASRESERVE03 { get { return _componentX04_SHMEASRESERVE03; } }
        public static String ComponentX04_SHMEASRESERVE04 { get { return _componentX04_SHMEASRESERVE04; } }
        public static String ComponentX04_SHMEASRESERVE05 { get { return _componentX04_SHMEASRESERVE05; } }
        public static String ComponentX04_SHMEASRESERVE06 { get { return _componentX04_SHMEASRESERVE06; } }
        public static String ComponentX04_SHMEASRESERVE07 { get { return _componentX04_SHMEASRESERVE07; } }
        public static String ComponentX04_SHMEASRESERVE08 { get { return _componentX04_SHMEASRESERVE08; } }
        public static String ComponentX04_SHMEASRESERVE09 { get { return _componentX04_SHMEASRESERVE09; } }
        public static String ComponentX04_SHMEASRESERVE10 { get { return _componentX04_SHMEASRESERVE10; } }
        public static String ComponentX04_SHMEASRESERVE11 { get { return _componentX04_SHMEASRESERVE11; } }
        public static String ComponentX04_SHMEASRESERVE12 { get { return _componentX04_SHMEASRESERVE12; } }
        public static String ComponentX04_SHMEASRESERVE13 { get { return _componentX04_SHMEASRESERVE13; } }
        public static String ComponentX04_SHMEASRESERVE14 { get { return _componentX04_SHMEASRESERVE14; } }
        public static String ComponentX04_SHMEASRESERVE15 { get { return _componentX04_SHMEASRESERVE15; } }
        public static String ComponentX04_SHMEASRESERVE16 { get { return _componentX04_SHMEASRESERVE16; } }
        public static String ComponentX04_SHMEASRESERVE17 { get { return _componentX04_SHMEASRESERVE17; } }
        public static String ComponentX04_SHMEASRESERVE18 { get { return _componentX04_SHMEASRESERVE18; } }
        public static String ComponentX04_SHMEASRESERVE19 { get { return _componentX04_SHMEASRESERVE19; } }
        public static String ComponentX04_SHMEASRESERVE20 { get { return _componentX04_SHMEASRESERVE20; } }
        public static String ComponentX04_SHMEASX01 { get { return _componentX04_SHMEASX01; } }
        public static String ComponentX04_SHMEASX02 { get { return _componentX04_SHMEASX02; } }
        public static String ComponentX04_SHMEASX03 { get { return _componentX04_SHMEASX03; } }
        public static String ComponentX04_SHMEASX04 { get { return _componentX04_SHMEASX04; } }
        public static String ComponentX04_SHMEASX05 { get { return _componentX04_SHMEASX05; } }
        public static String ComponentX04_SHMEASX06 { get { return _componentX04_SHMEASX06; } }
        public static String ComponentX04_SHMEASX07 { get { return _componentX04_SHMEASX07; } }
        public static String ComponentX04_SHMEASX08 { get { return _componentX04_SHMEASX08; } }
        public static String ComponentX04_SHMEASX09 { get { return _componentX04_SHMEASX09; } }
        public static String ComponentX04_SHMEASX10 { get { return _componentX04_SHMEASX10; } }
        public static String ComponentX04_SHMEASX11 { get { return _componentX04_SHMEASX11; } }
        public static String ComponentX04_SHMEASX12 { get { return _componentX04_SHMEASX12; } }
        public static String ComponentX04_SHMEASX13 { get { return _componentX04_SHMEASX13; } }
        public static String ComponentX04_SHMEASX14 { get { return _componentX04_SHMEASX14; } }
        public static String ComponentX04_SHMEASX15 { get { return _componentX04_SHMEASX15; } }
        public static String ComponentX04_SHMEASX16 { get { return _componentX04_SHMEASX16; } }
        public static String ComponentX04_SHMEASX17 { get { return _componentX04_SHMEASX17; } }
        public static String ComponentX04_SHMEASX18 { get { return _componentX04_SHMEASX18; } }
        public static String ComponentX04_SHMEASX19 { get { return _componentX04_SHMEASX19; } }
        public static String ComponentX04_SHMEASX20 { get { return _componentX04_SHMEASX20; } }
        public static String ComponentX04_SHMEASX21 { get { return _componentX04_SHMEASX21; } }
        public static String ComponentX04_SHMEASX22 { get { return _componentX04_SHMEASX22; } }
        public static String ComponentX04_SHMEASX23 { get { return _componentX04_SHMEASX23; } }
        public static String ComponentX04_SHMEASX24 { get { return _componentX04_SHMEASX24; } }
        public static String ComponentX04_SHMEASX25 { get { return _componentX04_SHMEASX25; } }
        public static String ComponentX04_SHMEASX26 { get { return _componentX04_SHMEASX26; } }
        public static String ComponentX04_SHMEASX27 { get { return _componentX04_SHMEASX27; } }
        public static String ComponentX04_SHMEASX28 { get { return _componentX04_SHMEASX28; } }
        public static String ComponentX04_SHMEASX29 { get { return _componentX04_SHMEASX29; } }
        public static String ComponentX04_SHMEASX30 { get { return _componentX04_SHMEASX30; } }
        public static String ComponentX04_SHMEASX31 { get { return _componentX04_SHMEASX31; } }
        public static String ComponentX04_SHMEASX32 { get { return _componentX04_SHMEASX32; } }
        public static String ComponentX04_SHMEASX33 { get { return _componentX04_SHMEASX33; } }
        public static String ComponentX04_SHMEASX34 { get { return _componentX04_SHMEASX34; } }
        public static String ComponentX04_SHMEASX35 { get { return _componentX04_SHMEASX35; } }
        public static String ComponentX04_SHMEASX36 { get { return _componentX04_SHMEASX36; } }
        public static String ComponentX04_SHMEASX37 { get { return _componentX04_SHMEASX37; } }
        public static String ComponentX04_SHMEASX38 { get { return _componentX04_SHMEASX38; } }
        public static String ComponentX04_SHMEASX39 { get { return _componentX04_SHMEASX39; } }
        public static String ComponentX04_SHMEASX40 { get { return _componentX04_SHMEASX40; } }
        public static String ComponentX04_SHMEASX41 { get { return _componentX04_SHMEASX41; } }
        public static String ComponentX04_SHMEASX42 { get { return _componentX04_SHMEASX42; } }
        public static String ComponentX04_SHMEASX43 { get { return _componentX04_SHMEASX43; } }
        public static String ComponentX04_SHMEASX44 { get { return _componentX04_SHMEASX44; } }
        public static String ComponentX04_SHMEASX45 { get { return _componentX04_SHMEASX45; } }
        public static String ComponentX04_SHMEASX46 { get { return _componentX04_SHMEASX46; } }
        public static String ComponentX04_SHMEASX47 { get { return _componentX04_SHMEASX47; } }
        public static String ComponentX04_SHMEASX48 { get { return _componentX04_SHMEASX48; } }
        public static String ComponentX04_SHMEASX49 { get { return _componentX04_SHMEASX49; } }
        public static String ComponentX04_SHMEASX50 { get { return _componentX04_SHMEASX50; } }
        #endregion Component

        #region Product
        public static String ProductUPC { get { return _productUPC; } }
        public static String ProductSTOCKCODE { get { return _productSTOCKCODE; } }
        public static String ProductLONGDESC { get { return _productLONGDESC; } }
        public static String ProductSIZE { get { return _productSIZE; } }
        public static String ProductUOM { get { return _productUOM; } }
        public static String ProductBRAND { get { return _productBRAND; } }
        public static String ProductSCREENDESC { get { return _productSCREENDESC; } }
        public static String ProductCHAR { get { return _productCHAR; } }
        public static String ProductCOLOR { get { return _productCOLOR; } }
        public static String ProductPLOTDESC1 { get { return _productPLOTDESC1; } }
        public static String ProductPLOTDESC2 { get { return _productPLOTDESC2; } }
        public static String ProductSYMBOL { get { return _productSYMBOL; } }
        public static String ProductCATEGORY { get { return _productCATEGORY; } }
        public static String ProductSUBCATEGORY { get { return _productSUBCATEGORY; } }
        public static String ProductCOMPONENT { get { return _productCOMPONENT; } }
        public static String ProductMANUFACTURER { get { return _productMANUFACTURER; } }
        public static String ProductPACKAGE { get { return _productPACKAGE; } }
        public static String ProductDISTRIBUTION { get { return _productDISTRIBUTION; } }
        public static String ProductMULTIPACK { get { return _productMULTIPACK; } }
        public static String ProductCASEPACK { get { return _productCASEPACK; } }
        public static String ProductMOVEMENTADJ { get { return _productMOVEMENTADJ; } }
        public static String ProductPRODHEIGHT { get { return _productPRODHEIGHT; } }
        public static String ProductPRODWIDTH { get { return _productPRODWIDTH; } }
        public static String ProductPRODDEPTH { get { return _productPRODDEPTH; } }
        public static String ProductCASEHEIGHT { get { return _productCASEHEIGHT; } }
        public static String ProductCASEWIDTH { get { return _productCASEWIDTH; } }
        public static String ProductCASEDEPTH { get { return _productCASEDEPTH; } }
        public static String ProductCASEWEIGHT { get { return _productCASEWEIGHT; } }
        public static String ProductNESTING { get { return _productNESTING; } }
        public static String ProductOVERHANG { get { return _productOVERHANG; } }
        public static String ProductXPEGHOLE { get { return _productXPEGHOLE; } }
        public static String ProductYPEGHOLE { get { return _productYPEGHOLE; } }
        public static String ProductFINGER { get { return _productFINGER; } }
        public static String ProductMAXFRONTS { get { return _productMAXFRONTS; } }
        public static String ProductMAXLAYOVERS { get { return _productMAXLAYOVERS; } }
        public static String ProductMAXFRONTSDEEP { get { return _productMAXFRONTSDEEP; } }
        public static String ProductADJTOCASECOST { get { return _productADJTOCASECOST; } }
        public static String ProductARRIVEAS { get { return _productARRIVEAS; } }
        public static String ProductAVRNOCASESDSDSH { get { return _productAVRNOCASESDSDSH; } }
        public static String ProductEXPECTEDMOVEMENT { get { return _productEXPECTEDMOVEMENT; } }
        public static String ProductMAXCASES { get { return _productMAXCASES; } }
        public static String ProductMAXDAYS { get { return _productMAXDAYS; } }
        public static String ProductMAXFACING { get { return _productMAXFACING; } }
        public static String ProductMAXHEIGHT { get { return _productMAXHEIGHT; } }
        public static String ProductMAXUNITS { get { return _productMAXUNITS; } }
        public static String ProductMINCASES { get { return _productMINCASES; } }
        public static String ProductMINDAYS { get { return _productMINDAYS; } }
        public static String ProductMINFACING { get { return _productMINFACING; } }
        public static String ProductMINUNITS { get { return _productMINUNITS; } }
        public static String ProductNETDAYSCREDIT { get { return _productNETDAYSCREDIT; } }
        public static String ProductNOCASESPALLET { get { return _productNOCASESPALLET; } }
        public static String ProductNOINNERPACKS { get { return _productNOINNERPACKS; } }
        public static String ProductNOTRAYSCASE { get { return _productNOTRAYSCASE; } }
        public static String ProductPattern { get { return _productPATTERN; } }
        public static String ProductRACKOPENHT { get { return _productRACKOPENHT; } }
        public static String ProductREPLENISHMETHOD { get { return _productREPLENISHMETHOD; } }
        public static String ProductRESERVED1 { get { return _productRESERVED1; } }
        public static String ProductRESERVED2 { get { return _productRESERVED2; } }
        public static String ProductRESERVED3 { get { return _productRESERVED3; } }
        public static String ProductRESERVED4 { get { return _productRESERVED4; } }
        public static String ProductRESERVED5 { get { return _productRESERVED5; } }
        public static String ProductRESERVED6 { get { return _productRESERVED6; } }
        public static String ProductRESTOCK { get { return _productRESTOCK; } }
        public static String ProductSECTIONID { get { return _productSECTIONID; } }
        public static String ProductSECTIONSHARE { get { return _productSECTIONSHARE; } }
        public static String ProductSERVICE { get { return _productSERVICE; } }
        public static String ProductSHARE { get { return _productSHARE; } }
        public static String ProductSTOREPRICEMETHOD { get { return _productSTOREPRICEMETHOD; } }
        public static String ProductSTORERECUNLOAD { get { return _productSTORERECUNLOAD; } }
        public static String ProductUSERDESCA { get { return _productUSERDESCA; } }
        public static String ProductUSERDESCB { get { return _productUSERDESCB; } }
        public static String ProductUSERDESCC { get { return _productUSERDESCC; } }
        public static String ProductUSERDESCD { get { return _productUSERDESCD; } }
        public static String ProductUSERDESCE { get { return _productUSERDESCE; } }
        public static String ProductUSERDESCF { get { return _productUSERDESCF; } }
        public static String ProductUSERDESCG { get { return _productUSERDESCG; } }
        public static String ProductUSERDESCH { get { return _productUSERDESCH; } }
        public static String ProductUSERDESCI { get { return _productUSERDESCI; } }
        public static String ProductUSERDESCJ { get { return _productUSERDESCJ; } }
        public static String ProductUSERMEASUREA { get { return _productUSERMEASUREA; } }
        public static String ProductUSERMEASUREB { get { return _productUSERMEASUREB; } }
        public static String ProductUSERMEASUREC { get { return _productUSERMEASUREC; } }
        public static String ProductUSERMEASURED { get { return _productUSERMEASURED; } }
        public static String ProductUSERMEASUREE { get { return _productUSERMEASUREE; } }
        public static String ProductUSERMEASUREF { get { return _productUSERMEASUREF; } }
        public static String ProductUSERMEASUREG { get { return _productUSERMEASUREG; } }
        public static String ProductUSERMEASUREH { get { return _productUSERMEASUREH; } }
        public static String ProductUSERMEASUREI { get { return _productUSERMEASUREI; } }
        public static String ProductUSERMEASUREJ { get { return _productUSERMEASUREJ; } }
        public static String ProductVARIANCE { get { return _productVARIANCE; } }
        public static String ProductWARERECEIVE { get { return _productWARERECEIVE; } }
        public static String ProductX06_DESCX01 { get { return _productX06_DESCX01; } }
        public static String ProductX06_DESCX02 { get { return _productX06_DESCX02; } }
        public static String ProductX06_DESCX03 { get { return _productX06_DESCX03; } }
        public static String ProductX06_DESCX04 { get { return _productX06_DESCX04; } }
        public static String ProductX06_DESCX05 { get { return _productX06_DESCX05; } }
        public static String ProductX06_DESCX06 { get { return _productX06_DESCX06; } }
        public static String ProductX06_DESCX07 { get { return _productX06_DESCX07; } }
        public static String ProductX06_DESCX08 { get { return _productX06_DESCX08; } }
        public static String ProductX06_DESCX09 { get { return _productX06_DESCX09; } }
        public static String ProductX06_DESCX10 { get { return _productX06_DESCX10; } }
        public static String ProductX06_MEASRESERVE01 { get { return _productX06_MEASRESERVE01; } }
        #endregion Product

        #region Section Product
        public static String SectionProductRealMovement { get { return _sectionProductRealMovement; } }
        public static String SectionProductCost { get { return _sectionProductCost; } }
        public static String SectionProductCaseCost { get { return _sectionProductCaseCost; } }
        public static String SectionProductVAT { get { return _sectionProductVAT; } }
        public static String SectionProductRETAIL { get { return _sectionProductRETAIL; } }
        public static String SectionProductDPCA { get { return _sectionProductDPCA; } }
        public static String SectionProductPAYDISCOUNT { get { return _sectionProductPAYDISCOUNT; } }
        public static String SectionProductSELLINGPRICE { get { return _sectionProductSELLINGPRICE; } }
        public static String SectionProductMARGIN { get { return _sectionProductMARGIN; } }
        #endregion Section Product

        #region Position
        public static String PositionSales { get { return _positionSales; } }
        public static String PositionACTUALMARGIN { get { return _positionACTUALMARGIN; } }
        public static String PositionADJUSTEDMOVEMENT { get { return _positionADJUSTEDMOVEMENT; } }
        public static String PositionBACKROOMSTOCK { get { return _positionBACKROOMSTOCK; } }
        public static String PositionCAPACITY { get { return _positionCAPACITY; } }
        public static String PositionCAPACITYPERCUBIC { get { return _positionCAPACITYPERCUBIC; } }
        public static String PositionCAPACITYPERLINEAR { get { return _positionCAPACITYPERLINEAR; } }
        public static String PositionCAPACITYPERSQUARE { get { return _positionCAPACITYPERSQUARE; } }
        public static String PositionCASES { get { return _positionCASES; } }
        public static String PositionCONTRIBTOMARGIN { get { return _positionCONTRIBTOMARGIN; } }
        public static String PositionCUBIC { get { return _positionCUBIC; } }
        public static String PositionDAYSOFSUPPLY { get { return _positionDAYSOFSUPPLY; } }
        public static String PositionDECOTEXT { get { return _positionDECOTEXT; } }
        public static String PositionDISTMOVEMENT { get { return _positionDISTMOVEMENT; } }
        public static String PositionDPP { get { return _positionDPP; } }
        public static String PositionDPPPERCENT { get { return _positionDPPPERCENT; } }
        public static String PositionDPPPERCUBIC { get { return _positionDPPPERCUBIC; } }
        public static String PositionDPPPERLINEAR { get { return _positionDPPPERLINEAR; } }
        public static String PositionDPPPERSQUARE { get { return _positionDPPPERSQUARE; } }
        public static String PositionDPPPERUNIT { get { return _positionDPPPERUNIT; } }
        public static String PositionEDGE { get { return _positionEDGE; } }
        public static String PositionFITSTATUS { get { return _positionFITSTATUS; } }
        public static String PositionGMROI { get { return _positionGMROI; } }
        public static String PositionINVCOSTPERCUBIC { get { return _positionINVCOSTPERCUBIC; } }
        public static String PositionINVCOSTPERLINEAR { get { return _positionINVCOSTPERLINEAR; } }
        public static String PositionINVCOSTPERSQUARE { get { return _positionINVCOSTPERSQUARE; } }
        public static String PositionINVENTORY { get { return _positionINVENTORY; } }
        public static String PositionINVENTORYATRETAIL { get { return _positionINVENTORYATRETAIL; } }
        public static String PositionINVRETPERCUBIC { get { return _positionINVRETPERCUBIC; } }
        public static String PositionINVRETPERLINEAR { get { return _positionINVRETPERLINEAR; } }
        public static String PositionINVRETPERSQUARE { get { return _positionINVRETPERSQUARE; } }
        public static String PositionLAYOVERS { get { return _positionLAYOVERS; } }
        public static String PositionLAYOVERSDEEP { get { return _positionLAYOVERSDEEP; } }
        public static String PositionLINEAR { get { return _positionLINEAR; } }
        public static String PositionLOSTSALES { get { return _positionLOSTSALES; } }
        public static String PositionMERCHANDISINGTYPE { get { return _positionMERCHANDISINGTYPE; } }
        public static String PositionMOVEMENTPERCUBIC { get { return _positionMOVEMENTPERCUBIC; } }
        public static String PositionMOVEMENTPERLINEAR { get { return _positionMOVEMENTPERLINEAR; } }
        public static String PositionMOVEMENTPERSQUARE { get { return _positionMOVEMENTPERSQUARE; } }
        public static String PositionMULTICAPACITY { get { return _positionMULTICAPACITY; } }
        public static String PositionMULTIORIENT { get { return _positionMULTIORIENT; } }
        public static String PositionNORMALSTOCK { get { return _positionNORMALSTOCK; } }
        public static String PositionORIENTATION { get { return _positionORIENTATION; } }
        public static String PositionPCTCAPACITYPERSECT { get { return _positionPCTCAPACITYPERSECT; } }
        public static String PositionPCTCAPACITYPERSHF { get { return _positionPCTCAPACITYPERSHF; } }
        public static String PositionPCTCUBICPERSECT { get { return _positionPCTCUBICPERSECT; } }
        public static String PositionPCTCUBICPERSHF { get { return _positionPCTCUBICPERSHF; } }
        public static String PositionPCTDPPPERSECT { get { return _positionPCTDPPPERSECT; } }
        public static String PositionPCTDPPPERSHF { get { return _positionPCTDPPPERSHF; } }
        public static String PositionPCTINVCOSTPERSECT { get { return _positionPCTINVCOSTPERSECT; } }
        public static String PositionPCTINVCOSTPERSHF { get { return _positionPCTINVCOSTPERSHF; } }
        public static String PositionPCTLINEARPERSECT { get { return _positionPCTLINEARPERSECT; } }
        public static String PositionPCTLINEARPERSHF { get { return _positionPCTLINEARPERSHF; } }
        public static String PositionPCTMOVEMENTPERSECT { get { return _positionPCTMOVEMENTPERSECT; } }
        public static String PositionPCTMOVEMENTPERSHF { get { return _positionPCTMOVEMENTPERSHF; } }
        public static String PositionPCTPROFITPERSECT { get { return _positionPCTPROFITPERSECT; } }
        public static String PositionPCTPROFITPERSHF { get { return _positionPCTPROFITPERSHF; } }
        public static String PositionPCTSALESPERSECT { get { return _positionPCTSALESPERSECT; } }
        public static String PositionPCTSALESPERSHF { get { return _positionPCTSALESPERSHF; } }
        public static String PositionPCTSQUAREPERSECT { get { return _positionPCTSQUAREPERSECT; } }
        public static String PositionPCTSQUAREPERSHF { get { return _positionPCTSQUAREPERSHF; } }
        public static String PositionPEGLENGTH { get { return _positionPEGLENGTH; } }
        public static String PositionPEGTYPE { get { return _positionPEGTYPE; } }
        public static String PositionPOSITION { get { return _positionPOSITION; } }
        public static String PositionPROFIT { get { return _positionPROFIT; } }
        public static String PositionPROFITPERCUBIC { get { return _positionPROFITPERCUBIC; } }
        public static String PositionPROFITPERLINEAR { get { return _positionPROFITPERLINEAR; } }
        public static String PositionPROFITPERSQUARE { get { return _positionPROFITPERSQUARE; } }
        public static String PositionPROFITPERUNIT { get { return _positionPROFITPERUNIT; } }
        public static String PositionRESERVED1 { get { return _positionRESERVED1; } }
        public static String PositionRESERVED2 { get { return _positionRESERVED2; } }
        public static String PositionRESERVED5 { get { return _positionRESERVED5; } }
        public static String PositionROII { get { return _positionROII; } }
        public static String PositionSAFETYSTOCK { get { return _positionSAFETYSTOCK; } }
        public static String PositionSALESGROWTH { get { return _positionSALESGROWTH; } }
        public static String PositionSALESPERCUBIC { get { return _positionSALESPERCUBIC; } }
        public static String PositionSALESPERFACING { get { return _positionSALESPERFACING; } }
        public static String PositionSALESPERLINEAR { get { return _positionSALESPERLINEAR; } }
        public static String PositionSALESPERSQUARE { get { return _positionSALESPERSQUARE; } }
        public static String PositionSQUARE { get { return _positionSQUARE; } }
        public static String PositionSUGGESTEDFACINGS { get { return _positionSUGGESTEDFACINGS; } }
        public static String PositionTAGNUMBER { get { return _positionTAGNUMBER; } }
        public static String PositionTURNS { get { return _positionTURNS; } }
        public static String PositionUPC { get { return _positionUPC; } }
        public static String PositionWEEKSOFSUPPLY { get { return _positionWEEKSOFSUPPLY; } }
        public static String PositionX_ENDPOS { get { return _positionX_ENDPOS; } }
        public static String PositionXDIM { get { return _positionXDIM; } }
        public static String PositionXFACINGS { get { return _positionXFACINGS; } }
        public static String PositionXGAP { get { return _positionXGAP; } }
        public static String PositionXPEGHOLEPOS { get { return _positionXPEGHOLEPOS; } }
        public static String PositionXPEGNUM { get { return _positionXPEGNUM; } }
        public static String PositionXPOS { get { return _positionXPOS; } }
        public static String PositionY_ENDPOS { get { return _positionY_ENDPOS; } }
        public static String PositionYDIM { get { return _positionYDIM; } }
        public static String PositionYFACINGS { get { return _positionYFACINGS; } }
        public static String PositionYGAP { get { return _positionYGAP; } }
        public static String PositionYPEGHOLEPOS { get { return _positionYPEGHOLEPOS; } }
        public static String PositionYPEGNUM { get { return _positionYPEGNUM; } }
        public static String PositionYPOS { get { return _positionYPOS; } }
        public static String PositionZ_ENDPOS { get { return _positionZ_ENDPOS; } }
        public static String PositionZDIM { get { return _positionZDIM; } }
        public static String PositionZFACINGS { get { return _positionZFACINGS; } }
        public static String PositionZGAP { get { return _positionZGAP; } }
        public static String PositionZPOS { get { return _positionZPOS; } }
        public static String PositionX05_POSMEASX04 { get { return _positionX05_POSMEASX04; } }
        public static String PositionX05_POSMEASX05 { get { return _positionX05_POSMEASX05; } }
        #endregion Position

        private static Dictionary<String, String> _fieldFriendlyNames = new Dictionary<String, String>()
        {
            #region Section

            {SectionName, Message.ApolloImport_Section_Name},
            {SectionALLOWFLOAT, Message.ApolloImport_Section_AllowFloat},
            {SectionALLOWOVERHANG, Message.ApolloImport_Section_AllowOverhang},
            {SectionALLOWOVERLAP, Message.ApolloImport_Section_AllowOverlap},
            {SectionARBITRARY, Message.ApolloImport_Section_Arbitrary},
            {SectionAVAILABLECUBIC,Message.ApolloImport_Section_AvailableCubic},
            {SectionAVAILABLELINEAR, Message.ApolloImport_Section_AvailableLinear},
            {SectionAVAILABLESQUARE, Message.ApolloImport_Section_AvailableSquare},
            {SectionAVERAGECASES,Message.ApolloImport_Section_AverageCases},
            {SectionAVERAGEFACINGS, Message.ApolloImport_Section_AverageFacings},
            {SectionBACKGROUND, Message.ApolloImport_Section_Background},
            {SectionCAPACITY, Message.ApolloImport_Section_Capacity},
            {SectionCASES, Message.ApolloImport_Section_Cases},
            {SectionCREATEDATE, Message.ApolloImport_Section_CreatedData},
            {SectionCRUNCH,  Message.ApolloImport_Section_Crunch},
            {SectionCUBICMULTIPLIER,Message.ApolloImport_Section_CubicMultiplier},
            {SectionCUBICTHROUGHPUT, Message.ApolloImport_Section_CubicThroughput},
            {SectionDAYSINWEEK,  Message.ApolloImport_Section_DaysInWeek},
            {SectionDECOHEIGHT, Message.ApolloImport_Section_DecoHeight},
            {SectionDECOTEXT,  Message.ApolloImport_Section_DecoText},
            {SectionDECOWIDTH,  Message.ApolloImport_Section_DecoWidth},
            {SectionDECOXPOS, Message.ApolloImport_Section_DecoXPos},
            {SectionDECOYPOS,  Message.ApolloImport_Section_DecoYPos},
            {SectionDEPTH, Message.ApolloImport_Section_Depth},
            {SectionDPP, Message.ApolloImport_Section_DPP},
            {SectionFACINGS, Message.ApolloImport_Section_Facings},
            {SectionFIRSTNOTCH, Message.ApolloImport_Section_FirstNotch},
            {SectionFOREGROUND, Message.ApolloImport_Section_Foreground},
            {SectionHEIGHT, Message.ApolloImport_Section_Height},
            {SectionHICASES,Message.ApolloImport_Section_HiCases},
            {SectionHIDAYS, Message.ApolloImport_Section_HiDays},
            {SectionHIFACINGS, Message.ApolloImport_Section_HiFacings},
            {SectionINCREMENT, Message.ApolloImport_Section_Increment},
            {SectionINVATRETAIL, Message.ApolloImport_Section_InvatRetail},
            {SectionINVENTORY, Message.ApolloImport_Section_Inventory},
            {SectionLINEARMULTIPLIER, Message.ApolloImport_Section_LinearMultiplier},
            {SectionLOGOPATH, Message.ApolloImport_Section_LogoPath},
            {SectionLOWCASES,  Message.ApolloImport_Section_LowCases},
            {SectionLOWDAYS,  Message.ApolloImport_Section_LowDays},
            {SectionLOWFACINGS, Message.ApolloImport_Section_LowFacings},
            {SectionMERCH, Message.ApolloImport_Section_Merch},
            {SectionMESSAGELINE1, Message.ApolloImport_Section_MessageLine1},
            {SectionMESSAGELINE2, Message.ApolloImport_Section_MessageLine2},
            {SectionMESSAGELINE3, Message.ApolloImport_Section_MessageLine3},
            {SectionMESSAGELINE4, Message.ApolloImport_Section_MessageLine4},
            {SectionMESSAGELINE5, Message.ApolloImport_Section_MessageLine5},
            {SectionMOVEMENT, Message.ApolloImport_Section_Movement},
            {SectionNOTCH, Message.ApolloImport_Section_Notch},
            {SectionNUMSHELVES, Message.ApolloImport_Section_NumShelves},
            {SectionNUMSKUS, Message.ApolloImport_Section_MumSKUs},
            {SectionPLOTDESC1, Message.ApolloImport_Section_PlotDesc1},
            {SectionPLOTDESC2, Message.ApolloImport_Section_PlotDesc2},
            {SectionPLOTDESC3, Message.ApolloImport_Section_PlotDesc3},
            {SectionPROFIT, Message.ApolloImport_Section_Profit},
            {SectionRESERVED1, Message.ApolloImport_Section_Reserved1},
            {SectionRESERVED2, Message.ApolloImport_Section_Reserved2},
            {SectionRESERVED3, Message.ApolloImport_Section_Reserved3},
            {SectionRESERVED4, Message.ApolloImport_Section_Reserved4},
            {SectionRESERVED5, Message.ApolloImport_Section_Reserved5},
            {SectionRESERVED6, Message.ApolloImport_Section_Reserved6},
            {SectionROII, Message.ApolloImport_Section_ROII},
            {SectionSALES, Message.ApolloImport_Section_Sales},
            {SectionSECTION, Message.ApolloImport_Section_Section},
            {SectionSECTIONDESC, Message.ApolloImport_Section_SectionDesc},
            {SectionSECTIONFLAGSBIT2, Message.ApolloImport_Section_FlagsBit2},
            {SectionSECTIONID, Message.ApolloImport_Section_SectionId},
            {SectionSHOWDECO, Message.ApolloImport_Section_ShowDeco},
            {SectionSQUAREMULTIPLIER, Message.ApolloImport_Section_SquareMultiplier},
            {SectionSTAGGEREDSHELVES, Message.ApolloImport_Section_StaggeredShelves},
            {SectionSTANDARDLENGTH, Message.ApolloImport_Section_StandardLength},
            {SectionTRAFFIC, Message.ApolloImport_Section_Traffic},
            {SectionUPDATEDATE, Message.ApolloImport_Section_UpdateDate},
            {SectionUPDATETIME,  Message.ApolloImport_Section_UpdateTime},
            {SectionUPRIGHT, Message.ApolloImport_Section_Upright},
            {SectionUPRIGHTBACKGROUND, Message.ApolloImport_Section_UprightBackground},
            {SectionUPRIGHTFOREGROUND,Message.ApolloImport_Section_UprightForground},
            {SectionUSEDCUBIC, Message.ApolloImport_Section_UsedCubic},
            {SectionUSEDLINEAR, Message.ApolloImport_Section_UsedLinear},
            {SectionUSEDSQUARE, Message.ApolloImport_Section_UsedSquare},
            {SectionUSER1, Message.ApolloImport_Section_User1},
            {SectionUSER2, Message.ApolloImport_Section_User2},
            {SectionVERSION, Message.ApolloImport_Section_Version},
            {SectionVERSIONDESC, Message.ApolloImport_Section_VersionDesc},
            {SectionWIDTH,  Message.ApolloImport_Section_Width},
            {SectionXPOS, Message.ApolloImport_Section_XPos},
            {SectionYPOS,  Message.ApolloImport_Section_YPos},
            {SectionZPOS,  Message.ApolloImport_Section_ZPos},
            {SectionX03_SEDESCX01, Message.ApolloImport_Section_X03_SEDESCX01},
            {SectionX03_SEDESCX02, Message.ApolloImport_Section_X03_SEDESCX02},
            {SectionX03_SEDESCX03, Message.ApolloImport_Section_X03_SEDESCX03},
            {SectionX03_SEDESCX04, Message.ApolloImport_Section_X03_SEDESCX04},
            {SectionX03_SEDESCX05, Message.ApolloImport_Section_X03_SEDESCX05},
            {SectionX03_SEDESCX06, Message.ApolloImport_Section_X03_SEDESCX06},
            {SectionX03_SEDESCX07, Message.ApolloImport_Section_X03_SEDESCX07},
            {SectionX03_SEDESCX08, Message.ApolloImport_Section_X03_SEDESCX08},
            {SectionX03_SEDESCX09, Message.ApolloImport_Section_X03_SEDESCX09},
            {SectionX03_SEDESCX10, Message.ApolloImport_Section_X03_SEDESCX10},
            {SectionX03_SEDESCX11, Message.ApolloImport_Section_X03_SEDESCX11},
            {SectionX03_SEDESCX12, Message.ApolloImport_Section_X03_SEDESCX12},
            {SectionX03_SEDESCX13, Message.ApolloImport_Section_X03_SEDESCX13},
            {SectionX03_SEDESCX14, Message.ApolloImport_Section_X03_SEDESCX14},
            {SectionX03_SEDESCX15, Message.ApolloImport_Section_X03_SEDESCX15},
            {SectionX03_SEDESCX16, Message.ApolloImport_Section_X03_SEDESCX16},
            {SectionX03_SEDESCX17, Message.ApolloImport_Section_X03_SEDESCX17},
            {SectionX03_SEDESCX18, Message.ApolloImport_Section_X03_SEDESCX18},
            {SectionX03_SEDESCX19, Message.ApolloImport_Section_X03_SEDESCX19},
            {SectionX03_SEDESCX20, Message.ApolloImport_Section_X03_SEDESCX20},
            {SectionX03_SEDESCX21, Message.ApolloImport_Section_X03_SEDESCX21},
            {SectionX03_SEDESCX22, Message.ApolloImport_Section_X03_SEDESCX22},
            {SectionX03_SEDESCX23, Message.ApolloImport_Section_X03_SEDESCX23},
            {SectionX03_SEDESCX24, Message.ApolloImport_Section_X03_SEDESCX24},
            {SectionX03_SEDESCX25, Message.ApolloImport_Section_X03_SEDESCX25},
            {SectionX03_SEDESCX26, Message.ApolloImport_Section_X03_SEDESCX26},
            {SectionX03_SEDESCX27, Message.ApolloImport_Section_X03_SEDESCX27},
            {SectionX03_SEDESCX28, Message.ApolloImport_Section_X03_SEDESCX28},
            {SectionX03_SEDESCX29, Message.ApolloImport_Section_X03_SEDESCX29},
            {SectionX03_SEDESCX30, Message.ApolloImport_Section_X03_SEDESCX30},
            {SectionX03_SEDESCX31, Message.ApolloImport_Section_X03_SEDESCX31},
            {SectionX03_SEDESCX32, Message.ApolloImport_Section_X03_SEDESCX32},
            {SectionX03_SEDESCX33, Message.ApolloImport_Section_X03_SEDESCX33},
            {SectionX03_SEDESCX34, Message.ApolloImport_Section_X03_SEDESCX34},
            {SectionX03_SEDESCX35, Message.ApolloImport_Section_X03_SEDESCX35},
            {SectionX03_SEDESCX36, Message.ApolloImport_Section_X03_SEDESCX36},
            {SectionX03_SEDESCX37, Message.ApolloImport_Section_X03_SEDESCX37},
            {SectionX03_SEDESCX38, Message.ApolloImport_Section_X03_SEDESCX38},
            {SectionX03_SEDESCX39, Message.ApolloImport_Section_X03_SEDESCX39},
            {SectionX03_SEDESCX40, Message.ApolloImport_Section_X03_SEDESCX40},
            {SectionX03_SEDESCX41, Message.ApolloImport_Section_X03_SEDESCX41},
            {SectionX03_SEDESCX42, Message.ApolloImport_Section_X03_SEDESCX42},
            {SectionX03_SEDESCX43, Message.ApolloImport_Section_X03_SEDESCX43},
            {SectionX03_SEDESCX44, Message.ApolloImport_Section_X03_SEDESCX44},
            {SectionX03_SEDESCX45, Message.ApolloImport_Section_X03_SEDESCX45},
            {SectionX03_SEDESCX46, Message.ApolloImport_Section_X03_SEDESCX46},
            {SectionX03_SEDESCX47, Message.ApolloImport_Section_X03_SEDESCX47},
            {SectionX03_SEDESCX48, Message.ApolloImport_Section_X03_SEDESCX48},
            {SectionX03_SEDESCX49, Message.ApolloImport_Section_X03_SEDESCX49},
            {SectionX03_SEDESCX50, Message.ApolloImport_Section_X03_SEDESCX50},
            //Unknown fields
            /*{SectionX03_SEDESCRESERVE02, "X03_SEDESCRESERVE02"},
            {SectionX03_SEDESCRESERVE03, "X03_SEDESCRESERVE03"},
            {SectionX03_SEDESCRESERVE04, "X03_SEDESCRESERVE04"},
            {SectionX03_SEDESCRESERVE05, "X03_SEDESCRESERVE05"},
            {SectionX03_SEDESCRESERVE07, "X03_SEDESCRESERVE07"},
            
            {SectionX03_SEMEASRESERVE01, "X03_SEMEASRESERVE01"},
            {SectionX03_SEMEASRESERVE02, "X03_SEMEASRESERVE02"},
            {SectionX03_SEMEASRESERVE03, "X03_SEMEASRESERVE03"},
            {SectionX03_SEMEASRESERVE04, "X03_SEMEASRESERVE04"},
            {SectionX03_SEMEASRESERVE05, "X03_SEMEASRESERVE05"},
            {SectionX03_SEMEASRESERVE06, "X03_SEMEASRESERVE06"},
            {SectionX03_SEMEASRESERVE07, "X03_SEMEASRESERVE07"},
            {SectionX03_SEMEASRESERVE08, "X03_SEMEASRESERVE08"},
            {SectionX03_SEMEASRESERVE09, "X03_SEMEASRESERVE09"},
            {SectionX03_SEMEASRESERVE10, "X03_SEMEASRESERVE10"},
            {SectionX03_SEMEASRESERVE11, "X03_SEMEASRESERVE11"},
            {SectionX03_SEMEASRESERVE12, "X03_SEMEASRESERVE12"},
            {SectionX03_SEMEASRESERVE13, "X03_SEMEASRESERVE13"},
            {SectionX03_SEMEASRESERVE14, "X03_SEMEASRESERVE14"},
            {SectionX03_SEMEASRESERVE15, "X03_SEMEASRESERVE15"},
            {SectionX03_SEMEASRESERVE16, "X03_SEMEASRESERVE16"},
            {SectionX03_SEMEASRESERVE17, "X03_SEMEASRESERVE17"},
            {SectionX03_SEMEASRESERVE18, "X03_SEMEASRESERVE18"},
            {SectionX03_SEMEASRESERVE19, "X03_SEMEASRESERVE19"},
            {SectionX03_SEMEASRESERVE20, "X03_SEMEASRESERVE20"},
            {SectionX03_SEMEASX01, "X03_SEMEASX01"},
            {SectionX03_SEMEASX02, "X03_SEMEASX02"},
            {SectionX03_SEMEASX03, "X03_SEMEASX03"},
            {SectionX03_SEMEASX04, "X03_SEMEASX04"},
            {SectionX03_SEMEASX05, "X03_SEMEASX05"},
            {SectionX03_SEMEASX06, "X03_SEMEASX06"},
            {SectionX03_SEMEASX07, "X03_SEMEASX07"},
            {SectionX03_SEMEASX08, "X03_SEMEASX08"},
            {SectionX03_SEMEASX09, "X03_SEMEASX09"},
            {SectionX03_SEMEASX10, "X03_SEMEASX10"},
            {SectionX03_SEMEASX11, "X03_SEMEASX11"},
            {SectionX03_SEMEASX12, "X03_SEMEASX12"},
            {SectionX03_SEMEASX13, "X03_SEMEASX13"},
            {SectionX03_SEMEASX14, "X03_SEMEASX14"},
            {SectionX03_SEMEASX15, "X03_SEMEASX15"},
            {SectionX03_SEMEASX16, "X03_SEMEASX16"},
            {SectionX03_SEMEASX17, "X03_SEMEASX17"},
            {SectionX03_SEMEASX18, "X03_SEMEASX18"},
            {SectionX03_SEMEASX19, "X03_SEMEASX19"},
            {SectionX03_SEMEASX20, "X03_SEMEASX20"},
            {SectionX03_SEMEASX21, "X03_SEMEASX21"},
            {SectionX03_SEMEASX22, "X03_SEMEASX22"},
            {SectionX03_SEMEASX23, "X03_SEMEASX23"},
            {SectionX03_SEMEASX24, "X03_SEMEASX24"},
            {SectionX03_SEMEASX25, "X03_SEMEASX25"},
            {SectionX03_SEMEASX26, "X03_SEMEASX26"},
            {SectionX03_SEMEASX27, "X03_SEMEASX27"},
            {SectionX03_SEMEASX28, "X03_SEMEASX28"},
            {SectionX03_SEMEASX29, "X03_SEMEASX29"},
            {SectionX03_SEMEASX30, "X03_SEMEASX30"},
            {SectionX03_SEMEASX31, "X03_SEMEASX31"},
            {SectionX03_SEMEASX32, "X03_SEMEASX32"},
            {SectionX03_SEMEASX33, "X03_SEMEASX33"},
            {SectionX03_SEMEASX34, "X03_SEMEASX34"},
            {SectionX03_SEMEASX35, "X03_SEMEASX35"},
            {SectionX03_SEMEASX36, "X03_SEMEASX36"},
            {SectionX03_SEMEASX37, "X03_SEMEASX37"},
            {SectionX03_SEMEASX38, "X03_SEMEASX38"},
            {SectionX03_SEMEASX39, "X03_SEMEASX39"},
            {SectionX03_SEMEASX40, "X03_SEMEASX40"},
            {SectionX03_SEMEASX41, "X03_SEMEASX41"},
            {SectionX03_SEMEASX42, "X03_SEMEASX42"},
            {SectionX03_SEMEASX43, "X03_SEMEASX43"},
            {SectionX03_SEMEASX44, "X03_SEMEASX44"},
            {SectionX03_SEMEASX45, "X03_SEMEASX45"},
            {SectionX03_SEMEASX46, "X03_SEMEASX46"},
            {SectionX03_SEMEASX47, "X03_SEMEASX47"},
            {SectionX03_SEMEASX48, "X03_SEMEASX48"},
            {SectionX03_SEMEASX49, "X03_SEMEASX49"},
            {SectionX03_SEMEASX50, "X03_SEMEASX50"},*/

            #endregion Section

            #region Store

            {StoreACV, Message.ApolloImport_Store_ACV},
            {StoreADDRESS1,  Message.ApolloImport_Store_Address1},
            {StoreADDRESS2, Message.ApolloImport_Store_Address2},
            {StoreCITY, Message.ApolloImport_Store_City},
            {StoreCLUSTER, Message.ApolloImport_Store_Cluster},
            {StoreCONTACT,  Message.ApolloImport_Store_Contact},
            {StoreDEPTH, Message.ApolloImport_Store_Depth},
            {StoreDISTRICT,  Message.ApolloImport_Store_District},
            {StoreHEIGHT,  Message.ApolloImport_Store_Height},
            {StoreNAME, Message.ApolloImport_Store_Name},
            {StorePHONE,  Message.ApolloImport_Store_Phone},
            {StoreSTATE,  Message.ApolloImport_Store_State},
            {StoreSTORE,  Message.ApolloImport_Store_Store},
            {StoreUSER1,  Message.ApolloImport_Store_User1},
            {StoreUSER2, Message.ApolloImport_Store_User2},
            {StoreUSER3,  Message.ApolloImport_Store_User3},
            {StoreUSER4,  Message.ApolloImport_Store_User4},
            {StoreWIDTH,  Message.ApolloImport_Store_Width},
            {StoreZIP,  Message.ApolloImport_Store_Zip},

            #endregion Store

            #region Retailer

            {RetailerADDRESS1, Message.ApolloImport_Retailer_Address1},
            {RetailerADDRESS2, Message.ApolloImport_Retailer_Address2},
            {RetailerCITY, Message.ApolloImport_Retailer_City},
            {RetailerCONTACT, Message.ApolloImport_Retailer_Contact},
            {RetailerNAME, Message.ApolloImport_Retailer_Name},
            {RetailerNUMSTORES, Message.ApolloImport_Retailer_NumStores},
            {RetailerPHONE, Message.ApolloImport_Retailer_Phone},
            {RetailerRETAILER, Message.ApolloImport_Retailer_Retailer},
            {RetailerSTATE, Message.ApolloImport_Retailer_State},
            {RetailerZIP, Message.ApolloImport_Retailer_Zip},

            #endregion Retailer

            #region Component
            {ComponentName, Message.ApolloImport_Component_Name},
            {ComponentAUTOCALCFRONTS, Message.ApolloImport_Component_AutoCalcFronts},
            {ComponentAVAILABLECUBIC, Message.ApolloImport_Component_AvailableCubic},
            {ComponentAVAILABLELINEAR, Message.ApolloImport_Component_AvailableLinear},
            {ComponentAVAILABLESQUARE, Message.ApolloImport_Component_AvailableSuqare},
            {ComponentAVERAGECASES, Message.ApolloImport_Component_AverageCases},
            {ComponentAVERAGEFACINGS, Message.ApolloImport_Component_AverageFacings},
            {ComponentBACKGROUND, Message.ApolloImport_Component_Background},
            {ComponentBRACKETDEPTH, Message.ApolloImport_Component_BracketDepth},
            {ComponentBRACKETHEIGHT,Message.ApolloImport_Component_BracketHeight},
            {ComponentBRACKETINCREMENT, Message.ApolloImport_Component_BracketIncrement},
            {ComponentBRACKETSLOPE,Message.ApolloImport_Component_BracketSlope},
            {ComponentBRACKETTHICKNESS, Message.ApolloImport_Component_BracketThickness},
            {ComponentCAPACITY, Message.ApolloImport_Component_Capacity},
            {ComponentCASES, Message.ApolloImport_Component_Cases},
            {ComponentCUBICTHROUGHPUT, Message.ApolloImport_Component_CubicThroughput},
            {ComponentDISPLAYNUM, Message.ApolloImport_Component_DisplayNum},
            {ComponentDIVINC, Message.ApolloImport_Component_DivInc},
            {ComponentDIVTHICK, Message.ApolloImport_Component_DivThick},
            {ComponentDPP, Message.ApolloImport_Component_DPP},
            {ComponentEDGE, Message.ApolloImport_Component_Edge},
            {ComponentFACINGS, Message.ApolloImport_Component_Facings},
            {ComponentFITSTATUS, Message.ApolloImport_Component_FitStatus},
            {ComponentFOREGROUND, Message.ApolloImport_Component_Foreground},
            {ComponentGRILLHEIGHT, Message.ApolloImport_Component_GrillHeight},
            {ComponentGRILLTHICKNESS, Message.ApolloImport_Component_GrillThickness},
            {ComponentHICASES, Message.ApolloImport_Component_HiCases},
            {ComponentHIDAYS, Message.ApolloImport_Component_HiDays},
            {ComponentHIFACINGS, Message.ApolloImport_Component_HiFacings},
            {ComponentINVATRETAIL, Message.ApolloImport_Component_Invatretail},
            {ComponentINVENTORY,Message.ApolloImport_Component_Inventory},
            {ComponentLOWCASES, Message.ApolloImport_Component_LowCases},
            {ComponentLOWDAYS, Message.ApolloImport_Component_LowDays},
            {ComponentLOWFACINGS, Message.ApolloImport_Component_LowFacings},
            {ComponentMAXMERCHDEPTH, Message.ApolloImport_Component_MaxMerchDepth},
            {ComponentMERCHDEPTH, Message.ApolloImport_Component_MerchDepth},
            {ComponentMERCHHEIGHT, Message.ApolloImport_Component_MerchHeight},
            {ComponentMOVEMENT, Message.ApolloImport_Component_Movement},
            {ComponentNOTCHNUMBER, Message.ApolloImport_Component_NotchNumber},
            {ComponentNUMSKUS, Message.ApolloImport_Component_NumSKUs},
            {ComponentPROFIT, Message.ApolloImport_Component_Profit},
            {ComponentRESERVED1, Message.ApolloImport_Component_Reserved1}, //Rotation
            {ComponentRESERVED2, Message.ApolloImport_Component_Reserved2},
            {ComponentRESERVED3, Message.ApolloImport_Component_Reserved3},
            {ComponentRESERVED4, Message.ApolloImport_Component_Reserved4},
            {ComponentRESERVED5, Message.ApolloImport_Component_Reserved5}, //Advanced Fixtures
            {ComponentRESERVED6, Message.ApolloImport_Component_Reserved6},
            {ComponentROII, Message.ApolloImport_Component_ROII},
            {ComponentSALES, Message.ApolloImport_Component_Sales},
            {ComponentSHELFDEPTH, Message.ApolloImport_Component_ShelfDepth},
            {ComponentSHELFHEIGHT, Message.ApolloImport_Component_ShelfHeight},
            {ComponentSHELFTYPE, Message.ApolloImport_Component_ShelfType},
            {ComponentSHELFWIDTH, Message.ApolloImport_Component_ShelfWidth},
            {ComponentSLOPE, Message.ApolloImport_Component_Slope},
            {ComponentSPREADMODE, Message.ApolloImport_Component_SpreadMode},
            {ComponentTHICKNESS, Message.ApolloImport_Component_Thickness},
            {ComponentUSEDCUBIC, Message.ApolloImport_Component_UsedCubic},
            {ComponentUSEDLINEAR, Message.ApolloImport_Component_UsedLinear},
            {ComponentUSEDSQUARE, Message.ApolloImport_Component_UsedSquare},
            {ComponentUSERDESC1, Message.ApolloImport_Component_UserDesc1},
            {ComponentUSERDESC2, Message.ApolloImport_Component_UserDesc2},
            {ComponentUSERDESC3, Message.ApolloImport_Component_UserDesc3},
            {ComponentUSERDESC4, Message.ApolloImport_Component_UserDesc4},
            {ComponentUSERNUM1, Message.ApolloImport_Component_UserNum1},
            {ComponentUSERNUM2, Message.ApolloImport_Component_UserNum2},
            {ComponentUSERNUM3, Message.ApolloImport_Component_UserNum3},
            {ComponentXINC, Message.ApolloImport_Component_Xinc},
            {ComponentXPEGEND, Message.ApolloImport_Component_XPegEnd},
            {ComponentXPEGSTART, Message.ApolloImport_Component_XPegStart},
            {ComponentXPOS, Message.ApolloImport_Component_XPos},
            {ComponentYINC, Message.ApolloImport_Component_YInc},
            {ComponentYPEGEND, Message.ApolloImport_Component_YPegEnd},
            {ComponentYPEGSTART, Message.ApolloImport_Component_YPegStart},
            {ComponentYPOS, Message.ApolloImport_Component_YPos},
            {ComponentZPOS, Message.ApolloImport_Component_ZPos},

            //Unknown
            //{ComponentX04_SHDESCX50, "X04_SHDESCX50"},
            //{ComponentX04_SHMEASRESERVE01, "X04_SHMEASRESERVE01"},
            //{ComponentX04_SHMEASRESERVE02, "X04_SHMEASRESERVE02"},
            //{ComponentX04_SHMEASRESERVE03, "X04_SHMEASRESERVE03"},
            //{ComponentX04_SHMEASRESERVE04, "X04_SHMEASRESERVE04"},
            //{ComponentX04_SHMEASRESERVE05, "X04_SHMEASRESERVE05"},
            //{ComponentX04_SHMEASRESERVE06, "X04_SHMEASRESERVE06"},
            //{ComponentX04_SHMEASRESERVE07, "X04_SHMEASRESERVE07"},
            //{ComponentX04_SHMEASRESERVE08, "X04_SHMEASRESERVE08"},
            //{ComponentX04_SHMEASRESERVE09, "X04_SHMEASRESERVE09"},
            //{ComponentX04_SHMEASRESERVE10, "X04_SHMEASRESERVE10"},
            //{ComponentX04_SHMEASRESERVE11, "X04_SHMEASRESERVE11"},
            //{ComponentX04_SHMEASRESERVE12, "X04_SHMEASRESERVE12"},
            //{ComponentX04_SHMEASRESERVE13, "X04_SHMEASRESERVE13"},
            //{ComponentX04_SHMEASRESERVE14, "X04_SHMEASRESERVE14"},
            //{ComponentX04_SHMEASRESERVE15, "X04_SHMEASRESERVE15"},
            //{ComponentX04_SHMEASRESERVE16, "X04_SHMEASRESERVE16"},
            //{ComponentX04_SHMEASRESERVE17, "X04_SHMEASRESERVE17"},
            //{ComponentX04_SHMEASRESERVE18, "X04_SHMEASRESERVE18"},
            //{ComponentX04_SHMEASRESERVE19, "X04_SHMEASRESERVE19"},
            //{ComponentX04_SHMEASRESERVE20, "X04_SHMEASRESERVE20"},
            //{ComponentX04_SHMEASX01, "X04_SHMEASX01"},
            //{ComponentX04_SHMEASX02, "X04_SHMEASX02"},
            //{ComponentX04_SHMEASX03, "X04_SHMEASX03"},
            //{ComponentX04_SHMEASX04, "X04_SHMEASX04"},
            //{ComponentX04_SHMEASX05, "X04_SHMEASX05"},
            //{ComponentX04_SHMEASX06, "X04_SHMEASX06"},
            //{ComponentX04_SHMEASX07, "X04_SHMEASX07"},
            //{ComponentX04_SHMEASX08, "X04_SHMEASX08"},
            //{ComponentX04_SHMEASX09, "X04_SHMEASX09"},
            //{ComponentX04_SHMEASX10, "X04_SHMEASX10"},
            //{ComponentX04_SHMEASX11, "X04_SHMEASX11"},
            //{ComponentX04_SHMEASX12, "X04_SHMEASX12"},
            //{ComponentX04_SHMEASX13, "X04_SHMEASX13"},
            //{ComponentX04_SHMEASX14, "X04_SHMEASX14"},
            //{ComponentX04_SHMEASX15, "X04_SHMEASX15"},
            //{ComponentX04_SHMEASX16, "X04_SHMEASX16"},
            //{ComponentX04_SHMEASX17, "X04_SHMEASX17"},
            //{ComponentX04_SHMEASX18, "X04_SHMEASX18"},
            //{ComponentX04_SHMEASX19, "X04_SHMEASX19"},
            //{ComponentX04_SHMEASX20, "X04_SHMEASX20"},
            //{ComponentX04_SHMEASX21, "X04_SHMEASX21"},
            //{ComponentX04_SHMEASX22, "X04_SHMEASX22"},
            //{ComponentX04_SHMEASX23, "X04_SHMEASX23"},
            //{ComponentX04_SHMEASX24, "X04_SHMEASX24"},
            //{ComponentX04_SHMEASX25, "X04_SHMEASX25"},
            //{ComponentX04_SHMEASX26, "X04_SHMEASX26"},
            //{ComponentX04_SHMEASX27, "X04_SHMEASX27"},
            //{ComponentX04_SHMEASX28, "X04_SHMEASX28"},
            //{ComponentX04_SHMEASX29, "X04_SHMEASX29"},
            //{ComponentX04_SHMEASX30, "X04_SHMEASX30"},
            //{ComponentX04_SHMEASX31, "X04_SHMEASX31"},
            //{ComponentX04_SHMEASX32, "X04_SHMEASX32"},
            //{ComponentX04_SHMEASX33, "X04_SHMEASX33"},
            //{ComponentX04_SHMEASX34, "X04_SHMEASX34"},
            //{ComponentX04_SHMEASX35, "X04_SHMEASX35"},
            //{ComponentX04_SHMEASX36, "X04_SHMEASX36"},
            //{ComponentX04_SHMEASX37, "X04_SHMEASX37"},
            //{ComponentX04_SHMEASX38, "X04_SHMEASX38"},
            //{ComponentX04_SHMEASX39, "X04_SHMEASX39"},
            //{ComponentX04_SHMEASX40, "X04_SHMEASX40"},
            //{ComponentX04_SHMEASX41, "X04_SHMEASX41"},
            //{ComponentX04_SHMEASX42, "X04_SHMEASX42"},
            //{ComponentX04_SHMEASX43, "X04_SHMEASX43"},
            //{ComponentX04_SHMEASX44, "X04_SHMEASX44"},
            //{ComponentX04_SHMEASX45, "X04_SHMEASX45"},
            //{ComponentX04_SHMEASX46, "X04_SHMEASX46"},
            //{ComponentX04_SHMEASX47, "X04_SHMEASX47"},
            //{ComponentX04_SHMEASX48, "X04_SHMEASX48"},
            //{ComponentX04_SHMEASX49, "X04_SHMEASX49"},
            //{ComponentX04_SHMEASX50, "X04_SHMEASX50"},

            #endregion Component

            #region Product

            {ProductUPC, Message.ApolloImport_Product_UPC},
            {ProductSTOCKCODE, Message.ApolloImport_Product_StockCode},
            {ProductLONGDESC, Message.ApolloImport_Product_LongDesc},             
            {ProductSIZE, Message.ApolloImport_Product_Size},
            {ProductBRAND, Message.ApolloImport_Product_Brand},
            {ProductUOM, Message.ApolloImport_Product_UOM},
            {ProductSCREENDESC, Message.ApolloImport_Product_ScreenDesc},
            {ProductCHAR, Message.ApolloImport_Product_Char},
            {ProductCOLOR, Message.ApolloImport_Product_Color},
            {ProductPLOTDESC1, Message.ApolloImport_Product_PlotDesc1},
            {ProductPLOTDESC2, Message.ApolloImport_Product_PlotDesc2},
            {ProductSYMBOL, Message.ApolloImport_Product_Symbol},
            {ProductCATEGORY, Message.ApolloImport_Product_Category},
            {ProductSUBCATEGORY, Message.ApolloImport_Product_SubCategory},
            {ProductCOMPONENT, Message.ApolloImport_Product_Component},
            {ProductMANUFACTURER, Message.ApolloImport_Product_Manufactuer},
            {ProductPACKAGE, Message.ApolloImport_Product_Package},
            {ProductDISTRIBUTION, Message.ApolloImport_Product_Description},
            {ProductMULTIPACK, Message.ApolloImport_Product_MultiPack},
            {ProductCASEPACK, Message.ApolloImport_Product_Casepack},
            {ProductMOVEMENTADJ, Message.ApolloImport_Product_MovementAdj},
            {ProductPRODHEIGHT, Message.ApolloImport_Product_ProductHeight},
            {ProductPRODWIDTH, Message.ApolloImport_Product_ProductWidth},
            {ProductPRODDEPTH, Message.ApolloImport_Product_ProductDepth},
            {ProductCASEHEIGHT, Message.ApolloImport_Product_CaseHeight},
            {ProductCASEWIDTH, Message.ApolloImport_Product_CaseWidth},
            {ProductCASEDEPTH, Message.ApolloImport_Product_CaseDepth},
            {ProductCASEWEIGHT, Message.ApolloImport_Product_Weight},
            {ProductNESTING, Message.ApolloImport_Product_Nesting},
            {ProductOVERHANG, Message.ApolloImport_Product_Overhang},
            {ProductXPEGHOLE, Message.ApolloImport_Product_XPeghole},
            {ProductYPEGHOLE, Message.ApolloImport_Product_YPeghole},
            {ProductFINGER, Message.ApolloImport_Product_Finger},
            {ProductMAXFRONTS, Message.ApolloImport_Product_MaxFronts},
            {ProductMAXLAYOVERS, Message.ApolloImport_Product_MaLayovers},
            {ProductMAXFRONTSDEEP, Message.ApolloImport_Product_MaxFrontsDeep},
            {ProductADJTOCASECOST, Message.ApolloImport_Product_AdjToCaseCost},
            {ProductARRIVEAS, Message.ApolloImport_Product_Arriveas},
            {ProductAVRNOCASESDSDSH, Message.ApolloImport_Product_AvrNoCaseDSDSh},
            {ProductEXPECTEDMOVEMENT,Message.ApolloImport_Product_ExpectedMovement},
            {ProductMAXCASES, Message.ApolloImport_Product_MaxCases},
            {ProductMAXDAYS, Message.ApolloImport_Product_MaxDays},
            {ProductMAXFACING, Message.ApolloImport_Product_MaxFacings},
            {ProductMAXHEIGHT, Message.ApolloImport_Product_MaxHeight},
            {ProductMAXUNITS, Message.ApolloImport_Product_MaxUnits},
            {ProductMINCASES, Message.ApolloImport_Product_MinCases},
            {ProductMINDAYS, Message.ApolloImport_Product_MinDays},
            {ProductMINFACING, Message.ApolloImport_Product_MinFacings},
            {ProductMINUNITS, Message.ApolloImport_Product_MinUnits},
            {ProductNETDAYSCREDIT, Message.ApolloImport_Product_NetDaysCredit},
            {ProductNOCASESPALLET, Message.ApolloImport_Product_NoCasesPallet},
            {ProductNOINNERPACKS, Message.ApolloImport_Product_NoInnerPacks},
            {ProductNOTRAYSCASE, Message.ApolloImport_Product_NoTrayCase},
            {ProductPattern, Message.ApolloImport_Product_Pattern},
            {ProductRACKOPENHT, Message.ApolloImport_Product_RackOpenHT},
            {ProductREPLENISHMETHOD, Message.ApolloImport_Product_ReplenishMethod},
            {ProductRESERVED1, Message.ApolloImport_Product_Reserve1},
            {ProductRESERVED2, Message.ApolloImport_Product_Reserve2},
            {ProductRESERVED3, Message.ApolloImport_Product_Reserve3},
            {ProductRESERVED4, Message.ApolloImport_Product_Reserve4},
            {ProductRESERVED5, Message.ApolloImport_Product_Reserve5},
            {ProductRESERVED6, Message.ApolloImport_Product_Reserve6},
            {ProductRESTOCK, Message.ApolloImport_Product_Restock},
            {ProductSECTIONID, Message.ApolloImport_Product_SectionId},
            {ProductSECTIONSHARE, Message.ApolloImport_Product_SectionShare},
            {ProductSERVICE, Message.ApolloImport_Product_Service},
            {ProductSHARE, Message.ApolloImport_Product_Share},
            {ProductSTOREPRICEMETHOD, Message.ApolloImport_Product_StorePriceMethod},
            {ProductSTORERECUNLOAD, Message.ApolloImport_Product_StoreRecUnload},
            {ProductUSERDESCA, Message.ApolloImport_Product_DescriptionA},
            {ProductUSERDESCB, Message.ApolloImport_Product_DescriptionB},
            {ProductUSERDESCC, Message.ApolloImport_Product_DescriptionC},
            {ProductUSERDESCD, Message.ApolloImport_Product_DescriptionD},
            {ProductUSERDESCE, Message.ApolloImport_Product_DescriptionE},
            {ProductUSERDESCF, Message.ApolloImport_Product_DescriptionF},
            {ProductUSERDESCG, Message.ApolloImport_Product_DescriptionG},
            {ProductUSERDESCH, Message.ApolloImport_Product_DescriptionH},
            {ProductUSERDESCI, Message.ApolloImport_Product_DescriptionI},
            {ProductUSERDESCJ, Message.ApolloImport_Product_DescriptionJ},
            {ProductUSERMEASUREA, Message.ApolloImport_Product_MeasureA},
            {ProductUSERMEASUREB, Message.ApolloImport_Product_MeasureB},
            {ProductUSERMEASUREC, Message.ApolloImport_Product_MeasureC},
            {ProductUSERMEASURED, Message.ApolloImport_Product_MeasureD},
            {ProductUSERMEASUREE, Message.ApolloImport_Product_MeasureE},
            {ProductUSERMEASUREF, Message.ApolloImport_Product_MeasureF},
            {ProductUSERMEASUREG, Message.ApolloImport_Product_MeasureG},
            {ProductUSERMEASUREH, Message.ApolloImport_Product_MeasureH},
            {ProductUSERMEASUREI, Message.ApolloImport_Product_MeasureI},
            {ProductUSERMEASUREJ, Message.ApolloImport_Product_MeasureJ},
            {ProductVARIANCE, Message.ApolloImport_Product_Variance},
            {ProductWARERECEIVE, Message.ApolloImport_Product_WareRecive},

            //Unknown
            {ProductX06_DESCX01, Message.ApolloImport_Product_X06_DESCX01},
            {ProductX06_DESCX02, Message.ApolloImport_Product_X06_DESCX02},
            {ProductX06_DESCX03, Message.ApolloImport_Product_X06_DESCX03},
            {ProductX06_DESCX04, Message.ApolloImport_Product_X06_DESCX04},
            {ProductX06_DESCX05, Message.ApolloImport_Product_X06_DESCX05},
            {ProductX06_DESCX06, Message.ApolloImport_Product_X06_DESCX06},
            {ProductX06_DESCX07, Message.ApolloImport_Product_X06_DESCX07},
            {ProductX06_DESCX08, Message.ApolloImport_Product_X06_DESCX08},
            {ProductX06_DESCX09, Message.ApolloImport_Product_X06_DESCX09},
            {ProductX06_DESCX10, Message.ApolloImport_Product_X06_DESCX10},
            //{ProductX06_MEASRESERVE01, "X06_MEASRESERVE01"},

            //Product Section
            {SectionProductRealMovement, Message.ApolloImport_Product_RealMovement},
            {SectionProductCost, Message.ApolloImport_Product_Cost},
            {SectionProductCaseCost, Message.ApolloImport_Product_CaseCost},
            {SectionProductVAT, Message.ApolloImport_Product_VAT},
            {SectionProductRETAIL, Message.ApolloImport_Product_Retail},
            {SectionProductDPCA, Message.ApolloImport_Product_DPCA},
            {SectionProductPAYDISCOUNT, Message.ApolloImport_Product_PayDiscount},
            {SectionProductSELLINGPRICE, Message.ApolloImport_Product_SellingPrice},
            {SectionProductMARGIN, Message.ApolloImport_Product_Margin},

            #endregion Product

            #region Position
            {PositionSales, Message.ApolloImport_Position_Sales},
            {PositionACTUALMARGIN, Message.ApolloImport_Position_ActualMargin},
            {PositionADJUSTEDMOVEMENT, Message.ApolloImport_Position_AdjustedMovement},
            {PositionBACKROOMSTOCK, Message.ApolloImport_Position_BackRoomStock},
            {PositionCAPACITY, Message.ApolloImport_Position_Capacity},
            {PositionCAPACITYPERCUBIC, Message.ApolloImport_Position_CapacityPerCubic},
            {PositionCAPACITYPERLINEAR, Message.ApolloImport_Position_CapacityPerLinear},
            {PositionCAPACITYPERSQUARE, Message.ApolloImport_Position_CapacityPerSquare},
            {PositionCASES, Message.ApolloImport_Position_Cases},
            {PositionCONTRIBTOMARGIN, Message.ApolloImport_Position_ContribToMargin},
            {PositionCUBIC, Message.ApolloImport_Position_Cubic},
            {PositionDAYSOFSUPPLY, Message.ApolloImport_Position_DaysOfSupply},
            {PositionDECOTEXT, Message.ApolloImport_Position_DecoText},
            {PositionDISTMOVEMENT, Message.ApolloImport_Position_DistMovement},
            {PositionDPP, Message.ApolloImport_Position_DPP},
            {PositionDPPPERCENT,Message.ApolloImport_Position_DPPPercent},
            {PositionDPPPERCUBIC, Message.ApolloImport_Position_DPPPerCubic},
            {PositionDPPPERLINEAR, Message.ApolloImport_Position_DPPPerLinear},
            {PositionDPPPERSQUARE, Message.ApolloImport_Position_DPPPerSquare},
            {PositionDPPPERUNIT, Message.ApolloImport_Position_DPPPerUnit},
            {PositionEDGE, Message.ApolloImport_Position_Edge},
            {PositionFITSTATUS, Message.ApolloImport_Position_FitStatus},
            {PositionGMROI, Message.ApolloImport_Position_GMROI},
            {PositionINVCOSTPERCUBIC, Message.ApolloImport_Position_InvCostPerCubic},
            {PositionINVCOSTPERLINEAR, Message.ApolloImport_Position_InvCostPerLinear},
            {PositionINVCOSTPERSQUARE, Message.ApolloImport_Position_InvCostPerSquare},
            {PositionINVENTORY, Message.ApolloImport_Position_Inventory},
            {PositionINVENTORYATRETAIL, Message.ApolloImport_Position_InventoryRetail},
            {PositionINVRETPERCUBIC, Message.ApolloImport_Position_InventoryPerCubic},
            {PositionINVRETPERLINEAR, Message.ApolloImport_Position_InventoryPerLinear},
            {PositionINVRETPERSQUARE, Message.ApolloImport_Position_InventoryPerSquare},
            {PositionLAYOVERS, Message.ApolloImport_Position_Layovers},
            {PositionLAYOVERSDEEP, Message.ApolloImport_Position_LayoversDeep},
            {PositionLINEAR, Message.ApolloImport_Position_Linear},
            {PositionLOSTSALES, Message.ApolloImport_Position_LostSales},
            {PositionMERCHANDISINGTYPE, Message.ApolloImport_Position_MerchandisingType},
            {PositionMOVEMENTPERCUBIC, Message.ApolloImport_Position_MovementPerCubic},
            {PositionMOVEMENTPERLINEAR, Message.ApolloImport_Position_MovementPerLinear},
            {PositionMOVEMENTPERSQUARE, Message.ApolloImport_Position_MovementPerSquare},
            {PositionMULTICAPACITY, Message.ApolloImport_Position_MultiCapcaity},
            {PositionMULTIORIENT, Message.ApolloImport_Position_MultiOrient},
            {PositionNORMALSTOCK, Message.ApolloImport_Position_NormalStock},
            {PositionORIENTATION, Message.ApolloImport_Position_Orientation},
            {PositionPCTCAPACITYPERSECT, Message.ApolloImport_Position_PCTCapacityPerSect},
            {PositionPCTCAPACITYPERSHF, Message.ApolloImport_Position_PCTCapacityPerShf},
            {PositionPCTCUBICPERSECT, Message.ApolloImport_Position_PCTCubicPerSect},
            {PositionPCTCUBICPERSHF, Message.ApolloImport_Position_PCTCubicPerShf},
            {PositionPCTDPPPERSECT,Message.ApolloImport_Position_PCTDPPPerSect},
            {PositionPCTDPPPERSHF, Message.ApolloImport_Position_PCTDPPPerShf},
            {PositionPCTINVCOSTPERSECT, Message.ApolloImport_Position_PCTInvCostPerSect},
            {PositionPCTINVCOSTPERSHF, Message.ApolloImport_Position_PCTInvCostPerShf},
            {PositionPCTLINEARPERSECT, Message.ApolloImport_Position_PCTLinearPerSect},
            {PositionPCTLINEARPERSHF, Message.ApolloImport_Position_PCTLinearPerShf},
            {PositionPCTMOVEMENTPERSECT, Message.ApolloImport_Position_PCTMovementPerSect},
            {PositionPCTMOVEMENTPERSHF, Message.ApolloImport_Position_PCTMovementPerShf},
            {PositionPCTPROFITPERSECT, Message.ApolloImport_Position_PCTProfitPerSect},
            {PositionPCTPROFITPERSHF, Message.ApolloImport_Position_PCTProfitPerShf},
            {PositionPCTSALESPERSECT, Message.ApolloImport_Position_PCTSalesPerSect},
            {PositionPCTSALESPERSHF, Message.ApolloImport_Position_PCTSalesPerShf},
            {PositionPCTSQUAREPERSECT, Message.ApolloImport_Position_PCTSquarePerSect},
            {PositionPCTSQUAREPERSHF, Message.ApolloImport_Position_PCTSquarePerShf},
            {PositionPEGLENGTH, Message.ApolloImport_Position_PegLength},
            {PositionPEGTYPE, Message.ApolloImport_Position_PegType},
            {PositionPOSITION, Message.ApolloImport_Position_Position},
            {PositionPROFIT, Message.ApolloImport_Position_Profit},
            {PositionPROFITPERCUBIC, Message.ApolloImport_Position_ProfitPerCubic},
            {PositionPROFITPERLINEAR, Message.ApolloImport_Position_ProfitPerLinear},
            {PositionPROFITPERSQUARE,Message.ApolloImport_Position_ProfitPerSquare},
            {PositionPROFITPERUNIT, Message.ApolloImport_Position_ProfitPerUnit},
            {PositionRESERVED1, Message.ApolloImport_Position_Reserved1},
            {PositionRESERVED2, Message.ApolloImport_Position_Reserved2},
            {PositionRESERVED5, Message.ApolloImport_Position_Reserved5},
            {PositionROII, Message.ApolloImport_Position_ROII},
            {PositionSAFETYSTOCK, Message.ApolloImport_Position_SafetyStock},
            {PositionSALESGROWTH, Message.ApolloImport_Position_SalesGrowth},
            {PositionSALESPERCUBIC, Message.ApolloImport_Position_SalesPerCubic},
            {PositionSALESPERFACING, Message.ApolloImport_Position_SalesPerFacing},
            {PositionSALESPERLINEAR, Message.ApolloImport_Position_SalesPerLinear},
            {PositionSALESPERSQUARE, Message.ApolloImport_Position_SalesPerSquare},
            {PositionSQUARE, Message.ApolloImport_Position_Square},
            {PositionSUGGESTEDFACINGS, Message.ApolloImport_Position_SuggestedFacings},
            {PositionTAGNUMBER, Message.ApolloImport_Position_TagNumber},
            {PositionTURNS, Message.ApolloImport_Position_Turns},
            {PositionUPC, Message.ApolloImport_Position_UPC},
            {PositionWEEKSOFSUPPLY, Message.ApolloImport_Position_WeeksOfSupply},
            {PositionX_ENDPOS, Message.ApolloImport_Position_XEndPos},
            {PositionXDIM, Message.ApolloImport_Position_XDim},
            {PositionXFACINGS, Message.ApolloImport_Position_XFacings},
            {PositionXGAP, Message.ApolloImport_Position_XGap},
            {PositionXPEGHOLEPOS, Message.ApolloImport_Position_XPegHolePos},
            {PositionXPEGNUM, Message.ApolloImport_Position_XPegNum},
            {PositionXPOS, Message.ApolloImport_Position_XPos},
            {PositionY_ENDPOS, Message.ApolloImport_Position_YEndPos},
            {PositionYDIM, Message.ApolloImport_Position_YDim},
            {PositionYFACINGS, Message.ApolloImport_Position_YFacings},
            {PositionYGAP, Message.ApolloImport_Position_YGap},
            {PositionYPEGHOLEPOS, Message.ApolloImport_Position_YPegHolePos},
            {PositionYPEGNUM, Message.ApolloImport_Position_YPegNum},
            {PositionYPOS, Message.ApolloImport_Position_YPos},
            {PositionZ_ENDPOS, Message.ApolloImport_Position_ZEndPos},
            {PositionZDIM, Message.ApolloImport_Position_ZDim},
            {PositionZFACINGS, Message.ApolloImport_Position_ZFacings},
            {PositionZGAP, Message.ApolloImport_Position_ZGap},
            {PositionZPOS, Message.ApolloImport_Position_ZPos},

            //Unknown
            //{PositionX05_POSMEASX04, "X05_POSMEASX04"},
            //{PositionX05_POSMEASX05, "X05_POSMEASX05"},
            #endregion Postion
        };

        #endregion

        #region Properties

        /// <summary>
        ///     Default name for the Apollo export template file. [Apollo Default.pogeft]
        /// </summary>
        public static String DefaultPogeft { get { return "Symphony EYC Apollo V11 Default.pogeft"; } }

        /// <summary>
        /// The version of Apollo we are exporting
        /// </summary>
        public static String ExportVersion { get { return "11.0.020"; } }


        public static List<String> AvailableVersions
        {
            get { return _availableVersions ?? (_availableVersions = new List<String> { "9.1", "11.0" }); }
        }

        public static IEnumerable<String> SupportedVersions
        {
            get
            {
                yield return "11.0";
            }
        }

        public static Dictionary<String, ObjectFieldInfo> PlanogramFieldInfos
        {
            get { return _planogramFieldInfos ?? (_planogramFieldInfos = InitializePlanogramFieldInfos()); }
        }

        private static Dictionary<String, ObjectFieldInfo> InitializePlanogramFieldInfos()
        {
            Dictionary<String, ObjectFieldInfo> output = new Dictionary<String, ObjectFieldInfo>();
            
            AddToInfoLookup<ApolloDataSetStore>(output);
            AddToInfoLookup<ApolloDataSetRetailer>( output);
            AddToInfoLookup<ApolloDataSetSectionSectionData>(output);
            
            return output;
        }

        public static Dictionary<String, ObjectFieldInfo> ComponentFieldInfos
        {
            get { return _componentFieldInfos ?? (_componentFieldInfos = InitializeComponentFieldInfos()); }
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeComponentFieldInfos()
        {
            Dictionary<String, ObjectFieldInfo> output = new Dictionary<String, ObjectFieldInfo>();
            AddToInfoLookup<ApolloDataSetSectionShelvesShelfShelfData>(output);
            return output;
        }

        public static Dictionary<String, ObjectFieldInfo> FixtureFieldInfos
        {
            get { return _fixtureFieldInfos ?? (_fixtureFieldInfos = InitializeFixtureFieldInfos()); }
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeFixtureFieldInfos()
        {
            Dictionary<String, ObjectFieldInfo> output = new Dictionary<String, ObjectFieldInfo>();
            AddToInfoLookup<ApolloDataSetSectionSectionData>(output);
            return output;
        }

        public static Dictionary<String, ObjectFieldInfo> ProductFieldInfos
        {
            get { return _productFieldInfos ?? (_productFieldInfos = InitializeProductFieldInfos()); }
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeProductFieldInfos()
        {
            Dictionary<String, ObjectFieldInfo> output = new Dictionary<String, ObjectFieldInfo>();
            AddToInfoLookup<ApolloDataSetProductsProduct>(output);
            AddToInfoLookup<ApolloDataSetSectionSectionProductsSectionProduct>(output);
            return output;
        }

        public static Dictionary<String, ObjectFieldInfo> PerformanceFieldInfos
        {
            get { return _performanceFieldInfos ?? (_performanceFieldInfos = InitializePerformanceFieldInfos()); }
        }

        private static Dictionary<String, ObjectFieldInfo> InitializePerformanceFieldInfos()
        {
            Dictionary<String, ObjectFieldInfo> output = new Dictionary<String, ObjectFieldInfo>();
            AddToInfoLookup<ApolloDataSetProductsProduct>(output);
            AddToInfoLookup<ApolloDataSetSectionSectionProductsSectionProduct>(output);
            AddToInfoLookup<PositionsPosition>(output);
            return output;
        }

        #endregion Properties

        #region Default Mappings

        public static IEnumerable<Tuple<String, String>> EnumerateDefaultMappings(PlanogramFieldMappingType value)
        {
            switch (value)
            {
                case PlanogramFieldMappingType.Planogram:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultPlanogramMappings())
                    {
                        yield return mapping;
                    }
                    yield break;
                case PlanogramFieldMappingType.Fixture:
                    //Note: V8-32321 - No mappings for fixture by default
                    //foreach (Tuple<String, String> mapping in EnumerateDefaultFixtureMappings())
                    //{
                    //    yield return mapping;
                    //}
                    yield break;
                case PlanogramFieldMappingType.Component:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultComponentMappings())
                    {
                        yield return mapping;
                    }
                    yield break;
                case PlanogramFieldMappingType.Product:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultProductMappings())
                    {
                        yield return mapping;
                    }
                    yield break;
                case PlanogramFieldMappingType.Performance:
                    yield break;
                default:
                    Debug.Fail("Unknown PlanogramFieldMappingType when calling EnumerateDefaultMappings in SpacePlanningExportHelper.");
                    yield break;
            }
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultPlanogramMappings()
        {
            Type ownerType = typeof(ApolloDataSetSectionSectionData);
            yield return new Tuple<String, String>(Planogram.LocationNameProperty.Name, StoreNAME);
            yield return new Tuple<String, String>(Planogram.NameProperty.Name, SectionName); 

            String customAttributeFormat = String.Format("{0}.{1}", Planogram.CustomAttributesProperty.Name, "{0}");
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text1Property.Name), SectionSECTION);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text3Property.Name), SectionVERSION);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text4Property.Name), SectionVERSIONDESC);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text5Property.Name), SectionSECTIONDESC);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text6Property.Name), SectionPLOTDESC1);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text7Property.Name), SectionPLOTDESC2);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text8Property.Name), SectionPLOTDESC3);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text9Property.Name), SectionUSER1);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text10Property.Name), SectionUSER2);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text11Property.Name), SectionMESSAGELINE1);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text12Property.Name), SectionMESSAGELINE2);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text13Property.Name), SectionMESSAGELINE3);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text14Property.Name), SectionMESSAGELINE4);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text15Property.Name), SectionMESSAGELINE5);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultFixtureMappings()
        {
            Type ownerType = typeof(ApolloDataSetSectionSectionData);
            yield return new Tuple<String, String>(PlanogramFixture.NameProperty.Name, SectionName);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultComponentMappings()
        {
            Type ownerType = typeof(ApolloDataSetSectionShelvesShelfShelfData);
            yield return new Tuple<String, String>(PlanogramComponent.NameProperty.Name, ComponentName);

            String customAttributeFormat = String.Format("{0}.{1}", Planogram.CustomAttributesProperty.Name, "{0}");
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text1Property.Name), ComponentUSERDESC1);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text2Property.Name), ComponentUSERDESC2);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text3Property.Name), ComponentUSERDESC3);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text4Property.Name), ComponentUSERDESC4);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value1Property.Name), ComponentUSERNUM1);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value2Property.Name), ComponentUSERNUM2);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value3Property.Name), ComponentUSERNUM3);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultProductMappings()
        {
            Type ownerType = typeof(ApolloDataSetProductsProduct);
            yield return new Tuple<String, String>(PlanogramProduct.GtinProperty.Name, ProductUPC);
            yield return new Tuple<String, String>(PlanogramProduct.NameProperty.Name, ProductLONGDESC);
            yield return new Tuple<String, String>(PlanogramProduct.BrandProperty.Name, ProductBRAND);
            yield return new Tuple<String, String>(PlanogramProduct.ShortDescriptionProperty.Name, ProductSCREENDESC);
            yield return new Tuple<String, String>(PlanogramProduct.SubcategoryProperty.Name, ProductSUBCATEGORY);
            yield return new Tuple<String, String>(PlanogramProduct.DeliveryMethodProperty.Name, ProductWARERECEIVE);
            yield return new Tuple<String, String>(PlanogramProduct.ManufacturerProperty.Name, ProductMANUFACTURER);
            yield return new Tuple<String, String>(PlanogramProduct.SizeProperty.Name, ProductSIZE);
            yield return new Tuple<String, String>(PlanogramProduct.UnitOfMeasureProperty.Name, ProductUOM);
            yield return new Tuple<String, String>(PlanogramProduct.SellPriceProperty.Name, SectionProductSELLINGPRICE);
            yield return new Tuple<String, String>(PlanogramProduct.RecommendedRetailPriceProperty.Name, SectionProductRETAIL);
            yield return new Tuple<String, String>(PlanogramProduct.CostPriceProperty.Name, SectionProductCost);
            yield return new Tuple<String, String>(PlanogramProduct.CaseCostProperty.Name, SectionProductCaseCost);
            yield return new Tuple<String, String>(PlanogramProduct.TaxRateProperty.Name, SectionProductVAT);
            yield return new Tuple<String, String>(PlanogramProduct.CasePackUnitsProperty.Name, ProductCASEPACK);

            String customAttributeFormat = String.Format("{0}.{1}", Planogram.CustomAttributesProperty.Name, "{0}");
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text1Property.Name), ProductUSERDESCA);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text2Property.Name), ProductUSERDESCB);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text3Property.Name), ProductUSERDESCC);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text4Property.Name), ProductUSERDESCD);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text5Property.Name), ProductUSERDESCE);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text6Property.Name), ProductUSERDESCF);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text7Property.Name), ProductUSERDESCG);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text8Property.Name), ProductUSERDESCH);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text9Property.Name), ProductUSERDESCI);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text10Property.Name), ProductUSERDESCJ);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text11Property.Name), ProductPLOTDESC1);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Text12Property.Name), ProductPLOTDESC2);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value1Property.Name), ProductUSERMEASUREA);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value2Property.Name), ProductUSERMEASUREB);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value3Property.Name), ProductUSERMEASUREC);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value4Property.Name), ProductUSERMEASURED);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value5Property.Name), ProductUSERMEASUREE);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value6Property.Name), ProductUSERMEASUREF);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value7Property.Name), ProductUSERMEASUREG);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value8Property.Name), ProductUSERMEASUREH);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value9Property.Name), ProductUSERMEASUREI);
            yield return new Tuple<String, String>(String.Format(customAttributeFormat, Model.CustomAttributeData.Value10Property.Name), ProductUSERMEASUREJ);

            //ProductUSERDESCJ
        }

        #endregion Default mappings

        #region Property Methods

        /// <summary>
        /// Combines a property name with its parent type into a | seperated value
        /// </summary>
        private static String CombinePropertyName<T>(PropertyInfo property)
        {
            Type type = typeof(T);
            return CombinePropertyName(type, property.Name);
        }
        private static String CombinePropertyName<T>(String propertyName)
        {
            Type type = typeof(T);
            return CombinePropertyName(type, propertyName);
        }
        private static String CombinePropertyName(Type type, String propertyName)
        {
            return String.Format(SourceFormat, type.Name, propertyName);
        }

        /// <summary>
        /// Adds Object field info from type T to the supplied lookup dictionary
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lookup"></param>
        private static void AddToInfoLookup<T>(Dictionary<String, ObjectFieldInfo> lookup)
        {
            Type type = typeof(T);
            Dictionary<String, ObjectFieldInfo> typeLookup;

            //Use the values we have already created if possible
            if (_typeInfoLookup.TryGetValue(type, out typeLookup))
            {
                foreach (var info in typeLookup)
                {
                    if (!lookup.ContainsKey(info.Key))
                    {
                        lookup.Add(info.Key, info.Value);
                    }
                }
            }
            else
            {
                typeLookup = new Dictionary<String, ObjectFieldInfo>();
                _typeInfoLookup.Add(type, typeLookup);
                PropertyInfo[] properties = type.GetProperties();
                foreach (PropertyInfo property in properties.OrderBy(p => GetPropertyFriendlyName(type, p)))
                {

                    // Console.WriteLine(String.Format("private static String _component{0} = CombinePropertyName<ApolloDataSetSectionShelvesShelfShelfData>(\"{0}\");", property.Name));
                    String propertyName = CombinePropertyName(type, property.Name);

                    String propertyFriendlyName = GetPropertyFriendlyName(type, property);

                    ObjectFieldInfo info = ObjectFieldInfo.NewObjectFieldInfo(type,
                                                                  _typeFriendlyNames[type],
                                                                  propertyName,
                                                                  propertyFriendlyName,
                                                                  typeof(String),
                                                                  ModelPropertyDisplayType.None);

                    //Add to both dictionaries.
                    if (!lookup.ContainsKey(propertyName))
                    {
                        lookup.Add(propertyName, info);
                    }
                    if (!typeLookup.ContainsKey(propertyName))
                    {
                        typeLookup.Add(propertyName, info);
                    }
                }
            }
        }


        private static String GetPropertyFriendlyName<T>(PropertyInfo property)
        {
            Type type = typeof(T);
            return GetPropertyFriendlyName(type, property);
        }
        private static String GetPropertyFriendlyName(Type type, PropertyInfo property)
        {
            String propertyFriendlyName;
            if (!_fieldFriendlyNames.TryGetValue(CombinePropertyName(type, property.Name), out propertyFriendlyName))
            {
                return property.Name;
            }

            return propertyFriendlyName;
        }

        public static PropertyInfo GetPropertyInfo<T>(String propertyName)
        {
            Type type = typeof(T);
            return GetPropertyInfo(type, propertyName);
        }
        public static PropertyInfo GetPropertyInfo(Type type, String propertyName)
        {
            PropertyInfo output;

            try
            {
                Dictionary<String, PropertyInfo> lookup;

                if (!_typePropertyLookups.TryGetValue(type, out lookup))
                {
                    lookup = type.GetProperties().ToDictionary(p => p.Name);
                    _typePropertyLookups.Add(type, lookup);
                }

                if (!lookup.TryGetValue(propertyName, out output))
                {
                    output = type.GetProperty(propertyName);
                    lookup.Add(propertyName, output);
                }
            }
            catch
            {
                output = null;
            }

            return output;
        }

        /// <summary>
        /// Gets the empty type contructor info for T from a
        /// lookup cashe, or generates it if it is not found.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ConstructorInfo GetConstructorInfo<T>()
        {
            Type type = typeof(T);
            return GetConstructorInfo(type);
        }
        /// <summary>
        /// Gets the empty type contructor info for the supplied type from a
        /// lookup cashe, or generates it if it is not found.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ConstructorInfo GetConstructorInfo(Type type)
        {
            ConstructorInfo output;
            try
            {

                if (_typeConstructorLookups.TryGetValue(type, out output))
                {
                    return output;
                }
                else
                {
                    return type.GetConstructor(System.Type.EmptyTypes);
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Creates a dictionary of setter methods for type T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static Dictionary<String, MethodInfo> GetSetterDic<T>()
        {
            Dictionary<String, MethodInfo> output = new Dictionary<String, MethodInfo>();
            foreach (var item in typeof(T).GetProperties())
            {
                output.Add(item.Name, item.GetSetMethod(true));
            }
            return output;
        }

        /// <summary>
        /// Invokes the setter method for the named property of the supplied item
        /// </summary>
        /// <param name="type"></param>
        /// <param name="propertyName"></param>
        /// <param name="item"></param>
        /// <param name="value"></param>
        public static void InvokeSetter(Type type, String propertyName, Object item, Object value)
        {
            MethodInfo setter;

            try
            {
                Dictionary<String, MethodInfo> lookup;

                if (_typeSetterLookups.TryGetValue(type, out lookup))
                {
                    if (lookup.TryGetValue(propertyName, out setter))
                    {
                        InvokeSetter(setter, item, value);
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Invokes the supplied setter method for the item.
        /// </summary>
        /// <param name="setter"></param>
        /// <param name="item"></param>
        /// <param name="value"></param>
        public static void InvokeSetter(MethodInfo setter, Object item, Object value)
        {
            lock (parameters)
            {
                parameters[0] = value;
                setter.Invoke(item, parameters);
            }
        }

        #endregion Property Methods


        #region Helper Methods

        /// <summary>
        /// Fetches a field value from the passed sourceDto object or its accompanying customAttributeDto, depending on its field value.
        /// If all else fails, returns the fallback value
        /// </summary>
        /// <typeparam name="T">The type of source object we are reading data from</typeparam>
        /// <param name="targetField">The target element to set</param>
        /// <param name="sourceDto">The source object</param>
        /// <param name="fieldMappings">The mappings lists</param>
        /// <param name="fallbackValue">The value to apply if no mapping exists or an error occurs</param>
        /// <returns>The new value to apply</returns>
        public static String GetFieldMappedValue(String targetField, Object sourceDto, IEnumerable<PlanogramFieldMappingDto> fieldMappings, Object fallbackValue)
        {
            String value = String.Empty;

            if (sourceDto == null) return value;
            if (!fieldMappings.Any()) return value;
            if (String.IsNullOrEmpty(targetField)) return value;

            //try to find the mapping for this field
            PlanogramFieldMappingDto mappingDto = fieldMappings.FirstOrDefault(m => m.Target == targetField);
            if (mappingDto != null)
            {
                String source = mappingDto.Source;
                PropertyInfo propInfo = ApolloImportHelper.GetPropertyInfo<PlanogramProductDto>(mappingDto.Source);
                object objectValue = propInfo.GetValue(sourceDto, null);
                //mapping found so return value
                value = objectValue != null ? objectValue.ToString() : String.Empty;
            }
            else
            {
                //mapping not found, use the fallback value
                value = fallbackValue != null ? fallbackValue.ToString() : String.Empty;
            }

            return value;
        }

        /// <summary>
        /// Fetches a field value from the passed sourceDto object or its accompanying customAttributeDto, depending on its field value.
        /// If all else fails, returns the fallback value
        /// </summary>
        /// <typeparam name="T">The type of source object we are reading data from</typeparam>
        /// <param name="targetField">The target element to set</param>
        /// <param name="sourceDto">The source object</param>
        /// <param name="customAttributeSourceDto">The custom source object</param>
        /// <param name="fieldMappings">The mappings lists</param>
        /// <param name="fallbackValue">The value to apply if no mapping exists or an error occurs</param>
        /// <returns>The new value to apply</returns>
        public static String GetFieldMappedValue<T>(String targetField, Object sourceDto, Object customAttributeSourceDto, IEnumerable<PlanogramFieldMappingDto> fieldMappings, Object fallbackValue = null)
        {
            String value = String.Empty;

            if (sourceDto == null) return value;
            if (!fieldMappings.Any()) return value;
            if (String.IsNullOrEmpty(targetField)) return value;

            //try to find the mapping for this field
            PlanogramFieldMappingDto mappingDto = fieldMappings.FirstOrDefault(m => m.Target == targetField);
            if (mappingDto != null)
            {
                try
                {
                    String source = mappingDto.Source;
                    String sourceField = source.Substring(source.LastIndexOf('.') + 1); //get the source field after the CustomAttributes. prefix if any
                    PropertyInfo propInfo = null;
                    object objectValue = null;
                    //Get the PropertyInfo
                    if (source.StartsWith("CustomAttributes.", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //the data is stored in a custom attribute
                        propInfo = ApolloImportHelper.GetPropertyInfo<CustomAttributeDataDto>(sourceField);
                        objectValue = propInfo.GetValue(customAttributeSourceDto, null);
                    }
                    else
                    {
                        //The data is stored in the passed object type
                        propInfo = ApolloImportHelper.GetPropertyInfo<T>(sourceField);
                        objectValue = propInfo.GetValue(sourceDto, null);
                    }

                    //mapping found so return value
                    value = objectValue != null ? objectValue.ToString() : String.Empty;
                }
                catch (Exception ex)
                {
                    //TODO Log error

                    value = fallbackValue != null ? fallbackValue.ToString() : String.Empty;
                }
            }
            else
            {
                //mapping not found, use the fallback value
                value = fallbackValue != null ? fallbackValue.ToString() : String.Empty;
            }

            return value;
        }

        /// <summary>
        /// Updates a field value in the objectToUpdate from from the passed dataDto object if a mapping applies for the field.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="mappingDto">The mapping</param>
        /// <param name="objectToUpdate">The object to update</param>
        /// <param name="metricDtos">The metric dtos</param>
        /// <param name="dataDto">The performance data dto</param>
        public static void SetPerformanceFieldMappedValue<T>(PlanogramMetricMappingDto mappingDto, Object objectToUpdate, List<PlanogramPerformanceMetricDto> metricDtos, PlanogramPerformanceDataDto dataDto)
        {
            if (dataDto == null) return; // there is no perofrmance data for this product so move on
            if (objectToUpdate == null) return;
            
            PlanogramPerformanceMetricDto metricDto = metricDtos.FirstOrDefault(m => m.Name == mappingDto.Name);
            if (metricDto == null) return; //metric defined in the mapping does not exist in the planogram's performance data

            if (metricDto != null)
            {
                try
                {
                    String destination = mappingDto.Source;
                    String destinationField = destination.Substring(destination.LastIndexOf('|') + 1); //get the source field after the CustomAttributes. prefix if any

                    if (!destination.StartsWith(typeof(T).ToString(), StringComparison.InvariantCultureIgnoreCase)) return; //mapping is not in our passed object type

                    PropertyInfo propInfo = null;
                    object objectValue = null;
                    //Get the PropertyInfo to set in the product object
                    propInfo = ApolloImportHelper.GetPropertyInfo<T>(destinationField);
                    if (propInfo == null) return; //object does not hold this property

                    //get the performance dto for the source product
                    PropertyInfo sourceDataPropInfo = dataDto.GetType().GetProperty(String.Format("P{0}", mappingDto.MetricId));
                    if (sourceDataPropInfo == null) return; //object does not hold this property

                    //get the value of the property
                    objectValue = sourceDataPropInfo.GetValue(dataDto, null);
                    String newValue = objectValue != null ? objectValue.ToString() : "0.0";

                    //set the value
                    propInfo.SetValue(objectToUpdate, newValue, null);
                }
                catch
                {
                    //Let calling method handle the error
                    throw;
                }
            }
        }

        /// <summary>
        /// Returns a string representation for a numeric value, taking allowance for null values
        /// </summary>
        /// <param name="value">The source value to convert</param>
        /// <param name="returnEmptyIfNull">If true, returns an empty string if the value is null</param>
        /// <returns>The string representation of the value</returns>
        public static String ParseNumericValue(Object value, Boolean returnEmptyIfNull = false)
        {
            if (value != null)
            {
                return value.ToString();
            }
            else
            {
                return returnEmptyIfNull? String.Empty : "0.0";
            }
        }

        /// <summary>
        /// Returns a string representation for a numeric value, taking allowance for null values
        /// </summary>
        /// <param name="value">The source value to convert</param>
        /// <param name="returnEmptyIfNull">If true, returns an empty string if the value is null</param>
        /// <param name="defaultIfNull">The value to return if the value is null and returnEmptyIfNull = false</param>
        /// <returns>The string representation of the value</returns>
        public static String ParseNumericValue(Object value, Boolean returnEmptyIfNull = false, String defaultIfNull = "0.0")
        {
            if (value != null)
            {
                return value.ToString();
            }
            else
            {
                return returnEmptyIfNull ? String.Empty : defaultIfNull;
            }
        }

        /// <summary>
        /// Converts colours from CCM to Apollo
        /// Our colours are in the order of Alpha/Red/Green/Blue going from highest number to lowest.
        /// Apollo colours are in the order of Blue/Green/Red going from highest number to lowest. Aplha is ignored
        /// </summary>
        /// <param name="ccmColourValue">The CCM V8 colour value</param>
        /// <returns>The Apollo converted colour</returns>
        public static Int32 ConvertColourCCMToApollo(Int32 ccmColourValue)
        {
            Byte r = (Byte)((ccmColourValue >> 0x10) & 0xff);
            Byte g = (Byte)((ccmColourValue >> 8) & 0xff);
            Byte b = (Byte)(ccmColourValue & 0xff);

            var uint32Color =
                    (((UInt32)b) << 16) |
                    (((UInt32)g) << 8) |
                    (UInt32)r;

            return (Int32)uint32Color;
        }

        /// <summary>
        /// Finds the nearest Apollo Colour preset value colour match to the passed CCM colour match
        /// </summary>
        /// <param name="ccmColourValue">The ccm colour to convert</param>
        /// <returns>The nearest Apollo Colour type value</returns>
        public static ApolloColourType ConvertColourCCMToNearestApolloColourType(Int32 ccmColourValue)
        {
            Byte r = (Byte)((ccmColourValue >> 0x10) & 0xff);
            Byte g = (Byte)((ccmColourValue >> 8) & 0xff);
            Byte b = (Byte)(ccmColourValue & 0xff);

            ApolloColourType nearestColour = ApolloColourType.White;
            Double nearestDistance = Double.MaxValue;

            Double distance = Double.MaxValue;

            foreach (ApolloColourType ct in Enum.GetValues(typeof(ApolloColourType)))
            {
                Byte ctr = ApolloColourTypeHelper.GetRed(ct);
                Byte ctg = ApolloColourTypeHelper.GetGreen(ct);
                Byte ctb = ApolloColourTypeHelper.GetBlue(ct);

                distance = Math.Sqrt(Math.Pow(ctr - r, 2) + Math.Pow(ctg - g, 2) + Math.Pow(ctb - b, 2));
                if (distance == 0) return ct; //exact match found

                if (distance < nearestDistance)
                {
                    //this colour type is a better match
                    nearestColour = ct;
                    nearestDistance = distance;
                }
            }
            return nearestColour;
        }

        /// <summary>
        /// Convert radians to degrees
        /// </summary>
        /// <param name="radians">The angle to convert</param>
        /// <returns>The angle in degrees</returns>
        public static Single ConvertRadiansToDegrees(Single radians)
        {
            Single angle = Convert.ToSingle(radians * (180 / Math.PI), CultureInfo.InvariantCulture);
            while (angle > 360) angle = angle - 360;
            return angle;
        }

        public static ApolloOrientationType GetApolloPositionOrientation(PlanogramPositionOrientationType positionOrientationType, PlanogramProductOrientationType productOrientationType)
        {
            if (positionOrientationType != PlanogramPositionOrientationType.Default)
            {
                switch (positionOrientationType)
                {
                    case PlanogramPositionOrientationType.Front0:
                    case PlanogramPositionOrientationType.Back0:
                    case PlanogramPositionOrientationType.Front180:
                    case PlanogramPositionOrientationType.Back180:
                        return ApolloOrientationType.FrontEnd;

                    case PlanogramPositionOrientationType.Front90:
                    case PlanogramPositionOrientationType.Back90:
                    case PlanogramPositionOrientationType.Front270:
                    case PlanogramPositionOrientationType.Back270:
                        return ApolloOrientationType.FrontSide;

                    case PlanogramPositionOrientationType.Right0:
                    case PlanogramPositionOrientationType.Left0:
                    case PlanogramPositionOrientationType.Right180:
                    case PlanogramPositionOrientationType.Left180:
                        return ApolloOrientationType.SideEnd;

                    case PlanogramPositionOrientationType.Right90:
                    case PlanogramPositionOrientationType.Left90:
                    case PlanogramPositionOrientationType.Right270:
                    case PlanogramPositionOrientationType.Left270:
                        return ApolloOrientationType.SideFront;

                    case PlanogramPositionOrientationType.Top0:
                    case PlanogramPositionOrientationType.Bottom0:
                    case PlanogramPositionOrientationType.Top180:
                    case PlanogramPositionOrientationType.Bottom180:
                        return ApolloOrientationType.EndFront;

                    case PlanogramPositionOrientationType.Top90:
                    case PlanogramPositionOrientationType.Bottom90:
                    case PlanogramPositionOrientationType.Top270:
                    case PlanogramPositionOrientationType.Bottom270:
                        return ApolloOrientationType.EndSide;
                    default:
                        return ApolloOrientationType.FrontEnd;
                }
            }
            else
            {
                switch (productOrientationType)
                {
                    case PlanogramProductOrientationType.Front0:
                    case PlanogramProductOrientationType.Back0:
                    case PlanogramProductOrientationType.Front180:
                    case PlanogramProductOrientationType.Back180:
                        return ApolloOrientationType.FrontEnd;

                    case PlanogramProductOrientationType.Front90:
                    case PlanogramProductOrientationType.Back90:
                    case PlanogramProductOrientationType.Front270:
                    case PlanogramProductOrientationType.Back270:
                        return ApolloOrientationType.FrontSide;

                    case PlanogramProductOrientationType.Right0:
                    case PlanogramProductOrientationType.Left0:
                    case PlanogramProductOrientationType.Right180:
                    case PlanogramProductOrientationType.Left180:
                        return ApolloOrientationType.SideEnd;

                    case PlanogramProductOrientationType.Right90:
                    case PlanogramProductOrientationType.Left90:
                    case PlanogramProductOrientationType.Right270:
                    case PlanogramProductOrientationType.Left270:
                        return ApolloOrientationType.SideFront;

                    case PlanogramProductOrientationType.Top0:
                    case PlanogramProductOrientationType.Bottom0:
                    case PlanogramProductOrientationType.Top180:
                    case PlanogramProductOrientationType.Bottom180:
                        return ApolloOrientationType.EndFront;

                    case PlanogramProductOrientationType.Top90:
                    case PlanogramProductOrientationType.Bottom90:
                    case PlanogramProductOrientationType.Top270:
                    case PlanogramProductOrientationType.Bottom270:
                        return ApolloOrientationType.EndSide;
                    default:
                        return ApolloOrientationType.FrontEnd;
                }
            }
        }

        /// <summary>
        /// In apollo Coffin positons have their Height/Width switched. The orientations therefore also need converting
        /// </summary>
        /// <param name="orientationType">The orientation type of the source position. Required as this method also works for left/right caps</param>
        /// <returns>The converted value</returns>
        public static ApolloOrientationType GetApolloCoffinPositionOrientation(PlanogramPositionOrientationType orientationType)
        {
            switch (orientationType)
            {
                case PlanogramPositionOrientationType.Front0:
                case PlanogramPositionOrientationType.Back0:
                case PlanogramPositionOrientationType.Front180:
                case PlanogramPositionOrientationType.Back180:
                    return ApolloOrientationType.EndFront;

                case PlanogramPositionOrientationType.Front90:
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Back270:
                    return ApolloOrientationType.SideFront;

                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Right180:
                case PlanogramPositionOrientationType.Left180:
                    return ApolloOrientationType.EndSide;

                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Right270:
                case PlanogramPositionOrientationType.Left270:
                    return ApolloOrientationType.FrontSide;

                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Top180:
                case PlanogramPositionOrientationType.Bottom180:
                    return ApolloOrientationType.FrontEnd;

                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Top270:
                case PlanogramPositionOrientationType.Bottom270:
                    return ApolloOrientationType.SideEnd;
                default:
                    return ApolloOrientationType.EndFront;
            }
        }

        /// <summary>
        /// Converts a CCM V8 merchandising style into an Apollo PO_MERCHANDISINGTYPE merchandising type
        /// </summary>
        /// <param name="style">The V8 Merchandising Style to convert</param>
        /// <returns>The converted ApolloMerchMethodType value</returns>
        public static ApolloMerchMethodType GetApolloMainBlockMerchandisingType(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (positionDto.MerchandisingStyle > 0)
            {
                switch ((PlanogramPositionMerchandisingStyle)positionDto.MerchandisingStyle)
                {
                    case PlanogramPositionMerchandisingStyle.Unit:
                    return ApolloMerchMethodType.Hand;
                    case PlanogramPositionMerchandisingStyle.Tray:
                        return ApolloMerchMethodType.TrayPack;
                    case PlanogramPositionMerchandisingStyle.Case:
                        return ApolloMerchMethodType.Case;
                    default:
                        return ApolloMerchMethodType.Hand;
                }
            }
            else
            {
                switch ((PlanogramProductMerchandisingStyle)productDto.MerchandisingStyle)
                {
                    case PlanogramProductMerchandisingStyle.Unit:
                        return ApolloMerchMethodType.Hand;
                    case PlanogramProductMerchandisingStyle.Tray:
                        return ApolloMerchMethodType.TrayPack;
                    case PlanogramProductMerchandisingStyle.Case:
                        return ApolloMerchMethodType.Case;
                    default:
                        return ApolloMerchMethodType.Hand;
                }
            }  
        }

        /// <summary>
        /// Converts a CCM V8 merchandising style into an Apollo PO_MERCHANDISINGTYPE merchandising type
        /// </summary>
        /// <param name="style">The V8 Merchandising Style to convert</param>
        /// <returns>The converted ApolloMerchMethodType value</returns>
        public static ApolloMerchMethodType GetApolloLeftRightCapMerchandisingType(PlanogramPositionDto positionDto, PlanogramProductDto productDto)
        {
            if (positionDto.MerchandisingStyleX > 0)
            {
                switch ((PlanogramPositionMerchandisingStyle)positionDto.MerchandisingStyleX)
                {
                    case PlanogramPositionMerchandisingStyle.Unit:
                        return ApolloMerchMethodType.Hand;
                    case PlanogramPositionMerchandisingStyle.Tray:
                        return ApolloMerchMethodType.TrayPack;
                    case PlanogramPositionMerchandisingStyle.Case:
                        return ApolloMerchMethodType.Case;
                    default:
                        return ApolloMerchMethodType.Hand;
                }
            }
            else
            {
                switch ((PlanogramProductMerchandisingStyle)productDto.MerchandisingStyle)
                {
                    case PlanogramProductMerchandisingStyle.Unit:
                        return ApolloMerchMethodType.Hand;
                    case PlanogramProductMerchandisingStyle.Tray:
                        return ApolloMerchMethodType.TrayPack;
                    case PlanogramProductMerchandisingStyle.Case:
                        return ApolloMerchMethodType.Case;
                    default:
                        return ApolloMerchMethodType.Hand;
                }
            }
        }

        /// <summary>
        /// Gets the Apollo  Product element value for PO_XGAP from a specified V8 product and its position.
        /// Notes:  Apollo XGap indicates horizontal squeeze if less than 0 or finger space to the side if greater than 0.
        ///         Apollo cannot have both of these V8 properties, so if both are set for a source position, 
        ///         the squeeze value is used.
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <param name="orientationType">The orientation type of the source position. Required as this method also works for left/right caps</param>
        /// <returns>A single value for the PO_XGAP</returns>
        public static Single GetPositionXGap(PlanogramPositionDto positionDto, PlanogramProductDto productDto, ApolloOrientationType orientationType)
        {
            //Squeeze takes precedence over finger space to the side
            Single xGap = productDto.FingerSpaceToTheSide;

            switch (orientationType)
            {
                case ApolloOrientationType.EndFront:
                case ApolloOrientationType.FrontEnd:
                    if (productDto.SqueezeWidth < 1 && productDto.SqueezeWidth > 0)
                    {
                        xGap = 0 - (productDto.Width * (1 - productDto.SqueezeWidth));
                    }
                    break;

                case ApolloOrientationType.EndSide:
                case ApolloOrientationType.SideEnd:
                    if (productDto.SqueezeDepth < 1 && productDto.SqueezeDepth > 0)
                    {
                        xGap = 0 - (productDto.Depth * (1 - productDto.SqueezeDepth));
                    }
                    break;

                case ApolloOrientationType.FrontSide:
                case ApolloOrientationType.SideFront:
                    if (productDto.SqueezeHeight < 1 && productDto.SqueezeHeight > 0)
                    {
                        xGap = 0 - (productDto.Height * (1 - productDto.SqueezeHeight));
                    }
                    break;
            }

            return xGap;
        }

        /// <summary>
        /// Gets the Apollo  Product element value for PO_YGAP from a specified V8 product and its position.
        /// Notes:  V8 uses Finger Space Above for spacing and does not have a direct relation to Apollo's +ve YGap.
        ///         A negative YGap indicates the vertical squeeze a product may have
        /// </summary>
        /// <param name="positionDto">The source position</param>
        /// <param name="productDto">The source product</param>
        /// <param name="orientationType">The orientation type of the source position. Required as this method also works for left/right caps</param>
        /// <returns>A single value for the PO_YGAP</returns>
        public static Single GetPositionYGap(PlanogramPositionDto positionDto, PlanogramProductDto productDto, ApolloOrientationType orientationType)
        {
            Single yGap = 0;

            switch (orientationType)
            {
                case ApolloOrientationType.FrontEnd:
                case ApolloOrientationType.SideEnd:
                    if (productDto.SqueezeHeight < 1 && productDto.SqueezeHeight > 0)
                    {
                        yGap = 0 - (productDto.Height * (1 - productDto.SqueezeHeight));
                    }
                    break;

                case ApolloOrientationType.EndFront:
                case ApolloOrientationType.SideFront:
                    if (productDto.SqueezeDepth < 1 && productDto.SqueezeDepth > 0)
                    {
                        yGap = 0 - (productDto.Depth * (1 - productDto.SqueezeDepth));
                    }
                    break;

                case ApolloOrientationType.FrontSide:
                case ApolloOrientationType.EndSide:
                    if (productDto.SqueezeWidth < 1 && productDto.SqueezeWidth > 0)
                    {
                        yGap = 0 - (productDto.Width * (1 - productDto.SqueezeWidth));
                    }
                    break;
            }

            return yGap;
        }
                
        /// <summary>
        /// Gets the Apollo SpreadMode value for an Apollo Shelf element. 
        /// </summary>
        /// <param name="subComponent">The source subcomponent for the Apollo shelf element</param>
        /// <returns></returns>
        public static ApolloSpreadModeType GetApolloShelfSpreadMode(PlanogramSubComponentDto subComponent, Boolean retainSourcePlanLookAndFeel, Byte shelfType, out Boolean modeIsSupported)
        {
            modeIsSupported = false;

            //Only shelves seem to support merchandising strategies in any kind of predictable fashion.
            if (retainSourcePlanLookAndFeel)
            {
                switch ((ApolloShelfType)shelfType)
                {
                    case ApolloShelfType.Shelf:
                        //Shelves support certain merch strategies (LeftStacked,Even,RightStacked). 
                        // However, if  in 'Even' mode, and we are retaining source plan presentation
                        //left and right cap position will be created, but they will not sit next to their
                        //appropriate main block position, they willb eevenly spaced. In this case, set
                        //strategy to Manual.
                        if (subComponent.MerchandisingStrategyX == (Byte)PlanogramSubComponentXMerchStrategyType.Even)
                        {
                            return ApolloSpreadModeType.Manual;
                        }
                        //CCM-18559 : For plans with multiple bays where can combine is set on sevlext next to each other,
                        //we need to allow any products that sit partly on two shelves to etain their position.
                        //The only way to do this is to override the ccm shelf merch strategy and set it to manual
                        if (subComponent.CombineType != (Byte)PlanogramSubComponentCombineType.None)
                        {
                            return ApolloSpreadModeType.Manual;
                        }
                        break;

                    default:
                        return ApolloSpreadModeType.Manual; //No crunch
                }
            }

            switch (subComponent.MerchandisingStrategyX)
            {
                case (Byte)PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    modeIsSupported = true;
                    return ApolloSpreadModeType.Left; //Left

                case (Byte)PlanogramSubComponentXMerchStrategyType.Even:
                    modeIsSupported = true;
                    return ApolloSpreadModeType.Even; //Even

                case (Byte)PlanogramSubComponentXMerchStrategyType.RightStacked:
                    modeIsSupported = true;
                    return ApolloSpreadModeType.Right; //Right

                case (Byte)PlanogramSubComponentXMerchStrategyType.Manual:
                    modeIsSupported = true;
                    return ApolloSpreadModeType.Manual;

                default:
                    return ApolloSpreadModeType.Manual; //No Crunch
            }
        }

        /// <summary>
        /// Determines whether the specified position has either left or right caps
        /// </summary>
        /// <param name="dto">The source Planogram Position Dto</param>
        /// <returns>True if the position has left or right caps, otherwise false</returns>
        public static Boolean PositionHasSideCappings(PlanogramPositionDto dto)
        {
            if ((dto.FacingsXHigh > 0) && (dto.FacingsXWide > 0) && (dto.FacingsXDeep > 0)) return true;
            return false;
        }

        /// <summary>
        /// Gets the Unit of Measure field for an Apollo Info element based on the source
        /// CCM V8 planogram setting.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>A string value representing the UOM in Apollo-speak</returns>
        public static String GetApolloInfoMeasure(PlanogramDto dto)
        {
            PlanogramLengthUnitOfMeasureType uom = (PlanogramLengthUnitOfMeasureType)dto.LengthUnitsOfMeasure;

            switch (uom)
            {
                case PlanogramLengthUnitOfMeasureType.Centimeters: return "Metric";
                case PlanogramLengthUnitOfMeasureType.Inches: return "Imperial";
                default: return "Metric";
            }
        }

        /// <summary>
        /// Returns the Notch start and spacingthat can be applied to an Apollo section. Since a section maps to multiple V8 fixtures
        /// we can only apply a notch value if all notches match across the planogram, totherwise we output 0 for both start and spacing
        /// </summary>
        /// <param name="components"></param>
        /// <param name="subComponents"></param>
        /// <param name="notchStartY"></param>
        /// <param name="notchSpacingY"></param>
        public static void GetApolloSectionNotchValues(List<PlanogramComponentDto> components, List<PlanogramSubComponentDto> subComponents, out Single notchStartY, out Single notchSpacingY)
        {
            notchStartY = -1;
            notchSpacingY = -1;
            Boolean notchStartMatchesAcrossBays = true;
            Boolean notchSpacingMatchesAcrossBays = true;

            List<PlanogramComponentDto> backboards = components.Where(c => c.ComponentType == (Byte)PlanogramComponentType.Backboard).ToList();

            foreach (PlanogramComponentDto component in backboards)
            {
                PlanogramSubComponentDto subComponent = subComponents.FirstOrDefault(s => s.PlanogramComponentId.Equals(component.Id));
                if (subComponent == null) continue;
                        
                //Check Notch StartY
                if (notchStartY < 0)
                {
                    notchStartY = subComponent.NotchStartY;
                }
                else
                {
                    //at least 1 notch has already been found, so check values are the same
                    if (notchStartY != subComponent.NotchStartY)
                    {
                        notchStartMatchesAcrossBays = false;
                    }
                }

                //Check Notch Spacing
                if (notchSpacingY < 0)
                {
                    notchSpacingY = subComponent.NotchSpacingY;
                }
                else
                {
                    //at least 1 notch has already been found, so check values are the same
                    if (notchSpacingY != subComponent.NotchSpacingY)
                    {
                        notchSpacingMatchesAcrossBays = false;
                    }
                }
            }
            
            //If they don't match pass out zero values
            if (!notchStartMatchesAcrossBays) notchStartY = 0;
            if (!notchSpacingMatchesAcrossBays) notchSpacingY = 0;
        }

        /// <summary>
        /// Determines the Apollo element type from a CCM sub-component
        /// </summary>
        /// <param name="subComponent">The CCM Sub-component to convert</param>
        /// <returns>The Apollo element type</returns>
        public static ApolloShelfType GetApolloShelfType(PlanogramSubComponentDto subComponent)
        {
            if (subComponent == null) return ApolloShelfType.Shelf;
            
            //  CCM         Merch Type       Apollo
            //  ---------   ---------       -----------
            //  Shelf       Stack           Shelf
            //  Shelf       Stack           FreehandSurface
            //  Pallet      Stack           Shelf
            //  Bar         Hang            Crossbar
            //  Rod         HangFromBottom  Hanging Bar
            //  Pegboard    Hang            Peg
            //  Chest       Stack           Coffin
            //  Slotwall    Hang            Stlowall
            //  Panel       None            Box

            //We do not use the copmponent type as this is only inferred from the underlying sub-component 
            //details so use these for more accuracy
            
            switch ((PlanogramSubComponentMerchandisingType)subComponent.MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.None:
                    return ApolloShelfType.Box;

                case PlanogramSubComponentMerchandisingType.Stack:
                    if (subComponent.Height <= 0.01) return ApolloShelfType.FreeHandSurface;
                    if (subComponent.FaceThicknessBottom == 0) return ApolloShelfType.Shelf;
                    return ApolloShelfType.Coffin; //Could be coffin or basket but we do not know which

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return ApolloShelfType.HangingBar; //it is a CCM Rod, which converts to an Apollo Hanging Bar

                case PlanogramSubComponentMerchandisingType.Hang:
                    //If there are no pegs defined, it must be a bar not a pegboard, so
                    //this will be exported as an Apollo Crossbar;
                    if (subComponent.MerchConstraintRow1StartX == 0 &&
                        subComponent.MerchConstraintRow1SpacingX == 0 &&
                        subComponent.MerchConstraintRow1StartY == 0 &&
                        subComponent.MerchConstraintRow1SpacingY == 0)
                    {
                        return ApolloShelfType.Crossbar;
                    }

                    // if it has pegs and they form only 1 line and first line == halfway up  
                    // subcomponent, then its a crossbar otherwise its a pegboard or slotwall

                    // See where the first line of pegs lies
                    Single firstLineY = 0;
                    if (subComponent.MerchConstraintRow1StartY == 0)
                    {
                        firstLineY = subComponent.MerchConstraintRow1SpacingY;
                    }
                    else
                    {
                        firstLineY = subComponent.MerchConstraintRow1StartY;
                    }

                    // See how many lines of pegs exist on the CCM element
                    if ((subComponent.Height - firstLineY) > subComponent.MerchConstraintRow1SpacingY)
                    {
                        //There is only 1 line
                        if (firstLineY.EqualTo(subComponent.Height / 2))
                        {
                            //That line is in the middle of the subcomponent
                            //so export as an Apollo Crossbar
                            return ApolloShelfType.Crossbar;
                        }
                    }

                    //we have peg info but the spacing betwwen pegs on x axis = 0
                    //so export as a slotwall
                    if (subComponent.MerchConstraintRow1SpacingX == 0) return ApolloShelfType.Sltowall;

                    //we have pegs, more than 1 line and a spacing between the pegs, 
                    //so export as an Apollo pegboard
                    return ApolloShelfType.Pegboard;
            }

            return ApolloShelfType.Shelf;
        }

        /// <summary>
        /// Apollo only has limited support for layover orientation and for every main facing orientation there is only 1 valid 
        /// layover orientation. V8 supports all orientations for top caps.
        /// </summary>
        /// <param name="positionDto">The position dto to check</param>
        /// <param name="mainFacingOrientation">The orientation of the main facings in Apollo-speak</param>
        /// <returns>True if the top caps has a valid orientation, otherwise false</returns>
        public static Boolean TopCapOrientationIsValid(PlanogramPositionDto positionDto, PlanogramProductDto productDto, ApolloOrientationType mainFacingOrientation)
        {
            ApolloOrientationType topCapOrientation = GetApolloPositionOrientation((PlanogramPositionOrientationType)positionDto.OrientationTypeY, (PlanogramProductOrientationType)productDto.OrientationType);

            switch (mainFacingOrientation)
            {
                case ApolloOrientationType.FrontEnd:
                    return (topCapOrientation == ApolloOrientationType.EndFront);

                case ApolloOrientationType.FrontSide:
                    return (topCapOrientation == ApolloOrientationType.SideFront);

                case ApolloOrientationType.SideEnd:
                    return (topCapOrientation == ApolloOrientationType.EndSide);

                case ApolloOrientationType.SideFront:
                    return (topCapOrientation == ApolloOrientationType.FrontSide);

                case ApolloOrientationType.EndFront:
                    return (topCapOrientation == ApolloOrientationType.FrontEnd);

                case ApolloOrientationType.EndSide:
                    return (topCapOrientation == ApolloOrientationType.SideEnd);

                default:
                    return false;
            }
        }

        /// <summary>
        /// Determnines whether a product is merchandised in trays or cases or both in its child position implementations
        /// </summary>
        /// <param name="productDto">The source product</param>
        /// <param name="positionDtos">The source positions</param>
        /// <param name="isMerchedInTrays"></param>
        /// <param name="isMerchedInCases"></param>
        public static void IsProductMerchandisedInTraysOrCases(PlanogramProductDto productDto, List<PlanogramPositionDto> positionDtos, out Boolean isMerchedInTrays, out Boolean isMerchedInCases)
        {
            isMerchedInTrays = false;
            isMerchedInCases = false;

            foreach (PlanogramPositionDto positionDto in positionDtos)
            {
                if (positionDto.MerchandisingStyle != (Byte)PlanogramPositionMerchandisingStyle.Default)
                {
                    //position-specific merch style implemented
                    if (!isMerchedInTrays) isMerchedInTrays = positionDto.MerchandisingStyle == (Byte)PlanogramPositionMerchandisingStyle.Tray;
                    if (!isMerchedInCases) isMerchedInCases = positionDto.MerchandisingStyle == (Byte)PlanogramPositionMerchandisingStyle.Case;
                }
                else
                {
                    //product-specific merch style
                    if (!isMerchedInTrays) isMerchedInTrays = productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Tray;
                    if (!isMerchedInCases) isMerchedInCases = productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Case;
                }
                if (isMerchedInTrays && isMerchedInCases) break;
            }
        }

        /// <summary>
        /// Gets the Apollo  Product element value for PR_CASEHEIGHT from a specified V8 product and its position.
        /// </summary>
        /// <param name="productDto">The source product</param>
        /// <param name="positionDto">The source positions</param>
        /// <returns>A single value for the PR_CASEHEIGHT</returns>
        public static Single GetApolloProductCaseHeight(PlanogramProductDto productDto, List<PlanogramPositionDto> positionDtos)
        {
            Single caseHeight = 0;
            if (positionDtos.Count == 0) return caseHeight;

            Boolean isMultiSited = positionDtos.Count > 1;

            //Notes: Apollo only has a case value not a tray and case value so we need to decide which value to take from V8
            // If the position is single-sited and merched as a unit, take the case values (apollo calls them case values so use these by default)
            // If the position is single-sited and merched as a case, take the case values
            // If the position is single-sited and merched as a tray, take the tray values
            // if the position is multi-sited and not merched in cases AND trays as above
            // if the position is multi-sited and IS merched in cases AND trays use case values

            Boolean isMerchedInTrays = false;
            Boolean isMerchedInCases= false;
            IsProductMerchandisedInTraysOrCases(productDto, positionDtos, out isMerchedInTrays, out isMerchedInCases);

            if (!isMultiSited)
            {
                if (isMerchedInTrays)
                {
                    caseHeight = productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
                else
                {
                    caseHeight = productDto.CaseHeight;
                }
            }
            else
            {
                //it is multi-sited. If only merched as trays use tray values
                if (isMerchedInTrays && !isMerchedInCases)
                {
                    caseHeight = productDto.TrayHeight > 0 ? productDto.TrayHeight : (productDto.TrayHigh * productDto.Height) + productDto.TrayThickHeight;
                }
                else
                {
                    //otherwise use the case values
                    caseHeight = productDto.CaseHeight;
                }
            }
                             
            return caseHeight;
        }

        /// <summary>
        /// Gets the Apollo  Product element value for PR_CASEWIDTH from a specified V8 product and its position.
        /// </summary>
        /// <param name="productDto">The source product</param>
        /// <param name="positionDtos">The source positions</param>
        /// <returns>A single value for the PR_CASEWIDTH</returns>
        public static Single GetApolloProductCaseWidth(PlanogramProductDto productDto, List<PlanogramPositionDto> positionDtos)
        {
            Single caseWidth = 0;
            if (positionDtos.Count == 0) return caseWidth;

            Boolean isMultiSited = positionDtos.Count > 1;

            //Notes: Apollo only has a case value not a tray and case value so we need to decide which value to take from V8
            // If the position is single-sited and merched as a unit, take the case values (apollo calls them case values so use these by default)
            // If the position is single-sited and merched as a case, take the case values
            // If the position is single-sited and merched as a tray, take the tray values
            // if the position is multi-sited and not merched in cases AND trays as above
            // if the position is multi-sited and IS merched in cases AND trays use case values

            Boolean isMerchedInTrays = false;
            Boolean isMerchedInCases = false;
            IsProductMerchandisedInTraysOrCases(productDto, positionDtos, out isMerchedInTrays, out isMerchedInCases);

            if (!isMultiSited)
            {
                if (isMerchedInTrays)
                {
                    caseWidth = productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);
                }
                else
                {
                    caseWidth = productDto.CaseWidth;
                }
            }
            else
            {
                //it is multi-sited. If only merched as trays use tray values
                if (isMerchedInTrays && !isMerchedInCases)
                {
                    caseWidth = productDto.TrayWidth > 0 ? productDto.TrayWidth : (productDto.TrayWide * productDto.Width) + (productDto.TrayThickWidth * 2);
                }
                else
                {
                    //otherwise use the case values
                    caseWidth = productDto.CaseWidth;
                }
            }

            return caseWidth;
        }

        /// <summary>
        /// Gets the Apollo  Product element value for PR_CASEDEPTH from a specified V8 product and its position.
        /// </summary>
        /// <param name="productDto">The source product</param>
        /// <param name="positionDto">The source position</param>
        /// <returns>A single value for the PR_CASEDEPTH</returns>
        public static Single GetApolloProductCaseDepth(PlanogramProductDto productDto, List<PlanogramPositionDto> positionDtos)
        {
            Single caseDepth = 0;

            if (positionDtos.Count == 0) return caseDepth;

            Boolean isMultiSited = positionDtos.Count > 1;

            //Notes: Apollo only has a case value not a tray and case value so we need to decide which value to take from V8
            // If the position is single-sited and merched as a unit, take the case values (apollo calls them case values so use these by default)
            // If the position is single-sited and merched as a case, take the case values
            // If the position is single-sited and merched as a tray, take the tray values
            // if the position is multi-sited and not merched in cases AND trays as above
            // if the position is multi-sited and IS merched in cases AND trays use case values

            Boolean isMerchedInTrays = false;
            Boolean isMerchedInCases = false;
            IsProductMerchandisedInTraysOrCases(productDto, positionDtos, out isMerchedInTrays, out isMerchedInCases);

            if (!isMultiSited)
            {
                if (isMerchedInTrays)
                {
                    caseDepth = productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
                }
                else
                {
                    caseDepth = productDto.CaseDepth;
                }
            }
            else
            {
                //it is multi-sited. If only merched as trays use tray values
                if (isMerchedInTrays && !isMerchedInCases)
                {
                    caseDepth = productDto.TrayDepth > 0 ? productDto.TrayDepth : (productDto.TrayDeep * productDto.Depth) + (productDto.TrayThickDepth * 2);
                }
                else
                {
                    //otherwise use the case values
                    caseDepth = productDto.CaseDepth;
                }
            }

            return caseDepth;
        }

        #endregion
    }
}
