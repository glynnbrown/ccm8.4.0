﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820

// V8-30757 : A.Silva
//  Created
// V8-30754 : A.Silva
//  Added Performance Field Infos.
//  Added GetIndex to extract the index from the string field name.
// V8-30724 : A.Silva
//  Initialized ObjectFieldInfos for Planogram, Component, Product.
// V8-31028 : A.Silva
//  Added some extra field mappings.
// V8-31032 : A.Silva
//  Added Converter ToSubComponentCombineType.
// V8-31030 : A.Silva
//  Added Drawing fields.
// V8-31073 : A.Silva
//  Added Performance fields.
// V8-31111 : A.Silva
//  Declared DefaultPogft here for use through the app.
// V8-31149 : A.Silva
//  Added conversion of prospace to ccm fill patterns.
// V8-31089 : A.Silva
//  Amended two fields belonging to Project that were shown as belonging to Planogram (CreatedBy and DateFinished).
// V8-31170 : A.Silva
//  Added ProSpaceMerchSizeType enum.
// V8-31189 : A.Silva
//  Added SupportedVersions Enumeration.
// V8-31153 : A.Silva
//  Amended ProSpaceMerchSizeType which was assigning the wrong numeric values.
// V8-31260 : A.Silva
//  Added importing Merchandising Style for X, Y, Z Blocks.
// V8-31175 : A.Silva
//  Removed Default Custom Mapping for Planogram Name.
// V8-31303 : A.Silva
//  Added Performance metric mapping sources and added missing default custom mappings.
// V8-31311 : A.Silva
//  Added PerfomanceSales and PerformanceProfit, although these will be calculated fields.
// V8-31322 : A.Silva
//  Corrected mappings for UnitPrice and UnitCost, added calculation of CaseCost as a calculated field.
// V8-31303 : A.Silva
//  Amended some of the default mappings.
// V8-31422 : A.Silva
//  Added PerformanceUnitCost.

#endregion

#region Version History: CCM830
// V8-32517 : M.Brumby
//  Apply squeeze/expand to unit merchandising style only
#endregion

#region Version History : CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Enums;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.External
{
    /// <summary>
    ///     <c>ProSpaceImportHelper</c> contains helper methods and properties useful when importing a <c>ProSpace</c> file.
    /// </summary>
    public static class ProSpaceImportHelper
    {
        #region Fields

        /// <summary>
        ///     List of file versions that can be imported at the moment.
        /// </summary>
        private static List<String> _availableVersions;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Project</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _projectFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Planogram</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _planogramFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Component</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _componentFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Fixture</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _fixtureFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Product</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _productFieldInfoByFieldInfos;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Performance</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _performanceFieldInfoByFieldIndex;

        private static Dictionary<ProSpaceComponentType, Int32> _defaultFillColourByProSpaceComponentType;
        private static Dictionary<String, ObjectFieldInfo> _allFieldInfos;

        #endregion

        #region Properties

        /// <summary>
        ///     Default name for the ProSpace template file. [ProSpace Default.pogft]
        /// </summary>
        public static String DefaultPogft { get { return "JDA Space Planning V8 Default.pogft"; } }

        /// <summary>
        ///     Gets the list of file versions that can be imported at the moment.
        /// </summary>
        /// <remarks>
        ///     The list of versions is lazy loaded when requested for the first time.
        /// </remarks>
        public static List<String> AvailableVersions
        {
            get { return _availableVersions ?? (_availableVersions = new List<String> {"2", "3", "4", "2005.0", "2006.0", "2007.0", "2008.0"}); }
        }

        public static IEnumerable<String> SupportedVersions
        {
            get
            {
                yield return "2008.0";
                yield return "2008.1";
            }
        }

        public static Dictionary<String, ObjectFieldInfo> PlanogramFieldInfos
        {
            get { return _planogramFieldInfoByFieldIndex ?? (_planogramFieldInfoByFieldIndex = InitializePlanogramFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> ComponentFieldInfos
        {
            get { return _componentFieldInfoByFieldIndex ?? (_componentFieldInfoByFieldIndex = InitializeComponentFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> FixtureFieldInfos
        {
            get { return _fixtureFieldInfoByFieldIndex ?? (_fixtureFieldInfoByFieldIndex = InitializeFixtureFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> ProductFieldInfos
        {
            get { return _productFieldInfoByFieldInfos ?? (_productFieldInfoByFieldInfos = InitializeProductFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> PerformanceFieldInfos
        {
            get { return _performanceFieldInfoByFieldIndex ?? (_performanceFieldInfoByFieldIndex = InitializePerformanceFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> AllFieldInfos
        {
            get { return _allFieldInfos ?? (_allFieldInfos = InitializeAllFieldInfos()); }
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeAllFieldInfos()
        {
            return PlanogramFieldInfos.Union(ComponentFieldInfos)
                                      .Union(FixtureFieldInfos)
                                      .Union(ProductFieldInfos)
                                      .Union(PerformanceFieldInfos)
                                      .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        #region Project Field Indexes

        public static String ProjectName { get { return "Project001"; } }
        public static String ProjectKey { get { return "Project002"; } }
        public static String ProjectPrimaryKey { get { return "Project003"; } } // 0=UPC, 1=ID, 2=BOTH
        public static String ProjectLayoutFile { get { return "Project004"; } }
        public static String ProjectMovementPeriod { get { return "Project005"; } }
        public static String ProjectCaseMultiple { get { return "Project006"; } }
        public static String ProjectDaysOfSupply { get { return "Project007"; } }
        public static String ProjectDemandCycleLength { get { return "Project008"; } }
        public static String ProjectPeakSafetyFactor { get { return "Project009"; } }
        public static String ProjectBackroomStock { get { return "Project010"; } }
        public static String ProjectPegId { get { return "Project011"; } }
        public static String ProjectMeasurement { get { return "Project012"; } } //  0=IMPERIAL, 1=METRIC
        public static String ProjectMerchXMin { get { return "Project014"; } } //  0=NO LIMIT
        public static String ProjectMerchXMax { get { return "Project015"; } } //  0=NO LIMIT
        public static String ProjectMerchXUprights { get { return "Project016"; } }
        public static String ProjectMerchXCaps { get { return "Project017"; } }
        public static String ProjectMerchXPlacement { get { return "Project018"; } } //  0=MANUAL, 1=EDGE, 2=STACK, 3=SPREAD
        public static String ProjectMerchXNumber { get { return "Project019"; } } //  0=MANUAL, 1=ONE, 2=FILL
        public static String ProjectMerchXSize { get { return "Project020"; } } // 0=NORMAL, 1=ADJUST, 2=SPACED
        public static String ProjectMerchXDirection { get { return "Project021"; } } //  0=NORMAL, 1=REVERSED
        public static String ProjectField022 { get { return "Project022"; } }
        public static String ProjectMerchYMin { get { return "Project023"; } } //  0=NO LIMIT
        public static String ProjectMerchYMax { get { return "Project024"; } } //  0=NO LIMIT
        public static String ProjectMerchYUprights { get { return "Project025"; } }
        public static String ProjectMerchYCaps { get { return "Project026"; } }
        public static String ProjectMerchYPlacement { get { return "Project027"; } } //  0=MANUAL, 1=EDGE, 2=STACK, 3=SPREAD
        public static String ProjectMerchYNumber { get { return "Project028"; } } //  0=MANUAL, 1=ONE, 2=FILL
        public static String ProjectMerchYSize { get { return "Project029"; } } // 0=NORMAL, 1=ADJUST, 2=SPACED
        public static String ProjectMerchYDirection { get { return "Project030"; } } //  0=NORMAL, 1=REVERSED
        public static String ProjectField031 { get { return "Project031"; } }
        public static String ProjectMerchZMin { get { return "Project032"; } } //  0=NO LIMIT
        public static String ProjectMerchZMax { get { return "Project033"; } } //  0=NO LIMIT
        public static String ProjectMerchZUprights { get { return "Project034"; } }
        public static String ProjectMerchZCaps { get { return "Project035"; } }
        public static String ProjectMerchZPlacement { get { return "Project036"; } } //  0=MANUAL, 1=EDGE, 2=STACK, 3=SPREAD
        public static String ProjectMerchZNumber { get { return "Project037"; } } //  0=MANUAL, 1=ONE, 2=FILL
        public static String ProjectMerchZSize { get { return "Project038"; } } // 0=NORMAL, 1=ADJUST, 2=SPACED
        public static String ProjectMerchZDirection { get { return "Project039"; } } //  0=NORMAL, 1=REVERSED
        public static String ProjectField040 { get { return "Project040"; } }
        public static String ProjectDemand01 { get { return "Project041"; } }
        public static String ProjectDemand02 { get { return "Project042"; } }
        public static String ProjectDemand03 { get { return "Project043"; } }
        public static String ProjectDemand04 { get { return "Project044"; } }
        public static String ProjectDemand05 { get { return "Project045"; } }
        public static String ProjectDemand06 { get { return "Project046"; } }
        public static String ProjectDemand07 { get { return "Project047"; } }
        public static String ProjectDemand08 { get { return "Project048"; } }
        public static String ProjectDemand09 { get { return "Project049"; } }
        public static String ProjectDemand10 { get { return "Project050"; } }
        public static String ProjectDemand11 { get { return "Project051"; } }
        public static String ProjectDemand12 { get { return "Project052"; } }
        public static String ProjectDemand13 { get { return "Project053"; } }
        public static String ProjectDemand14 { get { return "Project054"; } }
        public static String ProjectDemand15 { get { return "Project055"; } }
        public static String ProjectDemand16 { get { return "Project056"; } }
        public static String ProjectDemand17 { get { return "Project057"; } }
        public static String ProjectDemand18 { get { return "Project058"; } }
        public static String ProjectDemand19 { get { return "Project059"; } }
        public static String ProjectDemand20 { get { return "Project060"; } }
        public static String ProjectDemand21 { get { return "Project061"; } }
        public static String ProjectDemand22 { get { return "Project062"; } }
        public static String ProjectDemand23 { get { return "Project063"; } }
        public static String ProjectDemand24 { get { return "Project064"; } }
        public static String ProjectDemand25 { get { return "Project065"; } }
        public static String ProjectDemand26 { get { return "Project066"; } }
        public static String ProjectDemand27 { get { return "Project067"; } }
        public static String ProjectDemand28 { get { return "Project068"; } }
        public static String ProjectInventoryModelManual { get { return "Project069"; } }
        public static String ProjectInventoryModelCaseMultiple { get { return "Project070"; } }
        public static String ProjectInventoryModelDaysOfSupply { get { return "Project071"; } }
        public static String ProjectInventoryModelPeak { get { return "Project072"; } }
        public static String ProjectInventoryModelMinUnits { get { return "Project073"; } }
        public static String ProjectInventoryModelMaxUnits { get { return "Project074"; } }
        public static String ProjectValue01 { get { return "Project075"; } }
        public static String ProjectValue02 { get { return "Project076"; } }
        public static String ProjectValue03 { get { return "Project077"; } }
        public static String ProjectValue04 { get { return "Project078"; } }
        public static String ProjectValue05 { get { return "Project079"; } }
        public static String ProjectValue06 { get { return "Project080"; } }
        public static String ProjectValue07 { get { return "Project081"; } }
        public static String ProjectValue08 { get { return "Project082"; } }
        public static String ProjectValue09 { get { return "Project083"; } }
        public static String ProjectValue10 { get { return "Project084"; } }
        public static String ProjectValue11 { get { return "Project085"; } }
        public static String ProjectValue12 { get { return "Project086"; } }
        public static String ProjectValue13 { get { return "Project087"; } }
        public static String ProjectValue14 { get { return "Project088"; } }
        public static String ProjectValue15 { get { return "Project089"; } }
        public static String ProjectValue16 { get { return "Project090"; } }
        public static String ProjectValue17 { get { return "Project091"; } }
        public static String ProjectValue18 { get { return "Project092"; } }
        public static String ProjectValue19 { get { return "Project093"; } }
        public static String ProjectValue20 { get { return "Project094"; } }
        public static String ProjectValue21 { get { return "Project095"; } }
        public static String ProjectValue22 { get { return "Project096"; } }
        public static String ProjectValue23 { get { return "Project097"; } }
        public static String ProjectValue24 { get { return "Project098"; } }
        public static String ProjectValue25 { get { return "Project099"; } }
        public static String ProjectValue26 { get { return "Project100"; } }
        public static String ProjectValue27 { get { return "Project101"; } }
        public static String ProjectValue28 { get { return "Project102"; } }
        public static String ProjectValue29 { get { return "Project103"; } }
        public static String ProjectValue30 { get { return "Project104"; } }
        public static String ProjectValue31 { get { return "Project105"; } }
        public static String ProjectValue32 { get { return "Project106"; } }
        public static String ProjectValue33 { get { return "Project107"; } }
        public static String ProjectValue34 { get { return "Project108"; } }
        public static String ProjectValue35 { get { return "Project109"; } }
        public static String ProjectValue36 { get { return "Project110"; } }
        public static String ProjectValue37 { get { return "Project111"; } }
        public static String ProjectValue38 { get { return "Project112"; } }
        public static String ProjectValue39 { get { return "Project113"; } }
        public static String ProjectValue40 { get { return "Project114"; } }
        public static String ProjectValue41 { get { return "Project115"; } }
        public static String ProjectValue42 { get { return "Project116"; } }
        public static String ProjectValue43 { get { return "Project117"; } }
        public static String ProjectValue44 { get { return "Project118"; } }
        public static String ProjectValue45 { get { return "Project119"; } }
        public static String ProjectValue46 { get { return "Project120"; } }
        public static String ProjectValue47 { get { return "Project121"; } }
        public static String ProjectValue48 { get { return "Project122"; } }
        public static String ProjectValue49 { get { return "Project123"; } }
        public static String ProjectValue50 { get { return "Project124"; } }
        public static String ProjectDescription01 { get { return "Project125"; } }
        public static String ProjectDescription02 { get { return "Project126"; } }
        public static String ProjectDescription03 { get { return "Project127"; } }
        public static String ProjectDescription04 { get { return "Project128"; } }
        public static String ProjectDescription05 { get { return "Project129"; } }
        public static String ProjectDescription06 { get { return "Project130"; } }
        public static String ProjectDescription07 { get { return "Project131"; } }
        public static String ProjectDescription08 { get { return "Project132"; } }
        public static String ProjectDescription09 { get { return "Project133"; } }
        public static String ProjectDescription10 { get { return "Project134"; } }
        public static String ProjectDescription11 { get { return "Project135"; } }
        public static String ProjectDescription12 { get { return "Project136"; } }
        public static String ProjectDescription13 { get { return "Project137"; } }
        public static String ProjectDescription14 { get { return "Project138"; } }
        public static String ProjectDescription15 { get { return "Project139"; } }
        public static String ProjectDescription16 { get { return "Project140"; } }
        public static String ProjectDescription17 { get { return "Project141"; } }
        public static String ProjectDescription18 { get { return "Project142"; } }
        public static String ProjectDescription19 { get { return "Project143"; } }
        public static String ProjectDescription20 { get { return "Project144"; } }
        public static String ProjectDescription21 { get { return "Project145"; } }
        public static String ProjectDescription22 { get { return "Project146"; } }
        public static String ProjectDescription23 { get { return "Project147"; } }
        public static String ProjectDescription24 { get { return "Project148"; } }
        public static String ProjectDescription25 { get { return "Project149"; } }
        public static String ProjectDescription26 { get { return "Project150"; } }
        public static String ProjectDescription27 { get { return "Project151"; } }
        public static String ProjectDescription28 { get { return "Project152"; } }
        public static String ProjectDescription29 { get { return "Project153"; } }
        public static String ProjectDescription30 { get { return "Project154"; } }
        public static String ProjectDescription31 { get { return "Project155"; } }
        public static String ProjectDescription32 { get { return "Project156"; } }
        public static String ProjectDescription33 { get { return "Project157"; } }
        public static String ProjectDescription34 { get { return "Project158"; } }
        public static String ProjectDescription35 { get { return "Project159"; } }
        public static String ProjectDescription36 { get { return "Project160"; } }
        public static String ProjectDescription37 { get { return "Project161"; } }
        public static String ProjectDescription38 { get { return "Project162"; } }
        public static String ProjectDescription39 { get { return "Project163"; } }
        public static String ProjectDescription40 { get { return "Project164"; } }
        public static String ProjectDescription41 { get { return "Project165"; } }
        public static String ProjectDescription42 { get { return "Project166"; } }
        public static String ProjectDescription43 { get { return "Project167"; } }
        public static String ProjectDescription44 { get { return "Project168"; } }
        public static String ProjectDescription45 { get { return "Project169"; } }
        public static String ProjectDescription46 { get { return "Project170"; } }
        public static String ProjectDescription47 { get { return "Project171"; } }
        public static String ProjectDescription48 { get { return "Project172"; } }
        public static String ProjectDescription49 { get { return "Project173"; } }
        public static String ProjectDescription50 { get { return "Project174"; } }
        public static String ProjectFlag01 { get { return "Project175"; } }
        public static String ProjectFlag02 { get { return "Project176"; } }
        public static String ProjectFlag03 { get { return "Project177"; } }
        public static String ProjectFlag04 { get { return "Project178"; } }
        public static String ProjectFlag05 { get { return "Project179"; } }
        public static String ProjectFlag06 { get { return "Project180"; } }
        public static String ProjectFlag07 { get { return "Project181"; } }
        public static String ProjectFlag08 { get { return "Project182"; } }
        public static String ProjectFlag09 { get { return "Project183"; } }
        public static String ProjectFlag10 { get { return "Project184"; } }
        public static String ProjectNotes { get { return "Project185"; } }
        public static String ProjectField186 { get { return "Project186"; } }
        public static String ProjectField187 { get { return "Project187"; } }
        public static String ProjectField188 { get { return "Project188"; } }
        public static String ProjectField189 { get { return "Project189"; } }
        public static String ProjectField190 { get { return "Project190"; } }
        public static String ProjectField191 { get { return "Project191"; } }
        public static String ProjectField192 { get { return "Project192"; } }
        public static String ProjectField193 { get { return "Project193"; } }
        public static String ProjectField194 { get { return "Project194"; } }
        public static String ProjectField195 { get { return "Project195"; } }
        public static String ProjectField196 { get { return "Project196"; } }
        public static String ProjectUsePerformancePrice { get { return "Project197"; } } // 0=No, 1=Yes, 2=Only when not zero
        public static String ProjectUsePerformanceCost { get { return "Project198"; } } // 0=No, 1=Yes, 2=Only when not zero
        public static String ProjectUsePerformanceTaxCode { get { return "Project199"; } } // 0=No, 1=Yes, 2=Only when not zero
        public static String ProjectUsePerformanceMovement { get { return "Project200"; } } // 0=No, 1=Yes, 2=Only when not zero
        public static String ProjectStatus { get { return "Project201"; } } // BLANK, Live, Pending, Work in progress, Historic, Analysis
        public static String ProjectDateCreated { get { return "Project202"; } }
        public static String ProjectDateModified { get { return "Project203"; } }
        public static String ProjectDatePending { get { return "Project204"; } }
        public static String ProjectDateEffective { get { return "Project205"; } }
        public static String ProjectDateFinished { get { return "Project206"; } }
        public static String ProjectDate1 { get { return "Project207"; } }
        public static String ProjectDate2 { get { return "Project208"; } }
        public static String ProjectDate3 { get { return "Project209"; } }
        public static String ProjectCreatedBy { get { return "Project210"; } }
        public static String ProjectModifiedBy { get { return "Project211"; } }
        public static String ProjectPlanogramSpecificInventory { get { return "Project212"; } }
        public static String ProjectDeliverySchedule { get { return "Project213"; } }
        public static String ProjectNote1 { get { return "Project214"; } }
        public static String ProjectNote2 { get { return "Project215"; } }
        public static String ProjectNote3 { get { return "Project216"; } }
        public static String ProjectNote4 { get { return "Project217"; } }
        public static String ProjectNote5 { get { return "Project218"; } }
        #endregion

        #region Product Field Indexes

        public static String ProductUnitCost { get { return "ProductUnitCost"; } }
        public static String ProductUpc { get { return "Product001"; } }
        public static String ProductId { get { return "Product002"; } }
        public static String ProductName { get { return "Product003"; } }
        public static String ProductKey { get { return "Product004"; } }
        public static String ProductWidth { get { return "Product005"; } }
        public static String ProductHeight { get { return "Product006"; } }
        public static String ProductDepth { get { return "Product007"; } }
        public static String ProductColor { get { return "Product008"; } }
        public static String ProductAbbrevName { get { return "Product009"; } }
        public static String ProductSize { get { return "Product010"; } }
        public static String ProductUom { get { return "Product011"; } }
        public static String ProductManufacturer { get { return "Product012"; } }
        public static String ProductCategory { get { return "Product013"; } }
        public static String ProductSupplier { get { return "Product014"; } }
        public static String ProductInnerPack { get { return "Product015"; } }
        public static String ProductXNesting { get { return "Product016"; } }
        public static String ProductYNesting { get { return "Product017"; } }
        public static String ProductZNesting { get { return "Product018"; } }
        public static String ProductPegholes { get { return "Product019"; } } // Number of pegholes, 0-3
        public static String ProductPegholeX { get { return "Product020"; } }
        public static String ProductPegholeY { get { return "Product021"; } }
        public static String ProductPegholeWidth { get { return "Product022"; } }
        public static String ProductPeghole2X { get { return "Product023"; } }
        public static String ProductPeghole2Y { get { return "Product024"; } }
        public static String ProductPeghole2Width { get { return "Product025"; } }
        public static String ProductPeghole3X { get { return "Product026"; } }
        public static String ProductPeghole3Y { get { return "Product027"; } }
        public static String ProductPeghole3Width { get { return "Product028"; } }
        public static String ProductPackageStyle { get { return "Product029"; } }
        public static String ProductPegId { get { return "Product030"; } }
        public static String ProductFingerSpaceY { get { return "Product031"; } }
        public static String ProductJumbleFactor { get { return "Product032"; } }
        public static String ProductPrice { get { return "Product033"; } }
        public static String ProductCaseCost { get { return "Product034"; } }
        public static String ProductTaxCode { get { return "Product035"; } }
        public static String ProductUnitMovement { get { return "Product036"; } }
        public static String ProductShare { get { return "Product037"; } }
        public static String ProductCaseMultiple { get { return "Product038"; } }
        public static String ProductDaysSupply { get { return "Product039"; } }
        public static String ProductCombinedPerformanceIndex { get { return "Product040"; } }
        public static String ProductPegSpan { get { return "Product041"; } }
        public static String ProductMinimumUnits { get { return "Product042"; } }
        public static String ProductMaximumUnits { get { return "Product043"; } }
        public static String ProductShapeId { get { return "Product044"; } }
        public static String ProductBitmapIdOverrride { get { return "Product045"; } }
        public static String ProductTrayWidth { get { return "Product046"; } }
        public static String ProductTrayHeight { get { return "Product047"; } }
        public static String ProductTrayDepth { get { return "Product048"; } }
        public static String ProductTrayNumberWide { get { return "Product049"; } }
        public static String ProductTrayNumberHigh { get { return "Product050"; } }
        public static String ProductTrayNumberDeep { get { return "Product051"; } }
        public static String ProductTrayTotalNumber { get { return "Product052"; } } // Tray Pack Units
        public static String ProductTrayMaxHigh { get { return "Product053"; } }
        public static String ProductCaseWidth { get { return "Product054"; } }
        public static String ProductCaseHeight { get { return "Product055"; } }
        public static String ProductCaseDepth { get { return "Product056"; } }
        public static String ProductCaseNumberWide { get { return "Product057"; } }
        public static String ProductCaseNumberHigh { get { return "Product058"; } }
        public static String ProductCaseNumberDeep { get { return "Product059"; } }
        public static String ProductCaseTotalNumber { get { return "Product060"; } } // Case Pack Units
        public static String ProductCaseMaxHigh { get { return "Product061"; } }
        public static String ProductDisplayWidth { get { return "Product062"; } }
        public static String ProductDisplayHeight { get { return "Product063"; } }
        public static String ProductDisplayDepth { get { return "Product064"; } }
        public static String ProductDisplayNumberWide { get { return "Product065"; } }
        public static String ProductDisplayNumberHigh { get { return "Product066"; } }
        public static String ProductDisplayNumberDeep { get { return "Product067"; } }
        public static String ProductDisplayTotalNumber { get { return "Product068"; } }
        public static String ProductDisplayMaxHigh { get { return "Product069"; } }
        public static String ProductAlternateWidth { get { return "Product070"; } }
        public static String ProductAlternateHeight { get { return "Product071"; } }
        public static String ProductAlternateDepth { get { return "Product072"; } }
        public static String ProductAlternateNumberWide { get { return "Product073"; } }
        public static String ProductAlternateNumberHigh { get { return "Product074"; } }
        public static String ProductAlternateNumberDeep { get { return "Product075"; } }
        public static String ProductAlternateTotalNumber { get { return "Product076"; } }
        public static String ProductAlternateMaxHigh { get { return "Product077"; } }
        public static String ProductLooseWidth { get { return "Product078"; } }
        public static String ProductLooseHeight { get { return "Product079"; } }
        public static String ProductLooseDepth { get { return "Product080"; } }
        public static String ProductLooseNumberWide { get { return "Product081"; } }
        public static String ProductLooseNumberHigh { get { return "Product082"; } }
        public static String ProductLooseNumberDeep { get { return "Product083"; } }
        public static String ProductLooseTotalNumber { get { return "Product084"; } }
        public static String ProductLooseMaxHigh { get { return "Product085"; } }
        public static String ProductMerchXMin { get { return "Product086"; } }
        public static String ProductMerchXMax { get { return "Product087"; } }
        public static String ProductMerchXUprights { get { return "Product088"; } }
        public static String ProductMerchXCaps { get { return "Product089"; } }
        public static String ProductMerchXPlacement { get { return "Product090"; } }
        public static String ProductMerchXNumber { get { return "Product091"; } }
        public static String ProductMerchXSize { get { return "Product092"; } }
        public static String ProductField093 { get { return "Product093"; } }
        public static String ProductField094 { get { return "Product094"; } }
        public static String ProductMerchYMin { get { return "Product095"; } }
        public static String ProductMerchYMax { get { return "Product096"; } }
        public static String ProductMerchYUprights { get { return "Product097"; } }
        public static String ProductMerchYCaps { get { return "Product098"; } }
        public static String ProductMerchYPlacement { get { return "Product099"; } }
        public static String ProductMerchYNumber { get { return "Product100"; } }
        public static String ProductMerchYSize { get { return "Product101"; } }
        public static String ProductField102 { get { return "Product102"; } }
        public static String ProductField103 { get { return "Product103"; } }
        public static String ProductMerchZMin { get { return "Product104"; } }
        public static String ProductMerchZMax { get { return "Product105"; } }
        public static String ProductMerchZUprights { get { return "Product106"; } }
        public static String ProductMerchZCaps { get { return "Product107"; } }
        public static String ProductMerchZPlacement { get { return "Product108"; } }
        public static String ProductMerchZNumber { get { return "Product109"; } }
        public static String ProductMerchZSize { get { return "Product110"; } }
        public static String ProductField111 { get { return "Product111"; } }
        public static String ProductField112 { get { return "Product112"; } }
        public static String ProductField113 { get { return "Product113"; } }
        public static String ProductDescription01 { get { return "Product114"; } }
        public static String ProductDescription02 { get { return "Product115"; } }
        public static String ProductDescription03 { get { return "Product116"; } }
        public static String ProductDescription04 { get { return "Product117"; } }
        public static String ProductDescription05 { get { return "Product118"; } }
        public static String ProductDescription06 { get { return "Product119"; } }
        public static String ProductDescription07 { get { return "Product120"; } }
        public static String ProductDescription08 { get { return "Product121"; } }
        public static String ProductDescription09 { get { return "Product122"; } }
        public static String ProductDescription10 { get { return "Product123"; } }
        public static String ProductDescription11 { get { return "Product124"; } }
        public static String ProductDescription12 { get { return "Product125"; } }
        public static String ProductDescription13 { get { return "Product126"; } }
        public static String ProductDescription14 { get { return "Product127"; } }
        public static String ProductDescription15 { get { return "Product128"; } }
        public static String ProductDescription16 { get { return "Product129"; } }
        public static String ProductDescription17 { get { return "Product130"; } }
        public static String ProductDescription18 { get { return "Product131"; } }
        public static String ProductDescription19 { get { return "Product132"; } }
        public static String ProductDescription20 { get { return "Product133"; } }
        public static String ProductDescription21 { get { return "Product134"; } }
        public static String ProductDescription22 { get { return "Product135"; } }
        public static String ProductDescription23 { get { return "Product136"; } }
        public static String ProductDescription24 { get { return "Product137"; } }
        public static String ProductDescription25 { get { return "Product138"; } }
        public static String ProductDescription26 { get { return "Product139"; } }
        public static String ProductDescription27 { get { return "Product140"; } }
        public static String ProductDescription28 { get { return "Product141"; } }
        public static String ProductDescription29 { get { return "Product142"; } }
        public static String ProductDescription30 { get { return "Product143"; } }
        public static String ProductDescription31 { get { return "Product144"; } }
        public static String ProductDescription32 { get { return "Product145"; } }
        public static String ProductDescription33 { get { return "Product146"; } }
        public static String ProductDescription34 { get { return "Product147"; } }
        public static String ProductDescription35 { get { return "Product148"; } }
        public static String ProductDescription36 { get { return "Product149"; } }
        public static String ProductDescription37 { get { return "Product150"; } }
        public static String ProductDescription38 { get { return "Product151"; } }
        public static String ProductDescription39 { get { return "Product152"; } }
        public static String ProductDescription40 { get { return "Product153"; } }
        public static String ProductDescription41 { get { return "Product154"; } }
        public static String ProductDescription42 { get { return "Product155"; } }
        public static String ProductDescription43 { get { return "Product156"; } }
        public static String ProductDescription44 { get { return "Product157"; } }
        public static String ProductDescription45 { get { return "Product158"; } }
        public static String ProductDescription46 { get { return "Product159"; } }
        public static String ProductDescription47 { get { return "Product160"; } }
        public static String ProductDescription48 { get { return "Product161"; } }
        public static String ProductDescription49 { get { return "Product162"; } }
        public static String ProductDescription50 { get { return "Product163"; } }
        public static String ProductValue01 { get { return "Product164"; } }
        public static String ProductValue02 { get { return "Product165"; } }
        public static String ProductValue03 { get { return "Product166"; } }
        public static String ProductValue04 { get { return "Product167"; } }
        public static String ProductValue05 { get { return "Product168"; } }
        public static String ProductValue06 { get { return "Product169"; } }
        public static String ProductValue07 { get { return "Product170"; } }
        public static String ProductValue08 { get { return "Product171"; } }
        public static String ProductValue09 { get { return "Product172"; } }
        public static String ProductValue10 { get { return "Product173"; } }
        public static String ProductValue11 { get { return "Product174"; } }
        public static String ProductValue12 { get { return "Product175"; } }
        public static String ProductValue13 { get { return "Product176"; } }
        public static String ProductValue14 { get { return "Product177"; } }
        public static String ProductValue15 { get { return "Product178"; } }
        public static String ProductValue16 { get { return "Product179"; } }
        public static String ProductValue17 { get { return "Product180"; } }
        public static String ProductValue18 { get { return "Product181"; } }
        public static String ProductValue19 { get { return "Product182"; } }
        public static String ProductValue20 { get { return "Product183"; } }
        public static String ProductValue21 { get { return "Product184"; } }
        public static String ProductValue22 { get { return "Product185"; } }
        public static String ProductValue23 { get { return "Product186"; } }
        public static String ProductValue24 { get { return "Product187"; } }
        public static String ProductValue25 { get { return "Product188"; } }
        public static String ProductValue26 { get { return "Product189"; } }
        public static String ProductValue27 { get { return "Product190"; } }
        public static String ProductValue28 { get { return "Product191"; } }
        public static String ProductValue29 { get { return "Product192"; } }
        public static String ProductValue30 { get { return "Product193"; } }
        public static String ProductValue31 { get { return "Product194"; } }
        public static String ProductValue32 { get { return "Product195"; } }
        public static String ProductValue33 { get { return "Product196"; } }
        public static String ProductValue34 { get { return "Product197"; } }
        public static String ProductValue35 { get { return "Product198"; } }
        public static String ProductValue36 { get { return "Product199"; } }
        public static String ProductValue37 { get { return "Product200"; } }
        public static String ProductValue38 { get { return "Product201"; } }
        public static String ProductValue39 { get { return "Product202"; } }
        public static String ProductValue40 { get { return "Product203"; } }
        public static String ProductValue41 { get { return "Product204"; } }
        public static String ProductValue42 { get { return "Product205"; } }
        public static String ProductValue43 { get { return "Product206"; } }
        public static String ProductValue44 { get { return "Product207"; } }
        public static String ProductValue45 { get { return "Product208"; } }
        public static String ProductValue46 { get { return "Product209"; } }
        public static String ProductValue47 { get { return "Product210"; } }
        public static String ProductValue48 { get { return "Product211"; } }
        public static String ProductValue49 { get { return "Product212"; } }
        public static String ProductValue50 { get { return "Product213"; } }
        public static String ProductFlag01 { get { return "Product214"; } }
        public static String ProductFlag02 { get { return "Product215"; } }
        public static String ProductFlag03 { get { return "Product216"; } }
        public static String ProductFlag04 { get { return "Product217"; } }
        public static String ProductFlag05 { get { return "Product218"; } }
        public static String ProductFlag06 { get { return "Product219"; } }
        public static String ProductFlag07 { get { return "Product220"; } }
        public static String ProductFlag08 { get { return "Product221"; } }
        public static String ProductFlag09 { get { return "Product222"; } }
        public static String ProductFlag10 { get { return "Product223"; } }
        public static String ProductMinimumSqueezeFactorX { get { return "Product224"; } }
        public static String ProductMinimumSqueezeFactorY { get { return "Product225"; } }
        public static String ProductMinimumSqueezeFactorZ { get { return "Product226"; } }
        public static String ProductMaximumSqueezeFactorX { get { return "Product227"; } }
        public static String ProductMaximumSqueezeFactorY { get { return "Product228"; } }
        public static String ProductMaximumSqueezeFactorZ { get { return "Product229"; } }
        public static String ProductFillPattern { get { return "Product230"; } }
        public static String ProductFieldModelFilename { get { return "Product231"; } }
        public static String ProductBrand { get { return "Product232"; } }
        public static String ProductSubCategory { get { return "Product233"; } }
        public static String ProductWeight { get { return "Product234"; } }
        public static String ProductField235 { get { return "Product235"; } }
        public static String ProductField236 { get { return "Product236"; } }
        public static String ProductFrontOverhang { get { return "Product237"; } }
        public static String ProductFingerSpaceX { get { return "Product238"; } }
        public static String ProductField239 { get { return "Product239"; } }
        public static String ProductField240 { get { return "Product240"; } }
        public static String ProductField241 { get { return "Product241"; } }
        public static String ProductField242 { get { return "Product242"; } }
        public static String ProductField243 { get { return "Product243"; } }
        public static String ProductField244 { get { return "Product244"; } }
        public static String ProductField245 { get { return "Product245"; } }
        public static String ProductField246 { get { return "Product246"; } }
        public static String ProductField247 { get { return "Product247"; } }
        public static String ProductField248 { get { return "Product248"; } }
        public static String ProductStatus { get { return "Product249"; } } // BLANK, Live, Pending, Work in progress, Historic, Analysis
        public static String ProductDateCreated { get { return "Product250"; } }
        public static String ProductDateModified { get { return "Product251"; } }
        public static String ProductDatePending { get { return "Product252"; } }
        public static String ProductDateEffective { get { return "Product253"; } }
        public static String ProductDateFinished { get { return "Product254"; } }
        public static String ProductDate1 { get { return "Product255"; } }
        public static String ProductDate2 { get { return "Product256"; } }
        public static String ProductDate3 { get { return "Product257"; } }
        public static String ProductCreatedBy { get { return "Product258"; } }
        public static String ProductModifiedBy { get { return "Product259"; } }
        public static String ProductTransparency { get { return "Product260"; } }
        public static String ProductPeakSafetyFactor { get { return "Product261"; } }
        public static String ProductBackRoomStock { get { return "Product262"; } }
        public static String ProductDeliverySchedule { get { return "Product263"; } }
        public static String ProductPartId { get { return "Product264"; } }
        public static String ProductField265 { get { return "Product265"; } }
        public static String ProductBitmapIdOverrideUnit { get { return "Product266"; } }
        public static String ProductModelFilenameLookup { get { return "Product267"; } }
        // 0=None, 1=Search for merch style specific model, 2=Model file represents a single unit, 3=Model file represents any merch style
        public static String ProductDefaultMerchStyle { get { return "Product268"; } }
        // -1 Default, 0 Unit, 1 Tray, 2 Case, 3 Display, 4 Alternate, 5 Loose, 6 Log Stack
        public static String ProductAutomaticModel { get { return "Product269"; } }
        // 0=NONE, 1=MODEL ONLY, 2=MODEL+LABEL BITMAP, 3=MODEL+EXTENDED BITMAP
        public static String ProductSqueezeOnlyUnitMerchandising { get { return "Product273"; } }
        public static String ProductNote1 { get { return "Product274"; } }
        public static String ProductNote2 { get { return "Product275"; } }
        public static String ProductNote3 { get { return "Product276"; } }
        public static String ProductNote4 { get { return "Product277"; } }
        public static String ProductNote5 { get { return "Product278"; } }
        #endregion

        #region Planogram Field Indexes

        public static String PlanogramName { get { return "Planogram001"; } }
        public static String PlanogramKey { get { return "Planogram002"; } }
        public static String PlanogramWidth { get { return "Planogram003"; } }
        public static String PlanogramHeight { get { return "Planogram004"; } }
        public static String PlanogramDepth { get { return "Planogram005"; } }
        public static String PlanogramColor { get { return "Planogram006"; } }
        public static String PlanogramBackDepth { get { return "Planogram007"; } }
        public static String PlanogramDrawBack { get { return "Planogram008"; } }
        public static String PlanogramBaseWidth { get { return "Planogram009"; } }
        public static String PlanogramBaseHeight { get { return "Planogram010"; } }
        public static String PlanogramBaseDepth { get { return "Planogram011"; } }
        public static String PlanogramDrawBase { get { return "Planogram012"; } }
        public static String PlanogramBaseColor { get { return "Planogram013"; } }
        public static String PlanogramDrawNotches { get { return "Planogram014"; } }
        public static String PlanogramNotchOffset { get { return "Planogram015"; } }
        public static String PlanogramNotchSpacing { get { return "Planogram016"; } }
        public static String PlanogramDoubleNotches { get { return "Planogram017"; } }
        public static String PlanogramNotchColor { get { return "Planogram018"; } }
        public static String PlanogramField019 { get { return "Planogram019"; } }
        public static String PlanogramField020 { get { return "Planogram020"; } }
        public static String PlanogramField021 { get { return "Planogram021"; } }
        public static String PlanogramTrafficFlow { get { return "Planogram022"; } }
        public static String PlanogramField023 { get { return "Planogram023"; } }
        public static String PlanogramShapeId { get { return "Planogram024"; } }
        public static String PlanogramBitmapId { get { return "Planogram025"; } }
        public static String PlanogramMerchXMin { get { return "Planogram026"; } }
        public static String PlanogramMerchXMax { get { return "Planogram027"; } }
        public static String PlanogramMerchXUprights { get { return "Planogram028"; } }
        public static String PlanogramMerchXCaps { get { return "Planogram029"; } }
        public static String PlanogramMerchXPlacement { get { return "Planogram030"; } }
        public static String PlanogramMerchXNumber { get { return "Planogram031"; } }
        public static String PlanogramMerchXSize { get { return "Planogram032"; } }
        public static String PlanogramMerchXDirection { get { return "Planogram033"; } }
        public static String PlanogramField034 { get { return "Planogram034"; } }
        public static String PlanogramMerchYMin { get { return "Planogram035"; } }
        public static String PlanogramMerchYMax { get { return "Planogram036"; } }
        public static String PlanogramMerchYUprights { get { return "Planogram037"; } }
        public static String PlanogramMerchYCaps { get { return "Planogram038"; } }
        public static String PlanogramMerchYPlacement { get { return "Planogram039"; } }
        public static String PlanogramMerchYNumber { get { return "Planogram040"; } }
        public static String PlanogramMerchYSize { get { return "Planogram041"; } }
        public static String PlanogramMerchYDirection { get { return "Planogram042"; } }
        public static String PlanogramField043 { get { return "Planogram043"; } }
        public static String PlanogramMerchZMin { get { return "Planogram044"; } }
        public static String PlanogramMerchZMax { get { return "Planogram045"; } }
        public static String PlanogramMerchZUprights { get { return "Planogram046"; } }
        public static String PlanogramMerchZCaps { get { return "Planogram047"; } }
        public static String PlanogramMerchZPlacement { get { return "Planogram048"; } }
        public static String PlanogramMerchZNumber { get { return "Planogram049"; } }
        public static String PlanogramMerchZSize { get { return "Planogram050"; } }
        public static String PlanogramMerchZDirection { get { return "Planogram051"; } }
        public static String PlanogramField052 { get { return "Planogram052"; } }
        public static String PlanogramField053 { get { return "Planogram053"; } }
        public static String PlanogramNumerOfStores { get { return "Planogram054"; } }
        public static String PlanogramNotchWidth { get { return "Planogram055"; } }
        public static String PlanogramDescription01 { get { return "Planogram056"; } }
        public static String PlanogramDescription02 { get { return "Planogram057"; } }
        public static String PlanogramDescription03 { get { return "Planogram058"; } }
        public static String PlanogramDescription04 { get { return "Planogram059"; } }
        public static String PlanogramDescription05 { get { return "Planogram060"; } }
        public static String PlanogramDescription06 { get { return "Planogram061"; } }
        public static String PlanogramDescription07 { get { return "Planogram062"; } }
        public static String PlanogramDescription08 { get { return "Planogram063"; } }
        public static String PlanogramDescription09 { get { return "Planogram064"; } }
        public static String PlanogramDescription10 { get { return "Planogram065"; } }
        public static String PlanogramDescription11 { get { return "Planogram066"; } }
        public static String PlanogramDescription12 { get { return "Planogram067"; } }
        public static String PlanogramDescription13 { get { return "Planogram068"; } }
        public static String PlanogramDescription14 { get { return "Planogram069"; } }
        public static String PlanogramDescription15 { get { return "Planogram070"; } }
        public static String PlanogramDescription16 { get { return "Planogram071"; } }
        public static String PlanogramDescription17 { get { return "Planogram072"; } }
        public static String PlanogramDescription18 { get { return "Planogram073"; } }
        public static String PlanogramDescription19 { get { return "Planogram074"; } }
        public static String PlanogramDescription20 { get { return "Planogram075"; } }
        public static String PlanogramDescription21 { get { return "Planogram076"; } }
        public static String PlanogramDescription22 { get { return "Planogram077"; } }
        public static String PlanogramDescription23 { get { return "Planogram078"; } }
        public static String PlanogramDescription24 { get { return "Planogram079"; } }
        public static String PlanogramDescription25 { get { return "Planogram080"; } }
        public static String PlanogramDescription26 { get { return "Planogram081"; } }
        public static String PlanogramDescription27 { get { return "Planogram082"; } }
        public static String PlanogramDescription28 { get { return "Planogram083"; } }
        public static String PlanogramDescription29 { get { return "Planogram084"; } }
        public static String PlanogramDescription30 { get { return "Planogram085"; } }
        public static String PlanogramDescription31 { get { return "Planogram086"; } }
        public static String PlanogramDescription32 { get { return "Planogram087"; } }
        public static String PlanogramDescription33 { get { return "Planogram088"; } }
        public static String PlanogramDescription34 { get { return "Planogram089"; } }
        public static String PlanogramDescription35 { get { return "Planogram090"; } }
        public static String PlanogramDescription36 { get { return "Planogram091"; } }
        public static String PlanogramDescription37 { get { return "Planogram092"; } }
        public static String PlanogramDescription38 { get { return "Planogram093"; } }
        public static String PlanogramDescription39 { get { return "Planogram094"; } }
        public static String PlanogramDescription40 { get { return "Planogram095"; } }
        public static String PlanogramDescription41 { get { return "Planogram096"; } }
        public static String PlanogramDescription42 { get { return "Planogram097"; } }
        public static String PlanogramDescription43 { get { return "Planogram098"; } }
        public static String PlanogramDescription44 { get { return "Planogram099"; } }
        public static String PlanogramDescription45 { get { return "Planogram100"; } }
        public static String PlanogramDescription46 { get { return "Planogram101"; } }
        public static String PlanogramDescription47 { get { return "Planogram102"; } }
        public static String PlanogramDescription48 { get { return "Planogram103"; } }
        public static String PlanogramDescription49 { get { return "Planogram104"; } }
        public static String PlanogramDescription50 { get { return "Planogram105"; } }
        public static String PlanogramValue01 { get { return "Planogram106"; } }
        public static String PlanogramValue02 { get { return "Planogram107"; } }
        public static String PlanogramValue03 { get { return "Planogram108"; } }
        public static String PlanogramValue04 { get { return "Planogram109"; } }
        public static String PlanogramValue05 { get { return "Planogram110"; } }
        public static String PlanogramValue06 { get { return "Planogram111"; } }
        public static String PlanogramValue07 { get { return "Planogram112"; } }
        public static String PlanogramValue08 { get { return "Planogram113"; } }
        public static String PlanogramValue09 { get { return "Planogram114"; } }
        public static String PlanogramValue10 { get { return "Planogram115"; } }
        public static String PlanogramValue11 { get { return "Planogram116"; } }
        public static String PlanogramValue12 { get { return "Planogram117"; } }
        public static String PlanogramValue13 { get { return "Planogram118"; } }
        public static String PlanogramValue14 { get { return "Planogram119"; } }
        public static String PlanogramValue15 { get { return "Planogram120"; } }
        public static String PlanogramValue16 { get { return "Planogram121"; } }
        public static String PlanogramValue17 { get { return "Planogram122"; } }
        public static String PlanogramValue18 { get { return "Planogram123"; } }
        public static String PlanogramValue19 { get { return "Planogram124"; } }
        public static String PlanogramValue20 { get { return "Planogram125"; } }
        public static String PlanogramValue21 { get { return "Planogram126"; } }
        public static String PlanogramValue22 { get { return "Planogram127"; } }
        public static String PlanogramValue23 { get { return "Planogram128"; } }
        public static String PlanogramValue24 { get { return "Planogram129"; } }
        public static String PlanogramValue25 { get { return "Planogram130"; } }
        public static String PlanogramValue26 { get { return "Planogram131"; } }
        public static String PlanogramValue27 { get { return "Planogram132"; } }
        public static String PlanogramValue28 { get { return "Planogram133"; } }
        public static String PlanogramValue29 { get { return "Planogram134"; } }
        public static String PlanogramValue30 { get { return "Planogram135"; } }
        public static String PlanogramValue31 { get { return "Planogram136"; } }
        public static String PlanogramValue32 { get { return "Planogram137"; } }
        public static String PlanogramValue33 { get { return "Planogram138"; } }
        public static String PlanogramValue34 { get { return "Planogram139"; } }
        public static String PlanogramValue35 { get { return "Planogram140"; } }
        public static String PlanogramValue36 { get { return "Planogram141"; } }
        public static String PlanogramValue37 { get { return "Planogram142"; } }
        public static String PlanogramValue38 { get { return "Planogram143"; } }
        public static String PlanogramValue39 { get { return "Planogram144"; } }
        public static String PlanogramValue40 { get { return "Planogram145"; } }
        public static String PlanogramValue41 { get { return "Planogram146"; } }
        public static String PlanogramValue42 { get { return "Planogram147"; } }
        public static String PlanogramValue43 { get { return "Planogram148"; } }
        public static String PlanogramValue44 { get { return "Planogram149"; } }
        public static String PlanogramValue45 { get { return "Planogram150"; } }
        public static String PlanogramValue46 { get { return "Planogram151"; } }
        public static String PlanogramValue47 { get { return "Planogram152"; } }
        public static String PlanogramValue48 { get { return "Planogram153"; } }
        public static String PlanogramValue49 { get { return "Planogram154"; } }
        public static String PlanogramValue50 { get { return "Planogram155"; } }
        public static String PlanogramFlag01 { get { return "Planogram156"; } }
        public static String PlanogramFlag02 { get { return "Planogram157"; } }
        public static String PlanogramFlag03 { get { return "Planogram158"; } }
        public static String PlanogramFlag04 { get { return "Planogram159"; } }
        public static String PlanogramFlag05 { get { return "Planogram160"; } }
        public static String PlanogramFlag06 { get { return "Planogram161"; } }
        public static String PlanogramFlag07 { get { return "Planogram162"; } }
        public static String PlanogramFlag08 { get { return "Planogram163"; } }
        public static String PlanogramFlag09 { get { return "Planogram164"; } }
        public static String PlanogramFlag10 { get { return "Planogram165"; } }
        public static String PlanogramFillPattern { get { return "Planogram166"; } }
        public static String PlanogramSegmentsToPrint { get { return "Planogram167"; } }
        public static String PlanogramFileName { get { return "Planogram168"; } }
        public static String PlanogramField169 { get { return "Planogram169"; } }
        public static String PlanogramLayoutFileName { get { return "Planogram170"; } }
        public static String PlanogramNotes { get { return "Planogram171"; } }
        public static String PlanogramField172 { get { return "Planogram172"; } }
        public static String PlanogramField173 { get { return "Planogram173"; } }
        public static String PlanogramField174 { get { return "Planogram174"; } }
        public static String PlanogramField175 { get { return "Planogram175"; } }
        public static String PlanogramField176 { get { return "Planogram176"; } }
        public static String PlanogramField177 { get { return "Planogram177"; } }
        public static String PlanogramField178 { get { return "Planogram178"; } }
        public static String PlanogramField179 { get { return "Planogram179"; } }
        public static String PlanogramField180 { get { return "Planogram180"; } }
        public static String PlanogramField181 { get { return "Planogram181"; } }
        public static String PlanogramField182 { get { return "Planogram182"; } }
        public static String PlanogramStatus1 { get { return "Planogram183"; } }
        public static String PlanogramStatus2 { get { return "Planogram184"; } }
        public static String PlanogramStatus3 { get { return "Planogram185"; } }
        public static String PlanogramDateCreated { get { return "Planogram186"; } }
        public static String PlanogramDateModified { get { return "Planogram187"; } }
        public static String PlanogramDatePending { get { return "Planogram188"; } }
        public static String PlanogramDateEffective { get { return "Planogram189"; } }
        public static String PlanogramDateFinished { get { return "Planogram190"; } }
        public static String PlanogramDate1 { get { return "Planogram191"; } }
        public static String PlanogramDate2 { get { return "Planogram192"; } }
        public static String PlanogramDate3 { get { return "Planogram193"; } }
        public static String PlanogramCreatedBy { get { return "Planogram194"; } }
        public static String PlanogramModifiedBy { get { return "Planogram195"; } }
        public static String PlanogramFloorBitmapId { get { return "Planogram196"; } }
        public static String PlanogramDoorTransparency { get { return "Planogram197"; } }
        public static String PlanogramFloorTileWidth { get { return "Planogram198"; } }
        public static String PlanogramFloorTileDepth { get { return "Planogram199"; } }
        public static String PlanogramInventoryManual { get { return "Planogram200"; } }
        public static String PlanogramInventoryCaseMultiple { get { return "Planogram201"; } }
        public static String PlanogramInventoryDaysSupply { get { return "Planogram202"; } }
        public static String PlanogramInventoryPeak { get { return "Planogram203"; } }
        public static String PlanogramInventoryMinUnits { get { return "Planogram204"; } }
        public static String PlanogramInventoryMaxUnits { get { return "Planogram205"; } }
        public static String PlanogramCaseMultiple { get { return "Planogram206"; } }
        public static String PlanogramDaysSupply { get { return "Planogram207"; } }
        public static String PlanogramDemandCycleLength { get { return "Planogram208"; } }
        public static String PlanogramPeakSafetyFactor { get { return "Planogram209"; } }
        public static String PlanogramBackroomStock { get { return "Planogram210"; } }
        public static String PlanogramDemand01 { get { return "Planogram211"; } }
        public static String PlanogramDemand02 { get { return "Planogram212"; } }
        public static String PlanogramDemand03 { get { return "Planogram213"; } }
        public static String PlanogramDemand04 { get { return "Planogram214"; } }
        public static String PlanogramDemand05 { get { return "Planogram215"; } }
        public static String PlanogramDemand06 { get { return "Planogram216"; } }
        public static String PlanogramDemand07 { get { return "Planogram217"; } }
        public static String PlanogramDeliverySchedule { get { return "Planogram218"; } }
        public static String PlanogramId { get { return "Planogram219"; } }
        public static String PlanogramDepartment { get { return "Planogram220"; } }
        public static String PlanogramPartId { get { return "Planogram221"; } }
        public static String PlanogramGln { get { return "Planogram222"; } }
        public static String PlanogramPlanogramGuid { get { return "Planogram223"; } }
        public static String PlanogramField224 { get { return "Planogram224"; } }
        public static String PlanogramAbbrevName { get { return "Planogram225"; } }
        public static String PlanogramCategory { get { return "Planogram226"; } }
        public static String PlanogramSubCategory { get { return "Planogram227"; } }
        public static String PlanogramNote1 { get { return "Planogram228"; } }
        public static String PlanogramNote2 { get { return "Planogram229"; } }
        public static String PlanogramNote3 { get { return "Planogram230"; } }
        public static String PlanogramNote4 { get { return "Planogram231"; } }
        public static String PlanogramNote5 { get { return "Planogram232"; } }
        #endregion

        #region Segment Field Indexes

        public static String SegmentName { get { return "Segment001"; } }
        public static String SegmentKey { get { return "Segment002"; } }
        public static String SegmentX { get { return "Segment003"; } }
        public static String SegmentWidth { get { return "Segment004"; } }
        public static String SegmentY { get { return "Segment005"; } }
        public static String SegmentHeight { get { return "Segment006"; } }
        public static String SegmentZ { get { return "Segment007"; } }
        public static String SegmentDepth { get { return "Segment008"; } }
        public static String SegmentAngle { get { return "Segment009"; } }
        public static String SegmentOffsetX { get { return "Segment010"; } }
        public static String SegmentOffsetY { get { return "Segment011"; } }
        public static String SegmentDoor { get { return "Segment012"; } }
        public static String SegmentDoorDirection { get { return "Segment013"; } }
        public static String SegmentDescription01 { get { return "Segment014"; } }
        public static String SegmentDescription02 { get { return "Segment015"; } }
        public static String SegmentDescription03 { get { return "Segment016"; } }
        public static String SegmentDescription04 { get { return "Segment017"; } }
        public static String SegmentDescription05 { get { return "Segment018"; } }
        public static String SegmentDescription06 { get { return "Segment019"; } }
        public static String SegmentDescription07 { get { return "Segment020"; } }
        public static String SegmentDescription08 { get { return "Segment021"; } }
        public static String SegmentDescription09 { get { return "Segment022"; } }
        public static String SegmentDescription10 { get { return "Segment023"; } }
        public static String SegmentValue01 { get { return "Segment024"; } }
        public static String SegmentValue02 { get { return "Segment025"; } }
        public static String SegmentValue03 { get { return "Segment026"; } }
        public static String SegmentValue04 { get { return "Segment027"; } }
        public static String SegmentValue05 { get { return "Segment028"; } }
        public static String SegmentValue06 { get { return "Segment029"; } }
        public static String SegmentValue07 { get { return "Segment030"; } }
        public static String SegmentValue08 { get { return "Segment031"; } }
        public static String SegmentValue09 { get { return "Segment032"; } }
        public static String SegmentValue10 { get { return "Segment033"; } }
        public static String SegmentFlag01 { get { return "Segment034"; } }
        public static String SegmentFlag02 { get { return "Segment035"; } }
        public static String SegmentFlag03 { get { return "Segment036"; } }
        public static String SegmentFlag04 { get { return "Segment037"; } }
        public static String SegmentFlag05 { get { return "Segment038"; } }
        public static String SegmentFlag06 { get { return "Segment039"; } }
        public static String SegmentFlag07 { get { return "Segment040"; } }
        public static String SegmentFlag08 { get { return "Segment041"; } }
        public static String SegmentFlag09 { get { return "Segment042"; } }
        public static String SegmentFlag10 { get { return "Segment043"; } }
        public static String SegmentFrameWidth { get { return "Segment044"; } }
        public static String SegmentFrameHeight { get { return "Segment045"; } }
        public static String SegmentNumber { get { return "Segment046"; } }
        public static String SegmentFrameFillColor { get { return "Segment047"; } }
        public static String SegmentFrameFillPattern { get { return "Segment048"; } }
        public static String SegmentPartId { get { return "Segment049"; } }
        public static String SegmentGln { get { return "Segment050"; } } // Global Location Number

        #endregion

        #region Fixture Field Indexes

        public static String FixtureType { get { return "Fixture001"; } }
        public static String FixtureName { get { return "Fixture002"; } }
        public static String FixtureKey { get { return "Fixture003"; } }
        public static String FixtureX { get { return "Fixture004"; } }
        public static String FixtureWidth { get { return "Fixture005"; } }
        public static String FixtureY { get { return "Fixture006"; } }
        public static String FixtureHeight { get { return "Fixture007"; } } // For chests this is the wall height.
        public static String FixtureZ { get { return "Fixture008"; } }
        public static String FixtureDepth { get { return "Fixture009"; } }
        public static String FixtureSlope { get { return "Fixture010"; } }
        public static String FixtureAngle { get { return "Fixture011"; } }
        public static String FixtureRoll { get { return "Fixture012"; } }
        public static String FixtureColour { get { return "Fixture013"; } }
        public static String FixtureAssembly { get { return "Fixture014"; } }
        public static String FixtureXSpacing { get { return "Fixture015"; } } // Peg X Spacing, for shelves... Divider spacing.
        public static String FixtureYSpacing { get { return "Fixture016"; } } // Peg Y spacing.
        public static String FixtureXStart { get { return "Fixture017"; } } // Peg X Start.
        public static String FixtureYStart { get { return "Fixture018"; } } // Peg Y Start.
        public static String FixtureWallWidth { get { return "Fixture019"; } }
        public static String FixtureWallHeight { get { return "Fixture020"; } } // For chests this is the component's height
        public static String FixtureWallDepth { get { return "Fixture021"; } }
        public static String FixtureCurve { get { return "Fixture022"; } }
        public static String FixtureMerch { get { return "Fixture023"; } } // Merchandisible Height or Depth (depending on component type).
        public static String FixtureCanObstruct { get { return "Fixture024"; } }
        public static String FixtureDetectOtherFixtures { get { return "Fixture025"; } }
        public static String FixtureDetectPositionsOnOtherFixtures { get { return "Fixture026"; } }
        public static String FixtureLeftOverhang { get { return "Fixture027"; } }
        public static String FixtureRightOverhang { get { return "Fixture028"; } }
        public static String FixtureLowerOverhang { get { return "Fixture029"; } }
        public static String FixtureUpperOverhang { get { return "Fixture030"; } }
        public static String FixtureBackOverhang { get { return "Fixture031"; } }
        public static String FixtureFrontOverhang { get { return "Fixture032"; } }
        public static String FixtureMerchandisingStyle { get { return "Fixture033"; } }
        public static String FixtureDividerWidth { get { return "Fixture034"; } }
        public static String FixtureDividerHeight { get { return "Fixture035"; } }
        public static String FixtureDividerDepth { get { return "Fixture036"; } }
        public static String FixtureCanCombine { get { return "Fixture037"; } } // 0=No, 1=Yes, 2=Left, 3=Right.
        public static String FixtureGrilleHeight { get { return "Fixture038"; } }
        public static String FixtureNotchOffset { get { return "Fixture039"; } }
        public static String FixtureXSpacing2 { get { return "Fixture040"; } }
        public static String FixtureXStart2 { get { return "Fixture041"; } }
        public static String FixturePegDrop { get { return "Fixture042"; } }
        public static String FixturePegGapX { get { return "Fixture043"; } }
        public static String FixturePegGapY { get { return "Fixture044"; } }
        public static String FixturePrimaryLabelFormatName { get { return "Fixture045"; } }
        public static String FixtureSecondaryLabelFormatName { get { return "Fixture046"; } }
        public static String FixtureShapeId { get { return "Fixture047"; } }
        public static String FixtureBitmapId { get { return "Fixture048"; } }
        public static String FixtureMerchXMin { get { return "Fixture049"; } } // -1=Default, 0=No Limit, X=Value
        public static String FixtureMerchXMax { get { return "Fixture050"; } } // -1=Default, 0=No Limit, X=Value
        public static String FixtureMerchXUprights { get { return "Fixture051"; } } // -1=Default, X=Value
        public static String FixtureMerchXCaps { get { return "Fixture052"; } } // -1=Default, X=Value
        public static String FixtureMerchXPlacement { get { return "Fixture053"; } } // -1=Default, 0=Manual, 1=Edge, 2=Stack, 3=Spread
        public static String FixtureMerchXNumber { get { return "Fixture054"; } } // 0=Default, 1=Manual, 2=One, 3=Fill
        public static String FixtureMerchXSize { get { return "Fixture055"; } } // 0=Default, 1=Normal, 2=Adjust, 3=Space
        public static String FixtureMerchXDirection { get { return "Fixture056"; } } // 0=Default, 1=Normal, 2=Reverse
        public static String FixtureField057 { get { return "Fixture057"; } }
        public static String FixtureMerchYMin { get { return "Fixture058"; } } // -1=Default, 0=No Limit, X=Value
        public static String FixtureMerchYMax { get { return "Fixture059"; } } // -1=Default, 0=No Limit, X=Value
        public static String FixtureMerchYUprights { get { return "Fixture060"; } } // -1=Default, X=Value
        public static String FixtureMerchYCaps { get { return "Fixture061"; } } // -1=Default, X=Value
        public static String FixtureMerchYPlacement { get { return "Fixture062"; } } // -1=Default, 0=Manual, 1=Edge, 2=Stack, 3=Spread
        public static String FixtureMerchYNumber { get { return "Fixture063"; } } // 0=Default, 1=Manual, 2=One, 3=Fill
        public static String FixtureMerchYSize { get { return "Fixture064"; } } // 0=Default, 1=Normal, 2=Adjust, 3=Space
        public static String FixtureMerchYDirection { get { return "Fixture065"; } } // 0=Default, 1=Normal, 2=Reverse
        public static String FixtureField066 { get { return "Fixture066"; } }
        public static String FixtureMerchZMin { get { return "Fixture067"; } } // -1=Default, 0=No Limit, X=Value
        public static String FixtureMerchZMax { get { return "Fixture068"; } } // -1=Default, 0=No Limit, X=Value
        public static String FixtureMerchZUprights { get { return "Fixture069"; } } // -1=Default, X=Value
        public static String FixtureMerchZCaps { get { return "Fixture070"; } } // -1=Default, X=Value
        public static String FixtureMerchZPlacement { get { return "Fixture071"; } } // -1=Default, 0=Manual, 1=Edge, 2=Stack, 3=Spread
        public static String FixtureMerchZNumber { get { return "Fixture072"; } } // 0=Default, 1=Manual, 2=One, 3=Fill
        public static String FixtureMerchZSize { get { return "Fixture073"; } } // 0=Default, 1=Normal, 2=Adjust, 3=Space
        public static String FixtureMerchZDirection { get { return "Fixture074"; } } // 0=Default, 1=Normal, 2=Reverse
        public static String FixtureField075 { get { return "Fixture075"; } }
        public static String FixtureDescription01 { get { return "Fixture076"; } }
        public static String FixtureDescription02 { get { return "Fixture077"; } }
        public static String FixtureDescription03 { get { return "Fixture078"; } }
        public static String FixtureDescription04 { get { return "Fixture079"; } }
        public static String FixtureDescription05 { get { return "Fixture080"; } }
        public static String FixtureDescription06 { get { return "Fixture081"; } }
        public static String FixtureDescription07 { get { return "Fixture082"; } }
        public static String FixtureDescription08 { get { return "Fixture083"; } }
        public static String FixtureDescription09 { get { return "Fixture084"; } }
        public static String FixtureDescription10 { get { return "Fixture085"; } }
        public static String FixtureDescription11 { get { return "Fixture086"; } }
        public static String FixtureDescription12 { get { return "Fixture087"; } }
        public static String FixtureDescription13 { get { return "Fixture088"; } }
        public static String FixtureDescription14 { get { return "Fixture089"; } }
        public static String FixtureDescription15 { get { return "Fixture090"; } }
        public static String FixtureDescription16 { get { return "Fixture091"; } }
        public static String FixtureDescription17 { get { return "Fixture092"; } }
        public static String FixtureDescription18 { get { return "Fixture093"; } }
        public static String FixtureDescription19 { get { return "Fixture094"; } }
        public static String FixtureDescription20 { get { return "Fixture095"; } }
        public static String FixtureDescription21 { get { return "Fixture096"; } }
        public static String FixtureDescription22 { get { return "Fixture097"; } }
        public static String FixtureDescription23 { get { return "Fixture098"; } }
        public static String FixtureDescription24 { get { return "Fixture099"; } }
        public static String FixtureDescription25 { get { return "Fixture100"; } }
        public static String FixtureDescription26 { get { return "Fixture101"; } }
        public static String FixtureDescription27 { get { return "Fixture102"; } }
        public static String FixtureDescription28 { get { return "Fixture103"; } }
        public static String FixtureDescription29 { get { return "Fixture104"; } }
        public static String FixtureDescription30 { get { return "Fixture105"; } }
        public static String FixtureValue01 { get { return "Fixture106"; } }
        public static String FixtureValue02 { get { return "Fixture107"; } }
        public static String FixtureValue03 { get { return "Fixture108"; } }
        public static String FixtureValue04 { get { return "Fixture109"; } }
        public static String FixtureValue05 { get { return "Fixture110"; } }
        public static String FixtureValue06 { get { return "Fixture111"; } }
        public static String FixtureValue07 { get { return "Fixture112"; } }
        public static String FixtureValue08 { get { return "Fixture113"; } }
        public static String FixtureValue09 { get { return "Fixture114"; } }
        public static String FixtureValue10 { get { return "Fixture115"; } }
        public static String FixtureValue11 { get { return "Fixture116"; } }
        public static String FixtureValue12 { get { return "Fixture117"; } }
        public static String FixtureValue13 { get { return "Fixture118"; } }
        public static String FixtureValue14 { get { return "Fixture119"; } }
        public static String FixtureValue15 { get { return "Fixture120"; } }
        public static String FixtureValue16 { get { return "Fixture121"; } }
        public static String FixtureValue17 { get { return "Fixture122"; } }
        public static String FixtureValue18 { get { return "Fixture123"; } }
        public static String FixtureValue19 { get { return "Fixture124"; } }
        public static String FixtureValue20 { get { return "Fixture125"; } }
        public static String FixtureValue21 { get { return "Fixture126"; } }
        public static String FixtureValue22 { get { return "Fixture127"; } }
        public static String FixtureValue23 { get { return "Fixture128"; } }
        public static String FixtureValue24 { get { return "Fixture129"; } }
        public static String FixtureValue25 { get { return "Fixture130"; } }
        public static String FixtureValue26 { get { return "Fixture131"; } }
        public static String FixtureValue27 { get { return "Fixture132"; } }
        public static String FixtureValue28 { get { return "Fixture133"; } }
        public static String FixtureValue29 { get { return "Fixture134"; } }
        public static String FixtureValue30 { get { return "Fixture135"; } }
        public static String FixtureFlag01 { get { return "Fixture136"; } }
        public static String FixtureFlag02 { get { return "Fixture137"; } }
        public static String FixtureFlag03 { get { return "Fixture138"; } }
        public static String FixtureFlag04 { get { return "Fixture139"; } }
        public static String FixtureFlag05 { get { return "Fixture140"; } }
        public static String FixtureFlag06 { get { return "Fixture141"; } }
        public static String FixtureFlag07 { get { return "Fixture142"; } }
        public static String FixtureFlag08 { get { return "Fixture143"; } }
        public static String FixtureFlag09 { get { return "Fixture144"; } }
        public static String FixtureFlag10 { get { return "Fixture145"; } }
        public static String FixturePositionId { get { return "Fixture146"; } }
        public static String FixtureFillPattern { get { return "Fixture147"; } }
        public static String FixtureModelFilename { get { return "Fixture148"; } }
        public static String FixtureWeightCapacity { get { return "Fixture149"; } }
        public static String FixtureField150 { get { return "Fixture0150"; } }
        public static String FixtureDividerAtStart { get { return "Fixture151"; } }
        public static String FixtureDividerAtEnd { get { return "Fixture152"; } }
        public static String FixtureDividerBetweenFacings { get { return "Fixture153"; } }
        public static String FixtureTransparency { get { return "Fixture154"; } }
        public static String FixtureHideIfPrinting { get { return "Fixture155"; } }
        public static String FixtureProductAssociation { get { return "Fixture156"; } }
        public static String FixturePartId { get { return "Fixture157"; } }
        public static String FixtureHideViewDimensions { get { return "Fixture158"; } }
        public static String FixtureGln { get { return "Fixture159"; } }

        #endregion

        #region Position Field Indexes

        public static String PositionProductUpc { get { return "Position001"; } }
        public static String PositionProductId { get { return "Position002"; } }
        public static String PositionX { get { return "Position004"; } }
        public static String PositionWidth { get { return "Position005"; } }
        public static String PositionY { get { return "Position006"; } }
        public static String PositionHeight { get { return "Position007"; } }
        public static String PositionZ { get { return "Position008"; } }
        public static String PositionDepth { get { return "Position009"; } }
        public static String PositionSlope { get { return "Position010"; } }
        public static String PositionAngle { get { return "Position011"; } }
        public static String PositionRoll { get { return "Position012"; } }
        public static String PositionMerchStyle { get { return "Position013"; } }
        public static String PositionHFacings { get { return "Position014"; } } //  Facings Wide
        public static String PositionVFacings { get { return "Position015"; } } //  Facings High
        public static String PositionDFacings { get { return "Position016"; } } //  Facings Deep
        public static String PositionXCapNum { get { return "Position017"; } }
        public static String PositionXCapNested { get { return "Position018"; } }
        public static String PositionXCapReversed { get { return "Position019"; } } //  Right/Left
        public static String PositionXCapOrientation { get { return "Position020"; } }
        public static String PositionYCapNum { get { return "Position021"; } }
        public static String PositionYCapNested { get { return "Position022"; } }
        public static String PositionYCapReversed { get { return "Position023"; } } //  Top/Bottom
        public static String PositionYCapOrientation { get { return "Position024"; } }
        public static String PositionZCapNum { get { return "Position025"; } }
        public static String PositionZCapNested { get { return "Position026"; } }
        public static String PositionZCapReversed { get { return "Position027"; } } //  Top/Bottom
        public static String PositionZCapOrientation { get { return "Position028"; } }
        public static String PositionOrientation { get { return "Position029"; } }
        public static String Position030 { get { return "Position030"; } }
        public static String Position031 { get { return "Position031"; } }
        public static String Position032 { get { return "Position032"; } }
        public static String PositionMerchStyleWidth { get { return "Position033"; } } // Might be FullWidth
        public static String PositionMerchStyleHeight { get { return "Position034"; } } // Might be FullHeight
        public static String PositionMerchStyleDepth { get { return "Position035"; } } // Might be FullDepth
        public static String PositionFullWidth { get { return "Position036"; } } // Might be MerchStyleWidth
        public static String PositionFullHeight { get { return "Position037"; } } // Might be MerchStyleHeight
        public static String PositionFullDepth { get { return "Position038"; } } // Might be MerchStyleDepth
        public static String Position039 { get { return "Position039"; } }
        public static String Position040 { get { return "Position040"; } }
        public static String Position041 { get { return "Position041"; } }
        public static String PositionPegId { get { return "Position042"; } }
        public static String PositionManualUnits { get { return "Position043"; } }
        public static String PositionRankX { get { return "Position044"; } } //  Sequence X
        public static String PositionRankY { get { return "Position045"; } } //  Sequence Y
        public static String PositionRankZ { get { return "Position046"; } } //  Sequence Z
        public static String PositionAlwaysFloat { get { return "Position048"; } }
        public static String PositionPrimaryLabelFormat { get { return "Position049"; } }
        public static String PositionSecondaryLabelFormat { get { return "Position050"; } }
        public static String PositionMerchXMin { get { return "Position051"; } }
        public static String PositionMerchXMax { get { return "Position052"; } }
        public static String PositionMerchXUprights { get { return "Position053"; } }
        public static String PositionMerchXCaps { get { return "Position054"; } }
        public static String PositionMerchXPlacement { get { return "Position055"; } }
        public static String PositionMerchXnumber { get { return "Position056"; } }
        public static String PositionMerchXSize { get { return "Position057"; } }
        public static String Position058 { get { return "Position058"; } }
        public static String Position059 { get { return "Position059"; } }
        public static String PositionMerchYMin { get { return "Position060"; } }
        public static String PositionMerchYMax { get { return "Position061"; } }
        public static String PositionMerchYUprights { get { return "Position062"; } }
        public static String PositionMerchYCaps { get { return "Position063"; } }
        public static String PositionMerchYPlacement { get { return "Position064"; } }
        public static String PositionMerchYNumber { get { return "Position065"; } }
        public static String PositionMerchYSize { get { return "Position066"; } }
        public static String Position067 { get { return "Position067"; } }
        public static String Position068 { get { return "Position068"; } }
        public static String PositionMerchZMin { get { return "Position069"; } }
        public static String PositionMerchZMax { get { return "Position070"; } }
        public static String PositionMerchZUprights { get { return "Position071"; } }
        public static String PositionMerchZCaps { get { return "Position072"; } }
        public static String PositionMerchZPlacement { get { return "Position073"; } }
        public static String PositionMerchZNumber { get { return "Position074"; } }
        public static String PositionMerchZSize { get { return "Position075"; } }
        public static String Position076 { get { return "Position076"; } }
        public static String Position077 { get { return "Position077"; } }
        public static String Position078 { get { return "Position078"; } }
        public static String Position079 { get { return "Position079"; } }
        public static String Position080 { get { return "Position080"; } }
        public static String Position081 { get { return "Position081"; } }
        public static String Position082 { get { return "Position082"; } }
        public static String Position083 { get { return "Position083"; } }
        public static String Position084 { get { return "Position084"; } }
        public static String Position085 { get { return "Position085"; } }
        public static String Position086 { get { return "Position086"; } }
        public static String Position087 { get { return "Position087"; } }
        public static String Position088 { get { return "Position088"; } }
        public static String Position089 { get { return "Position089"; } }
        public static String Position090 { get { return "Position090"; } }
        public static String Position091 { get { return "Position091"; } }
        public static String Position092 { get { return "Position092"; } }
        public static String Position093 { get { return "Position093"; } }
        public static String Position094 { get { return "Position094"; } }
        public static String Position095 { get { return "Position095"; } }
        public static String Position096 { get { return "Position096"; } }
        public static String Position097 { get { return "Position097"; } }
        public static String Position098 { get { return "Position098"; } }
        public static String Position099 { get { return "Position099"; } }
        public static String Position100 { get { return "Position100"; } }
        public static String Position101 { get { return "Position101"; } }
        public static String Position102 { get { return "Position102"; } }
        public static String Position103 { get { return "Position103"; } }
        public static String Position104 { get { return "Position104"; } }
        public static String Position105 { get { return "Position105"; } }
        public static String Position106 { get { return "Position106"; } }
        public static String Position107 { get { return "Position107"; } }
        public static String Position108 { get { return "Position108"; } }
        public static String Position109 { get { return "Position109"; } }
        public static String Position110 { get { return "Position110"; } }
        public static String Position111 { get { return "Position111"; } }
        public static String Position112 { get { return "Position112"; } }
        public static String Position113 { get { return "Position113"; } }
        public static String Position114 { get { return "Position114"; } }
        public static String Position115 { get { return "Position115"; } }
        public static String Position116 { get { return "Position116"; } }
        public static String Position117 { get { return "Position117"; } }
        public static String Position118 { get { return "Position118"; } }
        public static String Position119 { get { return "Position119"; } }
        public static String Position120 { get { return "Position120"; } }
        public static String Position121 { get { return "Position121"; } }
        public static String Position122 { get { return "Position122"; } }
        public static String Position123 { get { return "Position123"; } }
        public static String Position124 { get { return "Position124"; } }
        public static String Position125 { get { return "Position125"; } }
        public static String Position126 { get { return "Position126"; } }
        public static String Position127 { get { return "Position127"; } }
        public static String Position128 { get { return "Position128"; } }
        public static String Position019 { get { return "Position129"; } }
        public static String Position130 { get { return "Position130"; } }
        public static String Position131 { get { return "Position131"; } }
        public static String Position132 { get { return "Position132"; } }
        public static String Position133 { get { return "Position133"; } }
        public static String Position134 { get { return "Position134"; } }
        public static String Position135 { get { return "Position135"; } }
        public static String Position136 { get { return "Position136"; } }
        public static String Position137 { get { return "Position137"; } }
        public static String Position138 { get { return "Position138"; } }
        public static String Position139 { get { return "Position139"; } }
        public static String Position140 { get { return "Position140"; } }
        public static String Position141 { get { return "Position141"; } }
        public static String Position142 { get { return "Position142"; } }
        public static String Position143 { get { return "Position143"; } }
        public static String Position144 { get { return "Position144"; } }
        public static String Position145 { get { return "Position145"; } }
        public static String Position146 { get { return "Position146"; } }
        public static String Position147 { get { return "Position147"; } }
        public static String PositionUseTargetSpaceX { get { return "Position148"; } }
        public static String PositionUseTargetSpaceY { get { return "Position149"; } }
        public static String PositionUseTargetSpaceZ { get { return "Position150"; } }
        public static String PositionTargetSpaceX { get { return "Position151"; } }
        public static String PositionTargetSpaceY { get { return "Position152"; } }
        public static String PositionTargetSpaceZ { get { return "Position153"; } }
        public static String PositionLocationId { get { return "Position154"; } }
        public static String PositionBitmapIdOverride { get { return "Position159"; } }
        public static String PositionHideIfPrinting { get { return "Position160"; } }
        public static String PositionBitmapIdOverrideUnit { get { return "Position162"; } }
        public static String PositionAutomaticModel { get { return "Position163"; } }
        public static String PositionMerchXIsTrayBroken { get { return "Position164"; } }
        public static String PositionMerchYIsTrayBroken { get { return "Position165"; } }
        public static String PositionMerchZIsTrayBroken { get { return "Position166"; } }

        #endregion

        #region Drawing Field Indexes

        public static String DrawingType { get { return "Drawing001"; } } // 0=Arc, 1=Ellipse, 2=Line, 3=Polygon, 4=Rectangle, 5=Text
        public static String DrawingName { get { return "Drawing002"; } }
        public static String DrawingKey { get { return "Drawing003"; } }
        public static String DrawingX { get { return "Drawing004"; } }
        public static String DrawingWidth { get { return "Drawing005"; } }
        public static String DrawingY { get { return "Drawing006"; } }
        public static String DrawingHeight { get { return "Drawing007"; } }
        public static String DrawingZ { get { return "Drawing008"; } }
        public static String DrawingDepth { get { return "Drawing009"; } }
        public static String DrawingColor { get { return "Drawing010"; } }
        public static String DrawingBackFill { get { return "Drawing011"; } }
        public static String DrawingBackColor { get { return "Drawing012"; } }
        public static String DrawingViewCreatedIn { get { return "Drawing013"; } } // 0=Side, 1=Top, 2=Front
        public static String DrawingShowInAllViews { get { return "Drawing014"; } }
        public static String DrawingWordWrap { get { return "Drawing015"; } } // 0=No, 1=Yes
        public static String DrawingCircular { get { return "Drawing016"; } }
        public static String DrawingStartX { get { return "Drawing017"; } }
        public static String DrawingStartY { get { return "Drawing018"; } }
        public static String DrawingStartZ { get { return "Drawing019"; } }
        public static String DrawingEndX { get { return "Drawing020"; } }
        public static String DrawingEndY { get { return "Drawing021"; } }
        public static String DrawingEndZ { get { return "Drawing022"; } }
        public static String DrawingString { get { return "Drawing023"; } }
        public static String DrawingScale { get { return "Drawing024"; } } // 0=Scaled, 1=Fixed, 2=Sized
        public static String DrawingOutline { get { return "Drawing025"; } } // 0=None, 1=Rectangle, 2=Elipse
        public static String DrawingCallout { get { return "Drawing026"; } } // 0=None, 1=Line, 2=Arrow
        public static String DrawingFontHeight { get { return "Drawing027"; } }
        public static String DrawingFontWidth { get { return "Drawing028"; } }
        public static String DrawingFontEscapement { get { return "Drawing029"; } }
        public static String DrawingFontOrientation { get { return "Drawing030"; } }
        public static String DrawingFontWeight { get { return "Drawing031"; } }
        public static String DrawingFontItalic { get { return "Drawing032"; } }
        public static String DrawingFontUnderline { get { return "Drawing033"; } }
        public static String DrawingFontStrikeOut { get { return "Drawing034"; } }
        public static String DrawingFontCharSet { get { return "Drawing035"; } }
        public static String DrawingFontOutPrecision { get { return "Drawing036"; } }
        public static String DrawingFontClipPrecision { get { return "Drawing037"; } }
        public static String DrawingFontQuality { get { return "Drawing038"; } }
        public static String DrawingFontPitchAndFamily { get { return "Drawing039"; } }
        public static String DrawingFontFaceName { get { return "Drawing040"; } }
        public static String DrawingCalloutX { get { return "Drawing041"; } } // Source coordinates for the callout line.
        public static String DrawingCalloutY { get { return "Drawing042"; } } // Source coordinates for the callout line.
        public static String DrawingCalloutZ { get { return "Drawing043"; } } // Source coordinates for the callout line.
        public static String DrawingField044 { get { return "Drawing044"; } }
        public static String DrawingCenterText { get { return "Drawing045"; } }
        public static String DrawingHideIfPrinting { get { return "Drawing046"; } }

        #endregion

        #region Performance Field Indexes

        public static String PerformanceUpc { get { return "Performance001"; } }
        public static String PerformanceId { get { return "Performance002"; } }
        public static String PerformanceKey { get { return "Performance003"; } }
        public static String PerformancePrice { get { return "Performance004"; } }
        public static String PerformanceCaseCost { get { return "Performance005"; } }
        public static String PerformanceTaxCode { get { return "Performance006"; } }
        public static String PerformanceUnitMovement { get { return "Performance007"; } }
        public static String PerformanceShare { get { return "Performance008"; } }
        public static String PerformancePerformanceIndex { get { return "Performance009"; } }
        public static String PerformanceDescription01 { get { return "Performance010"; } }
        public static String PerformanceDescription02 { get { return "Performance011"; } }
        public static String PerformanceDescription03 { get { return "Performance012"; } }
        public static String PerformanceDescription04 { get { return "Performance013"; } }
        public static String PerformanceDescription05 { get { return "Performance014"; } }
        public static String PerformanceDescription06 { get { return "Performance015"; } }
        public static String PerformanceDescription07 { get { return "Performance016"; } }
        public static String PerformanceDescription08 { get { return "Performance017"; } }
        public static String PerformanceDescription09 { get { return "Performance018"; } }
        public static String PerformanceDescription10 { get { return "Performance019"; } }
        public static String PerformanceValue01 { get { return "Performance020"; } }
        public static String PerformanceValue02 { get { return "Performance021"; } }
        public static String PerformanceValue03 { get { return "Performance022"; } }
        public static String PerformanceValue04 { get { return "Performance023"; } }
        public static String PerformanceValue05 { get { return "Performance024"; } }
        public static String PerformanceValue06 { get { return "Performance025"; } }
        public static String PerformanceValue07 { get { return "Performance026"; } }
        public static String PerformanceValue08 { get { return "Performance027"; } }
        public static String PerformanceValue09 { get { return "Performance028"; } }
        public static String PerformanceValue10 { get { return "Performance029"; } }
        public static String PerformanceFlag01 { get { return "Performance030"; } }
        public static String PerformanceFlag02 { get { return "Performance031"; } }
        public static String PerformanceFlag03 { get { return "Performance032"; } }
        public static String PerformanceFlag04 { get { return "Performance033"; } }
        public static String PerformanceFlag05 { get { return "Performance034"; } }
        public static String PerformanceFlag06 { get { return "Performance035"; } }
        public static String PerformanceFlag07 { get { return "Performance036"; } }
        public static String PerformanceFlag08 { get { return "Performance037"; } }
        public static String PerformanceFlag09 { get { return "Performance038"; } }
        public static String PerformanceFlag10 { get { return "Performance039"; } }
        public static String PerformanceFacings { get { return "Performance040"; } }
        public static String PerformanceDescription11 { get { return "Performance041"; } }
        public static String PerformanceDescription12 { get { return "Performance042"; } }
        public static String PerformanceDescription13 { get { return "Performance043"; } }
        public static String PerformanceDescription14 { get { return "Performance044"; } }
        public static String PerformanceDescription15 { get { return "Performance045"; } }
        public static String PerformanceDescription16 { get { return "Performance046"; } }
        public static String PerformanceDescription17 { get { return "Performance047"; } }
        public static String PerformanceDescription18 { get { return "Performance048"; } }
        public static String PerformanceDescription19 { get { return "Performance049"; } }
        public static String PerformanceDescription20 { get { return "Performance050"; } }
        public static String PerformanceDescription21 { get { return "Performance051"; } }
        public static String PerformanceDescription22 { get { return "Performance052"; } }
        public static String PerformanceDescription23 { get { return "Performance053"; } }
        public static String PerformanceDescription24 { get { return "Performance054"; } }
        public static String PerformanceDescription25 { get { return "Performance055"; } }
        public static String PerformanceDescription26 { get { return "Performance056"; } }
        public static String PerformanceDescription27 { get { return "Performance057"; } }
        public static String PerformanceDescription28 { get { return "Performance058"; } }
        public static String PerformanceDescription29 { get { return "Performance059"; } }
        public static String PerformanceDescription30 { get { return "Performance060"; } }
        public static String PerformanceValue11 { get { return "Performance061"; } }
        public static String PerformanceValue12 { get { return "Performance062"; } }
        public static String PerformanceValue13 { get { return "Performance063"; } }
        public static String PerformanceValue14 { get { return "Performance064"; } }
        public static String PerformanceValue15 { get { return "Performance065"; } }
        public static String PerformanceValue16 { get { return "Performance066"; } }
        public static String PerformanceValue17 { get { return "Performance067"; } }
        public static String PerformanceValue18 { get { return "Performance068"; } }
        public static String PerformanceValue19 { get { return "Performance069"; } }
        public static String PerformanceValue20 { get { return "Performance070"; } }
        public static String PerformanceValue21 { get { return "Performance071"; } }
        public static String PerformanceValue22 { get { return "Performance072"; } }
        public static String PerformanceValue23 { get { return "Performance073"; } }
        public static String PerformanceValue24 { get { return "Performance074"; } }
        public static String PerformanceValue25 { get { return "Performance075"; } }
        public static String PerformanceValue26 { get { return "Performance076"; } }
        public static String PerformanceValue27 { get { return "Performance077"; } }
        public static String PerformanceValue28 { get { return "Performance078"; } }
        public static String PerformanceValue29 { get { return "Performance079"; } }
        public static String PerformanceValue30 { get { return "Performance080"; } }
        public static String PerformanceCaseMultiple { get { return "Performance081"; } }
        public static String PerformanceDaysSupply { get { return "Performance082"; } }
        public static String PerformancePeakSafetyFactor { get { return "Performance083"; } }
        public static String PerformanceBackroomStock { get { return "Performance084"; } }
        public static String PerformanceMinimumUnits { get { return "Performance085"; } }
        public static String PerformanceMaximumUnits { get { return "Performance086"; } }
        public static String PerformanceDeliverySchedule { get { return "Performance087"; } }
        public static String PerformanceReplenishmentMin { get { return "Performance088"; } }
        public static String PerformanceReplenishmentMax { get { return "Performance089"; } }
        public static String PerformanceCpiRank { get { return "Performance090"; } }
        public static String PerformanceRecommendedFacings { get { return "Performance091"; } }
        public static String PerformanceAssortmentStrategy { get { return "Performance092"; } }
        public static String PerformanceAssortmentTactic { get { return "Performance093"; } }
        public static String PerformanceAssortmentReason { get { return "Performance094"; } }
        public static String PerformanceAssortmentAction { get { return "Performance095"; } }
        public static String PerformancePartId { get { return "Performance096"; } }
        public static String PerformanceClusterName { get { return "Performance097"; } }
        public static String PerformanceTargetDistributionStores { get { return "Performance098"; } }
        public static String PerformanceTargetDistribution { get { return "Performance099"; } }
        public static String PerformanceAssortmentNote { get { return "Performance100"; } }
        public static String PerformanceDescription31 { get { return "Performance101"; } }
        public static String PerformanceDescription32 { get { return "Performance102"; } }
        public static String PerformanceDescription33 { get { return "Performance103"; } }
        public static String PerformanceDescription34 { get { return "Performance104"; } }
        public static String PerformanceDescription35 { get { return "Performance105"; } }
        public static String PerformanceDescription36 { get { return "Performance106"; } }
        public static String PerformanceDescription37 { get { return "Performance107"; } }
        public static String PerformanceDescription38 { get { return "Performance108"; } }
        public static String PerformanceDescription39 { get { return "Performance109"; } }
        public static String PerformanceDescription40 { get { return "Performance110"; } }
        public static String PerformanceDescription41 { get { return "Performance111"; } }
        public static String PerformanceDescription42 { get { return "Performance112"; } }
        public static String PerformanceDescription43 { get { return "Performance113"; } }
        public static String PerformanceDescription44 { get { return "Performance114"; } }
        public static String PerformanceDescription45 { get { return "Performance115"; } }
        public static String PerformanceDescription46 { get { return "Performance116"; } }
        public static String PerformanceDescription47 { get { return "Performance117"; } }
        public static String PerformanceDescription48 { get { return "Performance118"; } }
        public static String PerformanceDescription49 { get { return "Performance119"; } }
        public static String PerformanceDescription50 { get { return "Performance120"; } }
        public static String PerformanceValue31 { get { return "Performance121"; } }
        public static String PerformanceValue32 { get { return "Performance122"; } }
        public static String PerformanceValue33 { get { return "Performance103"; } }
        public static String PerformanceValue34 { get { return "Performance124"; } }
        public static String PerformanceValue35 { get { return "Performance125"; } }
        public static String PerformanceValue36 { get { return "Performance126"; } }
        public static String PerformanceValue37 { get { return "Performance127"; } }
        public static String PerformanceValue38 { get { return "Performance128"; } }
        public static String PerformanceValue39 { get { return "Performance129"; } }
        public static String PerformanceValue40 { get { return "Performance120"; } }
        public static String PerformanceValue41 { get { return "Performance131"; } }
        public static String PerformanceValue42 { get { return "Performance132"; } }
        public static String PerformanceValue43 { get { return "Performance133"; } }
        public static String PerformanceValue44 { get { return "Performance134"; } }
        public static String PerformanceValue45 { get { return "Performance135"; } }
        public static String PerformanceValue46 { get { return "Performance136"; } }
        public static String PerformanceValue47 { get { return "Performance137"; } }
        public static String PerformanceValue48 { get { return "Performance138"; } }
        public static String PerformanceValue49 { get { return "Performance139"; } }
        public static String PerformanceValue50 { get { return "Performance140"; } }
        public static String PerformanceRecommendedOrientation { get { return "Performance141"; } }
        public static String PerformanceRecommendedMerchStyle { get { return "Performance142"; } }
        public static String PerformanceIgnoreRecommendations { get { return "Performance143"; } }
        public static String PerformanceNote1 { get { return "Performance144"; } }
        public static String PerformanceNote2 { get { return "Performance145"; } }
        public static String PerformanceNote3 { get { return "Performance146"; } }
        public static String PerformanceNote4 { get { return "Performance147"; } }
        public static String PerformanceNote5 { get { return "Performance148"; } }
        public static String PerformanceSales { get { return "PerformanceSales"; } }
        public static String PerformanceProfit { get { return "PerformanceProfit"; } }
        public static String PerformanceUnitCost { get { return "PerformanceUnitCost"; } }

        #endregion

        public static Dictionary<ProSpaceComponentType, Int32> DefaultFillColourByProSpaceComponentType
        {
            get
            {
                return _defaultFillColourByProSpaceComponentType ??
                       (_defaultFillColourByProSpaceComponentType =
                        new Dictionary<ProSpaceComponentType, Int32>
                        {
                            {ProSpaceComponentType.Shelf, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.Chest, SpacePlanningImportHelper.DefaultChestFillColour},
                            {ProSpaceComponentType.Bin, SpacePlanningImportHelper.DefaultChestFillColour},
                            {ProSpaceComponentType.PolygonalShelf, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.Rod, SpacePlanningImportHelper.DefaultRodFillColour},
                            {ProSpaceComponentType.LateralRod, SpacePlanningImportHelper.DefaultRodFillColour},
                            {ProSpaceComponentType.Bar, SpacePlanningImportHelper.DefaultBarFillColour},
                            {ProSpaceComponentType.Pegboard, SpacePlanningImportHelper.DefaultPegFillColour},
                            {ProSpaceComponentType.MultiRowPegboard, SpacePlanningImportHelper.DefaultPegFillColour},
                            {ProSpaceComponentType.CurvedRod, SpacePlanningImportHelper.DefaultRodFillColour},
                            {ProSpaceComponentType.Obstruction, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.Sign, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.GravityFeed, SpacePlanningImportHelper.DefaultShelfFillColour}
                        });
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Get the index from the field name.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public static Int32 GetIndex(String field)
        {
            String fieldIndexString = field.Substring(field.Length - 3);
            Int32 index;
            if (!Int32.TryParse(fieldIndexString, out index)) return -1;
            return index;
        }

        #region Type Converters

        /// <summary>
        ///     Converts a <see cref="ProSpaceFixtureCombineType" /> value to its corresponding
        ///     <see cref="PlanogramSubComponentCombineType" /> value.
        /// </summary>
        /// <param name="sourceValue">The source value to be converted.</param>
        /// <returns>The corresponding value in the new type.</returns>
        public static PlanogramSubComponentCombineType ToSubComponentCombineType(this ProSpaceFixtureCombineType sourceValue)
        {
            switch (sourceValue)
            {
                case ProSpaceFixtureCombineType.No:
                    return PlanogramSubComponentCombineType.None;
                case ProSpaceFixtureCombineType.Both:
                    return PlanogramSubComponentCombineType.Both;
                case ProSpaceFixtureCombineType.Right:
                    return PlanogramSubComponentCombineType.RightOnly;
                case ProSpaceFixtureCombineType.Left:
                    return PlanogramSubComponentCombineType.LeftOnly;
                default:
                    Debug.Fail("Unknown ProSpaceFixtureCombineType when calling DecodeCanCombine.");
                    break;
            }

            return PlanogramSubComponentCombineType.None;
        }

        /// <summary>
        ///     Converts a <see cref="PlanogramSubComponentCombineType" /> value to its corresponding
        ///     <see cref="ProSpaceFixtureCombineType" /> value.
        /// </summary>
        /// <param name="sourceValue">The source value to be converted.</param>
        /// <returns>The corresponding value in the new type.</returns>
        public static ProSpaceFixtureCombineType ToFixtureCombineType(this PlanogramSubComponentCombineType sourceValue)
        {
            switch (sourceValue)
            {
                case PlanogramSubComponentCombineType.None:
                    return ProSpaceFixtureCombineType.No;
                case PlanogramSubComponentCombineType.Both:
                    return ProSpaceFixtureCombineType.Both;
                case PlanogramSubComponentCombineType.RightOnly:
                    return ProSpaceFixtureCombineType.Right;
                case PlanogramSubComponentCombineType.LeftOnly:
                    return ProSpaceFixtureCombineType.Left;
                default:
                    Debug.Fail("Unknown PlanogramSubComponentCombineType when calling EncodeCanCombine.");
                    break;
            }

            return ProSpaceFixtureCombineType.No;
        }

        /// <summary>
        ///     Converts a <see cref="ProSpaceComponentType" /> value to its corresponding <see cref="PlanogramComponentType" />
        ///     value.
        /// </summary>
        /// <param name="sourceValue">The source value to be converted.</param>
        /// <returns>The corresponding value in the new type.</returns>
        public static PlanogramComponentType ToPlanogramComponentType(this ProSpaceComponentType sourceValue)
        {
            switch (sourceValue)
            {
                case ProSpaceComponentType.Shelf:
                    return PlanogramComponentType.Shelf;
                case ProSpaceComponentType.Chest:
                    return PlanogramComponentType.Chest;
                case ProSpaceComponentType.Bin:
                    return PlanogramComponentType.Chest;
                case ProSpaceComponentType.PolygonalShelf:
                    return PlanogramComponentType.Shelf;
                case ProSpaceComponentType.Rod:
                    return PlanogramComponentType.Rod;
                case ProSpaceComponentType.LateralRod:
                    return PlanogramComponentType.Rod;
                case ProSpaceComponentType.Bar:
                    return PlanogramComponentType.Bar;
                case ProSpaceComponentType.Pegboard:
                    return PlanogramComponentType.Peg;
                case ProSpaceComponentType.MultiRowPegboard:
                    return PlanogramComponentType.Peg;
                case ProSpaceComponentType.CurvedRod:
                    return PlanogramComponentType.Rod;
                case ProSpaceComponentType.Obstruction:
                    return PlanogramComponentType.Custom;
                case ProSpaceComponentType.Sign:
                    return PlanogramComponentType.Custom;
                case ProSpaceComponentType.GravityFeed:
                    return PlanogramComponentType.Custom;
                default:
                    Debug.Fail("Unknown ProSpace Component Type when calling ToPlanogramComponentType.");
                    break;
            }

            //  Return a defaul value if unable to map the source value.
            return PlanogramComponentType.Shelf;
        }

        public static PlanogramSubComponentFillPatternType ToPlanogramSubComponentFillPatternType(this ProSpaceFillPatternType source)
        {
            var patternType = PlanogramSubComponentFillPatternType.Solid;

            switch (source)
            {
                case ProSpaceFillPatternType.Solid:
                    patternType = PlanogramSubComponentFillPatternType.Solid;
                    break;
                case ProSpaceFillPatternType.DiagonalDown:
                case ProSpaceFillPatternType.DashDiagonalDown:
                case ProSpaceFillPatternType.LightDashDiagonalDown:
                case ProSpaceFillPatternType.StrongDashDiagonalDown:
                    patternType = PlanogramSubComponentFillPatternType.DiagonalDown;
                    break;
                case ProSpaceFillPatternType.DiagonalUp:
                case ProSpaceFillPatternType.DashDiagonalUp:
                case ProSpaceFillPatternType.LightDashDiagonalUp:
                case ProSpaceFillPatternType.StrongDashDiagonalUp:
                    patternType = PlanogramSubComponentFillPatternType.DiagonalUp;
                    break;
                case ProSpaceFillPatternType.Crosshatch:
                case ProSpaceFillPatternType.Reticle:
                case ProSpaceFillPatternType.Bricks:
                case ProSpaceFillPatternType.Scales:
                case ProSpaceFillPatternType.Chainmail:
                    patternType = PlanogramSubComponentFillPatternType.Crosshatch;
                    break;
                case ProSpaceFillPatternType.Border:
                    patternType = PlanogramSubComponentFillPatternType.Border;
                    break;
                case ProSpaceFillPatternType.Dots:
                case ProSpaceFillPatternType.Xes:
                case ProSpaceFillPatternType.Circles:
                case ProSpaceFillPatternType.Triangles:
                case ProSpaceFillPatternType.Squares:
                    patternType = PlanogramSubComponentFillPatternType.Dotted;
                    break;
                case ProSpaceFillPatternType.HorizontalWaves:
                case ProSpaceFillPatternType.HorizontalLines:
                case ProSpaceFillPatternType.Angles:
                case ProSpaceFillPatternType.HorizontalDashLanes:
                case ProSpaceFillPatternType.HorizontalDashLines:
                    patternType = PlanogramSubComponentFillPatternType.Horizontal;
                    break;
                case ProSpaceFillPatternType.VerticalWaves:
                case ProSpaceFillPatternType.VerticalLines:
                case ProSpaceFillPatternType.Ces:
                case ProSpaceFillPatternType.VerticalDashLanes:
                case ProSpaceFillPatternType.VerticalDashLines:
                    patternType = PlanogramSubComponentFillPatternType.Vertical;
                    break;
                default:
                    Debug.Fail("Unknown ProSpace Fill Pattern Type when calling ToPlanogramSubCoiComponentFillPatternType.");
                    break;
            }
            return patternType;
        }

        #endregion

        #region Field Info Initialization

        private static Dictionary<String, ObjectFieldInfo> InitializePlanogramFieldInfos()
        {
            var fieldInfos =
                new List<ObjectFieldInfo>
                {
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectPrimaryKey, Message.JdaFieldLabel_PrimaryKey),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectLayoutFile, Message.JdaFieldLabel_LayoutFile),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectMovementPeriod, Message.JdaFieldLabel_MovementPeriod),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDaysOfSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemandCycleLength, Message.JdaFieldLabel_DemandCycleLength),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectPeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectBackroomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectPegId, Message.JdaFieldLabel_PegId),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectMeasurement, Message.JdaFieldLabel_MeasurementUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchXDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_X)),
                    //{ProjectField022, NewObjectFieldInfo(ProjectField022, "Field 022")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchYDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Y)),
                    //{ProjectField031, NewObjectFieldInfo(ProjectField031, "Field 031")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectMerchZDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Z)),
                    //{ProjectField040, NewObjectFieldInfo(ProjectField040, "Field 040")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand01, String.Format(Message.JdaFieldLabel_Demand, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand02, String.Format(Message.JdaFieldLabel_Demand, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand03, String.Format(Message.JdaFieldLabel_Demand, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand04, String.Format(Message.JdaFieldLabel_Demand, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand05, String.Format(Message.JdaFieldLabel_Demand, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand06, String.Format(Message.JdaFieldLabel_Demand, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand07, String.Format(Message.JdaFieldLabel_Demand, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand08, String.Format(Message.JdaFieldLabel_Demand, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand09, String.Format(Message.JdaFieldLabel_Demand, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand10, String.Format(Message.JdaFieldLabel_Demand, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand11, String.Format(Message.JdaFieldLabel_Demand, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand12, String.Format(Message.JdaFieldLabel_Demand, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand13, String.Format(Message.JdaFieldLabel_Demand, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand14, String.Format(Message.JdaFieldLabel_Demand, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand15, String.Format(Message.JdaFieldLabel_Demand, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand16, String.Format(Message.JdaFieldLabel_Demand, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand17, String.Format(Message.JdaFieldLabel_Demand, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand18, String.Format(Message.JdaFieldLabel_Demand, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand20, String.Format(Message.JdaFieldLabel_Demand, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand21, String.Format(Message.JdaFieldLabel_Demand, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand22, String.Format(Message.JdaFieldLabel_Demand, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand23, String.Format(Message.JdaFieldLabel_Demand, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand24, String.Format(Message.JdaFieldLabel_Demand, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand25, String.Format(Message.JdaFieldLabel_Demand, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand26, String.Format(Message.JdaFieldLabel_Demand, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand27, String.Format(Message.JdaFieldLabel_Demand, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDemand28, String.Format(Message.JdaFieldLabel_Demand, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectInventoryModelManual, Message.JdaFieldLabel_InventoryModelManual),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectInventoryModelCaseMultiple,
                                       Message.JdaFieldLabel_InventoryModelCaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectInventoryModelDaysOfSupply,
                                       Message.JdaFieldLabel_InventoryModelDaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectInventoryModelPeak, Message.JdaFieldLabel_InventoryModelPeak),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectInventoryModelMinUnits, Message.JdaFieldLabel_InventoryModelMinUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectInventoryModelMaxUnits, Message.JdaFieldLabel_InventoryModelMaxUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue01, String.Format(Message.JdaFieldLabel_Value, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue02, String.Format(Message.JdaFieldLabel_Value, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue03, String.Format(Message.JdaFieldLabel_Value, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue04, String.Format(Message.JdaFieldLabel_Value, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue05, String.Format(Message.JdaFieldLabel_Value, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue06, String.Format(Message.JdaFieldLabel_Value, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue07, String.Format(Message.JdaFieldLabel_Value, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue08, String.Format(Message.JdaFieldLabel_Value, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue09, String.Format(Message.JdaFieldLabel_Value, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription01, String.Format(Message.JdaFieldLabel_Description, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription02, String.Format(Message.JdaFieldLabel_Description, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription03, String.Format(Message.JdaFieldLabel_Description, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription04, String.Format(Message.JdaFieldLabel_Description, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription05, String.Format(Message.JdaFieldLabel_Description, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription06, String.Format(Message.JdaFieldLabel_Description, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription07, String.Format(Message.JdaFieldLabel_Description, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription08, String.Format(Message.JdaFieldLabel_Description, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription09, String.Format(Message.JdaFieldLabel_Description, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription10, String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription11, String.Format(Message.JdaFieldLabel_Description, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription12, String.Format(Message.JdaFieldLabel_Description, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription13, String.Format(Message.JdaFieldLabel_Description, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription14, String.Format(Message.JdaFieldLabel_Description, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription15, String.Format(Message.JdaFieldLabel_Description, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription16, String.Format(Message.JdaFieldLabel_Description, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription17, String.Format(Message.JdaFieldLabel_Description, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription18, String.Format(Message.JdaFieldLabel_Description, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription19, String.Format(Message.JdaFieldLabel_Description, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription20, String.Format(Message.JdaFieldLabel_Description, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription21, String.Format(Message.JdaFieldLabel_Description, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription22, String.Format(Message.JdaFieldLabel_Description, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription23, String.Format(Message.JdaFieldLabel_Description, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription24, String.Format(Message.JdaFieldLabel_Description, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription25, String.Format(Message.JdaFieldLabel_Description, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription26, String.Format(Message.JdaFieldLabel_Description, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription27, String.Format(Message.JdaFieldLabel_Description, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription28, String.Format(Message.JdaFieldLabel_Description, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription29, String.Format(Message.JdaFieldLabel_Description, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription30, String.Format(Message.JdaFieldLabel_Description, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription31, String.Format(Message.JdaFieldLabel_Description, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription32, String.Format(Message.JdaFieldLabel_Description, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription33, String.Format(Message.JdaFieldLabel_Description, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription34, String.Format(Message.JdaFieldLabel_Description, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription35, String.Format(Message.JdaFieldLabel_Description, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription36, String.Format(Message.JdaFieldLabel_Description, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription37, String.Format(Message.JdaFieldLabel_Description, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription38, String.Format(Message.JdaFieldLabel_Description, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription39, String.Format(Message.JdaFieldLabel_Description, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription40, String.Format(Message.JdaFieldLabel_Description, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription41, String.Format(Message.JdaFieldLabel_Description, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription42, String.Format(Message.JdaFieldLabel_Description, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription43, String.Format(Message.JdaFieldLabel_Description, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription44, String.Format(Message.JdaFieldLabel_Description, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription45, String.Format(Message.JdaFieldLabel_Description, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription46, String.Format(Message.JdaFieldLabel_Description, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription47, String.Format(Message.JdaFieldLabel_Description, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription48, String.Format(Message.JdaFieldLabel_Description, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription49, String.Format(Message.JdaFieldLabel_Description, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDescription50, String.Format(Message.JdaFieldLabel_Description, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag01, String.Format(Message.JdaFieldLabel_Flag, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectNotes, Message.JdaFieldLabel_Notes),
                    //{ProjectField186, NewObjectFieldInfo(ProjectField186, "Field 186")},
                    //{ProjectField187, NewObjectFieldInfo(ProjectField187, "Field 187")},
                    //{ProjectField188, NewObjectFieldInfo(ProjectField188, "Field 188")},
                    //{ProjectField189, NewObjectFieldInfo(ProjectField189, "Field 189")},
                    //{ProjectField190, NewObjectFieldInfo(ProjectField190, "Field 190")},
                    //{ProjectField191, NewObjectFieldInfo(ProjectField191, "Field 191")},
                    //{ProjectField192, NewObjectFieldInfo(ProjectField192, "Field 192")},
                    //{ProjectField193, NewObjectFieldInfo(ProjectField193, "Field 193")},
                    //{ProjectField194, NewObjectFieldInfo(ProjectField194, "Field 194")},
                    //{ProjectField195, NewObjectFieldInfo(ProjectField195, "Field 195")},
                    //{ProjectField196, NewObjectFieldInfo(ProjectField196, "Field 196")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectUsePerformancePrice, Message.JdaFieldLabel_UsePerformancePrice),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectUsePerformanceCost, Message.JdaFieldLabel_UsePerformanceCost),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectUsePerformanceTaxCode, Message.JdaFieldLabel_UsePerformanceTaxCode),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectUsePerformanceMovement, Message.JdaFieldLabel_UsePerformanceMovement),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectStatus, Message.JdaFieldLabel_Status),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDateCreated, Message.JdaFieldLabel_DateCreated),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDateModified, Message.JdaFieldLabel_DateModified),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDatePending, Message.JdaFieldLabel_DatePending),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDateEffective, Message.JdaFieldLabel_DateEffective),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDateFinished, Message.JdaFieldLabel_DateFinished),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDate1, String.Format(Message.JdaFieldLabel_Date, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDate2, String.Format(Message.JdaFieldLabel_Date, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDate3, String.Format(Message.JdaFieldLabel_Date, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectCreatedBy, Message.JdaFieldLabel_CreatedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectModifiedBy, Message.JdaFieldLabel_ModifiedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProjectPlanogramSpecificInventory,
                                       Message.JdaFieldLabel_PlanogramSpecificInventory),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectNote1, String.Format(Message.JdaFieldLabel_Note, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectNote2, String.Format(Message.JdaFieldLabel_Note, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectNote3, String.Format(Message.JdaFieldLabel_Note, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectNote4, String.Format(Message.JdaFieldLabel_Note, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProjectNote5, String.Format(Message.JdaFieldLabel_Note, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramWidth, Message.JdaFieldLabel_Width),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramHeight, Message.JdaFieldLabel_Height),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDepth, Message.JdaFieldLabel_Depth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramColor, Message.JdaFieldLabel_Color),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramBackDepth, Message.JdaFieldLabel_BackDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDrawBack, Message.JdaFieldLabel_DrawBack),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramBaseWidth, Message.JdaFieldLabel_BaseWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramBaseHeight, Message.JdaFieldLabel_BaseHeight),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramBaseDepth, Message.JdaFieldLabel_BaseDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDrawBase, Message.JdaFieldLabel_DrawBase),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramBaseColor, Message.JdaFieldLabel_BaseColor),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDrawNotches, Message.JdaFieldLabel_DrawNotches),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNotchOffset, Message.JdaFieldLabel_NotchOffset),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNotchSpacing, Message.JdaFieldLabel_NotchSpacing),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDoubleNotches, Message.JdaFieldLabel_DoubleNotches),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNotchColor, Message.JdaFieldLabel_NotchColour),
                    //{PlanogramField019, NewObjectFieldInfo(PlanogramField019, "Field 019")},
                    //{PlanogramField020, NewObjectFieldInfo(PlanogramField020, "Field 020")},
                    //{PlanogramField021, NewObjectFieldInfo(PlanogramField021, "Field 021")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramTrafficFlow, Message.JdaFieldLabel_TrafficFlow),
                    //{PlanogramField023, NewObjectFieldInfo(PlanogramField023, "Field 023")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramShapeId, Message.JdaFieldLabel_ShapeId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramBitmapId, Message.JdaFieldLabel_BitmapId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchXDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_X)),
                    //{PlanogramField034, NewObjectFieldInfo(PlanogramField034, "Field 034")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchYDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Y)),
                    //{PlanogramField043, NewObjectFieldInfo(PlanogramField043, "Field 043")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramMerchZDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Z)),
                    //{PlanogramField052, NewObjectFieldInfo(PlanogramField052, "Field 052")},
                    //{PlanogramField053, NewObjectFieldInfo(PlanogramField053, "Field 053")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNumerOfStores, Message.JdaFieldLabel_NumberStores),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNotchWidth, Message.JdaFieldLabel_NotchWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription01,
                                       String.Format(Message.JdaFieldLabel_Description, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription02,
                                       String.Format(Message.JdaFieldLabel_Description, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription03,
                                       String.Format(Message.JdaFieldLabel_Description, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription04,
                                       String.Format(Message.JdaFieldLabel_Description, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription05,
                                       String.Format(Message.JdaFieldLabel_Description, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription06,
                                       String.Format(Message.JdaFieldLabel_Description, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription07,
                                       String.Format(Message.JdaFieldLabel_Description, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription08,
                                       String.Format(Message.JdaFieldLabel_Description, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription09,
                                       String.Format(Message.JdaFieldLabel_Description, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription10,
                                       String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription11,
                                       String.Format(Message.JdaFieldLabel_Description, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription12,
                                       String.Format(Message.JdaFieldLabel_Description, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription13,
                                       String.Format(Message.JdaFieldLabel_Description, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription14,
                                       String.Format(Message.JdaFieldLabel_Description, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription15,
                                       String.Format(Message.JdaFieldLabel_Description, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription16,
                                       String.Format(Message.JdaFieldLabel_Description, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription17,
                                       String.Format(Message.JdaFieldLabel_Description, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription18,
                                       String.Format(Message.JdaFieldLabel_Description, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription19,
                                       String.Format(Message.JdaFieldLabel_Description, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription20,
                                       String.Format(Message.JdaFieldLabel_Description, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription21,
                                       String.Format(Message.JdaFieldLabel_Description, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription22,
                                       String.Format(Message.JdaFieldLabel_Description, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription23,
                                       String.Format(Message.JdaFieldLabel_Description, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription24,
                                       String.Format(Message.JdaFieldLabel_Description, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription25,
                                       String.Format(Message.JdaFieldLabel_Description, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription26,
                                       String.Format(Message.JdaFieldLabel_Description, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription27,
                                       String.Format(Message.JdaFieldLabel_Description, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription28,
                                       String.Format(Message.JdaFieldLabel_Description, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription29,
                                       String.Format(Message.JdaFieldLabel_Description, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription30,
                                       String.Format(Message.JdaFieldLabel_Description, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription31,
                                       String.Format(Message.JdaFieldLabel_Description, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription32,
                                       String.Format(Message.JdaFieldLabel_Description, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription33,
                                       String.Format(Message.JdaFieldLabel_Description, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription34,
                                       String.Format(Message.JdaFieldLabel_Description, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription35,
                                       String.Format(Message.JdaFieldLabel_Description, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription36,
                                       String.Format(Message.JdaFieldLabel_Description, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription37,
                                       String.Format(Message.JdaFieldLabel_Description, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription38,
                                       String.Format(Message.JdaFieldLabel_Description, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription39,
                                       String.Format(Message.JdaFieldLabel_Description, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription40,
                                       String.Format(Message.JdaFieldLabel_Description, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription41,
                                       String.Format(Message.JdaFieldLabel_Description, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription42,
                                       String.Format(Message.JdaFieldLabel_Description, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription43,
                                       String.Format(Message.JdaFieldLabel_Description, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription44,
                                       String.Format(Message.JdaFieldLabel_Description, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription45,
                                       String.Format(Message.JdaFieldLabel_Description, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription46,
                                       String.Format(Message.JdaFieldLabel_Description, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription47,
                                       String.Format(Message.JdaFieldLabel_Description, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription48,
                                       String.Format(Message.JdaFieldLabel_Description, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription49,
                                       String.Format(Message.JdaFieldLabel_Description, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramDescription50,
                                       String.Format(Message.JdaFieldLabel_Description, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue01, String.Format(Message.JdaFieldLabel_Value, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue02, String.Format(Message.JdaFieldLabel_Value, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue03, String.Format(Message.JdaFieldLabel_Value, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue04, String.Format(Message.JdaFieldLabel_Value, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue05, String.Format(Message.JdaFieldLabel_Value, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue06, String.Format(Message.JdaFieldLabel_Value, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue07, String.Format(Message.JdaFieldLabel_Value, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue08, String.Format(Message.JdaFieldLabel_Value, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue09, String.Format(Message.JdaFieldLabel_Value, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag01, String.Format(Message.JdaFieldLabel_Flag, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag02, String.Format(Message.JdaFieldLabel_Flag, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag03, String.Format(Message.JdaFieldLabel_Flag, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag04, String.Format(Message.JdaFieldLabel_Flag, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag05, String.Format(Message.JdaFieldLabel_Flag, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag06, String.Format(Message.JdaFieldLabel_Flag, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag07, String.Format(Message.JdaFieldLabel_Flag, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag08, String.Format(Message.JdaFieldLabel_Flag, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag09, String.Format(Message.JdaFieldLabel_Flag, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFillPattern, Message.JdaFieldLabel_FillPattern),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramSegmentsToPrint, Message.JdaFieldLabel_SegmentsToPrint),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFileName, Message.JdaFieldLabel_FileName),
                    //{PlanogramField169, NewObjectFieldInfo(PlanogramField169, "Field 169")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramLayoutFileName, Message.JdaFieldLabel_LayoutFileName),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNotes, Message.JdaFieldLabel_Notes),
                    //{PlanogramField172, NewObjectFieldInfo(PlanogramField172, "Field 172")},
                    //{PlanogramField173, NewObjectFieldInfo(PlanogramField173, "Field 173")},
                    //{PlanogramField174, NewObjectFieldInfo(PlanogramField174, "Field 174")},
                    //{PlanogramField175, NewObjectFieldInfo(PlanogramField175, "Field 175")},
                    //{PlanogramField176, NewObjectFieldInfo(PlanogramField176, "Field 176")},
                    //{PlanogramField177, NewObjectFieldInfo(PlanogramField177, "Field 177")},
                    //{PlanogramField178, NewObjectFieldInfo(PlanogramField178, "Field 178")},
                    //{PlanogramField179, NewObjectFieldInfo(PlanogramField179, "Field 179")},
                    //{PlanogramField180, NewObjectFieldInfo(PlanogramField180, "Field 180")},
                    //{PlanogramField181, NewObjectFieldInfo(PlanogramField181, "Field 181")},
                    //{PlanogramField182, NewObjectFieldInfo(PlanogramField182, "Field 182")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramStatus1, String.Format(Message.JdaFieldLabel_StatusN, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramStatus2, String.Format(Message.JdaFieldLabel_StatusN, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramStatus3, String.Format(Message.JdaFieldLabel_StatusN, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDateCreated, Message.JdaFieldLabel_DateCreated),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDateModified, Message.JdaFieldLabel_DateModified),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDatePending, Message.JdaFieldLabel_DatePending),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDateEffective, Message.JdaFieldLabel_DateEffective),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDateFinished, Message.JdaFieldLabel_DateFinished),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDate1, String.Format(Message.JdaFieldLabel_Date, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDate2, String.Format(Message.JdaFieldLabel_Date, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDate3, String.Format(Message.JdaFieldLabel_Date, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramCreatedBy, Message.JdaFieldLabel_CreatedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramModifiedBy, Message.JdaFieldLabel_ModifiedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFloorBitmapId, Message.JdaFieldLabel_FloorBitmapId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDoorTransparency, Message.JdaFieldLabel_DoorTransparency),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFloorTileWidth, Message.JdaFieldLabel_FloorTileWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramFloorTileDepth, Message.JdaFieldLabel_FloorTileDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramInventoryManual, Message.JdaFieldLabel_InventoryModelManual),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramInventoryCaseMultiple,
                                       Message.JdaFieldLabel_InventoryModelCaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramInventoryDaysSupply,
                                       Message.JdaFieldLabel_InventoryModelDaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramInventoryPeak, Message.JdaFieldLabel_InventoryModelPeak),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramInventoryMinUnits,
                                       Message.JdaFieldLabel_InventoryModelMinUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       PlanogramInventoryMaxUnits,
                                       Message.JdaFieldLabel_InventoryModelMaxUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDaysSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemandCycleLength, Message.JdaFieldLabel_DemandCycleLength),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramPeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramBackroomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemand01, String.Format(Message.JdaFieldLabel_Demand, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemand02, String.Format(Message.JdaFieldLabel_Demand, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemand03, String.Format(Message.JdaFieldLabel_Demand, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemand04, String.Format(Message.JdaFieldLabel_Demand, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemand05, String.Format(Message.JdaFieldLabel_Demand, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemand06, String.Format(Message.JdaFieldLabel_Demand, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDemand07, String.Format(Message.JdaFieldLabel_Demand, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramId, Message.JdaFieldLabel_Id),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramDepartment, Message.JdaFieldLabel_Department),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramPartId, Message.JdaFieldLabel_PartId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramGln, Message.JdaFieldLabel_Gln),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramPlanogramGuid, Message.JdaFieldLabel_PlanogramGuid),
                    //{PlanogramField224, NewObjectFieldInfo(PlanogramField224, "Field 224")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramAbbrevName, Message.JdaFieldLabel_AbbrevName),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramCategory, Message.JdaFieldLabel_Category),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramSubCategory, Message.JdaFieldLabel_SubCategory),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNote1,  String.Format(Message.JdaFieldLabel_Note, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNote2,  String.Format(Message.JdaFieldLabel_Note, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNote3,  String.Format(Message.JdaFieldLabel_Note, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNote4,  String.Format(Message.JdaFieldLabel_Note, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, PlanogramNote5,  String.Format(Message.JdaFieldLabel_Note, 5)),
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeComponentFieldInfos()
        {
            var fieldInfos = new List<ObjectFieldInfo>
                             {
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureType, Message.JdaFieldLabel_Type),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureName, Message.JdaFieldLabel_Name),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureKey, Message.JdaFieldLabel_Key),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureX, Message.JdaFieldLabel_X),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureWidth, Message.JdaFieldLabel_Width),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureY, Message.JdaFieldLabel_Y),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureHeight, Message.JdaFieldLabel_Height),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureZ, Message.JdaFieldLabel_Z),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureDepth, Message.JdaFieldLabel_Depth),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureSlope, Message.JdaFieldLabel_Slope),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureAngle, Message.JdaFieldLabel_Angle),
                                 //{Fixture012, NewObjectFieldInfo(Fixture012, "Field012")},
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureColour, Message.JdaFieldLabel_Color),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureAssembly, Message.JdaFieldLabel_Assembly),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureXSpacing,
                                                    String.Format(Message.JdaFieldLabel_AxisSpacing, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureYSpacing,
                                                    String.Format(Message.JdaFieldLabel_AxisSpacing, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureXStart,
                                                    String.Format(Message.JdaFieldLabel_AxisStart, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureYStart,
                                                    String.Format(Message.JdaFieldLabel_AxisStart, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureWallWidth, Message.JdaFieldLabel_WallWidth),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureWallHeight, Message.JdaFieldLabel_WallHeight),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureWallDepth, Message.JdaFieldLabel_WallDepth),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureCurve, Message.JdaFieldLabel_Curve),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureMerch, Message.JdaFieldLabel_Merch),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureCanObstruct, Message.JdaFieldLabel_CanObstruct),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDetectOtherFixtures,
                                                    Message.JdaFieldLabel_DetectOtherFixtures),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDetectPositionsOnOtherFixtures,
                                                    Message.JdaFieldLabel_DetectPositionsOnOtherFixtures),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureLeftOverhang, Message.JdaFieldLabel_LeftOverhang),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureRightOverhang, Message.JdaFieldLabel_RightOverhang),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureLowerOverhang, Message.JdaFieldLabel_LowerOverhang),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureUpperOverhang, Message.JdaFieldLabel_UpperOverhang),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureBackOverhang, Message.JdaFieldLabel_BackOverhang),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFrontOverhang, Message.JdaFieldLabel_FrontOverhang),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchandisingStyle,
                                                    Message.JdaFieldLabel_MerchandisingStyle),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureDividerWidth, Message.JdaFieldLabel_DividerWidth),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureDividerHeight, Message.JdaFieldLabel_DividerHeight),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureDividerDepth, Message.JdaFieldLabel_DividerDepth),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureCanCombine, Message.JdaFieldLabel_CanCombine),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureGrilleHeight, Message.JdaFieldLabel_GrilleHeight),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureNotchOffset, Message.JdaFieldLabel_NotchOffset),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureXSpacing2, Message.JdaFieldLabel_XSpacing2),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureXStart2, Message.JdaFieldLabel_XStart2),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixturePegDrop, Message.JdaFieldLabel_PegDrop),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixturePegGapX,
                                                    String.Format(Message.JdaFieldLabel_PegCap, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixturePegGapY,
                                                    String.Format(Message.JdaFieldLabel_PegCap, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixturePrimaryLabelFormatName,
                                                    Message.JdaFieldLabel_PrimaryLabelFormat),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureSecondaryLabelFormatName,
                                                    Message.JdaFieldLabel_SecondaryLabelFormat),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureShapeId, Message.JdaFieldLabel_ShapeId),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureBitmapId, Message.JdaFieldLabel_BitmapId),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXMin,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXMax,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXUprights,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXCaps,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXPlacement,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXNumber,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXSize,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchXDirection,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_X)),
                                 //{FixtureField057, NewObjectFieldInfo(FixtureField057, "Field057")},
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYMin,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYMax,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYUprights,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYCaps,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYPlacement,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYNumber,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYSize,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchYDirection,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Y)),
                                 //{FixtureField066, NewObjectFieldInfo(FixtureField066, "Field066")},
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZMin,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZMax,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZUprights,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZCaps,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZPlacement,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZNumber,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZSize,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureMerchZDirection,
                                                    String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Z)),
                                 //{FixtureField075, NewObjectFieldInfo(FixtureField075, "Field075")},
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription01,
                                                    String.Format(Message.JdaFieldLabel_Description, 01)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription02,
                                                    String.Format(Message.JdaFieldLabel_Description, 02)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription03,
                                                    String.Format(Message.JdaFieldLabel_Description, 03)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription04,
                                                    String.Format(Message.JdaFieldLabel_Description, 04)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription05,
                                                    String.Format(Message.JdaFieldLabel_Description, 05)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription06,
                                                    String.Format(Message.JdaFieldLabel_Description, 06)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription07,
                                                    String.Format(Message.JdaFieldLabel_Description, 07)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription08,
                                                    String.Format(Message.JdaFieldLabel_Description, 08)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription09,
                                                    String.Format(Message.JdaFieldLabel_Description, 09)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription10,
                                                    String.Format(Message.JdaFieldLabel_Description, 10)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription11,
                                                    String.Format(Message.JdaFieldLabel_Description, 11)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription12,
                                                    String.Format(Message.JdaFieldLabel_Description, 12)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription13,
                                                    String.Format(Message.JdaFieldLabel_Description, 13)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription14,
                                                    String.Format(Message.JdaFieldLabel_Description, 14)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription15,
                                                    String.Format(Message.JdaFieldLabel_Description, 15)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription16,
                                                    String.Format(Message.JdaFieldLabel_Description, 16)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription17,
                                                    String.Format(Message.JdaFieldLabel_Description, 17)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription18,
                                                    String.Format(Message.JdaFieldLabel_Description, 18)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription19,
                                                    String.Format(Message.JdaFieldLabel_Description, 19)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription20,
                                                    String.Format(Message.JdaFieldLabel_Description, 20)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription21,
                                                    String.Format(Message.JdaFieldLabel_Description, 21)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription22,
                                                    String.Format(Message.JdaFieldLabel_Description, 22)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription23,
                                                    String.Format(Message.JdaFieldLabel_Description, 23)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription24,
                                                    String.Format(Message.JdaFieldLabel_Description, 24)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription25,
                                                    String.Format(Message.JdaFieldLabel_Description, 25)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription26,
                                                    String.Format(Message.JdaFieldLabel_Description, 26)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription27,
                                                    String.Format(Message.JdaFieldLabel_Description, 27)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription28,
                                                    String.Format(Message.JdaFieldLabel_Description, 28)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription29,
                                                    String.Format(Message.JdaFieldLabel_Description, 29)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDescription30,
                                                    String.Format(Message.JdaFieldLabel_Description, 30)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue01, String.Format(Message.JdaFieldLabel_Value, 01)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue02, String.Format(Message.JdaFieldLabel_Value, 02)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue03, String.Format(Message.JdaFieldLabel_Value, 03)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue04, String.Format(Message.JdaFieldLabel_Value, 04)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue05, String.Format(Message.JdaFieldLabel_Value, 05)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue06, String.Format(Message.JdaFieldLabel_Value, 06)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue07, String.Format(Message.JdaFieldLabel_Value, 07)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue08, String.Format(Message.JdaFieldLabel_Value, 08)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue09, String.Format(Message.JdaFieldLabel_Value, 09)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag01, String.Format(Message.JdaFieldLabel_Flag, 01)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixturePositionId, Message.JdaFieldLabel_PositionId),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureFillPattern, Message.JdaFieldLabel_FillPattern),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureModelFilename, Message.JdaFieldLabel_ModelFileName),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureWeightCapacity, Message.JdaFieldLabel_WeightCapacity),
                                 //{FixtureField150, NewObjectFieldInfo(FixtureField150, "Field150")},
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureDividerAtStart, Message.JdaFieldLabel_DividerStartAt),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureDividerAtEnd, Message.JdaFieldLabel_DividerAtEnd),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureDividerBetweenFacings,
                                                    Message.JdaFieldLabel_DividerBetweenFacings),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureTransparency, Message.JdaFieldLabel_Transparency),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureHideIfPrinting, Message.JdaFieldLabel_HideIfPrinting),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureProductAssociation,
                                                    Message.JdaFieldLabel_ProductAssociation),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixturePartId, Message.JdaFieldLabel_PartId),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                                    FixtureHideViewDimensions,
                                                    Message.JdaFieldLabel_HideViewDimensions),
                                 NewObjectFieldInfo(Message.JdaFileSection_Fixture, FixtureGln, Message.JdaFieldLabel_Gln)
                             };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeFixtureFieldInfos()
        {
            var fieldInfos = new List<ObjectFieldInfo>
                             {
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentName, Message.JdaFieldLabel_Name),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentKey, Message.JdaFieldLabel_Key),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentX, Message.JdaFieldLabel_X),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentWidth, Message.JdaFieldLabel_Width),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentY, Message.JdaFieldLabel_Y),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentHeight, Message.JdaFieldLabel_Height),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentZ, Message.JdaFieldLabel_Z),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentDepth, Message.JdaFieldLabel_Depth),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentAngle, Message.JdaFieldLabel_Angle),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentOffsetX,
                                                    String.Format(Message.JdaFieldLabel_Offset, Message.JdaFieldLabel_X)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentOffsetY,
                                                    String.Format(Message.JdaFieldLabel_Offset, Message.JdaFieldLabel_Y)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentDoor, Message.JdaFieldLabel_Door),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDoorDirection,
                                                    Message.JdaFieldLabel_DoorDirection),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription01,
                                                    String.Format(Message.JdaFieldLabel_Description, 01)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription02,
                                                    String.Format(Message.JdaFieldLabel_Description, 02)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription03,
                                                    String.Format(Message.JdaFieldLabel_Description, 03)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription04,
                                                    String.Format(Message.JdaFieldLabel_Description, 04)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription05,
                                                    String.Format(Message.JdaFieldLabel_Description, 05)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription06,
                                                    String.Format(Message.JdaFieldLabel_Description, 06)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription07,
                                                    String.Format(Message.JdaFieldLabel_Description, 07)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription08,
                                                    String.Format(Message.JdaFieldLabel_Description, 08)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription09,
                                                    String.Format(Message.JdaFieldLabel_Description, 09)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentDescription10,
                                                    String.Format(Message.JdaFieldLabel_Description, 10)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue01,
                                                    String.Format(Message.JdaFieldLabel_Value, 01)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue02,
                                                    String.Format(Message.JdaFieldLabel_Value, 02)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue03,
                                                    String.Format(Message.JdaFieldLabel_Value, 03)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue04,
                                                    String.Format(Message.JdaFieldLabel_Value, 04)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue05,
                                                    String.Format(Message.JdaFieldLabel_Value, 05)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue06,
                                                    String.Format(Message.JdaFieldLabel_Value, 06)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue07,
                                                    String.Format(Message.JdaFieldLabel_Value, 07)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue08,
                                                    String.Format(Message.JdaFieldLabel_Value, 08)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue09,
                                                    String.Format(Message.JdaFieldLabel_Value, 09)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentValue10,
                                                    String.Format(Message.JdaFieldLabel_Value, 10)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag01,
                                                    String.Format(Message.JdaFieldLabel_Flag, 01)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag02,
                                                    String.Format(Message.JdaFieldLabel_Flag, 02)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag03,
                                                    String.Format(Message.JdaFieldLabel_Flag, 03)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag04,
                                                    String.Format(Message.JdaFieldLabel_Flag, 04)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag05,
                                                    String.Format(Message.JdaFieldLabel_Flag, 05)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag06,
                                                    String.Format(Message.JdaFieldLabel_Flag, 06)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag07,
                                                    String.Format(Message.JdaFieldLabel_Flag, 07)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag08,
                                                    String.Format(Message.JdaFieldLabel_Flag, 08)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag09,
                                                    String.Format(Message.JdaFieldLabel_Flag, 09)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFlag10,
                                                    String.Format(Message.JdaFieldLabel_Flag, 10)),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFrameWidth,
                                                    Message.JdaFieldLabel_FrameWidth),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFrameHeight,
                                                    Message.JdaFieldLabel_FrameHeight),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentNumber, Message.JdaFieldLabel_Number),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFrameFillColor,
                                                    Message.JdaFieldLabel_FrameFillColour),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                                    SegmentFrameFillPattern,
                                                    Message.JdaFieldLabel_FrameFillPattern),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentPartId, Message.JdaFieldLabel_PartId),
                                 NewObjectFieldInfo(Message.JdaFileSection_Segment, SegmentGln, Message.JdaFieldLabel_Gln)
                             };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeProductFieldInfos()
        {
            var fieldInfos =
                new List<ObjectFieldInfo>
                {
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductUnitCost, Message.JdaFieldLabel_UnitCost),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductUpc, Message.JdaFieldLabel_Upc),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductId, Message.JdaFieldLabel_Id),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductWidth, Message.JdaFieldLabel_Width),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductHeight, Message.JdaFieldLabel_Height),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDepth, Message.JdaFieldLabel_Depth),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductColor, Message.JdaFieldLabel_Color),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAbbrevName, Message.JdaFieldLabel_AbbrevName),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductSize, Message.JdaFieldLabel_Size),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductUom, Message.JdaFieldLabel_Uom),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductManufacturer, Message.JdaFieldLabel_Manufacturer),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCategory, Message.JdaFieldLabel_Category),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductSupplier, Message.JdaFieldLabel_Supplier),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductInnerPack, Message.JdaFieldLabel_InnerPack),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductXNesting, String.Format(Message.JdaFieldLabel_AxisNesting, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductYNesting, String.Format(Message.JdaFieldLabel_AxisNesting, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductZNesting, String.Format(Message.JdaFieldLabel_AxisNesting, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPegholes, Message.JdaFieldLabel_Pegholes),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPegholeX, String.Format(Message.JdaFieldLabel_PegholeAxis, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPegholeY, String.Format(Message.JdaFieldLabel_PegholeAxis, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPegholeWidth, Message.JdaFieldLabel_PegholeWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPeghole2X, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_X, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPeghole2Y, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_Y, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPeghole2Width, String.Format(Message.JdaFieldLabel_ExtraPegholeWidth, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPeghole3X, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_X, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPeghole3Y, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_Y, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPeghole3Width, String.Format(Message.JdaFieldLabel_ExtraPegholeWidth, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPackageStyle, Message.PackageStyle),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPegId, Message.JdaFieldLabel_PegId),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFingerSpaceY, String.Format(Message.JdaFieldLabel_FingerSpaceAxis, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductJumbleFactor, Message.JdaFieldLabel_JumbleFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPrice, Message.JdaFieldLabel_Price),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseCost, Message.JdaFieldLabel_CaseCost),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTaxCode, Message.JdaFieldLabel_TaxCode),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductUnitMovement, Message.JdaFieldLabel_UnitMovement),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductShare, Message.JdaFieldLabel_Share),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDaysSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCombinedPerformanceIndex, Message.JdaFieldLabel_CombinedPerformance),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPegSpan, Message.JdaFieldLabel_PegSpan),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMinimumUnits, Message.JdaFieldLabel_MinimumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMaximumUnits, Message.JdaFieldLabel_MaximumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductShapeId, Message.JdaFieldLabel_ShapeId),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductBitmapIdOverrride, Message.JdaFieldLabel_BitmapIdFileOverride),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTrayMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCaseMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDisplayMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAlternateMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductLooseMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchXMin, String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchXMax, String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchXUprights, String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchXCaps, String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchXPlacement, String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchXNumber, String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchXSize, String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                    //{ProductField093, NewObjectFieldInfo(ProductField093, "Field 093")},
                    //{ProductField094, NewObjectFieldInfo(ProductField094, "Field 094")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchYMin, String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchYMax, String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchYUprights, String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchYCaps, String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchYPlacement, String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchYNumber, String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchYSize, String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                    //{ProductField102, NewObjectFieldInfo(ProductField102, "Field 102")},
                    //{ProductField103, NewObjectFieldInfo(ProductField103, "Field 103")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchZMin, String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)), 
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchZMax, String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchZUprights, String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchZCaps, String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchZPlacement, String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchZNumber, String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMerchZSize, String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                    //{ProductField111, NewObjectFieldInfo(ProductField111, "Field 111")},
                    //{ProductField112, NewObjectFieldInfo(ProductField112, "Field 112")},
                    //{ProductField113, NewObjectFieldInfo(ProductField113, "Field 113")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription01, String.Format(Message.JdaFieldLabel_Description, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription02, String.Format(Message.JdaFieldLabel_Description, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription03, String.Format(Message.JdaFieldLabel_Description, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription04, String.Format(Message.JdaFieldLabel_Description, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription05, String.Format(Message.JdaFieldLabel_Description, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription06, String.Format(Message.JdaFieldLabel_Description, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription07, String.Format(Message.JdaFieldLabel_Description, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription08, String.Format(Message.JdaFieldLabel_Description, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription09, String.Format(Message.JdaFieldLabel_Description, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription10, String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription11, String.Format(Message.JdaFieldLabel_Description, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription12, String.Format(Message.JdaFieldLabel_Description, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription13, String.Format(Message.JdaFieldLabel_Description, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription14, String.Format(Message.JdaFieldLabel_Description, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription15, String.Format(Message.JdaFieldLabel_Description, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription16, String.Format(Message.JdaFieldLabel_Description, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription17, String.Format(Message.JdaFieldLabel_Description, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription18, String.Format(Message.JdaFieldLabel_Description, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription19, String.Format(Message.JdaFieldLabel_Description, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription20, String.Format(Message.JdaFieldLabel_Description, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription21, String.Format(Message.JdaFieldLabel_Description, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription22, String.Format(Message.JdaFieldLabel_Description, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription23, String.Format(Message.JdaFieldLabel_Description, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription24, String.Format(Message.JdaFieldLabel_Description, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription25, String.Format(Message.JdaFieldLabel_Description, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription26, String.Format(Message.JdaFieldLabel_Description, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription27, String.Format(Message.JdaFieldLabel_Description, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription28, String.Format(Message.JdaFieldLabel_Description, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription29, String.Format(Message.JdaFieldLabel_Description, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription30, String.Format(Message.JdaFieldLabel_Description, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription31, String.Format(Message.JdaFieldLabel_Description, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription32, String.Format(Message.JdaFieldLabel_Description, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription33, String.Format(Message.JdaFieldLabel_Description, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription34, String.Format(Message.JdaFieldLabel_Description, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription35, String.Format(Message.JdaFieldLabel_Description, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription36, String.Format(Message.JdaFieldLabel_Description, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription37, String.Format(Message.JdaFieldLabel_Description, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription38, String.Format(Message.JdaFieldLabel_Description, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription39, String.Format(Message.JdaFieldLabel_Description, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription40, String.Format(Message.JdaFieldLabel_Description, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription41, String.Format(Message.JdaFieldLabel_Description, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription42, String.Format(Message.JdaFieldLabel_Description, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription43, String.Format(Message.JdaFieldLabel_Description, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription44, String.Format(Message.JdaFieldLabel_Description, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription45, String.Format(Message.JdaFieldLabel_Description, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription46, String.Format(Message.JdaFieldLabel_Description, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription47, String.Format(Message.JdaFieldLabel_Description, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription48, String.Format(Message.JdaFieldLabel_Description, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription49, String.Format(Message.JdaFieldLabel_Description, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDescription50, String.Format(Message.JdaFieldLabel_Description, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue01, String.Format(Message.JdaFieldLabel_Value, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue02, String.Format(Message.JdaFieldLabel_Value, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue03, String.Format(Message.JdaFieldLabel_Value, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue04, String.Format(Message.JdaFieldLabel_Value, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue05, String.Format(Message.JdaFieldLabel_Value, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue06, String.Format(Message.JdaFieldLabel_Value, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue07, String.Format(Message.JdaFieldLabel_Value, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue08, String.Format(Message.JdaFieldLabel_Value, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue09, String.Format(Message.JdaFieldLabel_Value, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag01, String.Format(Message.JdaFieldLabel_Flag, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMinimumSqueezeFactorX, String.Format(Message.JdaFieldLabel_MinimumSqueezeFactor, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMinimumSqueezeFactorY, String.Format(Message.JdaFieldLabel_MinimumSqueezeFactor, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMinimumSqueezeFactorZ, String.Format(Message.JdaFieldLabel_MinimumSqueezeFactor, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMaximumSqueezeFactorX, String.Format(Message.JdaFieldLabel_MaximumSqueezeFactor, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMaximumSqueezeFactorY, String.Format(Message.JdaFieldLabel_MaximumSqueezeFactor, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductMaximumSqueezeFactorZ, String.Format(Message.JdaFieldLabel_MaximumSqueezeFactor, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFillPattern, Message.JdaFieldLabel_FillPattern),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFieldModelFilename, Message.JdaFieldLabel_ModelFileName),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductBrand, Message.JdaFieldLabel_Brand),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductSubCategory, Message.JdaFieldLabel_SubCategory),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductWeight, Message.JdaFieldLabel_Weight),
                    //{ProductField235, NewObjectFieldInfo(ProductField235, "Field 235")},
                    //{ProductField236, NewObjectFieldInfo(ProductField236, "Field 236")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFrontOverhang, Message.JdaFieldLabel_FrontOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductFingerSpaceX, String.Format(Message.JdaFieldLabel_FingerSpaceAxis, Message.JdaFieldLabel_X)),
                    //{ProductField239, NewObjectFieldInfo(ProductField239, "Field 239")},
                    //{ProductField240, NewObjectFieldInfo(ProductField240, "Field 240")},
                    //{ProductField241, NewObjectFieldInfo(ProductField241, "Field 241")},
                    //{ProductField242, NewObjectFieldInfo(ProductField242, "Field 242")},
                    //{ProductField243, NewObjectFieldInfo(ProductField243, "Field 243")},
                    //{ProductField244, NewObjectFieldInfo(ProductField244, "Field 244")},
                    //{ProductField245, NewObjectFieldInfo(ProductField245, "Field 245")},
                    //{ProductField246, NewObjectFieldInfo(ProductField246, "Field 246")},
                    //{ProductField247, NewObjectFieldInfo(ProductField247, "Field 247")},
                    //{ProductField248, NewObjectFieldInfo(ProductField248, "Field 248")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductStatus, Message.JdaFieldLabel_Status),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDateCreated, Message.JdaFieldLabel_DateCreated),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDateModified, Message.JdaFieldLabel_DateModified),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDatePending, Message.JdaFieldLabel_DatePending),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDateEffective, Message.JdaFieldLabel_DateEffective),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDateFinished, Message.JdaFieldLabel_DateFinished),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDate1, String.Format(Message.JdaFieldLabel_Date, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDate2, String.Format(Message.JdaFieldLabel_Date, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDate3, String.Format(Message.JdaFieldLabel_Date, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductCreatedBy, Message.JdaFieldLabel_CreatedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductModifiedBy, Message.JdaFieldLabel_ModifiedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductTransparency, Message.JdaFieldLabel_Transparency),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductBackRoomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductPartId, Message.JdaFieldLabel_PartId),
                    //{ProductField265, NewObjectFieldInfo(ProductField265, "Field 265")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductBitmapIdOverrideUnit, Message.JdaFieldLabel_BitmapIdOverrideUnit),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductModelFilenameLookup, Message.JdaFieldLabel_ModelFilenameLookup),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductDefaultMerchStyle, Message.JdaFieldLabel_MerchStyle),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductAutomaticModel, Message.JdaFieldLabel_AutomaticModel),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductNote1, String.Format(Message.JdaFieldLabel_Note, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductNote2, String.Format(Message.JdaFieldLabel_Note, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductNote3, String.Format(Message.JdaFieldLabel_Note, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductNote4, String.Format(Message.JdaFieldLabel_Note, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProductNote5, String.Format(Message.JdaFieldLabel_Note, 5))
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializePerformanceFieldInfos()
        {
            var fieldInfos =
                new List<ObjectFieldInfo>
                {
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceSales, Message.JdaFieldLabel_Sales),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceProfit, Message.JdaFieldLabel_Profit),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceTaxCode, Message.JdaFieldLabel_TaxCode),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceUnitMovement, Message.JdaFieldLabel_UnitMovement),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceShare, Message.JdaFieldLabel_Share),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformancePerformanceIndex, Message.JdaFieldLabel_PerformanceIndex),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue01, String.Format(Message.JdaFieldLabel_Value, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue02, String.Format(Message.JdaFieldLabel_Value, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue03, String.Format(Message.JdaFieldLabel_Value, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue04, String.Format(Message.JdaFieldLabel_Value, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue05, String.Format(Message.JdaFieldLabel_Value, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue06, String.Format(Message.JdaFieldLabel_Value, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue07, String.Format(Message.JdaFieldLabel_Value, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue08, String.Format(Message.JdaFieldLabel_Value, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue09, String.Format(Message.JdaFieldLabel_Value, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag01, String.Format(Message.JdaFieldLabel_Flag, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceFacings, Message.JdaFieldLabel_Facings),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceDaysSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformancePeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceBackroomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceMinimumUnits, Message.JdaFieldLabel_MinimumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceMaximumUnits, Message.JdaFieldLabel_MaximumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceReplenishmentMin, Message.JdaFieldLabel_ReplenishmentMin),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceReplenishmentMax, Message.JdaFieldLabel_ReplenishmentMax),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceCpiRank, Message.JdaFieldLabel_CpiRank),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceRecommendedFacings, Message.JdaFieldLabel_RecommendedFacings),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceAssortmentStrategy, Message.JdaFieldLabel_AssortmentStrategy),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceAssortmentTactic, Message.JdaFieldLabel_AssortmentTactic),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceAssortmentReason, Message.JdaFieldLabel_AssortmentReason),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceAssortmentAction, Message.JdaFieldLabel_AssortmentAction),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceTargetDistributionStores, Message.JdaFieldLabel_TargetDistributionStores),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceTargetDistribution, Message.JdaFieldLabel_TargetDistribution),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, PerformanceValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        #endregion

        #region Default Enumerations

        public static IEnumerable<Tuple<String, String>> EnumerateDefaultMappings(PlanogramFieldMappingType value)
        {
            switch (value)
            {
                case PlanogramFieldMappingType.Planogram:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultPlanogramMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Fixture:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultFixtureMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Component:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultComponentMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Product:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultProductMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Performance:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultPerformanceMappings())
                        yield return mapping;
                    yield break;
                default:
                    Debug.Fail("Unknown PlanogramFieldMappingType when calling EnumerateDefaultMappings in ProSpaceImportHelper.");
                    yield break;
            }
        }

        /// <summary>
        ///     Enumerate the default Planogram Mappings.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Tuple<String, String>> EnumerateDefaultPlanogramMappings()
        {
            //yield return CreateMappingTuple(Planogram.NameProperty.Name, PlanogramName);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text1Property.Name, PlanogramDescription01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text2Property.Name, PlanogramDescription02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text3Property.Name, PlanogramDescription03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text4Property.Name, PlanogramDescription04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text5Property.Name, PlanogramDescription05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text6Property.Name, PlanogramDescription06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text7Property.Name, PlanogramDescription07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text8Property.Name, PlanogramDescription08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text9Property.Name, PlanogramDescription09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text10Property.Name, PlanogramDescription10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text11Property.Name, PlanogramDescription11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text12Property.Name, PlanogramDescription12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text13Property.Name, PlanogramDescription13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text14Property.Name, PlanogramDescription14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text15Property.Name, PlanogramDescription15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text16Property.Name, PlanogramDescription16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text17Property.Name, PlanogramDescription17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text18Property.Name, PlanogramDescription18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text19Property.Name, PlanogramDescription19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text20Property.Name, PlanogramDescription20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text21Property.Name, PlanogramDescription21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text22Property.Name, PlanogramDescription22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text23Property.Name, PlanogramDescription23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text24Property.Name, PlanogramDescription24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text25Property.Name, PlanogramDescription25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text26Property.Name, PlanogramDescription26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text27Property.Name, PlanogramDescription27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text28Property.Name, PlanogramDescription28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text29Property.Name, PlanogramDescription29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text30Property.Name, PlanogramDescription30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text31Property.Name, PlanogramDescription31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text32Property.Name, PlanogramDescription32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text33Property.Name, PlanogramDescription33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text34Property.Name, PlanogramDescription34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text35Property.Name, PlanogramDescription35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text36Property.Name, PlanogramDescription36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text37Property.Name, PlanogramDescription37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text38Property.Name, PlanogramDescription38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text39Property.Name, PlanogramDescription39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text40Property.Name, PlanogramDescription40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text41Property.Name, PlanogramDescription41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text42Property.Name, PlanogramDescription42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text43Property.Name, PlanogramDescription43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text44Property.Name, PlanogramDescription44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text45Property.Name, PlanogramDescription45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text46Property.Name, PlanogramDescription46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text47Property.Name, PlanogramDescription47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text48Property.Name, PlanogramDescription48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text49Property.Name, PlanogramDescription49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text50Property.Name, PlanogramDescription50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value1Property.Name, PlanogramValue01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value2Property.Name, PlanogramValue02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value3Property.Name, PlanogramValue03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value4Property.Name, PlanogramValue04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value5Property.Name, PlanogramValue05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value6Property.Name, PlanogramValue06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value7Property.Name, PlanogramValue07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value8Property.Name, PlanogramValue08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value9Property.Name, PlanogramValue09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value10Property.Name, PlanogramValue10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value11Property.Name, PlanogramValue11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value12Property.Name, PlanogramValue12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value13Property.Name, PlanogramValue13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value14Property.Name, PlanogramValue14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value15Property.Name, PlanogramValue15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value16Property.Name, PlanogramValue16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value17Property.Name, PlanogramValue17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value18Property.Name, PlanogramValue18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value19Property.Name, PlanogramValue19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value20Property.Name, PlanogramValue20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value21Property.Name, PlanogramValue21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value22Property.Name, PlanogramValue22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value23Property.Name, PlanogramValue23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value24Property.Name, PlanogramValue24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value25Property.Name, PlanogramValue25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value26Property.Name, PlanogramValue26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value27Property.Name, PlanogramValue27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value28Property.Name, PlanogramValue28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value29Property.Name, PlanogramValue29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value30Property.Name, PlanogramValue30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value31Property.Name, PlanogramValue31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value32Property.Name, PlanogramValue32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value33Property.Name, PlanogramValue33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value34Property.Name, PlanogramValue34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value35Property.Name, PlanogramValue35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value36Property.Name, PlanogramValue36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value37Property.Name, PlanogramValue37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value38Property.Name, PlanogramValue38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value39Property.Name, PlanogramValue39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value40Property.Name, PlanogramValue40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value41Property.Name, PlanogramValue41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value42Property.Name, PlanogramValue42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value43Property.Name, PlanogramValue43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value44Property.Name, PlanogramValue44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value45Property.Name, PlanogramValue45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value46Property.Name, PlanogramValue46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value47Property.Name, PlanogramValue47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value48Property.Name, PlanogramValue48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value49Property.Name, PlanogramValue49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value50Property.Name, PlanogramValue50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag1Property.Name, PlanogramFlag01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag2Property.Name, PlanogramFlag02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag3Property.Name, PlanogramFlag03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag4Property.Name, PlanogramFlag04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag5Property.Name, PlanogramFlag05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag6Property.Name, PlanogramFlag06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag7Property.Name, PlanogramFlag07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag8Property.Name, PlanogramFlag08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag9Property.Name, PlanogramFlag09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag10Property.Name, PlanogramFlag10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date1Property.Name, PlanogramDate1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date2Property.Name, PlanogramDate2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date3Property.Name, PlanogramDate3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note1Property.Name, PlanogramNote1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note2Property.Name, PlanogramNote2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note3Property.Name, PlanogramNote3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note4Property.Name, PlanogramNote4);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note5Property.Name, PlanogramNote5);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultFixtureMappings()
        {
            yield return CreateMappingTuple(PlanogramFixture.NameProperty.Name, SegmentName);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultComponentMappings()
        {
            yield return CreateMappingTuple(PlanogramComponent.NameProperty.Name, FixtureName);
            yield return CreateMappingTuple(PlanogramComponent.ManufacturerPartNumberProperty.Name, FixturePartId);
            yield return CreateMappingTuple(PlanogramComponent.WeightLimitProperty.Name, FixtureWeightCapacity);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text1Property.Name, FixtureDescription01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text2Property.Name, FixtureDescription02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text3Property.Name, FixtureDescription03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text4Property.Name, FixtureDescription04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text5Property.Name, FixtureDescription05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text6Property.Name, FixtureDescription06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text7Property.Name, FixtureDescription07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text8Property.Name, FixtureDescription08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text9Property.Name, FixtureDescription09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text10Property.Name, FixtureDescription10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text11Property.Name, FixtureDescription11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text12Property.Name, FixtureDescription12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text13Property.Name, FixtureDescription13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text14Property.Name, FixtureDescription14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text15Property.Name, FixtureDescription15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text16Property.Name, FixtureDescription16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text17Property.Name, FixtureDescription17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text18Property.Name, FixtureDescription18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text19Property.Name, FixtureDescription19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text20Property.Name, FixtureDescription20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text21Property.Name, FixtureDescription21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text22Property.Name, FixtureDescription22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text23Property.Name, FixtureDescription23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text24Property.Name, FixtureDescription24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text25Property.Name, FixtureDescription25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text26Property.Name, FixtureDescription26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text27Property.Name, FixtureDescription27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text28Property.Name, FixtureDescription28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text29Property.Name, FixtureDescription29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text30Property.Name, FixtureDescription30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value1Property.Name, FixtureValue01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value2Property.Name, FixtureValue02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value3Property.Name, FixtureValue03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value4Property.Name, FixtureValue04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value5Property.Name, FixtureValue05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value6Property.Name, FixtureValue06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value7Property.Name, FixtureValue07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value8Property.Name, FixtureValue08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value9Property.Name, FixtureValue09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value10Property.Name, FixtureValue10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value11Property.Name, FixtureValue11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value12Property.Name, FixtureValue12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value13Property.Name, FixtureValue13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value14Property.Name, FixtureValue14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value15Property.Name, FixtureValue15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value16Property.Name, FixtureValue16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value17Property.Name, FixtureValue17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value18Property.Name, FixtureValue18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value19Property.Name, FixtureValue19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value20Property.Name, FixtureValue20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value21Property.Name, FixtureValue21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value22Property.Name, FixtureValue22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value23Property.Name, FixtureValue23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value24Property.Name, FixtureValue24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value25Property.Name, FixtureValue25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value26Property.Name, FixtureValue26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value27Property.Name, FixtureValue27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value28Property.Name, FixtureValue28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value29Property.Name, FixtureValue29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value30Property.Name, FixtureValue30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag1Property.Name, FixtureFlag01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag2Property.Name, FixtureFlag02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag3Property.Name, FixtureFlag03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag4Property.Name, FixtureFlag04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag5Property.Name, FixtureFlag05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag6Property.Name, FixtureFlag06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag7Property.Name, FixtureFlag07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag8Property.Name, FixtureFlag08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag9Property.Name, FixtureFlag09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag10Property.Name, FixtureFlag10);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultProductMappings()
        {
            yield return CreateMappingTuple(PlanogramProduct.NameProperty.Name, ProductName);
            yield return CreateMappingTuple(PlanogramProduct.GtinProperty.Name, ProductUpc);
            yield return CreateMappingTuple(PlanogramProduct.BrandProperty.Name, ProductBrand);
            yield return CreateMappingTuple(PlanogramProduct.SubcategoryProperty.Name, ProductSubCategory);
            yield return CreateMappingTuple(PlanogramProduct.VendorProperty.Name, ProductSupplier);
            yield return CreateMappingTuple(PlanogramProduct.ManufacturerProperty.Name, ProductManufacturer);
            yield return CreateMappingTuple(PlanogramProduct.SizeProperty.Name, ProductSize);
            yield return CreateMappingTuple(PlanogramProduct.UnitOfMeasureProperty.Name, ProductUom);
            yield return CreateMappingTuple(PlanogramProduct.SellPriceProperty.Name, ProductPrice);
            yield return CreateMappingTuple(PlanogramProduct.CostPriceProperty.Name, ProductUnitCost);
            yield return CreateMappingTuple(PlanogramProduct.CaseCostProperty.Name, ProductCaseCost);
            yield return CreateMappingTuple(PlanogramProduct.SellPackCountProperty.Name, ProductInnerPack);
            yield return CreateMappingTuple(PlanogramProduct.CasePackUnitsProperty.Name, ProductCaseTotalNumber);

            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text1Property.Name, ProductDescription01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text2Property.Name, ProductDescription02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text3Property.Name, ProductDescription03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text4Property.Name, ProductDescription04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text5Property.Name, ProductDescription05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text6Property.Name, ProductDescription06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text7Property.Name, ProductDescription07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text8Property.Name, ProductDescription08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text9Property.Name, ProductDescription09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text10Property.Name, ProductDescription10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text11Property.Name, ProductDescription11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text12Property.Name, ProductDescription12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text13Property.Name, ProductDescription13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text14Property.Name, ProductDescription14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text15Property.Name, ProductDescription15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text16Property.Name, ProductDescription16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text17Property.Name, ProductDescription17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text18Property.Name, ProductDescription18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text19Property.Name, ProductDescription19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text20Property.Name, ProductDescription20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text21Property.Name, ProductDescription21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text22Property.Name, ProductDescription22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text23Property.Name, ProductDescription23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text24Property.Name, ProductDescription24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text25Property.Name, ProductDescription25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text26Property.Name, ProductDescription26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text27Property.Name, ProductDescription27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text28Property.Name, ProductDescription28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text29Property.Name, ProductDescription29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text30Property.Name, ProductDescription30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text31Property.Name, ProductDescription31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text32Property.Name, ProductDescription32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text33Property.Name, ProductDescription33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text34Property.Name, ProductDescription34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text35Property.Name, ProductDescription35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text36Property.Name, ProductDescription36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text37Property.Name, ProductDescription37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text38Property.Name, ProductDescription38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text39Property.Name, ProductDescription39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text40Property.Name, ProductDescription40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text41Property.Name, ProductDescription41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text42Property.Name, ProductDescription42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text43Property.Name, ProductDescription43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text44Property.Name, ProductDescription44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text45Property.Name, ProductDescription45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text46Property.Name, ProductDescription46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text47Property.Name, ProductDescription47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text48Property.Name, ProductDescription48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text49Property.Name, ProductDescription49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text50Property.Name, ProductDescription50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value1Property.Name, ProductValue01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value2Property.Name, ProductValue02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value3Property.Name, ProductValue03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value4Property.Name, ProductValue04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value5Property.Name, ProductValue05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value6Property.Name, ProductValue06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value7Property.Name, ProductValue07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value8Property.Name, ProductValue08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value9Property.Name, ProductValue09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value10Property.Name, ProductValue10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value11Property.Name, ProductValue11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value12Property.Name, ProductValue12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value13Property.Name, ProductValue13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value14Property.Name, ProductValue14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value15Property.Name, ProductValue15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value16Property.Name, ProductValue16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value17Property.Name, ProductValue17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value18Property.Name, ProductValue18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value19Property.Name, ProductValue19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value20Property.Name, ProductValue20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value21Property.Name, ProductValue21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value22Property.Name, ProductValue22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value23Property.Name, ProductValue23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value24Property.Name, ProductValue24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value25Property.Name, ProductValue25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value26Property.Name, ProductValue26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value27Property.Name, ProductValue27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value28Property.Name, ProductValue28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value29Property.Name, ProductValue29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value30Property.Name, ProductValue30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value31Property.Name, ProductValue31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value32Property.Name, ProductValue32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value33Property.Name, ProductValue33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value34Property.Name, ProductValue34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value35Property.Name, ProductValue35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value36Property.Name, ProductValue36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value37Property.Name, ProductValue37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value38Property.Name, ProductValue38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value39Property.Name, ProductValue39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value40Property.Name, ProductValue40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value41Property.Name, ProductValue41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value42Property.Name, ProductValue42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value43Property.Name, ProductValue43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value44Property.Name, ProductValue44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value45Property.Name, ProductValue45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value46Property.Name, ProductValue46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value47Property.Name, ProductValue47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value48Property.Name, ProductValue48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value49Property.Name, ProductValue49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value50Property.Name, ProductValue50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag1Property.Name, ProductFlag01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag2Property.Name, ProductFlag02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag3Property.Name, ProductFlag03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag4Property.Name, ProductFlag04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag5Property.Name, ProductFlag05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag6Property.Name, ProductFlag06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag7Property.Name, ProductFlag07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag8Property.Name, ProductFlag08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag9Property.Name, ProductFlag09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag10Property.Name, ProductFlag10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date1Property.Name, ProductDate1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date2Property.Name, ProductDate2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date3Property.Name, ProductDate3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note1Property.Name, ProductNote1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note2Property.Name, ProductNote2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note3Property.Name, ProductNote3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note4Property.Name, ProductNote4);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note5Property.Name, ProductNote5);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultPerformanceMappings()
        {
            yield break;
        }

        public static IEnumerable<SpacePlanningImportHelper.MetricData> EnumerateDefaultMetrics()
        {
            yield return
                new SpacePlanningImportHelper.MetricData(Message.PlanogramPerformance_DefaultMetric1_SalesValue,
                                                         Message.PlanogramPerformance_DefaultMetric1_RegularSalesValue,
                                                         PerformanceSales,
                                                         1);
            yield return
                new SpacePlanningImportHelper.MetricData(Message.PlanogramPerformance_DefaultMetric2_SalesVolume,
                                                         Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume,
                                                         PerformanceUnitMovement,
                                                         2,
                                                         MetricType.Integer,
                                                         MetricSpecialType.RegularSalesUnits);
            yield return
                new SpacePlanningImportHelper.MetricData(Message.PlanogramPerformance_DefaultMetric3_SalesMargin,
                                                         Message.PlanogramPerformance_DefaultMetric3_RegularSalesMargin,
                                                         PerformanceProfit,
                                                         3);
        }

        #endregion

        #region Object Field Info Creation

        private static ObjectFieldInfo NewObjectFieldInfo(String section, String field, String friendlyName)
        {
            return ObjectFieldInfo.NewObjectFieldInfo(typeof (Object),
                                                      section,
                                                      field,
                                                      friendlyName,
                                                      typeof (String),
                                                      ModelPropertyDisplayType.None);
        }

        private static Tuple<String, String> CreateMappingTuple(String targetPropertyName, String sourcePropertyName)
        {
            return new Tuple<String, String>(targetPropertyName, sourcePropertyName);
        }

        private static Tuple<String, String> CreateCustomAttributeMappingTuple(String targetPropertyName, String sourcePropertyName)
        {
            return new Tuple<String, String>(String.Format("{0}.{1}", PlanogramProduct.CustomAttributesProperty.Name, targetPropertyName),
                                             sourcePropertyName);
        }

        #endregion

        public static PlanogramPositionMerchandisingStyle ToPlanogramPositionMerchandisingStyle(this ProSpaceMerchandisingType source)
        {
            switch (source)
            {
                case ProSpaceMerchandisingType.Unit:
                case ProSpaceMerchandisingType.Loose:
                case ProSpaceMerchandisingType.LogStack:
                    return PlanogramPositionMerchandisingStyle.Unit;
                case ProSpaceMerchandisingType.Tray:
                    return PlanogramPositionMerchandisingStyle.Tray;
                case ProSpaceMerchandisingType.Case:
                    return PlanogramPositionMerchandisingStyle.Case;
                case ProSpaceMerchandisingType.Display:
                    return PlanogramPositionMerchandisingStyle.Display;
                case ProSpaceMerchandisingType.Alternate:
                    return PlanogramPositionMerchandisingStyle.Alternate;
                default:
                    Debug.Fail("Unknown value when calling ToPlanogramPositionMerchandisingStyle.");
                    break;
            }
            return PlanogramPositionMerchandisingStyle.Default;
        }

        #endregion
    }

    #region ProSpace Enum Types

    public enum ProSpaceComponentType
    {
        Shelf = 0,
        Chest = 1,
        Bin = 2,
        PolygonalShelf = 3,
        Rod = 4,
        LateralRod = 5,
        Bar = 6,
        Pegboard = 7,
        MultiRowPegboard = 8,
        CurvedRod = 9,
        Obstruction = 10,
        Sign = 11,
        GravityFeed = 12
    }

    public enum ProSpaceMerchandisingType
    {
        Default = -1,
        Unit = 0,
        Tray = 1,
        Case = 2,
        Display = 3,
        Alternate = 4,
        Loose = 5,
        LogStack = 6
    }

    public enum ProSpaceFillPatternType
    {
        Solid = 0,
        DiagonalDown = 1,
        DiagonalUp = 2,
        Crosshatch = 3,
        Border = 4,
        Dots = 5,
        Xes = 6,
        Circles = 7,
        DashDiagonalUp = 8,
        DashDiagonalDown = 9,
        Triangles = 10,
        HorizontalWaves = 11,
        VerticalWaves = 12,
        Squares = 13,
        HorizontalLines = 14,
        VerticalLines = 15,
        Reticle = 16,
        LightDashDiagonalUp = 17,
        LightDashDiagonalDown = 18,
        StrongDashDiagonalUp = 19,
        StrongDashDiagonalDown = 20,
        Ces = 21,
        Angles = 22,
        HorizontalDashLanes = 23,
        VerticalDashLanes = 24,
        HorizontalDashLines = 25,
        VerticalDashLines = 26,
        Bricks = 27,
        Scales = 28,
        Chainmail = 29
    }

    public enum ProSpaceOrientationType
    {
        Front = 0,
        Front90 = 1,
        Side = 2,
        Side90 = 3,
        Top = 4,
        Top90 = 5,
        Back = 6,
        Back90 = 7,
        Right = 8,
        Right90 = 9,
        Base = 10,
        Base90 = 11,
        Front180 = 12,
        Front270 = 13,
        Side180 = 14,
        Side270 = 15,
        Top180 = 16,
        Top270 = 17,
        Back180 = 18,
        Back270 = 19,
        Right180 = 20,
        Right270 = 21,
        Base180 = 22,
        Base270 = 23
    }

    public enum ProSpaceMerchPlacementType
    {
        Default = 0,
        Manual = 1,
        Edge = 2,
        Stacked = 3,
        Spread = 4
    }

    public enum ProSpaceMerchDirectionType
    {
        Default = -1,
        Normal = 0,
        Reverse = 1
    }

    public enum ProSpaceMerchSizeType
    {
        Default = 0,
        Normal = 1,
        ExpandSqueeze = 2,
        SpaceOutFacings = 3
    }

    public enum ProSpaceShapeType
    {
        Box = 0,
        Jar = 1,
        Can = 2,
        Roll = 3,
        Loose = 4,
        Holed = 5,
        Hairpin = 6,
        Clothing = 7,
        Bottle = 8
    }

    public enum ProSpaceFixtureCombineType
    {
        No = 0,
        Both = 1,
        Left = 2,
        Right = 3
    }

    public enum ProSpaceScaleType
    {
        Scaled,
        Fixed,
        Sized
    }

    public enum ProSpaceDrawingType
    {
        Arc = 0,
        Ellipse = 1,
        Line = 2,
        Polygon = 3,
        Rectangle = 4,
        Text = 5
    }

    public enum JdaSpacePlanningUsePerformanceValueType
    {
        No = 0,
        Yes = 1,
        WhenNotZero = 2
    }

    #endregion
}