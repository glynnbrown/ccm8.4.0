﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
// V8-32273 : M.Brumby
//  Removed calculated fields from export selection
#endregion

#region Version History : (CCM 8.4.0)
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.External
{
    public static class JDAExportHelper
    {
        #region Fields

        /// <summary>
        /// List of file versions that can be exported at the moment.
        /// </summary>
        private static List<String> _availableVersions;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Project</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _projectFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Planogram</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _planogramFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Component</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _componentFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Fixture</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _fixtureFieldInfoByFieldIndex;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Product</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _productFieldInfoByFieldInfos;

        /// <summary>
        ///     Dictionary of <c>Field Indexes</c> in a <c>ProSpace</c> file and their corresponding <see cref="ObjectFieldInfo" />
        ///     related to a <c>Performance</c> element.
        /// </summary>
        private static Dictionary<String, ObjectFieldInfo> _performanceFieldInfoByFieldIndex;

        private static Dictionary<ProSpaceComponentType, Int32> _defaultFillColourByProSpaceComponentType;
        private static Dictionary<String, ObjectFieldInfo> _allFieldInfos;

        #endregion

        #region Properties

        /// <summary>
        /// Default name for the ProSpace template file. [ProSpace Default.pogeft]
        /// </summary>
        public static String DefaultPogeft { get { return "JDA Space Planning V8 Default.pogeft"; } }

        /// <summary>
        /// Gets the list of file versions that can be exported at the moment.
        /// </summary>
        /// <remarks>
        /// The list of versions is lazy loaded when requested for the first time.
        /// </remarks>
        public static List<String> AvailableVersions
        {
            get { return _availableVersions ?? (_availableVersions = new List<String> { "2", "3", "4", "2005.0", "2006.0", "2007.0", "2008.0" }); }
        }

        public static IEnumerable<String> SupportedVersions
        {
            get
            {
                yield return "2008.0";
                yield return "2008.1";
            }
        }

        public static Dictionary<String, ObjectFieldInfo> PlanogramFieldInfos
        {
            get { return _planogramFieldInfoByFieldIndex ?? (_planogramFieldInfoByFieldIndex = InitializePlanogramFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> ComponentFieldInfos
        {
            get { return _componentFieldInfoByFieldIndex ?? (_componentFieldInfoByFieldIndex = InitializeComponentFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> FixtureFieldInfos
        {
            get { return _fixtureFieldInfoByFieldIndex ?? (_fixtureFieldInfoByFieldIndex = InitializeFixtureFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> ProductFieldInfos
        {
            get { return _productFieldInfoByFieldInfos ?? (_productFieldInfoByFieldInfos = InitializeProductFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> PerformanceFieldInfos
        {
            get { return _performanceFieldInfoByFieldIndex ?? (_performanceFieldInfoByFieldIndex = InitializePerformanceFieldInfos()); }
        }

        public static Dictionary<String, ObjectFieldInfo> AllFieldInfos
        {
            get { return _allFieldInfos ?? (_allFieldInfos = InitializeAllFieldInfos()); }
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeAllFieldInfos()
        {
            return PlanogramFieldInfos.Union(ComponentFieldInfos)
                                      .Union(FixtureFieldInfos)
                                      .Union(ProductFieldInfos)
                                      .Union(PerformanceFieldInfos)
                                      .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public static Dictionary<ProSpaceComponentType, Int32> DefaultFillColourByProSpaceComponentType
        {
            get
            {
                return _defaultFillColourByProSpaceComponentType ??
                       (_defaultFillColourByProSpaceComponentType =
                        new Dictionary<ProSpaceComponentType, Int32>
                        {
                            {ProSpaceComponentType.Shelf, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.Chest, SpacePlanningImportHelper.DefaultChestFillColour},
                            {ProSpaceComponentType.Bin, SpacePlanningImportHelper.DefaultChestFillColour},
                            {ProSpaceComponentType.PolygonalShelf, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.Rod, SpacePlanningImportHelper.DefaultRodFillColour},
                            {ProSpaceComponentType.LateralRod, SpacePlanningImportHelper.DefaultRodFillColour},
                            {ProSpaceComponentType.Bar, SpacePlanningImportHelper.DefaultBarFillColour},
                            {ProSpaceComponentType.Pegboard, SpacePlanningImportHelper.DefaultPegFillColour},
                            {ProSpaceComponentType.MultiRowPegboard, SpacePlanningImportHelper.DefaultPegFillColour},
                            {ProSpaceComponentType.CurvedRod, SpacePlanningImportHelper.DefaultRodFillColour},
                            {ProSpaceComponentType.Obstruction, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.Sign, SpacePlanningImportHelper.DefaultShelfFillColour},
                            {ProSpaceComponentType.GravityFeed, SpacePlanningImportHelper.DefaultShelfFillColour}
                        });
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Get the index from the field name.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public static Int32 GetIndex(String field)
        {
            String fieldIndexString = field.Substring(field.Length - 3);
            Int32 index;
            if (!Int32.TryParse(fieldIndexString, out index)) return -1;
            return index;
        }

        #region Field Info Initialization

        private static Dictionary<String, ObjectFieldInfo> InitializePlanogramFieldInfos()
        {
            var fieldInfos =
                new List<ObjectFieldInfo>
                {
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectPrimaryKey, Message.JdaFieldLabel_PrimaryKey),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectLayoutFile, Message.JdaFieldLabel_LayoutFile),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectMovementPeriod, Message.JdaFieldLabel_MovementPeriod),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDaysOfSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemandCycleLength, Message.JdaFieldLabel_DemandCycleLength),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectPeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectBackroomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectPegId, Message.JdaFieldLabel_PegId),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectMeasurement, Message.JdaFieldLabel_MeasurementUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchXDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_X)),
                    //{ProjectField022, NewObjectFieldInfo(ProjectField022, "Field 022")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchYDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Y)),
                    //{ProjectField031, NewObjectFieldInfo(ProjectField031, "Field 031")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectMerchZDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Z)),
                    //{ProjectField040, NewObjectFieldInfo(ProjectField040, "Field 040")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand01, String.Format(Message.JdaFieldLabel_Demand, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand02, String.Format(Message.JdaFieldLabel_Demand, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand03, String.Format(Message.JdaFieldLabel_Demand, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand04, String.Format(Message.JdaFieldLabel_Demand, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand05, String.Format(Message.JdaFieldLabel_Demand, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand06, String.Format(Message.JdaFieldLabel_Demand, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand07, String.Format(Message.JdaFieldLabel_Demand, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand08, String.Format(Message.JdaFieldLabel_Demand, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand09, String.Format(Message.JdaFieldLabel_Demand, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand10, String.Format(Message.JdaFieldLabel_Demand, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand11, String.Format(Message.JdaFieldLabel_Demand, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand12, String.Format(Message.JdaFieldLabel_Demand, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand13, String.Format(Message.JdaFieldLabel_Demand, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand14, String.Format(Message.JdaFieldLabel_Demand, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand15, String.Format(Message.JdaFieldLabel_Demand, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand16, String.Format(Message.JdaFieldLabel_Demand, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand17, String.Format(Message.JdaFieldLabel_Demand, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand18, String.Format(Message.JdaFieldLabel_Demand, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand20, String.Format(Message.JdaFieldLabel_Demand, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand21, String.Format(Message.JdaFieldLabel_Demand, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand22, String.Format(Message.JdaFieldLabel_Demand, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand23, String.Format(Message.JdaFieldLabel_Demand, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand24, String.Format(Message.JdaFieldLabel_Demand, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand25, String.Format(Message.JdaFieldLabel_Demand, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand26, String.Format(Message.JdaFieldLabel_Demand, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand27, String.Format(Message.JdaFieldLabel_Demand, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDemand28, String.Format(Message.JdaFieldLabel_Demand, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectInventoryModelManual, Message.JdaFieldLabel_InventoryModelManual),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectInventoryModelCaseMultiple,
                                       Message.JdaFieldLabel_InventoryModelCaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                       ProSpaceImportHelper.ProjectInventoryModelDaysOfSupply,
                                       Message.JdaFieldLabel_InventoryModelDaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectInventoryModelPeak, Message.JdaFieldLabel_InventoryModelPeak),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectInventoryModelMinUnits, Message.JdaFieldLabel_InventoryModelMinUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectInventoryModelMaxUnits, Message.JdaFieldLabel_InventoryModelMaxUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue01, String.Format(Message.JdaFieldLabel_Value, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue02, String.Format(Message.JdaFieldLabel_Value, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue03, String.Format(Message.JdaFieldLabel_Value, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue04, String.Format(Message.JdaFieldLabel_Value, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue05, String.Format(Message.JdaFieldLabel_Value, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue06, String.Format(Message.JdaFieldLabel_Value, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue07, String.Format(Message.JdaFieldLabel_Value, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue08, String.Format(Message.JdaFieldLabel_Value, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue09, String.Format(Message.JdaFieldLabel_Value, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription01, String.Format(Message.JdaFieldLabel_Description, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription02, String.Format(Message.JdaFieldLabel_Description, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription03, String.Format(Message.JdaFieldLabel_Description, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription04, String.Format(Message.JdaFieldLabel_Description, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription05, String.Format(Message.JdaFieldLabel_Description, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription06, String.Format(Message.JdaFieldLabel_Description, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription07, String.Format(Message.JdaFieldLabel_Description, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription08, String.Format(Message.JdaFieldLabel_Description, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription09, String.Format(Message.JdaFieldLabel_Description, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription10, String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription11, String.Format(Message.JdaFieldLabel_Description, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription12, String.Format(Message.JdaFieldLabel_Description, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription13, String.Format(Message.JdaFieldLabel_Description, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription14, String.Format(Message.JdaFieldLabel_Description, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription15, String.Format(Message.JdaFieldLabel_Description, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription16, String.Format(Message.JdaFieldLabel_Description, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription17, String.Format(Message.JdaFieldLabel_Description, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription18, String.Format(Message.JdaFieldLabel_Description, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription19, String.Format(Message.JdaFieldLabel_Description, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription20, String.Format(Message.JdaFieldLabel_Description, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription21, String.Format(Message.JdaFieldLabel_Description, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription22, String.Format(Message.JdaFieldLabel_Description, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription23, String.Format(Message.JdaFieldLabel_Description, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription24, String.Format(Message.JdaFieldLabel_Description, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription25, String.Format(Message.JdaFieldLabel_Description, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription26, String.Format(Message.JdaFieldLabel_Description, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription27, String.Format(Message.JdaFieldLabel_Description, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription28, String.Format(Message.JdaFieldLabel_Description, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription29, String.Format(Message.JdaFieldLabel_Description, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription30, String.Format(Message.JdaFieldLabel_Description, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription31, String.Format(Message.JdaFieldLabel_Description, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription32, String.Format(Message.JdaFieldLabel_Description, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription33, String.Format(Message.JdaFieldLabel_Description, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription34, String.Format(Message.JdaFieldLabel_Description, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription35, String.Format(Message.JdaFieldLabel_Description, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription36, String.Format(Message.JdaFieldLabel_Description, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription37, String.Format(Message.JdaFieldLabel_Description, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription38, String.Format(Message.JdaFieldLabel_Description, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription39, String.Format(Message.JdaFieldLabel_Description, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription40, String.Format(Message.JdaFieldLabel_Description, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription41, String.Format(Message.JdaFieldLabel_Description, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription42, String.Format(Message.JdaFieldLabel_Description, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription43, String.Format(Message.JdaFieldLabel_Description, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription44, String.Format(Message.JdaFieldLabel_Description, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription45, String.Format(Message.JdaFieldLabel_Description, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription46, String.Format(Message.JdaFieldLabel_Description, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription47, String.Format(Message.JdaFieldLabel_Description, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription48, String.Format(Message.JdaFieldLabel_Description, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription49, String.Format(Message.JdaFieldLabel_Description, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDescription50, String.Format(Message.JdaFieldLabel_Description, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag01, String.Format(Message.JdaFieldLabel_Flag, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectNotes, Message.JdaFieldLabel_Notes),
                    //{ProjectField186, NewObjectFieldInfo(ProjectField186, "Field 186")},
                    //{ProjectField187, NewObjectFieldInfo(ProjectField187, "Field 187")},
                    //{ProjectField188, NewObjectFieldInfo(ProjectField188, "Field 188")},
                    //{ProjectField189, NewObjectFieldInfo(ProjectField189, "Field 189")},
                    //{ProjectField190, NewObjectFieldInfo(ProjectField190, "Field 190")},
                    //{ProjectField191, NewObjectFieldInfo(ProjectField191, "Field 191")},
                    //{ProjectField192, NewObjectFieldInfo(ProjectField192, "Field 192")},
                    //{ProjectField193, NewObjectFieldInfo(ProjectField193, "Field 193")},
                    //{ProjectField194, NewObjectFieldInfo(ProjectField194, "Field 194")},
                    //{ProjectField195, NewObjectFieldInfo(ProjectField195, "Field 195")},
                    //{ProjectField196, NewObjectFieldInfo(ProjectField196, "Field 196")},
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectUsePerformancePrice, Message.JdaFieldLabel_UsePerformancePrice),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectUsePerformanceCost, Message.JdaFieldLabel_UsePerformanceCost),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectUsePerformanceTaxCode, Message.JdaFieldLabel_UsePerformanceTaxCode),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectUsePerformanceMovement, Message.JdaFieldLabel_UsePerformanceMovement),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectStatus, Message.JdaFieldLabel_Status),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDateCreated, Message.JdaFieldLabel_DateCreated),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDateModified, Message.JdaFieldLabel_DateModified),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDatePending, Message.JdaFieldLabel_DatePending),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDateEffective, Message.JdaFieldLabel_DateEffective),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDateFinished, Message.JdaFieldLabel_DateFinished),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDate1, String.Format(Message.JdaFieldLabel_Date, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDate2, String.Format(Message.JdaFieldLabel_Date, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDate3, String.Format(Message.JdaFieldLabel_Date, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectCreatedBy, Message.JdaFieldLabel_CreatedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectModifiedBy, Message.JdaFieldLabel_ModifiedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Project,
                                      ProSpaceImportHelper.ProjectPlanogramSpecificInventory,
                                       Message.JdaFieldLabel_PlanogramSpecificInventory),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectNote1, String.Format(Message.JdaFieldLabel_Note, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectNote2, String.Format(Message.JdaFieldLabel_Note, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectNote3, String.Format(Message.JdaFieldLabel_Note, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectNote4, String.Format(Message.JdaFieldLabel_Note, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Project, ProSpaceImportHelper.ProjectNote5, String.Format(Message.JdaFieldLabel_Note, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramWidth, Message.JdaFieldLabel_Width),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramHeight, Message.JdaFieldLabel_Height),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDepth, Message.JdaFieldLabel_Depth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramColor, Message.JdaFieldLabel_Color),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramBackDepth, Message.JdaFieldLabel_BackDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDrawBack, Message.JdaFieldLabel_DrawBack),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramBaseWidth, Message.JdaFieldLabel_BaseWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramBaseHeight, Message.JdaFieldLabel_BaseHeight),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramBaseDepth, Message.JdaFieldLabel_BaseDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDrawBase, Message.JdaFieldLabel_DrawBase),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramBaseColor, Message.JdaFieldLabel_BaseColor),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDrawNotches, Message.JdaFieldLabel_DrawNotches),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNotchOffset, Message.JdaFieldLabel_NotchOffset),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNotchSpacing, Message.JdaFieldLabel_NotchSpacing),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDoubleNotches, Message.JdaFieldLabel_DoubleNotches),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNotchColor, Message.JdaFieldLabel_NotchColour),
                    //{PlanogramField019, NewObjectFieldInfo(PlanogramField019, "Field 019")},
                    //{PlanogramField020, NewObjectFieldInfo(PlanogramField020, "Field 020")},
                    //{PlanogramField021, NewObjectFieldInfo(PlanogramField021, "Field 021")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramTrafficFlow, Message.JdaFieldLabel_TrafficFlow),
                    //{PlanogramField023, NewObjectFieldInfo(PlanogramField023, "Field 023")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramShapeId, Message.JdaFieldLabel_ShapeId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramBitmapId, Message.JdaFieldLabel_BitmapId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchXDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_X)),
                    //{PlanogramField034, NewObjectFieldInfo(PlanogramField034, "Field 034")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchYDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Y)),
                    //{PlanogramField043, NewObjectFieldInfo(PlanogramField043, "Field 043")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZMin,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZMax,
                                       String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZUprights,
                                       String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZCaps,
                                       String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZPlacement,
                                       String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZNumber,
                                       String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZSize,
                                       String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramMerchZDirection,
                                       String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Z)),
                    //{PlanogramField052, NewObjectFieldInfo(PlanogramField052, "Field 052")},
                    //{PlanogramField053, NewObjectFieldInfo(PlanogramField053, "Field 053")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNumerOfStores, Message.JdaFieldLabel_NumberStores),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNotchWidth, Message.JdaFieldLabel_NotchWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription01,
                                       String.Format(Message.JdaFieldLabel_Description, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription02,
                                       String.Format(Message.JdaFieldLabel_Description, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription03,
                                       String.Format(Message.JdaFieldLabel_Description, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription04,
                                       String.Format(Message.JdaFieldLabel_Description, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription05,
                                       String.Format(Message.JdaFieldLabel_Description, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription06,
                                       String.Format(Message.JdaFieldLabel_Description, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription07,
                                       String.Format(Message.JdaFieldLabel_Description, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription08,
                                       String.Format(Message.JdaFieldLabel_Description, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription09,
                                       String.Format(Message.JdaFieldLabel_Description, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription10,
                                       String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription11,
                                       String.Format(Message.JdaFieldLabel_Description, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription12,
                                       String.Format(Message.JdaFieldLabel_Description, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription13,
                                       String.Format(Message.JdaFieldLabel_Description, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription14,
                                       String.Format(Message.JdaFieldLabel_Description, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription15,
                                       String.Format(Message.JdaFieldLabel_Description, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription16,
                                       String.Format(Message.JdaFieldLabel_Description, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription17,
                                       String.Format(Message.JdaFieldLabel_Description, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription18,
                                       String.Format(Message.JdaFieldLabel_Description, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription19,
                                       String.Format(Message.JdaFieldLabel_Description, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription20,
                                       String.Format(Message.JdaFieldLabel_Description, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription21,
                                       String.Format(Message.JdaFieldLabel_Description, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription22,
                                       String.Format(Message.JdaFieldLabel_Description, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription23,
                                       String.Format(Message.JdaFieldLabel_Description, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription24,
                                       String.Format(Message.JdaFieldLabel_Description, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription25,
                                       String.Format(Message.JdaFieldLabel_Description, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription26,
                                       String.Format(Message.JdaFieldLabel_Description, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription27,
                                       String.Format(Message.JdaFieldLabel_Description, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription28,
                                       String.Format(Message.JdaFieldLabel_Description, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription29,
                                       String.Format(Message.JdaFieldLabel_Description, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription30,
                                       String.Format(Message.JdaFieldLabel_Description, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription31,
                                       String.Format(Message.JdaFieldLabel_Description, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription32,
                                       String.Format(Message.JdaFieldLabel_Description, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription33,
                                       String.Format(Message.JdaFieldLabel_Description, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription34,
                                       String.Format(Message.JdaFieldLabel_Description, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription35,
                                       String.Format(Message.JdaFieldLabel_Description, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription36,
                                       String.Format(Message.JdaFieldLabel_Description, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription37,
                                       String.Format(Message.JdaFieldLabel_Description, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription38,
                                       String.Format(Message.JdaFieldLabel_Description, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription39,
                                       String.Format(Message.JdaFieldLabel_Description, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription40,
                                       String.Format(Message.JdaFieldLabel_Description, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription41,
                                       String.Format(Message.JdaFieldLabel_Description, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription42,
                                       String.Format(Message.JdaFieldLabel_Description, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription43,
                                       String.Format(Message.JdaFieldLabel_Description, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription44,
                                       String.Format(Message.JdaFieldLabel_Description, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription45,
                                       String.Format(Message.JdaFieldLabel_Description, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription46,
                                       String.Format(Message.JdaFieldLabel_Description, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription47,
                                       String.Format(Message.JdaFieldLabel_Description, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription48,
                                       String.Format(Message.JdaFieldLabel_Description, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription49,
                                       String.Format(Message.JdaFieldLabel_Description, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramDescription50,
                                       String.Format(Message.JdaFieldLabel_Description, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue01, String.Format(Message.JdaFieldLabel_Value, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue02, String.Format(Message.JdaFieldLabel_Value, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue03, String.Format(Message.JdaFieldLabel_Value, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue04, String.Format(Message.JdaFieldLabel_Value, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue05, String.Format(Message.JdaFieldLabel_Value, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue06, String.Format(Message.JdaFieldLabel_Value, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue07, String.Format(Message.JdaFieldLabel_Value, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue08, String.Format(Message.JdaFieldLabel_Value, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue09, String.Format(Message.JdaFieldLabel_Value, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag01, String.Format(Message.JdaFieldLabel_Flag, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag02, String.Format(Message.JdaFieldLabel_Flag, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag03, String.Format(Message.JdaFieldLabel_Flag, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag04, String.Format(Message.JdaFieldLabel_Flag, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag05, String.Format(Message.JdaFieldLabel_Flag, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag06, String.Format(Message.JdaFieldLabel_Flag, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag07, String.Format(Message.JdaFieldLabel_Flag, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag08, String.Format(Message.JdaFieldLabel_Flag, 8)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag09, String.Format(Message.JdaFieldLabel_Flag, 9)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFillPattern, Message.JdaFieldLabel_FillPattern),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramSegmentsToPrint, Message.JdaFieldLabel_SegmentsToPrint),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFileName, Message.JdaFieldLabel_FileName),
                    //{PlanogramField169, NewObjectFieldInfo(PlanogramField169, "Field 169")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramLayoutFileName, Message.JdaFieldLabel_LayoutFileName),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNotes, Message.JdaFieldLabel_Notes),
                    //{PlanogramField172, NewObjectFieldInfo(PlanogramField172, "Field 172")},
                    //{PlanogramField173, NewObjectFieldInfo(PlanogramField173, "Field 173")},
                    //{PlanogramField174, NewObjectFieldInfo(PlanogramField174, "Field 174")},
                    //{PlanogramField175, NewObjectFieldInfo(PlanogramField175, "Field 175")},
                    //{PlanogramField176, NewObjectFieldInfo(PlanogramField176, "Field 176")},
                    //{PlanogramField177, NewObjectFieldInfo(PlanogramField177, "Field 177")},
                    //{PlanogramField178, NewObjectFieldInfo(PlanogramField178, "Field 178")},
                    //{PlanogramField179, NewObjectFieldInfo(PlanogramField179, "Field 179")},
                    //{PlanogramField180, NewObjectFieldInfo(PlanogramField180, "Field 180")},
                    //{PlanogramField181, NewObjectFieldInfo(PlanogramField181, "Field 181")},
                    //{PlanogramField182, NewObjectFieldInfo(PlanogramField182, "Field 182")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramStatus1, String.Format(Message.JdaFieldLabel_StatusN, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramStatus2, String.Format(Message.JdaFieldLabel_StatusN, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramStatus3, String.Format(Message.JdaFieldLabel_StatusN, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDateCreated, Message.JdaFieldLabel_DateCreated),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDateModified, Message.JdaFieldLabel_DateModified),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDatePending, Message.JdaFieldLabel_DatePending),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDateEffective, Message.JdaFieldLabel_DateEffective),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDateFinished, Message.JdaFieldLabel_DateFinished),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDate1, String.Format(Message.JdaFieldLabel_Date, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDate2, String.Format(Message.JdaFieldLabel_Date, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDate3, String.Format(Message.JdaFieldLabel_Date, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramCreatedBy, Message.JdaFieldLabel_CreatedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramModifiedBy, Message.JdaFieldLabel_ModifiedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFloorBitmapId, Message.JdaFieldLabel_FloorBitmapId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDoorTransparency, Message.JdaFieldLabel_DoorTransparency),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFloorTileWidth, Message.JdaFieldLabel_FloorTileWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramFloorTileDepth, Message.JdaFieldLabel_FloorTileDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramInventoryManual, Message.JdaFieldLabel_InventoryModelManual),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramInventoryCaseMultiple,
                                       Message.JdaFieldLabel_InventoryModelCaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramInventoryDaysSupply,
                                       Message.JdaFieldLabel_InventoryModelDaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramInventoryPeak, Message.JdaFieldLabel_InventoryModelPeak),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramInventoryMinUnits,
                                       Message.JdaFieldLabel_InventoryModelMinUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram,
                                       ProSpaceImportHelper.PlanogramInventoryMaxUnits,
                                       Message.JdaFieldLabel_InventoryModelMaxUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDaysSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemandCycleLength, Message.JdaFieldLabel_DemandCycleLength),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramPeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramBackroomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemand01, String.Format(Message.JdaFieldLabel_Demand, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemand02, String.Format(Message.JdaFieldLabel_Demand, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemand03, String.Format(Message.JdaFieldLabel_Demand, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemand04, String.Format(Message.JdaFieldLabel_Demand, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemand05, String.Format(Message.JdaFieldLabel_Demand, 5)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemand06, String.Format(Message.JdaFieldLabel_Demand, 6)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDemand07, String.Format(Message.JdaFieldLabel_Demand, 7)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramId, Message.JdaFieldLabel_Id),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramDepartment, Message.JdaFieldLabel_Department),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramPartId, Message.JdaFieldLabel_PartId),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramGln, Message.JdaFieldLabel_Gln),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramPlanogramGuid, Message.JdaFieldLabel_PlanogramGuid),
                    //{PlanogramField224, NewObjectFieldInfo(PlanogramField224, "Field 224")},
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramAbbrevName, Message.JdaFieldLabel_AbbrevName),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramCategory, Message.JdaFieldLabel_Category),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramSubCategory, Message.JdaFieldLabel_SubCategory),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNote1, String.Format(Message.JdaFieldLabel_Note, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNote2, String.Format(Message.JdaFieldLabel_Note, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNote3, String.Format(Message.JdaFieldLabel_Note, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNote4, String.Format(Message.JdaFieldLabel_Note, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Planogram, ProSpaceImportHelper.PlanogramNote5, String.Format(Message.JdaFieldLabel_Note, 5))
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeComponentFieldInfos()
        {
            var fieldInfos = new List<ObjectFieldInfo>
                {
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureType, Message.JdaFieldLabel_Type),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureX, Message.JdaFieldLabel_X),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureWidth, Message.JdaFieldLabel_Width),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureY, Message.JdaFieldLabel_Y),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureHeight, Message.JdaFieldLabel_Height),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureZ, Message.JdaFieldLabel_Z),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureDepth, Message.JdaFieldLabel_Depth),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureSlope, Message.JdaFieldLabel_Slope),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureAngle, Message.JdaFieldLabel_Angle),
                    //{Fixture012, NewObjectFieldInfo(Fixture012, "Field012")},
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureColour, Message.JdaFieldLabel_Color),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureAssembly, Message.JdaFieldLabel_Assembly),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureXSpacing,
                                    String.Format(Message.JdaFieldLabel_AxisSpacing, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureYSpacing,
                                    String.Format(Message.JdaFieldLabel_AxisSpacing, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureXStart,
                                    String.Format(Message.JdaFieldLabel_AxisStart, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureYStart,
                                    String.Format(Message.JdaFieldLabel_AxisStart, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureWallWidth, Message.JdaFieldLabel_WallWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureWallHeight, Message.JdaFieldLabel_WallHeight),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureWallDepth, Message.JdaFieldLabel_WallDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureCurve, Message.JdaFieldLabel_Curve),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureMerch, Message.JdaFieldLabel_Merch),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureCanObstruct, Message.JdaFieldLabel_CanObstruct),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDetectOtherFixtures,
                                    Message.JdaFieldLabel_DetectOtherFixtures),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDetectPositionsOnOtherFixtures,
                                    Message.JdaFieldLabel_DetectPositionsOnOtherFixtures),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureLeftOverhang, Message.JdaFieldLabel_LeftOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureRightOverhang, Message.JdaFieldLabel_RightOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureLowerOverhang, Message.JdaFieldLabel_LowerOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureUpperOverhang, Message.JdaFieldLabel_UpperOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureBackOverhang, Message.JdaFieldLabel_BackOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFrontOverhang, Message.JdaFieldLabel_FrontOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchandisingStyle,
                                    Message.JdaFieldLabel_MerchandisingStyle),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureDividerWidth, Message.JdaFieldLabel_DividerWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureDividerHeight, Message.JdaFieldLabel_DividerHeight),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureDividerDepth, Message.JdaFieldLabel_DividerDepth),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureCanCombine, Message.JdaFieldLabel_CanCombine),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureGrilleHeight, Message.JdaFieldLabel_GrilleHeight),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureNotchOffset, Message.JdaFieldLabel_NotchOffset),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureXSpacing2, Message.JdaFieldLabel_XSpacing2),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureXStart2, Message.JdaFieldLabel_XStart2),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixturePegDrop, Message.JdaFieldLabel_PegDrop),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixturePegGapX,
                                    String.Format(Message.JdaFieldLabel_PegCap, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixturePegGapY,
                                    String.Format(Message.JdaFieldLabel_PegCap, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixturePrimaryLabelFormatName,
                                    Message.JdaFieldLabel_PrimaryLabelFormat),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureSecondaryLabelFormatName,
                                    Message.JdaFieldLabel_SecondaryLabelFormat),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureShapeId, Message.JdaFieldLabel_ShapeId),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureBitmapId, Message.JdaFieldLabel_BitmapId),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXMin,
                                    String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXMax,
                                    String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXUprights,
                                    String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXCaps,
                                    String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXPlacement,
                                    String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXNumber,
                                    String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXSize,
                                    String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchXDirection,
                                    String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_X)),
                    //{FixtureField057, NewObjectFieldInfo(FixtureField057, "Field057")},
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYMin,
                                    String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYMax,
                                    String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYUprights,
                                    String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYCaps,
                                    String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYPlacement,
                                    String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYNumber,
                                    String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYSize,
                                    String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchYDirection,
                                    String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Y)),
                    //{FixtureField066, NewObjectFieldInfo(FixtureField066, "Field066")},
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZMin,
                                    String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZMax,
                                    String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZUprights,
                                    String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZCaps,
                                    String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZPlacement,
                                    String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZNumber,
                                    String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZSize,
                                    String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureMerchZDirection,
                                    String.Format(Message.JdaFieldLabel_MerchAxisDirection, Message.JdaFieldLabel_Z)),
                    //{FixtureField075, NewObjectFieldInfo(FixtureField075, "Field075")},
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription01,
                                    String.Format(Message.JdaFieldLabel_Description, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription02,
                                    String.Format(Message.JdaFieldLabel_Description, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription03,
                                    String.Format(Message.JdaFieldLabel_Description, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription04,
                                    String.Format(Message.JdaFieldLabel_Description, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription05,
                                    String.Format(Message.JdaFieldLabel_Description, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription06,
                                    String.Format(Message.JdaFieldLabel_Description, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription07,
                                    String.Format(Message.JdaFieldLabel_Description, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription08,
                                    String.Format(Message.JdaFieldLabel_Description, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription09,
                                    String.Format(Message.JdaFieldLabel_Description, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription10,
                                    String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription11,
                                    String.Format(Message.JdaFieldLabel_Description, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription12,
                                    String.Format(Message.JdaFieldLabel_Description, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription13,
                                    String.Format(Message.JdaFieldLabel_Description, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription14,
                                    String.Format(Message.JdaFieldLabel_Description, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription15,
                                    String.Format(Message.JdaFieldLabel_Description, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription16,
                                    String.Format(Message.JdaFieldLabel_Description, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription17,
                                    String.Format(Message.JdaFieldLabel_Description, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription18,
                                    String.Format(Message.JdaFieldLabel_Description, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription19,
                                    String.Format(Message.JdaFieldLabel_Description, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription20,
                                    String.Format(Message.JdaFieldLabel_Description, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription21,
                                    String.Format(Message.JdaFieldLabel_Description, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription22,
                                    String.Format(Message.JdaFieldLabel_Description, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription23,
                                    String.Format(Message.JdaFieldLabel_Description, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription24,
                                    String.Format(Message.JdaFieldLabel_Description, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription25,
                                    String.Format(Message.JdaFieldLabel_Description, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription26,
                                    String.Format(Message.JdaFieldLabel_Description, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription27,
                                    String.Format(Message.JdaFieldLabel_Description, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription28,
                                    String.Format(Message.JdaFieldLabel_Description, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription29,
                                    String.Format(Message.JdaFieldLabel_Description, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDescription30,
                                    String.Format(Message.JdaFieldLabel_Description, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue01, String.Format(Message.JdaFieldLabel_Value, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue02, String.Format(Message.JdaFieldLabel_Value, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue03, String.Format(Message.JdaFieldLabel_Value, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue04, String.Format(Message.JdaFieldLabel_Value, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue05, String.Format(Message.JdaFieldLabel_Value, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue06, String.Format(Message.JdaFieldLabel_Value, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue07, String.Format(Message.JdaFieldLabel_Value, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue08, String.Format(Message.JdaFieldLabel_Value, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue09, String.Format(Message.JdaFieldLabel_Value, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag01, String.Format(Message.JdaFieldLabel_Flag, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixturePositionId, Message.JdaFieldLabel_PositionId),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureFillPattern, Message.JdaFieldLabel_FillPattern),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureModelFilename, Message.JdaFieldLabel_ModelFileName),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureWeightCapacity, Message.JdaFieldLabel_WeightCapacity),
                    //{FixtureField150, NewObjectFieldInfo(FixtureField150, "Field150")},
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureDividerAtStart, Message.JdaFieldLabel_DividerStartAt),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureDividerAtEnd, Message.JdaFieldLabel_DividerAtEnd),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureDividerBetweenFacings,
                                    Message.JdaFieldLabel_DividerBetweenFacings),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureTransparency, Message.JdaFieldLabel_Transparency),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureHideIfPrinting, Message.JdaFieldLabel_HideIfPrinting),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureProductAssociation,
                                    Message.JdaFieldLabel_ProductAssociation),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixturePartId, Message.JdaFieldLabel_PartId),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture,
                                    ProSpaceImportHelper.FixtureHideViewDimensions,
                                    Message.JdaFieldLabel_HideViewDimensions),
                    NewObjectFieldInfo(Message.JdaFileSection_Fixture, ProSpaceImportHelper.FixtureGln, Message.JdaFieldLabel_Gln)
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeFixtureFieldInfos()
        {
            var fieldInfos = new List<ObjectFieldInfo>
                {
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentX, Message.JdaFieldLabel_X),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentWidth, Message.JdaFieldLabel_Width),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentY, Message.JdaFieldLabel_Y),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentHeight, Message.JdaFieldLabel_Height),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentZ, Message.JdaFieldLabel_Z),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentDepth, Message.JdaFieldLabel_Depth),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentAngle, Message.JdaFieldLabel_Angle),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentOffsetX,
                                    String.Format(Message.JdaFieldLabel_Offset, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentOffsetY,
                                    String.Format(Message.JdaFieldLabel_Offset, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentDoor, Message.JdaFieldLabel_Door),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDoorDirection,
                                    Message.JdaFieldLabel_DoorDirection),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription01,
                                    String.Format(Message.JdaFieldLabel_Description, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription02,
                                    String.Format(Message.JdaFieldLabel_Description, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription03,
                                    String.Format(Message.JdaFieldLabel_Description, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription04,
                                    String.Format(Message.JdaFieldLabel_Description, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription05,
                                    String.Format(Message.JdaFieldLabel_Description, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription06,
                                    String.Format(Message.JdaFieldLabel_Description, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription07,
                                    String.Format(Message.JdaFieldLabel_Description, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription08,
                                    String.Format(Message.JdaFieldLabel_Description, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription09,
                                    String.Format(Message.JdaFieldLabel_Description, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentDescription10,
                                    String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue01,
                                    String.Format(Message.JdaFieldLabel_Value, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue02,
                                    String.Format(Message.JdaFieldLabel_Value, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue03,
                                    String.Format(Message.JdaFieldLabel_Value, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue04,
                                    String.Format(Message.JdaFieldLabel_Value, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue05,
                                    String.Format(Message.JdaFieldLabel_Value, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue06,
                                    String.Format(Message.JdaFieldLabel_Value, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue07,
                                    String.Format(Message.JdaFieldLabel_Value, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue08,
                                    String.Format(Message.JdaFieldLabel_Value, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue09,
                                    String.Format(Message.JdaFieldLabel_Value, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentValue10,
                                    String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag01,
                                    String.Format(Message.JdaFieldLabel_Flag, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag02,
                                    String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag03,
                                    String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag04,
                                    String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag05,
                                    String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag06,
                                    String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag07,
                                    String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag08,
                                    String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag09,
                                    String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFlag10,
                                    String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFrameWidth,
                                    Message.JdaFieldLabel_FrameWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFrameHeight,
                                    Message.JdaFieldLabel_FrameHeight),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentNumber, Message.JdaFieldLabel_Number),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFrameFillColor,
                                    Message.JdaFieldLabel_FrameFillColour),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment,
                                    ProSpaceImportHelper.SegmentFrameFillPattern,
                                    Message.JdaFieldLabel_FrameFillPattern),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentPartId, Message.JdaFieldLabel_PartId),
                    NewObjectFieldInfo(Message.JdaFileSection_Segment, ProSpaceImportHelper.SegmentGln, Message.JdaFieldLabel_Gln)
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializeProductFieldInfos()
        {
            var fieldInfos =
                new List<ObjectFieldInfo>
                {
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductUpc, Message.JdaFieldLabel_Upc),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductId, Message.JdaFieldLabel_Id),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductName, Message.JdaFieldLabel_Name),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductKey, Message.JdaFieldLabel_Key),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductWidth, Message.JdaFieldLabel_Width),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductHeight, Message.JdaFieldLabel_Height),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDepth, Message.JdaFieldLabel_Depth),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductColor, Message.JdaFieldLabel_Color),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAbbrevName, Message.JdaFieldLabel_AbbrevName),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductSize, Message.JdaFieldLabel_Size),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductUom, Message.JdaFieldLabel_Uom),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductManufacturer, Message.JdaFieldLabel_Manufacturer),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCategory, Message.JdaFieldLabel_Category),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductSupplier, Message.JdaFieldLabel_Supplier),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductInnerPack, Message.JdaFieldLabel_InnerPack),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductXNesting, String.Format(Message.JdaFieldLabel_AxisNesting, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductYNesting, String.Format(Message.JdaFieldLabel_AxisNesting, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductZNesting, String.Format(Message.JdaFieldLabel_AxisNesting, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPegholes, Message.JdaFieldLabel_Pegholes),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPegholeX, String.Format(Message.JdaFieldLabel_PegholeAxis, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPegholeY, String.Format(Message.JdaFieldLabel_PegholeAxis, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPegholeWidth, Message.JdaFieldLabel_PegholeWidth),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPeghole2X, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_X, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPeghole2Y, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_Y, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPeghole2Width, String.Format(Message.JdaFieldLabel_ExtraPegholeWidth, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPeghole3X, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_X, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPeghole3Y, String.Format(Message.JdaFieldLabel_ExtraPegholeAxis, Message.JdaFieldLabel_Y, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPeghole3Width, String.Format(Message.JdaFieldLabel_ExtraPegholeWidth, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPackageStyle, Message.PackageStyle),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPegId, Message.JdaFieldLabel_PegId),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFingerSpaceY, String.Format(Message.JdaFieldLabel_FingerSpaceAxis, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductJumbleFactor, Message.JdaFieldLabel_JumbleFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPrice, Message.JdaFieldLabel_Price),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseCost, Message.JdaFieldLabel_CaseCost),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTaxCode, Message.JdaFieldLabel_TaxCode),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductUnitMovement, Message.JdaFieldLabel_UnitMovement),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductShare, Message.JdaFieldLabel_Share),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDaysSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCombinedPerformanceIndex, Message.JdaFieldLabel_CombinedPerformance),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPegSpan, Message.JdaFieldLabel_PegSpan),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMinimumUnits, Message.JdaFieldLabel_MinimumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMaximumUnits, Message.JdaFieldLabel_MaximumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductShapeId, Message.JdaFieldLabel_ShapeId),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductBitmapIdOverrride, Message.JdaFieldLabel_BitmapIdFileOverride),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTrayMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleTray)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCaseMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleCase)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDisplayMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleDispaly)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAlternateMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleAlternate)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseWidth, String.Format(Message.JdaFieldLabel_MerchandisingStyleWidth, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseHeight, String.Format(Message.JdaFieldLabel_MerchandisingStyleHeight, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseDepth, String.Format(Message.JdaFieldLabel_MerchandisingStyleDepth, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseNumberWide, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberWide, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseNumberHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberHigh, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseNumberDeep, String.Format(Message.JdaFieldLabel_MerchandisingStyleNumberDeep, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseTotalNumber, String.Format(Message.JdaFieldLabel_MerchandisingStyleTotalNumber, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductLooseMaxHigh, String.Format(Message.JdaFieldLabel_MerchandisingStyleMaxHigh, Message.JdaFieldLabel_MerchandisingStyleLoose)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchXMin, String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchXMax, String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchXUprights, String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchXCaps, String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchXPlacement, String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchXNumber, String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchXSize, String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_X)),
                    //{ProductField093, NewObjectFieldInfo(ProductField093, "Field 093")},
                    //{ProductField094, NewObjectFieldInfo(ProductField094, "Field 094")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchYMin, String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchYMax, String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchYUprights, String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchYCaps, String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchYPlacement, String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchYNumber, String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchYSize, String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Y)),
                    //{ProductField102, NewObjectFieldInfo(ProductField102, "Field 102")},
                    //{ProductField103, NewObjectFieldInfo(ProductField103, "Field 103")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchZMin, String.Format(Message.JdaFieldLabel_MerchAxisMin, Message.JdaFieldLabel_Z)), 
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchZMax, String.Format(Message.JdaFieldLabel_MerchAxisMax, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchZUprights, String.Format(Message.JdaFieldLabel_MerchAxisUprights, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchZCaps, String.Format(Message.JdaFieldLabel_MerchAxisCaps, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchZPlacement, String.Format(Message.JdaFieldLabel_MerchAxisPlacement, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchZNumber, String.Format(Message.JdaFieldLabel_MerchAxisNumber, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMerchZSize, String.Format(Message.JdaFieldLabel_MerchAxisSize, Message.JdaFieldLabel_Z)),
                    //{ProductField111, NewObjectFieldInfo(ProductField111, "Field 111")},
                    //{ProductField112, NewObjectFieldInfo(ProductField112, "Field 112")},
                    //{ProductField113, NewObjectFieldInfo(ProductField113, "Field 113")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription01, String.Format(Message.JdaFieldLabel_Description, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription02, String.Format(Message.JdaFieldLabel_Description, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription03, String.Format(Message.JdaFieldLabel_Description, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription04, String.Format(Message.JdaFieldLabel_Description, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription05, String.Format(Message.JdaFieldLabel_Description, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription06, String.Format(Message.JdaFieldLabel_Description, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription07, String.Format(Message.JdaFieldLabel_Description, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription08, String.Format(Message.JdaFieldLabel_Description, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription09, String.Format(Message.JdaFieldLabel_Description, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription10, String.Format(Message.JdaFieldLabel_Description, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription11, String.Format(Message.JdaFieldLabel_Description, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription12, String.Format(Message.JdaFieldLabel_Description, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription13, String.Format(Message.JdaFieldLabel_Description, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription14, String.Format(Message.JdaFieldLabel_Description, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription15, String.Format(Message.JdaFieldLabel_Description, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription16, String.Format(Message.JdaFieldLabel_Description, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription17, String.Format(Message.JdaFieldLabel_Description, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription18, String.Format(Message.JdaFieldLabel_Description, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription19, String.Format(Message.JdaFieldLabel_Description, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription20, String.Format(Message.JdaFieldLabel_Description, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription21, String.Format(Message.JdaFieldLabel_Description, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription22, String.Format(Message.JdaFieldLabel_Description, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription23, String.Format(Message.JdaFieldLabel_Description, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription24, String.Format(Message.JdaFieldLabel_Description, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription25, String.Format(Message.JdaFieldLabel_Description, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription26, String.Format(Message.JdaFieldLabel_Description, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription27, String.Format(Message.JdaFieldLabel_Description, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription28, String.Format(Message.JdaFieldLabel_Description, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription29, String.Format(Message.JdaFieldLabel_Description, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription30, String.Format(Message.JdaFieldLabel_Description, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription31, String.Format(Message.JdaFieldLabel_Description, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription32, String.Format(Message.JdaFieldLabel_Description, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription33, String.Format(Message.JdaFieldLabel_Description, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription34, String.Format(Message.JdaFieldLabel_Description, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription35, String.Format(Message.JdaFieldLabel_Description, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription36, String.Format(Message.JdaFieldLabel_Description, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription37, String.Format(Message.JdaFieldLabel_Description, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription38, String.Format(Message.JdaFieldLabel_Description, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription39, String.Format(Message.JdaFieldLabel_Description, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription40, String.Format(Message.JdaFieldLabel_Description, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription41, String.Format(Message.JdaFieldLabel_Description, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription42, String.Format(Message.JdaFieldLabel_Description, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription43, String.Format(Message.JdaFieldLabel_Description, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription44, String.Format(Message.JdaFieldLabel_Description, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription45, String.Format(Message.JdaFieldLabel_Description, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription46, String.Format(Message.JdaFieldLabel_Description, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription47, String.Format(Message.JdaFieldLabel_Description, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription48, String.Format(Message.JdaFieldLabel_Description, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription49, String.Format(Message.JdaFieldLabel_Description, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDescription50, String.Format(Message.JdaFieldLabel_Description, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue01, String.Format(Message.JdaFieldLabel_Value, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue02, String.Format(Message.JdaFieldLabel_Value, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue03, String.Format(Message.JdaFieldLabel_Value, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue04, String.Format(Message.JdaFieldLabel_Value, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue05, String.Format(Message.JdaFieldLabel_Value, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue06, String.Format(Message.JdaFieldLabel_Value, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue07, String.Format(Message.JdaFieldLabel_Value, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue08, String.Format(Message.JdaFieldLabel_Value, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue09, String.Format(Message.JdaFieldLabel_Value, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag01, String.Format(Message.JdaFieldLabel_Flag, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMinimumSqueezeFactorX, String.Format(Message.JdaFieldLabel_MinimumSqueezeFactor, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMinimumSqueezeFactorY, String.Format(Message.JdaFieldLabel_MinimumSqueezeFactor, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMinimumSqueezeFactorZ, String.Format(Message.JdaFieldLabel_MinimumSqueezeFactor, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMaximumSqueezeFactorX, String.Format(Message.JdaFieldLabel_MaximumSqueezeFactor, Message.JdaFieldLabel_X)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMaximumSqueezeFactorY, String.Format(Message.JdaFieldLabel_MaximumSqueezeFactor, Message.JdaFieldLabel_Y)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductMaximumSqueezeFactorZ, String.Format(Message.JdaFieldLabel_MaximumSqueezeFactor, Message.JdaFieldLabel_Z)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFillPattern, Message.JdaFieldLabel_FillPattern),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFieldModelFilename, Message.JdaFieldLabel_ModelFileName),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductBrand, Message.JdaFieldLabel_Brand),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductSubCategory, Message.JdaFieldLabel_SubCategory),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductWeight, Message.JdaFieldLabel_Weight),
                    //{ProductField235, NewObjectFieldInfo(ProductField235, "Field 235")},
                    //{ProductField236, NewObjectFieldInfo(ProductField236, "Field 236")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFrontOverhang, Message.JdaFieldLabel_FrontOverhang),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductFingerSpaceX, String.Format(Message.JdaFieldLabel_FingerSpaceAxis, Message.JdaFieldLabel_X)),
                    //{ProductField239, NewObjectFieldInfo(ProductField239, "Field 239")},
                    //{ProductField240, NewObjectFieldInfo(ProductField240, "Field 240")},
                    //{ProductField241, NewObjectFieldInfo(ProductField241, "Field 241")},
                    //{ProductField242, NewObjectFieldInfo(ProductField242, "Field 242")},
                    //{ProductField243, NewObjectFieldInfo(ProductField243, "Field 243")},
                    //{ProductField244, NewObjectFieldInfo(ProductField244, "Field 244")},
                    //{ProductField245, NewObjectFieldInfo(ProductField245, "Field 245")},
                    //{ProductField246, NewObjectFieldInfo(ProductField246, "Field 246")},
                    //{ProductField247, NewObjectFieldInfo(ProductField247, "Field 247")},
                    //{ProductField248, NewObjectFieldInfo(ProductField248, "Field 248")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductStatus, Message.JdaFieldLabel_Status),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDateCreated, Message.JdaFieldLabel_DateCreated),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDateModified, Message.JdaFieldLabel_DateModified),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDatePending, Message.JdaFieldLabel_DatePending),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDateEffective, Message.JdaFieldLabel_DateEffective),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDateFinished, Message.JdaFieldLabel_DateFinished),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDate1, String.Format(Message.JdaFieldLabel_Date, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDate2, String.Format(Message.JdaFieldLabel_Date, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDate3, String.Format(Message.JdaFieldLabel_Date, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductCreatedBy, Message.JdaFieldLabel_CreatedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductModifiedBy, Message.JdaFieldLabel_ModifiedBy),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductTransparency, Message.JdaFieldLabel_Transparency),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductBackRoomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductPartId, Message.JdaFieldLabel_PartId),
                    //{ProductField265, NewObjectFieldInfo(ProductField265, "Field 265")},
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductBitmapIdOverrideUnit, Message.JdaFieldLabel_BitmapIdOverrideUnit),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductModelFilenameLookup, Message.JdaFieldLabel_ModelFilenameLookup),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductDefaultMerchStyle, Message.JdaFieldLabel_MerchStyle),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductAutomaticModel, Message.JdaFieldLabel_AutomaticModel),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductNote1, String.Format(Message.JdaFieldLabel_Note, 1)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductNote2, String.Format(Message.JdaFieldLabel_Note, 2)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductNote3, String.Format(Message.JdaFieldLabel_Note, 3)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductNote4, String.Format(Message.JdaFieldLabel_Note, 4)),
                    NewObjectFieldInfo(Message.JdaFileSection_Product, ProSpaceImportHelper.ProductNote5, String.Format(Message.JdaFieldLabel_Note, 5))
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        private static Dictionary<String, ObjectFieldInfo> InitializePerformanceFieldInfos()
        {
            var fieldInfos =
                new List<ObjectFieldInfo>
                {
                    //NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceSales, Message.JdaFieldLabel_Sales), // don't export Caclulated field
                    //NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceProfit, Message.JdaFieldLabel_Profit), // don't export Caclulated field
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceTaxCode, Message.JdaFieldLabel_TaxCode),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceUnitMovement, Message.JdaFieldLabel_UnitMovement),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceShare, Message.JdaFieldLabel_Share),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformancePerformanceIndex, Message.JdaFieldLabel_PerformanceIndex),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue01, String.Format(Message.JdaFieldLabel_Value, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue02, String.Format(Message.JdaFieldLabel_Value, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue03, String.Format(Message.JdaFieldLabel_Value, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue04, String.Format(Message.JdaFieldLabel_Value, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue05, String.Format(Message.JdaFieldLabel_Value, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue06, String.Format(Message.JdaFieldLabel_Value, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue07, String.Format(Message.JdaFieldLabel_Value, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue08, String.Format(Message.JdaFieldLabel_Value, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue09, String.Format(Message.JdaFieldLabel_Value, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue10, String.Format(Message.JdaFieldLabel_Value, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag01, String.Format(Message.JdaFieldLabel_Flag, 01)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag02, String.Format(Message.JdaFieldLabel_Flag, 02)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag03, String.Format(Message.JdaFieldLabel_Flag, 03)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag04, String.Format(Message.JdaFieldLabel_Flag, 04)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag05, String.Format(Message.JdaFieldLabel_Flag, 05)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag06, String.Format(Message.JdaFieldLabel_Flag, 06)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag07, String.Format(Message.JdaFieldLabel_Flag, 07)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag08, String.Format(Message.JdaFieldLabel_Flag, 08)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag09, String.Format(Message.JdaFieldLabel_Flag, 09)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFlag10, String.Format(Message.JdaFieldLabel_Flag, 10)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceFacings, Message.JdaFieldLabel_Facings),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue11, String.Format(Message.JdaFieldLabel_Value, 11)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue12, String.Format(Message.JdaFieldLabel_Value, 12)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue13, String.Format(Message.JdaFieldLabel_Value, 13)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue14, String.Format(Message.JdaFieldLabel_Value, 14)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue15, String.Format(Message.JdaFieldLabel_Value, 15)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue16, String.Format(Message.JdaFieldLabel_Value, 16)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue17, String.Format(Message.JdaFieldLabel_Value, 17)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue18, String.Format(Message.JdaFieldLabel_Value, 18)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue19, String.Format(Message.JdaFieldLabel_Value, 19)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue20, String.Format(Message.JdaFieldLabel_Value, 20)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue21, String.Format(Message.JdaFieldLabel_Value, 21)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue22, String.Format(Message.JdaFieldLabel_Value, 22)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue23, String.Format(Message.JdaFieldLabel_Value, 23)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue24, String.Format(Message.JdaFieldLabel_Value, 24)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue25, String.Format(Message.JdaFieldLabel_Value, 25)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue26, String.Format(Message.JdaFieldLabel_Value, 26)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue27, String.Format(Message.JdaFieldLabel_Value, 27)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue28, String.Format(Message.JdaFieldLabel_Value, 28)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue29, String.Format(Message.JdaFieldLabel_Value, 29)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue30, String.Format(Message.JdaFieldLabel_Value, 30)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceCaseMultiple, Message.JdaFieldLabel_CaseMultiple),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceDaysSupply, Message.JdaFieldLabel_DaysOfSupply),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformancePeakSafetyFactor, Message.JdaFieldLabel_PeakSafetyFactor),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceBackroomStock, Message.JdaFieldLabel_BackroomStock),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceMinimumUnits, Message.JdaFieldLabel_MinimumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceMaximumUnits, Message.JdaFieldLabel_MaximumUnits),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceDeliverySchedule, Message.JdaFieldLabel_DeliverySchedule),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceReplenishmentMin, Message.JdaFieldLabel_ReplenishmentMin),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceReplenishmentMax, Message.JdaFieldLabel_ReplenishmentMax),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceCpiRank, Message.JdaFieldLabel_CpiRank),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceRecommendedFacings, Message.JdaFieldLabel_RecommendedFacings),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceAssortmentStrategy, Message.JdaFieldLabel_AssortmentStrategy),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceAssortmentTactic, Message.JdaFieldLabel_AssortmentTactic),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceAssortmentReason, Message.JdaFieldLabel_AssortmentReason),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceAssortmentAction, Message.JdaFieldLabel_AssortmentAction),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceTargetDistributionStores, Message.JdaFieldLabel_TargetDistributionStores),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceTargetDistribution, Message.JdaFieldLabel_TargetDistribution),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue31, String.Format(Message.JdaFieldLabel_Value, 31)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue32, String.Format(Message.JdaFieldLabel_Value, 32)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue33, String.Format(Message.JdaFieldLabel_Value, 33)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue34, String.Format(Message.JdaFieldLabel_Value, 34)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue35, String.Format(Message.JdaFieldLabel_Value, 35)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue36, String.Format(Message.JdaFieldLabel_Value, 36)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue37, String.Format(Message.JdaFieldLabel_Value, 37)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue38, String.Format(Message.JdaFieldLabel_Value, 38)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue39, String.Format(Message.JdaFieldLabel_Value, 39)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue40, String.Format(Message.JdaFieldLabel_Value, 40)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue41, String.Format(Message.JdaFieldLabel_Value, 41)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue42, String.Format(Message.JdaFieldLabel_Value, 42)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue43, String.Format(Message.JdaFieldLabel_Value, 43)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue44, String.Format(Message.JdaFieldLabel_Value, 44)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue45, String.Format(Message.JdaFieldLabel_Value, 45)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue46, String.Format(Message.JdaFieldLabel_Value, 46)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue47, String.Format(Message.JdaFieldLabel_Value, 47)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue48, String.Format(Message.JdaFieldLabel_Value, 48)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue49, String.Format(Message.JdaFieldLabel_Value, 49)),
                    NewObjectFieldInfo(Message.JdaFileSection_Performance, ProSpaceImportHelper.PerformanceValue50, String.Format(Message.JdaFieldLabel_Value, 50)),
                };
            return fieldInfos.OrderBy(info => info.PropertyFriendlyName).ToDictionary(info => info.PropertyName);
        }

        #endregion

        #region Default Enumerations

        /// <summary>
        ///     Enumerate the default Mappings.
        /// </summary>
        /// <returns>The mappings as an IEnumerable</returns>
        public static IEnumerable<Tuple<String, String>> EnumerateDefaultMappings(PlanogramFieldMappingType value)
        {
            switch (value)
            {
                case PlanogramFieldMappingType.Planogram:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultPlanogramMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Fixture:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultFixtureMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Component:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultComponentMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Product:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultProductMappings())
                        yield return mapping;
                    yield break;
                case PlanogramFieldMappingType.Performance:
                    foreach (Tuple<String, String> mapping in EnumerateDefaultPerformanceMappings())
                        yield return mapping;
                    yield break;
                default:
                    Debug.Fail("Unknown PlanogramFieldMappingType when calling EnumerateDefaultMappings in JDAExportHelper.");
                    yield break;
            }
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultPlanogramMappings()
        {
            //yield return CreateMappingTuple(Planogram.NameProperty.Name, PlanogramName);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text1Property.Name, ProSpaceImportHelper.PlanogramDescription01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text2Property.Name, ProSpaceImportHelper.PlanogramDescription02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text3Property.Name, ProSpaceImportHelper.PlanogramDescription03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text4Property.Name, ProSpaceImportHelper.PlanogramDescription04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text5Property.Name, ProSpaceImportHelper.PlanogramDescription05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text6Property.Name, ProSpaceImportHelper.PlanogramDescription06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text7Property.Name, ProSpaceImportHelper.PlanogramDescription07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text8Property.Name, ProSpaceImportHelper.PlanogramDescription08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text9Property.Name, ProSpaceImportHelper.PlanogramDescription09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text10Property.Name, ProSpaceImportHelper.PlanogramDescription10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text11Property.Name, ProSpaceImportHelper.PlanogramDescription11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text12Property.Name, ProSpaceImportHelper.PlanogramDescription12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text13Property.Name, ProSpaceImportHelper.PlanogramDescription13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text14Property.Name, ProSpaceImportHelper.PlanogramDescription14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text15Property.Name, ProSpaceImportHelper.PlanogramDescription15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text16Property.Name, ProSpaceImportHelper.PlanogramDescription16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text17Property.Name, ProSpaceImportHelper.PlanogramDescription17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text18Property.Name, ProSpaceImportHelper.PlanogramDescription18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text19Property.Name, ProSpaceImportHelper.PlanogramDescription19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text20Property.Name, ProSpaceImportHelper.PlanogramDescription20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text21Property.Name, ProSpaceImportHelper.PlanogramDescription21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text22Property.Name, ProSpaceImportHelper.PlanogramDescription22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text23Property.Name, ProSpaceImportHelper.PlanogramDescription23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text24Property.Name, ProSpaceImportHelper.PlanogramDescription24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text25Property.Name, ProSpaceImportHelper.PlanogramDescription25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text26Property.Name, ProSpaceImportHelper.PlanogramDescription26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text27Property.Name, ProSpaceImportHelper.PlanogramDescription27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text28Property.Name, ProSpaceImportHelper.PlanogramDescription28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text29Property.Name, ProSpaceImportHelper.PlanogramDescription29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text30Property.Name, ProSpaceImportHelper.PlanogramDescription30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text31Property.Name, ProSpaceImportHelper.PlanogramDescription31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text32Property.Name, ProSpaceImportHelper.PlanogramDescription32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text33Property.Name, ProSpaceImportHelper.PlanogramDescription33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text34Property.Name, ProSpaceImportHelper.PlanogramDescription34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text35Property.Name, ProSpaceImportHelper.PlanogramDescription35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text36Property.Name, ProSpaceImportHelper.PlanogramDescription36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text37Property.Name, ProSpaceImportHelper.PlanogramDescription37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text38Property.Name, ProSpaceImportHelper.PlanogramDescription38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text39Property.Name, ProSpaceImportHelper.PlanogramDescription39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text40Property.Name, ProSpaceImportHelper.PlanogramDescription40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text41Property.Name, ProSpaceImportHelper.PlanogramDescription41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text42Property.Name, ProSpaceImportHelper.PlanogramDescription42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text43Property.Name, ProSpaceImportHelper.PlanogramDescription43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text44Property.Name, ProSpaceImportHelper.PlanogramDescription44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text45Property.Name, ProSpaceImportHelper.PlanogramDescription45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text46Property.Name, ProSpaceImportHelper.PlanogramDescription46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text47Property.Name, ProSpaceImportHelper.PlanogramDescription47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text48Property.Name, ProSpaceImportHelper.PlanogramDescription48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text49Property.Name, ProSpaceImportHelper.PlanogramDescription49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text50Property.Name, ProSpaceImportHelper.PlanogramDescription50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value1Property.Name, ProSpaceImportHelper.PlanogramValue01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value2Property.Name, ProSpaceImportHelper.PlanogramValue02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value3Property.Name, ProSpaceImportHelper.PlanogramValue03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value4Property.Name, ProSpaceImportHelper.PlanogramValue04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value5Property.Name, ProSpaceImportHelper.PlanogramValue05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value6Property.Name, ProSpaceImportHelper.PlanogramValue06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value7Property.Name, ProSpaceImportHelper.PlanogramValue07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value8Property.Name, ProSpaceImportHelper.PlanogramValue08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value9Property.Name, ProSpaceImportHelper.PlanogramValue09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value10Property.Name, ProSpaceImportHelper.PlanogramValue10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value11Property.Name, ProSpaceImportHelper.PlanogramValue11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value12Property.Name, ProSpaceImportHelper.PlanogramValue12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value13Property.Name, ProSpaceImportHelper.PlanogramValue13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value14Property.Name, ProSpaceImportHelper.PlanogramValue14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value15Property.Name, ProSpaceImportHelper.PlanogramValue15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value16Property.Name, ProSpaceImportHelper.PlanogramValue16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value17Property.Name, ProSpaceImportHelper.PlanogramValue17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value18Property.Name, ProSpaceImportHelper.PlanogramValue18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value19Property.Name, ProSpaceImportHelper.PlanogramValue19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value20Property.Name, ProSpaceImportHelper.PlanogramValue20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value21Property.Name, ProSpaceImportHelper.PlanogramValue21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value22Property.Name, ProSpaceImportHelper.PlanogramValue22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value23Property.Name, ProSpaceImportHelper.PlanogramValue23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value24Property.Name, ProSpaceImportHelper.PlanogramValue24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value25Property.Name, ProSpaceImportHelper.PlanogramValue25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value26Property.Name, ProSpaceImportHelper.PlanogramValue26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value27Property.Name, ProSpaceImportHelper.PlanogramValue27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value28Property.Name, ProSpaceImportHelper.PlanogramValue28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value29Property.Name, ProSpaceImportHelper.PlanogramValue29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value30Property.Name, ProSpaceImportHelper.PlanogramValue30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value31Property.Name, ProSpaceImportHelper.PlanogramValue31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value32Property.Name, ProSpaceImportHelper.PlanogramValue32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value33Property.Name, ProSpaceImportHelper.PlanogramValue33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value34Property.Name, ProSpaceImportHelper.PlanogramValue34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value35Property.Name, ProSpaceImportHelper.PlanogramValue35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value36Property.Name, ProSpaceImportHelper.PlanogramValue36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value37Property.Name, ProSpaceImportHelper.PlanogramValue37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value38Property.Name, ProSpaceImportHelper.PlanogramValue38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value39Property.Name, ProSpaceImportHelper.PlanogramValue39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value40Property.Name, ProSpaceImportHelper.PlanogramValue40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value41Property.Name, ProSpaceImportHelper.PlanogramValue41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value42Property.Name, ProSpaceImportHelper.PlanogramValue42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value43Property.Name, ProSpaceImportHelper.PlanogramValue43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value44Property.Name, ProSpaceImportHelper.PlanogramValue44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value45Property.Name, ProSpaceImportHelper.PlanogramValue45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value46Property.Name, ProSpaceImportHelper.PlanogramValue46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value47Property.Name, ProSpaceImportHelper.PlanogramValue47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value48Property.Name, ProSpaceImportHelper.PlanogramValue48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value49Property.Name, ProSpaceImportHelper.PlanogramValue49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value50Property.Name, ProSpaceImportHelper.PlanogramValue50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag1Property.Name, ProSpaceImportHelper.PlanogramFlag01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag2Property.Name, ProSpaceImportHelper.PlanogramFlag02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag3Property.Name, ProSpaceImportHelper.PlanogramFlag03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag4Property.Name, ProSpaceImportHelper.PlanogramFlag04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag5Property.Name, ProSpaceImportHelper.PlanogramFlag05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag6Property.Name, ProSpaceImportHelper.PlanogramFlag06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag7Property.Name, ProSpaceImportHelper.PlanogramFlag07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag8Property.Name, ProSpaceImportHelper.PlanogramFlag08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag9Property.Name, ProSpaceImportHelper.PlanogramFlag09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag10Property.Name, ProSpaceImportHelper.PlanogramFlag10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date1Property.Name, ProSpaceImportHelper.PlanogramDate1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date2Property.Name, ProSpaceImportHelper.PlanogramDate2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date3Property.Name, ProSpaceImportHelper.PlanogramDate3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note1Property.Name, ProSpaceImportHelper.PlanogramNote1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note2Property.Name, ProSpaceImportHelper.PlanogramNote2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note3Property.Name, ProSpaceImportHelper.PlanogramNote3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note4Property.Name, ProSpaceImportHelper.PlanogramNote4);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note5Property.Name, ProSpaceImportHelper.PlanogramNote5);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultFixtureMappings()
        {
            yield return CreateMappingTuple(PlanogramFixture.NameProperty.Name, ProSpaceImportHelper.SegmentName);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultComponentMappings()
        {
            yield return CreateMappingTuple(PlanogramComponent.NameProperty.Name, ProSpaceImportHelper.FixtureName);
            yield return CreateMappingTuple(PlanogramComponent.ManufacturerPartNumberProperty.Name, ProSpaceImportHelper.FixturePartId);
            yield return CreateMappingTuple(PlanogramComponent.WeightLimitProperty.Name, ProSpaceImportHelper.FixtureWeightCapacity);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text1Property.Name, ProSpaceImportHelper.FixtureDescription01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text2Property.Name, ProSpaceImportHelper.FixtureDescription02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text3Property.Name, ProSpaceImportHelper.FixtureDescription03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text4Property.Name, ProSpaceImportHelper.FixtureDescription04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text5Property.Name, ProSpaceImportHelper.FixtureDescription05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text6Property.Name, ProSpaceImportHelper.FixtureDescription06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text7Property.Name, ProSpaceImportHelper.FixtureDescription07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text8Property.Name, ProSpaceImportHelper.FixtureDescription08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text9Property.Name, ProSpaceImportHelper.FixtureDescription09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text10Property.Name, ProSpaceImportHelper.FixtureDescription10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text11Property.Name, ProSpaceImportHelper.FixtureDescription11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text12Property.Name, ProSpaceImportHelper.FixtureDescription12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text13Property.Name, ProSpaceImportHelper.FixtureDescription13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text14Property.Name, ProSpaceImportHelper.FixtureDescription14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text15Property.Name, ProSpaceImportHelper.FixtureDescription15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text16Property.Name, ProSpaceImportHelper.FixtureDescription16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text17Property.Name, ProSpaceImportHelper.FixtureDescription17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text18Property.Name, ProSpaceImportHelper.FixtureDescription18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text19Property.Name, ProSpaceImportHelper.FixtureDescription19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text20Property.Name, ProSpaceImportHelper.FixtureDescription20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text21Property.Name, ProSpaceImportHelper.FixtureDescription21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text22Property.Name, ProSpaceImportHelper.FixtureDescription22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text23Property.Name, ProSpaceImportHelper.FixtureDescription23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text24Property.Name, ProSpaceImportHelper.FixtureDescription24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text25Property.Name, ProSpaceImportHelper.FixtureDescription25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text26Property.Name, ProSpaceImportHelper.FixtureDescription26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text27Property.Name, ProSpaceImportHelper.FixtureDescription27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text28Property.Name, ProSpaceImportHelper.FixtureDescription28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text29Property.Name, ProSpaceImportHelper.FixtureDescription29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text30Property.Name, ProSpaceImportHelper.FixtureDescription30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value1Property.Name, ProSpaceImportHelper.FixtureValue01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value2Property.Name, ProSpaceImportHelper.FixtureValue02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value3Property.Name, ProSpaceImportHelper.FixtureValue03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value4Property.Name, ProSpaceImportHelper.FixtureValue04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value5Property.Name, ProSpaceImportHelper.FixtureValue05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value6Property.Name, ProSpaceImportHelper.FixtureValue06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value7Property.Name, ProSpaceImportHelper.FixtureValue07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value8Property.Name, ProSpaceImportHelper.FixtureValue08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value9Property.Name, ProSpaceImportHelper.FixtureValue09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value10Property.Name, ProSpaceImportHelper.FixtureValue10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value11Property.Name, ProSpaceImportHelper.FixtureValue11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value12Property.Name, ProSpaceImportHelper.FixtureValue12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value13Property.Name, ProSpaceImportHelper.FixtureValue13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value14Property.Name, ProSpaceImportHelper.FixtureValue14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value15Property.Name, ProSpaceImportHelper.FixtureValue15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value16Property.Name, ProSpaceImportHelper.FixtureValue16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value17Property.Name, ProSpaceImportHelper.FixtureValue17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value18Property.Name, ProSpaceImportHelper.FixtureValue18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value19Property.Name, ProSpaceImportHelper.FixtureValue19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value20Property.Name, ProSpaceImportHelper.FixtureValue20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value21Property.Name, ProSpaceImportHelper.FixtureValue21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value22Property.Name, ProSpaceImportHelper.FixtureValue22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value23Property.Name, ProSpaceImportHelper.FixtureValue23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value24Property.Name, ProSpaceImportHelper.FixtureValue24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value25Property.Name, ProSpaceImportHelper.FixtureValue25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value26Property.Name, ProSpaceImportHelper.FixtureValue26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value27Property.Name, ProSpaceImportHelper.FixtureValue27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value28Property.Name, ProSpaceImportHelper.FixtureValue28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value29Property.Name, ProSpaceImportHelper.FixtureValue29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value30Property.Name, ProSpaceImportHelper.FixtureValue30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag1Property.Name, ProSpaceImportHelper.FixtureFlag01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag2Property.Name, ProSpaceImportHelper.FixtureFlag02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag3Property.Name, ProSpaceImportHelper.FixtureFlag03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag4Property.Name, ProSpaceImportHelper.FixtureFlag04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag5Property.Name, ProSpaceImportHelper.FixtureFlag05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag6Property.Name, ProSpaceImportHelper.FixtureFlag06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag7Property.Name, ProSpaceImportHelper.FixtureFlag07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag8Property.Name, ProSpaceImportHelper.FixtureFlag08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag9Property.Name, ProSpaceImportHelper.FixtureFlag09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag10Property.Name, ProSpaceImportHelper.FixtureFlag10);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultProductMappings()
        {
            yield return CreateMappingTuple(PlanogramProduct.NameProperty.Name, ProSpaceImportHelper.ProductName);
            yield return CreateMappingTuple(PlanogramProduct.GtinProperty.Name, ProSpaceImportHelper.ProductUpc);
            yield return CreateMappingTuple(PlanogramProduct.BrandProperty.Name, ProSpaceImportHelper.ProductBrand);
            yield return CreateMappingTuple(PlanogramProduct.SubcategoryProperty.Name, ProSpaceImportHelper.ProductSubCategory);
            yield return CreateMappingTuple(PlanogramProduct.VendorProperty.Name, ProSpaceImportHelper.ProductSupplier);
            yield return CreateMappingTuple(PlanogramProduct.ManufacturerProperty.Name, ProSpaceImportHelper.ProductManufacturer);
            yield return CreateMappingTuple(PlanogramProduct.SizeProperty.Name, ProSpaceImportHelper.ProductSize);
            yield return CreateMappingTuple(PlanogramProduct.UnitOfMeasureProperty.Name, ProSpaceImportHelper.ProductUom);
            yield return CreateMappingTuple(PlanogramProduct.SellPriceProperty.Name, ProSpaceImportHelper.ProductPrice);
            yield return CreateMappingTuple(PlanogramProduct.CaseCostProperty.Name, ProSpaceImportHelper.ProductCaseCost);
            yield return CreateMappingTuple(PlanogramProduct.SellPackCountProperty.Name, ProSpaceImportHelper.ProductInnerPack);
            yield return CreateMappingTuple(PlanogramProduct.CasePackUnitsProperty.Name, ProSpaceImportHelper.ProductCaseTotalNumber);

            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text1Property.Name, ProSpaceImportHelper.ProductDescription01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text2Property.Name, ProSpaceImportHelper.ProductDescription02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text3Property.Name, ProSpaceImportHelper.ProductDescription03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text4Property.Name, ProSpaceImportHelper.ProductDescription04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text5Property.Name, ProSpaceImportHelper.ProductDescription05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text6Property.Name, ProSpaceImportHelper.ProductDescription06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text7Property.Name, ProSpaceImportHelper.ProductDescription07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text8Property.Name, ProSpaceImportHelper.ProductDescription08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text9Property.Name, ProSpaceImportHelper.ProductDescription09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text10Property.Name, ProSpaceImportHelper.ProductDescription10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text11Property.Name, ProSpaceImportHelper.ProductDescription11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text12Property.Name, ProSpaceImportHelper.ProductDescription12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text13Property.Name, ProSpaceImportHelper.ProductDescription13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text14Property.Name, ProSpaceImportHelper.ProductDescription14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text15Property.Name, ProSpaceImportHelper.ProductDescription15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text16Property.Name, ProSpaceImportHelper.ProductDescription16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text17Property.Name, ProSpaceImportHelper.ProductDescription17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text18Property.Name, ProSpaceImportHelper.ProductDescription18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text19Property.Name, ProSpaceImportHelper.ProductDescription19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text20Property.Name, ProSpaceImportHelper.ProductDescription20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text21Property.Name, ProSpaceImportHelper.ProductDescription21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text22Property.Name, ProSpaceImportHelper.ProductDescription22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text23Property.Name, ProSpaceImportHelper.ProductDescription23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text24Property.Name, ProSpaceImportHelper.ProductDescription24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text25Property.Name, ProSpaceImportHelper.ProductDescription25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text26Property.Name, ProSpaceImportHelper.ProductDescription26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text27Property.Name, ProSpaceImportHelper.ProductDescription27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text28Property.Name, ProSpaceImportHelper.ProductDescription28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text29Property.Name, ProSpaceImportHelper.ProductDescription29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text30Property.Name, ProSpaceImportHelper.ProductDescription30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text31Property.Name, ProSpaceImportHelper.ProductDescription31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text32Property.Name, ProSpaceImportHelper.ProductDescription32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text33Property.Name, ProSpaceImportHelper.ProductDescription33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text34Property.Name, ProSpaceImportHelper.ProductDescription34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text35Property.Name, ProSpaceImportHelper.ProductDescription35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text36Property.Name, ProSpaceImportHelper.ProductDescription36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text37Property.Name, ProSpaceImportHelper.ProductDescription37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text38Property.Name, ProSpaceImportHelper.ProductDescription38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text39Property.Name, ProSpaceImportHelper.ProductDescription39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text40Property.Name, ProSpaceImportHelper.ProductDescription40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text41Property.Name, ProSpaceImportHelper.ProductDescription41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text42Property.Name, ProSpaceImportHelper.ProductDescription42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text43Property.Name, ProSpaceImportHelper.ProductDescription43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text44Property.Name, ProSpaceImportHelper.ProductDescription44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text45Property.Name, ProSpaceImportHelper.ProductDescription45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text46Property.Name, ProSpaceImportHelper.ProductDescription46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text47Property.Name, ProSpaceImportHelper.ProductDescription47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text48Property.Name, ProSpaceImportHelper.ProductDescription48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text49Property.Name, ProSpaceImportHelper.ProductDescription49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Text50Property.Name, ProSpaceImportHelper.ProductDescription50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value1Property.Name, ProSpaceImportHelper.ProductValue01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value2Property.Name, ProSpaceImportHelper.ProductValue02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value3Property.Name, ProSpaceImportHelper.ProductValue03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value4Property.Name, ProSpaceImportHelper.ProductValue04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value5Property.Name, ProSpaceImportHelper.ProductValue05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value6Property.Name, ProSpaceImportHelper.ProductValue06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value7Property.Name, ProSpaceImportHelper.ProductValue07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value8Property.Name, ProSpaceImportHelper.ProductValue08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value9Property.Name, ProSpaceImportHelper.ProductValue09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value10Property.Name, ProSpaceImportHelper.ProductValue10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value11Property.Name, ProSpaceImportHelper.ProductValue11);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value12Property.Name, ProSpaceImportHelper.ProductValue12);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value13Property.Name, ProSpaceImportHelper.ProductValue13);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value14Property.Name, ProSpaceImportHelper.ProductValue14);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value15Property.Name, ProSpaceImportHelper.ProductValue15);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value16Property.Name, ProSpaceImportHelper.ProductValue16);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value17Property.Name, ProSpaceImportHelper.ProductValue17);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value18Property.Name, ProSpaceImportHelper.ProductValue18);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value19Property.Name, ProSpaceImportHelper.ProductValue19);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value20Property.Name, ProSpaceImportHelper.ProductValue20);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value21Property.Name, ProSpaceImportHelper.ProductValue21);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value22Property.Name, ProSpaceImportHelper.ProductValue22);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value23Property.Name, ProSpaceImportHelper.ProductValue23);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value24Property.Name, ProSpaceImportHelper.ProductValue24);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value25Property.Name, ProSpaceImportHelper.ProductValue25);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value26Property.Name, ProSpaceImportHelper.ProductValue26);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value27Property.Name, ProSpaceImportHelper.ProductValue27);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value28Property.Name, ProSpaceImportHelper.ProductValue28);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value29Property.Name, ProSpaceImportHelper.ProductValue29);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value30Property.Name, ProSpaceImportHelper.ProductValue30);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value31Property.Name, ProSpaceImportHelper.ProductValue31);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value32Property.Name, ProSpaceImportHelper.ProductValue32);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value33Property.Name, ProSpaceImportHelper.ProductValue33);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value34Property.Name, ProSpaceImportHelper.ProductValue34);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value35Property.Name, ProSpaceImportHelper.ProductValue35);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value36Property.Name, ProSpaceImportHelper.ProductValue36);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value37Property.Name, ProSpaceImportHelper.ProductValue37);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value38Property.Name, ProSpaceImportHelper.ProductValue38);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value39Property.Name, ProSpaceImportHelper.ProductValue39);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value40Property.Name, ProSpaceImportHelper.ProductValue40);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value41Property.Name, ProSpaceImportHelper.ProductValue41);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value42Property.Name, ProSpaceImportHelper.ProductValue42);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value43Property.Name, ProSpaceImportHelper.ProductValue43);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value44Property.Name, ProSpaceImportHelper.ProductValue44);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value45Property.Name, ProSpaceImportHelper.ProductValue45);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value46Property.Name, ProSpaceImportHelper.ProductValue46);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value47Property.Name, ProSpaceImportHelper.ProductValue47);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value48Property.Name, ProSpaceImportHelper.ProductValue48);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value49Property.Name, ProSpaceImportHelper.ProductValue49);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Value50Property.Name, ProSpaceImportHelper.ProductValue50);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag1Property.Name, ProSpaceImportHelper.ProductFlag01);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag2Property.Name, ProSpaceImportHelper.ProductFlag02);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag3Property.Name, ProSpaceImportHelper.ProductFlag03);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag4Property.Name, ProSpaceImportHelper.ProductFlag04);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag5Property.Name, ProSpaceImportHelper.ProductFlag05);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag6Property.Name, ProSpaceImportHelper.ProductFlag06);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag7Property.Name, ProSpaceImportHelper.ProductFlag07);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag8Property.Name, ProSpaceImportHelper.ProductFlag08);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag9Property.Name, ProSpaceImportHelper.ProductFlag09);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Flag10Property.Name, ProSpaceImportHelper.ProductFlag10);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date1Property.Name, ProSpaceImportHelper.ProductDate1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date2Property.Name, ProSpaceImportHelper.ProductDate2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Date3Property.Name, ProSpaceImportHelper.ProductDate3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note1Property.Name, ProSpaceImportHelper.ProductNote1);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note2Property.Name, ProSpaceImportHelper.ProductNote2);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note3Property.Name, ProSpaceImportHelper.ProductNote3);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note4Property.Name, ProSpaceImportHelper.ProductNote4);
            yield return CreateCustomAttributeMappingTuple(CustomAttributeData.Note5Property.Name, ProSpaceImportHelper.ProductNote5);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateDefaultPerformanceMappings()
        {
            yield break;
        }

        public static IEnumerable<SpacePlanningImportHelper.MetricData> EnumerateDefaultMetrics()
        {
            yield break;
            //yield return
            //    new SpacePlanningImportHelper.MetricData(Message.PlanogramPerformance_DefaultMetric2_SalesVolume,
            //                                             Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume,
            //                                             ProSpaceImportHelper.PerformanceUnitMovement,
            //                                             2,
            //                                             PlanogramMetricType.Integer,
            //                                             PlanogramMetricSpecialType.RegularSalesUnits);
        }

        #endregion

        #region Object Field Info Creation

        private static ObjectFieldInfo NewObjectFieldInfo(String section, String field, String friendlyName)
        {
            return ObjectFieldInfo.NewObjectFieldInfo(typeof(Object),
                                                      section,
                                                      field,
                                                      friendlyName,
                                                      typeof(String),
                                                      ModelPropertyDisplayType.None);
        }

        private static Tuple<String, String> CreateMappingTuple(String targetPropertyName, String sourcePropertyName)
        {
            return new Tuple<String, String>(targetPropertyName, sourcePropertyName);
        }

        private static Tuple<String, String> CreateCustomAttributeMappingTuple(String targetPropertyName, String sourcePropertyName)
        {
            return new Tuple<String, String>(String.Format("{0}.{1}", PlanogramProduct.CustomAttributesProperty.Name, targetPropertyName),
                                             sourcePropertyName);
        }

        #endregion

        #endregion
    }
}
