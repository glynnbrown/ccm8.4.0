﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24971 : L.Ineson 
//     Created.
// V8-25531 : L.Ineson
//     Corrected issue with collision detection when placing blocks.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Helpers
{
    ///// <summary>
    ///// Denotes the block parts that make up a plan position.
    ///// </summary>
    //public enum PlanPositionBlockType
    //{
    //    Main,
    //    X,
    //    Y,
    //    Z
    //}

    /// <summary>
    /// Provides calculated information about the blocks which make up a position.
    /// </summary>
    public sealed class PlanogramPositionBlockHelper
    {
        #region Nested Classes

        //private class BlockRect3D
        //{
        //    public Single X { get; set; }
        //    public Single Y { get; set; }
        //    public Single Z { get; set; }
        //    public Single Height { get; set; }
        //    public Single Width { get; set; }
        //    public Single Depth { get; set; }

        //    private Boolean IntersectsWith(BlockRect3D rect)
        //    {
        //        return (rect.X <= (X + Width)) &&
        //           ((rect.X + rect.Width) >= X) &&
        //           (rect.Y <= (Y + Height)) &&
        //           ((rect.Y + rect.Height) >= Y) &&
        //           (rect.Z <= (Z + Depth)) &&
        //           ((rect.Z + rect.Depth) >= Z);
        //    }

        //    private BlockRect3D Intersect(BlockRect3D rect)
        //    {
        //        if (!this.IntersectsWith(rect))
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            BlockRect3D intersection = new BlockRect3D();

        //            intersection.X = Math.Max(X, rect.X);
        //            intersection.Y = Math.Max(Y, rect.Y);
        //            intersection.Z = Math.Max(Z, rect.Z);
        //            intersection.Width = Math.Min(X + Width, rect.X + rect.Width) - intersection.X;
        //            intersection.Height = Math.Min(Y + Height, rect.Y + rect.Height) - intersection.Y;
        //            intersection.Depth = Math.Min(Z + Depth, rect.Z + rect.Depth) - intersection.Z;

        //            return intersection;
        //        }
        //    }

        //    public BlockRect3D Union(BlockRect3D rect)
        //    {
        //        BlockRect3D unioned = new BlockRect3D();

        //        unioned.X = Math.Min(X, rect.X);
        //        unioned.Y = Math.Min(Y, rect.Y);
        //        unioned.Z = Math.Min(Z, rect.Z);
        //        unioned.Width = Math.Max(X + Width, rect.X + rect.Width) - unioned.X;
        //        unioned.Height = Math.Max(Y + Height, rect.Y + rect.Height) - unioned.Y;
        //        unioned.Depth = Math.Max(Z + Depth, rect.Z + rect.Depth) - unioned.Z;

        //        return unioned;
        //    }

        //    /// <summary>
        //    /// Returns true if the rectangle bounds collide.
        //    /// </summary>
        //    public Boolean CollidesWith(BlockRect3D rect)
        //    {
        //        //check if they actually collide rather than just contact.
        //        BlockRect3D intersection = Intersect(rect);
        //        if (intersection != null)
        //        {
        //            if (intersection.Width > 0
        //                && intersection.Height > 0
        //                && intersection.Depth > 0)
        //            {
        //                return true;
        //            }
        //        }

        //        return false;
        //    }
        //}

        #endregion

        #region Properties

        //Main Block
        public PlanogramProductMerchandisingStyle MainMerchStyle { get; private set; }
        public HeightWidthDepthValue MainUnitSize { get; private set; }

        public CoordinateValue MainCoordinates { get; private set; }
        public RotationValue MainRotation { get; private set; }
        public HeightWidthDepthValue MainRotatedUnitSize { get; private set; }
        public HeightWidthDepthValue MainTotalSize { get; private set; }

        //X Block
        public PlanogramProductMerchandisingStyle XMerchStyle { get; private set; }
        public HeightWidthDepthValue XUnitSize { get; private set; }

        public Boolean HasXBlock { get; private set; }
        public CoordinateValue XCoordinates { get; private set; }
        public RotationValue XRotation { get; private set; }
        public HeightWidthDepthValue XRotatedUnitSize { get; private set; }
        public HeightWidthDepthValue XTotalSize { get; private set; }

        //Y Block
        public PlanogramProductMerchandisingStyle YMerchStyle { get; private set; }
        public HeightWidthDepthValue YUnitSize { get; private set; }

        public Boolean HasYBlock { get; private set; }
        public CoordinateValue YCoordinates { get; private set; }
        public RotationValue YRotation { get; private set; }
        public HeightWidthDepthValue YRotatedUnitSize { get; private set; }
        public HeightWidthDepthValue YTotalSize { get; private set; }

        //Z Block
        public PlanogramProductMerchandisingStyle ZMerchStyle { get; private set; }
        public HeightWidthDepthValue ZUnitSize { get; private set; }

        public Boolean HasZBlock { get; private set; }
        public CoordinateValue ZStart { get; private set; }
        public RotationValue ZRotation { get; private set; }
        public HeightWidthDepthValue ZRotatedUnitSize { get; private set; }
        public HeightWidthDepthValue ZTotalSize { get; private set; }

        //General
        public HeightWidthDepthValue FacingSize { get; private set; }

        public HeightWidthDepthValue TotalSize { get; private set; }
        public HighWideDeepValue TotalUnits { get; private set; }
        public Int16 TotalUnitCount { get; private set; }

        #endregion

        #region Constructor

        public PlanogramPositionBlockHelper() { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Calculates information required to render the given position.
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static PlanogramPositionBlockHelper Calculate(PlanogramPosition pos, PlanogramProduct product, PlanogramSubComponent parentSubComponent)
        {
            PlanogramPositionBlockHelper helper = new PlanogramPositionBlockHelper();

            if (pos != null && product != null && parentSubComponent != null)
            {
                helper.LoadFromPositionModel(pos, product, parentSubComponent);

                helper.CalculateValues(pos, product);
            }

            return helper;
        }

        #endregion

        #region Methods

        private void LoadFromPositionModel(PlanogramPosition pos,
            PlanogramProduct product, PlanogramSubComponent parentSubComponent)
        {
            // Main Block
            this.MainMerchStyle = pos.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Main);
            this.MainUnitSize = product.GetUnorientatedUnitSize(this.MainMerchStyle);

            // X Block
            this.XMerchStyle = pos.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.X);
            this.XUnitSize = product.GetUnorientatedUnitSize(this.XMerchStyle);

            // Y Block
            this.YMerchStyle = pos.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Y);
            this.YUnitSize = product.GetUnorientatedUnitSize(this.YMerchStyle);

            //Z Block
            this.ZMerchStyle = pos.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Z);
            this.ZUnitSize = product.GetUnorientatedUnitSize(this.ZMerchStyle);

            #region Facing Space

            //FacingSpaceX
            Single spaceX = product.FingerSpaceToTheSide;

            if (parentSubComponent.IsDividerObstructionByFacing)
            {
                spaceX += parentSubComponent.DividerObstructionWidth;
            }

            if (parentSubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
            {
                Int32? merchConstraintRow = PlanogramHelpers.GetMerchConstraintRowNum(pos.Y, this.MainUnitSize.Height, product.PegY, parentSubComponent);
                if (merchConstraintRow.HasValue)
                {
                    Single rowSpacingX = (merchConstraintRow == 1) ?
                        parentSubComponent.MerchConstraintRow1SpacingX
                        : parentSubComponent.MerchConstraintRow2SpacingX;

                    if (rowSpacingX > 0)
                    {
                        spaceX = (rowSpacingX - ((this.MainUnitSize.Width + spaceX) % rowSpacingX));
                    }
                }
            }

            //FacingSpaceY
            Single spaceY = 0;

            if (parentSubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
            {
                spaceY += product.FingerSpaceAbove;

                Int32? merchConstraintRow = PlanogramHelpers.GetMerchConstraintRowNum(pos.Y, this.MainUnitSize.Height, product.PegY, parentSubComponent);
                if (merchConstraintRow.HasValue)
                {
                    Single rowSpacingY =
                        (merchConstraintRow == 1) ?
                        parentSubComponent.MerchConstraintRow1SpacingY
                        : parentSubComponent.MerchConstraintRow2SpacingY;

                    if (rowSpacingY > 0)
                    {
                        spaceY += (rowSpacingY - (this.MainUnitSize.Height % rowSpacingY));
                    }
                }
            }

            //FacingSpaceZ - TODO
            Single spaceZ = 0;

            this.FacingSize = new HeightWidthDepthValue(spaceY, spaceX, spaceZ);

            #endregion
        }

        private void CalculateValues(PlanogramPosition pos, PlanogramProduct product)
        {
            HighWideDeepValue units;

            #region Main Block

            units = new HighWideDeepValue(pos.FacingsHigh, pos.FacingsWide, pos.FacingsDeep);

            this.MainRotation = GetRotation(pos.OrientationType);

            if (this.MainMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                this.MainRotatedUnitSize = GetOrientatedSize(pos.OrientationType, product.TrayWidth, product.TrayHeight, product.TrayDepth);
            }
            else
            {
                this.MainRotatedUnitSize = GetOrientatedSize(pos.OrientationType, this.MainUnitSize.Width, this.MainUnitSize.Height, this.MainUnitSize.Depth);
            }

            this.MainTotalSize = new HeightWidthDepthValue(
                (units.High * this.MainRotatedUnitSize.Height) + ((units.High - 1) * this.FacingSize.Height),
                (units.Wide * this.MainRotatedUnitSize.Width) + ((units.Wide - 1) * this.FacingSize.Width),
                (units.Deep * this.MainRotatedUnitSize.Depth) + ((units.Deep - 1) * this.FacingSize.Depth));

            #endregion

            #region X Block
            units = new HighWideDeepValue(pos.FacingsXHigh, pos.FacingsXWide, pos.FacingsXDeep);

            if (units.High > 0 && units.Wide > 0 && units.Deep > 0)
            {
                this.HasXBlock = true;

                this.XRotation = GetRotation(pos.OrientationTypeX);

                if (this.XMerchStyle == PlanogramProductMerchandisingStyle.Tray)
                {
                    this.XRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeX, product.TrayWidth, product.TrayHeight, product.TrayDepth);
                }
                else
                {
                    this.XRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeX, this.XUnitSize.Width, this.XUnitSize.Height, this.XUnitSize.Depth);
                }

                this.XTotalSize = new HeightWidthDepthValue(
                    (units.High * this.XRotatedUnitSize.Height) + ((units.High - 1) * this.FacingSize.Height),
                    (units.Wide * this.XRotatedUnitSize.Width) + ((units.Wide - 1) * this.FacingSize.Width),
                    (units.Deep * this.XRotatedUnitSize.Depth) + ((units.Deep - 1) * this.FacingSize.Depth));
            }
            else
            {
                this.XRotation = new RotationValue();
                this.XRotatedUnitSize = new HeightWidthDepthValue();
                this.XTotalSize = new HeightWidthDepthValue();
            }
            #endregion

            #region Y Block

            units = new HighWideDeepValue(pos.FacingsYHigh, pos.FacingsYWide, pos.FacingsYDeep);
            if (units.High > 0 && units.Wide > 0 && units.Deep > 0)
            {
                this.HasYBlock = true;

                this.YRotation = GetRotation(pos.OrientationTypeY);

                if (this.YMerchStyle == PlanogramProductMerchandisingStyle.Tray)
                {
                    this.YRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeY, product.TrayWidth, product.TrayHeight, product.TrayDepth);
                }
                else
                {
                    this.YRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeY, this.YUnitSize.Width, this.YUnitSize.Height, this.YUnitSize.Depth);
                }


                this.YTotalSize = new HeightWidthDepthValue(
                    (units.High * this.YRotatedUnitSize.Height) + ((units.High - 1) * this.FacingSize.Height),
                    (units.Wide * this.YRotatedUnitSize.Width) + ((units.Wide - 1) * this.FacingSize.Width),
                    (units.Deep * this.YRotatedUnitSize.Depth) + ((units.Deep - 1) * this.FacingSize.Depth));
            }
            else
            {
                this.YRotation = new RotationValue();
                this.YRotatedUnitSize = new HeightWidthDepthValue();
                this.YTotalSize = new HeightWidthDepthValue();
            }
            #endregion

            #region Z Block

            units = new HighWideDeepValue(pos.FacingsZHigh, pos.FacingsZWide, pos.FacingsZDeep);

            if (units.High > 0 && units.Wide > 0 && units.Deep > 0)
            {
                this.HasZBlock = true;

                this.ZRotation = GetRotation(pos.OrientationTypeZ);

                if (this.ZMerchStyle == PlanogramProductMerchandisingStyle.Tray)
                {
                    this.ZRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeZ, product.TrayWidth, product.TrayHeight, product.TrayDepth);
                }
                else
                {
                    this.ZRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeZ, this.ZUnitSize.Width, this.ZUnitSize.Height, this.ZUnitSize.Depth);
                }


                this.ZTotalSize = new HeightWidthDepthValue(
                    (units.High * this.ZRotatedUnitSize.Height) + ((units.High - 1) * this.FacingSize.Height),
                    (units.Wide * this.ZRotatedUnitSize.Width) + ((units.Wide - 1) * this.FacingSize.Width),
                    (units.Deep * this.ZRotatedUnitSize.Depth) + ((units.Deep - 1) * this.FacingSize.Depth));
            }
            else
            {
                this.ZRotation = new RotationValue();
                this.ZRotatedUnitSize = new HeightWidthDepthValue();
                this.ZTotalSize = new HeightWidthDepthValue();
            }
            #endregion

            #region Start Positions

            Rect3D mainBlock = new Rect3D(
                (pos.IsXPlacedLeft) ? XTotalSize.Width : 0,
                (pos.IsYPlacedBottom) ? YTotalSize.Height : 0,
                (pos.IsZPlacedFront) ? 0 : ZTotalSize.Depth,
                MainTotalSize.Height,
                MainTotalSize.Width,
                MainTotalSize.Depth);

            Rect3D xBlock = new Rect3D(
                (pos.IsXPlacedLeft) ? 0 : MainTotalSize.Width,
                mainBlock.Y,
                (mainBlock.Z + MainTotalSize.Depth) - XTotalSize.Depth,
                XTotalSize.Height,
                XTotalSize.Width,
                XTotalSize.Depth);

            Rect3D yBlock = new Rect3D(
                mainBlock.X,
                (pos.IsYPlacedBottom) ? 0 : MainTotalSize.Height,
                (mainBlock.Z + MainTotalSize.Depth) - YTotalSize.Depth,
                YTotalSize.Height,
                YTotalSize.Width,
                YTotalSize.Depth);

            Rect3D zBlock = new Rect3D(
                mainBlock.X,
                mainBlock.Y,
                (pos.IsZPlacedFront) ? MainTotalSize.Depth : mainBlock.Z - ZTotalSize.Depth,
                ZTotalSize.Height,
                ZTotalSize.Width,
                ZTotalSize.Depth);

            if (!pos.IsXPlacedLeft)
            {
                //if z intersects with x shift x right
                if (xBlock.CollidesWith(zBlock))
                {
                    xBlock.X = (zBlock.X + zBlock.Width);
                }
            }

            if (pos.IsYPlacedBottom)
            {
                //see if x and z blocks can move down.
                xBlock.Y = 0;
                if (yBlock.CollidesWith(xBlock))
                {
                    xBlock.Y = mainBlock.Y;
                }

                zBlock.Y = 0;
                if (yBlock.CollidesWith(zBlock))
                {
                    zBlock.Y = mainBlock.Y;
                }
            }
            else
            {
                //push the y block up if it intersects with x or z.
                if (yBlock.CollidesWith(zBlock))
                {
                    yBlock.Y = (zBlock.Y + zBlock.Height);
                }

                if (yBlock.CollidesWith(xBlock))
                {
                    yBlock.Y = (xBlock.Y + xBlock.Height);
                }
            }

            Single minZ = new Single[] { mainBlock.Z, xBlock.Z, yBlock.Z, zBlock.Z }.Min();
            if (minZ < 0)
            {
                minZ = Math.Abs(minZ);
                mainBlock.Z += minZ;
                xBlock.Z += minZ;
                yBlock.Z += minZ;
                zBlock.Z += minZ;
            }


            Rect3D allBlocks = mainBlock;

            this.MainCoordinates = new CoordinateValue(mainBlock.X, mainBlock.Y, mainBlock.Z);

            if (this.HasXBlock)
            {
                allBlocks = allBlocks.Union(xBlock);
                this.XCoordinates = new CoordinateValue(xBlock.X, xBlock.Y, xBlock.Z);
            }
            if (this.HasYBlock)
            {
                allBlocks = allBlocks.Union(yBlock);
                this.YCoordinates = new CoordinateValue(yBlock.X, yBlock.Y, yBlock.Z);
            }
            if (this.HasZBlock)
            {
                allBlocks = allBlocks.Union(zBlock);
                this.ZStart = new CoordinateValue(zBlock.X, zBlock.Y, zBlock.Z);
            }
            this.TotalSize =
                new HeightWidthDepthValue(allBlocks.Height, allBlocks.Width, allBlocks.Depth);

            #endregion

           

            #region Total Units

            //Main
            Int16 mainUnitsHigh = pos.FacingsHigh;
            Int16 mainUnitsWide = pos.FacingsWide;
            Int16 mainUnitsDeep = pos.FacingsDeep;
            if (this.MainMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                mainUnitsHigh *= product.TrayHigh;
                mainUnitsWide *= product.TrayWide;
                mainUnitsDeep *= product.TrayDeep;
            }
            if (mainUnitsHigh == 0 || mainUnitsWide == 0 || mainUnitsDeep == 0)
            {
                mainUnitsHigh = 0;
                mainUnitsWide = 0;
                mainUnitsDeep = 0;
            }
            Int16 mainTotalUnits = Convert.ToInt16(mainUnitsHigh * mainUnitsWide * mainUnitsDeep);


            //X
            Int16 xUnitsHigh = pos.FacingsXHigh;
            Int16 xUnitsWide = pos.FacingsXWide;
            Int16 xUnitsDeep = pos.FacingsXDeep;
            if (this.XMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                xUnitsHigh *= product.TrayHigh;
                xUnitsWide *= product.TrayWide;
                xUnitsDeep *= product.TrayDeep;
            }
            if (xUnitsHigh == 0 || xUnitsWide == 0 || xUnitsDeep == 0)
            {
                xUnitsHigh = 0;
                xUnitsWide = 0;
                xUnitsDeep = 0;
            }
            Int16 xTotalUnits = Convert.ToInt16(xUnitsHigh * xUnitsWide * xUnitsDeep);


            //Y
            Int16 yUnitsHigh = pos.FacingsYHigh;
            Int16 yUnitsWide = pos.FacingsYWide;
            Int16 yUnitsDeep = pos.FacingsYDeep;
            if (this.YMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                yUnitsHigh *= product.TrayHigh;
                yUnitsWide *= product.TrayWide;
                yUnitsDeep *= product.TrayDeep;
            }
            if (yUnitsHigh == 0 || yUnitsWide == 0 || yUnitsDeep == 0)
            {
                yUnitsHigh = 0;
                yUnitsWide = 0;
                yUnitsDeep = 0;
            }
            Int16 yTotalUnits = Convert.ToInt16(yUnitsHigh * yUnitsWide * yUnitsDeep);



            //Z
            Int16 zUnitsHigh = pos.FacingsZHigh;
            Int16 zUnitsWide = pos.FacingsZWide;
            Int16 zUnitsDeep = pos.FacingsZDeep;
            if (this.ZMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                zUnitsHigh *= product.TrayHigh;
                zUnitsWide *= product.TrayWide;
                zUnitsDeep *= product.TrayDeep;
            }
            if (zUnitsHigh == 0 || zUnitsWide == 0 || zUnitsDeep == 0)
            {
                zUnitsHigh = 0;
                zUnitsWide = 0;
                zUnitsDeep = 0;
            }
            Int16 zTotalUnits = Convert.ToInt16(zUnitsHigh * zUnitsWide * zUnitsDeep);


            //calculate total units values too.
            Int16 totalUnitsHigh = Convert.ToInt16(new Int16[] { mainUnitsHigh, xUnitsHigh, zUnitsHigh }.Max() + yUnitsHigh);
            Int16 totalUnitsWide = Convert.ToInt16(new Int16[] { mainUnitsWide, yUnitsWide, zUnitsWide }.Max() + xUnitsWide);
            Int16 totalUnitsDeep = Convert.ToInt16(new Int16[] { mainUnitsDeep, xUnitsDeep, yUnitsDeep }.Max() + zUnitsDeep);
            this.TotalUnits = new HighWideDeepValue(totalUnitsHigh, totalUnitsWide, totalUnitsDeep);
            this.TotalUnitCount = Convert.ToInt16(mainTotalUnits + xTotalUnits + yTotalUnits + zTotalUnits);

            #endregion
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns the actual angle slope roll rotation for the given orientation type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static RotationValue GetRotation(PlanogramProductOrientationType type)
        {
            switch (type)
            {

                case PlanogramProductOrientationType.Front0:
                    return new RotationValue();


                case PlanogramProductOrientationType.Front90:
                    return new RotationValue()
                    {
                        Angle = 0,
                        Slope = 0,
                        Roll = ToRadians(90)
                    };

                case PlanogramProductOrientationType.Front180:
                    return new RotationValue()
                    {
                        Angle = 0,
                        Slope = 0,
                        Roll = ToRadians(180),
                    };

                case PlanogramProductOrientationType.Front270:
                    return new RotationValue()
                    {
                        Angle = 0,
                        Slope = 0,
                        Roll = ToRadians(270)
                    };


                case PlanogramProductOrientationType.Top0:
                    return new RotationValue()
                    {
                        Angle = 0,
                        Slope = ToRadians(270),
                        Roll = 0,
                    };


                case PlanogramProductOrientationType.Top90:
                    return new RotationValue()
                    {
                        Angle = ToRadians(90),
                        Slope = 0,
                        Roll = ToRadians(90),
                    };

                case PlanogramProductOrientationType.Top180:
                    return new RotationValue()
                    {
                        Angle = ToRadians(180),
                        Slope = ToRadians(90),
                        Roll = 0
                    };

                case PlanogramProductOrientationType.Top270:
                    return new RotationValue()
                    {
                        Angle = ToRadians(90),
                        Slope = ToRadians(180),
                        Roll = ToRadians(90),
                    };

                case PlanogramProductOrientationType.Right0:
                    return new RotationValue()
                    {
                        Angle = ToRadians(90),
                        Slope = 0,
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Right90:
                    return new RotationValue()
                    {
                        Angle = ToRadians(90),
                        Slope = ToRadians(90),
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Right180:
                    return new RotationValue()
                    {
                        Angle = ToRadians(270),
                        Slope = 0,
                        Roll = ToRadians(180),
                    };

                case PlanogramProductOrientationType.Right270:
                    return new RotationValue()
                    {
                        Angle = 0,
                        Slope = ToRadians(270),
                        Roll = ToRadians(270),
                    };

                case PlanogramProductOrientationType.Left0:
                    return new RotationValue()
                    {
                        Angle = ToRadians(270),
                        Slope = 0,
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Left90:
                    return new RotationValue()
                    {
                        Angle = ToRadians(270),
                        Slope = ToRadians(270),
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Left180:
                    return new RotationValue()
                    {
                        Angle = ToRadians(270),
                        Slope = ToRadians(180),
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Left270:
                    return new RotationValue()
                    {
                        Angle = ToRadians(270),
                        Slope = ToRadians(90),
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Back0:
                    return new RotationValue()
                    {
                        Angle = ToRadians(180),
                        Slope = 0,
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Back90:
                    return new RotationValue()
                    {
                        Angle = ToRadians(180),
                        Slope = 0,
                        Roll = ToRadians(270),
                    };

                case PlanogramProductOrientationType.Back180:
                    return new RotationValue()
                    {
                        Angle = ToRadians(180),
                        Slope = 0,
                        Roll = ToRadians(180),
                    };

                case PlanogramProductOrientationType.Back270:
                    return new RotationValue()
                    {
                        Angle = ToRadians(180),
                        Slope = 0,
                        Roll = ToRadians(90),
                    };

                case PlanogramProductOrientationType.Bottom0:
                    return new RotationValue()
                    {
                        Angle = 0,
                        Slope = ToRadians(90),
                        Roll = 0,
                    };

                case PlanogramProductOrientationType.Bottom90:
                    return new RotationValue()
                    {
                        Angle = ToRadians(270),
                        Slope = 0,
                        Roll = ToRadians(90),
                    };

                case PlanogramProductOrientationType.Bottom180:
                    return new RotationValue()
                    {
                        Angle = 0,
                        Slope = ToRadians(270),
                        Roll = ToRadians(180)
                    };

                case PlanogramProductOrientationType.Bottom270:
                    return new RotationValue()
                    {
                        Angle = ToRadians(90),
                        Slope = 0,
                        Roll = ToRadians(270),
                    };


                default: throw new NotImplementedException();
            }

        }

        /// <summary>
        /// Converts the given degree value to radians.
        /// </summary>
        /// <param name="degrees"></param>
        /// <returns></returns>
        private static Single ToRadians(Single degrees)
        {
            return Convert.ToSingle(degrees * (Math.PI / 180f));
        }

        /// <summary>
        /// Returns the item size orientated to the given orientation type.
        /// </summary>
        public static HeightWidthDepthValue GetOrientatedSize(PlanogramProductOrientationType orientationType,
            Single width, Single height, Single depth)
        {
            Single prodHeight = height;
            Single prodWidth = width;
            Single prodDepth = depth;

            switch (orientationType)
            {
                default:
                case PlanogramProductOrientationType.Front0:
                case PlanogramProductOrientationType.Back0:
                case PlanogramProductOrientationType.Front180:
                case PlanogramProductOrientationType.Back180:
                    return new HeightWidthDepthValue(prodHeight, prodWidth, prodDepth);

                case PlanogramProductOrientationType.Front90:
                case PlanogramProductOrientationType.Back90:
                case PlanogramProductOrientationType.Front270:
                case PlanogramProductOrientationType.Back270:
                    return new HeightWidthDepthValue(prodWidth, prodHeight, prodDepth);

                case PlanogramProductOrientationType.Right0:
                case PlanogramProductOrientationType.Left0:
                case PlanogramProductOrientationType.Right180:
                case PlanogramProductOrientationType.Left180:
                    return new HeightWidthDepthValue(prodHeight, prodDepth, prodWidth);

                case PlanogramProductOrientationType.Right90:
                case PlanogramProductOrientationType.Left90:
                case PlanogramProductOrientationType.Right270:
                case PlanogramProductOrientationType.Left270:
                    return new HeightWidthDepthValue(prodDepth, prodHeight, prodWidth);

                case PlanogramProductOrientationType.Top0:
                case PlanogramProductOrientationType.Bottom0:
                case PlanogramProductOrientationType.Top180:
                case PlanogramProductOrientationType.Bottom180:
                    return new HeightWidthDepthValue(prodDepth, prodWidth, prodHeight);

                case PlanogramProductOrientationType.Top90:
                case PlanogramProductOrientationType.Bottom90:
                case PlanogramProductOrientationType.Top270:
                case PlanogramProductOrientationType.Bottom270:
                    return new HeightWidthDepthValue(prodWidth, prodDepth, prodHeight);
            }
        }

        /// <summary>
        /// Gets the suggested right cap orientation for the given main 
        /// </summary>
        /// <param name="mainOrientation"></param>
        /// <returns></returns>
        public static PlanogramProductOrientationType SuggestRightCapOrientation(PlanogramProductOrientationType mainOrientation)
        {

            switch (mainOrientation)
            {
                case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Right0;
                case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Top90;
                case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Right180;
                case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Bottom270;
                case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Back270;
                case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Left270;
                case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Front270;
                case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Back0;
                case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Top180;
                case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Front180;
                case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Back180;
                case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Left0;
                case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Top270;
                case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Left180;
                case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Bottom90;
                case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Right90;
                case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Front90;
                case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Back90;
                case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Front0;
                case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Top0;
                case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Front180;
                case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Bottom0;

                default:
                    Debug.Fail("TODO");
                    return PlanogramProductOrientationType.Right0;

            }

        }

        /// <summary>
        /// Gets the suggested top cap orientation for the given main.
        /// </summary>
        /// <param name="mainOrientation"></param>
        /// <returns></returns>
        public static PlanogramProductOrientationType SuggestTopCapOrientation(PlanogramProductOrientationType mainOrientation)
        {
            switch (mainOrientation)
            {
                case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Top0;
                case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Left90;
                case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Top180;
                case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Front0;
                case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Left180;
                case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Front180;
                case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Right180;
                case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Top90;
                case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Front90;
                case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Bottom90;
                case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Front270;
                case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Bottom180;
                case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Right90;
                case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Bottom0;
                case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Front0;
                case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Left0;
                case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Back0;
                case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Right0;
                case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Top270;
                case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Back90;
                case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Bottom270;
                case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Front270;


                default:
                    Debug.Fail("TODO");
                    return PlanogramProductOrientationType.Top0;

            }
        }

        /// <summary>
        /// Returns a tuple per position facing. 
        /// Val1 = x start, Val2 = facing width.
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static List<Tuple<Single, Single>> GetFacingsXAndWidth(PlanogramPosition pos, PlanogramProduct product, PlanogramSubComponent parentSubComponent)
        {
            List<Tuple<Single, Single>> returnList = new List<Tuple<Single, Single>>();

            PlanogramPositionBlockHelper renderInfo = PlanogramPositionBlockHelper.Calculate(pos, product, parentSubComponent);
            Single unitSizeX;
            Single curX = 0;

            //left x block.
            if (renderInfo.HasXBlock && pos.IsXPlacedLeft)
            {
                unitSizeX = renderInfo.XRotatedUnitSize.Width;
                for (Int32 i = 0; i < pos.FacingsXWide; i++)
                {
                    returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
                    curX += unitSizeX;

                    //add spacing.
                    if (i <= pos.FacingsXWide - 1)
                    {
                        curX += renderInfo.FacingSize.Width;
                    }
                }
            }

            //main
            unitSizeX = renderInfo.MainRotatedUnitSize.Width;
            for (Int32 i = 0; i < pos.FacingsWide; i++)
            {
                returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
                curX += unitSizeX;

                //add spacing.
                if (i < pos.FacingsWide - 1)
                {
                    curX += renderInfo.FacingSize.Width;
                }
            }

            //right x block
            if (renderInfo.HasXBlock && !pos.IsXPlacedLeft)
            {
                //add another spacing
                curX += renderInfo.FacingSize.Width;

                unitSizeX = renderInfo.XRotatedUnitSize.Width;
                for (Int32 i = 0; i < pos.FacingsXWide; i++)
                {
                    returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
                    curX += unitSizeX;

                    //add spacing.
                    if (i < pos.FacingsXWide - 1)
                    {
                        curX += renderInfo.FacingSize.Width;
                    }
                }
            }

            return returnList;
        }

        

        #endregion

    }
}
