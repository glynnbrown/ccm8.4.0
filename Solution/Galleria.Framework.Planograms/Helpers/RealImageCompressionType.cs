#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28676 : A.Silva
//      Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Helpers
{
    public enum RealImageCompressionType
    {
        None = 0,
        Quarter = 25,
        Half = 50,
        ThreeQuarter = 75
    }

    /// <summary>
    ///     Helper class to work with <see cref="RealImageCompressionType"/> values.
    /// </summary>
    public static class RealImageCompressionTypeHelper
    {
        /// <summary>
        ///     Dictionary mapping each member of <see cref="RealImageCompressionType"/> to its localized name.
        /// </summary>
        public static Dictionary<RealImageCompressionType, String> FriendlyNames = new Dictionary<RealImageCompressionType, String>
        {
            {RealImageCompressionType.None, Message.RealImageCompressionType_None},
            {RealImageCompressionType.Quarter, Message.RealImageCompressionType_Quarter},
            {RealImageCompressionType.Half, Message.RealImageCompressionType_Half},
            {RealImageCompressionType.ThreeQuarter, Message.RealImageCompressionType_ThreeQuarter}
        };
    }
}