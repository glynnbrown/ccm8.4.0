#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28676 : A.Silva
//      Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Helpers
{
    public enum RealImageColourDepthType
    {       
        /// <summary>
        ///     4 bits colour depth, giving 16 colors.
        /// </summary>
        Bit4,

        /// <summary>
        ///     8 bits colour depth, giving 256 colors.
        /// </summary>
        Bit8,
        /// <summary>
        ///     24 bits colour depth, giving 16 million+ colors (true color).
        /// </summary>
        Bit24,
    }

    /// <summary>
    ///     Helper class to work with <see cref="RealImageColourDepthType"/> values.
    /// </summary>
    public static class RealImageColourDepthTypeHelper
    {
        /// <summary>
        ///     Dictionary mapping each member of <see cref="RealImageColourDepthType"/> to its localized name.
        /// </summary>
        public static Dictionary<RealImageColourDepthType, String> FriendlyNames = new Dictionary<RealImageColourDepthType, String>
        {
            {RealImageColourDepthType.Bit4, Message.RealImageColourDepthType_Bit4},
            {RealImageColourDepthType.Bit8, Message.Enum_RealImageColourDepthType_Bit8},
            {RealImageColourDepthType.Bit24, Message.Enum_RealImageColourDepthType_Bit24}
        };
    }
}