﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created
// V8-27467 : A.Kuszyk
//  Amended divider level number selection to only select a level number if the current divider is
//  an adjacent one.
// V8-27510 : A.Kuszyk
//  Amended GetParallelDividers() to compare positions rounded to 5 d.p. because the float comparison
//  was yielding errors.
// V8-27485 : A.Kuszyk
//  Added GetOverlap() method.
// V8-27667 : A.Kuszyk
//  Updated all Single comparisons to use Framework extension methods.
#endregion
#region Version History: (CCM 811)
// V8-30564 : D.Pleasance
//  Amended GetNextBlockingGroupColour so that standard colours are first colours to be assigned.
#endregion
#region Version History : CCM820
// V8-31077 : A.Kuszyk
//  Overloaded GetNextBlockingGroupColour to allow a sequence to be specified.
// V8-31268 : M.Brumby
//  GetNextDivider now looks to the next level down (and so on) if it has not found any dividers
//  at the same level as the supplied divider
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using System.Diagnostics;
using Galleria.Framework.Helpers;
using System.Globalization;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Provides Blocking related helper methods.
    /// </summary>
    public static class BlockingHelper
    {
        private static List<Tuple<Byte, Byte, Byte>> _standardColours = new List<Tuple<Byte, Byte, Byte>>();

        /// <summary>
        /// Returns the next available group colour
        /// </summary>
        /// <param name="sequence">If provided, the colours found in this sequence's groups will be ignored.</param>
        /// <returns></returns>
        public static Int32 GetNextBlockingGroupColour(IPlanogramBlocking blocking, PlanogramSequence sequence = null)
        {
            // attempt to obtain a standard colour first
            foreach (Tuple<Byte, Byte, Byte> standardColour in StandardColors())
            {
                Byte red = standardColour.Item1;
                Byte green = standardColour.Item2;
                Byte blue = standardColour.Item3;

                var colour = (((UInt32)255) << 24) |
                        (((UInt32)red) << 16) |
                        (((UInt32)green) << 8) |
                        (UInt32)blue;
                
                //make sure the colour has not already been picked.
                if (!blocking.Groups.Any(g => g.Colour == (Int32)colour) && // No blocks contain the colour
                    (sequence == null || !sequence.Groups.Any(g => g.Colour == (Int32)colour))) // No Sequence groups contain the colour
                {
                    return (Int32)colour;
                }
            }
            
            Int32 idx = blocking.Groups.Count();
            while (true)
            {
                Int32[] p = GetColourPattern(idx + 2);// add 2 to skip black and white.
                Int32 colour = GetColourElement(p[0]) << 16 | GetColourElement(p[1]) << 8 | GetColourElement(p[2]);

                //make sure the colour has not already been picked.
                if (!blocking.Groups.Any(g => g.Colour == (Int32)colour) && // No blocks contain the colour
                    (sequence == null || !sequence.Groups.Any(g => g.Colour == (Int32)colour))) // No Sequence groups contain the colour
                {
                    return colour;
                }
                else
                {
                    idx++;
                }
            }
        }

        /// <summary>
        /// Provides list of available standard colors
        /// </summary>
        public static List<Tuple<Byte, Byte, Byte>> StandardColors()
        {
            if (!_standardColours.Any())
            {
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(192, 0, 0));       // 1
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(146, 208, 80));    // 5
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(112, 48, 160));    // 10
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(255, 0, 0));       // 2
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(0, 176, 80));      // 6
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(0, 32, 96));       // 9
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(255, 192, 0));     // 3
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(0, 176, 240));     // 7
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(255, 255, 0));     // 4
                _standardColours.Add(new Tuple<Byte, Byte, Byte>(0, 112, 192));     // 8
            }

            return _standardColours;
        }

        /// <summary>
        /// Determines if the divider intersects the given location
        /// </summary>
        public static Boolean IntersectsWith(IPlanogramBlockingDivider divider, IPlanogramBlockingLocation loc)
        {
            Single divW = (divider.Type == PlanogramBlockingDividerType.Horizontal) ? divider.Length : 0.001F;
            Single divH = (divider.Type == PlanogramBlockingDividerType.Vertical) ? divider.Length : 0.001F;

            Boolean intersects =
                (divider.X.LessOrEqualThan(loc.X + loc.Width)) &&
                (divider.X + divW).GreaterOrEqualThan(loc.X) &&
                (divider.Y.LessOrEqualThan(loc.Y + loc.Height)) &&
                ((divider.Y + divH).GreaterOrEqualThan(loc.Y));

            if (!intersects) return false;

            Single intersectX = Math.Max(loc.X, divider.X);
            Single intersectY = Math.Max(loc.Y, divider.Y);

            if (divider.Type == PlanogramBlockingDividerType.Horizontal)
            {
                Single intersectionWidth = Math.Min(loc.X + loc.Width, divider.X + divider.Length) - intersectX;
                Boolean dividerYInLocationHeight = divider.Y.GreaterOrEqualThan(loc.Y) && divider.Y.LessOrEqualThan(loc.Y+loc.Height);
                if (intersectionWidth.EqualTo(0) || !dividerYInLocationHeight) return false;

            }
            else
            { 
                Single intersectionHeight = Math.Min(loc.Y + loc.Height, divider.Y + divider.Length) - intersectY;
                Boolean dividerXInLocationWidth = divider.X.GreaterOrEqualThan(loc.X) && divider.X.LessOrEqualThan(loc.X + loc.Width);
                if (intersectionHeight.EqualTo(0) || !dividerXInLocationWidth) return false;
            }

            return true;
        }

        /// <summary>
        /// Returns an array of the 2 dividers that are parallel to this one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="divider"></param>
        /// <param name="blocking"></param>
        /// <returns></returns>
        public static void GetParallelDividers<T>(T divider, IPlanogramBlocking blocking, out T nearestMin, out T nearestMax)
            where T : IPlanogramBlockingDivider
        {
            T minDiv = default(T);
            T maxDiv = default(T);

            if (blocking != null)
            {
                if (divider.Type == PlanogramBlockingDividerType.Vertical)
                {
                    foreach (T div in blocking.Dividers)
                    {
                        if (div.Type == PlanogramBlockingDividerType.Vertical
                            && !Object.Equals(div, divider))
                        {
                            Boolean isValid =
                                (divider.Y.LessThan(div.Y + div.Length)) &&
                                ((divider.Y + divider.Length).GreaterThan(div.Y));
                            if (!isValid) continue;

                            if (div.X.LessThan(divider.X,4))
                            {
                                if (minDiv == null || (div.X.GreaterThan(minDiv.X,4)))
                                {
                                    minDiv = div;
                                }
                            }
                            else if (div.X.GreaterThan(divider.X,4))
                            {
                                if (maxDiv == null || (div.X.LessThan(maxDiv.X,4)))
                                {
                                    maxDiv = div;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (T div in blocking.Dividers)
                    {
                        if (div.Type == PlanogramBlockingDividerType.Horizontal
                            && !Object.Equals(div, divider))
                        {
                            Boolean isValid =
                                divider.X.LessThan(div.X + div.Length) &&
                                (divider.X + divider.Length).GreaterThan(div.X);
                            if (!isValid) continue;

                            if (div.Y.LessThan(divider.Y,4))
                            {
                                if (minDiv == null || (div.Y.GreaterThan(minDiv.Y,4)))
                                {
                                    minDiv = div;
                                }
                            }
                            else if (div.Y.GreaterThan(divider.Y,4))
                            {
                                if (maxDiv == null || (div.Y.LessThan(maxDiv.Y,4)))
                                {
                                    maxDiv = div;
                                }
                            }
                        }
                    }
                }
            }


            nearestMin = minDiv;
            nearestMax = maxDiv;
        }

        /// <summary>
        /// Returns all dividers which are adjacent to that given.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="divider"></param>
        /// <param name="blocking"></param>
        /// <returns></returns>
        public static List<T> GetAdjacentDividers<T>(T divider, IPlanogramBlocking blocking)
            where T : IPlanogramBlockingDivider
        {
            List<T> adjacentDividers = new List<T>();
            if (blocking == null) return adjacentDividers;

            if (divider.Type == PlanogramBlockingDividerType.Vertical)
            {
                Double dividerYEnd = divider.Y + divider.Length;

                foreach (T d in blocking.Dividers)
                {
                    //an adjacent divider will always be higher in level
                    // and of the oposite type
                    if (d.Type == PlanogramBlockingDividerType.Horizontal
                        && d.Level > divider.Level)
                    {
                        //its start or end must touch the divider
                        if (d.X.EqualTo(divider.X) || (d.X + d.Length).EqualTo(divider.X))
                        {
                            //must be within the bounds of the divider length
                            if (d.Y.GreaterThan(divider.Y) && d.Y.LessThan(dividerYEnd))
                            {
                                adjacentDividers.Add(d);
                            }
                        }
                    }
                }
            }
            else if (divider.Type == PlanogramBlockingDividerType.Horizontal)
            {
                Double dividerXEnd = divider.X + divider.Length;

                foreach (T d in blocking.Dividers)
                {
                    //an adjacent divider will always be higher in level
                    // and of the oposite type
                    if (d.Type == PlanogramBlockingDividerType.Vertical
                        && d.Level > divider.Level)
                    {
                        //its start or end must touch the divider
                        if (d.Y.EqualTo(divider.Y) || (d.Y + d.Length).EqualTo(divider.Y))
                        {
                            //must be within the bounds of the divider length
                            if (d.X.GreaterThan(divider.X) && d.X.LessThan(dividerXEnd))
                            {
                                adjacentDividers.Add(d);
                            }
                        }

                    }
                }

            }

            return adjacentDividers;
        }

        /// <summary>
        /// Returns the values to create a new divider at the given point.
        /// </summary>
        /// <param name="blocking">the parent blocking of the new divider</param>
        /// <param name="x">the point x value </param>
        /// <param name="y">the point y value</param>
        /// <param name="type">the divider type</param>
        /// <param name="dividerX">output: the x value of the new divider</param>
        /// <param name="dividerY">output: the y value of the new divider</param>
        /// <param name="dividerLength">output: the length of the new divider</param>
        /// <param name="dividerLevel">output: the level of the new divider</param>
        public static void GetDividerValues(IPlanogramBlocking blocking, Single x, Single y, PlanogramBlockingDividerType type,
            out Single dividerX, out Single dividerY, out Single dividerLength, out Byte dividerLevel)
        {
            //calculate the actual divider values
            Byte levelNo = 1;
            Single divX = 0;
            Single divY = 0;
            Single length = 1;

            if (type == PlanogramBlockingDividerType.Vertical)
            {
                divX = x;

                //check if any horizontal dividers are above or below.
                Single maxY = 1;
                Single minY = 0;
                foreach (var div in blocking.Dividers)
                {
                    //if the type is horizontal and this spans accross the same x
                    if (div.Type == PlanogramBlockingDividerType.Horizontal
                        && div.X.LessOrEqualThan(x) && (div.X + div.Length).GreaterOrEqualThan(x))
                    {
                        //update the min and max y
                        if (div.Y.GreaterThan(y))
                        {
                            maxY = Math.Min(div.Y, maxY);
                        }
                        else
                        {
                            minY = Math.Max(div.Y, minY);
                        }

                        //set level no, if this divider is positioned at min or max y.
                        if (div.Y.EqualTo(minY) || div.Y.EqualTo(maxY))
                        {
                            levelNo = (Byte)Math.Max(levelNo, div.Level + 1);
                        }
                    }
                }

                //set the y and length
                divY = minY;
                length = maxY - minY;
            }
            else if (type == PlanogramBlockingDividerType.Horizontal)
            {
                divY = y;

                //check if any vertical dividers are left or right.
                Single maxX = 1;
                Single minX = 0;
                foreach (var div in blocking.Dividers)
                {
                    //if the type is vertical and this spans accross the same y
                    if (div.Type == PlanogramBlockingDividerType.Vertical
                        && div.Y.LessOrEqualThan(y) && (div.Y + div.Length).GreaterOrEqualThan(y))
                    {
                        //update the min and may X
                        if (div.X.GreaterThan(x))
                        {
                            maxX = Math.Min(div.X, maxX);
                        }
                        else
                        {
                            minX = Math.Max(div.X, minX);
                        }

                        //set level no, if this divider is positioned at min or max x.
                        if (div.X.EqualTo(minX) || div.X.EqualTo(maxX))
                        {
                            levelNo = (Byte)Math.Max(levelNo, div.Level + 1);
                        }
                    }
                }
                //set the x and length
                divX = minX;
                length = maxX - minX;
            }


            //set the output values
            dividerX = divX;
            dividerY = divY;
            dividerLength = length;
            dividerLevel = levelNo;
        }

        /// <summary>
        /// Returns true if the two areas can be connected
        /// </summary>
        public static Boolean CanCombine(IPlanogramBlockingLocation area1, IPlanogramBlockingLocation area2)
        {
            //either is null = false
            if (area1 == null) return false;
            if (area2 == null) return false;

            //both are same = false
            if (area1 == area2) return false;

            //already in same group = false
            if (Object.Equals(area1.PlanogramBlockingGroupId, area2.PlanogramBlockingGroupId)) return false;

            //not adjacent = false;
            Int32 matchingDivCount = GetDivIds(area1).Intersect(GetDivIds(area2)).Count();
            if (matchingDivCount == 0) return false;

            return true;
        }

        /// <summary>
        ///Returns the next parallel divider of the same level
        /// </summary>
        /// <param name="divider"></param>
        /// <returns></returns>
        public static T GetNextDivider<T>(T divider, IPlanogramBlocking blocking)
            where T : IPlanogramBlockingDivider
        {
            T maxDiv = default(T);

            Dictionary<Int32, T> maxDivPerLevel = new Dictionary<Int32, T>();

            if (divider.Type == PlanogramBlockingDividerType.Vertical)
            {
                foreach (T div in blocking.Dividers)
                {
                    if (div.Type == PlanogramBlockingDividerType.Vertical
                        && !Object.Equals(div, divider))
                    {
                        //we want a divider at the same level for this or lower
                        if (div.Level > divider.Level) continue;

                        Boolean isValid =
                            (divider.Y.LessThan(div.Y + div.Length)) &&
                            ((divider.Y + divider.Length).GreaterThan(div.Y));
                        if (!isValid) continue;

                        if (div.X.GreaterThan(divider.X))
                        {
                            //keep track of the max div per level
                            if (!maxDivPerLevel.TryGetValue(div.Level, out maxDiv) || div.X.LessThan(maxDiv.X))
                            {
                                maxDivPerLevel[div.Level] = div;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (T div in blocking.Dividers)
                {
                    if (div.Type == PlanogramBlockingDividerType.Horizontal
                        && !Object.Equals(div, divider))
                    {
                        //we want a divider at the same level for this or lower
                        if (div.Level > divider.Level) continue;

                        Boolean isValid =
                            (divider.X.LessThan(div.X + div.Length)) &&
                            ((divider.X + divider.Length).GreaterThan(div.X));
                        if (!isValid) continue;

                        if (div.Y.GreaterThan(divider.Y))
                        {
                            //keep track of the max div per level
                            if (!maxDivPerLevel.TryGetValue(div.Level, out maxDiv) || div.Y.LessThan(maxDiv.Y))
                            {
                                maxDivPerLevel[div.Level] = div;
                            }
                        }
                    }
                }
            }

            //work our way down through the levels untill we find a level
            //with a divider to use.
            maxDiv = default(T);
            for (Int32 x = divider.Level; x > 0; x--)
            {
                if (maxDivPerLevel.TryGetValue(x, out maxDiv)) break;
            }

            return maxDiv;
        }

        /// <summary>
        /// Calculates the percentage that two lines, a and b, overlap.
        /// </summary>
        /// <param name="aPosition">The starting position of line a.</param>
        /// <param name="aLength">The length of line a.</param>
        /// <param name="bPosition">The starting position of line b.</param>
        /// <param name="bLength">The length of line b.</param>
        /// <returns>A value between 0 and 1 representing the amount of a overlapping b.</returns>
        public static Single GetOverlap(Single aPosition, Single aLength, Single bPosition, Single bLength)
        {
            var a2 = aPosition + aLength;
            var b2 = bPosition + bLength;

            var internalX1 = Math.Max(aPosition, bPosition);
            var internalX2 = Math.Min(a2, b2);
            if (a2 - aPosition == 0) return 0f;
            return (internalX2 - internalX1) / (a2 - aPosition);
        }

        /// <summary>
        /// Calculates the percentage by which the two dividers overlap.
        /// </summary>
        /// <param name="divider1"></param>
        /// <param name="divider2"></param>
        /// <returns>The percentage that divider 1 overlaps with divider 2.</returns>
        public static Single GetOverlap(IPlanogramBlockingDivider divider1, IPlanogramBlockingDivider divider2)
        {
            if (divider1 == null) throw new ArgumentNullException("divider1");
            if (divider2 == null) throw new ArgumentNullException("divider2");

            if (divider1.Type == PlanogramBlockingDividerType.Horizontal)
            {
                if (divider2.Type == divider1.Type)
                {
                    return GetOverlap(divider1.X, divider1.Length, divider2.X, divider2.Length);
                }
                return Convert.ToSingle(
                    divider2.X.GreaterOrEqualThan(divider1.X) && divider2.X.LessOrEqualThan(divider1.X + divider1.Length));
            }
            else
            {
                if (divider2.Type == divider1.Type)
                {
                    return GetOverlap(divider1.Y, divider1.Length, divider2.Y, divider2.Length);
                }
                return Convert.ToSingle(
                    divider2.Y.GreaterOrEqualThan(divider1.Y) && divider2.Y.LessOrEqualThan(divider1.Y + divider1.Length));
            }
        }

        #region Private

        private static Int32 GetColourElement(Int32 index)
        {
            Int32 value = index - 1;
            Int32 v = 0;
            for (Int32 i = 0; i < 8; i++)
            {
                v = v | (value & 1);
                v <<= 1;
                value >>= 1;
            }
            v >>= 1;
            value = v & 0xFF;


            Byte red = Clamp((Byte)((value >> 16) & 0xff));
            Byte green = Clamp((Byte)((value >> 8) & 0xff));
            Byte blue = Clamp((Byte)((value) & 0xff));

            var uint32Color = (((UInt32)255) << 24) |
                    (((UInt32)red) << 16) |
                    (((UInt32)green) << 8) |
                    (UInt32)blue;

            return (Int32)uint32Color;
        }

        private static Int32[] GetColourPattern(Int32 index)
        {
            Int32 n = (Int32)Math.Pow(index, 1 / 3.0);
            index -= (n * n * n);

            Int32[] p = new Int32[3];
            p[0] = n;
            p[1] = n;
            p[2] = n;

            if (index == 0)
            {
                return p;
            }
            index--;

            Int32 v = index % 3;
            index = index / 3;

            if (index < n)
            {
                p[v] = index % n;
                return p;
            }

            index -= n;
            p[v] = index / n;
            p[++v % 3] = index % n;

            return p;
        }

        /// <summary>
        /// Clamps the supplied value bettween 0 and 255
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private static Byte Clamp(Single value)
        {
            Single output = Math.Max(value, 0);
            output = Math.Min(value, 255);
            return (Byte)(output);
        }

        private static IEnumerable<Object> GetDivIds(IPlanogramBlockingLocation loc)
        {
            if (loc.PlanogramBlockingDividerBottomId != null)
            {
                yield return loc.PlanogramBlockingDividerBottomId;
            }
            if (loc.PlanogramBlockingDividerTopId != null)
            {
                yield return loc.PlanogramBlockingDividerTopId;
            }
            if (loc.PlanogramBlockingDividerLeftId != null)
            {
                yield return loc.PlanogramBlockingDividerLeftId;
            }
            if (loc.PlanogramBlockingDividerRightId != null)
            {
                yield return loc.PlanogramBlockingDividerRightId;
            }
        }
        #endregion
    }
}
