﻿// Copyright © Galleria RTS Ltd 2016

using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Contains helper methods for resolving planogram field values.
    /// </summary>
    public static class PlanogramFieldHelper
    {
        /// <summary>
        /// Returns a dictionary of containing object field infos for all displayable properties in a planogram.
        /// </summary>
        /// <param name="includeMetadata"></param>
        /// <returns></returns>
        public static Dictionary<PlanogramItemType, IEnumerable<ObjectFieldInfo>> GetPlanogramObjectFieldInfos(Planogram context = null)
        {
            const Boolean includeMetadata = true;
            var dict = new Dictionary<PlanogramItemType, IEnumerable<ObjectFieldInfo>>();

            //Product
            dict.Add(PlanogramItemType.Product,
                PlanogramProduct.EnumerateDisplayableFieldInfos(includeMetadata, /*incCustom*/true, /*incPerformance*/true,
                (context != null) ? context.Performance : null));

            //Position
            dict.Add(PlanogramItemType.Position, PlanogramPosition.EnumerateDisplayableFieldInfos());

            //SubComponent
            dict.Add(PlanogramItemType.SubComponent, PlanogramSubComponent.EnumerateDisplayableFieldInfos());

            //Component
            dict.Add(PlanogramItemType.Component,

                //fixture component properties
                PlanogramFixtureComponent.EnumerateDisplayableFieldInfos()

                //component properties
                .Union(PlanogramComponent.EnumerateDisplayableFieldInfos(includeMetadata))

                );


            //Do we want assembly?

            // Fixture
            dict.Add(PlanogramItemType.Fixture,

               //fixture component properties
               PlanogramFixtureItem.EnumerateDisplayableFieldInfos(includeMetadata)

               //component properties
               .Union(PlanogramFixture.EnumerateDisplayableFieldInfos(includeMetadata))

               );

            //Planogram
            dict.Add(PlanogramItemType.Planogram, Planogram.EnumerateDisplayableFieldInfos(includeMetadata));


            return dict;
        }

        /// <summary>
        /// Enumerates through all available fields to be displayed against planogram model objects
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateAllFields(Planogram context = null)
        {
            foreach (var entry in GetPlanogramObjectFieldInfos(context).Values)
            {
                foreach (ObjectFieldInfo field in entry)
                {
                    yield return field;
                }
            }
        }

        /// <summary>
        /// Resolves the given field text to a label string.
        /// </summary>
        public static String ResolvePositionLabel(String labelFormat, PlanogramPosition position, IEnumerable<ObjectFieldInfo> masterFieldList)
        {
            if (position == null) return null;
            return ResolvePositionLabel(labelFormat, position, position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement(), masterFieldList);
        }

        /// <summary>
        /// Resolves the given field text to a label string.
        /// </summary>
        public static String ResolvePositionLabel(String labelFormat,
            PlanogramPosition position, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement)
        {
            return ResolvePositionLabel(labelFormat, position, product, subComponentPlacement, EnumerateAllFields());
        }

        /// <summary>
        /// Resolves the given field text to a label string.
        /// </summary>
        public static String ResolvePositionLabel(String labelFormat,
            PlanogramPosition position, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement,
            IEnumerable<ObjectFieldInfo> masterFieldList)
        {
            return ResolveLabel(labelFormat, masterFieldList,
                PlanogramItemPlacement.NewPlanogramItemPlacement(
                position,
                product,
                subComponentPlacement));
        }


        /// <summary>
        /// Resolves the given field text to a label string.
        /// </summary>
        public static String ResolveComponentLabel(String labelFormat,
            PlanogramSubComponentPlacement subComponentPlacement,
            IEnumerable<ObjectFieldInfo> masterFieldList)
        {
            return ResolveLabel(labelFormat, masterFieldList,
                PlanogramItemPlacement.NewPlanogramItemPlacement(subComponentPlacement));
        }


        /// <summary>
        /// Resolves the given field text to a label string.
        /// </summary>
        public static String ResolveLabel(String labelFormat,
            IEnumerable<ObjectFieldInfo> masterFieldList,
            PlanogramItemPlacement targetItem)
        {
            Object formulaResult =
                targetItem.ResolveFieldExpression(labelFormat, masterFieldList);

            String label = Convert.ToString(formulaResult, CultureInfo.CurrentCulture);
            if (String.IsNullOrEmpty(label))
            {
                label = Message.PlanogramHelper_LabelNoData;
            }

            return label;
        }


        /// <summary>
        /// Resolves the field for the given target
        /// </summary>
        public static Object ResolveField(ObjectFieldInfo fieldInfo, PlanogramItemPlacement targetItem)
        {
            return targetItem.ResolveField(fieldInfo);
        }


        /// <summary>
        /// Returns the value of the resolved formula for the given target placement.
        /// Uses the master planogram field list.
        /// </summary>
        /// <param name="fieldText"></param>
        /// <param name="targetItem"></param>
        /// <returns></returns>
        public static Object ResolveFieldExpression(String fieldText, PlanogramItemPlacement targetItem)
        {
            return targetItem.ResolveFieldExpression(fieldText, EnumerateAllFields());
        }

        /// <summary>
        /// Returns the value of the resolved formula for the given target placement.
        /// </summary>
        /// <param name="fieldText"></param>
        /// <param name="masterFieldList"></param>
        /// <param name="targetItem"></param>
        /// <returns></returns>
        public static Object ResolveFieldExpression(String fieldText, IEnumerable<ObjectFieldInfo> masterFieldList, PlanogramItemPlacement targetItem)
        {
            return targetItem.ResolveFieldExpression(fieldText, masterFieldList);
        }


        /// <summary>
        /// Returns the expected data level based on the given fields.
        /// </summary>
        /// <param name="fieldList">the fields that have been selected</param>
        /// <returns>the part type level to which data should be displayed.</returns>
        public static PlanogramItemType GetFieldDataLevel(IEnumerable<ObjectFieldInfo> fieldList)
        {
            List<Type> ownerTypes = fieldList.Select(f => f.OwnerType).Distinct().ToList();

            if (ownerTypes.Any(o => o == typeof(PlanogramPosition)))
                return PlanogramItemType.Position;

            if (ownerTypes.Any(o => o == typeof(PlanogramAnnotation)))
                return PlanogramItemType.Annotation;

            if (ownerTypes.Any(o => o == typeof(PlanogramProduct)))
            {
                //only return product if all columns are product.
                if (ownerTypes.All(o => o == typeof(PlanogramProduct)))
                {
                    return PlanogramItemType.Product;
                }
                else return PlanogramItemType.Position;
            }

            if (ownerTypes.Any(o => o == typeof(PlanogramSubComponent)))
                return PlanogramItemType.SubComponent;

            if (ownerTypes.Any(o => o == typeof(PlanogramComponent) || o == typeof(PlanogramFixtureComponent) || o == typeof(PlanogramAssemblyComponent)))
                return PlanogramItemType.Component;

            if (ownerTypes.Any(o => o == typeof(PlanogramAssembly) || o == typeof(PlanogramFixtureAssembly)))
                return PlanogramItemType.Assembly;

            if (ownerTypes.Any(o => o == typeof(PlanogramFixture) || o == typeof(PlanogramFixtureItem)))
                return PlanogramItemType.Fixture;

            return PlanogramItemType.Planogram;
        }

        /// <summary>
        /// Returns the key for the given item based on the load level type.
        /// </summary>
        public static Object GetKeyValue(PlanogramItemPlacement item, PlanogramItemType levelType)
        {
            switch (levelType)
            {
                case PlanogramItemType.Planogram:
                    return item.Planogram.Id;

                case PlanogramItemType.Fixture:
                    return item.FixtureItem.BaySequenceNumber;

                case PlanogramItemType.Assembly:
                    return item.Assembly.Name;

                case PlanogramItemType.Component:
                    return String.Format(CultureInfo.InvariantCulture, "{0} {1}",
                        item.FixtureItem.BaySequenceNumber,
                    (item.FixtureComponent != null) ?
                    item.FixtureComponent.ComponentSequenceNumber
                    : item.AssemblyComponent.ComponentSequenceNumber);

                case PlanogramItemType.Annotation:
                    return item.Annotation.Id;

                case PlanogramItemType.Position:
                    if (item.Product.MetaPositionCount == 1) return item.Product.Gtin;
                    else return item.Position.Id;

                case PlanogramItemType.Product:
                    return item.Product.Gtin;

                case PlanogramItemType.SubComponent:
                    return String.Format(CultureInfo.InvariantCulture, "{0} {1} {2}",
                        item.FixtureItem.BaySequenceNumber,
                   (item.FixtureComponent != null) ?
                    item.FixtureComponent.ComponentSequenceNumber
                    : item.AssemblyComponent.ComponentSequenceNumber,
                    item.Component.SubComponents.IndexOf(item.SubComponent));

                default: return null;
            }
        }

    }
}
