﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM v8.0)

// V8-24971 : L.Hodson ~ Created.
// V8-26812 : A.Silva
//      Added ValidationTypeConstraint, BuildErrorDescription.
// V8-27062 : A.Silva
//      Added internal extension ToObjectFieldInfoList to help enumerate fields in Planograms.
// V8-27058 : A.Probyn ~ Added Calculation helper methods
// V8-27742 : A.Silva
//      Added RectValue.Contains extension method.
// V8-27940 : L.Luong
//      Added method to get ObjectFieldInfo as a list
// V8-27456 : A.Silva
//      Amended ValidationTypeConstraint BusinessRule to account for EqualTo.
//      Amended BuildErrorsDescription to allow showing a more extense error description.

#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added RegisterRealImageProvider method and RealImageProvider property.

#endregion

#region Version History: CCM802

// V8-29028 : L.Ineson
// Added GetOrientatedMaxSqueeze
// V8-28811 : L.Luong
//  Added CalculateMetaPercentageLinearSpaceFilled

#endregion

#region Version History : CCM810

// V8-29789 : A.Kuszyk
//  Amended Equality methods used in GetOrientedMaxSqueeze method.

#endregion

#region Version History: CCM811

// V8-30164 : A.Silva
//  Encapsulated the calculation for MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex.
// V8-30352 : A.Probyn
//  Updated CalculateMetaTotalWhiteSpace to round merchandisable space value before processing. Defends against non
//  rounded values being passed in.
#endregion

#region Version History : CCM820
// V8-30844 : A.Kuszyk
//  Added LessThanOrEqualTo and GreaterThanOrEqualTo options to ValidationTypeConstraint rule.
// V8-30725 : M.Brumby
//  Added MovePlanogramItems and RestructureBaysByRatio
// V8-30782 : L.Ineson
//  Updated to support multiple fields in highlight.
// V8-31285 : M.Brumby
//  Added Gtin validation helper
// V8-31532 : L.Ineson
//  Added ConvertDecimalsToInvariant method to support using Datatable.Compute for formulas in lithuanian locale.
// V8-31585 : D.Pleasance
//  Amended GetOrientatedMaxSqueeze(), EqualTo method means that 0.1 or less is an invalid squeeze value.
#endregion

#region Version History : CCM830
//V8-31541 : L.Ineson
//  Added more field helpers.
// V8-32257 : L.Ineson
//  Made sure that data level returns as position if not only product level fields 
// are selected.
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
// V8-32421 : L.ineson
//  Moved some of the resolve filed methods against PlanogramItemPlacement.
// V8-32491 : M.Brumby
//  Corrected bay splitting with no input
// V8-32480 : M.Brumby
//  Default bay splitting to fixed
// V8-32489 : M.Brumby
//  Changed split bay name creation to include the original name
// V8-32543 : M.Brumby
//  Changed split component name creation to include original name
// V8-32565 : M.Brumby
//  Split bays now retains the angle and position of the 1st bay in the group
// V8-32566 : M.Brumby
//  Split bays now has a good go at splitting components that have funny angles
// V8-32677 : A.Heathcote
//  GetBayWidthByPattern now returns origional widths if "0" is entered 
//  (Button is disabled already but just in case everything goes floopy)
// V8-32799 : M.Brumby
//  Fixes for split bays being used multiple times before applying.
// V8-32835 : N.Haywood
//  Added GetOrientatedSize for planogram position
// V8-32953 : L.Ineson
// Added the ability to pass a userstate to real image fetch methods.
// V8-32447 : L.Ineson
//  Corrected GetUnorientatedSize for right 90.
// CCM-18559 : M.Pettit
//  Ensured that when restucturing planograms by size that the correct split components value is passed to underlying methods
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Csla.Core;
using Csla.Rules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Merchandising;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Provides helper methods for manipulating planograms
    /// </summary>
    /// <remarks>This is in the process of being split into smaller helpers.</remarks>
    public static class PlanogramHelper
    {
        #region Meta Data Calculation

        /// <summary>
        /// Helper method to calculate meta total white space
        /// </summary>
        /// <param name="metaTotalMerchandisableSpace"></param>
        /// <param name="totalSpaceActualMerchandised"></param>
        /// <returns>Total White Space Left</returns>
        /// <remarks>Valid for use on Linear, Area & Volumetric space.</remarks>
        public static Single? CalculateMetaTotalWhiteSpace(Single? metaTotalMerchandisableSpace, Single totalSpaceActualMerchandised)
        {
            if (metaTotalMerchandisableSpace.HasValue && metaTotalMerchandisableSpace.Value > 0)
            {
                //Calculate space not merchandised (white space)
                Single totalSpaceUnallocated = (Single)Math.Round(metaTotalMerchandisableSpace.Value, 5, MidpointRounding.AwayFromZero) - totalSpaceActualMerchandised;

                if (totalSpaceUnallocated != 0)
                {
                    //Calculate percentage against total space
                    return Convert.ToSingle(Math.Round((
                        totalSpaceUnallocated / metaTotalMerchandisableSpace.Value), 5, MidpointRounding.AwayFromZero));
                }
            }
            return 0;
        }

        /// <summary>
        /// Helper method to calculate meta Percentage Linear Space Filled on a Component
        /// </summary>
        /// <param name="metaTotalMerchandisableSpace"></param>
        /// <param name="totalSpaceActualMerchandised"></param>
        /// <returns></returns>
        public static Single? CalculateMetaPercentageLinearSpaceFilled(Single? metaTotalMerchandisableSpace, Single totalSpaceActualMerchandised)
        {
            if (metaTotalMerchandisableSpace.HasValue && metaTotalMerchandisableSpace.Value > 0)
            {
                //Calculate percentage of space filled
                return Convert.ToSingle(Math.Round((totalSpaceActualMerchandised / metaTotalMerchandisableSpace.Value), 4, MidpointRounding.AwayFromZero));
            }
            return 0;
        }

        /// <summary>
        ///     Calculate the meta data value <c>MetaTotalFacings</c> for the provided <paramref name="positions" />.
        /// </summary>
        /// <param name="positions">The list of <see cref="PlanogramPosition" /> objects to obtaing the average facing value for.</param>
        /// <param name="products"></param>
        /// <returns>
        ///     The total of <c>Main Block Facings</c> plus <c>X Block Facings</c>, that is, <c>FacingsWide + FacingsXWide</c>
        ///     for all positions. If there are no positions, 0 is returned.
        /// </returns>
        /// <remarks>
        ///     The result is not rounded away from zero to 2 decimal places.
        /// </remarks>
        public static Int32? CalculateMetaTotalFacings(ICollection<PlanogramPosition> positions, ICollection<PlanogramProduct> products)
        {
            if (positions == null || products == null)
            {
                Debug.Fail("Null reference provided. Expected collections of PlanogramPosition and PlanogramProducts.");
                return 0;
            }

            return !positions.Any() ? 0 : positions.Sum(p => GetEffectiveTotalFacings(p, products));
        }

        /// <summary>
        ///     Get the effective number of facings in the main and X blocks, considering that trays have facings*units as effective facings.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="products"></param>
        /// <returns></returns>
        private static Int32 GetEffectiveTotalFacings(PlanogramPosition position, IEnumerable<PlanogramProduct> products)
        {
            if (position == null) return 0;

            PlanogramProduct product = products.FirstOrDefault(p => Equals(p.Id, position.PlanogramProductId));
            if (product == null) return 0;

            Int32 effectiveFacingsWide = position.FacingsWide;
            Int32 effectiveFacingsXWide = position.FacingsXWide;
            if (position.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Main) == PlanogramProductMerchandisingStyle.Tray)
            {
                effectiveFacingsWide *= product
                    .GetRotatedTraySize(
                        position.GetAppliedOrientationType(product, PlanogramPositionBlockType.Main)
                                .ToPositionOrientationType())
                    .Wide;
            }
            if (position.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.X) == PlanogramProductMerchandisingStyle.Tray)
            {
                effectiveFacingsXWide *= product
                    .GetRotatedTraySize(
                        position.GetAppliedOrientationType(product, PlanogramPositionBlockType.X)
                                .ToPositionOrientationType())
                    .Wide;
            }
            return effectiveFacingsWide + effectiveFacingsXWide;
        }

        #endregion

        #region Value Validation

        /// <summary>
        /// Checks that the threshold values conform to the <see cref="PlanogramValidationType"/> for the planogram.
        /// </summary>
        /// <remarks>Depending on the <see cref="PlanogramValidationType"/>, the values for the Thresholds may be adjusted so they are valid.</remarks>
        public class ValidationTypeConstraint : BusinessRule
        {
            private IPropertyInfo Threshold1PropertyInfo { get; set; }
            private IPropertyInfo Threshold2PropertyInfo { get; set; }

            public ValidationTypeConstraint(IPropertyInfo validationTypeProperty, IPropertyInfo threshold1Property,
                IPropertyInfo threshold2Property)
                : base(validationTypeProperty)
            {
                InputProperties = new List<IPropertyInfo>
                                  {
                    validationTypeProperty,
                    threshold1Property,
                    threshold2Property
                };
                Threshold1PropertyInfo = threshold1Property;
                Threshold2PropertyInfo = threshold2Property;
            }

            protected override void Execute(RuleContext context)
            {
                var validationType = context.InputPropertyValues[PrimaryProperty] as PlanogramValidationType?;
                var threshold1 = context.InputPropertyValues[Threshold1PropertyInfo] as Single?;
                var threshold2 = context.InputPropertyValues[Threshold2PropertyInfo] as Single?;

                if (validationType == null || threshold1 == null || threshold2 == null)
                {
                    context.AddWarningResult(PrimaryProperty, "Unable to validate without values.");
                    return;
                }

                switch (validationType)
                {
                    case PlanogramValidationType.GreaterThan:
                    case PlanogramValidationType.GreaterThanOrEqualTo:
                        if (threshold1 < threshold2)
                        {
                            context.AddErrorResult(PrimaryProperty,
                                "The first threshold needs to be equal to or greater than the second.");
                        }
                        break;
                    case PlanogramValidationType.LessThan:
                    case PlanogramValidationType.LessThanOrEqualTo:
                        if (threshold1 > threshold2)
                        {
                            context.AddErrorResult(PrimaryProperty,
                                "The first threshold needs to be equal to or less than the second.");
                        }
                        break;
                    case PlanogramValidationType.EqualTo:
                        break;
                    case PlanogramValidationType.Contains:
                        break;
                    case PlanogramValidationType.DoesNotContain:
                        break;
                    case PlanogramValidationType.NotEqualTo:
                        break;
                    case PlanogramValidationType.True:
                        break;
                    case PlanogramValidationType.False:
                        break;
                    default:
                        context.AddErrorResult(PrimaryProperty, "Unknown Validation Type.");
                        break;
                }
            }
        }

        /// <summary>
        ///     Builds a descriptive list of errors from broken rules within the root.
        /// </summary>
        /// <param name="root">Root object from wich to return descriptions of all errors.</param>
        /// <param name="verbose">If <c>true</c>, the list will include tracing information about the broken rules.</param>
        /// <returns>A <see cref="String"/> with a list of all error description in the root or children.</returns>
        public static String BuildErrorsDescription(this IEditableBusinessObject root, Boolean verbose = false)
        {
            var allBrokenRules = BusinessRules.GetAllBrokenRules(root);
            var itemsWithErrors =
                allBrokenRules.Where(
                    r => r.BrokenRules != null && r.BrokenRules.Any(rule => rule.Severity == RuleSeverity.Error))
                    .Select(r => r.Object as BusinessBase).Where(o => o != null);
            var errorDescriptions = itemsWithErrors.SelectMany(o => o.BrokenRulesCollection.Select(rule => verbose ? String.Format("[{0}] Rule {1} for property {2} is broken: {3}", o.GetType().Name, rule.RuleName, rule.Property, rule.Description) : rule.Description));
            return String.Join(Environment.NewLine, errorDescriptions);
        }

      
        /// <summary>
        /// Creates a GS1 standard checksum value for a supplied code. Which is based on
        /// a standard mod 10 check digit calculation. This will only fail if there is an
        /// unexpected character in the code. This is for use with GTIN-8, GTIN-12,
        /// GTIN-13 and GTIN-14.
        /// </summary>
        /// <param name="code">A code of length 7, 11, 12 or 13. Each char should be a number</param>
        /// <param name="checkSum"></param>
        /// <returns></returns>
        public static Boolean CreateGS1StandardCheckSum(String code, out Int32 checkSum)
        {
            checkSum = 0;
            Boolean odd = true;
            foreach (Char digit in code.Reverse())
            {
                Int32 value = 0;

                if (Char.IsNumber(digit))
                {
                    value = Convert.ToInt32(Char.GetNumericValue(digit));
                }
                else
                {
                    checkSum = 0;
                    return false; // anything other than all numbers is a fail
                }

                if (odd)
                {
                    checkSum += value * 3;
                }
                else
                {
                    checkSum += value;
                }

                odd = !odd;
            }

            //calculate check sum
            Int32 mod10 = (checkSum % 10);
            checkSum = mod10 > 0 ? 10 - (checkSum % 10) : 0;

            return true;
        }
        #endregion
    }

    public enum GS1StandardGtinValidationFailType
    {
        None,
        InvalidLength,
        UnexpectedCharacter,
        FailedChecksum
    }

   
}
