#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History : CCM 830

// V8-32787 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    ///     Class implementing <see cref="IAssortmentRuleEnforcer"/> that allows detecting broken assortment rules.
    /// </summary>
    public class AssortmentRuleEnforcer : IAssortmentRuleEnforcer 
    {
        /// <summary>
        ///     Get the broken assortment rules for the given <paramref name="assortmentProduct"/>.
        /// </summary>
        /// <param name="assortmentProduct">The <see cref="PlanogramAssortmentProduct"/> to check for broken rules.</param>
        /// <param name="merchGroup"></param>
        /// <returns>A collection of <see cref="IBrokenAssortmentRule"/> intances, one for each broken rule found with the type of rule embedded in them.</returns>
        public IEnumerable<IBrokenAssortmentRule> GetBrokenRulesFor(PlanogramAssortmentProduct assortmentProduct, PlanogramMerchandisingGroup merchGroup)
        {
            if (assortmentProduct == null) return new List<IBrokenAssortmentRule>();

            IEnumerable<PlanogramPosition> affectedPositions = GetAffectedPositions(assortmentProduct.GetPlanogramProduct(), merchGroup).ToList();
            switch (assortmentProduct.RuleType)
            {
                case PlanogramAssortmentProductRuleType.None:
                    return GetNoneBrokenAssortmentRules(assortmentProduct, affectedPositions);
                case PlanogramAssortmentProductRuleType.Force:
                    return GetForceBrokenAssortmentRules(assortmentProduct, affectedPositions);
                case PlanogramAssortmentProductRuleType.Preserve:
                    return GetPreserveBrokenAssortmentRules(assortmentProduct, affectedPositions)
                        .Union(GetNoneBrokenAssortmentRules(assortmentProduct, affectedPositions));
                case PlanogramAssortmentProductRuleType.MinimumHurdle:
                    return GetMinimumHurdleBrokenAssortmentRules(assortmentProduct, affectedPositions)
                        .Union(GetNoneBrokenAssortmentRules(assortmentProduct, affectedPositions));
                case PlanogramAssortmentProductRuleType.Core:
                    // No checks needed for inventory.
                    return new List<IBrokenAssortmentRule>();
                default:
                    Debug.Fail("Unknown Rule Type when calling GetBrokenRulesFor()");
                    return new List<IBrokenAssortmentRule>();
            }
        }

        /// <summary>
        ///     Get whether the Minimum Hurdle Units or Facings rule is broken.
        /// </summary>
        /// <param name="assortmentProduct">The <see cref="PlanogramAssortmentProduct"/> to check for broken Minimum Hurdle rules.</param>
        /// <param name="affectedPositions"></param>
        /// <returns></returns>
        private static IEnumerable<IBrokenAssortmentRule> GetMinimumHurdleBrokenAssortmentRules(
            PlanogramAssortmentProduct assortmentProduct,
            IEnumerable<PlanogramPosition> affectedPositions)
        {
            if (assortmentProduct == null) return new List<IBrokenAssortmentRule>();

            var brokenAssortmentRules = new List<IBrokenAssortmentRule>();
            Int16? minValue = assortmentProduct.MinListUnits ?? assortmentProduct.MinListFacings;
            if (assortmentProduct.RuleValueType == PlanogramAssortmentProductRuleValueType.Units)
            {
                // Check Min List Units rule.
                if (minValue.HasValue &&
                    affectedPositions.Sum(position => position.GetTotalUnitCount()) < minValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.MinimumHurdleUnitsRule);
            }
            else
            {
                // Check Min List Facings rule.
                if (minValue.HasValue &&
                    affectedPositions.Sum(position => position.FacingsWide) < minValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.MinimumHurdleFacingsRule);
            }

            return brokenAssortmentRules;
        }

        /// <summary>
        ///     Get whether the Preserve Units or Facings rule is broken.
        /// </summary>
        /// <param name="assortmentProduct">The <see cref="PlanogramAssortmentProduct"/> to check for broken Preserve rules.</param>
        /// <param name="affectedPositions"></param>
        /// <returns></returns>
        private static IEnumerable<IBrokenAssortmentRule> GetPreserveBrokenAssortmentRules(
            PlanogramAssortmentProduct assortmentProduct,
            IEnumerable<PlanogramPosition> affectedPositions)
        {
            if (assortmentProduct == null) return new List<IBrokenAssortmentRule>();

            var brokenAssortmentRules = new List<IBrokenAssortmentRule>();
            Int16? minValue = assortmentProduct.PreserveListUnits ?? assortmentProduct.PreserveListFacings;
            if (assortmentProduct.RuleValueType == PlanogramAssortmentProductRuleValueType.Units)
            {
                // Check Min List Units rule.
                if (minValue.HasValue &&
                    affectedPositions.Sum(position => position.GetTotalUnitCount()) < minValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.PreserveUnitsRule);
            }
            else
            {
                // Check Min List Facings rule.
                if (minValue.HasValue &&
                    affectedPositions.Sum(position => position.FacingsWide) < minValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.PreserveFacingsRule);
            }

            return brokenAssortmentRules;
        }

        /// <summary>
        ///     Get whether the Force Units or Facings rule is broken.
        /// </summary>
        /// <param name="assortmentProduct">The <see cref="PlanogramAssortmentProduct"/> to check for broken Force rules.</param>
        /// <param name="affectedPositions"></param>
        /// <returns></returns>
        private static IEnumerable<IBrokenAssortmentRule> GetForceBrokenAssortmentRules(
            PlanogramAssortmentProduct assortmentProduct,
            IEnumerable<PlanogramPosition> affectedPositions)
        {
            if (assortmentProduct == null) return new List<IBrokenAssortmentRule>();

            var brokenAssortmentRules = new List<IBrokenAssortmentRule>();
            Int16? exactValue = assortmentProduct.ExactListUnits ?? assortmentProduct.ExactListFacings;
            if (assortmentProduct.RuleValueType == PlanogramAssortmentProductRuleValueType.Units)
            {
                // Check exact List Units rule.
                if (exactValue.HasValue &&
                    affectedPositions.Sum(position => position.GetTotalUnitCount()) != exactValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.ExactListUnitsRule);
            }
            else
            {
                // Check exact List Facings rule.
                if (exactValue.HasValue &&
                    affectedPositions.Sum(position => position.FacingsWide) != exactValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.ExactListFacingsRule);
            }

            return brokenAssortmentRules;
        }

        private static IEnumerable<PlanogramPosition> GetAffectedPositions(PlanogramProduct product, PlanogramMerchandisingGroup merchGroup = null)
        {
            if (product == null) return new List<PlanogramPosition>();

            IEnumerable<PlanogramPosition> currentPositions;
            if (merchGroup != null)
            {
                currentPositions = merchGroup.PositionPlacements
                                             .Where(placement => Equals(placement.Product.Id, product.Id))
                                             .Select(placement => placement.Position).ToArray();
            }
            else
            {
                currentPositions = new List<PlanogramPosition>();
            }

            Planogram plan = product.Parent;

            IEnumerable<PlanogramPosition> positionsOtherThanCurrentMerchGroup =
                product.GetPlanogramPositions()
                .Where(position => 
                currentPositions.All(current => 
                !Equals(current.Id, position.Id) 
                && !Equals(current.SequenceColour, position.SequenceColour) 
                && !Equals(plan.Products.FindById(current.PlanogramProductId).Gtin /*current.GetPlanogramProduct().Gtin*/, position.GetPlanogramProduct().Gtin)));

            IEnumerable<PlanogramPosition> positions = currentPositions.Union(positionsOtherThanCurrentMerchGroup).ToArray();
            return positions;
        }

        /// <summary>
        ///     Get whether the None Rule is broken. This is to account for Max Units/Facings.
        /// </summary>
        /// <param name="assortmentProduct">The <see cref="PlanogramAssortmentProduct"/> to check for broken None rules.</param>
        /// <param name="affectedPositions"></param>
        /// <returns></returns>
        private static IEnumerable<IBrokenAssortmentRule> GetNoneBrokenAssortmentRules(
            PlanogramAssortmentProduct assortmentProduct,
            IEnumerable<PlanogramPosition> affectedPositions)
        {
            if (assortmentProduct == null || !assortmentProduct.HasMaximum) return new List<IBrokenAssortmentRule>();

            var brokenAssortmentRules = new List<IBrokenAssortmentRule>();
            Int16? maxValue = assortmentProduct.RuleMaximumValue;
            if (assortmentProduct.RuleMaximumValueType == PlanogramAssortmentProductRuleValueType.Units)
            {
                //  Check Max List Units rule.
                if (maxValue.HasValue &&
                    affectedPositions.Sum(position => position.GetTotalUnitCount()) > maxValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.MaxListUnitsRule);
            }
            else
            {
                //  Check Max List Facings rule.
                if (maxValue.HasValue &&
                    affectedPositions.Sum(position => position.FacingsWide) > maxValue.Value)
                    brokenAssortmentRules.Add(BrokenAssortmentRuleFactory.MaxListFacingsRule);
            }

            return brokenAssortmentRules;
        }
    }

    /// <summary>
    ///     Static Factory class to generate the broken rules instances.
    /// </summary>
    public static class BrokenAssortmentRuleFactory
    {
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenMaxListUnitsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule MaxListUnitsRule { get { return new BrokenMaxListUnitsAssortmentRule(); } }
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenMaxListFacingsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule MaxListFacingsRule { get { return new BrokenMaxListFacingsAssortmentRule(); } }
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenPreserveUnitsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule PreserveUnitsRule { get { return new BrokenPreserveUnitsAssortmentRule(); } }
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenPreserveFacingsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule PreserveFacingsRule { get { return new BrokenPreserveFacingsAssortmentRule(); } }
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenMinimumHurdleUnitsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule MinimumHurdleUnitsRule { get { return new BrokenMinimumHurdleUnitsAssortmentRule(); } }
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenMinimumHurdleFacingsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule MinimumHurdleFacingsRule { get { return new BrokenMinimumHurdleFacingsAssortmentRule(); } }
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenExactListUnitsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule ExactListUnitsRule { get { return new BrokenExactListUnitsAssortmentRule(); } }
        /// <summary>
        ///     Gets a new instance of <see cref="BrokenExactListFacingsAssortmentRule"/>.
        /// </summary>
        public static IBrokenAssortmentRule ExactListFacingsRule { get { return new BrokenExactListFacingsAssortmentRule(); } }
    }

    /// <summary>
    ///     Enumeration for the possible types of rules that can be broken.
    /// </summary>
    public enum BrokenRuleType
    {
        ProductMaxUnits,
        ProductMaxFacings,
        ProductPreserveUnits,
        ProductPreserveFacings,
        ProductMinimumHurdleUnits,
        ProductMinimumHurdleFacings,
        ProductExactUnits,
        ProductExactFacings
    }

    /// <summary>
    ///     This class encapsulates a broken Exact List Facings Assortment Rule.
    /// </summary>
    public class BrokenExactListFacingsAssortmentRule : IBrokenAssortmentRule 
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductExactFacings; } }
    }

    /// <summary>
    ///     This class encapsulates a broken Exact List Units Assortment Rule.
    /// </summary>
    public class BrokenExactListUnitsAssortmentRule : IBrokenAssortmentRule 
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductExactUnits; } }
    }

    /// <summary>
    ///     This class encapsulates a broken Minimum Hurdle Facings Assortment Rule.
    /// </summary>
    public class BrokenMinimumHurdleFacingsAssortmentRule : IBrokenAssortmentRule
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductMinimumHurdleFacings; } }
    }

    /// <summary>
    ///     This class encapsulates a broken Minimum Hurdle Units Assortment Rule.
    /// </summary>
    public class BrokenMinimumHurdleUnitsAssortmentRule : IBrokenAssortmentRule
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductMinimumHurdleUnits; } }
    }

    /// <summary>
    ///     This class encapsulates a broken Preserve Facings Assortment Rule.
    /// </summary>
    public class BrokenPreserveFacingsAssortmentRule : IBrokenAssortmentRule
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductPreserveFacings; } }
    }

    /// <summary>
    ///     This class encapsulates a broken Preserve Units Assortment Rule.
    /// </summary>
    public class BrokenPreserveUnitsAssortmentRule : IBrokenAssortmentRule
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductPreserveUnits; } }
    }

    /// <summary>
    ///     This class encapsulates a broken Max List Facings Assortment Rule.
    /// </summary>
    public class BrokenMaxListFacingsAssortmentRule : IBrokenAssortmentRule
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductMaxFacings; } }
    }

    /// <summary>
    ///     This class encapsulates a broken Max List Units Assortment Rule.
    /// </summary>
    public class BrokenMaxListUnitsAssortmentRule : IBrokenAssortmentRule
    {
        public BrokenRuleType Type { get { return BrokenRuleType.ProductMaxUnits; } }
    }

    /// <summary>
    ///     This class represents a class that can enforce Assortment Rules.
    /// </summary>
    public interface IAssortmentRuleEnforcer
    {
        IEnumerable<IBrokenAssortmentRule> GetBrokenRulesFor(PlanogramAssortmentProduct assortmentProduct, PlanogramMerchandisingGroup merchGroup);
    }

    /// <summary>
    ///     This interface represents a class that can encapsulate a broken Assortment Rule.
    /// </summary>
    public interface IBrokenAssortmentRule
    {
        BrokenRuleType Type { get; }
    }
}