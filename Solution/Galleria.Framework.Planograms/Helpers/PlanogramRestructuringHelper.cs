﻿// Copyright © Galleria RTS Ltd 2016
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Csla.Core;
using Csla.Rules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Merchandising;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Contains helpers to be used when restructuring a planogram.
    /// </summary>
    public static class PlanogramRestructuringHelper
    {
        /// <summary>
        /// Restructures the plan so that it has a number of bays equal to the bayCount
        /// supplied. The bay widths will be created as a ratio of the original plan size based on
        /// the bayCount. The backboard settings from the first backboard on the plan will be
        /// used for all backboard in the resturcture.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="ratio"></param>
        public static void RestructureBaysByCount(Planogram plan, Int32 bayCount, Boolean splitComponents = true)
        {
            List<Double> ratio = new List<Double>();
            for (Int32 x = 0; x < bayCount; x++)
            {
                ratio.Add(1);
            }

            RestructureBaysByRatio(plan, ratio, splitComponents);
        }

        /// <summary>
        /// Restructures the plan so that it has a number of bays equal to the number of ratio values
        /// supplied. The bay widths will be created as a ratio of the original plan size based on
        /// the ratio numbers. The backboard settings from the first backboard on the plan will be
        /// used for all backboard in the resturcture.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="ratio"></param>
        public static void RestructureBaysByRatio(Planogram plan, IEnumerable<Double> ratio, Boolean splitComponents = true)
        {
            List<Double> sizes = new List<Double>();
            Double ratioSum = ratio.Sum();
            foreach (Double val in ratio)
            {
                sizes.Add((val / ratioSum) * plan.Width);
            }
            RestructureBaysBySize(plan, sizes, splitComponents);
        }

        /// <summary>
        /// Restructures the plan so that it has a number of bays equal to the number of bay size values
        /// supplied. The backboard settings from the first backboard on the plan will be
        /// used for all backboard in the resturcture.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="baySizes"></param>
        public static void RestructureBaysBySize(Planogram plan, IEnumerable<Double> baySizes, Boolean splitComponents = true)
        {
            RestructureBaysBySize(plan, plan.FixtureItems, baySizes, splitComponents);
        }

        /// <summary>
        /// Restructures a list of fixture items on a plan so that they have a number of bays equal to the number
        /// of bay size values supplied. The backboard settings from the first backboard on the plan will be
        /// used for all backboard in the resturcture.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="baySizes"></param>
        public static void RestructureBaysBySize(Planogram plan, IEnumerable<PlanogramFixtureItem> fixtureItems, IEnumerable<Double> baySizes, Boolean splitComponents = true)
        {
            PlanogramFixtureItem target = null;
            PointValue startPoint = new PointValue();
            List<PlanogramFixtureItem> addedItems = new List<PlanogramFixtureItem>();
            //Reduce to 1 bay
            foreach (PlanogramFixtureItem item in fixtureItems.OrderBy(f => f.X).ToList())
            {
                if (target == null)
                {
                    target = item;
                    startPoint = new PointValue(item.X, item.Y, item.Z);
                }
                else
                {
                    MovePlanogramItems(item, target);
                    plan.FixtureItems.Remove(item);
                    plan.Fixtures.Remove(item.GetPlanogramFixture());
                }
            }

            //ratio.ForEach(r => r /= ratioSum);
            PlanogramFixture originalFixture = target.GetPlanogramFixture();
            PlanogramComponent originalBackboard = originalFixture.Components.Select(c => c.GetPlanogramComponent()).FirstOrDefault(f => f.ComponentType == PlanogramComponentType.Backboard);
            PlanogramSubComponent originalBackboardSub = null;
            Int32 fill = 0;
            if (originalBackboard != null)
            {
                originalBackboardSub = originalBackboard.SubComponents[0];
                fill = originalBackboardSub.FillColourFront;
            }
            PlanogramComponent originalBase = originalFixture.Components.Select(c => c.GetPlanogramComponent()).FirstOrDefault(f => f.ComponentType == PlanogramComponentType.Base);
            Int32 baseFill = 0;
            PlanogramSubComponent originalBaseSub = null;
            if (originalBase != null)
            {
                originalBaseSub = originalBase.SubComponents[0];
                baseFill = originalBaseSub.FillColourFront;
            }

            List<PlanogramFixtureComponent> components = new List<PlanogramFixtureComponent>(originalFixture.Components);
            List<PlanogramFixtureAssembly> assemblies = new List<PlanogramFixtureAssembly>(originalFixture.Assemblies);
            List<PlanogramPosition> positions = new List<PlanogramPosition>(plan.Positions);
            List<PlanogramAnnotation> annotations = new List<PlanogramAnnotation>(plan.Annotations);

            PlanogramFixtureItem lastItem = target;

            Int32 bayNumber = 0;
            List<Double> finalSizes = baySizes.ToList();
            Double proposedSize = finalSizes.Sum();

            //Extend the last bay to take up the missing space.
            //Later functionality could be extended to do different things.
            //if (proposedSize.LessThan(plan.Width))
            //{
            //    finalSizes[finalSizes.Count - 1] += (plan.Width - proposedSize);
            //}

            foreach (PlanogramFixtureItem item in plan.FixtureItems.Where(f => f.BaySequenceNumber > target.BaySequenceNumber).ToList())
            {
                item.BaySequenceNumber += (Int16)(finalSizes.Count - 1);
            }

            List<PlanogramComponent> componentsToRename = new List<PlanogramComponent>();
            MatrixValue initalBayRotation = MatrixValue.CreateRotationMatrix(target.Angle, 0, 0);
            Int16 squenceNumber = target.BaySequenceNumber;
            for (Int32 b = 0; b < finalSizes.Count; b++)
            {
                Double bayWidth = finalSizes[b];
                bayNumber++;

                PlanogramFixture currentFixture = plan.Fixtures.Add(String.Format(Message.PlanogramFixture_Name_New, originalFixture.Name, bayNumber));
                currentFixture.Width = (Single)bayWidth;
                currentFixture.Height = originalFixture.Height;
                currentFixture.Depth = originalFixture.Depth;

                PlanogramFixtureItem currentFixtureItem = plan.FixtureItems.Add(currentFixture);
                addedItems.Add(currentFixtureItem);
                currentFixtureItem.X = startPoint.X;
                currentFixtureItem.Y = startPoint.Y;
                currentFixtureItem.Z = startPoint.Z;
                currentFixtureItem.Angle = target.Angle;
                currentFixtureItem.BaySequenceNumber = squenceNumber;
                squenceNumber++;

                List<PlanogramFixtureComponent> moveComponents = components.Where(c => (b == 0 || c.X + target.X >= currentFixtureItem.X) && c.X + target.X < currentFixtureItem.X + bayWidth).ToList();
                IEnumerable<PlanogramFixtureAssembly> moveAssemblies = assemblies.Where(c => (b == 0 || c.X + target.X >= currentFixtureItem.X) && c.X + target.X < currentFixtureItem.X + bayWidth);
                List<PlanogramPosition> movePositions = positions.Where(p => moveComponents.Select(c => c.Id).Contains(p.PlanogramFixtureComponentId) ||
                                                    moveAssemblies.Select(c => c.Id).Contains(p.PlanogramFixtureAssemblyId)).ToList();
                IEnumerable<PlanogramAnnotation> moveAnnotations = annotations.Where(p => moveComponents.Select(c => c.Id).Contains(p.PlanogramFixtureComponentId) ||
                                                    moveAssemblies.Select(c => c.Id).Contains(p.PlanogramFixtureAssemblyId));

                PointValue translation = new PointValue((Single)bayWidth, 0, 0);
                translation = initalBayRotation.Transform(translation);

                if (splitComponents && b != finalSizes.Count - 1)
                {
                    #region Split components that can merge
                    foreach (PlanogramFixtureComponent fComponent in moveComponents.ToList())
                    {
                        PlanogramComponent originalComponent = fComponent.GetPlanogramComponent();
                        MatrixValue initalComponentRotation = MatrixValue.CreateRotationMatrix(fComponent.Angle, fComponent.Slope, fComponent.Roll);
                        //only split very specific components for now
                        if (originalComponent != null && (originalComponent.ComponentType == PlanogramComponentType.Shelf || originalComponent.ComponentType == PlanogramComponentType.Bar))
                        {
                            PointValue size = new PointValue(originalComponent.Width, 0, 0);
                            size = initalComponentRotation.Transform(size);

                            Double endX = fComponent.X + size.X;
                            Single xOffset = currentFixtureItem.X - lastItem.X;
                            if (endX.GreaterThan(xOffset + bayWidth))
                            {
                                PlanogramFixture sourceFixture = lastItem.GetPlanogramFixture();
                                PlanogramSubComponent originalSubComponent = originalComponent.SubComponents.First();

                                #region Remove components
                                componentsToRename.Remove(originalComponent);
                                sourceFixture.Components.Remove(fComponent);
                                components.Remove(fComponent);
                                moveComponents.Remove(fComponent);
                                plan.Components.Remove(originalComponent);
                                #endregion

                                Single ratio1 = (Single)(bayWidth - (fComponent.X - xOffset)) / size.X;
                                PointValue c1Size = new PointValue(originalComponent.Width * ratio1, size.Y * ratio1, size.Z * ratio1);
                                PointValue c2Size = new PointValue(originalComponent.Width - c1Size.X, 0, 0);

                                PlanogramFixtureComponent fc1 = sourceFixture.Components.Add(originalComponent.ComponentType, c1Size.X, originalComponent.Height, originalComponent.Depth, originalSubComponent.FillColourFront);
                                PlanogramComponent c1 = fc1.GetPlanogramComponent();
                                c1.Name = originalComponent.Name;
                                fc1.X = fComponent.X;
                                fc1.Y = fComponent.Y;
                                fc1.Z = fComponent.Z;
                                fc1.Slope = fComponent.Slope;
                                fc1.Angle = fComponent.Angle;
                                fc1.Roll = fComponent.Roll;
                                components.Add(fc1);
                                moveComponents.Add(fc1);
                                componentsToRename.Add(c1);

                                PlanogramFixtureComponent fc2 = sourceFixture.Components.Add(originalComponent.ComponentType, c2Size.X, originalComponent.Height, originalComponent.Depth, originalSubComponent.FillColourFront);
                                PlanogramComponent c2 = fc2.GetPlanogramComponent();
                                c2.Name = originalComponent.Name;
                                fc2.X = xOffset + translation.X;
                                fc2.Y = fComponent.Y + c1Size.Y;
                                fc2.Z = fComponent.Z + c1Size.Z;
                                fc2.Slope = fComponent.Slope;
                                fc2.Angle = fComponent.Angle;
                                fc2.Roll = fComponent.Roll;
                                components.Add(fc2);
                                componentsToRename.Add(c2);

                                PlanogramSubComponent c1SubComponent = c1.SubComponents.First();
                                PlanogramSubComponent c2SubComponent = c2.SubComponents.First();
                                Object tempId = c1SubComponent.Id;
                                c1SubComponent = originalSubComponent.Copy();
                                c1SubComponent.Id = tempId;
                                c1SubComponent.Width = c1Size.X;
                                c1.SubComponents.Clear();
                                c1.SubComponents.Add(c1SubComponent);

                                tempId = c2SubComponent.Id;
                                c2SubComponent = originalSubComponent.Copy();
                                c2SubComponent.Id = tempId;
                                c2SubComponent.Width = c2Size.X;
                                c2.SubComponents.Clear();
                                c2.SubComponents.Add(c2SubComponent);

                                switch (originalSubComponent.CombineType)
                                {

                                    case PlanogramSubComponentCombineType.LeftOnly:
                                        c1SubComponent.CombineType = PlanogramSubComponentCombineType.Both;
                                        c2SubComponent.CombineType = PlanogramSubComponentCombineType.LeftOnly;
                                        break;
                                    case PlanogramSubComponentCombineType.RightOnly:
                                        c1SubComponent.CombineType = PlanogramSubComponentCombineType.RightOnly;
                                        c2SubComponent.CombineType = PlanogramSubComponentCombineType.Both;
                                        break;
                                    case PlanogramSubComponentCombineType.Both:
                                        c1SubComponent.CombineType = PlanogramSubComponentCombineType.Both;
                                        c2SubComponent.CombineType = PlanogramSubComponentCombineType.Both;
                                        break;
                                    case PlanogramSubComponentCombineType.None:
                                    default:
                                        c1SubComponent.CombineType = PlanogramSubComponentCombineType.RightOnly;
                                        c2SubComponent.CombineType = PlanogramSubComponentCombineType.LeftOnly;
                                        break;
                                }

                                foreach (PlanogramPosition position in movePositions.Where(p => Equals(p.PlanogramFixtureComponentId, fComponent.Id)).ToList())
                                {
                                    position.PlanogramFixtureComponentId = fc1.Id;
                                    position.PlanogramSubComponentId = c1.SubComponents.First().Id;
                                }
                            }
                        }
                    }
                    #endregion
                }

                MovePlanogramItems(lastItem, currentFixtureItem, moveComponents, moveAssemblies, movePositions, moveAnnotations);

                //if we had a backboard originally then make a new one
                if (originalBackboard != null)
                {
                    PlanogramFixtureComponent backboardFc = currentFixture.Components.Add(PlanogramComponentType.Backboard, (Single)bayWidth, currentFixture.Height, 1, fill);
                    PlanogramComponent backboard = plan.Components.FindById(backboardFc.PlanogramComponentId);

                    if (originalBackboardSub != null)
                    {
                        PlanogramSubComponent backBoardSub = backboard.SubComponents.First();
                        backBoardSub.IsNotchPlacedOnFront = originalBackboardSub.IsNotchPlacedOnFront;
                        backBoardSub.NotchHeight = originalBackboardSub.NotchHeight;
                        backBoardSub.NotchSpacingX = originalBackboardSub.NotchSpacingX;
                        backBoardSub.NotchSpacingY = originalBackboardSub.NotchSpacingY;
                        backBoardSub.NotchStartX = originalBackboardSub.NotchStartX;
                        backBoardSub.NotchStartY = originalBackboardSub.NotchStartY;
                        backBoardSub.NotchStyleType = originalBackboardSub.NotchStyleType;
                        backBoardSub.NotchWidth = originalBackboardSub.NotchWidth;

                    }
                    // PlanogramView.AdjustDefaultsForUOM(settings.LengthUnitOfMeasure, backboard);
                    backboard.Width = currentFixture.Width;
                    backboard.Height = originalBackboard.Height;
                    backboardFc.Z = -backboard.Depth;
                }
                //if we had a base originally then make a new one
                if (originalBase != null)
                {
                    PlanogramFixtureComponent baseFc = currentFixture.Components.Add(PlanogramComponentType.Base, (Single)bayWidth, originalBase.Height, originalBase.Depth, baseFill);
                    PlanogramComponent baseC = plan.Components.FindById(baseFc.PlanogramComponentId);
                    baseC.Width = currentFixture.Width;
                }


                startPoint += translation;
            }

            //Rename components that have been split
            List<String> splitComponentNames = new List<String>();
            foreach (PlanogramComponent component in componentsToRename)
            {
                Int32 i = 1;
                String name = String.Format("{0} ({1})", component.Name, i);
                while (splitComponentNames.Contains(name))
                {
                    i++;
                    name = String.Format("{0} ({1})", component.Name, i);
                }
                splitComponentNames.Add(name);
                component.Name = name;
            }

            plan.FixtureItems.Remove(target);
            plan.Fixtures.Remove(target.GetPlanogramFixture());



        }

        public static List<Double> BayWidthsByComponent(Planogram plan, Boolean byStart = true, Boolean byEnd = true)
        {
            List<Double> output = new List<Double>();
            foreach (PlanogramFixtureItem item in plan.FixtureItems)
            {
                output.AddRange(BayWidthsByComponent(plan, item, byStart, byEnd));
            }

            return output;
        }

        public static List<Double> BayWidthsByComponent(Planogram plan, PlanogramFixtureItem fixtureItem, Boolean byStart = true, Boolean byEnd = true)
        {
            Debug.Assert(byStart || byEnd, "Bay widths must be split by either start or end or both.");

            List<Double> output = new List<Double>();
            PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
            List<PlanogramFixtureComponent> fComponents = new List<PlanogramFixtureComponent>(fixture.Components.Where(f => f.GetPlanogramComponent() != null).OrderBy(c => c.X));
            List<Double> componentXStarts = fComponents.Select(f => Math.Max(f.X, 0d)).ToList();
            List<Double> componentXEnds = fComponents.Select(f => Math.Min(f.X + f.GetPlanogramComponent().Width, (Double)fixture.Width)).ToList();
            List<Double> bays = new List<Double>();

            if (byStart && byEnd)
            {
                bays = componentXStarts.Union(componentXEnds).Distinct().OrderBy(c => c).ToList();
            }
            else if (byStart)
            {
                bays = componentXStarts.Distinct().OrderBy(c => c).ToList();
            }
            else if (byEnd)
            {
                bays = componentXEnds.Distinct().OrderBy(c => c).ToList();
            }

            Double sum = 0;
            foreach (Double bayX in bays)
            {
                Double width = bayX - sum;
                if (width > 0)
                {
                    output.Add(width);
                    sum += width;
                }
            }
            Double totalWidth = fixture.Width;
            if (totalWidth.GreaterThan(sum))
            {
                output.Add(totalWidth - sum);
            }

            return output;
        }


        /// <summary>
        /// Moves all the planogram items from one fixture item to another, ignoring backboard and base components.
        /// </summary>
        /// <param name="component"></param>
        public static void MovePlanogramItems(PlanogramFixtureItem sourceFixtureItem, PlanogramFixtureItem targetFixtureItem)
        {
            if (!sourceFixtureItem.Equals(targetFixtureItem))
            {
                PlanogramFixture sourceFixture = sourceFixtureItem.GetPlanogramFixture();
                MovePlanogramItems(sourceFixtureItem, targetFixtureItem,
                    sourceFixture.Components,
                    sourceFixture.Assemblies,
                    sourceFixtureItem.Parent.Positions.Where(p => p.PlanogramFixtureItemId.Equals(sourceFixtureItem.Id)),
                    sourceFixtureItem.Parent.Annotations.Where(a => a.PlanogramFixtureItemId.Equals(sourceFixtureItem.Id)));
            }
        }

        /// <summary>
        /// Moves planogram items from one fixture item to another fixture item. Ignoring backboard and base items.
        /// </summary>
        /// <param name="sourceFixtureItem"></param>
        /// <param name="targetFixtureItem"></param>
        /// <param name="components"></param>
        /// <param name="assemblies"></param>
        /// <param name="positions"></param>
        /// <param name="annotations"></param>
        public static void MovePlanogramItems(PlanogramFixtureItem sourceFixtureItem, PlanogramFixtureItem targetFixtureItem,
            IEnumerable<PlanogramFixtureComponent> components,
            IEnumerable<PlanogramFixtureAssembly> assemblies,
             IEnumerable<PlanogramPosition> positions,
             IEnumerable<PlanogramAnnotation> annotations)
        {
            if (!sourceFixtureItem.Equals(targetFixtureItem))
            {
                PlanogramFixture sourceFixture = sourceFixtureItem.GetPlanogramFixture();
                PlanogramFixture targetFixture = targetFixtureItem.GetPlanogramFixture();
                PlanogramComponent targetBackboard = targetFixture.Components.Select(c => c.GetPlanogramComponent()).FirstOrDefault(pc => pc.ComponentType == PlanogramComponentType.Backboard);
                PlanogramComponent targetBase = targetFixture.Components.Select(c => c.GetPlanogramComponent()).FirstOrDefault(pc => pc.ComponentType == PlanogramComponentType.Base);

                //foreach (PlanogramPosition pos in positions.ToList())
                //{
                //    pos.PlanogramFixtureItemId = targetFixtureItem.Id;
                //}
                foreach (PlanogramAnnotation anno in annotations.ToList())
                {
                    anno.PlanogramFixtureItemId = targetFixtureItem.Id;
                }
                Single xMovement = sourceFixtureItem.X - targetFixtureItem.X;
                Single yMovement = 0; //sourceFixtureItem.Y - targetFixtureItem.Y;
                Single zMovement = 0; //sourceFixtureItem.Z - targetFixtureItem.Z;
                foreach (PlanogramFixtureComponent fixtureComponent in components.ToList())
                {
                    PlanogramComponent component = fixtureComponent.GetPlanogramComponent();

                    switch (component.ComponentType)
                    {
                        case PlanogramComponentType.Backboard:
                        case PlanogramComponentType.Base:
                            break;
                        default:
                            fixtureComponent.X += xMovement;
                            fixtureComponent.Y += yMovement;
                            fixtureComponent.Z += zMovement;
                            sourceFixture.Components.Remove(fixtureComponent);
                            PlanogramFixtureComponent newComponent = fixtureComponent.Copy();
                            targetFixture.Components.Add(newComponent);

                            foreach (PlanogramPosition position in positions.Where(p => Equals(p.PlanogramFixtureComponentId, fixtureComponent.Id)).ToList())
                            {
                                position.PlanogramFixtureItemId = targetFixtureItem.Id;
                                position.PlanogramFixtureComponentId = newComponent.Id;
                                position.PlanogramSubComponentId = newComponent.GetPlanogramComponent().SubComponents.First().Id;
                            }
                            break;
                    }
                }

                foreach (PlanogramFixtureAssembly fixtureAssembly in assemblies.ToList())
                {
                    fixtureAssembly.X += xMovement;
                    fixtureAssembly.Y += yMovement;
                    fixtureAssembly.Z += zMovement;
                    sourceFixture.Assemblies.Remove(fixtureAssembly);
                    targetFixture.Assemblies.Add(fixtureAssembly.Copy());
                }
            }
        }
    }


    /// <summary>
    /// Settings to determine how to resturcture a whole planogram
    /// or just a single bay.
    /// </summary>
    /// <remarks>Needs to be moved to its own file</remarks>
    public class RestructurePlanogramSettings
    {
        private Boolean _canSplitComponents = false;
        private Boolean _splitBaysByComponentStart = false;
        private Boolean _splitBaysByComponentEnd = false;
        private Boolean _splitByFixedSize = true;
        private Boolean _splitByRecurringPattern = false;
        private Boolean _splitByBayCount = false;
        private Double _fixedSize = 0;
        private String _RecurringPattern = "";
        private Int32 _bayCount = 1;


        public Boolean Enabled { get; set; }

        public Boolean CanSplitComponents
        {
            get
            {
                return _canSplitComponents;
            }
            set
            {
                _canSplitComponents = value;
            }
        }
        /// <summary>
        /// If true then any restructured plans will be split into
        /// <see cref="BayCount"/> equaly sized bays.
        /// </summary>
        public Boolean SplitByBayCount
        {
            get
            {
                return _splitByBayCount;
            }
            set
            {
                _splitByBayCount = value;
                if (value)
                {
                    SplitByComponent = false;
                    SplitByFixedSize = false;
                    SplitByRecurringPattern = false;
                }
            }
        }
        public Int32 BayCount
        {
            get
            {
                return _bayCount;
            }
            set
            {
                _bayCount = value;
            }
        }

        public Boolean SplitByComponent
        {
            get
            {
                return _splitBaysByComponentEnd || _splitBaysByComponentStart;
            }
            set
            {
                _splitBaysByComponentEnd = value;
                _splitBaysByComponentStart = value;
            }
        }

        public Boolean SplitByComponentStart
        {
            get
            {
                return _splitBaysByComponentStart;
            }
            set
            {
                _splitBaysByComponentStart = value;
            }
        }
        public Boolean SplitByComponentEnd
        {
            get
            {
                return _splitBaysByComponentEnd;
            }
            set
            {
                _splitBaysByComponentEnd = value;
            }
        }

        public Boolean SplitByFixedSize
        {
            get
            {
                return _splitByFixedSize;
            }
            set
            {
                _splitByFixedSize = value;
            }
        }
        public Double FixedSize
        {
            get
            {
                return _fixedSize;
            }
            set
            {
                _fixedSize = value;
            }
        }

        public Boolean SplitByRecurringPattern
        {
            get
            {
                return _splitByRecurringPattern;
            }
            set
            {
                _splitByRecurringPattern = value;
            }
        }
        public String RecurringPattern
        {
            get
            {
                return _RecurringPattern;
            }
            set
            {
                _RecurringPattern = value;
            }
        }

        #region Helper Properties
        /// <summary>
        /// Based on other properties sets
        /// shows if the splitting by fixed
        /// size is allowed
        /// </summary>
        public Boolean CanSplitByFixedSize
        {
            get
            {
                return _splitByFixedSize && !_splitByBayCount;
            }
        }

        /// <summary>
        /// Based on other properties sets
        /// shows if the splitting by recurring
        /// pattern is allowed
        /// </summary>
        public Boolean CanSplitByRecurringPattern
        {
            get
            {
                return _splitByRecurringPattern && !_splitByBayCount && !_splitByFixedSize;
            }
        }

        public Boolean CanRestructure
        {
            get
            {
                return CanSplitByFixedSize || CanSplitByRecurringPattern || SplitByComponent || SplitByBayCount;
            }
        }
        #endregion

        #region Constructors

        public RestructurePlanogramSettings()
        {
        }

        #endregion

        public void Restructure(Planogram plan)
        {
            //If we are splitting by bay count then we are completely clearing out
            //all of our bays
            if (SplitByBayCount && BayCount > 0)
            {
                PlanogramRestructuringHelper.RestructureBaysByCount(plan, BayCount, _canSplitComponents);
                return;
            }

            List<Double> bayWidths = GetCurrentBayWidths(plan);
            if (SplitByComponent)
            {
                bayWidths = PlanogramRestructuringHelper.BayWidthsByComponent(plan, _splitBaysByComponentStart, _splitBaysByComponentEnd);
            }

            if (CanSplitByFixedSize)
            {
                bayWidths = GetBayWidthsByMaxWidth(bayWidths, _fixedSize);
            }

            if (CanSplitByRecurringPattern)
            {
                bayWidths = GetBayWidthsByPattern(bayWidths, RecurringPattern);
            }

            if (CanRestructure)
            {
                PlanogramRestructuringHelper.RestructureBaysBySize(plan, plan.FixtureItems, bayWidths, _canSplitComponents);
                return;
            }
        }

        public void Restructure(Planogram plan, PlanogramFixtureItem fixtureItem)
        {
            Double bayWidth = GetBayWidth(fixtureItem);
            List<Double> bayWidths = new List<Double>() { bayWidth };

            //If we are splitting by bay count then we are
            //not doing anything else
            if (SplitByBayCount && BayCount > 0)
            {
                bayWidths = GetEvenWidths(bayWidth);
                PlanogramRestructuringHelper.RestructureBaysBySize(plan, new List<PlanogramFixtureItem>() { fixtureItem }, bayWidths, _canSplitComponents);
                return;
            }

            if (SplitByComponent)
            {
                bayWidths = PlanogramRestructuringHelper.BayWidthsByComponent(plan, fixtureItem, _splitBaysByComponentStart, _splitBaysByComponentEnd);
            }

            if (CanSplitByFixedSize)
            {
                bayWidths = GetBayWidthsByMaxWidth(bayWidths, _fixedSize);
            }

            if (CanSplitByRecurringPattern)
            {
                bayWidths = GetBayWidthsByPattern(bayWidths, RecurringPattern);
            }

            if (CanRestructure)
            {
                PlanogramRestructuringHelper.RestructureBaysBySize(plan, new List<PlanogramFixtureItem>() { fixtureItem }, bayWidths, _canSplitComponents);
                return;
            }
        }

        private List<Double> GetEvenWidths(Double totalWidth)
        {
            List<Double> output = new List<Double>();

            for (Int32 x = 0; x < BayCount; x++)
            {
                output.Add(totalWidth / BayCount);
            }

            return output;
        }

        private List<Double> GetCurrentBayWidths(Planogram plan)
        {
            List<Double> output = new List<Double>();
            foreach (PlanogramFixtureItem item in plan.FixtureItems)
            {
                output.Add(GetBayWidth(item));
            }

            return output;
        }

        private Double GetBayWidth(PlanogramFixtureItem fixtureItem)
        {
            PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
            if (fixture != null)
            {
                return fixture.Width;
            }

            return 0;
        }

        private List<Double> GetBayWidthsByMaxWidth(List<Double> originalWidths, Double maxWidth)
        {
            List<Double> output = new List<Double>();

            //do nothing
            if (maxWidth == 0) return originalWidths;

            foreach (Double width in originalWidths)
            {
                if (width.GreaterThan(maxWidth))
                {
                    Int32 wholeBays = (Int32)(width / maxWidth);
                    for (Int32 x = 0; x < wholeBays; x++)
                    {
                        output.Add(maxWidth);
                    }

                    //add the remaining widht if any
                    Double remainingWidth = width - (maxWidth * wholeBays);
                    if (remainingWidth > 0) output.Add(remainingWidth);
                }
                else
                {
                    output.Add(width);
                }
            }

            return output;
        }

        private List<Double> GetBayWidthsByPattern(List<Double> originalWidths, String pattern)
        {

            //Do nothing if there is no pattern
            if (String.IsNullOrWhiteSpace(pattern)) return originalWidths;
            if (pattern.Equals("0")) return originalWidths;

            List<Double> output = new List<Double>();
            String listSeparator = CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            String[] patternArray = pattern.Split(new String[] { listSeparator }, StringSplitOptions.RemoveEmptyEntries);
            List<Double> patternValues = new List<Double>();
            foreach (String part in patternArray)
            {
                Double value;
                if (Double.TryParse(part, out value))
                {
                    patternValues.Add(value);
                }
            }

            //Do nothing if there is still no pattern
            if (!patternValues.Any()) return originalWidths;

            Int32 patternIndex = 0;
            foreach (Double width in originalWidths)
            {
                if (width.GreaterThan(patternValues[patternIndex]))
                {
                    Double tempWidth = 0;
                    while (tempWidth + patternValues[patternIndex] < width)
                    {
                        tempWidth += patternValues[patternIndex];
                        output.Add(patternValues[patternIndex]);
                        patternIndex++;
                        if (patternIndex >= patternValues.Count)
                        {
                            patternIndex = 0;
                        }
                    }

                    //add the remaining widht if any
                    Double remainingWidth = width - tempWidth;
                    if (remainingWidth > 0) output.Add(remainingWidth);
                }
                else
                {
                    output.Add(width);
                }

                patternIndex = 0;
            }

            return output;
        }
    }
}
