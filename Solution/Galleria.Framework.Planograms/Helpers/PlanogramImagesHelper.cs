﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using System;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Contains helpers for dealing with planogram images.
    /// </summary>
    public static class PlanogramImagesHelper
    {
        private static Type _planogramImageRendererType;

        /// <summary>
        ///     Gets the real image provider for planograms to use.
        /// </summary>
        public static IRealImageProvider RealImageProvider { get; private set; }


        /// <summary>
        /// Registers the type of image renderer to use.
        /// </summary>
        public static void RegisterPlanogramImageRenderer<TRender>()
            where TRender : IPlanogramImageRenderer
        {
            _planogramImageRendererType = typeof(TRender);
        }

        /// <summary>
        /// Creates a new instance of the plangoram image renderer
        /// Note - a type must first be registered.
        /// </summary>
        public static IPlanogramImageRenderer GetPlanogramImageRenderer()
        {
            if (_planogramImageRendererType == null) return null;
            return (IPlanogramImageRenderer)Activator.CreateInstance(_planogramImageRendererType);
        }

        public static Boolean IsPlanogramImageRendererRegistered()
        {
            return (_planogramImageRendererType != null);
        }

        /// <summary>
        ///     Registers a real image provider instance to be used by planograms.
        /// </summary>
        /// <param name="realImageProvider">The instance to be used when requiring one implementing <see cref="IRealImageProvider"/>.</param>
        public static void RegisterRealImageProvider(IRealImageProvider realImageProvider)
        {
            if (RealImageProvider != null && !Equals(realImageProvider, RealImageProvider)) RealImageProvider.Dispose();
            RealImageProvider = realImageProvider;
        }

        /// <summary>
        /// Starts fetching the requested image asynchronously.
        /// </summary>
        /// <param name="imageTarget"></param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <param name="imageUpdateCallBack">the callback to invoke when the async fetch is complete.</param>
        /// <param name="userState">The parameter to pass to the callback</param>
        /// <returns>The img if already cached or an empty placeholder</returns>
        public static PlanogramImage GetRealImageAsync(object imageTarget,
            PlanogramImageFacingType facingType, PlanogramImageType imageType,
            Action<Object> imageUpdateCallBack, Object userState)
        {
            return RealImageProvider != null
                       ? RealImageProvider.GetImageAsync(imageTarget, facingType, imageType, imageUpdateCallBack, userState)
                       : null;
        }

        /// <summary>
        /// Returns the real image for the requested parameters.
        /// </summary>
        /// <param name="imageTarget"></param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <returns></returns>
        public static PlanogramImage GetRealImage(Object imageTarget, PlanogramImageFacingType facingType, PlanogramImageType imageType)
        {
            return RealImageProvider != null
                       ? RealImageProvider.GetImage(imageTarget, facingType, imageType)
                       : null;
        }
    }
}
