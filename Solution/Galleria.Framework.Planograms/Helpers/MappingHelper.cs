﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
#endregion

#region Version History: CCM830

// V8-31878 : A.Silva
//  Added SplitBy helper extension method to allow splitting comma separated strings taking escaped characters into account.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Mapping helper class
    /// </summary>
    public static class MappingHelper
    {
        #region Fields
        private static bool _initializing = false; // indicates if the helper is initializing mappings
        private static bool _initialized = false; // indicates if the helper has initialized all mappings
        private static object _initializeLock = new object(); // object used to lock while initializing
        #endregion

        #region Methods
        /// <summary>
        /// Initializes all mappings within the framework
        /// </summary>
        public static void InitializeMappings()
        {
            if ((!_initialized) && (!_initializing))
            {
                lock (_initializeLock)
                {
                    if ((!_initialized) && (!_initializing))
                    {
                        // set the initializing flag to indicate
                        // we are already initializing the mappings
                        _initializing = true;

                        // get the current assembly
                        Assembly assembly = Assembly.GetExecutingAssembly();
                        foreach (Type type in assembly.GetTypes())
                        {
                            if (type.Namespace == "Galleria.Framework.Planograms.Model")
                            {
                                MethodInfo method = type.GetMethod("CreateMappings", BindingFlags.Static | BindingFlags.NonPublic);
                                if (method != null)
                                {
                                    method.Invoke(null, null);
                                }
                            }
                        }

                        // set the initialized flag now to prevent re-entry
                        _initialized = true;
                    }
                }
            }
        }

        /// <summary>
        ///     Split a <see cref="String"/> using the given <paramref name="separator"/> while taking into account escape characters.
        /// </summary>
        /// <param name="rawLine"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static IEnumerable<String> SplitBy(this String rawLine, Char separator)
        {
            const String escapedComma = @"\,";
            const String commaPlaceholder = "&comma;";
            const String comma = ",";
            const String escapedDoubleQuote = @"\""";
            const String doubleQuote = @"""";
            const String escapedNewLine = @"\r\n";

            return rawLine
                .Replace(escapedComma, commaPlaceholder)
                .Replace(escapedDoubleQuote, doubleQuote)
                .Replace(escapedNewLine, Environment.NewLine)
                .Split(separator)
                .Select(d => d.Replace(commaPlaceholder, comma));
        }
        #endregion
    }
}