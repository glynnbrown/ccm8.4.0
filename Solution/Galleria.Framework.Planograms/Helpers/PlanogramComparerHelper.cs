﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31863 : A.Silva
//  Created
// V8-31910 : A.Silva
//  Amended CreateKey(PlanogramPosition) so that positions in assemplies do not cause a Null Reference exception.
// V8-31913 : A.Silva
//  Added filter to products so that non placed products may be ignored when necessary.
// V8-31931 : A.Silva
//  Amended CreateKey(PlanogramPosition) so that the Id is now the position's product GTIN and the multisited order.
// V8-31947 : A.Silva
//  Added CheckComparisonfields to ensure there are always default comparison fields at least.
// V8-31967 : A.Silva
//  AddChangedProducts and Positions now stores the results for every field compared EVEN if not different.
// V8-31945 : A.Silva
//  Refactored to contain the default fields for comparison.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Helpers
{
    public class PlanogramComparerHelper
    {
        #region Fields

        private static readonly List<IModelPropertyInfo> DefaultPositionComparisonFields =
            new List<IModelPropertyInfo>
            {
                PlanogramPosition.FacingsWideProperty,
                PlanogramPosition.FacingsHighProperty,
                PlanogramPosition.FacingsDeepProperty
            };

        private static readonly List<IModelPropertyInfo> DefaultProductComparisonFields =
            new List<IModelPropertyInfo>
            {
                PlanogramProduct.MetaPositionCountProperty
            };

        private Dictionary<String, PlanogramPosition> PositionByKey { get; set; }

        private Dictionary<String, PlanogramPosition> TargetPositionByKey { get; set; }

        #endregion

        #region Properties

        private Planogram Planogram { get; set; }
        private PlanogramComparisonResult TargetResult { get; set; }
        private Dictionary<String, PlanogramProduct> TargetProductByKey { get; set; }
        private Dictionary<String, PlanogramProduct> ProductByKey { get; set; }
        private Dictionary<String, Dictionary<String, Object>> ValuesByFieldByProductKey { get; set; }
        private Dictionary<String, Dictionary<String, Object>> ValuesByFieldByPositionKey { get; set; }
        private List<String> ProductFieldPlaceholders { get; set; }
        private List<String> PositionFieldPlaceholders { get; set; }
        private Dictionary<String, ObjectFieldInfo> InfoByFieldPlaceHolder { get; set; }

        #endregion

        #region Constructor

        public PlanogramComparerHelper(Planogram planogram)
        {
            Planogram = planogram;
            ProductByKey = planogram.Products.Where(IsPlacedProduct).ToLookupDictionary(CreateComparisonKey);
            PositionByKey = planogram.Positions.ToLookupDictionary(CreateComparisonKey);
            CheckComparisonFields(planogram.Comparison);
            ProductFieldPlaceholders = planogram.Comparison.ComparisonFields.Where(field => field.ItemType == PlanogramItemType.Product).Select(field => field.FieldPlaceholder).ToList();
            PositionFieldPlaceholders = planogram.Comparison.ComparisonFields.Where(field => field.ItemType == PlanogramItemType.Position).Select(field => field.FieldPlaceholder).ToList();
            InfoByFieldPlaceHolder =
                PlanogramFieldHelper.GetPlanogramObjectFieldInfos(planogram)
                               .SelectMany(pair => pair.Value)
                               .ToLookupDictionary(info => info.FieldPlaceholder);

            ValuesByFieldByProductKey = LookupValueByFieldByProductKey(InfoByFieldPlaceHolder, ProductFieldPlaceholders);
            ValuesByFieldByPositionKey = LookupValueByFieldByPositionKey(InfoByFieldPlaceHolder, PositionFieldPlaceholders);
        }

        /// <summary>
        ///     Create the correct Validation Key for the given <paramref name="product"/>.
        /// </summary>
        /// <param name="product">The <see cref="PlanogramProduct"/> instance to create a comparison key for.</param>
        /// <returns>A string containing a unique key to use when identifying other items comparable to this one.</returns>
        public static String CreateComparisonKey(PlanogramProduct product)
        {
            return String.Format("{0}", product.Gtin);
        }

        /// <summary>
        ///     Create the correct Validation Key for the given <paramref name="position"/>.
        /// </summary>
        /// <param name="position">The <see cref="PlanogramPosition"/> instance to create a comparison key for.</param>
        /// <returns>A string containing a unique key to use when identifying other items comparable to this one.</returns>
        public static String CreateComparisonKey(PlanogramPosition position)
        {
            PlanogramProduct product = position.GetPlanogramProduct();
            IEnumerable<PlanogramPosition> allPositions = product.GetPlanogramPositions();
            Int32 positionIndex = allPositions.OrderBy(planogramPosition =>
                                                 {
                                                     PlanogramSubComponentPlacement componentPlacement = position.GetPlanogramSubComponentPlacement();
                                                     Int16 baySequenceNumber = componentPlacement.FixtureItem.BaySequenceNumber;
                                                     IPlanogramFixtureComponent planogramFixtureComponent =
                                                         componentPlacement.FixtureComponent as IPlanogramFixtureComponent ?? componentPlacement.AssemblyComponent;
                                                     Int16 componentSequenceNumber = planogramFixtureComponent != null
                                                                                         ? planogramFixtureComponent.ComponentSequenceNumber ?? 0
                                                                                         : (Int16) 0;
                                                     Int16 positionSequenceNumber = position.PositionSequenceNumber ?? 0;
                                                     return String.Format("{0}.{1}.{2}", baySequenceNumber, componentSequenceNumber, positionSequenceNumber);
                                                 })
                                        .ToList().IndexOf(position) + 1;

            return String.Format("{0} ({1:D2})", product.Gtin, positionIndex);
        }

        #endregion

        #region Methods

        internal void CompareTarget(Planogram target)
        {
            TargetResult = Planogram.Comparison.Results.Add(target);
            TargetProductByKey = target.Products.Where(IsPlacedProduct).ToLookupDictionary(CreateComparisonKey);
            TargetPositionByKey = target.Positions.ToLookupDictionary(CreateComparisonKey);
            AddNewProductItems();
            AddNewPositionItems();
            AddNotFoundProductItems();
            AddNotFoundPositionItems();
            AddChangedProductItems();
            AddChangedPositionItems();
        }

        private Dictionary<String, Dictionary<String, Object>> LookupValueByFieldByProductKey(
            IDictionary<String, ObjectFieldInfo> infoByFieldPlaceHolder,
            IEnumerable<String> fieldPlaceholders)
        {
            //  Look up all the field infos for the comparable fields.
            List<ObjectFieldInfo> fieldInfos =
                fieldPlaceholders.Select(s =>
                                         {
                                             ObjectFieldInfo info;
                                             infoByFieldPlaceHolder.TryGetValue(s, out info);
                                             return info;
                                         })
                                 .Where(i => i != null)
                                 .ToList();

            var productInfoOwnerTypes = new List<Type> { typeof(Planogram), typeof(PlanogramInventory), typeof(PlanogramProduct) };
            List<ObjectFieldInfo> productObjectFieldInfos = fieldInfos.Where(i => productInfoOwnerTypes.Contains(i.OwnerType)).ToList();

            var sourceValuesByGtin = new Dictionary<String, Dictionary<String, Object>>();

            foreach (PlanogramProduct item in Planogram.Products)
            {
                //  Initialize a new dictionary to hold the source values per field for this product.
                var sourceValuesByFieldPlaceHolder = new Dictionary<String, Object>();
                sourceValuesByGtin[CreateComparisonKey(item)] = sourceValuesByFieldPlaceHolder;

                foreach (ObjectFieldInfo info in productObjectFieldInfos)
                {
                    //  Get the source value and add it to the collection of field values for the product.
                    Object value =
                        PlanogramFieldHelper.ResolveField(info, PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: Planogram, product: item)) ??
                        Message.PlanogramComparison_MissingFieldValue;
                    sourceValuesByFieldPlaceHolder[info.FieldPlaceholder] = value;
                }
            }

            return sourceValuesByGtin;
        }

        private Dictionary<String, Dictionary<String, Object>> LookupValueByFieldByPositionKey(
            IDictionary<String, ObjectFieldInfo> infoByFieldPlaceHolder,
            IEnumerable<String> fieldPlaceholders)
        {
            //  Look up all the field infos for the comparable fields.
            List<ObjectFieldInfo> fieldInfos =
                fieldPlaceholders.Select(s =>
                                         {
                                             ObjectFieldInfo info;
                                             infoByFieldPlaceHolder.TryGetValue(s, out info);
                                             return info;
                                         })
                                 .Where(i => i != null)
                                 .ToList();

            var sourceValuesByGtin = new Dictionary<String, Dictionary<String, Object>>();

            foreach (PlanogramPosition item in Planogram.Positions)
            {
                //  Initialize a new dictionary to hold the source values per field for this product.
                var sourceValuesByFieldPlaceHolder = new Dictionary<String, Object>();
                sourceValuesByGtin[CreateComparisonKey(item)] = sourceValuesByFieldPlaceHolder;

                foreach (ObjectFieldInfo info in fieldInfos)
                {
                    //  Get the source value and add it to the collection of field values for the product.
                    Object value =
                        PlanogramFieldHelper.ResolveField(info, PlanogramItemPlacement.NewPlanogramItemPlacement(item)) ??
                        Message.PlanogramComparison_MissingFieldValue;
                    sourceValuesByFieldPlaceHolder[info.FieldPlaceholder] = value;
                }
            }

            return sourceValuesByGtin;
        }

        private void AddNewProductItems()
        {
            foreach (KeyValuePair<String, PlanogramProduct> item in TargetProductByKey.Where(pair => !ProductByKey.ContainsKey(pair.Key)))
            {
                PlanogramComparisonItem newItem = TargetResult.ComparedItems.Add(item.Key, PlanogramItemType.Product, PlanogramComparisonItemStatusType.New);

                foreach (String fieldPlaceholder in ProductFieldPlaceholders)
                {
                    ObjectFieldInfo info;
                    if (!InfoByFieldPlaceHolder.TryGetValue(fieldPlaceholder, out info)) continue;

                    if (TargetResult.Status == PlanogramComparisonResultStatusType.Unchanged)
                        TargetResult.Status = PlanogramComparisonResultStatusType.RemovedItems;

                    Object value =
                        PlanogramFieldHelper.ResolveField(info, PlanogramItemPlacement.NewPlanogramItemPlacement(product: item.Value, planogram: Planogram)) ??
                        Message.PlanogramComparison_MissingFieldValue;
                    newItem.FieldValues.Add(PlanogramComparisonFieldValue.NewPlanogramComparisonFieldValue(fieldPlaceholder, value.ToString()));
                }
            }
        }

        private void AddNewPositionItems()
        {
            foreach (KeyValuePair<String, PlanogramPosition> item in TargetPositionByKey.Where(pair => !PositionByKey.ContainsKey(pair.Key)))
            {
                PlanogramComparisonItem newItem = TargetResult.ComparedItems.Add(item.Key, PlanogramItemType.Position, PlanogramComparisonItemStatusType.New);

                foreach (String fieldPlaceholder in PositionFieldPlaceholders)
                {
                    ObjectFieldInfo info;
                    if (!InfoByFieldPlaceHolder.TryGetValue(fieldPlaceholder, out info)) continue;

                    if (TargetResult.Status == PlanogramComparisonResultStatusType.Unchanged)
                        TargetResult.Status = PlanogramComparisonResultStatusType.RemovedItems;

                    Object value =
                        PlanogramFieldHelper.ResolveField(info, PlanogramItemPlacement.NewPlanogramItemPlacement(item.Value)) ??
                        Message.PlanogramComparison_MissingFieldValue;
                    newItem.FieldValues.Add(PlanogramComparisonFieldValue.NewPlanogramComparisonFieldValue(fieldPlaceholder, value.ToString()));
                }
            }
        }

        private void AddNotFoundProductItems()
        {
            foreach (KeyValuePair<String, PlanogramProduct> item in ProductByKey.Where(pair => !TargetProductByKey.ContainsKey(pair.Key)))
            {
                if (TargetResult.Status == PlanogramComparisonResultStatusType.Unchanged)
                    TargetResult.Status = PlanogramComparisonResultStatusType.AddedItems;
                if (TargetResult.Status == PlanogramComparisonResultStatusType.RemovedItems)
                    TargetResult.Status = PlanogramComparisonResultStatusType.Changed;
                TargetResult.ComparedItems.Add(item.Key, PlanogramItemType.Product, PlanogramComparisonItemStatusType.NotFound);
            }
        }

        private void AddNotFoundPositionItems()
        {
            foreach (KeyValuePair<String, PlanogramPosition> item in PositionByKey.Where(pair => !TargetPositionByKey.ContainsKey(pair.Key)))
            {
                if (TargetResult.Status == PlanogramComparisonResultStatusType.Unchanged)
                    TargetResult.Status = PlanogramComparisonResultStatusType.AddedItems;
                if (TargetResult.Status == PlanogramComparisonResultStatusType.RemovedItems)
                    TargetResult.Status = PlanogramComparisonResultStatusType.Changed;
                TargetResult.ComparedItems.Add(item.Key, PlanogramItemType.Position, PlanogramComparisonItemStatusType.NotFound);
            }
        }

        private void AddChangedProductItems()
        {
            PlanogramComparisonItemList comparedItems = TargetResult.ComparedItems;

            foreach (KeyValuePair<String, PlanogramProduct> item in TargetProductByKey.Where(pair => ProductByKey.ContainsKey(pair.Key)))
            {
                PlanogramComparisonItem changedItem = comparedItems.Add(item.Key, PlanogramItemType.Product, PlanogramComparisonItemStatusType.Unchanged);

                Dictionary<String, Object> valueByField;

                if (!ValuesByFieldByProductKey.TryGetValue(item.Key, out valueByField)) continue;

                foreach (String fieldPlaceholder in ProductFieldPlaceholders)
                {
                    //  Get the Object Field Info for the field to be compared.
                    ObjectFieldInfo info;
                    if (!InfoByFieldPlaceHolder.TryGetValue(fieldPlaceholder, out info)) continue;

                    //  Get the Source Value for the field to be compared.
                    Object value;
                    if (!valueByField.TryGetValue(fieldPlaceholder, out value)) continue;

                    //  Get the Target Value to be compared.
                    Object comparedValue =
                        PlanogramFieldHelper.ResolveField(info, PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: Planogram, product: item.Value)) ??
                        Message.PlanogramComparison_MissingFieldValue;

                    if (!Equals(comparedValue, value))
                    {
                        if (TargetResult.Status != PlanogramComparisonResultStatusType.Changed)
                            TargetResult.Status = PlanogramComparisonResultStatusType.Changed;

                        if (changedItem.Status != PlanogramComparisonItemStatusType.Changed)
                            changedItem.Status = PlanogramComparisonItemStatusType.Changed;
                    }

                    changedItem.FieldValues.Add(
                        PlanogramComparisonFieldValue.NewPlanogramComparisonFieldValue(fieldPlaceholder, comparedValue.ToString()));
                }
            }
        }

        private void AddChangedPositionItems()
        {
            PlanogramComparisonItemList comparedItems = TargetResult.ComparedItems;

            foreach (KeyValuePair<String, PlanogramPosition> item in TargetPositionByKey.Where(pair => PositionByKey.ContainsKey(pair.Key)))
            {
                PlanogramComparisonItem changedItem = comparedItems.Add(item.Key, PlanogramItemType.Position, PlanogramComparisonItemStatusType.Unchanged);

                Dictionary<String, Object> valueByField;

                if (!ValuesByFieldByPositionKey.TryGetValue(item.Key, out valueByField)) continue;

                foreach (String fieldPlaceholder in PositionFieldPlaceholders)
                {
                    //  Get the Object Field Info for the field to be compared.
                    ObjectFieldInfo info;
                    if (!InfoByFieldPlaceHolder.TryGetValue(fieldPlaceholder, out info)) continue;

                    //  Get the Source Value for the field to be compared.
                    Object value;
                    if (!valueByField.TryGetValue(fieldPlaceholder, out value)) continue;

                    //  Get the Target Value to be compared.
                    Object comparedValue =
                        PlanogramFieldHelper.ResolveField(info, PlanogramItemPlacement.NewPlanogramItemPlacement(item.Value)) ??
                        Message.PlanogramComparison_MissingFieldValue;

                    if (!Equals(comparedValue, value))
                    {
                        if (TargetResult.Status != PlanogramComparisonResultStatusType.Changed)
                            TargetResult.Status = PlanogramComparisonResultStatusType.Changed;

                        if (changedItem.Status != PlanogramComparisonItemStatusType.Changed)
                            changedItem.Status = PlanogramComparisonItemStatusType.Changed;
                    }

                    changedItem.FieldValues.Add(
                        PlanogramComparisonFieldValue.NewPlanogramComparisonFieldValue(fieldPlaceholder, comparedValue.ToString()));
                }
            }
        }

        /// <summary>
        ///     Check whether the product is placed or not in the planogram.
        /// </summary>
        /// <param name="product">The <see cref="PlanogramProduct"/> being checked for placements.</param>
        /// <returns><c>True</c> if the product is to be used in the comparison, <c>false</c> otherwise.</returns>
        private Boolean IsPlacedProduct(PlanogramProduct product)
        {
            if (!Planogram.Comparison.IgnoreNonPlacedProducts) return true;

            return product.GetPlanogramPositions().Any();
        }

        #endregion

        #region Static Helper Methods

        public static IEnumerable<IPlanogramComparisonSettingsField> EnumerateDefaultPositionComparisonFields()
        {
            PlanogramComparisonFieldList defaultComparisonFields = PlanogramComparisonFieldList.NewPlanogramComparisonFieldList();
            //  Set Default comparison fields for positions.
            List<ObjectFieldInfo> objectFieldInfos = PlanogramPosition.EnumerateDisplayableFieldInfos().ToList();
            defaultComparisonFields.AddRange(PlanogramItemType.Position, MatchByPropertyName(DefaultPositionComparisonFields, objectFieldInfos));

            return defaultComparisonFields;
        }

        public static IEnumerable<IPlanogramComparisonSettingsField> EnumerateDefaultProductComparisonFields()
        {
            PlanogramComparisonFieldList defaultComparisonFields = PlanogramComparisonFieldList.NewPlanogramComparisonFieldList();

            //  Set Default comparison fields for products.
            List<ObjectFieldInfo> objectFieldInfos = PlanogramProduct.EnumerateDisplayableFieldInfos(true, false, false).ToList();
            defaultComparisonFields.AddRange(PlanogramItemType.Product, MatchByPropertyName(DefaultProductComparisonFields, objectFieldInfos));

            return defaultComparisonFields;
        }

        /// <summary>
        ///     Enumerate the <see cref="IModelPropertyInfo" /> corresponding to a <see cref="ObjectFieldInfo" /> by Property Name.
        /// </summary>
        /// <param name="propertyInfos">Enumeration of <see cref="IModelPropertyInfo" /> to get matches from.</param>
        /// <param name="objectFieldInfos">Enumeration of <see cref="ObjectFieldInfo" /> to match by Property Name.</param>
        /// <returns>An enumeration of corresponding <see cref="IModelPropertyInfo" />.</returns>
        private static Dictionary<ObjectFieldInfo, Boolean> MatchByPropertyName(IEnumerable<IModelPropertyInfo> propertyInfos,
                                                                                IEnumerable<ObjectFieldInfo> objectFieldInfos)
        {
            return
                propertyInfos.Select(info => objectFieldInfos.FirstOrDefault(fieldInfo => fieldInfo.PropertyName == info.Name))
                             .Where(IsNotNull)
                             .ToDictionary(info => info, info => true);
        }

        /// <summary>
        ///     Check whether an object is null or not. Wrapper to use in LINQ expressions.
        /// </summary>
        /// <param name="o">The object being checked.</param>
        /// <returns><c>True</c> if the object is NOT null, <c>false</c> otherwise.</returns>
        private static Boolean IsNotNull(Object o)
        {
            return o != null;
        }

        private static void CheckComparisonFields(IPlanogramComparisonSettings settings)
        {
            //  Get the Comparison Fields by Plan Item Type.
            Dictionary<PlanogramItemType, List<IPlanogramComparisonSettingsField>> fieldsByType =
                settings.ComparisonFields.GroupBy(o => o.ItemType).ToLookupDictionary(g => g.Key, g => g.ToList());

            //  Check missing comparison fields for Products.
            List<IPlanogramComparisonSettingsField> comparisonFields;
            fieldsByType.TryGetValue(PlanogramItemType.Product, out comparisonFields);
            var missingFields = new List<IPlanogramComparisonSettingsField>();
            if (comparisonFields == null ||
                comparisonFields.Count == 0)
                missingFields.AddRange(EnumerateDefaultProductComparisonFields());

            //  Check missing comparison fields for Positions.
            fieldsByType.TryGetValue(PlanogramItemType.Position, out comparisonFields);
            if (comparisonFields == null ||
                comparisonFields.Count == 0)
                missingFields.AddRange(EnumerateDefaultPositionComparisonFields());

            //  Add any default comparison fields that were missing.
            if (missingFields.Any())
                settings.AddComparisonFields(missingFields);
        }

        #endregion
    }
}