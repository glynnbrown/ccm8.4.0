﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810

// V8-28661 : L.Ineson
// Created

#endregion

#region Version History: CCM811

// V8-30513 : L.Luong
//  Should now show correct colours
// V8-30565 : A.Silva
//  Amended PerformQuadrantAnalysis so that the highlight groups are correctly identified by their expressions when assigning colors.

#endregion

#region Version History: CCM820
// V8-30942 : D.Pleasance
//  Amended PerformGroupAnalysis to validate that used color is unique.
// V8-30972 : D.Pleasance
//  Amended so that standard colors are first colors to be used when creating highlight groups, this effects PerformGroupAnalysis, PerformCharacteristicAnalysis.
// V8-31361 :J.Pickup
//  PassesFilter now uses Invariant culture to allow highlights to work for Lithuanian. 
// V8-31478 : D.Pleasance
//  Amended so that PerformCharacteristicAnalysis uses same colour if loaded again.
#endregion

#region Version History: CCM830

// V8-31976 : A.Silva
//  Amended PassesFilter so that expressions are evaluated using ResolveFieldExpression straight away.
// V8-31966 : A.Silva
//  Amended the constructor to make sure that enum values are labeled with their Friendly Names if they have one.
//V8-32184 : L.Ineson
//  Corrected generation of colours for colourscale method type.
#endregion

#endregion

using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Static class providing helper methods for processing planogram highlights.
    /// </summary>
    public static class PlanogramHighlightHelper
    {
        /// <summary>
        /// Processes the given highlight against the given position items.
        /// </summary>
        /// <param name="plan">The planogram to be processed</param>
        /// <param name="highlight">The highlight to use</param>
        /// <returns>A list of items to be displayed by the legend.</returns>
        public static PlanogramHighlightResult ProcessPositionHighlight(Planogram plan, IPlanogramHighlight highlight)
        {
            List<PlanogramHighlightResultGroup> resultGroups = new List<PlanogramHighlightResultGroup>();

            if (highlight != null && plan != null)
            {
                //get the list of items to be highlighted from the plan.
                // At present this is only the positions but we could add component highlighting in future.
                List<PlanogramPosition> processItems = plan.Positions.ToList();

                //Process and get the highlight groups
                List<PlanogramHighlightResultGroup> groups = DetermineHighlightGroups(processItems, highlight);


                //process the groups against the ones in the setting and create legend items
                // NB - Setting groups are used to indicate where the user has manually changed a group label or colour and saved.
                // They did however end up causing a fair few problems so this should probably be reassessed at some point.
                ILookup<String, IPlanogramHighlightGroup> settingGroupDict = highlight.Groups.ToLookup(g => g.Name, StringComparer.OrdinalIgnoreCase);

                foreach (PlanogramHighlightResultGroup group in groups)
                {
                    String strValue = Convert.ToString(group.Value, CultureInfo.InvariantCulture);

                    IPlanogramHighlightGroup groupView = settingGroupDict[strValue].LastOrDefault();
                    if (groupView != null)
                    {
                        //update the label and colour.
                        group.Label = (!String.IsNullOrEmpty(groupView.DisplayName.Trim())) ? groupView.DisplayName : groupView.Name;
                       if(highlight.MethodType!= PlanogramHighlightMethodType.ColourScale) group.Colour = groupView.FillColour;
                    }

                    resultGroups.Add(group);
                }
            }

            //return the highlight process result.
            return PlanogramHighlightResult.NewPlanogramHighlightResult(highlight, resultGroups);
        }


        /// <summary>
        /// Creates highlight groups for the given items
        /// </summary>
        /// <param name="positions"></param> 
        /// <param name="highlight"></param>
        /// <returns></returns>
        public static List<PlanogramHighlightResultGroup> DetermineHighlightGroups(IEnumerable<PlanogramPosition> positions, IPlanogramHighlight highlight)
        {
            List<PlanogramHighlightResultGroup> groups = new List<PlanogramHighlightResultGroup>();

            if (highlight != null)
            {
                List<PlanogramPosition> filteredItems = positions.ToList();

                //Process the prefilter if req
                if (highlight.IsFilterEnabled && highlight.IsPreFilter)
                {
                    filteredItems = Filter(positions, highlight.IsAndFilter, highlight.Filters);
                }

                try
                {

                    //Process the method.
                    switch (highlight.MethodType)
                    {
                        case PlanogramHighlightMethodType.Group:
                            groups = PerformGroupAnalysis(filteredItems, highlight);
                            break;

                        case PlanogramHighlightMethodType.Quadrant:
                            groups =
                                PerformQuadrantAnalysis(filteredItems, highlight);
                            break;

                        case PlanogramHighlightMethodType.ColourScale:
                            groups = PerformSpectrumAnalysis(filteredItems, highlight);
                            break;

                        case PlanogramHighlightMethodType.Characteristic:
                            groups = PerformCharacteristicAnalysis(filteredItems, highlight);
                            break;

                        default: Debug.Fail("PlanogramHighlightMethodType not handled"); break;

                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.GetBaseException().Message);
                }

                //Process the post filter if req
                if (highlight.IsFilterEnabled && !highlight.IsPreFilter)
                {
                    filteredItems = Filter(filteredItems, highlight.IsAndFilter, highlight.Filters);

                    //remove from groups
                    List<PlanogramPosition> removeItems = positions.Except(filteredItems).ToList();
                    foreach (PlanogramHighlightResultGroup group in groups)
                    {
                        removeItems.ForEach(i => group.Items.Remove(i));
                    }
                }

            }

            //order groups by value before they are returned
            groups = groups.OrderBy(g => g.Value).ToList();


            return groups;
        }


        #region Analysis

        /// <summary>
        /// Returns a list of the given positions that pass the filters of the highlight setting.
        /// </summary>
        /// <param name="processItems">The positions to check</param>
        /// <param name="isAndFilter">Flag to indicate if all criteria should pass</param>
        /// <param name="filters">The list of filters to be assessed.</param>
        /// <returns>A list of positions that satisfy the filters</returns>
        private static List<PlanogramPosition> Filter(IEnumerable<PlanogramPosition> processItems,
            Boolean isAndFilter, IEnumerable<IPlanogramHighlightFilter> filters)
        {
            List<PlanogramPosition> filteredItems = new List<PlanogramPosition>();

            if (isAndFilter)
            {
                //all filters must pass.
                foreach (PlanogramPosition item in processItems)
                {
                    if (filters.All(filter => PassesFilter(filter.Field, filter.Type, filter.Value, item)))
                    {
                        filteredItems.Add(item);
                    }
                }
            }
            else
            {
                //any filter may pass.
                foreach (PlanogramPosition item in processItems)
                {
                    if (filters.Any(filter => PassesFilter(filter.Field, filter.Type, filter.Value, item)))
                    {
                        filteredItems.Add(item);
                    }
                }
            }

            return filteredItems;
        }

        /// <summary>
        /// Returns true if the given item passes the filter
        /// </summary>
        /// <param name="filterField">The field text to assess</param>
        /// <param name="operationType">The way in which it should be checked</param>
        /// <param name="filterValue">The criteria value to satisfy</param>
        /// <param name="item">The item to be assessed</param>
        /// <returns>True if the item satisfies the filter criteria</returns>
        private static Boolean PassesFilter(String filterField, PlanogramHighlightFilterType operationType, String filterValue, PlanogramPosition item)
        {
            //  If the field or the value being compared are null or empty, return true (partially set filters will always return true).
            if (String.IsNullOrEmpty(filterField) ||
                String.IsNullOrEmpty(filterValue)) return true;

            //  NB: Resolve the expression after making it into a formula so it can be evaluated.
            String filterOperator = PlanogramHighlightFilterTypeHelper.FriendlyNames[operationType];
            String expressionText = String.Format(CultureInfo.InvariantCulture, "{{ {0} {1} {2} }}", filterField, filterOperator, filterValue);
            Object expressionResult = PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                                                                             PlanogramFieldHelper.EnumerateAllFields(),
                                                                             PlanogramItemPlacement.NewPlanogramItemPlacement(item));

            //  Return the result for the evaluated expression if there is one,
            //  will return false if unable to evaluate (expressionResult is not Boolean).
            return expressionResult is Boolean && (Boolean) expressionResult;
        }

        /// <summary>
        /// Creates the highlight groups for the given items using a group analysis.
        /// </summary>
        private static List<PlanogramHighlightResultGroup> PerformGroupAnalysis(IEnumerable<PlanogramPosition> items, IPlanogramHighlight highlight)
        {
            List<PlanogramHighlightResultGroup> groups = new List<PlanogramHighlightResultGroup>();

            if (items.Any() && !String.IsNullOrEmpty(highlight.Field1))
            {
                //check if this is the blocking field.
                Boolean isColour =
                    highlight.Field1.TrimEnd(ObjectFieldInfo.FieldEnd).EndsWith(PlanogramProduct.FillColourProperty.Name) ||
                    highlight.Field1.TrimEnd(ObjectFieldInfo.FieldEnd).EndsWith(PlanogramPosition.SequenceColourProperty.Name);

                Random rand = new Random(123123123);

                Dictionary<String, PlanogramHighlightResultGroup> groupDict = new Dictionary<String, PlanogramHighlightResultGroup>();

                foreach (PlanogramPosition planItem in items)
                {
                    Object val = PlanogramFieldHelper.ResolveFieldExpression(highlight.Field1, PlanogramFieldHelper.EnumerateAllFields(), PlanogramItemPlacement.NewPlanogramItemPlacement(planItem));
                    String groupName = Convert.ToString(val);

                    PlanogramHighlightResultGroup group;
                    if (groupDict.TryGetValue(groupName, out group))
                    {
                        group.Items.Add(planItem);
                    }
                    else
                    {
                        Int32 colour = rand.Next();
                        if (isColour && val != null)
                        {
                            colour = (Int32)val;
                        }


                        group = new PlanogramHighlightResultGroup(val);
                        group.Items.Add(planItem);
                        group.Colour = colour;

                        groupDict.Add(groupName, group);
                    }
                }


                groups.AddRange(groupDict.Values);

                //ensure all values are of the same type.
                Type castType = typeof(String);
                if (groups.Any(g => g.Value != null))
                {
                    castType = groups.Where(g => g != null).First().Value.GetType();
                }
                foreach (PlanogramHighlightResultGroup g in groups)
                {
                    if (g.Value != null)
                    {
                        g.Value = Convert.ChangeType(g.Value, castType);
                    }
                }

                //set colours
                if (groups.Count > 0)
                {
                    groups = groups.OrderBy(g => g.Value).ToList();

                    if (!isColour)
                    {
                        Int32[] groupColours = GenerateGroupColours(groups.Count);
                        List<Int32> usedColors = new List<Int32>();
                        Int32 groupColorIndex = 2; // add 2 to skip black and white.

                        for (Int32 i = 0; i < groups.Count; i++)
                        {
                            if (highlight.Groups.Count() > i)
                            {
                                var group = highlight.Groups.FirstOrDefault(g => g.Name.Equals(groups[i].Label));
                                if (group != null)
                                {
                                    groups[i].Colour = ValidateUsedColor(usedColors, group.FillColour, ref groupColorIndex);
                                }
                                else 
                                {
                                    groups[i].Colour = ValidateUsedColor(usedColors, groupColours[i], ref groupColorIndex);
                                }
                            }
                            else
                            {
                                groups[i].Colour = ValidateUsedColor(usedColors, groupColours[i], ref groupColorIndex);
                            }
                        }
                    }
                }
            }

            return groups;
        }
        
        /// <summary>
        /// Creates the highlight groups for the given items using a quadrant analysis.
        /// </summary>
        private static List<PlanogramHighlightResultGroup> PerformQuadrantAnalysis(IEnumerable<PlanogramPosition> items,
                                                                                   IPlanogramHighlight highlight)
        {
            var groups = new List<PlanogramHighlightResultGroup>();
            //create the 4 quadrant groups.
            //horiz field < horiz method, vert field >= vert method
            var lowHighGroup = new PlanogramHighlightResultGroup("X < ? & Y >= ?") {Colour = /*Colors.SeaGreen*/ -12156236};
            groups.Add(lowHighGroup);

            //horiz f >= horiz m, vert f >= vert m
            var highHighGroup = new PlanogramHighlightResultGroup("X >= ? & Y >= ?") {Colour = /*Colors.Gold*/ -13726889};
            groups.Add(highHighGroup);

            //horiz f < horiz m, vert f < vert m
            var lowLowGroup = new PlanogramHighlightResultGroup("X < ? & Y < ?") {Colour = /*Colors.SteelBlue*/ -5103070};
            groups.Add(lowLowGroup);

            //horiz f < horiz m, vert f < vert m
            var highLowGroup = new PlanogramHighlightResultGroup("X >= ? & Y < ?") {Colour = /*Colors.Firebrick*/ -10496};
            groups.Add(highLowGroup);

            const String messagePlanogramighLightFailedGroup = "Failed"; // Message.PlanogramHighlight_FailedGroup
            var failedGroup =
                new PlanogramHighlightResultGroup(messagePlanogramighLightFailedGroup) {Colour = /*Colors.LightGray*/ ColorFromRgb(155, 155, 155)};
            groups.Add(failedGroup);

            //  Fix the enumeration, so it is not enumerated each time it is used.
            IEnumerable<PlanogramPosition> positions = items as IList<PlanogramPosition> ?? items.ToList();
            if (positions.Any() &&
                !String.IsNullOrEmpty(highlight.Field1) &&
                !String.IsNullOrEmpty(highlight.Field2))
            {
                //Resolve the horizontal and vertical field values for all plan Items
                var resolvedItems = new List<Tuple<PlanogramPosition, Decimal, Decimal>>();
                foreach (PlanogramPosition planItem in positions)
                {
                    Object horizVal = PlanogramFieldHelper.ResolveFieldExpression(highlight.Field1, PlanogramFieldHelper.EnumerateAllFields(), PlanogramItemPlacement.NewPlanogramItemPlacement(planItem));
                    Object vertVal = PlanogramFieldHelper.ResolveFieldExpression(highlight.Field2, PlanogramFieldHelper.EnumerateAllFields(), PlanogramItemPlacement.NewPlanogramItemPlacement(planItem));

                    if (horizVal is String ||
                        vertVal is String)
                    {
                        //fail all as we cannot process the fields.
                        failedGroup.Items.AddRange(positions);
                        return groups;
                    }

                    Decimal dcHoriz;
                    Decimal dcVert;
                    try
                    {
                        dcHoriz = Convert.ToDecimal(horizVal);
                        dcVert = Convert.ToDecimal(vertVal);
                    }
                    catch (Exception)
                    {
                        //fail all as we cannot process the fields.
                        failedGroup.Items.AddRange(positions);
                        return groups;
                    }

                    resolvedItems.Add(new Tuple<PlanogramPosition, Decimal, Decimal>(planItem, dcHoriz, dcVert));
                }

                //Determine the method values.
                Decimal horizMethodVal = 0;
                Decimal vertMethodVal = 0;

                #region X Axis Method

                List<Decimal> orderedXValues = resolvedItems.Select(r => r.Item2).OrderBy(x => x).ToList();

                switch (highlight.QuadrantXType)
                {
                    case PlanogramHighlightQuadrantType.Mean:
                        horizMethodVal = orderedXValues.Average();
                        break;

                    case PlanogramHighlightQuadrantType.Median:
                        horizMethodVal = orderedXValues.ElementAt((orderedXValues.Count/2));
                        break;

                    case PlanogramHighlightQuadrantType.MidRange:
                    {
                        Decimal min = orderedXValues.Min();
                        Decimal max = orderedXValues.Max();
                        horizMethodVal = min + ((max - min)/(Decimal) 2.0);
                    }
                        break;

                    case PlanogramHighlightQuadrantType.Constant:
                        horizMethodVal = Convert.ToDecimal(highlight.QuadrantXConstant);
                        break;

                    default:
                        Debug.Fail("PlanogramHighlightQuadrantType not handled");
                        break;
                }

                //round it.
                horizMethodVal = Math.Round(horizMethodVal, 2);

                #endregion

                #region Y Axis Method

                List<Decimal> orderedYValues = resolvedItems.Select(r => r.Item3).OrderBy(y => y).ToList();

                switch (highlight.QuadrantYType)
                {
                    case PlanogramHighlightQuadrantType.Mean:
                        vertMethodVal = orderedYValues.Average();
                        break;

                    case PlanogramHighlightQuadrantType.Median:
                        vertMethodVal = orderedYValues.ElementAt((orderedYValues.Count/2));
                        break;

                    case PlanogramHighlightQuadrantType.MidRange:
                    {
                        Decimal min = orderedYValues.Min();
                        Decimal max = orderedYValues.Max();
                        vertMethodVal = min + ((max - min)/(Decimal) 2.0);
                    }
                        break;

                    case PlanogramHighlightQuadrantType.Constant:
                        vertMethodVal = Convert.ToDecimal(highlight.QuadrantYConstant);
                        break;

                    default:
                        Debug.Fail("PlanogramHighlightQuadrantType not handled");
                        break;
                }

                //round it.
                vertMethodVal = Math.Round(vertMethodVal, 2);

                #endregion

                //Update group titles
                lowHighGroup.Label = String.Format("X < {0} & Y >= {1}", horizMethodVal, vertMethodVal);
                highHighGroup.Label = String.Format("X >= {0} & Y >= {1}", horizMethodVal, vertMethodVal);
                lowLowGroup.Label = String.Format("X < {0} & Y < {1}", horizMethodVal, vertMethodVal);
                highLowGroup.Label = String.Format("X >= {0} & Y < {1}", horizMethodVal, vertMethodVal);

                PlanogramHighlightResultGroup[] processGroups = {lowHighGroup, highHighGroup, lowLowGroup, highLowGroup};

                String[] hExpressions =
                {
                    "({2} < {0}) AND ({3} >= {1})",
                    "({2} >= {0}) AND ({3} >= {1})",
                    "({2} < {0}) AND ({3} < {1})",
                    "({2} >= {0}) AND ({3} < {1})"
                };

                //cycle positions assigning to groups
                var computeTable = new DataTable();
                foreach (Tuple<PlanogramPosition, Decimal, Decimal> posEntry in resolvedItems)
                {
                    PlanogramPosition planItem = posEntry.Item1;
                    Object posHorizVal = posEntry.Item2;
                    Object posVertVal = posEntry.Item3;

                    PlanogramHighlightResultGroup finalGroup = failedGroup;

                    try
                    {
                        //cycle through the expressions
                        for (var i = 0; i < processGroups.Count(); i++)
                        {
                            PlanogramHighlightResultGroup group = processGroups[i];

                            String expr =
                                String.Format(hExpressions[i], horizMethodVal, vertMethodVal, posHorizVal, posVertVal);

                            var result = computeTable.Compute(expr, null) as Boolean?;

                            if (result == true)
                            {
                                finalGroup = group;
                                break;
                            }
                            if (result != null) continue;

                            Debug.WriteLine("Failed to evalate position");
                            break;
                        }

                    }
                    catch (SyntaxErrorException) {}


                    //add the position to the group.
                    finalGroup.Items.Add(planItem);
                }
            }

            //  Assign colors to each Highligh group based on the expression used to name it.
            foreach (PlanogramHighlightResultGroup resultGroup in groups)
            {
                String pattern = resultGroup.Value.ToString().Replace("?", ".+");
                IPlanogramHighlightGroup highlightGroup = highlight.Groups.FirstOrDefault(g => Regex.IsMatch(g.Name, pattern));
                resultGroup.Colour = highlightGroup != null ? highlightGroup.FillColour : resultGroup.Colour;
            }

            //if the failed group contains no positions then remove it.
            if (failedGroup.Items.Count == 0)
            {
                groups.Remove(failedGroup);
            }

            return groups;
        }

        /// <summary>
        /// Creates the highlight groups for the given items using spectrum analysis.
        /// </summary>
        private static List<PlanogramHighlightResultGroup> PerformSpectrumAnalysis(IEnumerable<PlanogramPosition> items, IPlanogramHighlight highlight)
        {
            List<PlanogramHighlightResultGroup> groups = new List<PlanogramHighlightResultGroup>();

            //create groups based on the resolved field values.
            if (items.Any() && !String.IsNullOrEmpty(highlight.Field1))
            {
                Dictionary<String, PlanogramHighlightResultGroup> groupDict = new Dictionary<String, PlanogramHighlightResultGroup>();
                Dictionary<Double, PlanogramHighlightResultGroup> groupValuesDict = new Dictionary<Double, PlanogramHighlightResultGroup>();

                foreach (PlanogramPosition planItem in items)
                {
                    Object val = PlanogramFieldHelper.ResolveFieldExpression(highlight.Field1, PlanogramFieldHelper.EnumerateAllFields(), PlanogramItemPlacement.NewPlanogramItemPlacement(planItem));
                    String groupName = Convert.ToString(val);

                    //get the group and add item
                    PlanogramHighlightResultGroup group;
                    if (groupDict.TryGetValue(groupName, out group))
                    {
                        group.Items.Add(planItem);
                    }
                    else
                    {
                        group = new PlanogramHighlightResultGroup(val);
                        group.Colour = -1; //initialize group to white.
                        group.Items.Add(planItem);
                        groupDict.Add(groupName, group);


                        //convert group value to single for colouring processing.
                        Double number;
                        if (val != null &&
                            Double.TryParse(Convert.ToString(val, CultureInfo.InvariantCulture),
                            System.Globalization.NumberStyles.Any, NumberFormatInfo.InvariantInfo, out number))
                        {
                            groupValuesDict.Add(Math.Round(number, MathHelper.DefaultPrecision), group);
                        }
                    }
                }

                groups.AddRange(groupDict.Values);

                if (groupValuesDict.Values.Count > 0)
                {
                    //order the values and assign colours.
                    //nb - these should be recreated every time!
                    Int32 min = (Int32)(groupValuesDict.Keys.Min() * 100);
                    Int32 max = (Int32)(groupValuesDict.Keys.Max() * 100);
                    
                    foreach (var entry in groupValuesDict)
                    {
                        entry.Value.Colour = GetSpectrumColour(min, max, (Int32)(entry.Key * 100));
                    }
                }
            }

            return groups;
        }

        /// <summary>
        /// Creates the highlight groups for the given items using characteristic.
        /// </summary>
        private static List<PlanogramHighlightResultGroup> PerformCharacteristicAnalysis(
            IEnumerable<PlanogramPosition> items, IPlanogramHighlight highlight)
        {
            List<PlanogramPosition> remainingItems = items.ToList();

            List<PlanogramHighlightResultGroup> groups = new List<PlanogramHighlightResultGroup>();

            //process
            foreach (IPlanogramHighlightCharacteristic aspect in highlight.Characteristics)
            {
                List<PlanogramPosition> matchItems =
                    CharacteristicAnalysisFilter(remainingItems, aspect.IsAndFilter, aspect.Rules);

                PlanogramHighlightResultGroup group = new PlanogramHighlightResultGroup(aspect.Name);
                group.Items.AddRange(matchItems);
                groups.Add(group);

                matchItems.ForEach(m => remainingItems.Remove(m));
            }

            //set colours
            if (groups.Count > 0)
            {
                Int32[] groupColours = GenerateGroupColours(groups.Count);
                List<Int32> usedColors = new List<Int32>();
                Int32 groupColorIndex = 2; // add 2 to skip black and white.

                for (Int32 i = 0; i < groups.Count; i++)
                {
                    if (highlight.Groups.Count() > i)
                    {
                        var group = highlight.Groups.FirstOrDefault(g => g.Name.Equals(groups[i].Label));
                        if (group != null)
                        {
                            groups[i].Colour = ValidateUsedColor(usedColors, group.FillColour, ref groupColorIndex);
                        }
                        else
                        {
                            groups[i].Colour = ValidateUsedColor(usedColors, groupColours[i], ref groupColorIndex);
                        }
                    }
                    else
                    {
                        groups[i].Colour = ValidateUsedColor(usedColors, groupColours[i], ref groupColorIndex);
                    }
                }
            }

            return groups;
        }

        /// <summary>
        /// Filters the given items to those that fulfil the given aspect rules.
        /// </summary>
        /// <param name="processItems">the items to be processed</param>
        /// <param name="isAndFilter">if true this is an and filter</param>
        /// <param name="rules">the rules to check</param>
        /// <returns>A list of items that pass the rules.</returns>
        private static List<PlanogramPosition> CharacteristicAnalysisFilter(IEnumerable<PlanogramPosition> processItems,
            Boolean isAndFilter, IEnumerable<IPlanogramHighlightCharacteristicRule> rules)
        {
            List<PlanogramPosition> filteredItems = new List<PlanogramPosition>();

            if (rules.Any())
            {
                if (isAndFilter)
                {
                    //all filters must pass.
                    foreach (PlanogramPosition item in processItems)
                    {
                        if (rules.All(filter => PassesFilter(filter.Field, (PlanogramHighlightFilterType)filter.Type, filter.Value, item)))
                        {
                            filteredItems.Add(item);
                        }
                    }
                }
                else
                {
                    //any filter may pass.
                    foreach (PlanogramPosition item in processItems)
                    {
                        if (rules.Any(filter => PassesFilter(filter.Field, (PlanogramHighlightFilterType)filter.Type, filter.Value, item)))
                        {
                            filteredItems.Add(item);
                        }
                    }
                }
            }


            return filteredItems;

        }

        #endregion

        #region Colour Generation

        /// <summary>
        /// Returns a colour for the given group number
        /// </summary>
        /// <param name="groupNumber"></param>
        /// <returns></returns>
        private static Int32[] GenerateGroupColours(Int32 groupCount)
        {
            Int32[] groupColours = new Int32[groupCount];
            Int32 idx = 0;

            // create standard colors first
            foreach (Tuple<Byte, Byte, Byte> standardColour in BlockingHelper.StandardColors())
            {
                Byte red = standardColour.Item1;
                Byte green = standardColour.Item2;
                Byte blue = standardColour.Item3;

                var colour = (((UInt32)255) << 24) |
                        (((UInt32)red) << 16) |
                        (((UInt32)green) << 8) |
                        (UInt32)blue;

                groupColours[idx] = (Int32)colour;
                idx++;
                if (idx == groupCount) break;
            }

            Int32 colorIdx = 2; // add 2 to skip black and white.

            while (idx < groupCount)
            {
                Int32[] p = GetColourPattern(colorIdx);
                Int32 colour = GetColourElement(p[0]) << 16 | GetColourElement(p[1]) << 8 | GetColourElement(p[2]);
                colour = ForceColourAlpha(colour);
                
                if (!groupColours.Contains(colour))
                {
                    groupColours[idx] = colour;
                    idx++;
                }
                else
                {
                    colorIdx++;
                }                
            }

            return groupColours;
        }
        private static Int32 GetColourElement(Int32 index)
        {
            Int32 value = index - 1;
            Int32 v = 0;
            for (Int32 i = 0; i < 8; i++)
            {
                v = v | (value & 1);
                v <<= 1;
                value >>= 1;
            }
            v >>= 1;
            return v & 0xFF;
        }
        private static Int32[] GetColourPattern(Int32 index)
        {
            Int32 n = (Int32)Math.Pow(index, 1 / 3.0);
            index -= (n * n * n);

            Int32[] p = new Int32[3];
            p[0] = n;
            p[1] = n;
            p[2] = n;

            if (index == 0)
            {
                return p;
            }
            index--;

            Int32 v = index % 3;
            index = index / 3;

            if (index < n)
            {
                p[v] = index % n;
                return p;
            }

            index -= n;
            p[v] = index / n;
            p[++v % 3] = index % n;

            return p;
        }

        /// <summary>
        /// Returns a color for the given point in spectrum analysis
        /// </summary>
        /// <param name="rangeStart"></param>
        /// <param name="rangeEnd"></param>
        /// <param name="actualValue"></param>
        /// <returns></returns>
        private static Int32 GetSpectrumColour(Int32 rangeStart, Int32 rangeEnd, Int32 actualValue)
        {
            if (rangeStart >= rangeEnd) return /*white*/-1;

            Double max = rangeEnd - rangeStart; // make the scale start from 0
            Double value = actualValue - rangeStart; // adjust the value accordingly

            Double hue = (95 * value) / max;

            return GetColorFromHsl(hue, 200, 100);
        }

        private static Int32 GetColorFromHsl(Double h, Double s, Double l, Byte alpha = 255)
        {
            double hue = h / 240.0;
            double sat = s / 240.0;
            double lum = l / 240.0;
            double r = 0;
            double g = 0;
            double b = 0;
            double temp1, temp2;

            if (lum != 0)
            {
                if (sat == 0)
                {
                    r = lum;
                    g = lum;
                    b = lum;
                }
                else
                {

                    temp2 = ((lum <= 0.5) ? lum * (1.0 + sat) : lum + sat - (lum * sat));
                    temp1 = 2.0 * lum - temp2;

                    double[] t3 = new double[] { hue + 1.0 / 3.0, hue, hue - 1.0 / 3.0 };
                    double[] clr = new double[] { 0, 0, 0 };

                    for (int i = 0; i < 3; i++)
                    {
                        if (t3[i] < 0)
                        {
                            t3[i] += 1.0;
                        }

                        if (t3[i] > 1)
                        {
                            t3[i] -= 1.0;
                        }


                        if (6.0 * t3[i] < 1.0)
                        {
                            clr[i] = temp1 + (temp2 - temp1) * t3[i] * 6.0;
                        }
                        else if (2.0 * t3[i] < 1.0)
                        {
                            clr[i] = temp2;
                        }
                        else if (3.0 * t3[i] < 2.0)
                        {
                            clr[i] = (temp1 + (temp2 - temp1) * ((2.0 / 3.0) - t3[i]) * 6.0);
                        }
                        else
                        {
                            clr[i] = temp1;
                        }

                    }

                    r = clr[0];
                    g = clr[1];
                    b = clr[2];

                }

            }


            var uint32Color = (((UInt32)alpha) << 24) |
                    (((UInt32)Convert.ToByte(255 * r)) << 16) |
                    (((UInt32)Convert.ToByte(255 * g)) << 8) |
                    (UInt32)Convert.ToByte(255 * b);
          
            return (Int32)uint32Color;

        }

        private static Int32 ColorFromRgb(Byte red, Byte green, Byte blue, Byte alpha = 255)
        {
            var uint32Color = (((UInt32)alpha) << 24) |
                    (((UInt32)red) << 16) |
                    (((UInt32)green) << 8) |
                    (UInt32)blue;

            return (Int32)uint32Color;
        }

        private static Int32 ForceColourAlpha(Int32 color)
        {
            Int32 colorAsInt = color;
            Byte R = ClampColour((Byte)((colorAsInt >> 16) & 0xff));
            Byte G = ClampColour((Byte)((colorAsInt >> 8) & 0xff));
            Byte B = ClampColour((Byte)((colorAsInt) & 0xff));
            Byte A = 255;//ClampColour((Byte)((colorAsInt >> 24) & 0xff));
            return ColorFromRgb(R, G, B, A);
        }

        /// <summary>
        /// Clamps the supplied value bettween 0 and 255
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private static Byte ClampColour(Single value)
        {
            Single output = Math.Max(value, 0);
            output = Math.Min(value, 255);
            return (Byte)(output);
        }

        /// <summary>
        /// Validates that the color is unique, if it isnt then a new color is generated.
        /// </summary>
        /// <param name="usedColors">The list of used colors</param>
        /// <param name="color">The current color</param>
        /// <param name="groupColorIndex">The current group color index</param>
        /// <returns>A unique color</returns>
        private static Int32 ValidateUsedColor(List<Int32> usedColors, Int32 color, ref Int32 groupColorIndex)
        {
            while (usedColors.Contains(color))
            {
                Int32[] p = GetColourPattern(groupColorIndex);
                color = GetColourElement(p[0]) << 16 | GetColourElement(p[1]) << 8 | GetColourElement(p[2]);
                color = ForceColourAlpha(color);
                groupColorIndex++;
            }

            usedColors.Add(color);
            return color;
        }
        #endregion
    }

    /// <summary>
    /// Represents the result of a highlight process.
    /// </summary>
    public class PlanogramHighlightResult
    {
        #region Fields
        private IPlanogramHighlight _highlight;
        private List<PlanogramHighlightResultGroup> _groups;
        #endregion

        #region Properties

        public IPlanogramHighlight Highlight
        {
            get { return _highlight; }
        }

        /// <summary>
        /// Returns the list of result items for the processed highlight.
        /// </summary>
        public IEnumerable<PlanogramHighlightResultGroup> Groups
        {
            get { return _groups; }
        }

        #endregion

        #region Consrtuctor
        private PlanogramHighlightResult() { }
        #endregion

        #region Factory Methods

        public static PlanogramHighlightResult NewPlanogramHighlightResult(IPlanogramHighlight highlight, IEnumerable<PlanogramHighlightResultGroup> groups)
        {
            PlanogramHighlightResult item = new PlanogramHighlightResult();
            item.Create(highlight, groups);
            return item;
        }

        #endregion

        #region Data Access

        private void Create(IPlanogramHighlight highlight, IEnumerable<PlanogramHighlightResultGroup> groups)
        {
            _highlight = highlight;
            _groups = groups.ToList();
        }

        #endregion
    }

    /// <summary>
    /// Represents a single highlight group 
    /// </summary>
    public class PlanogramHighlightResultGroup
    {
        #region Fields
        private String _label;
        private Object _value;
        private Int32 _colour;
        private readonly List<Object> _items = new List<Object>();
        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets the group label
        /// </summary>
        public String Label
        {
            get { return _label; }
            set { _label = value; }
        }

        /// <summary>
        /// Gets/Sets the value of this group.
        /// </summary>
        public Object Value
        {
            get { return _value; }
            set { _value = value; }
        }

        /// <summary>
        /// Gets/Sets the group colour.
        /// </summary>
        public Int32 Colour
        {
            get { return _colour; }
            set
            {
                _colour = value;
                ////correct alpha
                //Color color = IntToColorConverter.IntToColor(value);
                //color.A = 255;//correct the alpha
                //_colour = IntToColorConverter.ColorToInt(color);
            }
        }

        /// <summary>
        /// Returns the collection of items belonging to this group
        /// </summary>
        public List<Object> Items
        {
            get { return _items; }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="value"></param>
        public PlanogramHighlightResultGroup(Object value)
        {
            this.Value = value;
            this.Label = GetLabelFromValue(value);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Get the correct label from the given <paramref name="value"/>/
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static String GetLabelFromValue(Object value)
        {
            const String defaultLabel = "<null>";

            //  Without a value to get the label from, use the default label.
            if (value == null) value = defaultLabel;

            //  Take enums into account. The label in this case should be the Friendly Name if possible.
            if (value.GetType().IsEnum)
            {
                value = GetEnumHelperDictionaryString(value, defaultLabel);
            }

            //  Get the String in value.
            String stringValue = Convert.ToString(value, CultureInfo.CurrentCulture);

            //  An empty label should show the default label, otherwise return the label.
            return String.IsNullOrWhiteSpace(stringValue) ? defaultLabel : stringValue;
        }

        /// <summary>
        ///     Get the String matching the given <paramref name="value"/> if it is an enum, and there is a Helper for it. Otherwise, return the <paramref name="defaultValue"/>.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <param name="dictionaryPropertyName"></param>
        /// <returns></returns>
        private static String GetEnumHelperDictionaryString(Object value, String defaultValue = null, String dictionaryPropertyName = "FriendlyNames")
        {
            //  If it is not an enum value, return the default value.
            Type enumType = value.GetType();
            if (!enumType.IsEnum) return defaultValue;

            //  From this point onwards, if there is no value, use the member name as default.
            defaultValue = Convert.ToString(value);

            //  Get the enum helper type, if there is none, return the member name.
            Type enumHelperType = GetEnumHelperType(enumType);
            if (enumHelperType == null) return defaultValue;

            //  Get the containing dictionary, if not found return the member name.
            FieldInfo fieldInfo = enumHelperType.GetField(dictionaryPropertyName);
            if (fieldInfo == null) return defaultValue;

            var friendlyNames = fieldInfo.GetValue(null) as IDictionary;
            if (friendlyNames == null) return defaultValue;

            //  If there is no entry for this value, return the member name.
            if (!friendlyNames.Contains(value)) return defaultValue;

            //  Return the found value as a string, unless it is null or empty, then return the member name.
            var stringValue = friendlyNames[value] as String;
            return !String.IsNullOrWhiteSpace(stringValue) ? stringValue : defaultValue;
        }

        /// <summary>
        ///     Get the Helper Type for the given <paramref name="enumType"/>.
        /// </summary>
        /// <param name="enumType">The Enum type for which to retrieve a static Helper.</param>
        /// <returns></returns>
        private static Type GetEnumHelperType(Type enumType)
        {
            String enumTypeName = enumType.AssemblyQualifiedName;
            if (enumTypeName == null)
            {
                Debug.Fail("The enum type should have an AssemblyQualifiedName.");
                return null;
            }
            String enumHelperTypeName = enumTypeName.Replace(enumType.Name, String.Format("{0}Helper", enumType.Name));
            Type enumHelperType = Type.GetType(enumHelperTypeName);
            return enumHelperType;
        }

        #endregion
    }

}