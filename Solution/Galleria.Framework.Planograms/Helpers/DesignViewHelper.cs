﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-27510 : A.Kuszyk
//	Created.
// V8-27582 : A.Kuszyk
//  GetDividerTopDownPositions amended to take into account face thickness values.
// V8-27858 : A.Kuszyk
//  Added GetPositionPlacementPosition and GetMerchandisingGroupPostion methods.
// V8-27509 : A.Kuszyk
//  Added methods for Assemblies and AssemblyComponents.
// V8-28298 : M.Brumby
//  Added method to get position blocking colour "GetPositionBlockingGroupColour"
// V8-28323 : A.Kuszyk
//  Fixed GetPositionPlacementPosition method.
// V8-28665 : N.Foster
//  Fixed issue where helper is using the wrong GetPositionDetails() method
#endregion

#region Version History: CCM802

// V8-29047 : A.Silva
//      Marked as obsolete static methods that now exist as constructors in DesignViewPosition.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Provides a set of methods to get Planogram design-view relative co-ordinates.
    /// </summary>
    /// <remarks>
    /// Design-view relative co-ordinates are independant of Fixture Item rotation, since all fixture
    /// items appear flattened and adjacent. However, component and sub-component rotation is taken
    /// into account when returning positions. Co-ordinates themselves remain absolute and are <b>not</b>
    /// expressed as percentages.
    /// </remarks>
    public static class DesignViewHelper
    {
        /// <summary>
        /// Gets the design-view position of the given Assembly Component.
        /// </summary>
        /// <param name="assemblyComponent">The Assembly Component whose position should be returned.</param>
        /// <param name="fixtureAssembly">The fixture assembly in which the assembly component is placed.</param>
        /// <param name="fixtureItem">The fixture item upon which the fixture assembly is placed.</param>
        [Obsolete("Use the constructor in DesignViewPosition")]
        public static DesignViewPosition GetAssemblyComponentPosition(
            PlanogramAssemblyComponent assemblyComponent, 
            PlanogramFixtureAssembly fixtureAssembly, 
            PlanogramFixtureItem fixtureItem, 
            Single topDownHeightOffset)
        {
            return new DesignViewPosition(assemblyComponent, fixtureItem, fixtureAssembly, topDownHeightOffset);
        }

        /// <summary>
        /// Gets the design view position of the given fixture assembly.
        /// </summary>
        /// <param name="fixtureAssembly">The fixture assembly whose design view position should be returned.</param>
        /// <param name="fixtureItemPosition">The position of the fixture item upon which the fixture assembly is placed.</param>
        /// <param name="topDownHeightOffset">The top down height offset for the planogram.</param>
        [Obsolete("Use the constructor in DesignViewPosition")]
        public static DesignViewPosition GetFixtureAssemblyPosition(
            PlanogramFixtureAssembly fixtureAssembly, PlanogramFixtureItem fixtureItem, Single topDownHeightOffset)
        {
            return new DesignViewPosition(fixtureAssembly, fixtureItem, topDownHeightOffset);
        }

        /// <summary>
        /// Gets the design-view position of the given Fixture Component.
        /// </summary>
        /// <param name="fixtureComponent">The Fixture Component whose position should be returned.</param>
        /// <param name="fixtureItem">The Fixture Item that the Fixture Component belongs to.</param>
        /// <param name="topDownHeightOffset">The height offset due to any top-down merchandised components.</param>
        /// <returns>A DesignViewPosition object representing the design-view relative position.</returns>
        [Obsolete("Use the constructor in DesignViewPosition")]
        public static DesignViewPosition GetFixtureComponentPosition(
            PlanogramFixtureComponent fixtureComponent, PlanogramFixtureItem fixtureItem, Single topDownHeightOffset)
        {
            return new DesignViewPosition(fixtureComponent, fixtureItem, topDownHeightOffset);
        }

        /// <summary>
        /// Gets the design-view position of the given Fixture Item.
        /// </summary>
        /// <param name="fixtureItem">The Fixture Item whose position should be returned.</param>
        /// <returns>A DesignViewPosition object representing the design-view relative position.</returns>
        /// <remarks>
        /// The position of the Fixture Item that is returned takes into account cumulative width and 
        /// absolute height, but ignores depth.
        /// </remarks>
        /// <exception cref="ArgumentException">Thrown if the fixtureItem's Parent is null.</exception>
        [Obsolete("Use the constructor method in DesignViewPosition")]
        public static DesignViewPosition GetFixtureItemPosition(PlanogramFixtureItem fixtureItem)
        {
            return new DesignViewPosition(fixtureItem);
        }

        /// <summary>
        /// Gets the design-view position of the given Sub Component.
        /// </summary>
        /// <param name="subComponent">The Sub Component whose position should be returned.</param>
        /// <param name="fixtureComponentPosition">
        /// The DesignViewPosition of the Fixture Component to which the Sub Component belongs.
        /// </param>
        /// <returns>A DesignViewPosition object representing the design-view relative position.</returns>
        [Obsolete("Use the constructor in DesignViewPosition")]
        public static DesignViewPosition GetSubComponentPosition(
            PlanogramSubComponent subComponent, DesignViewPosition fixtureComponentPosition)
        {
            return new DesignViewPosition(subComponent, fixtureComponentPosition);
        }

        /// <summary>
        /// Gets the design-view relative positions of the peg holes on the given Sub Component.
        /// </summary>
        /// <param name="subComponent">The Sub Component whose peg hole positions should be returned.</param>
        /// <param name="subComponentPosition">The DesignViewPosition of the given Sub Component.</param>
        /// <returns>A list of co-ordinates representing the design-view positions.</returns>
        public static IEnumerable<PointValue> GetPegHolePositions(PlanogramSubComponent subComponent, DesignViewPosition subComponentPosition)
        {
            var pegHoles = subComponent.GetPegHoles();
            var pegHolePointValues =
                pegHoles.Row1.Select(p => new PointValue(p.X, p.Y, p.Z)).
                Union(
                pegHoles.Row2.Select(p => new PointValue(p.X, p.Y, p.Z)));
            return pegHolePointValues.Select(pv => subComponentPosition.Transform(pv));
        }

        /// <summary>
        /// Gets the design-view relative Y positions of the dividers on the given Sub Component.
        /// </summary>
        /// <param name="subComponent">The Sub Component whose peg hole positions should be returned.</param>
        /// <param name="subComponentPosition">The DesignViewPosition of the given Sub Component.</param>
        /// <returns>A list of Singles representing the design-view positions.</returns>
        public static IEnumerable<Single> GetDividerYPositions(
            PlanogramSubComponent subComponent, DesignViewPosition subComponentPosition)
        {
            var returnList = new List<Single>();

            Single dividerY = subComponent.FaceThicknessBottom + subComponent.DividerObstructionStartY;
            if (subComponent.Height <= dividerY || subComponent.DividerObstructionSpacingY == 0) return returnList;
            while (dividerY < subComponent.Height - subComponent.FaceThicknessTop)
            {
                var dividerPosition = subComponentPosition.Transform(new PointValue(0, dividerY, 0));
                returnList.Add(dividerPosition.Y);
                dividerY += subComponent.DividerObstructionSpacingY;
            }
            return returnList.Distinct();
        }

        /// <summary>
        /// Gets the design-view relative X positions of the dividers on the given Sub Component.
        /// </summary>
        /// <param name="subComponent">The Sub Component whose peg hole positions should be returned.</param>
        /// <param name="subComponentPosition">The DesignViewPosition of the given Sub Component.</param>
        /// <returns>A list of Singles representing the design-view positions.</returns>
        public static IEnumerable<Single> GetDividerXPositions(PlanogramSubComponent subComponent, DesignViewPosition subComponentPosition)
        {
            var returnList = new List<Single>();

            Single dividerX = subComponent.FaceThicknessLeft + subComponent.DividerObstructionStartX;
            if (subComponent.Width <= dividerX || subComponent.DividerObstructionSpacingX == 0) return returnList;
            while (dividerX < subComponent.Width - subComponent.FaceThicknessRight)
            {
                var dividerPosition = subComponentPosition.Transform(new PointValue(dividerX, 0, 0));
                returnList.Add(dividerPosition.X);
                dividerX += subComponent.DividerObstructionSpacingX;
            }
            return returnList.Distinct();
        }

        /// <summary>
        /// Gets the design-view relative Y positions of the dividers on the given Sub Component, as viewed top-down.
        /// </summary>
        /// <param name="subComponent">The Sub Component whose peg hole positions should be returned.</param>
        /// <param name="subComponentPosition">The DesignViewPosition of the given Sub Component.</param>
        /// <returns>A list of Singles representing the design-view positions.</returns>
        public static IEnumerable<Single> GetDividerTopDownPositions(
            PlanogramSubComponent subComponent, DesignViewPosition subComponentPosition)
        {
            var returnList = new List<Single>();

            Single dividerZ = subComponent.FaceThicknessBack + subComponent.DividerObstructionStartZ;
            if (dividerZ >= subComponent.Depth || subComponent.DividerObstructionSpacingZ == 0) return returnList;
            while (dividerZ < subComponent.Depth - subComponent.FaceThicknessFront)
            {
                var dividerPosition = subComponentPosition.Transform(new PointValue(0, 0, dividerZ));
                returnList.Add(dividerPosition.Y);
                dividerZ += subComponent.DividerObstructionSpacingZ;
            }
            return returnList.Distinct();
        }

        /// <summary>
        /// Gets the DesignViewPosition of the given position placement.
        /// </summary>
        /// <param name="positionPlacement">The position placement whose design view position should be returned.</param>
        /// <param name="topDownOffset">The height offset due to top down components.</param>
        /// <returns>The design view position for the given position placement.</returns>
        [Obsolete("Use the factory method in DesignViewPosition")]
        public static DesignViewPosition GetPositionPlacementPosition(PlanogramPositionPlacement positionPlacement, Single topDownOffset)
        {
            return new DesignViewPosition(positionPlacement, topDownOffset);
        }

        [Obsolete("Use the factory method in DesignViewPosition")]
        public static DesignViewPosition GetMerchandisingGroupPosition(PlanogramMerchandisingGroup merchandisingGroup)
        {
            return new DesignViewPosition(merchandisingGroup);
        }

        /// <summary>
        /// Get the colour of the block group a position is mostly within.
        /// </summary>
        /// <param name="blocking">the blocking to check for clours</param>
        /// <param name="designViewPosition">the despign view positon of a planogramPosition</param>
        /// <param name="designWidth">The total planogram design view width</param>
        /// <param name="designHight">The total planogram design view height</param>
        /// <returns>The blocking group colour</returns>
        public static Int32 GetPositionBlockingGroupColour(PlanogramBlocking blocking, DesignViewPosition designViewPosition, Single designWidth, Single designHight)
        {
            Dictionary<PlanogramBlockingGroup, Single> blockingIntersectAreas = new Dictionary<PlanogramBlockingGroup, Single>();

            foreach (PlanogramBlockingGroup group in blocking.Groups)
            {
                //Start the group of with no area
                blockingIntersectAreas.Add(group, 0);
                foreach (var location in group.GetBlockingLocations())
                {
                    //Get the 2D bounding rectangle of the location
                    RectValue locationRect = new RectValue(
                        location.X * designWidth,
                        location.Y * designHight,
                        0,
                        location.Width * designWidth,
                        location.Height * designHight,
                        1);

                    //Get the 2D bounding rectangle of the position
                    RectValue positionRect = new RectValue(
                        designViewPosition.BoundX,
                        designViewPosition.BoundY,
                        0,
                        designViewPosition.BoundWidth,
                        designViewPosition.BoundHeight,
                        1);

                    //Ge the intersect rectangle
                    RectValue? intersect = locationRect.Intersect(positionRect);

                    if (intersect.HasValue)
                    {
                        //Ass the intersect area to the blocking group
                        blockingIntersectAreas[group] += (intersect.Value.Height * intersect.Value.Width);
                    }
                }
            }

            //If the position is present in more than one group then we want to use the group that the
            //position is mostly within.
            KeyValuePair<PlanogramBlockingGroup, Single>? largestIntersect = blockingIntersectAreas.OrderByDescending(i => i.Value).FirstOrDefault();
            if (largestIntersect.HasValue && largestIntersect.Value.Value > 0)
            {
                //return the group colour
                return largestIntersect.Value.Key.Colour;
            }

            //return no colour (white)
            return Int32.MaxValue;
        }

    }
}
