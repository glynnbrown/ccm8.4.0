﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using System;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Contains helper methods for dealing with product and position orientations.
    /// </summary>
    public static class ProductOrientationHelper
    {
        /// <summary>
        /// Returns the actual angle slope roll rotation for the given orientation type.
        /// </summary>
        public static RotationValue GetRotation(PlanogramProductOrientationType type)
        {
            switch (type)
            {
                case PlanogramProductOrientationType.Front0:
                    return new RotationValue();

                case PlanogramProductOrientationType.Front90:
                    return new RotationValue(0, 0, DegreesToRadians(90));

                case PlanogramProductOrientationType.Front180:
                    return new RotationValue(0, 0, DegreesToRadians(180));

                case PlanogramProductOrientationType.Front270:
                    return new RotationValue(0, 0, DegreesToRadians(270));

                case PlanogramProductOrientationType.Top0:
                    return new RotationValue(0, DegreesToRadians(270), 0);

                case PlanogramProductOrientationType.Top90:
                    return new RotationValue(DegreesToRadians(90), 0, DegreesToRadians(90));

                case PlanogramProductOrientationType.Top180:
                    return new RotationValue(DegreesToRadians(180), DegreesToRadians(90), 0);

                case PlanogramProductOrientationType.Top270:
                    return new RotationValue(DegreesToRadians(90), DegreesToRadians(180), DegreesToRadians(90));

                case PlanogramProductOrientationType.Right0:
                    return new RotationValue(DegreesToRadians(90), 0, 0);

                case PlanogramProductOrientationType.Right90:
                    return new RotationValue(DegreesToRadians(90), DegreesToRadians(90), 0);

                case PlanogramProductOrientationType.Right180:
                    return new RotationValue(DegreesToRadians(270), 0, DegreesToRadians(180));

                case PlanogramProductOrientationType.Right270:
                    return new RotationValue(0, DegreesToRadians(270), DegreesToRadians(270));

                case PlanogramProductOrientationType.Left0:
                    return new RotationValue(DegreesToRadians(270), 0, 0);

                case PlanogramProductOrientationType.Left90:
                    return new RotationValue(DegreesToRadians(270), DegreesToRadians(270), 0);

                case PlanogramProductOrientationType.Left180:
                    return new RotationValue(DegreesToRadians(270), DegreesToRadians(180), 0);

                case PlanogramProductOrientationType.Left270:
                    return new RotationValue(DegreesToRadians(270), DegreesToRadians(90), 0);

                case PlanogramProductOrientationType.Back0:
                    return new RotationValue(DegreesToRadians(180), 0, 0);

                case PlanogramProductOrientationType.Back90:
                    return new RotationValue(DegreesToRadians(180), 0, DegreesToRadians(270));

                case PlanogramProductOrientationType.Back180:
                    return new RotationValue(DegreesToRadians(180), 0, DegreesToRadians(180));

                case PlanogramProductOrientationType.Back270:
                    return new RotationValue(DegreesToRadians(180), 0, DegreesToRadians(90));

                case PlanogramProductOrientationType.Bottom0:
                    return new RotationValue(0, DegreesToRadians(90), 0);

                case PlanogramProductOrientationType.Bottom90:
                    return new RotationValue(DegreesToRadians(270), 0, DegreesToRadians(90));

                case PlanogramProductOrientationType.Bottom180:
                    return new RotationValue(0, DegreesToRadians(270), DegreesToRadians(180));

                case PlanogramProductOrientationType.Bottom270:
                    return new RotationValue(DegreesToRadians(90), 0, DegreesToRadians(270));

                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Converts the given degree value to radians.
        /// </summary>
        private static Single DegreesToRadians(Single degrees)
        {
            return Convert.ToSingle(degrees * (Math.PI / 180f));
        }

        public static WidthHeightDepthValue GetOrientatedSize(PlanogramProductOrientationType orientationType, WidthHeightDepthValue size)
        {
            return GetOrientatedSize(orientationType, size.Width, size.Height, size.Depth);
        }

        /// <summary>
        /// Returns the item size orientated to the given orientation type.
        /// </summary>
        public static WidthHeightDepthValue GetOrientatedSize(PlanogramProductOrientationType orientationType, Single width, Single height, Single depth)
        {
            switch (orientationType)
            {
                default:
                case PlanogramProductOrientationType.Front0:
                case PlanogramProductOrientationType.Back0:
                case PlanogramProductOrientationType.Front180:
                case PlanogramProductOrientationType.Back180:
                    return new WidthHeightDepthValue(width, height, depth);

                case PlanogramProductOrientationType.Front90:
                case PlanogramProductOrientationType.Back90:
                case PlanogramProductOrientationType.Front270:
                case PlanogramProductOrientationType.Back270:
                    return new WidthHeightDepthValue(height, width, depth);

                case PlanogramProductOrientationType.Right0:
                case PlanogramProductOrientationType.Left0:
                case PlanogramProductOrientationType.Right180:
                case PlanogramProductOrientationType.Left180:
                    return new WidthHeightDepthValue(depth, height, width);

                case PlanogramProductOrientationType.Right90:
                case PlanogramProductOrientationType.Left90:
                case PlanogramProductOrientationType.Right270:
                case PlanogramProductOrientationType.Left270:
                    return new WidthHeightDepthValue(height, depth, width);

                case PlanogramProductOrientationType.Top0:
                case PlanogramProductOrientationType.Bottom0:
                case PlanogramProductOrientationType.Top180:
                case PlanogramProductOrientationType.Bottom180:
                    return new WidthHeightDepthValue(width, depth, height);

                case PlanogramProductOrientationType.Top90:
                case PlanogramProductOrientationType.Bottom90:
                case PlanogramProductOrientationType.Top270:
                case PlanogramProductOrientationType.Bottom270:
                    return new WidthHeightDepthValue(depth, width, height);
            }
        }

        /// <summary>
        /// Returns the item size orientated to the given orientation type.
        /// </summary>
        public static WidthHeightDepthValue GetOrientatedSize(PlanogramPositionOrientationType orientationType, WidthHeightDepthValue size)
        {
            return GetOrientatedSize(orientationType, size.Width, size.Height, size.Depth);
        }

        /// <summary>
        /// Returns the item size orientated to the given orientation type.
        /// </summary>
        public static WidthHeightDepthValue GetOrientatedSize(PlanogramPositionOrientationType orientationType, Single width, Single height, Single depth)
        {
            switch (orientationType)
            {
                default:
                case PlanogramPositionOrientationType.Front0:
                case PlanogramPositionOrientationType.Back0:
                case PlanogramPositionOrientationType.Front180:
                case PlanogramPositionOrientationType.Back180:
                    return new WidthHeightDepthValue(width, height, depth);

                case PlanogramPositionOrientationType.Front90:
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Back270:
                    return new WidthHeightDepthValue(height, width, depth);

                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Right180:
                case PlanogramPositionOrientationType.Left180:
                    return new WidthHeightDepthValue(depth, height, width);

                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Right270:
                case PlanogramPositionOrientationType.Left270:
                    return new WidthHeightDepthValue(height, depth, width);

                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Top180:
                case PlanogramPositionOrientationType.Bottom180:
                    return new WidthHeightDepthValue(width, depth, height);

                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Top270:
                case PlanogramPositionOrientationType.Bottom270:
                    return new WidthHeightDepthValue(depth, width, height);
            }
        }

        /// <summary>
        /// Resets the values of the given orientation back to front0.
        /// </summary>
        public static WidthHeightDepthValue GetUnorientatedSize(PlanogramProductOrientationType currentOrientationType, Single width, Single height, Single depth)
        {
            switch (currentOrientationType)
            {
                case PlanogramProductOrientationType.Front0:
                case PlanogramProductOrientationType.Back0:
                case PlanogramProductOrientationType.Front180:
                case PlanogramProductOrientationType.Back180:
                    return new WidthHeightDepthValue(width, height, depth);

                case PlanogramProductOrientationType.Right90:
                case PlanogramProductOrientationType.Left90:
                case PlanogramProductOrientationType.Right270:
                case PlanogramProductOrientationType.Left270:
                    return new WidthHeightDepthValue(depth, width, height);

                default:
                    //just call it again?
                    return GetOrientatedSize(currentOrientationType, width, height, depth);
            }

        }

        /// <summary>
        /// Returns the maximum squeeze percentages for the given product to the requested orientation.
        /// </summary>
        public static WidthHeightDepthValue GetOrientatedMaxSqueeze(PlanogramProduct product, PlanogramProductMerchandisingStyle merchStyle, PlanogramProductOrientationType orientation)
        {
            //if not merchandising as a unit, return no squeeze.
            if (merchStyle != PlanogramProductMerchandisingStyle.Unit) return new WidthHeightDepthValue(1, 1, 1);

            WidthHeightDepthValue rotatedMaxSqueeze = GetOrientatedSize(orientation, product.SqueezeWidth, product.SqueezeHeight, product.SqueezeDepth);
            if ((rotatedMaxSqueeze.Width * 100).EqualTo(0)) rotatedMaxSqueeze.Width = 1;
            if ((rotatedMaxSqueeze.Height * 100).EqualTo(0)) rotatedMaxSqueeze.Height = 1;
            if ((rotatedMaxSqueeze.Depth * 100).EqualTo(0)) rotatedMaxSqueeze.Depth = 1;

            return rotatedMaxSqueeze;
        }

        /// <summary>
        /// Gets the suggested right cap orientation for the given main 
        /// </summary>
        public static PlanogramProductOrientationType GetRightCapOrientation(PlanogramProductOrientationType mainOrientation)
        {
            switch (mainOrientation)
            {
                case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Right0;
                case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Top90;
                case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Right180;
                case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Bottom270;
                case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Back270;
                case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Left270;
                case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Front270;
                case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Back0;
                case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Top180;
                case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Front180;
                case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Back180;
                case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Left0;
                case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Top270;
                case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Left180;
                case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Bottom90;
                case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Right90;
                case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Front90;
                case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Back90;
                case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Front0;
                case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Top0;
                case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Front180;
                case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Bottom0;
                default:
                    Debug.Fail("TODO");
                    return PlanogramProductOrientationType.Right0;
            }
        }

        /// <summary>
        /// Gets the suggested top cap orientation for the given main.
        /// </summary>
        public static PlanogramProductOrientationType GetTopCapOrientation(PlanogramProductOrientationType mainOrientation)
        {
            switch (mainOrientation)
            {
                case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Top0;
                case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Left90;
                case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Top180;
                case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Front0;
                case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Left180;
                case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Front180;
                case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Right180;
                case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Top90;
                case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Front90;
                case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Bottom90;
                case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Front270;
                case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Bottom180;
                case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Right90;
                case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Bottom0;
                case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Right270;
                case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Front0;
                case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Left0;
                case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Back0;
                case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Right0;
                case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Top270;
                case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Back90;
                case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Bottom270;
                case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Front270;
                default:
                    Debug.Fail("TODO");
                    return PlanogramProductOrientationType.Top0;
            }
        }
    }
}
