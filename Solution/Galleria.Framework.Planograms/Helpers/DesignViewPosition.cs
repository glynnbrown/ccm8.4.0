﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-27510 : A.Kuszyk
//	Created.
#endregion

#region Version History: CCM802

// V8-29407 : A.Silva
//      Refactored static methods from DesignViewHelper to be constructors for DesignViewPosition.
// V8-29000 : A.Silva
//      Added ToRectValue2D method to help getting the flat rect for the DesignViewPosition.
// V8-29309 : A.Silva
//      Amended DesignViewPosition(PlanogramPositionPlacement, Single) so that the position offset is calculated correctly.

#endregion

#region Version History : CCM 803
// V8-29137 : A.Kuszyk
//  Re-factored merchandising group and position placement constructors to take account of top-down components.
//  Changed IntersectsWith to OverlapsWith.
#endregion

#region Version History : CCM811
// V8-30152 : A.Kuszyk
//  Fixed issues with rotation in merchandising group and position placement constructors and exposed TransformationMatrix 
//  and related properties.
#endregion

#region Version History : CCM830
// V8-31804 : A.Kuszyk
//  Added OverlapsBy method.
// V8-32870 : A.Kuszyk
//  Added additional constructor for PlanogramSubComponentPlacements.
// V8-32948 : A.Kuszyk
//  Added GetBoundCoordinate and GetBoundSize helper methods.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Enums;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Represents the Planogram design-view relative co-ordinates and size of
    /// a visual item on a Planogram.
    /// </summary>
    public class DesignViewPosition
    {
        #region Properties
        /// <summary>
        /// The absolute Slope of the item.
        /// </summary>
        public Single Slope { get; private set; }

        /// <summary>
        /// The absolute Angle of the item.
        /// </summary>
        public Single Angle { get; private set; }

        /// <summary>
        /// The absolute Roll of the item.
        /// </summary>
        public Single Roll { get; private set; }

        /// <summary>
        /// The X position of the bounding box that encompasses the item.
        /// </summary>
        public Single BoundX { get; private set; }

        /// <summary>
        /// The Y position of the bounding box that encompasses the item.
        /// </summary>
        public Single BoundY { get; private set; }

        /// <summary>
        /// The Z position of the bounding box that encompasses the item.
        /// </summary>
        public Single BoundZ { get; private set; }

        /// <summary>
        /// The Width of the bounding box that encompasses the item.
        /// </summary>
        public Single BoundWidth { get; private set; }

        /// <summary>
        /// The Height of the bounding box that encompasses the item.
        /// </summary>
        public Single BoundHeight { get; private set; }

        /// <summary>
        /// The Depth of the bounding box that encompasses the item.
        /// </summary>
        public Single BoundDepth { get; private set; }

        /// <summary>
        /// The <see cref="MatrixValue"/> that represents the transform that an item relative to this would have
        /// to go through to be in design-view space.
        /// </summary>
        public MatrixValue TransformationMatrix { get; private set;  }

        /// <summary>
        /// The rotation component of <see cref="TransformationMatrix"/> as a <see cref="RotationValue"/>.
        /// </summary>
        public RotationValue TransformationRotation { get; private set; }

        /// <summary>
        /// The translation component of <see cref="TransformationMatrix"/> as a <see cref="PointValue"/>.
        /// </summary>
        public PointValue TransformationTranslation { get; private set; }

        /// <summary>
        /// The translation component of <see cref="TransformationMatrix"/> as a <see cref="MatrixValue"/>.
        /// </summary>
        public MatrixValue TranslationMatrix { get; private set; }

        /// <summary>
        /// The rotation component of <see cref="TransformationMatrix"/> as a <see cref="MatrixValue"/>.
        /// </summary>
        public MatrixValue RotationMatrix { get; private set; }
        #endregion

        #region Constructors

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramFixtureItem"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        public DesignViewPosition(PlanogramFixtureItem source)
        {
            PlanogramFixture fixture = source.GetPlanogramFixture();

            var rotation = new RotationValue();
            var size = new WidthHeightDepthValue(fixture.Width, fixture.Height, fixture.Depth);

            //  Get the total x offset due to bays to the left of this.
            Single xOffset = source.Parent.FixtureItems
                .OrderBy(f => f.BaySequenceNumber)
                .TakeWhile(item => item != source)
                .Sum(item => item.GetPlanogramFixture().Width);
            var origin = new PointValue(xOffset, source.Y, 0);

            Initialize(origin, size, rotation);
        }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramFixtureComponent"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        /// <param name="topDownHeightOffset">The height offset due to any top-down merchandised components.</param>
        /// <param name="fixtureItem">The <see cref="PlanogramFixtureItem"/> containing the <paramref name="source"/>.</param>
        public DesignViewPosition(PlanogramFixtureAssembly source, PlanogramFixtureItem fixtureItem, Single topDownHeightOffset)
        {
            PlanogramAssembly assembly = source.GetPlanogramAssembly();

            var rotation = new RotationValue(source.Angle, source.Slope, source.Roll);
            var size = new WidthHeightDepthValue(assembly.Width, assembly.Height, assembly.Depth);
            var origin = new PointValue(source.X, source.Y + topDownHeightOffset, source.Z);
            origin = new DesignViewPosition(fixtureItem).Transform(origin);

            Initialize(origin, size, rotation);
        }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramFixtureComponent"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        /// <param name="topDownHeightOffset">The height offset due to any top-down merchandised components.</param>
        /// <param name="fixtureItem">The <see cref="PlanogramFixtureItem"/> containing the <paramref name="source"/>.</param>
        public DesignViewPosition(PlanogramFixtureComponent source, PlanogramFixtureItem fixtureItem, Single topDownHeightOffset)
        {
            PlanogramComponent component = source.GetPlanogramComponent();

            var rotation = new RotationValue(source.Angle, source.Slope, source.Roll);
            var size = new WidthHeightDepthValue(component.Width, component.Height, component.Depth);
            var origin = new PointValue(source.X, source.Y + topDownHeightOffset, source.Z);
            origin = new DesignViewPosition(fixtureItem).Transform(origin);

            // Rotate component down by 90 degrees if it is merchandised top down.
            // This will affect all subsequent positions (such as those for sub-components).
            if (component.IsMerchandisedTopDown)
            {
                rotation.Slope -= Convert.ToSingle(Math.PI / 2f);
                origin.Y += size.Height;
            }

            Initialize(origin, size, rotation);
        }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramFixtureComponent"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        /// <param name="fixtureItem">The <see cref="PlanogramFixtureItem"/> containing the <paramref name="fixtureAssembly"/>.</param>
        /// <param name="fixtureAssembly">The <see cref="PlanogramFixtureAssembly"/> containing the <paramref name="source"/></param>
        /// <param name="topDownHeightOffset">The height offset due to any top-down merchandised components.</param>
        public DesignViewPosition(PlanogramAssemblyComponent source, PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly, Single topDownHeightOffset)
        {
            PlanogramComponent component = source.GetPlanogramComponent();

            // Compound rotation of fixture assembly to assembly component's.
            var rotation = new RotationValue(source.Angle + fixtureAssembly.Angle, source.Slope + fixtureAssembly.Slope, source.Roll + fixtureAssembly.Roll);
            var size = new WidthHeightDepthValue(component.Width, component.Height, component.Depth);
            var origin = new PointValue(source.X, source.Y + topDownHeightOffset, source.Z);
            origin = new DesignViewPosition(fixtureAssembly, fixtureItem, topDownHeightOffset).Transform(origin);

            // Rotate component down by 90 degrees if it is merchandised top down.
            // This will affect all subsequent positions (such as those for sub-components).
            if (component.IsMerchandisedTopDown)
            {
                rotation.Slope -= Convert.ToSingle(Math.PI / 2);
                origin.Y += size.Height;
            }

            Initialize(origin, size, rotation);
        }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramSubComponent"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        /// <param name="fixtureComponentPosition">The <see cref="DesignViewPosition"/> that the subcomponent belongs to.</param>
        public DesignViewPosition(PlanogramSubComponent source, DesignViewPosition fixtureComponentPosition)
        {
            InitializeSubComponent(source, fixtureComponentPosition);
        }

        private void InitializeSubComponent(PlanogramSubComponent source, DesignViewPosition fixtureComponentPosition)
        {
            // Compound rotation of the sub-component to the fixture component's.
            var rotation = new RotationValue(source.Angle + fixtureComponentPosition.Angle, source.Slope + fixtureComponentPosition.Slope, source.Roll + fixtureComponentPosition.Roll);
            var size = new WidthHeightDepthValue(source.Width, source.Height, source.Depth);
            var origin = new PointValue(source.X, source.Y, source.Z);
            origin = fixtureComponentPosition.Transform(origin);

            Initialize(origin, size, rotation);
        }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramPositionPlacement"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        /// <param name="topDownOffset">The height offset due to top down components.</param>
        public DesignViewPosition(PlanogramPositionPlacement source, Single topDownOffset)
        {
            //  A PlanogramPositionPlacement.Position has coordinates 
            //  relative to its parent MerchandisingGroup.
            var parentDesignViewPosition = new DesignViewPosition(source.MerchandisingGroup, topDownOffset);

            //  Account for any rotation that the parent element might have.
            var rotation = new RotationValue(parentDesignViewPosition.Angle,
                                             parentDesignViewPosition.Slope,
                                             parentDesignViewPosition.Roll);

            //  Get size of the position.
            PlanogramPositionDetails positionDetails = source.GetPositionDetails();
            WidthHeightDepthValue size = positionDetails.TotalSize;

            //  Get the origin coordinates, 
            //  accounting for the parent element's offset.
            RectValue merchGroupRelativeBoundingBox = new RectValue(
                source.GetMerchandisingGroupRelativeCoordinates(),
                size);
            RectValue transformedBoundingBox = parentDesignViewPosition.TransformBounds(merchGroupRelativeBoundingBox);
            PointValue origin = new PointValue(transformedBoundingBox.X, transformedBoundingBox.Y, transformedBoundingBox.Z);
            Initialize(origin, size.Rotate(rotation), rotation, applyRotation: false);
        }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramMerchandisingGroup"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        public DesignViewPosition(PlanogramMerchandisingGroup source)
        {
            // Get the design view dimensions of the planogram.
            Single width;
            Single height;
            Single offset;
            IEnumerable<PlanogramSubComponentPlacement> placements = source.SubComponentPlacements.ToList();
            placements.First().Planogram.GetBlockingAreaSize(out height, out width, out offset);

            InitializeMerchandisingGroupDvp(source, offset);
        }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DesignViewPosition"/> from a <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramMerchandisingGroup"/> from which to instantiate a <see cref="DesignViewPosition"/>.</param>
        /// <param name="planogramBlockingHeightOffset"></param>
        public DesignViewPosition(PlanogramMerchandisingGroup source, Single planogramBlockingHeightOffset)
        {
            InitializeMerchandisingGroupDvp(source, planogramBlockingHeightOffset);
        }

        /// <summary>
        /// Instantiates a new DesignViewPosition from the given Blocking Location.
        /// </summary>
        /// <param name="blockingLocation">The blocking location to create a design view position for.</param>
        /// <param name="planogramWidth">The design-view width of the planogram.</param>
        /// <param name="planogramHeight">The design-view height of the planogram.</param>
        /// <param name="blockDepth">The depth to assign to the blocking location's space.</param>
        public DesignViewPosition(PlanogramBlockingLocation blockingLocation, Single planogramWidth, Single planogramHeight, Single blockDepth = 0)
        {
            Initialize(
                new PointValue(blockingLocation.X * planogramWidth, blockingLocation.Y * planogramHeight, 0),
                new WidthHeightDepthValue(blockingLocation.Width * planogramWidth, blockingLocation.Height * planogramHeight, blockDepth),
                new RotationValue());
        }

        public DesignViewPosition(PlanogramSubComponentPlacement subComponentPlacement, Single planogramBlockingHeightOffset)
        {
            if (subComponentPlacement.FixtureComponent != null)
            {
                InitializeSubComponent(
                    subComponentPlacement.SubComponent,
                    new DesignViewPosition(
                        subComponentPlacement.FixtureComponent,
                        subComponentPlacement.FixtureItem,
                        planogramBlockingHeightOffset));
            }
            else if (subComponentPlacement.AssemblyComponent != null)
            {
                InitializeSubComponent(
                    subComponentPlacement.SubComponent,
                    new DesignViewPosition(
                        subComponentPlacement.AssemblyComponent,
                        subComponentPlacement.FixtureItem,
                        subComponentPlacement.FixtureAssembly,
                        planogramBlockingHeightOffset));
            }
            else
            {
                Debug.Fail("A design view position can only be calculated if the sub component placement has either a fixture component or assembly component.");
            }
        }

        public DesignViewPosition(PlanogramMerchandisingBlockPlacement blockPlacement, DesignViewPosition merchGroupDvp)
        {
            RectValue designViewBounds = merchGroupDvp.TransformBounds(blockPlacement.Bounds);
            BoundX = designViewBounds.X;
            BoundY = designViewBounds.Y;
            BoundZ = designViewBounds.Z;
            BoundWidth = designViewBounds.Width;
            BoundHeight = designViewBounds.Height;
            BoundDepth = designViewBounds.Depth;
            TransformationRotation = merchGroupDvp.TransformationRotation;
            TransformationTranslation = merchGroupDvp.TransformationTranslation;
            TranslationMatrix = merchGroupDvp.TranslationMatrix;
            RotationMatrix = merchGroupDvp.RotationMatrix;
            TransformationMatrix = merchGroupDvp.TranslationMatrix;
            Angle = merchGroupDvp.Angle;
            Slope = merchGroupDvp.Slope;
            Roll = merchGroupDvp.Roll;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Transforms the given PointValue through the position and rotation of this item.
        /// </summary>
        /// <param name="pointValue">The co-ordinates to translate.</param>
        /// <returns>The transformed co-ordinates.</returns>
        public PointValue Transform(PointValue pointValue)
        {
            return TransformationMatrix.Transform(pointValue);
        }

        /// <summary>
        /// Transforms the given <see cref="RectValue"/> through the position and rotation of this item.
        /// </summary>
        /// <param name="rectValue">The bounds to transform.</param>
        /// <returns>The transformed bounds.</returns>
        public RectValue TransformBounds(RectValue rectValue)
        {
            return TransformationMatrix.TransformBounds(rectValue);
        }

        /// <summary>
        ///     Initializes the values for this instance.
        /// </summary>
        /// <param name="origin">The co-ordinates of the item.</param>
        /// <param name="size">The actual, flat size of the item. I.e. its un-transformed width, height and depth.</param>
        /// <param name="rotation">The absolute rotation values of the item.</param>
        private void Initialize(
            PointValue origin, 
            WidthHeightDepthValue size, 
            RotationValue rotation, 
            Boolean applyRotation = true, 
            RotationValue? transformationRotation = null,
            PointValue? transformationTranslation = null)
        {
            TransformationRotation = transformationRotation ?? rotation;
            TransformationTranslation = transformationTranslation ?? origin;
            TranslationMatrix = MatrixValue.CreateTranslateMatix(TransformationTranslation);
            RotationMatrix = MatrixValue.CreateRotationMatrix(TransformationRotation);
            TransformationMatrix = MatrixValue.CreateTransformationMatrix(TransformationRotation, TransformationTranslation);

            Angle = rotation.Angle;
            Slope = rotation.Slope;
            Roll = rotation.Roll;
            if (applyRotation)
            {
                List<PointValue> vertices = size.GetRotatedVertices(rotation, origin).ToList();
                BoundX = vertices.Min(o => o.X);
                BoundY = vertices.Min(o => o.Y);
                BoundZ = vertices.Min(o => o.Z);
                BoundWidth = vertices.Max(o => o.X) - BoundX;
                BoundHeight = vertices.Max(o => o.Y) - BoundY;
                BoundDepth = vertices.Max(o => o.Z) - BoundZ;
            }
            else
            {
                BoundX = origin.X;
                BoundY = origin.Y;
                BoundZ = origin.Z;
                BoundWidth = size.Width;
                BoundHeight = size.Height;
                BoundDepth = size.Depth;
            }
        }

        private void InitializeMerchandisingGroupDvp(PlanogramMerchandisingGroup source, Single planogramBlockingHeightOffset)
        {
            PlanogramSubComponentPlacement firstSubCompPlacement = source.SubComponentPlacements.First();
            //PointValue origin = firstSubCompPlacement.GetPlanogramRelativeCoordinates();

            DesignViewPosition firstSubCompDvp;

            if (firstSubCompPlacement.FixtureComponent != null)
            {
                firstSubCompDvp =
                    new DesignViewPosition(
                    firstSubCompPlacement.SubComponent,
                        new DesignViewPosition(
                            firstSubCompPlacement.FixtureComponent,
                            firstSubCompPlacement.FixtureItem,
                            planogramBlockingHeightOffset));
            }
            else if (firstSubCompPlacement.AssemblyComponent != null)
            {
                firstSubCompDvp =
                    new DesignViewPosition(
                    firstSubCompPlacement.SubComponent,
                        new DesignViewPosition(
                            firstSubCompPlacement.AssemblyComponent,
                            firstSubCompPlacement.FixtureItem,
                            firstSubCompPlacement.FixtureAssembly,
                            planogramBlockingHeightOffset));
            }
            else return;

            PointValue origin = new PointValue(firstSubCompDvp.BoundX, firstSubCompDvp.BoundY, firstSubCompDvp.BoundZ);

            RotationValue rotation = new RotationValue(
                firstSubCompDvp.Angle, firstSubCompDvp.Slope, firstSubCompDvp.Roll);

            WidthHeightDepthValue size = new WidthHeightDepthValue(
                        source.SubComponentPlacements.Sum(s => s.SubComponent.Width),
                        firstSubCompPlacement.SubComponent.Height,
                        firstSubCompPlacement.SubComponent.Depth)
                        .Rotate(rotation);

            Initialize(
                origin,
                size,
                rotation,
                applyRotation: false,
                transformationRotation: firstSubCompDvp.TransformationRotation,
                transformationTranslation: firstSubCompDvp.TransformationTranslation);
        }

        
        public RectValue ToRectValue2D()
        {
            var coordinates = new PointValue(BoundX, BoundY, 0);
            var space = new WidthHeightDepthValue(BoundWidth, BoundHeight, 1);
            return new RectValue(coordinates, space);
        }

        public RectValue ToRectValue()
        {
            return new RectValue(BoundX, BoundY, BoundZ, BoundWidth, BoundHeight, BoundDepth);
        }

        /// <summary>
        /// Returns true if the given blocking location overlaps with this design view position in the XY plane by more than
        /// the given percentage threshold.
        /// </summary>
        /// <param name="blockingLocation">The blocking location to test for overlap.</param>
        /// <param name="percentageThreshold">The percentage threshold above which an overlap will be considered.</param>
        /// <param name="planogramBlockingWidth">The plan's blocking width.</param>
        /// <param name="planogramBlockingHeight">The plan's blocking height.</param>
        /// <returns>True if there is an overlap greater than the given threshold.</returns>
        public Boolean OverlapsWith(
            PlanogramBlockingLocation blockingLocation, 
            Single percentageThreshold,
            Single planogramBlockingWidth, 
            Single planogramBlockingHeight)
        {
            Single xOverlap = GetXOverlap(blockingLocation, planogramBlockingWidth);
            if (xOverlap.LessOrEqualThan(0)) return false;

            Single yOverlap = GetYOverlap(blockingLocation, planogramBlockingHeight);
            if (yOverlap.LessOrEqualThan(0)) return false;

            return ((xOverlap * yOverlap) / (BoundWidth * BoundHeight)).GreaterThan(percentageThreshold,4); // Maximum precision, because we're dealing with percentages.
        }

        /// <summary>
        /// Returns the percentage of this design view position that if partially overpas with the blocking location given in
        /// the XY plane.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <returns></returns>
        public Double PartialOverlapsBy(PlanogramBlockingLocation location, Single blockingWidth, Single blockingHeight)
        {
            Single xOverlap = GetXOverlap(location, blockingWidth);
            if (xOverlap.LessOrEqualThan(0)) return 0d;

            Single yOverlap = GetYOverlap(location, blockingHeight);
            if (yOverlap.LessOrEqualThan(0)) return 0d;

            return (xOverlap * yOverlap) / (BoundWidth * BoundHeight);
        }

        /// <summary>
        /// Returns the percentage of this design view position that if fully overpas with the blocking location given in
        /// the XY plane.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <returns></returns>
        public Double FullOverlapsBy(PlanogramBlockingLocation location, Single blockingWidth, Single blockingHeight)
        {
            Single xOverlap = GetXOverlap(location, blockingWidth);
            if (xOverlap.LessOrEqualThan(0)) return 0d;

            Single yOverlap = GetYOverlap(location, blockingHeight);
            if (yOverlap.LessOrEqualThan(BoundHeight)) return 0d;

            return (xOverlap * yOverlap) / (BoundWidth * BoundHeight);
        }

        private Single GetYOverlap(PlanogramBlockingLocation blockingLocation, Single planogramBlockingHeight)
        {
            Single yOverlap =
                Math.Min(BoundY + BoundHeight, blockingLocation.Y * planogramBlockingHeight + blockingLocation.Height * planogramBlockingHeight) -
                Math.Max(BoundY, blockingLocation.Y * planogramBlockingHeight);
            return yOverlap;
        }

        private Single GetXOverlap(PlanogramBlockingLocation blockingLocation, Single planogramBlockingWidth)
        {
            Single xOverlap =
                Math.Min(BoundX + BoundWidth, blockingLocation.X * planogramBlockingWidth + blockingLocation.Width * planogramBlockingWidth) -
                Math.Max(BoundX, blockingLocation.X * planogramBlockingWidth);
            return xOverlap;
        }

        /// <summary>
        /// Gets the Bounding co-ordinate of the design view position, in the <paramref name="axis"/> specified.
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        public Single GetBoundCoordinate(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return BoundX;
                case AxisType.Y:
                    return BoundY;
                case AxisType.Z:
                    return BoundZ;
                default:
                    return BoundX;
            }
        }

        /// <summary>
        /// Gets the Bounding size of the design view position, in the <paramref name="axis"/> specified.
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        public Single GetBoundSize(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return BoundWidth;
                case AxisType.Y:
                    return BoundHeight;
                case AxisType.Z:
                    return BoundDepth;
                default:
                    return BoundWidth;
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        ///     Determines whether two instances are considered equal.
        /// </summary>
        /// <param name="other">The other instance to be compared.</param>
        private Boolean Equals(DesignViewPosition other)
        {
            return Slope.Equals(other.Slope) &&
                   Angle.Equals(other.Angle) &&
                   Roll.Equals(other.Roll) &&
                   BoundX.Equals(other.BoundX) &&
                   BoundY.Equals(other.BoundY) &&
                   BoundZ.Equals(other.BoundZ) &&
                   BoundWidth.Equals(other.BoundWidth) &&
                   BoundHeight.Equals(other.BoundHeight) &&
                   BoundDepth.Equals(other.BoundDepth);
        }

        /// <summary>
        ///     Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (DesignViewPosition)) return false;
            return Equals((DesignViewPosition) obj);
        }

        /// <summary>
        ///     Serves as a hash function for <see cref="DesignViewPosition"/> instances. 
        /// </summary>
        /// <returns>
        ///     A hash code for the current instance.
        /// </returns>
        public override Int32 GetHashCode()
        {
            unchecked
            {
                int hashCode = Slope.GetHashCode();
                hashCode = (hashCode*397) ^ Angle.GetHashCode();
                hashCode = (hashCode*397) ^ Roll.GetHashCode();
                hashCode = (hashCode*397) ^ BoundX.GetHashCode();
                hashCode = (hashCode*397) ^ BoundY.GetHashCode();
                hashCode = (hashCode*397) ^ BoundZ.GetHashCode();
                hashCode = (hashCode*397) ^ BoundWidth.GetHashCode();
                hashCode = (hashCode*397) ^ BoundHeight.GetHashCode();
                hashCode = (hashCode*397) ^ BoundDepth.GetHashCode();
                return hashCode;
            }
        }

        #endregion
    }

    internal static class GeometryHelper
    {
        /// <summary>
        ///     Enumerates the vertices after applying the rotation.
        /// </summary>
        /// <param name="size">The actual, flat size of the item. I.e. its un-transformed width, height and depth.</param>
        /// <param name="rotation">The absolute rotation to be applied.</param>
        /// <param name="originPoint">The pivot point for the rotation.</param>
        /// <returns>An enumeration of <see cref="PointValue"/> instances representing the vertices after applying the transformation.</returns>
        /// <remarks>The <paramref name="originPoint"/> is expected to be one of the vertices, and will be returned as one, untransformed.</remarks>
        internal static IEnumerable<PointValue> GetRotatedVertices(this WidthHeightDepthValue size, RotationValue rotation, PointValue originPoint)
        {
            MatrixValue rotationMatrix = MatrixValue.CreateRotationMatrix(rotation);
            Single height = size.Height;
            Single width = size.Width;
            Single depth = size.Depth;
            yield return originPoint;
            yield return originPoint + rotationMatrix.Transform(new PointValue(0, height, 0));
            yield return originPoint + rotationMatrix.Transform(new PointValue(width, 0, 0));
            yield return originPoint + rotationMatrix.Transform(new PointValue(width, height, 0));
            yield return originPoint + rotationMatrix.Transform(new PointValue(0, 0, depth));
            yield return originPoint + rotationMatrix.Transform(new PointValue(0, height, depth));
            yield return originPoint + rotationMatrix.Transform(new PointValue(width, 0, depth));
            yield return originPoint + rotationMatrix.Transform(new PointValue(width, height, depth));
        }

        internal static WidthHeightDepthValue Rotate(this WidthHeightDepthValue size, RotationValue rotation)
        {
            RectValue rotatedSizeBounds = MatrixValue.CreateRotationMatrix(rotation).TransformBounds(
                new RectValue(
                    new PointValue(0, 0, 0),
                    size));
            return new WidthHeightDepthValue(rotatedSizeBounds.Width, rotatedSizeBounds.Height, rotatedSizeBounds.Depth);
        }
    }
}