﻿#region Header Information

// Copyright © Galleria RTS Ltd 2016

#region Version History : CCM830
//V8-13761 : L.Ineson
//  Created
#endregion

#endregion

using Galleria.Framework.DataStructures;
using System;

namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Provides information on bounding boxes for an item.
    /// Generally used for collision detection.
    /// </summary>
    public sealed class BoundaryInfo
    {
        #region Fields
        private RectValue? _worldRelativeBounds { get; set; }
        private OrientedRectValue? _worldRelativeOrientedBounds { get; set; }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the matrix value which can be used to transform the bounds
        /// of the item to be world space relative.
        /// </summary>
        public MatrixValue ToWorld { get; private set; }

        /// <summary>
        /// Gets the height of the item bounding box.
        /// </summary>
        public Single Height { get; private set; }

        /// <summary>
        /// Gets the width of the item bounding box.
        /// </summary>
        public Single Width { get; private set; }

        /// <summary>
        /// Gets the depth of the item bounding box.
        /// </summary>
        public Single Depth { get; private set;}

        /// <summary>
        /// Gets the world relative axis aligned bounds of the item.
        /// </summary>
        /// <remarks>Lazy loaded.</remarks>
        public RectValue WorldRelativeBounds
        {
            get
            {
                if (!_worldRelativeBounds.HasValue)
                {
                    _worldRelativeBounds = ToWorld.TransformBounds(
                         new RectValue(0,0,0, this.Width, this.Height, this.Depth));
                }
                return _worldRelativeBounds.Value;
            }
        }

        /// <summary>
        /// Gets the world relative oriented bounds of the item
        /// </summary>
        /// /// <remarks>Lazy loaded.</remarks>
        public OrientedRectValue? WorldRelativeOrientedBounds
        {
            get
            {
                if (!_worldRelativeOrientedBounds.HasValue)
                {
                    _worldRelativeOrientedBounds = 
                        new OrientedRectValue(this.Height, this.Width, this.Depth, this.ToWorld);
                }
                return _worldRelativeOrientedBounds.Value;
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="size">the size of the item</param>
        /// <param name="transformToWorld">world relative transform matrix</param>
        public BoundaryInfo(Single h, Single w, Single d, MatrixValue transformToWorld)
        {
            this.Height = h;
            this.Width = w;
            this.Depth = d;
            this.ToWorld = transformToWorld;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns true if the boundary has any rotation applied.
        /// </summary>
        public Boolean HasWorldRotation()
        {
            //Compare each 

            //X
            if (this.ToWorld.M11 != 1) return true;
            if (this.ToWorld.M12 != 0) return true;
            if (this.ToWorld.M13 != 0) return true;

            //Y
            if (this.ToWorld.M21 != 0) return true;
            if (this.ToWorld.M22 != 1) return true;
            if (this.ToWorld.M23 != 0) return true;

            //Z
            if (this.ToWorld.M31 != 0) return true;
            if (this.ToWorld.M32 != 0) return true;
            if (this.ToWorld.M33 != 1) return true;

            return false;
        }

        /// <summary>
        /// Returns true if this boundary collides with another.
        /// </summary>
        /// <param name="other">Boundry to compare against</param>
        /// <returns>True if boundaries collide.</returns>
        public Boolean CollidesWith(BoundaryInfo other)
        {
            if (other == null) return false;

            //check axis aligned bounding boxes
            if (!other.WorldRelativeBounds.CollidesWith(this.WorldRelativeBounds)) return false;

            //only check oriented bounds if either of them actually has a rotation.
            if (other.HasWorldRotation() || this.HasWorldRotation())
            {
                return other.WorldRelativeOrientedBounds.Value.CollidesWith(this.WorldRelativeOrientedBounds.Value);
            }

            return true;
        }

        #endregion
    }

}
