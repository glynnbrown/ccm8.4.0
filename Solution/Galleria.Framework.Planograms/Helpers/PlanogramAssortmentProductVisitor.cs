﻿using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Helpers
{
    public class PlanogramAssortmentProductVisitor
    {
        public interface IPlanAssortmentData
        {
            PlanogramPositionList PositionsInPlanogram { get; }
            PlanogramAssortmentInventoryRuleList AssortmentInventoryRules { get; }
            PlanogramAssortmentLocalProductList PlanogramLocalProducts { get; }
            PlanogramAssortmentProductBuddyList PlanogramProductBuddies { get; }
            PlanogramAssortmentRegionList PlanogramRegions { get; }
        }

        public class PlanAssortmentData : IPlanAssortmentData
        {
            public PlanAssortmentData(Planogram planogram, PlanogramAssortment planogramAssortment)
            {
                PositionsInPlanogram = planogram.Positions;
                AssortmentInventoryRules = planogramAssortment.InventoryRules;
                PlanogramLocalProducts = planogramAssortment.LocalProducts;
                PlanogramProductBuddies = planogramAssortment.ProductBuddies;
                PlanogramRegions = planogramAssortment.Regions;
            }

            public PlanogramPositionList PositionsInPlanogram { get; }

            public PlanogramAssortmentInventoryRuleList AssortmentInventoryRules { get; }

            public PlanogramAssortmentLocalProductList PlanogramLocalProducts { get; }

            public PlanogramAssortmentProductBuddyList PlanogramProductBuddies { get; }

            public PlanogramAssortmentRegionList PlanogramRegions { get; }
        }

        public PlanogramAssortmentProductVisitor(IPlanAssortmentData assortmentData)
        {
            AssortmentData = assortmentData;
        }

        private IPlanAssortmentData AssortmentData { get; }

        public void UpdateRangedAssortmentProduct(PlanogramAssortmentProduct prod, List<String> processedGtins, Func<PlanogramPosition, Boolean> positionMatch)
        {
            //find any positions for this product in the planogram
            List<PlanogramPosition> positionsInPlan =
                AssortmentData.PositionsInPlanogram.Where(positionMatch).ToList();

            if (positionsInPlan.Any())
            {
                prod.IsRanged = true;
                prod.Facings = SafeConvertToByte(positionsInPlan.Sum(p => p.FacingsWide));
                prod.Units = SafeConvertToInt16(positionsInPlan.Sum(p => p.TotalUnits));
            }
            //else no products are on this plan
            else
            {
                //clear any rules for the product?
                prod.IsRanged = false;

                AssortmentData.AssortmentInventoryRules.RemoveByGtin(prod.Gtin);
                List<PlanogramAssortmentLocalProduct> localProducts =
                    AssortmentData.PlanogramLocalProducts.Where(p => p.ProductGtin == prod.Gtin).ToList();
                AssortmentData.PlanogramLocalProducts.RemoveList(localProducts);
                List<PlanogramAssortmentProductBuddy> productBuddies =
                    AssortmentData.PlanogramProductBuddies.Where(p => p.ProductGtin == prod.Gtin).ToList();
                AssortmentData.PlanogramProductBuddies.RemoveList(productBuddies);
                foreach (PlanogramAssortmentRegion region in AssortmentData.PlanogramRegions)
                {
                    List<PlanogramAssortmentRegionProduct> regionProducts =
                        new List<PlanogramAssortmentRegionProduct>();

                    regionProducts.AddRange(region.Products.Where(p => p.PrimaryProductGtin == prod.Gtin).ToList());
                    regionProducts.AddRange(region.Products.Where(p => p.RegionalProductGtin == prod.Gtin).ToList());

                    region.Products.RemoveList(regionProducts);
                }
            }

            processedGtins.Add(prod.Gtin);
        }

        public static byte SafeConvertToByte(Int32 value, byte safeValueToReturn = byte.MaxValue)
        {
            try
            {
                return Convert.ToByte(value);
            }
            catch (OverflowException)
            {
                return safeValueToReturn;
            }
        }

        public static Int16 SafeConvertToInt16(Int32 value, Int16 safeValueToReturn = Int16.MaxValue)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch (OverflowException)
            {
                return safeValueToReturn;
            }
        }
    }
}