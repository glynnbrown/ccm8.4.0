#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28676 : A.Silva
//      Created.
// V8-28679 : A.Silva
//      Added GFS RealImageProviderType.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Helpers
{
    public enum RealImageProviderType
    {
        Repository,
        Folder,
        Gfs
    }

    /// <summary>
    ///     Helper static class for <see cref="RealImageProviderType"/>.
    /// </summary>
    public static class RealImageProviderTypeHelper
    {
        /// <summary>
        ///     The matching friendly names for the values in the Enum <see cref="RealImageProviderType"/>.
        /// </summary>
        public static readonly Dictionary<RealImageProviderType, String> FriendlyNames = new Dictionary<RealImageProviderType, String>
        {
            {RealImageProviderType.Repository, Message.RealImageProviderType_Repository},
            {RealImageProviderType.Folder, Message.RealImageProviderType_Folder},
            {RealImageProviderType.Gfs, Message.RealImageProviderType_Gfs}
        };

        /// <summary>
        ///     The matching friendly descriptions for the values in the Enum <see cref="RealImageProviderType"/>.
        /// </summary>
        public static readonly Dictionary<RealImageProviderType, String> FriendlyDescriptions = new Dictionary<RealImageProviderType, String>
        {
            {RealImageProviderType.Repository, Message.RealImageProviderType_Repository_Desc},
            {RealImageProviderType.Folder, Message.RealImageProviderType_Folder_Desc},
            {RealImageProviderType.Gfs, Message.RealImageProviderType_Gfs_Desc}
        };
    }
}