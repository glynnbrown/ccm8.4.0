﻿// Copyright © Galleria RTS Ltd 2016

using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using System;
using System.Collections.Generic;


namespace Galleria.Framework.Planograms.Helpers
{
    /// <summary>
    /// Contains extensions to be included with this dll.
    /// </summary>
    public static class Extensions
    {

        /// <summary>
        /// Safer alternative to the Enumerable ToDictionary extension.
        /// This won't throw an exception if the key exists multiple times 
        /// - instead it simply works as last one wins.
        /// </summary>
        public static Dictionary<TKey, TSource> ToLookupDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            Dictionary<TKey, TSource> dict = new Dictionary<TKey, TSource>();

            foreach (TSource item in source)
            {
                dict[keySelector(item)] = item;
            }

            return dict;
        }

        /// <summary>
        /// Safer alternative to the Enumerable ToDictionary extension.
        /// This won't throw an exception if the key exists multiple times 
        /// - instead it simply works as last one wins.
        /// </summary>
        public static Dictionary<TKey, TElement> ToLookupDictionary<TSource, TKey, TElement>(
            this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> valueSelector)
        {
            Dictionary<TKey, TElement> dict = new Dictionary<TKey, TElement>();

            foreach (TSource item in source)
            {
                dict[keySelector(item)] = valueSelector(item);
            }

            return dict;
        }



        /// <summary>
        ///     Returns whether the <see cref="RectValue"/> <paramref name="a"/>
        /// contains the <see cref="RectValue"/> <paramref name="b"/>.
        /// </summary>
        /// <param name="a">The <see cref="RectValue"/> that is containing.</param>
        /// <param name="b">the <see cref="RectValue"/> that is being checked as contained.</param>
        /// <returns>True if the second <see cref="RectValue"/> is contained in the first.</returns>
        /// <remarks>Should be moved to RectValue.</remarks>
        public static Boolean Contains(this RectValue a, RectValue b)
        {
            return (!(a.X.GreaterThan(b.X))) &&
                   (!(a.Y.GreaterThan(b.Y))) &&
                   (!(a.Z.GreaterThan(b.Z))) &&
                   (!((a.X + a.Width).LessThan(b.X + b.Width))) &&
                   (!((a.Y + a.Height).LessThan(b.Y + b.Height))) &&
                   (!((a.Z + a.Depth).LessThan(b.Z + b.Depth)));
        }
    }
}
