﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.1.0
// V8-28242 : M.Shelley
//  Allow update of planogram attributes
//  Currently these are the DateApproved, DateArchived, DateWip and Status values
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30059 : L.Ineson
//  Added isset properties.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramAttributes Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramAttributesDto
    {
        #region Properties

        public Object PlanogramId { get; set; }

        public String CategoryCode { get; set; }
        public Boolean CategoryCodeIsSet { get; set; }
        public String CategoryName { get; set; }
        public Boolean CategoryNameIsSet { get; set; }
        public String LocationCode { get; set; }
        public Boolean LocationCodeIsSet { get; set; }
        public String LocationName { get; set; }
        public Boolean LocationNameIsSet { get; set; }
        public String ClusterSchemeName { get; set; }
        public Boolean ClusterSchemeNameIsSet { get; set; }
        public String ClusterName { get; set; }
        public Boolean ClusterNameIsSet { get; set; }
        public DateTime? DateApproved { get; set; }
        public Boolean DateApprovedIsSet { get; set; }
        public DateTime? DateArchived { get; set; }
        public Boolean DateArchivedIsSet { get; set; }
        public DateTime? DateWip { get; set; }
        public Boolean DateWipIsSet { get; set; }
        public PlanogramStatusType Status { get; set; }
        public Boolean StatusIsSet { get; set; }
        public PlanogramType PlanogramType { get; set; }
        public Boolean PlanogramTypeIsSet { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.PlanogramId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramAttributesDto other = obj as PlanogramAttributesDto;
            if (other != null)
            {
                if ((other.PlanogramId != null) && (this.PlanogramId != null))
                {
                    if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
                }
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }

                if (other.CategoryCode != this.CategoryCode) { return false; }
                if (other.CategoryCodeIsSet != this.CategoryCodeIsSet) { return false; }

                if (other.CategoryName != this.CategoryName) { return false; }
                if (other.CategoryNameIsSet != this.CategoryNameIsSet) { return false; }

                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.LocationCodeIsSet != this.LocationCodeIsSet) { return false; }

                if (other.LocationName != this.LocationName) { return false; }
                if (other.LocationNameIsSet != this.LocationNameIsSet) { return false; }

                if (other.ClusterSchemeName != this.ClusterSchemeName) { return false; }
                if (other.ClusterSchemeNameIsSet != this.ClusterSchemeNameIsSet) { return false; }

                if (other.ClusterName != this.ClusterName) { return false; }
                if (other.ClusterNameIsSet != this.ClusterNameIsSet) { return false; }

                if (other.DateApproved != this.DateApproved) { return false; }
                if (other.DateApprovedIsSet != this.DateApprovedIsSet) { return false; }

                if (other.DateArchived != this.DateArchived) { return false; }
                if (other.DateArchivedIsSet != this.DateArchivedIsSet) { return false; }

                if (other.DateWip != this.DateWip) { return false; }
                if (other.DateWipIsSet != this.DateWipIsSet) { return false; }

                if (other.Status != this.Status) { return false; }
                if (other.StatusIsSet != this.StatusIsSet) { return false; }

                if (other.PlanogramType != this.PlanogramType) { return false; }
                if (other.PlanogramTypeIsSet != this.PlanogramTypeIsSet) { return false; }
            }
            else
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
