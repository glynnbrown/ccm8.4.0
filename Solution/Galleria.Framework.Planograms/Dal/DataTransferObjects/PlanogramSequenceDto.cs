﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     Data Transfer Object for PlanogramSequence.
    /// </summary>
    [Serializable]
    public class PlanogramSequenceDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramSequenceDtoKey DtoKey
        {
            get { return new PlanogramSequenceDtoKey {PlanogramId = PlanogramId}; }
        }

        [ForeignKey(typeof (PlanogramDto), typeof (IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }

        public Object ExtendedData { get; set; }

        #endregion

        #region Equality Members

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            PlanogramSequenceDto other = obj as PlanogramSequenceDto;
            return other != null 
                && Equals(other.Id, Id) 
                && Equals(other.PlanogramId, PlanogramId) 
                && Equals(other.ExtendedData, ExtendedData);
        }

        #endregion
    }

    public class PlanogramSequenceDtoKey
    {
        #region Properties

        public Object PlanogramId { get; set; }

        #endregion

        #region Equality Members

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return PlanogramId.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            PlanogramSequenceDtoKey other = obj as PlanogramSequenceDtoKey;
            return other != null 
                && Equals(other.PlanogramId, PlanogramId);
        }

        #endregion
    }
}