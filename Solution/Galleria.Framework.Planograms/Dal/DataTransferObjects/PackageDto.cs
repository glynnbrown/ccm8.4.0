﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// V8-24290 : N.Foster
//  Created
// V8-24290 : A. Kuszyk
// Ongoing work toward an initial planogram structure that can be saved.
// V8-25916 : N.Foster
//  Added RowVersion, DateDeleted. Removed CreatedBy, LastModifiedBy, Description
// V8-25881 : A.Probyn
//  Replaced PlanogramCount with MetaPlanogramCount
//  Added DateMetadataCalculated
// V8-27237 : A.Silva   ~ Added DateValidationCalculated
// V8-27411 : M.Pettit
//  Added UserName
#endregion
#region Version History: CCM802
// V8-28840 : L.Luong
//  Added EntityId
#endregion
#region Version History: CCM830
// V8-31546 : M.Pettit
//  Added FieldMappings property, to be used for file-based Dals only
#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PackageDto
    {
        #region Properties
        public Object Id { get; set; }
        public PackageDtoKey DtoKey
        {
            get { return new PackageDtoKey() { Id = this.Id }; }
        }
        public RowVersion RowVersion { get; set; }
        public Object EntityId { get; set; }
        public String Name { get; set; }
        public String UserName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        public Object ExtendedData { get; set; }
        public Int32? MetaPlanogramCount { get; set; }
        public DateTime? DateMetadataCalculated { get; set; }
        public DateTime? DateValidationDataCalculated { get; set; }
        public Object FieldMappings { get; set; }   // field mappings are used by file system dals only and will be ignored by other dal types
        #endregion

        #region Methods
        public override bool Equals(Object obj)
        {
            PackageDto other = obj as PackageDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                
                // EntityId
                if ((other.EntityId != null) && (this.EntityId != null))
                {
                    if (!other.EntityId.Equals(this.EntityId)) { return false; }
                }
                if ((other.EntityId != null) && (this.EntityId == null)) { return false; }
                if ((other.EntityId == null) && (this.EntityId != null)) { return false; }

                if (other.RowVersion != this.RowVersion) return false;
                if (other.Name != this.Name) return false;
                if (other.UserName != this.UserName) return false;
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.DateDeleted != this.DateDeleted) return false;
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
                if (other.MetaPlanogramCount != this.MetaPlanogramCount) return false;
                if (other.DateMetadataCalculated != this.DateMetadataCalculated) return false;
                if (other.DateValidationDataCalculated != this.DateValidationDataCalculated) return false;
                //FieldMappings
                if ((other.FieldMappings != null) && (this.FieldMappings != null))
                {
                    if (!other.FieldMappings.Equals(this.FieldMappings)) { return false; }
                }
                if ((other.FieldMappings != null) && (this.FieldMappings == null)) { return false; }
                if ((other.FieldMappings == null) && (this.FieldMappings != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }

    public class PackageDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PackageDtoKey other = obj as PackageDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
