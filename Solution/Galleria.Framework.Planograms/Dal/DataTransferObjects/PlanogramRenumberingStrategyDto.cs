﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion
#region Version History: (CCM 8.2)
// V8-30193 : L.Ineson
//  Added IsEnabled.
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     Data transfer object for Planogram Renumbering Strategy.
    /// </summary>
    [Serializable]
    public class PlanogramRenumberingStrategyDto
    {
        #region Properties

        public PlanogramRenumberingStrategyDtoKey DtoKey
        {
            get { return new PlanogramRenumberingStrategyDtoKey { PlanogramId = PlanogramId }; }
        }

        public Object Id { get; set; }

        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }

        public String Name { get; set; }
        public Byte BayComponentXRenumberStrategyPriority { get; set; }
        public Byte BayComponentYRenumberStrategyPriority { get; set; }
        public Byte BayComponentZRenumberStrategyPriority { get; set; }
        public Byte PositionXRenumberStrategyPriority { get; set; }
        public Byte PositionYRenumberStrategyPriority { get; set; }
        public Byte PositionZRenumberStrategyPriority { get; set; }
        public Byte BayComponentXRenumberStrategy { get; set; }
        public Byte BayComponentYRenumberStrategy { get; set; }
        public Byte BayComponentZRenumberStrategy { get; set; }
        public Byte PositionXRenumberStrategy { get; set; }
        public Byte PositionYRenumberStrategy { get; set; }
        public Byte PositionZRenumberStrategy { get; set; }
        public Boolean RestartComponentRenumberingPerBay { get; set; }
        public Boolean IgnoreNonMerchandisingComponents { get; set; }
        public Boolean RestartPositionRenumberingPerComponent { get; set; }
        public Boolean UniqueNumberMultiPositionProductsPerComponent { get; set; }
        public Boolean ExceptAdjacentPositions { get; set; }
        public Boolean IsEnabled { get; set; }
        public Object ExtendedData { get; set; }
        public Boolean RestartPositionRenumberingPerBay { get; set; }
        public Boolean RestartComponentRenumberingPerComponentType { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(object obj)
        {
            PlanogramRenumberingStrategyDto other = obj as PlanogramRenumberingStrategyDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                // PlanogramId
                if ((other.PlanogramId != null) && (this.PlanogramId != null))
                {
                    if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
                }
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }


                if (other.Name != this.Name) return false;
                if (other.BayComponentXRenumberStrategyPriority != this.BayComponentXRenumberStrategyPriority) return false;
                if (other.BayComponentYRenumberStrategyPriority != this.BayComponentYRenumberStrategyPriority) return false;
                if (other.BayComponentZRenumberStrategyPriority != this.BayComponentZRenumberStrategyPriority) return false;
                if (other.PositionXRenumberStrategyPriority != this.PositionXRenumberStrategyPriority) return false;
                if (other.PositionYRenumberStrategyPriority != this.PositionYRenumberStrategyPriority) return false;
                if (other.PositionZRenumberStrategyPriority != this.PositionZRenumberStrategyPriority) return false;
                if (other.BayComponentXRenumberStrategy != this.BayComponentXRenumberStrategy) return false;
                if (other.BayComponentYRenumberStrategy != this.BayComponentYRenumberStrategy) return false;
                if (other.BayComponentZRenumberStrategy != this.BayComponentZRenumberStrategy) return false;
                if (other.PositionXRenumberStrategy != this.PositionXRenumberStrategy) return false;
                if (other.PositionYRenumberStrategy != this.PositionYRenumberStrategy) return false;
                if (other.PositionZRenumberStrategy != this.PositionZRenumberStrategy) return false;
                if (other.RestartComponentRenumberingPerBay != this.RestartComponentRenumberingPerBay) return false;
                if (other.IgnoreNonMerchandisingComponents != this.IgnoreNonMerchandisingComponents) return false;
                if (other.RestartPositionRenumberingPerComponent != this.RestartPositionRenumberingPerComponent) return false;
                if (other.UniqueNumberMultiPositionProductsPerComponent != this.UniqueNumberMultiPositionProductsPerComponent) return false;
                if (other.ExceptAdjacentPositions != this.ExceptAdjacentPositions) return false;
                if (other.IsEnabled != this.IsEnabled) return false;
                if (other.RestartPositionRenumberingPerBay != this.RestartPositionRenumberingPerBay) return false;
                if (other.RestartComponentRenumberingPerComponentType != this.RestartComponentRenumberingPerComponentType) return false;


                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        #endregion
    }

    public sealed class PlanogramRenumberingStrategyDtoKey
    {
        #region Properties

        public Object PlanogramId { get; set; }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return PlanogramId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramRenumberingStrategyDtoKey;
            if (other == null) return false;

            // PlanogramId
            if ((other.PlanogramId != null) && (this.PlanogramId != null))
            {
                if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
            }
            if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
            if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }

            return true;
        }
        #endregion
    }
}
