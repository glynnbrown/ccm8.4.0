﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramAssortmentDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAssortmentDtoKey DtoKey
        { 
            get { return new PlanogramAssortmentDtoKey{ PlanogramId = this.PlanogramId } ; }
        }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        public String Name { get; set; }
        public Object ExtendedData { get; set; } 
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramId
            if ((other.PlanogramId != null) && (this.PlanogramId != null))
            {
                if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
            }
            if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
            if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            return this.Name == other.Name;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentDtoKey
    {
        #region Properties
        public Object PlanogramId { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return PlanogramId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentDtoKey;
            if (other == null) return false;

            // PlanogramId
            if ((other.PlanogramId != null) && (this.PlanogramId != null))
            {
                if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
            }
            if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
            if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }

            return true;
        }
        #endregion
    }
}
