﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31963 : A.Silva
//  Corrected Equals to compare Status too.

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     <c>Data Transfer Object</c> for a <c>Planogram Comparison Result</c> model object.
    /// </summary>
    [Serializable]
    public class PlanogramComparisonItemDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramComparisonItemDtoKey DtoKey
        {
            get
            {
                return new PlanogramComparisonItemDtoKey
                {
                    PlanogramComparisonResultId = PlanogramComparisonResultId,
                    ItemId = ItemId,
                    ItemType = ItemType
                };
            }
        }

        [ForeignKey(typeof(PlanogramComparisonResultDto), typeof(IPlanogramComparisonResultDal), DeleteBehavior.Cascade)]
        public Object PlanogramComparisonResultId { get; set; }

        public String ItemId { get; set; }

        public Byte ItemType { get; set; }

        public Byte Status { get; set; }

        public Object ExtendedData { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonItemDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.PlanogramComparisonResultId, PlanogramComparisonResultId) &&
                   Equals(other.ItemId, ItemId) &&
                   Equals(other.ItemType, ItemType) &&
                   Equals(other.Status, Status) &&
                   Equals(other.ExtendedData, ExtendedData);
        }

        #endregion
    }

    [Serializable]
    public class PlanogramComparisonItemDtoKey
    {
        #region Properties

        public Object PlanogramComparisonResultId { get; set; }

        public String ItemId { get; set; }

        public Byte ItemType { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return PlanogramComparisonResultId.GetHashCode() +
                   ItemId.GetHashCode() +
                   ItemType.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonItemDtoKey;
            return other != null &&
                   Equals(other.PlanogramComparisonResultId, PlanogramComparisonResultId) &&
                   Equals(other.ItemId, ItemId) &&
                   Equals(other.ItemType, ItemType);
        }

        #endregion
    }
}