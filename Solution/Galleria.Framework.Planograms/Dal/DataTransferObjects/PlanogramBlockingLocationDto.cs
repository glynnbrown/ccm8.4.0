﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramBlockingLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramBlockingLocationDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramBlockingLocationDtoKey DtoKey
        {
            get
            {
                return new PlanogramBlockingLocationDtoKey()
                {
                    Id = this.Id
                };
            }
        }


        [ForeignKey(typeof(PlanogramBlockingDto), typeof(IPlanogramBlockingDal), DeleteBehavior.Cascade)]
        public Object PlanogramBlockingId { get; set; }

        [ForeignKey(typeof(PlanogramBlockingGroupDto), typeof(IPlanogramBlockingGroupDal))]
        public Object PlanogramBlockingGroupId { get; set; }

        public Object PlanogramBlockingDividerTopId { get; set; }
        public Object PlanogramBlockingDividerBottomId { get; set; }
        public Object PlanogramBlockingDividerLeftId { get; set; }
        public Object PlanogramBlockingDividerRightId { get; set; }
        public Single SpacePercentage { get; set; }
        public Object ExtendedData { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramBlockingLocationDto other = obj as PlanogramBlockingLocationDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                // PlanogramBlockingId
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId != null))
                {
                    if (!other.PlanogramBlockingId.Equals(this.PlanogramBlockingId)) { return false; }
                }
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId == null)) { return false; }
                if ((other.PlanogramBlockingId == null) && (this.PlanogramBlockingId != null)) { return false; }

                // PlanogramBlockingGroupId
                if ((other.PlanogramBlockingGroupId != null) && (this.PlanogramBlockingGroupId != null))
                {
                    if (!other.PlanogramBlockingGroupId.Equals(this.PlanogramBlockingGroupId)) { return false; }
                }
                if ((other.PlanogramBlockingGroupId != null) && (this.PlanogramBlockingGroupId == null)) { return false; }
                if ((other.PlanogramBlockingGroupId == null) && (this.PlanogramBlockingGroupId != null)) { return false; }

                // PlanogramBlockingDividerTopId
                if ((other.PlanogramBlockingDividerTopId != null) && (this.PlanogramBlockingDividerTopId != null))
                {
                    if (!other.PlanogramBlockingDividerTopId.Equals(this.PlanogramBlockingDividerTopId)) { return false; }
                }
                if ((other.PlanogramBlockingDividerTopId != null) && (this.PlanogramBlockingDividerTopId == null)) { return false; }
                if ((other.PlanogramBlockingDividerTopId == null) && (this.PlanogramBlockingDividerTopId != null)) { return false; }

                // PlanogramBlockingDividerBottomId
                if ((other.PlanogramBlockingDividerBottomId != null) && (this.PlanogramBlockingDividerBottomId != null))
                {
                    if (!other.PlanogramBlockingDividerBottomId.Equals(this.PlanogramBlockingDividerBottomId)) { return false; }
                }
                if ((other.PlanogramBlockingDividerBottomId != null) && (this.PlanogramBlockingDividerBottomId == null)) { return false; }
                if ((other.PlanogramBlockingDividerBottomId == null) && (this.PlanogramBlockingDividerBottomId != null)) { return false; }

                // PlanogramBlockingDividerLeftId
                if ((other.PlanogramBlockingDividerLeftId != null) && (this.PlanogramBlockingDividerLeftId != null))
                {
                    if (!other.PlanogramBlockingDividerLeftId.Equals(this.PlanogramBlockingDividerLeftId)) { return false; }
                }
                if ((other.PlanogramBlockingDividerLeftId != null) && (this.PlanogramBlockingDividerLeftId == null)) { return false; }
                if ((other.PlanogramBlockingDividerLeftId == null) && (this.PlanogramBlockingDividerLeftId != null)) { return false; }

                // PlanogramBlockingDividerRightId
                if ((other.PlanogramBlockingDividerRightId != null) && (this.PlanogramBlockingDividerRightId != null))
                {
                    if (!other.PlanogramBlockingDividerRightId.Equals(this.PlanogramBlockingDividerRightId)) { return false; }
                }
                if ((other.PlanogramBlockingDividerRightId != null) && (this.PlanogramBlockingDividerRightId == null)) { return false; }
                if ((other.PlanogramBlockingDividerRightId == null) && (this.PlanogramBlockingDividerRightId != null)) { return false; }

                if (other.SpacePercentage != this.SpacePercentage) { return false; }

                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramBlockingLocationDtoKey
    {

        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramBlockingLocationDtoKey other = obj as PlanogramBlockingLocationDtoKey;
            if (other != null)
            {

                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
