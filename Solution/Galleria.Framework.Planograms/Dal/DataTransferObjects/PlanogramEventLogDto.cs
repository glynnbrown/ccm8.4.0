﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)
// V8-27404 : L.Luong
//   Changed LevelType to EntryType and added Content
// V8-27759 : A.Kuszyk
//  Added AffectedId and AffectedType fields.
// V8-28059 : A.Kuszyk
//  Added WorkpackageSource, TaskSource and EventId fields.
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new Score property
#endregion

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Corrected DtoKey as the ID is what makes a record unique, not planogram id
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     PlanogramEventLog Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramEventLogDto
    {
        #region Properties

        public Object Id { get; set; }
        public PlanogramEventLogDtoKey DtoKey
        {
            get { return new PlanogramEventLogDtoKey { Id = this.Id }; }
        }

        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        public Byte EventType { get; set; }
        public Byte EntryType { get; set; }
        public Byte? AffectedType { get; set; }
        public Object AffectedId { get; set; }
        public String WorkpackageSource { get; set; }
        public String TaskSource { get; set; }
        public Int32 EventId { get; set; }
        public DateTime DateTime { get; set; }
        public String Description { get; set; }
        public String Content { get; set; }
        public Object ExtendedData { get; set; }
        public Byte Score { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramEventLogDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }

            // PlanogramId
            if ((other.PlanogramId != null) && (this.PlanogramId != null))
            {
                if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
            }
            if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
            if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }

            // AffectedId
            if ((other.AffectedId != null) && (this.AffectedId != null))
            {
                if (!other.AffectedId.Equals(this.AffectedId)) { return false; }
            }
            if ((other.AffectedId != null) && (this.AffectedId == null)) { return false; }
            if ((other.AffectedId == null) && (this.AffectedId != null)) { return false; }

            // AffectedType
            if ((other.AffectedType != null) && (this.AffectedType != null))
            {
                if (!other.AffectedType.Equals(this.AffectedType)) { return false; }
            }
            if ((other.AffectedType != null) && (this.AffectedType == null)) { return false; }
            if ((other.AffectedType == null) && (this.AffectedType != null)) { return false; }

            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }

            // ExtendedData
            if ((other.DateTime != null) && (this.DateTime != null))
            {
                if (!other.DateTime.Equals(this.DateTime)) { return false; }
            }
            if ((other.DateTime != null) && (this.DateTime == null)) { return false; }
            if ((other.DateTime == null) && (this.DateTime != null)) { return false; }

            //Score
            if ((other.Score != null) && (this.Score != null))
            {
                if (!other.Score.Equals(this.Score)) { return false; }
            }
            if ((other.Score != null) && (this.Score == null)) { return false; }
            if ((other.Score == null) && (this.Score != null)) { return false; }


            return this.EventType == other.EventType
                && this.EntryType == other.EntryType
                && this.WorkpackageSource == other.WorkpackageSource
                && this.TaskSource == other.TaskSource
                && this.EventId == other.EventId
                && this.Description == other.Description
                && this.Content == other.Content;
        }
        #endregion
    }

    public class PlanogramEventLogDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramEventLogDtoKey;
            if (other == null) return false;

            // PlanogramId
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }

            return true;
        }
        #endregion
    }
}
