﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentInventoryRule Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramAssortmentInventoryRuleDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAssortmentInventoryRuleDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentInventoryRuleDtoKey()
                {
                    ProductGtin = this.ProductGtin,
                    PlanogramAssortmentId = this.PlanogramAssortmentId
                };
            }
        }
        [ForeignKey(typeof(PlanogramAssortmentDto), typeof(IPlanogramAssortmentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentId { get; set; }
        public String ProductGtin { get; set; }
        public Single CasePack { get; set; }
        public Single DaysOfSupply { get; set; }
        public Single ShelfLife { get; set; }
        public Single ReplenishmentDays { get; set; }
        public Single WasteHurdleUnits { get; set; }
        public Single WasteHurdleCasePack { get; set; }
        public Int32 MinUnits { get; set; }
        public Int32 MinFacings { get; set; }

        public Object ExtendedData { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramAssortmentInventoryRuleDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }

            if (other.ProductGtin != this.ProductGtin) { return false; }
            if (other.CasePack != this.CasePack) { return false; }
            if (other.DaysOfSupply != this.DaysOfSupply) { return false; }
            if (other.ShelfLife != this.ShelfLife) { return false; }
            if (other.ReplenishmentDays != this.ReplenishmentDays) { return false; }
            if (other.WasteHurdleUnits != this.WasteHurdleUnits) { return false; }
            if (other.WasteHurdleCasePack != this.WasteHurdleCasePack) { return false; }
            if (other.MinUnits != this.MinUnits) { return false; }
            if (other.MinFacings != this.MinFacings) { return false; }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentInventoryRuleDtoKey
    {
        #region Properties
        public String ProductGtin { get; set; }
        public Object PlanogramAssortmentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ProductGtin.GetHashCode() +
                PlanogramAssortmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramAssortmentInventoryRuleDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }

            return this.ProductGtin == other.ProductGtin;
        }
        #endregion
    }
}
    

