﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramSequenceGroupSubGroupDto
    {
        #region Properties

        public Object Id { get; set; }
        public PlanogramSequenceGroupSubGroupDtoKey DtoKey
        {
            get
            {
                return new PlanogramSequenceGroupSubGroupDtoKey 
                { 
                    PlanogramSequenceGroupId = PlanogramSequenceGroupId,
                    Name = Name,
                };
            }
        }
        [ForeignKey(typeof(PlanogramSequenceGroupDto), typeof(IPlanogramSequenceGroupDal), DeleteBehavior.Cascade)]
        public Object PlanogramSequenceGroupId { get; set; }
        public Byte Alignment { get; set; }
        public String Name { get; set; }
        public Object ExtendedData { get; set; }

        #endregion

        #region Methods
        public override bool Equals(Object obj)
        {
            PlanogramSequenceGroupSubGroupDto other = obj as PlanogramSequenceGroupSubGroupDto;
            return
                other != null &&
                Object.Equals(this.Id, other.Id) &&
                Object.Equals(this.Alignment, other.Alignment) &&
                Object.Equals(this.Name, other.Name) &&
                Object.Equals(ExtendedData, other.ExtendedData) &&
                Object.Equals(this.PlanogramSequenceGroupId, other.PlanogramSequenceGroupId);
        }

        public override int GetHashCode()
        {
            return HashCode.Start.Hash(PlanogramSequenceGroupId).Hash(Alignment).Hash(Name);
        }
        #endregion
    }

    public class PlanogramSequenceGroupSubGroupDtoKey
    {
        #region Properties
        public Object PlanogramSequenceGroupId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        public override bool Equals(object obj)
        {
            PlanogramSequenceGroupSubGroupDtoKey other = obj as PlanogramSequenceGroupSubGroupDtoKey;
            return
                other != null &&
                Object.Equals(this.PlanogramSequenceGroupId, other.PlanogramSequenceGroupId) &&
                Object.Equals(this.Name, other.Name);
        }

        public override int GetHashCode()
        {
            return HashCode.Start.Hash(PlanogramSequenceGroupId).Hash(Name);
        }
        #endregion
    }
}
