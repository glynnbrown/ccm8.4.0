﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26954 : A.Silva ~ Added ResultType Property.
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     PlanogramValidationTemplateGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramValidationTemplateGroupDto
    {
        #region Properties

        public PlanogramValidationTemplateGroupDtoKey DtoKey
        {
            get
            {
                return new PlanogramValidationTemplateGroupDtoKey
                {
                    PlanogramValidationTemplateId = PlanogramValidationTemplateId,
                    Name = Name
                };
            }
        }
        public Object ExtendedData { get; set; }
        public Object Id { get; set; }
        public String Name { get; set; }

        [ForeignKey(typeof (PlanogramValidationTemplateDto), typeof (IPlanogramValidationTemplateDal),
            DeleteBehavior.Cascade)]
        public Object PlanogramValidationTemplateId { get; set; }

        /// <summary>
        ///     Gets or sets the validation result type for this group.
        /// </summary>
        public Byte ResultType { get; set; }

        public Single Threshold1 { get; set; }
        public Single Threshold2 { get; set; }

        /// <summary>
        ///     Gets or sets the PlanogramValidationType for this instance.
        /// </summary>
        public Byte ValidationType { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(object obj)
        {
            var other = obj as PlanogramValidationTemplateGroupDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (Id != null))
            {
                if (!other.Id.Equals(Id))
                {
                    return false;
                }
            }
            if ((other.Id != null) && (Id == null))
            {
                return false;
            }
            if ((other.Id == null) && (Id != null))
            {
                return false;
            }

            // PlanogramValidationTemplateId
            if ((other.PlanogramValidationTemplateId != null) && (PlanogramValidationTemplateId != null))
            {
                if (!other.PlanogramValidationTemplateId.Equals(PlanogramValidationTemplateId))
                {
                    return false;
                }
            }
            if ((other.PlanogramValidationTemplateId != null) && (PlanogramValidationTemplateId == null))
            {
                return false;
            }
            if ((other.PlanogramValidationTemplateId == null) && (PlanogramValidationTemplateId != null))
            {
                return false;
            }

            // ExtendedData
            if ((other.ExtendedData != null) && (ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(ExtendedData))
                {
                    return false;
                }
            }
            if ((other.ExtendedData != null) && (ExtendedData == null))
            {
                return false;
            }
            if ((other.ExtendedData == null) && (ExtendedData != null))
            {
                return false;
            }

            if (other.Name != Name)
            {
                return false;
            }
            if (other.Threshold1 != Threshold1)
            {
                return false;
            }
            if (other.Threshold2 != Threshold2)
            {
                return false;
            }
            if (other.ResultType != ResultType)
            {
                return false;
            }
            if (other.ValidationType != ValidationType)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        #endregion
    }

    public class PlanogramValidationTemplateGroupDtoKey
    {
        #region Properties

        public String Name { get; set; }
        public Object PlanogramValidationTemplateId { get; set; }

        #endregion

        #region Methods

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramValidationTemplateGroupDtoKey;
            if (other == null) return false;

            // PlanogramValidationTemplateId
            if ((other.PlanogramValidationTemplateId != null) && (PlanogramValidationTemplateId != null))
            {
                if (!other.PlanogramValidationTemplateId.Equals(PlanogramValidationTemplateId))
                {
                    return false;
                }
            }
            if ((other.PlanogramValidationTemplateId != null) && (PlanogramValidationTemplateId == null))
            {
                return false;
            }
            if ((other.PlanogramValidationTemplateId == null) && (PlanogramValidationTemplateId != null))
            {
                return false;
            }

            return Name == other.Name;
        }

        public override int GetHashCode()
        {
            return PlanogramValidationTemplateId.GetHashCode() + Name.GetHashCode();
        }

        #endregion
    }
}