﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24971 : L.Hodson
//  Changes to structure.
// V8-25477 : A.Kuszyk
//  Added ForeignKey attributes to properties.
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27474 : A.Silva
//      Added PositionSequenceNumber.
// V8-27823 : L.Ineson
//  Facings properties are now Int16
#endregion

#region Version History: CCM802
// V8-28766 : J.Pickup
//  IsManuallyPlaced property introduced.
// V8-29054 : L.Ineson
//  Added squeeze properties.
// V8-29054 : M.Pettit
//  Added MetaIsUnderMinDeep, MetaIsOverMaxDeep, MetaIsOverMaxRightCap,
//      MetaIsOverMaxStack, MetaIsOverMaxTopCap, MetaIsInvalidMerchandisingStyle, 
//      MetaIsHangingTray, MetaIsPegOverfilled, MetaIsOutsideMerchandisingSpace
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
// V8-30011 : L.Ineson
//  Added addition linear, area and volumetric metadata.
#endregion

#region Version History: CCM820
// V8-31164 : D.Pleasance
//  Added SequenceColour \ SequenceNumber
#endregion

#region Version History: CCM830
// V8-31947 : A.Silva
//  Added MetaComparisonStatus property.
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName & MetaSequenceGroupName.
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramPosition Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramPositionDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramPositionDtoKey DtoKey
        {
            get { return new PlanogramPositionDtoKey() { Id = this.Id }; }
        }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        [ForeignKey(typeof(PlanogramFixtureItemDto), typeof(IPlanogramFixtureItemDal), DeleteBehavior.Cascade)]
        public Object PlanogramFixtureItemId { get; set; }
        [ForeignKey(typeof(PlanogramFixtureAssemblyDto), typeof(IPlanogramFixtureAssemblyDal), DeleteBehavior.Cascade)]
        public Object PlanogramFixtureAssemblyId { get; set; }
        [ForeignKey(typeof(PlanogramAssemblyComponentDto), typeof(IPlanogramAssemblyComponentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssemblyComponentId { get; set; }
        [ForeignKey(typeof(PlanogramFixtureComponentDto), typeof(IPlanogramFixtureComponentDal), DeleteBehavior.Cascade)]
        public Object PlanogramFixtureComponentId { get; set; }
        [ForeignKey(typeof(PlanogramSubComponentDto), typeof(IPlanogramSubComponentDal), DeleteBehavior.Cascade)]
        public Object PlanogramSubComponentId { get; set; }
        [ForeignKey(typeof(PlanogramProductDto), typeof(IPlanogramProductDal), DeleteBehavior.Cascade)]
        public Object PlanogramProductId { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
        public Single Slope { get; set; }
        public Single Angle { get; set; }
        public Single Roll { get; set; }
        public Int16 FacingsHigh { get; set; }
        public Int16 FacingsWide { get; set; }
        public Int16 FacingsDeep { get; set; }
        public Byte OrientationType { get; set; }
        public Byte MerchandisingStyle { get; set; }
        public Int16 FacingsXHigh { get; set; }
        public Int16 FacingsXWide { get; set; }
        public Int16 FacingsXDeep { get; set; }
        public Byte MerchandisingStyleX { get; set; }
        public Byte OrientationTypeX { get; set; }
        public Boolean IsXPlacedLeft { get; set; }
        public Int16 FacingsYHigh { get; set; }
        public Int16 FacingsYWide { get; set; }
        public Int16 FacingsYDeep { get; set; }
        public Byte MerchandisingStyleY { get; set; }
        public Byte OrientationTypeY { get; set; }
        public Boolean IsYPlacedBottom { get; set; }
        public Int16 FacingsZHigh { get; set; }
        public Int16 FacingsZWide { get; set; }
        public Int16 FacingsZDeep { get; set; }
        public Byte MerchandisingStyleZ { get; set; }
        public Byte OrientationTypeZ { get; set; }
        public Boolean IsZPlacedFront { get; set; }
        public Int16 Sequence { get; set; }
        public Int16 SequenceX { get; set; }
        public Int16 SequenceY { get; set; }
        public Int16 SequenceZ { get; set; }
        public Int16 UnitsHigh { get; set; }
        public Int16 UnitsWide { get; set; }
        public Int16 UnitsDeep { get; set; }
        public Int32 TotalUnits { get; set; }
        public Object ExtendedData { get; set; }
        public Single? MetaAchievedCases { get; set; }
        public Boolean? MetaIsPositionCollisions { get; set; }
        public Int16? MetaFrontFacingsWide { get; set; }
        public Int16? PositionSequenceNumber { get; set; }
        public Boolean IsManuallyPlaced { get; set; }
        public Single HorizontalSqueeze { get; set; }
        public Single VerticalSqueeze { get; set; }
        public Single DepthSqueeze { get; set; }
        public Single HorizontalSqueezeX { get; set; }
        public Single VerticalSqueezeX { get; set; }
        public Single DepthSqueezeX { get; set; }
        public Single HorizontalSqueezeY { get; set; }
        public Single VerticalSqueezeY { get; set; }
        public Single DepthSqueezeY { get; set; }
        public Single HorizontalSqueezeZ { get; set; }
        public Single VerticalSqueezeZ { get; set; }
        public Single DepthSqueezeZ { get; set; }
        public Boolean? MetaIsUnderMinDeep { get; set; }
        public Boolean? MetaIsOverMaxDeep { get; set; }
        public Boolean? MetaIsOverMaxRightCap { get; set; }
        public Boolean? MetaIsOverMaxStack { get; set; }
        public Boolean? MetaIsOverMaxTopCap { get; set; }
        public Boolean? MetaIsInvalidMerchandisingStyle { get; set; }
        public Boolean? MetaIsHangingTray { get; set; }
        public Boolean? MetaIsPegOverfilled { get; set; }
        public Boolean? MetaIsOutsideMerchandisingSpace { get; set; }
        public Boolean? MetaIsOutsideOfBlockSpace { get; set; }
        public Single? MetaHeight { get; set; }
        public Single? MetaWidth { get; set; }
        public Single? MetaDepth { get; set; }
        public Single? MetaWorldX { get; set; }
        public Single? MetaWorldY { get; set; }
        public Single? MetaWorldZ { get; set; }
        public Single? MetaTotalLinearSpace { get; set; }
        public Single? MetaTotalAreaSpace { get; set; }
        public Single? MetaTotalVolumetricSpace { get; set; }
        public Single? MetaPlanogramLinearSpacePercentage { get; set; }
        public Single? MetaPlanogramAreaSpacePercentage { get; set; }
        public Single? MetaPlanogramVolumetricSpacePercentage { get; set; }
        public Int32? SequenceColour { get; set; }
        public Int32? SequenceNumber { get; set; }
        public Byte MetaComparisonStatus { get; set; }
        public String MetaSequenceSubGroupName { get; set; }
        public String MetaSequenceGroupName { get; set; }
        public Int32? MetaPegRowNumber { get; set; }
        public Int32? MetaPegColumnNumber { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramPositionDto other = obj as PlanogramPositionDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                // PlanogramId
                if ((other.PlanogramId != null) && (this.PlanogramId != null))
                {
                    if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
                }
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }
                // PlanogramFixtureItemId
                if ((other.PlanogramFixtureItemId != null) && (this.PlanogramFixtureItemId != null))
                {
                    if (!other.PlanogramFixtureItemId.Equals(this.PlanogramFixtureItemId)) { return false; }
                }
                if ((other.PlanogramFixtureItemId != null) && (this.PlanogramFixtureItemId == null)) { return false; }
                if ((other.PlanogramFixtureItemId == null) && (this.PlanogramFixtureItemId != null)) { return false; }
                // PlanogramFixtureAssemblyId
                if ((other.PlanogramFixtureAssemblyId != null) && (this.PlanogramFixtureAssemblyId != null))
                {
                    if (!other.PlanogramFixtureAssemblyId.Equals(this.PlanogramFixtureAssemblyId)) { return false; }
                }
                if ((other.PlanogramFixtureAssemblyId != null) && (this.PlanogramFixtureAssemblyId == null)) { return false; }
                if ((other.PlanogramFixtureAssemblyId == null) && (this.PlanogramFixtureAssemblyId != null)) { return false; }
                // PlanogramAssemblyComponentId
                if ((other.PlanogramAssemblyComponentId != null) && (this.PlanogramAssemblyComponentId != null))
                {
                    if (!other.PlanogramAssemblyComponentId.Equals(this.PlanogramAssemblyComponentId)) { return false; }
                }
                if ((other.PlanogramAssemblyComponentId != null) && (this.PlanogramAssemblyComponentId == null)) { return false; }
                if ((other.PlanogramAssemblyComponentId == null) && (this.PlanogramAssemblyComponentId != null)) { return false; }
                // PlanogramFixtureComponentId
                if ((other.PlanogramFixtureComponentId != null) && (this.PlanogramFixtureComponentId != null))
                {
                    if (!other.PlanogramFixtureComponentId.Equals(this.PlanogramFixtureComponentId)) { return false; }
                }
                if ((other.PlanogramFixtureComponentId != null) && (this.PlanogramFixtureComponentId == null)) { return false; }
                if ((other.PlanogramFixtureComponentId == null) && (this.PlanogramFixtureComponentId != null)) { return false; }
                // PlanogramSubComponentId
                if ((other.PlanogramSubComponentId != null) && (this.PlanogramSubComponentId != null))
                {
                    if (!other.PlanogramSubComponentId.Equals(this.PlanogramSubComponentId)) { return false; }
                }
                if ((other.PlanogramSubComponentId != null) && (this.PlanogramSubComponentId == null)) { return false; }
                if ((other.PlanogramSubComponentId == null) && (this.PlanogramSubComponentId != null)) { return false; }
                // PlanogramProductId
                if ((other.PlanogramProductId != null) && (this.PlanogramProductId != null))
                {
                    if (!other.PlanogramProductId.Equals(this.PlanogramProductId)) { return false; }
                }
                if ((other.PlanogramProductId != null) && (this.PlanogramProductId == null)) { return false; }
                if ((other.PlanogramProductId == null) && (this.PlanogramProductId != null)) { return false; }

                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Z != this.Z) { return false; }
                if (other.Slope != this.Slope) { return false; }
                if (other.Angle != this.Angle) { return false; }
                if (other.Roll != this.Roll) { return false; }
                if (other.FacingsHigh != this.FacingsHigh) { return false; }
                if (other.FacingsWide != this.FacingsWide) { return false; }
                if (other.FacingsDeep != this.FacingsDeep) { return false; }
                if (other.OrientationType != this.OrientationType) { return false; }
                if (other.MerchandisingStyle != this.MerchandisingStyle) { return false; }
                if (other.FacingsXHigh != this.FacingsXHigh) { return false; }
                if (other.FacingsXWide != this.FacingsXWide) { return false; }
                if (other.FacingsXDeep != this.FacingsXDeep) { return false; }
                if (other.MerchandisingStyleX != this.MerchandisingStyleX) { return false; }
                if (other.OrientationTypeX != this.OrientationTypeX) { return false; }
                if (other.IsXPlacedLeft != this.IsXPlacedLeft) { return false; }
                if (other.FacingsYHigh != this.FacingsYHigh) { return false; }
                if (other.FacingsYWide != this.FacingsYWide) { return false; }
                if (other.FacingsYDeep != this.FacingsYDeep) { return false; }
                if (other.MerchandisingStyleY != this.MerchandisingStyleY) { return false; }
                if (other.OrientationTypeY != this.OrientationTypeY) { return false; }
                if (other.IsYPlacedBottom != this.IsYPlacedBottom) { return false; }
                if (other.FacingsZHigh != this.FacingsZHigh) { return false; }
                if (other.FacingsZWide != this.FacingsZWide) { return false; }
                if (other.FacingsZDeep != this.FacingsZDeep) { return false; }
                if (other.MerchandisingStyleZ != this.MerchandisingStyleZ) { return false; }
                if (other.OrientationTypeZ != this.OrientationTypeZ) { return false; }
                if (other.IsZPlacedFront != this.IsZPlacedFront) { return false; }
                if (other.Sequence != this.Sequence) { return false; }
                if (other.SequenceX != this.SequenceX) { return false; }
                if (other.SequenceY != this.SequenceY) { return false; }
                if (other.SequenceZ != this.SequenceZ) { return false; }
                if (other.UnitsHigh != this.UnitsHigh) { return false; }
                if (other.UnitsWide != this.UnitsWide) { return false; }
                if (other.UnitsDeep != this.UnitsDeep) { return false; }
                if (other.TotalUnits != this.TotalUnits) { return false; }
                if (other.MetaAchievedCases != this.MetaAchievedCases) { return false; }
                if (other.MetaIsPositionCollisions != this.MetaIsPositionCollisions) { return false; }
                if (other.MetaFrontFacingsWide != this.MetaFrontFacingsWide) { return false; }
                if (other.PositionSequenceNumber != this.PositionSequenceNumber) { return false; }
                if (other.IsManuallyPlaced != this.IsManuallyPlaced) { return false; }
                if (other.HorizontalSqueeze != this.HorizontalSqueeze) { return false; }
                if (other.VerticalSqueeze != this.VerticalSqueeze) { return false; }
                if (other.DepthSqueeze != this.DepthSqueeze) { return false; }
                if(other.HorizontalSqueezeX != this.HorizontalSqueezeX) {return false;}
                if (other.VerticalSqueezeX != this.VerticalSqueezeX) { return false; }
                if (other.DepthSqueezeX != this.DepthSqueezeX) { return false; }
                if (other.HorizontalSqueezeY != this.HorizontalSqueezeY) { return false; }
                if (other.VerticalSqueezeY != this.VerticalSqueezeY) { return false; }
                if (other.DepthSqueezeY != this.DepthSqueezeY) { return false; }
                if (other.HorizontalSqueezeZ != this.HorizontalSqueezeZ) { return false; }
                if (other.VerticalSqueezeZ != this.VerticalSqueezeZ) { return false; }
                if (other.DepthSqueezeZ != this.DepthSqueezeZ) { return false; }
                if (other.MetaIsUnderMinDeep != this.MetaIsUnderMinDeep) { return false; }
                if (other.MetaIsOverMaxDeep != this.MetaIsOverMaxDeep) { return false; }
                if (other.MetaIsOverMaxRightCap != this.MetaIsOverMaxRightCap) { return false; }
                if (other.MetaIsOverMaxStack != this.MetaIsOverMaxStack) { return false; }
                if (other.MetaIsOverMaxTopCap != this.MetaIsOverMaxTopCap) { return false; }
                if (other.MetaIsInvalidMerchandisingStyle != this.MetaIsInvalidMerchandisingStyle) { return false; }
                if (other.MetaIsHangingTray != this.MetaIsHangingTray) { return false; }
                if (other.MetaIsPegOverfilled != this.MetaIsPegOverfilled) { return false; }
                if (other.MetaIsOutsideMerchandisingSpace != this.MetaIsOutsideMerchandisingSpace) { return false; }
                if (other.MetaIsOutsideOfBlockSpace != this.MetaIsOutsideOfBlockSpace) { return false; }
                if (other.MetaHeight != this.MetaHeight) { return false; }
                if (other.MetaWidth != this.MetaWidth) { return false; }
                if (other.MetaDepth != this.MetaDepth) { return false; }
                if (other.MetaWorldX != this.MetaWorldX) { return false; }
                if (other.MetaWorldY != this.MetaWorldY) { return false; }
                if (other.MetaWorldZ != this.MetaWorldZ) { return false; }
                if (other.MetaTotalLinearSpace != this.MetaTotalLinearSpace) { return false; }
                if (other.MetaTotalAreaSpace != this.MetaTotalAreaSpace) { return false; }
                if (other.MetaTotalVolumetricSpace != this.MetaTotalVolumetricSpace) { return false; }
                if (other.MetaPlanogramLinearSpacePercentage != this.MetaPlanogramLinearSpacePercentage) { return false; }
                if (other.MetaPlanogramAreaSpacePercentage != this.MetaPlanogramAreaSpacePercentage) { return false; }
                if (other.MetaPlanogramVolumetricSpacePercentage != this.MetaPlanogramVolumetricSpacePercentage) { return false; }
                if (other.SequenceColour != this.SequenceColour) { return false; }
                if (other.SequenceNumber != this.SequenceNumber) { return false; }
                if (other.MetaComparisonStatus != this.MetaComparisonStatus) { return false; }
                if (other.MetaSequenceSubGroupName != this.MetaSequenceSubGroupName) { return false; }
                if (other.MetaSequenceGroupName != this.MetaSequenceGroupName) { return false; }
                if(other.MetaPegRowNumber != this.MetaPegRowNumber) { return false; }
                if(other.MetaPegColumnNumber != this.MetaPegColumnNumber) { return false; }

                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramPositionDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramPositionDtoKey other = obj as PlanogramPositionDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
