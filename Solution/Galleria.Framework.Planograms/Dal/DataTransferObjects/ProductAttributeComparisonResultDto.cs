using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System;
using System.Data.SqlClient;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class ProductAttributeComparisonResultDto
    {
        #region Properties

        public Object Id { get; set; }

        public ProductAttributeComparisonResultDtoKey DtoKey { get { return new ProductAttributeComparisonResultDtoKey { PlanogramId = PlanogramId }; } }

        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }

        public String ProductGtin { get; set; }

        public String ComparedProductAttribute { get; set; }

        public String MasterDataValue { get; set; }

        public String ProductValue { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as ProductAttributeComparisonResultDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.PlanogramId, PlanogramId) &&
                   String.Equals(other.ProductGtin, ProductGtin) &&
                   String.Equals(other.ComparedProductAttribute, ComparedProductAttribute) &&
                   Equals(other.MasterDataValue, MasterDataValue) &&
                   Equals(other.ProductValue, ProductValue);
        }

        #endregion
    }

    [Serializable]
    public class ProductAttributeComparisonResultDtoKey
    {
        #region Properties

        public Object PlanogramId { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return PlanogramId.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as ProductAttributeComparisonResultDtoKey;
            return other != null &&
                   Equals(other.PlanogramId, PlanogramId);
        }

        #endregion
    }
}