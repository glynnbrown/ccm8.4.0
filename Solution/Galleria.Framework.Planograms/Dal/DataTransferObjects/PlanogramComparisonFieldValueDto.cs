﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     <c>Data Transfer Object</c> for a <c>Planogram Comparison Field Value</c> model object.
    /// </summary>
    [Serializable]
    public class PlanogramComparisonFieldValueDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramComparisonFieldValueDtoKey DtoKey
        {
            get
            {
                return new PlanogramComparisonFieldValueDtoKey
                {
                    PlanogramComparisonItemId = PlanogramComparisonItemId,
                    FieldPlaceholder = FieldPlaceholder
                };
            }
        }

        [ForeignKey(typeof(PlanogramComparisonItemDto), typeof(IPlanogramComparisonItemDal), DeleteBehavior.Cascade)]
        public Object PlanogramComparisonItemId { get; set; }

        public Object ExtendedData { get; set; }

        public String FieldPlaceholder { get; set; }

        public String Value { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonFieldValueDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.PlanogramComparisonItemId, PlanogramComparisonItemId) &&
                   Equals(other.ExtendedData, ExtendedData) &&
                   other.FieldPlaceholder == FieldPlaceholder &&
                   other.Value == Value;
        }

        #endregion
    }

    [Serializable]
    public class PlanogramComparisonFieldValueDtoKey
    {
        #region Properties

        public Object PlanogramComparisonItemId { get; set; }

        public String FieldPlaceholder { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return PlanogramComparisonItemId.GetHashCode() +
                   FieldPlaceholder.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonFieldValueDtoKey;
            return other != null &&
                   Equals(other.PlanogramComparisonItemId, PlanogramComparisonItemId) &&
                   other.FieldPlaceholder == FieldPlaceholder;
        }

        #endregion
    }
}