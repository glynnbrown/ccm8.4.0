﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
// V8-27476 : L.Luong
//      Added BlockPlacementXType and BlockPlacementYType
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#region Version History : CCM 820
// V8-30765 :I.George
//	Added PlanogramBlockingGroup metaData Properties
#endregion
#region Version History : CCM 830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramBlockingGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramBlockingGroupDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramBlockingGroupDtoKey DtoKey
        {
            get
            {
                return new PlanogramBlockingGroupDtoKey()
                {
                    PlanogramBlockingId = this.PlanogramBlockingId,
                    Name = this.Name
                };
            }
        }


        [ForeignKey(typeof(PlanogramBlockingDto), typeof(IPlanogramBlockingDal), DeleteBehavior.Cascade)]
        public Object PlanogramBlockingId { get; set; }

        public String Name { get; set; }
        public Int32 Colour { get; set; }
        public Byte FillPatternType { get; set; }
        public Boolean CanCompromiseSequence { get; set; }
        public Boolean IsRestrictedByComponentType { get; set; }
        public Boolean CanMerge { get; set; }
        public Boolean IsLimited { get; set; }
        public Single LimitedPercentage { get; set; }
        public Byte BlockPlacementPrimaryType { get; set; }
        public Byte BlockPlacementSecondaryType { get; set; }
        public Byte BlockPlacementTertiaryType { get; set; }
        public Single TotalSpacePercentage { get; set; }
        public Object ExtendedData { get; set; }
        public Int32? MetaCountOfProducts { get; set; }
        public Int32? MetaCountOfProductRecommendedOnPlanogram { get; set; }
        public Int32? MetaCountOfProductPlacedOnPlanogram { get; set; }
        public Single? MetaOriginalPercentAllocated { get; set; }
        public Single? MetaPerformancePercentAllocated { get; set; }
        public Single? MetaFinalPercentAllocated { get; set; }
        public Single? MetaLinearProductPlacementPercent { get; set; }
        public Single? MetaAreaProductPlacementPercent { get; set; }
        public Single? MetaVolumetricProductPlacementPercent { get; set; }

        public Single? P1 { get; set; }
        public Single? P2 { get; set; }
        public Single? P3 { get; set; }
        public Single? P4 { get; set; }
        public Single? P5 { get; set; }
        public Single? P6 { get; set; }
        public Single? P7 { get; set; }
        public Single? P8 { get; set; }
        public Single? P9 { get; set; }
        public Single? P10 { get; set; }
        public Single? P11 { get; set; }
        public Single? P12 { get; set; }
        public Single? P13 { get; set; }
        public Single? P14 { get; set; }
        public Single? P15 { get; set; }
        public Single? P16 { get; set; }
        public Single? P17 { get; set; }
        public Single? P18 { get; set; }
        public Single? P19 { get; set; }
        public Single? P20 { get; set; }

        public Single? MetaP1Percentage { get; set; }
        public Single? MetaP2Percentage { get; set; }
        public Single? MetaP3Percentage { get; set; }
        public Single? MetaP4Percentage { get; set; }
        public Single? MetaP5Percentage { get; set; }
        public Single? MetaP6Percentage { get; set; }
        public Single? MetaP7Percentage { get; set; }
        public Single? MetaP8Percentage { get; set; }
        public Single? MetaP9Percentage { get; set; }
        public Single? MetaP10Percentage { get; set; }
        public Single? MetaP11Percentage { get; set; }
        public Single? MetaP12Percentage { get; set; }
        public Single? MetaP13Percentage { get; set; }
        public Single? MetaP14Percentage { get; set; }
        public Single? MetaP15Percentage { get; set; }
        public Single? MetaP16Percentage { get; set; }
        public Single? MetaP17Percentage { get; set; }
        public Single? MetaP18Percentage { get; set; }
        public Single? MetaP19Percentage { get; set; }
        public Single? MetaP20Percentage { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramBlockingGroupDto other = obj as PlanogramBlockingGroupDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                // PlanogramId
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId != null))
                {
                    if (!other.PlanogramBlockingId.Equals(this.PlanogramBlockingId)) { return false; }
                }
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId == null)) { return false; }
                if ((other.PlanogramBlockingId == null) && (this.PlanogramBlockingId != null)) { return false; }

                //other
                if (other.Name != this.Name) { return false; }
                if (other.Colour != this.Colour) { return false; }
                if (other.FillPatternType != this.FillPatternType) { return false; }
                if (other.CanCompromiseSequence != this.CanCompromiseSequence) { return false; }
                if (other.IsRestrictedByComponentType != this.IsRestrictedByComponentType) { return false; }
                if (other.CanMerge != this.CanMerge) { return false; }
                if (other.IsLimited != this.IsLimited) { return false; }
                if (other.LimitedPercentage != this.LimitedPercentage) { return false; }
                if (other.BlockPlacementPrimaryType != this.BlockPlacementPrimaryType) { return false; }
                if (other.BlockPlacementSecondaryType != this.BlockPlacementSecondaryType) { return false; }
                if (other.BlockPlacementTertiaryType != this.BlockPlacementTertiaryType) { return false; }
                if (other.TotalSpacePercentage != this.TotalSpacePercentage) { return false; }

                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }

                //MetaData
                if (other.MetaCountOfProducts != null && this.MetaCountOfProducts != null)
                {
                    if (!other.MetaCountOfProducts.Equals(this.MetaCountOfProducts)) { return false; }
                }
                if (other.MetaCountOfProducts != null && this.MetaCountOfProducts == null) { return false; }
                if (other.MetaCountOfProducts == null && this.MetaCountOfProducts != null) { return false; }

                if (other.MetaCountOfProductRecommendedOnPlanogram != null && this.MetaCountOfProductRecommendedOnPlanogram != null)
                {
                    if (!other.MetaCountOfProductRecommendedOnPlanogram.Equals(this.MetaCountOfProductRecommendedOnPlanogram)) { return false; }
                }
                if (other.MetaCountOfProductRecommendedOnPlanogram != null && this.MetaCountOfProductRecommendedOnPlanogram == null) { return false; }
                if (other.MetaCountOfProductRecommendedOnPlanogram == null && this.MetaCountOfProductRecommendedOnPlanogram != null) { return false; }

                if (other.MetaCountOfProductPlacedOnPlanogram != null && this.MetaCountOfProductPlacedOnPlanogram != null)
                {
                    if (!other.MetaCountOfProductPlacedOnPlanogram.Equals(this.MetaCountOfProductPlacedOnPlanogram)) { return false; }
                }
                if (other.MetaCountOfProductPlacedOnPlanogram != null && this.MetaCountOfProductPlacedOnPlanogram == null) { return false; }
                if (other.MetaCountOfProductPlacedOnPlanogram == null && this.MetaCountOfProductPlacedOnPlanogram != null) { return false; }

                if (other.MetaOriginalPercentAllocated != null && this.MetaOriginalPercentAllocated != null)
                {
                    if (!other.MetaOriginalPercentAllocated.Equals(this.MetaOriginalPercentAllocated)) { return false; }
                }
                if (other.MetaOriginalPercentAllocated != null && this.MetaOriginalPercentAllocated == null) { return false; }
                if (other.MetaOriginalPercentAllocated == null && this.MetaOriginalPercentAllocated != null) { return false; }

                if (other.MetaPerformancePercentAllocated != null && this.MetaPerformancePercentAllocated != null)
                {
                    if (!other.MetaPerformancePercentAllocated.Equals(this.MetaPerformancePercentAllocated)) { return false; }
                }
                if (other.MetaPerformancePercentAllocated != null && this.MetaPerformancePercentAllocated == null) { return false; }
                if (other.MetaPerformancePercentAllocated == null && this.MetaPerformancePercentAllocated != null) { return false; }

                if (other.MetaFinalPercentAllocated != null && this.MetaFinalPercentAllocated != null)
                {
                    if (!other.MetaFinalPercentAllocated.Equals(this.MetaFinalPercentAllocated)) { return false; }
                }
                if (other.MetaFinalPercentAllocated != null && this.MetaFinalPercentAllocated == null) { return false; }
                if (other.MetaFinalPercentAllocated == null && this.MetaFinalPercentAllocated != null) { return false; }

                if (other.MetaLinearProductPlacementPercent != null && this.MetaLinearProductPlacementPercent != null)
                {
                    if (!other.MetaLinearProductPlacementPercent.Equals(this.MetaLinearProductPlacementPercent)) { return false; }
                }
                if (other.MetaLinearProductPlacementPercent != null && this.MetaLinearProductPlacementPercent == null) { return false; }
                if (other.MetaLinearProductPlacementPercent == null && this.MetaLinearProductPlacementPercent != null) { return false; }

                if (other.MetaAreaProductPlacementPercent != null && this.MetaAreaProductPlacementPercent != null)
                {
                    if (!other.MetaAreaProductPlacementPercent.Equals(this.MetaAreaProductPlacementPercent)) { return false; }
                }
                if (other.MetaAreaProductPlacementPercent != null && this.MetaAreaProductPlacementPercent == null) { return false; }
                if (other.MetaAreaProductPlacementPercent == null && this.MetaAreaProductPlacementPercent != null) { return false; }

                if (other.MetaVolumetricProductPlacementPercent != null && this.MetaVolumetricProductPlacementPercent != null)
                {
                    if (!other.MetaVolumetricProductPlacementPercent.Equals(this.MetaVolumetricProductPlacementPercent)) { return false; }
                }
                if (other.MetaVolumetricProductPlacementPercent != null && this.MetaVolumetricProductPlacementPercent == null) { return false; }
                if (other.MetaVolumetricProductPlacementPercent == null && this.MetaVolumetricProductPlacementPercent != null) { return false; }

                #region Meta data performance values

                if (other.P1 != null && this.P1 != null)
                {
                    if (!other.P1.Equals(this.P1)) { return false; }
                }
                if ((other.P1 != null) && (this.P1 == null)) { return false; }
                if ((other.P1 == null) && (this.P1 != null)) { return false; }

                if (other.P2 != null && this.P2 != null)
                {
                    if (!other.P2.Equals(this.P2)) { return false; }
                }
                if ((other.P2 != null) && (this.P2 == null)) { return false; }
                if ((other.P2 == null) && (this.P2 != null)) { return false; }

                if (other.P3 != null && this.P3 != null)
                {
                    if (!other.P3.Equals(this.P3)) { return false; }
                }
                if ((other.P3 != null) && (this.P3 == null)) { return false; }
                if ((other.P3 == null) && (this.P3 != null)) { return false; }

                if (other.P4 != null && this.P4 != null)
                {
                    if (!other.P4.Equals(this.P4)) { return false; }
                }
                if ((other.P4 != null) && (this.P4 == null)) { return false; }
                if ((other.P4 == null) && (this.P4 != null)) { return false; }

                if (other.P5 != null && this.P5 != null)
                {
                    if (!other.P5.Equals(this.P5)) { return false; }
                }
                if ((other.P5 != null) && (this.P5 == null)) { return false; }
                if ((other.P5 == null) && (this.P5 != null)) { return false; }

                if (other.P6 != null && this.P6 != null)
                {
                    if (!other.P6.Equals(this.P6)) { return false; }
                }
                if ((other.P6 != null) && (this.P6 == null)) { return false; }
                if ((other.P6 == null) && (this.P6 != null)) { return false; }

                if (other.P7 != null && this.P7 != null)
                {
                    if (!other.P7.Equals(this.P7)) { return false; }
                }
                if ((other.P7 != null) && (this.P7 == null)) { return false; }
                if ((other.P7 == null) && (this.P7 != null)) { return false; }

                if (other.P8 != null && this.P8 != null)
                {
                    if (!other.P8.Equals(this.P8)) { return false; }
                }
                if ((other.P8 != null) && (this.P8 == null)) { return false; }
                if ((other.P8 == null) && (this.P8 != null)) { return false; }

                if (other.P9 != null && this.P9 != null)
                {
                    if (!other.P9.Equals(this.P9)) { return false; }
                }
                if ((other.P9 != null) && (this.P9 == null)) { return false; }
                if ((other.P9 == null) && (this.P9 != null)) { return false; }

                if (other.P10 != null && this.P10 != null)
                {
                    if (!other.P10.Equals(this.P10)) { return false; }
                }
                if ((other.P10 != null) && (this.P10 == null)) { return false; }
                if ((other.P10 == null) && (this.P10 != null)) { return false; }

                if (other.P11 != null && this.P11 != null)
                {
                    if (!other.P11.Equals(this.P11)) { return false; }
                }
                if ((other.P11 != null) && (this.P11 == null)) { return false; }
                if ((other.P11 == null) && (this.P11 != null)) { return false; }

                if (other.P12 != null && this.P12 != null)
                {
                    if (!other.P12.Equals(this.P12)) { return false; }
                }
                if ((other.P12 != null) && (this.P12 == null)) { return false; }
                if ((other.P12 == null) && (this.P12 != null)) { return false; }

                if (other.P13 != null && this.P13 != null)
                {
                    if (!other.P13.Equals(this.P13)) { return false; }
                }
                if ((other.P13 != null) && (this.P13 == null)) { return false; }
                if ((other.P13 == null) && (this.P13 != null)) { return false; }

                if (other.P14 != null && this.P14 != null)
                {
                    if (!other.P14.Equals(this.P14)) { return false; }
                }
                if ((other.P14 != null) && (this.P14 == null)) { return false; }
                if ((other.P14 == null) && (this.P14 != null)) { return false; }

                if (other.P15 != null && this.P15 != null)
                {
                    if (!other.P15.Equals(this.P15)) { return false; }
                }
                if ((other.P15 != null) && (this.P15 == null)) { return false; }
                if ((other.P15 == null) && (this.P15 != null)) { return false; }

                if (other.P16 != null && this.P16 != null)
                {
                    if (!other.P16.Equals(this.P16)) { return false; }
                }
                if ((other.P16 != null) && (this.P16 == null)) { return false; }
                if ((other.P16 == null) && (this.P16 != null)) { return false; }

                if (other.P17 != null && this.P17 != null)
                {
                    if (!other.P17.Equals(this.P17)) { return false; }
                }
                if ((other.P17 != null) && (this.P17 == null)) { return false; }
                if ((other.P17 == null) && (this.P17 != null)) { return false; }

                if (other.P18 != null && this.P18 != null)
                {
                    if (!other.P18.Equals(this.P18)) { return false; }
                }
                if ((other.P18 != null) && (this.P18 == null)) { return false; }
                if ((other.P18 == null) && (this.P18 != null)) { return false; }

                if (other.P19 != null && this.P19 != null)
                {
                    if (!other.P19.Equals(this.P19)) { return false; }
                }
                if ((other.P19 != null) && (this.P19 == null)) { return false; }
                if ((other.P19 == null) && (this.P19 != null)) { return false; }

                if (other.P20 != null && this.P20 != null)
                {
                    if (!other.P20.Equals(this.P20)) { return false; }
                }
                if ((other.P20 != null) && (this.P20 == null)) { return false; }
                if ((other.P20 == null) && (this.P20 != null)) { return false; }

                #endregion

                #region Meta data performance percentage values

                if (other.MetaP1Percentage != null && this.MetaP1Percentage != null)
                {
                    if (!other.MetaP1Percentage.Equals(this.MetaP1Percentage)) { return false; }
                }
                if ((other.MetaP1Percentage != null) && (this.MetaP1Percentage == null)) { return false; }
                if ((other.MetaP1Percentage == null) && (this.MetaP1Percentage != null)) { return false; }

                if (other.MetaP2Percentage != null && this.MetaP2Percentage != null)
                {
                    if (!other.MetaP2Percentage.Equals(this.MetaP2Percentage)) { return false; }
                }
                if ((other.MetaP2Percentage != null) && (this.MetaP2Percentage == null)) { return false; }
                if ((other.MetaP2Percentage == null) && (this.MetaP2Percentage != null)) { return false; }

                if (other.MetaP3Percentage != null && this.MetaP3Percentage != null)
                {
                    if (!other.MetaP3Percentage.Equals(this.MetaP3Percentage)) { return false; }
                }
                if ((other.MetaP3Percentage != null) && (this.MetaP3Percentage == null)) { return false; }
                if ((other.MetaP3Percentage == null) && (this.MetaP3Percentage != null)) { return false; }

                if (other.MetaP4Percentage != null && this.MetaP4Percentage != null)
                {
                    if (!other.MetaP4Percentage.Equals(this.MetaP4Percentage)) { return false; }
                }
                if ((other.MetaP4Percentage != null) && (this.MetaP4Percentage == null)) { return false; }
                if ((other.MetaP4Percentage == null) && (this.MetaP4Percentage != null)) { return false; }

                if (other.MetaP5Percentage != null && this.MetaP5Percentage != null)
                {
                    if (!other.MetaP5Percentage.Equals(this.MetaP5Percentage)) { return false; }
                }
                if ((other.MetaP5Percentage != null) && (this.MetaP5Percentage == null)) { return false; }
                if ((other.MetaP5Percentage == null) && (this.MetaP5Percentage != null)) { return false; }

                if (other.MetaP6Percentage != null && this.MetaP6Percentage != null)
                {
                    if (!other.MetaP6Percentage.Equals(this.MetaP6Percentage)) { return false; }
                }
                if ((other.MetaP6Percentage != null) && (this.MetaP6Percentage == null)) { return false; }
                if ((other.MetaP6Percentage == null) && (this.MetaP6Percentage != null)) { return false; }

                if (other.MetaP7Percentage != null && this.MetaP7Percentage != null)
                {
                    if (!other.MetaP7Percentage.Equals(this.MetaP7Percentage)) { return false; }
                }
                if ((other.MetaP7Percentage != null) && (this.MetaP7Percentage == null)) { return false; }
                if ((other.MetaP7Percentage == null) && (this.MetaP7Percentage != null)) { return false; }

                if (other.MetaP8Percentage != null && this.MetaP8Percentage != null)
                {
                    if (!other.MetaP8Percentage.Equals(this.MetaP8Percentage)) { return false; }
                }
                if ((other.MetaP8Percentage != null) && (this.MetaP8Percentage == null)) { return false; }
                if ((other.MetaP8Percentage == null) && (this.MetaP8Percentage != null)) { return false; }

                if (other.MetaP9Percentage != null && this.MetaP9Percentage != null)
                {
                    if (!other.MetaP9Percentage.Equals(this.MetaP9Percentage)) { return false; }
                }
                if ((other.MetaP9Percentage != null) && (this.MetaP9Percentage == null)) { return false; }
                if ((other.MetaP9Percentage == null) && (this.MetaP9Percentage != null)) { return false; }

                if (other.MetaP10Percentage != null && this.MetaP10Percentage != null)
                {
                    if (!other.MetaP10Percentage.Equals(this.MetaP10Percentage)) { return false; }
                }
                if ((other.MetaP10Percentage != null) && (this.MetaP10Percentage == null)) { return false; }
                if ((other.MetaP10Percentage == null) && (this.MetaP10Percentage != null)) { return false; }

                if (other.MetaP11Percentage != null && this.MetaP11Percentage != null)
                {
                    if (!other.MetaP11Percentage.Equals(this.MetaP11Percentage)) { return false; }
                }
                if ((other.MetaP11Percentage != null) && (this.MetaP11Percentage == null)) { return false; }
                if ((other.MetaP11Percentage == null) && (this.MetaP11Percentage != null)) { return false; }

                if (other.MetaP12Percentage != null && this.MetaP12Percentage != null)
                {
                    if (!other.MetaP12Percentage.Equals(this.MetaP12Percentage)) { return false; }
                }
                if ((other.MetaP12Percentage != null) && (this.MetaP12Percentage == null)) { return false; }
                if ((other.MetaP12Percentage == null) && (this.MetaP12Percentage != null)) { return false; }

                if (other.MetaP13Percentage != null && this.MetaP13Percentage != null)
                {
                    if (!other.MetaP13Percentage.Equals(this.MetaP13Percentage)) { return false; }
                }
                if ((other.MetaP13Percentage != null) && (this.MetaP13Percentage == null)) { return false; }
                if ((other.MetaP13Percentage == null) && (this.MetaP13Percentage != null)) { return false; }

                if (other.MetaP14Percentage != null && this.MetaP14Percentage != null)
                {
                    if (!other.MetaP14Percentage.Equals(this.MetaP14Percentage)) { return false; }
                }
                if ((other.MetaP14Percentage != null) && (this.MetaP14Percentage == null)) { return false; }
                if ((other.MetaP14Percentage == null) && (this.MetaP14Percentage != null)) { return false; }

                if (other.MetaP15Percentage != null && this.MetaP15Percentage != null)
                {
                    if (!other.MetaP15Percentage.Equals(this.MetaP15Percentage)) { return false; }
                }
                if ((other.MetaP15Percentage != null) && (this.MetaP15Percentage == null)) { return false; }
                if ((other.MetaP15Percentage == null) && (this.MetaP15Percentage != null)) { return false; }

                if (other.MetaP16Percentage != null && this.MetaP16Percentage != null)
                {
                    if (!other.MetaP16Percentage.Equals(this.MetaP16Percentage)) { return false; }
                }
                if ((other.MetaP16Percentage != null) && (this.MetaP16Percentage == null)) { return false; }
                if ((other.MetaP16Percentage == null) && (this.MetaP16Percentage != null)) { return false; }

                if (other.MetaP17Percentage != null && this.MetaP17Percentage != null)
                {
                    if (!other.MetaP17Percentage.Equals(this.MetaP17Percentage)) { return false; }
                }
                if ((other.MetaP17Percentage != null) && (this.MetaP17Percentage == null)) { return false; }
                if ((other.MetaP17Percentage == null) && (this.MetaP17Percentage != null)) { return false; }

                if (other.MetaP18Percentage != null && this.MetaP18Percentage != null)
                {
                    if (!other.MetaP18Percentage.Equals(this.MetaP18Percentage)) { return false; }
                }
                if ((other.MetaP18Percentage != null) && (this.MetaP18Percentage == null)) { return false; }
                if ((other.MetaP18Percentage == null) && (this.MetaP18Percentage != null)) { return false; }

                if (other.MetaP19Percentage != null && this.MetaP19Percentage != null)
                {
                    if (!other.MetaP19Percentage.Equals(this.MetaP19Percentage)) { return false; }
                }
                if ((other.MetaP19Percentage != null) && (this.MetaP19Percentage == null)) { return false; }
                if ((other.MetaP19Percentage == null) && (this.MetaP19Percentage != null)) { return false; }

                if (other.MetaP20Percentage != null && this.MetaP20Percentage != null)
                {
                    if (!other.MetaP20Percentage.Equals(this.MetaP20Percentage)) { return false; }
                }
                if ((other.MetaP20Percentage != null) && (this.MetaP20Percentage == null)) { return false; }
                if ((other.MetaP20Percentage == null) && (this.MetaP20Percentage != null)) { return false; }

                #endregion
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramBlockingGroupDtoKey
    {

        #region Properties
        public Object PlanogramBlockingId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PlanogramBlockingId.GetHashCode() ^
                Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramBlockingGroupDtoKey other = obj as PlanogramBlockingGroupDtoKey;
            if (other != null)
            {

                // Id
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId != null))
                {
                    if (!other.PlanogramBlockingId.Equals(this.PlanogramBlockingId)) { return false; }
                }
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId == null)) { return false; }
                if ((other.PlanogramBlockingId == null) && (this.PlanogramBlockingId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}


