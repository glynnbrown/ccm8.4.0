﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramAssortmentRegionDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAssortmentRegionDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentRegionDtoKey
                {
                    Name = this.Name,
                    PlanogramAssortmentId = this.PlanogramAssortmentId
                };
            }
        }
        [ForeignKey(typeof(PlanogramAssortmentDto), typeof(IPlanogramAssortmentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentId { get; set; }
        public String Name { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentRegionDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            return this.Name == other.Name;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentRegionDtoKey
    {
        #region Properties
        public Object PlanogramAssortmentId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return PlanogramAssortmentId.GetHashCode() + Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentRegionDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }

            return this.Name == other.Name;
        }
        #endregion
    }
}
