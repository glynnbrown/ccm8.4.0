﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts.
// V8-32005 : A.Silva
//  Added Name, Description and DataOrderType.

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     <c>Data Transfer Object</c> for a <c>Planogram Comparison</c> model object.
    /// </summary>
    [Serializable]
    public class PlanogramComparisonDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramComparisonDtoKey DtoKey { get { return new PlanogramComparisonDtoKey { PlanogramId = PlanogramId }; } }

        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public DateTime? DateLastCompared { get; set; }

        public Boolean IgnoreNonPlacedProducts { get; set; }

        public Byte DataOrderType { get; set; }

        public Object ExtendedData { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.PlanogramId, PlanogramId) &&
                   String.Equals(other.Name, Name) &&
                   String.Equals(other.Description,Description) &&
                   Equals(other.DateLastCompared, DateLastCompared) &&
                   Equals(other.IgnoreNonPlacedProducts, IgnoreNonPlacedProducts) &&
                   Equals(other.DataOrderType, DataOrderType) &&
                   Equals(other.ExtendedData, ExtendedData);
        }

        #endregion
    }

    [Serializable]
    public class PlanogramComparisonDtoKey
    {
        #region Properties

        public Object PlanogramId { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return PlanogramId.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonDtoKey;
            return other != null &&
                   Equals(other.PlanogramId, PlanogramId);
        }

        #endregion
    }
}