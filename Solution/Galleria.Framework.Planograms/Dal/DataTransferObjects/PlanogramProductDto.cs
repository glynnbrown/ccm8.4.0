﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24658 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24972 : L.Hodson
//  Added product image fields for display, tray,  pointofpurchase, alternate and case
// V8-25477 : A.Kuszyk
//  Added ForeignKey attributes to properties.
// V8-26041 : A.Kuszyk
//  Added additional Product properties.
// V8-27058 : A.Probyn
//  Added new meta data properties
//V8-27606 : L.Ineson
//  Added FinancialGroupCode and FinancialGroupName
// V8-26777 : L.Ineson
//  Removed CanMerch properties
#endregion

#region Version History: CCM801
// V8-27636 : L.Ineson
//  Added MetaTotalUnits
#endregion

#region Version History: CCM802

// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-28811 : L.Luong
//  Added Meta Data Properties
// V8-29232 : A.Silva
//      Added PlanogramProductSequenceNumber.

#endregion

#region Version History: CCM803
// V8-29506 : M.Shelley
//  Add new metadata properties for reporting :
//      MetaPositionCount, MetaTotalFacings, MetaTotalLinearSpace, MetaTotalAreaSpace, MetaTotalVolumetricSpace
// V8-28491 : L.Luong
//  Added MetaIsInMasterData
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
#endregion

#region Version History : CCM 820
// V8-30728 : A.Kuszyk
//  Added ColourGroupValue field.
// V8-29998 : L.Ineson
// Changed the dtokey to PlanogramId and Gtin
// V8-30763 :I.George
//	Added MetaCDTNode 
// V8-30705 : A.Probyn
//  Added new meta data fields relating to the assortment rules.           
// V8-31164 : D.Pleasance
//  Removed BlockingColour \ SequenceNumber
#endregion

#region Version History: CCM 830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31947 : A.Silva
//  Added MetaComparisonStatus field.
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32396 : A.Probyn
//  Added MetaIsDelistFamilyRuleBroken
// V8-32609 : L.Ineson
//  Removed old peg prong offset field
// V8-32814 : M.Pettit
//  Added MetaIsBuddied
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramProduct Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramProductDto
    {
       
        #region Properties
        public Object Id { get; set; }
        public PlanogramProductDtoKey DtoKey
        {
            get { return new PlanogramProductDtoKey() 
            {
                PlanogramId = this.PlanogramId,
                Gtin = this.Gtin 
            }; }
        }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        public String Gtin { get; set; }
        public String Name { get; set; }
        public String Brand { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Single DisplayHeight { get; set; }
        public Single DisplayWidth { get; set; }
        public Single DisplayDepth { get; set; }
        public Single AlternateHeight { get; set; }
        public Single AlternateWidth { get; set; }
        public Single AlternateDepth { get; set; }
        public Single PointOfPurchaseHeight { get; set; }
        public Single PointOfPurchaseWidth { get; set; }
        public Single PointOfPurchaseDepth { get; set; }
        public Byte NumberOfPegHoles { get; set; }
        public Single PegX { get; set; }
        public Single PegX2 { get; set; }
        public Single PegX3 { get; set; }
        public Single PegY { get; set; }
        public Single PegY2 { get; set; }
        public Single PegY3 { get; set; }
        public Single PegProngOffsetX { get; set; }
        public Single PegProngOffsetY { get; set; }
        public Single PegDepth { get; set; }
        public Single SqueezeHeight { get; set; }
        public Single SqueezeWidth { get; set; }
        public Single SqueezeDepth { get; set; }
        public Single NestingHeight { get; set; }
        public Single NestingWidth { get; set; }
        public Single NestingDepth { get; set; }
        public Int16 CasePackUnits { get; set; }
        public Byte CaseHigh { get; set; }
        public Byte CaseWide { get; set; }
        public Byte CaseDeep { get; set; }
        public Single CaseHeight { get; set; }
        public Single CaseWidth { get; set; }
        public Single CaseDepth { get; set; }
        public Byte MaxStack { get; set; }
        public Byte MaxTopCap { get; set; }
        public Byte MaxRightCap { get; set; }
        public Byte MinDeep { get; set; }
        public Byte MaxDeep { get; set; }
        public Int16 TrayPackUnits { get; set; }
        public Byte TrayHigh { get; set; }
        public Byte TrayWide { get; set; }
        public Byte TrayDeep { get; set; }
        public Single TrayHeight { get; set; }
        public Single TrayWidth { get; set; }
        public Single TrayDepth { get; set; }
        public Single TrayThickHeight { get; set; }
        public Single TrayThickWidth { get; set; }
        public Single TrayThickDepth { get; set; }
        public Single FrontOverhang { get; set; }
        public Single FingerSpaceAbove { get; set; }
        public Single FingerSpaceToTheSide { get; set; }
        public Byte StatusType { get; set; }
        public Byte OrientationType { get; set; }
        public Byte MerchandisingStyle { get; set; }
        public Boolean IsFrontOnly { get; set; }
        public Boolean IsPlaceHolderProduct { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean CanBreakTrayUp { get; set; }
        public Boolean CanBreakTrayDown { get; set; }
        public Boolean CanBreakTrayBack { get; set; }
        public Boolean CanBreakTrayTop { get; set; }
        public Boolean ForceMiddleCap { get; set; }
        public Boolean ForceBottomCap { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdFront { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdBack { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdTop { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdBottom { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdLeft { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdRight { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdDisplayFront { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdDisplayBack { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdDisplayTop { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdDisplayBottom { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdDisplayLeft { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdDisplayRight { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdTrayFront { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdTrayBack { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdTrayTop { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdTrayBottom { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdTrayLeft { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdTrayRight { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdPointOfPurchaseFront { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdPointOfPurchaseBack { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdPointOfPurchaseTop { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdPointOfPurchaseBottom { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdPointOfPurchaseLeft { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdPointOfPurchaseRight { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdAlternateFront { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdAlternateBack { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdAlternateTop { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdAlternateBottom { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdAlternateLeft { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdAlternateRight { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdCaseFront { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdCaseBack { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdCaseTop { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdCaseBottom { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdCaseLeft { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object PlanogramImageIdCaseRight { get; set; }

        public Object ExtendedData { get; set; }
        public Byte ShapeType { get; set; }
        public Byte FillPatternType { get; set; }
        public Int32 FillColour { get; set; }

        // Additional Product properties
        public String Shape { get; set; }
        public String PointOfPurchaseDescription { get; set; }
        public String ShortDescription { get; set; }
        public String Subcategory { get; set; }
        public String CustomerStatus { get; set; }
        public String Colour { get; set; }
        public String Flavour { get; set; }
        public String PackagingShape { get; set; }
        public String PackagingType { get; set; }
        public String CountryOfOrigin { get; set; }
        public String CountryOfProcessing { get; set; }
        public Int16 ShelfLife { get; set; }
        public Single? DeliveryFrequencyDays { get; set; }
        public String DeliveryMethod { get; set; }
        public String VendorCode { get; set; }
        public String Vendor { get; set; }
        public String ManufacturerCode { get; set; }
        public String Manufacturer { get; set; }
        public String Size { get; set; }
        public String UnitOfMeasure { get; set; }
        public DateTime? DateIntroduced { get; set; }
        public DateTime? DateDiscontinued { get; set; }
        public DateTime? DateEffective { get; set; }
        public String Health { get; set; }
        public String CorporateCode { get; set; }
        public String Barcode { get; set; }
        public Single? SellPrice { get; set; }
        public Int16? SellPackCount { get; set; }
        public String SellPackDescription { get; set; }
        public Single? RecommendedRetailPrice { get; set; }
        public Single? ManufacturerRecommendedRetailPrice { get; set; }
        public Single? CostPrice { get; set; }
        public Single? CaseCost { get; set; }
        public Single? TaxRate { get; set; }
        public String ConsumerInformation { get; set; }
        public String Texture { get; set; }
        public Int16? StyleNumber { get; set; }
        public String Pattern { get; set; }
        public String Model { get; set; }
        public String GarmentType { get; set; }
        public Boolean IsPrivateLabel { get; set; }
        public Boolean IsNewProduct { get; set; }
        public String FinancialGroupCode { get; set; }
        public String FinancialGroupName { get; set; }
        public String ColourGroupValue { get; set; }
        #endregion

        #region Meta Data Properties

        public Boolean? MetaNotAchievedInventory { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Single? MetaPlanogramLinearSpacePercentage { get; set; }
        public Single? MetaPlanogramAreaSpacePercentage { get; set; }
        public Single? MetaPlanogramVolumetricSpacePercentage { get; set; }
        public Int32? MetaPositionCount { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Single? MetaTotalLinearSpace { get; set; }
        public Single? MetaTotalAreaSpace { get; set; }
        public Single? MetaTotalVolumetricSpace { get; set; }
        public Boolean? MetaIsInMasterData { get; set; }
        public Boolean? MetaNotAchievedCases { get; set; }
        public Boolean? MetaNotAchievedDOS { get; set; }
        public Boolean? MetaIsOverShelfLifePercent { get; set; }
        public Boolean? MetaNotAchievedDeliveries { get; set; }
        public Int32? MetaTotalMainFacings { get; set; }
        public Int32? MetaTotalXFacings { get; set; }
        public Int32? MetaTotalYFacings { get; set; }
        public Int32? MetaTotalZFacings { get; set; }
        public Boolean? MetaIsRangedInAssortment { get; set; }
        public String MetaCDTNode { get; set; }
        public Boolean? MetaIsProductRuleBroken { get; set; }
        public Boolean? MetaIsFamilyRuleBroken { get; set; }
        public Boolean? MetaIsInheritanceRuleBroken { get; set; }
        public Boolean? MetaIsLocalProductRuleBroken { get; set; }
        public Boolean? MetaIsDistributionRuleBroken { get; set; }
        public Boolean? MetaIsCoreRuleBroken { get; set; }
        public Boolean? MetaIsDelistProductRuleBroken { get; set; }
        public Boolean? MetaIsForceProductRuleBroken { get; set; }
        public Boolean? MetaIsPreserveProductRuleBroken { get; set; }
        public Boolean? MetaIsMinimumHurdleProductRuleBroken { get; set; }
        public Boolean? MetaIsMaximumProductFamilyRuleBroken { get; set; }
        public Boolean? MetaIsMinimumProductFamilyRuleBroken { get; set; }
        public Boolean? MetaIsDependencyFamilyRuleBroken { get; set; }
        public Boolean? MetaIsDelistFamilyRuleBroken { get; set; }
        public Byte MetaComparisonStatus { get; set; }
        public Boolean? MetaIsBuddied { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramProductDto other = obj as PlanogramProductDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id == null)) return false;
                if ((other.Id == null) && (this.Id != null)) return false;
                if ((other.Id != null) && (this.Id != null)) if (!other.Id.Equals(this.Id)) return false;
                // Planogram Id
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) return false;
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) return false;
                if ((other.PlanogramId != null) && (this.PlanogramId != null)) if (!other.PlanogramId.Equals(this.PlanogramId)) return false;

                if (other.Gtin != this.Gtin) return false;
                if (other.Name != this.Name) return false;
                if (other.Brand != this.Brand) return false;
                if (other.Height != this.Height) return false;
                if (other.Width != this.Width) return false;
                if (other.Depth != this.Depth) return false;
                if (other.DisplayHeight != this.DisplayHeight) return false;
                if (other.DisplayWidth != this.DisplayWidth) return false;
                if (other.DisplayDepth != this.DisplayDepth) return false;
                if (other.AlternateHeight != this.AlternateHeight) return false;
                if (other.AlternateWidth != this.AlternateWidth) return false;
                if (other.AlternateDepth != this.AlternateDepth) return false;
                if (other.PointOfPurchaseHeight != this.PointOfPurchaseHeight) return false;
                if (other.PointOfPurchaseWidth != this.PointOfPurchaseWidth) return false;
                if (other.PointOfPurchaseDepth != this.PointOfPurchaseDepth) return false;
                if (other.NumberOfPegHoles != this.NumberOfPegHoles) return false;
                if (other.PegX != this.PegX) return false;
                if (other.PegX2 != this.PegX2) return false;
                if (other.PegX3 != this.PegX3) return false;
                if (other.PegY != this.PegY) return false;
                if (other.PegY2 != this.PegY2) return false;
                if (other.PegY3 != this.PegY3) return false;
                if (other.PegProngOffsetX != this.PegProngOffsetX) return false;
                if (other.PegProngOffsetY != this.PegProngOffsetY) return false;
                if (other.PegDepth != this.PegDepth) return false;
                if (other.SqueezeHeight != this.SqueezeHeight) return false;
                if (other.SqueezeWidth != this.SqueezeWidth) return false;
                if (other.SqueezeDepth != this.SqueezeDepth) return false;
                if (other.NestingHeight != this.NestingHeight) return false;
                if (other.NestingWidth != this.NestingWidth) return false;
                if (other.NestingDepth != this.NestingDepth) return false;
                if (other.CasePackUnits != this.CasePackUnits) return false;
                if (other.CaseHigh != this.CaseHigh) return false;
                if (other.CaseWide != this.CaseWide) return false;
                if (other.CaseDeep != this.CaseDeep) return false;
                if (other.CaseHeight != this.CaseHeight) return false;
                if (other.CaseWidth != this.CaseWidth) return false;
                if (other.CaseDepth != this.CaseDepth) return false;
                if (other.MaxStack != this.MaxStack) return false;
                if (other.MaxTopCap != this.MaxTopCap) return false;
                if (other.MaxRightCap != this.MaxRightCap) return false;
                if (other.MinDeep != this.MinDeep) return false;
                if (other.MaxDeep != this.MaxDeep) return false;
                if (other.TrayPackUnits != this.TrayPackUnits) return false;
                if (other.TrayHigh != this.TrayHigh) return false;
                if (other.TrayWide != this.TrayWide) return false;
                if (other.TrayDeep != this.TrayDeep) return false;
                if (other.TrayHeight != this.TrayHeight) return false;
                if (other.TrayWidth != this.TrayWidth) return false;
                if (other.TrayDepth != this.TrayDepth) return false;
                if (other.TrayThickHeight != this.TrayThickHeight) return false;
                if (other.TrayThickWidth != this.TrayThickWidth) return false;
                if (other.TrayThickDepth != this.TrayThickDepth) return false;
                if (other.FrontOverhang != this.FrontOverhang) return false;
                if (other.FingerSpaceAbove != this.FingerSpaceAbove) return false;
                if (other.FingerSpaceToTheSide != this.FingerSpaceToTheSide) return false;
                if (other.StatusType != this.StatusType) return false;
                if (other.OrientationType != this.OrientationType) return false;
                if (other.MerchandisingStyle != this.MerchandisingStyle) return false;
                if (other.IsFrontOnly != this.IsFrontOnly) return false;
                if (other.IsPlaceHolderProduct != this.IsPlaceHolderProduct) return false;
                if (other.IsActive != this.IsActive) return false;
                if (other.CanBreakTrayUp != this.CanBreakTrayUp) return false;
                if (other.CanBreakTrayDown != this.CanBreakTrayDown) return false;
                if (other.CanBreakTrayBack != this.CanBreakTrayBack) return false;
                if (other.CanBreakTrayTop != this.CanBreakTrayTop) return false;
                if (other.ForceMiddleCap != this.ForceMiddleCap) return false;
                if (other.ForceBottomCap != this.ForceBottomCap) return false;
                // PlanogramImageIdFront
                if ((other.PlanogramImageIdFront != null) && (this.PlanogramImageIdFront == null)) return false;
                if ((other.PlanogramImageIdFront == null) && (this.PlanogramImageIdFront != null)) return false;
                if ((other.PlanogramImageIdFront != null) && (this.PlanogramImageIdFront != null)) if (!other.PlanogramImageIdFront.Equals(this.PlanogramImageIdFront)) return false;
                // PlanogramImageIdBack
                if ((other.PlanogramImageIdBack != null) && (this.PlanogramImageIdBack == null)) return false;
                if ((other.PlanogramImageIdBack == null) && (this.PlanogramImageIdBack != null)) return false;
                if ((other.PlanogramImageIdBack != null) && (this.PlanogramImageIdBack != null)) if (!other.PlanogramImageIdBack.Equals(this.PlanogramImageIdBack)) return false;
                // PlanogramImageIdTop
                if ((other.PlanogramImageIdTop != null) && (this.PlanogramImageIdTop == null)) return false;
                if ((other.PlanogramImageIdTop == null) && (this.PlanogramImageIdTop != null)) return false;
                if ((other.PlanogramImageIdTop != null) && (this.PlanogramImageIdTop != null)) if (!other.PlanogramImageIdTop.Equals(this.PlanogramImageIdTop)) return false;
                // PlanogramImageIdBottom
                if ((other.PlanogramImageIdBottom != null) && (this.PlanogramImageIdBottom == null)) return false;
                if ((other.PlanogramImageIdBottom == null) && (this.PlanogramImageIdBottom != null)) return false;
                if ((other.PlanogramImageIdBottom != null) && (this.PlanogramImageIdBottom != null)) if (!other.PlanogramImageIdBottom.Equals(this.PlanogramImageIdBottom)) return false;
                // PlanogramImageIdLeft
                if ((other.PlanogramImageIdLeft != null) && (this.PlanogramImageIdLeft == null)) return false;
                if ((other.PlanogramImageIdLeft == null) && (this.PlanogramImageIdLeft != null)) return false;
                if ((other.PlanogramImageIdLeft != null) && (this.PlanogramImageIdLeft != null)) if (!other.PlanogramImageIdLeft.Equals(this.PlanogramImageIdLeft)) return false;
                // PlanogramImageIdRight
                if ((other.PlanogramImageIdRight != null) && (this.PlanogramImageIdRight == null)) return false;
                if ((other.PlanogramImageIdRight == null) && (this.PlanogramImageIdRight != null)) return false;
                if ((other.PlanogramImageIdRight != null) && (this.PlanogramImageIdRight != null)) if (!other.PlanogramImageIdRight.Equals(this.PlanogramImageIdRight)) return false;
                // PlanogramImageIdDisplayFront
                if ((other.PlanogramImageIdDisplayFront != null) && (this.PlanogramImageIdDisplayFront == null)) return false;
                if ((other.PlanogramImageIdDisplayFront == null) && (this.PlanogramImageIdDisplayFront != null)) return false;
                if ((other.PlanogramImageIdDisplayFront != null) && (this.PlanogramImageIdDisplayFront != null)) if (!other.PlanogramImageIdDisplayFront.Equals(this.PlanogramImageIdDisplayFront)) return false;
                // PlanogramImageIdDisplayBack
                if ((other.PlanogramImageIdDisplayBack != null) && (this.PlanogramImageIdDisplayBack == null)) return false;
                if ((other.PlanogramImageIdDisplayBack == null) && (this.PlanogramImageIdDisplayBack != null)) return false;
                if ((other.PlanogramImageIdDisplayBack != null) && (this.PlanogramImageIdDisplayBack != null)) if (!other.PlanogramImageIdDisplayBack.Equals(this.PlanogramImageIdDisplayBack)) return false;
                // PlanogramImageIdDisplayTop
                if ((other.PlanogramImageIdDisplayTop != null) && (this.PlanogramImageIdDisplayTop == null)) return false;
                if ((other.PlanogramImageIdDisplayTop == null) && (this.PlanogramImageIdDisplayTop != null)) return false;
                if ((other.PlanogramImageIdDisplayTop != null) && (this.PlanogramImageIdDisplayTop != null)) if (!other.PlanogramImageIdDisplayTop.Equals(this.PlanogramImageIdDisplayTop)) return false;
                // PlanogramImageIdDisplayBottom
                if ((other.PlanogramImageIdDisplayBottom != null) && (this.PlanogramImageIdDisplayBottom == null)) return false;
                if ((other.PlanogramImageIdDisplayBottom == null) && (this.PlanogramImageIdDisplayBottom != null)) return false;
                if ((other.PlanogramImageIdDisplayBottom != null) && (this.PlanogramImageIdDisplayBottom != null)) if (!other.PlanogramImageIdDisplayBottom.Equals(this.PlanogramImageIdDisplayBottom)) return false;
                // PlanogramImageIdDisplayLeft
                if ((other.PlanogramImageIdDisplayLeft != null) && (this.PlanogramImageIdDisplayLeft == null)) return false;
                if ((other.PlanogramImageIdDisplayLeft == null) && (this.PlanogramImageIdDisplayLeft != null)) return false;
                if ((other.PlanogramImageIdDisplayLeft != null) && (this.PlanogramImageIdDisplayLeft != null)) if (!other.PlanogramImageIdDisplayLeft.Equals(this.PlanogramImageIdDisplayLeft)) return false;
                // PlanogramImageIdDisplayRight
                if ((other.PlanogramImageIdDisplayRight != null) && (this.PlanogramImageIdDisplayRight == null)) return false;
                if ((other.PlanogramImageIdDisplayRight == null) && (this.PlanogramImageIdDisplayRight != null)) return false;
                if ((other.PlanogramImageIdDisplayRight != null) && (this.PlanogramImageIdDisplayRight != null)) if (!other.PlanogramImageIdDisplayRight.Equals(this.PlanogramImageIdDisplayRight)) return false;
                // PlanogramImageIdTrayFront
                if ((other.PlanogramImageIdTrayFront != null) && (this.PlanogramImageIdTrayFront == null)) return false;
                if ((other.PlanogramImageIdTrayFront == null) && (this.PlanogramImageIdTrayFront != null)) return false;
                if ((other.PlanogramImageIdTrayFront != null) && (this.PlanogramImageIdTrayFront != null)) if (!other.PlanogramImageIdTrayFront.Equals(this.PlanogramImageIdTrayFront)) return false;
                // PlanogramImageIdTrayBack
                if ((other.PlanogramImageIdTrayBack != null) && (this.PlanogramImageIdTrayBack == null)) return false;
                if ((other.PlanogramImageIdTrayBack == null) && (this.PlanogramImageIdTrayBack != null)) return false;
                if ((other.PlanogramImageIdTrayBack != null) && (this.PlanogramImageIdTrayBack != null)) if (!other.PlanogramImageIdTrayBack.Equals(this.PlanogramImageIdTrayBack)) return false;
                // PlanogramImageIdTrayTop
                if ((other.PlanogramImageIdTrayTop != null) && (this.PlanogramImageIdTrayTop == null)) return false;
                if ((other.PlanogramImageIdTrayTop == null) && (this.PlanogramImageIdTrayTop != null)) return false;
                if ((other.PlanogramImageIdTrayTop != null) && (this.PlanogramImageIdTrayTop != null)) if (!other.PlanogramImageIdTrayTop.Equals(this.PlanogramImageIdTrayTop)) return false;
                // PlanogramImageIdTrayBottom
                if ((other.PlanogramImageIdTrayBottom != null) && (this.PlanogramImageIdTrayBottom == null)) return false;
                if ((other.PlanogramImageIdTrayBottom == null) && (this.PlanogramImageIdTrayBottom != null)) return false;
                if ((other.PlanogramImageIdTrayBottom != null) && (this.PlanogramImageIdTrayBottom != null)) if (!other.PlanogramImageIdTrayBottom.Equals(this.PlanogramImageIdTrayBottom)) return false;
                // PlanogramImageIdTrayLeft
                if ((other.PlanogramImageIdTrayLeft != null) && (this.PlanogramImageIdTrayLeft == null)) return false;
                if ((other.PlanogramImageIdTrayLeft == null) && (this.PlanogramImageIdTrayLeft != null)) return false;
                if ((other.PlanogramImageIdTrayLeft != null) && (this.PlanogramImageIdTrayLeft != null)) if (!other.PlanogramImageIdTrayLeft.Equals(this.PlanogramImageIdTrayLeft)) return false;
                // PlanogramImageIdTrayRight
                if ((other.PlanogramImageIdTrayRight != null) && (this.PlanogramImageIdTrayRight == null)) return false;
                if ((other.PlanogramImageIdTrayRight == null) && (this.PlanogramImageIdTrayRight != null)) return false;
                if ((other.PlanogramImageIdTrayRight != null) && (this.PlanogramImageIdTrayRight != null)) if (!other.PlanogramImageIdTrayRight.Equals(this.PlanogramImageIdTrayRight)) return false;
                // PlanogramImageIdPointOfPurchaseFront
                if ((other.PlanogramImageIdPointOfPurchaseFront != null) && (this.PlanogramImageIdPointOfPurchaseFront == null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseFront == null) && (this.PlanogramImageIdPointOfPurchaseFront != null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseFront != null) && (this.PlanogramImageIdPointOfPurchaseFront != null)) if (!other.PlanogramImageIdPointOfPurchaseFront.Equals(this.PlanogramImageIdPointOfPurchaseFront)) return false;
                // PlanogramImageIdPointOfPurchaseBack
                if ((other.PlanogramImageIdPointOfPurchaseBack != null) && (this.PlanogramImageIdPointOfPurchaseBack == null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseBack == null) && (this.PlanogramImageIdPointOfPurchaseBack != null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseBack != null) && (this.PlanogramImageIdPointOfPurchaseBack != null)) if (!other.PlanogramImageIdPointOfPurchaseBack.Equals(this.PlanogramImageIdPointOfPurchaseBack)) return false;
                // PlanogramImageIdPointOfPurchaseTop
                if ((other.PlanogramImageIdPointOfPurchaseTop != null) && (this.PlanogramImageIdPointOfPurchaseTop == null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseTop == null) && (this.PlanogramImageIdPointOfPurchaseTop != null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseTop != null) && (this.PlanogramImageIdPointOfPurchaseTop != null)) if (!other.PlanogramImageIdPointOfPurchaseTop.Equals(this.PlanogramImageIdPointOfPurchaseTop)) return false;
                // PlanogramImageIdPointOfPurchaseBottom
                if ((other.PlanogramImageIdPointOfPurchaseBottom != null) && (this.PlanogramImageIdPointOfPurchaseBottom == null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseBottom == null) && (this.PlanogramImageIdPointOfPurchaseBottom != null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseBottom != null) && (this.PlanogramImageIdPointOfPurchaseBottom != null)) if (!other.PlanogramImageIdPointOfPurchaseBottom.Equals(this.PlanogramImageIdPointOfPurchaseBottom)) return false;
                // PlanogramImageIdPointOfPurchaseLeft
                if ((other.PlanogramImageIdPointOfPurchaseLeft != null) && (this.PlanogramImageIdPointOfPurchaseLeft == null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseLeft == null) && (this.PlanogramImageIdPointOfPurchaseLeft != null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseLeft != null) && (this.PlanogramImageIdPointOfPurchaseLeft != null)) if (!other.PlanogramImageIdPointOfPurchaseLeft.Equals(this.PlanogramImageIdPointOfPurchaseLeft)) return false;
                // PlanogramImageIdPointOfPurchaseRight
                if ((other.PlanogramImageIdPointOfPurchaseRight != null) && (this.PlanogramImageIdPointOfPurchaseRight == null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseRight == null) && (this.PlanogramImageIdPointOfPurchaseRight != null)) return false;
                if ((other.PlanogramImageIdPointOfPurchaseRight != null) && (this.PlanogramImageIdPointOfPurchaseRight != null)) if (!other.PlanogramImageIdPointOfPurchaseRight.Equals(this.PlanogramImageIdPointOfPurchaseRight)) return false;
                // PlanogramImageIdAlternateFront
                if ((other.PlanogramImageIdAlternateFront != null) && (this.PlanogramImageIdAlternateFront == null)) return false;
                if ((other.PlanogramImageIdAlternateFront == null) && (this.PlanogramImageIdAlternateFront != null)) return false;
                if ((other.PlanogramImageIdAlternateFront != null) && (this.PlanogramImageIdAlternateFront != null)) if (!other.PlanogramImageIdAlternateFront.Equals(this.PlanogramImageIdAlternateFront)) return false;
                // PlanogramImageIdAlternateBack
                if ((other.PlanogramImageIdAlternateBack != null) && (this.PlanogramImageIdAlternateBack == null)) return false;
                if ((other.PlanogramImageIdAlternateBack == null) && (this.PlanogramImageIdAlternateBack != null)) return false;
                if ((other.PlanogramImageIdAlternateBack != null) && (this.PlanogramImageIdAlternateBack != null)) if (!other.PlanogramImageIdAlternateBack.Equals(this.PlanogramImageIdAlternateBack)) return false;
                // PlanogramImageIdAlternateTop
                if ((other.PlanogramImageIdAlternateTop != null) && (this.PlanogramImageIdAlternateTop == null)) return false;
                if ((other.PlanogramImageIdAlternateTop == null) && (this.PlanogramImageIdAlternateTop != null)) return false;
                if ((other.PlanogramImageIdAlternateTop != null) && (this.PlanogramImageIdAlternateTop != null)) if (!other.PlanogramImageIdAlternateTop.Equals(this.PlanogramImageIdAlternateTop)) return false;
                // PlanogramImageIdAlternateBottom
                if ((other.PlanogramImageIdAlternateBottom != null) && (this.PlanogramImageIdAlternateBottom == null)) return false;
                if ((other.PlanogramImageIdAlternateBottom == null) && (this.PlanogramImageIdAlternateBottom != null)) return false;
                if ((other.PlanogramImageIdAlternateBottom != null) && (this.PlanogramImageIdAlternateBottom != null)) if (!other.PlanogramImageIdAlternateBottom.Equals(this.PlanogramImageIdAlternateBottom)) return false;
                // PlanogramImageIdAlternateLeft
                if ((other.PlanogramImageIdAlternateLeft != null) && (this.PlanogramImageIdAlternateLeft == null)) return false;
                if ((other.PlanogramImageIdAlternateLeft == null) && (this.PlanogramImageIdAlternateLeft != null)) return false;
                if ((other.PlanogramImageIdAlternateLeft != null) && (this.PlanogramImageIdAlternateLeft != null)) if (!other.PlanogramImageIdAlternateLeft.Equals(this.PlanogramImageIdAlternateLeft)) return false;
                // PlanogramImageIdAlternateRight
                if ((other.PlanogramImageIdAlternateRight != null) && (this.PlanogramImageIdAlternateRight == null)) return false;
                if ((other.PlanogramImageIdAlternateRight == null) && (this.PlanogramImageIdAlternateRight != null)) return false;
                if ((other.PlanogramImageIdAlternateRight != null) && (this.PlanogramImageIdAlternateRight != null)) if (!other.PlanogramImageIdAlternateRight.Equals(this.PlanogramImageIdAlternateRight)) return false;
                // PlanogramImageIdCaseFront
                if ((other.PlanogramImageIdCaseFront != null) && (this.PlanogramImageIdCaseFront == null)) return false;
                if ((other.PlanogramImageIdCaseFront == null) && (this.PlanogramImageIdCaseFront != null)) return false;
                if ((other.PlanogramImageIdCaseFront != null) && (this.PlanogramImageIdCaseFront != null)) if (!other.PlanogramImageIdCaseFront.Equals(this.PlanogramImageIdCaseFront)) return false;
                // PlanogramImageIdCaseBack
                if ((other.PlanogramImageIdCaseBack != null) && (this.PlanogramImageIdCaseBack == null)) return false;
                if ((other.PlanogramImageIdCaseBack == null) && (this.PlanogramImageIdCaseBack != null)) return false;
                if ((other.PlanogramImageIdCaseBack != null) && (this.PlanogramImageIdCaseBack != null)) if (!other.PlanogramImageIdCaseBack.Equals(this.PlanogramImageIdCaseBack)) return false;
                // PlanogramImageIdCaseTop
                if ((other.PlanogramImageIdCaseTop != null) && (this.PlanogramImageIdCaseTop == null)) return false;
                if ((other.PlanogramImageIdCaseTop == null) && (this.PlanogramImageIdCaseTop != null)) return false;
                if ((other.PlanogramImageIdCaseTop != null) && (this.PlanogramImageIdCaseTop != null)) if (!other.PlanogramImageIdCaseTop.Equals(this.PlanogramImageIdCaseTop)) return false;
                // PlanogramImageIdCaseBottom
                if ((other.PlanogramImageIdCaseBottom != null) && (this.PlanogramImageIdCaseBottom == null)) return false;
                if ((other.PlanogramImageIdCaseBottom == null) && (this.PlanogramImageIdCaseBottom != null)) return false;
                if ((other.PlanogramImageIdCaseBottom != null) && (this.PlanogramImageIdCaseBottom != null)) if (!other.PlanogramImageIdCaseBottom.Equals(this.PlanogramImageIdCaseBottom)) return false;
                // PlanogramImageIdCaseLeft
                if ((other.PlanogramImageIdCaseLeft != null) && (this.PlanogramImageIdCaseLeft == null)) return false;
                if ((other.PlanogramImageIdCaseLeft == null) && (this.PlanogramImageIdCaseLeft != null)) return false;
                if ((other.PlanogramImageIdCaseLeft != null) && (this.PlanogramImageIdCaseLeft != null)) if (!other.PlanogramImageIdCaseLeft.Equals(this.PlanogramImageIdCaseLeft)) return false;
                // PlanogramImageIdCaseRight
                if ((other.PlanogramImageIdCaseRight != null) && (this.PlanogramImageIdCaseRight == null)) return false;
                if ((other.PlanogramImageIdCaseRight == null) && (this.PlanogramImageIdCaseRight != null)) return false;
                if ((other.PlanogramImageIdCaseRight != null) && (this.PlanogramImageIdCaseRight != null)) if (!other.PlanogramImageIdCaseRight.Equals(this.PlanogramImageIdCaseRight)) return false;
                // Extended Data
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) return false;
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) return false;
                if ((other.ExtendedData != null) && (this.ExtendedData != null)) if (!other.ExtendedData.Equals(this.ExtendedData)) return false;

                if (other.ShapeType != this.ShapeType) return false;
                if (other.FillPatternType != this.FillPatternType) return false;
                if (other.FillColour!= this.FillColour) return false;
                if (other.Shape != this.Shape) return false;
                if (other.PointOfPurchaseDescription != this.PointOfPurchaseDescription) return false;
                if (other.ShortDescription != this.ShortDescription) return false;
                if (other.Subcategory != this.Subcategory) return false;
                if (other.CustomerStatus != this.CustomerStatus) return false;
                if (other.Colour != this.Colour) return false;
                if (other.Flavour != this.Flavour) return false;
                if (other.PackagingShape != this.PackagingShape) return false;
                if (other.PackagingType != this.PackagingType) return false;
                if (other.CountryOfOrigin != this.CountryOfOrigin) return false;
                if (other.CountryOfProcessing != this.CountryOfProcessing) return false;
                if (other.ShelfLife != this.ShelfLife) return false;
                if (other.DeliveryFrequencyDays != this.DeliveryFrequencyDays) return false;
                if (other.DeliveryMethod != this.DeliveryMethod) return false;
                if (other.VendorCode != this.VendorCode) return false;
                if (other.Vendor != this.Vendor) return false;
                if (other.ManufacturerCode != this.ManufacturerCode) return false;
                if (other.Manufacturer != this.Manufacturer) return false;
                if (other.Size != this.Size) return false;
                if (other.UnitOfMeasure != this.UnitOfMeasure) return false;
                // DateIntroduced
                if ((other.DateIntroduced == null) && (this.DateIntroduced != null)) return false;
                if ((other.DateIntroduced != null) && (this.DateIntroduced == null)) return false;
                if ((other.DateIntroduced != null) && (this.DateIntroduced != null)) if (!other.DateIntroduced.Equals(this.DateIntroduced)) return false;
                // DateDiscontinued
                if ((other.DateDiscontinued == null) && (this.DateDiscontinued != null)) return false;
                if ((other.DateDiscontinued != null) && (this.DateDiscontinued == null)) return false;
                if ((other.DateDiscontinued != null) && (this.DateDiscontinued != null)) if (!other.DateDiscontinued.Equals(this.DateDiscontinued)) return false;
                // DateEffective
                if ((other.DateEffective == null) && (this.DateEffective != null)) return false;
                if ((other.DateEffective != null) && (this.DateEffective == null)) return false;
                if ((other.DateEffective != null) && (this.DateEffective != null)) if (!other.DateEffective.Equals(this.DateEffective)) return false;

                if (other.Health != this.Health) return false;
                if (other.CorporateCode != this.CorporateCode) return false;
                if (other.Barcode != this.Barcode) return false;
                // SellPrice
                if ((other.SellPrice == null) && (this.SellPrice != null)) return false;
                if ((other.SellPrice != null) && (this.SellPrice == null)) return false;
                if ((other.SellPrice != null) && (this.SellPrice != null)) if (!other.SellPrice.Equals(this.SellPrice)) return false;
                // SellPackCount
                if ((other.SellPackCount == null) && (this.SellPackCount != null)) return false;
                if ((other.SellPackCount != null) && (this.SellPackCount == null)) return false;
                if ((other.SellPackCount != null) && (this.SellPackCount != null)) if (!other.SellPackCount.Equals(this.SellPackCount)) return false;

                if (other.SellPackDescription != this.SellPackDescription) return false;
                // RecommendedRetailPrice
                if ((other.RecommendedRetailPrice == null) && (this.RecommendedRetailPrice != null)) return false;
                if ((other.RecommendedRetailPrice != null) && (this.RecommendedRetailPrice == null)) return false;
                if ((other.RecommendedRetailPrice != null) && (this.RecommendedRetailPrice != null)) if (!other.RecommendedRetailPrice.Equals(this.RecommendedRetailPrice)) return false;
                // ManufacturerRecommendedRetailPrice
                if ((other.ManufacturerRecommendedRetailPrice == null) && (this.ManufacturerRecommendedRetailPrice != null)) return false;
                if ((other.ManufacturerRecommendedRetailPrice != null) && (this.ManufacturerRecommendedRetailPrice == null)) return false;
                if ((other.ManufacturerRecommendedRetailPrice != null) && (this.ManufacturerRecommendedRetailPrice != null)) if (!other.ManufacturerRecommendedRetailPrice.Equals(this.ManufacturerRecommendedRetailPrice)) return false;
                // CostPrice
                if ((other.CostPrice == null) && (this.CostPrice != null)) return false;
                if ((other.CostPrice != null) && (this.CostPrice == null)) return false;
                if ((other.CostPrice != null) && (this.CostPrice != null)) if (!other.CostPrice.Equals(this.CostPrice)) return false;
                // CaseCost
                if ((other.CaseCost == null) && (this.CaseCost != null)) return false;
                if ((other.CaseCost != null) && (this.CaseCost == null)) return false;
                if ((other.CaseCost != null) && (this.CaseCost != null)) if (!other.CaseCost.Equals(this.CaseCost)) return false;
                // TaxRate
                if ((other.TaxRate == null) && (this.TaxRate != null)) return false;
                if ((other.TaxRate != null) && (this.TaxRate == null)) return false;
                if ((other.TaxRate != null) && (this.TaxRate != null)) if (!other.TaxRate.Equals(this.TaxRate)) return false;

                if (other.ConsumerInformation != this.ConsumerInformation) return false;
                if (other.Texture != this.Texture) return false;
                // StyleNumber
                if ((other.StyleNumber == null) && (this.StyleNumber != null)) return false;
                if ((other.StyleNumber != null) && (this.StyleNumber == null)) return false;
                if ((other.StyleNumber != null) && (this.StyleNumber != null)) if (!other.StyleNumber.Equals(this.StyleNumber)) return false;

                if (other.Pattern != this.Pattern) return false;
                if (other.Model != this.Model) return false;
                if (other.GarmentType != this.GarmentType) return false;
                if (other.IsPrivateLabel != this.IsPrivateLabel) return false;
                if (other.IsNewProduct != this.IsNewProduct) return false;
                if (other.FinancialGroupCode != this.FinancialGroupCode) return false;
                if (other.FinancialGroupName != this.FinancialGroupName) return false;
                if (other.MetaNotAchievedInventory != this.MetaNotAchievedInventory) return false;
                if (other.MetaTotalUnits != this.MetaTotalUnits) return false;
                if (other.MetaPlanogramLinearSpacePercentage != this.MetaPlanogramLinearSpacePercentage) return false;
                if (other.MetaPlanogramAreaSpacePercentage != this.MetaPlanogramAreaSpacePercentage) return false;
                if (other.MetaPlanogramVolumetricSpacePercentage!= this.MetaPlanogramVolumetricSpacePercentage) return false;
                
                if (other.MetaPositionCount != this.MetaPositionCount) return false;
                if (other.MetaTotalFacings != this.MetaTotalFacings) return false;
                if (other.MetaTotalLinearSpace != this.MetaTotalLinearSpace) return false;
                if (other.MetaTotalAreaSpace != this.MetaTotalAreaSpace) return false;
                if (other.MetaTotalVolumetricSpace != this.MetaTotalVolumetricSpace) return false;
                if (other.MetaIsInMasterData != this.MetaIsInMasterData) return false;
                if (other.MetaNotAchievedCases != this.MetaNotAchievedCases) return false;
                if (other.MetaNotAchievedDOS != this.MetaNotAchievedDOS) return false;
                if (other.MetaIsOverShelfLifePercent != this.MetaIsOverShelfLifePercent) return false;
                if (other.MetaNotAchievedDeliveries != this.MetaNotAchievedDeliveries) return false;
                if (other.MetaTotalMainFacings != this.MetaTotalMainFacings) return false;
                if (other.MetaTotalXFacings != this.MetaTotalXFacings) return false;
                if (other.MetaTotalYFacings != this.MetaTotalYFacings) return false;
                if (other.MetaTotalZFacings != this.MetaTotalZFacings) return false;
                if (other.MetaIsRangedInAssortment != this.MetaIsRangedInAssortment) return false;
                if (other.ColourGroupValue != this.ColourGroupValue) return false;
                if (other.MetaCDTNode != this.MetaCDTNode) return false;
                if (other.MetaIsProductRuleBroken != this.MetaIsProductRuleBroken) return false;
                if (other.MetaIsFamilyRuleBroken != this.MetaIsFamilyRuleBroken) return false;
                if (other.MetaIsInheritanceRuleBroken != this.MetaIsInheritanceRuleBroken) return false;
                if (other.MetaIsLocalProductRuleBroken != this.MetaIsLocalProductRuleBroken) return false;
                if (other.MetaIsDistributionRuleBroken != this.MetaIsDistributionRuleBroken) return false;
                if (other.MetaIsCoreRuleBroken != this.MetaIsCoreRuleBroken) return false;
                if (other.MetaIsDelistProductRuleBroken != this.MetaIsDelistProductRuleBroken) return false;
                if (other.MetaIsForceProductRuleBroken != this.MetaIsForceProductRuleBroken) return false;
                if (other.MetaIsPreserveProductRuleBroken != this.MetaIsPreserveProductRuleBroken) return false;
                if (other.MetaIsMinimumHurdleProductRuleBroken != this.MetaIsMinimumHurdleProductRuleBroken) return false;
                if (other.MetaIsMaximumProductFamilyRuleBroken != this.MetaIsMaximumProductFamilyRuleBroken) return false;
                if (other.MetaIsMinimumProductFamilyRuleBroken != this.MetaIsMinimumProductFamilyRuleBroken) return false;
                if (other.MetaIsDependencyFamilyRuleBroken != this.MetaIsDependencyFamilyRuleBroken) return false;
                if (other.MetaComparisonStatus != this.MetaComparisonStatus) return false;
                if (other.MetaIsDelistFamilyRuleBroken != this.MetaIsDelistFamilyRuleBroken) return false;
                if (other.MetaIsBuddied != this.MetaIsBuddied) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramProductDtoKey
    {
        #region Properties
        public Object PlanogramId { get; set; }
        public String Gtin { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return 
                PlanogramId.GetHashCode() ^
                Gtin.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramProductDtoKey other = obj as PlanogramProductDtoKey;
            if (other != null)
            {
                if (other.Gtin != this.Gtin) return false;

                // Planogram Id
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) return false;
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) return false;
                if ((other.PlanogramId != null) && (this.PlanogramId != null)) if (!other.PlanogramId.Equals(this.PlanogramId)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
