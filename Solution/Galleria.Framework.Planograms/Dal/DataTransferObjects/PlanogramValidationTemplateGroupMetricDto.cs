﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26954 : A.Silva ~ Added ResultType property.
// V8-26812 : A.Silva ~ Added ValidationType property.
#endregion

#region Version History: CCM803
// V8-29596 : L.Luong
//  Added Criteria
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     PlanogramValidationTemplateGroupMetric Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramValidationTemplateGroupMetricDto
    {
        #region Properties

        public Object Id { get; set; }
        public PlanogramValidationTemplateGroupMetricDtoKey DtoKey
        {
            get 
            { 
                return new PlanogramValidationTemplateGroupMetricDtoKey 
                {
                    AggregationType = AggregationType,
                    Field = Field,
                    PlanogramValidationTemplateGroupId = this.PlanogramValidationTemplateGroupId 
                }; 
            }
        }
        [ForeignKey(typeof(PlanogramValidationTemplateGroupDto), typeof(IPlanogramValidationTemplateGroupDal), DeleteBehavior.Cascade)]
        public Object PlanogramValidationTemplateGroupId { get; set; }

        public String Field { get; set; }
        public Single Threshold1 { get; set; }
        public Single Threshold2 { get; set; }
        public Single Score1 { get; set; }
        public Single Score2 { get; set; }
        public Single Score3 { get; set; }
        public Byte AggregationType { get; set; }
        public String Criteria { get; set; }
        public Object ExtendedData { get; set; }

        /// <summary>
        ///     Gets or sets the validation result type for this instance.
        /// </summary>
        public Byte ResultType { get; set; }

        /// <summary>
        ///     Gets or sets the validation type for this instance.
        /// </summary>
        public Byte ValidationType { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramValidationTemplateGroupMetricDto;
            if (other == null) return false;

            // Id
            if (other.Id != null && this.Id != null)
            {
                if (!other.Id.Equals(this.Id)) return false;
            }
            if ((other.Id == null && this.Id != null) || (other.Id != null && this.Id == null))
            {
                return false;
            }
            //  PlanogramValidationTemplateGroupId
            if (other.PlanogramValidationTemplateGroupId != null && this.PlanogramValidationTemplateGroupId != null)
            {
                if (!other.PlanogramValidationTemplateGroupId.Equals(this.PlanogramValidationTemplateGroupId)) return false;
            }
            if ((other.PlanogramValidationTemplateGroupId == null && this.PlanogramValidationTemplateGroupId != null) ||
                (other.PlanogramValidationTemplateGroupId != null && this.PlanogramValidationTemplateGroupId == null))
            {
                return false;
            }
            // ExtendedData
            if (other.ExtendedData != null && this.ExtendedData != null)
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) return false;
            }
            if ((other.ExtendedData == null && this.ExtendedData != null) || (other.ExtendedData != null && this.ExtendedData == null))
            {
                return false;
            }
            
            return other.Field == this.Field
                   && other.Threshold1 == this.Threshold1
                   && other.Threshold2 == this.Threshold2
                   && other.Score1 == this.Score1
                   && other.Score2 == this.Score2
                   && other.Score3 == this.Score3
                   && other.AggregationType == this.AggregationType
                   && other.ResultType == this.ResultType
                   && other.ValidationType == this.ValidationType
                   && other.Criteria == this.Criteria;
        }
        #endregion
    }

    public class PlanogramValidationTemplateGroupMetricDtoKey
    {
        #region Properties

        public Byte AggregationType { get; set; }
        public String Field { get; set; }
        public Object PlanogramValidationTemplateGroupId { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return AggregationType.GetHashCode()
                + Field.GetHashCode()
                + PlanogramValidationTemplateGroupId.GetHashCode();
        }

       /// <summary>
        ///     Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramValidationTemplateGroupMetricDtoKey;
            if (other == null) return false;
            
            // PlanogramValidationTemplateGroupId
            if ((other.PlanogramValidationTemplateGroupId != null) && (this.PlanogramValidationTemplateGroupId != null))
            {
                if (!other.PlanogramValidationTemplateGroupId.Equals(this.PlanogramValidationTemplateGroupId)) { return false; }
            }
            if ((other.PlanogramValidationTemplateGroupId != null) && (this.PlanogramValidationTemplateGroupId == null)) { return false; }
            if ((other.PlanogramValidationTemplateGroupId == null) && (this.PlanogramValidationTemplateGroupId != null)) { return false; }

            return this.AggregationType == other.AggregationType
                && this.Field == other.Field;
        }

        #endregion
    }
}

