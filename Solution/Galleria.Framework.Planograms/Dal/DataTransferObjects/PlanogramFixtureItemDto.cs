﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24658 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25477 : A.Kuszyk
//  Added ForeignKey attributes to properties.
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27558 : L.Ineson
//  Removed IsBay
#endregion
#region Version History: CCM803
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramFixtureItemDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramFixtureItemDtoKey DtoKey
        {
            get { return new PlanogramFixtureItemDtoKey() { Id = this.Id }; }
        }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        [ForeignKey(typeof(PlanogramFixtureDto), typeof(IPlanogramFixtureDal), DeleteBehavior.Cascade)]
        public Object PlanogramFixtureId { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
        public Single Slope { get; set; }
        public Single Angle { get; set; }
        public Single Roll { get; set; }
        public Int16 BaySequenceNumber { get; set; }
        public Object ExtendedData { get; set; }

        #region Meta Data Properties

        public Int32? MetaComponentCount { get; set; }
        public Single? MetaTotalMerchandisableLinearSpace { get; set; }
        public Single? MetaTotalMerchandisableAreaSpace { get; set; }
        public Single? MetaTotalMerchandisableVolumetricSpace { get; set; }
        public Single? MetaTotalLinearWhiteSpace { get; set; }
        public Single? MetaTotalAreaWhiteSpace { get; set; }
        public Single? MetaTotalVolumetricWhiteSpace { get; set; }
        public Int32? MetaProductsPlaced { get; set; }
        public Int32? MetaNewProducts { get; set; }
        public Int32? MetaChangesFromPreviousCount { get; set; }
        public Int32? MetaChangeFromPreviousStarRating { get; set; }
        public Int32? MetaBlocksDropped { get; set; }
        public Int32? MetaNotAchievedInventory { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Int16? MetaAverageFacings { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Int32? MetaAverageUnits { get; set; }
        public Single? MetaMinDos { get; set; }
        public Single? MetaMaxDos { get; set; }
        public Single? MetaAverageDos { get; set; }
        public Single? MetaMinCases { get; set; }
        public Single? MetaAverageCases { get; set; }
        public Single? MetaMaxCases { get; set; }
        public Int16? MetaSpaceToUnitsIndex { get; set; }
        public Int32? MetaTotalComponentCollisions { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedDepth { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedHeight { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedWidth { get; set; }
        public Int32? MetaTotalPositionCollisions { get; set; }
        public Int16? MetaTotalFrontFacings { get; set; }
        public Single? MetaAverageFrontFacings { get; set; }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramFixtureItemDto other = obj as PlanogramFixtureItemDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                // PlanogramId
                if ((other.PlanogramId != null) && (this.PlanogramId != null))
                {
                    if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
                }
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }
                // PlanogramFixtureId
                if ((other.PlanogramFixtureId != null) && (this.PlanogramFixtureId != null))
                {
                    if (!other.PlanogramFixtureId.Equals(this.PlanogramFixtureId)) { return false; }
                }
                if ((other.PlanogramFixtureId != null) && (this.PlanogramFixtureId == null)) { return false; }
                if ((other.PlanogramFixtureId == null) && (this.PlanogramFixtureId != null)) { return false; }

                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Z != this.Z) { return false; }
                if (other.Slope != this.Slope) { return false; }
                if (other.Angle != this.Angle) { return false; }
                if (other.Roll != this.Roll) { return false; }
                if (other.BaySequenceNumber != this.BaySequenceNumber) { return false; }
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
                if (other.MetaComponentCount != this.MetaComponentCount) return false;
                if (other.MetaTotalMerchandisableLinearSpace != this.MetaTotalMerchandisableLinearSpace) return false;
                if (other.MetaTotalMerchandisableAreaSpace != this.MetaTotalMerchandisableAreaSpace) return false;
                if (other.MetaTotalMerchandisableVolumetricSpace != this.MetaTotalMerchandisableVolumetricSpace) return false;
                if (other.MetaTotalLinearWhiteSpace != this.MetaTotalLinearWhiteSpace) return false;
                if (other.MetaTotalAreaWhiteSpace != this.MetaTotalAreaWhiteSpace) return false;
                if (other.MetaTotalVolumetricWhiteSpace != this.MetaTotalVolumetricWhiteSpace) return false;
                if (other.MetaProductsPlaced != this.MetaProductsPlaced) return false;
                if (other.MetaNewProducts != this.MetaNewProducts) return false;
                if (other.MetaChangesFromPreviousCount != this.MetaChangesFromPreviousCount) return false;
                if (other.MetaChangeFromPreviousStarRating != this.MetaChangeFromPreviousStarRating) return false;
                if (other.MetaBlocksDropped != this.MetaBlocksDropped) return false;
                if (other.MetaNotAchievedInventory != this.MetaNotAchievedInventory) return false;
                if (other.MetaTotalFacings != this.MetaTotalFacings) return false;
                if (other.MetaAverageFacings != this.MetaAverageFacings) return false;
                if (other.MetaTotalUnits != this.MetaTotalUnits) return false;
                if (other.MetaAverageUnits != this.MetaAverageUnits) return false;
                if (other.MetaMinDos != this.MetaMinDos) return false;
                if (other.MetaMaxDos != this.MetaMaxDos) return false;
                if (other.MetaAverageDos != this.MetaAverageDos) return false;
                if (other.MetaMinCases != this.MetaMinCases) return false;
                if (other.MetaAverageCases != this.MetaAverageCases) return false;
                if (other.MetaMaxCases != this.MetaMaxCases) return false;
                if (other.MetaSpaceToUnitsIndex != this.MetaSpaceToUnitsIndex) return false;
                if (other.MetaTotalComponentCollisions != this.MetaTotalComponentCollisions) return false;
                if (other.MetaTotalComponentsOverMerchandisedDepth != this.MetaTotalComponentsOverMerchandisedDepth) return false;
                if (other.MetaTotalComponentsOverMerchandisedHeight != this.MetaTotalComponentsOverMerchandisedHeight) return false;
                if (other.MetaTotalComponentsOverMerchandisedWidth != this.MetaTotalComponentsOverMerchandisedWidth) return false;
                if (other.MetaTotalPositionCollisions != this.MetaTotalPositionCollisions) return false;
                if (other.MetaTotalFrontFacings != this.MetaTotalFrontFacings) return false;
                if (other.MetaAverageFrontFacings != this.MetaAverageFrontFacings) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramFixtureItemDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramFixtureItemDtoKey other = obj as PlanogramFixtureItemDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
