﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31963 : A.Silva
//  Added Display field.

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     <c>Data Transfer Object</c> for a <c>Planogram Comparison Field</c> model object.
    /// </summary>
    [Serializable]
    public class PlanogramComparisonFieldDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramComparisonFieldDtoKey DtoKey
        {
            get
            {
                return new PlanogramComparisonFieldDtoKey
                {
                    PlanogramComparisonId = PlanogramComparisonId,
                    ItemType = ItemType,
                    FieldPlaceholder = FieldPlaceholder
                };
            }
        }

        [ForeignKey(typeof(PlanogramComparisonDto), typeof(IPlanogramComparisonDal), DeleteBehavior.Cascade)]
        public Object PlanogramComparisonId { get; set; }

        public Byte ItemType { get; set; }

        public String FieldPlaceholder { get; set; }
        
        public String DisplayName { get; set; }

        public Int16 Number { get; set; }

        public Boolean Display { get; set; }

        public Object ExtendedData { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonFieldDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.ItemType, ItemType) &&
                   Equals(other.PlanogramComparisonId, PlanogramComparisonId) &&
                   Equals(other.ExtendedData, ExtendedData) &&
                   other.DisplayName == DisplayName &&
                   other.Number == Number &&
                   other.Display == Display &&
                   other.FieldPlaceholder == FieldPlaceholder;
        }

        #endregion
    }

    [Serializable]
    public class PlanogramComparisonFieldDtoKey
    {
        #region Properties

        public Object PlanogramComparisonId { get; set; }

        public Byte ItemType { get; set; }

        public String FieldPlaceholder { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return PlanogramComparisonId.GetHashCode() +
                   ItemType.GetHashCode() +
                   FieldPlaceholder.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonFieldDtoKey;
            return other != null &&
                   Equals(other.PlanogramComparisonId, PlanogramComparisonId) &&
                   other.ItemType == ItemType &&
                   other.FieldPlaceholder == FieldPlaceholder;
        }

        #endregion
    }
}