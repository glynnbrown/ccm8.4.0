﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25477 : A.Kuszyk
//  Added ForeignKey attributes to properties.
// V8-25787 : N.Foster
//  Added IsCarPark
// V8-25881 : A.Probyn
//  Updated so NumberOfSubComponents, NumberOfMerchandisableSubComponent, NumberOfShelfEdgeLabels are meta data
// V8-27058 : A.Probyn
//  Removed MetaNumberOfShelfEdgeLabels
// V8-27468 : L.Ineson
//  Added IsMerchandisedTopDown
#endregion
#region Version History: CCM820
// V8-30873 : L.Ineson
//  Removed IsCarPark.
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramComponentDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramComponentDtoKey DtoKey
        {
            get { return new PlanogramComponentDtoKey() { Id = this.Id }; }
        }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        public Object Mesh3DId { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object ImageIdFront { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object ImageIdBack { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object ImageIdTop { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object ImageIdBottom { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object ImageIdLeft { get; set; }
        [ForeignKey(typeof(PlanogramImageDto), typeof(IPlanogramImageDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Object ImageIdRight { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int16? MetaNumberOfSubComponents { get; set; }
        public Int16? MetaNumberOfMerchandisedSubComponents { get; set; }
        public Boolean IsMoveable { get; set; }
        public Boolean IsDisplayOnly { get; set; }
        public Boolean CanAttachShelfEdgeLabel { get; set; }
        public String RetailerReferenceCode { get; set; }
        public String BarCode { get; set; }
        public String Manufacturer { get; set; }
        public String ManufacturerPartName { get; set; }
        public String ManufacturerPartNumber { get; set; }
        public String SupplierName { get; set; }
        public String SupplierPartNumber { get; set; }
        public Single? SupplierCostPrice { get; set; }
        public Single? SupplierDiscount { get; set; }
        public Single? SupplierLeadTime { get; set; }
        public Int32? MinPurchaseQty { get; set; }
        public Single? WeightLimit { get; set; }
        public Single? Weight { get; set; }
        public Single? Volume { get; set; }
        public Single? Diameter { get; set; }
        public Int16? Capacity { get; set; }
        public Byte ComponentType { get; set; }
        public Boolean IsMerchandisedTopDown { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramComponentDto other = obj as PlanogramComponentDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null)) if (!other.Id.Equals(this.Id)) return false;
                if ((other.Id != null) && (this.Id == null)) return false;
                if ((other.Id == null) && (this.Id != null)) return false;
                // PlanogramId
                if ((other.PlanogramId != null) && (this.PlanogramId != null)) if (!other.PlanogramId.Equals(this.PlanogramId)) return false;
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) return false;
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) return false;
                // Mesh3DId
                if ((other.Mesh3DId != null) && (this.Mesh3DId != null)) if (!other.Mesh3DId.Equals(this.Mesh3DId)) return false;
                if ((other.Mesh3DId != null) && (this.Mesh3DId == null)) return false;
                if ((other.Mesh3DId == null) && (this.Mesh3DId != null)) return false;
                // ImageIdFront
                if ((other.ImageIdFront != null) && (this.ImageIdFront != null)) if (!other.ImageIdFront.Equals(this.ImageIdFront)) return false;
                if ((other.ImageIdFront != null) && (this.ImageIdFront == null)) return false;
                if ((other.ImageIdFront == null) && (this.ImageIdFront != null)) return false;
                // ImageIdBack
                if ((other.ImageIdBack != null) && (this.ImageIdBack != null)) if (!other.ImageIdBack.Equals(this.ImageIdBack)) return false;
                if ((other.ImageIdBack != null) && (this.ImageIdBack == null)) return false;
                if ((other.ImageIdBack == null) && (this.ImageIdBack != null)) return false;
                // ImageIdTop
                if ((other.ImageIdTop != null) && (this.ImageIdTop != null)) if (!other.ImageIdTop.Equals(this.ImageIdTop)) return false;
                if ((other.ImageIdTop != null) && (this.ImageIdTop == null)) return false;
                if ((other.ImageIdTop == null) && (this.ImageIdTop != null)) return false;
                // ImageIdBottom
                if ((other.ImageIdBottom != null) && (this.ImageIdBottom != null)) if (!other.ImageIdBottom.Equals(this.ImageIdBottom)) return false;
                if ((other.ImageIdBottom != null) && (this.ImageIdBottom == null)) return false;
                if ((other.ImageIdBottom == null) && (this.ImageIdBottom != null)) return false;
                // ImageIdLeft
                if ((other.ImageIdLeft != null) && (this.ImageIdLeft != null)) if (!other.ImageIdLeft.Equals(this.ImageIdLeft)) return false;
                if ((other.ImageIdLeft != null) && (this.ImageIdLeft == null)) return false;
                if ((other.ImageIdLeft == null) && (this.ImageIdLeft != null)) return false;
                // ImageIdRight
                if ((other.ImageIdRight != null) && (this.ImageIdRight != null)) if (!other.ImageIdRight.Equals(this.ImageIdRight)) return false;
                if ((other.ImageIdRight != null) && (this.ImageIdRight == null)) return false;
                if ((other.ImageIdRight == null) && (this.ImageIdRight != null)) return false;

                if (other.Name != this.Name) return false;
                if (other.Height != this.Height) return false;
                if (other.Width != this.Width) return false;
                if (other.Depth != this.Depth) return false;
                if (other.MetaNumberOfSubComponents != this.MetaNumberOfSubComponents) return false;
                if (other.MetaNumberOfMerchandisedSubComponents != this.MetaNumberOfMerchandisedSubComponents) return false;
                if (other.IsMoveable != this.IsMoveable) return false;
                if (other.IsDisplayOnly != this.IsDisplayOnly) return false;
                if (other.CanAttachShelfEdgeLabel != this.CanAttachShelfEdgeLabel) return false;
                if (other.RetailerReferenceCode != this.RetailerReferenceCode) return false;
                if (other.BarCode != this.BarCode) return false;
                if (other.Manufacturer != this.Manufacturer) return false;
                if (other.ManufacturerPartName != this.ManufacturerPartName) return false;
                if (other.ManufacturerPartNumber != this.ManufacturerPartNumber) return false;
                if (other.SupplierName != this.SupplierName) return false;
                if (other.SupplierPartNumber != this.SupplierPartNumber) return false;
                if (other.SupplierCostPrice != this.SupplierCostPrice) return false;
                if (other.SupplierDiscount != this.SupplierDiscount) return false;
                if (other.SupplierLeadTime != this.SupplierLeadTime) return false;
                if (other.MinPurchaseQty != this.MinPurchaseQty) return false;
                if (other.WeightLimit != this.WeightLimit) return false;
                if (other.Weight != this.Weight) return false;
                if (other.Volume != this.Volume) return false;
                if (other.Diameter != this.Diameter) return false;
                if (other.Capacity != this.Capacity) return false;
                if (other.ComponentType != this.ComponentType) return false;
                if (other.IsMerchandisedTopDown != this.IsMerchandisedTopDown) return false;
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null)) if (!other.ExtendedData.Equals(this.ExtendedData)) return false;
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) return false;
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramComponentDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramComponentDtoKey other = obj as PlanogramComponentDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null)) if (!other.Id.Equals(this.Id)) return false;
                if ((other.Id != null) && (this.Id == null)) return false;
                if ((other.Id == null) && (this.Id != null)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
