﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramSequenceGroupDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramSequenceGroupDtoKey DtoKey
        {
            get
            {
                return new PlanogramSequenceGroupDtoKey
                       {
                           PlanogramSequenceId = PlanogramSequenceId,
                           Colour = Colour,
                           Name = Name
                       };
            }
        }

        [ForeignKey(typeof (PlanogramSequenceDto), typeof (IPlanogramSequenceDal), DeleteBehavior.Cascade)]
        public Object PlanogramSequenceId { get; set; }

        public String Name { get; set; }
        public Int32 Colour { get; set; }
        public Object ExtendedData { get; set; }

        #endregion

        #region Equality Members

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        ///     <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />;
        ///     otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" />. </param>
        public override Boolean Equals(Object obj)
        {
            PlanogramSequenceGroupDto other = obj as PlanogramSequenceGroupDto;
            return other != null
                   && Equals(other.Id, Id)
                   && Equals(other.PlanogramSequenceId, PlanogramSequenceId)
                   && String.Equals(other.Name, Name)
                   && other.Colour == Colour
                   && Equals(other.ExtendedData, ExtendedData);
        }

        #endregion
    }

    public class PlanogramSequenceGroupDtoKey
    {
        #region Properties

        public Object PlanogramSequenceId { get; set; }

        public String Name { get; set; }

        public Int32 Colour { get; set; }

        #endregion

        #region Equality Members

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(PlanogramSequenceId).Hash(Name).Hash((Colour));
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramSequenceGroupDtoKey;
            return other != null 
                && Equals(other.PlanogramSequenceId, PlanogramSequenceId)
                && String.Equals(other.Name, Name)
                && other.Colour == Colour;
        }

        #endregion


    }
}