﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     <c>Data Transfer Object</c> for a <c>Planogram Comparison Result</c> model object.
    /// </summary>
    [Serializable]
    public class PlanogramComparisonResultDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramComparisonResultDtoKey DtoKey
        {
            get
            {
                return new PlanogramComparisonResultDtoKey
                {
                    PlanogramComparisonId = PlanogramComparisonId,
                    PlanogramName = PlanogramName
                };
            }
        }

        [ForeignKey(typeof(PlanogramComparisonDto), typeof(IPlanogramComparisonDal), DeleteBehavior.Cascade)]
        public Object PlanogramComparisonId { get; set; }

        public String PlanogramName { get; set; }

        public Byte Status { get; set; }

        public Object ExtendedData { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonResultDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.PlanogramComparisonId, PlanogramComparisonId) &&
                   Equals(other.ExtendedData, ExtendedData) &&
                   other.PlanogramName == PlanogramName &&
                   other.Status == Status;
        }

        #endregion
    }

    [Serializable]
    public class PlanogramComparisonResultDtoKey
    {
        #region Properties

        public Object PlanogramComparisonId { get; set; }

        public String PlanogramName { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return PlanogramComparisonId.GetHashCode() +
                   PlanogramName.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonResultDtoKey;
            return other != null &&
                   Equals(other.PlanogramComparisonId, PlanogramComparisonId) &&
                   other.PlanogramName == PlanogramName;
        }

        #endregion
    }
}