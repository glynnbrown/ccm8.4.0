﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26426 : A.Kuszyk
//  Created.
// V8-26799 : A.Kuszyk
//  Removed LocationCode property.
// V8-27058 : A.Probyn
//  Updated Gtin to be GTIN
#endregion
#region Version History: CCM820
// V8-30762 : I.George
//  Added MetaData property
// V8-30705 : A.Probyn
//  Added DelistedDueToAssortmentRule
#endregion
// CCM-18621 : A.Silva
//  GTIN property now matches the model property name Gtin (Fixed failing test MatchesDto).
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramAssortmentProductDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAssortmentProductDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentProductDtoKey
                {
                    PlanogramAssortmentId = this.PlanogramAssortmentId,
                    Gtin = this.Gtin
                };
            }
        }
        [ForeignKey(typeof(PlanogramAssortmentDto), typeof(IPlanogramAssortmentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentId { get; set; }
        public String Gtin { get; set; }
        public String Name { get; set; }
        public Boolean IsRanged { get; set; }
        public Int16 Rank { get; set; }
        public String Segmentation { get; set; }
        public Byte Facings { get; set; }
        public Int16 Units { get; set; }
        public Byte ProductTreatmentType { get; set; }
        public Byte ProductLocalizationType { get; set; }
        public String Comments { get; set; }
        public Byte? ExactListFacings { get; set; }
        public Int16? ExactListUnits { get; set; }
        public Byte? PreserveListFacings { get; set; }
        public Int16? PreserveListUnits { get; set; }
        public Byte? MaxListFacings { get; set; }
        public Int16? MaxListUnits { get; set; }
        public Byte? MinListFacings { get; set; }
        public Int16? MinListUnits { get; set; }
        public String FamilyRuleName { get; set; }
        public Byte FamilyRuleType { get; set; }
        public Byte? FamilyRuleValue { get; set; }
        public Boolean IsPrimaryRegionalProduct { get; set; }
        public Object ExtendedData { get; set; }
        public Boolean DelistedDueToAssortmentRule { get; set; }
        public Boolean? MetaIsProductPlacedOnPlanogram { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentProductDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            if (other.Gtin != this.Gtin) { return false; }
            if (other.Name != this.Name) { return false; }
            if (other.IsRanged != this.IsRanged) { return false; }
            if (other.Rank != this.Rank) { return false; }
            if (other.Segmentation != this.Segmentation) { return false; }
            if (other.Facings != this.Facings) { return false; }
            if (other.Units != this.Units) { return false; }
            if (other.ProductTreatmentType != this.ProductTreatmentType) { return false; }
            if (other.ProductLocalizationType != this.ProductLocalizationType) { return false; }
            if (other.Comments != this.Comments) { return false; }
            if (other.ExactListFacings != this.ExactListFacings) { return false; }
            if (other.ExactListUnits != this.ExactListUnits) { return false; }
            if (other.PreserveListFacings != this.PreserveListFacings) { return false; }
            if (other.PreserveListUnits != this.PreserveListUnits) { return false; }
            if (other.MaxListFacings != this.MaxListFacings) { return false; }
            if (other.MaxListUnits != this.MaxListUnits) { return false; }
            if (other.MinListFacings != this.MinListFacings) { return false; }
            if (other.MinListUnits != this.MinListUnits) { return false; }
            if (other.FamilyRuleName != this.FamilyRuleName) { return false; }
            if (other.FamilyRuleType != this.FamilyRuleType) { return false; }
            if (other.FamilyRuleValue != this.FamilyRuleValue) { return false; }
            if (other.IsPrimaryRegionalProduct != this.IsPrimaryRegionalProduct) { return false; }
            if (other.DelistedDueToAssortmentRule != this.DelistedDueToAssortmentRule) { return false; }
            if (other.MetaIsProductPlacedOnPlanogram != this.MetaIsProductPlacedOnPlanogram) { return false; }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentProductDtoKey
    {
        #region Properties
        public Object PlanogramAssortmentId { get; set; }
        public String Gtin { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return PlanogramAssortmentId.GetHashCode() + Gtin.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentProductDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }

            return this.Gtin == other.Gtin;
        }
        #endregion
    }
}


