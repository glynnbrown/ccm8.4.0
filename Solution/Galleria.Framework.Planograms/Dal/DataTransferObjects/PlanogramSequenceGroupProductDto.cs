﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#region Version History : CCM830
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroupId
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramSequenceGroupProductDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramSequenceGroupProductDtoKey DtoKey
        {
            get
            {
                return new PlanogramSequenceGroupProductDtoKey {PlanogramSequenceGroupId = PlanogramSequenceGroupId};
            }
        }

        [ForeignKey(typeof (PlanogramSequenceGroupDto), typeof (IPlanogramSequenceGroupDal), DeleteBehavior.Cascade)]
        public Object PlanogramSequenceGroupId { get; set; }
        public Object PlanogramSequenceGroupSubGroupId { get; set; }
        public String Gtin { get; set; }
        public Int32 SequenceNumber { get; set; }
        public Object ExtendedData { get; set; }

        #endregion

        #region Equality Members

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            PlanogramSequenceGroupProductDto other = obj as PlanogramSequenceGroupProductDto;
            return other != null
                && Equals(other.Id, Id)
                && Equals(other.PlanogramSequenceGroupId, PlanogramSequenceGroupId)
                && Equals(other.PlanogramSequenceGroupSubGroupId, PlanogramSequenceGroupSubGroupId)
                && Equals(other.Gtin, Gtin)
                && Equals(other.SequenceNumber, SequenceNumber)
                && Equals(other.ExtendedData, ExtendedData);
        }

        #endregion
    }

    public class PlanogramSequenceGroupProductDtoKey
    {
        #region Properties

        public Object PlanogramSequenceGroupId { get; set; }

        #endregion

        #region Equality Members

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return PlanogramSequenceGroupId.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            PlanogramSequenceGroupProductDtoKey other = obj as PlanogramSequenceGroupProductDtoKey;
            return other != null
                && Equals(other.PlanogramSequenceGroupId, PlanogramSequenceGroupId);
        }

        #endregion
    }
}
