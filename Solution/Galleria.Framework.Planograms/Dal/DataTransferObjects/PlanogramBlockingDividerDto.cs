﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#region Version History : CCM 830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramBlockingDivider Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramBlockingDividerDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramBlockingDividerDtoKey DtoKey
        {
            get
            {
                return new PlanogramBlockingDividerDtoKey()
                {
                    Id = this.Id
                };
            }
        }


        [ForeignKey(typeof(PlanogramBlockingDto), typeof(IPlanogramBlockingDal), DeleteBehavior.Cascade)]
        public Object PlanogramBlockingId { get; set; }

        public Byte Type { get; set; }
        public Byte Level { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Length { get; set; }
        public Boolean IsSnapped { get; set; }
        public Object ExtendedData { get; set; }
        public Boolean IsLimited { get; set; }
        public Single LimitedPercentage { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramBlockingDividerDto other = obj as PlanogramBlockingDividerDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                // PlanogramId
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId != null))
                {
                    if (!other.PlanogramBlockingId.Equals(this.PlanogramBlockingId)) { return false; }
                }
                if ((other.PlanogramBlockingId != null) && (this.PlanogramBlockingId == null)) { return false; }
                if ((other.PlanogramBlockingId == null) && (this.PlanogramBlockingId != null)) { return false; }

                //other
                if (other.Type != this.Type) { return false; }
                if (other.Level != this.Level) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Length != this.Length) { return false; }
                if (other.IsSnapped != this.IsSnapped) { return false; }
                if (other.IsLimited != this.IsLimited) { return false; }
                if (other.LimitedPercentage != this.LimitedPercentage) { return false; }

                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramBlockingDividerDtoKey
    {

        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramBlockingDividerDtoKey other = obj as PlanogramBlockingDividerDtoKey;
            if (other != null)
            {

                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
