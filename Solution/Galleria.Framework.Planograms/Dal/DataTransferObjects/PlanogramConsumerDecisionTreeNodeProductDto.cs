﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)
// V8-26956 : L.Luong
//   Modified ProductGtin to PlanogramProductId
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     PlanogramCustomerDecisionTreeNodeProduct Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramConsumerDecisionTreeNodeProductDto
    {
        #region Properties

        public Object Id { get; set; }
        public PlanogramConsumerDecisionTreeNodeProductDtoKey DtoKey
        {
            get
            {
                return new PlanogramConsumerDecisionTreeNodeProductDtoKey()
                {
                    PlanogramConsumerDecisionTreeNodeId = this.PlanogramConsumerDecisionTreeNodeId,
                    PlanogramProductId = this.PlanogramProductId
                };
            }
        }

        [ForeignKey(typeof(PlanogramConsumerDecisionTreeNodeDto), typeof(IPlanogramConsumerDecisionTreeNodeDal), DeleteBehavior.MoveUpToRoot)]
        public Object PlanogramConsumerDecisionTreeNodeId { get; set; }
        [ForeignKey(typeof(PlanogramProductDto), typeof(IPlanogramProductDal), DeleteBehavior.MoveUpToRoot)]
        public Object PlanogramProductId { get; set; }
        public Object ExtendedData { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramConsumerDecisionTreeNodeProductDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }

            // PlanogramProductId
            if ((other.PlanogramProductId != null) && (this.PlanogramProductId != null))
            {
                if (!other.PlanogramProductId.Equals(this.PlanogramProductId)) { return false; }
            }
            if ((other.PlanogramProductId != null) && (this.PlanogramProductId == null)) { return false; }
            if ((other.PlanogramProductId == null) && (this.PlanogramProductId != null)) { return false; }

            // PlanogramConsumerDecisionTreeNodeId
            if ((other.PlanogramConsumerDecisionTreeNodeId != null) && (this.PlanogramConsumerDecisionTreeNodeId != null))
            {
                if (!other.PlanogramConsumerDecisionTreeNodeId.Equals(this.PlanogramConsumerDecisionTreeNodeId)) { return false; }
            }
            if ((other.PlanogramConsumerDecisionTreeNodeId != null) && (this.PlanogramConsumerDecisionTreeNodeId == null)) { return false; }
            if ((other.PlanogramConsumerDecisionTreeNodeId == null) && (this.PlanogramConsumerDecisionTreeNodeId != null)) { return false; }

            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            return true;
        }

        #endregion
    }

    public class PlanogramConsumerDecisionTreeNodeProductDtoKey
    {
        #region Properties
        public Object PlanogramConsumerDecisionTreeNodeId { get; set; }
        public Object PlanogramProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                PlanogramConsumerDecisionTreeNodeId.GetHashCode() +
                PlanogramProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            var other = obj as PlanogramConsumerDecisionTreeNodeProductDtoKey;
            if (other == null) return false;

            // PlanogramConsumerDecisionTreeNodeId
            if ((other.PlanogramConsumerDecisionTreeNodeId != null) && (this.PlanogramConsumerDecisionTreeNodeId != null))
            {
                if (!other.PlanogramConsumerDecisionTreeNodeId.Equals(this.PlanogramConsumerDecisionTreeNodeId)) { return false; }
            }
            if ((other.PlanogramConsumerDecisionTreeNodeId != null) && (this.PlanogramConsumerDecisionTreeNodeId == null)) { return false; }
            if ((other.PlanogramConsumerDecisionTreeNodeId == null) && (this.PlanogramConsumerDecisionTreeNodeId != null)) { return false; }

            // PlanogramConsumerDecisionTreeNodeId
            if ((other.PlanogramProductId != null) && (this.PlanogramProductId != null))
            {
                if (!other.PlanogramProductId.Equals(this.PlanogramProductId)) { return false; }
            }
            if ((other.PlanogramProductId != null) && (this.PlanogramProductId == null)) { return false; }
            if ((other.PlanogramProductId == null) && (this.PlanogramProductId != null)) { return false; }

            return true;
        }
        #endregion
    }
}
