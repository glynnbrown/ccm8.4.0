﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24658 : K.Pickup
//  Fixed Equals() methods.  The comparisons for all Object-valued properties were comparing references rather than
//  values.
// V8-25477 : A.Kuszyk
//  Added ForeignKey attributes to properties.
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramAnnotationDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAnnotationDtoKey DtoKey
        {
            get { return new PlanogramAnnotationDtoKey() { Id = this.Id }; }
        }
        [ForeignKey(typeof(PlanogramDto),typeof(IPlanogramDal),DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        [ForeignKey(typeof(PlanogramFixtureItemDto), typeof(IPlanogramFixtureItemDal), DeleteBehavior.Cascade)]
        public Object PlanogramFixtureItemId { get; set; }
        [ForeignKey(typeof(PlanogramFixtureAssemblyDto), typeof(IPlanogramFixtureAssemblyDal), DeleteBehavior.Cascade)]
        public Object PlanogramFixtureAssemblyId { get; set; }
        [ForeignKey(typeof(PlanogramFixtureComponentDto), typeof(IPlanogramFixtureComponentDal), DeleteBehavior.Cascade)]
        public Object PlanogramFixtureComponentId { get; set; }
        [ForeignKey(typeof(PlanogramAssemblyComponentDto), typeof(IPlanogramAssemblyComponentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssemblyComponentId { get; set; }
        [ForeignKey(typeof(PlanogramSubComponentDto), typeof(IPlanogramSubComponentDal), DeleteBehavior.Cascade)]
        public Object PlanogramSubComponentId { get; set; }
        [ForeignKey(typeof(PlanogramPositionDto), typeof(IPlanogramPositionDal), DeleteBehavior.Cascade)]
        public Object PlanogramPositionId { get; set; }
        public Byte AnnotationType { get; set; }
        public String Text { get; set; }
        public Single? X { get; set; }
        public Single? Y { get; set; }
        public Single? Z { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int32 BorderColour { get; set; }
        public Single BorderThickness { get; set; }
        public Int32 BackgroundColour { get; set; }
        public Int32 FontColour { get; set; }
        public Single FontSize { get; set; }
        public String FontName { get; set; }
        public Boolean CanReduceFontToFit { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramAnnotationDto other = obj as PlanogramAnnotationDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                // PlanogramId
                if ((other.PlanogramId != null) && (this.PlanogramId != null))
                {
                    if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
                }
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }
                // PlanogramFixtureItemId
                if ((other.PlanogramFixtureItemId != null) && (this.PlanogramFixtureItemId != null))
                {
                    if (!other.PlanogramFixtureItemId.Equals(this.PlanogramFixtureItemId)) { return false; }
                }
                if ((other.PlanogramFixtureItemId != null) && (this.PlanogramFixtureItemId == null)) { return false; }
                if ((other.PlanogramFixtureItemId == null) && (this.PlanogramFixtureItemId != null)) { return false; }
                // PlanogramFixtureAssemblyId
                if ((other.PlanogramFixtureAssemblyId != null) && (this.PlanogramFixtureAssemblyId != null))
                {
                    if (!other.PlanogramFixtureAssemblyId.Equals(this.PlanogramFixtureAssemblyId)) { return false; }
                }
                if ((other.PlanogramFixtureAssemblyId != null) && (this.PlanogramFixtureAssemblyId == null)) { return false; }
                if ((other.PlanogramFixtureAssemblyId == null) && (this.PlanogramFixtureAssemblyId != null)) { return false; }
                // PlanogramFixtureComponentId
                if ((other.PlanogramFixtureComponentId != null) && (this.PlanogramFixtureComponentId != null))
                {
                    if (!other.PlanogramFixtureComponentId.Equals(this.PlanogramFixtureComponentId)) { return false; }
                }
                if ((other.PlanogramFixtureComponentId != null) && (this.PlanogramFixtureComponentId == null)) { return false; }
                if ((other.PlanogramFixtureComponentId == null) && (this.PlanogramFixtureComponentId != null)) { return false; }
                // PlanogramAssemblyComponentId
                if ((other.PlanogramAssemblyComponentId != null) && (this.PlanogramAssemblyComponentId != null))
                {
                    if (!other.PlanogramAssemblyComponentId.Equals(this.PlanogramAssemblyComponentId)) { return false; }
                }
                if ((other.PlanogramAssemblyComponentId != null) && (this.PlanogramAssemblyComponentId == null)) { return false; }
                if ((other.PlanogramAssemblyComponentId == null) && (this.PlanogramAssemblyComponentId != null)) { return false; }
                // PlanogramSubComponentId
                if ((other.PlanogramSubComponentId != null) && (this.PlanogramSubComponentId != null))
                {
                    if (!other.PlanogramSubComponentId.Equals(this.PlanogramSubComponentId)) { return false; }
                }
                if ((other.PlanogramSubComponentId != null) && (this.PlanogramSubComponentId == null)) { return false; }
                if ((other.PlanogramSubComponentId == null) && (this.PlanogramSubComponentId != null)) { return false; }
                // PlanogramPositionId
                if ((other.PlanogramPositionId != null) && (this.PlanogramPositionId != null))
                {
                    if (!other.PlanogramPositionId.Equals(this.PlanogramPositionId)) { return false; }
                }
                if ((other.PlanogramPositionId != null) && (this.PlanogramPositionId == null)) { return false; }
                if ((other.PlanogramPositionId == null) && (this.PlanogramPositionId != null)) { return false; }

                if (other.AnnotationType != this.AnnotationType) { return false; }
                if (other.Text != this.Text) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Z != this.Z) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.BorderColour != this.BorderColour) { return false; }
                if (other.BorderThickness != this.BorderThickness) { return false; }
                if (other.BackgroundColour != this.BackgroundColour) { return false; }
                if (other.FontColour != this.FontColour) { return false; }
                if (other.FontSize != this.FontSize) { return false; }
                if (other.FontName != this.FontName) { return false; }
                if (other.CanReduceFontToFit != this.CanReduceFontToFit) { return false; }
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramAnnotationDtoKey
    {

        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramAnnotationDtoKey other = obj as PlanogramAnnotationDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
