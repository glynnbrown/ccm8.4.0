﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27647 : L.Luong 
//   Created.
#endregion
#region Version History: (CCM 8.20)
// V8-30773 : J.Pickup 
//   InventoryMetricType added.
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    ///     PlanogramEvInventory Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramInventoryDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramInventoryDtoKey DtoKey
        {
            get { return new PlanogramInventoryDtoKey { PlanogramId = this.PlanogramId }; }
        }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }

        public Single DaysOfPerformance { get; set; }
        public Single MinCasePacks { get; set; }
        public Single MinDos { get; set; }
        public Single MinShelfLife { get; set; }
        public Single MinDeliveries { get; set; }

        public Boolean IsCasePacksValidated { get; set; }
        public Boolean IsDosValidated { get; set; }
        public Boolean IsShelfLifeValidated { get; set; }
        public Boolean IsDeliveriesValidated { get; set; }

        public Object ExtendedData { get; set; }

        public Byte InventoryMetricType { get; set; }
        #endregion

        #region Methods

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(object obj)
        {
            var other = obj as PlanogramInventoryDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                // PlanogramId
                if ((other.PlanogramId != null) && (this.PlanogramId != null))
                {
                    if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
                }
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }

                if (other.DaysOfPerformance != this.DaysOfPerformance) return false;
                if (other.MinCasePacks != this.MinCasePacks) return false;
                if (other.MinDos != this.MinDos) return false;
                if (other.MinShelfLife != this.MinShelfLife) return false;
                if (other.MinDeliveries != this.MinDeliveries) return false;
                if (other.IsCasePacksValidated != this.IsCasePacksValidated) return false;
                if (other.IsDosValidated != this.IsDosValidated) return false;
                if (other.IsShelfLifeValidated != this.IsShelfLifeValidated) return false;
                if (other.IsDeliveriesValidated != this.IsDeliveriesValidated) return false;
                if (other.InventoryMetricType != this.InventoryMetricType) return false;
            }
            else 
            { 
                return false; 
            }
            return true;
        }
        #endregion
    }

    public class PlanogramInventoryDtoKey
    {
        #region Properties
        public Object PlanogramId { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return PlanogramId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramInventoryDtoKey;
            if (other == null) return false;

            // PlanogramId
            if ((other.PlanogramId != null) && (this.PlanogramId != null))
            {
                if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
            }
            if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
            if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }

            return true;
        }
        #endregion
    }
}
