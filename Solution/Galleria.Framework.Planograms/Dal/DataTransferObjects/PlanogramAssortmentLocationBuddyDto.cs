﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentLocationBuddy Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramAssortmentLocationBuddyDto
    {
        #region Properties
        public Object Id { get; set; }
        [ForeignKey(typeof(PlanogramAssortmentDto), typeof(IPlanogramAssortmentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentId { get; set; }
        public PlanogramAssortmentLocationBuddyDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentLocationBuddyDtoKey
                {
                    PlanogramAssortmentId = this.PlanogramAssortmentId,
                    LocationCode = this.LocationCode
                };
            }
        }
        public String LocationCode { get; set; }
        public Byte TreatmentType { get; set; }
        public String S1LocationCode { get; set; }
        public Single? S1Percentage { get; set; }
        public String S2LocationCode { get; set; }
        public Single? S2Percentage { get; set; }
        public String S3LocationCode { get; set; }
        public Single? S3Percentage { get; set; }
        public String S4LocationCode { get; set; }
        public Single? S4Percentage { get; set; }
        public String S5LocationCode { get; set; }
        public Single? S5Percentage { get; set; }

        public Object ExtendedData { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramAssortmentLocationBuddyDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }

            if (other.LocationCode != this.LocationCode) { return false; }
            if (other.TreatmentType != this.TreatmentType) { return false; }
            if (other.S1LocationCode != this.S1LocationCode) { return false; }
            if (other.S1Percentage != this.S1Percentage) { return false; }
            if (other.S2LocationCode != this.S2LocationCode) { return false; }
            if (other.S2Percentage != this.S2Percentage) { return false; }
            if (other.S3LocationCode != this.S3LocationCode) { return false; }
            if (other.S3Percentage != this.S3Percentage) { return false; }
            if (other.S4LocationCode != this.S4LocationCode) { return false; }
            if (other.S4Percentage != this.S4Percentage) { return false; }
            if (other.S5LocationCode != this.S5LocationCode) { return false; }
            if (other.S5Percentage != this.S5Percentage) { return false; }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentLocationBuddyDtoKey
    {
        #region Properties
        public String LocationCode { get; set; }
        public Object PlanogramAssortmentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationCode.GetHashCode() +
                PlanogramAssortmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramAssortmentLocationBuddyDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }

            return this.LocationCode == other.LocationCode;
        }
        #endregion
    }
}

