﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup / A. Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24658 : K.Pickup
//  Added DtoKey.
// V8-25479 : A.Kuszyk
//  Added DateCreated, DateModified and DateDeleted propeties.
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26709 : L.Ineson
//  Removed old unit of measure property.
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27411 : M.Pettit
//  Added UserName (owner) property
// V8-27783 : M.Brumby
//	Added UniqueContentReference

#endregion

#region Version History: CCM802

// V8-28987 : I.George
// Added LocationCode, LocationName, ClusterSchemeName and ClusterName 

#endregion

#region Version History: CCM803

// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-28242 : M.Shelley
//  Added Status, DateWIP, DateApproved, DateArchived 

#endregion

#region Version History: CCM810

// V8-29844 : L.Ineson
//  Added more metadata
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30036 : L.Ineson
//  Added MetaPercentOfProductsNotAchievedInventory 
// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents

#endregion

#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to <Single?>.

#endregion

#region Version History : CCM 820
// V8-30728 : A.Kuszyk
//  Added HighlightSequenceStrategy field.
// V8-30705 : A.Probyn
//  Added new meta data fields relating to the assortment rules.
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
#endregion

#region Version History : CCM 830
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added MetaCountOfProductsBuddied
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramDto
    {
        #region Properties
        public Object Id { get; set; }
        public String UniqueContentReference { get; set; } //String instead of Guid as GalleriaBinaryFile.ItemPropertyType.Guid doesn't exist
        public PlanogramDtoKey DtoKey
        {
            get { return new PlanogramDtoKey() { Id = this.Id }; }
        }
        public Object PackageId { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public String UserName { get; set; }
        public Byte LengthUnitsOfMeasure { get; set; }
        public Byte AreaUnitsOfMeasure { get; set; }
        public Byte VolumeUnitsOfMeasure { get; set; }
        public Byte WeightUnitsOfMeasure { get; set; }
        public Byte AngleUnitsOfMeasure { get; set; }
        public Byte CurrencyUnitsOfMeasure { get; set; }
        public Byte ProductPlacementX { get; set; }
        public Byte ProductPlacementY { get; set; }
        public Byte ProductPlacementZ { get; set; }
        public Object ExtendedData { get; set; }
        public String CategoryCode { get; set; }
        public String CategoryName { get; set; }
        public String SourcePath { get; set; }
        public String LocationCode { get; set; }
        public String LocationName { get; set; }
        public String ClusterSchemeName { get; set; }
        public String ClusterName { get; set; }

        public Byte Status { get; set; }
        public DateTime? DateWip { get; set; }
        public DateTime? DateApproved { get; set; }
        public DateTime? DateArchived { get; set; }
        public Byte PlanogramType { get; set; }
        public String HighlightSequenceStrategy { get; set; }

        #region Meta Data Properties

        public Int32? MetaBayCount { get; set; }
        public Int32? MetaUniqueProductCount { get; set; }
        public Int32? MetaComponentCount { get; set; }
        public Single? MetaTotalMerchandisableLinearSpace { get; set; }
        public Single? MetaTotalMerchandisableAreaSpace { get; set; }
        public Single? MetaTotalMerchandisableVolumetricSpace { get; set; }
        public Single? MetaTotalLinearWhiteSpace { get; set; }
        public Single? MetaTotalAreaWhiteSpace { get; set; }
        public Single? MetaTotalVolumetricWhiteSpace { get; set; }
        public Int32? MetaProductsPlaced { get; set; }
        public Int32? MetaProductsUnplaced { get; set; }
        public Int32? MetaNewProducts { get; set; }
        public Int32? MetaChangesFromPreviousCount { get; set; }
        public Int32? MetaChangeFromPreviousStarRating { get; set; }
        public Int32? MetaBlocksDropped { get; set; }
        public Int32? MetaNotAchievedInventory { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Single? MetaAverageFacings { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Single? MetaAverageUnits { get; set; }
        public Single? MetaMinDos { get; set; }
        public Single? MetaMaxDos { get; set; }
        public Single? MetaAverageDos { get; set; }
        public Single? MetaMinCases { get; set; }
        public Single? MetaAverageCases { get; set; }
        public Single? MetaMaxCases { get; set; }
        public Int32? MetaTotalComponentCollisions { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedDepth { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedHeight { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedWidth { get; set; }
        public Boolean? MetaHasComponentsOutsideOfFixtureArea { get; set; }
        public Int32? MetaTotalPositionCollisions { get; set; }
        public Int16? MetaTotalFrontFacings { get; set; }
        public Single? MetaAverageFrontFacings { get; set; }
        public Int32? MetaNoOfErrors { get; set; }
        public Int32? MetaNoOfWarnings { get; set; }
        public Int32? MetaNoOfInformationEvents { get; set; }
        public Int32? MetaNoOfDebugEvents { get; set; }
        public Int32? MetaTotalErrorScore { get; set; }
        public Byte? MetaHighestErrorScore { get; set; }
        public Int32? MetaCountOfProductNotAchievedCases { get; set; }
        public Int32? MetaCountOfProductNotAchievedDOS { get; set; }
        public Int32? MetaCountOfProductOverShelfLifePercent { get; set; }
        public Int32? MetaCountOfProductNotAchievedDeliveries { get; set; }
        public Single? MetaPercentOfProductNotAchievedCases { get; set; }
        public Single? MetaPercentOfProductNotAchievedDOS { get; set; }
        public Single? MetaPercentOfProductOverShelfLifePercent { get; set; }
        public Single? MetaPercentOfProductNotAchievedDeliveries { get; set; }
        public Int32? MetaCountOfPositionsOutsideOfBlockSpace { get; set; }
        public Single? MetaPercentOfPositionsOutsideOfBlockSpace { get; set; }
        public Single? MetaPercentOfPlacedProductsRecommendedInAssortment { get; set; }
        public Int32? MetaCountOfPlacedProductsRecommendedInAssortment { get; set; }
        public Int32? MetaCountOfPlacedProductsNotRecommendedInAssortment { get; set; }
        public Int32? MetaCountOfRecommendedProductsInAssortment { get; set; }
        public Single? MetaPercentOfProductNotAchievedInventory { get; set; }
        public Int16? MetaNumberOfAssortmentRulesBroken { get; set; }
        public Int16? MetaNumberOfProductRulesBroken { get; set; }
        public Int16? MetaNumberOfFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfInheritanceRulesBroken { get; set; }
        public Int16? MetaNumberOfLocalProductRulesBroken { get; set; }
        public Int16? MetaNumberOfDistributionRulesBroken { get; set; }
        public Int16? MetaNumberOfCoreRulesBroken { get; set; }
        public Single? MetaPercentageOfAssortmentRulesBroken { get; set; }
        public Single? MetaPercentageOfProductRulesBroken { get; set; }
        public Single? MetaPercentageOfFamilyRulesBroken { get; set; }
        public Single? MetaPercentageOfInheritanceRulesBroken { get; set; }
        public Single? MetaPercentageOfLocalProductRulesBroken { get; set; }
        public Single? MetaPercentageOfDistributionRulesBroken { get; set; }
        public Single? MetaPercentageOfCoreRulesBroken { get; set; }
        public Int16? MetaNumberOfDelistProductRulesBroken { get; set; }
        public Int16? MetaNumberOfForceProductRulesBroken { get; set; }
        public Int16? MetaNumberOfPreserveProductRulesBroken { get; set; }
        public Int16? MetaNumberOfMinimumHurdleProductRulesBroken { get; set; }
        public Int16? MetaNumberOfMaximumProductFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfMinimumProductFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfDependencyFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfDelistFamilyRulesBroken { get; set; }
        public Int16? MetaCountOfProductsBuddied { get; set; }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramDto other = obj as PlanogramDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id == null)) return false;
                if ((other.Id == null) && (this.Id != null)) return false;
                if ((other.Id != null) && (this.Id != null)) if (!other.Id.Equals(this.Id)) return false;

                // PackageId
                if ((other.PackageId != null) && (this.PackageId == null)) return false;
                if ((other.PackageId == null) && (this.PackageId != null)) return false;
                if ((other.PackageId != null) && (this.PackageId != null)) if (!other.PackageId.Equals(this.PackageId)) return false;

                if (other.UniqueContentReference != this.UniqueContentReference) return false;
                if (other.Name != this.Name) return false;
                if (other.Height != this.Height) return false;
                if (other.Width != this.Width) return false;
                if (other.Depth != this.Depth) return false;
                if (other.UserName != this.UserName) return false;
                if (other.LengthUnitsOfMeasure != this.LengthUnitsOfMeasure) return false;
                if (other.AreaUnitsOfMeasure != this.AreaUnitsOfMeasure) return false;
                if (other.VolumeUnitsOfMeasure != this.VolumeUnitsOfMeasure) return false;
                if (other.WeightUnitsOfMeasure != this.WeightUnitsOfMeasure) return false;
                if (other.AngleUnitsOfMeasure != this.AngleUnitsOfMeasure) return false;
                if (other.CurrencyUnitsOfMeasure != this.CurrencyUnitsOfMeasure) return false;
                if (other.ProductPlacementX != this.ProductPlacementX) return false;
                if (other.ProductPlacementY != this.ProductPlacementY) return false;
                if (other.ProductPlacementZ != this.ProductPlacementZ) return false;

                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) return false;
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) return false;
                if ((other.ExtendedData != null) && (this.ExtendedData != null)) if (!other.ExtendedData.Equals(this.ExtendedData)) return false;

                if (other.CategoryCode != this.CategoryCode) return false;
                if (other.CategoryName != this.CategoryName) return false;
                if (other.SourcePath != this.SourcePath) return false;
                if (other.MetaBayCount != this.MetaBayCount) return false;
                if (other.MetaUniqueProductCount != this.MetaUniqueProductCount) return false;
                if (other.MetaComponentCount != this.MetaComponentCount) return false;
                if (other.MetaTotalMerchandisableLinearSpace != this.MetaTotalMerchandisableLinearSpace) return false;
                if (other.MetaTotalMerchandisableAreaSpace != this.MetaTotalMerchandisableAreaSpace) return false;
                if (other.MetaTotalMerchandisableVolumetricSpace != this.MetaTotalMerchandisableVolumetricSpace) return false;
                if (other.MetaTotalLinearWhiteSpace != this.MetaTotalLinearWhiteSpace) return false;
                if (other.MetaTotalAreaWhiteSpace != this.MetaTotalAreaWhiteSpace) return false;
                if (other.MetaTotalVolumetricWhiteSpace != this.MetaTotalVolumetricWhiteSpace) return false;
                if (other.MetaProductsPlaced != this.MetaProductsPlaced) return false;
                if (other.MetaProductsUnplaced != this.MetaProductsUnplaced) return false;
                if (other.MetaNewProducts != this.MetaNewProducts) return false;
                if (other.MetaChangesFromPreviousCount != this.MetaChangesFromPreviousCount) return false;
                if (other.MetaChangeFromPreviousStarRating != this.MetaChangeFromPreviousStarRating) return false;
                if (other.MetaBlocksDropped != this.MetaBlocksDropped) return false;
                if (other.MetaNotAchievedInventory != this.MetaNotAchievedInventory) return false;
                if (other.MetaTotalFacings != this.MetaTotalFacings) return false;
                if (other.MetaAverageFacings != this.MetaAverageFacings) return false;
                if (other.MetaTotalUnits != this.MetaTotalUnits) return false;
                if (other.MetaAverageUnits != this.MetaAverageUnits) return false;
                if (other.MetaMinDos != this.MetaMinDos) return false;
                if (other.MetaMaxDos != this.MetaMaxDos) return false;
                if (other.MetaAverageDos != this.MetaAverageDos) return false;
                if (other.MetaMinCases != this.MetaMinCases) return false;
                if (other.MetaAverageCases != this.MetaAverageCases) return false;
                if (other.MetaMaxCases != this.MetaMaxCases) return false;
                if (other.MetaTotalComponentCollisions != this.MetaTotalComponentCollisions) return false;
                if (other.MetaTotalComponentsOverMerchandisedDepth != this.MetaTotalComponentsOverMerchandisedDepth) return false;
                if (other.MetaTotalComponentsOverMerchandisedHeight != this.MetaTotalComponentsOverMerchandisedHeight) return false;
                if (other.MetaTotalComponentsOverMerchandisedWidth != this.MetaTotalComponentsOverMerchandisedWidth) return false;
                if (other.MetaHasComponentsOutsideOfFixtureArea != this.MetaHasComponentsOutsideOfFixtureArea) return false;
                if (other.MetaTotalPositionCollisions != this.MetaTotalPositionCollisions) return false;
                if (other.MetaTotalFrontFacings != this.MetaTotalFrontFacings) return false;
                if (other.MetaAverageFrontFacings != this.MetaAverageFrontFacings) return false;
                if (other.LocationCode != this.LocationCode) return false;
                if (other.LocationName != this.LocationName) return false;
                if (other.ClusterSchemeName != this.ClusterSchemeName) return false;
                if (other.ClusterName != this.ClusterName) return false;
                if (other.MetaNoOfErrors != this.MetaNoOfErrors) return false;
                if (other.MetaNoOfWarnings != this.MetaNoOfWarnings) return false;
                if (other.MetaNoOfInformationEvents != this.MetaNoOfInformationEvents) return false;
                if (other.MetaNoOfDebugEvents != this.MetaNoOfDebugEvents) return false;
                if (other.MetaTotalErrorScore != this.MetaTotalErrorScore) return false;
                if (other.MetaHighestErrorScore != this.MetaHighestErrorScore) return false;
                if (other.Status != this.Status) return false;
                if (other.DateWip != this.DateWip) return false;
                if (other.DateApproved != this.DateApproved) return false;
                if (other.DateArchived != this.DateArchived) return false;
                if (other.MetaCountOfProductNotAchievedCases != this.MetaCountOfProductNotAchievedCases) return false;
                if (other.MetaCountOfProductNotAchievedDOS != this.MetaCountOfProductNotAchievedDOS) return false;
                if (other.MetaCountOfProductOverShelfLifePercent != this.MetaCountOfProductOverShelfLifePercent) return false;
                if (other.MetaCountOfProductNotAchievedDeliveries != this.MetaCountOfProductNotAchievedDeliveries) return false;
                if (other.MetaPercentOfProductNotAchievedCases != this.MetaPercentOfProductNotAchievedCases) return false;
                if (other.MetaPercentOfProductNotAchievedDOS != this.MetaPercentOfProductNotAchievedDOS) return false;
                if (other.MetaPercentOfProductOverShelfLifePercent != this.MetaPercentOfProductOverShelfLifePercent) return false;
                if (other.MetaPercentOfProductNotAchievedDeliveries != this.MetaPercentOfProductNotAchievedDeliveries) return false;
                if (other.MetaCountOfPositionsOutsideOfBlockSpace != this.MetaCountOfPositionsOutsideOfBlockSpace) return false;
                if (other.MetaPercentOfPositionsOutsideOfBlockSpace != this.MetaPercentOfPositionsOutsideOfBlockSpace) return false;
                if (other.MetaPercentOfPlacedProductsRecommendedInAssortment != this.MetaPercentOfPlacedProductsRecommendedInAssortment) return false;
                if (other.MetaCountOfPlacedProductsRecommendedInAssortment != this.MetaCountOfPlacedProductsRecommendedInAssortment) return false;
                if (other.MetaCountOfPlacedProductsNotRecommendedInAssortment != this.MetaCountOfPlacedProductsNotRecommendedInAssortment) return false;
                if (other.MetaCountOfRecommendedProductsInAssortment != this.MetaCountOfRecommendedProductsInAssortment) return false;
                if (other.PlanogramType != this.PlanogramType) return false;
                if (other.MetaPercentOfProductNotAchievedInventory != this.MetaPercentOfProductNotAchievedInventory) return false;
                if (other.HighlightSequenceStrategy != this.HighlightSequenceStrategy) return false;
                if (other.MetaNumberOfAssortmentRulesBroken != this.MetaNumberOfAssortmentRulesBroken) return false;
                if (other.MetaNumberOfProductRulesBroken != this.MetaNumberOfProductRulesBroken) return false;
                if (other.MetaNumberOfFamilyRulesBroken != this.MetaNumberOfFamilyRulesBroken) return false;
                if (other.MetaNumberOfInheritanceRulesBroken != this.MetaNumberOfInheritanceRulesBroken) return false;
                if (other.MetaNumberOfLocalProductRulesBroken != this.MetaNumberOfLocalProductRulesBroken) return false;
                if (other.MetaNumberOfDistributionRulesBroken != this.MetaNumberOfDistributionRulesBroken) return false;
                if (other.MetaNumberOfCoreRulesBroken != this.MetaNumberOfCoreRulesBroken) return false;
                if (other.MetaPercentageOfAssortmentRulesBroken != this.MetaPercentageOfAssortmentRulesBroken) return false;
                if (other.MetaPercentageOfProductRulesBroken != this.MetaPercentageOfProductRulesBroken) return false;
                if (other.MetaPercentageOfFamilyRulesBroken != this.MetaPercentageOfFamilyRulesBroken) return false;
                if (other.MetaPercentageOfInheritanceRulesBroken != this.MetaPercentageOfInheritanceRulesBroken) return false;
                if (other.MetaPercentageOfLocalProductRulesBroken != this.MetaPercentageOfLocalProductRulesBroken) return false;
                if (other.MetaPercentageOfDistributionRulesBroken != this.MetaPercentageOfDistributionRulesBroken) return false;
                if (other.MetaPercentageOfCoreRulesBroken != this.MetaPercentageOfCoreRulesBroken) return false;
                if (other.MetaNumberOfDelistProductRulesBroken != this.MetaNumberOfDelistProductRulesBroken) return false;
                if (other.MetaNumberOfForceProductRulesBroken != this.MetaNumberOfForceProductRulesBroken) return false;
                if (other.MetaNumberOfPreserveProductRulesBroken != this.MetaNumberOfPreserveProductRulesBroken) return false;
                if (other.MetaNumberOfMinimumHurdleProductRulesBroken != this.MetaNumberOfMinimumHurdleProductRulesBroken) return false;
                if (other.MetaNumberOfMaximumProductFamilyRulesBroken != this.MetaNumberOfMaximumProductFamilyRulesBroken) return false;
                if (other.MetaNumberOfMinimumProductFamilyRulesBroken != this.MetaNumberOfMinimumProductFamilyRulesBroken) return false;
                if (other.MetaNumberOfDependencyFamilyRulesBroken != this.MetaNumberOfDependencyFamilyRulesBroken) return false;
                if (other.MetaNumberOfDelistFamilyRulesBroken != this.MetaNumberOfDelistFamilyRulesBroken) return false;
                if (other.MetaCountOfProductsBuddied != this.MetaCountOfProductsBuddied) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramDtoKey other = obj as PlanogramDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id == null)) return false;
                if ((other.Id == null) && (this.Id != null)) return false;
                if ((other.Id != null) && (this.Id != null)) if (!other.Id.Equals(this.Id)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}