﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion

#region Version History: CCM803
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramPerformanceMetricDto
    {
        #region Properties
		public Object Id { get; set; }
        public PlanogramPerformanceMetricDtoKey DtoKey 
        { 
            get
            {
                return new PlanogramPerformanceMetricDtoKey()
                {
                    MetricId = this.MetricId,
                    PlanogramPerformanceId = this.PlanogramPerformanceId
                };
            }
        }
        [ForeignKey(typeof(PlanogramPerformanceDto), typeof(IPlanogramPerformanceDal), DeleteBehavior.Cascade)]
        public Object PlanogramPerformanceId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Byte Direction { get; set; }
        public Byte SpecialType { get; set; }
        public Byte MetricType { get; set; }
        public Byte MetricId { get; set; }
        public Object ExtendedData { get; set; }
        public Byte AggregationType { get; set; }
	    #endregion

        #region Methods

        public override int  GetHashCode()
        {
 	         return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramPerformanceMetricDto;
            if (other == null) return false;
            // Id
            if (other.Id != null && this.Id != null)
            {
                if (!other.Id.Equals(this.Id)) return false;
            }
            if ((other.Id == null && this.Id != null) || (other.Id != null && this.Id == null))
            {
                return false;
            }
            // PlanogramPerformanceId
            if (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId != null)
            {
                if (!other.PlanogramPerformanceId.Equals(this.PlanogramPerformanceId)) return false;
            }
            if ((other.PlanogramPerformanceId == null && this.PlanogramPerformanceId != null) || 
                (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId == null))
            {
                return false;
            }
            // ExtendedData
            if (other.ExtendedData != null && this.ExtendedData != null)
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) return false;
            }
            if ((other.ExtendedData == null && this.ExtendedData != null) || (other.ExtendedData != null && this.ExtendedData == null))
            {
                return false;
            }
            return
                other.Description == this.Description &&
                other.Direction == this.Direction &&
                other.MetricId == this.MetricId &&
                other.MetricType == this.MetricType &&
                other.Name == this.Name &&
                other.SpecialType == this.SpecialType &&
                other.AggregationType == this.AggregationType;
        }

        #endregion
    }

    [Serializable]
    public class PlanogramPerformanceMetricDtoKey
    {
        #region Properties
        public Int32 MetricId { get; set; }
        public Object PlanogramPerformanceId { get; set; } 
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return MetricId.GetHashCode() + PlanogramPerformanceId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramPerformanceMetricDtoKey;
            if (other == null) return false;
            if (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId != null)
            {
                if (!other.PlanogramPerformanceId.Equals(this.PlanogramPerformanceId)) return false;
            }
            if ((other.PlanogramPerformanceId == null && this.PlanogramPerformanceId != null) ||
                (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId == null))
            {
                return false;
            }
            return other.MetricId == this.MetricId;
        }
        #endregion
    }
}
