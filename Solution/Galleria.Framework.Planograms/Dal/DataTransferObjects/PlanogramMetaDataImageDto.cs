﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27058 : A.Probyn
//		Created 
#endregion
#endregion

using System;
using System.Linq;

using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramMetadataImage Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramMetadataImageDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramMetadataImageDtoKey DtoKey
        {
            get { return new PlanogramMetadataImageDtoKey() { Id = this.Id }; }
        }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Object PlanogramId { get; set; }
        public String FileName { get; set; }
        public String Description { get; set; }
        public Byte[] ImageData { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramMetadataImageDto other = obj as PlanogramMetadataImageDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                // PlanogramId
                if ((other.PlanogramId != null) && (this.PlanogramId != null))
                {
                    if (!other.PlanogramId.Equals(this.PlanogramId)) { return false; }
                }
                if ((other.PlanogramId != null) && (this.PlanogramId == null)) { return false; }
                if ((other.PlanogramId == null) && (this.PlanogramId != null)) { return false; }

                if (other.FileName != this.FileName) { return false; }
                if (other.Description != this.Description) { return false; }
                //ImageData
                if ((other.ImageData != null) && (this.ImageData == null))
                {
                    return false;
                }
                if ((other.ImageData == null) && (this.ImageData != null))
                {
                    return false;
                }
                if ((other.ImageData != null) && (this.ImageData != null) && (!other.ImageData.SequenceEqual(this.ImageData)))
                {
                    return false;
                }
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class PlanogramMetadataImageDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramMetadataImageDtoKey other = obj as PlanogramMetadataImageDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
