﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentProductBuddy Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramAssortmentProductBuddyDto
    {
        #region Properties
        public Object Id { get; set; }
        [ForeignKey(typeof(PlanogramAssortmentDto), typeof(IPlanogramAssortmentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentId { get; set; }
        public PlanogramAssortmentProductBuddyDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentProductBuddyDtoKey
                {
                    PlanogramAssortmentId = this.PlanogramAssortmentId,
                    ProductGtin = this.ProductGtin
                };
            }
        }
        public String ProductGtin { get; set; }
        public Byte SourceType { get; set; }
        public Byte TreatmentType { get; set; }
        public Single TreatmentTypePercentage { get; set; }
        public Byte ProductAttributeType { get; set; }
        public String S1ProductGtin { get; set; }
        public Single? S1Percentage { get; set; }
        public String S2ProductGtin { get; set; }
        public Single? S2Percentage { get; set; }
        public String S3ProductGtin { get; set; }
        public Single? S3Percentage { get; set; }
        public String S4ProductGtin { get; set; }
        public Single? S4Percentage { get; set; }
        public String S5ProductGtin { get; set; }
        public Single? S5Percentage { get; set; }
        
        public Object ExtendedData { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramAssortmentProductBuddyDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }

            if (other.ProductGtin != this.ProductGtin) { return false; }
            if (other.SourceType != this.SourceType) { return false; }
            if (other.TreatmentType != this.TreatmentType) { return false; }
            if (other.TreatmentTypePercentage != this.TreatmentTypePercentage) { return false; }
            if (other.ProductAttributeType != this.ProductAttributeType) { return false; }
            if (other.S1ProductGtin != this.S1ProductGtin) { return false; }
            if (other.S1Percentage != this.S1Percentage) { return false; }
            if (other.S2ProductGtin != this.S2ProductGtin) { return false; }
            if (other.S2Percentage != this.S2Percentage) { return false; }
            if (other.S3ProductGtin != this.S3ProductGtin) { return false; }
            if (other.S3Percentage != this.S3Percentage) { return false; }
            if (other.S4ProductGtin != this.S4ProductGtin) { return false; }
            if (other.S4Percentage != this.S4Percentage) { return false; }
            if (other.S5ProductGtin != this.S5ProductGtin) { return false; }
            if (other.S5Percentage != this.S5Percentage) { return false; }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentProductBuddyDtoKey
    {
        #region Properties
        public String ProductGtin { get; set; }
        public Object PlanogramAssortmentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ProductGtin.GetHashCode() +
                PlanogramAssortmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramAssortmentProductBuddyDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }

            return this.ProductGtin == other.ProductGtin;
        }
        #endregion
    }
}

