﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion

#region Version History: CCM802

// CCM-28811 : L.Luong
//  Added MetaP1Percentage to MetaP20Percentage properties

#endregion

#region Version History: CCM 8.2.0
// V8-29998 : I.George
//	Added PlanogramPerformanceData_MetaP1Rank to PlanogramPerformanceData_MetaP20Rank properties
// V8-30992 : A.Probyn
//  Added MinimumInventoryUnits
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramPerformanceDataDto
    {
        #region Properties

        public Object Id { get; set; }
        public PlanogramPerformanceDataDtoKey DtoKey
        {
            get
            {
                return new PlanogramPerformanceDataDtoKey()
                {
                    PlanogramProductId = this.PlanogramProductId,
                    PlanogramPerformanceId = this.PlanogramPerformanceId
                };
            }
        }
        [ForeignKey(typeof(PlanogramPerformanceDto), typeof(IPlanogramPerformanceDal), DeleteBehavior.Cascade)]
        public Object PlanogramPerformanceId { get; set; }
        [ForeignKey(typeof(PlanogramProductDto), typeof(IPlanogramProductDal), DeleteBehavior.Cascade)]
        public Object PlanogramProductId { get; set; }
        public Single? P1 { get; set; }
        public Single? P2 { get; set; }
        public Single? P3 { get; set; }
        public Single? P4 { get; set; }
        public Single? P5 { get; set; }
        public Single? P6 { get; set; }
        public Single? P7 { get; set; }
        public Single? P8 { get; set; }
        public Single? P9 { get; set; }
        public Single? P10 { get; set; }
        public Single? P11 { get; set; }
        public Single? P12 { get; set; }
        public Single? P13 { get; set; }
        public Single? P14 { get; set; }
        public Single? P15 { get; set; }
        public Single? P16 { get; set; }
        public Single? P17 { get; set; }
        public Single? P18 { get; set; }
        public Single? P19 { get; set; }
        public Single? P20 { get; set; }

        public Single? CP1 { get; set; }
        public Single? CP2 { get; set; }
        public Single? CP3 { get; set; }
        public Single? CP4 { get; set; }
        public Single? CP5 { get; set; }
        public Single? CP6 { get; set; }
        public Single? CP7 { get; set; }
        public Single? CP8 { get; set; }
        public Single? CP9 { get; set; }
        public Single? CP10 { get; set; }

        public Single? UnitsSoldPerDay { get; set; }
        public Single? AchievedCasePacks { get; set; }
        public Single? AchievedDos { get; set; }
        public Single? AchievedShelfLife { get; set; }
        public Single? AchievedDeliveries { get; set; }
        public Single? MinimumInventoryUnits { get; set; }
        public Object ExtendedData { get; set; }

        #endregion

        #region Meta Data Properties

        public Single? MetaP1Percentage { get; set; }
        public Single? MetaP2Percentage { get; set; }
        public Single? MetaP3Percentage { get; set; }
        public Single? MetaP4Percentage { get; set; }
        public Single? MetaP5Percentage { get; set; }
        public Single? MetaP6Percentage { get; set; }
        public Single? MetaP7Percentage { get; set; }
        public Single? MetaP8Percentage { get; set; }
        public Single? MetaP9Percentage { get; set; }
        public Single? MetaP10Percentage { get; set; }
        public Single? MetaP11Percentage { get; set; }
        public Single? MetaP12Percentage { get; set; }
        public Single? MetaP13Percentage { get; set; }
        public Single? MetaP14Percentage { get; set; }
        public Single? MetaP15Percentage { get; set; }
        public Single? MetaP16Percentage { get; set; }
        public Single? MetaP17Percentage { get; set; }
        public Single? MetaP18Percentage { get; set; }
        public Single? MetaP19Percentage { get; set; }
        public Single? MetaP20Percentage { get; set; }

        public Single? MetaCP1Percentage { get; set; }
        public Single? MetaCP2Percentage { get; set; }
        public Single? MetaCP3Percentage { get; set; }
        public Single? MetaCP4Percentage { get; set; }
        public Single? MetaCP5Percentage { get; set; }
        public Single? MetaCP6Percentage { get; set; }
        public Single? MetaCP7Percentage { get; set; }
        public Single? MetaCP8Percentage { get; set; }
        public Single? MetaCP9Percentage { get; set; }
        public Single? MetaCP10Percentage { get; set; }

        public Int32? MetaP1Rank { get; set; }
        public Int32? MetaP2Rank { get; set; }
        public Int32? MetaP3Rank { get; set; }
        public Int32? MetaP4Rank { get; set; }
        public Int32? MetaP5Rank { get; set; }
        public Int32? MetaP6Rank { get; set; }
        public Int32? MetaP7Rank { get; set; }
        public Int32? MetaP8Rank { get; set; }
        public Int32? MetaP9Rank { get; set; }
        public Int32? MetaP10Rank { get; set; }
        public Int32? MetaP11Rank { get; set; }
        public Int32? MetaP12Rank { get; set; }
        public Int32? MetaP13Rank { get; set; }
        public Int32? MetaP14Rank { get; set; }
        public Int32? MetaP15Rank { get; set; }
        public Int32? MetaP16Rank { get; set; }
        public Int32? MetaP17Rank { get; set; }
        public Int32? MetaP18Rank { get; set; }
        public Int32? MetaP19Rank { get; set; }
        public Int32? MetaP20Rank { get; set; }

        public Int32? MetaCP1Rank { get; set; }
        public Int32? MetaCP2Rank { get; set; }
        public Int32? MetaCP3Rank { get; set; }
        public Int32? MetaCP4Rank { get; set; }
        public Int32? MetaCP5Rank { get; set; }
        public Int32? MetaCP6Rank { get; set; }
        public Int32? MetaCP7Rank { get; set; }
        public Int32? MetaCP8Rank { get; set; }
        public Int32? MetaCP9Rank { get; set; }
        public Int32? MetaCP10Rank { get; set; }
        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramPerformanceDataDto;
            if (other == null) return false;
            // Id
            if (other.Id != null && this.Id != null)
            {
                if (!other.Id.Equals(this.Id)) return false;
            }
            if ((other.Id == null && this.Id != null) || (other.Id != null && this.Id == null))
            {
                return false;
            }
            // PlanogramPerformanceId
            if (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId != null)
            {
                if (!other.PlanogramPerformanceId.Equals(this.PlanogramPerformanceId)) return false;
            }
            if ((other.PlanogramPerformanceId == null && this.PlanogramPerformanceId != null) ||
                (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId == null))
            {
                return false;
            }
            // PlanogramProductId
            if (other.PlanogramProductId != null && this.PlanogramProductId != null)
            {
                if (!other.PlanogramProductId.Equals(this.PlanogramProductId)) return false;
            }
            if ((other.PlanogramProductId == null && this.PlanogramProductId != null) ||
                (other.PlanogramProductId != null && this.PlanogramProductId == null))
            {
                return false;
            }
            // ExtendedData
            if (other.ExtendedData != null && this.ExtendedData != null)
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) return false;
            }
            if ((other.ExtendedData == null && this.ExtendedData != null) ||
                (other.ExtendedData != null && this.ExtendedData == null))
            {
                return false;
            }
            return
                other.P1 == this.P1 &&
                other.P2 == this.P2 &&
                other.P3 == this.P3 &&
                other.P4 == this.P4 &&
                other.P5 == this.P5 &&
                other.P6 == this.P6 &&
                other.P7 == this.P7 &&
                other.P8 == this.P8 &&
                other.P9 == this.P9 &&
                other.P10 == this.P10 &&
                other.P11 == this.P11 &&
                other.P12 == this.P12 &&
                other.P13 == this.P13 &&
                other.P14 == this.P14 &&
                other.P15 == this.P15 &&
                other.P16 == this.P16 &&
                other.P17 == this.P17 &&
                other.P18 == this.P18 &&
                other.P19 == this.P19 &&
                other.P20 == this.P20 &&
                other.UnitsSoldPerDay == this.UnitsSoldPerDay &&
                other.AchievedCasePacks == this.AchievedCasePacks &&
                other.AchievedDos == this.AchievedDos &&
                other.AchievedShelfLife == this.AchievedShelfLife &&
                other.AchievedDeliveries == this.AchievedDeliveries &&
                other.MinimumInventoryUnits == this.MinimumInventoryUnits &&
                other.MetaP1Percentage == this.MetaP1Percentage &&
                other.MetaP2Percentage == this.MetaP2Percentage &&
                other.MetaP3Percentage == this.MetaP3Percentage &&
                other.MetaP4Percentage == this.MetaP4Percentage &&
                other.MetaP5Percentage == this.MetaP5Percentage &&
                other.MetaP6Percentage == this.MetaP6Percentage &&
                other.MetaP7Percentage == this.MetaP7Percentage &&
                other.MetaP8Percentage == this.MetaP8Percentage &&
                other.MetaP9Percentage == this.MetaP9Percentage &&
                other.MetaP10Percentage == this.MetaP10Percentage &&
                other.MetaP11Percentage == this.MetaP11Percentage &&
                other.MetaP12Percentage == this.MetaP12Percentage &&
                other.MetaP13Percentage == this.MetaP13Percentage &&
                other.MetaP14Percentage == this.MetaP14Percentage &&
                other.MetaP15Percentage == this.MetaP15Percentage &&
                other.MetaP16Percentage == this.MetaP16Percentage &&
                other.MetaP17Percentage == this.MetaP17Percentage &&
                other.MetaP18Percentage == this.MetaP18Percentage &&
                other.MetaP19Percentage == this.MetaP19Percentage &&
                other.MetaP20Percentage == this.MetaP20Percentage &&
                other.MetaP1Rank == this.MetaP1Rank &&
                other.MetaP2Rank == this.MetaP2Rank &&
                other.MetaP3Rank == this.MetaP3Rank &&
                other.MetaP4Rank == this.MetaP4Rank &&
                other.MetaP5Rank == this.MetaP5Rank &&
                other.MetaP6Rank == this.MetaP6Rank &&
                other.MetaP7Rank == this.MetaP7Rank &&
                other.MetaP8Rank == this.MetaP8Rank &&
                other.MetaP9Rank == this.MetaP9Rank &&
                other.MetaP10Rank == this.MetaP10Rank &&
                other.MetaP11Rank == this.MetaP11Rank &&
                other.MetaP12Rank == this.MetaP12Rank &&
                other.MetaP13Rank == this.MetaP13Rank &&
                other.MetaP14Rank == this.MetaP14Rank &&
                other.MetaP15Rank == this.MetaP15Rank &&
                other.MetaP16Rank == this.MetaP16Rank &&
                other.MetaP17Rank == this.MetaP17Rank &&
                other.MetaP18Rank == this.MetaP18Rank &&
                other.MetaP19Rank == this.MetaP19Rank &&
                other.MetaP20Rank == this.MetaP20Rank &&
                other.CP1 == this.CP1 &&
                other.CP2 == this.CP2 &&
                other.CP3 == this.CP3 &&
                other.CP4 == this.CP4 &&
                other.CP5 == this.CP5 &&
                other.CP6 == this.CP6 &&
                other.CP7 == this.CP7 &&
                other.CP8 == this.CP8 &&
                other.CP9 == this.CP9 &&
                other.CP10 == this.CP10 &&
                other.MetaCP1Percentage == this.MetaCP1Percentage &&
                other.MetaCP2Percentage == this.MetaCP2Percentage &&
                other.MetaCP3Percentage == this.MetaCP3Percentage &&
                other.MetaCP4Percentage == this.MetaCP4Percentage &&
                other.MetaCP5Percentage == this.MetaCP5Percentage &&
                other.MetaCP6Percentage == this.MetaCP6Percentage &&
                other.MetaCP7Percentage == this.MetaCP7Percentage &&
                other.MetaCP8Percentage == this.MetaCP8Percentage &&
                other.MetaCP9Percentage == this.MetaCP9Percentage &&
                other.MetaCP10Percentage == this.MetaCP10Percentage &&
                other.MetaCP1Rank == this.MetaCP1Rank &&
                other.MetaCP2Rank == this.MetaCP2Rank &&
                other.MetaCP3Rank == this.MetaCP3Rank &&
                other.MetaCP4Rank == this.MetaCP4Rank &&
                other.MetaCP5Rank == this.MetaCP5Rank &&
                other.MetaCP6Rank == this.MetaCP6Rank &&
                other.MetaCP7Rank == this.MetaCP7Rank &&
                other.MetaCP8Rank == this.MetaCP8Rank &&
                other.MetaCP9Rank == this.MetaCP9Rank &&
                other.MetaCP10Rank == this.MetaCP10Rank;

        }

        #endregion
    }

    [Serializable]
    public class PlanogramPerformanceDataDtoKey
    {
        #region Properties

        public Object PlanogramPerformanceId { get; set; }
        public Object PlanogramProductId { get; set; }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return PlanogramPerformanceId.GetHashCode() + PlanogramProductId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramPerformanceDataDtoKey;
            if (other == null) return false;
            // PlanogramPerformanceId
            if (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId != null)
            {
                if (!other.PlanogramPerformanceId.Equals(this.PlanogramPerformanceId)) return false;
            }
            if ((other.PlanogramPerformanceId == null && this.PlanogramPerformanceId != null) ||
                (other.PlanogramPerformanceId != null && this.PlanogramPerformanceId == null))
            {
                return false;
            }
            // PlanogramProductId
            if (other.PlanogramProductId != null && this.PlanogramProductId != null)
            {
                if (!other.PlanogramProductId.Equals(this.PlanogramProductId)) return false;
            }
            if ((other.PlanogramProductId == null && this.PlanogramProductId != null) ||
                (other.PlanogramProductId != null && this.PlanogramProductId == null))
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
