﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26147 : L.Ineson
//  Created
// CCM-26332 : L.Ineson
//  Added IsSetDto
#endregion

#region Version History : CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// CustomAttributeData Data Transfer Object
    /// </summary>
    [Serializable]
    public class CustomAttributeDataDto
    {
        #region Properties

        public Object Id { get; set; }
        public CustomAttributeDataDtoKey DtoKey
        {
            get { return new CustomAttributeDataDtoKey() { ParentId = this.ParentId, ParentType = this.ParentType }; }
        }
        public Object ParentId { get; set; }
        public Byte ParentType { get; set; }
        public String Text1 { get; set; }
        public String Text2 { get; set; }
        public String Text3 { get; set; }
        public String Text4 { get; set; }
        public String Text5 { get; set; }
        public String Text6 { get; set; }
        public String Text7 { get; set; }
        public String Text8 { get; set; }
        public String Text9 { get; set; }
        public String Text10 { get; set; }
        public String Text11 { get; set; }
        public String Text12 { get; set; }
        public String Text13 { get; set; }
        public String Text14 { get; set; }
        public String Text15 { get; set; }
        public String Text16 { get; set; }
        public String Text17 { get; set; }
        public String Text18 { get; set; }
        public String Text19 { get; set; }
        public String Text20 { get; set; }
        public String Text21 { get; set; }
        public String Text22 { get; set; }
        public String Text23 { get; set; }
        public String Text24 { get; set; }
        public String Text25 { get; set; }
        public String Text26 { get; set; }
        public String Text27 { get; set; }
        public String Text28 { get; set; }
        public String Text29 { get; set; }
        public String Text30 { get; set; }
        public String Text31 { get; set; }
        public String Text32 { get; set; }
        public String Text33 { get; set; }
        public String Text34 { get; set; }
        public String Text35 { get; set; }
        public String Text36 { get; set; }
        public String Text37 { get; set; }
        public String Text38 { get; set; }
        public String Text39 { get; set; }
        public String Text40 { get; set; }
        public String Text41 { get; set; }
        public String Text42 { get; set; }
        public String Text43 { get; set; }
        public String Text44 { get; set; }
        public String Text45 { get; set; }
        public String Text46 { get; set; }
        public String Text47 { get; set; }
        public String Text48 { get; set; }
        public String Text49 { get; set; }
        public String Text50 { get; set; }
        public Single Value1 { get; set; }
        public Single Value2 { get; set; }
        public Single Value3 { get; set; }
        public Single Value4 { get; set; }
        public Single Value5 { get; set; }
        public Single Value6 { get; set; }
        public Single Value7 { get; set; }
        public Single Value8 { get; set; }
        public Single Value9 { get; set; }
        public Single Value10 { get; set; }
        public Single Value11 { get; set; }
        public Single Value12 { get; set; }
        public Single Value13 { get; set; }
        public Single Value14 { get; set; }
        public Single Value15 { get; set; }
        public Single Value16 { get; set; }
        public Single Value17 { get; set; }
        public Single Value18 { get; set; }
        public Single Value19 { get; set; }
        public Single Value20 { get; set; }
        public Single Value21 { get; set; }
        public Single Value22 { get; set; }
        public Single Value23 { get; set; }
        public Single Value24 { get; set; }
        public Single Value25 { get; set; }
        public Single Value26 { get; set; }
        public Single Value27 { get; set; }
        public Single Value28 { get; set; }
        public Single Value29 { get; set; }
        public Single Value30 { get; set; }
        public Single Value31 { get; set; }
        public Single Value32 { get; set; }
        public Single Value33 { get; set; }
        public Single Value34 { get; set; }
        public Single Value35 { get; set; }
        public Single Value36 { get; set; }
        public Single Value37 { get; set; }
        public Single Value38 { get; set; }
        public Single Value39 { get; set; }
        public Single Value40 { get; set; }
        public Single Value41 { get; set; }
        public Single Value42 { get; set; }
        public Single Value43 { get; set; }
        public Single Value44 { get; set; }
        public Single Value45 { get; set; }
        public Single Value46 { get; set; }
        public Single Value47 { get; set; }
        public Single Value48 { get; set; }
        public Single Value49 { get; set; }
        public Single Value50 { get; set; }
        public Boolean Flag1 { get; set; }
        public Boolean Flag2 { get; set; }
        public Boolean Flag3 { get; set; }
        public Boolean Flag4 { get; set; }
        public Boolean Flag5 { get; set; }
        public Boolean Flag6 { get; set; }
        public Boolean Flag7 { get; set; }
        public Boolean Flag8 { get; set; }
        public Boolean Flag9 { get; set; }
        public Boolean Flag10 { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public DateTime? Date3 { get; set; }
        public DateTime? Date4 { get; set; }
        public DateTime? Date5 { get; set; }
        public DateTime? Date6 { get; set; }
        public DateTime? Date7 { get; set; }
        public DateTime? Date8 { get; set; }
        public DateTime? Date9 { get; set; }
        public DateTime? Date10 { get; set; }
        public String Note1 { get; set; }
        public String Note2 { get; set; }
        public String Note3 { get; set; }
        public String Note4 { get; set; }
        public String Note5 { get; set; }
        public Object ExtendedData { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomAttributeDataDto other = obj as CustomAttributeDataDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id == null)) return false;
                if ((other.Id == null) && (this.Id != null)) return false;
                if ((other.Id != null) && (this.Id != null)) if (!other.Id.Equals(this.Id)) return false;
                // Parent Id
                if ((other.ParentId != null) && (this.ParentId == null)) return false;
                if ((other.ParentId == null) && (this.ParentId != null)) return false;
                if ((other.ParentId != null) && (this.ParentId != null)) if (!other.ParentId.Equals(this.ParentId)) return false;

                if (other.ParentType != this.ParentType) return false;
                if (other.Text1 != this.Text1) return false;
                if (other.Text2 != this.Text2) return false;
                if (other.Text3 != this.Text3) return false;
                if (other.Text4 != this.Text4) return false;
                if (other.Text5 != this.Text5) return false;
                if (other.Text6 != this.Text6) return false;
                if (other.Text7 != this.Text7) return false;
                if (other.Text8 != this.Text8) return false;
                if (other.Text9 != this.Text9) return false;
                if (other.Text10 != this.Text10) return false;
                if (other.Text11 != this.Text11) return false;
                if (other.Text12 != this.Text12) return false;
                if (other.Text13 != this.Text13) return false;
                if (other.Text14 != this.Text14) return false;
                if (other.Text15 != this.Text15) return false;
                if (other.Text16 != this.Text16) return false;
                if (other.Text17 != this.Text17) return false;
                if (other.Text18 != this.Text18) return false;
                if (other.Text19 != this.Text19) return false;
                if (other.Text20 != this.Text20) return false;
                if (other.Text21 != this.Text21) return false;
                if (other.Text22 != this.Text22) return false;
                if (other.Text23 != this.Text23) return false;
                if (other.Text24 != this.Text24) return false;
                if (other.Text25 != this.Text25) return false;
                if (other.Text26 != this.Text26) return false;
                if (other.Text27 != this.Text27) return false;
                if (other.Text28 != this.Text28) return false;
                if (other.Text29 != this.Text29) return false;
                if (other.Text30 != this.Text30) return false;
                if (other.Text31 != this.Text31) return false;
                if (other.Text32 != this.Text32) return false;
                if (other.Text33 != this.Text33) return false;
                if (other.Text34 != this.Text34) return false;
                if (other.Text35 != this.Text35) return false;
                if (other.Text36 != this.Text36) return false;
                if (other.Text37 != this.Text37) return false;
                if (other.Text38 != this.Text38) return false;
                if (other.Text39 != this.Text39) return false;
                if (other.Text40 != this.Text40) return false;
                if (other.Text41 != this.Text41) return false;
                if (other.Text42 != this.Text42) return false;
                if (other.Text43 != this.Text43) return false;
                if (other.Text44 != this.Text44) return false;
                if (other.Text45 != this.Text45) return false;
                if (other.Text46 != this.Text46) return false;
                if (other.Text47 != this.Text47) return false;
                if (other.Text48 != this.Text48) return false;
                if (other.Text49 != this.Text49) return false;
                if (other.Text50 != this.Text50) return false;
                if (other.Value1 != this.Value1) return false;
                if (other.Value2 != this.Value2) return false;
                if (other.Value3 != this.Value3) return false;
                if (other.Value4 != this.Value4) return false;
                if (other.Value5 != this.Value5) return false;
                if (other.Value6 != this.Value6) return false;
                if (other.Value7 != this.Value7) return false;
                if (other.Value8 != this.Value8) return false;
                if (other.Value9 != this.Value9) return false;
                if (other.Value10 != this.Value10) return false;
                if (other.Value11 != this.Value11) return false;
                if (other.Value12 != this.Value12) return false;
                if (other.Value13 != this.Value13) return false;
                if (other.Value14 != this.Value14) return false;
                if (other.Value15 != this.Value15) return false;
                if (other.Value16 != this.Value16) return false;
                if (other.Value17 != this.Value17) return false;
                if (other.Value18 != this.Value18) return false;
                if (other.Value19 != this.Value19) return false;
                if (other.Value20 != this.Value20) return false;
                if (other.Value21 != this.Value21) return false;
                if (other.Value22 != this.Value22) return false;
                if (other.Value23 != this.Value23) return false;
                if (other.Value24 != this.Value24) return false;
                if (other.Value25 != this.Value25) return false;
                if (other.Value26 != this.Value26) return false;
                if (other.Value27 != this.Value27) return false;
                if (other.Value28 != this.Value28) return false;
                if (other.Value29 != this.Value29) return false;
                if (other.Value30 != this.Value30) return false;
                if (other.Value31 != this.Value31) return false;
                if (other.Value32 != this.Value32) return false;
                if (other.Value33 != this.Value33) return false;
                if (other.Value34 != this.Value34) return false;
                if (other.Value35 != this.Value35) return false;
                if (other.Value36 != this.Value36) return false;
                if (other.Value37 != this.Value37) return false;
                if (other.Value38 != this.Value38) return false;
                if (other.Value39 != this.Value39) return false;
                if (other.Value40 != this.Value40) return false;
                if (other.Value41 != this.Value41) return false;
                if (other.Value42 != this.Value42) return false;
                if (other.Value43 != this.Value43) return false;
                if (other.Value44 != this.Value44) return false;
                if (other.Value45 != this.Value45) return false;
                if (other.Value46 != this.Value46) return false;
                if (other.Value47 != this.Value47) return false;
                if (other.Value48 != this.Value48) return false;
                if (other.Value49 != this.Value49) return false;
                if (other.Value50 != this.Value50) return false;
                if (other.Flag1 != this.Flag1) return false;
                if (other.Flag2 != this.Flag2) return false;
                if (other.Flag3 != this.Flag3) return false;
                if (other.Flag4 != this.Flag4) return false;
                if (other.Flag5 != this.Flag5) return false;
                if (other.Flag6 != this.Flag6) return false;
                if (other.Flag7 != this.Flag7) return false;
                if (other.Flag8 != this.Flag8) return false;
                if (other.Flag9 != this.Flag9) return false;
                if (other.Flag10 != this.Flag10) return false;
                if (other.Date1 != this.Date1) return false;
                if (other.Date2 != this.Date2) return false;
                if (other.Date3 != this.Date3) return false;
                if (other.Date4 != this.Date4) return false;
                if (other.Date5 != this.Date5) return false;
                if (other.Date6 != this.Date6) return false;
                if (other.Date7 != this.Date7) return false;
                if (other.Date8 != this.Date8) return false;
                if (other.Date9 != this.Date9) return false;
                if (other.Date10 != this.Date10) return false;
                if (other.Note1 != this.Note1) return false;
                if (other.Note2 != this.Note2) return false;
                if (other.Note3 != this.Note3) return false;
                if (other.Note4 != this.Note4) return false;
                if (other.Note5 != this.Note5) return false;
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) return false;
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) return false;
                if ((other.ExtendedData != null) && (this.ExtendedData != null)) if (!other.ExtendedData.Equals(this.ExtendedData)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// Dto key definition for CustomAttributeDataDto
    /// </summary>
    public class CustomAttributeDataDtoKey
    {
        #region Properties
        public Object ParentId { get; set; }
        public Byte ParentType { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return ParentId.GetHashCode()
                + ParentType.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomAttributeDataDtoKey other = obj as CustomAttributeDataDtoKey;
            if (other != null)
            {
                // ParentId
                if ((other.ParentId != null) && (this.ParentId == null)) return false;
                if ((other.ParentId == null) && (this.ParentId != null)) return false;
                if ((other.ParentId != null) && (this.ParentId != null)) if (!other.ParentId.Equals(this.ParentId)) return false;

                //ParentType
                if (other.ParentType != this.ParentType) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class CustomAttributeDataIsSetDto
    {

        #region Properties

        public Boolean IsParentIdSet { get; set; }
        public Boolean IsParentTypeSet { get; set; }
        public Boolean IsText1Set { get; set; }
        public Boolean IsText2Set { get; set; }
        public Boolean IsText3Set { get; set; }
        public Boolean IsText4Set { get; set; }
        public Boolean IsText5Set { get; set; }
        public Boolean IsText6Set { get; set; }
        public Boolean IsText7Set { get; set; }
        public Boolean IsText8Set { get; set; }
        public Boolean IsText9Set { get; set; }
        public Boolean IsText10Set { get; set; }
        public Boolean IsText11Set { get; set; }
        public Boolean IsText12Set { get; set; }
        public Boolean IsText13Set { get; set; }
        public Boolean IsText14Set { get; set; }
        public Boolean IsText15Set { get; set; }
        public Boolean IsText16Set { get; set; }
        public Boolean IsText17Set { get; set; }
        public Boolean IsText18Set { get; set; }
        public Boolean IsText19Set { get; set; }
        public Boolean IsText20Set { get; set; }
        public Boolean IsText21Set { get; set; }
        public Boolean IsText22Set { get; set; }
        public Boolean IsText23Set { get; set; }
        public Boolean IsText24Set { get; set; }
        public Boolean IsText25Set { get; set; }
        public Boolean IsText26Set { get; set; }
        public Boolean IsText27Set { get; set; }
        public Boolean IsText28Set { get; set; }
        public Boolean IsText29Set { get; set; }
        public Boolean IsText30Set { get; set; }
        public Boolean IsText31Set { get; set; }
        public Boolean IsText32Set { get; set; }
        public Boolean IsText33Set { get; set; }
        public Boolean IsText34Set { get; set; }
        public Boolean IsText35Set { get; set; }
        public Boolean IsText36Set { get; set; }
        public Boolean IsText37Set { get; set; }
        public Boolean IsText38Set { get; set; }
        public Boolean IsText39Set { get; set; }
        public Boolean IsText40Set { get; set; }
        public Boolean IsText41Set { get; set; }
        public Boolean IsText42Set { get; set; }
        public Boolean IsText43Set { get; set; }
        public Boolean IsText44Set { get; set; }
        public Boolean IsText45Set { get; set; }
        public Boolean IsText46Set { get; set; }
        public Boolean IsText47Set { get; set; }
        public Boolean IsText48Set { get; set; }
        public Boolean IsText49Set { get; set; }
        public Boolean IsText50Set { get; set; }
        public Boolean IsValue1Set { get; set; }
        public Boolean IsValue2Set { get; set; }
        public Boolean IsValue3Set { get; set; }
        public Boolean IsValue4Set { get; set; }
        public Boolean IsValue5Set { get; set; }
        public Boolean IsValue6Set { get; set; }
        public Boolean IsValue7Set { get; set; }
        public Boolean IsValue8Set { get; set; }
        public Boolean IsValue9Set { get; set; }
        public Boolean IsValue10Set { get; set; }
        public Boolean IsValue11Set { get; set; }
        public Boolean IsValue12Set { get; set; }
        public Boolean IsValue13Set { get; set; }
        public Boolean IsValue14Set { get; set; }
        public Boolean IsValue15Set { get; set; }
        public Boolean IsValue16Set { get; set; }
        public Boolean IsValue17Set { get; set; }
        public Boolean IsValue18Set { get; set; }
        public Boolean IsValue19Set { get; set; }
        public Boolean IsValue20Set { get; set; }
        public Boolean IsValue21Set { get; set; }
        public Boolean IsValue22Set { get; set; }
        public Boolean IsValue23Set { get; set; }
        public Boolean IsValue24Set { get; set; }
        public Boolean IsValue25Set { get; set; }
        public Boolean IsValue26Set { get; set; }
        public Boolean IsValue27Set { get; set; }
        public Boolean IsValue28Set { get; set; }
        public Boolean IsValue29Set { get; set; }
        public Boolean IsValue30Set { get; set; }
        public Boolean IsValue31Set { get; set; }
        public Boolean IsValue32Set { get; set; }
        public Boolean IsValue33Set { get; set; }
        public Boolean IsValue34Set { get; set; }
        public Boolean IsValue35Set { get; set; }
        public Boolean IsValue36Set { get; set; }
        public Boolean IsValue37Set { get; set; }
        public Boolean IsValue38Set { get; set; }
        public Boolean IsValue39Set { get; set; }
        public Boolean IsValue40Set { get; set; }
        public Boolean IsValue41Set { get; set; }
        public Boolean IsValue42Set { get; set; }
        public Boolean IsValue43Set { get; set; }
        public Boolean IsValue44Set { get; set; }
        public Boolean IsValue45Set { get; set; }
        public Boolean IsValue46Set { get; set; }
        public Boolean IsValue47Set { get; set; }
        public Boolean IsValue48Set { get; set; }
        public Boolean IsValue49Set { get; set; }
        public Boolean IsValue50Set { get; set; }
        public Boolean IsFlag1Set { get; set; }
        public Boolean IsFlag2Set { get; set; }
        public Boolean IsFlag3Set { get; set; }
        public Boolean IsFlag4Set { get; set; }
        public Boolean IsFlag5Set { get; set; }
        public Boolean IsFlag6Set { get; set; }
        public Boolean IsFlag7Set { get; set; }
        public Boolean IsFlag8Set { get; set; }
        public Boolean IsFlag9Set { get; set; }
        public Boolean IsFlag10Set { get; set; }
        public Boolean IsDate1Set { get; set; }
        public Boolean IsDate2Set { get; set; }
        public Boolean IsDate3Set { get; set; }
        public Boolean IsDate4Set { get; set; }
        public Boolean IsDate5Set { get; set; }
        public Boolean IsDate6Set { get; set; }
        public Boolean IsDate7Set { get; set; }
        public Boolean IsDate8Set { get; set; }
        public Boolean IsDate9Set { get; set; }
        public Boolean IsDate10Set { get; set; }
        public Boolean IsNote1Set { get; set; }
        public Boolean IsNote2Set { get; set; }
        public Boolean IsNote3Set { get; set; }
        public Boolean IsNote4Set { get; set; }
        public Boolean IsNote5Set { get; set; }
        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public CustomAttributeDataIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public CustomAttributeDataIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
     IsParentIdSet.GetHashCode() +
     IsParentTypeSet.GetHashCode() +
     IsText1Set.GetHashCode() +
     IsText2Set.GetHashCode() +
     IsText3Set.GetHashCode() +
     IsText4Set.GetHashCode() +
     IsText5Set.GetHashCode() +
     IsText6Set.GetHashCode() +
     IsText7Set.GetHashCode() +
     IsText8Set.GetHashCode() +
     IsText9Set.GetHashCode() +
     IsText10Set.GetHashCode() +
     IsText11Set.GetHashCode() +
     IsText12Set.GetHashCode() +
     IsText13Set.GetHashCode() +
     IsText14Set.GetHashCode() +
     IsText15Set.GetHashCode() +
     IsText16Set.GetHashCode() +
     IsText17Set.GetHashCode() +
     IsText18Set.GetHashCode() +
     IsText19Set.GetHashCode() +
     IsText20Set.GetHashCode() +
     IsText21Set.GetHashCode() +
     IsText22Set.GetHashCode() +
     IsText23Set.GetHashCode() +
     IsText24Set.GetHashCode() +
     IsText25Set.GetHashCode() +
     IsText26Set.GetHashCode() +
     IsText27Set.GetHashCode() +
     IsText28Set.GetHashCode() +
     IsText29Set.GetHashCode() +
     IsText30Set.GetHashCode() +
     IsText31Set.GetHashCode() +
     IsText32Set.GetHashCode() +
     IsText33Set.GetHashCode() +
     IsText34Set.GetHashCode() +
     IsText35Set.GetHashCode() +
     IsText36Set.GetHashCode() +
     IsText37Set.GetHashCode() +
     IsText38Set.GetHashCode() +
     IsText39Set.GetHashCode() +
     IsText40Set.GetHashCode() +
     IsText41Set.GetHashCode() +
     IsText42Set.GetHashCode() +
     IsText43Set.GetHashCode() +
     IsText44Set.GetHashCode() +
     IsText45Set.GetHashCode() +
     IsText46Set.GetHashCode() +
     IsText47Set.GetHashCode() +
     IsText48Set.GetHashCode() +
     IsText49Set.GetHashCode() +
     IsText50Set.GetHashCode() +
     IsValue1Set.GetHashCode() +
     IsValue2Set.GetHashCode() +
     IsValue3Set.GetHashCode() +
     IsValue4Set.GetHashCode() +
     IsValue5Set.GetHashCode() +
     IsValue6Set.GetHashCode() +
     IsValue7Set.GetHashCode() +
     IsValue8Set.GetHashCode() +
     IsValue9Set.GetHashCode() +
     IsValue10Set.GetHashCode() +
     IsValue11Set.GetHashCode() +
     IsValue12Set.GetHashCode() +
     IsValue13Set.GetHashCode() +
     IsValue14Set.GetHashCode() +
     IsValue15Set.GetHashCode() +
     IsValue16Set.GetHashCode() +
     IsValue17Set.GetHashCode() +
     IsValue18Set.GetHashCode() +
     IsValue19Set.GetHashCode() +
     IsValue20Set.GetHashCode() +
     IsValue21Set.GetHashCode() +
     IsValue22Set.GetHashCode() +
     IsValue23Set.GetHashCode() +
     IsValue24Set.GetHashCode() +
     IsValue25Set.GetHashCode() +
     IsValue26Set.GetHashCode() +
     IsValue27Set.GetHashCode() +
     IsValue28Set.GetHashCode() +
     IsValue29Set.GetHashCode() +
     IsValue30Set.GetHashCode() +
     IsValue31Set.GetHashCode() +
     IsValue32Set.GetHashCode() +
     IsValue33Set.GetHashCode() +
     IsValue34Set.GetHashCode() +
     IsValue35Set.GetHashCode() +
     IsValue36Set.GetHashCode() +
     IsValue37Set.GetHashCode() +
     IsValue38Set.GetHashCode() +
     IsValue39Set.GetHashCode() +
     IsValue40Set.GetHashCode() +
     IsValue41Set.GetHashCode() +
     IsValue42Set.GetHashCode() +
     IsValue43Set.GetHashCode() +
     IsValue44Set.GetHashCode() +
     IsValue45Set.GetHashCode() +
     IsValue46Set.GetHashCode() +
     IsValue47Set.GetHashCode() +
     IsValue48Set.GetHashCode() +
     IsValue49Set.GetHashCode() +
     IsValue50Set.GetHashCode() +
     IsFlag1Set.GetHashCode() +
     IsFlag2Set.GetHashCode() +
     IsFlag3Set.GetHashCode() +
     IsFlag4Set.GetHashCode() +
     IsFlag5Set.GetHashCode() +
     IsFlag6Set.GetHashCode() +
     IsFlag7Set.GetHashCode() +
     IsFlag8Set.GetHashCode() +
     IsFlag9Set.GetHashCode() +
     IsFlag10Set.GetHashCode() +
     IsDate1Set.GetHashCode() +
     IsDate2Set.GetHashCode() +
     IsDate3Set.GetHashCode() +
     IsDate4Set.GetHashCode() +
     IsDate5Set.GetHashCode() +
     IsDate6Set.GetHashCode() +
     IsDate7Set.GetHashCode() +
     IsDate8Set.GetHashCode() +
     IsDate9Set.GetHashCode() +
     IsDate10Set.GetHashCode() +
     IsNote1Set.GetHashCode() +
     IsNote2Set.GetHashCode() +
     IsNote3Set.GetHashCode() +
     IsNote4Set.GetHashCode() +
     IsNote5Set.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomAttributeDataIsSetDto other = obj as CustomAttributeDataIsSetDto;
            if (other != null)
            {

                if (other.IsParentIdSet != this.IsParentIdSet) { return false; }
                if (other.IsParentTypeSet != this.IsParentTypeSet) { return false; }
                if (other.IsText1Set != this.IsText1Set) { return false; }
                if (other.IsText2Set != this.IsText2Set) { return false; }
                if (other.IsText3Set != this.IsText3Set) { return false; }
                if (other.IsText4Set != this.IsText4Set) { return false; }
                if (other.IsText5Set != this.IsText5Set) { return false; }
                if (other.IsText6Set != this.IsText6Set) { return false; }
                if (other.IsText7Set != this.IsText7Set) { return false; }
                if (other.IsText8Set != this.IsText8Set) { return false; }
                if (other.IsText9Set != this.IsText9Set) { return false; }
                if (other.IsText10Set != this.IsText10Set) { return false; }
                if (other.IsText11Set != this.IsText11Set) { return false; }
                if (other.IsText12Set != this.IsText12Set) { return false; }
                if (other.IsText13Set != this.IsText13Set) { return false; }
                if (other.IsText14Set != this.IsText14Set) { return false; }
                if (other.IsText15Set != this.IsText15Set) { return false; }
                if (other.IsText16Set != this.IsText16Set) { return false; }
                if (other.IsText17Set != this.IsText17Set) { return false; }
                if (other.IsText18Set != this.IsText18Set) { return false; }
                if (other.IsText19Set != this.IsText19Set) { return false; }
                if (other.IsText20Set != this.IsText20Set) { return false; }
                if (other.IsText21Set != this.IsText21Set) { return false; }
                if (other.IsText22Set != this.IsText22Set) { return false; }
                if (other.IsText23Set != this.IsText23Set) { return false; }
                if (other.IsText24Set != this.IsText24Set) { return false; }
                if (other.IsText25Set != this.IsText25Set) { return false; }
                if (other.IsText26Set != this.IsText26Set) { return false; }
                if (other.IsText27Set != this.IsText27Set) { return false; }
                if (other.IsText28Set != this.IsText28Set) { return false; }
                if (other.IsText29Set != this.IsText29Set) { return false; }
                if (other.IsText30Set != this.IsText30Set) { return false; }
                if (other.IsText31Set != this.IsText31Set) { return false; }
                if (other.IsText32Set != this.IsText32Set) { return false; }
                if (other.IsText33Set != this.IsText33Set) { return false; }
                if (other.IsText34Set != this.IsText34Set) { return false; }
                if (other.IsText35Set != this.IsText35Set) { return false; }
                if (other.IsText36Set != this.IsText36Set) { return false; }
                if (other.IsText37Set != this.IsText37Set) { return false; }
                if (other.IsText38Set != this.IsText38Set) { return false; }
                if (other.IsText39Set != this.IsText39Set) { return false; }
                if (other.IsText40Set != this.IsText40Set) { return false; }
                if (other.IsText41Set != this.IsText41Set) { return false; }
                if (other.IsText42Set != this.IsText42Set) { return false; }
                if (other.IsText43Set != this.IsText43Set) { return false; }
                if (other.IsText44Set != this.IsText44Set) { return false; }
                if (other.IsText45Set != this.IsText45Set) { return false; }
                if (other.IsText46Set != this.IsText46Set) { return false; }
                if (other.IsText47Set != this.IsText47Set) { return false; }
                if (other.IsText48Set != this.IsText48Set) { return false; }
                if (other.IsText49Set != this.IsText49Set) { return false; }
                if (other.IsText50Set != this.IsText50Set) { return false; }
                if (other.IsValue1Set != this.IsValue1Set) { return false; }
                if (other.IsValue2Set != this.IsValue2Set) { return false; }
                if (other.IsValue3Set != this.IsValue3Set) { return false; }
                if (other.IsValue4Set != this.IsValue4Set) { return false; }
                if (other.IsValue5Set != this.IsValue5Set) { return false; }
                if (other.IsValue6Set != this.IsValue6Set) { return false; }
                if (other.IsValue7Set != this.IsValue7Set) { return false; }
                if (other.IsValue8Set != this.IsValue8Set) { return false; }
                if (other.IsValue9Set != this.IsValue9Set) { return false; }
                if (other.IsValue10Set != this.IsValue10Set) { return false; }
                if (other.IsValue11Set != this.IsValue11Set) { return false; }
                if (other.IsValue12Set != this.IsValue12Set) { return false; }
                if (other.IsValue13Set != this.IsValue13Set) { return false; }
                if (other.IsValue14Set != this.IsValue14Set) { return false; }
                if (other.IsValue15Set != this.IsValue15Set) { return false; }
                if (other.IsValue16Set != this.IsValue16Set) { return false; }
                if (other.IsValue17Set != this.IsValue17Set) { return false; }
                if (other.IsValue18Set != this.IsValue18Set) { return false; }
                if (other.IsValue19Set != this.IsValue19Set) { return false; }
                if (other.IsValue20Set != this.IsValue20Set) { return false; }
                if (other.IsValue21Set != this.IsValue21Set) { return false; }
                if (other.IsValue22Set != this.IsValue22Set) { return false; }
                if (other.IsValue23Set != this.IsValue23Set) { return false; }
                if (other.IsValue24Set != this.IsValue24Set) { return false; }
                if (other.IsValue25Set != this.IsValue25Set) { return false; }
                if (other.IsValue26Set != this.IsValue26Set) { return false; }
                if (other.IsValue27Set != this.IsValue27Set) { return false; }
                if (other.IsValue28Set != this.IsValue28Set) { return false; }
                if (other.IsValue29Set != this.IsValue29Set) { return false; }
                if (other.IsValue30Set != this.IsValue30Set) { return false; }
                if (other.IsValue31Set != this.IsValue31Set) { return false; }
                if (other.IsValue32Set != this.IsValue32Set) { return false; }
                if (other.IsValue33Set != this.IsValue33Set) { return false; }
                if (other.IsValue34Set != this.IsValue34Set) { return false; }
                if (other.IsValue35Set != this.IsValue35Set) { return false; }
                if (other.IsValue36Set != this.IsValue36Set) { return false; }
                if (other.IsValue37Set != this.IsValue37Set) { return false; }
                if (other.IsValue38Set != this.IsValue38Set) { return false; }
                if (other.IsValue39Set != this.IsValue39Set) { return false; }
                if (other.IsValue40Set != this.IsValue40Set) { return false; }
                if (other.IsValue41Set != this.IsValue41Set) { return false; }
                if (other.IsValue42Set != this.IsValue42Set) { return false; }
                if (other.IsValue43Set != this.IsValue43Set) { return false; }
                if (other.IsValue44Set != this.IsValue44Set) { return false; }
                if (other.IsValue45Set != this.IsValue45Set) { return false; }
                if (other.IsValue46Set != this.IsValue46Set) { return false; }
                if (other.IsValue47Set != this.IsValue47Set) { return false; }
                if (other.IsValue48Set != this.IsValue48Set) { return false; }
                if (other.IsValue49Set != this.IsValue49Set) { return false; }
                if (other.IsValue50Set != this.IsValue50Set) { return false; }
                if (other.IsFlag1Set != this.IsFlag1Set) { return false; }
                if (other.IsFlag2Set != this.IsFlag2Set) { return false; }
                if (other.IsFlag3Set != this.IsFlag3Set) { return false; }
                if (other.IsFlag4Set != this.IsFlag4Set) { return false; }
                if (other.IsFlag5Set != this.IsFlag5Set) { return false; }
                if (other.IsFlag6Set != this.IsFlag6Set) { return false; }
                if (other.IsFlag7Set != this.IsFlag7Set) { return false; }
                if (other.IsFlag8Set != this.IsFlag8Set) { return false; }
                if (other.IsFlag9Set != this.IsFlag9Set) { return false; }
                if (other.IsFlag10Set != this.IsFlag10Set) { return false; }
                if (other.IsDate1Set != this.IsDate1Set) { return false; }
                if (other.IsDate2Set != this.IsDate2Set) { return false; }
                if (other.IsDate3Set != this.IsDate3Set) { return false; }
                if (other.IsDate4Set != this.IsDate4Set) { return false; }
                if (other.IsDate5Set != this.IsDate5Set) { return false; }
                if (other.IsDate6Set != this.IsDate6Set) { return false; }
                if (other.IsDate7Set != this.IsDate7Set) { return false; }
                if (other.IsDate8Set != this.IsDate8Set) { return false; }
                if (other.IsDate9Set != this.IsDate9Set) { return false; }
                if (other.IsDate10Set != this.IsDate10Set) { return false; }
                if (other.IsNote1Set != this.IsNote1Set) { return false; }
                if (other.IsNote2Set != this.IsNote2Set) { return false; }
                if (other.IsNote3Set != this.IsNote3Set) { return false; }
                if (other.IsNote4Set != this.IsNote4Set) { return false; }
                if (other.IsNote5Set != this.IsNote5Set) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {

            IsParentIdSet = isSet;
            IsParentTypeSet = isSet;
            IsText1Set = isSet;
            IsText2Set = isSet;
            IsText3Set = isSet;
            IsText4Set = isSet;
            IsText5Set = isSet;
            IsText6Set = isSet;
            IsText7Set = isSet;
            IsText8Set = isSet;
            IsText9Set = isSet;
            IsText10Set = isSet;
            IsText11Set = isSet;
            IsText12Set = isSet;
            IsText13Set = isSet;
            IsText14Set = isSet;
            IsText15Set = isSet;
            IsText16Set = isSet;
            IsText17Set = isSet;
            IsText18Set = isSet;
            IsText19Set = isSet;
            IsText20Set = isSet;
            IsText21Set = isSet;
            IsText22Set = isSet;
            IsText23Set = isSet;
            IsText24Set = isSet;
            IsText25Set = isSet;
            IsText26Set = isSet;
            IsText27Set = isSet;
            IsText28Set = isSet;
            IsText29Set = isSet;
            IsText30Set = isSet;
            IsText31Set = isSet;
            IsText32Set = isSet;
            IsText33Set = isSet;
            IsText34Set = isSet;
            IsText35Set = isSet;
            IsText36Set = isSet;
            IsText37Set = isSet;
            IsText38Set = isSet;
            IsText39Set = isSet;
            IsText40Set = isSet;
            IsText41Set = isSet;
            IsText42Set = isSet;
            IsText43Set = isSet;
            IsText44Set = isSet;
            IsText45Set = isSet;
            IsText46Set = isSet;
            IsText47Set = isSet;
            IsText48Set = isSet;
            IsText49Set = isSet;
            IsText50Set = isSet;
            IsValue1Set = isSet;
            IsValue2Set = isSet;
            IsValue3Set = isSet;
            IsValue4Set = isSet;
            IsValue5Set = isSet;
            IsValue6Set = isSet;
            IsValue7Set = isSet;
            IsValue8Set = isSet;
            IsValue9Set = isSet;
            IsValue10Set = isSet;
            IsValue11Set = isSet;
            IsValue12Set = isSet;
            IsValue13Set = isSet;
            IsValue14Set = isSet;
            IsValue15Set = isSet;
            IsValue16Set = isSet;
            IsValue17Set = isSet;
            IsValue18Set = isSet;
            IsValue19Set = isSet;
            IsValue20Set = isSet;
            IsValue21Set = isSet;
            IsValue22Set = isSet;
            IsValue23Set = isSet;
            IsValue24Set = isSet;
            IsValue25Set = isSet;
            IsValue26Set = isSet;
            IsValue27Set = isSet;
            IsValue28Set = isSet;
            IsValue29Set = isSet;
            IsValue30Set = isSet;
            IsValue31Set = isSet;
            IsValue32Set = isSet;
            IsValue33Set = isSet;
            IsValue34Set = isSet;
            IsValue35Set = isSet;
            IsValue36Set = isSet;
            IsValue37Set = isSet;
            IsValue38Set = isSet;
            IsValue39Set = isSet;
            IsValue40Set = isSet;
            IsValue41Set = isSet;
            IsValue42Set = isSet;
            IsValue43Set = isSet;
            IsValue44Set = isSet;
            IsValue45Set = isSet;
            IsValue46Set = isSet;
            IsValue47Set = isSet;
            IsValue48Set = isSet;
            IsValue49Set = isSet;
            IsValue50Set = isSet;
            IsFlag1Set = isSet;
            IsFlag2Set = isSet;
            IsFlag3Set = isSet;
            IsFlag4Set = isSet;
            IsFlag5Set = isSet;
            IsFlag6Set = isSet;
            IsFlag7Set = isSet;
            IsFlag8Set = isSet;
            IsFlag9Set = isSet;
            IsFlag10Set = isSet;
            IsDate1Set = isSet;
            IsDate2Set = isSet;
            IsDate3Set = isSet;
            IsDate4Set = isSet;
            IsDate5Set = isSet;
            IsDate6Set = isSet;
            IsDate7Set = isSet;
            IsDate8Set = isSet;
            IsDate9Set = isSet;
            IsDate10Set = isSet;
            IsNote1Set = isSet;
            IsNote2Set = isSet;
            IsNote3Set = isSet;
            IsNote4Set = isSet;
            IsNote5Set = isSet;
        }
        #endregion
    }
}

