﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramAssortmentLocalProductDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAssortmentLocalProductDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentLocalProductDtoKey
                {
                    PlanogramAssortmentId = this.PlanogramAssortmentId,
                    ProductGtin = this.ProductGtin,
                    LocationCode = this.LocationCode
                };
            }
        }
        [ForeignKey(typeof(PlanogramAssortmentDto), typeof(IPlanogramAssortmentDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentId { get; set; }
        public String ProductGtin { get; set; }
        public String LocationCode { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentLocalProductDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }

            return this.ProductGtin == other.ProductGtin && this.LocationCode == other.LocationCode;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentLocalProductDtoKey
    {
        #region Properties
        public Object PlanogramAssortmentId { get; set; }
        public String ProductGtin { get; set; }
        public String LocationCode { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return PlanogramAssortmentId.GetHashCode() + ProductGtin.GetHashCode() + LocationCode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentLocalProductDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentId
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId != null))
            {
                if (!other.PlanogramAssortmentId.Equals(this.PlanogramAssortmentId)) { return false; }
            }
            if ((other.PlanogramAssortmentId != null) && (this.PlanogramAssortmentId == null)) { return false; }
            if ((other.PlanogramAssortmentId == null) && (this.PlanogramAssortmentId != null)) { return false; }

            return this.ProductGtin == other.ProductGtin && this.LocationCode == other.LocationCode;
        }
        #endregion
    }
}
