﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramAssortmentRegionProductDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAssortmentRegionProductDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentRegionProductDtoKey
                {
                    PlanogramAssortmentRegionId = this.PlanogramAssortmentRegionId,
                    PrimaryProductGtin = this.PrimaryProductGtin
                };
            }
        }
        [ForeignKey(typeof(PlanogramAssortmentRegionDto), typeof(IPlanogramAssortmentRegionDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentRegionId { get; set; }
        public String PrimaryProductGtin { get; set; }
        public String RegionalProductGtin { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentRegionProductDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentRegionId
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId != null))
            {
                if (!other.PlanogramAssortmentRegionId.Equals(this.PlanogramAssortmentRegionId)) { return false; }
            }
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId == null)) { return false; }
            if ((other.PlanogramAssortmentRegionId == null) && (this.PlanogramAssortmentRegionId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            return this.PrimaryProductGtin == other.PrimaryProductGtin &&
                this.RegionalProductGtin == other.RegionalProductGtin;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentRegionProductDtoKey
    {
        #region Properties
        public Object PlanogramAssortmentRegionId { get; set; }
        public String PrimaryProductGtin { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return PlanogramAssortmentRegionId.GetHashCode() + PrimaryProductGtin.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentRegionProductDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentRegionId
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId != null))
            {
                if (!other.PlanogramAssortmentRegionId.Equals(this.PlanogramAssortmentRegionId)) { return false; }
            }
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId == null)) { return false; }
            if ((other.PlanogramAssortmentRegionId == null) && (this.PlanogramAssortmentRegionId != null)) { return false; }
            return this.PrimaryProductGtin == other.PrimaryProductGtin;
        }
        #endregion
    }
}
