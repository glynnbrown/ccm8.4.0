﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-28622 : D.Pleasance
//  Created
#endregion
#region Version History: CCM810
// V8-30215 : M.Brumby
//  Added AggregationType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PackageMetricMapping Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramMetricMappingDto
    {
        #region Properties
        public String Name { get; set; }
        public String Description { get; set; }
        public Byte Direction { get; set; }
        public Byte SpecialType { get; set; }
        public Byte MetricType { get; set; }
        public Byte MetricId { get; set; }
        public String Source { get; set; }
        public Byte AggregationType { get; set; }
        public PackageMetricMappingDtoKey DtoKey
        {
            get { return new PackageMetricMappingDtoKey() { MetricId = this.MetricId, Source = this.Source }; }
        }
        #endregion

        #region Methods
        public override bool Equals(Object obj)
        {
            PlanogramMetricMappingDto other = obj as PlanogramMetricMappingDto;
            if (other != null)
            {
                if (other.Name != this.Name) return false;
                if (other.Description != this.Description) return false;
                if (other.Direction != this.Direction) return false;
                if (other.SpecialType != this.SpecialType) return false;
                if (other.MetricType != this.MetricType) return false;
                if (other.MetricId != this.MetricId) return false;
                if (other.Source != this.Source) return false;
                if (other.AggregationType != this.AggregationType) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return
                this.MetricId.GetHashCode() ^
                this.Source.GetHashCode();
        }
        #endregion
    }

    public class PackageMetricMappingDtoKey
    {
        #region Properties
        public Byte MetricId { get; set; }
        public String Source { get; set; }
        #endregion

        #region Methods
        public override bool Equals(Object obj)
        {
            PackageMetricMappingDtoKey other = obj as PackageMetricMappingDtoKey;
            if (other != null)
            {
                if (other.MetricId != this.MetricId) return false;
                if (other.Source != this.Source) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return this.Source.GetHashCode() ^
                this.MetricId.GetHashCode();
        }
        #endregion
    }
}