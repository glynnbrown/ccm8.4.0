﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27919 : L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    /// <summary>
    /// PackageFieldMapping Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramFieldMappingDto
    {
        #region Properties
        public Byte Type { get; set; }
        public String Target { get; set; }
        public String Source { get; set; }
        public PackageFieldMappingDtoKey DtoKey
        {
            get { return new PackageFieldMappingDtoKey() { Target = this.Target, Source = this.Source }; }
        }
        #endregion

        #region Methods
        public override bool Equals(Object obj)
        {
            PlanogramFieldMappingDto other = obj as PlanogramFieldMappingDto;
            if (other != null)
            {
                if (other.Type != this.Type) return false;
                if (other.Target != this.Target) return false;
                if (other.Source != this.Source) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return
                this.Type.GetHashCode() ^
                this.Target.GetHashCode() ^
                this.Source.GetHashCode();
        }
        #endregion
    }

    public class PackageFieldMappingDtoKey
    {
        #region Properties
        public String Target { get; set; }
        public String Source { get; set; }
        #endregion

        #region Methods
        public override bool Equals(Object obj)
        {
            PackageFieldMappingDtoKey other = obj as PackageFieldMappingDtoKey;
            if (other != null)
            {
                if (other.Target != this.Target) return false;
                if (other.Source != this.Source) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return this.Source.GetHashCode() ^
                this.Target.GetHashCode();
        }
        #endregion
    }
}
