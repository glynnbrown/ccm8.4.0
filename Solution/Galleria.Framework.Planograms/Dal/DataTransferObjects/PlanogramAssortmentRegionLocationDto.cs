﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramAssortmentRegionLocationDto
    {
        #region Properties
        public Object Id { get; set; }
        public PlanogramAssortmentRegionLocationDtoKey DtoKey
        {
            get
            {
                return new PlanogramAssortmentRegionLocationDtoKey
                {
                    LocationCode = this.LocationCode,
                    PlanogramAssortmentRegionId = this.PlanogramAssortmentRegionId
                };
            }
        }
        [ForeignKey(typeof(PlanogramAssortmentRegionDto), typeof(IPlanogramAssortmentRegionDal), DeleteBehavior.Cascade)]
        public Object PlanogramAssortmentRegionId { get; set; }
        public String LocationCode { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentRegionLocationDto;
            if (other == null) return false;

            // Id
            if ((other.Id != null) && (this.Id != null))
            {
                if (!other.Id.Equals(this.Id)) { return false; }
            }
            if ((other.Id != null) && (this.Id == null)) { return false; }
            if ((other.Id == null) && (this.Id != null)) { return false; }
            // PlanogramAssortmentRegionId
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId != null))
            {
                if (!other.PlanogramAssortmentRegionId.Equals(this.PlanogramAssortmentRegionId)) { return false; }
            }
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId == null)) { return false; }
            if ((other.PlanogramAssortmentRegionId == null) && (this.PlanogramAssortmentRegionId != null)) { return false; }
            // ExtendedData
            if ((other.ExtendedData != null) && (this.ExtendedData != null))
            {
                if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
            }
            if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
            if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            return this.LocationCode == other.LocationCode;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramAssortmentRegionLocationDtoKey
    {
        #region Properties
        public Object PlanogramAssortmentRegionId { get; set; }
        public String LocationCode { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return PlanogramAssortmentRegionId.GetHashCode() + LocationCode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as PlanogramAssortmentRegionLocationDtoKey;
            if (other == null) return false;

            // PlanogramAssortmentRegionId
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId != null))
            {
                if (!other.PlanogramAssortmentRegionId.Equals(this.PlanogramAssortmentRegionId)) { return false; }
            }
            if ((other.PlanogramAssortmentRegionId != null) && (this.PlanogramAssortmentRegionId == null)) { return false; }
            if ((other.PlanogramAssortmentRegionId == null) && (this.PlanogramAssortmentRegionId != null)) { return false; }

            return this.LocationCode == other.LocationCode;
        }
        #endregion
    }
}
