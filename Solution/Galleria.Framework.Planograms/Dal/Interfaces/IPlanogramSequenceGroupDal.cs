﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    /// <summary>
    ///     Provides methods to interact with the DAL for a planogram sequence group.
    /// </summary>
    [DefaultFetchMethod("FetchByPlanogramSequenceId", "PlanogramSequenceId")]
    public interface IPlanogramSequenceGroupDal : IBatchDal<PlanogramSequenceGroupDto>
    {
        IEnumerable<PlanogramSequenceGroupDto> FetchByPlanogramSequenceId(Object planogramSequenceId);
        new void Insert(PlanogramSequenceGroupDto dto);
        new void Update(PlanogramSequenceGroupDto dto);
        void DeleteById(Object id);
    }
}