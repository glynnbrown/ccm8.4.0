﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-23658 : K.Pickup
//  Added DefaultFetchMethod property.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramFixtureId", "PlanogramFixtureId")]
    public interface IPlanogramFixtureComponentDal : IBatchDal<PlanogramFixtureComponentDto>
    {
        IEnumerable<PlanogramFixtureComponentDto> FetchByPlanogramFixtureId(Object planogramFixtureId);
        void Insert(PlanogramFixtureComponentDto dto);
        void Update(PlanogramFixtureComponentDto dto);
        void DeleteById(Object id);
    }
}
