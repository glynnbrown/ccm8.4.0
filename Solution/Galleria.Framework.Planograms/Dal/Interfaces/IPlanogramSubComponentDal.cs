﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//	Created (Auto-generated)
// V8-23658 : K.Pickup
//  Added DefaultFetchMethod property.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramComponentId", "PlanogramComponentId")]
    public interface IPlanogramSubComponentDal : IBatchDal<PlanogramSubComponentDto>
    {
        IEnumerable<PlanogramSubComponentDto> FetchByPlanogramComponentId(Object planogramComponentId);
        void Insert(PlanogramSubComponentDto dto);
        void Update(PlanogramSubComponentDto dto);
        void DeleteById(Object id);
    }
}
