﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-23658 : K.Pickup
//  Added DefaultFetchMethod property.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPackageId", "PackageId")]
    public interface IPlanogramDal : IDal
    {
        IEnumerable<PlanogramDto> FetchByPackageId(Object packageId);
        void Insert(PackageDto packageDto, PlanogramDto planogramDto);
        void Update(PackageDto packageDto, PlanogramDto planogramDto);
        void DeleteById(Object id);

        /// <summary>
        /// Present only to get the dal testing framework to work
        /// </summary>
        [Obsolete]
        void Insert(PlanogramDto planogramDto);
        /// <summary>
        /// Present only to get the dal testing framework to work
        /// </summary>
        [Obsolete]
        void Update(PlanogramDto planogramDto);
    }
}
