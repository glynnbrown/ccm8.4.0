﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramId", "PlanogramId")]
    public interface IPlanogramBlockingDal : IBatchDal<PlanogramBlockingDto>
    {
        IEnumerable<PlanogramBlockingDto> FetchByPlanogramId(Object id);
        void Insert(PlanogramBlockingDto dto);
        void Update(PlanogramBlockingDto dto);
        void DeleteById(Object id);
    }
}
