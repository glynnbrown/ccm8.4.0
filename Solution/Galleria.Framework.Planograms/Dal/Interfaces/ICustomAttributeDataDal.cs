﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26147 : L.Ineson
//		Created (Auto-generated)
// CCM-26322 : L.Ineson
//      Added upsert.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-31456 : Added operation to fetch attributes by group of parent ids
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByParentTypeParentId", "ParentType", "ParentId")]
    [DefaultDeleteMethod("DeleteById", "Id")]
    public interface ICustomAttributeDataDal : IBatchDal<CustomAttributeDataDto>
    {
        CustomAttributeDataDto FetchByParentTypeParentId(Byte parentType, Object parentId);
        IEnumerable<CustomAttributeDataDto> FetchByParentTypeParentIds(Byte parentType, IEnumerable<Object> parentIds);
        void Insert(CustomAttributeDataDto dto); 
        void Update(CustomAttributeDataDto dto);
        void Upsert(IEnumerable<CustomAttributeDataDto> dtoList, CustomAttributeDataIsSetDto isSetDto);
        void DeleteById(Object id);
    }
}
