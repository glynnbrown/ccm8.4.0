﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramAssortmentId", "PlanogramAssortmentId")]
    public interface IPlanogramAssortmentInventoryRuleDal : IBatchDal<PlanogramAssortmentInventoryRuleDto>
    {
        IEnumerable<PlanogramAssortmentInventoryRuleDto> FetchByPlanogramAssortmentId(Object assortmentId);
        void Insert(PlanogramAssortmentInventoryRuleDto dto);
        void Update(PlanogramAssortmentInventoryRuleDto dto);
        void DeleteById(Object id);
    }
}
