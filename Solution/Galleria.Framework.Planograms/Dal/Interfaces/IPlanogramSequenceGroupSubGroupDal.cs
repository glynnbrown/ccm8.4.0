﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    /// <summary>
    ///     Provides methods to interact with the DAL for a planogram sequence group sub group.
    /// </summary>
    [DefaultFetchMethod("FetchByPlanogramSequenceGroupId", "PlanogramSequenceGroupId")]
    public interface IPlanogramSequenceGroupSubGroupDal : IBatchDal<PlanogramSequenceGroupSubGroupDto>
    {
        IEnumerable<PlanogramSequenceGroupSubGroupDto> FetchByPlanogramSequenceGroupId(Object planogramSequenceGroupId);
        new void Insert(PlanogramSequenceGroupSubGroupDto dto);
        new void Update(PlanogramSequenceGroupSubGroupDto dto);
        void DeleteById(Object id);
    }
}
