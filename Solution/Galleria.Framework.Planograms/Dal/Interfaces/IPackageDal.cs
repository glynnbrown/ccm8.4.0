﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-24290 : N.Foster
//  Created
// V8-25949 : N.Foster
//  Changed LockById to return success or failure
// V8-27411 : M.Pettit
//  LockById now requires userId and locktype parameters
//  UnlockById now requires userId parameters
// V8-27919 : L.Ineson
//  Added fetchArgument parameter to FetchById
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    public interface IPackageDal : IDal
    {
        Byte LockById(Object id, Object lockUserId, Byte lockType, Boolean lockReadOnly);
        Byte UnlockById(Object id, Object lockUserId, Byte lockType, Boolean lockReadOnly);
        PackageDto FetchById(Object id);
        PackageDto FetchById(Object id, Object fetchArgument);
        void Insert(PackageDto dto);
        void Update(PackageDto dto);
        void DeleteById(Object id);

        Byte UpdatePlanogramAttributes(IEnumerable<PlanogramAttributesDto> planAttributesDtoList);
        IList<object> FetchIdsExceptFor(IEnumerable<string> ucrs);
        IList<object> FetchByUcrsDateLastModified(IEnumerable<String> ucrs, DateTime dateLastModified);
        IList<object> FetchDeletedByUcrs(IEnumerable<String> ucrs);
    }
}
