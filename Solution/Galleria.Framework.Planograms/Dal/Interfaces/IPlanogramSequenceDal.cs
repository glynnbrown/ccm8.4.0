﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    /// <summary>
    ///     Provides methods to interact with the DAL for a planogram sequence.
    /// </summary>
    [DefaultFetchMethod("FetchByPlanogramId", "PlanogramId")]
    public interface IPlanogramSequenceDal : IBatchDal<PlanogramSequenceDto>
    {
        PlanogramSequenceDto FetchByPlanogramId(Object planogramId);
        new void Insert(PlanogramSequenceDto dto);
        new void Update(PlanogramSequenceDto dto);
        void DeleteById(Object id);
    }
}
