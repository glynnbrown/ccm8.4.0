﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramId", "PlanogramId")]
    public interface IPlanogramValidationTemplateDal : IBatchDal<PlanogramValidationTemplateDto>
    {
        PlanogramValidationTemplateDto FetchByPlanogramId(Object id);
        void Insert(PlanogramValidationTemplateDto dto);
        void Update(PlanogramValidationTemplateDto dto);
        void DeleteById(Object id);
    }
}
