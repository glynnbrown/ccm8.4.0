﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramAssortmentId", "PlanogramAssortmentId")]
    public interface IPlanogramAssortmentProductBuddyDal : IBatchDal<PlanogramAssortmentProductBuddyDto>
    {
        IEnumerable<PlanogramAssortmentProductBuddyDto> FetchByPlanogramAssortmentId(Object assortmentId);
        void Insert(PlanogramAssortmentProductBuddyDto dto);
        void Update(PlanogramAssortmentProductBuddyDto dto);
        void DeleteById(Object id);
    }
}
