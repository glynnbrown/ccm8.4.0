﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramValidationTemplateId", "PlanogramValidationTemplateId")]
    public interface IPlanogramValidationTemplateGroupDal : IBatchDal<PlanogramValidationTemplateGroupDto>
    {
        IEnumerable<PlanogramValidationTemplateGroupDto> FetchByPlanogramValidationTemplateId(Object planogramValidationTemplateId);
        void Insert(PlanogramValidationTemplateGroupDto dto);
        void Update(PlanogramValidationTemplateGroupDto dto);
        void DeleteById(Object id);
    }
}
