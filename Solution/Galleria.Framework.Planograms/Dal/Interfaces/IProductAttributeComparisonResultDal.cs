﻿using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramId", "PlanogramId")]
    [DefaultDeleteMethod("DeleteByPlanogramId", "PlanogramId")]
    public interface IProductAttributeComparisonResultDal : IBatchDal<ProductAttributeComparisonResultDto>
    {
        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison</c> model object that belongs to a <c>Planogram</c> with the given<paramref name="PlanogramId"/>.
        /// </summary>
        /// <param name="id"><see cref="object"/> containing the id of the parent <c>Planogram</c>.</param>
        /// <returns>A new instance of <see cref="EntityComparisonAttributeDto"/>.</returns>
        IEnumerable<ProductAttributeComparisonResultDto> FetchByPlanogramId(Object planogramId);

        /// <summary>
        ///     Insert the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        new void Insert(ProductAttributeComparisonResultDto dto);

        /// <summary>
        ///     Update the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        new void Update(ProductAttributeComparisonResultDto dto);

        /// <summary>
        ///     Delete the data linked to the given <paramref name="id"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(Object id);
    }
}
