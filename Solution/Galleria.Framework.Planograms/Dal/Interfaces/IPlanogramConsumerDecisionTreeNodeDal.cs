﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong 
//   Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramConsumerDecisionTreeId", "PlanogramConsumerDecisionTreeId")]
    public interface IPlanogramConsumerDecisionTreeNodeDal : IBatchDal<PlanogramConsumerDecisionTreeNodeDto>
    {
        IEnumerable<PlanogramConsumerDecisionTreeNodeDto> FetchByPlanogramConsumerDecisionTreeId(Object planogramConsumerDecisionTreeId);
        void Insert(PlanogramConsumerDecisionTreeNodeDto dto);
        void Update(PlanogramConsumerDecisionTreeNodeDto dto);
        void DeleteById(Object id);
    }
}
