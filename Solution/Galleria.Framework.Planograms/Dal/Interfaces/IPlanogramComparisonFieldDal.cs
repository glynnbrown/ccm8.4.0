﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    /// <summary>
    ///     Interface declaring the expected behavior for a <c>Planogram Comparison Field</c> DAL implementation.
    /// </summary>
    /// <remarks>The default fetch method is <c>FetchByPlanogramComparisonId</c> which requires the <c>PlanogramComparisonId</c> value.</remarks>
    [DefaultFetchMethod("FetchByPlanogramComparisonId", "PlanogramComparisonId")]
    public interface IPlanogramComparisonFieldDal : IBatchDal<PlanogramComparisonFieldDto>
    {
        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Field</c> model object that belongs to a <c>Planogram Comparison</c> with the given<paramref name="id"/>.
        /// </summary>
        /// <param name="id"><see cref="Object"/> containing the id of the parent <c>Planogram Comparison</c>.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonFieldDto"/>.</returns>
        IEnumerable<PlanogramComparisonFieldDto> FetchByPlanogramComparisonId(Object id);

        /// <summary>
        ///     Insert the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        new void Insert(PlanogramComparisonFieldDto dto);

        /// <summary>
        ///     Update the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        new void Update(PlanogramComparisonFieldDto dto);

        /// <summary>
        ///     Delete the data linked to the given <paramref name="id"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(Object id);
    }
}