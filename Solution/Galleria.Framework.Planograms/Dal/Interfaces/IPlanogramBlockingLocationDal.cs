﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramBlockingId", "PlanogramBlockingId")]
    public interface IPlanogramBlockingLocationDal : IBatchDal<PlanogramBlockingLocationDto>
    {
        IEnumerable<PlanogramBlockingLocationDto> FetchByPlanogramBlockingId(Object id);
        void Insert(PlanogramBlockingLocationDto dto);
        void Update(PlanogramBlockingLocationDto dto);
        void DeleteById(Object id);
    }
}
