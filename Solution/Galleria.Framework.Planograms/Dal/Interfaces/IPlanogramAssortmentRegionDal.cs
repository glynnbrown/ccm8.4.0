﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramAssortmentId", "PlanogramAssortmentId")]
    public interface IPlanogramAssortmentRegionDal : IBatchDal<PlanogramAssortmentRegionDto>
    {
        IEnumerable<PlanogramAssortmentRegionDto> FetchByPlanogramAssortmentId(Object id);
        void Insert(PlanogramAssortmentRegionDto dto);
        void Update(PlanogramAssortmentRegionDto dto);
        void DeleteById(Object id);
    }
}
