﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramPerformanceId", "PlanogramPerformanceId")]
    public interface IPlanogramPerformanceDataDal : IBatchDal<PlanogramPerformanceDataDto>
    {
        IEnumerable<PlanogramPerformanceDataDto> FetchByPlanogramPerformanceId(Object id);
        void Insert(PlanogramPerformanceDataDto dto);
        void Update(PlanogramPerformanceDataDto dto);
        void DeleteById(Object id);
    }
}
