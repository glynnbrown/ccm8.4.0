﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    /// <summary>
    ///     Provides methods to interact with the DAL for a planogram sequence.
    /// </summary>
    [DefaultFetchMethod("FetchByPlanogramSequenceGroupId", "PlanogramSequenceGroupId")]
    public interface IPlanogramSequenceGroupProductDal :IBatchDal<PlanogramSequenceGroupProductDto>
    {
        IEnumerable<PlanogramSequenceGroupProductDto> FetchByPlanogramSequenceGroupId(Object planogramSequenceGroupId);
        new void Insert(PlanogramSequenceGroupProductDto dto);
        new void Update(PlanogramSequenceGroupProductDto dto);
        void DeleteById(Object id);
    }
}