﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27153 : A.Silva
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByPlanogramId", "PlanogramId")]
    public interface IPlanogramRenumberingStrategyDal : IBatchDal<PlanogramRenumberingStrategyDto>
    {
        /// <summary>
        ///     Fetches all the <see cref="PlanogramRenumberingStrategyDto"/> for a given <paramref name="planogramId"/>.
        /// </summary>
        /// <param name="planogramId">Unique identifier of the planogram to retrieve items from.</param>
        /// <returns>A new collection of <see cref="PlanogramRenumberingStrategyDto"/> instances that match the given <paramref name="planogramId"/>.</returns>
        PlanogramRenumberingStrategyDto FetchByPlanogramId(Object planogramId);

        /// <summary>
        ///     Inserts a planogram with the values from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramRenumberingStrategyDto"/> containing the values to insert.</param>
        void Insert(PlanogramRenumberingStrategyDto dto);

        /// <summary>
        ///     Updates a planogram with the values from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramRenumberingStrategyDto"/> containing the values to update.</param>
        void Update(PlanogramRenumberingStrategyDto dto);

        /// <summary>
        ///     Deletes the planogram renumbering strategy matching the given <paramref name="id"/> from the planogram.
        /// </summary>
        /// <param name="id">Unique id for the planogram renumbering strategy to be removed.</param>
        void DeleteById(Object id);
    }
}
