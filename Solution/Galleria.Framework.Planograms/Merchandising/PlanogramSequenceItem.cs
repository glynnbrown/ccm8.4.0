﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 803
// V8-29137 : A.Kuszyk
//  Created.
#endregion

#region Version History : CCM830
// V8-32539 : A.Kuszyk
// Re-named to PlanogramSequenceItem and made generic to allow for use with blocks as well.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents an item sequenced in a blocking group, with booleans indicating
    /// if the sequence snake direction for this subcomponent in the blocking group is reversed.
    /// </summary>
    public sealed class PlanogramSequencedItem<T> where T : IPlanogramSequencableItem
    {
        #region Properties
        /// <summary>
        /// The sequencable item that this object refers to.
        /// </summary>
        public T Item { get; private set; }

        /// <summary>
        /// True indicates that the blocking group's primary direction should be reversed.
        /// </summary>
        public Boolean IsPrimaryReversed { get; private set; }

        /// <summary>
        /// True indicates that the blocking group's secondary direction should be reversed.
        /// </summary>
        public Boolean IsSecondaryReversed { get; private set; }

        /// <summary>
        /// True indicates that the blocking group's tertiary direction should be reversed.
        /// </summary>
        public Boolean IsTertiaryReversed { get; private set; }

        /// <summary>
        /// The index for the order in which this item should be merchandised in the sequence snake.
        /// </summary>
        public Int32 Sequence { get; private set; }
        #endregion

        #region Constructor
        public PlanogramSequencedItem(
            T item,
            Boolean isPrimaryReversed,
            Boolean isSecondaryReversed,
            Boolean isTertiaryReversed,
            Int32 sequence)
        {
            Item = item;
            IsPrimaryReversed = isPrimaryReversed;
            IsSecondaryReversed = isSecondaryReversed;
            IsTertiaryReversed = isTertiaryReversed;
            Sequence = sequence;
        }
        #endregion
    }

    /// <summary>
    /// An interface for items that can be sequenced by their co-ordinates.
    /// </summary>
    public interface IPlanogramSequencableItem
    {
        /// <summary>
        /// Returns the co-ordinates by which this item should be sequenced.
        /// </summary>
        /// <returns></returns>
        PointValue GetSequencableCoords();
    }
}
