﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
// V8-27738 : N.Foster
//  Changed to a POCO for performance
#endregion
#endregion

using System;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Provides a mechanism for keeping track
    /// of divider placements
    /// </summary>
    public class PlanogramDividerPlacement
    {
        #region Fields
        private Single _x;
        private Single _y;
        private Single _z;
        private Single _width;
        private Single _height;
        private Single _depth;
        #endregion

        #region Properties

        #region X
        /// <summary>
        /// Returns the dividers X position
        /// </summary>
        public Single X
        {
            get { return _x;}
            set { _x= value;}
        }
        #endregion

        #region Y
        /// <summary>
        /// Returns the dividers y position
        /// </summary>
        public Single Y
        {
            get { return _y; }
            set { _y = value; }
        }
        #endregion

        #region Z
        /// <summary>
        /// Returns the dividers z position
        /// </summary>
        public Single Z
        {
            get { return _z; }
            set { _z = value; }
        }
        #endregion

        #region Width
        /// <summary>
        /// Returns the position width
        /// </summary>
        public Single Width
        {
            get { return _width; }
            set { _width = value;}
        }
        #endregion

        #region Height
        /// <summary>
        /// Returns the position height
        /// </summary>
        public Single Height
        {
            get { return _height; }
            set { _height = value; }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Returns the position depth
        /// </summary>
        public Single Depth
        {
            get { return _depth; }
            set { _depth = value; }
        }
        #endregion

        #endregion

        #region Constructors
        private PlanogramDividerPlacement() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramDividerPlacement NewPlanogramDividerPlacement()
        {
            PlanogramDividerPlacement item = new PlanogramDividerPlacement();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create()
        {
        }
        #endregion

        #endregion
    }
}
