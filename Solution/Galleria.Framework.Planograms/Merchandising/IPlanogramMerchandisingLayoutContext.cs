﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016
#endregion

using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Defines that members that are available on a context object for the merchandising layout methods.
    /// </summary>
    /// <typeparam name="TPostLayoutContext">The type of the context object that is used as a parameter for the <see cref="PostLayoutMethods"/>.</typeparam>
    public interface IPlanogramMerchandisingLayoutContext<TPostLayoutContext> : IPlanogramMerchandisingLayoutContext where TPostLayoutContext : class
    {
        /// <summary>
        /// The context object used by the <see cref="PostLayoutMethods"/>.
        /// </summary>
        TPostLayoutContext PostLayoutContext { get; }

        /// <summary>
        /// The methods to be executed after each layout pass, but before compacting block space.
        /// Each of these methods should return a boolean. True indicates that another layout pass
        /// should be executed immediately and this method should be executed again in the next pass. 
        /// False indicates that this method has completed its action and the next method in the collection
        /// should be used. When false is returned, the method will not be executed on subsequent layout passes.
        /// </summary>
        IEnumerable<Func<TPostLayoutContext, Boolean>> PostLayoutMethods { get; }
    }

    /// <summary>
    /// Defines the members that are available on a context object for the merchandising layout methods.
    /// </summary>
    public interface IPlanogramMerchandisingLayoutContext
    {
        /// <summary>
        /// Stores the gtins of the products that were placed in the previous layout pass.
        /// </summary>
        ICollection<String> PreviouslyPlacedGtins { get; }

        /// <summary>
        /// Indicates whether placed products must achieve the units specified in <see cref="TargetUnitsByGtin"/>.
        /// True indicates that the target units must be achieved, whilst false ensures that the <see cref="MinimumUnitsByGtin"/>
        /// are satisfied, even if the target units are not.
        /// </summary>
        Boolean MustAchieveTargetUnits { get; }

        /// <summary>
        /// The minimum units that products should be placed at. If these units cannot be achieved, then products
        /// will not be placed.
        /// </summary>
        Dictionary<String, Int32> MinimumUnitsByGtin { get; }

        /// <summary>
        /// A dictionary containing the ranks of all the products in the planogram, keyed by gtin.
        /// </summary>
        Dictionary<String, Int32> RankByGtin { get; }

        /// <summary>
        /// The target units that products should be placed at. If <see cref="MustAchieveTargetUnits"/> is true, 
        /// then products will only be placed if these units can be achieved. Otherwise, <see cref="MinimumUnitsByGtin"/>
        /// will be satisifed and the units specified here will be achieved if possible.
        /// </summary>
        Dictionary<String, Int32> TargetUnitsByGtin { get; }

        /// <summary>
        /// The stack of products that should be placed by the layout code. They will be popped off in order,
        /// so this stack will have the first products to be placed at the top of the stack. This stack will be 
        /// populated by the <see cref="GenerateProductStackMethod"/>.
        /// </summary>
        Stack<PlanogramProduct> ProductsToPlace { get; }

        /// <summary>
        /// The products that have been skipped due to sequence sub-groups. This stack is cleared and re-populated
        /// on each layout pass.
        /// </summary>
        Stack<PlanogramProduct> SkippedSubGroupProducts { get; }

        /// <summary>
        /// The method that should be used to re-stack the <see cref="ProductsToPlace"/> stack.
        /// </summary>
        Action<Stack<PlanogramProduct>> GenerateProductStackMethod { get; }

        /// <summary>
        /// The planogram being layed out by the layout method.
        /// </summary>
        Planogram Planogram { get; }

        /// <summary>
        /// Indicates whether or not the primary sequence direction is currently being reversed in the layout.
        /// </summary>
        Boolean ReversePrimary { get; set; }

        /// <summary>
        /// Indicates whether or not the secondary sequence direction is currently being reversed in the layout.
        /// </summary>
        Boolean ReverseSecondary { get; set; }

        /// <summary>
        /// Indicates whether or not the tertiary sequence direction is currently being reversed in the layout.
        /// </summary>
        Boolean ReverseTertiary { get; set; }

        /// <summary>
        /// Indicates whether or not the block space should be compacted after a successful layout.
        /// </summary>
        Boolean ShouldCompactBlockSpace { get; }

        /// <summary>
        /// The gtins of products that have been skipped due to first on block behaviour.
        /// </summary>
        ICollection<String> SkippedFirstOnBlock { get; set; }

        /// <summary>
        /// The current placement direction.
        /// </summary>
        PlanogramBlockingGroupPlacementType? CurrentSequenceDirection { get; set; }

        /// <summary>
        /// Indicates if merchandising dimension should be ignored when placing products.
        /// </summary>
        Boolean IgnoreMerchandisingDimensions { get; }

        /// <summary>
        /// The collection of <see cref="PlanogramMerchandisingBlockPlacement"/> instances that did not get any placements 
        /// due to the <c>First on Block</c> rule.
        /// </summary>
        HashSet<PlanogramMerchandisingBlockPlacement> SkippedBlockPlacement { get; }

        /// <summary>
        /// Indicates whether the inventory of positions should be modified by increasing units or facings.
        /// </summary>
        PlanogramMerchandisingInventoryChangeType InventoryChangeType { get; }
    }
}
