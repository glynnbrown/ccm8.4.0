﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
// V8-27738 : N.Foster
//  Refactored to a POCO for performance
// V8-27477 : L.Ineson
//  Added CanBreakMin and CanBreakMax
#endregion
#region Version History: CCM830
// V8-32715 : L.Ineson
//  Divider start is now ignored if the placement strategy is even
#endregion
#endregion

using System;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents the details for an axis
    /// within a merchandising group
    /// </summary>
    [Serializable]
    public class PlanogramMerchandisingGroupAxis
    {
        #region Fields
        private AxisType _axisType;
        private PlanogramPlacementStrategy _strategy;
        private Boolean _isStackedStrategy;
        private Single _dividerSpacing;
        private Single _dividerStart;
        private Single _pegRow1Spacing;
        private Single _pegRow1Start;
        private Single _pegRow2Spacing;
        private Single _pegRow2Start;
        private Single _min;
        private Single _max;
        private Boolean _canBreakMin;
        private Boolean _canBreakMax;
        #endregion

        #region Properties

        #region AxisType
        /// <summary>
        /// Returns the axis type
        /// </summary>
        public AxisType AxisType
        {
            get { return _axisType; }
        }
        #endregion

        #region Strategy
        /// <summary>
        /// Returns the axis placement strategy
        /// </summary>
        public PlanogramPlacementStrategy Strategy
        {
            get { return _strategy; }
        }
        #endregion

        #region IsStackedStrategy
        /// <summary>
        /// Indicates of the strategy is stacked
        /// </summary>
        public Boolean IsStackedStrategy
        {
            get { return _isStackedStrategy; }
        }
        #endregion

        #region DividerSpacing
        /// <summary>
        /// Returns the divider spacing value
        /// </summary>
        public Single DividerSpacing
        {
            get { return _dividerSpacing; }
        }
        #endregion

        #region DividerStart
        /// <summary>
        /// Returns the start position of the divider along this axis
        /// </summary>
        public Single DividerStart
        {
            get { return _dividerStart; }
        }
        #endregion

        #region PegRow1Spacing
        /// <summary>
        /// Returns the peg row 1 spacing value
        /// </summary>
        public Single PegRow1Spacing
        {
            get { return _pegRow1Spacing; }
        }
        #endregion

        #region PegRow1Start
        /// <summary>
        /// Returns the peg row1 start position
        /// </summary>
        public Single PegRow1Start
        {
            get { return _pegRow1Start; }
        }
        #endregion

        #region PegRow2Spacing
        /// <summary>
        /// Returns the peg row 1 spacing value
        /// </summary>
        public Single PegRow2Spacing
        {
            get { return _pegRow2Spacing; }
        }
        #endregion

        #region PegRow2Start
        /// <summary>
        /// Returns the peg row1 start position
        /// </summary>
        public Single PegRow2Start
        {
            get { return _pegRow2Start; }
        }
        #endregion

        #region Min
        /// <summary>
        /// Returns the min value
        /// </summary>
        public Single Min
        {
            get { return _min; }
        }
        #endregion

        #region Max
        /// <summary>
        /// Returns the max value
        /// </summary>
        public Single Max
        {
            get { return _max; }
        }
        #endregion

        #region CanBreakMin

        /// <summary>
        /// Returns true if the min value of
        /// this axis can be broken.
        /// </summary>
        public Boolean CanBreakMin
        {
            get { return _canBreakMin; }
        }

        #endregion

        #region CanBreakMax

        /// <summary>
        /// Returns true if the max value of 
        /// this axis can be broken.
        /// </summary>
        public Boolean CanBreakMax
        {
            get { return _canBreakMax; }
        }

        #endregion

        /// <summary>
        /// The overhang at the min end of the <see cref="PlanogramMerchandisingGroup"/>.
        /// </summary>
        public Single MinOverhang { get; private set; }

        /// <summary>
        /// The overhang at the max end of the <see cref="PlanogramMerchandisingGroup"/>.
        /// </summary>
        public Single MaxOverhang { get; private set; }

        #endregion

        #region Constructors
        private PlanogramMerchandisingGroupAxis() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramMerchandisingGroupAxis NewPlanogramMerchandisingGroupAxis(
            AxisType axisType,
            PlanogramSubComponent subComponent,
            Single min,
            Single max)
        {
            PlanogramMerchandisingGroupAxis item = new PlanogramMerchandisingGroupAxis();
            item.Create(axisType, subComponent, min, max);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(AxisType axisType,
            PlanogramSubComponent subComponent, Single min, Single max)
        {
            _axisType = axisType;

            _min = min;
            _max = max;

            //initialize min and max to be breakable
            _canBreakMin = true;
            _canBreakMax = true;

            switch (axisType)
            {
                #region X
                case AxisType.X:
                    {
                        switch (subComponent.MerchandisingStrategyX)
                        {
                            case PlanogramSubComponentXMerchStrategyType.Left:
                                _strategy = PlanogramPlacementStrategy.Min;
                                break;

                            case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                                _strategy = PlanogramPlacementStrategy.Min;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentXMerchStrategyType.Right:
                                _strategy = PlanogramPlacementStrategy.Max;
                                break;

                            case PlanogramSubComponentXMerchStrategyType.RightStacked:
                                _strategy = PlanogramPlacementStrategy.Max;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentXMerchStrategyType.Even:
                                _strategy = PlanogramPlacementStrategy.Even;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentXMerchStrategyType.Manual:
                                _strategy = PlanogramPlacementStrategy.Manual;
                                _isStackedStrategy = true;
                                break;
                        }

                        //Ensure that we cannot break axis wall constraints
                        if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack
                                    && subComponent.HasFaceThickness())
                        {
                            if (subComponent.FaceThicknessLeft > 0) _canBreakMin = false;
                            if (subComponent.FaceThicknessRight > 0) _canBreakMax = false;
                        }

                        //Pegholes
                        if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                        {
                            _pegRow1Spacing = subComponent.MerchConstraintRow1SpacingX;
                            _pegRow1Start = subComponent.MerchConstraintRow1StartX;
                            _pegRow2Spacing = subComponent.MerchConstraintRow2SpacingX;
                            _pegRow2Start = subComponent.MerchConstraintRow2StartX;
                        }


                        //Dividers
                        if (subComponent.DividerObstructionWidth.GreaterThan(0)
                                        && !subComponent.IsDividerObstructionByFacing)
                        {
                            //if we have a width set and are not placing between facings.
                            // nb - ignore start if strategy is not even
                            if (this.Strategy != PlanogramPlacementStrategy.Even)
                            {
                                _dividerStart = subComponent.DividerObstructionStartX;
                            }
                            _dividerSpacing = subComponent.DividerObstructionSpacingX;
                        }

                        MinOverhang = subComponent.LeftOverhang;
                        MaxOverhang = subComponent.RightOverhang;
                    }
                    break;
                #endregion

                #region Y
                case AxisType.Y:
                    {
                        switch (subComponent.MerchandisingStrategyY)
                        {
                            case PlanogramSubComponentYMerchStrategyType.Bottom:
                                _strategy = PlanogramPlacementStrategy.Min;
                                break;

                            case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                                _strategy = PlanogramPlacementStrategy.Min;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentYMerchStrategyType.Top:
                                _strategy = PlanogramPlacementStrategy.Max;
                                break;

                            case PlanogramSubComponentYMerchStrategyType.TopStacked:
                                _strategy = PlanogramPlacementStrategy.Max;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentYMerchStrategyType.Even:
                                _strategy = PlanogramPlacementStrategy.Even;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentYMerchStrategyType.Manual:
                                _strategy = PlanogramPlacementStrategy.Manual;
                                _isStackedStrategy = true;
                                break;
                        }


                        //Ensure that a stack merch type can never break min.
                        if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                        {
                            _canBreakMin = false;

                            //nor can we break the top wall.
                            if (subComponent.FaceThicknessTop > 0 && subComponent.HasFaceThickness()) _canBreakMax = false;
                        }
                        //or if we are hanging from the bottom we cannot break the max
                        else if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
                        {
                            _canBreakMax = false;
                        }


                        //Pegholes
                        if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                        {
                            _pegRow1Spacing = subComponent.MerchConstraintRow1SpacingY;
                            _pegRow1Start = subComponent.MerchConstraintRow1StartY;
                            _pegRow2Spacing = subComponent.MerchConstraintRow2SpacingY;
                            _pegRow2Start = subComponent.MerchConstraintRow2StartY;
                        }

                        //Dividers
                        if (subComponent.DividerObstructionWidth.GreaterThan(0)
                             && !subComponent.IsDividerObstructionByFacing
                            && subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                        {
                            //if we have a width set and are not placing between facings.
                            //also, only process y dividers for Hang merch type.
                            // nb - ignore start if strategy is not even
                            if (this.Strategy != PlanogramPlacementStrategy.Even)
                            {
                                _dividerStart = subComponent.DividerObstructionStartY;
                            }
                            _dividerSpacing = subComponent.DividerObstructionSpacingY;
                        }

                        MinOverhang = subComponent.BottomOverhang;
                        MaxOverhang = subComponent.TopOverhang;
                    }
                    break;
                #endregion

                #region Z
                case AxisType.Z:
                    {
                        switch (subComponent.MerchandisingStrategyZ)
                        {
                            case PlanogramSubComponentZMerchStrategyType.Back:
                                _strategy = PlanogramPlacementStrategy.Min;
                                break;
                            case PlanogramSubComponentZMerchStrategyType.BackStacked:
                                _strategy = PlanogramPlacementStrategy.Min;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentZMerchStrategyType.Front:
                                _strategy = PlanogramPlacementStrategy.Max;
                                break;

                            case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                                _strategy = PlanogramPlacementStrategy.Max;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentZMerchStrategyType.Even:
                                _strategy = PlanogramPlacementStrategy.Even;
                                _isStackedStrategy = true;
                                break;

                            case PlanogramSubComponentZMerchStrategyType.Manual:
                                _strategy = PlanogramPlacementStrategy.Manual;
                                _isStackedStrategy = true;
                                break;
                        }

                        //Ensure that a hang merch type can never break its min axis constraint.
                        if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                        {
                            _canBreakMin = false;
                        }

                        //and that we cannot break walls
                        else if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack
                                        && subComponent.HasFaceThickness())
                        {
                            if (subComponent.FaceThicknessBack > 0) _canBreakMin = false;
                            if (subComponent.FaceThicknessFront > 0) _canBreakMax = false;
                        }

                        //Pegholes - no z constraint.
                        _pegRow1Spacing = 0;
                        _pegRow1Start = 0;
                        _pegRow2Spacing = 0;
                        _pegRow2Start = 0;

                        //Dividers
                        if (subComponent.DividerObstructionWidth.GreaterThan(0)
                                        && !subComponent.IsDividerObstructionByFacing)
                        {
                            //if we have a width set and are not placing between facings.

                            //Only process z dividers if the type is stack or hang below
                            if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack
                                || subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
                            {
                                // nb - ignore start if strategy is not even
                                if (this.Strategy != PlanogramPlacementStrategy.Even)
                                {
                                    _dividerStart = subComponent.DividerObstructionStartZ;
                                }
                                _dividerSpacing = subComponent.DividerObstructionSpacingZ;
                            }
                        }

                        MinOverhang = subComponent.BackOverhang;
                        MaxOverhang = subComponent.FrontOverhang;
                    }
                    break;
                #endregion
            }
        }
        #endregion

        #endregion
    }
}
