﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-26421 : N.Foster
//  Created
// V8-27477 : L.Ineson
//  Fixed chest face thickness 
//  and split out process methods.
// V8-27595 : L.Ineson
//  Initializing for combined shelves now also updates placement x pos.
// V8-27603 : L.Ineson
//  Amended check for face thickness when initializing.
// V8-27748 : A.Kuszyk
//  Added GetAvailableSpace methods.
// V8-27762 : N.Foster
//  Refactored to support the rollback of changes in planogram position placement
//  Refactored as a POCO for performance
// V8-27831 : A.Kuszyk
//  Added GetBounds methods.
// V8-27832 : A.Kuszyk
//  Added GetAdjacentPositions methods.
// V8-27858 : A.Kuszyk
//  Added DesignViewPosition property, GetBlockingSpacing method and re-factored GetAvailableSpace methods
//  to use an optional Blocking Location.
// V8-27920 : A.Kuszyk
//  Added GetGroupedPlacements and removed GetBounds methods.
// V8-27946 : A.Kuszyk
//  Updated GetGroupedPlacements to take account of merchandising strategies.
// V8-27477 : L.Ineson
//  Added IDisposable implementation.
//  Amended even strategy, added the CanBreakMin & max constraint flags.
// Split out placement methods as it was becoming silly.
// V8-27946 : A.Kuszyk
//  Added AreDividersPlacedByFacing and AreDividersPlacedNormally helper methods.
// V8-27989 : L.Ineson
//  Made sure that subcomponents are reqsequenced on apply edit in case positions have changed sub.
// V8-28157 : A.Kuszyk
//  Added min sequence evaluation to Initialize().
// V8-28620 : N.Foster
//  Fixed issue regarding edit levels

#endregion
#region Version History: CCM801

//V8-28642 : L.Ineson
//  Moved process finalize call into apply edit.
// V8-28711 : L.Ineson
//  Added missing merch x even code for single group placement.
// V8-28837 : A.Kuszyk
//  Added GetPositionsAtSameSequence.

#endregion
#region Version History: CCM802

// V8-28893 : L.Ineson
//  Amended how maz z is calculated for peg depths.
// V8-29032 : L.Ineson
//  Product Squeeze is now applied as required.
// V8-29023 : M.Pettit
//  Z-placement using Max strategy for pegboards should take the pegboard depth into account
// V8-28766 : J.Pickup
//  Introduced ResequenceSequenceValuesIncludingCombinedGroupings() as to enable my continuation of autofill work. 
// V8-29105 : A.Silva
//      Removed GetAvailableSpace, GetBlockSpacing methods that were very outdated and unused.
// V8-29137 : A.Kuszyk
//  Exposed BeginEdit and CancelEdit methods.

#endregion
#region Version History: CCM803

// V8-29502 : A.Silva
//  Marked GetGroupedPlacements as obsolete. GetPositionsAtSameSequence gives more correct results.
// V8-29280 : A.Silva
//  Added IsOverfilled() and HasOverlapping() methods to check the validity of the placements.
// V8-28766 : N.Foster
//  Stubbed out autofill methods
// V8-29622 : L.Ineson
//  Added new methods for insert, move and remove positions.
// Group now allows for nested editing.
// Moved sequencing methods here too.
// V8-29343 : L.Ineson
//  When sequencing manual strategies the coordinates are now rounded to give a slight tolerance.
// V8-29723 : D.Pleasance
//  Added PlanogramPositionAnchorDirection.Instead to be used for replace action.

#endregion
#region Version History: CCM810

// V8-29137 : A.Kuszyk
//  Amended CorrectPositionSequencing method to reset sequence values to start at 1 in their relavent rows/columns.
// V8-28766 : J.Pickup
//  Populated Autofill() with initial logic. Introduced, ProcessAutofillX(), ProcessAutofillY(), ProcessAutofill(). (More changes to follow).
// V8-29723 : D.Pleasance
//  Added ReplacePositionPlacement
// V8-29770 : A.Kuszyk
//  Update Initialise method to take merchandising space into account when calculating the maxZ for Hang components.
// V8-29826 : N.Foster
//  Enhance behavior of merchandising group when dealing with Begin/Cancel/Apply of edits
// V8-29769 : A.Kuszyk
//  Added GetOverlappingSize() to GetPlacedWidth/Height/Depth to resolve partial squeeze issues on pegboards.
// V8-28766 : J.Pickup
//  Populated SetMinimumUnits(1) replaced with new method, set minimumUnits, introduced ProcessaAutoCapX(), processAutoCapY().
// V8-29903 : L.Ineson
//  Added InsertExistingPositionPlacement methods.
// V8-29453 : L.Ineson
//  Amended placement of z dividers.
// V8-29889 : A.Kuszyk
//  Added EnumerateSubComponentPlacementMerchandisingSpaces and GetMerchandisingSpace methods.
// V8-29938 : A.Kuszyk
//  Amended GetOverlappingSize to take the first placement from each group to avoid overlaps
//  between existing positions and newly added ones.
// V8-28766 : J.Pickup
//  Introduced OrderPositionPlacementsByRankThenGtin() introduced to allow the ordered list to filter unwanted positions and to improve code reusability.
// V8-29957 : J.Pickup
//  IsOverfilledZ can now be negated by the use of a pegged position.
// V8-29888 : A.Kuszyk
//  Updated PlaceAtMin/Max X/Y/Z methods to ensure that SnapToPegHole is always called.
// V8-29982 : A.Kuszyk
//  Updated Initialize method to get Min and Max values from PlanogramMerchandisingSpace.
// V8-30003 : A.Silva
//  Amended GetAvailableSpaceOnAxis to get the correct dimensions for the Merchandising Group's volume taking into account stack/hang/hang below.
// V8-30004 : A.Kuszyk
//  Fixed MaxY for Hang to be the height of the subcomponent.
// V8-29950 : J.Pickup
//  I have added collision detection to the autofill, code optimisisations and autofill now returns whether it made a change.
// V8-30033 : A.Silva
//  Added peg hole awareness to IsOverFilled.
// V8-30032 : A.Kuszyk
//  Removed reduceByMinMax parameter from GetMerchandsingSpace and EnumerateSubComponentPlacementMerchandisingSpaces methods.
// V8-30016 : A.Kuszyk
//  Corrected issue with overlaps on even pegboards.
// V8-30084 : A.Kuszyk
//  Ensured GetMerchandisingSpace uses the minimum merch height of all the subcomponents.
// V8-30103 : J.Pickup
//  Added some metrics for performanceMonitoring. MerchSpace is passed down for improved performance.
// V8-30120 : A.Silva
//  Small amendment to account for overfilled when dealing with Hang From Bottom Components.
// V8-30103 : N.Foster
//  Autofill performance enhancements
// V8-30085 : A.Kuszyk
//  Re-factored GetMerchandisingSpace and amended Initialize method.
// V8-30034 : A.Silva
//  Amended ResequencePositionsByX and ResequencePositionsByZ so that pegboards are resequenced like the rest of components.
// V8-30145 : J.Pickup
//  Calls to process for hang types when checking overfill in X axis are now added. Bars no longer overfill with autofill.
// V8-30053 : A.Kuszyk
//  Resolved issues with even X and dividers with a start position.
// V8-30173 : L.Ineson
//  Amended autofill checks to IsOverfilled to only check placements in the same pillar to the position being processed.
// V8-30222 : A.Silva
//  Amended IsOverfilled to report overfilled for Hang from Bottom components with more than one facing on the x axis,
//      and for Hang/Hang From Bottom when the axis that allows only one facing has more than one placement stacked,
//      and for Hang components with pegs when two or more placements "share" the same peg.
// V8-30238 : J.Pickup
//      Amended IsOverfilled to hanlde non stacked axis and autofill not to shrink non stacked axis where no overfill has taken place.
// V8-30205 : L.Ineson
//  Removed subcomponent collision check from autofill as this is already done up front by the merch space.
// V8-30118 : A.Silva
//  Added LookupAxesStackedValue to quickly get a per axis account of which are stacked or not.
// V8-30300 : A.Silva
//  Amended IsOverfilled to account for peg depth when checking Z axis for a pegged product.

#endregion
#region Version History: CCM811

// V8-30313 : L.Ineson
//  Added overload for Autofill to take in the list of all plan merch groups.
// V8-30376 : A.Silva
//  Amended Autofill so that new total units are recalculated for the affected positions after running it.
// V8-30406 : A.Silva
//  Updated ProcessAutoCap due to changes in the ProcessAutoCap methods.
// V8-30152 : A.Kuszyk
//  Updated GetMerchandisingSpace to take the bounding space as provided (no adjustments are made for top components).
// V8-30114 : A.Kuszyk
//  Fixed issues with GetMerchandisingSpace and exposed private IsOverfilled method.
// V8-30524 : D.Pleasance
//  Amended typo in ResequencePositionsByX(), was updating SequenceY instead of SequenceX!
// V8-30218 : A.Kuszyk
//  Fixed some issues with PlaceGroupsAlongXYZWithDividers methods relating to the placement of dividers
//  on even components when underfilled. Re-factored these changes into common methods used by all three of the
//  affected methods.
// V8-30474 : A.Silva
//  Amended OrderPositionPlacementsByRankThenGtin to allow ignoring or not placements with positions set as IsManuallyPlaced.
// V8-30598 : M.Brumby
//  Hanging merch types minX now take into account overhangs.
// V8-30637 : A.Kuszyk
//  Changed GetMerchandisingSpace to use PlanogramMerchandisingSpace.AddToRight to ensure that all the spaces
//  of the subcomponents are combined on the right irrespective of their rotation.
#endregion
#region Version History: CCM820
// V8-30778 : N.Foster
//  Added additional metrics to assist in performance tuning
// V8-30818 : N.Foster
//  Performance tweaks
// V8-31059 : J.Pickup
//  Fixed overfill issue that treated hangbars as a slotwall and slightly improved slotwall handling. 
// V8-31233 : A.Probyn
//  ~ Updated PlaceEvenWhenUnderfilledAlongAxis so that spacing is kept to 2 decimal places (without rounding) to eliminate
//  the tiny discrepencies we get when values are calculated such as 0.006. These add up over time with positions and alter
//  the X position, when they shouldn't
// V8-31407 : J.Pickup
//  Slight autofill performance increase - getreservedspace no longer calculated an exponential amount of times. 
// V8-31409 : D.Pleasance
//  Added overload to InsertPositionPlacement() to include sequence details.
// V8-31525 : J.Pickup
//  Made a change to overfill checks on stacked merchgroups to ensure placements are considered to be overfilled if they exceed max stack rules as well as physical bounds. 
#endregion
#region Version History: CCM830
// V8-31672 : L.Ineson
//  Changes to AutoFill and IsPlacementGroupOverfilled methods to allow fill high to consider subcomponent obstructions and improve performance.
// V8-31715 : D.Pleasance
//  Amended ProcessAutofill() to process axis direction being filled with no squeeze set. This is to prevent autofill from increasing units by the products squeeze minimum. Squeeze should only be applied if required.
//  In addition to this also added new method overload to Process() so that squeeze type can be set on all directions.
// V8-31858  : J.Pickup
//  FWD integration: ProcessAutofill() to check isOverfilled after decreasing. (Because on a rare occasion if squeeze is applied
// V8-31804 : A.Kuszyk
//  Added a cache for the results of GetMerchandisingSpace to improve performance. The merchandising space depends on 
//  subcomponents, but not positions, should be unaffected by mutations of this instance.
// V8-31700 : L.Ineson
//  Autofill IsPlacementGroupOverfilled check no longer checks position collisions if the axis strategy is even.
// V8-31982 : L.Ineson
//  Auto fill final process now only applies squeeze in the axis not being auto filled.
// V8-31619 : L.Ineson
//  Process now calls recalculate units
// V8-32019 : A.Kuszyk
//  Updated processing code to use same overfill checks as autofill.
// V8-32004 : L.Ineson
//  Auto fill depth subcomponent collision check is a bit cleverer
// V8-31804 : A.Kuszyk
//  Added GetWhiteSpaceOnAxis method.
// V8-32098 : L.Ineson
//  Sped up autofill peghole checks.
// V8-31884 : M.Brumby
//  Made use of new function IsMaxExceeded in PlanogramPositionPlacement in IsOverfilled() and IsPlacementGroupOverfilled()
//  so there is a central place to check max values for an axis.
// V8-32178 : A.Silva
//  Amended IsOverfilled so it will call IsPlacementGroupOverfilled, that way the overfill checking code is shared avoiding duplicates.
//  Refactored IsPlacementGroupOverfilled to avoid excesive complexity.
//  Corrected two logical issues that made pegboards and clipstrips be considered overfilled always.
// V8-32189 : A.Silva
//  Added logical switch to CheckOverfilledMerchSpace so that the merch space boundaries check can be overriden.
// V8-32327 : A.Silva
//  Refactored InsertExistingPosition to avoid duplicate code.
//  Refactored MovePositionPlacement to reduce complexity.
// V8-31385 : A.Silva
//  Amended CheckOverfilledHangComponent and CheckOverfilledMerchSpace so that hang components check the correct width when there are overhangs.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32433 : D.Pleasance
//  Amended MovePositionPlacement() so that above placement applies the same logic as below placement in that, if its hang, presentation lines are 
//  processed accross y so we need to update all other placements not just those in the same xz group.
// V8-32529 : L.Ineson
//  Amended HasPlacementsWithoutPeg to stop products being autofilled of the bottom of the board.
// V8-32647 : A.Silva
//  Amended CheckOverfilledHangComponent so that the MathHelper comparison methods are used when comparing peg hole coordinates, to avoid floating point issues.
// V8-32652 : A.Silva
//  Amended Overfill methods to allow overhanging pegboards while keeping autofill from using them.
// V8-32675 : L.Ineson
//  Various performance enhancements 
//  - Stopped squeeze from being set if none of the positions can be squeezed
//  - Place at methods now return the placed length.
// V8-32267 : L.Ineson
//  Added support for PegProngOffsetX and PegProngOffsetY
// V8-32503 : L.Ineson
//  Added new MovePosition method.
// V8-32796 : L.Ineson
//  Amended to remove unnecessary peghole generation.
//  V8-32775 : A.Silva
//  Amended to make dividers snap to their correct positions when there are overhangs or the strategy is even.
// V8-32821 : L.Bailey
//  Autofill faceup ordering
// V8-32873 : L.Bailey
//  Autofill faceup ordering - units being recalculated on saving plan/opening plan/cancelling out of Planogram Info screen
// V8-32875 : L.Ineson
//  IsOverfilled now returns true if a placement has caused overlaps in y.
// V8-32864 : A.Silva
//  Added GetPresentationStates().
// CCM-18435 : A.Silva
//  Refactored HasOverlaps() to simplify logic. Added HasCollisionOverlapping. Removed unused method GetOverlappingSize.
// CCM-13761 : L.Ineson
//  Amended IsOverfilled checks to use new BoundaryInfo class.
// CCM-13147 : L.Ineson
//  Refactored to use MathHelper comparison extension methods.
// CCM-18440 : A.Kuszyk
//  Moved GetPresentationStates into PlanogramMerchandisingGroupList.
// CCM-18462 : A.Kuszyk
//  Added GetFacingAxis helper method.
// CCM-18307 : J.Pickup
//  Adjusted ProcessFinalize to resequence based on center of position rather than the start coordinate.
// CCM-18533 : L.Ineson
//  Made sure that dividers per facing get offsetted when moving an underfitted even strategy single product placement.
#endregion

#endregion

using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Enum denoting the different types of possible squeeze.
    /// </summary>
    public enum PlanogramSubComponentSqueezeType
    {
        NoSqueeze,
        PartialSqueeze,
        FullSqueeze
    }

    /// <summary>
    /// Performs the layout out of positions and dividers within a merchandisiable area.
    /// </summary>
    public class PlanogramMerchandisingGroup : IDisposable
    {
        #region Nested Classes

        /// <summary>
        /// Class that represents a divider placed within a merchandising group
        /// </summary>
        private class DividerPlacementValue
        {
            /// <summary>
            /// Indicates if this is the first divider
            /// </summary>
            public Boolean IsStartDivider { get; set; }

            /// <summary>
            /// Indicates if this is the last divider
            /// </summary>
            public Boolean IsEndDivider { get; set; }

            /// <summary>
            /// The divider value
            /// </summary>
            public Single Value { get; set; }

            /// <summary>
            /// The divider thickness
            /// </summary>
            public Single Thickness { get; private set; }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="val">The placement coord for this divider</param>
            /// <param name="thick">The thickness size of the divider.</param>
            public DividerPlacementValue(Single val, Single thick)
            {
                Value = val;
                Thickness = thick;
            }
        }

        /// <summary>
        /// Helper class to order the placement and the assortment products (See OrderPositionPlacementsByRankThenGtin)
        /// </summary>
        private class PlacementAssortmentPair
        {
            public PlanogramPositionPlacement Placement { get; set; }
            public PlanogramAssortmentProduct AssortmentProduct { get; set; }

            public PlacementAssortmentPair(PlanogramPositionPlacement placement, PlanogramAssortmentProduct assortmentProduct)
            {
                Placement = placement;
                AssortmentProduct = assortmentProduct;
            }
        }

        /// <summary>
        /// Deletegate defining a method which can be used by squeeze to place products
        /// </summary>
        /// <param name="placementGroups">the groups to be placed.</param>
        /// <param name="dividerValues">the divider placements</param>
        /// <returns>the placed length</returns>
        private delegate Single PlaceGroupsDelegate(
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            out List<DividerPlacementValue> dividerValues);

        #endregion

        #region Fields

        private IEnumerable<PlanogramSubComponentPlacement> _subComponentPlacements;
        private PlanogramPositionPlacementList _positionPlacements;
        private List<PlanogramDividerPlacement> _dividerPlacements;
        private PlanogramMerchandisingGroupAxis _xAxis;
        private PlanogramMerchandisingGroupAxis _yAxis;
        private PlanogramMerchandisingGroupAxis _zAxis;
        private PlanogramSubComponentMerchandisingType _merchandisingType;
        private Boolean _isProductOverlapAllowed;
        private Single _dividerWidth;
        private Single _dividerHeight;
        private Single _dividerDepth;
        private Boolean _placeStartDivider;
        private Boolean _placeEndDivider;
        private Boolean _isDividerObstructionByFacing;
        private Boolean _hasRow1PegHoles;
        private Boolean _hasRow2PegHoles;
        private Single _minX;
        private Single _maxX;
        private Single _minY;
        private Single _maxY;
        private Single _minZ;
        private Single _maxZ;
        private PlanogramSubComponentXMerchStrategyType _strategyX;
        private PlanogramSubComponentYMerchStrategyType _strategyY;
        private PlanogramSubComponentZMerchStrategyType _strategyZ;
        private Int32 _editLevel;
        private DesignViewPosition _designViewPosition;
        private PlanogramSubComponentSqueezeType _squeezeTypeX;
        private PlanogramSubComponentSqueezeType _squeezeTypeY;
        private PlanogramSubComponentSqueezeType _squeezeTypeZ;
        private Dictionary<AxisType, Boolean> _canUsePlacementGroupLookup = new Dictionary<AxisType, Boolean>();

        /// <summary>
        /// Holds a cache of merchandising spaces that have been requested through the <see cref="GetMerchandisingSpace"/> method.
        /// </summary>
        private Dictionary<Tuple<Boolean, Boolean, Boolean, RectValue?>, PlanogramMerchandisingSpace> _getMerchandisingSpaceCache =
            new Dictionary<Tuple<Boolean, Boolean, Boolean, RectValue?>, PlanogramMerchandisingSpace>();

        #endregion

        #region Properties

        #region SubComponentPlacements

        /// <summary>
        /// Returns all sub components within this merchandising group
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> SubComponentPlacements
        {
            get { return _subComponentPlacements; }
        }

        #endregion

        #region PositionPlacements

        /// <summary>
        /// Returns all position placements within this merchandising group
        /// </summary>
        public PlanogramPositionPlacementList PositionPlacements
        {
            get { return _positionPlacements; }
        }

        #endregion

        #region DividerPlacements

        /// <summary>
        /// Returns all divider placements within this merchandising group
        /// </summary>
        public List<PlanogramDividerPlacement> DividerPlacements
        {
            get { return _dividerPlacements; }
        }

        #endregion

        #region XAxis

        /// <summary>
        /// Returns details on the x axis of the merchandising group
        /// </summary>
        public PlanogramMerchandisingGroupAxis XAxis
        {
            get { return _xAxis; }
        }

        #endregion

        #region YAxis

        /// <summary>
        /// Returns details on the y axis of the merchandising group
        /// </summary>
        public PlanogramMerchandisingGroupAxis YAxis
        {
            get { return _yAxis; }
        }

        #endregion

        #region ZAxis

        /// <summary>
        /// Returns details on the z axis of the merchandising group
        /// </summary>
        public PlanogramMerchandisingGroupAxis ZAxis
        {
            get { return _zAxis; }
        }

        #endregion

        #region MerchandisingType

        /// <summary>
        /// Returns the merchandising type for the
        /// sub components within this group
        /// </summary>
        public PlanogramSubComponentMerchandisingType MerchandisingType
        {
            get { return _merchandisingType; }
        }

        #endregion

        #region IsProductOverlapAllowed

        /// <summary>
        /// Indicates if positions are allowed to overlap
        /// within this merchandising group
        /// </summary>
        public Boolean IsProductOverlapAllowed
        {
            get { return _isProductOverlapAllowed; }
        }

        #endregion

        #region DividerWidth

        /// <summary>
        /// Returns the divider width
        /// </summary>
        public Single DividerWidth
        {
            get { return _dividerWidth; }
        }

        #endregion

        #region DividerHeight

        /// <summary>
        /// Returns the divider height
        /// </summary>
        public Single DividerHeight
        {
            get { return _dividerHeight; }
        }

        #endregion

        #region DividerDepth

        /// <summary>
        /// Returns the divider depth
        /// </summary>
        public Single DividerDepth
        {
            get { return _dividerDepth; }
        }

        #endregion

        #region PlaceStartDivider

        /// <summary>
        /// Indicates if a start divider should be placed
        /// </summary>
        public Boolean PlaceStartDivider
        {
            get { return _placeStartDivider; }
        }

        #endregion

        #region PlaceEndDivider

        /// <summary>
        /// Indicates that the end divider should be placed
        /// </summary>
        public Boolean PlaceEndDivider
        {
            get { return _placeEndDivider; }
        }

        #endregion

        #region IsDividerObstructionByFacing

        /// <summary>
        /// Indicates if a divider is obstructed by a facing
        /// </summary>
        private Boolean IsDividerObstructionByFacing
        {
            get { return _isDividerObstructionByFacing; }
        }

        #endregion

        #region HasRow1PegHoles

        /// <summary>
        /// Indicates if the merchandising group
        /// has row 1 peg holes
        /// </summary>
        public Boolean HasRow1PegHoles
        {
            get { return _hasRow1PegHoles; }
        }

        #endregion

        #region HasRow2PegHoles

        /// <summary>
        /// Indicates if the merchandising group
        /// has row 2 peg holes
        /// </summary>
        public Boolean HasRow2PegHoles
        {
            get { return _hasRow2PegHoles; }
        }

        #endregion

        #region MinX

        /// <summary>
        /// Returns the min x value
        /// </summary>
        public Single MinX
        {
            get { return _minX; }
        }

        #endregion

        #region MaxX

        /// <summary>
        /// Returns the max x value
        /// </summary>
        public Single MaxX
        {
            get { return _maxX; }
        }

        #endregion

        #region MinY

        /// <summary>
        /// Returns the min y value
        /// </summary>
        public Single MinY
        {
            get { return _minY; }
        }

        #endregion

        #region MaxY

        /// <summary>
        /// Returns the max y value
        /// </summary>
        public Single MaxY
        {
            get { return _maxY; }
        }

        #endregion

        #region MinZ

        /// <summary>
        /// Returns the min z value
        /// </summary>
        public Single MinZ
        {
            get { return _minZ; }
        }

        #endregion

        #region MaxZ

        /// <summary>
        /// Returns the max z value
        /// </summary>
        public Single MaxZ
        {
            get { return _maxZ; }
        }

        #endregion

        #region StrategyX

        /// <summary>
        /// Returns the merchandising x strategy
        /// </summary>
        public PlanogramSubComponentXMerchStrategyType StrategyX
        {
            get { return _strategyX; }
        }

        #endregion

        #region StrategyY

        /// <summary>
        /// Returns the merchandising y strategy
        /// </summary>
        public PlanogramSubComponentYMerchStrategyType StrategyY
        {
            get { return _strategyY; }
        }

        #endregion

        #region StrategyZ

        /// <summary>
        /// Returns the merchandising z strategy
        /// </summary>
        public PlanogramSubComponentZMerchStrategyType StrategyZ
        {
            get { return _strategyZ; }
        }

        #endregion

        #region SqueezeTypeX

        /// <summary>
        /// Returns the type of squeeze to be applied when processing on X direction
        /// </summary>
        public PlanogramSubComponentSqueezeType SqueezeTypeX
        {
            get { return _squeezeTypeX; }
        }

        #endregion

        #region SqueezeTypeY

        /// <summary>
        /// Returns the type of squeeze to be applied when processing on Y direction
        /// </summary>
        public PlanogramSubComponentSqueezeType SqueezeTypeY
        {
            get { return _squeezeTypeY; }
        }

        #endregion

        #region SqueezeTypeZ

        /// <summary>
        /// Returns the type of squeeze to be applied when processing on Z direction
        /// </summary>
        public PlanogramSubComponentSqueezeType SqueezeTypeZ
        {
            get { return _squeezeTypeZ; }
        }

        #endregion

        #region EditLevel

        /// <summary>
        /// Returns the current edit level for this group
        /// </summary>
        public Int32 EditLevel
        {
            get { return _editLevel; }
        }

        #endregion

        #region DesignViewPosition

        /// <summary>
        /// The Design View Position of this merchandising group.
        /// </summary>
        public DesignViewPosition DesignViewPosition
        {
            get { return _designViewPosition ?? (_designViewPosition = new DesignViewPosition(this)); }
        }

        #endregion

        #endregion

        #region Constructors

        private PlanogramMerchandisingGroup() { } // force use of factory methods

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="subComponentPlacements">The sub placements that make up this group. If there are multiple combined then these should be passed 
        /// in X coordinate order from left to right.</param>
        public static PlanogramMerchandisingGroup NewPlanogramMerchandisingGroup(IEnumerable<PlanogramSubComponentPlacement> subComponentPlacements)
        {
            PlanogramMerchandisingGroup item = new PlanogramMerchandisingGroup();
            item.Create(subComponentPlacements);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="subComponentPlacement">The single subplacement that makes up this merch group.</param>
        public static PlanogramMerchandisingGroup NewPlanogramMerchandisingGroup(PlanogramSubComponentPlacement subComponentPlacement)
        {
            PlanogramMerchandisingGroup item = new PlanogramMerchandisingGroup();
            item.Create(new PlanogramSubComponentPlacement[] { subComponentPlacement });
            return item;
        }


        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="subComponentPlacements">The sub placements that make up this group.</param>
        private void Create(IEnumerable<PlanogramSubComponentPlacement> subComponentPlacements)
        {
            // initialize collections
            _subComponentPlacements = subComponentPlacements;
            _dividerPlacements = new List<PlanogramDividerPlacement>();

            // create the position placements for positions on all of the included subs.
            List<PlanogramPositionPlacement> placements = new List<PlanogramPositionPlacement>();
            foreach (PlanogramSubComponentPlacement subComponentPlacement in _subComponentPlacements)
            {
                foreach (PlanogramPosition position in subComponentPlacement.GetPlanogramPositions().OrderBy(p => p.Sequence))
                {
                    placements.Add(PlanogramPositionPlacement.NewPlanogramPositionPlacement(
                        position,
                        position.GetPlanogramProduct(),
                        subComponentPlacement,
                        this));
                }
            }
            _positionPlacements = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(placements);

            // place this group into edit mode.
            BeginEdit();
        }

        #endregion

        #endregion

        #region Methods

        #region Initialize

        /// <summary>
        /// Called when initializing this instance, this sets up values that tell us
        /// the size of the merchandising group and how its placements should be layed out 
        /// according to the settings against the subcomponents that make up the group
        /// </summary>
        private void Initialize()
        {
            List<PlanogramSubComponentPlacement> subComponentPlacements = this.SubComponentPlacements.ToList();
            PlanogramSubComponentPlacement firstSubComponentPlacement = subComponentPlacements[0];
            PlanogramSubComponent firstSubComponent = firstSubComponentPlacement.SubComponent;

            // Strategies - the first subcomponent in the set will always take the lead here.
            _strategyX = firstSubComponent.MerchandisingStrategyX;
            _strategyY = firstSubComponent.MerchandisingStrategyY;
            _strategyZ = firstSubComponent.MerchandisingStrategyZ;
            _merchandisingType = firstSubComponent.MerchandisingType;

            // Determine if products can squeeze or overlap
            _isProductOverlapAllowed = firstSubComponent.IsProductOverlapAllowed;
            _squeezeTypeX = (firstSubComponent.IsProductSqueezeAllowed)
                ? PlanogramSubComponentSqueezeType.PartialSqueeze
                : PlanogramSubComponentSqueezeType.NoSqueeze;
            _squeezeTypeY = _squeezeTypeX;
            _squeezeTypeZ = _squeezeTypeX;

            // Set divider values from the first sub
            _dividerHeight = firstSubComponent.DividerObstructionHeight;
            _dividerWidth = firstSubComponent.DividerObstructionWidth;
            _dividerDepth = firstSubComponent.DividerObstructionDepth;
            _placeStartDivider = firstSubComponent.IsDividerObstructionAtStart;
            _placeEndDivider = firstSubComponent.IsDividerObstructionAtEnd;

            _hasRow1PegHoles = firstSubComponent.MerchConstraintRow1SpacingY.GreaterThan(0) && firstSubComponent.MerchConstraintRow1Width.GreaterThan(0) &&
                               firstSubComponent.MerchConstraintRow1Height.GreaterThan(0);
            _hasRow2PegHoles = firstSubComponent.MerchConstraintRow2SpacingY.GreaterThan(0) && firstSubComponent.MerchConstraintRow2Width.GreaterThan(0) &&
                               firstSubComponent.MerchConstraintRow2Height.GreaterThan(0);

            // Set mins and maxes. We use the merchandisable space here as a basis and adjust as necessary.
            Single minX, maxX, minY, maxY, minZ, maxZ;
            PlanogramMerchandisingSpace merchSpace = GetMerchandisingSpace(
                planogramRelative: false,
                maximiseWidthForHang: false,
                maximiseDepthForHang: false);

            switch (firstSubComponent.MerchandisingType)
            {
                default:
                    minX = maxX = minY = maxY = minZ = maxZ = 0;
                    Debug.Fail("Type not recognised");
                    break;


                case PlanogramSubComponentMerchandisingType.Stack:
                    {
                        // Set the basic values for stack. The merchandising space
                        // handles face thickness and overhangs, so we don't need to worry
                        // about them here.
                        minX = merchSpace.SpaceReducedByObstructions.X;
                        maxX = minX + merchSpace.SpaceReducedByObstructions.Width;
                        minY = merchSpace.SpaceReducedByObstructions.Y;
                        maxY = minY + merchSpace.SpaceReducedByObstructions.Height;
                        minZ = merchSpace.SpaceReducedByObstructions.Z;
                        maxZ = minZ + merchSpace.SpaceReducedByObstructions.Depth;
                    }
                    break;


                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    {
                        //products are going to hang from the bottom facing of the subcomponent e.g. rod.
                        // Overhangs are taken into account in the merchandising space calculations.
                        minX = merchSpace.SpaceReducedByObstructions.X;
                        maxX = minX + merchSpace.SpaceReducedByObstructions.Width;
                        minY = 0; //min - dependent on position height.
                        maxY = 0; // max is to the bottom of the subcomponent
                        minZ = merchSpace.SpaceReducedByObstructions.Z; //from the back of the sub
                        maxZ = merchSpace.SpaceReducedByObstructions.Depth; // to the front.
                    }
                    break;


                case PlanogramSubComponentMerchandisingType.Hang:
                    {
                        // Overhangs are taken into account in the merchandising space calculations.
                        minX = merchSpace.SpaceReducedByObstructions.X;
                        maxX = minX + merchSpace.SpaceReducedByObstructions.Width;
                        minY = -firstSubComponent.BottomOverhang;
                        maxY = firstSubComponent.Height + firstSubComponent.BottomOverhang + firstSubComponent.TopOverhang;
                        minZ = firstSubComponent.Depth;
                        maxZ = minZ + merchSpace.SpaceReducedByObstructions.Depth;
                    }
                    break;
            }

            _minX = minX;
            _maxX = maxX;
            _minY = minY;
            _maxY = maxY;
            _minZ = minZ;
            _maxZ = maxZ;

            _xAxis = PlanogramMerchandisingGroupAxis.NewPlanogramMerchandisingGroupAxis(AxisType.X, firstSubComponent, minX, maxX);
            _yAxis = PlanogramMerchandisingGroupAxis.NewPlanogramMerchandisingGroupAxis(AxisType.Y, firstSubComponent, minY, maxY);
            _zAxis = PlanogramMerchandisingGroupAxis.NewPlanogramMerchandisingGroupAxis(AxisType.Z, firstSubComponent, minZ, maxZ);

            // If we are combining subcomponents and making a change where stacking is 
            // involved then update the placement sequencing and x value accordingly
            if (subComponentPlacements.Count > 0)
            {
                Int16 subSeqOffset = 0;
                Int16 subSeqXOffset = 0;
                Single nextSubX = 0;
                foreach (PlanogramSubComponentPlacement subComponentPlacement in subComponentPlacements)
                {
                    PlanogramSubComponentPlacement placement = subComponentPlacement;
                    ICollection<PlanogramPositionPlacement> positions =
                        PositionPlacements.Where(p => Equals(p.SubComponentPlacement, placement)).ToList();

                    //If the position is not from the first sub in the set
                    if (!Equals(subComponentPlacement, firstSubComponentPlacement))
                    {
                        foreach (PlanogramPositionPlacement positionPlacement in positions)
                        {
                            //Increment on their sequence numbers and x coords to make sure
                            // they are relative to the merch group before we start.
                            positionPlacement.Position.Sequence += subSeqOffset;
                            positionPlacement.Position.SequenceX += subSeqXOffset;
                            positionPlacement.Position.X += nextSubX;
                            positionPlacement.IsRelativeToMerchandisingGroup = true;
                        }
                    }

                    if (positions.Any())
                    {
                        subSeqOffset = positions.Max(p => p.Position.Sequence);
                        subSeqXOffset = positions.Max(p => p.Position.SequenceX);
                    }

                    nextSubX += subComponentPlacement.SubComponent.Width;
                }
            }
        }

        #endregion

        #region Process

        /// <summary>
        /// Processes this merchandising group according to the current settings.
        /// </summary>
        /// <remarks>This method may be called repeatedly before the group changes are actually committed.</remarks>
        public void Process()
        {
            Process(PlanogramSubComponentSqueezeType.PartialSqueeze);
        }

        /// <summary>
        /// Processes this merchandising group
        /// </summary>
        /// <param name="squeezeType">The type of squeeze to apply to all axis.</param>
        /// <remarks>This method may be called repeatedly before the group changes are actually committed.</remarks>
        public void Process(PlanogramSubComponentSqueezeType squeezeType)
        {
            Process(squeezeType, squeezeType, squeezeType);
        }

        /// <summary>
        /// Processes this merchandising group
        /// </summary>
        /// <param name="squeezeTypeX">The type of squeeze to apply in the x axis</param>
        /// <param name="squeezeTypeY">The type of squeeze to apply in the y axis</param>
        /// <param name="squeezeTypeZ">The type of squeeze to apply in the z axis</param>
        /// <remarks>This method may be called repeatedly before the group changes are actually committed.</remarks>
        public void Process(PlanogramSubComponentSqueezeType squeezeTypeX, PlanogramSubComponentSqueezeType squeezeTypeY, PlanogramSubComponentSqueezeType squeezeTypeZ)
        {
            //dont bother if we have no positions.
            if (!this.PositionPlacements.Any()) return;

            //Check if we are allowed to squeeze products at all -
            // i.e. the component allows it and we actually have products that could squeeze.
            Boolean canSqueeze =
                (this.SubComponentPlacements.ElementAt(0).SubComponent.IsProductSqueezeAllowed)
                && this.PositionPlacements.Any(p => p.Product.SqueezeHeight.LessThan(1) || p.Product.SqueezeWidth.LessThan(1) || p.Product.SqueezeDepth.LessThan(1));
            _squeezeTypeX = canSqueeze ? squeezeTypeX : PlanogramSubComponentSqueezeType.NoSqueeze;
            _squeezeTypeY = canSqueeze ? squeezeTypeY : PlanogramSubComponentSqueezeType.NoSqueeze;
            _squeezeTypeZ = canSqueeze ? squeezeTypeZ : PlanogramSubComponentSqueezeType.NoSqueeze;


            // As this method will be called over and over first we need to make
            // sure certain values are reset.

            // Clear off any old dividers
            _dividerPlacements.Clear();

            // Squeeze values should start unsqueezed
            foreach (PlanogramPositionPlacement p in this.PositionPlacements)
            {
                p.Position.ResetSqueezeValues();
            }

            // Now we can start placing.
            switch (this.MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.None:
                    break;

                case PlanogramSubComponentMerchandisingType.Stack:
                    this.ProcessStacked();
                    break;

                case PlanogramSubComponentMerchandisingType.Hang:
                    this.ProcessHang();
                    break;

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    this.ProcessHangFromBottom();
                    break;

                default:
                    Debug.Fail("MerchandisingType not handled");
                    return;
            }


            //make sure that total units are up to date.
            // whilst its unlikely that anything has changed, this is the 
            // best way of making sure that they are always up to date
            foreach (PlanogramPositionPlacement p in this.PositionPlacements)
            {
                p.RecalculateUnits();
            }

        }

        /// <summary>
        /// Processes this merch group as a stacked merchandising type.
        /// This means that products are placed on the top facing of the group subs like a shelf.
        /// </summary>
        private void ProcessStacked()
        {
            Debug.Assert(this.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack,
                "This method should only be executed if the merch type is stack.");

            //For stacked types we aim to keep the products in neat vertically lined up groups. 
            // Therefore the order in which we process the axis and how the positions are grouped when we do this is important.


            //X AXIS:
            //Group positions by their sequence x value then place the groups along the X Axis.
            List<PlanogramPositionPlacementGroup> xPlacementGroups = new List<PlanogramPositionPlacementGroup>();
            foreach (var xGroup in this.PositionPlacements.GroupBy(p => p.Position.SequenceX).OrderBy(g => g.Key))
            {
                xPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                    /*orderedPlacements*/xGroup.OrderBy(p => p.Position.X).ToList(),
                    /*seqX*/xGroup.Key,
                    /*seqY*/null,
                    /*seqZ*/null));
            }
            this.ProcessXAxis(xPlacementGroups);

            //Y AXIS:
            //As y is secondary, we process the positions in each sequence x,z group separately.
            foreach (var xzGroup in this.PositionPlacements.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceZ)))
            {
                List<PlanogramPositionPlacementGroup> yPlacementGroups = new List<PlanogramPositionPlacementGroup>();
                foreach (var yGroup in xzGroup.GroupBy(p => p.Position.SequenceY).OrderBy(g => g.Key))
                {
                    yPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                        /*placements*/yGroup.ToList(),
                        /*seqX*/xzGroup.Key.Item1,
                        /*seqY*/yGroup.Key,
                        /*seqZ*/xzGroup.Key.Item2));
                }
                this.ProcessYAxis(yPlacementGroups);
            }

            //Z AXIS:
            // As z is tertiary, we process the positions in each sequence x,y group separately.
            foreach (var xyGroup in this.PositionPlacements.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceY)))
            {
                List<PlanogramPositionPlacementGroup> zPlacementGroups = new List<PlanogramPositionPlacementGroup>();
                foreach (var zGroup in xyGroup.GroupBy(p => p.Position.SequenceZ).OrderBy(g => g.Key))
                {
                    zPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                        /*placements*/zGroup.ToList(),
                        /*seqX*/xyGroup.Key.Item1,
                        /*seqY*/xyGroup.Key.Item2,
                        /*seqZ*/zGroup.Key));
                }
                this.ProcessZAxis(zPlacementGroups);
            }
        }

        /// <summary>
        /// Processes this merch group as a hanged merchandising type.
        /// This means that products are hung from the front facing of the merch group subs
        /// usually on pegs like a pegboard or hanging bar.
        /// </summary>
        private void ProcessHang()
        {
            Debug.Assert(this.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang,
                "This method should only be executed if the merch type is hang.");


            // For hang types we aim to keep the products in neat horizontally lined up groups. 
            // Therefore the order in which we process the axis and how the positions are grouped when we do this is important.
            // Please note that the presentation axis here is different to stack types. This was a concious decision made when 
            // we first started implementing V8. If this needs changing to vertical lines then this method can just be swapped for the
            // code in the process stacked version.

            //Y AXIS:
            //Group positions by their sequence y value.
            List<PlanogramPositionPlacementGroup> yPlacementGroups = new List<PlanogramPositionPlacementGroup>();
            foreach (var yGroup in this.PositionPlacements.GroupBy(p => p.Position.SequenceY).OrderBy(g => g.Key))
            {
                yPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                    /*placements*/yGroup.ToList(),
                    /*seqX*/null,
                    /*seqY*/yGroup.Key,
                    null));
            }
            this.ProcessYAxis(yPlacementGroups);

            // X AXIS:
            //Process each group of y,z placements separately.
            foreach (var yzGroup in this.PositionPlacements.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceZ)))
            {
                List<PlanogramPositionPlacementGroup> xPlacementGroups = new List<PlanogramPositionPlacementGroup>();
                foreach (var xGroup in yzGroup.GroupBy(p => p.Position.SequenceX).OrderBy(g => g.Key))
                {
                    xPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                        /*placements*/xGroup.ToList(),
                        /*seqX*/xGroup.Key,
                        /*seqY*/yzGroup.Key.Item1,
                        /*seqZ*/yzGroup.Key.Item2));
                }
                this.ProcessXAxis(xPlacementGroups);
            }

            //Z AXIS:
            // Process each group of y,x placements separately
            foreach (var yxGroup in this.PositionPlacements.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceX)))
            {
                List<PlanogramPositionPlacementGroup> zPlacementGroups = new List<PlanogramPositionPlacementGroup>();
                foreach (var zGroup in yxGroup.GroupBy(p => p.Position.SequenceZ).OrderBy(g => g.Key))
                {
                    zPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                        /*placements*/zGroup.ToList(),
                        /*seqX*/yxGroup.Key.Item2,
                        /*seqY*/yxGroup.Key.Item1,
                        /*seqZ*/zGroup.Key));
                }
                this.ProcessZAxis(zPlacementGroups);
            }
        }

        /// <summary>
        /// Processes this group as hang from bottom merchandising type.
        /// This means that positions are to be hung from the bottom facing of the subs
        /// like a rod.
        /// </summary>
        private void ProcessHangFromBottom()
        {
            Debug.Assert(this.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom,
                "This method should only be executed if the merch type is HangFromBottom.");


            // For this type we aim to keep the products in neat horizontally lined up groups. 
            // Therefore the order in which we process the axis and how the positions are grouped when we do this is important.

            //Please be aware that this has been kept separate for posterity but is actually the same type of process as the hang type version.


            //Y AXIS:
            //Group positions by their sequence y value.
            List<PlanogramPositionPlacementGroup> yPlacementGroups = new List<PlanogramPositionPlacementGroup>();
            foreach (var yGroup in this.PositionPlacements.GroupBy(p => p.Position.SequenceY).OrderBy(g => g.Key))
            {
                yPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                    /*placements*/yGroup.ToList(),
                    /*seqX*/null,
                    /*seqY*/yGroup.Key,
                    /*seqZ*/null));
            }
            this.ProcessYAxis(yPlacementGroups);

            // X AXIS:
            //Process each group of y,z placements separately.
            foreach (var yzGroup in this.PositionPlacements.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceZ)))
            {
                List<PlanogramPositionPlacementGroup> xPlacementGroups = new List<PlanogramPositionPlacementGroup>();
                foreach (var xGroup in yzGroup.GroupBy(p => p.Position.SequenceX).OrderBy(g => g.Key))
                {
                    xPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                        /*placements*/xGroup.ToList(),
                        /*seqX*/xGroup.Key,
                        /*seqY*/yzGroup.Key.Item1,
                        /*seqZ*/yzGroup.Key.Item2));
                }
                this.ProcessXAxis(xPlacementGroups);
            }

            //Z AXIS:
            // Process each group of y,x placements separately
            foreach (var yxGroup in this.PositionPlacements.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceX)))
            {
                List<PlanogramPositionPlacementGroup> zPlacementGroups = new List<PlanogramPositionPlacementGroup>();
                foreach (var zGroup in yxGroup.GroupBy(p => p.Position.SequenceZ).OrderBy(g => g.Key))
                {
                    zPlacementGroups.Add(PlanogramPositionPlacementGroup.NewPlanogramPositionPlacementGroup(
                        /*placements*/zGroup.ToList(),
                        /*seqX*/yxGroup.Key.Item2,
                        /*seqY*/yxGroup.Key.Item1,
                        /*seqZ*/zGroup.Key));
                }
                this.ProcessZAxis(zPlacementGroups);
            }
        }

        /// <summary>
        /// Finalizes the process details. This will only be called when the merch group is 
        /// ready to apply its changes back to the original position models.
        /// Its main purpose is to place positions on the correct sub parent and ensure that their
        /// x coord and seq values are relative to that parent.
        /// </summary>
        private void ProcessFinalize()
        {
            // get the relative spread bounds for each sub
            Dictionary<PlanogramSubComponentPlacement, Single> subBounds =
                new Dictionary<PlanogramSubComponentPlacement, Single>();
            Single nextSubX = 0;
            foreach (PlanogramSubComponentPlacement sub in this.SubComponentPlacements)
            {
                subBounds.Add(sub, nextSubX);
                nextSubX += sub.SubComponent.Width;
            }

            // commit new position placements
            foreach (PlanogramPositionPlacement placement in this.PositionPlacements)
            {
                PlanogramPosition position = placement.Position;
                PlanogramPositionDetails positionDetails = placement.GetPositionDetails();

                if (this.SubComponentPlacements.Any())
                {
                    //determine which sub the position now belongs to.
                    Single newX = position.X;

                    //Use center of position i.e. greatest proportion on component wins
                    Single posCenterX = ((positionDetails.TotalSize.Width / 2));
                    posCenterX += newX;

                    PlanogramSubComponentPlacement parentSub = this.SubComponentPlacements.First();
                    foreach (var s in subBounds)
                    {
                        if (posCenterX.LessThan(s.Value))
                        {
                            break;
                        }
                        parentSub = s.Key;
                    }

                    //move the position if required.
                    if (parentSub != placement.SubComponentPlacement)
                    {
                        placement.SubComponentPlacement = parentSub;
                    }

                    //adjust the newX relative to the parent sub
                    newX = newX - subBounds[parentSub];

                    placement.Position.X = newX;
                }

                // Commit placement
                Boolean isRightFlow = (this.XAxis.Strategy == PlanogramPlacementStrategy.Max);
                Single newPosX = position.X;
                Single newPosY = position.Y;
                Single newPosZ = position.Z;

                //X
                if (isRightFlow)
                {
                    Single actualPosWidth = positionDetails.TotalSize.Width;

                    // divider will be placed to the right
                    newPosX = (position.X + positionDetails.TotalSpace.Width) -
                              positionDetails.TotalSize.Width;
                }

                //Z
                if (this.ZAxis.Strategy == PlanogramPlacementStrategy.Max)
                {
                    newPosZ = (position.Z + positionDetails.TotalSpace.Depth) -
                              positionDetails.TotalSize.Depth;
                }

                //Set
                //NB- I am rounding the values here so they come out neater 
                //TODO: Look into why they are dodgy in the first place.
                position.X = Convert.ToSingle(Math.Round(newPosX, 5));
                position.Y = Convert.ToSingle(Math.Round(newPosY, 5));
                position.Z = Convert.ToSingle(Math.Round(newPosZ, 5));
                placement.IsRelativeToMerchandisingGroup = false;
            }

            //[V8-27989] resequence subcomponents in case anything has moved sub
            foreach (PlanogramSubComponentPlacement subComponentPlacement in this.SubComponentPlacements)
            {
                IEnumerable<PlanogramPositionPlacement> positions =
                    this.PositionPlacements.Where(p => p.SubComponentPlacement == subComponentPlacement);

                if (positions.Any())
                {
                    Int16 minSeq = (Int16)(positions.Min(p => p.Position.Sequence) - 1);
                    Int16 minSeqX = (Int16)(positions.Min(p => p.Position.SequenceX) - 1);
                    foreach (PlanogramPositionPlacement p in positions)
                    {
                        p.Position.Sequence -= minSeq;
                        p.Position.SequenceX -= minSeqX;
                    }
                }
            }
        }

        #region X Axis Methods

        /// <summary>
        /// Places the provided positions along
        /// the x axis, returning divider details
        /// </summary>
        private IEnumerable<PlanogramDividerPlacement> ProcessXAxis(
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            //Apply full width squeeze if required:
            if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.FullSqueeze)
            {
                foreach (PlanogramPositionPlacementGroup group in placementGroups)
                {
                    group.ApplyFullHorizontalSqueeze();
                }
            }


            //determine if we have normal dividers, dividers per facing or no dividers at all.
            PlanogramMerchandisingGroupAxis axis = this.GetAxis(AxisType.X);
            Boolean isDividerPerFacing = AreDividersPlacedByFacing();
            Boolean isNormalDividers = AreDividersPlacedNormally(axis);

            List<DividerPlacementValue> dividerValues = null;

            if (isDividerPerFacing)
            {
                //Place the groups with a divider per facing.
                dividerValues = PlaceGroupsAlongXWithDividerPerFacing(axis, placementGroups);
            }
            else if (isNormalDividers)
            {
                //Place the groups with a divider per group.
                dividerValues = PlaceGroupsAlongXWithDividers(axis, placementGroups);
            }
            else
            {
                //Place the groups without any dividers
                PlaceGroupsAlongX(axis, placementGroups);
            }


            //create output dividers if required.
            List<PlanogramDividerPlacement> dividers = new List<PlanogramDividerPlacement>();
            if (dividerValues != null && dividerValues.Any())
            {
                foreach (DividerPlacementValue n in dividerValues.Distinct())
                {
                    PlanogramDividerPlacement div = PlanogramDividerPlacement.NewPlanogramDividerPlacement();

                    div.Width = this.DividerWidth;
                    div.X = n.Value;

                    if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                    {
                        div.Y = this.MinY;
                        div.Height = Math.Max(1, this.DividerHeight);

                        div.Depth = this.MaxZ - this.MinZ;
                        div.Z = this.MinZ;
                    }
                    else if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                    {
                        div.Y = placementGroups.Min(g => g.Placements.Min(p => p.Position.Y));
                        div.Height =
                            placementGroups.Max(
                                g => g.Placements.Max(p => p.Position.Y + p.GetPositionDetails().TotalSpace.Height)) -
                            div.Y;

                        div.Depth = Math.Max(1, this.DividerDepth);
                        div.Z = this.MinZ;
                    }

                    dividers.Add(div);
                }
                this.DividerPlacements.AddRange(dividers);
            }

            return dividers;
        }

        #region X Placement - Normal

        /// <summary>
        /// Organises the given placement groups along the x axis without any dividers according to the axis strategy.
        /// </summary>
        private void PlaceGroupsAlongX(PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            switch (axis.Strategy)
            {
                #region Min

                case PlanogramPlacementStrategy.Min:
                    {
                        Single placedLength = PlaceAtMinX(axis.Min, placementGroups);

                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            //If we have space and if any placement groups are overfilled
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements)))
                            {
                                //Squeeze the groups widths keeping a track of the new
                                // occupied space.
                                SqueezeProductWidth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMinX(axis.Min, g);
                                    }),
                                    ref placedLength);
                            }
                        }


                        //if placements break max but flag says we can't
                        // and we are allowed to break min instead then replace
                        if (!axis.CanBreakMax && axis.CanBreakMin)
                        {
                            Single maxX = placementGroups.Max(o => o.Placements.Max(p => p.Position.X));
                            if (maxX.GreaterThan(axis.Max))
                            {
                                //replace at max
                                PlaceAtMaxX(axis.Max, placementGroups);
                            }
                        }

                    }
                    break;

                #endregion

                #region Max

                case PlanogramPlacementStrategy.Max:
                    {
                        Single placedLength = PlaceAtMaxX(axis.Max, placementGroups);

                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            //If we have space and if any placement groups are overfilled
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements)))
                            {
                                //squeeze the products to fit them on.
                                SqueezeProductWidth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMaxX(axis.Max, g);
                                    }),
                                    ref placedLength);
                            }
                        }


                        //if placements break min but flag says we can't
                        // and we are allowed to break max instead then replace
                        if (!axis.CanBreakMin && axis.CanBreakMax)
                        {
                            Single minX = placementGroups.Min(o => o.Placements.Min(p => p.Position.X));
                            if (minX.LessThan(axis.Min))
                            {
                                //replace at min.
                                PlaceAtMinX(axis.Min, placementGroups);
                            }
                        }
                    }
                    break;

                #endregion

                #region Even
                case PlanogramPlacementStrategy.Even:
                    {
                        //Start by placing the groups as left stacked so that we 
                        //can get the min width the entire placement will need.
                        Single totalPlacedWidth = PlaceAtMinX(axis.Min, placementGroups);


                        //Check if we are over or under fitted:
                        Single axisLength = axis.Max - axis.Min;
                        if (axisLength.GreaterThan(0))
                        {
                            //If we are overfitted,
                            // Try to apply a partial squeeze to products
                            if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements)))
                            {
                                //Try to squeeze the products to fit them on.
                                SqueezeProductWidth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMinX(axis.Min, g);
                                    }),
                                    ref totalPlacedWidth);
                            }



                            //If we are still overfitted:
                            if (totalPlacedWidth.GreaterThan(axisLength))
                            {
                                //  Overfitted but cannot break min:
                                if (!axis.CanBreakMin && axis.CanBreakMax)
                                {
                                    //do nothing - we have already placed at min.
                                }

                                //Overfitted but cannot break max - place at max instead 
                                else if (axis.CanBreakMin && !axis.CanBreakMax)
                                {
                                    PlaceAtMaxX(axis.Max, placementGroups);
                                }

                                else
                                {
                                    //placed width is over the merch area width so need
                                    // to overhang equally over both sides
                                    Single placementOffset = -((totalPlacedWidth - axisLength) / 2F);
                                    foreach (var pGroup in placementGroups)
                                    {
                                        pGroup.SetGroupX(pGroup.GetBounds().X + placementOffset);
                                    }
                                }
                            }

                            //else if we are underfilled:
                            else if (totalPlacedWidth.LessThan(axisLength))
                            {
                                if (placementGroups.Count() == 1)
                                {
                                    //only one group so just place it centrally.
                                    placementGroups.First().SetGroupX((axisLength - totalPlacedWidth) / 2F);
                                }
                                else
                                {
                                    PlaceEvenWhenUnderfilledAlongAxis(axis, placementGroups, axisLength, totalPlacedWidth);
                                }

                            }

                        }
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramPlacementStrategy.Manual:
                    {
                        IEnumerable<PlanogramPositionPlacement> posList = placementGroups.SelectMany(p => p.Placements);

                        //enforce min and max
                        if (!axis.CanBreakMin ||
                            !axis.CanBreakMax)
                        {
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                PlanogramPositionDetails positionDetails = p.GetPositionDetails();
                                if (!axis.CanBreakMax &&
                                    ((p.Position.X + positionDetails.TotalSpace.Width).GreaterThan(axis.Max)))
                                {
                                    p.Position.X = axis.Max - positionDetails.TotalSpace.Width;
                                }

                                if (!axis.CanBreakMin &&
                                    (p.Position.X.LessThan(axis.Min)))
                                {
                                    p.Position.X = axis.Min;
                                }
                            }
                        }
                        else if (this.HasRow1PegHoles ||
                                 this.HasRow2PegHoles)
                        {
                            //force positions to snap to the nearest peghole
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                p.SnapPositionToPegHole();
                            }
                        }

                        //correct collisions if overlapping is not allowed
                        if (!this.IsProductOverlapAllowed)
                        {
                            List<PlanogramPositionPlacement> placedPositions = new List<PlanogramPositionPlacement>();

                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                foreach (PlanogramPositionPlacement other in placedPositions)
                                {
                                    if (p.CollidesWith(other))
                                    {
                                        p.Position.X = other.Position.X + other.GetPositionDetails().TotalSpace.Width;
                                    }
                                }

                                placedPositions.Add(p);
                            }
                        }
                    }
                    break;

                    #endregion
            }
        }

        /// <summary>
        /// Places the groups at min x without any dividers.
        /// </summary>
        /// <returns>The placed width of the groups</returns>
        private static Single PlaceAtMinX(Single axisMin, IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            Single nextN = axisMin;

            foreach (var posGroup in placementGroups)
            {
                foreach (PlanogramPositionPlacement p in posGroup.Placements)
                {
                    //if the coord is not changing then
                    // make sure that it is snapped to pegholes.
                    if (p.Position.X.EqualTo(nextN))
                    {
                        p.SnapPositionToPegHole();
                    }
                    else
                    {
                        p.Position.X = nextN;
                    }
                }
                nextN = posGroup.Placements.Min(p => p.Position.X) +
                        posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Width);
            }

            return nextN - axisMin;
        }

        /// <summary>
        /// Places groups at max X without any dividers
        /// </summary>
        /// <returns>The placed width of the groups</returns>
        private static Single PlaceAtMaxX(Single axisMax, IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            IEnumerable<PlanogramPositionPlacementGroup> orderedPlacementGroups = placementGroups;

            //we are placing at max so reverse the group order.
            orderedPlacementGroups = placementGroups.Reverse();

            Single nextEndN = axisMax;

            //cycle through stacking max to min
            foreach (var posGroup in orderedPlacementGroups)
            {
                foreach (PlanogramPositionPlacement p in posGroup.Placements)
                {
                    Single nextN = nextEndN - p.GetPositionDetails().TotalSpace.Width;

                    //if the coord is not changing then
                    // make sure that it is snapped to pegholes.
                    if (p.Position.X.EqualTo(nextN))
                    {
                        p.SnapPositionToPegHole();
                    }
                    else
                    {
                        p.Position.X = nextN;
                    }
                }
                nextEndN = posGroup.Placements.Min(p => p.Position.X);
            }

            return axisMax - nextEndN;
        }

        #endregion

        #region X Placement - with dividers

        /// <summary>
        /// Organises the given placement groups along the x axis with a dividers between positions.
        /// </summary>
        /// <returns>A list of dividers to be created.</returns>
        private List<DividerPlacementValue> PlaceGroupsAlongXWithDividers(
            PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            List<DividerPlacementValue> dividerValues = new List<DividerPlacementValue>();

            switch (axis.Strategy)
            {
                #region Min

                case PlanogramPlacementStrategy.Min:
                    {
                        Single placedLength = PlaceAtMinXWithDividers(axis, placementGroups, out dividerValues);

                        //Apply a partial squeeze if required:
                        if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            //correct the axis max before trying to squeeze
                            Single axisMax = axis.Max;
                            if (this.PlaceEndDivider)
                            {
                                //if the axis max would cause the last divider to go over
                                // then we need to adjust the max down the the last possible slot.
                                axisMax = MathHelper.RoundToNearestIncrement(axisMax, axis.DividerSpacing);
                                if (axisMax.GreaterOrEqualThan(axis.Max)) axisMax = axisMax - axis.DividerSpacing + this.DividerWidth;
                            }

                            Single axisLength = axisMax - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements)))
                            {
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductWidth(placementGroups,
                                    axisLength,
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                     out List<DividerPlacementValue> divs) => PlaceAtMinXWithDividers(axis, placementGroups, out divs),
                                    ref placedLength,
                                    out squeezedDividerValues);
                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;

                            }
                        }


                        //if placements break max but flag says we can't
                        // and we are allowed to break min instead then replace
                        if (!axis.CanBreakMax && axis.CanBreakMin)
                        {
                            Single maxX = placementGroups.Max(o => o.Placements.Max(p => p.Position.X));
                            if (dividerValues.Count > 0) maxX = Math.Max(maxX, dividerValues.Select(d => d.Value).Max());

                            if (maxX.GreaterThan(axis.Max))
                            {
                                //replace at max
                                PlaceAtMaxXWithDividers(axis, placementGroups, out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Max

                case PlanogramPlacementStrategy.Max:
                    {
                        Single placedLength = PlaceAtMaxXWithDividers(axis, placementGroups, out dividerValues);


                        //Apply a partial squeeze if required:
                        if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements)))
                            {
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductWidth(placementGroups,
                                    axisLength,
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                     out List<DividerPlacementValue> divs) => PlaceAtMaxXWithDividers(axis, placementGroups, out divs),
                                    ref placedLength,
                                    out squeezedDividerValues);
                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;

                            }
                        }


                        //if placements break min but flag says we can't
                        // and we are allowed to break max instead then replace
                        if (!axis.CanBreakMin && axis.CanBreakMax)
                        {
                            Single minX = placementGroups.Min(o => o.Placements.Min(p => p.Position.X));
                            if (dividerValues.Count > 0) minX = Math.Min(minX, dividerValues.Select(d => d.Value).Min());

                            if (minX.LessThan(axis.Min))
                            {
                                //replace at min.
                                PlaceAtMinXWithDividers(axis, placementGroups, out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Even

                case PlanogramPlacementStrategy.Even:
                    {
                        //Start by placing the groups as left stacked so that we 
                        //can get the min width the entire placement will need.
                        Single totalPlacedWidth = PlaceAtMinXWithDividers(axis, placementGroups, out dividerValues);

                        //Check if we are over or under fitted:
                        Single axisLength = axis.Max - axis.Min;
                        if (axisLength.EqualTo(0)) break;

                        //If we are overfitted,
                        // Try to apply a partial squeeze to products
                        if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze
                            && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements)))
                        {
                            //Try to squeeze the products to fit them on.
                            List<DividerPlacementValue> squeezedDividerValues;
                            SqueezeProductWidth(placementGroups,
                                axisLength,
                                (IEnumerable<PlanogramPositionPlacementGroup> g,
                                 out List<DividerPlacementValue> divs) => PlaceAtMinXWithDividers(axis, placementGroups, out divs),
                                    ref totalPlacedWidth,
                                out squeezedDividerValues);
                            if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                        }

                        //If we are still overfitted:
                        if (totalPlacedWidth.GreaterThan(axisLength))
                        {
                            #region Overfitted

                            //  Overfilled but cannot break min:
                            if (!axis.CanBreakMin && axis.CanBreakMax)
                            {
                                //do nothing - we have already placed at min.
                            }

                            //Overfilled but cannot break max:
                            else if (axis.CanBreakMin && !axis.CanBreakMax)
                            {
                                //place at max instead
                                PlaceAtMaxXWithDividers(axis, placementGroups, out dividerValues);
                            }

                            else
                            {
                                Single placementOffset = -((totalPlacedWidth - axisLength) / 2F);

                                //snap to slot if req
                                Single newStart = GetDividerStartCoordinate(axis) + placementOffset;
                                newStart =
                                    Convert.ToSingle(
                                        Math.Round(newStart / axis.DividerSpacing, MidpointRounding.AwayFromZero) *
                                        axis.DividerSpacing);
                                placementOffset = newStart + GetDividerStartCoordinate(axis);


                                foreach (var pGroup in placementGroups)
                                {
                                    pGroup.SetGroupX(pGroup.GetBounds().X + placementOffset);
                                }
                                foreach (var div in dividerValues)
                                {
                                    div.Value += placementOffset;
                                }
                            }

                            #endregion
                        }

                        //else if we are underfitted:
                        else if (totalPlacedWidth.LessThan(axisLength))
                        {
                            #region Underfitted

                            Single merchCenter = GetDividerStartCoordinate(axis) + (axisLength / 2F);

                            //UNDER-FILLED:
                            //the positions need to be spread equally within the available space.
                            if (placementGroups.Count() == 1)
                            {
                                //Underfilled - 1 group
                                PlanogramPositionPlacementGroup placementGroup = placementGroups.First();
                                RectValue groupBounds = placementGroup.GetBounds();
                                placementGroup.SetGroupX(merchCenter - (groupBounds.X + groupBounds.Width / 2F));
                                foreach (var div in dividerValues)
                                {
                                    div.Value += placementGroup.GetBounds().X - groupBounds.X;
                                }
                            }
                            else
                            {
                                //Underfilled - with dividers
                                PlaceEvenStrategyUnderfilledWithNormalDividers(
                                    axis, axisLength, placementGroups, ref dividerValues);
                            }

                            #endregion
                        }

                    }
                    break;

                #endregion

                #region Manual

                case PlanogramPlacementStrategy.Manual:
                    {
                        IEnumerable<PlanogramPositionPlacement> posList = placementGroups.SelectMany(p => p.Placements);

                        //enforce min and max
                        if (!axis.CanBreakMin ||
                            !axis.CanBreakMax)
                        {
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                PlanogramPositionDetails positionDetails = p.GetPositionDetails();
                                if (!axis.CanBreakMax &&
                                    ((p.Position.X + positionDetails.TotalSpace.Width).GreaterThan(axis.Max)))
                                {
                                    p.Position.X = axis.Max - positionDetails.TotalSpace.Width;
                                }

                                if (!axis.CanBreakMin &&
                                    (p.Position.X.LessThan(axis.Min)))
                                {
                                    p.Position.X = axis.Min;
                                }
                            }
                        }
                        else if (this.HasRow1PegHoles ||
                                 this.HasRow2PegHoles)
                        {
                            //force positions to snap to the nearest peghole
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                p.SnapPositionToPegHole();
                            }
                        }

                        //correct collisions if overlapping is not allowed
                        if (!this.IsProductOverlapAllowed)
                        {
                            List<PlanogramPositionPlacement> placedPositions = new List<PlanogramPositionPlacement>();

                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                foreach (PlanogramPositionPlacement other in placedPositions)
                                {
                                    if (p.CollidesWith(other))
                                    {
                                        p.Position.X = other.Position.X + other.GetPositionDetails().TotalSpace.Width;
                                    }
                                }

                                placedPositions.Add(p);
                            }
                        }


                        //place dividers
                        foreach (PlanogramPositionPlacement p in posList.OrderBy(p => p.Position.X))
                        {
                            PlanogramPositionDetails positionDetails = p.GetPositionDetails();

                            Single placementN = p.Position.X;
                            if (placementN.GreaterOrEqualThan(axis.DividerStart))
                            {
                                Boolean isFirst = posList.First() == p;
                                Boolean isLast = posList.Last() == p;

                                if (isFirst)
                                {
                                    if (this.PlaceStartDivider)
                                    {
                                        Single mod = (placementN - axis.DividerStart) % axis.DividerSpacing;

                                        Single firstDiv = placementN - mod;
                                        if (firstDiv.GreaterThan(axis.DividerStart))
                                        {
                                            dividerValues.Add(new DividerPlacementValue(placementN - mod, this.DividerWidth)
                                            {
                                                IsStartDivider = true
                                            });
                                        }
                                        else
                                        {
                                            dividerValues.Add(new DividerPlacementValue(axis.DividerStart, this.DividerWidth)
                                            {
                                                IsStartDivider = true
                                            });
                                        }
                                    }
                                }
                                if (!isLast ||
                                    (isLast && this.PlaceEndDivider))
                                {
                                    Single mod = (placementN + positionDetails.TotalSpace.Width - axis.DividerStart) %
                                                 axis.DividerSpacing;

                                    Single afterDiv = placementN + positionDetails.TotalSpace.Width +
                                                      (axis.DividerSpacing - mod);
                                    dividerValues.Add(new DividerPlacementValue(afterDiv, this.DividerWidth)
                                    {
                                        IsEndDivider = isLast
                                    });
                                }

                                isFirst = false;
                            }
                        }

                    }
                    break;

                    #endregion
            }

            return dividerValues;
        }

        /// <summary>
        /// Places the groups at min x with dividers between positions.
        /// </summary>
        private Single PlaceAtMinXWithDividers(PlanogramMerchandisingGroupAxis axis,
                                               IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
                                               out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();
            Single nextN = axis.Min;
            Boolean IgnoreLeftOverhang = true;
            Single divStart = GetDividerStartCoordinate(axis, IgnoreLeftOverhang);
            Single nextDivX = divStart;

            var isFirstDiv = true;

            //cycle through stacking min to max
            foreach (PlanogramPositionPlacementGroup posGroup in placementGroups)
            {
                //if under the divider start point
                //or if the first div is being placed and would intersect
                if (nextN.GreaterOrEqualThan(divStart)
                    ||
                    (isFirstDiv && PlaceStartDivider && (nextN + posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Width)).GreaterOrEqualThan(divStart)))
                {
                    //set the slot constraint on the group
                    posGroup.DividerSlotSizeX = axis.DividerSpacing;
                    posGroup.DividerSlotReservedX = DividerWidth;

                    if (isFirstDiv)
                    {
                        //place the start divider
                        if (PlaceStartDivider || axis.DividerStart.GreaterThan(0))
                        {
                            dividerValues.Add(new DividerPlacementValue(nextDivX, DividerWidth) { IsStartDivider = PlaceStartDivider });
                            PlanogramSubComponent subComponent = SubComponentPlacements.First().SubComponent;
                            if (Math.Abs(subComponent.LeftOverhang) > subComponent.DividerObstructionStartX)
                            {
                                nextN = Math.Abs(subComponent.LeftOverhang) + DividerWidth;
                            }else
                            {
                                nextN = nextDivX + DividerWidth;
                            }    
                        }
                        isFirstDiv = false;
                    }
                    else
                    {
                        dividerValues.Add(new DividerPlacementValue(nextDivX, DividerWidth));
                        nextN = nextDivX + DividerWidth;
                    }

                    //butt each position to the end X
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.X = nextN;
                    }

                    //set the next div pos
                    Single maxPosN = posGroup.Placements.Max(p => p.Position.X + p.GetPositionDetails().TotalSpace.Width);
                    while (nextDivX.LessThan(maxPosN))
                    {
                        nextDivX += axis.DividerSpacing;
                    }
                    nextN = nextDivX + DividerWidth;
                }
                else
                {
                    //mark the group as not in a slot
                    posGroup.DividerSlotSizeX = 0;
                    posGroup.DividerSlotReservedX = 0;

                    //place normally without dividers
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.X = nextN;
                    }
                    nextN = posGroup.Placements.Min(p => p.Position.X) +
                            posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Width);

                    //update the next div
                    while (nextDivX.LessThan(nextN))
                    {
                        nextDivX += axis.DividerSpacing;
                    }
                }
            }

            //place the end divider
            if (PlaceEndDivider)
            {
                dividerValues.Add(new DividerPlacementValue(nextDivX, DividerWidth) { IsEndDivider = true });
            }

            //return the length of the space that is taken up on the axis when the positions and dividers are placed.
            Single divMax = dividerValues.Any() ? dividerValues.Max(d => d.Value + DividerWidth) : Single.NegativeInfinity;
            Single posMax = placementGroups.SelectMany(g => g.Placements).Max(p => p.Position.X + p.GetPositionDetails().TotalSpace.Width);
            return Math.Max(posMax, divMax) - axis.Min;
        }

        /// <summary>
        /// Places groups at max x with normal dividers
        /// </summary>
        private Single PlaceAtMaxXWithDividers(PlanogramMerchandisingGroupAxis axis,
                                               IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
                                               out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();

            IEnumerable<PlanogramPositionPlacementGroup> orderedPlacementGroups = placementGroups.Reverse();

            Single nextEndN = axis.Max;
            Boolean IgnoreRightOverhang = true;
            Single divStart = GetDividerStartCoordinate(axis, IgnoreRightOverhang);
            Single nextDivX = divStart - DividerWidth;

            var isFirstDiv = true;

            //cycle through stacking max to min
            foreach (PlanogramPositionPlacementGroup posGroup in orderedPlacementGroups)
            {
                //if under the divider start point
                //or if the first div is being placed and would intersect
                if (nextEndN.LessOrEqualThan(divStart)
                    ||
                    (isFirstDiv && PlaceStartDivider &&
                     (nextEndN - posGroup.Placements.Max(p => p.GetPositionDetails().TotalSize.Width)).LessOrEqualThan(divStart)))
                {
                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = axis.DividerSpacing;
                    posGroup.DividerSlotReservedX = DividerWidth;

                    if (isFirstDiv)
                    {
                        //place the start divider
                        if (PlaceStartDivider || axis.DividerStart.GreaterThan(0))
                        {
                            dividerValues.Add(new DividerPlacementValue(nextDivX, DividerWidth) { IsStartDivider = PlaceStartDivider });
                            PlanogramSubComponent subComponentLast = SubComponentPlacements.Last().SubComponent;
                            PlanogramSubComponent subComponentFirst = SubComponentPlacements.First().SubComponent;

                            Single MaxComponentsWidth = SubComponentPlacements.Sum(placement => placement.SubComponent.Width);

                            if (Math.Abs(subComponentLast.RightOverhang) > subComponentFirst.DividerObstructionStartX)
                            {
                                nextEndN = (MaxComponentsWidth - Math.Abs(subComponentLast.RightOverhang)) - DividerWidth;
                            }
                            else
                            {
                                nextEndN = nextDivX;
                            }
                        }
                        isFirstDiv = false;
                    }
                    else
                    {
                        dividerValues.Add(new DividerPlacementValue(nextDivX, DividerWidth));
                        nextEndN = nextDivX;
                    }

                    //butt each position to the end X
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.X = nextEndN - p.GetPositionDetails().TotalSpace.Width;
                    }

                    //set the next div pos
                    Single minPosN = posGroup.Placements.Min(p => p.Position.X);
                    while ((nextDivX + DividerWidth).GreaterThan(minPosN))
                    {
                        nextDivX -= axis.DividerSpacing;
                    }
                }
                else
                {
                    //mark the group as not in a slot
                    posGroup.DividerSlotSizeX = 0;
                    posGroup.DividerSlotReservedX = 0;

                    //place normally without dividers
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.X = nextEndN - p.GetPositionDetails().TotalSpace.Width;
                    }
                    nextEndN = posGroup.Placements.Min(p => p.Position.X);

                    //update the next div
                    while ((nextDivX + DividerWidth).GreaterThan(nextEndN))
                    {
                        nextDivX -= axis.DividerSpacing;
                    }
                }
            }

            //place the end divider
            if (PlaceEndDivider)
            {
                dividerValues.Add(new DividerPlacementValue(nextDivX, DividerWidth) { IsEndDivider = true });
            }

            Single divMin = dividerValues.Any() ? dividerValues.Min(d => d.Value) : Single.PositiveInfinity;
            Single posMin = orderedPlacementGroups.SelectMany(g => g.Placements).Min(p => p.Position.X);
            return axis.Max - Math.Min(posMin, divMin);
        }

        #endregion

        #region X Placement - divider per facing

        /// <summary>
        /// Organises the given placement groups along the x axis with a divider between each facing.
        /// </summary>
        /// <returns>A list of dividers to be created.</returns>
        private List<DividerPlacementValue> PlaceGroupsAlongXWithDividerPerFacing(
            PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            List<DividerPlacementValue> dividerValues = new List<DividerPlacementValue>();

            switch (axis.Strategy)
            {
                #region Min

                case PlanogramPlacementStrategy.Min:
                    {
                        Single placedLength =
                            PlaceAtMinXWithDividerPerFacing(axis.Min,
                                placementGroups,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);

                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0))
                            {
                                //Single placedLength = GetPlacedWidth(placementGroups, dividerValues);
                                if (placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements))
                                    || ((placedLength.GreaterThan(axisLength)) && !this.XAxis.IsStackedStrategy))
                                {
                                    List<DividerPlacementValue> squeezedDividerValues;
                                    SqueezeProductWidth(placementGroups,
                                        axisLength,
                                        new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMinXWithDividerPerFacing(axis.Min,
                                            placementGroups,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref placedLength,
                                        out squeezedDividerValues);
                                    if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                                }
                            }
                        }


                        //if placements break max but flag says we can't
                        // and we are allowed to break min instead then replace
                        if (!axis.CanBreakMax && axis.CanBreakMin)
                        {
                            Single maxX = placementGroups.Max(o => o.Placements.Max(p => p.Position.X));
                            if (dividerValues.Count > 0) maxX = Math.Max(maxX, dividerValues.Select(d => d.Value).Max());

                            if (maxX.GreaterThan(axis.Max))
                            {
                                //replace at max
                                PlaceAtMaxXWithDividerPerFacing(axis.Max,
                                    placementGroups,
                                    this.DividerWidth,
                                    this.PlaceStartDivider,
                                    this.PlaceEndDivider,
                                    out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Max

                case PlanogramPlacementStrategy.Max:
                    {
                        Single placedLength =
                            PlaceAtMaxXWithDividerPerFacing(axis.Max,
                                placementGroups,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);


                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0))
                            {
                                //Single placedLength = GetPlacedWidth(placementGroups, dividerValues);
                                if (placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements))
                                    || ((placedLength.GreaterThan(axisLength)) && !this.XAxis.IsStackedStrategy))
                                {
                                    List<DividerPlacementValue> squeezedDividerValues;
                                    SqueezeProductWidth(placementGroups,
                                        axisLength,
                                        new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMaxXWithDividerPerFacing(axis.Max,
                                            placementGroups,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref placedLength,
                                        out squeezedDividerValues);
                                    if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                                }
                            }
                        }


                        //if placements break min but flag says we can't
                        // and we are allowed to break max instead then replace
                        if (!axis.CanBreakMin && axis.CanBreakMax)
                        {
                            Single minX = placementGroups.Min(o => o.Placements.Min(p => p.Position.X));
                            if (dividerValues.Count > 0) minX = Math.Min(minX, dividerValues.Select(d => d.Value).Min());

                            if (minX.LessThan(axis.Min))
                            {
                                //replace at min.
                                PlaceAtMinXWithDividerPerFacing(axis.Min,
                                    placementGroups,
                                    this.DividerWidth,
                                    this.PlaceStartDivider,
                                    this.PlaceEndDivider,
                                    out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Even

                case PlanogramPlacementStrategy.Even:
                    {
                        //TODO: Can break min max checks

                        //Start by placing the groups as left stacked so that we 
                        //can get the min width the entire placement will need.
                        Single totalPlacedWidth =
                            PlaceAtMinXWithDividerPerFacing(axis.Min,
                                placementGroups,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);

                        Single axisLength = axis.Max - axis.Min;
                        if (axisLength.GreaterThan(0))
                        {
                            //Apply a partial squeeze to products if required:
                            if (this.SqueezeTypeX == PlanogramSubComponentSqueezeType.PartialSqueeze
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.X, g.Placements)))
                            {
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductWidth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMinXWithDividerPerFacing(axis.Min,
                                            placementGroups,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref totalPlacedWidth,
                                    out squeezedDividerValues);
                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                            }

                            //If we are still overfitted:
                            if (totalPlacedWidth.GreaterThan(axisLength))
                            {
                                Single placementOffset = -((totalPlacedWidth - axisLength) / 2F);

                                //snap to slot if req
                                if (dividerValues.Count > 0 &&
                                    axis.DividerSpacing.GreaterThan(0))
                                {
                                    Single newStart = axis.Min + placementOffset;
                                    newStart =
                                        Convert.ToSingle(
                                            Math.Round(newStart / axis.DividerSpacing, MidpointRounding.AwayFromZero) *
                                            axis.DividerSpacing);
                                    placementOffset = newStart + axis.Min;
                                }

                                foreach (var pGroup in placementGroups)
                                {
                                    pGroup.SetGroupX(pGroup.GetBounds().X + placementOffset);
                                }
                                foreach (var div in dividerValues)
                                {
                                    div.Value += placementOffset;
                                }
                            }

                            //else if we are underfilled:
                            else if (totalPlacedWidth.LessThan(axisLength))
                            {
                                Single merchCenter = axis.Min + (axisLength / 2F);

                                //the positions need to be spread equally within the available space.
                                if (placementGroups.Count() == 1)
                                {
                                    //Underfilled - 1 group
                                    RectValue groupBounds = placementGroups.First().GetBounds();

                                    Single newGroupX = merchCenter - (groupBounds.X + groupBounds.Width / 2F);
                                    placementGroups.First().SetGroupX(newGroupX);

                                    //offset divs
                                    Single placementOffset = newGroupX - groupBounds.X;
                                    foreach (var div in dividerValues) div.Value += placementOffset;
                                }
                                else
                                {
                                    Single placementOffset = -((totalPlacedWidth - axisLength) / 2F);
                                    foreach (var pGroup in placementGroups)
                                    {
                                        pGroup.SetGroupX(pGroup.GetBounds().X + placementOffset);
                                    }
                                    foreach (var div in dividerValues)
                                    {
                                        div.Value += placementOffset;
                                    }

                                }

                            }
                        }
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramPlacementStrategy.Manual:
                    {
                        //TODO: Can break min max checks
                        IEnumerable<PlanogramPositionPlacement> placements = placementGroups.SelectMany(p => p.Placements);

                        foreach (PlanogramPositionPlacement placement in placements.OrderBy(p => p.Position.X))
                        {
                            PlanogramPositionDetails positionDetails = placement.GetPositionDetails();

                            Boolean isFirst = placements.First() == placement;
                            Boolean isLast = placements.Last() == placement;

                            Single placementN = placement.Position.X;

                            if (isFirst)
                            {
                                if (this.PlaceStartDivider)
                                {
                                    Single firstDiv = placementN - this.DividerWidth;
                                    if (firstDiv.GreaterThan(axis.Min))
                                    {
                                        dividerValues.Add(new DividerPlacementValue(firstDiv, this.DividerWidth)
                                        {
                                            IsStartDivider = true
                                        });
                                    }
                                    else
                                    {
                                        dividerValues.Add(new DividerPlacementValue(axis.Min, this.DividerWidth)
                                        {
                                            IsStartDivider = true
                                        });
                                    }
                                }
                            }
                            if (!isLast ||
                                (isLast && this.PlaceEndDivider))
                            {
                                Single afterDiv = placementN + positionDetails.TotalSpace.Width;
                                dividerValues.Add(new DividerPlacementValue(afterDiv, this.DividerWidth)
                                {
                                    IsEndDivider = isLast
                                });
                            }

                            //place internal dividers
                            IEnumerable<Single> facings = placement.GetFacingsXAndWidth().Select(i => i.Item1);
                            foreach (Single facingStart in facings)
                            {
                                dividerValues.Add(new DividerPlacementValue(placementN + facingStart - this.DividerWidth,
                                    this.DividerWidth));
                            }

                            isFirst = false;
                        }
                    }
                    break;

                    #endregion
            }

            return dividerValues;
        }

        /// <summary>
        /// Places the groups at min x with a divider per facing.
        /// </summary>
        private static Single PlaceAtMinXWithDividerPerFacing(Single axisMin,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single dividerWidth,
            Boolean placeStartDivider,
            Boolean placeEndDivider,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();

            Single nextN = axisMin;
            Boolean isFirst = true;

            foreach (PlanogramPositionPlacementGroup group in placementGroups)
            {
                //just use the largest position for now.
                PlanogramPositionPlacement placement =
                    @group.Placements.OrderByDescending(p => p.GetPositionDetails().TotalSpace.Width).First();

                PlanogramPositionDetails positionDetails = placement.GetPositionDetails();

                Single posX = nextN;

                //if this is the first divider then only place it if
                // the flag is enabled.
                if (isFirst && placeStartDivider)
                {
                    posX += dividerWidth;
                    dividerValues.Add(new DividerPlacementValue(nextN, dividerWidth) { IsStartDivider = true });
                }

                // otherwise just increment the width.
                else if (!isFirst)
                {
                    posX += dividerWidth;
                }


                //update the x coordinates of all positions in the group.
                foreach (PlanogramPositionPlacement p in @group.Placements)
                {
                    p.Position.X = posX;
                }


                //place internal dividers
                // if we only have one position in the group.
                if (@group.Placements.Count() == 1)
                {
                    IEnumerable<Single> facings = placement.GetFacingsXAndWidth().Select(i => i.Item1);
                    for (Int32 i = 0; i < facings.Count(); i++)
                    {
                        if (isFirst && i == 0) continue;

                        Single divValue = placement.Position.X + facings.ElementAt(i) - dividerWidth;
                        if (!dividerValues.Select(d => d.Value).Contains(divValue))
                        {
                            dividerValues.Add(new DividerPlacementValue(divValue, dividerWidth));
                        }
                    }
                }


                isFirst = false;
                nextN = placement.Position.X + positionDetails.TotalSpace.Width;
            }

            //place the end divider if required.
            if (placeEndDivider)
            {
                if (!dividerValues.Select(d => d.Value).Contains(nextN))
                {
                    dividerValues.Add(new DividerPlacementValue(nextN, dividerWidth) { IsEndDivider = true });
                    nextN += dividerWidth;
                }
            }

            return nextN - axisMin;
        }

        /// <summary>
        /// Places groups at max x with a divider per facing.
        /// </summary>
        private static Single PlaceAtMaxXWithDividerPerFacing(Single axisMax,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single dividerThick,
            Boolean placeStartDivider,
            Boolean placeEndDivider,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();

            //we are placing at max so reverse the group order.
            IEnumerable<PlanogramPositionPlacementGroup> orderedPlacementGroups = placementGroups.Reverse();

            Single nextEndN = axisMax;

            //add far right divider if req
            if (placeStartDivider)
            {
                nextEndN -= dividerThick;
                dividerValues.Add(new DividerPlacementValue(nextEndN, dividerThick) { IsStartDivider = true });
            }

            foreach (PlanogramPositionPlacementGroup group in orderedPlacementGroups)
            {
                //just use the largest position for now.
                PlanogramPositionPlacement placement =
                    group.Placements.OrderByDescending(p => p.GetPositionDetails().TotalSpace.Width).First();

                PlanogramPositionDetails positionDetails = placement.GetPositionDetails();

                //place all positions
                foreach (PlanogramPositionPlacement p in group.Placements)
                {
                    p.Position.X = nextEndN - placement.GetPositionDetails().TotalSpace.Width;
                }


                //set the next end x
                Single posWidth = positionDetails.TotalSpace.Width + dividerThick;
                nextEndN -= posWidth;

                //place internal dividers except for the first
                IEnumerable<Single> facings = placement.GetFacingsXAndWidth().Select(i => i.Item1);
                foreach (Single facingStart in facings.Reverse())
                {
                    Single divValue = placement.Position.X + facingStart - dividerThick;
                    if (!dividerValues.Select(d => d.Value).Contains(divValue))
                    {
                        dividerValues.Add(new DividerPlacementValue(divValue, dividerThick));
                    }
                }


                //foreach (PlanogramPositionPlacement placement in placements.OrderByDescending(p => p.Position.X))
                //{
                //    PlanogramPositionDetails positionDetails = placement.GetPositionDetails();

                //    placement.Position.X = nextEndN - positionDetails.TotalSpace.Width;

                //    //set the next end x
                //    Single posWidth = positionDetails.TotalSpace.Width + dividerThick;
                //    nextEndN -= posWidth;

                //    //place internal dividers except for the first
                //    IEnumerable<Single> facings = placement.GetFacingsXAndWidth().Select(i => i.Item1);
                //    foreach (Single facingStart in facings.Reverse())
                //    {
                //        Single divValue = placement.Position.X + facingStart - dividerThick;
                //        if (!dividerValues.Select(d => d.Value).Contains(divValue))
                //        {
                //            dividerValues.Add(new DividerPlacementValue(divValue, dividerThick));
                //        }
                //    }
                //}


            }

            //place last divider
            if (placeEndDivider)
            {
                dividerValues.Last().IsEndDivider = true;
            }
            else
            {
                dividerValues.Remove(dividerValues.Last());
                nextEndN += dividerThick;
            }

            return axisMax - nextEndN;
        }


        #endregion

        #region X Squeeze

        /// <summary>
        /// Squeezes the placement groups to fit the given merch area width.
        /// </summary>
        private Boolean SqueezeProductWidth(IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single merchAreaWidth,
            PlaceGroupsDelegate placeGroupsAction,
            ref Single currentWidth)
        {
            List<DividerPlacementValue> dividerValues;
            return SqueezeProductWidth(placementGroups, merchAreaWidth, placeGroupsAction,
                ref currentWidth,
                out dividerValues);
        }

        /// <summary>
        /// Squeezes the placement groups to fit the given merch area width with output dividers
        /// </summary>
        private Boolean SqueezeProductWidth(IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single merchAreaWidth,
            PlaceGroupsDelegate placeGroupsAction,
            ref Single currentWidth,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = null;

            //+ Check if we can squeeze by width:

            //if the current width is over the merch area width already return out.
            if (currentWidth.LessThan(merchAreaWidth)) return false;

            //determine if any groups can possibly be squeezed.
            List<PlanogramPositionPlacementGroup> squeezeableGroups =
                placementGroups.Where(p => p.GetFullySqueezedBounds().Width.LessThan(p.GetBounds().Width)).ToList();

            if (!squeezeableGroups.Any()) return false;


            //work out how much we need to squeeze by
            Single squeezePerGroup = (currentWidth - merchAreaWidth) / (Single)squeezeableGroups.Count();


            //+ Apply squeeze first to groups that would hit their minimum.
            Boolean applyFullSqueeze = true;
            while (applyFullSqueeze)
            {
                applyFullSqueeze = false;
                foreach (PlanogramPositionPlacementGroup group in squeezeableGroups.ToList())
                {
                    Single groupWidth = group.GetBounds().Width;
                    Single targetWidth = groupWidth - squeezePerGroup;
                    if (targetWidth.LessThan(group.GetFullySqueezedBounds().Width))
                    {
                        group.ApplyFullHorizontalSqueeze();
                        squeezeableGroups.Remove(group);
                        applyFullSqueeze = true;
                    }
                }


                if (applyFullSqueeze)
                {
                    //something changed so replace
                    currentWidth = placeGroupsAction(placementGroups, out dividerValues);

                    //recalc amount to squeeze remaining by
                    squeezePerGroup = (currentWidth - merchAreaWidth) / (Single)squeezeableGroups.Count();
                }
            }

            //now apply the squeeze accross anything left over.
            List<PlanogramPositionPlacementGroup> failedGroups = new List<PlanogramPositionPlacementGroup>();
            foreach (PlanogramPositionPlacementGroup group in squeezeableGroups.ToList())
            {
                squeezeableGroups.Remove(group);

                Single targetWidth = group.GetBounds().Width - squeezePerGroup;
                if (!group.SqueezeWidthTo(targetWidth))
                {
                    failedGroups.Add(group);
                }

                //recalc the squeeze per group
                currentWidth = placeGroupsAction(placementGroups, out dividerValues);
                squeezePerGroup = (currentWidth - merchAreaWidth) / (Single)squeezeableGroups.Count();
            }


            //if any of the groups failed to meet their targets
            // and we are still over the target
            // - go through squeezing them to their minimum
            if (failedGroups.Any())
            {
                currentWidth = placeGroupsAction(placementGroups, out dividerValues);

                if (currentWidth.GreaterThan(merchAreaWidth))
                {
                    foreach (PlanogramPositionPlacementGroup group in failedGroups)
                    {
                        group.ApplyFullHorizontalSqueeze();

                        currentWidth = placeGroupsAction(placementGroups, out dividerValues);

                        if (currentWidth.LessOrEqualThan(merchAreaWidth)) return true;
                    }

                    return false;
                }
            }


            //replace and return.
            currentWidth = placeGroupsAction(placementGroups, out dividerValues);
            return true;
        }


        #endregion

        #endregion

        #region Y Axis Methods

        /// <summary>
        /// Places the provided positions along
        /// the y axis, returning divider details
        /// </summary>
        private IEnumerable<PlanogramDividerPlacement> ProcessYAxis(
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            PlanogramMerchandisingGroupAxis axis = this.GetAxis(AxisType.Y);
            Boolean isNormalDividers = (!this.IsDividerObstructionByFacing) &&
                                       ((axis.DividerSpacing.GreaterThan(0)) && (this.DividerWidth.GreaterThan(0))
                                       && (this.DividerHeight.GreaterThan(0)) && (this.DividerDepth.GreaterThan(0)));

            //Apply full height squeeze if required:
            if (this.SqueezeTypeY == PlanogramSubComponentSqueezeType.FullSqueeze)
            {
                foreach (PlanogramPositionPlacementGroup group in placementGroups)
                {
                    group.ApplyFullVerticalSqueeze();
                }
            }

            List<DividerPlacementValue> dividerValues = null;
            if (isNormalDividers)
            {
                //Organise the groups along the y axis with dividers.
                dividerValues = PlaceGroupsAlongYWithDividers(axis, placementGroups);
            }
            else
            {
                //Organise the groups along the y axis without dividers.
                PlaceGroupsAlongY(axis, placementGroups);
            }

            //Create divider output
            List<PlanogramDividerPlacement> dividers = new List<PlanogramDividerPlacement>();
            if (dividerValues != null &&
                dividerValues.Count > 0)
            {
                Single dividerDepth = Math.Max(1, this.DividerDepth);

                foreach (Single n in dividerValues.Select(d => d.Value).Distinct())
                {
                    PlanogramDividerPlacement div = PlanogramDividerPlacement.NewPlanogramDividerPlacement();
                    div.Height = this.DividerWidth;
                    div.Width = this.MaxX - this.MinX;
                    div.X = this.MinX;
                    div.Y = n;

                    if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                    {
                        div.Depth = Math.Min(dividerDepth, this.MaxZ - this.MinZ);
                        div.Z = this.MaxZ - div.Depth;
                    }
                    else if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                    {
                        div.Depth = dividerDepth;
                        div.Z = this.MinZ;
                    }

                    dividers.Add(div);
                }
                this.DividerPlacements.AddRange(dividers);
            }

            return dividers;
        }

        #region Y Placement - normal

        /// <summary>
        /// Organises the given placement groups along the y axis without any dividers according to the axis strategy.
        /// </summary>
        private void PlaceGroupsAlongY(PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            //Adjust the axis min and max
            Single axisMin = axis.Min;

            Single axisMax = axis.Max;
            if ((axisMin.EqualTo(axisMax)) &&
                this.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
            {
                //if the min and max are the same and we are stacking then no merch height was specified.
                // so take the max as the max sum size y accross the groups
                axisMax = axisMin +
                          placementGroups.Sum(g => g.Placements.Max(p => p.GetPositionDetails().TotalSpace.Height));
            }

            switch (axis.Strategy)
            {
                #region Min

                case PlanogramPlacementStrategy.Min:
                    {
                        Single placedLength = PlaceAtMinY(axisMin, placementGroups);

                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeY == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0))
                            {
                                //Single placedLength = GetPlacedHeight(placementGroups);
                                if (placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                                {
                                    //Squeeze the groups widths.
                                    SqueezeProductHeight(placementGroups,
                                        axisLength,
                                        new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMinY(axisMin, g);
                                    }),
                                    ref placedLength);
                                }
                            }
                        }


                        //if placements break max but flag says we can't
                        // and we are allowed to break min instead then replace
                        if (!axis.CanBreakMax && axis.CanBreakMin)
                        {
                            Single maxY = placementGroups.Max(o => o.Placements.Max(p => p.Position.Y));
                            if (maxY.GreaterThan(axisMax))
                            {
                                //replace at max
                                PlaceAtMaxY(axisMax, placementGroups);
                            }
                        }
                    }
                    break;

                #endregion

                #region Max

                case PlanogramPlacementStrategy.Max:
                    {
                        Single placedLength = PlaceAtMaxY(axisMax, placementGroups);

                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeY == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0))
                            {
                                //Single placedLength = GetPlacedHeight(placementGroups);
                                if (placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                                {
                                    //squeeze the products to fit them on.
                                    SqueezeProductHeight(placementGroups,
                                        axisLength,
                                        new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMaxY(axisMax, placementGroups);
                                    }),
                                    ref placedLength);
                                }
                            }
                        }

                        //if placements break min but flag says we can't
                        // and we are allowed to break max instead then replace
                        if (!axis.CanBreakMin && axis.CanBreakMax)
                        {
                            Single minY = placementGroups.Min(o => o.Placements.Min(p => p.Position.Y));
                            if (minY.LessThan(axisMin))
                            {
                                //replace at min.
                                PlaceAtMinY(axisMin, placementGroups);
                            }
                        }

                    }
                    break;

                #endregion

                #region Even

                case PlanogramPlacementStrategy.Even:
                    {

                        //start by placing as min so that we can see how much space this occupies.
                        Single totalPlacedLength = PlaceAtMinY(axisMin, placementGroups);

                        Single axisLength = axisMax - axisMin;
                        if (axisLength.GreaterThan(0))
                        {
                            if (this.SqueezeTypeY == PlanogramSubComponentSqueezeType.PartialSqueeze
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                            {
                                //Try to squeeze the products to fit them on.
                                SqueezeProductHeight(placementGroups,
                                    axisLength,
                                     new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMinY(axisMin, g);
                                    }),
                                    ref totalPlacedLength);
                            }


                            //If we are still overfitted:
                            if (totalPlacedLength.GreaterThan(axisLength))
                            {
                                //  Overfilled but cannot break min:
                                if (!axis.CanBreakMin &&
                                    axis.CanBreakMax)
                                {
                                    //do nothing - we have already placed at min.
                                }

                                //Overfilled but cannot break max - place at max instead
                                else if (axis.CanBreakMin &&
                                         !axis.CanBreakMax)
                                {
                                    PlaceAtMaxY(axisMax, placementGroups);
                                }

                                // Overfilled - no dividers:
                                else
                                {
                                    //placed width is over the merch area width so need
                                    // to overhang equally over both sides
                                    Single placementOffset = -((totalPlacedLength - axisLength) / 2F);
                                    foreach (var pGroup in placementGroups)
                                    {
                                        pGroup.SetGroupY(pGroup.GetBounds().Y + placementOffset);
                                    }
                                }
                            }

                            //else if we are underfitted:
                            else if (totalPlacedLength.LessThan(axisLength))
                            {
                                if (placementGroups.Count() == 1)
                                {
                                    //Underfilled - 1 group
                                    Single axisCenter = axisMin + (axisLength / 2F);
                                    RectValue groupBounds = placementGroups.First().GetBounds();
                                    placementGroups.First().SetGroupY(axisCenter - (groupBounds.Y + groupBounds.Height / 2F));
                                }
                                else
                                {
                                    // Underfilled
                                    PlaceEvenWhenUnderfilledAlongAxis(axis, placementGroups, axisLength, totalPlacedLength);
                                }
                            }
                        }
                    }
                    break;


                #endregion

                #region Manual

                case PlanogramPlacementStrategy.Manual:
                    {
                        IEnumerable<PlanogramPositionPlacement> posList = placementGroups.SelectMany(p => p.Placements);

                        //enforce min and max
                        if (!axis.CanBreakMin ||
                            !axis.CanBreakMax)
                        {
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                PlanogramPositionDetails positionDetails = p.GetPositionDetails();
                                if (!axis.CanBreakMax &&
                                    ((p.Position.Y + positionDetails.TotalSpace.Height).GreaterThan(axisMax)))
                                {
                                    p.Position.Y = axisMax - positionDetails.TotalSpace.Height;
                                }

                                if (!axis.CanBreakMin &&
                                    (p.Position.Y.LessThan(axisMin)))
                                {
                                    p.Position.Y = axisMin;
                                }
                            }
                        }
                        else if (this.HasRow1PegHoles ||
                                 this.HasRow2PegHoles)
                        {
                            //force positions to snap to the nearest peghole
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                p.SnapPositionToPegHole();
                            }
                        }

                        //correct collisions if overlapping is not allowed
                        if (!this.IsProductOverlapAllowed)
                        {
                            List<PlanogramPositionPlacement> placedPositions = new List<PlanogramPositionPlacement>();

                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                foreach (PlanogramPositionPlacement other in placedPositions)
                                {
                                    if (p.CollidesWith(other))
                                    {
                                        p.Position.Y = other.Position.Y + other.GetPositionDetails().TotalSpace.Height;
                                    }
                                }

                                placedPositions.Add(p);
                            }

                        }
                    }
                    break;

                    #endregion

            }
        }

        /// <summary>
        /// Places groups at min y without any dividers.
        /// </summary>
        private static Single PlaceAtMinY(Single axisMin, IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            Single nextN = axisMin;

            foreach (var posGroup in placementGroups)
            {
                foreach (PlanogramPositionPlacement p in posGroup.Placements)
                {
                    // Ensure that the coordinate is snapped to pegholes.
                    if (p.Position.Y.EqualTo(nextN))
                    {
                        p.SnapPositionToPegHole();
                    }
                    else
                    {
                        p.Position.Y = nextN;
                    }
                }
                nextN = posGroup.Placements.Min(p => p.Position.Y) +
                        posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Height);
            }

            return nextN - axisMin;
        }

        /// <summary>
        /// Places groups at max y without any dividers
        /// </summary>
        private static Single PlaceAtMaxY(Single axisMax, IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            List<PlanogramPositionPlacementGroup> orderedPlacementGroups = placementGroups.ToList();

            //we are placing at max so reverse the group order.
            orderedPlacementGroups.Reverse();

            Single nextEndN = axisMax;

            //cycle through stacking max to min
            foreach (var posGroup in orderedPlacementGroups)
            {
                foreach (PlanogramPositionPlacement p in posGroup.Placements)
                {
                    // Ensure that the coordinate is snapped to pegholes.
                    Single newN = nextEndN - p.GetPositionDetails().TotalSpace.Height;
                    if (p.Position.Y.EqualTo(newN))
                    {
                        p.SnapPositionToPegHole();
                    }
                    else
                    {
                        p.Position.Y = newN;
                    }
                }
                nextEndN = posGroup.Placements.Min(p => p.Position.Y);
            }

            return axisMax - nextEndN;
        }

        #endregion

        #region Y Placement - with dividers

        /// <summary>
        /// Organises the given placement groups along the y axis with dividers according to the axis strategy.
        /// </summary>
        private List<DividerPlacementValue> PlaceGroupsAlongYWithDividers(
            PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            List<DividerPlacementValue> dividerValues = new List<DividerPlacementValue>();

            //Adjust the axis min and max
            Single axisMin = axis.Min;

            Single axisMax = axis.Max;
            if ((axisMin.EqualTo(axisMax)) &&
                this.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
            {
                //if the min and max are the same and we are stacking then no merch height was specified.
                // so take the max as the max sum size y accross the groups
                axisMax = axisMin +
                          placementGroups.Sum(g => g.Placements.Max(p => p.GetPositionDetails().TotalSpace.Height));
            }


            switch (axis.Strategy)
            {
                #region Min

                case PlanogramPlacementStrategy.Min:
                    {
                        Single placedLength =
                            PlaceAtMinYWithDividers(axisMin,
                                placementGroups,
                                axis.DividerStart,
                                axis.DividerSpacing,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);


                        //Apply a partial squeeze if required:
                        if (this.SqueezeTypeY == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            //correct the axis max before trying to squeeze
                            if (this.PlaceEndDivider)
                            {
                                //if the axis max would cause the last divider to go over
                                // then we need to adjust the max down the the last possible slot.
                                axisMax = MathHelper.RoundToNearestIncrement(axisMax, axis.DividerSpacing);
                                if (axisMax.GreaterOrEqualThan(axis.Max)) axisMax = axisMax - axis.DividerSpacing + this.DividerWidth;
                            }

                            Single axisLength = axisMax - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                            {
                                //Single placedLength = GetPlacedHeight(placementGroups, dividerValues);
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductHeight(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMinYWithDividers(axisMin,
                                            placementGroups,
                                            axis.DividerStart,
                                            axis.DividerSpacing,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref placedLength,
                                    out squeezedDividerValues);
                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;

                            }
                        }


                        //if placements break max but flag says we can't
                        // and we are allowed to break min instead then replace
                        if (!axis.CanBreakMax && axis.CanBreakMin)
                        {
                            Single maxY = placementGroups.Max(o => o.Placements.Max(p => p.Position.Y));
                            if (dividerValues.Count > 0) maxY = Math.Max(maxY, dividerValues.Select(d => d.Value).Max());

                            if (maxY.GreaterThan(axisMax))
                            {
                                //replace at max
                                PlaceAtMaxYWithDividers(axisMax,
                                    placementGroups,
                                    axis.DividerStart,
                                    axis.DividerSpacing,
                                    this.DividerWidth,
                                    this.PlaceStartDivider,
                                    this.PlaceEndDivider,
                                    out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Max

                case PlanogramPlacementStrategy.Max:
                    {
                        Single placedLength =
                            PlaceAtMaxYWithDividers(axisMax,
                                placementGroups,
                                axis.DividerStart,
                                axis.DividerSpacing,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);


                        //Apply a partial squeeze if required:
                        if (this.SqueezeTypeY == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axisMax - axisMin;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                            {
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductHeight(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMaxYWithDividers(axisMax,
                                            placementGroups,
                                            axis.DividerStart,
                                            axis.DividerSpacing,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref placedLength,
                                    out squeezedDividerValues);
                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                            }
                        }


                        //if placements break min but flag says we can't
                        // and we are allowed to break max instead then replace
                        if (!axis.CanBreakMin && axis.CanBreakMax)
                        {
                            Single minY = placementGroups.Min(o => o.Placements.Min(p => p.Position.Y));
                            if (dividerValues.Count > 0) minY = Math.Min(minY, dividerValues.Select(d => d.Value).Min());

                            if (minY.LessThan(axisMin))
                            {
                                //replace at min.
                                PlaceAtMinYWithDividers(axisMin,
                                    placementGroups,
                                    axis.DividerStart,
                                    axis.DividerSpacing,
                                    this.DividerWidth,
                                    this.PlaceStartDivider,
                                    this.PlaceEndDivider,
                                    out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Even

                case PlanogramPlacementStrategy.Even:
                    {
                        //start by placing as min so that we can see how much space this occupies.
                        Single totalPlacedHeight =
                            PlaceAtMinYWithDividers(axisMin,
                                placementGroups,
                                axis.DividerStart,
                                axis.DividerSpacing,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);


                        //Check if we are over or under fitted:
                        Single axisLength = axisMax - axisMin;
                        if (axisLength.GreaterThan(0))
                        {
                            // Try to apply a partial squeeze to products
                            if (this.SqueezeTypeY == PlanogramSubComponentSqueezeType.PartialSqueeze
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                            {
                                //Try to squeeze the products to fit them on.
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductHeight(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMinYWithDividers(axisMin,
                                            placementGroups,
                                            axis.DividerStart,
                                            axis.DividerSpacing,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref totalPlacedHeight,
                                    out squeezedDividerValues);
                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                            }


                            //If we are still overfitted:
                            if (totalPlacedHeight.GreaterThan(axisLength))
                            {
                                #region Overfilled

                                //  Overfilled but cannot break min:
                                if (!axis.CanBreakMin &&
                                    axis.CanBreakMax)
                                {
                                    //do nothing - we have already placed at min.
                                }

                                //Overfilled but cannot break max:
                                else if (axis.CanBreakMin && !axis.CanBreakMax)
                                {
                                    //place at max instead
                                    PlaceAtMaxYWithDividers(axisMax,
                                        placementGroups,
                                        axis.DividerStart,
                                        axis.DividerSpacing,
                                        this.DividerWidth,
                                        this.PlaceStartDivider,
                                        this.PlaceEndDivider,
                                        out dividerValues);
                                }


                                // Overfilled - with dividers:
                                else
                                {
                                    Single placementOffset = -((totalPlacedHeight - axisLength) / 2F);

                                    //snap to slot
                                    Single newStart = axisMin + placementOffset;
                                    newStart =
                                        Convert.ToSingle(
                                            Math.Round(newStart / axis.DividerSpacing, MidpointRounding.AwayFromZero) *
                                            axis.DividerSpacing);
                                    placementOffset = newStart + axisMin;

                                    foreach (var pGroup in placementGroups)
                                    {
                                        pGroup.SetGroupX(pGroup.GetBounds().X + placementOffset);
                                    }
                                    foreach (var div in dividerValues)
                                    {
                                        div.Value += placementOffset;
                                    }
                                }

                                #endregion
                            }

                            //else if we are underfitted:
                            else if (totalPlacedHeight.LessThan(axisLength))
                            {
                                Single merchCenter = axisMin + (axisLength / 2F);

                                #region Underfilled

                                if (placementGroups.Count() == 1)
                                {
                                    //Underfilled - 1 group
                                    RectValue groupBounds = placementGroups.First().GetBounds();
                                    placementGroups.First().SetGroupY(merchCenter - (groupBounds.Y + groupBounds.Height / 2F));
                                }
                                else
                                {
                                    //Underfilled - with dividers
                                    PlaceEvenStrategyUnderfilledWithNormalDividers(
                                        axis, axisLength, placementGroups, ref dividerValues);
                                }

                                #endregion
                            }
                        }
                    }
                    break;


                #endregion

                #region Manual

                case PlanogramPlacementStrategy.Manual:
                    {
                        IEnumerable<PlanogramPositionPlacement> posList = placementGroups.SelectMany(p => p.Placements);

                        //enforce min and max
                        if (!axis.CanBreakMin ||
                            !axis.CanBreakMax)
                        {
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                PlanogramPositionDetails positionDetails = p.GetPositionDetails();
                                if (!axis.CanBreakMax &&
                                    ((p.Position.Y + positionDetails.TotalSpace.Height).GreaterThan(axisMax)))
                                {
                                    p.Position.Y = axisMax - positionDetails.TotalSpace.Height;
                                }

                                if (!axis.CanBreakMin &&
                                    (p.Position.Y.LessThan(axisMin)))
                                {
                                    p.Position.Y = axisMin;
                                }
                            }
                        }
                        else if (this.HasRow1PegHoles ||
                                 this.HasRow2PegHoles)
                        {
                            //force positions to snap to the nearest peghole
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                p.SnapPositionToPegHole();
                            }
                        }

                        //correct collisions if overlapping is not allowed
                        if (!this.IsProductOverlapAllowed)
                        {
                            List<PlanogramPositionPlacement> placedPositions = new List<PlanogramPositionPlacement>();

                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                foreach (PlanogramPositionPlacement other in placedPositions)
                                {
                                    if (p.CollidesWith(other))
                                    {
                                        p.Position.Y = other.Position.Y + other.GetPositionDetails().TotalSpace.Height;
                                    }
                                }

                                placedPositions.Add(p);
                            }

                        }

                        //place dividers as required
                        Single dividerWidth = this.DividerWidth;
                        foreach (PlanogramPositionPlacement p in posList.OrderBy(p => p.Position.Y))
                        {
                            PlanogramPositionDetails positionDetails = p.GetPositionDetails();

                            Single placementN = p.Position.Y;
                            if (placementN.GreaterOrEqualThan(axis.DividerStart))
                            {
                                Boolean isFirst = posList.First() == p;
                                Boolean isLast = posList.Last() == p;

                                if (isFirst)
                                {
                                    if (this.PlaceStartDivider)
                                    {
                                        Single mod = (placementN - axis.DividerStart) % axis.DividerSpacing;

                                        Single firstDiv = placementN - mod;
                                        if (firstDiv.GreaterThan(axis.DividerStart))
                                        {
                                            dividerValues.Add(new DividerPlacementValue(placementN - mod, dividerWidth)
                                            {
                                                IsStartDivider = true
                                            });
                                        }
                                        else
                                        {
                                            dividerValues.Add(new DividerPlacementValue(axis.DividerStart, dividerWidth)
                                            {
                                                IsStartDivider = true
                                            });
                                        }
                                    }
                                }
                                if (!isLast ||
                                    (isLast && this.PlaceEndDivider))
                                {
                                    Single mod = (placementN + positionDetails.TotalSpace.Height - axis.DividerStart) %
                                                 axis.DividerSpacing;

                                    Single afterDiv = placementN + positionDetails.TotalSpace.Height +
                                                      (axis.DividerSpacing - mod);
                                    dividerValues.Add(new DividerPlacementValue(afterDiv, dividerWidth));
                                }

                                isFirst = false;
                            }


                        }
                    }
                    break;

                    #endregion
            }

            return dividerValues;
        }

        /// <summary>
        /// Places groups at min along the y axis with dividers between.
        /// </summary>
        private static Single PlaceAtMinYWithDividers(
            Single axisMin,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single dividerStart,
            Single dividerSpacing,
            Single dividerThick,
            Boolean placeStartDivider,
            Boolean placeEndDivider,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();

            Single nextN = axisMin;
            Single divStart = axisMin + dividerStart;
            Single nextDivY = axisMin + dividerStart;

            Boolean isFirstDiv = true;

            //cycle through stacking min to max
            foreach (var posGroup in placementGroups)
            {
                //if under the divider start point
                //or if the first div is being placed and would intersect
                if (nextN.GreaterOrEqualThan(divStart)
                    ||
                    (isFirstDiv && placeStartDivider &&
                     (nextN + posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Height)).GreaterOrEqualThan(divStart)))
                {
                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = dividerSpacing;
                    posGroup.DividerSlotReservedX = dividerThick;


                    if (isFirstDiv)
                    {
                        //place the start divider
                        if (placeStartDivider
                            || (!placeStartDivider && dividerStart.GreaterThan(0)))
                        {
                            dividerValues.Add(new DividerPlacementValue(nextDivY, dividerThick)
                            {
                                IsStartDivider = placeStartDivider
                            });
                            nextN = nextDivY + dividerThick;
                        }
                        isFirstDiv = false;
                    }
                    else
                    {
                        dividerValues.Add(new DividerPlacementValue(nextDivY, dividerThick));
                        nextN = nextDivY + dividerThick;
                    }

                    //butt each position to the end X
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Y = nextN;
                    }

                    //set the next div pos
                    Single maxPosN =
                        posGroup.Placements.Max(p => p.Position.Y + p.GetPositionDetails().TotalSpace.Height);
                    while (nextDivY.LessThan(maxPosN))
                    {
                        nextDivY += dividerSpacing;
                    }
                }
                else
                {
                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = 0;
                    posGroup.DividerSlotReservedX = 0;

                    //place normally without dividers
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Y = nextN;
                    }
                    nextN = posGroup.Placements.Min(p => p.Position.X) +
                            posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Height);

                    //update the next div
                    while (nextDivY.LessThan(nextN))
                    {
                        nextDivY += dividerSpacing;
                    }
                }
            }

            //place the end divider
            if (placeEndDivider)
            {
                dividerValues.Add(new DividerPlacementValue(nextDivY, dividerThick) { IsEndDivider = true });
            }

            Single divMax = (dividerValues.Any()) ? dividerValues.Max(d => d.Value + dividerThick) : Single.NegativeInfinity;
            return Math.Max(nextN, divMax) - axisMin;
        }

        /// <summary>
        /// Places groups at max along the y axis with dividers between
        /// </summary>
        private static Single PlaceAtMaxYWithDividers(
            Single axisMax,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single dividerStart,
            Single dividerSpacing,
            Single dividerThick,
            Boolean placeStartDivider,
            Boolean placeEndDivider,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();

            IEnumerable<PlanogramPositionPlacementGroup> orderedPlacementGroups = placementGroups;

            //placing at max so reverse the group order.
            orderedPlacementGroups = placementGroups.Reverse();

            Single nextEndN = axisMax;
            Single divStart = axisMax - dividerStart;
            Single nextDivY = axisMax - dividerStart - dividerThick;

            Boolean isFirstDiv = true;

            //cycle through stacking max to min
            foreach (var posGroup in orderedPlacementGroups)
            {
                //set the group slot constraint
                posGroup.DividerSlotSizeX = dividerSpacing;
                posGroup.DividerSlotReservedX = dividerThick;

                //if under the divider start point
                //or if the first div is being placed and would intersect
                if (nextEndN.LessOrEqualThan(divStart)
                    ||
                    (isFirstDiv && placeStartDivider &&
                     (nextEndN - posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Height)).LessOrEqualThan(divStart)))
                {
                    if (isFirstDiv)
                    {
                        //place the start divider
                        if (placeStartDivider
                            || (!placeStartDivider && dividerStart.GreaterThan(0)))
                        {
                            dividerValues.Add(new DividerPlacementValue(nextDivY, dividerThick)
                            {
                                IsStartDivider = placeStartDivider
                            });
                            nextEndN = nextDivY;
                        }
                        isFirstDiv = false;
                    }
                    else
                    {
                        dividerValues.Add(new DividerPlacementValue(nextDivY, dividerThick));
                        nextEndN = nextDivY;
                    }

                    //butt each position to the end Y
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Y = nextEndN - p.GetPositionDetails().TotalSpace.Height;
                    }

                    //set the next div pos
                    Single minPosN = posGroup.Placements.Min(p => p.Position.Y);
                    while ((nextDivY + dividerThick).GreaterThan(minPosN))
                    {
                        nextDivY -= dividerSpacing;
                    }
                }
                else
                {
                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = 0;
                    posGroup.DividerSlotReservedX = 0;

                    //place normally without dividers
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Y = nextEndN - p.GetPositionDetails().TotalSpace.Height;
                    }
                    nextEndN = posGroup.Placements.Min(p => p.Position.Y);

                    //update the next div
                    while ((nextDivY + dividerThick).GreaterThan(nextEndN))
                    {
                        nextDivY -= dividerSpacing;
                    }
                }
            }

            //place the end divider
            if (placeEndDivider)
            {
                dividerValues.Add(new DividerPlacementValue(nextDivY, dividerThick) { IsEndDivider = true });
            }

            Single divMin = (dividerValues.Any()) ? dividerValues.Min(d => d.Value) : Single.PositiveInfinity;
            return axisMax - Math.Min(nextEndN, divMin);
        }

        #endregion

        #region Y Squeeze

        /// <summary>
        /// Squeezes the placement groups to fit the given merch area height.
        /// </summary>
        private Boolean SqueezeProductHeight(IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single merchAreaHeight,
            PlaceGroupsDelegate placeGroupsAction,
            ref Single currentHeight)
        {
            List<DividerPlacementValue> dividerValues;
            return SqueezeProductHeight(placementGroups, merchAreaHeight, placeGroupsAction, ref currentHeight,
                out dividerValues);
        }

        /// <summary>
        /// Squeezes the placement groups to fit the given merch area height.
        /// </summary>
        private Boolean SqueezeProductHeight(
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single merchAreaHeight,
            PlaceGroupsDelegate placeGroupsAction,
            ref Single currentHeight,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = null;

            //+ Check if we can squeeze by width:

            //if the current width is over the merch area width already return out.
            if (currentHeight.LessThan(merchAreaHeight)) return false;

            //determine if any groups can possibly be squeezed.
            List<PlanogramPositionPlacementGroup> squeezeableGroups =
                placementGroups.Where(p => p.GetFullySqueezedBounds().Height.LessThan(p.GetBounds().Height)).ToList();

            if (!squeezeableGroups.Any()) return false;


            //work out how much we need to squeeze by
            Single squeezePerGroup = (currentHeight - merchAreaHeight) / (Single)squeezeableGroups.Count();


            //+ Apply squeeze first to groups that would hit their minimum.
            Boolean applyFullSqueeze = true;
            while (applyFullSqueeze)
            {
                applyFullSqueeze = false;
                foreach (PlanogramPositionPlacementGroup group in squeezeableGroups.ToList())
                {
                    Single groupHeight = group.GetBounds().Height;
                    Single targetHeight = groupHeight - squeezePerGroup;
                    if (targetHeight.LessThan(group.GetFullySqueezedBounds().Height))
                    {
                        group.ApplyFullVerticalSqueeze();
                        squeezeableGroups.Remove(group);
                        applyFullSqueeze = true;
                    }
                }


                if (applyFullSqueeze)
                {
                    //something changed so replace
                    currentHeight = placeGroupsAction(placementGroups, out dividerValues);

                    //recalc amount to squeeze remaining by
                    squeezePerGroup = (currentHeight - merchAreaHeight) / (Single)squeezeableGroups.Count();
                }
            }

            //now apply the squeeze accross anything left over.
            List<PlanogramPositionPlacementGroup> failedGroups = new List<PlanogramPositionPlacementGroup>();
            foreach (PlanogramPositionPlacementGroup group in squeezeableGroups.ToList())
            {
                squeezeableGroups.Remove(group);

                Single targetHeight = group.GetBounds().Height - squeezePerGroup;
                if (!group.SqueezeHeightTo(targetHeight))
                {
                    failedGroups.Add(group);

                    //recalc the squeeze per group
                    currentHeight = placeGroupsAction(placementGroups, out dividerValues);

                    squeezePerGroup = (currentHeight - merchAreaHeight) / (Single)squeezeableGroups.Count();
                }
            }


            //if any of the groups failed to meet their targets
            // and we are still over the target
            // - go through squeezing them to their minimum
            if (failedGroups.Any())
            {
                currentHeight = placeGroupsAction(placementGroups, out dividerValues);

                if (currentHeight.GreaterThan(merchAreaHeight))
                {
                    foreach (PlanogramPositionPlacementGroup group in failedGroups)
                    {
                        group.ApplyFullVerticalSqueeze();

                        currentHeight = placeGroupsAction(placementGroups, out dividerValues);

                        if (currentHeight.LessOrEqualThan(merchAreaHeight)) return true;
                    }

                    return false;
                }
            }


            //replace and return.
            currentHeight = placeGroupsAction(placementGroups, out dividerValues);
            return true;
        }

        ///// <summary>
        ///// Returns the overall placed height of the given groups and dividers.
        ///// </summary>
        //private Single GetPlacedHeight(IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
        //    List<DividerPlacementValue> dividerValues = null)
        //{
        //    Single minY = Single.PositiveInfinity;
        //    Single maxY = Single.NegativeInfinity;

        //    foreach (PlanogramPositionPlacementGroup group in placementGroups)
        //    {
        //        RectValue groupBounds = group.GetBounds();
        //        minY = Math.Min(minY, groupBounds.Y);
        //        maxY = Math.Max(maxY, groupBounds.Y + groupBounds.Height);
        //    }

        //    // Adjust the max y coordinate to take into account any overlaps within the placement group. This method
        //    // will return 0 if overlaps are allowed. This max y co-ordinate represents the virtual space occupied by the
        //    // furthest right position, if there were no overlaps in the placement groups (not its actual position).
        //    maxY += GetOverlappingSize(AxisType.Y, placementGroups);

        //    if (dividerValues != null)
        //    {
        //        foreach (DividerPlacementValue divider in dividerValues)
        //        {
        //            minY = Math.Min(minY, divider.Value);
        //            maxY = Math.Max(maxY, divider.Value + divider.Thickness);
        //        }
        //    }

        //    if (maxY > minY) return maxY - minY;
        //    else return 0;
        //}

        #endregion

        #endregion

        #region Z Axis Methods

        /// <summary>
        /// Places the provided positions along
        /// the z axis, returning divider details
        /// </summary>
        private IEnumerable<PlanogramDividerPlacement> ProcessZAxis(
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            PlanogramMerchandisingGroupAxis axis = this.GetAxis(AxisType.Z);
            Boolean isNormalDividers = AreDividersPlacedNormally(axis);

            List<DividerPlacementValue> dividerValues = null;

            //Apply full depth squeeze if required:
            if (this.SqueezeTypeZ == PlanogramSubComponentSqueezeType.FullSqueeze)
            {
                foreach (PlanogramPositionPlacementGroup group in placementGroups)
                {
                    group.ApplyFullDepthSqueeze();
                }
            }

            if (isNormalDividers)
            {
                dividerValues = PlaceGroupsAlongZWithDividers(axis, placementGroups);
            }
            else
            {
                //Place the groups without dividers
                PlaceGroupsAlongZ(axis, placementGroups);
            }

            //create output dividers
            List<PlanogramDividerPlacement> dividers = new List<PlanogramDividerPlacement>();
            if (dividerValues != null &&
                dividerValues.Count > 0)
            {
                foreach (Single n in dividerValues.Select(d => d.Value).Distinct())
                {
                    PlanogramDividerPlacement div = PlanogramDividerPlacement.NewPlanogramDividerPlacement();

                    div.Height = Math.Max(1, this.DividerHeight);
                    div.Depth = this.DividerWidth;
                    div.Y = this.MinY;
                    div.Z = n;

                    PlanogramMerchandisingGroupAxis xAxis = GetAxis(AxisType.X);

                    if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                    {
                        //place dividers with x groups.
                        div.X = placementGroups.Min(g => g.Placements.Min(p => p.Position.X));
                        div.Width =
                            placementGroups.Max(
                                g => g.Placements.Max(p => p.Position.X + p.GetPositionDetails().TotalSpace.Width)) -
                            div.X;
                        if (isNormalDividers && AreDividersPlacedNormally(xAxis))
                        {
                            Single dWidth = div.Width;
                            div.Width = MathHelper.RoundToNearestIncrement(div.Width, xAxis.DividerSpacing);
                            if (div.Width.LessOrEqualThan(dWidth)) div.Width += xAxis.DividerSpacing;
                        }
                    }
                    else
                    {
                        div.Width = this.MaxX - this.MinX;
                        div.X = this.MinX;
                    }



                    dividers.Add(div);
                }

                this.DividerPlacements.AddRange(dividers);
            }

            return dividers;
        }

        #region Z Placement - normal

        /// <summary>
        /// Organises groups along the z axis without dividers according to the axis strategy.
        /// </summary>
        private void PlaceGroupsAlongZ(PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            switch (axis.Strategy)
            {
                #region Min

                case PlanogramPlacementStrategy.Min:
                    {
                        Single placedLength = PlaceAtMinZ(axis.Min, placementGroups);

                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeZ == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Z, g.Placements)))
                            {
                                //Squeeze the groups widths.
                                SqueezeProductDepth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMinZ(axis.Min, placementGroups);
                                    }),
                                    ref placedLength);
                            }
                        }


                        //if placements break max but flag says we can't
                        // and we are allowed to break min instead then replace
                        if (!axis.CanBreakMax && axis.CanBreakMin)
                        {
                            Single maxZ = placementGroups.Max(o => o.Placements.Max(p => p.Position.Z));
                            if (maxZ.GreaterThan(axis.Max))
                            {
                                //replace at max
                                PlaceAtMaxZ(axis.Min, axis.Max, this.MerchandisingType, placementGroups);
                            }
                        }
                    }
                    break;

                #endregion

                #region Max

                case PlanogramPlacementStrategy.Max:
                    {
                        Single placedLength = PlaceAtMaxZ(axis.Min, axis.Max, this.MerchandisingType, placementGroups);

                        //Apply a partial squeeze to products if required:
                        if (this.SqueezeTypeZ == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Z, g.Placements)))
                            {
                                //squeeze the products to fit them on.
                                SqueezeProductDepth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMaxZ(axis.Min, axis.Max, this.MerchandisingType, g);
                                    }),
                                    ref placedLength);
                            }
                        }


                        //if placements break min but flag says we can't
                        // and we are allowed to break max instead then replace
                        if (!axis.CanBreakMin && axis.CanBreakMax)
                        {
                            Single minZ = placementGroups.Min(o => o.Placements.Min(p => p.Position.Z));
                            if (minZ.LessThan(axis.Min))
                            {
                                //replace at min.
                                PlaceAtMinZ(axis.Min, placementGroups);
                            }
                        }
                    }
                    break;

                #endregion

                #region Even

                case PlanogramPlacementStrategy.Even:
                    {
                        //TODO: Can break min max checks

                        //Start by placing the groups as left stacked so that we 
                        //can get the min width the entire placement will need.
                        Single totalPlacedLength = PlaceAtMinZ(axis.Min, placementGroups);

                        Single axisLength = axis.Max - axis.Min;
                        if (axisLength == 0) break;

                        //Single totalPlacedLength = GetPlacedDepth(placementGroups);

                        //If we are overfitted,
                        // Try to apply a partial squeeze to products
                        if (this.SqueezeTypeZ == PlanogramSubComponentSqueezeType.PartialSqueeze
                            && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Z, g.Placements)))
                        {
                            //Try to squeeze the products to fit them on.
                            SqueezeProductDepth(placementGroups,
                                axisLength,
                                new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        divs = null;
                                        return PlaceAtMinZ(axis.Min, g);
                                    }),
                                    ref totalPlacedLength);
                        }


                        //If we are still overfitted:
                        if (totalPlacedLength.GreaterThan(axisLength))
                        {
                            //  Overfilled but cannot break min:
                            if (!axis.CanBreakMin && axis.CanBreakMax)
                            {
                                //do nothing - we have already placed at min.
                            }

                            //Overfilled but cannot break max:
                            else if (axis.CanBreakMin && !axis.CanBreakMax)
                            {
                                //place at max instead
                                PlaceAtMaxZ(axis.Min, axis.Max, this.MerchandisingType, placementGroups);
                            }
                            else
                            {
                                //placed width is over the merch area width so need
                                // to overhang equally over both sides
                                Single placementOffset = -((totalPlacedLength - axisLength) / 2F);
                                foreach (var pGroup in placementGroups)
                                {
                                    pGroup.SetGroupZ(pGroup.GetBounds().Z + placementOffset);
                                }
                            }
                        }

                        //else if we are underfitted:
                        else if (totalPlacedLength.LessThan(axisLength))
                        {
                            if (placementGroups.Count() == 1)
                            {
                                Single axisCenter = axis.Min + (axisLength / 2F);
                                foreach (var p in placementGroups.SelectMany(p => p.Placements))
                                {
                                    p.Position.Z = axisCenter -
                                                   (p.Position.Z + p.GetPositionDetails().TotalSize.Depth / 2F);
                                }
                            }
                            else
                            {
                                //UNDER-FILLED
                                if (placementGroups.Count() == 1)
                                {
                                    //only one group so just place it centrally.
                                    placementGroups.First().SetGroupZ((axisLength - totalPlacedLength) / 2F);
                                }
                                else
                                {
                                    PlaceEvenWhenUnderfilledAlongAxis(axis, placementGroups, axisLength,
                                        totalPlacedLength);
                                }
                            }
                        }

                    }
                    break;

                #endregion

                #region Manual

                case PlanogramPlacementStrategy.Manual:
                    {
                        IEnumerable<PlanogramPositionPlacement> posList = placementGroups.SelectMany(p => p.Placements);

                        //enforce min and max
                        if (!axis.CanBreakMin ||
                            !axis.CanBreakMax)
                        {
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                PlanogramPositionDetails positionDetails = p.GetPositionDetails();
                                if (!axis.CanBreakMax &&
                                    ((p.Position.Z + positionDetails.TotalSpace.Depth).GreaterThan(axis.Max)))
                                {
                                    p.Position.Z = axis.Max - positionDetails.TotalSpace.Depth;
                                }

                                if (!axis.CanBreakMin &&
                                    (p.Position.Z.LessThan(axis.Min)))
                                {
                                    p.Position.Z = axis.Min;
                                }
                            }
                        }
                        else if (this.HasRow1PegHoles ||
                                 this.HasRow2PegHoles)
                        {
                            //force positions to snap to the nearest peghole
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                p.SnapPositionToPegHole();
                            }
                        }

                        //correct collisions if overlaps are not allowed
                        if (!this.IsProductOverlapAllowed)
                        {
                            List<PlanogramPositionPlacement> placedPositions = new List<PlanogramPositionPlacement>();

                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                foreach (PlanogramPositionPlacement other in placedPositions)
                                {
                                    if (p.CollidesWith(other))
                                    {
                                        p.Position.Z = other.Position.Z + other.GetPositionDetails().TotalSpace.Depth;
                                    }
                                }

                                placedPositions.Add(p);
                            }
                        }

                    }
                    break;

                    #endregion
            }
        }

        /// <summary>
        /// Places groups at min z.
        /// </summary>
        private static Single PlaceAtMinZ(Single axisMin, IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            Single nextN = axisMin;

            foreach (var posGroup in placementGroups)
            {
                foreach (PlanogramPositionPlacement p in posGroup.Placements)
                {
                    // Ensure that the coordinate is snapped to pegholes.
                    if (p.Position.Z.EqualTo(nextN))
                    {
                        p.SnapPositionToPegHole();
                    }
                    else
                    {
                        p.Position.Z = nextN;
                    }
                }
                nextN = posGroup.Placements.Min(p => p.Position.Z) +
                        posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Depth);
            }

            return nextN - axisMin;
        }

        /// <summary>
        /// Places groups at max z.
        /// </summary>
        private static Single PlaceAtMaxZ(Single axisMin,
            Single axisMax,
            PlanogramSubComponentMerchandisingType groupMerchType,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            IEnumerable<PlanogramPositionPlacementGroup> orderedPlacementGroups = placementGroups;
            orderedPlacementGroups = placementGroups.Reverse();

            Single nextEndN = axisMax;

            //if the products are being hanged then consider the peg depth.
            if (groupMerchType == PlanogramSubComponentMerchandisingType.Hang)
            {
                Single pegDepth = orderedPlacementGroups.Max(g => g.Placements.Max(p => p.Product.PegDepth));
                if (pegDepth.GreaterThan(0))
                {
                    nextEndN = axisMin + pegDepth;
                }
            }

            //cycle through stacking max to min
            foreach (var posGroup in orderedPlacementGroups)
            {
                foreach (PlanogramPositionPlacement p in posGroup.Placements)
                {
                    Single newN = nextEndN - p.GetPositionDetails().TotalSpace.Depth;
                    // Ensure that the coordinate is snapped to pegholes.
                    if (p.Position.Z.EqualTo(newN))
                    {
                        p.SnapPositionToPegHole();
                    }
                    else
                    {
                        p.Position.Z = newN;
                    }
                    p.Position.Z = newN;
                }
                nextEndN = posGroup.Placements.Min(p => p.Position.Z);
            }

            return axisMax - nextEndN;
        }

        #endregion

        #region Z Placement - with dividers

        /// <summary>
        /// Places groups along the Z axis with dividers between positions.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placementGroups"></param>
        /// <returns></returns>
        private List<DividerPlacementValue> PlaceGroupsAlongZWithDividers(
            PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups)
        {
            List<DividerPlacementValue> dividerValues = new List<DividerPlacementValue>();

            switch (axis.Strategy)
            {
                #region Min

                case PlanogramPlacementStrategy.Min:
                    {
                        Single placedLength =
                            PlaceAtMinZWithDividers(axis.Min,
                                placementGroups,
                                axis.DividerStart,
                                axis.DividerSpacing,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);

                        //Apply a partial squeeze if required:
                        if (this.SqueezeTypeZ == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            //correct the axis max before trying to squeeze
                            Single axisMax = axis.Max;
                            if (this.PlaceEndDivider)
                            {
                                //if the axis max would cause the last divider to go over
                                // then we need to adjust the max down the the last possible slot.
                                axisMax = MathHelper.RoundToNearestIncrement(axisMax, axis.DividerSpacing);
                                if (axisMax.GreaterOrEqualThan(axis.Max)) axisMax = axisMax - axis.DividerSpacing + this.DividerWidth;
                            }

                            Single axisLength = axisMax - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                            {
                                //Single placedLength = GetPlacedDepth(placementGroups, dividerValues);

                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductDepth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMinZWithDividers(axis.Min,
                                            placementGroups,
                                            axis.DividerStart,
                                            axis.DividerSpacing,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref placedLength,
                                    out squeezedDividerValues);

                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;

                            }
                        }


                        //if placements break max but flag says we can't
                        // and we are allowed to break min instead then replace
                        if (!axis.CanBreakMax && axis.CanBreakMin)
                        {
                            Single maxZ = placementGroups.Max(o => o.Placements.Max(p => p.Position.Z));
                            if (dividerValues.Count > 0) maxZ = Math.Max(maxZ, dividerValues.Select(d => d.Value).Max());

                            if (maxZ.GreaterThan(axis.Max))
                            {
                                //replace at max
                                PlaceAtMaxZWithDividers(axis.Min,
                                    axis.Max,
                                    this.MerchandisingType,
                                    placementGroups,
                                    axis.DividerStart,
                                    axis.DividerSpacing,
                                    this.DividerWidth,
                                    this.PlaceStartDivider,
                                    this.PlaceEndDivider,
                                    out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Max

                case PlanogramPlacementStrategy.Max:
                    {
                        Single placedLength =
                            PlaceAtMaxZWithDividers(axis.Min,
                                axis.Max,
                                this.MerchandisingType,
                                placementGroups,
                                axis.DividerStart,
                                axis.DividerSpacing,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);


                        //Apply a partial squeeze if required:
                        if (this.SqueezeTypeZ == PlanogramSubComponentSqueezeType.PartialSqueeze)
                        {
                            Single axisLength = axis.Max - axis.Min;
                            if (axisLength.GreaterThan(0)
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                            {
                                //Single placedLength = GetPlacedDepth(placementGroups, dividerValues);
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductDepth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMaxZWithDividers(axis.Min,
                                            axis.Max,
                                            this.MerchandisingType,
                                            placementGroups,
                                            axis.DividerStart,
                                            axis.DividerSpacing,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref placedLength,
                                    out squeezedDividerValues);

                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                            }
                        }



                        //if placements break min but flag says we can't
                        // and we are allowed to break max instead then replace
                        if (!axis.CanBreakMin && axis.CanBreakMax)
                        {
                            Single minZ = placementGroups.Min(o => o.Placements.Min(p => p.Position.Z));
                            if (dividerValues.Count > 0) minZ = Math.Min(minZ, dividerValues.Select(d => d.Value).Min());

                            if (minZ.LessThan(axis.Min))
                            {
                                //replace at min.
                                PlaceAtMinZWithDividers(axis.Min,
                                    placementGroups,
                                    axis.DividerStart,
                                    axis.DividerSpacing,
                                    this.DividerWidth,
                                    this.PlaceStartDivider,
                                    this.PlaceEndDivider,
                                    out dividerValues);
                            }
                        }
                    }
                    break;

                #endregion

                #region Even

                case PlanogramPlacementStrategy.Even:
                    {
                        //Start by placing the groups as left stacked so that we 
                        //can get the min width the entire placement will need.
                        Single totalPlacedDepth =
                            PlaceAtMinZWithDividers(axis.Min,
                                placementGroups,
                                axis.DividerStart,
                                axis.DividerSpacing,
                                this.DividerWidth,
                                this.PlaceStartDivider,
                                this.PlaceEndDivider,
                                out dividerValues);

                        //Check if we are over or under fitted:
                        Single axisLength = axis.Max - axis.Min;
                        if (axisLength.GreaterThan(0))
                        {
                            //If we are overfitted,
                            // Try to apply a partial squeeze to products
                            if (this.SqueezeTypeZ == PlanogramSubComponentSqueezeType.PartialSqueeze
                                && placementGroups.Any(g => IsPlacementGroupOverfilled(AxisType.Y, g.Placements)))
                            {
                                //Try to squeeze the products to fit them on.
                                List<DividerPlacementValue> squeezedDividerValues;
                                SqueezeProductDepth(placementGroups,
                                    axisLength,
                                    new PlaceGroupsDelegate(
                                    (IEnumerable<PlanogramPositionPlacementGroup> g,
                                        out List<DividerPlacementValue> divs) =>
                                    {
                                        return PlaceAtMinZWithDividers(axis.Min,
                                            placementGroups,
                                            axis.DividerStart,
                                            axis.DividerSpacing,
                                            this.DividerWidth,
                                            this.PlaceStartDivider,
                                            this.PlaceEndDivider,
                                            out divs);
                                    }),
                                    ref totalPlacedDepth,
                                    out squeezedDividerValues);
                                if (squeezedDividerValues != null) dividerValues = squeezedDividerValues;
                            }

                            //if we are still overfitted:
                            if (totalPlacedDepth.GreaterThan(axisLength))
                            {
                                #region Overfitted

                                //  Overfilled but cannot break min:
                                if (!axis.CanBreakMin && axis.CanBreakMax)
                                {
                                    //do nothing - we have already placed at min.
                                }

                                //Overfilled but cannot break max:
                                else if (axis.CanBreakMin && !axis.CanBreakMax)
                                {
                                    //place at max instead
                                    PlaceAtMaxZWithDividers(axis.Min,
                                        axis.Max,
                                        this.MerchandisingType,
                                        placementGroups,
                                        axis.DividerStart,
                                        axis.DividerSpacing,
                                        this.DividerWidth,
                                        this.PlaceStartDivider,
                                        this.PlaceEndDivider,
                                        out dividerValues);
                                }
                                else
                                {
                                    Single placementOffset = -((totalPlacedDepth - axisLength) / 2F);

                                    //snap to slot
                                    Single newStart = axis.Min + placementOffset;
                                    newStart =
                                        Convert.ToSingle(
                                            Math.Round(newStart / axis.DividerSpacing, MidpointRounding.AwayFromZero) *
                                            axis.DividerSpacing);
                                    placementOffset = newStart + axis.Min;


                                    foreach (var pGroup in placementGroups)
                                    {
                                        pGroup.SetGroupZ(pGroup.GetBounds().Z + placementOffset);
                                    }
                                    foreach (var div in dividerValues)
                                    {
                                        div.Value += placementOffset;
                                    }
                                }

                                #endregion
                            }

                            //else if we are underfitted
                            else if (totalPlacedDepth.LessThan(axisLength))
                            {
                                #region Underfitted:

                                Single merchCenter = axis.Min + (axisLength / 2F);

                                //the positions need to be spread equally within the available space.
                                if (placementGroups.Count() == 1)
                                {
                                    //Underfilled - 1 group
                                    RectValue groupBounds = placementGroups.First().GetBounds();
                                    placementGroups.First().SetGroupZ(merchCenter - (groupBounds.Depth / 2F));
                                }
                                else
                                {
                                    //Underfilled - with dividers and div spacing
                                    PlaceEvenStrategyUnderfilledWithNormalDividers(
                                        axis, axisLength, placementGroups, ref dividerValues);
                                }

                                #endregion
                            }
                        }
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramPlacementStrategy.Manual:
                    {
                        IEnumerable<PlanogramPositionPlacement> posList = placementGroups.SelectMany(p => p.Placements);

                        //enforce min and max
                        if (!axis.CanBreakMin ||
                            !axis.CanBreakMax)
                        {
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                PlanogramPositionDetails positionDetails = p.GetPositionDetails();
                                if (!axis.CanBreakMax &&
                                    ((p.Position.Z + positionDetails.TotalSpace.Depth).GreaterThan(axis.Max)))
                                {
                                    p.Position.Z = axis.Max - positionDetails.TotalSpace.Depth;
                                }

                                if (!axis.CanBreakMin &&
                                    (p.Position.Z.LessThan(axis.Min)))
                                {
                                    p.Position.Z = axis.Min;
                                }
                            }
                        }
                        else if (this.HasRow1PegHoles ||
                                 this.HasRow2PegHoles)
                        {
                            //force positions to snap to the nearest peghole
                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                p.SnapPositionToPegHole();
                            }
                        }

                        //correct collisions if overlaps are not allowed
                        if (!this.IsProductOverlapAllowed)
                        {
                            List<PlanogramPositionPlacement> placedPositions = new List<PlanogramPositionPlacement>();

                            foreach (PlanogramPositionPlacement p in posList)
                            {
                                foreach (PlanogramPositionPlacement other in placedPositions)
                                {
                                    if (p.CollidesWith(other))
                                    {
                                        p.Position.Z = other.Position.Z + other.GetPositionDetails().TotalSpace.Depth;
                                    }
                                }

                                placedPositions.Add(p);
                            }
                        }

                        //place dividers 
                        Single dividerThick = this.DividerWidth;
                        foreach (PlanogramPositionPlacement p in posList.OrderBy(p => p.Position.Z))
                        {
                            PlanogramPositionDetails positionDetails = p.GetPositionDetails();

                            Single placementN = p.Position.Z;
                            if (placementN.GreaterOrEqualThan(axis.DividerStart))
                            {
                                Boolean isFirst = posList.First() == p;
                                Boolean isLast = posList.Last() == p;

                                if (isFirst)
                                {
                                    if (this.PlaceStartDivider)
                                    {
                                        Single mod = (placementN - axis.DividerStart) % axis.DividerSpacing;

                                        Single firstDiv = placementN - mod;
                                        if (firstDiv.GreaterThan(axis.DividerStart))
                                        {
                                            dividerValues.Add(new DividerPlacementValue(placementN - mod, dividerThick)
                                            {
                                                IsStartDivider = true
                                            });
                                        }
                                        else
                                        {
                                            dividerValues.Add(new DividerPlacementValue(axis.DividerStart, dividerThick)
                                            {
                                                IsStartDivider = true
                                            });
                                        }
                                    }
                                }
                                if (!isLast ||
                                    (isLast && this.PlaceEndDivider))
                                {
                                    Single mod = (placementN + positionDetails.TotalSpace.Depth - axis.DividerStart) %
                                                 axis.DividerSpacing;

                                    Single afterDiv = placementN + positionDetails.TotalSpace.Depth +
                                                      (axis.DividerSpacing - mod);
                                    dividerValues.Add(new DividerPlacementValue(afterDiv, dividerThick)
                                    {
                                        IsEndDivider = isLast
                                    });
                                }


                                isFirst = false;
                            }

                        }

                    }
                    break;

                    #endregion
            }

            return dividerValues;
        }

        /// <summary>
        /// Places groups at min z with a divider between each group.
        /// </summary>
        private static Single PlaceAtMinZWithDividers(
            Single axisMin,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single dividerStart,
            Single dividerSpacing,
            Single dividerThick,
            Boolean placeStartDivider,
            Boolean placeEndDivider,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();

            Single nextN = axisMin;
            Single divStart = axisMin + dividerStart;
            Single nextDivZ = axisMin + dividerStart;

            Boolean isFirstDiv = true;

            //cycle through stacking min to max
            foreach (var posGroup in placementGroups)
            {
                //if under the divider start point
                //or if the first div is being placed and would intersect
                if (nextN.GreaterOrEqualThan(divStart)
                    ||
                    (isFirstDiv && placeStartDivider &&
                     (nextN + posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Depth)).GreaterOrEqualThan(divStart)))
                {

                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = dividerSpacing;
                    posGroup.DividerSlotReservedX = dividerThick;

                    if (isFirstDiv)
                    {
                        //place the start divider
                        if (placeStartDivider
                            || (!placeStartDivider && dividerStart.GreaterThan(0)))
                        {
                            dividerValues.Add(new DividerPlacementValue(nextDivZ, dividerThick)
                            {
                                IsStartDivider = placeStartDivider
                            });
                            nextN = nextDivZ + dividerThick;
                        }
                        isFirstDiv = false;
                    }
                    else
                    {
                        dividerValues.Add(new DividerPlacementValue(nextDivZ, dividerThick));
                        nextN = nextDivZ + dividerThick;
                    }

                    //butt each position to the end X
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Z = nextN;
                    }

                    //set the next div pos
                    Single maxPosN = posGroup.Placements.Max(p => p.Position.Z + p.GetPositionDetails().TotalSpace.Depth);
                    while (nextDivZ.LessThan(maxPosN))
                    {
                        nextDivZ += dividerSpacing;
                    }
                }
                else
                {
                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = 0;
                    posGroup.DividerSlotReservedX = 0;

                    //place normally without dividers
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Z = nextN;
                    }
                    nextN = posGroup.Placements.Min(p => p.Position.Z) +
                            posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Depth);

                    //update the next div
                    while (nextDivZ.LessThan(nextN))
                    {
                        nextDivZ += dividerSpacing;
                    }
                }
            }

            //place the end divider
            if (placeEndDivider)
            {
                dividerValues.Add(new DividerPlacementValue(nextDivZ, dividerThick) { IsEndDivider = true });
            }

            Single divMax = (dividerValues.Any()) ? dividerValues.Max(d => d.Value + dividerThick) : Single.NegativeInfinity;
            return Math.Max(nextN, divMax) - axisMin;
        }

        /// <summary>
        /// Places groups at max z with a divider between each group.
        /// </summary>
        private static Single PlaceAtMaxZWithDividers(
            Single axisMin,
            Single axisMax,
            PlanogramSubComponentMerchandisingType groupMerchType,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single dividerStart,
            Single dividerSpacing,
            Single dividerThick,
            Boolean placeStartDivider,
            Boolean placeEndDivider,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = new List<DividerPlacementValue>();

            IEnumerable<PlanogramPositionPlacementGroup> orderedPlacementGroups = placementGroups;
            orderedPlacementGroups = placementGroups.Reverse();

            Single nextEndN = axisMax;

            //if the products are being hanged then consider the peg depth.
            if (groupMerchType == PlanogramSubComponentMerchandisingType.Hang)
            {
                Single pegDepth = orderedPlacementGroups.Max(g => g.Placements.Max(p => p.Product.PegDepth));
                if (pegDepth.GreaterThan(0))
                {
                    //need to start from start of the pegboard backplate (subcomponent.depth)
                    nextEndN = axisMin + pegDepth;
                }
            }

            Single divStart = nextEndN - dividerStart;
            Single nextDivZ = nextEndN - dividerStart - dividerThick;

            Boolean isFirstDiv = true;

            //cycle through stacking max to min
            foreach (var posGroup in orderedPlacementGroups)
            {
                //if under the divider start point
                //or if the first div is being placed and would intersect
                if (nextEndN.LessOrEqualThan(divStart)
                    ||
                    (isFirstDiv && placeStartDivider &&
                     (nextEndN - posGroup.Placements.Max(p => p.GetPositionDetails().TotalSpace.Depth)).LessOrEqualThan(divStart)))
                {
                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = dividerSpacing;
                    posGroup.DividerSlotReservedX = dividerThick;

                    if (isFirstDiv)
                    {
                        //place the start divider
                        if (placeStartDivider
                            || (!placeStartDivider && dividerStart.GreaterThan(0)))
                        {
                            dividerValues.Add(new DividerPlacementValue(nextDivZ, dividerThick)
                            {
                                IsStartDivider = placeStartDivider
                            });
                            nextEndN = nextDivZ;
                        }
                        isFirstDiv = false;
                    }
                    else
                    {
                        dividerValues.Add(new DividerPlacementValue(nextDivZ, dividerThick));
                        nextEndN = nextDivZ;
                    }

                    //butt each position to the end X
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Z = nextEndN - p.GetPositionDetails().TotalSpace.Depth;
                    }

                    //set the next div pos
                    Single minPosN = posGroup.Placements.Min(p => p.Position.Z);
                    while ((nextDivZ + dividerThick).GreaterThan(minPosN))
                    {
                        nextDivZ -= dividerSpacing;
                    }
                }
                else
                {
                    //set the group slot constraint
                    posGroup.DividerSlotSizeX = 0;
                    posGroup.DividerSlotReservedX = 0;

                    //place normally without dividers
                    foreach (PlanogramPositionPlacement p in posGroup.Placements)
                    {
                        p.Position.Z = nextEndN - p.GetPositionDetails().TotalSpace.Depth;
                    }
                    nextEndN = posGroup.Placements.Min(p => p.Position.Z);

                    //update the next div
                    while ((nextDivZ + dividerThick).GreaterThan(nextEndN))
                    {
                        nextDivZ -= dividerSpacing;
                    }
                }
            }

            //place the end divider
            if (placeEndDivider)
            {
                dividerValues.Add(new DividerPlacementValue(nextDivZ, dividerThick) { IsEndDivider = true });
            }

            Single divMin = (dividerValues.Any()) ? dividerValues.Min(d => d.Value) : Single.PositiveInfinity;
            return axisMax - Math.Min(nextEndN, divMin);
        }

        #endregion

        #region Z Squeeze

        /// <summary>
        /// Squeezes the placement groups to fit the given merch area depth.
        /// </summary>
        private Boolean SqueezeProductDepth(IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
                                            Single merchAreaDepth,
                                            PlaceGroupsDelegate placeGroupsAction,
            ref Single currentDepth)
        {
            List<DividerPlacementValue> dividerValues;
            return SqueezeProductDepth(placementGroups, merchAreaDepth, placeGroupsAction, ref currentDepth, out dividerValues);
        }

        /// <summary>
        /// Squeezes the placement groups to fit the given merch area depth with output dividers
        /// </summary>
        private Boolean SqueezeProductDepth(IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single merchAreaDepth,
            PlaceGroupsDelegate placeGroupsAction,
            ref Single currentDepth,
            out List<DividerPlacementValue> dividerValues)
        {
            dividerValues = null;

            //+ Check if we can squeeze by depth:

            //if the current width is over the merch area width already return out.
            if (currentDepth.LessThan(merchAreaDepth)) return false;

            //determine if any groups can possibly be squeezed.
            List<PlanogramPositionPlacementGroup> squeezeableGroups =
                placementGroups.Where(p => p.GetFullySqueezedBounds().Depth.LessThan(p.GetBounds().Depth)).ToList();

            if (!squeezeableGroups.Any()) return false;


            //work out how much we need to squeeze by
            Single squeezePerGroup = (currentDepth - merchAreaDepth) / (Single)squeezeableGroups.Count();


            //+ Apply squeeze first to groups that would hit their minimum.
            Boolean applyFullSqueeze = true;
            while (applyFullSqueeze)
            {
                applyFullSqueeze = false;
                foreach (PlanogramPositionPlacementGroup group in squeezeableGroups.ToList())
                {
                    Single groupDepth = group.GetBounds().Depth;
                    Single targetDepth = groupDepth - squeezePerGroup;
                    if (targetDepth.LessThan(group.GetFullySqueezedBounds().Depth))
                    {
                        group.ApplyFullDepthSqueeze();
                        squeezeableGroups.Remove(group);
                        applyFullSqueeze = true;
                    }
                }


                if (applyFullSqueeze)
                {
                    //something changed so replace
                    currentDepth = placeGroupsAction(placementGroups, out dividerValues);

                    //recalc amount to squeeze remaining by
                    squeezePerGroup = (currentDepth - merchAreaDepth) / (Single)squeezeableGroups.Count();
                }
            }

            //now apply the squeeze accross anything left over.
            List<PlanogramPositionPlacementGroup> failedGroups = new List<PlanogramPositionPlacementGroup>();
            foreach (PlanogramPositionPlacementGroup group in squeezeableGroups.ToList())
            {
                squeezeableGroups.Remove(group);

                Single targetDepth = group.GetBounds().Depth - squeezePerGroup;
                if (!group.SqueezeDepthTo(targetDepth))
                {
                    failedGroups.Add(group);

                    //recalc the squeeze per group
                    currentDepth = placeGroupsAction(placementGroups, out dividerValues);

                    squeezePerGroup = (currentDepth - merchAreaDepth) / (Single)squeezeableGroups.Count();
                }
            }


            //if any of the groups failed to meet their targets
            // and we are still over the target
            // - go through squeezing them to their minimum
            if (failedGroups.Any())
            {
                currentDepth = placeGroupsAction(placementGroups, out dividerValues);

                if (currentDepth.GreaterThan(merchAreaDepth))
                {
                    foreach (PlanogramPositionPlacementGroup group in failedGroups)
                    {
                        group.ApplyFullDepthSqueeze();

                        currentDepth = placeGroupsAction(placementGroups, out dividerValues);

                        if (currentDepth.LessOrEqualThan(merchAreaDepth)) return true;
                    }

                    return false;
                }
            }


            //replace and return.
            currentDepth = placeGroupsAction(placementGroups, out dividerValues);
            return true;
        }

        ///// <summary>
        ///// Returns the overall placed height of the given groups and dividers.
        ///// </summary>
        //private Single GetPlacedDepth(IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
        //    List<DividerPlacementValue> dividerValues = null)
        //{
        //    Single minZ = Single.PositiveInfinity;
        //    Single maxZ = Single.NegativeInfinity;

        //    foreach (PlanogramPositionPlacementGroup group in placementGroups)
        //    {
        //        RectValue groupBounds = group.GetBounds();
        //        minZ = Math.Min(minZ, groupBounds.Z);
        //        maxZ = Math.Max(maxZ, groupBounds.Z + groupBounds.Depth);
        //    }

        //    // Adjust the max z coordinate to take into account any overlaps within the placement group. This method
        //    // will return 0 if overlaps are allowed. This max z co-ordinate represents the virtual space occupied by the
        //    // furthest front position, if there were no overlaps in the placement groups (not its actual position).
        //    maxZ += GetOverlappingSize(AxisType.Z, placementGroups);

        //    if (dividerValues != null)
        //    {
        //        foreach (DividerPlacementValue divider in dividerValues)
        //        {
        //            minZ = Math.Min(minZ, divider.Value);
        //            maxZ = Math.Max(maxZ, divider.Value + divider.Thickness);
        //        }
        //    }

        //    if (maxZ > minZ) return maxZ - minZ;
        //    else return 0;
        //}

        #endregion

        #endregion

        #region General Axis Methods

        /// <summary>
        /// Arranges the given <paramref name="placementGroups"/> and <paramref name="dividerValues"/> in ascending order based on their
        /// position in <paramref name="axis"/>, alternating them given the starting position of the first divider.
        /// </summary>
        /// <param name="placementGroups">The groups to arrange.</param>
        /// <param name="dividerValues">The dividers to arrange.</param>
        /// <param name="axis">The direction to arrange in.</param>
        /// <returns>An ordered list of <see cref="PlanogramPositionPlacementGroup"/>s and <see cref="DividerPlacementValue"/>s.</returns>
        private List<Object> GetPlaceOrder(
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            List<DividerPlacementValue> dividerValues,
            PlanogramMerchandisingGroupAxis axis)
        {
            Stack<DividerPlacementValue> toPlaceDivs =
                new Stack<DividerPlacementValue>(dividerValues.OrderByDescending(d => d.Value));
            List<Object> placeOrder = new List<Object>(toPlaceDivs.Count + placementGroups.Count());
            Boolean isDivFirst = toPlaceDivs.Any() ? (toPlaceDivs.First().IsStartDivider) : false;
            Boolean alternate = false;
            for (Int32 i = 0; i < placementGroups.Count(); i++)
            {
                // Get the current group and add it to the place order.
                PlanogramPositionPlacementGroup currentGroup = placementGroups.ElementAt(i);
                RectValue currentGroupBounds = currentGroup.GetBounds();

                // Place the group before the divider if dividers don't come first.
                if (!isDivFirst) placeOrder.Add(currentGroup);

                // Check if the next divider is after the next divider.
                Boolean nextGroupIsAfterDivider = false;
                if (i + 1 < placementGroups.Count() && toPlaceDivs.Any())
                {
                    RectValue nextGroupBounds = placementGroups.ElementAt(i + 1).GetBounds();
                    nextGroupIsAfterDivider =
                        toPlaceDivs.Peek()
                            .Value.LessThan(nextGroupBounds.GetCoordinate(axis.AxisType) +
                                            nextGroupBounds.GetSize(axis.AxisType));
                }

                // Check if the current group ends after the starting divider coordinate.
                Boolean currentGroupIsAfterStartDivider =
                    (axis.DividerStart + axis.Min).LessThan(currentGroupBounds.GetCoordinate(axis.AxisType) +
                                                            currentGroupBounds.GetSize(axis.AxisType));

                // Decide if we should start alternating between placement groups and dividers.
                if (!alternate && (nextGroupIsAfterDivider || currentGroupIsAfterStartDivider))
                {
                    alternate = true;
                }

                // Add the next divider if we're alternating.
                if (alternate && toPlaceDivs.Any())
                {
                    placeOrder.Add(toPlaceDivs.Pop());
                }

                // Place the group after the divider if the divider is placed first.
                if (isDivFirst) placeOrder.Add(currentGroup);
            }
            return placeOrder;
        }


        /// <summary>
        /// Method used by all axis to place groups as even strategy with normal dividers when underfilled.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="axisLength"></param>
        /// <param name="placementGroups"></param>
        /// <param name="dividerValues"></param>
        private void PlaceEvenStrategyUnderfilledWithNormalDividers(
            PlanogramMerchandisingGroupAxis axis,
            Single axisLength,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            ref List<DividerPlacementValue> dividerValues)
        {
            Debug.Assert(placementGroups.Count() > 1, "This method is intended for multiple groups");

            Single fullSize = placementGroups.Sum(g => g.GetBounds().GetSize(axis.AxisType));
            Single placementSpacing = Convert.ToSingle(Math.Truncate((axisLength - fullSize) / (placementGroups.Count() - 1) * 100) / 100);

            // Lay all the products out, leaving the placement spacing in between.
            Single lastCoord = 0;
            foreach (var pGroup in placementGroups)
            {
                // Calculate the new co-ordinate of the position. This is just the last coord plus the placement
                // spacing, unless this doesn't result in an increment of a divider, in which case we artificially
                // increase the new coord and decrease the next placement spacing accordingly.
                Single newCoord = lastCoord + placementSpacing;
                Single distanceToPreceedingDividerSlot =
                    newCoord -
                    Convert.ToSingle(
                        Math.Ceiling((lastCoord - axis.DividerStart) / axis.DividerSpacing) *
                        axis.DividerSpacing +
                        GetDividerSize(axis.AxisType));
                if (distanceToPreceedingDividerSlot.LessThan(0))
                {
                    //Single spaceToEndOfNextDivider = Math.Abs(distanceToPreceedingDividerSlot) + GetDividerSize(axis.AxisType);
                    //newCoord += spaceToEndOfNextDivider;
                    newCoord += Math.Abs(distanceToPreceedingDividerSlot);
                }

                //skip the first group as it should start at the beginning.
                if (lastCoord.GreaterThan(0)) pGroup.SetGroupCoordinate(axis.AxisType, newCoord);

                RectValue groupBounds = pGroup.GetBounds(applySlotConstraints: false);
                lastCoord = groupBounds.GetCoordinate(axis.AxisType) + groupBounds.GetSize(axis.AxisType);
            }


            //get a list of the axis divider slots
            List<Single> availableDividerSlots = new List<Single>();

            for (Single curSlotX = GetDividerStartCoordinate(axis); // axis.DividerStart + axis.Min
                (curSlotX + DividerWidth).LessThan(axis.Max);
                curSlotX += axis.DividerSpacing)
            {
                availableDividerSlots.Add(curSlotX);
            }


            //now place the dividers.
            dividerValues.Clear();
            dividerValues.AddRange(CalculateDividersForEven(axis, placementGroups, availableDividerSlots));
        }

        
        /// <summary>
        /// Calculates the divider positions for a collection of <paramref name="placementGroups"/> that have been placed
        /// evenly accross a <see cref="PlanogramMerchandisingGroup"/>.
        /// </summary>
        /// <param name="axis">The axis to calculate divider positions in.</param>
        /// <param name="placementGroups">The placement groups between which dividers should be calculated.</param>
        /// <param name="availableDividerSlots">The available slots for dividers to be placed at.</param>
        /// <returns>A collection of <see cref="DividerPlacementValue"/>s where dividers should, ideally, be placed.</returns>
        private IEnumerable<DividerPlacementValue> CalculateDividersForEven(
            PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            List<Single> availableDividerSlots)
        {
            var dividerValues = new List<DividerPlacementValue>();
            Single dividerSize = this.DividerWidth; //it is always width regardless of axis..
            //switch (axis.AxisType)
            //{
            //    case AxisType.X:
            //        dividerSize = this.DividerWidth;
            //        break;
            //    case AxisType.Y:
            //        dividerSize = this.DividerHeight;
            //        break;
            //    case AxisType.Z:
            //        dividerSize = this.DividerDepth;
            //        break;
            //    default:
            //        throw new NotSupportedException();
            //}

            // Iterate over the placement groups and work out which ones need dividers before or after
            // or both.
            PlanogramPositionPlacementGroup firstPlacementGroup = placementGroups.First();
            PlanogramPositionPlacementGroup lastPlacementGroup = placementGroups.Last();
            foreach (PlanogramPositionPlacementGroup pGroup in placementGroups)
            {
                RectValue groupBounds = pGroup.GetBounds();
                Single groupEnd = groupBounds.GetCoordinate(axis.AxisType) +
                    pGroup.Placements.Max(p => p.GetMerchandisingGroupRelativeBounds().GetSize(axis.AxisType));
                //groupBounds.GetSize(axis.AxisType);

                // We're only interested in this group if it ends after the dividers start.
                if (groupEnd.GreaterThan(axis.DividerStart + axis.Min))
                {
                    // Calculate the next and previous slots for a divider before and after this group.
                    Single nextSlot = availableDividerSlots.FirstOrDefault(s => s.GreaterOrEqualThan(groupEnd));
                    Single previousSlot = ((IEnumerable<Single>)availableDividerSlots)
                        .Reverse()
                        .FirstOrDefault(s => s.LessThan(groupBounds.GetCoordinate(axis.AxisType)));

                    // If this group is the last group, we need to decide whether or not to place a divider after it.
                    if (pGroup == lastPlacementGroup)
                    {
                        // Place after if there should be an end divider.
                        if (this.PlaceEndDivider && nextSlot.GreaterOrEqualThan(groupEnd))
                        {
                            var newDivider = new DividerPlacementValue(nextSlot, dividerSize);
                            newDivider.IsEndDivider = true;
                            dividerValues.Add(newDivider);
                        }
                    }
                    else
                    {
                        // Place after there is an available slot.
                        if (nextSlot.GreaterOrEqualThan(groupEnd))
                        {
                            dividerValues.Add(new DividerPlacementValue(nextSlot, dividerSize));
                        }
                    }

                    // If this is the first group, we need to decide whether or not to place a divider before.
                    if (pGroup == firstPlacementGroup)
                    {
                        // Place before if there should be a start divider.
                        if (this.PlaceStartDivider &&
                            previousSlot.LessOrEqualThan(groupBounds.GetCoordinate(axis.AxisType)))
                        {
                            var newDivider = new DividerPlacementValue(previousSlot, dividerSize);
                            newDivider.IsStartDivider = true;
                            dividerValues.Add(newDivider);
                        }
                    }
                }
            }

            return dividerValues;
        }


        #endregion

        #endregion

        #region Begin/Cancel/Apply

        /// <summary>
        /// Begins an edit operation on this group and all of
        /// its position placement. 
        /// </summary>
        public void BeginEdit()
        {
            // begin an edit operation on the placements
            this.PositionPlacements.BeginEdit();

            // increment the edit level
            _editLevel++;

            // initialize the merchandising group
            // if this is the first edit level
            if (_editLevel == 1)
            {
                // initialize
                this.Initialize();

                // ensure all sequence values are correct
                this.CorrectPositionSequencing();

                // Now set the dividers by facing value, which needs to be done after we have initialized and corrected sequences.
                _isDividerObstructionByFacing = GetIsDividersbyFacing();
            }
        }

        /// <summary>
        /// Cancels an edit operation on this group
        /// </summary>
        public void CancelEdit()
        {
            // we must not cancel the last edit
            // as we always ensure we have at least
            // one edit level on the go
            if (_editLevel == 1) throw new InvalidOperationException();

            // cancel an edit operation on the placements
            this.PositionPlacements.CancelEdit();

            // decrement the edit level
            _editLevel--;
        }

        /// <summary>
        /// Applies the current edit operation to this group, applying all the current edits
        /// back to the original positions in the Planogram.
        /// </summary>
        public void ApplyEdit()
        {
            // we should never get in the situation
            // where the edit level is zero, as we
            // always ensure that we have one edit
            // on the go
            if (_editLevel == 0) throw new InvalidOperationException();

            // Call finalize, which sets position coordinates back to
            // being relative to their parent subcominent
            this.ProcessFinalize();


            // CCM-18739 : Delete before PositionPlacements.ApplyEdit() so DOS isn't doubled
            // for each position in the deleted placement list
            // ensure the position is removed from the planogram
            foreach (PlanogramPositionPlacement placement in this.PositionPlacements.DeletedItems)
            {
                placement.Reset();
                placement.SubComponentPlacement.Planogram.Positions.Remove(placement.Position);
            }

            // apply all edit operation on the placements
            this.PositionPlacements.ApplyEdit();

            // for each position in the placement list
            // ensure the position is added to the planogram
            foreach (PlanogramPositionPlacement placement in this.PositionPlacements)
            {
                if (placement.Position.Parent == null)
                    placement.SubComponentPlacement.Planogram.Positions.Add(placement.Position);
            }
            
            // reset the edit level back to zero
            _editLevel = 0;

            // and start a new edit
            this.BeginEdit();
        }

        /// <summary>
        /// Cancels all current edits on the merchandising group
        /// resetting the merchandising group back to its initial state
        /// </summary>
        public void Reset()
        {
            // reset all current edits on the positions
            this.PositionPlacements.Reset();

            // set the edit level back to zero
            _editLevel = 0;

            // start a new edit
            this.BeginEdit();
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Gets the facing axis of the first <see cref="PlanogramSubComponent"/> in <see cref="SubComponentPlacements"/>.
        /// </summary>
        /// <returns></returns>
        public AxisType GetFacingAxis()
        {
            if (SubComponentPlacements == null) return AxisType.X;
            if (!SubComponentPlacements.Any()) return AxisType.X;
            if (SubComponentPlacements.First().SubComponent == null) return AxisType.X;
            return SubComponentPlacements.First().SubComponent.GetFacingAxis();
        }


        /// <summary>
        /// Places the given placement groups evenly, taking into account peg hole constraints.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placementGroups"></param>
        /// <param name="axisLength"></param>
        /// <param name="totalPlacedLength"></param>
        private void PlaceEvenWhenUnderfilledAlongAxis(
            PlanogramMerchandisingGroupAxis axis,
            IEnumerable<PlanogramPositionPlacementGroup> placementGroups,
            Single axisLength,
            Single totalPlacedLength)
        {
            Single axisMin = axis.Min;
            Boolean finished = false;

            // Set the initial placement spacing to an even portion of the unplaced space.
            //Single placementSpacing = (axisLength - totalPlacedLength)/(placementGroups.Count() - 1);
            //V8-31233 Truncate to 2 decimal places, without rounding to eliminate tiny discrerpencies.
            Single placementSpacing = Convert.ToSingle(Math.Truncate((axisLength - totalPlacedLength) / (placementGroups.Count() - 1) * 100) / 100);

            while (!finished)
            {
                // Lay all the products out, leaving the placement spacing in between.
                Single lastCoord = 0;
                foreach (var pGroup in placementGroups)
                {
                    Single newCoord = lastCoord + placementSpacing;

                    //skip the first group as it should start at the beginning.
                    if (lastCoord.GreaterThan(0)) pGroup.SetGroupCoordinate(axis.AxisType, newCoord);

                    RectValue groupBounds = pGroup.GetBounds();
                    lastCoord = groupBounds.GetCoordinate(axis.AxisType) + groupBounds.GetSize(axis.AxisType);
                }

                // Check to see if there were any overlaps in the direction we're placing, due
                // to peg hole snapping.
                if ((HasRow1PegHoles || HasRow2PegHoles) &&
                    placementGroups.Select(g => g.GetBounds()).HasOverlapsIn(axis.AxisType))
                {
                    // Re-place at min and decrease the placement spacing.
                    switch (axis.AxisType)
                    {
                        case AxisType.X:
                            PlaceAtMinX(axisMin, placementGroups);
                            break;
                        case AxisType.Y:
                            PlaceAtMinY(axisMin, placementGroups);
                            break;
                        case AxisType.Z:
                            PlaceAtMinZ(axisMin, placementGroups);
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                    placementSpacing = placementSpacing * 0.9f;

                    // If the new placement spacing is less than the peg spacing, then we can't get any better
                    // results and an overlap is inevitable and we're finished.
                    if ((HasRow1PegHoles && placementSpacing.LessThan(axis.PegRow1Spacing)) ||
                        (HasRow2PegHoles && placementSpacing.LessThan(axis.PegRow2Spacing)))
                    {
                        finished = true;
                    }
                }
                else
                {
                    // If there weren't any peg holes or overlaps, then we're finished.
                    finished = true;
                }
            }
        }


        /// <summary>
        /// Returns the specified axis info
        /// </summary>
        public PlanogramMerchandisingGroupAxis GetAxis(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return this.XAxis;
                case AxisType.Y:
                    return this.YAxis;
                case AxisType.Z:
                    return this.ZAxis;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Returns the X position of the specified peg hole
        /// </summary>
        /// <remarks>
        /// zero based, from the left
        /// </remarks>
        public Single GetPegHoleX(Int32 holeNo,IList<Single> pegs)
        {
            if (pegs.Count == 0) return 0;
            if (pegs.Count <= holeNo) return pegs.Last();
            if (holeNo < 0) return pegs.First();
            return pegs[holeNo];
        }

        /// <summary>
        /// Returns the Y position of the specified peg hole
        /// </summary>
        /// <remarks>zero based, from the top</remarks>
        public Single GetPegHoleY(Int32 holeNo, IList<Single> pegs)
        {
            PlanogramMerchandisingGroupAxis axisInfo = YAxis;
            if (pegs.Count == 0) return 0;
            if (pegs.Count <= holeNo) return pegs.Last();
            if (holeNo < 0) return pegs.First();
            return pegs[holeNo];
        }

        #region GetAdjacentPositions

        /// <summary>
        /// Gets positions that are adjacent to the given position placement, in all directions.
        /// </summary>
        /// <param name="positionPlacement">The position placement to find neighbours for.</param>
        /// <exception cref="ArgumentNullException">Thrown if positionPlacement is null.</exception>
        public PlanogramPositionPlacementAdjacency GetAdjacentPositionPlacements(PlanogramPositionPlacement positionPlacement)
        {
            if (positionPlacement == null) throw new ArgumentNullException("positionPlacement", "positionPlacement cannot be null");
            return GetAdjacentPositionPlacements(positionPlacement, PositionPlacements);
        }

        /// <summary>
        /// Gets the positions that lie adjacent to the given position in the collection of
        /// other placements provided.
        /// </summary>
        /// <param name="positionPlacement">The position whose adjacent neighbours should be returned.</param>
        /// <param name="otherPositionPlacements">The other positions that should be checked for adjacency.</param>
        internal static PlanogramPositionPlacementAdjacency GetAdjacentPositionPlacements(
            PlanogramPositionPlacement positionPlacement,
            IEnumerable<PlanogramPositionPlacement> otherPositionPlacements)
        {
            // Get the co-ordinates and size of the position relative to the Planogram.
            RectValue positionBounds = positionPlacement.GetMerchandisingGroupRelativeBounds();

            // Get the placements that overlap positionPlacement in all dimension and store
            // their size and planogram relative coordinates.
            var overlappingPlacementsAndBounds = new Dictionary<PlanogramPositionPlacement, RectValue>();
            foreach (var otherPlacement in otherPositionPlacements)
            {
                RectValue otherPositionBounds = otherPlacement.GetMerchandisingGroupRelativeBounds();

                Boolean xOverlap = BlockingHelper.GetOverlap(
                    otherPositionBounds.X,
                    otherPositionBounds.Width,
                    positionBounds.X,
                    positionBounds.Width).
                    GreaterThan(0);
                Boolean yOverlap = BlockingHelper.GetOverlap(
                    otherPositionBounds.Y,
                    otherPositionBounds.Height,
                    positionBounds.Y,
                    positionBounds.Height).
                    GreaterThan(0);
                Boolean zOverlap = BlockingHelper.GetOverlap(
                    otherPositionBounds.Z,
                    otherPositionBounds.Depth,
                    positionBounds.Z,
                    positionBounds.Depth).
                    GreaterThan(0);

                // If the position placement overlaps in more than one dimension, add
                // it to the overlapping list.
                if (xOverlap && yOverlap ||
                    xOverlap && zOverlap ||
                    yOverlap && zOverlap)
                {
                    overlappingPlacementsAndBounds.Add(otherPlacement, otherPositionBounds);
                }
            }

            // From the positions that overlap, determine those closest on each "side".
            var above = GetClosestPositions(true, AxisType.Y, positionPlacement, positionBounds,
                overlappingPlacementsAndBounds);
            var below = GetClosestPositions(false, AxisType.Y, positionPlacement, positionBounds,
                overlappingPlacementsAndBounds);
            var right = GetClosestPositions(true, AxisType.X, positionPlacement, positionBounds,
                overlappingPlacementsAndBounds);
            var left = GetClosestPositions(false, AxisType.X, positionPlacement, positionBounds,
                overlappingPlacementsAndBounds);
            var front = GetClosestPositions(true, AxisType.Z, positionPlacement, positionBounds,
                overlappingPlacementsAndBounds);
            var back = GetClosestPositions(false, AxisType.Z, positionPlacement, positionBounds,
                overlappingPlacementsAndBounds);

            return new PlanogramPositionPlacementAdjacency(positionPlacement, above, below, front, back, left, right);
        }

        /// <summary>
        /// Gets the closest position placements surrounding positionPlacement that appear in otherPositionsAndDetails,
        /// in the direction specified.
        /// </summary>
        /// <param name="higher">True indicates to look above the position.</param>
        /// <param name="axis">The direction to look for close positions in.</param>
        /// <param name="otherPlacementsAndBounds">A dictionary of positions that surround positionPlacement and their bounds.</param>
        /// <returns>A collection of the closest surrounding positionPlacement.</returns>
        internal static IEnumerable<PlanogramPositionPlacement> GetClosestPositions(
            Boolean higher,
            AxisType axis,
            PlanogramPositionPlacement positionPlacement,
            RectValue positionBounds,
            Dictionary<PlanogramPositionPlacement, RectValue> otherPlacementsAndBounds)
        {
            var positionsHigherOrLower = otherPlacementsAndBounds.Keys.
                Where(p => IsHigherOrLower(higher, axis, positionBounds, otherPlacementsAndBounds[p]));
            if (!positionsHigherOrLower.Any()) return new List<PlanogramPositionPlacement>();
            Single minDistance = positionsHigherOrLower.
                Min(p => GetMinDistance(higher, axis, positionBounds, otherPlacementsAndBounds[p]));
            return positionsHigherOrLower.Where(p =>
                GetMinDistance(higher, axis, positionBounds, otherPlacementsAndBounds[p]).EqualTo(minDistance));
        }

        /// <summary>
        /// Determines if otherPositionBounds is higher or lower than positionBounds.
        /// </summary>
        /// <param name="higher">True if we are testing for higher.</param>
        private static Boolean IsHigherOrLower(
            Boolean higher,
            AxisType axis,
            RectValue positionBounds,
            RectValue otherPositionBounds)
        {
            if (axis == AxisType.X)
            {
                if (higher) return otherPositionBounds.X.GreaterOrEqualThan(positionBounds.X + positionBounds.Width);
                return (otherPositionBounds.X + otherPositionBounds.Width).LessOrEqualThan(positionBounds.X);
            }
            else if (axis == AxisType.Y)
            {
                if (higher) return otherPositionBounds.Y.GreaterOrEqualThan(positionBounds.Y + positionBounds.Height);
                return (otherPositionBounds.Y + otherPositionBounds.Height).LessOrEqualThan(positionBounds.Y);
            }
            else
            {
                if (higher) return otherPositionBounds.Z.GreaterOrEqualThan(positionBounds.Z + positionBounds.Depth);
                return (otherPositionBounds.Z + otherPositionBounds.Depth).LessOrEqualThan(positionBounds.Z);
            }
        }

        /// <summary>
        /// Gets the distance between the given positionBounds and otherPositionBounds.
        /// </summary>
        /// <param name="higher">True if otherPosition is above position.</param>
        /// <param name="axis">The direction we are looking in.</param>
        /// <returns>A single representing the distance between the two positions bounds.</returns>
        private static Single GetMinDistance(
            Boolean higher,
            AxisType axis,
            RectValue positionBounds,
            RectValue otherPositionBounds)
        {
            if (axis == AxisType.X)
            {
                if (higher) return otherPositionBounds.X - positionBounds.X + positionBounds.Width;
                return positionBounds.X - otherPositionBounds.X + otherPositionBounds.Width;
            }
            else if (axis == AxisType.Y)
            {
                if (higher) return otherPositionBounds.Y - positionBounds.Y + positionBounds.Height;
                return positionBounds.Y - otherPositionBounds.Y + otherPositionBounds.Height;
            }
            else
            {
                if (higher) return otherPositionBounds.Z - positionBounds.Z + positionBounds.Depth;
                return positionBounds.Z - otherPositionBounds.Z + otherPositionBounds.Depth;
            }
        }

        #endregion

        /// <summary>
        /// Gets the white space on this merchandising group in the given <paramref name="axis"/>, checking for the space
        /// occupied by other positions at the same sequence numbers in the axes normal to <paramref name="axis"/> as 
        /// <paramref name="positionPlacement"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="positionPlacement">The position to find whitespace around.</param>
        /// <returns></returns>
        public Single GetWhiteSpaceOnAxis(AxisType axis, PlanogramPositionPlacement positionPlacement)
        {
            Single availableSpace = GetAvailableSpaceOnAxis(axis);
            IEnumerable<PlanogramPositionPlacement> placedPositions =
                GetPositionsAtSameSequence(positionPlacement, axis.GetNormals().First(), axis.GetNormals().Last());
            Single placedSpace = placedPositions.Union(new[] { positionPlacement }).Sum(p => p.GetReservedSpace(axis));
            Single whiteSpace = availableSpace - placedSpace;
            return whiteSpace.LessThan(0) ? 0f : whiteSpace;
        }

        /// <summary>
        /// Gets the position placements at the same X, Y or Z sequence as the given position placement.
        /// </summary>
        /// <param name="positionPlacement">The position placement to match.</param>
        /// <param name="axes">The axes to match sequence numbers in.</param>
        /// <returns>An enumeration of matching position placements.</returns>
        public IEnumerable<PlanogramPositionPlacement> GetPositionsAtSameSequence(
            PlanogramPositionPlacement positionPlacement,
            params AxisType[] axes)
        {
            if (!axes.Any()) return new List<PlanogramPositionPlacement>();

            return PositionPlacements.
                Except(new[] { positionPlacement }).
                Where(p =>
                {
                    foreach (AxisType axisNormal in axes)
                    {
                        if (p.Position.GetSequence(axisNormal) != positionPlacement.Position.GetSequence(axisNormal))
                        {
                            return false;
                        }
                    }
                    return true;
                });
        }

        /// <summary>
        /// Returns true if the axis position sequence numbers should all be 1.
        /// </summary>
        private Boolean IsSingleGroupStrategy(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return
                        this.StrategyX == PlanogramSubComponentXMerchStrategyType.Left
                        || this.StrategyX == PlanogramSubComponentXMerchStrategyType.Right;

                case AxisType.Y:
                    return
                        this.StrategyY == PlanogramSubComponentYMerchStrategyType.Bottom
                        || this.StrategyY == PlanogramSubComponentYMerchStrategyType.Top;

                case AxisType.Z:
                    return
                        this.StrategyZ == PlanogramSubComponentZMerchStrategyType.Front
                        || this.StrategyZ == PlanogramSubComponentZMerchStrategyType.Back;

                default:
                    return false;
            }
        }

        /// <summary>
        /// Gets the merchandising space for this Merchandising Group based on the merchandising space of all
        /// of it's subcomponents.
        /// </summary>
        /// <param name="planogramRelative">
        /// True indicates that the space returned should be planogram relative. 
        /// False indiciates that the space should be relative to this Merchandising Group.
        /// </param>
        /// <param name="maximiseWidthForHang">True if merchandisable width should be maximised for hang components.</param>
        /// <param name="maximiseDepthForHang">True if the merchandisable depth should be maximised for hang components.</param>
        /// <returns>Null if <paramref name="constrainingSpace"/> does not contain the merchandisable space for this <see cref="PlanogramMerchandisingGroup"/></returns>
        public PlanogramMerchandisingSpace GetMerchandisingSpace(
            Boolean planogramRelative = true,
            Boolean maximiseWidthForHang = true,
            Boolean maximiseDepthForHang = true,
            RectValue? constrainingSpace = null)
        {
            // Before we calculate the merchandising space, see if we have already calcuated it for this combination of parameters.
            Tuple<Boolean, Boolean, Boolean, RectValue?> parameters = new Tuple<Boolean, Boolean, Boolean, RectValue?>(
                planogramRelative, maximiseWidthForHang, maximiseDepthForHang, constrainingSpace);
            PlanogramMerchandisingSpace merchandisingSpace = null;
            if (_getMerchandisingSpaceCache.TryGetValue(parameters, out merchandisingSpace))
            {
                return merchandisingSpace;
            }

            IEnumerable<PlanogramSubComponentPlacement> allSubCompPlacements = SubComponentPlacements
                .First()
                .Planogram
                .GetPlanogramSubComponentPlacements()
                .ToList();

            foreach (PlanogramSubComponentPlacement subComponentPlacement in SubComponentPlacements)
            {
                // Get the planogram relative merchandising space for the subcomponent.
                var merchSpace = new PlanogramMerchandisingSpace(subComponentPlacement,
                    allSubCompPlacements,
                    ignoreSpaceOverlaps: true,
                    maximiseWidthForHang: maximiseWidthForHang,
                    maximiseDepthForHang: maximiseDepthForHang);

                // Combine the space in the space we're building for the merch group, minimising
                // the height to ensure that the space is only as tall as the shortest merch space.
                if (merchandisingSpace == null)
                {
                    merchandisingSpace = merchSpace;
                }
                else
                {
                    // We combine the spaces by simply adding them to the right of this one, discounting
                    // the next space's rotation. This works, because merchandising groups should only
                    // be formed of components that form a continuous space in the x direction.
                    merchandisingSpace = merchandisingSpace.AddToRight(merchSpace);
                }
            }

            // Now convert the merch space to merch group coords if needed.
            if (!planogramRelative && merchandisingSpace != null)
            {
                merchandisingSpace = merchandisingSpace.ToMerchandisingGroupRelative(this);

                // Reduce the space by the constraining space if necessary.
                if (constrainingSpace.HasValue)
                {
                    merchandisingSpace = merchandisingSpace.ReduceToFit(constrainingSpace.Value);
                }
            }

            // Before returning the new merchandising space, add it to the cache so it won't be re-calculated next
            // time it is requested.
            _getMerchandisingSpaceCache.Add(parameters, merchandisingSpace);

            return merchandisingSpace;
        }

        /// <summary>
        /// Gets an enumeration of RectValues that represent the merchandising group relative bounding boxes
        /// of all the subcomponents in this merchandising group.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RectValue> GetSubComponentPlacementBounds()
        {
            Single cumulativeWidth = 0;
            foreach (PlanogramSubComponentPlacement subCompPlacement in SubComponentPlacements)
            {
                yield return new RectValue(
                    cumulativeWidth,
                    0,
                    0,
                    subCompPlacement.SubComponent.Width,
                    subCompPlacement.SubComponent.Height,
                    subCompPlacement.SubComponent.Depth);
                cumulativeWidth += subCompPlacement.SubComponent.Width;
            }
        }

        /// <summary>
        ///     Lookup what axes are stacked for this <c>Merchandising Group</c>.
        /// </summary>
        /// <returns>
        ///     A <see cref="Dictionary{AxisType,Boolean}" /> with a key per axis and <c>True</c> if the axis is stacked, or
        ///     <c>False</c> otherwise as the value.
        /// </returns>
        public Dictionary<AxisType, Boolean> LookupAxesStackedValue()
        {
            var isStacked = new Dictionary<AxisType, Boolean>
            {
                {
                    AxisType.X,
                    (StrategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked) ||
                    (StrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked)
                },
                {
                    AxisType.Y,
                    (StrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked) ||
                    (StrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked)
                },
                {
                    AxisType.Z,
                    (StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked) ||
                    (StrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked)
                }
            };
            return isStacked;
        }

        #endregion

        #region Sequencing x,y,z

        /// <summary>
        /// Ensures position placement sequence values are within the expected ranges.
        /// </summary>
        private void CorrectPositionSequencing()
        {
            //update the main sequence
            Int16 i = 1;
            foreach (PlanogramPosition pos in OrderPositionsBySequence().ToList())
            {
                pos.Sequence = i;
                i++;
            }

            Boolean isSingleX = IsSingleGroupStrategy(AxisType.X);
            Boolean isSingleY = IsSingleGroupStrategy(AxisType.Y);
            Boolean isSingleZ = IsSingleGroupStrategy(AxisType.Z);

            //Correct sequence numbers for single group strategies first
            if (isSingleX ||
                isSingleY ||
                isSingleZ)
            {
                foreach (PlanogramPositionPlacement placement in this.PositionPlacements)
                {
                    if (isSingleX) placement.Position.SequenceX = 1;
                    if (isSingleY) placement.Position.SequenceY = 1;
                    if (isSingleZ) placement.Position.SequenceZ = 1;
                }
            }

            //now correct for stacked strategies.
            if (!isSingleX)
            {
                if (this.StrategyX == PlanogramSubComponentXMerchStrategyType.Manual)
                {
                    ResequencePositionsByX();
                }
                else
                {
                    // First, group the position placements according to the merch type of this group.
                    IEnumerable<IEnumerable<PlanogramPositionPlacement>> masterGroups;
                    switch (this.MerchandisingType)
                    {
                        // Hang and Hang from bottom are sequenced in Y lines, before grouping by X.
                        case PlanogramSubComponentMerchandisingType.Hang:
                        case PlanogramSubComponentMerchandisingType.HangFromBottom:
                            {
                                masterGroups = this.PositionPlacements.GroupBy(p => p.Position.SequenceY);
                                break;
                            }
                        // Stack components are sequenced in X lines, which is what we're about to do, so just take
                        // the position placements.
                        case PlanogramSubComponentMerchandisingType.Stack:
                            {
                                masterGroups = new List<IEnumerable<PlanogramPositionPlacement>>() { this.PositionPlacements };
                                break;
                            }
                        default:
                            return;
                    }

                    // Now, for each of the master groups, group by X.
                    foreach (IEnumerable<PlanogramPositionPlacement> masterGroup in masterGroups)
                    {
                        // Reset all the X sequences in the master group to start at 1.
                        Int16 seqX = 1;
                        foreach (
                            var group in masterGroup.GroupBy(p => p.Position.SequenceX).OrderBy(g => g.Key).ToArray())
                        {
                            foreach (var p in group) p.Position.SequenceX = seqX;
                            seqX++;
                        }
                    }
                }
            }

            if (!isSingleY)
            {
                if (this.StrategyY == PlanogramSubComponentYMerchStrategyType.Manual)
                {
                    ResequencePositionsByY();
                }
                else
                {
                    // First, group the position placements according to the merch type of this group.
                    IEnumerable<IEnumerable<PlanogramPositionPlacement>> masterGroups;
                    switch (this.MerchandisingType)
                    {
                        // Hang and Hang from bottom are sequenced in Y lines, which is what we're about to do,
                        // so just take the position placements.
                        case PlanogramSubComponentMerchandisingType.Hang:
                        case PlanogramSubComponentMerchandisingType.HangFromBottom:
                            {
                                masterGroups = new List<IEnumerable<PlanogramPositionPlacement>>() { this.PositionPlacements };
                                break;
                            }
                        // Stack components are sequence in X lines, so group the positions by X first.
                        case PlanogramSubComponentMerchandisingType.Stack:
                            {
                                masterGroups = this.PositionPlacements.GroupBy(p => p.Position.SequenceX);
                                break;
                            }
                        default:
                            return;
                    }

                    // Now, for each of the master groups, group by Y.
                    foreach (IEnumerable<PlanogramPositionPlacement> masterGroup in masterGroups)
                    {
                        // Sequence each master group with Ys starting at 1.
                        Int16 seqY = 1;
                        foreach (
                            var group in
                                this.PositionPlacements.GroupBy(p => p.Position.SequenceY).OrderBy(g => g.Key).ToArray()
                            )
                        {
                            foreach (var p in group) p.Position.SequenceY = seqY;
                            seqY++;
                        }
                    }
                }
            }

            if (!isSingleZ)
            {
                if (this.StrategyZ == PlanogramSubComponentZMerchStrategyType.Manual)
                {
                    ResequencePositionsByZ();
                }
                else
                {
                    // First, group the position placements according to the merch type of this group.
                    IEnumerable<IEnumerable<PlanogramPositionPlacement>> masterGroups;
                    switch (this.MerchandisingType)
                    {
                        // Hang and Hang from bottom components are sequenced in Y, X and then Z. As such,
                        // we need to group the positions by Y and X before resequencing in Z.
                        case PlanogramSubComponentMerchandisingType.Hang:
                        case PlanogramSubComponentMerchandisingType.HangFromBottom:
                            {
                                var yxGroups = new List<IEnumerable<PlanogramPositionPlacement>>();
                                foreach (
                                    IEnumerable<PlanogramPositionPlacement> yGroup in
                                        PositionPlacements.GroupBy(p => p.Position.SequenceY))
                                {
                                    yxGroups.AddRange(yGroup.GroupBy(p => p.Position.SequenceX));
                                }
                                masterGroups = yxGroups;
                                break;
                            }
                        // Stack components are sequenced in X, then Z, then Y, so we just need to group by X here.
                        case PlanogramSubComponentMerchandisingType.Stack:
                            {
                                masterGroups = this.PositionPlacements.GroupBy(p => p.Position.SequenceX);
                                break;
                            }
                        default:
                            return;
                    }

                    // Now, for each of the master groups, group by Z.
                    foreach (IEnumerable<PlanogramPositionPlacement> masterGroup in masterGroups)
                    {
                        // Reset the sequences for this group to start at 1.
                        Int16 seqZ = 1;
                        foreach (
                            var group in masterGroup.GroupBy(p => p.Position.SequenceZ).OrderBy(g => g.Key).ToArray())
                        {
                            foreach (var p in group) p.Position.SequenceZ = seqZ;
                            seqZ++;
                        }
                    }
                }
            }

            //now check for duplicates
            if (isSingleX &&
                isSingleY &&
                isSingleZ) return;
            var groupedPlacements =
                this.PositionPlacements.GroupBy(
                    p =>
                        new Tuple<Int16, Int16, Int16>(p.Position.SequenceX, p.Position.SequenceY, p.Position.SequenceZ));
            Boolean hasDuplicates =
                groupedPlacements
                    .Any(g => g.Count() > 1);

            if (!hasDuplicates) return;
            if (!isSingleX) ResequencePositionsByX();
            else if (!isSingleY) ResequencePositionsByY();
            else ResequencePositionsByZ();
        }

        /// <summary>
        /// Takes a sequence of positions and orders them
        /// based on the merchandising strategy of this sub component
        /// </summary>
        private IEnumerable<PlanogramPosition> OrderPositionsBySequence()
        {
            return OrderPositionsBySequence(this.PositionPlacements.Select(p => p.Position).ToList());
        }

        /// <summary>
        /// Takes a sequence of positions and orders them
        /// based on the merchandising strategy of this sub component
        /// </summary>
        private IEnumerable<PlanogramPosition> OrderPositionsBySequence(IEnumerable<PlanogramPosition> positions)
        {
            //start with the positions ordered by the original sequence
            List<PlanogramPosition> positionList = positions.OrderBy(p => p.Sequence).ToList();
            IOrderedEnumerable<PlanogramPosition> orderedPositions;
            if (IsSingleGroupStrategy(AxisType.X))
            {
                orderedPositions = positionList.OrderBy(p => positionList.IndexOf(p));
            }
            else
            {
                orderedPositions = positionList.OrderBy(p => p.SequenceX);
            }

            if (IsSingleGroupStrategy(AxisType.Y))
            {
                orderedPositions = orderedPositions.ThenBy(p => positionList.IndexOf(p));
            }
            else
            {
                orderedPositions = orderedPositions.ThenBy(p => p.SequenceY);
            }

            if (IsSingleGroupStrategy(AxisType.Z))
            {
                orderedPositions = orderedPositions.ThenBy(p => positionList.IndexOf(p));
            }
            else
            {
                orderedPositions = orderedPositions.ThenBy(p => p.SequenceZ);
            }
            return orderedPositions;
        }

        /// <summary>
        /// Resequences a set of positions based on the x strategy of this sub component
        /// </summary>
        public void ResequencePositionsByX()
        {
            IEnumerable<PlanogramPositionPlacement> positions =
                this.PositionPlacements.OrderBy(p => p.Position.Sequence).ToList();
            PlanogramSubComponentXMerchStrategyType strategyX = this.StrategyX;

            #region Manual

            if (strategyX == PlanogramSubComponentXMerchStrategyType.Manual)
            {
                if (MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                {
                    Int16 seqX = 1;

                    //order all positions in a list by x
                    var xList = positions.OrderBy(p => p.Position.X).ToList();

                    //[V8-30571] Extended this to be a bit cleverer and consider positions
                    // that may be lined up to right or centrally.
                    while (xList.Count > 0)
                    {
                        PlanogramPositionPlacement curPos = xList.First();
                        curPos.Position.SequenceX = seqX;
                        xList.Remove(curPos);

                        Single curPosWidth = curPos.GetPositionDetails().TotalSpace.Width;
                        Single xIntersectLeft = MathHelper.RoundToNearestIncrement(curPos.Position.X, 1);
                        Single xIntersect = curPos.Position.X + (curPosWidth / 2F);
                        Single xIntersectRight = MathHelper.RoundToNearestIncrement(curPos.Position.X + curPosWidth, 1);

                        foreach (PlanogramPositionPlacement p in xList.ToList())
                        {
                            Single pWidth = p.GetPositionDetails().TotalSpace.Width;

                            // if the position matches left, right or center
                            if (xIntersectLeft.EqualTo(MathHelper.RoundToNearestIncrement(p.Position.X, 1))
                                || xIntersectRight.EqualTo(MathHelper.RoundToNearestIncrement(p.Position.X + pWidth, 1))
                                || (xIntersect.GreaterThan(p.Position.X) && xIntersect.LessThan((p.Position.X + pWidth))))
                            {
                                p.Position.SequenceX = seqX;
                                xList.Remove(p);
                            }
                            else break;
                        }

                        seqX++;

                    }

                }
                else
                {
                    PlanogramSubComponent subComponent = this.SubComponentPlacements.First().SubComponent;
                    //PlanogramPegHoles subPegholes = subComponent.GetPegHoles();
                    if (subComponent.HasPegHoles())//(subPegholes.All.Any())
                    {
                        //use the nearest peghole x
                        var posSnapXs = new Dictionary<PlanogramPositionPlacement, Single>();
                        foreach (PlanogramPositionPlacement p in positions)
                        {
                            Single snapX, snapY;
                            p.GetProductPegSnapXandY(out snapX, out snapY);

                            PointValue? holeCoord = subComponent.GetNearestPeghole(snapX, snapY/*, subPegholes*/);
                            posSnapXs[p] = (holeCoord.HasValue) ? holeCoord.Value.X : snapX;
                        }


                        Int16 seqX = 1;
                        foreach (var group in positions.GroupBy(p => posSnapXs[p]).OrderBy(g => g.Key))
                        {
                            foreach (PlanogramPositionPlacement pos in group)
                            {
                                pos.Position.SequenceX = seqX;
                            }
                            seqX++;
                        }
                    }
                    else
                    {
                        //go from the left of the position.
                        Int16 seqX = 1;
                        foreach (var group in positions.OrderBy(p => p.Position.X).GroupBy(p => p.Position.X))
                        {
                            foreach (PlanogramPositionPlacement pos in group)
                            {
                                pos.Position.SequenceX = seqX;
                            }
                            seqX++;
                        }
                    }
                }
            }

            #endregion

            #region Single Group

            else if (IsSingleGroupStrategy(AxisType.X))
            {
                //set all x to 1.
                foreach (PlanogramPositionPlacement p in positions)
                {
                    p.Position.SequenceX = 1;
                }


                //check if we have caused any duplicates
                Boolean hasDuplicates =
                    positions.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceZ))
                        .Any(g => g.Count() > 1);

                if (hasDuplicates)
                {
                    if (!IsSingleGroupStrategy(AxisType.Y))
                    {
                        Int16 ySequence = 1;
                        foreach (
                            var yzGroup in
                                positions.GroupBy(
                                    p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceZ))
                                    .OrderBy(k => k.Key.Item1)
                                    .ToList())
                        {
                            foreach (PlanogramPositionPlacement p in yzGroup)
                            {
                                p.Position.SequenceY = ySequence;
                                ySequence++;
                            }
                        }
                    }
                    else if (!IsSingleGroupStrategy(AxisType.Z))
                    {
                        //shift all positions by z
                        Int16 zSequence = 1;
                        foreach (
                            var yzGroup in
                                positions.GroupBy(
                                    p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceZ))
                                    .OrderBy(k => k.Key.Item2)
                                    .ToList())
                        {
                            foreach (PlanogramPositionPlacement p in yzGroup)
                            {
                                p.Position.SequenceZ = zSequence;
                                zSequence++;
                            }
                        }
                    }
                }
            }
            #endregion

            #region MultiGroup

            else
            {
                List<List<PlanogramPositionPlacement>> xGroups = new List<List<PlanogramPositionPlacement>>();
                foreach (
                    var group in
                        positions.OrderBy(p => p.Position.SequenceX)
                            .ThenBy(p => p.Position.Sequence)
                            .GroupBy(p => p.Position.SequenceX)
                    )
                {
                    xGroups.Add(group.ToList());
                }

                //cycle through groups breaking them up if required.
                if (xGroups.Count < positions.Count())
                {
                    foreach (var group in xGroups.ToList())
                    {
                        //check if we have any
                        Int32 idx = xGroups.IndexOf(group);
                        foreach (
                            var yzGroup in
                                group.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceY, p.Position.SequenceZ))
                                    .ToList())
                        {
                            if (yzGroup.Count() > 1)
                            {
                                foreach (PlanogramPositionPlacement p in yzGroup)
                                {
                                    //skip the first one
                                    if (p == yzGroup.First()) continue;

                                    xGroups.Insert(idx + 1, new List<PlanogramPositionPlacement> { p });
                                    group.Remove(p);
                                    idx++;
                                }
                            }
                        }
                    }
                }

                //now apply the sequence
                Int16 seq = 1;
                foreach (var group in xGroups.Where(g => g.Count > 0))
                {
                    foreach (PlanogramPositionPlacement p in group) p.Position.SequenceX = seq;
                    seq++;
                }
            }

            #endregion
        }

        /// <summary>
        /// Resequences a set of positions based on the y strategy of this sub component
        /// </summary>
        public void ResequencePositionsByY()
        {
            IEnumerable<PlanogramPositionPlacement> positions =
                this.PositionPlacements.OrderBy(p => p.Position.Sequence).ToList();
            PlanogramSubComponentYMerchStrategyType strategyY = this.StrategyY;

            #region Manual

            if (strategyY == PlanogramSubComponentYMerchStrategyType.Manual)
            {
                if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                {
                    Int16 seqY = 1;
                    foreach (
                        var group in
                            positions.OrderBy(p => p.Position.Y)
                                .GroupBy(p => MathHelper.RoundToNearestIncrement(p.Position.Y, 1)))
                    {
                        foreach (PlanogramPositionPlacement pos in group)
                        {
                            pos.Position.SequenceY = seqY;
                        }
                        seqY++;
                    }
                }
                else
                {
                    PlanogramSubComponent subComponent = SubComponentPlacements.First().SubComponent;
                    if (subComponent.HasPegHoles())
                    {
                        //use the nearest peghole y
                        var posSnapYs = new Dictionary<PlanogramPositionPlacement, Single>();
                        foreach (PlanogramPositionPlacement p in positions)
                        {
                            Single snapX, snapY;
                            p.GetProductPegSnapXandY(out snapX, out snapY);

                            PointValue? holeCoord = subComponent.GetNearestPeghole(snapX, snapY);
                            posSnapYs[p] = (holeCoord.HasValue) ? holeCoord.Value.Y : snapY;
                        }


                        Int16 seqY = 1;
                        foreach (var group in positions.GroupBy(p => posSnapYs[p]).OrderBy(g => g.Key))
                        {
                            foreach (PlanogramPositionPlacement pos in group)
                            {
                                pos.Position.SequenceY = seqY;
                            }
                            seqY++;
                        }
                    }
                    else
                    {
                        //go from the top of the position.
                        Int16 seqY = 1;
                        foreach (var group in positions.OrderBy(p => p.Position.Y).GroupBy(p => p.Position.Y))
                        {
                            foreach (PlanogramPositionPlacement pos in group)
                            {
                                pos.Position.SequenceY = seqY;
                            }
                            seqY++;
                        }
                    }
                }
            }
            #endregion

            #region Single Group

            else if (IsSingleGroupStrategy(AxisType.Y))
            {
                //set all position y to 1
                foreach (PlanogramPositionPlacement pos in positions)
                {
                    pos.Position.SequenceY = 1;
                }

                //check if we have caused any duplicates
                Boolean hasDuplicates =
                    positions.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceZ))
                        .Any(g => g.Count() > 1);
                if (hasDuplicates)
                {
                    if (!IsSingleGroupStrategy(AxisType.X))
                    {
                        //shift all positions by x
                        Int16 xSequence = 1;
                        foreach (
                            var xzGroup in
                                positions.GroupBy(
                                    p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceZ))
                                    .OrderBy(k => k.Key.Item1)
                                    .ToList())
                        {
                            foreach (PlanogramPositionPlacement p in xzGroup)
                            {
                                p.Position.SequenceX = xSequence;
                                xSequence++;
                            }
                        }
                    }
                    else if (!IsSingleGroupStrategy(AxisType.Z))
                    {
                        //shift all positions by z
                        Int16 zSequence = 1;
                        foreach (
                            var xzGroup in
                                positions.GroupBy(
                                    p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceZ))
                                    .OrderBy(k => k.Key.Item2)
                                    .ToList())
                        {
                            foreach (PlanogramPositionPlacement p in xzGroup)
                            {
                                p.Position.SequenceZ = zSequence;
                                zSequence++;
                            }
                        }
                    }
                }


            }
            #endregion

            #region MultiGroup

            else
            {
                List<List<PlanogramPositionPlacement>> yGroups = new List<List<PlanogramPositionPlacement>>();
                foreach (
                    var group in
                        positions.OrderBy(p => p.Position.SequenceY)
                            .ThenBy(p => p.Position.Sequence)
                            .GroupBy(p => p.Position.SequenceY))
                {
                    yGroups.Add(group.ToList());
                }

                //cycle through groups breaking them up if required.
                if (yGroups.Count < positions.Count())
                {
                    foreach (var group in yGroups.ToList())
                    {
                        //check if we have any
                        Int32 idx = yGroups.IndexOf(group);
                        foreach (
                            var xzGroup in
                                group.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceZ))
                                    .ToList())
                        {
                            if (xzGroup.Count() > 1)
                            {
                                foreach (PlanogramPositionPlacement p in xzGroup)
                                {
                                    yGroups.Insert(idx + 1, new List<PlanogramPositionPlacement> { p });
                                    idx++;
                                }
                            }
                        }
                    }
                }

                //now apply the sequence
                Int16 seq = 1;
                foreach (var group in yGroups)
                {
                    foreach (PlanogramPositionPlacement p in group) p.Position.SequenceY = seq;
                    seq++;
                }

            }

            #endregion
        }

        /// <summary>
        /// Resequences a set of positions based on the z strategy of this sub component
        /// </summary>
        public void ResequencePositionsByZ()
        {
            IEnumerable<PlanogramPositionPlacement> positions =
                this.PositionPlacements.OrderBy(p => p.Position.Sequence).ToList();
            PlanogramSubComponentZMerchStrategyType strategyZ = this.StrategyZ;

            #region Manual

            if (strategyZ == PlanogramSubComponentZMerchStrategyType.Manual)
            {
                Int16 seq = 1;
                foreach (
                    var group in
                        positions.OrderBy(p => p.Position.Z)
                            .GroupBy(p => MathHelper.RoundToNearestIncrement(p.Position.Z, 1)))
                {
                    foreach (PlanogramPositionPlacement pos in group)
                    {
                        pos.Position.SequenceZ = seq;
                    }
                    seq++;
                }
            }
            #endregion

            #region SingleGroup

            else if (IsSingleGroupStrategy(AxisType.Z))
            {
                //set all z to 1
                foreach (PlanogramPositionPlacement pos in positions)
                {
                    pos.Position.SequenceZ = 1;
                }


                //check if we have caused any duplicates
                Boolean hasDuplicates =
                    positions.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceY))
                        .Any(g => g.Count() > 1);
                if (hasDuplicates)
                {
                    if (!IsSingleGroupStrategy(AxisType.X))
                    {
                        Int16 xSequence = 1;
                        foreach (
                            var xyGroup in
                                positions.GroupBy(
                                    p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceY))
                                    .OrderBy(k => k.Key.Item1)
                                    .ToList())
                        {
                            foreach (PlanogramPositionPlacement p in xyGroup)
                            {
                                p.Position.SequenceX = xSequence;
                                xSequence++;
                            }
                        }
                    }
                    else if (!IsSingleGroupStrategy(AxisType.Y))
                    {
                        Int16 ySequence = 1;
                        foreach (
                            var xyGroup in
                                positions.GroupBy(
                                    p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceY))
                                    .OrderBy(k => k.Key.Item2)
                                    .ToList())
                        {
                            foreach (PlanogramPositionPlacement p in xyGroup)
                            {
                                p.Position.SequenceY = ySequence;
                                ySequence++;
                            }
                        }
                    }
                }

            }
            #endregion

            #region MultiGroup

            else
            {

                List<List<PlanogramPositionPlacement>> zGroups = new List<List<PlanogramPositionPlacement>>();
                foreach (
                    var group in
                        positions.OrderBy(p => p.Position.SequenceZ)
                            .ThenBy(p => p.Position.Sequence)
                            .GroupBy(p => p.Position.SequenceZ))
                {
                    zGroups.Add(group.ToList());
                }

                //cycle through groups breaking them up if required.
                if (zGroups.Count < positions.Count())
                {
                    foreach (var group in zGroups.ToList())
                    {
                        //check if we have any
                        Int32 idx = zGroups.IndexOf(group);
                        foreach (
                            var xyGroup in
                                group.GroupBy(p => new Tuple<Int16, Int16>(p.Position.SequenceX, p.Position.SequenceY))
                                    .ToList())
                        {
                            if (xyGroup.Count() > 1)
                            {
                                foreach (PlanogramPositionPlacement p in xyGroup)
                                {
                                    zGroups.Insert(idx + 1, new List<PlanogramPositionPlacement> { p });
                                    idx++;
                                }
                            }
                        }
                    }
                }


                //now apply the sequence
                Int16 seq = 1;
                foreach (var group in zGroups)
                {
                    foreach (PlanogramPositionPlacement p in group) p.Position.SequenceZ = seq;
                    seq++;
                }

            }

            #endregion
        }

        #endregion

        #region Overfilled

        /// <summary>
        ///     Determines whether there are any sequence groups of placements that do not fit in the merchandising group.
        /// </summary>
        /// <remarks>There needs to be just one axis with overfill for the whole group to be reported as overfilled.</remarks>
        public Boolean IsOverfilled(PlanogramMerchandisingSpace space = null)
        {
            return IsOverfilled(AxisType.X, space) || IsOverfilled(AxisType.Y, space) || IsOverfilled(AxisType.Z, space);
        }

        /// <summary>
        ///     Determines whether there are any sequence placements that
        ///     do not fit in the given <paramref name="axis"/> of this merchandising group.
        /// </summary>
        /// /// <param name="checkBoundaries">Flag added for block placement task. 
        /// TODO: Needs review as the task is incorrectly returning overfilled for sloped shelves.</param>
        /// <returns>True if overfilled, else false</returns>
        public Boolean IsOverfilled(AxisType axis, PlanogramMerchandisingSpace space = null, Boolean checkBoundaries = false)
        {
            return IsOverfilled(axis, this.PositionPlacements, space, checkBoundaries);
        }

        /// <summary>
        ///     Determines whether there are any <c>Placements</c> that
        ///     do not fit in the given <paramref name="axis"/> of this <c>Merchandising Group</c>.
        /// </summary>
        /// <param name="checkBoundaries">Flag added for block placement task. 
        /// TODO: Needs review as the task is incorrectly returning overfilled for sloped shelves.</param>
        /// <returns><c>True</c> if the overfilled, else <c>false</c></returns>
        /// <remarks>
        /// When checking for overfills the <c>Placements</c> will be grouped by normal axes, i.e. if checking the <c>X axis</c>, <c>Placements</c> will be grouped by <c>SequenceY</c> and <c>SequenceZ</c> (forming rows on the X axis).
        /// <para></para>
        /// <para></para>When <see cref="PlanogramSubComponentMerchandisingType.None"/> <c>IsOverfilled</c> always returns <c>False</c>.
        /// <para></para>When <see cref="PlanogramSubComponentMerchandisingType.Stack"/> <c>IsOverfilled</c> will check the accumulated size on the <paramref name="axis"/> for each group of <c>Placements</c>.
        /// <para></para>When <see cref="PlanogramSubComponentMerchandisingType.Hang"/> <c>IsOverfilled</c> will check:
        /// <para></para>   + For the X and Y axes: That every <c>Placement</c>'s facings on a pegged component is on a peg, that there is no more than one <c>Placement</c>'s facing on each peg; If the <c>SubComponent</c> does not have pegs, then that no more than half a product overhangs on either side.
        /// <para></para>   + For the Z axis: That the accumulated size on the <paramref name="axis"/> for each group of <c>Placements</c> is not overfiled on the peg, if the product is pegged, and in the merchandising space if not.
        /// <para></para>When <see cref="PlanogramSubComponentMerchandisingType.HangFromBottom"/> <c>IsOverfilled</c> will check:
        /// <para></para>   + For the X and Y axes: That every placement does not have more than one facing.
        /// <para></para>   + for the Z axis: That the accumulated size on the <paramref name="axis"/> for each group of <c>Placements</c> fits in the available merchandising space.
        /// </remarks>
        public Boolean IsOverfilled(AxisType axis,
            IEnumerable<PlanogramPositionPlacement> positionPlacements,
            PlanogramMerchandisingSpace space = null,
            Boolean checkBoundaries = false,
            List<BoundaryInfo> externalObstructions = null)
        {
            //  Get the placements to take into account.
            List<PlanogramPositionPlacement> placements = positionPlacements == null
                                                              ? new List<PlanogramPositionPlacement>()
                                                              : positionPlacements.ToList();

            //  Without any position placements, the merchandising group is never overfilled.
            if (!placements.Any()) return false;

            //get the merch space if we dont have it yet.
            if (space == null) space = GetMerchandisingSpace();

            //if we are checking boundaries then get the list of external obstructions.
            if (checkBoundaries && externalObstructions == null)
            {
                externalObstructions = GetExternalObstructions(space);
            }


            foreach (List<PlanogramPositionPlacement> positionPlacementGroup in
                PlanogramPositionPlacementList.GroupByAxis(axis, placements).Select(g => g.Value))
            {
                if (IsPlacementGroupOverfilled(axis, positionPlacementGroup, externalObstructions, space, checkBoundaries))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns true if the given placement belongs to a placement group which is overfilled.
        /// Checks groups in all axis.
        /// </summary>
        /// <param name="space"></param>
        /// <returns></returns>
        public Boolean IsPlacementOverfilled(PlanogramPositionPlacement targetPlacement, PlanogramMerchandisingSpace space = null)
        {
            Debug.Assert(this.PositionPlacements.Contains(targetPlacement),
                "Placement target does not belong to this merch group.");

            //get the merch space if we dont have it yet.
            if (space == null) space = GetMerchandisingSpace();

            //Check each axis.
            if (IsPlacementGroupOverfilled(AxisType.X, targetPlacement, null,
                space: space, checkBoundaries: false, allowNeededOverhangOnPegs: true))
                return true;

            if (IsPlacementGroupOverfilled(AxisType.Y, targetPlacement, null,
                space: space, checkBoundaries: false, allowNeededOverhangOnPegs: true))
                return true;

            if (IsPlacementGroupOverfilled(AxisType.Z, targetPlacement, null,
                space: space, checkBoundaries: false, allowNeededOverhangOnPegs: true))
                return true;

            return false;
        }


        /// <summary>
        /// Gets the available space for the given axis.
        /// </summary>
        /// <param name="axis">Axis to check on.</param>
        /// <param name="space">Merch space - optional.</param>
        /// <remarks>Provide space parameter if already known (for performance).</remarks>
        private Single GetAvailableSpaceOnAxis(AxisType axis, PlanogramMerchandisingSpace space = null)
        {
            if (space == null) space = GetMerchandisingSpace(false, false);

            RectValue spaceReducedByObstructions = space.SpaceReducedByObstructions;
            Single availableSpaceOnAxis = 0;
            switch (axis)
            {
                case AxisType.X:
                    availableSpaceOnAxis = spaceReducedByObstructions.Width;
                    break;
                case AxisType.Y:
                    availableSpaceOnAxis = spaceReducedByObstructions.Height;
                    break;
                case AxisType.Z:
                    availableSpaceOnAxis = spaceReducedByObstructions.Depth;
                    break;
                default:
                    Debug.Fail("Unknown axis type.");
                    break;
            }
            return availableSpaceOnAxis;
        }

        /// <summary>
        /// Check used by autofill to determine if a group of placements, identified by <paramref name="targetPlacement"/>, are
        /// overfilled.
        /// </summary>
        /// <param name="axis">The axis to be checked</param>
        /// <param name="targetPlacement">The placement we are targeting</param>
        /// <param name="externalObstructions">Any external obstruction boundaries which intrude on the merchandising space.
        /// This is primarily other positions in the plan as subcomponents are included in PlanogramMerchandisingSpace instead. </param>
        /// <param name="space">The PlanogramMerchandisingSpace info for this merch group, in planogram relative coordinates.</param>
        /// <returns>True if the placement group is overfilled.</returns>
        private Boolean IsPlacementGroupOverfilled(AxisType axis, PlanogramPositionPlacement targetPlacement,
            List<BoundaryInfo> externalObstructions,
            PlanogramMerchandisingSpace space = null, Boolean checkBoundaries = true, Boolean allowNeededOverhangOnPegs = true)
        {
            if (targetPlacement == null) return false;
            if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.None) return false;

            //get the pillar of placements to check
            List<PlanogramPositionPlacement> placements = GetPlacementGroups(axis, targetPlacement);

            return IsPlacementGroupOverfilled(axis, placements, externalObstructions, space, checkBoundaries, allowNeededOverhangOnPegs);
        }
        
        /// <summary>
        /// Check to see if this merch group can use placement groups
        /// for the supplied axis. If a merch group is stacked in more
        /// than one direction then some directions may not be able to
        /// use placement groups.
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        private Boolean CanUsePlacementGroups(AxisType axis)
        {
            Boolean output = true;
            if(_canUsePlacementGroupLookup.TryGetValue(axis, out output))
            {
                return output;
            }

            List<AxisType> axisOrder = GetAxisProcessingOrder(true, true, true);
            Dictionary<AxisType, Boolean> axisFlags = GetAxisStackingFlags();
            AxisType firstAxis = axisOrder.First();

            if (firstAxis == axis) output = true;

            Int32 stackedCount = 0;
            foreach (AxisType oAxis in axisOrder)
            {
                if (axisFlags[oAxis])
                {
                    stackedCount++;
                }

                if (oAxis == axis)
                {
                    output = stackedCount <= 1;
                    break;
                }

                
            }

            _canUsePlacementGroupLookup[axis] = output;

            return output;
        }

        /// <summary>
        /// Returns a dictionary of flags denoting if an axis had a stacking merch style (left/right/even);
        /// </summary>
        /// <returns></returns>
        private Dictionary<AxisType, Boolean> GetAxisStackingFlags()
        {
            Dictionary<AxisType, Boolean> output = new Dictionary<AxisType, Boolean>();
            PlanogramSubComponent subComponent = this.SubComponentPlacements.First().SubComponent;

            switch(subComponent.MerchandisingStrategyX)
            {
                case PlanogramSubComponentXMerchStrategyType.Even:
                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                case PlanogramSubComponentXMerchStrategyType.Manual: //assume manual can stack as it could go anywhere
                    output[AxisType.X] = true;
                    break;
                default:
                    output[AxisType.X] = false;
                    break;
            }

            switch (subComponent.MerchandisingStrategyY)
            {
                case PlanogramSubComponentYMerchStrategyType.Even:
                case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                case PlanogramSubComponentYMerchStrategyType.TopStacked:
                case PlanogramSubComponentYMerchStrategyType.Manual: //assume manual can stack as it could go anywhere
                    output[AxisType.Y] = true;
                    break;
                default:
                    output[AxisType.Y] = false;
                    break;
            }

            switch (subComponent.MerchandisingStrategyZ)
            {
                case PlanogramSubComponentZMerchStrategyType.Even:
                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                case PlanogramSubComponentZMerchStrategyType.Manual: //assume manual can stack as it could go anywhere
                    output[AxisType.Z] = true;
                    break;
                default:
                    output[AxisType.Z] = false;
                    break;
            }

            return output;
        }
        
        /// <summary>
        /// Check used by autofill to determine if a group of placements, identified by <paramref name="targetPlacement"/>, are
        /// overfilled.
        /// </summary>
        /// <param name="axis">The axis to be checked</param>
        /// <param name="placements">The placement group to check</param>
        /// <param name="checkBoundaries"></param>
        /// <param name="externalObstructions">Any external obstruction boundaries which intrude on the merchandising space.
        /// This is primarily other positions in the plan as subcomponents are included in PlanogramMerchandisingSpace instead. </param>
        /// <param name="space">The PlanogramMerchandisingSpace info for this merch group, in planogram relative coordinates.</param>
        /// <param name="allowNeededOverhangOnPegs">Whether pegged positions are allowed to overhang as long as on a peg, even without explicit Overhang on the component.</param>
        /// <returns>True if the placement group is overfilled.</returns>
        private Boolean IsPlacementGroupOverfilled(AxisType axis, IList<PlanogramPositionPlacement> placements,
            List<BoundaryInfo> externalObstructions = null, PlanogramMerchandisingSpace space = null,
            Boolean checkBoundaries = true, Boolean allowNeededOverhangOnPegs = true)
        {
            const Boolean isOverfilled = true;
            const Boolean notOverfilled = false;

            // check that there are some positions to count
            if (!placements.Any()) return notOverfilled;


            // Do not exceed max rules for this axis.
            //  TODO: Maybe this should be checked outside Overfill checks? Exceeding max is a product rule broken, not overfill.
            if (placements.Any(p => !p.Position.IsManuallyPlaced && p.IsMaxExceeded(axis))) return isOverfilled;

            //get the merch space if we dont have it yet.
            if (space == null) space = GetMerchandisingSpace();

            if (CheckOverfilledMerchSpace(axis, CanUsePlacementGroups(axis) ? placements : PositionPlacements, space, checkBoundaries)) return isOverfilled;

            if (checkBoundaries && CheckCollisionsWithObstructions(axis, placements, space, externalObstructions)) return isOverfilled;

            //Perform merch type specific checks:
            switch (MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.Hang:
                    return CheckOverfilledHangComponent(axis, placements, allowNeededOverhangOnPegs);

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return CheckOverfilledHangFromBottomComponent(axis, placements);
            }

            //if we have gotten this far then the group is not overfilled - yay!
            return notOverfilled;
        }

        /// <summary>
        ///     Check that there is enough merchspace for the <paramref name="placementGroup"/>, considering the <paramref name="axis"/> and the <paramref name="space"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placementGroup"></param>
        /// <param name="space"></param>
        /// <param name="checkBoundaries"></param>
        /// <returns></returns>
        /// <remarks>Pegged positions won't check the Z axis as they have different rules.</remarks>
        private Boolean CheckOverfilledMerchSpace(AxisType axis, ICollection<PlanogramPositionPlacement> placementGroup, PlanogramMerchandisingSpace space, Boolean checkBoundaries)
        {
            // Get all position bounds that should check for merch space overfill 
            //  (pegged position will be ignored; these usually check to see if they are on a valid peg).
            List<RectValue> nonPeggedPositionBounds = placementGroup.Where(p => !p.IsAPeggedPosition()).Select(p => p.GetMerchandisingGroupRelativeBounds()).ToList();
            if (!nonPeggedPositionBounds.Any()) return false;

            //  Get the limits.
            RectValue merchGroupSpace = checkBoundaries ? space.UnhinderedSpace : space.SpaceReducedByObstructions;
            Single availableLength = merchGroupSpace.GetSize(axis);
            Single minAllowedCoord = GetAxis(axis).Min;
            Single maxAllowedCoord = minAllowedCoord + availableLength;

            Single maxCoord = nonPeggedPositionBounds.Max(p => p.GetCoordinate(axis) + p.GetSize(axis));
            Single minCoord = nonPeggedPositionBounds.Min(p => p.GetCoordinate(axis));
            if (checkBoundaries)
            {
                // Check that the "right"-most point fits in the available space.
                //  This check does not need to be performed on hang-type groups in the X direction, because these
                //  merch groups allows "sides" overhangs.
                Boolean isAnyHangType = MerchandisingType == PlanogramSubComponentMerchandisingType.Hang ||
                                        MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom;
                if (maxCoord.GreaterThan(maxAllowedCoord)) return true;

                // Check that the "left"-most point fits in the available space.
                // This check does not need to be performed on hang-type groups in the X,Y directions, because these
                // merch groups allow "bottom" and "sides" overhangs.
                if (!(isAnyHangType && (axis == AxisType.Y || axis == AxisType.X)) &&
                    minCoord.LessThan(minAllowedCoord)) return true;               
            }

            if (!CanUsePlacementGroups(axis))
            {
                placementGroup = this.PositionPlacements;
            }

            // Check that the placed length fits within the available space.
            IEnumerable<PointValue> positionPlacementsCoords = this.PositionPlacements.Select(p => p.GetMerchandisingGroupRelativeCoordinates()).ToArray();
            Single placedLength = GetTotalAxisLength(placementGroup.Select(p => p.GetReservedSpace(positionPlacementsCoords)), axis);
            return placedLength.GreaterThan(availableLength);
        }

        /// <summary>
        /// Gets the sum length of all the <see cref="RectValue"/> sizes in the given axis. This sum value
        /// removes any overlapping space and does not include any white space between <see cref="RectValue"/>s
        /// </summary>
        private Single GetTotalAxisLength(IEnumerable<RectValue> rects, AxisType axis)
        {
            Single output = 0;
            List<RectValue> checkedRects = new List<RectValue>();
         
            foreach (RectValue rect in rects)
            {
                List<RectValue> inersectRects;
                if (TryIntersects(axis, rect, checkedRects, out inersectRects))
                {
                    output += rect.GetSize(axis);

                    //remove any overlapping lengths
                    //this keeps going till even the
                    //overlaps have no overlaps.
                    output -= GetTotalAxisLength(inersectRects, axis);

                    checkedRects.Add(rect);
                }
            }

            return output;
        }

        /// <summary>
        /// Tries to create a list of intersecting <see cref="RectValue"/>. If the supplied <see cref="RectValue"/> has an intersect
        /// with any <see cref="RectValue"/> within the checkedRects list that is equal in size to iself. Then the TryIntersect will
        /// return false.
        /// </summary>
        /// <param name="axis">the only <see cref="AxisType"/> to check intersects against</param>
        /// <param name="rect">The <see cref="RectValue"/> to check against the checkRects list</param>
        /// <param name="checkedRects">A list of <see cref="RectValue"/> to check intersections against</param>
        /// <param name="inersectRects"></param>
        /// <returns>false if the intersection would be the whole <see cref="RectValue"/> axis value</returns>
        private Boolean TryIntersects(AxisType axis, RectValue rect, List<RectValue> checkedRects, out List<RectValue> inersectRects)
        {
            inersectRects = new List<RectValue>();
            foreach (RectValue checkRect in checkedRects)
            {
                RectValue? intersect = AxisIntersect(checkRect, rect, axis);
                if (intersect.HasValue)
                {
                    //skip any rectangles with the same overlap size
                    //as the rect size as there is no net gain or loss
                    if (intersect.Value.GetSize(axis).EqualTo(rect.GetSize(axis)))
                    {
                        return false;
                    }

                    inersectRects.Add(intersect.Value);
                }
            }

            return true;
        }

        /// <summary>
        /// Checks for any overlap in 1 axis and returns a <see cref="RectValue?"/> that holds rectA's
        /// position and dimensions in the other axis and the amount of overlap and positon of the overlap
        /// in the supplied axis.
        /// </summary>
        public RectValue? AxisIntersect(RectValue rectA, RectValue rectB, AxisType axis)
        {
            Single axisStartA = rectA.GetCoordinate(axis);
            Single axisEndA = axisStartA + rectA.GetSize(axis);
            Single axisStartB = rectB.GetCoordinate(axis);
            Single axisEndB = axisStartB + rectB.GetSize(axis);

            if (!((axisStartB.GreaterOrEqualThan(axisStartA) && axisStartB.LessThan(axisEndA)) ||
                 (axisEndB.LessOrEqualThan(axisEndA) && axisEndB.GreaterThan(axisStartA)) ||
                 (axisStartB.LessOrEqualThan(axisStartA) && axisEndB.GreaterOrEqualThan(axisEndA))))
            {
                return null;
            }
            else
            {
                PointValue coordinates = new PointValue(
                    axis == AxisType.X ? Math.Max(rectA.X, rectB.X) : rectA.X,
                    axis == AxisType.Y ? Math.Max(rectA.Y, rectB.Y) : rectA.Y,
                    axis == AxisType.Z ? Math.Max(rectA.Z, rectB.Z) : rectA.Z);

                WidthHeightDepthValue space = new WidthHeightDepthValue(
                    axis == AxisType.X ? Math.Min(rectA.X + rectA.Width, rectB.X + rectB.Width) - coordinates.X : rectA.Width,
                    axis == AxisType.Y ? Math.Min(rectA.Y + rectA.Height, rectB.Y + rectB.Height) - coordinates.Y : rectA.Height,
                    axis == AxisType.Z ? Math.Min(rectA.Z + rectA.Depth, rectB.Z + rectB.Depth) - coordinates.Z : rectA.Depth);

                return new RectValue(coordinates, space);
            }
        }
        
        /// <summary>
        ///     Check whether there are collisions between the <paramref name="placementGroup"/> and <paramref name="externalObstructions"/> or other positions in the <paramref name="space"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placementGroup"></param>
        /// <param name="space"></param>
        /// <returns></returns>
        /// <remarks>Only the given <paramref name="axis"/> is checked for collisions.</remarks>
        private Boolean CheckCollisionsWithObstructions(AxisType axis, IEnumerable<PlanogramPositionPlacement> placementGroup,
            PlanogramMerchandisingSpace space, List<BoundaryInfo> externalObstructions = null)
        {
            //check if the positions collides with any obstructions to the merch space
            // or any positions outside of this merch group which may obstruct the merch area.
            //We only do this on the axis we are actually dealing with.

            foreach (PlanogramPositionPlacement cp in placementGroup)
            {
                BoundaryInfo posBound = cp.GetPlanogramRelativeBoundaryInfo();

                switch (axis)
                {
                    #region X:

                    case AxisType.X:
                        switch (this.StrategyX)
                        {
                            //only check space collisions on this axis for now.

                            case PlanogramSubComponentXMerchStrategyType.Right:
                            case PlanogramSubComponentXMerchStrategyType.RightStacked:
                                {
                                    Single maxX = space.SpaceReducedByObstructions.X + space.SpaceReducedByObstructions.Width;

                                    if (externalObstructions != null)
                                    {
                                        foreach (BoundaryInfo other in externalObstructions)
                                        {
                                            //if the other bounds finishes before the max
                                            if ((other.WorldRelativeBounds.X + other.WorldRelativeBounds.Width).LessThan(maxX)
                                                && posBound.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                    else
                                    {
                                        //TODO: Change merch space obstructions over to boundary info
                                        // so that we can get rid of this.
                                        foreach (RectValue other in space.Obstructions)
                                        {
                                            //if the other bounds finishes before the max
                                            if ((other.X + other.Width).LessThan(maxX) &&
                                                posBound.WorldRelativeBounds.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                }
                                break;

                            case PlanogramSubComponentXMerchStrategyType.Left:
                            case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                                {
                                    Single minX = space.SpaceReducedByObstructions.X;

                                    if (externalObstructions != null)
                                    {
                                        foreach (BoundaryInfo other in externalObstructions)
                                        {
                                            //if the other bounds starts after the min.
                                            if (other.WorldRelativeBounds.X.GreaterThan(minX)
                                                && posBound.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                    else
                                    {
                                        foreach (RectValue other in space.Obstructions)
                                        {
                                            //if the other bounds starts after the min.
                                            if (other.X.GreaterThan(minX) &&
                                                posBound.WorldRelativeBounds.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                }
                                break;

                            case PlanogramSubComponentXMerchStrategyType.Manual:
                            case PlanogramSubComponentXMerchStrategyType.Even:
                                //do nothing.
                                break;
                        }
                        break;

                    #endregion

                    #region Y:

                    case AxisType.Y:




                        switch (this.StrategyY)
                        {
                            //only check space collisions on this axis for now.

                            case PlanogramSubComponentYMerchStrategyType.Top:
                            case PlanogramSubComponentYMerchStrategyType.TopStacked:
                                {
                                    Single maxY = space.SpaceReducedByObstructions.Y + space.SpaceReducedByObstructions.Height;

                                    if (externalObstructions != null)
                                    {
                                        IEnumerable<BoundaryInfo> obstructionsToCheck =
                                           externalObstructions
                                           .Union(this.PositionPlacements.Where(p => !placementGroup.Contains(p)).Select(p => p.GetPlanogramRelativeBoundaryInfo()));

                                        foreach (BoundaryInfo other in obstructionsToCheck)
                                        {
                                            //if the other bounds finishes before the max
                                            if ((other.WorldRelativeBounds.Y + other.WorldRelativeBounds.Height).LessThan(maxY)
                                                && posBound.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                    else
                                    {
                                        IEnumerable<RectValue> obstructionsToCheck =
                                       /*otherSubs*/space.Obstructions
                                            .Union(this.PositionPlacements.Where(p => !placementGroup.Contains(p)).Select(p => p.GetPlanogramRelativeBoundingBox()));

                                        foreach (RectValue other in obstructionsToCheck)
                                        {
                                            //if the other bounds finishes before the max
                                            if ((other.Y + other.Height).LessThan(maxY) &&
                                                posBound.WorldRelativeBounds.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                }
                                break;

                            case PlanogramSubComponentYMerchStrategyType.Bottom:
                            case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                                {
                                    Single minY = space.SpaceReducedByObstructions.Y;

                                    if (externalObstructions != null)
                                    {
                                        IEnumerable<BoundaryInfo> obstructionsToCheck =
                                       externalObstructions
                                       .Union(this.PositionPlacements.Where(p => !placementGroup.Contains(p)).Select(p => p.GetPlanogramRelativeBoundaryInfo()));

                                        foreach (BoundaryInfo other in obstructionsToCheck)
                                        {
                                            //if the other bounds starts after the min.
                                            if (other.WorldRelativeBounds.Y.GreaterThan(minY)
                                                && posBound.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                    else
                                    {
                                        IEnumerable<RectValue> obstructionsToCheck =
                                       /*otherSubs*/space.Obstructions
                                            .Union(this.PositionPlacements.Where(p => !placementGroup.Contains(p)).Select(p => p.GetPlanogramRelativeBoundingBox()));

                                        foreach (RectValue other in obstructionsToCheck)
                                        {
                                            //if the other bounds starts after the min.
                                            if (other.Y.GreaterThan(minY) &&
                                                posBound.WorldRelativeBounds.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                }
                                break;

                            case PlanogramSubComponentYMerchStrategyType.Manual:
                            case PlanogramSubComponentYMerchStrategyType.Even:
                                //do nothing.
                                break;
                        }
                        break;

                    #endregion

                    #region Z:

                    case AxisType.Z:
                        switch (this.StrategyZ)
                        {
                            //only check space collisions on this axis for now.

                            case PlanogramSubComponentZMerchStrategyType.Front:
                            case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                                {
                                    Single maxZ = space.SpaceReducedByObstructions.Z + space.SpaceReducedByObstructions.Depth;

                                    if (externalObstructions != null)
                                    {
                                        foreach (BoundaryInfo other in externalObstructions)
                                        {
                                            //if the other bounds finishes before the max
                                            if ((other.WorldRelativeBounds.Z + other.WorldRelativeBounds.Depth).LessThan(maxZ)
                                                && posBound.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                    else
                                    {
                                        foreach (RectValue other in space.Obstructions)
                                        {
                                            //if the other bounds finishes before the max
                                            if ((other.Z + other.Depth).LessThan(maxZ) &&
                                                posBound.WorldRelativeBounds.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                }
                                break;

                            case PlanogramSubComponentZMerchStrategyType.Back:
                            case PlanogramSubComponentZMerchStrategyType.BackStacked:
                                {
                                    Single minZ = space.SpaceReducedByObstructions.Z;

                                    if (externalObstructions != null)
                                    {
                                        foreach (BoundaryInfo other in externalObstructions)
                                        {
                                            //if the other bounds starts after the min.
                                            if (other.WorldRelativeBounds.Z.GreaterThan(minZ)
                                                && posBound.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                    else
                                    {
                                        foreach (RectValue other in space.Obstructions)
                                        {
                                            //if the other bounds starts after the min.
                                            if (other.Z.GreaterThan(minZ) &&
                                                posBound.WorldRelativeBounds.CollidesWith(other))
                                                return true;
                                        }
                                    }
                                }
                                break;

                            case PlanogramSubComponentZMerchStrategyType.Manual:
                            case PlanogramSubComponentZMerchStrategyType.Even:
                                //do nothing.
                                break;
                        }
                        break;

                        #endregion
                }
            }
            return false;
        }

        /// <summary>
        /// Returns true if any of the products have too many facings deep according to
        /// their peg depth.
        /// </summary>
        /// <param name="axis">the axis to check..... This param is redundant as this is always going to be z...</param>
        /// <param name="placements">the placements to validate</param>
        /// <returns>True if any peg is overfilled.</returns>
        private static Boolean CheckPegDepthIsOverfilled(AxisType axis, IList<PlanogramPositionPlacement> placements)
        {
            Single maxpegDepth = 0;
            Single accumulatedDepth = 0;
            foreach (PlanogramPositionPlacement placement in placements.Where(p => p.IsAPeggedPosition()))
            {
                Single placementDepth = placement.GetReservedSpace(axis);
                Single pegDepth = placement.Product.PegDepth;
                if (placementDepth.GreaterThan(pegDepth)) return true;
                accumulatedDepth += placementDepth;
                maxpegDepth = Math.Max(maxpegDepth, pegDepth);
            }
            return accumulatedDepth.GreaterThan(maxpegDepth);
        }

        /// <summary>
        ///     Check whether there are any placements in the <paramref name="placementGroup"/> that do not have a peg on the component's surface.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placementGroup"></param>
        /// <param name="allowNeededOverhangOnPegs">Whether pegged positions are allowed to overhang as long as on a peg, even without explicit Overhang on the component.</param>
        /// <returns></returns>
        /// <remarks>If there are two placements 'sharing' the same peg, they would be be considered as lacking a peg.</remarks>
        private Boolean HasPlacementsWithoutPeg(AxisType axis, IList<PlanogramPositionPlacement> placementGroup, Boolean allowNeededOverhangOnPegs)
        {
            //  A Hang component with peg holes is overfilled if any of its position placements' peg holes is not on a peg,
            //  or if any two position placements are 'sharing' the same peg.

            //List<PointValue> subPegHoles = SubComponentPlacements.SelectMany(sc => sc.SubComponent.GetPegHoles().All).ToList();
            //Single pegHoleMinX = subPegHoles.Min(s => s.X);
            //Single pegHoleMinY = subPegHoles.Min(s => s.Y);
            //Single pegHoleMaxX = subPegHoles.Max(s => s.X);
            //Single pegHoleMaxY = subPegHoles.Max(s => s.Y);

            Single[] pegholeXCoords = this.SubComponentPlacements.SelectMany(sc => sc.SubComponent.EnumeratePegholeXCoords()).Distinct().ToArray();
            Single[] pegHoleYCoords = this.SubComponentPlacements.SelectMany(sc => sc.SubComponent.EnumeratePegholeYCoords()).Distinct().ToArray();

            Single pegHoleMinX = pegholeXCoords.Min();
            Single pegHoleMinY = pegHoleYCoords.Min();
            Single pegHoleMaxX = pegholeXCoords.Max();
            Single pegHoleMaxY = pegHoleYCoords.Max();


            var positionPegHoleCoordinates = new List<Tuple<Single, Single>>(placementGroup.Count());
            foreach (PlanogramPositionPlacement p in placementGroup)
            {
                //Get expected pegholes relative to the merch group.
                Tuple<Single, Single>[] pPegHoles = p.GetPegHoles().Select(ph => new Tuple<Single, Single>(ph.X, ph.Y)).ToArray();

                //if any of the pegs have already been taken then we are sharing so return straight out.
                if (pPegHoles.Any(pegHole => positionPegHoleCoordinates.Contains(pegHole))) return true;

                positionPegHoleCoordinates.AddRange(pPegHoles);

                PlanogramMerchandisingGroupAxis axisInfo;
                RectValue pBounds;
                switch (axis)
                {
                    case AxisType.Z:
                        //if this is z axis then we dont need to check anything else.
                        continue;
                    case AxisType.X:
                        if (!allowNeededOverhangOnPegs)
                        {
                            axisInfo = this.GetAxis(axis);
                            // If no free overhang is allowed on pegs check that the position does not overhang the edge of the subcomponent
                            // by more than its spacing.
                            pBounds = p.GetMerchandisingGroupRelativeBounds();
                            if (pBounds.X.LessThan(pegHoleMinX - axisInfo.PegRow1Spacing)
                                ||
                                (pBounds.X + pBounds.Width).GreaterThan(pegHoleMaxX + axisInfo.PegRow1Spacing))
                            {
                                return true;
                            }
                        }
                        //now check that all pegholes are within the bounds.
                        if (pPegHoles.Any(peghole => peghole.Item1.LessThan(pegHoleMinX)
                                                     || peghole.Item1.GreaterThan(pegHoleMaxX)))
                        {
                            return true;
                        }
                        break;
                    case AxisType.Y:
                        if (!allowNeededOverhangOnPegs)
                        {
                            axisInfo = this.GetAxis(axis);
                            //  If no free overhang is allowed on pegs check that the position does not overhang the edge of the subcomponent
                            // by more than its spacing.
                            pBounds = p.GetMerchandisingGroupRelativeBounds();
                            if (pBounds.Y.LessThan(pegHoleMinY - axisInfo.PegRow1Spacing)
                                ||
                                (pBounds.Y + pBounds.Height).GreaterThan(pegHoleMaxY + axisInfo.PegRow1Spacing))
                            {
                                return true;
                            }

                        }
                        //now check that all pegholes are within the bounds.
                        if (pPegHoles.Any(peghole => peghole.Item2.LessThan(pegHoleMinY)
                                                     || peghole.Item2.GreaterThan(pegHoleMaxY)))
                        {
                            return true;
                        }
                        break;
                }
            }

            return false;
        }

        /// <summary>
        ///     Check whether aditional overfill rules for Hang components are broken.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placements"></param>
        /// <param name="allowNeededOverhangOnPegs">Whether pegged positions are allowed to overhang as long as on a peg, even without explicit Overhang on the component.</param>
        /// <returns></returns>
        private Boolean CheckOverfilledHangComponent(AxisType axis, IList<PlanogramPositionPlacement> placements, Boolean allowNeededOverhangOnPegs)
        {
            if (axis == AxisType.Z)
            {
                return CheckPegDepthIsOverfilled(axis, placements);
            }

            //  For both X and Y axis, determine if slotwall characteristics
            Boolean isSlot = SubComponentPlacements.Any(p => p.SubComponent.MerchConstraintRow1SpacingX.EqualTo(0) && placements.Any(pos => pos.GetPegHoles().Any()));

            //  Except for slotwalls, all placements need a peg on hang components for axis X or Y.
            //  Ignore pegholes if there are overhangs.
            if (!isSlot &&
                (HasRow1PegHoles || HasRow2PegHoles) && SubComponentPlacements.All(p => p.SubComponent.RightOverhang.EqualTo(0) && p.SubComponent.LeftOverhang.EqualTo(0)))
                return HasPlacementsWithoutPeg(axis, placements, allowNeededOverhangOnPegs);

            //  For the X axis, a Hang component without pegs will be overfilled when in that axis any facing is completeley outside the (combined) subcomponents width.
            if (axis == AxisType.X)
                return placements.Any(pp => pp.DoesPositionOverfillXAxisForSubcomponentWithoutPegholes());

            //  Process the Y axis.
            //  Except for slotwalls, a hang component with no pegs is overfilled if more than one facing is on the Y axis,
            //  or more than one stacked placement on the same (X,Z) coordinates.
            if (!isSlot)
                return placements.Any(p => p.Position.FacingsHigh > 1) || placements.GroupBy(p => new Tuple<Single, Single>(p.Position.SequenceX, p.Position.SequenceZ)).Any(g => g.Count() > 1);


            //if slot and in the Y axis then check that products occupy their own y snap point.
            if (isSlot && axis == AxisType.Y)
            {
                if (placements.GroupBy(p =>
                    {
                        Single snapX, snapY;
                        p.GetProductPegSnapXandY(p.GetPositionDetails(), out snapX, out snapY);
                        return snapY;
                    }).Any(g => g.Count() > 1))
                {
                    return true;
                }
            }

            //  A slotwall component is overfilled if any of its position placements' peg holes is not on a peg y.
            //IEnumerable<Single> subComponentPegCoordinates = SubComponentPlacements.SelectMany(sc => sc.SubComponent.GetPegHoles().All.Select(ph => ph.Y)).Distinct();
            IEnumerable<Single> subComponentPegCoordinates = SubComponentPlacements.SelectMany(sc => sc.SubComponent.EnumeratePegholeYCoords()).Distinct();

            IEnumerable<Single> positionPegHoleYCoordinates = placements.SelectMany(p => p.GetPegHoles().Select(ph => ph.Y));
            return !positionPegHoleYCoordinates.All(thisValue => subComponentPegCoordinates.Any(otherValue => otherValue.EqualTo(thisValue)));

        }

        /// <summary>
        ///     Check whether aditional overfill rules for Hang from Bottom components are broken.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placements"></param>
        /// <returns></returns>
        private static Boolean CheckOverfilledHangFromBottomComponent(AxisType axis, IList<PlanogramPositionPlacement> placements)
        {
            //TODO: Check this as the assertions do not seem to be correct.

            switch (axis)
            {
                case AxisType.X:
                    // A Hang From Bottom component is overfilled when any placement has more than one facing wide,
                    //  or more than one stacked placement on the same (Y,Z) coordinates.
                    return placements.Any(p => p.Position.FacingsWide > 1) ||
                           placements.GroupBy(p => new Tuple<Single, Single>(p.Position.SequenceY, p.Position.SequenceZ))
                                     .Any(g => g.Count() > 1);

                case AxisType.Y:
                    // A Hang From Bottom component is overfilled when any placement has more than one facing high,
                    //  or more than one stacked placement on the same (X,Z) coordinates.
                    return placements.Any(p => p.Position.FacingsHigh > 1) ||
                           placements.GroupBy(p => new Tuple<Single, Single>(p.Position.SequenceX, p.Position.SequenceZ))
                                     .Any(g => g.Count() > 1);

                case AxisType.Z:
                    //  Nothing to check for axis Z.
                    return false;

                default:
                    Debug.Fail("Unknown Axis when calling CheckOverfilledHangFromBottomComponent");
                    return false;
            }
        }

        /// <summary>
        /// Returns a list of potential obstructions that are external to this group. 
        /// </summary>
        /// <param name="merchSpace"></param>
        /// <param name="allPlanogramMerchandisingGroups"></param>
        /// <returns></returns>
        public List<BoundaryInfo> GetExternalObstructions(PlanogramMerchandisingSpace merchSpace,
            IEnumerable<PlanogramMerchandisingGroup> allPlanogramMerchandisingGroups = null)
        {
            List<BoundaryInfo> externalObstructions = new List<BoundaryInfo>();

            Planogram plan = this.SubComponentPlacements.First().Planogram;

            //start with the unhindered merch space but then make sure that we cover any positions
            // that go out of bounds.
            RectValue areaToCheck = merchSpace.UnhinderedSpace;
            foreach (PlanogramPositionPlacement positionPlacement in this.PositionPlacements)
            {
                areaToCheck = areaToCheck.Union(positionPlacement.GetPlanogramRelativeBoundingBox());
            }


            //add any subcomponents that go into the area.
            foreach (PlanogramSubComponentPlacement other in plan.GetPlanogramSubComponentPlacements().Where(IsNotNull))
            {
                if (this.SubComponentPlacements.Any(s => Object.Equals(s.Id, other.Id))) continue;

                BoundaryInfo subBounds = other.GetPlanogramRelativeBoundaryInfo();
                if (!merchSpace.IsPlanogramRelative || subBounds.WorldRelativeBounds.CollidesWith(areaToCheck))
                    externalObstructions.Add(subBounds);
            }

            //if we have included merch groups then also add positions.
            if (allPlanogramMerchandisingGroups != null && merchSpace.IsPlanogramRelative)
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in allPlanogramMerchandisingGroups)
                {
                    if (merchandisingGroup == this) continue;

                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        BoundaryInfo positionBounds = positionPlacement.GetPlanogramRelativeBoundaryInfo();
                        if (positionBounds.WorldRelativeBounds.CollidesWith(merchSpace.UnhinderedSpace))
                            externalObstructions.Add(positionBounds);
                    }
                }
            }

            return externalObstructions;
        }

        private static Boolean IsNotNull<T>(T placement) => placement != null;

        #endregion

        #region Overlaps

        /// <summary>
        ///  Determines whether there are any placements overlapping in the merchandising group.
        /// </summary>
        /// <returns></returns>
        public Boolean HasOverlapping(IEnumerable<RectValue> reservedSpaces = null, Single overlapThreshold = 0f)
        {
            //  Lookup reserved spaces if necessary.
            if (reservedSpaces == null)
            {
                reservedSpaces = PositionPlacements.Select(p => p.GetReservedSpace());
            }

            return reservedSpaces.HasPercentageOverlaps(overlapThreshold);
        }

        /// <summary>
        ///     Determines whether there are any placements overlapping in the merchandising group using collision overlapping.
        /// </summary>
        /// <param name="reservedSpaces">[<c>Optional</c>] Enumeration of <see cref="RectValue"/> instances that represent the spaces to check. If <c>null</c> all of this instance's reserved spaces will be checked.</param>
        /// <returns><c>True</c> if there are any overlapping spaces; <c>false</c> otherwise.</returns>
        /// <remarks>
        /// This method uses the <see cref="RectValueExtensions.HasCollisionOverlaps"/> methods to detect overlaps.
        /// </remarks>
        public Boolean HasCollisionOverlapping(IEnumerable<RectValue> reservedSpaces = null)
        {
            //  Lookup reserved spaces if necessary.
            if (reservedSpaces == null)
            {
                reservedSpaces = PositionPlacements.Select(p => p.GetReservedSpace());
            }

            return reservedSpaces.HasCollisionOverlaps();
        }

        #endregion

        #region AutoFill

        /// <summary>
        /// Autofills all positions on this merchandising group, giving priority to Rank and then Gtin.
        /// </summary>
        /// <param name="allPlanogramMerchandisingGroups">The full list of merchandising groups for the planogram.
        /// This is done for performance as creating the groups each time takes ages.</param>
        /// <returns>True when made a change and processed the merch group, otherwise false.</returns>
        public Boolean Autofill(PlanogramMerchandisingGroupList allPlanogramMerchandisingGroups)
        {
            // Load planogram and settings
            Planogram plan = this.SubComponentPlacements.First().Planogram;

            // Check to see if we are allowed to autofill
            Boolean shouldFillX = (plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide && this.StrategyX != PlanogramSubComponentXMerchStrategyType.Manual);
            Boolean shouldFillY = (plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh && this.StrategyY != PlanogramSubComponentYMerchStrategyType.Manual);
            Boolean shouldFillZ = (plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep && this.StrategyZ != PlanogramSubComponentZMerchStrategyType.Manual);
            if (!shouldFillX && !shouldFillY && !shouldFillZ) return false;

            // evaluate if our merch group contains any products.
            if (PositionPlacements.Count == 0) return false;

            // Get the merchandising space and store it for use later as we don't expect this to change.
            PlanogramMerchandisingSpace merchSpace = this.GetMerchandisingSpace();

            //Create a list of all external boundaries of other subcomponents and positions on the plan
            // that could interfere with the layout of this group.
            List<BoundaryInfo> externalObstructions = GetExternalObstructions(merchSpace, allPlanogramMerchandisingGroups);


            //Process to ensure that we are up to date before the overfilled checks.
            this.Process();

            //Always shrink down all positions to minimum to start - 
            // whilst its a performance improvement to not do this, it causes autofill to behave really inconsistantly depending
            // on what order settings have been applied in.
            Boolean shouldShrinkX = shouldFillX; //&& (GetAxis(AxisType.X).IsStackedStrategy || this.IsOverfilledX());
            Boolean shouldShrinkY = shouldFillY; //&& (GetAxis(AxisType.Y).IsStackedStrategy || this.IsOverfilledY());
            Boolean shouldShrinkZ = shouldFillZ; //&& (GetAxis(AxisType.Z).IsStackedStrategy || this.IsOverfilledZ());

            // Need to reset the units to 1 for all positions in all axis and remove all uneccessary caps on specific axis.
            foreach (PlanogramPositionPlacement positionPlacement in PositionPlacements)
            {
                // Caps will only get removed on the axis that we choose to reduce.
                positionPlacement.SetMinimumUnits(shouldShrinkX, shouldShrinkY, shouldShrinkZ);

                //set minimum caps in all directions if they are 0.
                //This is so that manually set caps do not get lost during the autofill.
                if (!shouldFillX &&
                    positionPlacement.Position.FacingsXWide != 0)
                {
                    if (positionPlacement.Position.FacingsXHigh == 0) positionPlacement.Position.FacingsXHigh = 1;
                    if (positionPlacement.Position.FacingsXDeep == 0) positionPlacement.Position.FacingsXDeep = 1;
                }
                if (!shouldFillY &&
                    positionPlacement.Position.FacingsYHigh != 0)
                {
                    if (positionPlacement.Position.FacingsYWide == 0) positionPlacement.Position.FacingsYWide = 1;
                    if (positionPlacement.Position.FacingsYDeep == 0) positionPlacement.Position.FacingsYDeep = 1;
                }
                if (!shouldFillZ &&
                    positionPlacement.Position.FacingsZDeep != 0)
                {
                    if (positionPlacement.Position.FacingsZHigh == 0) positionPlacement.Position.FacingsZHigh = 1;
                    if (positionPlacement.Position.FacingsZWide == 0) positionPlacement.Position.FacingsZWide = 1;
                }
            }

            //Determine the order that the axis should be autofilled in. Subcomponent facing axis has priority.
            List<AxisType> axisOrder = GetAxisProcessingOrder(shouldFillX, shouldFillY, shouldFillZ);

            // Load a list of position placements which will be increased in order of rank (from assortment) then gtin. 
            List<PlanogramPositionPlacement> positionPlacementsOrderedByRankGtin = OrderPositionPlacementsByRankThenGtin(PositionPlacements, plan.Assortment, ignoreManuallyPlaced: true);

            // Then evaluate if our merch group contains any products.
            if (positionPlacementsOrderedByRankGtin.Count == 0) return false;


            //Process the autofill:
            foreach (AxisType axis in axisOrder)
            {
                ProcessAutofill(axis, positionPlacementsOrderedByRankGtin, externalObstructions, merchSpace);
            }

            // Remove squeeze before re-processing
            foreach (PlanogramPositionPlacement p in this.PositionPlacements)
            {
                p.Position.ResetSqueezeValues();
            }


            // Reprocess again for good measure..
            // This should only allow squeeze in non-autofilled directions (V8-31979)
            // Partial Squeeze has been re-enabled, before the processing methods now check for overfills in the same
            // as the autofill methods (in the Y axis). (V8-32013).
            this.Process();

            foreach (PlanogramPositionPlacement positionPlacement in positionPlacementsOrderedByRankGtin)
            {
                PlanogramPositionDetails posDetails = positionPlacement.GetPositionDetails();

                //Tidy up any cap values where caps are not in use.
                positionPlacement.Position.CleanUpUnusedCapValues();

                //  Recalculate units for the affected position placements.
                positionPlacement.RecalculateUnits();
            }

            return true;
        }

        private List<AxisType> GetAxisProcessingOrder(Boolean shouldFillX, Boolean shouldFillY, Boolean shouldFillZ)
        {
            List<AxisType> axisOrder = new List<AxisType>();
            AxisType axisTypeToHandleFirst = this.SubComponentPlacements.First().GetFacingAxis();
            switch (axisTypeToHandleFirst)
            {
                case AxisType.X:
                    if (shouldFillX) axisOrder.Add(AxisType.X);
                    if (shouldFillZ) axisOrder.Add(AxisType.Z);
                    if (shouldFillY) axisOrder.Add(AxisType.Y);
                    break;
                case AxisType.Y:
                    if (shouldFillY) axisOrder.Add(AxisType.Y);
                    if (shouldFillZ) axisOrder.Add(AxisType.Z);
                    if (shouldFillX) axisOrder.Add(AxisType.X);
                    break;
                case AxisType.Z:
                    if (shouldFillZ) axisOrder.Add(AxisType.Z);
                    if (shouldFillX) axisOrder.Add(AxisType.X);
                    if (shouldFillY) axisOrder.Add(AxisType.Y);
                    break;
            }

            return axisOrder;
        }

        private List<AxisType> GetAxisProcessingOrder()
        {
            Planogram plan = this.SubComponentPlacements.First().Planogram;
            Boolean shouldFillX = (plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide && this.StrategyX != PlanogramSubComponentXMerchStrategyType.Manual);
            Boolean shouldFillY = (plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh && this.StrategyY != PlanogramSubComponentYMerchStrategyType.Manual);
            Boolean shouldFillZ = (plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep && this.StrategyZ != PlanogramSubComponentZMerchStrategyType.Manual);

            return GetAxisProcessingOrder(shouldFillX, shouldFillY, shouldFillZ);
        }

        /// <summary>
        /// Processes autofill along the given axis.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="masterPositionPlacementsByRankGtin"></param>
        /// <param name="externalObstructions"></param>
        /// <param name="merchSpace"></param>
        public void ProcessAutofill(AxisType axis, List<PlanogramPositionPlacement> masterPositionPlacementsByRankGtin, List<BoundaryInfo> externalObstructions, PlanogramMerchandisingSpace merchSpace)
        {
            // Holds the list of products that have reached maximum faceup per itteration. 
            List<PlanogramPositionPlacement> positionPlacementsToRemoveFromFaceupProcess = new List<PlanogramPositionPlacement>();
            List<PlanogramPositionPlacement> positionPlacementsByRankGtin = new List<PlanogramPositionPlacement>(masterPositionPlacementsByRankGtin);

            // If we have any positions to faceup then go through and do so until every position would be overfilled if we faced it up more or simply cannot faceup.
            while (positionPlacementsByRankGtin.Any())
            {
                foreach (PlanogramPositionPlacement currentPlacement in positionPlacementsByRankGtin)
                {
                    // Attempt to increase y facings
                    Boolean isFaceupSuccessful = currentPlacement.IncreaseUnits(axis);

                    if (!isFaceupSuccessful)
                    {
                        //if the face up was not successful then our unit count has not changed so just 
                        // Remove our positon from list as we are now no longer able to face up.
                        positionPlacementsToRemoveFromFaceupProcess.Add(currentPlacement);
                        continue;
                    }

                    //Reprocess the group. No squeeze is applied to the direction of that auto fill is processing, as squeeze values should only be applied if required.
                    //Apply Full squeeze rather than partial for best fit. Squeeze will be removed later.
                    this.Process(axis == AxisType.X ? PlanogramSubComponentSqueezeType.NoSqueeze : PlanogramSubComponentSqueezeType.FullSqueeze, axis == AxisType.Y ? PlanogramSubComponentSqueezeType.NoSqueeze : PlanogramSubComponentSqueezeType.FullSqueeze, axis == AxisType.Z ? PlanogramSubComponentSqueezeType.NoSqueeze : PlanogramSubComponentSqueezeType.FullSqueeze);

                    foreach (var normalAxis in GetAxisProcessingOrder())
                    {
                        //Correct any overfill on axis being processed (as the merch type could change)
                        Boolean corrected = TryFixOverfill(normalAxis, externalObstructions, merchSpace,
                            currentPlacement);

                        //Remove our positon from list as we are now no longer able to face up.
                        if (corrected && normalAxis == axis)
                            positionPlacementsToRemoveFromFaceupProcess.Add(currentPlacement);
                    }

                    // if no assortment data(Rank) and the planogram inventory setting has MinDos set about zero
                    // then recalculate the postion order for the next auto fill.
                    // Break added because we only need to auto fill one at a time, recalculate the postion order for each time
                    if (this.SubComponentPlacements.First().Planogram.Assortment.Products.Count == 0
                        && this.SubComponentPlacements.First().Planogram.Inventory.MinDos > 0
                        && positionPlacementsByRankGtin.First().Product.GetPlanogramPerformanceData().UnitsSoldPerDay > 0)
                    {
                        //positionPlacementsByRankGtin = OrderPositionPlacementsByRankThenGtin(PositionPlacements, this.SubComponentPlacements.First().Planogram.Assortment, ignoreManuallyPlaced: true);                        
                        positionPlacementsByRankGtin = OrderPositionPlacementsByRankThenGtin(positionPlacementsByRankGtin, this.SubComponentPlacements.First().Planogram.Assortment, ignoreManuallyPlaced: true);
                        break;
                    }
                }

                // Once we have faced up current itteration remove the positions that have reached their maximum facings high.
                positionPlacementsToRemoveFromFaceupProcess.ForEach(p => positionPlacementsByRankGtin.Remove(p));
            }
        }

        /// <summary>
        /// Checks to see if an axis is overfilled and decreases units untill it isn't
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="externalObstructions"></param>
        /// <param name="merchSpace"></param>
        /// <param name="currentPlacement"></param>
        /// <returns>true if it was originally overfilled</returns>
        private Boolean TryFixOverfill(AxisType axis, List<BoundaryInfo> externalObstructions, PlanogramMerchandisingSpace merchSpace,
            PlanogramPositionPlacement currentPlacement)
        {
            Boolean isOverFilled = IsPlacementGroupOverfilled(axis, currentPlacement, externalObstructions, merchSpace,
                checkBoundaries: false, allowNeededOverhangOnPegs: false);

            //It IS necessary to check overfilled more than once as sometimes one decrease is not enough. esp if squeeze is involved.
            while (isOverFilled)
            {
                // It is so, Decrease again, reprocess and recheck until either we cannot decrease anymore or we are 
                // no longer overfilled.
                if (!currentPlacement.DecreaseUnits(axis)) return false;

                this.Process(
                    axis == AxisType.X
                        ? PlanogramSubComponentSqueezeType.NoSqueeze
                        : PlanogramSubComponentSqueezeType.FullSqueeze,
                    axis == AxisType.Y
                        ? PlanogramSubComponentSqueezeType.NoSqueeze
                        : PlanogramSubComponentSqueezeType.FullSqueeze,
                    axis == AxisType.Z
                        ? PlanogramSubComponentSqueezeType.NoSqueeze
                        : PlanogramSubComponentSqueezeType.FullSqueeze);

                isOverFilled = IsPlacementGroupOverfilled(axis, currentPlacement, externalObstructions, merchSpace,
                    allowNeededOverhangOnPegs: false);
            }

            return true;
        }

        /// <summary>
        /// Gets all the placements that form a placement group with the given <paramref name="targetPlacement"/> in the
        /// given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="targetPlacement"></param>
        /// <returns></returns>
        private List<PlanogramPositionPlacement> GetPlacementGroups(AxisType axis, PlanogramPositionPlacement targetPlacement)
        {
            List<PlanogramPositionPlacement> placements;
            switch (axis)
            {
                case AxisType.X:
                    placements = this.PositionPlacements.Where(p => p.Position.SequenceY == targetPlacement.Position.SequenceY && p.Position.SequenceZ == targetPlacement.Position.SequenceZ).ToList();
                    break;

                case AxisType.Y:
                    placements = this.PositionPlacements.Where(p => p.Position.SequenceX == targetPlacement.Position.SequenceX && p.Position.SequenceZ == targetPlacement.Position.SequenceZ).ToList();
                    break;

                case AxisType.Z:
                    placements = this.PositionPlacements.Where(p => p.Position.SequenceX == targetPlacement.Position.SequenceX && p.Position.SequenceY == targetPlacement.Position.SequenceY).ToList();
                    break;

                default:
                    placements = new List<PlanogramPositionPlacement>();
                    break;
            }
            return placements;
        }

        /// <summary>
        ///     Orders a list of position placements by Rank if an assortment exists and then by product GTIN.
        /// </summary>
        /// <param name="placementsToOrder">The placements you require ordering</param>
        /// <param name="planogramAssortment">An assortment that holds rank for the positions products</param>
        /// <param name="placementsThatShouldBeKept">
        ///     Optional. The positons you want to be included in your filtered list from the
        ///     placementsToOrder.
        /// </param>
        /// <param name="ignoreManuallyPlaced">
        ///     <c>Optional</c> Whether placements with positions having IsManuallyPlaced set should
        ///     be ignored when ranking the placements.
        /// </param>
        /// <remarks>
        ///     placementsThatShouldBeKept allows you to filter positions out of placementsToOrder. Leave null if you wish to
        ///     return all.
        /// </remarks>
        /// <returns>An ordered list of placementsToOrder by Rank then GTIN.</returns>
        public List<PlanogramPositionPlacement> OrderPositionPlacementsByRankThenGtin(IEnumerable<PlanogramPositionPlacement> relevantPlacements, PlanogramAssortment planogramAssortment, List<PlanogramPositionPlacement> placementsThatShouldBeKept = null, Boolean ignoreManuallyPlaced = false)
        {
            //  Determine whether to include manually placed items or not.
            //  This allows manual Fill to work on Manually Placed, 
            ////  while keeping AutoFill from filling those in turn.
            if (ignoreManuallyPlaced)
                relevantPlacements = relevantPlacements.Where(pp => !pp.Position.IsManuallyPlaced);

            // Load a list of position placements which will be increased in order of rank (from assortment) 
            // then by Min Dos
            // then gtin.
            var placementAssortmentPairs = relevantPlacements
                .GroupJoin(planogramAssortment.Products,
                           planogramPositionPlacement => planogramPositionPlacement.Product.Gtin,
                           planogramAssortmentProductList => planogramAssortmentProductList.Gtin,
                           (planogramPositionPlacement, assortmentProducts) =>
                           new { Placement = planogramPositionPlacement, AssortmentProducts = assortmentProducts })
                .SelectMany(firstPair => firstPair.AssortmentProducts.DefaultIfEmpty(), (t, product) => new PlacementAssortmentPair(t.Placement, product));
            var orderedOptions =
                placementAssortmentPairs.OrderBy(RankOrMaxValue)
                                        .ThenBy(DosOrMaxValue)
                                        .ThenBy(arg => arg.Placement.Product.Gtin)
                                        .Select(arg => arg.Placement);

            #region Removal of placements that shouldn't be kept. Optional.

            // If we specify some that should not kept then then only include those placements otherwise we return all.
            if (placementsThatShouldBeKept != null)
            {
                // Create a list and presume we will remove all items from our returned collection
                List<PlanogramPositionPlacement> itemsToRemove = new List<PlanogramPositionPlacement>();
                itemsToRemove = orderedOptions.ToList();

                // Go through and remove any items from the list of items we have specified to remove that we have specified to keep.
                foreach (var itemToKeep in placementsThatShouldBeKept)
                {
                    foreach (var itemToRemove in orderedOptions.ToList())
                    {
                        if (itemToRemove.Id.Equals(itemToKeep.Id)) itemsToRemove.Remove(itemToKeep);
                    }
                }

                //Generate the final list to return.
                List<PlanogramPositionPlacement> listToReturn = orderedOptions.ToList();
                foreach (var itemToKeepSoRemoveFromRemoveList in itemsToRemove)
                {
                    listToReturn.Remove(itemToKeepSoRemoveFromRemoveList);
                }

                return listToReturn;
            }

            #endregion

            return orderedOptions.ToList();
        }

        /// <summary>
        /// Used to recalculate the Achieved Dos value, but only if the plan has a MinDos is greater then 0, if not then return MaxValue
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static Single? DosOrMaxValue(PlacementAssortmentPair arg)
        {
            PlanogramPerformanceData performanceData = arg.Placement.Product.GetPlanogramPerformanceData();
            return
                arg.Placement.Product.Parent.Inventory.MinDos > 0 && performanceData != null && performanceData.UnitsSoldPerDay > 0 && performanceData.AchievedDos.HasValue ?
                (Single)(Math.Round(arg.Placement.Position.TotalUnits / (Decimal)(performanceData.UnitsSoldPerDay), MathHelper.DefaultPrecision)) : Single.MaxValue;
        }

        /// <summary>
        /// Used to return the Assortment Ran value, but only if the Assortment a valid product rank, if not then return MaxValue
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static Int32 RankOrMaxValue(PlacementAssortmentPair arg)
        {
            return arg.AssortmentProduct != null ? arg.AssortmentProduct.Rank : Int32.MaxValue;
        }

        #endregion

        #region Position Placements

        /// <summary>
        /// Returns the position placement for the position with the given id.
        /// </summary>
        public PlanogramPositionPlacement FindPlacementByPositionId(Object positionId)
        {
            return this.PositionPlacements.FirstOrDefault(p => Equals(p.Id, positionId));
        }

        /// <summary>
        /// Creates a new position for the specified product and
        /// inserts the position at the end of the merchandising group
        /// </summary>
        public PlanogramPositionPlacement InsertPositionPlacement(PlanogramProduct product)
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(0, product);
            return this.InsertPositionPlacement(position, product);
        }

        /// <summary>
        /// Creates a new position for the specified product and
        /// inserts the position at the end of the merchandising group, product sequence details also provided.
        /// </summary>
        public PlanogramPositionPlacement InsertPositionPlacement(PlanogramProduct product, Int32 sequenceNumber, Int32 sequenceColour)
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(0, product, null, sequenceNumber, sequenceColour);
            return this.InsertPositionPlacement(position, product);
        }

        /// <summary>
        /// Creates a new position placement and inserts the
        /// position at the end of the merchandising group
        /// </summary>
        public PlanogramPositionPlacement InsertPositionPlacement(PlanogramPosition position, PlanogramProduct product)
        {
            // assert that the specified position does not already exist
            // within this merchandising group
            Debug.Assert(this.FindPlacementByPositionId(position.Id) == null, "The specified position already exists within the merchandising group");

            // get all existing positions within this list
            List<PlanogramPositionPlacement> otherPlacements = this.PositionPlacements.ToList();

            // create a new position placement
            PlanogramPositionPlacement insertPlacement = PlanogramPositionPlacement.NewPlanogramPositionPlacement(position, product, this.SubComponentPlacements.First(), this);

            // start an edit on the placement we are going to insert
            insertPlacement.BeginEdit();

            // add this placement to the position placement list
            this.PositionPlacements.Add(insertPlacement);

            // set the position sequence numbers 
            if (otherPlacements.Count > 0)
            {
                insertPlacement.Position.SequenceX = (Int16)(this.PositionPlacements.Max(p => p.Position.SequenceX) + 1);
                insertPlacement.Position.SequenceY = (Int16)(this.PositionPlacements.Max(p => p.Position.SequenceY) + 1);
                insertPlacement.Position.SequenceZ = (Int16)(this.PositionPlacements.Max(p => p.Position.SequenceZ) + 1);
                insertPlacement.Position.Sequence = (Int16)(this.PositionPlacements.Max(p => p.Position.Sequence) + 1);
                this.CorrectPositionSequencing();
            }
            else
            {
                insertPlacement.Position.SequenceX = 1;
                insertPlacement.Position.SequenceY = 1;
                insertPlacement.Position.SequenceZ = 1;
                insertPlacement.Position.Sequence = 1;
            }

            // return the new position placement
            return insertPlacement;
        }

        /// <summary>
        /// Creates a new position for the specified product and
        /// inserts the position relative to the given anchor
        /// </summary>
        public PlanogramPositionPlacement InsertPositionPlacement(PlanogramProduct product, PlanogramPositionPlacement anchor, PlanogramPositionAnchorDirection direction)
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(0, product);
            return this.InsertPositionPlacement(position, product, anchor, direction);
        }

        /// <summary>
        /// Creates a new position placement and inserts
        /// this position relative to the given anchor
        /// </summary>
        public PlanogramPositionPlacement InsertPositionPlacement(PlanogramPosition position, PlanogramProduct product, PlanogramPositionPlacement anchor, PlanogramPositionAnchorDirection direction)
        {
            // assert that the specified position does not already exist
            // within this merchandising group
            Debug.Assert(this.FindPlacementByPositionId(position.Id) == null, "The specified position already exists within the merchandising group");

            // assert that the specified anchor exists within
            // this merchandising group
            Debug.Assert(this.FindPlacementByPositionId(anchor.Position.Id) != null, "The specified anchor does not exist within the merchandising group");

            // create a new position placement
            PlanogramPositionPlacement insertPlacement = PlanogramPositionPlacement.NewPlanogramPositionPlacement(position, product, this.SubComponentPlacements.First(), this);

            // start an edit on the placement we are going to insert
            insertPlacement.BeginEdit();

            // add this placement to the position placement list
            this.PositionPlacements.Add(insertPlacement);

            // move this position relative to the anchor
            this.MovePositionPlacement(insertPlacement, anchor, direction);
            insertPlacement.EnforceRules(new AssortmentRuleEnforcer());

            // return the position placement
            return insertPlacement;
        }

        /// <summary>
        /// Inserts the given placement into this group, removing it from its original one.
        /// </summary>
        public PlanogramPositionPlacement InsertExistingPositionPlacement(PlanogramPositionPlacement originalPlacement, PlanogramPositionPlacement anchor = null, PlanogramPositionAnchorDirection direction = PlanogramPositionAnchorDirection.ToRight)
        {
            // assert that the specified position does not already exist
            // within this merchandising group
            Debug.Assert(originalPlacement.MerchandisingGroup != this && this.FindPlacementByPositionId(originalPlacement.Position.Id) == null, "The specified position already exists within the merchandising group");

            // assert that the specified anchor exists within
            // this merchandising group
            Debug.Assert(anchor == null || this.FindPlacementByPositionId(anchor.Position.Id) != null, "The specified anchor does not exist within the merchandising group");

            //remove the placement from its existing group.
            originalPlacement.MerchandisingGroup.PositionPlacements.RemoveWithoutDelete(originalPlacement);

            // create a new position placement
            PlanogramPositionPlacement insertPlacement = PlanogramPositionPlacement.NewPlanogramPositionPlacement(originalPlacement.GetOriginalState(), originalPlacement.Product, this.SubComponentPlacements.First(), this);

            // start an edit on the placement we are going to insert
            insertPlacement.BeginEdit();

            //copy the state of the original placement
            if (originalPlacement.EditLevel > 0) insertPlacement.Position.CopyState(originalPlacement.Position);

            // add this placement to the position placement list
            this.PositionPlacements.Add(insertPlacement);

            if (anchor != null)
            {
                // move this position relative to the anchor
                this.MovePositionPlacement(insertPlacement, anchor, direction);
            }
            else
            {
                SetAsNextInSequence(insertPlacement);
            }

            // return the position placement
            return insertPlacement;
        }

        /// <summary>
        /// Sets the new placement as the next in the sequence.
        /// </summary>
        /// <param name="insertPlacement"></param>
        private void SetAsNextInSequence(PlanogramPositionPlacement insertPlacement)
        {
            Int16 maxSequence = 1;
            Int16 maxSequenceX = 1;
            Int16 maxSequenceY = 1;
            Int16 maxSequenceZ = 1;
            if (PositionPlacements.Count > 0)
            {
                maxSequence = PositionPlacements.Max(p => p.Position.Sequence);
                maxSequenceX = PositionPlacements.Max(p => p.Position.SequenceX);
                maxSequenceY = PositionPlacements.Max(p => p.Position.SequenceY);
                maxSequenceZ = PositionPlacements.Max(p => p.Position.SequenceZ);
            }
            //  This is the first placement for this instance, just add it as the first one.
            insertPlacement.Position.Sequence = maxSequence;
            insertPlacement.Position.SequenceX = maxSequenceX;
            insertPlacement.Position.SequenceY = maxSequenceY;
            insertPlacement.Position.SequenceZ = maxSequenceZ;
            CorrectPositionSequencing();
        }

        /// <summary>
        /// Moves the given position placement relative to
        /// the given anchor and updates other placements as required.
        /// </summary>
        public void MovePositionPlacement(PlanogramPositionPlacement placement, PlanogramPositionPlacement anchor, PlanogramPositionAnchorDirection direction)
        {
            // assert that the specified position exists within
            // the merchandising group
            Debug.Assert(this.FindPlacementByPositionId(placement.Position.Id) != null, "The specified position does not exist within the merchandising group");

            // assert that the specified anchor exists within
            // this merchandising group
            Debug.Assert(this.FindPlacementByPositionId(anchor.Position.Id) != null, "The specified anchor does not exist within the merchandising group");

            placement.Position.SequenceX = anchor.Position.SequenceX;
            placement.Position.SequenceY = anchor.Position.SequenceY;
            placement.Position.SequenceZ = anchor.Position.SequenceZ;

            // hang is processed with presentation lines accross y so we need to update 
            // all other placements not just those in the same xz group.
            Boolean allPositionGroups = MerchandisingType == PlanogramSubComponentMerchandisingType.Hang;

            // based on direction
            // set the position sequence values
            switch (direction)
            {
                case PlanogramPositionAnchorDirection.InFront:
                    placement.Position.SequenceZ++;
                    PositionPlacements.ShiftSequenceZFrom(placement);
                    break;

                case PlanogramPositionAnchorDirection.Behind:
                    PositionPlacements.ShiftSequenceZFrom(placement);
                    break;

                case PlanogramPositionAnchorDirection.Above:
                    placement.Position.SequenceY++;
                    PositionPlacements.ShiftSequenceYFrom(placement, allPositionGroups);
                    break;

                case PlanogramPositionAnchorDirection.Below:
                    PositionPlacements.ShiftSequenceYFrom(placement, allPositionGroups);
                    break;

                case PlanogramPositionAnchorDirection.ToLeft:
                    PositionPlacements.ShiftSequenceXFrom(placement);
                    break;

                case PlanogramPositionAnchorDirection.ToRight:
                    placement.Position.SequenceX++;
                    PositionPlacements.ShiftSequenceXFrom(placement);
                    break;
            }

            // correct the position sequence numbers
            this.CorrectPositionSequencing();
        }

        /// <summary>
        /// Moves the position to the given x,y,z coordinates on the component and resequences other placements to achieve this.
        /// NB- Coordinates may be adjusted on process according to the merchandising strategies
        /// </summary>
        /// <param name="placement">the placement to move</param>
        /// <param name="x">the new x</param>
        /// <param name="y">the new y</param>
        /// <param name="z">the new z</param>
        public void MovePositionPlacement(PlanogramPositionPlacement placement, Single x, Single y, Single z)
        {
            // assert that the specified position exists within
            // the merchandising group
            Debug.Assert(this.FindPlacementByPositionId(placement.Position.Id) != null, "The specified position does not exist within the merchandising group");

            //set the coordinates against the placement.
            placement.Position.X = x;
            placement.Position.Y = y;
            placement.Position.Z = z;

            //if this is the only placement then dont bother.
            if (this.PositionPlacements.Count == 1) return;

            //Determine the new sequence numbers for the position:

            //Get X sequence:
            PlanogramPositionPlacement xAnchor =
                this.PositionPlacements.OrderBy(p => p.Position.SequenceX)
               .FirstOrDefault(p => p.Position.X.GreaterOrEqualThan(x));

            Int16 anchorXSeq = (xAnchor != null) ? xAnchor.Position.SequenceX : this.PositionPlacements.Max(p => p.Position.SequenceX);
            if (this.StrategyX == PlanogramSubComponentXMerchStrategyType.Manual
                || !this.GetAxis(AxisType.X).IsStackedStrategy)
            {
                //if the x strategy is manual then place before so that the x coordinate is maintained, 
                placement.Position.SequenceX = anchorXSeq;
            }
            else
            {
                placement.Position.SequenceX = (Int16)(anchorXSeq + 1);
            }
            PositionPlacements.ShiftSequenceXFrom(placement);


            //Get Y sequence:
            PlanogramPositionPlacement yAnchor =
                this.PositionPlacements.OrderBy(p => p.Position.SequenceY)
                .FirstOrDefault(p => p.Position.Y.GreaterOrEqualThan(y));

            Int16 anchorYSeq = (yAnchor != null) ? yAnchor.Position.SequenceY : this.PositionPlacements.Max(p => p.Position.SequenceY);
            if (this.StrategyY == PlanogramSubComponentYMerchStrategyType.Manual
                || !this.GetAxis(AxisType.Y).IsStackedStrategy)
            {
                placement.Position.SequenceY = anchorYSeq;
            }
            else if (this.GetAxis(AxisType.Y).IsStackedStrategy)
            {
                placement.Position.SequenceY = (Int16)(anchorYSeq + 1);
            }
            PositionPlacements.ShiftSequenceYFrom(placement, MerchandisingType == PlanogramSubComponentMerchandisingType.Hang);


            //Get Z sequence:
            PlanogramPositionPlacement zAnchor =
                this.PositionPlacements.OrderBy(p => p.Position.SequenceZ)
                .FirstOrDefault(p => p.Position.Z.GreaterOrEqualThan(z));

            Int16 anchorZSeq = (zAnchor != null) ? zAnchor.Position.SequenceZ : this.PositionPlacements.Max(p => p.Position.SequenceZ);
            if (this.StrategyZ == PlanogramSubComponentZMerchStrategyType.Manual
                || !this.GetAxis(AxisType.Z).IsStackedStrategy)
            {
                placement.Position.SequenceZ = anchorZSeq;
            }
            else if (this.GetAxis(AxisType.Z).IsStackedStrategy)
            {
                placement.Position.SequenceZ = (Int16)(anchorZSeq + 1);
            }
            PositionPlacements.ShiftSequenceZFrom(placement);



            // correct the position sequence numbers
            this.CorrectPositionSequencing();
        }

        /// <summary>
        /// Replaces the specified anchor position with
        /// the specified planogram position placement
        /// </summary>
        public PlanogramPositionPlacement ReplacePositionPlacement(PlanogramPosition position, PlanogramProduct product, PlanogramPositionPlacement anchor)
        {
            // assert that the specified position does not already exist
            // within this merchandising group
            Debug.Assert(this.FindPlacementByPositionId(position.Id) == null, "The specified position already exists within the merchandising group");

            // assert that the specified anchor exists within
            // this merchandising group
            Debug.Assert(this.FindPlacementByPositionId(anchor.Position.Id) != null, "The specified anchor does not exist within the merchandising group");

            // create a new position placement
            PlanogramPositionPlacement replacePlacement = PlanogramPositionPlacement.NewPlanogramPositionPlacement(position, product, this.SubComponentPlacements.First(), this);

            // start an edit on the placement we are going to insert
            replacePlacement.BeginEdit();

            // add this placement to the position placement list
            this.PositionPlacements.Add(replacePlacement);

            // move the position to replace the anchor
            replacePlacement.Position.SequenceX = anchor.Position.SequenceX;
            replacePlacement.Position.SequenceY = anchor.Position.SequenceY;
            replacePlacement.Position.SequenceZ = anchor.Position.SequenceZ;
            replacePlacement.Position.X = anchor.Position.X;
            replacePlacement.Position.Y = anchor.Position.Y;
            replacePlacement.Position.Z = anchor.Position.Z;

            // remove the anchor position from the merchandising group
            this.PositionPlacements.Remove(anchor);

            // correct the sequence numbers
            this.CorrectPositionSequencing();

            // return the placement position
            return replacePlacement;
        }

        /// <summary>
        /// Removes the specified position from the merchandising group
        /// </summary>
        /// <remarks>
        /// Only removes the position from the merchandising group.
        /// Changes to the planogram are only made once the edit has been applied.
        /// </remarks>
        public void RemovePosition(PlanogramPosition position)
        {
            PlanogramPositionPlacement placement = FindPlacementByPositionId(position.Id);
            if (placement != null)
            {
                this.PositionPlacements.Remove(placement);
                this.CorrectPositionSequencing();
            }
        }

        /// <summary>
        /// Removes the specified placement from the merchandising group
        /// </summary>
        /// <remarks>
        /// Only removes the position from the merchandising group.
        /// Changes to the planogram are only made once the edit has been applied.
        /// </remarks>
        public void RemovePositionPlacement(PlanogramPositionPlacement placement)
        {
            if (this.PositionPlacements.Contains(placement))
            {
                this.PositionPlacements.Remove(placement);
                this.CorrectPositionSequencing();
            }
        }

        #endregion

        #region Divider Helpers

        /// <summary>
        /// Indicates whether dividers are place between product facings for this merchandising group.
        /// </summary>
        /// <returns>True if dividers are placed between positions or false if they are placed normally or not at all.</returns>
        public Boolean AreDividersPlacedByFacing()
        {
            return
                this.IsDividerObstructionByFacing &&
                this.DividerWidth.GreaterThan(0) &&
                this.DividerHeight.GreaterThan(0);
        }

        /// <summary>
        /// Indiciates whether dividers are actually placed normally (between positions)
        /// for this merchandising group, for the axis specified.
        /// </summary>
        /// <param name="axis">The merchandising group axis in which divider placements should be evaluated.</param>
        /// <returns>True if dividers are placed normally and false if they are not place normally, or at all.</returns>
        public Boolean AreDividersPlacedNormally(PlanogramMerchandisingGroupAxis axis)
        {
            return axis.DividerSpacing.GreaterThan(0);
            //return
            //    !this.IsDividerObstructionByFacing &&
            //    axis.DividerSpacing.GreaterThan(0) &&
            //    this.DividerWidth.GreaterThan(0) &&
            //    this.DividerHeight.GreaterThan(0) &&
            //    this.DividerDepth.GreaterThan(0);
        }

        /// <summary>
        ///     Gets the correct starting coordinate for dividers on the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        private Single GetDividerStartCoordinate(PlanogramMerchandisingGroupAxis axis, Boolean IgnoreOverhang = false)
        {
            PlanogramSubComponent subComponent = SubComponentPlacements.First().SubComponent;
            Single result;
            Single dividerStart;
            Int32 dividerOffset;

            switch (subComponent.MerchandisingStrategyX)
            {
                case PlanogramSubComponentXMerchStrategyType.Right:
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    Single MaxComponentsWidth = SubComponentPlacements.Sum(placement => placement.SubComponent.Width);

                    if (IgnoreOverhang)
                    {
                        result = MaxComponentsWidth;
                    }else
                    {
                        result = axis.Max - subComponent.DividerObstructionStartX;
                    }
                  
                    dividerStart = MaxComponentsWidth - (subComponent.HasFaceThickness() ? subComponent.FaceThicknessRight : 0) - subComponent.DividerObstructionStartX;
                    dividerOffset = (Int32)Math.Floor((result - dividerStart) / subComponent.DividerObstructionSpacingX);
                    return dividerStart + dividerOffset * subComponent.DividerObstructionSpacingX;
                case PlanogramSubComponentXMerchStrategyType.Left:
                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    if(IgnoreOverhang)
                    {
                        result = subComponent.DividerObstructionStartX;
                    }else
                    {
                        result = axis.Min + subComponent.DividerObstructionStartX;
                    }
             
                    dividerStart = (subComponent.HasFaceThickness() ? subComponent.FaceThicknessLeft : 0) + subComponent.DividerObstructionStartX;
                    dividerOffset = (Int32)Math.Ceiling((result - dividerStart) / subComponent.DividerObstructionSpacingX);
                    return dividerStart + dividerOffset * subComponent.DividerObstructionSpacingX;
                case PlanogramSubComponentXMerchStrategyType.Even:
                    if (IgnoreOverhang)
                    {
                        result = subComponent.DividerObstructionStartX;
                    }
                    else
                    {
                        result =
                        axis.MinOverhang.LessThan(0) ?
                        axis.Min + axis.MinOverhang :
                        axis.Min;
                    }
                    dividerStart = (subComponent.HasFaceThickness() ? subComponent.FaceThicknessLeft : 0);
                    dividerOffset = (Int32)Math.Ceiling((result - dividerStart) / subComponent.DividerObstructionSpacingX);
                    return dividerStart + dividerOffset * subComponent.DividerObstructionSpacingX;
                case PlanogramSubComponentXMerchStrategyType.Manual:
                default:
                    return axis.Min;
            }
        }

        /// <summary>
        /// Returns the size of the dividers on this <see cref="PlanogramMerchandisingGroup"/> in the
        /// given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        private Single GetDividerSize(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return DividerWidth;
                case AxisType.Y:
                    return DividerHeight;
                case AxisType.Z:
                    return DividerDepth;
                default:
                    Debug.Fail($"Unknown axis type: {axis}");
                    return DividerWidth;
            }
        }

        /// <summary>
        /// Returns true if this merch group's layout strategy requires that
        /// dividers be placed between each product facing (x axis only)
        /// </summary>
        /// <returns></returns>
        private Boolean GetIsDividersbyFacing()
        {
            // Ensure we set correct divider values.
            List<PlanogramPosition> planogramPositions = PositionPlacements.Select(p => p.Position).ToList();
            PlanogramSubComponent subComponent = SubComponentPlacements.First().SubComponent;

            // We need to know the minumum sequnce on each axis, which might not be one 
            // if products have been removed since the last time the group was processed.
            // Int16 minSequenceX = planogramPositionsCount == 0 ? (Int16)1 : planogramPositions.Min(p => p.SequenceX);
            Int16 minSequenceY = planogramPositions.Any() ? planogramPositions.Min(p => p.SequenceY) : (Int16)1;
            Int16 minSequenceZ = planogramPositions.Any() ? planogramPositions.Min(p => p.SequenceZ) : (Int16)1;

            // Determine if any products exist at a higher sequence than the minimum.
            // Boolean hasMultiX = planogramPositions.Any(p => p.SequenceX > minSequenceX);
            Boolean hasMultiY = planogramPositions.Any(p => p.SequenceY > minSequenceY);
            Boolean hasMultiZ = planogramPositions.Any(p => p.SequenceZ > minSequenceZ);

            if ((!hasMultiY || subComponent.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Manual)
                &&
                (!hasMultiZ || subComponent.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Manual))
            {
                return subComponent.IsDividerObstructionByFacing;
            }
            return false;
        }

        #endregion

        #endregion

        #region IDisposable

        /// <summary>
        /// Carries out any dispose actions on this class.
        /// </summary>
        public void Dispose()
        {
            //dispose of the position placements
            this.PositionPlacements.Dispose();
        }

        #endregion
    }
}