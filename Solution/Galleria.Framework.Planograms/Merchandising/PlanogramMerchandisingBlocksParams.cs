﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History : CCM80230
// CCM-18462 : A.Kuszyk
//  Created..
#endregion
#endregion

using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// A simple class designed to house the parameters that the <see cref="PlanogramMerchandisingBlocks"/>
    /// object graph requires.
    /// </summary>
    public class PlanogramMerchandisingBlocksParams
    {
        #region Properties

        /// <summary>
        /// The width of the blocking space on the Planogram.
        /// </summary>
        public Single BlockingWidth { get; private set; }

        /// <summary>
        /// The height of the blocking space on the planogram.
        /// </summary>
        public Single BlockingHeight { get; private set; }

        /// <summary>
        /// The blocking height offset due to top down components.
        /// </summary>
        public Single BlockingHeightOffset { get; private set; }

        /// <summary>
        /// The merchandising groups on the planogram.
        /// </summary>
        public IEnumerable<PlanogramMerchandisingGroup> MerchandisingGroups { get; private set; }

        /// <summary>
        /// The blocking from the planogram that is to be evaluated.
        /// </summary>
        public PlanogramBlocking Blocking { get; private set; }

        /// <summary>
        /// Used to enforce assortment rules when merchandising.
        /// </summary>
        public AssortmentRuleEnforcer AssortmentRuleEnforcer { get; private set; }

        /// <summary>
        /// The planogram that the blocking is on.
        /// </summary>
        public Planogram Planogram { get; private set; }

        #endregion

        #region Nested Types

        /// <summary>
        /// Holds parameter information specifically for the <see cref="PlanogramMerchandisingBlock"/> class.
        /// </summary>
        internal class Block : PlanogramMerchandisingBlocksParams
        {
            #region Properties

            /// <summary>
            /// The blocking group to be represented.
            /// </summary>
            public PlanogramBlockingGroup BlockingGroup { get; internal set; }

            /// <summary>
            /// The sequence group to be represented, can be null.
            /// </summary>
            public PlanogramSequenceGroup SequenceGroup { get; internal set; }

            /// <summary>
            /// The design view positions of the merchandising groups.
            /// </summary>
            public Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> MerchandisingGroupsAndDvps { get; internal set; }

            /// <summary>
            /// A reference to the parent <see cref="PlanogramMerchandisingBlocks"/> object.
            /// </summary>
            public PlanogramMerchandisingBlocks ParentBlocks { get; internal set; }
            #endregion


            internal Block() { }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Prevent public construction so that factory methods have to be used.
        /// </summary>
        private PlanogramMerchandisingBlocksParams() { }
        
        #endregion

        #region Factory Methods
        
        /// <summary>
        /// Instantiates a new parameter object for a <see cref="PlanogramMerchandisingBlocks"/> instance.
        /// </summary>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <param name="blockingHeightOffset"></param>
        /// <param name="merchandisingGroups"></param>
        /// <param name="blocking"></param>
        /// <param name="planogram"></param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns></returns>
        public static PlanogramMerchandisingBlocksParams NewBlocksParams(
            Single blockingWidth, 
            Single blockingHeight, 
            Single blockingHeightOffset, 
            IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups,
            PlanogramBlocking blocking,
            Planogram planogram,
            AssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            return new PlanogramMerchandisingBlocksParams()
            {
                Blocking = blocking,
                BlockingWidth = blockingWidth,
                BlockingHeight = blockingHeight,
                BlockingHeightOffset = blockingHeightOffset,
                MerchandisingGroups = merchandisingGroups,
                AssortmentRuleEnforcer = assortmentRuleEnforcer,
                Planogram = planogram,
            };
        }

        /// <summary>
        /// Uses data from this instance to create a new parameters object for a new <see cref="PlanogramMerchandisingBlock"/> instance.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <param name="sequenceGroup"></param>
        /// <param name="merchandisingGroupsAndDvps"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        internal Block NewBlockParams(
            PlanogramBlockingGroup blockingGroup,
            PlanogramSequenceGroup sequenceGroup,
            Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps,
            PlanogramMerchandisingBlocks parent)
        {
            return new Block()
            {
                BlockingGroup = blockingGroup,
                SequenceGroup = sequenceGroup,
                ParentBlocks = parent,
                MerchandisingGroupsAndDvps = merchandisingGroupsAndDvps,
                Blocking = this.Blocking,
                BlockingWidth = this.BlockingWidth,
                BlockingHeight = this.BlockingHeight,
                BlockingHeightOffset = this.BlockingHeightOffset,
                MerchandisingGroups = this.MerchandisingGroups,
                AssortmentRuleEnforcer = this.AssortmentRuleEnforcer,
                Planogram = this.Planogram,
            };
        }
        
        #endregion
    }
}
