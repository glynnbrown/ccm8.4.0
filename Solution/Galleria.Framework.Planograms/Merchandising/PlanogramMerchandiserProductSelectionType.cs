﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Denotes the different options for selecting products for merchandising operations.
    /// </summary>
    public enum PlanogramMerchandiserProductSelectionType
    {
        /// <summary>
        /// Indicates that all products should be selected.
        /// </summary>
        AllProducts = 0,
        /// <summary>
        /// Indicates that only products below the average days of supply should be selected.
        /// </summary>
        ProductsBelowAverageDos = 1,
        /// <summary>
        /// Indicates that only a top percentage of ranked products should be selected.
        /// </summary>
        TopRankedProducts = 2,
    }
}
