﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM802

// V8-29137 : A.Kuszyk
//  Created.

#endregion

#region Version History: CCM811

// V8-29184 : M.Brumby
//  Added ability to create dummy sequence groups if needed.
// V8-30631 : A.Silva
//  Added a check to make sure the colours of every Blocking Group used to generate Planogram Merchandising Blocks is unique (ignoring any ducplicate).

#endregion

#region Version History : CCM 820
// V8-30818 : A.Kuszyk
//  Performance improvement to AnyPositionOverlaps.
#endregion

#region Version History : CCM830
// V8-31804 : A.Kuszyk
//  Added additional constructor to calculate block space from product representation on the planogram.
// V8-32505 : A.Kuszyk
//  Prototyped sequence sub group changes.
// V8-32612 : D.Pleasance
//  Refactored PlanogramMerchandisingBlocks constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
// V8-32870 : A.Kuszyk
//  Updated call to PlanogramMerchandisingBlock constructor for blocking location order fix.
// V8-32787 : A.Silva
//  Added an optional AssortmentRuleEnforcer to the constructors to allow enabling or not rule enforcement.
// V8-32962 : A.Kuszyk
//  Implemented minor performance improvements.
// CCM-18320 : A.Kuszyk
//  Ensured blocks are added in optimisation order.
// CCM-18462 : A.Kuszyk
//  Added params object to constructors. Did not refactor this out into constructor parameters,
//  because this affects 80+ references.
// CCM-18530 : A.Silva
//  Extracted AnyPositionsOverlap method to PlanogramMerchandisingBlockPlacement.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents the space allocated to the blocking on a Planogram, taking into account
    /// merchandisable space and sequence directions. Each blocking group in the given blocking
    /// is represented by one child PlanogramMerchandisingBlock.
    /// </summary>
    public sealed class PlanogramMerchandisingBlocks : IEnumerable<PlanogramMerchandisingBlock>
    {
        #region Fields
        private List<PlanogramMerchandisingBlock> _blocks;
        private Planogram _planogram;

        /// <summary>
        /// A cache of block placements indexed by merchandising group, which is built on request
        /// by the <see cref="GetBlockPlacementsWithSameMerchandisingGroupAs"/> method.
        /// </summary>
        private Dictionary<PlanogramMerchandisingGroup, List<PlanogramMerchandisingBlockPlacement>> blockPlacementsByMerchandisingGroup =
            new Dictionary<PlanogramMerchandisingGroup, List<PlanogramMerchandisingBlockPlacement>>();

        #endregion

        #region Properties
        /// <summary>
        /// The <see cref="Planogram"/> that this object represents the blocking on.
        /// </summary>
        public Planogram Planogram { get { return _planogram; } }
        #endregion

        #region Constructors

        /// <summary>
        /// Instantiates a new collection of <see cref="PlanogramMerchandisingBlock"/>s that represent
        /// the merchandisable block space on the plan, calculating the block space using the positions for
        /// the products in <paramref name="productStacksByBlockColour"/>. Positions are identified as belonging
        /// to blocks based on their <see cref="SequenceColour"/> values. If these are not populated, this constructor
        /// will be unable to correctly populate the blocks.
        /// </summary>
        /// <param name="blocking"></param>
        /// <param name="productStacksByBlockColour"></param>
        /// <param name="merchandisingGroups"></param>
        /// <param name="blockOfInterest">
        /// The <see cref="PlanogramBlockingGroup"/> whose <see cref="PlanogramMerchandisingBlock"/> should include white-space from its <see cref="PlanogramMerchandisingGroup"/>s.
        /// </param>
        /// <param name="canExceedBlockSpace">
        /// True indicates that all available white space will be used to augment a block's size, whereas false 
        /// indicates that white space will be used, but that the <see cref="PlanogramBlocking"/> block space will not be exceeded.
        /// </param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <param name="planogramBlockingWidth"></param>
        /// <remarks>
        /// The blocks are created with the minimum of either their block space as represented by positions, or their block 
        /// space as specified in the <paramref name="blocking"/>. Specifying a <paramref name="blockOfInterest"/> will result
        /// in the given <see cref="PlanogramBlockingGroup"/>'s <see cref="PlanogramMerchandisingBlock"/> including as much
        /// un-merchandised space on the relevant <see cref="PlanogramMerchandisingGroup"/>s as possible.
        /// <para></para>
        /// When additional un-merchandised space is used to augment a <see cref="PlanogramMerchandisingBlock"/>'s space, the
        /// <paramref name="canExceedBlockSpace"/> flag is used to determine whether all space should be used, or just the
        /// un-merchandised space within the block.
        /// </remarks>
        public PlanogramMerchandisingBlocks(
            PlanogramBlocking blocking,
            Dictionary<Int32, Stack<PlanogramProduct>> productStacksByBlockColour,
            IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups,
            Single planogramBlockingWidth,
            Single planogramBlockingHeight,
            Single planogramBlockingHeightOffset,
            PlanogramBlockingGroup blockOfInterest,
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType,
            AssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            Debug.Assert(blocking != null, "blocking cannot be null");
            Debug.Assert(blocking.Parent != null, "blocking.Parent cannot be null");
            Debug.Assert(productStacksByBlockColour != null, "productStacksByBlockColour cannot be null");
            Debug.Assert(merchandisingGroups != null, "merchandisingGroups cannot be null");

            _blocks = new List<PlanogramMerchandisingBlock>();
            _planogram = blocking.Parent;

            // Build a dictionary of merch groups and their dvps for this plan, to pass to the block constructors.
            Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps =
                merchandisingGroups.ToLookupDictionary(m => m, m => new DesignViewPosition(m, planogramBlockingHeightOffset));

            Dictionary<Int32, PlanogramSequenceGroup> sequenceGroupsByColour =
                blocking.Parent.Sequence.Groups.GroupBy(g => g.Colour).Select(g => g.First()).ToDictionary(g => g.Colour, g => g);

            // Build params object. Ideally this would be passed into the constructor, but this constructor
            // is referenced in a lot of places!
            PlanogramMerchandisingBlocksParams blocksParams = PlanogramMerchandisingBlocksParams.NewBlocksParams(
                planogramBlockingWidth,
                planogramBlockingHeight,
                planogramBlockingHeightOffset,
                merchandisingGroups,
                blocking,
                _planogram,
                assortmentRuleEnforcer);

            foreach (PlanogramBlockingGroup blockingGroup in blocking.EnumerateGroupsInOptimisationOrder().ToList())
            {
                Stack<PlanogramProduct> productStack;
                if (!productStacksByBlockColour.TryGetValue(blockingGroup.Colour, out productStack)) continue;

                PlanogramSequenceGroup sequenceGroup;
                if (!sequenceGroupsByColour.TryGetValue(blockingGroup.Colour, out sequenceGroup)) continue;

                _blocks.Add(new PlanogramMerchandisingBlock(
                    blocksParams.NewBlockParams(blockingGroup,sequenceGroup, merchandisingGroupsAndDvps, this),
                    productStack.ToList(),
                    blockingGroup == blockOfInterest,
                    spaceConstraintType));
            }
        }

        /// <summary>
        /// Instantiates a new collection of <see cref="PlanogramMerchandisingBlock"/>s that represent the 
        /// merchandisable block space on the planogram, based on the space allocated in <paramref name="blocking"/>.
        /// </summary>
        /// <param name="blocking">The planogram blocking which will be used to populate the list of Merchandising Blocks.</param>
        /// <param name="sequence">The planogram sequence which will be used to populate the list of Merchandising Blocks.</param>
        /// <param name="merchandisingGroups">The merchandising groups for the planogram being analysed. None of the subcomponents in these groups should be manually merchandised.</param>
        /// <param name="planogramBlockingWidth"></param>
        /// <param name="planogramBlockingHeight"></param>
        /// <param name="planogramBlockingHeightOffset"></param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <param name="canCreateDummySequenceGroups"></param>
        public PlanogramMerchandisingBlocks(
            PlanogramBlocking blocking,
            PlanogramSequence sequence,
            IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups,
            Single planogramBlockingWidth,
            Single planogramBlockingHeight,
            Single planogramBlockingHeightOffset,
            Boolean canCreateDummySequenceGroups = false,
            AssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            if (blocking == null) throw new ArgumentNullException("blocking");
            if (blocking.Groups == null) throw new ArgumentException("blocking.Groups cannot be null");
            if (sequence == null) throw new ArgumentNullException("sequence");
            if (sequence.Groups == null) throw new ArgumentException("sequence.Groups cannot be null");
            if (sequence.Parent == null) throw new ArgumentException("sequence.Parent cannot be null");

            _blocks = new List<PlanogramMerchandisingBlock>();
            _planogram = sequence.Parent;

            // Build a dictionary of the sequence groups by colour so that we can look them up from the blocking groups.
            // V8-30631 : As a precaution, make sure colours are unique. If not, take the first instance of a colour and disregard the rest.
            Dictionary<Int32, PlanogramSequenceGroup> sequenceGroupsByColour =
                sequence.Groups.GroupBy(g => g.Colour).Select(g => g.First()).ToDictionary(g => g.Colour, g => g);

            // Build a dictionary of merch groups and their dvps for this plan, to pass to the block constructors.
            Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps =
                merchandisingGroups.ToLookupDictionary(m => m, m => new DesignViewPosition(m, planogramBlockingHeightOffset));

            // Build params object. Ideally this would be passed into the constructor, but this constructor
            // is referenced in a lot of places!
            PlanogramMerchandisingBlocksParams blocksParams = PlanogramMerchandisingBlocksParams.NewBlocksParams(
                planogramBlockingWidth,
                planogramBlockingHeight,
                planogramBlockingHeightOffset,
                merchandisingGroups,
                blocking,
                _planogram,
                assortmentRuleEnforcer);

            // Iterate over the blocking groups and construct a merchandising block for each.
            // A copy of the Groups collection is taken, because it may be modified by activity in the UI
            // whilst this iteration executes.
            foreach (PlanogramBlockingGroup blockingGroup in blocking.EnumerateGroupsInOptimisationOrder().ToList())
            {
                // Try to get the matching sequence group.
                PlanogramSequenceGroup sequenceGroup;
                if (!sequenceGroupsByColour.TryGetValue(blockingGroup.Colour, out sequenceGroup))
                {
                    if (!canCreateDummySequenceGroups)
                        continue;
                    sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                }

                // Add a new merchandising block to this objects internal list.
                _blocks.Add(new PlanogramMerchandisingBlock(
                    blocksParams.NewBlockParams(blockingGroup, sequenceGroup, merchandisingGroupsAndDvps, this)));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the Block Placements contained within the Merchandising Blocks that have the same merchandising group
        /// as the given Block Placement.
        /// </summary>
        /// <param name="blockPlacement">The Block Placement whose merchandising group should be matched.</param>
        /// <returns></returns>
        internal IEnumerable<PlanogramMerchandisingBlockPlacement> GetBlockPlacementsWithSameMerchandisingGroupAs(
            PlanogramMerchandisingBlockPlacement blockPlacement)
        {
            List<PlanogramMerchandisingBlockPlacement> blockPlacements;
            if (!blockPlacementsByMerchandisingGroup.TryGetValue(blockPlacement.MerchandisingGroup, out blockPlacements))
            {
                blockPlacements = this.SelectMany(b =>
                    b.BlockPlacements.Where(bp =>
                        bp.MerchandisingGroup == blockPlacement.MerchandisingGroup)).ToList();
                blockPlacementsByMerchandisingGroup.Add(blockPlacement.MerchandisingGroup, blockPlacements);
            }
            return blockPlacements.Except(new[] { blockPlacement });
        }

        #endregion

        #region IEnumerable implementation
        public IEnumerator<PlanogramMerchandisingBlock> GetEnumerator()
        {
            return _blocks.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _blocks.GetEnumerator();
        } 
        #endregion
    }
}