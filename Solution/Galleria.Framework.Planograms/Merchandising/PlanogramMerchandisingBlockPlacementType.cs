﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History : CCM830
// CCM-18440 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Denotes the different types of <see cref="PlanogramMerchandisingBlockPlacement"/> objects.
    /// </summary>
    public enum PlanogramMerchandisingBlockPlacementType
    {
        /// <summary>
        /// Indicates that the <see cref="PlanogramMerchandisingBlockPlacement"/> was created using position space bounds.
        /// </summary>
        PositionSpace = 0,

        /// <summary>
        /// Indicates that the <see cref="PlanogramMerchandisingBlockPlacement"/> was created using block space bounds.
        /// </summary>
        BlockSpace = 1,
    }
}
