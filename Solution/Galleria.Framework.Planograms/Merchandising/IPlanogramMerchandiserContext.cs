﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32982 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;
using System.Reflection;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Defines the members required by <see cref="PlanogramMerchandiser"/> in order
    /// to merchandise changes onto plans.
    /// </summary>
    public interface IPlanogramMerchandiserContext
    {
        /// <summary>
        /// The <see cref="PlanogramBlocking"/> in the <see cref="Planogram"/> to use when merchandising.
        /// </summary>
        PlanogramBlocking Blocking { get; }

        /// <summary>
        /// Stacks of products to place, keyed by sequence colour, to be used when merchandising.
        /// </summary>
        Dictionary<Int32, Stack<PlanogramProduct>> ProductStacksBySequenceColour { get; }

        /// <summary>
        /// A dictionary of target units keyed by product gtin and sequence colour, used to 
        /// indicate the units that should be achieved unless they are otherwise specified.
        /// </summary>
        Dictionary<Int32, Dictionary<String, Int32>> TargetUnitsBySequenceColour { get; }

        /// <summary>
        /// The planogram to merchandise.
        /// </summary>
        Planogram Planogram { get; }

        /// <summary>
        /// The merchandising groups to use when merchandising.
        /// </summary>
        PlanogramMerchandisingGroupList MerchandisingGroups { get; }

        /// <summary>
        /// The width of the blocking area.
        /// </summary>
        Single BlockingWidth { get; }

        /// <summary>
        /// The height of the blocking area.
        /// </summary>
        Single BlockingHeight { get; }

        /// <summary>
        /// The height offset of the blocking area.
        /// </summary>
        Single BlockingHeightOffset { get; }

        /// <summary>
        /// The space constraint to apply when merchandising.
        /// </summary>
        PlanogramMerchandisingSpaceConstraintType SpaceConstraint { get; }

        /// <summary>
        /// A dictionary of <see cref="PlanogramBlockingGroup"/>s keyed by their colour.
        /// </summary>
        Dictionary<Int32, PlanogramBlockingGroup> BlockingGroupsByColour { get; }

        /// <summary>
        /// A method to log starting units in some way.
        /// </summary>
        /// <param name="position"></param>
        void LogStartingUnits(PlanogramPositionPlacement position);

        /// <summary>
        /// A method to log final units in some way.
        /// </summary>
        void LogFinalUnits(PlanogramPositionPlacement position);

        /// <summary>
        /// A method to log final units in some way.
        /// </summary>
        /// <param name="gtin"></param>
        /// <param name="units"></param>
        void LogFinalUnits(String gtin, Int32 units);

        List<String> PositionsToIgnore { get; }

        /// <summary>
        /// A dictionary of ranks by product gtins, used for logging purposes.
        /// </summary>
        Dictionary<String, Int32> RanksByProductGtin { get; }

        /// <summary>
        /// A dictionary of planogram performance data keyed by the id of the product it relates to.
        /// </summary>
        Dictionary<Object, PlanogramPerformanceData> PerformanceDataByProductId { get; }

        /// <summary>
        /// Indicates whether inventory changes should be made by facings or units.
        /// </summary>
        PlanogramMerchandisingInventoryChangeType InventoryChangeType { get; }
    }
}
