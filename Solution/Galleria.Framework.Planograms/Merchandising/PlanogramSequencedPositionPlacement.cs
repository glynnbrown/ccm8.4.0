﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31004 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Merchandising
{
    public sealed class PlanogramSequencedPositionPlacement
    {
        #region Properties
        /// <summary>
        /// The id of the sequenced position item
        /// </summary>
        public IPlanogramSubComponentSequencedItem Position { get; private set; }

        /// <summary>
        /// True indicates that the blocking group's primary direction should be reversed.
        /// </summary>
        public Boolean IsPrimaryReversed { get; private set; }

        /// <summary>
        /// True indicates that the blocking group's secondary direction should be reversed.
        /// </summary>
        public Boolean IsSecondaryReversed { get; private set; }

        /// <summary>
        /// True indicates that the blocking group's tertiary direction should be reversed.
        /// </summary>
        public Boolean IsTertiaryReversed { get; private set; }

        #endregion

        #region Constructor
        public PlanogramSequencedPositionPlacement(
            IPlanogramSubComponentSequencedItem position, 
            Boolean isBlockPrimaryPlacementTypeReversed, 
            Boolean isBlockSecondaryPlacementTypeReversed, 
            Boolean isBlockTertiaryPlacementTypeReversed)
        {
            Position = position;
            IsPrimaryReversed = isBlockPrimaryPlacementTypeReversed;
            IsSecondaryReversed = isBlockSecondaryPlacementTypeReversed;
            IsTertiaryReversed = isBlockTertiaryPlacementTypeReversed;
        } 
        #endregion

        public Boolean GetIsReversed(Enums.AxisType axisType, Model.PlanogramBlockingGroup planogramBlockingGroup)
        {
            switch (axisType)
            {
                case Galleria.Framework.Enums.AxisType.X:
                    if (planogramBlockingGroup.BlockPlacementPrimaryType.IsX()) return IsPrimaryReversed;
                    if (planogramBlockingGroup.BlockPlacementSecondaryType.IsX()) return IsSecondaryReversed;
                    if (planogramBlockingGroup.BlockPlacementTertiaryType.IsX()) return IsTertiaryReversed;
                    break;
                case Galleria.Framework.Enums.AxisType.Y:
                    if (planogramBlockingGroup.BlockPlacementPrimaryType.IsY()) return IsPrimaryReversed;
                    if (planogramBlockingGroup.BlockPlacementSecondaryType.IsY()) return IsSecondaryReversed;
                    if (planogramBlockingGroup.BlockPlacementTertiaryType.IsY()) return IsTertiaryReversed;
                    break;
                case Galleria.Framework.Enums.AxisType.Z:
                    if (planogramBlockingGroup.BlockPlacementPrimaryType.IsZ()) return IsPrimaryReversed;
                    if (planogramBlockingGroup.BlockPlacementSecondaryType.IsZ()) return IsSecondaryReversed;
                    if (planogramBlockingGroup.BlockPlacementTertiaryType.IsZ()) return IsTertiaryReversed;
                    break;
            }

            return IsPrimaryReversed;
        }
    }
}
