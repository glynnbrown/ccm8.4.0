﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27485 : A.Kuszyk
//	Moved from Framework.Planograms assembly.
// V8-27536 : A.Kuszyk
//  Changed method used to pick up snappable boundaries to be fixture independent.
// V8-27585 : A.Kuszyk
//  Changed selection of boundaries such that peg holes and component dividers are only
//  picked up when the divider is over the sub-component and, in this case, edges are ignored.
// V8-27646 : A.Kuszyk
//  Added Order to SnappableBoundaries. Removed no-leap-frog rule from Sanitise().
// V8-27667 : A.Kuszyk
//  Changed all Single equality comparisons to use Framework extension methods.
// V8-27509 : A.Kuszyk
//  Re-factored to take account of Assembly Components.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved into common namespace.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Moved to framework
// V8-30188 : A.Kuszyk
//  Updated SanitiseList to exclude 0 and 1.
#endregion
#region Version History : CCM830
// V8-32018 : A.Kuszyk
//  Amended Sanitise method to take order from property. Refactored factory method into constructor.
// V8-32449 : A.Kuszyk
//  Amended constructor to allow snapped dividers to be passed in.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Indicates what order snapping positions should be evaluated in.
    /// </summary>
    public enum PlanogramBlockingDividerSnappingPositionsOrder
    {
        EdgesDividersPegs,
        DividersPegsEdges
    }

    /// <summary>
    /// A class that holds lists of boundaries that can be snapped to by a divider, which are accessible
    /// in an order of precedence.
    /// </summary>
    public sealed class PlanogramBlockingDividerSnappingPositions
    {
        #region Fields
        private List<Single> _currentList;
        #endregion

        #region Properties

        /// <summary>
        /// Boundaries found on the edge of a component.
        /// </summary>
        public List<Single> Edges { get; private set; }

        /// <summary>
        /// Boundaries found at divider positions, within a subcomponent.
        /// </summary>
        public List<Single> Dividers { get; private set; }

        /// <summary>
        /// Boundaries found at peg hole positions, within a subcomponent.
        /// </summary>
        public List<Single> Pegs { get; private set; }

        /// <summary>
        /// Indicates whether all of the boundaries stored can be ignored or not.
        /// </summary>
        public Boolean CanIgnoreSnappingPositions { get; set; }

        /// <summary>
        /// Indicates the order in which the lists should be returned.
        /// </summary>
        public PlanogramBlockingDividerSnappingPositionsOrder Order { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance with lists populated from those provided.
        /// </summary>
        internal PlanogramBlockingDividerSnappingPositions(
            IEnumerable<Single> edges, 
            IEnumerable<Single> dividers, 
            IEnumerable<Single> pegs, 
            Boolean canIgnoreBoundaries, 
            PlanogramBlockingDividerSnappingPositionsOrder order)
        {
            Edges = edges.ToList();
            Dividers = dividers.ToList();
            Pegs = pegs.ToList();
            CanIgnoreSnappingPositions = canIgnoreBoundaries;
            Order = order;
        }

        /// <summary>
        /// Gets a list of percentage positions that represent the boundaries of components
        /// that can be snapped to. 
        /// </summary>
        /// <returns>A list of values that represent the percentage positions of component boundaries.</returns>
        /// <remarks>
        /// This method selects boundaries based on the MerchandisingType of the merchandisable sub-components of
        /// the components that the divider is over. The list that is returned is cleaned
        /// to ensure that only boundaries between this divider and another divider are returned and that boundaries
        /// already occupied by another divider are removed.
        /// </remarks>
        /// <exception cref="InvalidOperationException">Thrown if the Parent property is null.</exception>
        public PlanogramBlockingDividerSnappingPositions(
            PlanogramBlockingDivider divider, 
            Single planWidth, 
            Single planHeight, 
            Single planHeightOffset, 
            IEnumerable<PlanogramBlockingDivider> snappedDividers = null,
            Boolean removeBoundaries = true)
        {
            if (divider.Parent == null || divider.Parent.Parent == null)
                throw new InvalidOperationException("Parent cannot be null");
            Order = PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs;
            Edges = new List<Single>();
            Pegs = new List<Single>();
            Dividers = new List<Single>();
            Dictionary<PlanogramSubComponent, DesignViewPosition> snappablePositionsBySubComponent = new Dictionary<PlanogramSubComponent, DesignViewPosition>();

            // Get boundaries for each component that overlaps divider and is merchandisable.
            Dictionary <IPlanogramFixtureComponent, DesignViewPosition> snappablePositionsByComponent =
                GetSnappingPositionsByComponent(divider, planWidth, planHeight, planHeightOffset);

            IEnumerable<IPlanogramFixtureComponent> orderedComponents = snappablePositionsByComponent.Keys.OrderBy(c => c.Y);
            PlanogramSubComponent prevSubComponent = null;

            // Iterate over the IPlanogramComponents (these could be FixtureComponents or
            // AssemblyComponents) and calculate the snappable positions for each.
            foreach (IPlanogramFixtureComponent component in orderedComponents)
            {
                PlanogramComponent planogramComponent = component.GetPlanogramComponent();

                // Get the design-view relative component position
                DesignViewPosition componentPosition = snappablePositionsByComponent[component];

                // Get all the sub-components in this component that can be merchandised.
                List<PlanogramSubComponent> subComponents = planogramComponent.SubComponents.
                    Where(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None).ToList();

                subComponents.ForEach(s => snappablePositionsBySubComponent[s] = new DesignViewPosition(s, componentPosition));

                //// Get dictionary of sub-components and their design-view positions.
                //Dictionary<PlanogramSubComponent, DesignViewPosition> snappablePositionsBySubComponent =
                //    subComponents.ToDictionary(s => s, s => new DesignViewPosition(s, componentPosition));

                // Set CanIgnoreBoundaries flag if divider is over component and component is
                // merchandised top down
                CanIgnoreSnappingPositions =
                    divider.IsOverDesignViewPosition(componentPosition, planWidth, planHeight) &&
                    planogramComponent.IsMerchandisedTopDown &&
                    divider.GetMinDistanceToNeighbour() > Constants.MinDistanceToNeighbour;

                // Calculate edge, divider and peg hole boundaries.
                Edges.AddRange(GetEdgeBoundaries(divider, subComponents, snappablePositionsBySubComponent, planWidth, planHeight, prevSubComponent));
                Dividers.AddRange(GetDividerBoundaries(
                    divider, 
                    subComponents, 
                    snappablePositionsBySubComponent, 
                    planWidth, 
                    planHeight, 
                    planogramComponent.IsMerchandisedTopDown));
                Pegs.AddRange(GetPegBoundaries(divider, subComponents, snappablePositionsBySubComponent, planWidth, planHeight));

                prevSubComponent = subComponents.OrderByDescending(s => s.Y).FirstOrDefault();

                // If the order has already been changed, then we're finished with this component.
                if (Order == PlanogramBlockingDividerSnappingPositionsOrder.DividersPegsEdges) continue;

                // If the order hasn't yet been changed, we need to check to see if it should be.
                foreach (
                    KeyValuePair<PlanogramSubComponent, DesignViewPosition> dvpBySubComponent in
                        snappablePositionsBySubComponent)
                {
                    if (SubComponentHasSnappablePegsOrDividers(dvpBySubComponent.Key) &&
                        divider.IsOverDesignViewPosition(dvpBySubComponent.Value, planWidth, planHeight))
                    {
                        Order = PlanogramBlockingDividerSnappingPositionsOrder.DividersPegsEdges;
                        break;
                    }
                }
            }

            // Return a set of boundaries cleaned of duplicates and other eroneous values.
            IEnumerable<PlanogramBlockingDivider> snappedDividersOfSameType = 
                (snappedDividers == null ? divider.Parent.Dividers : snappedDividers)
                .Where(d => d.Type == divider.Type).ToList();
            Edges = SanitiseList(Edges, snappedDividersOfSameType, divider, removeBoundaries).ToList();
            Dividers = SanitiseList(Dividers, snappedDividersOfSameType, divider, removeBoundaries).ToList();
            Pegs = SanitiseList(Pegs, snappedDividersOfSameType, divider, removeBoundaries).ToList();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns all of the boundaries identified in <see cref="Edges"/>, <see cref="Pegs"/> and <see cref="Dividers"/>
        /// in one flattened list of <see cref="Single"/>s.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Single> GetFlattenedBoundaries()
        {
            return Edges.Union(Pegs).Union(Dividers).ToList();
        }

        /// <summary>
        /// Retrieves the first list in the precendence order.
        /// </summary>
        /// <returns></returns>
        public List<Single> GetFirstList()
        {
            _currentList = null;
            return GetNextList();
        }

        /// <summary>
        /// Gets the next list in the precedence order.
        /// </summary>
        /// <returns></returns>
        public List<Single> GetNextList()
        {
            if (Order == PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs)
            {
                if (_currentList == null) _currentList = Edges;
                else if (_currentList == Edges) _currentList = Dividers;
                else if (_currentList == Dividers) _currentList = Pegs;
                else if (_currentList == Pegs) return null;
                return _currentList.ToList();
            }
            else
            {
                if (_currentList == null) _currentList = Dividers;
                else if (_currentList == Edges) _currentList = Pegs;
                else if (_currentList == Dividers) _currentList = Edges;
                else if (_currentList == Pegs) return null;
                return _currentList.ToList();
            }
        }

        /// <summary>
        /// Cleans all of the lists for invalid positions and returns a new, cleaned set of lists.
        /// </summary>
        /// <param name="divider">The Blocking Divider for which these boundaries should be sanitised.</param>
        /// <returns>A new, sanitised set of boundaries.</returns>
        /// <exception cref="ArgumentException">Thrown if the divider's parent is null.</exception>
        public PlanogramBlockingDividerSnappingPositions Sanitise(PlanogramBlockingDivider divider)
        {
            if (divider.Parent == null) throw new ArgumentException("divider Parent cannot be null");

            // Position cannot be occupied by another divider that overlaps with this one.
            var dividersOfSameType = divider.Parent.Dividers.Where(d => d.Type == divider.Type);
            return new PlanogramBlockingDividerSnappingPositions(
                SanitiseList(Edges, dividersOfSameType, divider),
                SanitiseList(Dividers, dividersOfSameType, divider),
                SanitiseList(Pegs, dividersOfSameType, divider),
                CanIgnoreSnappingPositions,
                Order);
        }

        private IEnumerable<Single> SanitiseList(
            IEnumerable<Single> dirtyList,
            IEnumerable<PlanogramBlockingDivider> dividersOfSameType,
            PlanogramBlockingDivider divider,
            Boolean removeBoundaries = true)
        {
            // First, remove any duplicate values and ensure that 0 and 1 are removed.
            List<Single> cleanList =
                removeBoundaries ?
                dirtyList.Distinct().Where(i => !i.EqualTo(0) && !i.EqualTo(1)).ToList() :
                dirtyList.Distinct().ToList();

            // Now, check the original dirty list for values that are already occupied.
            foreach (Single pos in dirtyList.Distinct())
            {
                // Get dividers that are at the same X/Y position that might overlap.
                IEnumerable<PlanogramBlockingDivider> potentiallyOverlappingDividers = dividersOfSameType
                    .Where(d => (d.Type == PlanogramBlockingDividerType.Horizontal ? d.Y : d.X).EqualTo(pos, 4));

                // For each of these dividers, check to see if it actually overlaps.
                foreach (PlanogramBlockingDivider potentiallyOverlappingDivider in potentiallyOverlappingDividers)
                {
                    Boolean dividerDoesOverlap = potentiallyOverlappingDivider
                        .GetOverlap(divider)
                        .GreaterThan(Constants.MinPercentageToConsiderOverlap, 4);

                    // If it does overlap, remove it from the clean list (if it still appears).
                    if (dividerDoesOverlap)
                    {
                        if (cleanList.Contains(pos))
                        {
                            cleanList.Remove(pos);
                            break;
                        }
                    }
                }
            }
            return cleanList;
        }

        /// <summary>
        /// Returns a copy of this instance containing the same <see cref="Edges"/>, <see cref="Dividers"/>, <see cref="Pegs"/> and other
        /// properties.
        /// </summary>
        /// <returns></returns>
        public PlanogramBlockingDividerSnappingPositions Copy()
        {
            return new PlanogramBlockingDividerSnappingPositions(this.Edges, this.Dividers, this.Pegs, this.CanIgnoreSnappingPositions, this.Order);
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Gets the relevant divider boundaries on the given sub-components of the given component.
        /// </summary>
        /// <param name="divider">The Blocking Divider we are finding boundaries for.</param>
        /// <param name="subComponents">The sub-components whose dividers should be evaluated.</param>
        /// <param name="subComponentPositions">A dictionary of relative sub-component positions.</param>
        /// <param name="planogramWidth">The total flattened planogram width.</param>
        /// <param name="planogramHeight">The total planogram height.</param>
        /// <param name="fixtureComponentPosition">The planogram relative component co-ordinates.</param>
        /// <param name="isFixtureComponentTopDown">Indicates whether the fixture component is merchandised top down.</param>
        /// <returns>A collection of relevant divider boundaries.</returns>
        private static IEnumerable<Single> GetDividerBoundaries(
            PlanogramBlockingDivider divider,
            IEnumerable<PlanogramSubComponent> subComponents,
            Dictionary<PlanogramSubComponent, DesignViewPosition> subComponentPositions,
            Single planogramWidth,
            Single planogramHeight,
            Boolean isFixtureComponentTopDown)
        {
            var returnList = new List<Single>();
            foreach (var subComponent in subComponents)
            {
                DesignViewPosition subComponentPosition;
                if (!subComponentPositions.TryGetValue(subComponent, out subComponentPosition)) continue;

                // V8-27585
                // Ignore sub-component dividers unless the blocking divider is over this sub-component.
                if (!divider.IsOverDesignViewPosition(subComponentPosition, planogramWidth, planogramHeight)) continue;

                if (divider.Type == PlanogramBlockingDividerType.Horizontal)
                {
                    if (isFixtureComponentTopDown)
                    {
                        returnList.AddRange(
                            DesignViewHelper.GetDividerTopDownPositions(subComponent, subComponentPosition).
                                Select(y => planogramHeight.EqualTo(0) ? 0 : y/planogramHeight));
                    }
                    else
                    {
                        returnList.AddRange(
                            DesignViewHelper.GetDividerYPositions(subComponent, subComponentPosition).
                                Select(y => planogramHeight.EqualTo(0) ? 0 : y/planogramHeight));
                    }
                }
                else
                {
                    returnList.AddRange(
                        DesignViewHelper.GetDividerXPositions(subComponent, subComponentPosition).
                            Select(x => planogramWidth.EqualTo(0) ? 0 : x/planogramWidth));
                }
            }
            return returnList.Distinct();
        }

        /// <summary>
        /// Gets the relevant edge boundaries for the given Component, by evaluating its given merchandisable
        /// sub-components.
        /// </summary>
        /// <param name="divider">The Blocking Divider we are finding boundaries for.</param>
        /// <param name="subComponents">The merchandisable sub-components of the component.</param>
        /// <param name="subComponentPositions">A dictionary of relative sub-component positions.</param>
        /// <param name="planogramWidth">The total flattened planogram width.</param>
        /// <param name="planogramHeight">The total planogram height.</param>
        /// <param name="fixtureComponentPosition">The relative position of the fixture component to which the sub-components belong.</param>
        /// <returns>A collection of edge boundaries.</returns>
        /// <param name="subComponentBelow">the</param>
        private static IEnumerable<Single> GetEdgeBoundaries(
            PlanogramBlockingDivider divider,
            IEnumerable<PlanogramSubComponent> subComponents,
            Dictionary<PlanogramSubComponent, DesignViewPosition> subComponentPositions,
            Single planogramWidth,
            Single planogramHeight,
            PlanogramSubComponent subComponentBelow)
        {
            var snappableBoundaries = new List<Single>();
            IEnumerable<PlanogramSubComponent> orderedSubComponents = subComponents.OrderBy(s => s.Y).ToList();
            
            foreach (PlanogramSubComponent subComponent in orderedSubComponents)
            {
                DesignViewPosition subComponentPosition;
                if (!subComponentPositions.TryGetValue(subComponent, out subComponentPosition)) continue;

                if (subComponentBelow != null && divider.Type == PlanogramBlockingDividerType.Horizontal)
                {
                    DesignViewPosition prevSubComponentPosition = null;
                    if (subComponentPositions.TryGetValue(subComponentBelow, out prevSubComponentPosition))
                    {
                        //If we are moving from as stacking component to a hanging component then we might want to
                        //create a snapping position between them. Currently the only way to do this is to set the
                        //snapping position to the merch height of the stacking component if it has one. If there
                        //is no merch height then we don't add a snapping position as the user has given free reign
                        //to the engine.
                        if (subComponentBelow.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack &&
                            subComponentBelow.MerchandisableHeight > 0 &&
                            (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom ||
                            subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang))
                        {
                            Single midboundaryPosition = planogramHeight.EqualTo(0)
                                           ? 0
                                           : (prevSubComponentPosition.BoundY + prevSubComponentPosition.BoundHeight + subComponentBelow.MerchandisableHeight) / planogramHeight;

                            Single currentLowerBoundaryPosition = planogramHeight.EqualTo(0)
                                            ? 0
                                            : (subComponentPosition.BoundY + subComponentPosition.BoundHeight) / planogramHeight;

                            //ensure we don't create a boundary position above our current subcomponent
                            if (midboundaryPosition.LessThan(currentLowerBoundaryPosition))
                            {
                                snappableBoundaries.Add(
                                           planogramHeight.EqualTo(0)
                                               ? 0
                                               : (prevSubComponentPosition.BoundY + prevSubComponentPosition.BoundHeight + subComponentBelow.MerchandisableHeight) / planogramHeight);
                            }
                        }
                    }

                }
                // V8-27585
                // Skip sub-component if it has pegs or dividers and is not Stack and the
                // divider is positioned over it. This ensures that only peg or divider boundaries
                // are picked up when the blocking divider is over a component like a peg board.
                if (SubComponentHasSnappablePegsOrDividers(subComponent) &&
                    divider.IsOverDesignViewPosition(subComponentPosition, planogramWidth, planogramHeight)) continue;

                // For horizontal dividers, select boundaries based on the merchandising style.
                if (divider.Type == PlanogramBlockingDividerType.Horizontal)
                {
                    // Boundary is at the top of the sub-component when products hang from the bottom
                    // (e.g. a rod or bar).
                    if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
                    {
                        snappableBoundaries.Add(
                            planogramHeight.EqualTo(0)
                                ? 0
                                : (subComponentPosition.BoundY + subComponentPosition.BoundHeight)/planogramHeight);
                    }
                    else
                    // Boundary is at the bottom of the sub-component when products hang from the side
                    // (e.g. a clipstrip) or are stacked on top (e.g. a shelf).
                    {
                        snappableBoundaries.Add(planogramHeight.EqualTo(0)
                            ? 0
                            : subComponentPosition.BoundY/planogramHeight);
                    }
                }
                else // For Vertical dividers, select boundaries based on the X positions of the sub-component.
                {
                    snappableBoundaries.Add(planogramWidth.EqualTo(0) ? 0 : subComponentPosition.BoundX/planogramWidth);
                    snappableBoundaries.Add(
                        planogramWidth.EqualTo(0)
                            ? 0
                            : (subComponentPosition.BoundX + subComponentPosition.BoundWidth)/planogramWidth);
                }


                subComponentBelow = subComponent;
            }

            return snappableBoundaries;
        }

        private static Boolean SubComponentHasSnappablePegsOrDividers(PlanogramSubComponent subComponent)
        {
            Boolean subComponentHasDividers =
                subComponent.DividerObstructionSpacingX.GreaterThan(0) ||
                subComponent.DividerObstructionSpacingY.GreaterThan(0) ||
                subComponent.DividerObstructionSpacingZ.GreaterThan(0);
            Boolean subComponentHasPegs =
                subComponent.MerchConstraintRow1SpacingX.GreaterThan(0) ||
                subComponent.MerchConstraintRow1SpacingY.GreaterThan(0) ||
                subComponent.MerchConstraintRow2SpacingX.GreaterThan(0) ||
                subComponent.MerchConstraintRow2SpacingY.GreaterThan(0);
            return
                subComponentHasDividers ||
                (subComponentHasPegs && subComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.Stack);
        }

        /// <summary>
        /// Gets the relevant peg boundaries for the give sub-component.
        /// </summary>
        /// <param name="divider">The Blocking Divider we are finding boundaries for.</param>
        /// <param name="subComponents">The sub-components whose peg boundaries should be found.</param>
        /// <param name="subComponentPositions">A dictionary of relative sub-component positions.</param>
        /// <param name="planogramWidth">The total width of the planogram.</param>
        /// <param name="planogramHeight">The total height of the planogram.</param>
        /// <param name="fixtureComponentPosition">The relative position of the fixture component to which the sub-components belong.</param>
        /// <returns>A collection of peg boundaries.</returns>
        private static IEnumerable<Single> GetPegBoundaries(
            PlanogramBlockingDivider divider,
            IEnumerable<PlanogramSubComponent> subComponents,
            Dictionary<PlanogramSubComponent, DesignViewPosition> subComponentPositions,
            Single planogramWidth,
            Single planogramHeight)
        {
            var returnList = new List<Single>();

            foreach (var subComponent in subComponents)
            {
                DesignViewPosition subComponentPosition;
                if (!subComponentPositions.TryGetValue(subComponent, out subComponentPosition)) continue;

                // V8-27585
                // Ignore peg holes unless the divider is over this sub-component.
                if (!divider.IsOverDesignViewPosition(subComponentPosition, planogramWidth, planogramHeight)) continue;

                var pegHolePositions = DesignViewHelper.GetPegHolePositions(subComponent, subComponentPosition);
                returnList.AddRange(
                    divider.Type == PlanogramBlockingDividerType.Horizontal
                        ? pegHolePositions.Select(p => planogramHeight.EqualTo(0) ? 0 : p.Y/planogramHeight)
                        : pegHolePositions.Select(p => planogramWidth.EqualTo(0) ? 0 : p.X/planogramWidth));
            }

            return returnList.Distinct();
        }

        /// <summary>
        /// Gets a collection of IPlanogramFixtureComponents (which could represent either Fixture Components or
        /// Fixture Assembly Components), and their positions, that overlap this divider in its dimension, which 
        /// are therefore eligible for a snapping operation.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if divider.Parent or divider.Parent.Parent is null.</exception>
        private static Dictionary<IPlanogramFixtureComponent, DesignViewPosition> GetSnappingPositionsByComponent(
            PlanogramBlockingDivider divider,
            Single planogramWidth,
            Single planogramHeight,
            Single topDownHeightOffset)
        {
            if (divider.Parent == null || divider.Parent.Parent == null)
                throw new ArgumentException("divider.Parent or divider.Parent.Parent cannot be null");
            var planogram = divider.Parent.Parent;
            var returnDictionary = new Dictionary<IPlanogramFixtureComponent, DesignViewPosition>();

            // Iterate over every fixture item in the planogram looking for snappable
            // components. These can either be FixtureComponents or AssemblyComponents.
            foreach (var fixtureItem in planogram.FixtureItems)
            {
                var fixture = fixtureItem.GetPlanogramFixture();

                // Build dictionary of fixture/assembly components and their design view positions.
                var positionsByComponent = new Dictionary<IPlanogramFixtureComponent, DesignViewPosition>();
                foreach (var fixtureComponent in fixture.Components)
                {
                    if (positionsByComponent.ContainsKey(fixtureComponent)) continue;
                    positionsByComponent.Add(
                        fixtureComponent,
                        new DesignViewPosition(fixtureComponent, fixtureItem, topDownHeightOffset));
                }
                foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies)
                {
                    foreach (
                        PlanogramAssemblyComponent assemblyComponent in
                            fixtureAssembly.GetPlanogramAssembly().Components)
                    {
                        if (positionsByComponent.ContainsKey(assemblyComponent)) continue;
                        positionsByComponent.Add(
                            assemblyComponent,
                            new DesignViewPosition(assemblyComponent, fixtureItem, fixtureAssembly, topDownHeightOffset));
                    }
                }

                // Review the Fixture's FixtureComponents and FixtureAssemblyComponents.
                foreach (var kvp in positionsByComponent)
                {
                    IPlanogramFixtureComponent fixtureComponent = kvp.Key;
                    DesignViewPosition fixtureComponentPosition = kvp.Value;

                    // Check that component is merchandisable before adding position.
                    var component = fixtureComponent.GetPlanogramComponent();
                    if (component == null || !component.IsMerchandisable) continue;

                    if (divider.Type == PlanogramBlockingDividerType.Horizontal)
                    {
                        var relativeX = planogramWidth.EqualTo(0) ? 0 : fixtureComponentPosition.BoundX/planogramWidth;
                        var relativeLength = planogramWidth.EqualTo(0)
                            ? 0
                            : fixtureComponentPosition.BoundWidth/planogramWidth;
                        if (
                            BlockingHelper.GetOverlap(divider.X, divider.Length, relativeX, relativeLength)
                                .GreaterThan(Constants.MinPercentageToConsiderOverlap))
                        {
                            returnDictionary.Add(fixtureComponent, fixtureComponentPosition);
                        }
                    }
                    else
                    {
                        var relativeY = planogramHeight.EqualTo(0) ? 0 : fixtureComponentPosition.BoundY/planogramHeight;
                        var relativeHeight = planogramHeight.EqualTo(0)
                            ? 0
                            : fixtureComponentPosition.BoundHeight/planogramHeight;
                        if (
                            BlockingHelper.GetOverlap(divider.Y, divider.Length, relativeY, relativeHeight)
                                .GreaterThan(Constants.MinPercentageToConsiderOverlap))
                        {
                            returnDictionary.Add(fixtureComponent, fixtureComponentPosition);
                        }
                    }
                }
            }

            return returnDictionary;
        }

        #endregion
    }
}
