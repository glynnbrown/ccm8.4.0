﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27692 : A.Kuszyk
//	Created.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved to common namespace.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Moved to framework
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    public class PlanogramBlockingException : Exception
    {
        #region Properties

        public PlanogramEventLogEntryType LogEntryType { get; private set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramBlockingException(String message, PlanogramEventLogEntryType entryType)
            : base(message)
        {
            LogEntryType = entryType;
        }

        #endregion
    }
}
