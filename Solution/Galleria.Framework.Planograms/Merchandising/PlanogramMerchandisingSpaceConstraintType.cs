﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32612 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Text;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Space constraint types. Determines how block space is constrained. (To Within Block space, Beyond block space, Within block presentation)
    /// </summary>
    public enum PlanogramMerchandisingSpaceConstraintType
    {
        /// <summary>
        /// Block space is calculated to the bounds of the blocking or the positions that the blocking represents.
        /// </summary>
        WithinBlockSpace = 0,
        /// <summary>
        /// Calculates block space using all available whitespace within the merchandising group
        /// </summary>
        BeyondBlockSpace = 1,
        /// <summary>
        /// Calculates the minimum white space gain within compact placement groupings
        /// </summary>
        WithinBlockPresentation = 2
    }
}