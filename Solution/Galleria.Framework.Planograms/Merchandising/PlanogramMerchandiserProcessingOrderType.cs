﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Denotes the different ways products can be ordered in merchandising operations.
    /// </summary>
    public enum PlanogramMerchandiserProcessingOrderType
    {
        /// <summary>
        /// Indicates that products should be ordered in ascending achieved DOS order.
        /// </summary>
        ByAchievedDos = 0,
        /// <summary>
        /// Indicates that products should be ordered in assortment rank order.
        /// </summary>
        ByRank = 1,
        /// <summary>
        /// Indicates that the products with the lowest achieved DOS values should be processed first.
        /// </summary>
        ByLowestDos = 2,
    }
}
