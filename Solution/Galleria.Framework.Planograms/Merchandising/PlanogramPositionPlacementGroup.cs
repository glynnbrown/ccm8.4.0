﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
// V8-27738 : N.Foster
//  Refactored to a POCO for performance
#endregion
#region Version History: CCM802
//V8-29028 : L.Ineson
//  Added methods to help apply product squeeze.
#endregion
#region Version History : CCM810
// V8-29769 : A.Kuszyk
//  Added GetSequence and HasSequences.
// V8-30016 : A.Kuszyk
//  Added SetGroupCoordinate method.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using MathHelper = Galleria.Framework.Helpers.MathHelper;
using Galleria.Framework.Helpers;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents a group of position placements
    /// </summary>
    /// <remarks>TODO: Consider adding dividervalue collections to here.</remarks>
    public class PlanogramPositionPlacementGroup
    {
        #region Fields
        private PlanogramPositionPlacementList _placements;
        private Int16? _sequenceX;
        private Int16? _sequenceY;
        private Int16? _sequenceZ;

        private Single _dividerSlotSizeX;
        private Single _dividerSlotReservedX;
        private Single _dividerSlotSizeY;
        private Single _dividerSlotReservedY;
        private Single _dividerSlotSizeZ;
        private Single _dividerSlotReservedZ;

        #endregion

        #region Properties

        #region Placements
        /// <summary>
        /// Returns all placements within this group
        /// </summary>
        public PlanogramPositionPlacementList Placements
        {
            get { return _placements; }
        }
        #endregion

        #region SeqeuenceX
        /// <summary>
        /// Gets or sets the x sequence value for this group of positions
        /// </summary>
        public Int16? SequenceX
        {
            get { return _sequenceX; }
            set { _sequenceX = value; }
        }
        #endregion

        #region SeqeuenceY
        /// <summary>
        /// Gets or sets the x sequence value for this group of positions
        /// </summary>
        public Int16? SequenceY
        {
            get { return _sequenceY; }
            set { _sequenceY = value; }
        }
        #endregion

        #region SeqeuenceZ
        /// <summary>
        /// Gets or sets the x sequence value for this group of positions
        /// </summary>
        public Int16? SequenceZ
        {
            get { return _sequenceZ; }
            set { _sequenceZ = value; }
        }
        #endregion

        #region DividerSlotSizeX
        /// <summary>
        /// Gets/Sets the size of the x axis divider slot
        /// that this position is currently placed within.
        /// </summary>
        public Single DividerSlotSizeX
        {
            get { return _dividerSlotSizeX; }
            set { _dividerSlotSizeX = value; }
        }
        #endregion

        #region DividerSlotReservedX
        /// <summary>
        /// Gets/Sets the amount of width that should be reserved
        /// for the divider in the divider slot
        /// </summary>
        public Single DividerSlotReservedX
        {
            get { return _dividerSlotReservedX; }
            set { _dividerSlotReservedX = value; }
        }
        #endregion

        #region DividerSlotSizeY
        /// <summary>
        /// Gets/Sets the size of the y axis divider slot
        /// that this position is currently placed within.
        /// </summary>
        public Single DividerSlotSizeY
        {
            get { return _dividerSlotSizeY; }
            set { _dividerSlotSizeY = value; }
        }
        #endregion

        #region DividerSlotReservedY
        /// <summary>
        /// Gets/Sets the amount of height that should be reserved
        /// for the divider in the divider slot
        /// </summary>
        public Single DividerSlotReservedY
        {
            get { return _dividerSlotReservedY; }
            set { _dividerSlotReservedY = value; }
        }
        #endregion

        #region DividerSlotSizeZ
        /// <summary>
        /// Gets/Sets the size of the z axis divider slot
        /// that this position is currently placed within.
        /// </summary>
        public Single DividerSlotSizeZ
        {
            get { return _dividerSlotSizeZ; }
            set { _dividerSlotSizeZ = value; }
        }
        #endregion

        #region DividerSlotReservedZ
        /// <summary>
        /// Gets/Sets the amount of depth that should be reserved
        /// for the divider in the divider slot
        /// </summary>
        public Single DividerSlotReservedZ
        {
            get { return _dividerSlotReservedZ; }
            set { _dividerSlotReservedZ = value; }
        }
        #endregion

        #endregion

        #region Constructors
        private PlanogramPositionPlacementGroup() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionPlacementGroup NewPlanogramPositionPlacementGroup(
            IEnumerable<PlanogramPositionPlacement> placements,
            Int16? sequenceX,
            Int16? sequenceY,
            Int16? sequenceZ)
        {
            PlanogramPositionPlacementGroup item = new PlanogramPositionPlacementGroup();
            item.Create(placements, sequenceX, sequenceY, sequenceZ);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(
            IEnumerable<PlanogramPositionPlacement> placements,
            Int16? sequenceX,
            Int16? sequenceY,
            Int16? sequenceZ)
        {
            _placements = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(placements);
            _sequenceX = sequenceX;
            _sequenceY = sequenceY;
            _sequenceZ = sequenceZ;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the bounds of this groups as a whole.
        /// </summary>
        public RectValue GetBounds(Boolean applySlotConstraints = true)
        {
            Single x = Single.PositiveInfinity;
            Single y = Single.PositiveInfinity;
            Single z = Single.PositiveInfinity;
            Single x2 = Single.NegativeInfinity;
            Single y2 = Single.NegativeInfinity;
            Single z2 = Single.NegativeInfinity;

            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                x = Math.Min(x, p.Position.X);
                y = Math.Min(y, p.Position.Y);
                z = Math.Min(z, p.Position.Z);

                var posDetails = p.GetPositionDetails();
                x2 = Math.Max(x2, p.Position.X + posDetails.TotalSize.Width);
                y2 = Math.Max(y2, p.Position.Y + posDetails.TotalSize.Height);
                z2 = Math.Max(z2, p.Position.Z + posDetails.TotalSize.Depth);
            }

            Single width = x2 - x;
            Single height = y2 - y;
            Single depth = z2 - z;

            //apply slot constraints.
            if (applySlotConstraints)
            {
                width = ApplySlotConstraint(width, this.DividerSlotSizeX, this.DividerSlotReservedX);
                height = ApplySlotConstraint(height, this.DividerSlotSizeY, this.DividerSlotReservedY);
                depth = ApplySlotConstraint(depth, this.DividerSlotSizeZ, this.DividerSlotReservedZ); 
            }

            return new RectValue(x, y, z, width, height, depth);
        }

        /// <summary>
        /// Moves the entire group to the given coordinate, in the given axis.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="coordinate"></param>
        public void SetGroupCoordinate(AxisType axis, Single coordinate)
        {
            switch (axis)
            {
                case AxisType.X:
                    SetGroupX(coordinate);
                    break;
                case AxisType.Y:
                    SetGroupY(coordinate);
                    break;
                case AxisType.Z:
                    SetGroupZ(coordinate);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Moves the entire group of placements to the given x coordinate.
        /// </summary>
        public void SetGroupX(Single newGroupX)
        {
            Single curGroupX = this.Placements.Min(p => p.Position.X);

            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                Single offset = p.Position.X - curGroupX;
                p.Position.X = newGroupX + offset;
            }
        }

        /// <summary>
        /// Moves the entire group of placements to the given y coordinate.
        /// </summary>
        public void SetGroupY(Single newGroupY)
        {
            Single curGroupY = this.Placements.Min(p => p.Position.Y);

            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                Single offset = p.Position.Y - curGroupY;
                p.Position.Y = newGroupY + offset;
            }
        }

        /// <summary>
        /// Moves the entire group of placements to the given z coordinate.
        /// </summary>
        public void SetGroupZ(Single newGroupZ)
        {
            Single curGroupZ = this.Placements.Min(p => p.Position.Z);

            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                Single offset = p.Position.Z - curGroupZ;
                p.Position.Z = newGroupZ + offset;
            }
        }

        /// <summary>
        /// Gets the Sequence value of this placement group for the given axis.
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        public Int16? GetSequence(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return SequenceX;
                case AxisType.Y:
                    return SequenceY;
                case AxisType.Z:
                    return SequenceZ;
                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Returns true if the placement group's sequence values all have values.
        /// </summary>
        /// <returns></returns>
        public Boolean HasSequences()
        {
            return SequenceX.HasValue && SequenceY.HasValue && SequenceZ.HasValue;
        }

        #region Product Squeeze

        /// <summary>
        /// Returns the bounds of this placement group if all
        /// products within it were to be fully squeezed.
        /// </summary>
        public RectValue GetFullySqueezedBounds()
        {
            Single x = Single.PositiveInfinity;
            Single y = Single.PositiveInfinity;
            Single z = Single.PositiveInfinity;
            Single x2 = Single.NegativeInfinity;
            Single y2 = Single.NegativeInfinity;
            Single z2 = Single.NegativeInfinity;

            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                x = Math.Min(x, p.Position.X);
                y = Math.Min(y, p.Position.Y);
                z = Math.Min(z, p.Position.Z);

                var posDetails = p.GetPositionDetails();
                x2 = Math.Max(x2, p.Position.X + posDetails.MinimumSize.Width);
                y2 = Math.Max(y2, p.Position.Y + posDetails.MinimumSize.Height);
                z2 = Math.Max(z2, p.Position.Z + posDetails.MinimumSize.Depth);
            }

            Single width = x2 - x;
            Single height = y2 - y;
            Single depth = z2 - z;

            //apply slot constraints.
            width = ApplySlotConstraint(width, this.DividerSlotSizeX, this.DividerSlotReservedX);
            height = ApplySlotConstraint(height, this.DividerSlotSizeY, this.DividerSlotReservedY);
            depth = ApplySlotConstraint(depth, this.DividerSlotSizeZ, this.DividerSlotReservedZ);

            return new RectValue(x, y, z, width, height, depth);
        }

        /// <summary>
        /// Applies squeeze to the products of this group
        /// so that it takes up the minimum possible width.
        /// </summary>
        public void ApplyFullHorizontalSqueeze()
        {
            //cycle through placements squeezing them to the group minimum.
            Single targetWidth = GetFullySqueezedBounds().Width;
            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                p.SqueezeWidthTo(targetWidth);
            }
        }

        /// <summary>
        /// Applies squeeze to the products of this group
        /// to hit the given target width.
        /// </summary>
        public Boolean SqueezeWidthTo(Single targetWidth)
        {
            RectValue curBounds = GetBounds();

            //return out if the width is already ok.
            if (MathHelper.LessOrEqualThan(curBounds.Width, targetWidth)) return true;

            //correct the target if the group has a slot constraint
            if (this.DividerSlotSizeX > 0)
            {
                targetWidth = ApplySlotConstraint(targetWidth, this.DividerSlotSizeX, this.DividerSlotReservedX, roundUp:false);
            }

            //return out if the width cannot be changed.
            Single minWidth = GetFullySqueezedBounds().Width;
            if (curBounds.Width == minWidth) return false;

            //if reducing by the amount would place us under minimum
            //then just squeeze to min and return false.
            if (targetWidth < minWidth)
            {
                ApplyFullHorizontalSqueeze();
                return false;
            }

            //otherwise squeeze by the requested amount.
            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                p.SqueezeWidthTo(targetWidth);
            }
            return true;
        }

        /// <summary>
        /// Applies squeeze to the products of this group
        /// so that it takes up the minimum possible height.
        /// </summary>
        public void ApplyFullVerticalSqueeze()
        {
            //cycle through placements squeezing them to the group minimum.
            Single target = GetFullySqueezedBounds().Height;
            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                p.SqueezeHeightTo(target);
            }
        }

        /// <summary>
        /// Applies squeeze to the products of this group
        /// to hit the given target height.
        /// </summary>
        public Boolean SqueezeHeightTo(Single targetHeight)
        {
            RectValue curBounds = GetBounds();

            //return out if the width is already ok.
            if (MathHelper.LessOrEqualThan(curBounds.Height, targetHeight)) return true;

            //correct the target if the group has a slot constraint
            if (this.DividerSlotSizeY > 0)
            {
                targetHeight = ApplySlotConstraint(targetHeight, this.DividerSlotSizeY, this.DividerSlotReservedY);

                //if the bounds are now less then the target
                // we return false as we failed to actually squeeze anything.
                if (MathHelper.LessOrEqualThan(curBounds.Height, targetHeight)) return false;
            }


            //return out if the width cannot be changed.
            Single minHeight = GetFullySqueezedBounds().Height;
            if (curBounds.Height == minHeight) return false;

            //if reducing by the amount would place us under minimum
            //then just squeeze to min and return false.
            if (targetHeight < minHeight)
            {
                ApplyFullVerticalSqueeze();
                return false;
            }

            //otherwise squeeze by the requested amount.
            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                p.SqueezeHeightTo(targetHeight);
            }
            return true;
        }

        /// <summary>
        /// Applies squeeze to the products of this group
        /// so that it takes up the minimum possible depth.
        /// </summary>
        public void ApplyFullDepthSqueeze()
        {
            //cycle through placements squeezing them to the group minimum.
            Single target = GetFullySqueezedBounds().Depth;
            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                p.SqueezeDepthTo(target);
            }
        }

        /// <summary>
        /// Applies squeeze to the products of this group
        /// to hit the given target depth.
        /// </summary>
        public Boolean SqueezeDepthTo(Single targetDepth)
        {
            RectValue curBounds = GetBounds();

            //return out if the depth is already ok.
            if (MathHelper.LessOrEqualThan(curBounds.Depth, targetDepth)) return true;

            //correct the target if the group has a slot constraint
            if (this.DividerSlotSizeZ > 0)
            {
                targetDepth = ApplySlotConstraint(targetDepth, this.DividerSlotSizeZ, this.DividerSlotReservedZ);

                //if the bounds are now less then the target
                // we return false as we failed to actually squeeze anything.
                if (MathHelper.LessOrEqualThan(curBounds.Depth, targetDepth)) return false;
            }

            //return out if the depth cannot be changed.
            Single minDepth = GetFullySqueezedBounds().Depth;
            if (curBounds.Depth == minDepth) return false;

            //if reducing by the amount would place us under minimum
            //then just squeeze to min and return false.
            if (targetDepth < minDepth)
            {
                ApplyFullDepthSqueeze();
                return false;
            }

            //otherwise squeeze by the requested amount.
            foreach (PlanogramPositionPlacement p in this.Placements)
            {
                p.SqueezeDepthTo(targetDepth);
            }
            return true;
        }

        /// <summary>
        /// Constrains the given value to the nearest whole slot value.
        /// </summary>
        /// <param name="value">The value to constrain</param>
        /// <param name="slotSize">The size of a single slot</param>
        /// <param name="slotReserved">The amount of space that should be reserved in the slot for the divider.</param>
        /// <param name="roundUp">Flag to indicate if the value should be rounded up. Generally this is true,
        /// but will be false if we are trying to calculate squeeze.</param>
        /// <returns>The constrained value.</returns>
        private static Single ApplySlotConstraint(Single value, Single slotSize, Single slotReserved, Boolean roundUp = true)
        {
            if (slotSize <= 0) return value;

            //first constrain the value
            Single constrainedValue =
                MathHelper.RoundToNearestIncrement(value + slotReserved, slotSize)
                - slotReserved;

            //if it falls below then increase by another slot.
            if (roundUp && constrainedValue <= value)
            {
                constrainedValue += slotSize;
            }

            return constrainedValue;
        }

        #endregion

        #endregion
    }
}
