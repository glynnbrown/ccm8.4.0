﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31807 : D.Pleasance
//  Created.
// V8-32612 : D.Pleasance
//  Refactored PlanogramMerchandisingProductLayout constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
// V8-32671 : D.Pleasance
//  Added call to update merchandising block sequence numbers, UpdateMerchandisingBlockPositionPlacementsWithActualSequenceGroupNumber()
// V8-32825 : A.Silva
//  Amended TryRelayoutBlockWithNewProduct to not reset the sequence numbers as that is part of the tast itself.
// V8-32787 : A.Silva
//  Added an optional AssortmentRuleEnforcer parameter to the constructors, so that any task that need to enforce rules can do so.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    public sealed class PlanogramMerchandisingProductLayout
    {
        #region Fields

        private PlanogramSequenceGroup _sequenceGroup;

        #endregion

        #region Properties

        /// <summary>
        /// The planogram being layed out by the layout method.
        /// </summary>
        public Planogram Planogram { get; private set; }

        /// <summary>
        /// The list of <see cref="PlanogramMerchandisingGroup"/> instances present in the Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; private set; }

        /// <summary>
        /// The list of possible sequence groups to place the product
        /// </summary>
        public List<PlanogramSequenceGroup> AvailableSequenceGroups { get; private set; }

        /// <summary>
        /// The plans <see cref="PlanogramBlocking"/> lookup. 
        /// </summary>
        public PlanogramBlocking PlanogramBlocking { get; private set; }

        /// <summary>
        /// The current planogram blocking width.
        /// </summary>
        public Single PlanogramBlockingWidth { get; private set; }

        /// <summary>
        /// The current planogram blocking height.
        /// </summary>
        public Single PlanogramBlockingHeight { get; private set; }

        /// <summary>
        /// The planogram blocking height offset.
        /// </summary>
        public Single PlanogramBlockingHeightOffset { get; private set; }

        /// <summary>
        /// The product to add
        /// </summary>
        public PlanogramProduct Product { get; private set; }
        
        /// <summary>
        /// A dictionary of product stacks, keyed by their sequence group colour.
        /// </summary>
        public Dictionary<Int32, Stack<PlanogramProduct>> ProductStacksBySequenceColour { get; private set; }

        /// <summary>
        /// A dictionary of target unit dictionaries (units keyed by gtin), keyed by the sequence group colour they belong to.
        /// </summary>
        public Dictionary<Int32, Dictionary<String, Int32>> TargetUnitsBySequenceColour { get; private set; }

        /// <summary>
        /// The target product unit value
        /// </summary>
        public Int32 TargetProductUnits { get; private set; }

        /// <summary>
        /// Can product placement allow block space to be exceeded
        /// </summary>
        public PlanogramMerchandisingSpaceConstraintType SpaceConstraintType { get; private set; }

        public PlanogramSequenceGroup ProductSequenceGroup
        {
            get { return _sequenceGroup; }
        }

        public AssortmentRuleEnforcer AssortmentRuleEnforcer { get; set; }

        #endregion

        #region Constructor

        public PlanogramMerchandisingProductLayout(Planogram planogram, 
                                                PlanogramMerchandisingGroupList merchandisingGroups, 
                                                List<PlanogramSequenceGroup> availableSequenceGroups, 
                                                PlanogramBlocking planogramBlocking,
                                                Single planogramBlockingWidth, 
                                                Single planogramBlockingHeight, 
                                                Single planogramBlockingHeightOffset, 
                                                PlanogramProduct product, 
                                                Dictionary<Int32, Stack<PlanogramProduct>> productStacksBySequenceColour, 
                                                Dictionary<Int32, Dictionary<String, Int32>> targetUnitsBySequenceColour, 
                                                Int32 currentTargetUnits,
                                                PlanogramMerchandisingSpaceConstraintType spaceConstraintType, 
                                                AssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            Planogram = planogram;
            MerchandisingGroups = merchandisingGroups;
            AvailableSequenceGroups = availableSequenceGroups;
            PlanogramBlocking = planogramBlocking;
            PlanogramBlockingWidth = planogramBlockingWidth;
            PlanogramBlockingHeight = planogramBlockingHeight;
            PlanogramBlockingHeightOffset = planogramBlockingHeightOffset;
            Product = product;
            ProductStacksBySequenceColour = productStacksBySequenceColour;
            TargetUnitsBySequenceColour  = targetUnitsBySequenceColour;
            TargetProductUnits  = currentTargetUnits;
            SpaceConstraintType = spaceConstraintType;
            AssortmentRuleEnforcer = assortmentRuleEnforcer;
        }

        #endregion

        #region Methods
        
        public Boolean TryRelayoutBlockWithNewProduct()
        {
            if (!AllocateProductToBlockingGroup()) return false;

            PlanogramBlockingGroup blockingGroup = PlanogramBlocking.Groups.FirstOrDefault(g => g.Colour.Equals(_sequenceGroup.Colour));
            if (blockingGroup == null) return false;
                
            // Prepare the merchandising blocks that represent the space that we're going to try to make
            // a change into.
            PlanogramMerchandisingBlock merchandisingBlock = new PlanogramMerchandisingBlocks(
                PlanogramBlocking,
                ProductStacksBySequenceColour,
                MerchandisingGroups,
                PlanogramBlockingWidth,
                PlanogramBlockingHeight,
                PlanogramBlockingHeightOffset,
                blockingGroup,
                SpaceConstraintType,
                AssortmentRuleEnforcer)
                .FirstOrDefault(b => b.BlockingGroup == blockingGroup);
            if (merchandisingBlock == null) return false;

            if (merchandisingBlock.BlockPlacements.Select(p => p.MerchandisingGroup).Distinct().Any(p => p.IsOverfilled())) return false;
                                
            merchandisingBlock.BeginMerchandisingEdit();

            // Try to find the target units dictionary for this block.
            Dictionary<String, Int32> targetUnits;
            TargetUnitsBySequenceColour.TryGetValue(merchandisingBlock.SequenceGroup.Colour, out targetUnits);
            if (targetUnits == null) return false;

            // Add new product inventory detail
            targetUnits.Add(Product.Gtin, TargetProductUnits);

            // Layout the block prepared for the change.
            LayoutBlock(merchandisingBlock, targetUnits);

            // Find the position that we've added (it isn't the same instance anymore and will be a clone).
            IEnumerable<PlanogramPositionPlacement> positionPlacements;
            PlanogramPositionPlacement placedPosition;
            GetPositionPlacementsAndPlacedPosition(Product.Gtin, merchandisingBlock, out positionPlacements, out placedPosition);

            // Ensure that we placed all products at their target units and that we didn't drop any products.
            if (IsLayoutSuccess(targetUnits, TargetProductUnits, positionPlacements, placedPosition))
            {
                // Record the target units for this product (in this block) to be the current units of the
                // new position. We do this, because an increase in units may have exceeded the old target units.
                targetUnits[placedPosition.Product.Gtin] = placedPosition.Position.TotalUnits;
                
                // Apply any changes to the merchandising groups for the planogram.
                merchandisingBlock.ApplyMerchandisingEdit();
                return true;
            }

            // If the layout was not a success, we need to reverse the change we have made. This involves 
            // re-laying out the block into the space that it originally occupied.
            RemoveProductFromSequenceStack();
            targetUnits.Remove(Product.Gtin);

            merchandisingBlock.CancelMerchandisingEdit();

            GetPositionPlacementsAndPlacedPosition(Product.Gtin, merchandisingBlock, out positionPlacements, out placedPosition);
            if (!IsLayoutSuccess(targetUnits, 0, positionPlacements, placedPosition))
            {
                System.Diagnostics.Debug.Fail("Products should never be dropped after an unsuccessful inventory change");
            }
            return false;
        }
        
        /// <summary>
        /// Removes the current product from the current sequence stack
        /// </summary>
        /// <param name="context">The current task context</param>
        private void RemoveProductFromSequenceStack()
        {
            Stack<PlanogramProduct> productStack;
            ProductStacksBySequenceColour.TryGetValue(_sequenceGroup.Colour, out productStack);

            List<PlanogramProduct> planogramSequenceProducts = productStack.ToList();
            planogramSequenceProducts.Reverse(); // reverse the list as adding to the stack
            productStack.Clear();

            foreach (PlanogramProduct planogramSequenceProduct in planogramSequenceProducts)
            {
                if (planogramSequenceProduct.Gtin != Product.Gtin)
                {
                    productStack.Push(planogramSequenceProduct);
                }
            }
        }

        /// <summary>
        /// Finds and allocates current product to the most appropriate sequence group.
        /// </summary>
        /// <param name="context">The task context</param>
        /// <returns></returns>
        private Boolean AllocateProductToBlockingGroup()
        {
            List<PlanogramSequenceGroup> sequenceGroupsOnThePlan = new List<PlanogramSequenceGroup>();

            // find the possible sequence groups that exist on the plan
            foreach (PlanogramSequenceGroup sequenceGroup in AvailableSequenceGroups)
            {
                if (PlanogramBlocking.Groups.Where(p => p.Colour == sequenceGroup.Colour).Any())
                {
                    sequenceGroupsOnThePlan.Add(sequenceGroup);
                }
            }

            ProductSequenceGroupHelper productSequenceGroup = GetProductSequenceGroup(sequenceGroupsOnThePlan, Product);

            if (productSequenceGroup != null)
            {
                _sequenceGroup = productSequenceGroup.SequenceGroup;
                InsertProductIntoSequenceStack(productSequenceGroup);
            }

            return (productSequenceGroup != null);
        }

        /// <summary>
        /// Gets the most appropriate sequence group to allocate the given product. This is achieved by finding an anchor within the sequence 
        /// that causes the least deviation from the original sequence.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sequenceGroups"></param>
        /// <param name="planogramProduct"></param>
        /// <returns></returns>
        private ProductSequenceGroupHelper GetProductSequenceGroup(List<PlanogramSequenceGroup> sequenceGroups, PlanogramProduct planogramProduct)
        {
            List<ProductSequenceGroupHelper> productSequenceGroups = new List<ProductSequenceGroupHelper>();

            foreach (PlanogramSequenceGroup planogramSequenceGroup in sequenceGroups)
            {
                Stack<PlanogramProduct> productStack;
                ProductStacksBySequenceColour.TryGetValue(planogramSequenceGroup.Colour, out productStack);

                if (productStack != null)
                {
                    List<PlanogramProduct> planogramSequenceGroupProducts = productStack.ToList();
                    productSequenceGroups.Add(new ProductSequenceGroupHelper(planogramSequenceGroup, planogramSequenceGroupProducts, planogramProduct.Gtin));
                }
            }

            return productSequenceGroups.OrderBy(p => p.AnchorDeviation).
                                          ThenBy(p => p.SequenceGroupTotalDeviation).FirstOrDefault();
        }

        /// <summary>
        /// Inserts the current product into the sequence group provided by <paramref name="productSequenceGroup"/> into the correct position according to the anchor detail.
        /// </summary>
        /// <param name="context">The current task context</param>
        /// <param name="productSequenceGroup">The product sequence group to insert</param>
        private  void InsertProductIntoSequenceStack(ProductSequenceGroupHelper productSequenceGroup)
        {
            Stack<PlanogramProduct> productStack;
            ProductStacksBySequenceColour.TryGetValue(productSequenceGroup.SequenceGroup.Colour, out productStack);

            List<PlanogramProduct> planogramSequenceProducts = productSequenceGroup.CurrentPlanogramSequenceGroupProducts;

            //if there are currently no sequence products for this group on the plan
            //just push the product onto the stack and return
            if (planogramSequenceProducts.Count == 0)
            {
                productStack.Push(Product);
                return;
            }

            planogramSequenceProducts.Reverse(); // reverse the list as adding to the stack
            productStack.Clear();
            

            foreach (PlanogramProduct planogramSequenceProduct in planogramSequenceProducts)
            {
                if (planogramSequenceProduct.Gtin == productSequenceGroup.AnchorProductGtin)
                {
                    if (!productSequenceGroup.AnchorAddAfter) // invert add after as adding to the stack
                    {
                        productStack.Push(planogramSequenceProduct);
                        productStack.Push(Product);
                    }
                    else
                    {
                        productStack.Push(Product);
                        productStack.Push(planogramSequenceProduct);
                    }
                }
                else
                {
                    productStack.Push(planogramSequenceProduct);
                }
            }
        }
        
        private static void GetPositionPlacementsAndPlacedPosition(String gtin, PlanogramMerchandisingBlock merchandisingBlock, out IEnumerable<PlanogramPositionPlacement> positionPlacements, out PlanogramPositionPlacement placedPosition)
        {
            positionPlacements = merchandisingBlock.BlockPlacements
                .Select(bp => bp.MerchandisingGroup)
                .Distinct()
                .SelectMany(bp => bp.PositionPlacements.Where(p => p.Position.SequenceColour.Equals(merchandisingBlock.BlockingGroup.Colour)))
                .ToList();
            placedPosition = positionPlacements.FirstOrDefault(p => p.Product.Gtin.Equals(gtin));
        }

        private static Boolean IsLayoutSuccess(Dictionary<String, Int32> targetUnits, Int32 targetUnitsValue, IEnumerable<PlanogramPositionPlacement> positionPlacements, PlanogramPositionPlacement placedPosition)
        {
            Boolean isLayoutSuccess =
                    (placedPosition != null) ?
                        placedPosition.Position.TotalUnits >= targetUnitsValue && // It achieved its target units.
                        positionPlacements.Count() == targetUnits.Keys.Count() :  // There was no change in range, we didn't drop products.
                        positionPlacements.Count() == targetUnits.Keys.Count();

            return isLayoutSuccess;
        }
        
        private void LayoutBlock(PlanogramMerchandisingBlock merchandisingBlock, Dictionary<String, Int32> targetUnits)
        {
            IPlanogramMerchandisingLayoutContext<PlanogramMerchandisingProductLayout> layoutContext =
                new PlanogramMerchandisingLayoutContext<PlanogramMerchandisingProductLayout>(
                    targetUnits,
                    Planogram,
                /*shouldCompactSpace*/ true,
                    new Stack<PlanogramProduct>(ProductStacksBySequenceColour[merchandisingBlock.SequenceGroup.Colour].Reverse()));
            merchandisingBlock.Layout<PlanogramMerchandisingProductLayout>(layoutContext);
        }

        #endregion
    }

    public class ProductSequenceGroupHelper
    {
        #region Fields

        private Dictionary<String, Int32> _planogramSequence = new Dictionary<String, Int32>();
        private List<PlanogramProduct> _currentPlanogramSequenceGroupProducts;
        private PlanogramSequenceGroup _sequenceGroup;
        private Boolean _anchorAddAfter = false;
        private String _anchorProductGtin;
        private Int32 _anchorDeviation = 0;
        private Int32 _sequenceGroupTotalDeviation = 0;

        #endregion

        #region Properties

        public PlanogramSequenceGroup SequenceGroup
        {
            get { return _sequenceGroup; }
        }

        public List<PlanogramProduct> CurrentPlanogramSequenceGroupProducts
        {
            get { return _currentPlanogramSequenceGroupProducts; }
        }

        public Boolean AnchorAddAfter
        {
            get { return _anchorAddAfter; }
        }
        public String AnchorProductGtin
        {
            get { return _anchorProductGtin; }
        }
        public Int32 AnchorDeviation
        {
            get { return _anchorDeviation; }
        }
        public Int32 SequenceGroupTotalDeviation 
        {
            get { return _sequenceGroupTotalDeviation; }
        }

        #endregion

        #region Constructor

        public ProductSequenceGroupHelper(PlanogramSequenceGroup planogramSequenceGroup, List<PlanogramProduct> currentPlanogramSequenceGroupProducts, String productGtin)
        {
            _sequenceGroup = planogramSequenceGroup;
            _currentPlanogramSequenceGroupProducts = currentPlanogramSequenceGroupProducts;

            // Get the sequence for existing sequence group products
            GetExistingPlanogramProductSequence();

            // Get the actual sequence details for the positions that we do have
            Dictionary<String, Int32> sequenceGroupProducts = GetActualSequenceOfProductsOnThePlan(planogramSequenceGroup);

            // Get the planogram product deviation away from the actual sequence
            Dictionary<String, Int32> productDeviationOnPlan = sequenceGroupProducts
                                                .ToDictionary(p => p.Key,
                                                p => _planogramSequence.ContainsKey(p.Key) ? Math.Abs(_planogramSequence[p.Key] - p.Value) : 0);

            // get the product sequnce number to find anchor from
            Int32 planogramProductSequenceNumber = planogramSequenceGroup.Products.FirstOrDefault(p => p.Gtin == productGtin)?.SequenceNumber ?? 0;
            PlanogramSequenceGroupProduct closestPreSequenceGroupProduct = GetClosestPreSequenceGroupProduct(planogramSequenceGroup, planogramProductSequenceNumber);
            PlanogramSequenceGroupProduct closestPostSequenceGroupProduct = GetClosestPostSequenceGroupProduct(planogramSequenceGroup, planogramProductSequenceNumber);

            // get the most appropriate anchor, least deviation.
            GetMostAppropriateAnchor(productDeviationOnPlan, closestPreSequenceGroupProduct, closestPostSequenceGroupProduct);

            // Get the total deviation for the sequence group.
            _sequenceGroupTotalDeviation = productDeviationOnPlan.Sum(p => p.Value);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the Planogram product sequence
        /// </summary>
        private void GetExistingPlanogramProductSequence()
        {
            Int32 sequence = 1;
            _planogramSequence = new Dictionary<String, Int32>();
            foreach (PlanogramProduct planogramSequenceProduct in _currentPlanogramSequenceGroupProducts)
            {
                _planogramSequence.Add(planogramSequenceProduct.Gtin, sequence++);
            }
        }

        /// <summary>
        /// Get the actual sequence groups sequence
        /// </summary>
        /// <param name="planogramSequenceGroup"></param>
        /// <returns></returns>
        private Dictionary<String, Int32> GetActualSequenceOfProductsOnThePlan(PlanogramSequenceGroup planogramSequenceGroup)
        {
            Dictionary<String, Int32> sequenceGroupProducts = new Dictionary<String, Int32>();
            Int32 sequence = 1;
            foreach (PlanogramSequenceGroupProduct planogramSequenceGroupProduct in planogramSequenceGroup.Products.OrderBy(p => p.SequenceNumber))
            {
                if (_planogramSequence.ContainsKey(planogramSequenceGroupProduct.Gtin))
                {
                    sequenceGroupProducts.Add(planogramSequenceGroupProduct.Gtin, sequence++);
                }
            }
            return sequenceGroupProducts;
        }

        private PlanogramSequenceGroupProduct GetClosestPreSequenceGroupProduct(PlanogramSequenceGroup planogramSequenceGroup, Int32 planogramProductSequenceNumber)
        {
            PlanogramSequenceGroupProduct closestPreSequenceGroupProduct = null;

            // get first item pre
            foreach (PlanogramSequenceGroupProduct preSequenceGroupProduct in planogramSequenceGroup.Products.Where(p => p.SequenceNumber < planogramProductSequenceNumber).OrderByDescending(p=> p.SequenceNumber))
            {
                if (_planogramSequence.ContainsKey(preSequenceGroupProduct.Gtin))
                {
                    closestPreSequenceGroupProduct = preSequenceGroupProduct;
                    break;
                }
            }
            return closestPreSequenceGroupProduct;
        }

        private PlanogramSequenceGroupProduct GetClosestPostSequenceGroupProduct(PlanogramSequenceGroup planogramSequenceGroup, Int32 planogramProductSequenceNumber)
        {
            PlanogramSequenceGroupProduct closestPostSequenceGroupProduct = null;

            // get first item post
            foreach (PlanogramSequenceGroupProduct postSequenceGroupProduct in planogramSequenceGroup.Products.Where(p => p.SequenceNumber > planogramProductSequenceNumber).OrderBy(p => p.SequenceNumber))
            {
                if (_planogramSequence.ContainsKey(postSequenceGroupProduct.Gtin))
                {
                    closestPostSequenceGroupProduct = postSequenceGroupProduct;
                    break;
                }
            }
            return closestPostSequenceGroupProduct;
        }

        /// <summary>
        /// Gets the anchor with the least possible deviance. If the deviance is the same always add after pre item.
        /// </summary>
        /// <param name="productDeviationOnPlan"></param>
        /// <param name="closestPreSequenceGroupProduct"></param>
        /// <param name="closestPostSequenceGroupProduct"></param>
        private void GetMostAppropriateAnchor(Dictionary<String, Int32> productDeviationOnPlan, PlanogramSequenceGroupProduct closestPreSequenceGroupProduct, PlanogramSequenceGroupProduct closestPostSequenceGroupProduct)
        {
            if (closestPreSequenceGroupProduct != null && closestPostSequenceGroupProduct != null)
            {
                if (productDeviationOnPlan[closestPreSequenceGroupProduct.Gtin] == productDeviationOnPlan[closestPostSequenceGroupProduct.Gtin])
                {
                    ApplyAnchorDetail(closestPreSequenceGroupProduct.Gtin, productDeviationOnPlan[closestPreSequenceGroupProduct.Gtin], true);
                }
                else if (productDeviationOnPlan[closestPreSequenceGroupProduct.Gtin] > productDeviationOnPlan[closestPostSequenceGroupProduct.Gtin])
                {
                    ApplyAnchorDetail(closestPostSequenceGroupProduct.Gtin, productDeviationOnPlan[closestPostSequenceGroupProduct.Gtin], false);
                }
                else if (productDeviationOnPlan[closestPreSequenceGroupProduct.Gtin] < productDeviationOnPlan[closestPostSequenceGroupProduct.Gtin])
                {
                    ApplyAnchorDetail(closestPreSequenceGroupProduct.Gtin, productDeviationOnPlan[closestPreSequenceGroupProduct.Gtin], true);
                }
            }
            else if (closestPreSequenceGroupProduct != null)
            {
                ApplyAnchorDetail(closestPreSequenceGroupProduct.Gtin, productDeviationOnPlan[closestPreSequenceGroupProduct.Gtin], true);
            }
            else if (closestPostSequenceGroupProduct != null)
            {
                ApplyAnchorDetail(closestPostSequenceGroupProduct.Gtin, productDeviationOnPlan[closestPostSequenceGroupProduct.Gtin], false);
            }
        }
                        
        private void ApplyAnchorDetail(String anchorProductGtin, Int32 anchorDeviation, Boolean anchorAddAfter)
        {
            _anchorProductGtin = anchorProductGtin;
            _anchorDeviation = anchorDeviation;
            _anchorAddAfter = anchorAddAfter;
        }

        #endregion
    }
}
