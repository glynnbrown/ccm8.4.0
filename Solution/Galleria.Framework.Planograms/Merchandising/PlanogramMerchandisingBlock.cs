﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM802
// V8-29137 : A.Kuszyk
//  Created.
// V8-29198 : A.Silva
//      Refactored EnumerateSubComponentsInSequence to be an instance method of PlanogramBlockingGroup.
#endregion

#region Version History : CCM810
// V8-29889 : A.Kuszyk
//  Refactored calculation of merchandising space into PlanogramMerchandisingGroup.
// V8-29772 : A.Silva
//  Added FacingSpaceReductionPercentage property to help with block compacting.
// V8-30035 : A.Kuszyk
//  Ensured that block placements are always merch group relative and are reduced correctly by blocking spaces.
// V8-30084 : A.Kuszyk
//  Ensured one block placement is constructed per merch group per blocks.
#endregion

#region Version History : CCM811
// V8-30152 : A.Kuszyk
//  Ensured that design view relative merchandising spaces are compared to blocking spaces.
// V8-30382 : A.Kuszyk
//  Ensured that a distinct collection of subcomponents are compiled into the sequenced subcomponents dictionary.
// V8-30114 : A.Kuszyk
//  Removed exception for manually merchandised components - they have no impact on this class' construction.
//  Ensured that the top component only gets all the space if it is in the top block.
#endregion

#region Version History : CCM820
// V8-31004 : D.Pleasance
//  Added static property to expose out OverlapThreshold.
#endregion

#region Version History : CCM830
// V8-31874 : A.Kuszyk
//  Re-factored layout code in from the Merchandise Planogram task.
// V8-31804 : A.Kuszyk
//  Added additional constructor to calculate block space from product representation on the planogram.
// V8-32025 : A.Probyn
//  Added defensive code to GetLargestPlacementsInFacingAxis where empty collection was having invalid methods applied. This now
//  results in 0 if no items to enumerate.
// V8-32505 : A.Kuszyk
//  Prototyped sequence sub group changes.
// V8-32433 : D.Pleasance
//  Amended GetPositionClusters() to calculate position clusters by sequence lines.
// V8-32539 : A.Kuszyk
//  Removed unused CalculateBlockSpaceInFacingAxis method.
// V8-32612 : D.Pleasance
//  Refactored PlanogramMerchandisingBlock constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
//  Also implemented PlanogramMerchandisingSpaceConstraintType.MinimumBlockSpace
// V8-32671 : D.Pleasance
//  Amended SetNextFacingSpaceReduction(), so that when next facing space reduction was not greater than previous we still need to apply the 
//  last facing space reduction as it formed part of a successful layout.
// V8-32671 : D.Pleasance
//  Added UpdateMerchandisingBlockPositionPlacementsWithActualSequenceGroupNumber()
// V8-32778 : D.Pleasance
//  Added CalculateNextFacingSpaceReductionPercentage(), rather than specifying 0.005% to reduce by we now calculate what the % would be in order to reduce by physical space of 0.005. 
//  Also increased decimal place to 5.
// V8-32809 : A.Kuszyk
//  Introduced changes to the way that compact placement groupings are created and also modified the way that they are compacted
//  such that they are now compacted one at a time.
// V8-32825 : A.Silva
//  Removed UpdateMerchandisingBlockPositionPlacementsWithActualSequenceGroupNumber as it is now being done outside the Merch Block.
// V8-32870 : A.Kuszyk
//  Amended the way blocking locations are ordered to ensure they are ordered in sequence order.
// V8-32906 : A.Kuszyk
//  Amended EnumerateBlockPlacementsTargetedForCompaction to return all of the chosen block placements
//  and not remove any duplicates.
// V8-32922 : A.Kuszyk
//  Re-factored compact block space behaviour to fully account for sub-blocks.
// V8-32948 : A.Kuszyk
//  Grouped merchandising groups by co-ordinate in facing axis when compacting, rather than picking
//  axis based on the orientation of the blocking.
// V8-32787 : A.Silva
//  Added AssortmentRuleEnforcer property to enable rule enforcement.
// V8-32984 : A.Kuszyk
//  Re-added behaviour to filter out block placements with the same compacted size in EnumerateBlockPlacementsTargettedForCompaction
//  to ensure that only one product is pushed out at a time.
// V8-32864 : A.Silva
//  PositionsPresentationStates now stores the states no matter how this instance is created (so that Merchandise Planogram will have them too).
// CCM-18328 : A.Kuszyk
//  Amended block space constructor to use component depth for block space.
// CCM-18440 : A.Kuszyk
//  Re-factored merchandising group block placement construction into common method and implemented in position
//  space constructor. Also added support for block placement type property.
// CCM-18462 : A.Kuszyk
//  Re-factored and re-organised methods and added support for combining block placements before
//  finishing instantiation.
// CCM-13871 : A.Silva
//  Added debug Output Placement Peg to show sequence X, Y, Z values.
// CCM-18503 : A.Kuszyk
//  Ensured that ApplyEdit method applies all edits.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Enums;
using static Galleria.Framework.Planograms.Logging.DebugLayout;
using System.Collections;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents the space that can be merchandising for a blocking group. The space is represented
    /// by the child collection of PlanogramMerchandisingBlockPlacements, each representing a part of the
    /// space available to the blocking group.
    /// </summary>
    public sealed class PlanogramMerchandisingBlock
    {
        #region Fields

        private readonly SortedList<Int32,PlanogramMerchandisingBlockPlacement> _blockPlacements;

        /// <summary>
        /// The threshold below which a percentage overlap is considered insignificant between a blocking location
        /// and a merchandising group. Set to below 1%, because 1% is the minimum size of a blocking location.
        /// </summary>
        private const Single _overlapThreshold = 0.008f;

        /// <summary>
        /// A collection of <see cref="CompactPlacementGrouping"/>s that represents the <see cref="PlanogramBlockingLocation"/>s
        /// that can be considered continuous in the <see cref="PlanogramBlocking"/> and the <see cref="PlanogramMerchandisingBlockPlacement"/>s
        /// that represent the physical volumes that can be merchandised.
        /// </summary>
        private readonly IEnumerable<CompactPlacementGrouping> _compactPlacementGroupings;

        /// <summary>
        /// A dictionary of the merchandising groups in this block and their edit levels at the time when
        /// <see cref="BeginMerchandisingEdit"/> was called.
        /// </summary>
        private Dictionary<PlanogramMerchandisingGroup, Int32> _merchandisingGroupsAndEditLevels;

        /// <summary>
        /// The merchandising groups covered by this merchandising block.
        /// </summary>
        private IEnumerable<PlanogramMerchandisingGroup> _merchandisingGroups;

        /// <summary>
        /// Indicates whether or not <see cref="BeginMerchandisingEdit"/> has been called on this block.
        /// </summary>
        private Boolean _isEditing = false;

        /// <summary>
        /// A dictionary of <see cref="PlanogramSequenceGroupProduct"/>s for the <see cref="SequenceGroup"/>,
        /// keyed by their Gtin.
        /// </summary>
        private Dictionary<String, PlanogramSequenceGroupProduct> _sequenceProductsByGtin;

        /// <summary>
        /// Used for enforcing assortment rules.
        /// </summary>
        private readonly AssortmentRuleEnforcer _assortmentRuleEnforcer;

        #endregion

        #region Properties

        /// <summary>
        /// The blocking group that this object represents.
        /// </summary>
        public PlanogramBlockingGroup BlockingGroup { get; private set; }

        /// <summary>
        /// The sequence group that this object represents.
        /// </summary>
        public PlanogramSequenceGroup SequenceGroup { get; private set; }

        /// <summary>
        /// The blocking/sequence colour that this object relates to.
        /// </summary>
        public Int32 Colour { get; private set; }

        /// <summary>
        /// This block's block placements, arranged in ascending sequence order.
        /// </summary>
        public IEnumerable<PlanogramMerchandisingBlockPlacement> BlockPlacements 
        {
            get { return _blockPlacements.Values; }
        }

        /// <summary>
        /// The parent PlanogramMerchandisingBlocks that represents the blocking strategy that this block is part of.
        /// </summary>
        public PlanogramMerchandisingBlocks Parent { get; private set; }

        /// <summary>
        /// Gets the The threshold below which a percentage overlap is considered insignificant between a blocking location
        /// and a merchandising group. Set to below 1%, because 1% is the minimum size of a blocking location.
        /// </summary>
        public static Single OverlapThreshold { get { return _overlapThreshold; } }

        /// <summary>
        /// A dictionary of the original presentation states of the positions in this block, keyed by their gtins.
        /// </summary>
        public Dictionary<String, PlanogramMerchandisingPositionPresentationState> PositionsPresentationStates { get; private set; }

        #endregion

        #region Nested Types

        private sealed class BlockingLocationPlaceHolder
        {
            public PlanogramBlockingLocation BlockingLocation { get; }

            public BlockingLocationPlaceHolder(PlanogramBlockingLocation blockingLocation)
            {
                BlockingLocation = blockingLocation;
            }

            public override Boolean Equals(Object obj)
            {
                BlockingLocationPlaceHolder other = obj as BlockingLocationPlaceHolder;
                if (other == null) return false;
                return Object.Equals(BlockingLocation, other.BlockingLocation);
            }

            public override Int32 GetHashCode()
            {
                if(BlockingLocation == null)
                {
                    return 0;
                }
                return BlockingLocation.GetHashCode();
            }
        }

        /// <summary>
        /// Block placement comparer helper
        /// </summary>
        private class BlockPlacementComparer : IEqualityComparer<Single>, IComparer<Single>
        {
            public bool Equals(float x, float y)
            {
                return x.EqualTo(y);
            }

            public int GetHashCode(float obj)
            {
                return obj.GetHashCode();
            }

            public int Compare(float x, float y)
            {
                if (x.LessThan(y)) return -1;
                if (x.GreaterThan(y)) return 1;

                return 0;
            }
        }

        /// <summary>
        /// A simple class representing a group of <see cref="PlanogramBlockingLocation"/>s and the <see cref="PlanogramMerchandisingBlockPlacement"/>s
        /// that represent the volume that can be merchandised for them, grouped for compacting purposes. This class represents the group
        /// of <see cref="PlanogramMerchandisingBlockPlacement">s that can be compacted together as one presentation area.
        /// </summary>
        /// <remarks>
        /// The <see cref="BlockingLocations"/> may be empty if no <see cref="PlanogramBlockingLocation"/> can be found to match the
        /// <see cref="BlockPlacements"/>.
        /// </remarks>
        private class CompactPlacementGrouping
        {
            #region Fields
            
            private List<PlanogramBlockingLocation> _blockingLocations;
            private List<PlanogramMerchandisingBlockPlacement> _blockPlacements;
            private List<PlanogramMerchandisingGroup> _merchandisingGroups;
            
            #endregion

            #region Properties

            /// <summary>
            /// The members of <see cref="BlockPlacements"/> that were compacted on the last compact attempt.
            /// </summary>
            public IEnumerable<PlanogramMerchandisingBlockPlacement> PreviousCompactingBlockPlacements { get; set; }

            /// <summary>
            /// The current members of <see cref="BlockPlacements"/> that are being compacted.
            /// </summary>
            public IEnumerable<PlanogramMerchandisingBlockPlacement> CurrentCompactingBlockPlacements { get; set; }

            /// <summary>
            /// The <see cref="PlanogramBlockingLocation"/>s that the <see cref="BlockPlacements"/> represent.
            /// </summary>
            public IEnumerable<PlanogramBlockingLocation> BlockingLocations
            {
                get { return _blockingLocations; }
            }

            /// <summary>
            /// The <see cref="PlanogramMerchandisingBlockPlacement"/>s that can be compacted together as a group.
            /// </summary>
            public IList<PlanogramMerchandisingBlockPlacement> BlockPlacements 
            {
                get { return _blockPlacements; }
            }

            /// <summary>
            /// The <see cref="PlanogramMerchandisingGroup"/>s that the <see cref="BlockPlacements"/> cover.
            /// </summary>
            public IEnumerable<PlanogramMerchandisingGroup> MerchandisingGroups
            {
                get { return _merchandisingGroups; }
            }

            /// <summary>
            /// Indicates whether this <see cref="CompactPlacementGrouping"/> has been fully compacted.
            /// </summary>
            public Boolean IsFullyCompacted { get; set; }

            #endregion

            #region Nested Types

            /// <summary>
            /// A simple class representing a collection of <see cref="PlanogramBlockingLocation"/>s that
            /// can be grouped together when compacting block space.
            /// </summary>
            private sealed class LocationGrouping : IEnumerable<PlanogramBlockingLocation>
            {
                /// <summary>
                /// The backing field for this object's collection.
                /// </summary>
                private List<PlanogramBlockingLocation> _locationGrouping;

                /// <summary>
                /// Creates a new grouping from the given <paramref name="locationGrouping"/>.
                /// </summary>
                /// <param name="locationGrouping"></param>
                public LocationGrouping(IEnumerable<PlanogramBlockingLocation> locationGrouping)
                {
                    _locationGrouping = locationGrouping.ToList();
                }

                /// <summary>
                /// IEnumerable implementation.
                /// </summary>
                /// <returns></returns>
                public IEnumerator<PlanogramBlockingLocation> GetEnumerator()
                {
                    return _locationGrouping.GetEnumerator();
                }

                /// <summary>
                /// IEnumerable implementation.
                /// </summary>
                /// <returns></returns>
                IEnumerator IEnumerable.GetEnumerator()
                {
                    return _locationGrouping.GetEnumerator();
                }

                /// <summary>
                /// Groups the <see cref="BlockingGroup"/>'s <see cref="PlanogramBlockingLocation"/>s based on whether or not they
                /// are continuous in the primary direction the blocking was blocked in.
                /// </summary>
                /// <returns></returns>
                public static IEnumerable<LocationGrouping> GetLocationGroupings(PlanogramBlockingGroup blockingGroup)
                {
                    List<LocationGrouping> locationGroupings = new List<LocationGrouping>();
                    List<PlanogramBlockingLocation> blockingLocations = blockingGroup.GetBlockingLocations();
                    if (!blockingLocations.Any() || blockingGroup.Parent == null) return locationGroupings;
                    Boolean blockingIsHorizontal = IsBlockingHorizontal(blockingGroup);

                    while (blockingLocations.Any())
                    {
                        List<PlanogramBlockingLocation> locationGrouping = new List<PlanogramBlockingLocation>();

                        // Take the first location out and add it to the grouping.
                        PlanogramBlockingLocation locationToGroup = blockingLocations.First();
                        blockingLocations.Remove(locationToGroup);
                        locationGrouping.Add(locationToGroup);

                        // Find any other locations that can be grouped.
                        List<PlanogramBlockingLocation> locationsToRemove = new List<PlanogramBlockingLocation>();
                        foreach (PlanogramBlockingLocation blockingLocation in blockingLocations)
                        {
                            // If this blocking's root divider level is horizontal, we need to check if this blocking location
                            // is continuous in the X direction with locationToGroup. We do this, by that their co-ordinate
                            // and size exactly match in the Y direction and that they at least touch in the X direction:
                            //       ___ ___         ___ ___
                            //      |   |   |       |___|   |
                            //      |___|___|           |___|
                            //
                            //      Continuous      Not continuous
                            //
                            // If the root divider is vertical, we check that the co-ordinate and size exactly match in the X
                            // direction and that they at least touch in the Y direction.

                            Boolean canBeGrouped =
                                blockingIsHorizontal ?
                                locationToGroup.ToRectValue().IsContinuousWith(blockingLocation.ToRectValue(), AxisType.X) :
                                locationToGroup.ToRectValue().IsContinuousWith(blockingLocation.ToRectValue(), AxisType.Y);

                            if (!canBeGrouped) continue;

                            locationsToRemove.Add(blockingLocation);
                            locationGrouping.Add(blockingLocation);
                        }

                        // Remove any of the locations that we grouped.
                        foreach (PlanogramBlockingLocation locationToRemove in locationsToRemove)
                        {
                            blockingLocations.Remove(locationToRemove);
                        }

                        if (locationGrouping.Any()) locationGroupings.Add(new LocationGrouping(locationGrouping));
                    }

                    return locationGroupings;
                }
            }

            #endregion  

            #region Constructors

            /// <summary>
            /// Instantiates a new <see cref="CompactPlacementGrouping"/> for the given <paramref name="blockingLocations"/> and 
            /// <paramref name="merchandisingGroups"/>.
            /// </summary>
            /// <param name="blockingLocations"></param>
            /// <param name="merchandisingGroups"></param>
            private CompactPlacementGrouping(
                IEnumerable<PlanogramBlockingLocation> blockingLocations, IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups)
            {
                _merchandisingGroups = merchandisingGroups.ToList();
                _blockingLocations = blockingLocations.ToList();
                _blockPlacements = new List<PlanogramMerchandisingBlockPlacement>();
                IsFullyCompacted = false;
            } 

            #endregion

            #region Methods

            /// <summary>
            /// Groups blocking locations together into continuous bands that can be compacted together and then further
            /// checks these bands against the merchandising groups for continuity (based on their co-ordinates) to ensure
            /// that oddly placed merchandising groups are not compacted with the other groups for that location band.
            /// </summary>
            /// <param name="merchandisingGroupsAndDvps"></param>
            /// <param name="blockingWidth"></param>
            /// <param name="blockingHeight"></param>
            /// <returns></returns>
            /// <remarks>
            /// In order to identify which compact placement groupings should exist, we need to:
            /// <list type="bullet">
            /// <item>Group the blocking locations into continuous bands;</item>
            /// <item>Identify which merchandising groups intersect with each location grouping;</item>
            /// <item>Where merchandising groups intersect with multiple location groupings, place 
            ///         them in separate compact placement groupings;</item>
            /// <item>Where merchandising groups are separated out, they should be grouped with other 
            ///         separated merchandising groups based on their co-ordinates and size.</item>
            /// </list>
            /// </remarks>
            public static IEnumerable<CompactPlacementGrouping> GetCompactPlacementGroupings(
                PlanogramMerchandisingBlocksParams.Block blockParams)
            {
                // Arrange the locations into continuous groups. Its these groups that form the basis of our arrangement
                // of block placements into compactable groups.
                IEnumerable<LocationGrouping> locationGroupings = LocationGrouping.GetLocationGroupings(blockParams.BlockingGroup);

                // Arrange the merchandising groups into continuous groups. We arrange the merchandising groups
                // into bands based on their design view co-ordinate in their facing axis, since this is the axis
                // that will be compacted.
                IEnumerable<IEnumerable<PlanogramMerchandisingGroup>> merchGroupGroupings = GetMerchGroupGroupings(blockParams);

                // Identify which location groupings each of the merchandising groups intersect with.
                Dictionary<PlanogramMerchandisingGroup, List<LocationGrouping>> locationGroupingsByMerchGroup =
                    GetLocationGroupingsByMerchGroup(locationGroupings, blockParams);

                // Pull out any merchandising groups that have no intersecting location grouping, or have more than
                // one location grouping - we need to deal with these separately.
                List<PlanogramMerchandisingGroup> merchGroupsToSeparate = locationGroupingsByMerchGroup
                    .Where(kvp => !kvp.Value.Any() || kvp.Value.Count > 1)
                    .Select(kvp => kvp.Key)
                    .ToList();

                // Organise the merchandising groups into their respective location groupings. This dictionarry may well
                // not contain all of the merchandising groups, but thats fine because our separated list tells us
                // which merchandising groups we need to mop up at the end.
                Dictionary<LocationGrouping, List<PlanogramMerchandisingGroup>> merchGroupsByLocationGrouping =
                    GetMerchGroupsByLocationGroupings(
                        locationGroupings, blockParams.MerchandisingGroupsAndDvps, blockParams.BlockingWidth, blockParams.BlockingHeight);

                // Group the location groupings into compact placement groupings, minus the merchandising groups we've
                // identified to group separately.
                List<CompactPlacementGrouping> compactPlacementGroupings = GetCompactPlacementGroupingsForLocationGroupings(
                    locationGroupings, merchGroupsToSeparate, merchGroupsByLocationGrouping);

                // At this stage, we should have a compact placement grouping for each location grouping.
                // Now we need to mop up any merchandising groups that we identified we needed to handle separately.
                compactPlacementGroupings.AddRange(
                    GetCompactPlacementGroupingsForRemainingMerchGroups(blockParams, merchGroupGroupings, merchGroupsToSeparate));

                return compactPlacementGroupings;
            }

            private static IEnumerable<CompactPlacementGrouping> GetCompactPlacementGroupingsForRemainingMerchGroups(
                PlanogramMerchandisingBlocksParams.Block blockParams, 
                IEnumerable<IEnumerable<PlanogramMerchandisingGroup>> merchGroupGroupings, 
                List<PlanogramMerchandisingGroup> merchGroupsToSeparate)
            {
                List<CompactPlacementGrouping> compactPlacementGroupings = new List<CompactPlacementGrouping>();

                while (merchGroupsToSeparate.Any())
                {
                    PlanogramMerchandisingGroup merchGroup = merchGroupsToSeparate.First();

                    IEnumerable<PlanogramMerchandisingGroup> merchGroupGrouping = merchGroupGroupings
                        .FirstOrDefault(m => m.Contains(merchGroup));

                    IEnumerable<PlanogramMerchandisingGroup> compactPlacementMerchGroups =
                        (merchGroupGrouping == null ?
                        new[] { merchGroup } :
                        new[] { merchGroup }.Union(merchGroupGrouping.Where(mg => merchGroupsToSeparate.Contains(mg))))
                        .ToList();

                    IEnumerable<PlanogramBlockingLocation> locationsOverlappingMerchGroups = blockParams.BlockingGroup.GetBlockingLocations()
                        .Where(l => compactPlacementMerchGroups.Any(mg => mg.DesignViewPosition.OverlapsWith(l, _overlapThreshold, blockParams.BlockingWidth, blockParams.BlockingHeight)))
                        .Distinct();

                    compactPlacementGroupings.Add(new CompactPlacementGrouping(
                        locationsOverlappingMerchGroups, compactPlacementMerchGroups));

                    foreach (PlanogramMerchandisingGroup compactPlacementMerchGroup in compactPlacementMerchGroups)
                    {
                        merchGroupsToSeparate.Remove(compactPlacementMerchGroup);
                    }
                }

                return compactPlacementGroupings;
            }

            private static Dictionary<PlanogramMerchandisingGroup, List<LocationGrouping>> GetLocationGroupingsByMerchGroup(
                IEnumerable<LocationGrouping> locationGroupings, PlanogramMerchandisingBlocksParams.Block blockParams)
            {
                Dictionary<PlanogramMerchandisingGroup, List<LocationGrouping>> locationGroupingsByMerchGroup =
                    new Dictionary<PlanogramMerchandisingGroup, List<LocationGrouping>>();

                foreach(KeyValuePair<PlanogramMerchandisingGroup, DesignViewPosition> merchGroupDvp in blockParams.MerchandisingGroupsAndDvps)
                {
                    List<LocationGrouping> matchingLocationGroupings = locationGroupings
                        .Where(lg =>
                            lg.Any(l =>
                                merchGroupDvp.Value.OverlapsWith(l, _overlapThreshold, blockParams.BlockingWidth, blockParams.BlockingHeight)))
                        .ToList();
                    locationGroupingsByMerchGroup.Add(merchGroupDvp.Key, matchingLocationGroupings);
                }

                return locationGroupingsByMerchGroup;
            }

            /// <summary>
            /// Creates a compact placement grouping for each location grouping provided, using the merchandising groups that have not been
            /// identified for separation. If no merchandising groups can be found for a given location grouping, no compact placement
            /// grouping is created for it.
            /// </summary>
            /// <param name="locationGroupings"></param>
            /// <param name="merchGroupsToSeparate"></param>
            /// <param name="merchGroupsByLocationGrouping"></param>
            /// <returns></returns>
            private static List<CompactPlacementGrouping> GetCompactPlacementGroupingsForLocationGroupings(IEnumerable<LocationGrouping> locationGroupings, IEnumerable<PlanogramMerchandisingGroup> merchGroupsToSeparate, Dictionary<LocationGrouping, List<PlanogramMerchandisingGroup>> merchGroupsByLocationGrouping)
            {
                List<CompactPlacementGrouping> compactPlacementGroupings = new List<CompactPlacementGrouping>();

                foreach (LocationGrouping locationGrouping in locationGroupings)
                {
                    List<PlanogramMerchandisingGroup> merchandisingGroups;
                    if (!merchGroupsByLocationGrouping.TryGetValue(locationGrouping, out merchandisingGroups)) continue;

                    IEnumerable<PlanogramMerchandisingGroup> merchGroupsExceptSeparated = merchandisingGroups
                        .Except(merchGroupsToSeparate)
                        .ToList();

                    if (!merchGroupsExceptSeparated.Any()) continue;

                    compactPlacementGroupings.Add(new CompactPlacementGrouping(locationGrouping, merchGroupsExceptSeparated));
                }

                return compactPlacementGroupings;
            }

            private static IEnumerable<IEnumerable<PlanogramMerchandisingGroup>> GetMerchGroupGroupings(PlanogramMerchandisingBlocksParams.Block blockParams)
            {
                return blockParams.MerchandisingGroupsAndDvps
                    .GroupBy(kvp =>
                        new Tuple<Single, Single>(
                            kvp.Value.GetBoundCoordinate(kvp.Key.GetFacingAxis()), kvp.Value.GetBoundSize(kvp.Key.GetFacingAxis())))
                    .Select(g => g.Select(kvp => kvp.Key))
                    .ToList();
            }

            /// <summary>
            /// Groups the merchandising groups by location grouping, based on which ones they overlap with. If a merchandising group
            /// overlaps with more than one location grouping, it will appear in the return dictionary more than once.
            /// </summary>
            /// <param name="locationGroupings"></param>
            /// <param name="merchandisingGroupsAndDvps"></param>
            /// <param name="blockingWidth"></param>
            /// <param name="blockingHeight"></param>
            /// <returns></returns>
            private static Dictionary<LocationGrouping, List<PlanogramMerchandisingGroup>> GetMerchGroupsByLocationGroupings(
                IEnumerable<LocationGrouping> locationGroupings, 
                Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps,
                Single blockingWidth,
                Single blockingHeight)
            {
                Dictionary<LocationGrouping, List<PlanogramMerchandisingGroup>> merchGroupsByLocationGroupings =
                    new Dictionary<LocationGrouping, List<PlanogramMerchandisingGroup>>();

                foreach(LocationGrouping locationGrouping in locationGroupings)
                {
                    List<PlanogramMerchandisingGroup> overlappingMerchGroups = new List<PlanogramMerchandisingGroup>();
                    foreach(KeyValuePair<PlanogramMerchandisingGroup, DesignViewPosition> merchGroupDvp in merchandisingGroupsAndDvps)
                    {
                        if(locationGrouping.Any(l => merchGroupDvp.Value.OverlapsWith(l,_overlapThreshold, blockingWidth, blockingHeight)))
                        {
                            overlappingMerchGroups.Add(merchGroupDvp.Key);
                        }
                    }

                    merchGroupsByLocationGroupings.Add(locationGrouping, overlappingMerchGroups);
                }

                return merchGroupsByLocationGroupings;
            }

            private static Boolean IsBlockingHorizontal(PlanogramBlockingGroup blockingGroup)
            {
                PlanogramBlockingDivider rootDivider = blockingGroup.Parent.Dividers.FirstOrDefault(d => d.Level == 1);
                Boolean blockingIsHorizontal = rootDivider == null ? false : rootDivider.Type == PlanogramBlockingDividerType.Horizontal;
                return blockingIsHorizontal;
            }

            #endregion
        }

        /// <summary>
        /// Simple class containing information about a <see cref="PlanogramMerchandisingBlockPlacement"/> prior
        /// to its addition to the <see cref="BlockPlacements"/> collection.
        /// </summary>
        private sealed class ProtoBlockPlacement
        {
            /// <summary>
            /// The <see cref="PlanogramMerchandisingBlockPlacement"/> that has been instantiated, but not yet added
            /// to the <see cref="BlockPlacements"/> collection.
            /// </summary>
            public PlanogramMerchandisingBlockPlacement BlockPlacement { get; }

            /// <summary>
            /// The <see cref="CompactPlacementGrouping"/> to which the <see cref="BlockPlacement"/> should be added.
            /// </summary>
            public CompactPlacementGrouping CompactPlacementGrouping { get; }

            public ProtoBlockPlacement(PlanogramMerchandisingBlockPlacement blockPlacement, CompactPlacementGrouping compactPlacementGrouping)
            {
                BlockPlacement = blockPlacement;
                CompactPlacementGrouping = compactPlacementGrouping;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Common constructor used by other internal constructors.
        /// </summary>
        private PlanogramMerchandisingBlock(
            PlanogramMerchandisingBlocksParams.Block blockParams,
            ICollection<PlanogramProduct> productsInSequence)
        {
            Boolean hasProductsInSequence = productsInSequence?.Any() ?? false;
            if (blockParams.BlockingGroup == null)
                Debug.Fail($"{nameof(blockParams.BlockingGroup)} should not be null.");
            if (!hasProductsInSequence &&
                blockParams.SequenceGroup == null)
                Debug.Fail($"{nameof(blockParams.SequenceGroup)} should not be null when no {nameof(productsInSequence)} are provided.");
            if (blockParams.ParentBlocks == null)
                Debug.Fail($"{nameof(blockParams.ParentBlocks)} should not be null.");
            if (blockParams.SequenceGroup != null &&
                blockParams.BlockingGroup.Colour != blockParams.SequenceGroup.Colour)
                Debug.Fail(
                    $"{nameof(blockParams.BlockingGroup)}.{nameof(blockParams.BlockingGroup.Colour)} should not be the same as {nameof(blockParams.SequenceGroup)}.{nameof(blockParams.SequenceGroup.Colour)}.");

            Parent = blockParams.ParentBlocks;
            BlockingGroup = blockParams.BlockingGroup;
            SequenceGroup = 
                hasProductsInSequence ? 
                CreateSequenceGroupFromProducts(productsInSequence, blockParams.SequenceGroup) :
                blockParams.SequenceGroup;
            Colour = BlockingGroup.Colour;
            _sequenceProductsByGtin = SequenceGroup.Products.ToDictionary(p => p.Gtin);
            _assortmentRuleEnforcer = blockParams.AssortmentRuleEnforcer;
            PositionsPresentationStates = new Dictionary<String, PlanogramMerchandisingPositionPresentationState>();
        }

        /// <summary>
        /// Position Space Constructor - Constructs a <see cref="PlanogramMerchandisingBlock"/> that represents the space occupied on the planogram
        /// by the <paramref name="blockingGroup"/>'s positions, identified from <paramref name="productsInSequence"/>.
        /// </summary>
        /// <param name="productsInSequence">
        /// The products that exist in the <paramref name="blockingGroup"/>. This enumeration provides the range and sequence
        /// for this block.
        /// </param>
        /// <param name="isBlockOfInterest">
        /// If true, the block's space will be augmented by the available whitespace on its merchandising groups. 
        /// If false, just the space occupied by the block's positions will be used.
        /// </param>
        /// <remarks>
        /// This method relies on the <see cref="SequenceColour"/> property being set against <see cref="PlanogramPosition"/>.
        /// Without this value, the method is unable to find positions for the blocking group.
        /// </remarks>
        internal PlanogramMerchandisingBlock(
            PlanogramMerchandisingBlocksParams.Block blockParams,
            ICollection<PlanogramProduct> productsInSequence,
            Boolean isBlockOfInterest,
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType) 
            : this(blockParams, productsInSequence)
        {
            // First, build a lookup of the merchandising groups that intersect with this blocking group, keyed
            // by blocking location. We need these to make sure that we don't miss any merchandising groups that
            // the block intersects with that don't have any positions on.
            Dictionary<PlanogramMerchandisingGroup, List<PlanogramBlockingLocation>> intersectingLocationsByMerchGroup = 
                BlockingGroup.GetIntersectingLocationsByMerchGroup(
                    blockParams.MerchandisingGroupsAndDvps, _overlapThreshold, blockParams.BlockingWidth, blockParams.BlockingHeight);

            // Get the existing positions, if any, that exist on the planogram. This constructor uses
            // these positions to determine the blocking space available (blockingGroup is just used for 
            // presentation reference). 
            List<String> gtinsInSequence = productsInSequence.Select(p => p.Gtin).ToList();
            Dictionary<PlanogramMerchandisingGroup, PlanogramPositionPlacementList> blockPositionsByMerchGroup =
                GetBlockPositionsByMerchandisingGroup(blockParams, gtinsInSequence);

            // If the block is of interest, store the presentation state of the positions.
            if (isBlockOfInterest)
            {
                PlanogramMerchandisingGroupList merchGroupList = new PlanogramMerchandisingGroupList();
                merchGroupList.AddRange(blockParams.MerchandisingGroupsAndDvps.Keys);
                PositionsPresentationStates = merchGroupList.GetPresentationStates(blockParams.BlockingGroup, gtinsInSequence);
            }

            // Now build a collection of merchandising groups that this block covers. These are the merchandising
            // groups that the block's positions are on as well as any merchandising groups that this block
            // intersects with.
            IEnumerable<PlanogramMerchandisingGroup> blockMerchandisingGroups = intersectingLocationsByMerchGroup
                .Keys
                .Union(blockPositionsByMerchGroup.Keys)
                .Distinct()
                .ToList();

            // We'll also need a dictionary of the sequenced subcomponent information (describing which order to place products
            // in the sequence snake), keyed by the subcomponent's id.
            Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById =
                GetSequencedSubComponentsById(blockMerchandisingGroups);

            // Arrange the merchandising groups into "groups" of locations. The location groupings are based
            // on continuity in space in the blocking's primary axis (the direction of its root level dividers).
            IEnumerable<CompactPlacementGrouping> compactPlacementGroupings = 
                CompactPlacementGrouping.GetCompactPlacementGroupings(blockParams);
            Dictionary<PlanogramMerchandisingGroup, Dictionary<BlockingLocationPlaceHolder,CompactPlacementGrouping>> compactPlacementGroupingsByMerchGroupBlockingLocation =
                GetCompactPlacementGroupingsByMerchandisingGroupBlockingLocation(compactPlacementGroupings);

            // Iterate through each of the merchandising groups for this block in sequence order and create a
            // block placement for each of the position clusters within it.
            IEnumerable<ProtoBlockPlacement> protoBlockPlacements = BuildPositionSpaceProtoBlockPlacements(
                blockParams,
                isBlockOfInterest,
                spaceConstraintType,
                intersectingLocationsByMerchGroup,
                gtinsInSequence,
                blockPositionsByMerchGroup,
                blockMerchandisingGroups,
                sequencedSubComponentsById,
                compactPlacementGroupingsByMerchGroupBlockingLocation);

            // Finally, merge and block placements that are continuous and add the block placements and
            // their compact placement groups to this instance's backing fields.
            IEnumerable<ProtoBlockPlacement> finalProtoPlacements = MergeContinuousProtoBlockPlacements(protoBlockPlacements);
            _compactPlacementGroupings = PrepareCompactPlacementGroupings(finalProtoPlacements);
            _blockPlacements = GetSortedBlockPlacements(finalProtoPlacements);

            // Apply the "within block presentation" constraint to the block placements, if required.
            if (isBlockOfInterest && spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation)
            {
                CalculateAndApplyMinimumBlockSpaceInFacingAxis();
            }
        }

        /// <summary>
        /// Block Space Constructor - Constructs a new <see cref="PlanogramMerchandisingBlock"/> for the given <paramref name="blockingGroup"/> and 
        /// <paramref name="sequenceGroup"/>, populating <see cref="BlockPlacements"/> property in the order that they 
        /// should be processed for sequencing.
        /// </summary>
        /// <remarks>
        /// Note that this constructor is internal, because <see cref="PlanogramMerchandisingBlock"/> objects should be instantiated
        /// using the constructor of their parent, <see cref="PlanogramMerchandisingBlocks"/>.
        /// <para></para>
        /// The aim of this constructor is to create a list of <see cref="PlanogramMerchandisingBlockPlacement"/>s that represent
        /// chunks of space that can be merchandised within this <see cref="BlockingGroup"/>. In order to do this,
        /// we'll need to consider the space occupied by the <see cref="PlanogramBlockingLocation"/>s within this <see cref="BlockingGroup"/>,
        /// the space available to merchandise on the <see cref="PlanogramSubComponent"/>s that intersect with this group and 
        /// whether or not any of the resulting blocks of space are continuous and can be combined.
        /// </remarks>
        internal PlanogramMerchandisingBlock(PlanogramMerchandisingBlocksParams.Block blockParams) 
            : this(blockParams, null)
        {
            // First, build a lookup of the merchandising groups that intersect with this blocking group, keyed
            // by blocking location.
            ILookup<PlanogramBlockingLocation, PlanogramMerchandisingGroup> intersectingMerchGroupsByLocation =
                BlockingGroup.GetIntersectingMerchGroupsByLocation(
                    blockParams.MerchandisingGroupsAndDvps, _overlapThreshold, blockParams.BlockingWidth, blockParams.BlockingHeight);
            Dictionary<PlanogramMerchandisingGroup, List<PlanogramBlockingLocation>> intersectingLocationsByMerchGroup =
                BlockingGroup.GetIntersectingLocationsByMerchGroup(
                    blockParams.MerchandisingGroupsAndDvps, _overlapThreshold, blockParams.BlockingWidth, blockParams.BlockingHeight);

            // We'll also need a dictionary of the sequenced subcomponent information (describing which order to place products
            // in the sequence snake), keyed by the subcomponent's id.
            Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById =
                GetSequencedSubComponentsById(intersectingLocationsByMerchGroup.Keys);

            // Arrange the merchandising groups into "groups" of locations. The location groupings are based
            // on continuity in space in the blocking's primary axis (the direction of its root level dividers).
            IEnumerable<CompactPlacementGrouping> compactPlacementGroupings = 
                CompactPlacementGrouping.GetCompactPlacementGroupings(blockParams);
            Dictionary<PlanogramMerchandisingGroup, Dictionary<BlockingLocationPlaceHolder,CompactPlacementGrouping>> compactPlacementGroupingsByMerchGroupBlockingLocation =
                GetCompactPlacementGroupingsByMerchandisingGroupBlockingLocation(compactPlacementGroupings);

            // Next, prepare an enumeration of blocking locations for this blocking group, in their sequence order.
            IEnumerable<PlanogramSequencedItem<PlanogramBlockingLocation>> blockingLocationsOrderedBySequence =
                EnumerateBlockingLocationsInSequence(blockParams.BlockingGroup, intersectingMerchGroupsByLocation)
                .ToList();

            HashSet<String> sequenceGtinsSet = new HashSet<String>(blockParams.SequenceGroup.Products.Select(product => product.Gtin));

            // Store the presentation states of this block's merchandising groups.
            PlanogramMerchandisingGroupList merchGroupList = new PlanogramMerchandisingGroupList();
            merchGroupList.AddRange(blockParams.MerchandisingGroupsAndDvps.Keys);
            PositionsPresentationStates = merchGroupList.GetPresentationStates(blockParams.BlockingGroup, sequenceGtinsSet);

            // The Block placements represents the space that can be merchandised on a merchandising group
            // for this blocking group. We take the merchandising groups that intersect with the space occupied
            // by this blocking group and then evaluate each one for the volumes that correspond to space that can
            // be merchandised.
            IEnumerable<ProtoBlockPlacement> protoBlockPlacements = BuildBlockSpaceProtoBlockPlacements(
                blockParams, 
                intersectingLocationsByMerchGroup, 
                sequencedSubComponentsById,
                compactPlacementGroupingsByMerchGroupBlockingLocation, 
                blockingLocationsOrderedBySequence);

            // Finally, merge and block placements that are continuous and add the block placements and
            // their compact placement groups to this instance's backing fields.
            IEnumerable<ProtoBlockPlacement> finalProtoPlacements = MergeContinuousProtoBlockPlacements(protoBlockPlacements);
            _compactPlacementGroupings = PrepareCompactPlacementGroupings(finalProtoPlacements);
            _blockPlacements = GetSortedBlockPlacements(finalProtoPlacements);
        }
        #endregion

        #region Methods

        #region Construction Methods

        private IEnumerable<ProtoBlockPlacement> BuildPositionSpaceProtoBlockPlacements(
            PlanogramMerchandisingBlocksParams.Block blockParams, 
            Boolean isBlockOfInterest, 
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType, 
            Dictionary<PlanogramMerchandisingGroup, List<PlanogramBlockingLocation>> intersectingLocationsByMerchGroup, 
            List<String> gtinsInSequence, 
            Dictionary<PlanogramMerchandisingGroup, PlanogramPositionPlacementList> blockPositionsByMerchGroup, 
            IEnumerable<PlanogramMerchandisingGroup> blockMerchandisingGroups, 
            Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById, 
            Dictionary<PlanogramMerchandisingGroup, Dictionary<BlockingLocationPlaceHolder,CompactPlacementGrouping>> compactPlacementGroupingsByMerchGroupBlockingLocation)
        {
            Int32 sequence = 1;
            List<ProtoBlockPlacement> protoBlockPlacements = new List<ProtoBlockPlacement>();

            foreach (PlanogramMerchandisingGroup merchGroup in GetMerchGroupsInSequence(blockMerchandisingGroups, sequencedSubComponentsById))
            {
                // Get the location grouping for this merch group.
                Dictionary<BlockingLocationPlaceHolder, CompactPlacementGrouping> compactPlacementGroupingsByBlockingLocation;
                if (!compactPlacementGroupingsByMerchGroupBlockingLocation.TryGetValue(merchGroup, out compactPlacementGroupingsByBlockingLocation)) continue;

                // If there are position placements for this merchandising group, then create block placements
                // for each of the position clusters on the merchandising group.
                PlanogramPositionPlacementList positionPlacements;
                if (blockPositionsByMerchGroup.TryGetValue(merchGroup, out positionPlacements))
                {
                    // Arrange the positions for this block/merch group in sequence order.
                    IEnumerable<ProtoBlockPlacement> positionClusterProtoPlacements = CreateProtoPlacementsFromPositionClusters(
                        blockParams,
                        positionPlacements,
                        merchGroup,
                        spaceConstraintType,
                        sequencedSubComponentsById,
                        compactPlacementGroupingsByBlockingLocation,
                        sequence,
                        isBlockOfInterest,
                        gtinsInSequence);

                    protoBlockPlacements.AddRange(positionClusterProtoPlacements);
                    sequence += positionClusterProtoPlacements.Count();
                }
                // If there are no positions on this merchandising group, then build a block placement for the whole
                // merchandising group (within the block), as long as this is a block of interest.
                else if (isBlockOfInterest)
                {
                    DesignViewPosition merchGroupDvp;
                    if (!blockParams.MerchandisingGroupsAndDvps.TryGetValue(merchGroup, out merchGroupDvp)) continue;

                    List<PlanogramBlockingLocation> blockingLocations;
                    if (!intersectingLocationsByMerchGroup.TryGetValue(merchGroup, out blockingLocations)) continue;

                    foreach (PlanogramBlockingLocation blockingLocation in blockingLocations)
                    {
                        CompactPlacementGrouping compactPlacementGrouping;
                        if (!compactPlacementGroupingsByBlockingLocation.TryGetValue(new BlockingLocationPlaceHolder(blockingLocation), out compactPlacementGrouping)) continue;

                        // There were no positions found for this merchandising group, but this group
                        // should still form part of the block's merchandisable space, because it intersects
                        // with the blocking group.
                        ProtoBlockPlacement protoBlockPlacement = CreateProtoPlacementForMerchandisingGroup(
                            blockParams,
                            merchGroup,
                            merchGroupDvp,
                            sequence,
                            blockingLocation,
                            compactPlacementGrouping,
                            sequencedSubComponentsById);

                        if (protoBlockPlacement != null)
                        {
                            sequence++;
                            protoBlockPlacements.Add(protoBlockPlacement);
                        } 
                    }
                }
            }

            return protoBlockPlacements;
        }

        private Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> GetSequencedSubComponentsById(
            IEnumerable<PlanogramMerchandisingGroup> blockMerchandisingGroups)
        {
            return BlockingGroup
                            .EnumerateSequencableItemsInSequence(blockMerchandisingGroups.SelectMany(m => m.SubComponentPlacements))
                            .ToDictionary(s => s.Item.SubComponent.Id);
        }

        private static IEnumerable<PlanogramMerchandisingGroup> GetMerchGroupsInSequence(
            IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups,
            Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById)
        {
            return merchandisingGroups
                .OrderBy(m => sequencedSubComponentsById[m.SubComponentPlacements.First().SubComponent.Id].Sequence);
        }

        private IEnumerable<ProtoBlockPlacement> BuildBlockSpaceProtoBlockPlacements(
            PlanogramMerchandisingBlocksParams.Block blockParams,
            Dictionary<PlanogramMerchandisingGroup, List<PlanogramBlockingLocation>> intersectingLocationsByMerchGroup, 
            Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById, 
            Dictionary<PlanogramMerchandisingGroup, Dictionary<BlockingLocationPlaceHolder,CompactPlacementGrouping>> compactPlacementGroupingsByBlockingLocationMerchGroup, 
            IEnumerable<PlanogramSequencedItem<PlanogramBlockingLocation>> blockingLocationsOrderedBySequence)
        {
            Int32 sequence = 1;
            List<ProtoBlockPlacement> protoBlockPlacements = new List<ProtoBlockPlacement>();
            foreach (PlanogramMerchandisingGroup merchandisingGroup in
                GetMerchGroupsInSequence(intersectingLocationsByMerchGroup.Keys, sequencedSubComponentsById))
            {
                DesignViewPosition merchGroupDvp;
                if (!blockParams.MerchandisingGroupsAndDvps.TryGetValue(merchandisingGroup, out merchGroupDvp)) continue;

                Dictionary<BlockingLocationPlaceHolder, CompactPlacementGrouping> compactPlacementGroupingsByBlockingLocation;
                if (!compactPlacementGroupingsByBlockingLocationMerchGroup.TryGetValue(merchandisingGroup, out compactPlacementGroupingsByBlockingLocation)) continue;

                // Now identify the blocking locations that this merchandising group covers.
                List<PlanogramBlockingLocation> blockingLocations;
                if (!intersectingLocationsByMerchGroup.TryGetValue(merchandisingGroup, out blockingLocations)) continue;

                // Now, arrange those blocking locations in sequence order.
                IEnumerable<PlanogramBlockingLocation> locationsInSequence = blockingLocationsOrderedBySequence
                    .Where(l => blockingLocations.Contains(l.Item))
                    .Select(l => l.Item);

                foreach (PlanogramBlockingLocation blockingLocation in locationsInSequence)
                {
                    CompactPlacementGrouping compactPlacementGrouping;
                    if (!compactPlacementGroupingsByBlockingLocation.TryGetValue(new BlockingLocationPlaceHolder(blockingLocation), out compactPlacementGrouping)) continue;

                    ProtoBlockPlacement protoBlockPlacement = CreateProtoPlacementForMerchandisingGroup(
                        blockParams,
                        merchandisingGroup,
                        merchGroupDvp,
                        sequence,
                        blockingLocation,
                        compactPlacementGrouping,
                        sequencedSubComponentsById);

                    if (protoBlockPlacement != null)
                    {
                        sequence++;
                        protoBlockPlacements.Add(protoBlockPlacement);
                    }
                }
            }

            return protoBlockPlacements;
        }

        /// <summary>
        /// Re-sequences and returns the given <paramref name="protoBlockPlacements"/> in sequence order.
        /// </summary>
        /// <param name="protoBlockPlacements"></param>
        /// <returns></returns>
        private SortedList<Int32, PlanogramMerchandisingBlockPlacement> GetSortedBlockPlacements(IEnumerable<ProtoBlockPlacement> protoBlockPlacements)
        {
            SortedList<Int32, PlanogramMerchandisingBlockPlacement> sortedBlockPlacements =
                new SortedList<int, PlanogramMerchandisingBlockPlacement>();

            // Add the block placements to the return list in sequence order. We keep track of the sequences
            // here and re-assign them if necessary, because there might be gaps in the final list of
            // proto block placement sequences.
            Int32 sequence = 1;
            foreach (PlanogramMerchandisingBlockPlacement blockPlacement in
                protoBlockPlacements.Select(p => p.BlockPlacement).OrderBy(bp => bp.SequenceNumber))
            {
                if (blockPlacement.SequenceNumber != sequence)
                {
                    blockPlacement.ReSequence(sequence);
                }

                sortedBlockPlacements.Add(sequence, blockPlacement);

                sequence++;
            }

            return sortedBlockPlacements;
        }

        /// <summary>
        /// Merges any continuous blockplacements and returns the merged collection.
        /// </summary>
        /// <param name="protoBlockPlacements"></param>
        /// <returns></returns>
        private IEnumerable<ProtoBlockPlacement> MergeContinuousProtoBlockPlacements(IEnumerable<ProtoBlockPlacement> protoBlockPlacements)
        {
            List<ProtoBlockPlacement> mergedBlockPlacements = new List<ProtoBlockPlacement>();
            List<ProtoBlockPlacement> availableBlockPlacements = new List<ProtoBlockPlacement>(
                protoBlockPlacements.OrderBy(p => p.BlockPlacement.SequenceNumber));

            while (availableBlockPlacements.Any())
            {
                ProtoBlockPlacement currentPlacement = availableBlockPlacements.First();
                availableBlockPlacements.Remove(currentPlacement);
                mergedBlockPlacements.Add(currentPlacement);

                IEnumerable<ProtoBlockPlacement> blockPlacementsOnSameMerchGroup = availableBlockPlacements
                    .Where(p => p.BlockPlacement.MerchandisingGroup == currentPlacement.BlockPlacement.MerchandisingGroup)
                    .OrderBy(p => p.BlockPlacement.Bounds.GetCoordinate(p.BlockPlacement.MerchandisingGroup.GetFacingAxis()))
                    .ToList();

                foreach (ProtoBlockPlacement protoPlacement in blockPlacementsOnSameMerchGroup)
                {
                    if (!currentPlacement.BlockPlacement.CanMergeWith(protoPlacement.BlockPlacement)) continue;
                    currentPlacement.BlockPlacement.MergeWith(protoPlacement.BlockPlacement);
                    availableBlockPlacements.Remove(protoPlacement);
                }
            }

            return mergedBlockPlacements;
        }

        /// <summary>
        /// Gets dictionary lookup of sequence line positions
        /// </summary>
        /// <param name="positionPlacements"></param>
        /// <param name="merchGroup"></param>
        /// <returns></returns>
        private static Dictionary<Int16, List<PlanogramPositionPlacement>> GetSequenceLinePlacements(IEnumerable<PlanogramPositionPlacement> positionPlacements, PlanogramMerchandisingGroup merchGroup)
        {
            Dictionary<Int16, List<PlanogramPositionPlacement>> sequenceLinePlacements = new Dictionary<Int16, List<PlanogramPositionPlacement>>();
            Boolean sequenceLineYDirection = false;
            PlanogramSubComponentMerchandisingType planogramSubComponentMerchandisingType = merchGroup.SubComponentPlacements.First().SubComponent.MerchandisingType;

            if (planogramSubComponentMerchandisingType == PlanogramSubComponentMerchandisingType.Hang ||
                planogramSubComponentMerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
            {
                sequenceLineYDirection = true;
            }

            foreach (PlanogramPositionPlacement positionPlacement in positionPlacements)
            {
                List<PlanogramPositionPlacement> placements;
                if (!sequenceLinePlacements.TryGetValue(sequenceLineYDirection ? positionPlacement.Position.SequenceY : positionPlacement.Position.SequenceZ, out placements))
                {
                    placements = new List<PlanogramPositionPlacement>();
                    sequenceLinePlacements.Add(sequenceLineYDirection ? positionPlacement.Position.SequenceY : positionPlacement.Position.SequenceZ, placements);
                }
                placements.Add(positionPlacement);
            }
            return sequenceLinePlacements;
        }

        /// <summary>
        /// Gets the merchandisable space for <paramref name="merchandisingGroup"/> that fits within <paramref name="designViewBlockingSpace"/>
        /// relative to the <paramref name="merchandisingGroup"/>.
        /// </summary>
        /// <param name="merchandisingGroup"></param>
        /// <param name="merchGroupDvp"></param>
        /// <param name="designViewBlockingSpace"></param>
        /// <returns>A <see cref="PlanogramMerchandisingSpace"/> or null if the space does not lie within <paramref name="designViewBlockingSpace"/>.</returns>
        private static PlanogramMerchandisingSpace GetMerchandisingSpaceInBlock(
            PlanogramMerchandisingGroup merchandisingGroup,
            DesignViewPosition merchGroupDvp,
            RectValue designViewBlockingSpace,
            Boolean isBlockingLocationAtTop)
        {
            // First, get the plan relative merchandising space for the merch group
            PlanogramMerchandisingSpace merchSpace = merchandisingGroup.GetMerchandisingSpace();

            // Now, if the merch space is at the top of the plan, ensure that the constraining space encompasses
            // its height, because it may have been adjusted to fit the highest product.
            RectValue adjustedBlockingSpace;
            if (merchSpace.IsTopSubComponent &&
                isBlockingLocationAtTop &&
                !merchandisingGroup.SubComponentPlacements.First().Component.IsMerchandisedTopDown)
            {
                adjustedBlockingSpace = new RectValue(
                    designViewBlockingSpace.X,
                    designViewBlockingSpace.Y,
                    designViewBlockingSpace.Z,
                    designViewBlockingSpace.Width,
                    Math.Max(
                        merchSpace.SpaceReducedByObstructions.Y + merchSpace.SpaceReducedByObstructions.Height -
                        designViewBlockingSpace.Y,
                        designViewBlockingSpace.Height),
                    designViewBlockingSpace.Depth);
            }
            else
            {
                adjustedBlockingSpace = designViewBlockingSpace;
            }

            // Next, transform the merch space to be merch group relative.
            PlanogramMerchandisingSpace merchGroupMerchSpace =
                merchSpace.ToMerchandisingGroupRelative(merchandisingGroup);

            // Then apply the design view position's transformation (which is merch group relative) to get the merch space
            // in world design view space.
            PlanogramMerchandisingSpace designViewMerchSpace =
                merchGroupMerchSpace.Transform(merchGroupDvp.TranslationMatrix, merchGroupDvp.TransformationRotation);

            // Compare the design view space to the blocking space (which should also be in design view space).
            PlanogramMerchandisingSpace designViewMerchSpaceInBlock =
                designViewMerchSpace.ReduceToFit(adjustedBlockingSpace);
            if (designViewMerchSpaceInBlock == null) return null;

            // Finally, revers the design view transform of the reduced space to get the reduced space in merch group relative space.
            PlanogramMerchandisingSpace merchGroupMerchSpaceInBlock =
                designViewMerchSpaceInBlock.ReverseTransform(merchGroupDvp.TranslationMatrix,
                    merchGroupDvp.TransformationRotation);

            return merchGroupMerchSpaceInBlock;
        }

        private RectValue ClipToMerchGroupBounds(RectValue spaceToClip, PlanogramMerchandisingGroup merchGroup)
        {
            return new RectValue(
                ClipCoordToMerchGroupBounds(AxisType.X, spaceToClip, merchGroup),
                ClipCoordToMerchGroupBounds(AxisType.Y, spaceToClip, merchGroup),
                ClipCoordToMerchGroupBounds(AxisType.Z, spaceToClip, merchGroup),
                spaceToClip.Width,
                spaceToClip.Height,
                spaceToClip.Depth);
        }

        private Single ClipCoordToMerchGroupBounds(AxisType axis, RectValue rectValue, PlanogramMerchandisingGroup merchGroup)
        {
            Single coord = rectValue.GetCoordinate(axis);
            if (coord.LessThan(merchGroup.GetAxis(axis).Min))
            {
                coord = merchGroup.GetAxis(axis).Min;
            }
            else if ((coord + rectValue.GetSize(axis)).GreaterThan(merchGroup.GetAxis(axis).Max))
            {
                Single newCoord = merchGroup.GetAxis(axis).Max - rectValue.GetSize(axis);
                if (newCoord.GreaterOrEqualThan(merchGroup.GetAxis(axis).Min))
                {
                    coord = newCoord;
                }
                else
                {
                    coord = merchGroup.GetAxis(axis).Min;
                }
            }
            return coord;
        }

        /// <summary>
        /// Obtains the minimum block space gain of block placements within compact placement groupings. Once this is obtained this will be applied to all block placements that exceed the minimum block space gain.
        /// </summary>
        private void CalculateAndApplyMinimumBlockSpaceInFacingAxis()
        {
            foreach (CompactPlacementGrouping compactPlacementGrouping in _compactPlacementGroupings)
            {
                if (!compactPlacementGrouping.BlockPlacements.Any()) continue;
                AxisType facingAxis = compactPlacementGrouping.BlockPlacements.First().MerchandisingGroup.SubComponentPlacements.First().GetFacingAxis();
                Single minBlockSpaceInFacingAxis = compactPlacementGrouping.BlockPlacements.Min(b => b.Bounds.GetSize(facingAxis));

                foreach (PlanogramMerchandisingBlockPlacement blockPlacement in compactPlacementGrouping.BlockPlacements)
                {
                    blockPlacement.UpdatePlacementBounds(new RectValue(
                        blockPlacement.Bounds.X,
                        blockPlacement.Bounds.Y,
                        blockPlacement.Bounds.Z,
                        GetMinSpaceInFacingAxis(AxisType.X, blockPlacement, minBlockSpaceInFacingAxis, facingAxis),
                        GetMinSpaceInFacingAxis(AxisType.Y, blockPlacement, minBlockSpaceInFacingAxis, facingAxis),
                        GetMinSpaceInFacingAxis(AxisType.Z, blockPlacement, minBlockSpaceInFacingAxis, facingAxis)));
                }
            }
        }

        private Single GetMinSpaceInFacingAxis(AxisType axis, PlanogramMerchandisingBlockPlacement blockPlacement, Single minSize, AxisType facingAxis)
        {
            if (axis != facingAxis) return blockPlacement.Bounds.GetSize(axis);
            return Math.Max(blockPlacement.OriginalBounds.GetSize(axis), minSize);
        }

        /// <summary>
        /// Groups the <paramref name="positionPlacements"/> into consecutive clusters based on their merchandising sequence values
        /// </summary>
        /// <param name="positionPlacements"></param>
        /// <param name="merchGroup"></param>
        /// <returns></returns>
        private static IEnumerable<PlanogramPositionPlacementList> GetPositionClusters(
            IEnumerable<PlanogramPositionPlacement> positionPlacements, PlanogramMerchandisingGroup merchGroup)
        {
            List<PlanogramPositionPlacementList> positionClusters = new List<PlanogramPositionPlacementList>();

            // Create sequence line placements (used to determine if the position is consecutive)
            Dictionary<Int16, List<PlanogramPositionPlacement>> sequenceLinePlacementLookup = GetSequenceLinePlacements(positionPlacements, merchGroup);

            PlanogramPositionPlacementList positionCluster = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList();
            positionClusters.Add(positionCluster);

            // process by sequence lines
            foreach (Int16 sequenceLine in sequenceLinePlacementLookup.Keys)
            {
                foreach (PlanogramPositionPlacement position in sequenceLinePlacementLookup[sequenceLine])
                {
                    // get current cluster positions and next sequence line positions (required to find if the positions are consecutive)
                    if (IsConsecutiveWithClusterOrOtherSequenceLinePositions(position, positionCluster, sequenceLinePlacementLookup[sequenceLine]))
                    {
                        positionCluster.Add(position);
                    }
                    else
                    {
                        if (positionCluster.Any()) // may not need to create a new cluster if this is the first position being processed.
                        {
                            // The position is not consecutive new cluster required
                            positionCluster = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList();
                            positionClusters.Add(positionCluster);
                        }

                        positionCluster.Add(position);
                    }
                }
            }

            return positionClusters;
        }

        /// <summary>
        /// Determines if the <paramref name="position"/> is consecutive within the current cluster based upon the given positions and sequence values.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="positionCluster"></param>
        /// <returns></returns>
        private static Boolean IsConsecutiveWithClusterOrOtherSequenceLinePositions(
            PlanogramPositionPlacement position, PlanogramPositionPlacementList positionCluster, List<PlanogramPositionPlacement> sequencePositions)
        {
            foreach (PlanogramPositionPlacement positionPlacement in positionCluster)
            {
                // validate position against existing cluster placements
                if (position.IsConsecutiveInSequenceWith(positionPlacement))
                {
                    return true;
                }

                // Now check the position placement against sequence line placements to see if the position is part of a consecuetive sequence group.
                foreach (PlanogramPositionPlacement sequencePlacement in sequencePositions)
                {
                    if (positionPlacement.IsConsecutiveInSequenceWith(sequencePlacement))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static Dictionary<PlanogramMerchandisingGroup, PlanogramPositionPlacementList> GetBlockPositionsByMerchandisingGroup(
            PlanogramMerchandisingBlocksParams.Block blockParams,
            List<String> gtinsInSequence)
        {
            Dictionary<PlanogramMerchandisingGroup, PlanogramPositionPlacementList> blockPositionsByMerchGroup =
                new Dictionary<PlanogramMerchandisingGroup, PlanogramPositionPlacementList>();
            foreach (PlanogramMerchandisingGroup merchandisingGroup in blockParams.MerchandisingGroups)
            {
                Boolean positionAdded = false;
                PlanogramPositionPlacementList positionPlacements = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList();
                foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                {
                    if (!positionPlacement.Position.SequenceColour.Equals(blockParams.BlockingGroup.Colour) ||
                        !gtinsInSequence.Contains(positionPlacement.Product.Gtin)) continue;

                    positionPlacements.Add(positionPlacement);
                    positionAdded = true;
                }
                if (positionAdded) blockPositionsByMerchGroup.Add(merchandisingGroup, positionPlacements);
            }
            return blockPositionsByMerchGroup;
        }

        private PlanogramSequenceGroup CreateSequenceGroupFromProducts(
                    IEnumerable<PlanogramProduct> productsInSequence, PlanogramSequenceGroup originalSequenceGroup)
        {
            PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(originalSequenceGroup);
            Int32 sequence = 0;
            foreach (PlanogramProduct product in productsInSequence)
            {
                PlanogramSequenceGroupProduct sequenceProduct = sequenceGroup.Products.FirstOrDefault(p => p.Gtin.Equals(product.Gtin));
                if (sequenceProduct == null) continue;
                sequenceProduct.SequenceNumber = ++sequence;
            }
            return sequenceGroup;
        }

        private static PlanogramMerchandisingSpace GetMerchandisingSpaceInBlockingLocations(
                            PlanogramMerchandisingBlocksParams.Block blockParams,
                            PlanogramMerchandisingGroup merchGroup,
                            IEnumerable<PlanogramBlockingLocation> blockingLocations)
        {
            PlanogramMerchandisingSpace merchGroupMerchSpaceInBlock = null;
            foreach (PlanogramBlockingLocation blockingLocation in blockingLocations)
            {
                Boolean isBlockingLocationAtTop = (blockingLocation.Y + blockingLocation.Height).EqualTo(1);
                RectValue designViewBlockingSpace = new DesignViewPosition(
                    blockingLocation,
                    blockParams.BlockingWidth,
                    blockParams.BlockingHeight,
                    merchGroup.SubComponentPlacements.First().Fixture.Depth)
                    .ToRectValue();

                if (merchGroupMerchSpaceInBlock == null)
                {
                    merchGroupMerchSpaceInBlock = GetMerchandisingSpaceInBlock(
                        merchGroup, merchGroup.DesignViewPosition, designViewBlockingSpace, isBlockingLocationAtTop);
                }
                else
                {
                    PlanogramMerchandisingSpace combineWith = GetMerchandisingSpaceInBlock(
                        merchGroup, merchGroup.DesignViewPosition, designViewBlockingSpace, isBlockingLocationAtTop);
                    if (combineWith != null)
                    {
                        merchGroupMerchSpaceInBlock = merchGroupMerchSpaceInBlock.CombineWith(combineWith);
                    }
                }
            }
            return merchGroupMerchSpaceInBlock;
        }

        private RectValue IncreaseSpaceByWhiteSpace(
                            RectValue space,
                            PlanogramMerchandisingGroup merchGroup,
                            PlanogramPositionPlacement positionPlacement,
                            Int32 positionClusterCount,
                            AxisType facingAxis)
        {
            return new RectValue(
                space.X,
                space.Y,
                space.Z,
                space.Width + merchGroup.GetWhiteSpaceOnAxis(AxisType.X, positionPlacement) / (facingAxis == AxisType.X ? positionClusterCount : 1),
                space.Height + merchGroup.GetWhiteSpaceOnAxis(AxisType.Y, positionPlacement) / (facingAxis == AxisType.Y ? positionClusterCount : 1),
                space.Depth + merchGroup.GetWhiteSpaceOnAxis(AxisType.Z, positionPlacement) / (facingAxis == AxisType.Z ? positionClusterCount : 1));
        }

        private RectValue CenterSpaceOn(RectValue spaceToCenter, RectValue targetSpace)
        {
            PointValue desiredCenterPoint = targetSpace.GetCenterPoint();
            PointValue existingCenterPoint = spaceToCenter.GetCenterPoint();

            return new RectValue(
                spaceToCenter.X + desiredCenterPoint.X - existingCenterPoint.X,
                spaceToCenter.Y + desiredCenterPoint.Y - existingCenterPoint.Y,
                spaceToCenter.Z + desiredCenterPoint.Z - existingCenterPoint.Z,
                spaceToCenter.Width,
                spaceToCenter.Height,
                spaceToCenter.Depth);
        }

        /// <summary>
        /// Gets the <see cref="PlanogramBlockingLocation"/>s in the <paramref name="blockingGroup"/> provided
        /// ordered by the sequence that their <see cref="PlanogramMerchandisingGroup"/>s appear in 
        /// <paramref name="sequencedSubComponentsById"/>.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <param name="intersectingMerchGroupsByLocation"></param>
        /// <param name="sequencedSubComponentsById"></param>
        /// <returns></returns>
        private static IEnumerable<PlanogramSequencedItem<PlanogramBlockingLocation>> EnumerateBlockingLocationsInSequence(
            PlanogramBlockingGroup blockingGroup,
            ILookup<PlanogramBlockingLocation, PlanogramMerchandisingGroup> intersectingMerchGroupsByLocation)
        {
            IEnumerable<PlanogramBlockingLocation> blockingLocations = blockingGroup
                .GetBlockingLocations()
                .ToList()
                .Where(l => intersectingMerchGroupsByLocation.Contains(l));

            return blockingGroup
                .EnumerateSequencableItemsInSequence<PlanogramBlockingLocation>(blockingLocations);
        }

        /// <summary>
        /// Arranges the <paramref name="compactPlacementGroupings"/> by their <see cref="PlanogramMerchandisingGroup"/>s.
        /// </summary>
        /// <param name="compactPlacementGroupings"></param>
        /// <returns></returns>
        private static Dictionary<PlanogramMerchandisingGroup, Dictionary<BlockingLocationPlaceHolder,CompactPlacementGrouping>> GetCompactPlacementGroupingsByMerchandisingGroupBlockingLocation(
            IEnumerable<CompactPlacementGrouping> compactPlacementGroupings)
        {
            Dictionary<PlanogramMerchandisingGroup, Dictionary<BlockingLocationPlaceHolder,CompactPlacementGrouping>> compactPlacementGroupingsByMerchGroupBlockingLocation =
                new Dictionary<PlanogramMerchandisingGroup, Dictionary<BlockingLocationPlaceHolder, CompactPlacementGrouping>>();

            // First, group the compact placement groupings by merchandising group.
            Dictionary<PlanogramMerchandisingGroup, List<CompactPlacementGrouping>> compactPlacementGroupingsByMerchGroup
                 = new Dictionary<PlanogramMerchandisingGroup, List<CompactPlacementGrouping>>();
            foreach (CompactPlacementGrouping compactPlacementGrouping in compactPlacementGroupings)
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in compactPlacementGrouping.MerchandisingGroups)
                {
                    List<CompactPlacementGrouping> merchGroupCompactPlacementGroupings;
                    if(!compactPlacementGroupingsByMerchGroup.TryGetValue(merchandisingGroup, out merchGroupCompactPlacementGroupings))
                    {
                        merchGroupCompactPlacementGroupings = new List<CompactPlacementGrouping>();
                        compactPlacementGroupingsByMerchGroup.Add(merchandisingGroup, merchGroupCompactPlacementGroupings);
                    }

                    merchGroupCompactPlacementGroupings.Add(compactPlacementGrouping);
                }
            }

            // Then, key each item in each group by its blocking locations
            foreach(KeyValuePair<PlanogramMerchandisingGroup,List<CompactPlacementGrouping>> compactPlacementGroupingGroup in
                compactPlacementGroupingsByMerchGroup)
            {
                Dictionary<BlockingLocationPlaceHolder, CompactPlacementGrouping> compactPlacementGroupingsByBlockingLocation =
                    new Dictionary<BlockingLocationPlaceHolder, CompactPlacementGrouping>();

                foreach(CompactPlacementGrouping compactPlacementGrouping in compactPlacementGroupingGroup.Value)
                {
                    if(compactPlacementGrouping.BlockingLocations.Any())
                    {
                        foreach(PlanogramBlockingLocation blockingLocation in compactPlacementGrouping.BlockingLocations)
                        {
                            BlockingLocationPlaceHolder blockingLocationPlaceholder = new BlockingLocationPlaceHolder(blockingLocation);
                            if(compactPlacementGroupingsByBlockingLocation.ContainsKey(blockingLocationPlaceholder))
                            {
                                Debug.Fail("The merchandising group/blocking location combination should be unique for compact placement groupings");
                                continue;
                            }

                            compactPlacementGroupingsByBlockingLocation.Add(blockingLocationPlaceholder, compactPlacementGrouping);
                        }
                    }
                    else
                    {
                        BlockingLocationPlaceHolder blockingLocationPlaceholder = new BlockingLocationPlaceHolder(null);
                        if (compactPlacementGroupingsByBlockingLocation.ContainsKey(blockingLocationPlaceholder))
                        {
                            Debug.Fail("The merchandising group/blocking location combination should be unique for compact placement groupings");
                            continue;
                        }

                        compactPlacementGroupingsByBlockingLocation.Add(blockingLocationPlaceholder, compactPlacementGrouping);
                    }
                }

                compactPlacementGroupingsByMerchGroupBlockingLocation.Add(
                    compactPlacementGroupingGroup.Key, compactPlacementGroupingsByBlockingLocation);
            }

            return compactPlacementGroupingsByMerchGroupBlockingLocation;
        }

        private IEnumerable<CompactPlacementGrouping> PrepareCompactPlacementGroupings(IEnumerable<ProtoBlockPlacement> protoBlockPlacements)
        {
            // Add the block placements to their respective compact placement groupings.
            foreach (ProtoBlockPlacement protoBlockPlacement in protoBlockPlacements)
            {
                protoBlockPlacement.CompactPlacementGrouping.BlockPlacements.Add(protoBlockPlacement.BlockPlacement);
            }

            // Return out the compact placement groupings of the required proto placements.
            return protoBlockPlacements
                .Select(p => p.CompactPlacementGrouping)
                .Distinct()
                .ToList();
        }

        private CompactPlacementGrouping GetBestMatchingCompactPlacementGroupingFor(
            PlanogramMerchandisingBlockPlacement blockPlacement, 
            Dictionary<BlockingLocationPlaceHolder, CompactPlacementGrouping> compactPlacementGroupingsByBlockingLocation,
            PlanogramMerchandisingBlocksParams.Block blockParams)
        {
            DesignViewPosition merchGroupDvp;
            if (!blockParams.MerchandisingGroupsAndDvps.TryGetValue(blockPlacement.MerchandisingGroup, out merchGroupDvp)) return null;
            
            DesignViewPosition blockPlacementDvp = new DesignViewPosition(blockPlacement, merchGroupDvp);


            PlanogramBlockingLocation maxOverlappingBlockingLocation = null;

            // If is a self and the MerchandisingType is Stack then check a FullOverlapsBy 
            if (blockPlacement.MerchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack && !blockPlacement.MerchandisingGroup.SubComponentPlacements.Any(c => c.SubComponent.HasFaceThickness()))
            {
                maxOverlappingBlockingLocation = compactPlacementGroupingsByBlockingLocation
                  .Keys
                  .Where(l => l.BlockingLocation != null)
                  .Select(l => l.BlockingLocation)
                  .OrderByDescending(l => blockPlacementDvp.FullOverlapsBy(l, blockParams.BlockingWidth, blockParams.BlockingHeight))
                  .FirstOrDefault();
            }
            // otherwise do it as before and check for PartialOverlapsBy 
            else
            {
                maxOverlappingBlockingLocation = compactPlacementGroupingsByBlockingLocation
                   .Keys
                   .Where(l => l.BlockingLocation != null)
                   .Select(l => l.BlockingLocation)
                   .OrderByDescending(l => blockPlacementDvp.PartialOverlapsBy(l, blockParams.BlockingWidth, blockParams.BlockingHeight))
                   .FirstOrDefault();
            }

            if(maxOverlappingBlockingLocation == null)
            {
                return compactPlacementGroupingsByBlockingLocation[new BlockingLocationPlaceHolder(null)];
            }
            else
            {
                return compactPlacementGroupingsByBlockingLocation.First().Value;
            }
        }

        #endregion

        #region Proto Placement Creation
        /// <summary>
        /// Creates Block PLacements for the clusters of positions that exist on <paramref name="merchGroup"/>
        /// for <see cref="BlockingGroup"/>.
        /// </summary>
        /// <param name="positionPlacements"></param>
        /// <param name="merchGroup"></param>
        /// <param name="spaceConstraintType"></param>
        /// <param name="sequencedSubComponentsById"></param>
        /// <param name="planogramBlockingWidth"></param>
        /// <param name="planogramBlockingHeight"></param>
        /// <param name="compactPlacementGrouping"></param>
        /// <param name="sequence"></param>
        /// <param name="isBlockOfInterest"></param>
        /// <param name="gtinsInSequence"></param>
        /// <param name="facingAxis"></param>
        /// <returns></returns>
        private IEnumerable<ProtoBlockPlacement> CreateProtoPlacementsFromPositionClusters(
            PlanogramMerchandisingBlocksParams.Block blockParams,
            PlanogramPositionPlacementList positionPlacements,
            PlanogramMerchandisingGroup merchGroup,
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType,
            Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById,
            Dictionary<BlockingLocationPlaceHolder, CompactPlacementGrouping> compactPlacementGroupingsByBlockingLocation,
            Int32 sequence,
            Boolean isBlockOfInterest,
            List<String> gtinsInSequence)
        {
            List<ProtoBlockPlacement> protoBlockPlacements = new List<ProtoBlockPlacement>();

            AxisType facingAxis = merchGroup.SubComponentPlacements.First().GetFacingAxis();
            IEnumerable<PlanogramPositionPlacement> positionPlacementsInSequence =
                positionPlacements.OrderBy(p => gtinsInSequence.IndexOf(p.Product.Gtin)).ToList();

            // Get the "clusters" of positions - those positions that are consecutively in merchandising sequence.
            IEnumerable<PlanogramPositionPlacementList> positionClusters =
                GetPositionClusters(positionPlacementsInSequence, merchGroup);

            // Create a block placement for each of the position clusters.
            Int32 positionClustersCount = positionClusters.Count();
            foreach (PlanogramPositionPlacementList positionCluster in positionClusters)
            {
                if (!positionCluster.Any()) continue;

                // Need to calculate the space that the block placement for this position cluster
                // should occupy, in each axis. We do this by constraining the available merchandising space
                // by a rect value which accounts for the various options available.
                RectValue merchSpaceConstraint;

                // Start with the volume occupied by the position cluster, which is the reserved spaces of 
                // all of the positions in the cluster aggregated into one volume.
                RectValue positionClusterReservedSpace = ClipToMerchGroupBounds(positionCluster.GetReservedSpace(), merchGroup);

                if (!isBlockOfInterest)
                {
                    // If this block is not of interest, the constraint is just the placed space.
                    merchSpaceConstraint = positionClusterReservedSpace;
                }
                else
                {
                    // If this is the block of interest, then we might add to this space, depending on whether
                    // or not we can exceed block space.
                    if (spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace ||
                        spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation)
                    {
                        // If we're exceeding block space, then add on any white space on the merchandising group
                        // to each axis of the placed volume.
                        RectValue augmentedPlacedSpace = IncreaseSpaceByWhiteSpace(
                            positionClusterReservedSpace, merchGroup, positionCluster.First(), positionClustersCount, facingAxis);
                        RectValue centeredAugmentedSpace = CenterSpaceOn(augmentedPlacedSpace, positionClusterReservedSpace);
                        merchSpaceConstraint = ClipToMerchGroupBounds(centeredAugmentedSpace, merchGroup);
                    }
                    else
                    {
                        // If we're not exceeding block space, then we need to compare the placed space to the blocked 
                        // space for this group.
                        if (!compactPlacementGroupingsByBlockingLocation.Any(g => g.Key.BlockingLocation != null))
                        {
                            // If there is no matching blocking location for this merch group, then we just use the placed
                            // space.
                            merchSpaceConstraint = positionClusterReservedSpace;
                        }
                        else
                        {
                            // If there is a matching block, then we compare the blocking space to the placed volume for each
                            // axis and take the largest value.

                            // First, we need the merchandising space within the blocking locations. This effectively 
                            // represents the block space available to this block placement.
                            IEnumerable<PlanogramBlockingLocation> blockingLocations =
                                compactPlacementGroupingsByBlockingLocation.Keys.Select(p => p.BlockingLocation).Where(l => l != null);
                            PlanogramMerchandisingSpace intersectingBlockMerchSpace = GetMerchandisingSpaceInBlockingLocations(
                                blockParams, merchGroup, blockingLocations);
                            if (intersectingBlockMerchSpace == null) continue;
                            RectValue blockMerchSpace = intersectingBlockMerchSpace.SpaceReducedByObstructions;
                            RectValue blockSpace = new RectValue(
                                blockMerchSpace.X,
                                blockMerchSpace.Y,
                                blockMerchSpace.Z,
                                blockMerchSpace.Width / (facingAxis == AxisType.X ? positionClustersCount : 1),
                                blockMerchSpace.Height / (facingAxis == AxisType.Y ? positionClustersCount : 1),
                                blockMerchSpace.Depth / (facingAxis == AxisType.Z ? positionClustersCount : 1));

                            // Next, we calculate the space constraint as the max of either the blocked space or
                            // placed space, in all axes.
                            Boolean useBlockSpaceX = blockSpace.Width.GreaterThan(positionClusterReservedSpace.Width);
                            Boolean useBlockSpaceY = blockSpace.Height.GreaterThan(positionClusterReservedSpace.Height);
                            Boolean useBlockSpaceZ = blockSpace.Depth.GreaterThan(positionClusterReservedSpace.Depth);

                            RectValue positionSpaceAugmentedByBlockSpace = new RectValue(
                                positionClusterReservedSpace.X,
                                positionClusterReservedSpace.Y,
                                positionClusterReservedSpace.Z,
                                useBlockSpaceX ? blockSpace.Width : positionClusterReservedSpace.Width,
                                useBlockSpaceY ? blockSpace.Height : positionClusterReservedSpace.Height,
                                useBlockSpaceZ ? blockSpace.Depth : positionClusterReservedSpace.Depth);

                            RectValue augmentedSpaceCenteredOnPositions = CenterSpaceOn(
                                positionSpaceAugmentedByBlockSpace, positionClusterReservedSpace);

                            merchSpaceConstraint = ClipToMerchGroupBounds(augmentedSpaceCenteredOnPositions, merchGroup);
                        }
                    }
                }

                // Finally, we reduce the merch group's full merchandising space by the constraint we have calculated.
                PlanogramMerchandisingSpace blockPlacementSpace = merchGroup
                    .GetMerchandisingSpace(planogramRelative: false)
                    .ReduceToFit(merchSpaceConstraint);
                if (blockPlacementSpace == null) continue;

                PlanogramMerchandisingSpace originalBlockPlacementSpace = merchGroup
                    .GetMerchandisingSpace(planogramRelative: false)
                    .ReduceToFit(positionClusterReservedSpace);
                if (originalBlockPlacementSpace == null) continue;


                // Lastly, we need the sequenced subcomponent for this merchandising group, so that we know which directions
                // we should be merchandising in within the block placement.
                PlanogramSequencedItem<PlanogramSubComponentPlacement> sequencedSubComponent;
                Object subComponentId = merchGroup.SubComponentPlacements.First().SubComponent.Id;
                if (!sequencedSubComponentsById.TryGetValue(subComponentId, out sequencedSubComponent)) continue;

                PlanogramMerchandisingBlockPlacement blockPlacement = new PlanogramMerchandisingBlockPlacement(
                    sequence,
                    BlockingGroup.BlockPlacementPrimaryType.Invert(sequencedSubComponent.IsPrimaryReversed),
                    BlockingGroup.BlockPlacementSecondaryType.Invert(sequencedSubComponent.IsSecondaryReversed),
                    BlockingGroup.BlockPlacementTertiaryType.Invert(sequencedSubComponent.IsTertiaryReversed),
                    merchGroup,
                    blockPlacementSpace,
                    originalBlockPlacementSpace,
                    this,
                    PlanogramMerchandisingBlockPlacementType.PositionSpace,
                    positionPlacementsInSequence,
                    _assortmentRuleEnforcer);

                CompactPlacementGrouping compactPlacementGrouping = GetBestMatchingCompactPlacementGroupingFor(
                    blockPlacement, compactPlacementGroupingsByBlockingLocation, blockParams);
                if (compactPlacementGrouping == null) continue;

                protoBlockPlacements.Add(new ProtoBlockPlacement(blockPlacement, compactPlacementGrouping));

                sequence++;
            }

            return protoBlockPlacements;
        }

        /// <summary>
        /// Creates a block placement for the given <paramref name="merchandisingGroup"/>, returning true if the block placement
        /// was added or false if not.
        /// </summary>
        /// <param name="merchandisingGroup"></param>
        /// <param name="merchGroupDvp"></param>
        /// <param name="sequence"></param>
        /// <param name="blockingLocation"></param>
        /// <param name="planogramBlockingWidth"></param>
        /// <param name="planogramBlockingHeight"></param>
        /// <param name="isBlockingLocationAtTop"></param>
        /// <param name="compactPlacementGroupingsByMerchGroup"></param>
        /// <param name="sequencedSubComponentsById"></param>
        /// <returns></returns>
        private ProtoBlockPlacement CreateProtoPlacementForMerchandisingGroup(
            PlanogramMerchandisingBlocksParams.Block blockParams,
            PlanogramMerchandisingGroup merchandisingGroup,
            DesignViewPosition merchGroupDvp,
            Int32 sequence,
            PlanogramBlockingLocation blockingLocation,
            CompactPlacementGrouping compactPlacementGrouping,
            Dictionary<Object, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById)
        {

            // First, find the volume of space that represent this blocking location in design-view space. This
            // space has no special transformations for top-down components, etc. We deal with these complications
            // next when we get the merchandisable space. Note that we take the depth of the block space from the 
            // merchandising space of the merchandising group that we're on at the moment.
            // Note: that the design view space for the block will start at Z = 0, so we need to calculate an
            // appropriate depth. To do this, we start with the design view Z of the merch group and then
            // add on the Z offset of the merch group relative merch space and the z size of the merch 
            // group merch space.
            PlanogramMerchandisingSpace merchGroupRelativeMerchSpace = merchandisingGroup.GetMerchandisingSpace(planogramRelative: false);
            Single blockDesignViewDepth =
                merchGroupDvp.BoundZ +
                merchGroupRelativeMerchSpace.UnhinderedSpace.Z +
                merchGroupRelativeMerchSpace.UnhinderedSpace.Depth;
            RectValue designViewBlockingSpace = new DesignViewPosition(
                blockingLocation,
                blockParams.BlockingWidth,
                blockParams.BlockingHeight,
                blockDesignViewDepth)
                .ToRectValue();

            // Now we need the merchandisable space, relative to the merchandising group, that fits within the design
            // view blocking space. To get this, we need the merch space transformed into design view space, and then
            // compared with the blocking space, before being transformed back into merch group space.
            Boolean isBlockingLocationAtTop = (blockingLocation.Y + blockingLocation.Height).EqualTo(1);
            PlanogramMerchandisingSpace merchGroupMerchSpaceInBlock =
                GetMerchandisingSpaceInBlock(merchandisingGroup, merchGroupDvp, designViewBlockingSpace, isBlockingLocationAtTop);
            if (merchGroupMerchSpaceInBlock == null) return null;

            // Finally, we need the sequenced subcomponent for this merchandising group, so that we know which directions
            // we should be merchandising in within the block placement.
            PlanogramSequencedItem<PlanogramSubComponentPlacement> sequencedSubComponent;
            Object subComponentId = merchGroupMerchSpaceInBlock.SubComponentPlacement.SubComponent.Id;
            if (!sequencedSubComponentsById.TryGetValue(subComponentId, out sequencedSubComponent)) return null;

            PlanogramMerchandisingBlockPlacement blockPlacement = new PlanogramMerchandisingBlockPlacement(
                sequence,
                BlockingGroup.BlockPlacementPrimaryType.Invert(sequencedSubComponent.IsPrimaryReversed),
                BlockingGroup.BlockPlacementSecondaryType.Invert(sequencedSubComponent.IsSecondaryReversed),
                BlockingGroup.BlockPlacementTertiaryType.Invert(sequencedSubComponent.IsTertiaryReversed),
                merchandisingGroup,
                merchGroupMerchSpaceInBlock,
                merchGroupMerchSpaceInBlock,
                this,
                PlanogramMerchandisingBlockPlacementType.BlockSpace,
                null,
                _assortmentRuleEnforcer);

            return new ProtoBlockPlacement(blockPlacement, compactPlacementGrouping);
        }
        #endregion

        #region Layout Methods
        private PlanogramSequenceGroupSubGroup GetSubGroup(PlanogramProduct product)
        {
            PlanogramSequenceGroupProduct sequenceProduct = SequenceGroup.Products
                .FirstOrDefault(p => p.Gtin.Equals(product.Gtin));
            if (sequenceProduct == null) return null;
            return sequenceProduct.GetSubGroup();
        }
        private static Boolean ValidateSpace(Single smallestAvailableSpace)
        {
            //  If the smallest available space is 0 or below,
            //  the space won't reduce more, desist.
            if (smallestAvailableSpace.LessThan(0) || smallestAvailableSpace.GreaterOrEqualThan(1))
            {
                System.Diagnostics.Debug.WriteLine("No further reduction is possible");
                System.Diagnostics.Debug.WriteLine("--");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Lays out this merchandising block placement using the data provided in the <paramref name="context"/>.
        /// </summary>
        /// <typeparam name="TPostLayoutContext"></typeparam>
        /// <param name="context"></param>
        public void Layout<TPostLayoutContext>(IPlanogramMerchandisingLayoutContext<TPostLayoutContext> context) where TPostLayoutContext : class
        {
            DebugLogLayoutPass(BlockingGroup.Name, BlockPlacements.ToList());

            Boolean isLastPass = false;
            Int32 postLayoutMethodIndex = 0;

            // Reset all of the facing space reductions to zero for this new layout pass.
            foreach (PlanogramMerchandisingBlockPlacement blockPlacement in BlockPlacements)
            {
                blockPlacement.FacingSpaceReductionPercentage = 0f;
            }

            // Reset all of the compact placement groupings to not be fully compacted.
            foreach (CompactPlacementGrouping compactPlacementGrouping in _compactPlacementGroupings)
            {
                compactPlacementGrouping.IsFullyCompacted = false;
            }

            while (true)
            {
                // Re-stack the products to be placed.
                context.GenerateProductStackMethod.Invoke(context.ProductsToPlace);
                context.SkippedSubGroupProducts.Clear();

                // Remove the positions from all the block placements.
                ClearAllPositions<TPostLayoutContext>(context);

                // Layout each of the block placements.
                foreach (PlanogramMerchandisingBlockPlacement blockPlacement in BlockPlacements)
                {
                    context.ReversePrimary = false;
                    context.ReverseSecondary = false;
                    context.ReverseTertiary = false;

                    WidthHeightDepthValue blockPlacementSize = blockPlacement.GetSizeReducedByFacingSpacePercentage();
                    System.Diagnostics.Debug.WriteLine("Placement size (w,h,d): " + blockPlacementSize.Width + ", " + blockPlacementSize.Height + ", " + blockPlacementSize.Depth);

                    blockPlacement.Layout(context);

                    DebugLogPositionPlacements(blockPlacement, context.RankByGtin);
                }

                // If we have finished all post-layout actions, exit the while loop to finish the layout.
                if (isLastPass) break;

                // Execute any post-layout methods that were provided.
                Boolean continueWithNextPass = false;
                while (postLayoutMethodIndex < context.PostLayoutMethods.Count())
                {
                    Func<TPostLayoutContext, Boolean> method = context.PostLayoutMethods.ElementAt(postLayoutMethodIndex);
                    if (method.Invoke(context.PostLayoutContext))
                    {
                        continueWithNextPass = true;
                        break;
                    }
                    postLayoutMethodIndex++;
                }
                if (continueWithNextPass) continue;

                // Compact block space if required.
                if (!context.ShouldCompactBlockSpace) break;
                foreach (CompactPlacementGrouping compactPlacementGrouping in _compactPlacementGroupings)
                {
                    if (!compactPlacementGrouping.BlockPlacements.Any()) compactPlacementGrouping.IsFullyCompacted = true;
                    if (compactPlacementGrouping.IsFullyCompacted) continue;

                    //If we have altered the compaction of a placement then we shouldn't try
                    //to compact the placements this time.
                    if (UncompactEmptyPlacementGroupings()) continue;

                    Boolean shouldReLayout = CompactBlockSpace(context, compactPlacementGrouping);
                    compactPlacementGrouping.IsFullyCompacted = !shouldReLayout;
                    break;
                }
                if (_compactPlacementGroupings.Any(g => !g.IsFullyCompacted)) continue;

                isLastPass = true;
            }
        }

        /// <summary>
        /// Sets FacingSpaceReductionPercentage to 0 for any placement that has no positions
        /// but has been compacted.
        /// </summary>
        /// <returns>true if any placement has been reset</returns>
        public Boolean UncompactEmptyPlacementGroupings()
        {
            Boolean output = false;
            foreach (CompactPlacementGrouping compactPlacementGrouping in _compactPlacementGroupings)
            {
                if (compactPlacementGrouping.CurrentCompactingBlockPlacements == null) continue;

                foreach (PlanogramMerchandisingBlockPlacement placement in compactPlacementGrouping.CurrentCompactingBlockPlacements)
                {
                    if (!placement.PositionPlacements.Any() && placement.FacingSpaceReductionPercentage.GreaterThan(0))
                    {
                        placement.PreviousFacingSpaceReductionPercentage = placement.FacingSpaceReductionPercentage;
                        placement.FacingSpaceReductionPercentage = 0;
                        output = true;
                    }
                }
            }

            return output;
        }

        /// <summary>
        /// Clears all <see cref="PlanogramPositionPlacement"/>s from the <see cref="BlockPlacements"/> on this
        /// <see cref="PlanogramMerchandisingBlock"/>.
        /// </summary>
        public void ClearAllPositions()
        {
            // If any of the block placements have positions, then we'll use the clear method.
            foreach (PlanogramMerchandisingBlockPlacement blockPlacement in BlockPlacements)
            {
                blockPlacement.ClearPositions();
            }
        }

        private void ClearAllPositions<TPostLayoutContext>(IPlanogramMerchandisingLayoutContext<TPostLayoutContext> context) where TPostLayoutContext : class
        {
            if (BlockPlacements.Any(bp => bp.PositionPlacements.Any()))
            {
                ClearAllPositions();
            }
            else if (BlockPlacements.Any(bp => bp.MerchandisingGroup.PositionPlacements.Any()))
            {
                // If none of the block placements had positions, but their merchandising
                // groups do, this could mean that they were instantiated on top of a 
                // plan that has already been merchandised. In this case, we'll try to remove 
                // positions that we are planning to place.
                IEnumerable<String> productsToPlaceGtins = context.ProductsToPlace.Select(p => p.Gtin).ToList();
                foreach (PlanogramMerchandisingGroup merchandisingGroup in BlockPlacements.Select(bp => bp.MerchandisingGroup).Distinct())
                {
                    List<PlanogramPositionPlacement> positionPlacementsToRemove = new List<PlanogramPositionPlacement>();
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        if (productsToPlaceGtins.Contains(positionPlacement.Product.Gtin))
                        {
                            positionPlacementsToRemove.Add(positionPlacement);
                        }
                    }

                    foreach (PlanogramPositionPlacement positionPlacement in positionPlacementsToRemove)
                    {
                        merchandisingGroup.RemovePositionPlacement(positionPlacement);
                    }
                }
            }
        }

        /// <summary>
        /// Compacts the block space by squeezing it's size to try to better distribute products.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>
        /// True if further layout passes are required. 
        /// False if the last compaction was unsuccessful and one last layout is required to restore the block's previous layout.
        /// </returns>
        private Boolean CompactBlockSpace(IPlanogramMerchandisingLayoutContext context, CompactPlacementGrouping compactPlacementGrouping)
        {
            // First, store the placements that we compacted last time. We need this for if we need
            // to reset facing space reduction percentages.
            compactPlacementGrouping.PreviousCompactingBlockPlacements = compactPlacementGrouping.CurrentCompactingBlockPlacements;

            // Then, check for dropped products - if products have been dropped, we don't need to do
            // anything other than restoring the previously compacted state.
            ICollection<String> currentlyPlacedGtins = this.BlockPlacements.SelectMany(b => b.PositionPlacements).Select(p => p.Product.Gtin).ToList();
            if (!SetPreviouslyPlacedGtins(context, compactPlacementGrouping, currentlyPlacedGtins)) return false;
            if (ProductsHaveBeenDropped(context, compactPlacementGrouping, currentlyPlacedGtins)) return false;

            // Also, check that all placements do not have one or less products in. If they
            // do, then we can't achieve anything by compacting further.
            if (AllPlacementsHaveOneOrLessProducts(compactPlacementGrouping)) return false;

            // Now, find the merchandising Block Placement with the largest facing size - 
            // we'll reduce the block placementwith the greatest size each time.
            compactPlacementGrouping.CurrentCompactingBlockPlacements = EnumerateBlockPlacementsTargetedForCompaction(compactPlacementGrouping).ToList();
            if (compactPlacementGrouping.CurrentCompactingBlockPlacements == null ||
                !compactPlacementGrouping.CurrentCompactingBlockPlacements.Any())
            {
                // If we couldn't idendify which placements to compact, then there's
                // no further compaction that we can do.
                return false;
            }

            //  Calculate a space reduction that will move one the product out of a component.
            Single smallestAvailableSpace = compactPlacementGrouping.CurrentCompactingBlockPlacements
                .Min(b => b.GetAvailableFacingSpaceAsPercentage());
            if (!ValidateSpace(smallestAvailableSpace)) return false;

            // Make the smallest space slightly larger - this is a small proportional change, just 
            // large enough to knock a product out of its position.
            Single nextFacingSpaceReductionPercentage = CalculateNextFacingSpaceReductionPercentage(compactPlacementGrouping, smallestAvailableSpace);

            if (!ValidateNextFacingSpaceReduction(compactPlacementGrouping, nextFacingSpaceReductionPercentage)) return false;

            SetNextFacingSpaceReduction(compactPlacementGrouping, nextFacingSpaceReductionPercentage);

            DebugLogBlockSpaceCompactResult(compactPlacementGrouping.CurrentCompactingBlockPlacements.First().PreviousFacingSpaceReductionPercentage, nextFacingSpaceReductionPercentage);

            //  Return success.
            return true;
        }

        /// <summary>
        /// Calculates the next physical facing space reduction (0.005) as a percentage. 
        /// </summary>
        /// <param name="compactPlacementGrouping">The block placements</param>
        /// <param name="smallestAvailableSpace">The smallest available space value</param>
        /// <returns></returns>
        private static Single CalculateNextFacingSpaceReductionPercentage(CompactPlacementGrouping compactPlacementGrouping, Single smallestAvailableSpace)
        {
            PlanogramMerchandisingBlockPlacement minAvailableBlockPlacement = null;
            Single nextFacingSpaceReductionPercentage = 0;

            if (compactPlacementGrouping.CurrentCompactingBlockPlacements.Count() > 1)
            {
                minAvailableBlockPlacement = compactPlacementGrouping.CurrentCompactingBlockPlacements.Where(p => p.GetAvailableFacingSpaceAsPercentage().EqualTo(smallestAvailableSpace, 5)).FirstOrDefault();
            }
            else
            {
                minAvailableBlockPlacement = compactPlacementGrouping.CurrentCompactingBlockPlacements.First();
            }

            if (minAvailableBlockPlacement != null)
            {
                AxisType axis = minAvailableBlockPlacement.MerchandisingGroup.SubComponentPlacements.First().GetFacingAxis();
                Single totalSpace = minAvailableBlockPlacement.Size.GetSize(axis);

                // From the smallest available space, reduce space enough to knock a product out of its position.
                Single nextSpaceChange = ((1 - smallestAvailableSpace) * totalSpace) - 0.005F;

                // Calculate the facing space reduction to force this position move
                nextFacingSpaceReductionPercentage = 1 - nextSpaceChange / totalSpace;
            }
            return nextFacingSpaceReductionPercentage;
        }

        private static void SetNextFacingSpaceReduction(CompactPlacementGrouping compactPlacementGrouping, Single nextFacingSpaceReductionPercentage)
        {
            foreach (PlanogramMerchandisingBlockPlacement blockPlacement in compactPlacementGrouping.CurrentCompactingBlockPlacements)
            {
                if (nextFacingSpaceReductionPercentage.GreaterThan(blockPlacement.FacingSpaceReductionPercentage, 5))
                {
                    blockPlacement.PreviousFacingSpaceReductionPercentage = blockPlacement.FacingSpaceReductionPercentage;
                    blockPlacement.FacingSpaceReductionPercentage = nextFacingSpaceReductionPercentage;
                }
                else
                {
                    // facing space reduction was not greater than previous but we still need to apply previous facing reduction as this formed a successful layout.
                    blockPlacement.PreviousFacingSpaceReductionPercentage = blockPlacement.FacingSpaceReductionPercentage;
                }
            }
        }

        private static Boolean ValidateNextFacingSpaceReduction(CompactPlacementGrouping compactPlacementGrouping, Single nextFacingSpaceReductionPercentage)
        {
            //  If the smallestAvailableSpace is the same as previously,
            //  the task is not being able to reduce it for some reason, desist.
            Boolean validSpaceReduction = false;

            foreach (PlanogramMerchandisingBlockPlacement blockPlacement in compactPlacementGrouping.CurrentCompactingBlockPlacements)
            {
                if (nextFacingSpaceReductionPercentage.GreaterThan(blockPlacement.PreviousFacingSpaceReductionPercentage, 5))
                {
                    validSpaceReduction = true;
                    break;
                }
            }

            if (!validSpaceReduction)
            {
                System.Diagnostics.Debug.WriteLine(String.Format(
                        "Next reduction ({0}) is the same as or worse than the last one",
                        compactPlacementGrouping.CurrentCompactingBlockPlacements.First().FacingSpaceReductionPercentage));
                System.Diagnostics.Debug.WriteLine("--");
            }

            return validSpaceReduction;
        }

        /// <summary>
        /// Enumerates the block placements that should be compacted out of the <paramref name="compactPlacementGrouping"/>.
        /// </summary>
        /// <param name="compactPlacementGrouping"></param>
        /// <returns></returns>
        /// <remarks>
        /// Block placements are selected for compaction using the following process:
        /// <list type="number">
        /// <item>Block placements are grouped by their starting size in the facing axis of their merchandising group;</item>
        /// <item>The groups are ordered by the maximum size of their block placements' potential compacted sizes;</item>
        /// <item>Then the groups are ordered by their maximum size;</item>
        /// <item>The largest group in this ordering is taken;</item>
        /// <item>This group is then grouped by the potential compacted size of its block placements;</item>
        /// <item>Groups with one block placement just have their block placement returned;</item>
        /// <item>Groups with more than one placement have the placement with the most position placements returned.</item>
        /// </list>
        /// </remarks>
        private IEnumerable<PlanogramMerchandisingBlockPlacement> EnumerateBlockPlacementsTargetedForCompaction(
            CompactPlacementGrouping compactPlacementGrouping)
        {
            // First, we're going to need to know how big each of the block placements would be
            // in the facing axis of their merchandising group, if they were compacted.
            Dictionary<PlanogramMerchandisingBlockPlacement, Single> compactedSizesByBlockPlacement =
                compactPlacementGrouping.BlockPlacements.ToDictionary(
                    p => p,
                    p => p.GetSizeReducedByFacingSpacePercentage(p.GetAvailableFacingSpaceAsPercentage()).
                        GetSize(p.MerchandisingGroup.SubComponentPlacements.First().GetFacingAxis()));

            // Before we select block placements for compaction, we want to remove any block placements
            // that don't have any positions.
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions =
                compactPlacementGrouping.BlockPlacements.Where(bp => bp.PositionPlacements.Any()).ToList();
            if (!blockPlacementsWithPositions.Any()) return blockPlacementsWithPositions;

            // Now, we group the block placements based on their size in their facing axis. This gives us groups
            // of block placements that are the same size which we should compact together in order to maintain
            // presentation.
            ILookup<Single, PlanogramMerchandisingBlockPlacement> blockPlacementsBySize = blockPlacementsWithPositions
                .ToLookup(
                    p => p.Bounds.GetSize(p.MerchandisingGroup.SubComponentPlacements.First().GetFacingAxis()),
                    p => p, new BlockPlacementComparer());

            // Of these groups, we're interested in the group of placements with the largest compacted size, because
            // we want to compact the placements that are the biggest. In case of duplicate compacted sizes,
            // we'll also order by uncompacted size, which is fine, because we grouped by this originally.
            IOrderedEnumerable<IGrouping<Single, PlanogramMerchandisingBlockPlacement>> blockPlacementsBySizeOrderedDesc =
                blockPlacementsBySize
                 .OrderByDescending(g => g.Max(p => compactedSizesByBlockPlacement[p]), new BlockPlacementComparer())
                 .ThenByDescending(g => g.Key, new BlockPlacementComparer());

            // Now that we've arranged our groups of similarly sized placements into descending size order, we
            // take the first one, because we want to compact the largest group of placements.
            IEnumerable<PlanogramMerchandisingBlockPlacement> targetedBlockPlacementsGroup =
                blockPlacementsBySizeOrderedDesc
                .Where(g => GetBlockPlacementsIneligibleForCompaction(g).Count() != g.Count())
                .FirstOrDefault();
            if (targetedBlockPlacementsGroup == null) return new List<PlanogramMerchandisingBlockPlacement>();

            // At this stage, we have identified the block placements that we want to compact out of this compact
            // grouping, based on the largest compacted and un-compacted size. These block placements represent
            // a continuous area of space with a hard edge that should be compacted together. If any of the 
            // block placements are not eligible for compaction, for sub-group or other reasons, then we
            // need to make sure that this is not the block placement that will be used to determine the compaction
            // reduction, as this will result in products being bumped out of the block placement all together.
            Dictionary<PlanogramMerchandisingBlockPlacement, Single> spaceReductionsByBlockPlacement =
                targetedBlockPlacementsGroup.ToDictionary(bp => bp, bp => bp.GetAvailableFacingSpaceAsPercentage());
            Single minSpaceReduction = spaceReductionsByBlockPlacement.Values.Min();
            IEnumerable<PlanogramMerchandisingBlockPlacement> ineligiblePlacements =
                GetBlockPlacementsIneligibleForCompaction(targetedBlockPlacementsGroup);
            if (ineligiblePlacements.Any(bp => spaceReductionsByBlockPlacement[bp].EqualTo(minSpaceReduction)))
            {
                // If one of the ineligible placements has the min space reduction, that means that it
                // will be the one to lose products. Because its ineligible, this means that it will be left
                // empty if it is compacted, so we can't compact this group at all.
                return new List<PlanogramMerchandisingBlockPlacement>();
            }

            // If we reach this stage, then all of our ineligible placements were safe from losing products
            // so we can go ahead and compact the placements that we have targeted. However, we need
            // to be careful that we don't knock more than one product out at once. This might happen if
            // more than one of the targeted block placements have the same compacted size. To deal with
            // this, we group the targeted placements by their compacted size and only return one placement
            // per group.
            ILookup<Single, PlanogramMerchandisingBlockPlacement> blockPlacementsGroupedByCompactedSize =
                targetedBlockPlacementsGroup
                    .ToLookup(p => compactedSizesByBlockPlacement[p], p => p, new BlockPlacementComparer());

            List<PlanogramMerchandisingBlockPlacement> blockPlacementsToReturn = new List<PlanogramMerchandisingBlockPlacement>();
            foreach (IGrouping<Single, PlanogramMerchandisingBlockPlacement> blockPlacementGroup in blockPlacementsGroupedByCompactedSize)
            {
                if (!blockPlacementGroup.Any()) continue;

                if (blockPlacementGroup.Count() == 1)
                {
                    blockPlacementsToReturn.Add(blockPlacementGroup.First());
                }
                else
                {
                    // In the case of more than one block placement sharing the same compacted size,
                    // we take the one with the most positions. However, if this is the only group
                    // of block placements in those that we have selected, we've got two block placements
                    // with the same compacted size. That means that, at the moment, they share a hard edge.
                    // If we only compact one of them, we'll be skewing that edge and (because there aren't
                    // any other groups for the products to move into in subsequence compactions) we won't be 
                    // able to reverse this change. As such, we pick the block placement with the most
                    // positions only if there are other block placement groups to compact.
                    if (compactPlacementGrouping.BlockPlacements.Count != blockPlacementGroup.Count())
                    {
                        blockPlacementsToReturn.Add(blockPlacementGroup
                            .OrderByDescending(p => p.PositionPlacements.Count()).First());
                    }
                }
            }

            return blockPlacementsToReturn;
        }

        /// <summary>
        /// Returns the block placements that are in-eligible for compaction based on the criteria outlined
        /// in remarks.
        /// </summary>
        /// <param name="blockPlacements"></param>
        /// <returns></returns>
        /// <remarks>
        /// In order to be eligible for compaction, a block placement must:
        /// <list type="bullet">
        /// <item>
        ///     Have more than one position (placements with one or zero positions cannot be compacted);
        /// </item>
        /// <item>
        ///     Have positions that are in more than one sub-group or not in a sub-group - 
        ///     a block placement containing only one sub-group should be treated the same as if 
        ///     it only had one product.
        /// </item>
        /// </list>
        /// </remarks>
        private IEnumerable<PlanogramMerchandisingBlockPlacement> GetBlockPlacementsIneligibleForCompaction(
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacements)
        {
            List<PlanogramMerchandisingBlockPlacement> ineligibleBlockPlacements = new List<PlanogramMerchandisingBlockPlacement>();
            foreach (PlanogramMerchandisingBlockPlacement blockPlacement in blockPlacements)
            {
                // Block placements with only one position cannot be compacted.
                if (blockPlacement.PositionPlacements.Count() == 1)
                {
                    ineligibleBlockPlacements.Add(blockPlacement);
                    continue;
                }

                // Find the sub groups represented in the positions (including nulls).
                IEnumerable<PlanogramSequenceGroupSubGroup> subGroups = blockPlacement.PositionPlacements
                    .Select(p => GetSubGroup(p.Product))
                    .Distinct();

                // If there is only one sub group represented and it is not null, the block
                // placement should be treated as if it only has one position and cannot
                // be compacted.
                if (!(subGroups.Count() == 1 && subGroups.First() != null)) continue;

                // If the block placement passes all the above tests, it is eligible for
                // compaction and can be added to the return list.
                ineligibleBlockPlacements.Add(blockPlacement);
            }
            return ineligibleBlockPlacements;
        }

        private static Boolean ProductsHaveBeenDropped(
                    IPlanogramMerchandisingLayoutContext context,
                    CompactPlacementGrouping compactPlacementGrouping,
                    ICollection<String> currentlyPlacedGtins)
        {
            Boolean productsHaveBeenDropped = !CollectionAtLeastContains(currentlyPlacedGtins, context.PreviouslyPlacedGtins);

            if (productsHaveBeenDropped)
            {
                ResetFacingSpacePercentages(compactPlacementGrouping);

                System.Diagnostics.Debug.WriteLine(String.Format(
                    "Last reduction ({0}) pushed products out, revert to previous state.",
                    compactPlacementGrouping.CurrentCompactingBlockPlacements.First().PreviousFacingSpaceReductionPercentage));
                System.Diagnostics.Debug.WriteLine("--");
                return true;
            }

            return false;
        }

        private static void ResetFacingSpacePercentages(CompactPlacementGrouping compactPlacementGrouping)
        {
            if (compactPlacementGrouping.PreviousCompactingBlockPlacements != null)
            {
                foreach (PlanogramMerchandisingBlockPlacement blockPlacement in compactPlacementGrouping.PreviousCompactingBlockPlacements)
                {
                    blockPlacement.FacingSpaceReductionPercentage = blockPlacement.PreviousFacingSpaceReductionPercentage;
                }
            }
        }

        private Boolean AllPlacementsHaveOneOrLessProducts(CompactPlacementGrouping compactPlacementGrouping)
        {
            //  If all placements have one or less products, nothing to reduce.
            Boolean hasPlacementsWithMoreThanOneProduct = compactPlacementGrouping.BlockPlacements.Any(b => b.PositionPlacements.Count() > 1);
            if (!hasPlacementsWithMoreThanOneProduct)
            {
                System.Diagnostics.Debug.WriteLine("No products that could be squeezed to another component, nothing to compact.");
                System.Diagnostics.Debug.WriteLine("--");
                return true;
            }

            return false;
        }

        private static Boolean SetPreviouslyPlacedGtins(
                    IPlanogramMerchandisingLayoutContext context,
                    CompactPlacementGrouping compactPlacementGrouping,
                    ICollection<String> currentlyPlacedGtins)
        {
            if (!context.PreviouslyPlacedGtins.Any())
            {
                foreach (String gtin in currentlyPlacedGtins)
                {
                    context.PreviouslyPlacedGtins.Add(gtin);
                }
            }

            if (!context.PreviouslyPlacedGtins.Any())
            {
                System.Diagnostics.Debug.WriteLine("No products placed as original state, nothing to compact.");
                System.Diagnostics.Debug.WriteLine("--");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks that <paramref name="collection"/> contains at least the same items that are in <paramref name="minimumItems"/>.
        /// </summary>
        /// <param name="collection">The collection to check.</param>
        /// <param name="minimumItems">The items that must at least appear in <paramref name="collection"/>.</param>
        /// <returns>
        /// False if there are less items in <paramref name="collection"/> than <paramref name="minimumItems"/>.
        /// True if there are the same items in <paramref name="collection"/> and <paramref name="minimumItems"/>.
        /// True if the items in <paramref name="minimumItems"/> all appear in <paramref name="collection"/>, along with other items.
        /// </returns>
        private static Boolean CollectionAtLeastContains(IEnumerable<String> collection, IEnumerable<String> minimumItems)
        {
            if (collection.Count() < minimumItems.Count())
            {
                return false;
            }

            if (collection.Count() == minimumItems.Count())
            {
                return collection.OrderBy(i => i).Zip(minimumItems.OrderBy(i => i), String.Equals).All(b => b);
            }

            if (collection.Count() > minimumItems.Count())
            {
                return minimumItems.All(i => collection.Contains(i));
            }

            return false;
        }

        public IEnumerable<PlanogramMerchandisingBlockPlacement> GetBlockPlacementsWithSameMerchandisingGroupAs(
                    PlanogramMerchandisingBlockPlacement blockPlacement)
        {
            return BlockPlacements.Where(b =>
                b != blockPlacement && b.MerchandisingGroup == blockPlacement.MerchandisingGroup);
        }

        public PlanogramSequenceGroupProduct GetSequenceGroupProduct(PlanogramProduct product)
        {
            PlanogramSequenceGroupProduct sequenceProduct;
            if (_sequenceProductsByGtin.TryGetValue(product.Gtin, out sequenceProduct))
            {
                return sequenceProduct;
            }
            return null;
        }

        #endregion

        #region Merchandising Edit Methods

        /// <summary>
        /// Begin an edit on all the <see cref="PlanogramMerchandisingGroup"/>s for this <see cref="PlanogramMerchandisingBlock"/>,
        /// recording their edit levels in case the edit needs to be cancelled with <see cref="CancelMerchandisingEdit"/>.
        /// </summary>
        public void BeginMerchandisingEdit()
        {
            _merchandisingGroups = BlockPlacements.Select(b => b.MerchandisingGroup).Distinct().ToList();
            _merchandisingGroupsAndEditLevels = _merchandisingGroups.ToDictionary(m => m, m => m.EditLevel);
            foreach (PlanogramMerchandisingGroup merchandisingGroup in _merchandisingGroups)
            {
                merchandisingGroup.BeginEdit();
            }
            _isEditing = true;
        }

        /// <summary>
        /// Cancels all edits to the <see cref="PlanogramMerchandisingGroup"/>s for this <see cref="PlanogramMerchandisingBlock"/>,
        /// taking them back to the edit level they were at when <see cref="BeginMerchandisingEdit"/> was called.
        /// </summary>
        public void CancelMerchandisingEdit()
        {
            if (!_isEditing)
            {
                Debug.Fail("CancelMerchandisingEdit can only be called after BeginMerchandisingEdit.");
                return;
            }

            foreach (KeyValuePair<PlanogramMerchandisingGroup, Int32> merchGroupEditLevel in _merchandisingGroupsAndEditLevels)
            {
                while (merchGroupEditLevel.Key.EditLevel > merchGroupEditLevel.Value)
                {
                    merchGroupEditLevel.Key.CancelEdit();
                }
            }

            _isEditing = false;
        }

        /// <summary>
        /// Applies changes to the <see cref="PlanogramMerchandisingGroup"/>s for this <see cref="PlanogramMerchandisingBlock"/>.
        /// </summary>
        public void ApplyMerchandisingEdit()
        {
            if (!_isEditing)
            {
                Debug.Fail("ApplyMerchandisingEdit can only be called after BeginMerchandisingEdit.");
                return;
            }

            // Apply all edits to the merchandising group, because the existing ApplyEdit method
            // does not support applying cherry picked edits.
            foreach (PlanogramMerchandisingGroup merchandisingGroup in _merchandisingGroups)
            {
                if (merchandisingGroup.EditLevel > 0) merchandisingGroup.ApplyEdit();
            }

            _isEditing = false;
        } 

        #endregion

        #endregion
    }
}