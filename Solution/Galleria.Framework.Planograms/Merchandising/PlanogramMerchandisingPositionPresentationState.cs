﻿using Galleria.Framework.Planograms.Model;
using System;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Holds information about the original presentation state of a <see cref="PlanogramPosition"/>.
    /// </summary>
    public sealed class PlanogramMerchandisingPositionPresentationState
    {
        #region Properties
        
        public PlanogramPositionMerchandisingStyle MerchandisingStyle { get; }
        public PlanogramPositionOrientationType OrientationType { get; }

        public PlanogramPositionMerchandisingStyle MerchandisingStyleX { get; }
        public PlanogramPositionOrientationType OrientationTypeX { get; }

        public PlanogramPositionMerchandisingStyle MerchandisingStyleY { get; }
        public PlanogramPositionOrientationType OrientationTypeY { get; }

        public PlanogramPositionMerchandisingStyle MerchandisingStyleZ { get; }
        public PlanogramPositionOrientationType OrientationTypeZ { get; }

        public Int16 FacingsWide { get; }
        public Int16 FacingsHigh { get; }
        public Int16 FacingsDeep { get; }

        public Int16 FacingsXWide { get; }
        public Int16 FacingsXHigh { get; }
        public Int16 FacingsXDeep { get; }

        public Int16 FacingsYWide { get; }
        public Int16 FacingsYHigh { get; }
        public Int16 FacingsYDeep { get; }

        public Int16 FacingsZWide { get; }
        public Int16 FacingsZHigh { get; }
        public Int16 FacingsZDeep { get; }

        #endregion

        #region Constructor

        public PlanogramMerchandisingPositionPresentationState(PlanogramPosition position)
        {
            MerchandisingStyle = position.MerchandisingStyle;
            OrientationType = position.OrientationType;

            MerchandisingStyleX = position.MerchandisingStyleX;
            OrientationTypeX = position.OrientationTypeX;

            MerchandisingStyleY = position.MerchandisingStyleY;
            OrientationTypeY = position.OrientationTypeY;

            MerchandisingStyleZ = position.MerchandisingStyleZ;
            OrientationTypeZ = position.OrientationTypeZ;

            FacingsWide = position.FacingsWide;
            FacingsHigh = position.FacingsHigh;
            FacingsDeep = position.FacingsDeep;
            FacingsXWide = position.FacingsXWide;
            FacingsXHigh = position.FacingsXHigh;
            FacingsXDeep = position.FacingsXDeep;
            FacingsYWide = position.FacingsYWide;
            FacingsYHigh = position.FacingsYHigh;
            FacingsYDeep = position.FacingsYDeep;
            FacingsZWide = position.FacingsZWide;
            FacingsZHigh = position.FacingsZHigh;
            FacingsZDeep = position.FacingsZDeep;
        }

        #endregion
    }
}