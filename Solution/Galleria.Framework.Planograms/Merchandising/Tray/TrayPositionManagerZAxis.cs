﻿using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using System;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising.Tray
{
    /// <summary>
    /// This manages increase and decreasing units for a tray in the Z axis
    /// </summary>
    public class TrayPositionManagerZAxis : ITrayPositionManager
    {

        private readonly IBreakTrayProcess _breakTrayProcess;

        public TrayPositionManagerZAxis(IBreakTrayProcess breakTrayProcess)
        {
            _breakTrayProcess = breakTrayProcess;
        }

        /// <summary>
        /// Tries to increase the number of units for a tray position by at least 1
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="subComponentPlacement"></param>
        /// <param name="merchandisingGroup"></param>
        /// <returns></returns>
        public Boolean IncreaseUnits(PlanogramProduct product, PlanogramPosition position,
            PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            if (!product.IsMerchandisingStyleAnyOf(PlanogramProductMerchandisingStyle.Tray)) return false;

            // We should get the rotated tray wide dimensions.
            WideHighDeepValue rotatedTraySize = product.GetRotatedTraySize(position.OrientationType);

            //Enforce the tray product placed into a tray correctly. Aka if enough high/wide/deep then should be in one tray. 
            position.TryMakeTray(product, rotatedTraySize);           

            //If current position is a tray and we can't break down or back then simply increase facing deep
            if (TrySimpleUnitIncreaseZ(product, position, rotatedTraySize)) return true;

            // If is smaller than a tray and we cannot become a tray just increase facing Deep.
            if (TryPreTrayIncreaseUnitsZ(product, position, rotatedTraySize)) return true;
            
            return _breakTrayProcess.TryBreakTray(product, position, rotatedTraySize);

        }

        /// <summary>
        /// Try to increase units if the tray product is currently not a tray position
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="rotatedTraySize"></param>
        /// <returns></returns>
        private Boolean TryPreTrayIncreaseUnitsZ(PlanogramProduct product, PlanogramPosition position,
            WideHighDeepValue rotatedTraySize)
        {
            if (!position.IsAppliedMerchandisingStyleAnyOf(product, PlanogramProductMerchandisingStyle.Tray))
            {
                //Must reach tray high and wide before increasing Deep could make a tray.
                if (!(rotatedTraySize.High <= position.FacingsHigh) ||
                    !(rotatedTraySize.Wide <= position.FacingsWide))
                {
                    position.FacingsDeep++;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Increase units if there are no break tray complications
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="rotatedTraySize"></param>
        /// <returns></returns>
        private Boolean TrySimpleUnitIncreaseZ(PlanogramProduct product, PlanogramPosition position,
            WideHighDeepValue rotatedTraySize)
        {
            if (!product.CanBreakTrayDown &&
                !product.CanBreakTrayBack)
            {
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                position.FacingsDeep++;

                //Update the y & x Caps also:
                _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Z, product, position);

                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries to decrease the number of units for a tray position by at least 1
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="subComponentPlacement"></param>
        /// <param name="merchandisingGroup"></param>
        /// <returns></returns>
        public Boolean DecreaseUnits(PlanogramProduct product, PlanogramPosition position,
            PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            //Tray Products not yet handled in Y axis.
            //Anything else has generic behaviour of decreasing main positions facings high by 1.
            
            if (!product.IsMerchandisingStyleAnyOf(PlanogramProductMerchandisingStyle.Tray)) return false;

            // At this point we know if the Product is a tray product but how is the actual position merchandised?
            Boolean isPositionCurrentlyAUnit = (position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit);

            // We should get the rotated tray wide dimensions.
            WideHighDeepValue rotatedTraySize = product.GetRotatedTraySize(position.OrientationType);

            // If we are a single unit and a tray then we can never decrease further.
            if (isPositionCurrentlyAUnit && position.FacingsDeep <= 1) return false;

            // If we are a single tray with single facing deep and cannot face down or up then we can never decrease further.
            if (!isPositionCurrentlyAUnit &&
                !product.CanBreakTrayDown &&
                position.FacingsDeep == 1 &&
                !product.CanBreakTrayBack) return false;

            // If position is currently a unit then we must be broken down further than a single tray 
            // and therefore can only reduce when we have more that one facing.
            if (isPositionCurrentlyAUnit && position.FacingsDeep > 1)
            {
                position.FacingsDeep--;
                position.TryMakeTray(product, rotatedTraySize);

                //Update the y & x Caps also:
                _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Z, product, position);
                return true;
            }


            // If we have at least one full tray then decrease cap by one unless there are no caps in which case
            // We remove an entire tray facing and add in the new caps.
            if (!isPositionCurrentlyAUnit &&
                position.FacingsZDeep != 0)
            {
                position.FacingsZDeep--;
                position.TryMakeTray(product, rotatedTraySize);

                // Update the y & x Caps also:
                _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Z, product, position);
                return true;
            }
            else
            {
                // Evaluate here whether we can break down & back. 
                if (position.FacingsDeep > 1)
                {
                    if (product.CanBreakTrayBack &&
                        rotatedTraySize.Deep > 1)
                    {
                        // Remove a facing and add missing Z facings.
                        position.FacingsDeep--;
                        position.FacingsZDeep = Convert.ToInt16(rotatedTraySize.Deep - 1);
                        position.FacingsZHigh = Convert.ToInt16(rotatedTraySize.High* position.FacingsHigh);
                        position.FacingsZWide = Convert.ToInt16(rotatedTraySize.Wide* position.FacingsWide);
                        position.TryMakeTray(product, rotatedTraySize);

                        // Update the y & x Caps also:
                        _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Z, product, position);
                        return true;
                    }
                    else
                    {
                        // We can only remove entire facings in which case do so and ensure there are no caps.
                        position.FacingsDeep--;
                        position.SetFacingCapUnits(AxisType.Z, 0);
                        position.TryMakeTray(product, rotatedTraySize);

                        // Update the y & x Caps also:
                        _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Z, product, position);
                        return true;
                    }
                }
                else if (position.FacingsDeep == 1 &&
                         product.CanBreakTrayDown &&
                         rotatedTraySize.Deep > 1)
                {
                    //If we have just one tray and can break down then we are no longer a tray but must become a unit 
                    // - we need to adjust facing values acordingly.
                    position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
                    position.FacingsDeep = Convert.ToInt16(rotatedTraySize.Deep - 1);
                    position.FacingsHigh = Convert.ToInt16(rotatedTraySize.High* position.FacingsHigh);
                    position.FacingsWide = Convert.ToInt16(rotatedTraySize.Wide* position.FacingsWide);
                    position.TryMakeTray(product, rotatedTraySize);

                    // Update the y & z Caps also:
                    // Because we are a unit at this point we shouldn't have any caps. 
                    // If we do go integrate them into the main position body. 
                    _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Z, product, position);

                    return true;
                }
            }

            //If any of the above fails for trays then we should return false
            return false;

        }      
    }
}
