﻿namespace Galleria.Framework.Planograms.Merchandising.Tray
{
    /// <summary>
    /// Static hepler for accessing tray position managers    
    /// </summary>
    /// <remarks> This should be refactored out during any major merchandising code refactor</remarks>
    public static class TrayPositionHelper
    {
        private static ITrayPositionManager _yAxisManager = new TrayPositionManagerYAxis(new BreakTrayYAxisProcess());
        private static ITrayPositionManager _zAxisManager = new TrayPositionManagerZAxis(new BreakTrayZAxisProcess());

        public static ITrayPositionManager YAxisManager => _yAxisManager;
        public static ITrayPositionManager ZAxisManager => _zAxisManager;
    }
}
