﻿using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising.Tray
{
    /// <summary>
    /// The process for breaking and unbreaking a tray into units when
    /// the total units of a tray position has changed. This is the process
    /// order for affecting change in the ZAxis
    /// </summary>
    public class BreakTrayZAxisProcess : BreakTrayProcessBase, IBreakTrayProcess
    {

        public bool TryBreakTray(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            //If we can't break down but can break back we increase caps until caps deep == number in a tray at which point
            //We switch the caps to a single tray facing AND we switch to a tray type.
            if (TryBreakTrayBackOnly(product, position, rotatedTraySize)) return true;

            //When we cannot break up but just down max out at one facing deep:
            if (TryBreakTrayDownOnly(product, position, rotatedTraySize)) return true;

            //If we can do both?
            return TryBreakTrayBoth(product, position, rotatedTraySize);
        }

        private bool TryBreakTrayBoth(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            if (product.CanBreakTrayDown &&
                product.CanBreakTrayBack)
            {
                //If merch style is a unit then we have less than one single tray:
                if (position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit)
                {
                    //We increase until we make a single tray
                    if ((position.FacingsDeep + 1) == rotatedTraySize.Deep)
                    {
                        position.FacingsDeep = 1;

                        //We want to update the facings, but only when it wouldn't cause 0 facings.
                        Int16 trayFacingsWide = Convert.ToInt16(position.FacingsWide/rotatedTraySize.Wide);
                        if (trayFacingsWide >= 1)
                        {
                            //Don't forget remaining x caps
                            position.FacingsXWide =
                                Convert.ToInt16(position.FacingsWide - (trayFacingsWide*rotatedTraySize.Wide));

                            //Update the actual facings.
                            position.FacingsWide = trayFacingsWide;
                        }
                        Int16 trayFacingsHigh = Convert.ToInt16(position.FacingsHigh/rotatedTraySize.High);
                        if (trayFacingsHigh >= 1)
                        {
                            //Don't forget remaining high caps
                            position.FacingsYHigh =
                                Convert.ToInt16(position.FacingsHigh - (trayFacingsHigh*rotatedTraySize.High));

                            //Update the actual facings.
                            position.FacingsHigh = trayFacingsHigh;
                        }

                        position.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                        position.OrientationTypeX = product.OrientationType.ToPositionOrientationType();
                        position.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                        position.OrientationTypeY = product.OrientationType.ToPositionOrientationType();
                        position.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
                        position.OrientationTypeZ = product.OrientationType.ToPositionOrientationType();

                        position.FacingsXHigh = Convert.ToInt16(position.FacingsHigh*rotatedTraySize.High);
                        position.FacingsXDeep = Convert.ToInt16(position.FacingsDeep*rotatedTraySize.Deep);
                        position.FacingsYWide = Convert.ToInt16(position.FacingsWide*rotatedTraySize.Wide);
                        position.FacingsYDeep = Convert.ToInt16(position.FacingsDeep*rotatedTraySize.Deep);

                        position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                        UpdateBreakTrayUnits(AxisType.Z, product, position);

                        return true;
                    }
                    // Just increase facings wide, should stay as a unit.
                    position.FacingsDeep++;
                    return true;
                }
                if ((position.FacingsZDeep + 1) == rotatedTraySize.Deep)
                {
                    position.FacingsDeep++;
                    position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                    SetFacingCapUnits(AxisType.Z, position, 0);

                    //Update the y & x Caps also:
                    UpdateBreakTrayUnits(AxisType.Z, product, position);

                    return true;
                }
                position.FacingsZDeep++;
                position.FacingsZHigh = Convert.ToInt16(rotatedTraySize.High*position.FacingsHigh);
                position.FacingsZWide = Convert.ToInt16(rotatedTraySize.Wide*position.FacingsWide);
                position.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
                position.OrientationTypeZ = product.OrientationType.ToPositionOrientationType();

                //Update the y & x Caps also:
                UpdateBreakTrayUnits(AxisType.Z, product, position);

                return true;
            }
            return false;
        }

        private bool TryBreakTrayDownOnly(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            if (product.CanBreakTrayDown &&
                !product.CanBreakTrayBack)
            {
                // If we have a total units deep less than the product.tray deep we should just increase normal unit facings deep 
                // (When we are less than a single tray).
                if (position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit &&
                    position.FacingsDeep < rotatedTraySize.Deep)
                {
                    // If should unit facings should become a single facing
                    if ((position.FacingsDeep + 1) == rotatedTraySize.Deep)
                    {
                        position.FacingsDeep = 1;
                        //We want to update the facings, but only when it wouldn't cause 0 facings.
                        if (Convert.ToInt16(position.FacingsWide/rotatedTraySize.Wide) >= 1)
                            position.FacingsWide = Convert.ToInt16(position.FacingsWide/rotatedTraySize.Wide);
                        if ((Convert.ToInt16(position.FacingsHigh/rotatedTraySize.High) >= 1))
                            position.FacingsHigh = Convert.ToInt16(position.FacingsHigh/rotatedTraySize.High);

                        position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                        return true;
                    }
                    // Just increase facings Deep, should stay as a unit.
                    position.FacingsDeep++;
                    return true;
                }
                // We reach here when we have at least one full main tray. 
                // As we cannot break tray up then we have to go and increase facings Deep only.
                position.FacingsDeep++;

                //Update the y & x Caps also:
                UpdateBreakTrayUnits(AxisType.Z, product, position);

                return true;
            }
            return false;
        }

        private bool TryBreakTrayBackOnly(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            if (product.CanBreakTrayBack &&
                !product.CanBreakTrayDown)
            {
                //If we are unable to break the tray down then enforce the main block as a tray:
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

                if ((position.FacingsZDeep + 1) < rotatedTraySize.Deep)
                {
                    position.FacingsZDeep++;
                    position.FacingsZWide = Convert.ToInt16(rotatedTraySize.Wide*position.FacingsWide);
                    position.FacingsZHigh = Convert.ToInt16(rotatedTraySize.High*position.FacingsHigh);
                    position.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
                    position.OrientationTypeZ = product.OrientationType.ToPositionOrientationType();

                    //Update the y & x Caps also:
                    UpdateBreakTrayUnits(AxisType.Z, product, position);

                    return true;
                }
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                SetFacingCapUnits(AxisType.Z, position, 0);

                if (position.FacingsDeep == 0)
                {
                    position.FacingsDeep = 1;

                    // Update the y & x Caps also:
                    UpdateBreakTrayUnits(AxisType.Z, product, position);

                    return true;
                }
                position.FacingsDeep++;

                // Update the y & x Caps also:
                UpdateBreakTrayUnits(AxisType.Z, product, position);

                return true;
            }
            return false;
        }
    }
}
