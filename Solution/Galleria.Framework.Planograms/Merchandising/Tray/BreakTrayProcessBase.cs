﻿using System;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising.Tray
{
    public class BreakTrayProcessBase
    {
        /// <summary>
        /// Sets the existing cappings to fill the remaining dimension of the positions body and then to include dimensions of other cappings on the other axis.
        /// </summary>
        /// <param name="axis">The capping facing axis the caps are to be updated. i.e. facingsx is axistype.x</param>
        /// <remarks>Useful to ensure capps are increased along with the facings. </remarks>
        public void UpdateBreakTrayUnits(AxisType axis, PlanogramProduct product, PlanogramPosition position)
        {
            //If we are a unit on a tray product we shoudln't have any caps!
            //If we do they require integrating into the main body.
            //Note: This behaviour >may< change if in the future we choose to implement tray capping features.
            if (position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit)
            {
                foreach (AxisType axisWithCaps in axis.GetNormals().Where(a => HasFacingCaps(a, position)))
                {
                    MergeCappingFacingsWithMain(axis, axisWithCaps, position);
                    SetFacingCapUnits(axisWithCaps, position, 0);
                }
            }
            else
            {
                SetBreakUnitsToMaxValue(axis, product, position);
               // CleanUpCaps(position, axis);
            }
        }

        private void SetBreakUnitsToMaxValue(AxisType axis, PlanogramProduct product, PlanogramPosition position)
        {
            SetBreakUnitsToMaxValue(AxisType.X, product, position, axis == AxisType.X ? 0 : 1);
            SetBreakUnitsToMaxValue(AxisType.Y, product, position, axis == AxisType.Y ? 0 : 1);
            SetBreakUnitsToMaxValue(AxisType.Z, product, position, axis == AxisType.Z ? 0 : 1);
           
        }

        /// <summary>
        ///     Sets the normal axes of a given <paramref name="axis"/> facing cap units to the specified <paramref name="value"/>.
        /// </summary>
        /// <remarks>Any facing that has no cap units will be left alone.</remarks>
        private void SetBreakUnitsToMaxValue(AxisType axis, PlanogramProduct product, PlanogramPosition position, Int32 minimumValue)
        {
            var traySize = product.GetUnorientatedTraySize();
            var productMainOrientedDimensions = ProductOrientationHelper.GetOrientatedSize(position.OrientationType, traySize.Width, traySize.Height,
               traySize.Depth);
            Single sizeLimit;

            var productXOrientedDimensions = ProductOrientationHelper.GetOrientatedSize(position.OrientationTypeX, product.Width, product.Height,
                product.Depth);
            var productYOrientedDimensions = ProductOrientationHelper.GetOrientatedSize(position.OrientationTypeY, product.Width, product.Height,
                product.Depth);
            var productZOrientedDimensions = ProductOrientationHelper.GetOrientatedSize(position.OrientationTypeZ, product.Width, product.Height,
                product.Depth);

            switch (axis)
            {
                case AxisType.X:
                    sizeLimit = GetFacings(axis, position) * productMainOrientedDimensions.Width;
                    if (position.FacingsYHigh > 0) position.FacingsYWide = (Int16)Math.Max(sizeLimit / productYOrientedDimensions.Width, minimumValue);
                    if (position.FacingsZDeep > 0) position.FacingsZWide = (Int16)Math.Max(sizeLimit / productZOrientedDimensions.Width, minimumValue);
                    break;
                case AxisType.Y:
                    sizeLimit = GetFacings(axis, position) * productMainOrientedDimensions.Height;
                    if (position.FacingsXWide > 0) position.FacingsXHigh = (Int16)Math.Max(sizeLimit / productXOrientedDimensions.Height, minimumValue);
                    if (position.FacingsZDeep > 0) position.FacingsZHigh = (Int16)Math.Max(sizeLimit / productZOrientedDimensions.Height, minimumValue);
                    break;
                case AxisType.Z:
                    sizeLimit = GetFacings(axis, position) * productMainOrientedDimensions.Depth;
                    if (position.FacingsYHigh > 0) position.FacingsYDeep = (Int16)Math.Max(sizeLimit / productYOrientedDimensions.Depth, minimumValue);
                    if (position.FacingsXWide > 0) position.FacingsXDeep = (Int16)Math.Max(sizeLimit / productXOrientedDimensions.Depth, minimumValue);
                    break;
                default:
                    Debug.Fail("Unknown axis when processing SetFacingCapUnitsMain(axis, value)");
                    break;
            }
        }

        private void CleanUpCaps(PlanogramPosition position, AxisType axis)
        {
            if (position.FacingsXHigh == 0 || position.FacingsXWide == 0 || position.FacingsXDeep == 0)
            {
                if (axis != AxisType.X)
                {
                    position.SetFacingCapUnits(AxisType.X, 0);
                }
                else
                {
                    position.FacingsXHigh = Math.Max(position.FacingsXHigh, (Int16)1);
                    position.FacingsXWide = Math.Max(position.FacingsXWide, (Int16)1);
                    position.FacingsXDeep = Math.Max(position.FacingsXDeep, (Int16)1);
                }
            }
            if (position.FacingsYHigh == 0 || position.FacingsYWide == 0 || position.FacingsYDeep == 0)
            {
                if (axis != AxisType.Y)
                {
                    position.SetFacingCapUnits(AxisType.Y, 0);
                }
                else
                {
                    position.FacingsYHigh = Math.Max(position.FacingsYHigh, (Int16)1);
                    position.FacingsYWide = Math.Max(position.FacingsYWide, (Int16)1);
                    position.FacingsYDeep = Math.Max(position.FacingsYDeep, (Int16)1);
                }
            }
            if (position.FacingsZHigh == 0 || position.FacingsZWide == 0 || position.FacingsZDeep == 0)
            {
                if (axis != AxisType.Z)
                {
                    position.SetFacingCapUnits(AxisType.Z, 0);
                }
                else
                {
                    position.FacingsZHigh = Math.Max(position.FacingsZHigh, (Int16)1);
                    position.FacingsZWide = Math.Max(position.FacingsZWide, (Int16)1);
                    position.FacingsZDeep = Math.Max(position.FacingsZDeep, (Int16)1);
                }
            }
        }

        protected void MergeCappingFacingsWithMain(AxisType mainAxis, AxisType cappingAxis, PlanogramPosition position)
        {

            Int16 value = (Int16)(GetFacings(mainAxis, position) + GetFacingCapUnits(cappingAxis, position));

            //  Facings should not be lower than 1.
            if (value < 1) value = 1;

            switch (cappingAxis)
            {
                case AxisType.X:
                    position.FacingsWide = Convert.ToInt16(value);
                    break;
                case AxisType.Y:
                    position.FacingsHigh = Convert.ToInt16(value);
                    break;
                case AxisType.Z:
                    position.FacingsDeep = Convert.ToInt16(value);
                    break;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.SetFacings");
                    break;
            }
        }

        /// <summary>
        ///     Get the <c>Cap Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to return the <c>Cap Facings</c> count for.</param>
        /// <returns>The number of cap facings set for this instance on the given <paramref name="axis"/>.</returns>
        protected Int32 GetFacingCapUnits(AxisType axis, PlanogramPosition position)
        {
            switch (axis)
            {
                case AxisType.X:
                    return position.FacingsXWide;
                case AxisType.Y:
                    return position.FacingsYHigh;
                case AxisType.Z:
                    return position.FacingsZDeep;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.GetFacingCapUnits");
                    return 0;
            }
        }

        /// <summary>
        ///     Get the <c>Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to return the <c>Facings</c> count for.</param>
        /// <returns>The number of facings set for this instance on the given <paramref name="axis"/>.</returns>
        protected Int32 GetFacings(AxisType axis, PlanogramPosition position)
        {
            switch (axis)
            {
                case AxisType.X:
                    return position.FacingsWide;
                case AxisType.Y:
                    return position.FacingsHigh;
                case AxisType.Z:
                    return position.FacingsDeep;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.GetFacings");
                    return 1;
            }
        }

        /// <summary>
        ///     Set the <c>Cap Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to set the <c>Cap Facings</c> count for.</param>
        /// <param name="value">The amount of <C>Cap Facings</C> to add on the given <paramref name="axis"/>.</param>
        /// <remarks>
        ///     If <paramref name="value"/> is ZERO, all facing cap units values for the axis will be set to ZERO.
        /// <para></para>
        ///     If <paramref name="value"/> is not ZERO, all other facing cap units for the axis will be set AT LEAST to 1 (they will not be adjusted if they are already not ZERO).</remarks>
        protected void SetFacingCapUnits(AxisType axis, PlanogramPosition position, Int32 value)
        {
            //  Facing cap units should not be lower than 0.
            if (value < 0) value = 0;

            switch (axis)
            {
                case AxisType.X:
                    position.FacingsXWide = Convert.ToInt16(value);
                    if (value == 0)
                    {
                        position.FacingsXHigh = 0;
                        position.FacingsXDeep = 0;
                    }
                    else
                    {
                        if (position.FacingsXHigh == 0) position.FacingsXHigh = 1;
                        if (position.FacingsXDeep == 0) position.FacingsXDeep = 1;
                    }
                    break;
                case AxisType.Y:
                    position.FacingsYHigh = Convert.ToInt16(value);
                    if (value == 0)
                    {
                        position.FacingsYWide = 0;
                        position.FacingsYDeep = 0;
                    }
                    else
                    {
                        if (position.FacingsYWide == 0) position.FacingsYWide = 1;
                        if (position.FacingsYDeep == 0) position.FacingsYDeep = 1;
                    }
                    break;
                case AxisType.Z:
                    position.FacingsZDeep = Convert.ToInt16(value);
                    if (value == 0)
                    {
                        position.FacingsZHigh = 0;
                        position.FacingsZWide = 0;
                    }
                    else
                    {
                        if (position.FacingsZHigh == 0) position.FacingsZHigh = 1;
                        if (position.FacingsZWide == 0) position.FacingsZWide = 1;
                    }
                    break;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.SetFacingCapUnits");
                    break;
            }
        }

        /// <summary>
        ///     Check whether this instance has valid caps on the given <paramref name="axis" />.
        /// </summary>
        /// <param name="axis">The axis on which to check for valid caps.</param>
        /// <returns><c>True</c> if there are valid caps on the axis; <c>False</c> otherwise.</returns>
        /// <remarks>
        ///     Valid caps implies that the axis has cap units specified in all <c>Wide</c>, <c>High</c> and <c>Deep</c>
        ///     dimensions.
        /// </remarks>
        protected Boolean HasFacingCaps(AxisType axis, PlanogramPosition position)
        {
            switch (axis)
            {
                case AxisType.X:
                    return position.FacingsXWide > 0 && position.FacingsXHigh > 0 && position.FacingsXDeep > 0;
                case AxisType.Y:
                    return position.FacingsYWide > 0 && position.FacingsYHigh > 0 && position.FacingsYDeep > 0;
                case AxisType.Z:
                    return position.FacingsZWide > 0 && position.FacingsZHigh > 0 && position.FacingsZDeep > 0;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.HasFacingCaps");
                    return false;
            }
        }
    }
}
