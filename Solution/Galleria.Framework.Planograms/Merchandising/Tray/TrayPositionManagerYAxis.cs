﻿using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using System;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising.Tray
{

    /// <summary>
    /// This manages increase and decreasing units for a tray in the Y axis
    /// </summary>
    public class TrayPositionManagerYAxis : ITrayPositionManager
    {
        private readonly IBreakTrayProcess _breakTrayProcess;

        public TrayPositionManagerYAxis(IBreakTrayProcess breakTrayProcess)
        {
            _breakTrayProcess = breakTrayProcess;
        }

        /// <summary>
        /// Tries to increase the number of units for a tray position by at least 1
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="subComponentPlacement"></param>
        /// <param name="merchandisingGroup"></param>
        /// <returns></returns>
        public Boolean IncreaseUnits(PlanogramProduct product, PlanogramPosition position,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {

            if (!product.IsMerchandisingStyleAnyOf(PlanogramProductMerchandisingStyle.Tray)) return false;

            // We should get the rotated tray wide dimensions.
            WideHighDeepValue rotatedTraySize = product.GetRotatedTraySize(position.OrientationType);

            //Enforce the tray product placed into a tray correctly. Aka if enough high/wide/deep then should be in one.
            position.TryMakeTray(product, rotatedTraySize);
          
            // If current position is a tray and we can't break down or top then simply increase facing high.
            if (TrySimpleUnitIncreaseY(product, position, rotatedTraySize)) return true;

            // If is smaller than a tray and we cannot become a tray just increase facing High.
            if (TryPreTrayIncreaseUnitsY(product, position, rotatedTraySize)) return true;

            return _breakTrayProcess.TryBreakTray(product, position, rotatedTraySize);
        }


        /// <summary>
        /// Try to increase units if the tray product is currently not a tray position
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="rotatedTraySize"></param>
        /// <returns></returns>
        private Boolean TryPreTrayIncreaseUnitsY(PlanogramProduct product, PlanogramPosition position,
            WideHighDeepValue rotatedTraySize)
        {
            if (!position.IsAppliedMerchandisingStyleAnyOf(product, PlanogramProductMerchandisingStyle.Tray))
            {
                // Must reach tray Wide and deep before increasing High could make a tray.
                if (!(rotatedTraySize.Wide <= position.FacingsWide) ||
                    !(rotatedTraySize.Deep <= position.FacingsDeep))
                {
                    position.FacingsHigh++;
                    
                    if (product.MaxStack > 0 && Math.Ceiling(position.FacingsHigh/(Double) rotatedTraySize.High) > product.MaxStack)
                    {
                        position.FacingsHigh--;
                        return true;
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Increase units if there are no break tray complications
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="rotatedTraySize"></param>
        /// <returns></returns>
        private Boolean TrySimpleUnitIncreaseY(PlanogramProduct product, PlanogramPosition position,
            WideHighDeepValue rotatedTraySize)
        {
            if (!product.CanBreakTrayTop &&
                !product.CanBreakTrayDown)
            {
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                position.FacingsHigh++;

                if (position.IsMaxStackExceeded(product))
                {
                    position.FacingsHigh--;
                    return true;
                }
                // Update the x & z Caps also:
                _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Y, product, position);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries to decrease the number of units for a tray position by at least 1
        /// </summary>
        /// <param name="product"></param>
        /// <param name="position"></param>
        /// <param name="subComponentPlacement"></param>
        /// <param name="merchandisingGroup"></param>
        /// <returns></returns>
        public Boolean DecreaseUnits(PlanogramProduct product, PlanogramPosition position,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            if (!product.IsMerchandisingStyleAnyOf(PlanogramProductMerchandisingStyle.Tray)) return false;

            // At this point we know if the Product is a tray product but how is the actual position merchandised?
            Boolean isPositionCurrentlyAUnit = (position.MerchandisingStyle ==
                                                PlanogramPositionMerchandisingStyle.Unit);

            // We should get the rotated tray wide dimensions.
            WideHighDeepValue rotatedTraySize = product.GetRotatedTraySize(position.OrientationType);

            // If we are a single unit then we can never decrease further.
            if (isPositionCurrentlyAUnit && position.FacingsHigh <= 1) return false;

            // If we are a single tray with single facing wide and cannot face down or top then we can never decrease further.
            if (!isPositionCurrentlyAUnit &&
                !product.CanBreakTrayDown &&
                position.FacingsHigh == 1 &&
                !product.CanBreakTrayTop) return false;

            // If position is currently a unit then we must be broken down further than a single tray 
            // and therefore can only reduce when we have more that one facing.
            if (isPositionCurrentlyAUnit && position.FacingsHigh > 1)
            {
                position.FacingsHigh--;
                position.TryMakeTray(product, rotatedTraySize);
                return true;
            }

            // If we have at least one full tray then decrease cap by one unless there are no caps in which case
            // We remove an entire tray facing and add in the new caps.
            if (!isPositionCurrentlyAUnit &&
                position.FacingsYHigh != 0)
            {
                position.FacingsYHigh--;
                position.TryMakeTray(product, rotatedTraySize);

                //sort out caps
                _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Y, product, position);
                return true;
            }

            // Evaluate here whether we can break down & top. 
            if (position.FacingsHigh > 1)
            {
                if (product.CanBreakTrayTop &&
                    rotatedTraySize.High > 1)
                {
                    // Remove a facing and add missing y facings.
                    position.FacingsHigh--;
                    position.FacingsYHigh = Convert.ToInt16(rotatedTraySize.High - 1);

                    position.FacingsYWide = Convert.ToInt16(rotatedTraySize.Wide*position.FacingsWide);
                    position.FacingsYDeep = Convert.ToInt16(rotatedTraySize.Deep*position.FacingsDeep);

                    position.TryMakeTray(product, rotatedTraySize);

                    // update caps
                    _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Y, product, position);
                    return true;
                }


                // We can only remove entire facings in which case do so and ensure there are no caps.
                position.FacingsHigh--;
                position.SetFacingCapUnits(AxisType.Y, 0);
                position.TryMakeTray(product, rotatedTraySize);

                // Update out caps 
                _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Y, product, position);
                return true;

            }

            if (position.FacingsHigh == 1 &&
                product.CanBreakTrayDown &&
                rotatedTraySize.High > 1)
            {
                // If we have just one tray and can break down then we are no longer a tray but must become a unit 
                // - we need to adjust facing values acordingly.
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
                position.FacingsHigh = Convert.ToInt16(rotatedTraySize.High - 1);
                position.FacingsWide = Convert.ToInt16(rotatedTraySize.Wide*position.FacingsWide);
                position.FacingsDeep = Convert.ToInt16(rotatedTraySize.Deep*position.FacingsDeep);

                position.TryMakeTray(product, rotatedTraySize);

                // Update the y & z Caps also
                // Because we are a unit at this point we shouldn't have any caps. 
                // If we do go integrate them into the main position body. 
                _breakTrayProcess.UpdateBreakTrayUnits(AxisType.Y, product, position);

                return true;
            }
            //If any of the above fails for trays then we should return false
            return false;
        }
    }
}
