﻿using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using System;
using Galleria.Framework.Planograms.Model;
namespace Galleria.Framework.Planograms.Merchandising.Tray
{
    /// <summary>
    /// The process for breaking and unbreaking a tray into units when
    /// the total units of a tray position has changed. This is the process
    /// order for affecting change in the YAxis
    /// </summary>
    public class BreakTrayYAxisProcess : BreakTrayProcessBase, IBreakTrayProcess
    {
        public Boolean TryBreakTray(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            if (TryBreakTrayTopOnly(product, position, rotatedTraySize)) return true;
            if (TryBreakTrayDownOnly(product, position, rotatedTraySize)) return true;
            return TryBreakTrayBoth(product, position, rotatedTraySize);
        }

        private Boolean TryBreakTrayBoth(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            if (product.CanBreakTrayDown &&
                product.CanBreakTrayTop)
            {
                //If merch style is a unit then we have less than one single tray:
                if (position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit)
                {
                    //We increase until we make a single tray
                    if ((position.FacingsHigh + 1) == rotatedTraySize.High)
                    {
                        position.FacingsHigh = 1;
                        //We want to update, but only when it won't cause 0 facings to occur
                        Int16 trayFacingsWide = Convert.ToInt16(position.FacingsWide / rotatedTraySize.Wide);
                        if (trayFacingsWide >= 1)
                        {
                            position.FacingsXWide =
                                Convert.ToInt16(position.FacingsWide - trayFacingsWide * rotatedTraySize.Wide);

                            position.FacingsWide = trayFacingsWide;
                        }
                        Int16 trayFacingsDeep = Convert.ToInt16(position.FacingsDeep / rotatedTraySize.Deep);
                        if (trayFacingsDeep >= 1)
                        {
                            position.FacingsZDeep =
                                Convert.ToInt16(position.FacingsDeep - trayFacingsDeep * rotatedTraySize.Deep);

                            position.FacingsDeep = trayFacingsDeep;
                        }

                        position.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                        position.OrientationTypeX = product.OrientationType.ToPositionOrientationType();
                        position.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                        position.OrientationTypeY = product.OrientationType.ToPositionOrientationType();
                        position.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
                        position.OrientationTypeZ = product.OrientationType.ToPositionOrientationType();

                        position.FacingsXHigh = Convert.ToInt16(position.FacingsHigh * rotatedTraySize.High);
                        position.FacingsXDeep = Convert.ToInt16(position.FacingsDeep * rotatedTraySize.Deep);
                        position.FacingsZWide = Convert.ToInt16(position.FacingsWide * rotatedTraySize.Wide);
                        position.FacingsZHigh = Convert.ToInt16(position.FacingsHigh * rotatedTraySize.High);

                        position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

                        // Update the caps
                        UpdateBreakTrayUnits(AxisType.Y, product, position);

                        return true;
                    }
                    // Just increase facings high, should stay as a unit.
                    position.FacingsHigh++;
                    return true;
                }
                if ((position.FacingsYHigh + 1) == rotatedTraySize.High)
                {
                    position.FacingsHigh++;

                    if (position.IsMaxStackExceeded(product))
                    {
                        position.FacingsHigh--;
                        return true;
                    }
                    position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                    SetFacingCapUnits(AxisType.Y, position, 0);

                    //Update caps
                    UpdateBreakTrayUnits(AxisType.Y, product, position);

                    return true;
                }
                position.FacingsYHigh++;

                // Update Caps
                position.FacingsYWide = Convert.ToInt16(rotatedTraySize.Wide * position.FacingsWide);
                position.FacingsYDeep = Convert.ToInt16(rotatedTraySize.Deep * position.FacingsDeep);
                position.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                position.OrientationTypeY = product.OrientationType.ToPositionOrientationType();

                return true;
            }
            return false;
        }

        private Boolean TryBreakTrayDownOnly(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            if (product.CanBreakTrayDown &&
                !product.CanBreakTrayTop)
            {
                // If we have a total units high less than the product.tray high we should just increase normal unit facings high 
                // (When we are less than a single tray).
                if (position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit &&
                    position.FacingsHigh < rotatedTraySize.High)
                {
                    // If should unit facings should become a single facing
                    if ((position.FacingsHigh + 1) == rotatedTraySize.High)
                    {
                        position.FacingsHigh = 1;
                        //We want to update, but only when it won't cause 0 facings to occur
                        if ((position.FacingsWide / rotatedTraySize.Wide >= 1))
                            position.FacingsWide = Convert.ToInt16(position.FacingsWide / rotatedTraySize.Wide);
                        if ((position.FacingsDeep / rotatedTraySize.Deep) >= 1)
                            position.FacingsDeep = Convert.ToInt16(position.FacingsDeep / rotatedTraySize.Deep);

                        position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

                        //Update caps
                        UpdateBreakTrayUnits(AxisType.Y, product, position);

                        return true;
                    }
                    // Just increase facings high, should stay as a unit.
                    position.FacingsHigh++;

                    if (position.IsMaxStackExceeded(product))
                    {
                        position.FacingsHigh--;
                        return true;
                    }
                    // Update Caps
                    UpdateBreakTrayUnits(AxisType.Y, product, position);
                    return true;
                }
                // We reach here when we have at least one full main tray. 
                // As we cannot break tray top then we have to go and increase facings high only.
                position.FacingsHigh++;

                if (position.IsMaxStackExceeded(product))
                {
                    position.FacingsHigh--;
                    return true;
                }
                // Update Caps
                UpdateBreakTrayUnits(AxisType.Y, product, position);
                return true;
            }
            return false;
        }

        public Boolean TryBreakTrayTopOnly(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize)
        {
            if (product.CanBreakTrayTop &&
                !product.CanBreakTrayDown)
            {
                // If we are unable to break the tray down then enforce the main block as a tray:
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

                if ((position.FacingsYHigh + 1) < rotatedTraySize.High)
                {
                    position.FacingsYHigh++;
                    position.FacingsYWide = Convert.ToInt16(rotatedTraySize.Wide * position.FacingsWide);
                    position.FacingsYDeep = Convert.ToInt16(rotatedTraySize.Deep * position.FacingsDeep);
                    position.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                    position.OrientationTypeY = product.OrientationType.ToPositionOrientationType();

                    //update cappings
                    UpdateBreakTrayUnits(AxisType.Y, product, position);
                    return true;
                }
                position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                SetFacingCapUnits(AxisType.Y, position, 0);

                if (position.FacingsHigh == 0)
                {
                    position.FacingsHigh = 1;

                    if (position.IsMaxStackExceeded(product))
                    {
                        position.FacingsHigh--;
                        return true;
                    }
                    //Update caps
                    UpdateBreakTrayUnits(AxisType.Y, product, position);
                    return true;
                }
                position.FacingsHigh++;

                if (position.IsMaxStackExceeded(product))
                {
                    position.FacingsHigh--;
                    return true;
                }
                //UpdateCaps
                UpdateBreakTrayUnits(AxisType.Y, product, position);
                return true;
            }
            return false;
        }        
    }
}
