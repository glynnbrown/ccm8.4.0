﻿using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising.Tray
{
    public interface ITrayPositionManager
    {
        Boolean IncreaseUnits(PlanogramProduct product, PlanogramPosition position,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup);

        Boolean DecreaseUnits(PlanogramProduct product, PlanogramPosition position,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup);
    }
}
