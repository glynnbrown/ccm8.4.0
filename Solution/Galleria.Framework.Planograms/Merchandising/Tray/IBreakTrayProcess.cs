﻿using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising.Tray
{
    public interface IBreakTrayProcess
    {
        Boolean TryBreakTray(PlanogramProduct product, PlanogramPosition position, WideHighDeepValue rotatedTraySize);
        
        void UpdateBreakTrayUnits(AxisType axis, PlanogramProduct product, PlanogramPosition position);
    }
}
