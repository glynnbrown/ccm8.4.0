﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27439 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved into common namespace.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Moved to framework
#endregion
#region Version History : CCM830
// V8-32686 : A.Kuszyk
//  Modified methods to use group scores rather than product scores.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// A class that represents the space calculations that need to be performed for a range
    /// of dividers.
    /// </summary>
    public sealed class BlockingDividerSpaceCalculation
    {
        #region Fields
        private List<BlockingDividerSpaceCalculationItem> _calculationItemsOrderedByPosition = new List<BlockingDividerSpaceCalculationItem>();
        private Single _startingBound;
        private Single _endingBound;
        #endregion

        #region Nested Classes
        /// <summary>
        /// A class that represents the divider space calculation for a single divider.
        /// </summary>
        internal class BlockingDividerSpaceCalculationItem : IComparable
        {
            #region Properties

            /// <summary>
            /// The divider to which this calculation item refers.
            /// </summary>
            public PlanogramBlockingDivider Divider { get; private set; }

            /// <summary>
            /// The original X or Y position (depending on the divider type) that was occupied by the divider.
            /// </summary>
            public Single OriginalPosition { get; private set; }

            /// <summary>
            /// The calculated score for this divider. Null unless CalculateScore has been called.
            /// </summary>
            public Double? Score { get; private set; }

            /// <summary>
            /// The normalised position of this divider, based on it's score. Null unless 
            /// CalculateNormalisedPosition has been called.
            /// </summary>
            public Double? NormalisedPosition { get; private set; }

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new calculation item.
            /// </summary>
            /// <param name="divider">The divider that this item will calculate.</param>
            public BlockingDividerSpaceCalculationItem(PlanogramBlockingDivider divider)
            {
                Divider = divider;
                OriginalPosition = Divider.Type == PlanogramBlockingDividerType.Vertical ? Divider.X : Divider.Y;
            }
            #endregion

            #region Methods

            /// <summary>
            /// Calculates the score for this divider based on the products in it's adjacent locations.
            /// </summary>
            /// <param name="metricProfile">The metric profile to use when evaluating psi values.</param>
            /// <param name="scoresByGroup">A dictionary of <see cref="PlanogramBlockingGroup"/>s and their scores.</param>
            public void CalculateScore(IPlanogramMetricProfile metricProfile, Dictionary<PlanogramBlockingGroup, Double> scoresByGroup)
            {
                Score = GetScoreForDividerLocations(metricProfile, scoresByGroup);
            }

            /// <summary>
            /// Calculates the normalised position for this divider given the total score and the starting and ending bounds.
            /// </summary>
            /// <param name="previousPosition">The position value of the previous divider.</param>
            /// <param name="totalScore">The total score against which this score should be normalised.</param>
            /// <param name="startingBound">The minimum starting bound.</param>
            /// <param name="endingBound">The maximum ending bound.</param>
            /// <exception cref="InvalidOperationException">Thrown if Score is null.</exception>
            public void CalculateNormalisedPosition(Double previousPosition, Double totalScore, Single startingBound, Single endingBound)
            {
                if (totalScore.EqualTo(0)) throw new ArgumentException("totalScore cannot be zero");
                if (Score == null) throw new InvalidOperationException("Score cannot be null");

                NormalisedPosition =
                    startingBound + previousPosition + ((Score / totalScore) * (endingBound - startingBound));
            }

            /// <summary>
            /// Returns the result of this calculation.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="InvalidOperationException">Thrown if NormalisedPosition is null.</exception>
            public PlanogramBlockingDividerSpaceCalculationResult ToResult()
            {
                if (NormalisedPosition == null) throw new InvalidOperationException("NormalisedPosition cannot be null");
                return new PlanogramBlockingDividerSpaceCalculationResult(this.Divider, (Single)this.NormalisedPosition);
            }

            /// <summary>
            /// Gets the amount by which this divider should move in accordance with the performance
            /// data associated with the products in the location(s) between itself and its nearest, same-level
            /// neighbour (below or left by default).
            /// </summary>
            /// <param name="locationsFromAbove">
            /// If true, the delta will be calculated based on the products in the locations above (right) this divider.
            /// Otherwise, the locations below (left) will be used</param>
            /// <returns>The amount by which this divider should move.</returns>
            /// <exception cref="InvalidOperationException">Thrown if Divider.Parent is null.</exception>
            public Double GetScoreForDividerLocations(
                IPlanogramMetricProfile metricProfile,
                Dictionary<PlanogramBlockingGroup, Double> scoresByGroup,
                Boolean locationsFromAbove = false)
            {
                if (Divider.Parent == null || Divider.Parent.Parent == null) throw new InvalidOperationException("Parent cannot be null");

                // Get dividers above or below this one, within the dividers group.
                // This excludes the parent dividers.
                IEnumerable<PlanogramBlockingDivider> dividersBelowOrAbove = GetDividersBelowOrAbove(locationsFromAbove);

                // Get the divider between which locations should be found for this divider's score.
                PlanogramBlockingDivider dividerBetweenWhichAreLocations;
                if (dividersBelowOrAbove.Any()) // Dividers in this group have been found.
                {
                    // Select closest divider from the list.
                    Double distanceToClosestGroupDivider = dividersBelowOrAbove.
                        Select(d => Math.Abs(d.Type == PlanogramBlockingDividerType.Vertical ? Divider.X - d.X : Divider.Y - d.Y)).
                        Min();

                    dividerBetweenWhichAreLocations = dividersBelowOrAbove.FirstOrDefault(d =>
                        Math.Abs((d.Type == PlanogramBlockingDividerType.Vertical ? Divider.X - d.X : Divider.Y - d.Y)).
                        EqualTo(distanceToClosestGroupDivider));
                }
                else // No dividers in this group were found, so we need the parallel parent divider.
                {
                    dividerBetweenWhichAreLocations = Divider.GetParallelParentDivider(locationsFromAbove);
                }

                // Capture locations from which to take scores.
                IEnumerable<PlanogramBlockingLocation> relevantLocations =
                    locationsFromAbove ?
                    Divider.Parent.GetLocationsBetweenDividers(Divider, dividerBetweenWhichAreLocations) :
                    Divider.Parent.GetLocationsBetweenDividers(dividerBetweenWhichAreLocations, Divider);

                // Take the sum of the psis for the products in each location's group, scaled by the amount of space
                // occupied by the location in its group.
                Double score = 0;
                foreach (PlanogramBlockingLocation location in relevantLocations)
                {
                    PlanogramBlockingGroup group = location.GetPlanogramBlockingGroup();
                    if (group.TotalSpacePercentage.EqualTo(0)) continue;
                    Double groupScore;
                    if (!scoresByGroup.TryGetValue(group, out groupScore))
                    {
                        groupScore = 0D;
                    }
                    score += (location.SpacePercentage / group.TotalSpacePercentage) * groupScore;
                }
                return score;
            }

            /// <summary>
            /// Gets the dividers above or below that represented by this calculation item, depending on the given flag,
            /// within the divider "band" that this divider occupies.
            /// </summary>
            /// <param name="locationsFromAbove">
            /// True if dividers in the positive direction should be returned, false
            /// if dividers in the negative direction should be returned.
            /// </param>
            /// <returns>An enumeration of dividers above or below this one, in this divider's "band".</returns>
            internal IEnumerable<PlanogramBlockingDivider> GetDividersBelowOrAbove(Boolean locationsFromAbove)
            {
                foreach (PlanogramBlockingDivider d in Divider.Parent.Dividers)
                {
                    // If the divider is this divider, or is of a different type of level, then ignore it.
                    if (d == Divider || d.Type != Divider.Type || d.Level != Divider.Level) continue;

                    switch (d.Type)
                    {
                        case PlanogramBlockingDividerType.Horizontal:
                            // Ensure that the divider is in the same "band" as this one.
                            if (!d.X.EqualTo(Divider.X)) continue;

                            // Check that it meets the locationsFromAbove criteria.
                            if (locationsFromAbove)
                            {
                                if (!d.Y.GreaterThan(Divider.Y)) continue;
                            }
                            else
                            {
                                if (!d.Y.LessThan(Divider.Y)) continue;
                            }

                            break;

                        case PlanogramBlockingDividerType.Vertical:
                            // Ensure that the divider is in the same "band" as this one.
                            if (!d.Y.EqualTo(Divider.Y)) continue;

                            // Check that it meets the locationsFromAbove criteria.
                            if (locationsFromAbove)
                            {
                                if (!d.X.GreaterThan(Divider.X)) continue;
                            }
                            else
                            {
                                if (!d.X.LessThan(Divider.X)) continue;
                            }
                            break;

                        default:
                            yield break;
                            throw new NotImplementedException();
                    }

                    yield return d;
                }
                yield break;
            }

            #endregion

            #region IComparable members
            /// <summary>
            /// Compares this instance to the object provided.
            /// </summary>
            /// <returns>-1 if this instance should appear before the object, +1 if it should appear after and 0 if the same.</returns>
            public Int32 CompareTo(Object obj)
            {
                var other = obj as BlockingDividerSpaceCalculationItem;
                if (other == null) throw new ArgumentException("obj is not comparable to DividerCalculation");

                if (OriginalPosition < other.OriginalPosition) return -1;
                if (OriginalPosition > other.OriginalPosition) return 1;
                return 0;
            }
            #endregion
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new divider space calculation, for the given dividers.
        /// </summary>
        /// <param name="dividers">The dividers whose space should be calculated.</param>
        /// <param name="endingBound">The maximum position value in the area occupied by the dividers.</param>
        /// <param name="startingBound">The minimum position value in the area occupied by the dividers.</param>
        /// <exception cref="ArgumentNullException">Thrown if dividers is null.</exception>
        /// <exception cref="ArgumentException">Thrown if dividers is empty.</exception>
        public BlockingDividerSpaceCalculation(IEnumerable<PlanogramBlockingDivider> dividers, Single startingBound, Single endingBound)
        {
            if (dividers == null) throw new ArgumentNullException("dividers cannot be null");
            if (!dividers.Any()) throw new ArgumentException("dividers cannot be empty");

            // Ensure that the starting and ending bounds are the right way round.
            _startingBound = startingBound <= endingBound ? startingBound : endingBound;
            _endingBound = endingBound >= startingBound ? endingBound : startingBound;

            // Create a calculation item for each divider.
            foreach (PlanogramBlockingDivider divider in dividers)
            {
                BlockingDividerSpaceCalculationItem dividerCalculationItem = new BlockingDividerSpaceCalculationItem(divider);
                _calculationItemsOrderedByPosition.Add(dividerCalculationItem);
            }

            // Dividers are sorted by their original position, ascending. This means
            // that all dividers will be processed in turn from the bottom to the top.
            _calculationItemsOrderedByPosition.Sort();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Calculates the normalised positions for all calculation items.
        /// </summary>
        /// <param name="metricProfile">The metric profile to use for the calculations.</param>
        /// <param name="scoresByGroup">A dictionary of <see cref="PlanogramBlockingGroup"/>s and their combined scores values to use for the calculations.</param>
        /// <returns>A collection of results from the calculation.</returns>
        /// <exception cref="ArgumentNullException">Thrown if metricProfile is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if productPsis is null.</exception>
        public IEnumerable<PlanogramBlockingDividerSpaceCalculationResult> CalculateNormalisedPositions(
            IPlanogramMetricProfile metricProfile,
            Dictionary<PlanogramBlockingGroup, Double> scoresByGroup)
        {
            if (metricProfile == null) throw new ArgumentNullException("metricProfile cannot be null");
            if (scoresByGroup == null) throw new ArgumentNullException("productScores cannot be null");

            // In order to calculate the normalised positions for each of the dividers, we need to calculate
            // the aspirational position of each divider first, and then normalise these positions against
            // the total to make sure that we don't exceed the available space (1).

            // Calculate the cumulative Psi score for all the products in the locations below the divider.
            foreach (BlockingDividerSpaceCalculationItem calculation in _calculationItemsOrderedByPosition)
            {
                calculation.CalculateScore(metricProfile, scoresByGroup);
            }

            // Now, calculate the total score for normalisation.
            Double previousPosition = 0;
            Double totalScore = _calculationItemsOrderedByPosition.Sum(i => i.Score == null ? 0 : (Double) i.Score);
            // Add on the score for the locations above the last divider.
            totalScore += _calculationItemsOrderedByPosition.
                Last().
                GetScoreForDividerLocations(metricProfile, scoresByGroup, locationsFromAbove: true);

            // If total score is zero then all the locations were missing products and we can't continue.
            // We throw a GetBlockingException here to be caught by the task execution to indicate that the
            // recalculation failed.
            if (totalScore.EqualTo(0))
            {
                throw new PlanogramBlockingException("Zero score due to missing products",
                    PlanogramEventLogEntryType.Warning);
            }

            // Now, go back through the calculations and normalise against the total.
            foreach (BlockingDividerSpaceCalculationItem calculation in _calculationItemsOrderedByPosition)
            {
                calculation.CalculateNormalisedPosition(previousPosition, totalScore, _startingBound, _endingBound);
                previousPosition = (Double) calculation.NormalisedPosition - _startingBound;
            }

            // Finally, return the calculation items as results.
            return _calculationItemsOrderedByPosition.Select(i => i.ToResult());
        }

        #endregion
    }
}
