﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
#endregion
#region Version History : CCM820
// V8-30873 : L.Ineson
//  Removed IsCarPark flag
#endregion
#region Version History : CCM830
// V8-31672 : L.Ineson
//  Added RemerchandiseAllGroups
// V8-32302 : D.Pleasance
//  Added CleanUpUnusedCapValues() to RemerchandiseAllGroups
// CCM-18440 : A.Kuszyk
//  Moved GetPresentationStates from PlanogramMerchandisingGroup.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Merchandising
{
    public class PlanogramMerchandisingGroupList : List<PlanogramMerchandisingGroup>, IDisposable
    {
        #region Methods

        /// <summary>
        ///     Begin edit operations for all merch groups in this list.
        /// </summary>
        public void BeginEdit()
        {
            foreach (PlanogramMerchandisingGroup merchGroup in this)
            {
                merchGroup.BeginEdit();
            }
        }

        /// <summary>
        ///     Cancel edit operations for all merch groups in this list.
        /// </summary>
        public void CancelEdit()
        {
            foreach (PlanogramMerchandisingGroup merchGoup in this)
            {
                merchGoup.CancelEdit();
            }
        }

        /// <summary>
        /// Called to apply the edits to all groups.
        /// </summary>
        public void ApplyEdit()
        {
            foreach (PlanogramMerchandisingGroup merchandisingGroup in this)
            {
                merchandisingGroup.ApplyEdit();
            }
        }

        /// <summary>
        ///     Create a look up dictionary of every Gtin and its <see cref="PlanogramPositionPlacement"/> instances from this collection.
        /// </summary>
        /// <remarks>No placement on subcomponents flagged as IsCarPark are included in the lookup.</remarks>
        public Dictionary<String, PlanogramPositionPlacementList> LookUpPlacementsByGtin()
        {
            var placementsByGtin = new Dictionary<String, PlanogramPositionPlacementList>();
            foreach (PlanogramMerchandisingGroup merchandisingGroup in this)
            {
                //  Iterate all positions
                foreach (PlanogramPositionPlacement placement in merchandisingGroup.PositionPlacements)
                {
                    String gtin = placement.Product.Gtin;
                    if (!placementsByGtin.ContainsKey(gtin))
                        placementsByGtin.Add(gtin, PlanogramPositionPlacementList.NewPlanogramPositionPlacementList());
                    placementsByGtin[gtin].Add(placement);
                }
            }
            return placementsByGtin;
        }

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        public void Dispose()
        {
            foreach (PlanogramMerchandisingGroup merchandisingGroup in this)
            {
                merchandisingGroup.Dispose();
            }
        }

        /// <summary>
        /// Ensures that all groups have been processed and autofilled.
        /// </summary>
        public void RemerchandiseAllGroups()
        {
            if (!this.Any()) return;

            Planogram plan = this.First().SubComponentPlacements.First().Planogram;

            //Shrink down all of the affected groups first if we have any hanging components.
            // This is so that bars which overlap with shelves get processed correctly.
            if (this.Any(g => g.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang
                || g.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom))
            {
                foreach (PlanogramMerchandisingGroup merchGroup in this)
                {
                    Boolean shouldFillX = (plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide
                        && merchGroup.StrategyX != PlanogramSubComponentXMerchStrategyType.Manual);

                    Boolean shouldFillY = (plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh
                        && merchGroup.StrategyY != PlanogramSubComponentYMerchStrategyType.Manual);

                    Boolean shouldFillZ = (plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep
                        && merchGroup.StrategyZ != PlanogramSubComponentZMerchStrategyType.Manual);

                    foreach (PlanogramPositionPlacement positionPlacement in merchGroup.PositionPlacements)
                    {
                        positionPlacement.SetMinimumUnits(shouldFillX, shouldFillY, shouldFillZ);
                    }
                }
            }

            //Proces the groups working down the plan so that hang down obstructions are dealt with
            foreach (PlanogramMerchandisingGroup merchandisingGroup in 
                this.OrderByDescending(mgl => mgl.SubComponentPlacements.First().GetPlanogramRelativeCoordinates().Y))
            {
                if (!merchandisingGroup.Autofill(this))
                    merchandisingGroup.Process();

                foreach (PlanogramPositionPlacement position in merchandisingGroup.PositionPlacements)
                {
                    position.Position.CleanUpUnusedCapValues();
                }

                merchandisingGroup.ApplyEdit();
            }
        }

        /// <summary>
        ///     Get the <see cref="PlanogramMerchandisingPositionPresentationState"/> instances 
        /// for each <see cref="PlanogramPositionPlacement"/> instance in this <see cref="PlanogramMerchandisingGroup"/> 
        /// that matches the provided <paramref name="blockingGroup"/> color and <paramref name="sequenceGtinsSet"/>.
        /// </summary>
        /// <param name="blockingGroup">The <see cref="IPlanogramBlockingGroup"/> instance whose color is to be matched by the placements.</param>
        /// <param name="sequenceGtinsSet">The set of GTINS in a sequence that must be matched by the placements.</param>
        /// <returns>A <see cref="Dictionary{TKey,TValue}"/> of GTINS
        ///  and their respective (first) <see cref="PlanogramMerchandisingPositionPresentationState"/>.</returns>
        /// <remarks>When a product GTIN appears in more than one placement only the first gets its presentation state returned.</remarks>
        internal Dictionary<String, PlanogramMerchandisingPositionPresentationState> GetPresentationStates(
            IPlanogramBlockingGroup blockingGroup,
            ICollection<String> sequenceGtinsSet)
        {
            var placementsForSequenceColor = this
                .SelectMany(m => m.PositionPlacements)
                .Where(placement => placement.Position.SequenceColour.Equals(blockingGroup.Colour));
            var placementsInSequence = placementsForSequenceColor.Where(placement => sequenceGtinsSet.Contains(placement.Product.Gtin));
            var uniqueGtinPlacementsInSequence = placementsInSequence.Distinct(new PlanogramPositionPlacement.ByGtinComparer());
            return uniqueGtinPlacementsInSequence
                .ToDictionary(placement => placement.Product.Gtin,
                              placement => new PlanogramMerchandisingPositionPresentationState(placement.Position));
        }

        #endregion
    }
}
