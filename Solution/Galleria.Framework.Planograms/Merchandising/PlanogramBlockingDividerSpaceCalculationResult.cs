﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27439 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved into common namespace.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Move to framework assembly
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// A class to contain a divider and its delta calculation result
    /// </summary>
    public sealed class PlanogramBlockingDividerSpaceCalculationResult
    {
        #region Properties

        /// <summary>
        /// The divider that this result is for.
        /// </summary>
        public PlanogramBlockingDivider Divider { get; private set; }

        /// <summary>
        /// The change in position that has been calculated for this divider.
        /// </summary>
        public Single Delta { get; private set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new result.
        /// </summary>
        /// <param name="divider">The divider this result is for.</param>
        /// <param name="delta">The amount this divider needs to move.</param>
        public PlanogramBlockingDividerSpaceCalculationResult(PlanogramBlockingDivider divider, Single normalisedPosition)
        {
            Divider = divider;
            Delta = normalisedPosition - (Divider.Type == PlanogramBlockingDividerType.Horizontal ? Divider.Y : Divider.X);
        }

        #endregion
    }
}
