﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM802
// V8-29137 : A.Kuszyk
//  Created.
// V8-29722 : A.Kuszyk
//  Added IsOverSpacedInAnyDirectionBut method.
#endregion

#region Version History: CCM803

// V8-29805 : A.Silva
//  Amended AddPosition so that it will now return the new position placement that was added, instead of just a boolean.

#endregion

#region Version History : CCM810

// V8-29770 : A.Kuszyk
//  Amended IsOverfilled method to check for positions outside of merchandising space in non-even axes.
// V8-29585 : A.Silva
//  Refactored IsReversed so that !IsPositive is used instead.
//  Temporarily removed V8-29770 changes until they can be tested and work correctly for any component.
// V8-29772 : A.Silva
//  Added code to account for FacingSpaceReductionPercentage when dealing with the FacingAxis in IsOverSpacedIn.
// V8-29972 : A.Silva
//  Amended AddPositionRelativeToMerchGroup so that the closest placements with position placements are considered, and not only adjacent ones.
// V8-30035 : A.Kuszyk
//  Remove SpaceReducedByObstructions and ensure that Coords, Size and Bounds all use this space by default.
#endregion

#region Version History : CCM811
// V8-30114 : A.Kuszyk
//  Updated IsOverfilled to take into account the merchandisable space available within the block space.
//  Refactored AddPositionRelativeToMerchGroup to take into account surrounding blocks and make a more intelligent decision
//  about where to add the product.
#endregion

#region Version History : CCM820
// V8-30997 : D.Pleasance
//  Amended AddPositionRelativeToMerchGroup, so that the validation check IsSequenceDirectionInvalid uses the original PlanogramBlockingGroupPlacementType.
// V8-31164 : D.Pleasance
//  Amended to obtain sequence number from sequence group.
#endregion

#region Version History : CCM830
// V8-31666 : A.Kuszyk
//  Corrected an issue in IsOverfilled related to checking all dimensions on the Merchandising Group.
// V8-31874 : A.Kuszyk
//  Re-factored layout code in from the Merchandise Planogram task.
// V8-31804 : A.Kuszyk
//  Removed inaccurate population of positions from constructor.
// V8-32433 : D.Pleasance
//  Refactored AddPositionRelativeToMerchGroup(), added peg \ chest placement improvements
// V8-32505 : A.Kuszyk
//  Prototyped sequence sub group changes.
// V8-32433 : D.Pleasance
//  Amended TryGetAnchorAndDirection() when trying to get anchor from Y direction. Now takes into account where blocks exist and places accordingly rather than first in the sequence.
// V8-32613 : D.Pleasance
//  Amended GetAvailableFacingSpaceAsPercentage(), the block placement may be for a peg \ chest and have multiple sequence lines in the facing axis. 
// V8-32778 : D.Pleasance
//  Amended GetAvailableFacingSpaceAsPercentage() removed rounding from result value. When calculating % reduction precision is important.
// V8-32881 : A.Kuszyk
//  Updated GetSurroundingBlockPlacementsThatIntersectFirstAndSecondNormals to check for adjacency in sequence direction.
// V8-32906 : A.Kuszyk
//  Amended PushProductToNextBlockPlacement to push products to next block placement if compacting blocks.
// V8-32787 : A.Silva
//  Added the Assortment Rule Enforcer to TryProductPlacement, so that rules can be observed.
//  Added an optional AssortmentRuleEnforcer property, so that any task that need to enforce rules can do so.
// CCM-18435 : A.Silva
//  Removed the check for IsProductOverlapAllowed. Automation should not overlap as that is a manual decision.
// CCM-18440 : A.Kuszyk
//  Added Type property and made changes to the way adjacent block placements are assessed.
// CCM-18462 : A.Kuszyk
//  Added merging methods.
// CCM-13871 : A.Silva
//  Refactored AddPosition to reduce nesting and simplify logic.
//  Amended GetSequenceLineAnchorPosition so that the closest position to the sequence origin is used every time.
//  TryAddPositionRelativeToPositionPlacements will not use anchors that would break the anchor's block placement (no inserts from other blocks).
// CCM-18505 : A.Silva
//  Amended GetSequenceLineAnchorPosition when the anchor is on the Z axis so that the primary sequence is correctly factored in.
// CCM-18507 : A.Silva
//  Layout method will push all remaining products to the next placement if ALL were dropped due to the First on Block rule.
// CCM-18530 : A.Silva
//  Moved the AnyPositionsOverlap method from PlanogramMerchandisingBlocks and amended it so that only direct collisions within or around the BlockPlacement are considered.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using static Galleria.Framework.Planograms.Logging.DebugLayout;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents a volume of space within a blocking group that exists on a merchandising group.
    /// This space could contain a fraction of one or more blocking locations depending on their
    /// arrangement and whether or not the form a continuous region of space when combined.
    /// </summary>
    [DebuggerDisplay("Block Placement {SequenceNumber}, XYZ: ({Bounds.X}, {Bounds.Y}, {Bounds.Z}), WHD: ({Bounds.Width}, {Bounds.Height}, {Bounds.Depth}), Positions: {_positionPlacements.Count}")]
    public sealed class PlanogramMerchandisingBlockPlacement
    {
        #region Fields

        private readonly AssortmentRuleEnforcer _assortmentAssortmentRuleEnforcer;
        private Single _overlapThreshold = 0.1f;
        private IEnumerable<PlanogramMerchandisingBlockPlacement> _merchGroupBlockPlacementsInParentBlock;
        private IEnumerable<PlanogramMerchandisingBlockPlacement> _merchGroupBlockPlacementsInRootBlocking;
        private PlanogramMerchandisingGroup _merchandisingGroup;
        private readonly List<PlanogramPositionPlacement> _positionPlacements;
        private RectValue _bounds;

        /// <summary>
        /// The <see cref="PlanogramMerchandisingSpace"/> of <see cref="MerchandisingSpace"/> that intersects with <see cref="Bounds"/>.
        /// </summary>
        private PlanogramMerchandisingSpace _intersectingMerchSpace;

        #endregion

        #region Constructor

        /// <summary>
        /// Instantiates a new Block Placement with the given coordinates, size and obstructions, relative
        /// to the merchandising group provided.
        /// </summary>
        internal PlanogramMerchandisingBlockPlacement(
            Int32 sequenceNumber,
            PlanogramBlockingGroupPlacementType primarySequenceDirection,
            PlanogramBlockingGroupPlacementType secondarySequenceDirection,
            PlanogramBlockingGroupPlacementType tertiarySequenceDirection,
            PlanogramMerchandisingGroup merchandisingGroup,
            PlanogramMerchandisingSpace merchandisingSpace,
            PlanogramMerchandisingSpace originalMerchandisingSpace,
            PlanogramMerchandisingBlock parent,
            PlanogramMerchandisingBlockPlacementType type,
            IEnumerable<PlanogramPositionPlacement> positionPlacements = null,
            AssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            Type = type;
            SequenceNumber = sequenceNumber;
            PrimarySequenceDirection = primarySequenceDirection;
            SecondarySequenceDirection = secondarySequenceDirection;
            TertiarySequenceDirection = tertiarySequenceDirection;
            _assortmentAssortmentRuleEnforcer = assortmentRuleEnforcer;

            OriginalBounds = originalMerchandisingSpace.SpaceReducedByObstructions;
            MerchandisingGroup = merchandisingGroup;
            Bounds = merchandisingSpace.SpaceReducedByObstructions;
            _positionPlacements = positionPlacements == null ? new List<PlanogramPositionPlacement>() : positionPlacements.ToList();
            Obstructions = merchandisingSpace.Obstructions.ToList();
            Parent = parent;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Indicates what type of bounds this instance was created with.
        /// </summary>
        public PlanogramMerchandisingBlockPlacementType Type { get; }

        /// <summary>
        /// The <see cref="FacingSpaceReductionPercentage"/> set on the previous layout pass that affected this
        /// <see cref="PlanogramMerchandisingBlockPlacement"/>.
        /// </summary>
        public Single PreviousFacingSpaceReductionPercentage { get; set; }

        /// <summary>
        /// The percentage points by which the facing size of this placement should be reduced when checking for overfills..
        /// </summary>
        public Single FacingSpaceReductionPercentage { get; set; }

        /// <summary>
        /// The PlanogramMerchandisingBlock that represents the Blocking Group to which this space belongs.
        /// </summary>
        public PlanogramMerchandisingBlock Parent { get; private set; }

        /// <summary>
        /// The primary direction in which products should be placed in this space.
        /// </summary>
        public PlanogramBlockingGroupPlacementType PrimarySequenceDirection { get; private set; }
        
        /// <summary>
        /// The secondary direction in which products should be placed in this space.
        /// </summary>
        public PlanogramBlockingGroupPlacementType SecondarySequenceDirection { get; private set; }
        
        /// <summary>
        /// The tertiary direction in which products should be placed in this space.
        /// </summary>
        public PlanogramBlockingGroupPlacementType TertiarySequenceDirection { get; private set; }

        /// <summary>
        /// The sequence in which this block placement should be merchandised.
        /// </summary>
        public Int32 SequenceNumber { get; private set; }
        
        /// <summary>
        /// The location of this space relative to the Merchandising Group that it is associated with.
        /// </summary>
        public PointValue Coordinates { get { return Bounds.ToPointValue(); } }

        /// <summary>
        /// The size of the volume that can be merchandised, restricted by surrounding obstructions.
        /// </summary>
        public WidthHeightDepthValue Size { get { return Bounds.ToWidthHeightDepth(); } }

        /// <summary>
        /// The size of this volume reduced to fit under the closest obstructions, leaving un-restricted
        /// space that can definitely be merchandised (component space permitting).
        /// </summary>
        public RectValue Bounds 
        {
            get { return _bounds; }
            private set
            {
                _bounds = value;
                _intersectingMerchSpace = MerchandisingGroup.GetMerchandisingSpace(
                    planogramRelative: false,
                    maximiseWidthForHang: false,
                    maximiseDepthForHang: true,
                    constrainingSpace: _bounds);
            }
        }

        /// <summary>
        /// The size of this volume reduced to fit under the closest obstructions, leaving un-restricted
        /// space that can definitely be merchandised (component space permitting). 
        /// This is the original bounds based upon original planogram positions and does not include white-space
        /// </summary>
        public RectValue OriginalBounds { get; private set; }

        /// <summary>
        /// Any obstructions to the size of this volume that may not be merchandisable.
        /// </summary>
        public IEnumerable<RectValue> Obstructions { get; private set; }
        
        /// <summary>
        /// The merchandising group upon which (and relative to) this block placement exists.
        /// </summary>
        public PlanogramMerchandisingGroup MerchandisingGroup 
        {
            get { return _merchandisingGroup; }
            set { _merchandisingGroup = value; }
        }
        
        /// <summary>
        /// The positions that are to be placed into this space.
        /// </summary>
        public IEnumerable<PlanogramPositionPlacement> PositionPlacements
        {
            get { return _positionPlacements; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Lays out products into this block placement using the data in the <paramref name="cnotext"/>.
        /// </summary>
        /// <typeparam name="TPostLayoutContext"></typeparam>
        /// <param name="context"></param>
        public void Layout(IPlanogramMerchandisingLayoutContext context)
        {
            var skippedDueToFirstOnBlock = new List<PlanogramProduct>();
            while (context.ProductsToPlace.Any())
            {
                PlanogramProduct currentProduct = context.ProductsToPlace.Pop();

                //  If the current product had been skipped as first on block before, clear that state before trying to place it again.
                if (context.SkippedFirstOnBlock.Contains(currentProduct.Gtin))
                {
                    context.SkippedFirstOnBlock.Remove(currentProduct.Gtin);
                }

                // Start each product placement by attempting the primary placement direction.
                context.CurrentSequenceDirection = PrimarySequenceDirection;

                //  Continue processing if the layout was succesful.
                if (LayoutProduct(context, currentProduct)) continue;

                //  If the placement failed, but this is the first product to be placed on the component, skip it.
                if (!PositionPlacements.Any())
                {
                    skippedDueToFirstOnBlock.Add(currentProduct);
                    context.SkippedFirstOnBlock.Add(currentProduct.Gtin);
                    continue;
                }

                //  If it was not the first product on the component, try moving it to the next one.
                PushProductToNextBlockPlacement(context, currentProduct);
                break;
            }

            // Ensure that all components are attempted at least once 
            //  before dropping the block completely 
            //  when some will fit NO remaining products.
            Boolean componentEmptyDueToFirstOnBlock = skippedDueToFirstOnBlock.Count != 0 && !PositionPlacements.Any();
            if (componentEmptyDueToFirstOnBlock)
                PushSkippedProductsToNextBlockPlacement(context, skippedDueToFirstOnBlock);
        }

        /// <summary>
        /// Moves a product onto the next block placement, if appropriate, by manipulating the product stack.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="currentProduct"></param>
        private void PushProductToNextBlockPlacement(IPlanogramMerchandisingLayoutContext context, PlanogramProduct currentProduct)
        {
            PlanogramSequenceGroupSubGroup subGroup = GetSubGroup(currentProduct);

            // If this product is not in a sub group, just put it back on the stack for the next block placement.
            if (subGroup == null)
            {
                context.ProductsToPlace.Push(currentProduct);
                return;
            }

            if (ProductIsFirstInSubGroup(currentProduct, subGroup) || FacingSpaceReductionPercentage.GreaterThan(0))
            {
                // If the product is first in the sub group and it didn't fit, bump it onto the next block placement.
                // Or, if we are compacting space, bump the whole sub-group out.
                SkipBlockPlacementForSubGroup(context, currentProduct, subGroup);
            }
            else
            {
                // If the product is not the first in the sub group, then the sub group has aleady established
                // itself on this block placement, so skip the remaining sub group products on any future
                // block placements.
                SkipSubsequentProductsForSubGroup(context, currentProduct, subGroup);
            }
        }

        /// <summary>
        ///     Ignore this <see cref="PlanogramMerchandisingBlockPlacement"/> due to the <c>First on Block</c> rule
        /// by pushing all remaining products to the next one.
        /// </summary>
        /// <param name="context">The <see cref="IPlanogramMerchandisingLayoutContext"/> in use when laying out this instance.</param>
        /// <param name="skippedDueToFirstOnBlock">
        ///     Collection of <see cref="PlanogramProduct"/> instances that should have been placed on the placement
        /// but were not due to the <c>First on Block</c> rule.</param>
        private void PushSkippedProductsToNextBlockPlacement(IPlanogramMerchandisingLayoutContext context, List<PlanogramProduct> skippedDueToFirstOnBlock)
        {
            context.SkippedBlockPlacement.Add(this);
            skippedDueToFirstOnBlock.Reverse();
            foreach (PlanogramProduct product in skippedDueToFirstOnBlock)
            {
                context.ProductsToPlace.Push(product);
            }
        }

        /// <summary>
        /// Determines if the <paramref name="currentProduct"/> is first in sequence in the <paramref name="subGroup"/>.
        /// </summary>
        /// <param name="currentProduct"></param>
        /// <param name="subGroup"></param>
        /// <returns></returns>
        private Boolean ProductIsFirstInSubGroup(PlanogramProduct currentProduct, PlanogramSequenceGroupSubGroup subGroup)
        {
            if (!subGroup.EnumerateProducts().Any()) return false;
            return subGroup.EnumerateProducts().OrderBy(p => p.SequenceNumber).First().Gtin.Equals(currentProduct.Gtin);
        }

        /// <summary>
        /// Removes products from <paramref name="currentProduct"/> onwards in the <paramref name="subGroup"/> from the product stack
        /// to ensure they do not get placed on any subsequent components.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="currentProduct"></param>
        /// <param name="subGroup"></param>
        private void SkipSubsequentProductsForSubGroup(
            IPlanogramMerchandisingLayoutContext context, 
            PlanogramProduct currentProduct,
            PlanogramSequenceGroupSubGroup subGroup)
        {
            Debug.WriteLine(String.Format(
                "Product {0} was part of a sub-group, but couldn't fit. Removing all subsequent products from product stack.",
                currentProduct.Gtin));

            // Work out which products should be left in the stack.
            IEnumerable<PlanogramProduct> remainingProducts = context.ProductsToPlace.Union(new[] { currentProduct }).ToList();
            IEnumerable<String> subGroupGtins = subGroup.EnumerateProducts().Select(p => p.Gtin).ToList();
            IEnumerable<PlanogramProduct> remainingProductsNotInSubGroup = remainingProducts.Where(p => !subGroupGtins.Contains(p.Gtin));
            IEnumerable<PlanogramProduct> remainingProductsInSubGroup = remainingProducts.Where(p => subGroupGtins.Contains(p.Gtin));

            // Re-build the product stack.
            context.ProductsToPlace.Clear();
            foreach (PlanogramProduct productToPlace in remainingProductsNotInSubGroup.Reverse())
            {
                context.ProductsToPlace.Push(productToPlace);
            }

            // Store the skipped products, so that we can consider them in any post-layout actions if needed.
            foreach (PlanogramProduct skippedProduct in remainingProductsInSubGroup.Reverse())
            {
                context.SkippedSubGroupProducts.Push(skippedProduct);
            }
        }

        /// <summary>
        /// Gets the sub group for the <paramref name="currentProduct"/>.
        /// </summary>
        /// <param name="currentProduct"></param>
        /// <returns></returns>
        private PlanogramSequenceGroupSubGroup GetSubGroup(PlanogramProduct currentProduct)
        {
            PlanogramSequenceGroupProduct sequenceGroupProduct = Parent.GetSequenceGroupProduct(currentProduct);
            if (!(sequenceGroupProduct != null && sequenceGroupProduct.PlanogramSequenceGroupSubGroupId != null)) return null;
            return sequenceGroupProduct.GetSubGroup();
        }

        /// <summary>
        /// Removes all products for the <paramref name="subGroup"/> from the current block placement and pushes them back into
        /// the product stack to be re-placed in the next block placement.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="currentProduct"></param>
        /// <param name="subGroup"></param>
        private void SkipBlockPlacementForSubGroup(
            IPlanogramMerchandisingLayoutContext context, 
            PlanogramProduct currentProduct,
            PlanogramSequenceGroupSubGroup subGroup)
        {
            Debug.WriteLine(String.Format(
                "Product {0} was part of a sub-group, but couldn't fit. Removing all sub-group positions from component.",
                currentProduct.Gtin));

            // If this product was part of a sub group, move the whole sub-group onto the next placement.
            foreach (PlanogramSequenceGroupProduct subGroupProduct in subGroup.EnumerateProducts().OrderByDescending(p => p.SequenceNumber))
            {
                if (subGroupProduct.Gtin.Equals(currentProduct.Gtin))
                {
                    context.ProductsToPlace.Push(currentProduct);
                    continue;
                }

                PlanogramPositionPlacement subGroupProductPlacement = PositionPlacements
                    .FirstOrDefault(p => p.Product.Gtin.Equals(subGroupProduct.Gtin));
                if (subGroupProductPlacement == null) continue;
                MerchandisingGroup.RemovePositionPlacement(subGroupProductPlacement);
                _positionPlacements.Remove(subGroupProductPlacement);
                context.ProductsToPlace.Push(subGroupProductPlacement.Product);
            }
        }

        /// <summary>
        /// Lays out the <paramref name="currentProduct"/> on this block placement.
        /// </summary>
        /// <typeparam name="TPostLayoutContext"></typeparam>
        /// <param name="context"></param>
        /// <param name="currentProduct">The product to place.</param>
        /// <returns>True if product was successfully placed.</returns>
        private Boolean LayoutProduct(IPlanogramMerchandisingLayoutContext context, PlanogramProduct currentProduct)
        {
            while (context.CurrentSequenceDirection != null)
            {
                //  If the product was placed,  break the while loop
                //  and continue to the next one.
                if (TryProductPlacement(context, currentProduct)) return true;

                //  End of line, change the placement direction.
                //  Reverse the direction that we're on for next time we try it.
                if (PositionPlacements.Any())
                {
                    PlanogramBlockingGroupPlacementType currentSequenceDirection = context.CurrentSequenceDirection.Value;

                    if (currentSequenceDirection == PrimarySequenceDirection)
                        context.ReversePrimary = !context.ReversePrimary;
                    else if (currentSequenceDirection == SecondarySequenceDirection)
                        context.ReverseSecondary = !context.ReverseSecondary;
                    else if (currentSequenceDirection == TertiarySequenceDirection)
                        context.ReverseTertiary = !context.ReverseTertiary;
                }
                context.CurrentSequenceDirection = GetNextSequenceDirection(context.CurrentSequenceDirection);
            }
            return false;
        }

        /// <summary>
        /// Trys to place the <paramref name="currentProduct"/> in this block placement.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="currentProduct">The product to try placing.</param>
        /// <returns>True if the product was successfully placed.</returns>
        private Boolean TryProductPlacement(IPlanogramMerchandisingLayoutContext context, PlanogramProduct currentProduct)
        {
            if (!context.CurrentSequenceDirection.HasValue) return false;
            MerchandisingGroup.BeginEdit();

            // Check if we need to reverse the sequence direction when placing the product.
            Boolean isReversed = context.CurrentSequenceDirection.Value == PrimarySequenceDirection
                              ? context.ReversePrimary
                              : (context.CurrentSequenceDirection.Value == SecondarySequenceDirection
                                     ? context.ReverseSecondary
                                     : context.ReverseTertiary);

            // If we weren't able to place the product, continue to try the next sequence direction.
            PlanogramBlockingGroupPlacementType sequenceDirection = context.CurrentSequenceDirection.Value.Invert(isReversed);
            PlanogramPositionPlacement newPositionPlacement = AddPosition(currentProduct, sequenceDirection);

            if (newPositionPlacement == null)
            {
                MerchandisingGroup.CancelEdit();
                return false;
            }

            PlanogramMerchandisingPositionPresentationState positionPresentation;
            if (Parent.PositionsPresentationStates.TryGetValue(currentProduct.Gtin, out positionPresentation))
            {
                newPositionPlacement.ApplyPositionPresentation(positionPresentation);
            }

            // Get the minimum and target units, where necessary.
            Int32 minimumUnits;
            Int32 targetUnits;
            GetMinAndTargetUnits(context, currentProduct, out minimumUnits, out targetUnits);

            // Set the units at or close to the target, as appropriate.
            PlanogramPositionInventoryTargetType inventoryTargetType =
                context.MustAchieveTargetUnits ?
                PlanogramPositionInventoryTargetType.GreaterThanOrEqualTo :
                PlanogramPositionInventoryTargetType.ClosestOrEqualTo;

            newPositionPlacement.SetUnits(targetUnits, inventoryTargetType, _assortmentAssortmentRuleEnforcer, context.InventoryChangeType);

            // Process the merch group
            newPositionPlacement.RecalculateUnits();
            MerchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);

            // If we have overfilled the block placement, or haven't achieved minimum units, 
            // cancel the add operation and return false.
            // If we must achieve target units, then we always place at or above target.
            Boolean achievedMinimumUnits = context.MustAchieveTargetUnits || newPositionPlacement.Position.TotalUnits >= minimumUnits;

            // If all seems well, do not cancel the edit to the merch group (it will be applied at the end of the task execution)
            // and remove the product from the stack. Return true, because we don't want to try any other sequence directions.
            if (!IsOverFilled(sequenceDirection, context.IgnoreMerchandisingDimensions) && achievedMinimumUnits) return true;

            MerchandisingGroup.CancelEdit();
            RemoveLastPosition();
            return false;
        }

        private static void GetMinAndTargetUnits(
            IPlanogramMerchandisingLayoutContext context, 
            PlanogramProduct currentProduct, 
            out Int32 minimumUnits,
            out Int32 targetUnits)
        {
            context.TargetUnitsByGtin.TryGetValue(currentProduct.Gtin, out targetUnits);
            if (targetUnits == 0) targetUnits = 1;

            minimumUnits = 1;
            if (!context.MustAchieveTargetUnits)
            {
                context.MinimumUnitsByGtin.TryGetValue(currentProduct.Gtin, out minimumUnits);
            }
            else
            {
                minimumUnits = targetUnits;
            }
        }

        /// <summary>
        /// Determines if the block placement and merchandising group are overfilled, given the various options in <paramref name="context"/>.
        /// </summary>
        /// <param name="sequenceDirection"></param>
        /// <param name="ignoreMerchandisingDimensions"></param>
        /// <returns></returns>
        private Boolean IsOverFilled(PlanogramBlockingGroupPlacementType sequenceDirection, Boolean ignoreMerchandisingDimensions)
        {
            Boolean isOverfilled;
            //  Determine if the full merchandising group should be used as the avaiable space for overfill,
            //  this is true just for the first placement on the block placement. Any subsequent ones need to use just the
            //  block placement's space.
            Boolean useFullMerchandisingGroup = PositionPlacements.Count() == 1;
            if (ignoreMerchandisingDimensions)
            {
                // If we're allowed to ignore merchandising dimensions, then we're only interested in whether or not
                // we have overfilled the block placement in the direction we are placing, or if we have caused a product
                // overlap within the merchandising group. Product overlaps outside the merchandising group are likely
                // to result from ignoring the merchandising dimensions.
                AxisType axisType = sequenceDirection.GetAxis();

                isOverfilled = useFullMerchandisingGroup
                                   ? MerchandisingGroup.IsOverfilled(axisType)
                                   : IsOverFilled(axisType);
            }
            else
            {
                // If we're not allowed to ignore merchandising dimensions, then we need to check that the block placement
                // hasn't been overfilled in any direction and that we haven't caused any product overlaps - either within
                // the merchandising group or on the planogram as a whole.
                isOverfilled = useFullMerchandisingGroup
                                   ? MerchandisingGroup.IsOverfilled()
                                   : IsOverFilled();
            }

            //  Even if it is not overfilled check the overlaps.
            //  When ignoring merchandising dimensions, overlaps should be checked locally, not all over the plan.
            return isOverfilled || HasOverlaps(includePlanogram: !ignoreMerchandisingDimensions);
        }

        /// <summary>
        /// Gets the amount of facing space on this block placement that is not occupied positions, expressed
        /// as a percentage of this block placement's <see cref="Size"/>.
        /// </summary>
        /// <returns></returns>
        public Single GetAvailableFacingSpaceAsPercentage()
        {
            AxisType axis = MerchandisingGroup.SubComponentPlacements.First().GetFacingAxis();
            Single totalSpace = Size.GetSize(axis);
            if (totalSpace.LessOrEqualThan(0)) return 0.0F;
            Single usedSpace = 0;
            Single maxSequenceUsedSpace = 0;

            foreach (Int16 sequenceLine in GetSequenceLines(PositionPlacements, axis))
            {
                usedSpace = GetSequenceLinePositions(PositionPlacements, sequenceLine, axis).Sum(p => p.GetReservedSpace().GetSize(axis));

                if (usedSpace.GreaterThan(maxSequenceUsedSpace))
                {
                    maxSequenceUsedSpace = usedSpace;
                }
            }

            usedSpace = maxSequenceUsedSpace;

            return (Single)(1 - usedSpace / totalSpace);
        }

        /// <summary>
        /// Gets all positons that form the sequence line 
        /// (Sequence direction X, sequence line Y)
        /// (Sequence direction Z, sequence line X)
        /// </summary>
        /// <param name="blockPlacementsWithPositions"></param>
        /// <param name="sequenceLine"></param>
        /// <param name="sequenceLineDirection"></param>
        /// <returns></returns>
        private static IEnumerable<PlanogramPositionPlacement> GetSequenceLinePositions(IEnumerable<PlanogramPositionPlacement> placements, Int16 sequenceLine, AxisType axisType)
        {
            switch (axisType)
            {
                case AxisType.X:
                    return placements.Where(p => p.Position.SequenceY == sequenceLine);
                case AxisType.Z:
                    return placements.Where(p => p.Position.SequenceX == sequenceLine);
                default:
                    break;
            }

            return placements.Where(p => p.Position.SequenceX == sequenceLine);
        }

        /// <summary>
        /// Adds the given product to the block placement by attempting to add it to the subcomponent space
        /// on the merchandising group that lies within the bounds of this block placement. If a position
        /// could not be added, then null is returned.
        /// </summary>
        /// <param name="product">The product to add a position placement for.</param>
        /// <param name="sequenceDirection"></param>
        /// <returns>
        /// Null may be returned if the given product is not within the sequence of products already placed. 
        /// E.g. If a product with a sequence of 1 has been placed and then a product with a sequence of 3 is passed
        /// to this method, null will be returned, because it cannot be placed.
        /// </returns>
        public PlanogramPositionPlacement AddPosition(PlanogramProduct product, PlanogramBlockingGroupPlacementType sequenceDirection)
        {
            PlanogramSequenceGroupProduct planogramSequenceGroupProduct = Parent.SequenceGroup.Products.FirstOrDefault(p => p.Gtin == product.Gtin);

            if (planogramSequenceGroupProduct == null) return null;

            Int32 sequenceNumber = planogramSequenceGroupProduct.SequenceNumber;
            PlanogramPositionPlacement placement;

            if (TryAddAsFirstPosition(product, sequenceDirection, sequenceNumber, out placement) ||
                TryAddPositionRelativeToPositionPlacements(product, sequenceDirection, PositionPlacements.ToList(), sequenceNumber, out placement))
                return placement;

            // There are positions on the merch group, but none of them are within this block placement's parent
            // block, so we need to work out how to sequence the position.
            // OR
            // Note (31804):
            // There may be multiple block placements on a merchandising group, but they might not be consecutive.
            // In this case, we should be searching for the most appropriate block placement to place relative to
            // out of all the block placements on the merchandising group, not just the ones from this block.
            // As such, we follow the same procedure as if there were no other block placements for this block on
            // the merchandising group.
            return AddPositionRelativeToMerchGroup(product, sequenceDirection, sequenceNumber);
        }

        /// <summary>
        /// Returns true if the block placement contains more positions than the space available in this volume.
        /// </summary>
        /// <returns></returns>
        public Boolean IsOverFilled(AxisType? axis = null)
        {
            if (_intersectingMerchSpace == null) return true;

            Dictionary<Int32, RectValue> reservedSpacesByPosition = MerchandisingGroup.PositionPlacements
                .ToDictionary(p => Convert.ToInt32(p.Id), p => p.GetReservedSpace());

            switch (MerchandisingGroup.MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.Hang:
                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    if ((!axis.HasValue || axis.Value == AxisType.X) &&
                        MerchandisingGroup.IsOverfilled(AxisType.X, PositionPlacements, null)) return true;
                    if ((!axis.HasValue || axis.Value == AxisType.Y) &&
                        MerchandisingGroup.IsOverfilled(AxisType.Y, PositionPlacements, null)) return true;
                    if ((!axis.HasValue || axis.Value == AxisType.Y) &&
                        MerchandisingGroup.IsOverfilled(AxisType.Y, PositionPlacements, null)) return true;
                    break;

                default:
                    break;
            }

            // Check that the sum of position reserved space sizes for unique combinations of sequence values
            // fits within the size of this block placement.
            RectValue space;
            if (MerchandisingGroup.HasRow1PegHoles ||
                MerchandisingGroup.HasRow2PegHoles ||
                MerchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
            {
                space = Bounds;
            }
            else
            {
                space = _intersectingMerchSpace.SpaceReducedByObstructions;
            }
            if ((!axis.HasValue || axis.Value == AxisType.X) && IsOverSpacedIn(AxisType.X, reservedSpacesByPosition, space)) return true;
            if ((!axis.HasValue || axis.Value == AxisType.Y) && IsOverSpacedIn(AxisType.Y, reservedSpacesByPosition, space)) return true;
            if ((!axis.HasValue || axis.Value == AxisType.Z) && IsOverSpacedIn(AxisType.Z, reservedSpacesByPosition, space)) return true;

            // Now, check that the difference between the min and max coords do not exceed this block's bounds.
            // Whether or not we carry out these checks depends on what type of merchandising group we're dealing with.
            // We only want to apply this check to non-even merchandising groups, because even ones will spread positions 
            // out such that they exceed this block placement's bounds.

            // We're only interested in X on stack merch groups. On Hang or Hang from bottom, Y groups may cause
            // positions with in this block placement to be placed in very different X locations.
            if (MerchandisingGroup.XAxis.Strategy != PlanogramPlacementStrategy.Even &&
                MerchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
            {
                if (IsOverfilledIn(AxisType.X, reservedSpacesByPosition)) return true;
            }

            // We're only interested in Y if the merch group is not stack, because in stack positions
            // in the same block placement may end up in very different Y locations.
            if (MerchandisingGroup.YAxis.Strategy != PlanogramPlacementStrategy.Even &&
                MerchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Stack)
            {
                if (IsOverfilledIn(AxisType.Y, reservedSpacesByPosition)) return true;
            }

            // We're only interested in Z if the merch group is not stack, because in stack positions
            // in the same block placement may end up in very different Z locations.
            if (MerchandisingGroup.ZAxis.Strategy != PlanogramPlacementStrategy.Even &&
                MerchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Stack)
            {
                if (IsOverfilledIn(AxisType.Z, reservedSpacesByPosition)) return true;
            }

            // Finally, we need to check that the positions to not exceed the merchandising space of the merchandising group.

            // First, we need the merchandising space of the merchandising group which we get by combining the merchandising
            // spaces of all the subcomponent placements in the merch group.
            PlanogramMerchandisingSpace merchGroupMerchSpace =
                MerchandisingGroup.GetMerchandisingSpace(planogramRelative: false);
            IEnumerable<RectValue> merchGroupSubCompSpaces = MerchandisingGroup.GetSubComponentPlacementBounds().ToList();

            // Next, we need to check that all the position placements lie within this merchandising space.
            foreach (PlanogramPositionPlacement positionPlacement in MerchandisingGroup.PositionPlacements)
            {
                RectValue reservedSpace = reservedSpacesByPosition[Convert.ToInt32(positionPlacement.Id)];

                // Check that the position's space is within the merchandising space of the merch group.
                if (!reservedSpace.IsInside(merchGroupMerchSpace.SpaceReducedByObstructions, axis)) return true;

                // Check that the position's space overlaps at least partially with at least one of 
                // the subcomponents in the merch group. This only applies to hang and hang from bottom, because
                // on a stack component, positions may be in contact with each other, but not the component itself.
                // The IntersectsWith method is used, because we need to check that the position and the subcomponent
                // are at least touching. In the case of components that hang products, this will always be the case
                // even if it is just two points intersecting, but not colliding.
                if (MerchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang ||
                    MerchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
                {
                    if (!merchGroupSubCompSpaces.Any(s => s.IntersectsWith(reservedSpace))) return true;
                }
            }

            // Finally, check that the merchandising group isn't overfilled.
            if (axis.HasValue)
            {
                if (MerchandisingGroup.IsOverfilled(axis.Value)) return true;
            }
            else
            {
                if (MerchandisingGroup.IsOverfilled()) return true;
            }

            return false;
        }

        /// <summary>
        ///     Determine whether this <see cref="PlanogramMerchandisingBlockPlacement"/> instance's <see cref="MerchandisingGroup"/> has any positions that are overlapping.
        /// </summary>
        /// <param name="includePlanogram">[<c>Optional</c>] Whether to include the whole planogram's positions to find overlaps. <c>True</c> by default.</param>
        /// <returns><c>True</c> if at least one placement is overlaping another. <c>False</c> otherwise.</returns>
        /// <remarks>
        /// When determining overlaps, if the <see cref="PlanogramSubComponentXMerchStrategyType"/> is 
        /// <see cref="PlanogramSubComponentXMerchStrategyType.Manual"/> and the user set the group's 
        /// <see cref="PlanogramMerchandisingGroup.IsProductOverlapAllowed"/> to true this method will return <c>false</c>.
        /// </remarks>
        private Boolean HasOverlaps(Boolean includePlanogram = true)
        {
            return includePlanogram
                       ? AnyPositionsOverlap()
                       : MerchandisingGroup.HasCollisionOverlapping(
                           MerchandisingGroup.PositionPlacements.ToDictionary(p => Convert.ToInt32(p.Id), p => p.GetReservedSpace()).Values);
        }

        /// <summary>
        ///     Check whether local positions collide among themselves or with other positions outside the block's Merchgroup.
        /// </summary>
        /// <returns></returns>
        private Boolean AnyPositionsOverlap()
        {
            //  Get the bounding boxes for all positions local to the block placement
            //  (this includes any merch group that the block targets)
            //  and check for overlaps.
            PlanogramMerchandisingBlock affectedBlock = Parent;
            IEnumerable<PlanogramMerchandisingGroup> affectedMerchGroups =
                affectedBlock.BlockPlacements.Select(blockPlacement => blockPlacement.MerchandisingGroup).Distinct().ToList();
            IEnumerable<RectValue> rectValueSet =
                affectedMerchGroups.SelectMany(
                    merchGroup => merchGroup.PositionPlacements.Select(positionPlacement => positionPlacement.GetPlanogramRelativeBoundingBox()))
                                   .ToList();

            if (rectValueSet.HasCollisionOverlaps()) return true;

            //  Even if there are no overlaps within the block's merchgroups,
            //  get the bounding boxes for all other positions outside the block placement
            //  (that is, those that are on merch groups other than the block's targets)
            //  and check for overlaps with the local positions.
            IEnumerable<PlanogramMerchandisingBlock> otherBlocks = affectedBlock.Parent.Where(block => !block.Equals(affectedBlock));
            IEnumerable<PlanogramMerchandisingGroup> otherMerchGroups =
                otherBlocks.SelectMany(block => block.BlockPlacements)
                           .Select(blockPlacement => blockPlacement.MerchandisingGroup)
                           .Distinct()
                           .Where(merchGroup => !affectedMerchGroups.Contains(merchGroup));
            IEnumerable<RectValue> otherRectValueSet =
                otherMerchGroups.SelectMany(
                    merchGroup => merchGroup.PositionPlacements.Select(positionPlacement => positionPlacement.GetPlanogramRelativeBoundingBox()));

            Boolean anyPositionsOverlap = otherRectValueSet.Any(rectValue => rectValueSet.Any(rectValue.CollidesWith));
            return anyPositionsOverlap;
        }

        /// <summary>
        /// Gets the next (primary, secondary, tertiary) sequence direction from this group based on the given sequence direction,
        /// or null if the given sequence direction is tertiary or unknown.
        /// </summary>
        /// <param name="sequenceDirection"></param>
        /// <returns></returns>
        public PlanogramBlockingGroupPlacementType? GetNextSequenceDirection(PlanogramBlockingGroupPlacementType? sequenceDirection)
        {
            if (sequenceDirection == PrimarySequenceDirection) return SecondarySequenceDirection;
            if (sequenceDirection == SecondarySequenceDirection) return TertiarySequenceDirection;
            if (sequenceDirection == TertiarySequenceDirection) return null;
            if (sequenceDirection == null) return null;
            throw new ArgumentException("sequenceDirection was not found on this block placement");
        }

        /// <summary>
        /// Removes the last position in the position placements list from the block placement and the
        /// Merchandising Group.
        /// </summary>
        public void RemoveLastPosition()
        {
            PlanogramPositionPlacement lastPosition = _positionPlacements.Last();
            MerchandisingGroup.RemovePosition(lastPosition.Position);
            _positionPlacements.Remove(lastPosition);
        }
        
        /// <summary>
        /// Removes all <see cref="PlanogramPositionPlacement"/>s from <see cref="PositionPlacements"/> and from the
        /// <see cref="MerchandisingGroup"/>.
        /// </summary>
        public void ClearPositions()
        {
            foreach (PlanogramPositionPlacement positionPlacement in _positionPlacements)
            {
                MerchandisingGroup.RemovePositionPlacement(positionPlacement);
            }
            _positionPlacements.Clear();
        }

        #region Overfill/Overspace Methods
        /// <summary>
        /// Checks the difference between the min and max co-ordinates of <see cref="PositionPlacements"/> within <paramref name="reservedSpacesByPosition"/>
        /// and compares this size to the available size in <see cref="Bounds"/> along the direction indicated by <paramref name="axisType"/>.
        /// </summary>
        /// <param name="axisType">The direction to compare sizes in.</param>
        /// <param name="reservedSpacesByPosition">A dictionary containing the sizes of the <see cref="PositionPlacements"/>.</param>
        /// <returns>True if more space exists between the min and max co-ordinates than is available in <see cref="Bounds"/>.</returns>
        private Boolean IsOverfilledIn(AxisType axisType, Dictionary<Int32, RectValue> reservedSpacesByPosition)
        {
            if (!reservedSpacesByPosition.Any()) return false;

            List<RectValue> positionPlacementReservedSpaces = new List<RectValue>();
            foreach (PlanogramPositionPlacement p in PositionPlacements)
            {
                RectValue reservedSpace;
                if (!reservedSpacesByPosition.TryGetValue(Convert.ToInt32(p.Id), out reservedSpace)) continue;
                positionPlacementReservedSpaces.Add(reservedSpace);
            }
            if (!positionPlacementReservedSpaces.Any()) return false;

            Single minCoord = positionPlacementReservedSpaces.Min(r => r.GetCoordinate(axisType));
            Single maxCoord = positionPlacementReservedSpaces.Max(r => r.GetCoordinate(axisType) + r.GetSize(axisType));
            Single maxSeparation = Math.Abs(maxCoord - minCoord);
            return maxSeparation.GreaterThan(ReduceByFacingSpaceReduction(axisType, Bounds));
        }

        /// <summary>
        /// Checks the sum of the sizes of the <see cref="PositionPlacements"/> and compares it to the size available
        /// in <see cref="Bounds"/>.
        /// </summary>
        /// <param name="axis">The direction to compare sizes in.</param>
        /// <param name="reservedSpacesByPosition">A dictionary containing the sizes of the <see cref="PositionPlacements"/>.</param>
        /// <returns>
        /// True if the sum of sizes in the given direction for a unique combination of sequence values
        /// exceeds the size of <see cref="Bounds"/>.
        /// </returns>
        private Boolean IsOverSpacedIn(AxisType axis, Dictionary<Int32, RectValue> reservedSpacesByPosition, RectValue space)
        {
            AxisType firstNormal = axis.GetNormals().First();
            AxisType secondNormal = axis.GetNormals().Last();

            foreach (Int16 firstNormalSequence in PositionPlacements.Select(p => p.Position.GetSequence(firstNormal)).Distinct())
            {
                foreach (Int16 secondNormalSequence in PositionPlacements.Where(p => p.Position.GetSequence(firstNormal) == firstNormalSequence).Select(p => p.Position.GetSequence(secondNormal)).Distinct())
                {
                    IEnumerable<PlanogramPosition> matchingPositions = PositionPlacements
                        .Where(p =>
                            p.Position.GetSequence(firstNormal) == firstNormalSequence &&
                            p.Position.GetSequence(secondNormal) == secondNormalSequence)
                        .Select(p => p.Position);
                    List<RectValue> reservedSpaces = new List<RectValue>();
                    foreach (PlanogramPosition matchingPosition in matchingPositions)
                    {
                        RectValue reservedSpace;
                        if (!reservedSpacesByPosition.TryGetValue(Convert.ToInt32(matchingPosition.Id), out reservedSpace)) continue;
                        reservedSpaces.Add(reservedSpace);
                    }

                    Single sumOfSizes = reservedSpaces.Sum(r => r.GetSize(axis));
                    if (sumOfSizes.GreaterThan(ReduceByFacingSpaceReduction(axis, space))) return true;
                }
            }

            return false;
        }
 
        /// <summary>
        /// Reduces the given <paramref name="space"/> by the <see cref="Parent"/>'s <see cref="FacingSpaceReductionPercentage"/>
        /// and returns the amended space in the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="space"></param>
        /// <returns></returns>
        private Single ReduceByFacingSpaceReduction(AxisType axis, RectValue space)
        {
            AxisType facingAxis = MerchandisingGroup.SubComponentPlacements.First().GetFacingAxis();
            Single percentageFactor = 1;
            if (axis == facingAxis)
            {
                percentageFactor -= FacingSpaceReductionPercentage;
            }

            return space.GetSize(axis) * percentageFactor;
        }

        /// <summary>
        /// Gets the actual size of the block placement with the facing reduction factor
        /// </summary>
        /// <param name="facingSpaceReductionPercentage">optional param so that new reduction percentage can be evaluated</param>
        /// <returns></returns>
        public WidthHeightDepthValue GetSizeReducedByFacingSpacePercentage(Single? facingSpaceReductionPercentage = null)
        {
            AxisType facingAxis = MerchandisingGroup.SubComponentPlacements.First().GetFacingAxis();
            Single percentageFactor = facingSpaceReductionPercentage == null ? (1 - FacingSpaceReductionPercentage) : (1 - (Single)facingSpaceReductionPercentage);
            return new WidthHeightDepthValue(
                facingAxis == AxisType.X ? Size.Width * percentageFactor : Size.Width,
                facingAxis == AxisType.Y ? Size.Height * percentageFactor : Size.Height,
                facingAxis == AxisType.Z ? Size.Depth * percentageFactor : Size.Depth);
        }
        #endregion

        #region Add Position Methods

        /// <summary>
        /// Creates a position for this product and adds it to the merchandising group with sequence values of 1.
        /// </summary>
        /// <param name="product">The product to add.</param>
        /// <param name="placementType">Used to determine the if a placement is valid on the target subcomponent.</param>
        /// <param name="sequenceNumber"></param>
        /// <param name="position"></param>
        /// <returns>True if placement was a success.</returns>
        private Boolean TryAddAsFirstPosition(
            PlanogramProduct product,
            PlanogramBlockingGroupPlacementType placementType,
            Int32 sequenceNumber,
            out PlanogramPositionPlacement position)
        {
            position = null;
            if (MerchandisingGroup.PositionPlacements.Any()) return false;

            // If the merch group has no placements, it's safe to just create a placement with sequence values of 1.
            // Determine the subcomponent placement from the merchandising group based on the sequence direction.
            IEnumerable<PlanogramSubComponentPlacement> placements = MerchandisingGroup.SubComponentPlacements;
            PlanogramSubComponentPlacement placement = GetXSequenceDirection().IsPositive()
                                                           ? placements.Last()
                                                           : placements.First();

            // If the sequence direction isn't valid for our subcomponent, don't place a position.
            if (IsSequenceDirectionInvalid(placementType, placement.SubComponent)) return false;

            // Add a position with default values (sequence values of 1).
            position = MerchandisingGroup.InsertPositionPlacement(
                PlanogramPosition.NewPlanogramPosition(0, product, placement, sequenceNumber, Parent.Colour),
                product);
            _positionPlacements.Add(position);

            return true;
        }

        /// <summary>
        /// Creates and adds a position for the given product relative to the other positions in the merchandising group.
        /// This method is used to work out where the first position in a new block should go in relation to the surrounding blocks.
        /// </summary>
        /// <param name="product">The product to add.</param>
        /// <param name="sequenceDirection">The sequence direction to add in.</param>
        /// <returns>The <see cref="PlanogramPositionPlacement"/> that was added, or null if none could be added.</returns>
        /// <remarks>
        /// This method covers the situation where the <see cref="PlanogramMerchandisingBlockPlacement"/> we are adding to
        /// doesn't have any <see cref="PositionPlacements"/>, but its <see cref="MerchandisingGroup"/> does. In this case,
        /// there are already positions on the <see cref="MerchandisingGroup"/>, but none have yet been placed in this
        /// <see cref="PlanogramMerchandisingBlockPlacement"/>.
        /// <para></para>
        /// In this situation, we need to find:
        /// <list type="number">
        /// <item>The <see cref="PlanogramMerchandisingBlockPlacement"/> to place relative to.</item>
        /// <item>The <see cref="PlanogramPositionPlacement"/> within this block placement to place relative to.</item>
        /// </list>
        /// Finding the most appropriate <see cref="PlanogramMerchandisingBlockPlacement"/> depends upon the direction
        /// we are placing products in (<paramref name="sequenceDirection"/>) and whether or not any other block placements
        /// share the same co-ordinates as this one.
        /// <para></para>
        /// Once the block placement has been identified, the most appropriate <see cref="PlanogramPositionPlacement"/> is
        /// easily identifiable from the sequence directions of this <see cref="PlanogramMerchandisingBlockPlacement"/>.
        /// </remarks>
        private PlanogramPositionPlacement AddPositionRelativeToMerchGroup(
            PlanogramProduct product,
            PlanogramBlockingGroupPlacementType sequenceDirection,
            Int32 sequenceNumber)
        {
            //  There should be no placements on this block placement. 
            //  If there are, it means no more choices to place on this one.
            if (PositionPlacements.Any()) return null;

            // First, we need the block placements that share this Merchandising Group that have positions. If there aren't 
            // any then this method can't continue.
            ICollection<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions =
                GetMerchandisingGroupBlockPlacementsInRootBlocking().Where(p => p.PositionPlacements.Any()).ToList();
            if (!blockPlacementsWithPositions.Any()) return null;

            List<PlanogramBlockingGroupPlacementType> sequenceDirectionsToCheck = GetSequenceDirectionsToCheck(sequenceDirection);
            PlanogramBlockingGroupPlacementType originalSequenceDirection = sequenceDirection;

            Boolean isBlockPlacementAfter = false;
            PlanogramPositionPlacement precedingPosition = null;

            foreach (PlanogramBlockingGroupPlacementType sequence in sequenceDirectionsToCheck)
            {
                ICollection<PlanogramMerchandisingBlockPlacement> matchingPlacements =
                    GetSurroundingBlockPlacementsThatIntersectFirstAndSecondNormals(blockPlacementsWithPositions, sequence).ToList();
                if (matchingPlacements.Count == 0) continue;

                if (sequence == sequenceDirectionsToCheck[0])
                {
                    // filter to find block placements that surround this block
                    ICollection<PlanogramMerchandisingBlockPlacement> adjacentBlockPlacements =
                        GetAdjacentBlockPlacements(matchingPlacements, sequence.GetAxis());

                    // Gets list of positions that exist on valid sequence lines (positions can be considered if no other block placements existon the sequence line,  
                    //                                                          if it does we may make an incorrect decision on where to place this product)
                    List<PlanogramPositionPlacement> validSequencePlacements = GetAllValidSequenceLinePositions(blockPlacementsWithPositions,
                                                                                                                adjacentBlockPlacements,
                                                                                                                sequence,
                                                                                                                sequenceDirectionsToCheck[1],
                                                                                                                sequenceDirectionsToCheck[2]);

                    if (!validSequencePlacements.Any()) continue;

                    // If we have positions we can place against we must select the correct one according to our sequence. 
                    // Once we have found the correct sequence line position we need to find if this sequence line appears before / after 
                    // in the sequence and then fianlly we can pick the correct position to place against.
                    precedingPosition = GetSequenceLineAnchorPosition(blockPlacementsWithPositions,
                                                                      validSequencePlacements,
                                                                      sequence,
                                                                      out isBlockPlacementAfter);
                    sequenceDirection = isBlockPlacementAfter ? sequence.ToNegative() : sequence.ToPositive();
                    break;
                }

                PlanogramMerchandisingBlockPlacement blockPlacementToUse;
                GetBlockPlacementBeforeOrAfter(out blockPlacementToUse, out isBlockPlacementAfter, sequence, matchingPlacements);
                sequenceDirection = isBlockPlacementAfter ? sequence.ToNegative() : sequence.ToPositive();

                // If we couldn't get one before or after in the sequence direction, return null, because we need to try
                // again in another direction.
                if (blockPlacementToUse == null) continue;

                // Work out the sequence values of the most appropriate product within the block placement that we have selected.
                Int16 precedingSequenceX;
                Int16 precedingSequenceY;
                Int16 precedingSequenceZ;
                CalculatePrecedingSequences(
                    out precedingSequenceX,
                    out precedingSequenceY,
                    out precedingSequenceZ,
                    sequenceDirection,
                    blockPlacementToUse,
                    isBlockPlacementAfter);

                // Find the position at the sequence values.
                precedingPosition = blockPlacementToUse.PositionPlacements
                                                       .FirstOrDefault(p =>
                                                                       p.Position.SequenceX == precedingSequenceX &&
                                                                       p.Position.SequenceY == precedingSequenceY &&
                                                                       p.Position.SequenceZ == precedingSequenceZ);
                break;
            }

            // If we couldn't find an anchor or the sequence direction isn't valid on the anchor's sub component, then we can't continue.
            if (precedingPosition == null ||
                IsSequenceDirectionInvalid(originalSequenceDirection, precedingPosition.SubComponentPlacement.SubComponent)) return null;

            // Finally, insert a new position placement into the merchandising group using the preceding position as an anchor.
            PlanogramPosition newPosition = PlanogramPosition.NewPlanogramPosition(0,
                                                                                   product,
                                                                                   precedingPosition.SubComponentPlacement,
                                                                                   sequenceNumber,
                                                                                   this.Parent.Colour);
            PlanogramPositionAnchorDirection anchorDirection = GetAnchorDirection(
                sequenceDirection,
                sequenceDirection.IsPositive() ? isBlockPlacementAfter : !isBlockPlacementAfter);
            PlanogramPositionPlacement newPositionPlacement = MerchandisingGroup
                .InsertPositionPlacement(newPosition, product, precedingPosition, anchorDirection);

            _positionPlacements.Add(newPositionPlacement);

            DebugLogFirstProductInBlogPlacementDecision(anchorDirection, product.Gtin, precedingPosition.Product.Gtin);

            return newPositionPlacement;
        }

        private List<PlanogramBlockingGroupPlacementType> GetSequenceDirectionsToCheck(PlanogramBlockingGroupPlacementType sequenceDirection)
        {
            PlanogramSubComponentMerchandisingType type = MerchandisingGroup.SubComponentPlacements.First().SubComponent.MerchandisingType;
            switch (type)
            {
                case PlanogramSubComponentMerchandisingType.Hang:
                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return new List<PlanogramBlockingGroupPlacementType>
                           {
                               GetSequenceDirection(AxisType.X),
                               GetSequenceDirection(AxisType.Z),
                               GetSequenceDirection(AxisType.Y)
                           };
                case PlanogramSubComponentMerchandisingType.Stack:
                    return new List<PlanogramBlockingGroupPlacementType>
                           {
                               GetSequenceDirection(AxisType.Z),
                               GetSequenceDirection(AxisType.Y),
                               GetSequenceDirection(AxisType.X)
                           };
                case PlanogramSubComponentMerchandisingType.None:
                default:
                    return new List<PlanogramBlockingGroupPlacementType>
                           {
                               sequenceDirection,
                               GetNextSequenceDirection(sequenceDirection).Value,
                               GetNextSequenceDirection(GetNextSequenceDirection(sequenceDirection)).Value
                           };
            }
        }

        /// <summary>
        /// Gets all valid positions that form a sequnce line from our target blocks. A valid sequence line position is a sequence line that doesnt contain another block placement position 
        /// that intersects with the target block placement in the sequence directions first and second normal directions. If a positions block placement that is contained within a sequence line 
        /// and itersects in the first second normals then this would make a better choice for anchor if no other valid sequence lines can be found. 
        /// </summary>
        /// <param name="blockPlacementsWithPositions"></param>
        /// <param name="targetPlacementsWithPositions"></param>
        /// <param name="sequenceLineDirection"></param>
        /// <param name="sequenceLineFirstNormal"></param>
        /// <param name="sequenceLineSecondNormal"></param>
        /// <returns></returns>
        private List<PlanogramPositionPlacement> GetAllValidSequenceLinePositions(IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions, IEnumerable<PlanogramMerchandisingBlockPlacement> targetPlacementsWithPositions, PlanogramBlockingGroupPlacementType sequenceLineDirection, PlanogramBlockingGroupPlacementType sequenceLineFirstNormal, PlanogramBlockingGroupPlacementType sequenceLineSecondNormal)
        {
            IEnumerable<PlanogramPositionPlacement> targetPositionPlacements = targetPlacementsWithPositions.SelectMany(p => p.PositionPlacements);
            IEnumerable<Int32?> sequenceColourToExclude = targetPositionPlacements.Select(p => p.Position.SequenceColour).Distinct();
            List<PlanogramPositionPlacement> validSequencePlacements = new List<PlanogramPositionPlacement>();
            List<PlanogramMerchandisingBlockPlacement> validBlockPlacements = new List<PlanogramMerchandisingBlockPlacement>();
            AxisType axisType = sequenceLineDirection.GetAxis();

            // check each of the sequence lines positions
            foreach (Int16 sequenceLine in GetSequenceLines(targetPositionPlacements, axisType))
            {
                Boolean validSequenceLine = true;
                // build up list of possible sequence lines that dont contain contaminated lines and appear within the bounds of our block placement
                foreach (PlanogramPositionPlacement sequenceLinePlacement in GetSequenceLinePositionsFromBlockPlacements(blockPlacementsWithPositions, sequenceLine, axisType))
                {
                    IEnumerable<PlanogramMerchandisingBlockPlacement> positionsBlockPlacement = blockPlacementsWithPositions.Where(p => p.Parent.Colour == sequenceLinePlacement.Position.SequenceColour);

                    if (!sequenceColourToExclude.Contains(sequenceLinePlacement.Position.SequenceColour))
                    {
                        // validate if this position isnt going to cause us a problem
                        IEnumerable<PlanogramMerchandisingBlockPlacement> sequenceLinePositionCheckNormals = GetSurroundingBlockPlacementsThatIntersectFirstAndSecondNormals(positionsBlockPlacement, sequenceLineFirstNormal);
                        if (!sequenceLinePositionCheckNormals.Any())
                        {
                            sequenceLinePositionCheckNormals = GetSurroundingBlockPlacementsThatIntersectFirstAndSecondNormals(positionsBlockPlacement, sequenceLineSecondNormal);
                        }

                        validSequenceLine = !sequenceLinePositionCheckNormals.Any();
                        if (!validSequenceLine) break;
                    }

                    validBlockPlacements.AddRange(positionsBlockPlacement);
                }

                if (validSequenceLine)
                {
                    // we only consider positions from the target placements that make up the sequence line
                    validSequencePlacements.AddRange(GetSequenceLinePositionsFromBlockPlacements(targetPlacementsWithPositions, sequenceLine, axisType));
                }
            }

            return validSequencePlacements;
        }

        /// <summary>
        /// Gets all positons from block placements that form the sequence line 
        /// (Sequence direction X, sequence line Y)
        /// (Sequence direction Z, sequence line X)
        /// </summary>
        /// <param name="blockPlacementsWithPositions"></param>
        /// <param name="sequenceLine"></param>
        /// <param name="sequenceLineDirection"></param>
        /// <returns></returns>
        private static IEnumerable<PlanogramPositionPlacement> GetSequenceLinePositionsFromBlockPlacements(IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions, Int16 sequenceLine, AxisType axisType)
        {
            switch (axisType)
            {
                case AxisType.X:
                    return blockPlacementsWithPositions.SelectMany(p => p.PositionPlacements).Where(p => p.Position.SequenceY == sequenceLine);
                case AxisType.Z:
                    return blockPlacementsWithPositions.SelectMany(p => p.PositionPlacements).Where(p => p.Position.SequenceX == sequenceLine);
                default:
                    break;
            }

            return blockPlacementsWithPositions.SelectMany(p => p.PositionPlacements).Where(p => p.Position.SequenceX == sequenceLine);
        }

        /// <summary>
        /// Gets all available sequence lines for the sequence direction
        /// </summary>
        /// <param name="targetPositionPlacements"></param>
        /// <param name="sequenceLineDirection"></param>
        /// <returns></returns>
        private static IEnumerable<Int16> GetSequenceLines(IEnumerable<PlanogramPositionPlacement> targetPositionPlacements, AxisType axisType)
        {
            switch (axisType)
            {
                case AxisType.X:
                    return targetPositionPlacements.Select(p => p.Position.SequenceY).Distinct();
                case AxisType.Z:
                    return targetPositionPlacements.Select(p => p.Position.SequenceX).Distinct();
                default:
                    break;
            }

            return targetPositionPlacements.Select(p => p.Position.SequenceX).Distinct();
        }

        /// <summary>
        /// Gets the anchor position for the available sequence positions.
        /// Firstly we need to find the correct sequence line position according to our sequence
        /// Then find if this positions block placement appears before / after 
        /// Then finally we can select the correct position on the sequence line according to whether its before \ after.
        /// </summary>
        /// <param name="blockPlacementsWithPositions"></param>
        /// <param name="validSequencePlacements"></param>
        /// <param name="sequence"></param>
        /// <param name="isBlockPlacementAfter"></param>
        /// <returns></returns>
        private PlanogramPositionPlacement GetSequenceLineAnchorPosition(ICollection<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions, IEnumerable<PlanogramPositionPlacement> validSequencePlacements, PlanogramBlockingGroupPlacementType sequence, out Boolean isBlockPlacementAfter)
        {
            PlanogramPositionPlacement sequenceLinePosition = FindClosestPlacementBySequenceDirection(validSequencePlacements, PrimarySequenceDirection, SecondarySequenceDirection, TertiarySequenceDirection);

            // find if this block is before \ after
            PlanogramMerchandisingBlockPlacement blockPlacementToUse;
            Int32? sequenceColour = sequenceLinePosition.Position.SequenceColour;
            GetBlockPlacementBeforeOrAfter(out blockPlacementToUse,
                                           out isBlockPlacementAfter,
                                           sequence,
                                           blockPlacementsWithPositions.Where(p => p.Parent.Colour == sequenceColour));

            // From this we can determine if its the first last in the sequence line we take as the anchor.
            IEnumerable<PlanogramPositionPlacement> placements;
            switch (sequence.GetAxis())
            {
                case AxisType.X:
                    placements = GetSequenceLinePositionsFromBlockPlacements(blockPlacementsWithPositions, sequenceLinePosition.Position.SequenceY, AxisType.X).Where(p => p.Position.SequenceColour == sequenceColour);
                    sequenceLinePosition = isBlockPlacementAfter
                                               ? placements.OrderBy(p => p.Position.SequenceX).FirstOrDefault()
                                               : placements.OrderByDescending(p => p.Position.SequenceX).FirstOrDefault();
                    break;
                case AxisType.Z:
                    // If we are placing behind or in front on Z, 
                    //  we need to find a position that is on the correct side of the primary axis 
                    //  as we are aligned with the preceeding or following block.
                    // This means picking the position at the other side on the Primary sequence direction 
                    //  (so we pass it inverted for the calculation).
                    sequenceLinePosition = FindClosestPlacementBySequenceDirection(
                        blockPlacementToUse.PositionPlacements,
                        PrimarySequenceDirection.Invert(true),
                        SecondarySequenceDirection,
                        TertiarySequenceDirection);
                    placements = GetSequenceLinePositionsFromBlockPlacements(blockPlacementsWithPositions, sequenceLinePosition.Position.SequenceX, AxisType.Z).Where(p => p.Position.SequenceColour == sequenceColour);
                    sequenceLinePosition = isBlockPlacementAfter
                                               ? placements.OrderBy(p => p.Position.SequenceZ).FirstOrDefault()
                                               : placements.OrderByDescending(p => p.Position.SequenceZ).FirstOrDefault();
                    break;
                default:
                    break;
            }
            return sequenceLinePosition;
        }

        private PlanogramPositionPlacement FindClosestPlacementBySequenceDirection(IEnumerable<PlanogramPositionPlacement> validSequencePlacements, PlanogramBlockingGroupPlacementType primarySequenceDirection, PlanogramBlockingGroupPlacementType secondarySequenceDirection, PlanogramBlockingGroupPlacementType tertiarySequenceDirection)
        {
            // Find the placements that are closer to the tertiary direction.
            IGrouping<Int16, PlanogramPositionPlacement> placementsClosestInTertiaryDirection =
                GetPlanogramPositionPlacementSequenceGroupsAndDirection(
                    validSequencePlacements,
                    tertiarySequenceDirection.GetAxis(),
                    tertiarySequenceDirection.IsPositive()).First();

            IGrouping<Int16, PlanogramPositionPlacement> placementsClosestInSecondaryDirection =
                GetPlanogramPositionPlacementSequenceGroupsAndDirection(
                    GetSequencePlacements(placementsClosestInTertiaryDirection, tertiarySequenceDirection.GetAxis()),
                    secondarySequenceDirection.GetAxis(),
                    secondarySequenceDirection.IsPositive()).First();

            PlanogramPositionPlacement sequenceLinePosition =
                GetPlanogramPositionPlacementSequenceGroupsAndDirection(
                    GetSequencePlacements(placementsClosestInSecondaryDirection, secondarySequenceDirection.GetAxis()),
                    primarySequenceDirection.GetAxis(),
                    primarySequenceDirection.IsPositive()).First().First();
            return sequenceLinePosition;
        }

        /// <summary>
        /// Gets the first sequence placements for the sequence groupings according to axis
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="axisType"></param>
        /// <returns></returns>
        private IEnumerable<PlanogramPositionPlacement> GetSequencePlacements(IGrouping<Int16, PlanogramPositionPlacement> sequence, AxisType axisType)
        {
            switch (axisType)
            {
                case AxisType.X:
                    return sequence.Where(p => p.Position.SequenceX == sequence.Key);
                case AxisType.Y:
                    return sequence.Where(p => p.Position.SequenceY == sequence.Key);
                case AxisType.Z:
                    return sequence.Where(p => p.Position.SequenceZ == sequence.Key);
                default:
                    break;
            }

            return sequence.Where(p => p.Position.SequenceX == sequence.Key);
        }

        /// <summary>
        /// Gets the axis directionsequence groups and direction
        /// </summary>
        /// <param name="validSequencePlacements"></param>
        /// <param name="axisType"></param>
        /// <param name="isPositive"></param>
        /// <returns></returns>
        private static IEnumerable<IGrouping<Int16, PlanogramPositionPlacement>> GetPlanogramPositionPlacementSequenceGroupsAndDirection(
            IEnumerable<PlanogramPositionPlacement> validSequencePlacements,
            AxisType axisType,
            Boolean isPositive)
        {
            IEnumerable<IGrouping<Int16, PlanogramPositionPlacement>> positionsInSequence = 
                validSequencePlacements.GroupBy(p => p.Position.GetSequence(axisType));
            switch (axisType)
            {
                case AxisType.X:
                    return isPositive
                               ? positionsInSequence.OrderByDescending(p => p.Key)
                               : positionsInSequence.OrderBy(p => p.Key);
                case AxisType.Y:
                case AxisType.Z:
                    return isPositive
                               ? positionsInSequence.OrderBy(p => p.Key)
                               : positionsInSequence.OrderByDescending(p => p.Key);
                default:
                    break;
            }

            return null;
        }

        /// <summary>
        /// Gets all block placements that intersect in the first and second normal directions of the given sequence
        /// </summary>
        /// <param name="blockPlacementsWithPositions"></param>
        /// <param name="sequenceDirection"></param>
        /// <returns></returns>
        private IEnumerable<PlanogramMerchandisingBlockPlacement> GetSurroundingBlockPlacementsThatIntersectFirstAndSecondNormals(
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions, 
            PlanogramBlockingGroupPlacementType sequenceDirection)
        {
            // We need to check for overlaps in the two directions normal to the sequence direction
            // and for adjacency in the direction of the sequence direction.
            AxisType axis = sequenceDirection.GetAxis();
            AxisType firstNormal = sequenceDirection.GetAxis().GetNormals().ElementAt(0);
            AxisType secondNormal = sequenceDirection.GetAxis().GetNormals().ElementAt(1);

            foreach(PlanogramMerchandisingBlockPlacement blockPlacement in blockPlacementsWithPositions)
            {
                Boolean overlapsInFirstNormal = GetOriginalBoundsOverlapIn(firstNormal, blockPlacement);
                Boolean overlapsInSecondNormal = GetOriginalBoundsOverlapIn(secondNormal, blockPlacement);
                Boolean isAdjacentInDirection = IsAdjacentInDirection(axis, this, blockPlacement);

                if (overlapsInFirstNormal && overlapsInSecondNormal && isAdjacentInDirection)
                {
                    yield return blockPlacement;
                }
            }

            yield break;
        }

        /// <summary>
        /// Determines if the block placements are adjacent based upon whether or not the share the same starting or ending co-ordinates,
        /// if they are both position space placements. If one of them is a block space placement, then they are considered adjacent
        /// as long as the block space placement's facing axis is the same as <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="firstBlockPlacement"></param>
        /// <returns></returns>
        private static Boolean IsAdjacentInDirection(
            AxisType axis, 
            PlanogramMerchandisingBlockPlacement firstBlockPlacement,
            PlanogramMerchandisingBlockPlacement secondBlockPlacement)
        {
            Boolean isStartingCoordEqual = secondBlockPlacement.Bounds.GetCoordinate(axis).EqualTo(firstBlockPlacement.Bounds.GetCoordinate(axis));
            Boolean isEndingCoordEqual = 
                (secondBlockPlacement.Bounds.GetCoordinate(axis) + secondBlockPlacement.Bounds.GetSize(axis))
                .EqualTo(firstBlockPlacement.Bounds.GetCoordinate(axis) + firstBlockPlacement.Bounds.GetSize(axis));

            Boolean isAdjacentInDirection = !(isStartingCoordEqual || isEndingCoordEqual);

            // We consider the block placements adjacent if they don't share the same starting or ending 
            // co-ordinates, or if one of their facing axes is the same as this axis.
            return 
                isAdjacentInDirection || 
                firstBlockPlacement.MerchandisingGroup.GetFacingAxis().Equals(axis) ||
                secondBlockPlacement.MerchandisingGroup.GetFacingAxis().Equals(axis);

        }

        private Boolean GetOriginalBoundsOverlapIn(AxisType axis, PlanogramMerchandisingBlockPlacement blockPlacement)
        {
            return 
                Math.Max(
                    blockPlacement.OriginalBounds.GetCoordinate(axis),
                    OriginalBounds.GetCoordinate(axis))
                .LessThan(
                Math.Min(
                    blockPlacement.OriginalBounds.GetCoordinate(axis) + blockPlacement.OriginalBounds.GetSize(axis),
                    OriginalBounds.GetCoordinate(axis) + OriginalBounds.GetSize(axis)));
        }

        /// <summary>
        /// Filters block placements to only block placemenets that appear directly before \ after current block
        /// </summary>
        /// <param name="candidates">The collection of <see cref="PlanogramMerchandisingBlockPlacement"/> to find adjacent ones from.</param>
        /// <param name="axis">The axis to determine adjacency. Only this axis will be considered for adjacency 
        /// (that is, any placements must somewhat overlap with this instance's block placement on this axis.</param>
        /// <returns>A collection of <see cref="PlanogramMerchandisingBlockPlacement"/> originally present in the <paramref name="candidates"/> collection
        /// that are adjacent to this instance's Block Placement.</returns>
        private ICollection<PlanogramMerchandisingBlockPlacement> GetAdjacentBlockPlacements(
            ICollection<PlanogramMerchandisingBlockPlacement> candidates, AxisType axis)
        {
            List<PlanogramMerchandisingBlockPlacement> blockPlacementsBefore =
                candidates.Where(p => p.OriginalBounds.GetCoordinate(axis).LessThan(OriginalBounds.GetCoordinate(axis))).ToList();

            List<PlanogramMerchandisingBlockPlacement> blockPlacementsAfter =
                candidates.Where(p => p.OriginalBounds.GetCoordinate(axis).GreaterThan(OriginalBounds.GetCoordinate(axis))).ToList();

            List<PlanogramMerchandisingBlockPlacement> blockPlacements =
                blockPlacementsBefore
                    .Where(placement =>
                           blockPlacementsBefore.Where(p => !p.Equals(placement))
                                                .All(
                                                    other =>
                                                    !HaveOcclusionForAxis(axis, placement, other) ||
                                                    placement.OriginalBounds.GetCenterPoint()
                                                             .GetCoordinate(axis)
                                                             .GreaterOrEqualThan(other.OriginalBounds.GetCenterPoint().GetCoordinate(axis))))
                    .Union(blockPlacementsAfter.Where(
                        placement =>
                        blockPlacementsAfter.Where(p => !p.Equals(placement))
                                             .All(
                                                 other =>
                                                 !HaveOcclusionForAxis(axis, placement, other) ||
                                                 placement.OriginalBounds.GetCenterPoint()
                                                          .GetCoordinate(axis)
                                                          .LessOrEqualThan(
                                                              other.OriginalBounds.GetCenterPoint().GetCoordinate(axis)))))
                    .ToList();

            return blockPlacements;
        }

        /// <summary>
        ///     Check whether <paramref name="placementA"/> and <paramref name="placementB"/> occlude
        ///     for the provided axis (that is, the would intersect for at least one value for that axis).
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="placementA"></param>
        /// <param name="placementB"></param>
        /// <returns></returns>
        private static Boolean HaveOcclusionForAxis(
            AxisType axis,
            PlanogramMerchandisingBlockPlacement placementA,
            PlanogramMerchandisingBlockPlacement placementB)
        {
            //  If the placements intersct coordinates in both normal axes then they occlude in the axis.
            return axis.GetNormals().All(normalAxis =>
                                         {
                                             Single minA = placementA.OriginalBounds.GetCoordinate(normalAxis);
                                             Single maxA = minA + placementA.OriginalBounds.GetSize(normalAxis);
                                             Single minB = placementB.OriginalBounds.GetCoordinate(normalAxis);
                                             Single maxB = minB + placementB.OriginalBounds.GetSize(normalAxis);
                                             return minA.GreaterOrEqualThan(minB) && minA.LessOrEqualThan(maxB) ||
                                                    minB.GreaterOrEqualThan(minA) && minB.LessOrEqualThan(maxA);
                                         });
        }

        private Tuple<PlanogramBlockingGroupPlacementType, PlanogramBlockingGroupPlacementType> GetSequenceDirectionsInOrderOtherThan(
            PlanogramBlockingGroupPlacementType sequenceDirection)
        {
            PlanogramBlockingGroupPlacementType placementDirectionFromSequenceDirection;
            if (sequenceDirection.IsX())
            {
                placementDirectionFromSequenceDirection = GetXSequenceDirection();
            }
            else if (sequenceDirection.IsY())
            {
                placementDirectionFromSequenceDirection = GetYSequenceDirection();
            }
            else if (sequenceDirection.IsZ())
            {
                placementDirectionFromSequenceDirection = GetZSequenceDirection();
            }
            else
            {
                Debug.Fail("sequenceDirection was not X, Y or Z");
                return new Tuple<PlanogramBlockingGroupPlacementType, PlanogramBlockingGroupPlacementType>(PlanogramBlockingGroupPlacementType.BackToFront, PlanogramBlockingGroupPlacementType.BackToFront);
            }

            IEnumerable<PlanogramBlockingGroupPlacementType> allSequenceDirections =
                new[] { PrimarySequenceDirection, SecondarySequenceDirection, TertiarySequenceDirection };
            IEnumerable<PlanogramBlockingGroupPlacementType> remainingSequenceDirections =
                allSequenceDirections.Except(new[] { placementDirectionFromSequenceDirection });
            return new Tuple<PlanogramBlockingGroupPlacementType, PlanogramBlockingGroupPlacementType>(
                remainingSequenceDirections.First(),
                remainingSequenceDirections.Last());
        }

        /// <summary>
        /// Gets the <see cref="PlanogramMerchandisingBlockPlacement"/> before or after this one based on the <paramref name="sequenceDirection"/>
        /// and also identifies if <paramref name="blockPlacementToUse"/> is before or after this one in the <paramref name="sequenceDirection"/>.
        /// </summary>
        /// <param name="blockPlacementToUse"></param>
        /// <param name="isBlockPlacementAfter"></param>
        /// <param name="sequenceDirection"></param>
        /// <param name="blockPlacementsWithPositions"></param>
        private void GetBlockPlacementBeforeOrAfter(
            out PlanogramMerchandisingBlockPlacement blockPlacementToUse,
            out Boolean isBlockPlacementAfter,
            PlanogramBlockingGroupPlacementType sequenceDirection,
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions)
        {
            // Block placement before.
            PlanogramMerchandisingBlockPlacement blockPlacementBefore = null;
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacementsBefore =
                blockPlacementsWithPositions.Where(c =>
                    c.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis())
                    .LessThan(this.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis()))).ToList();
            if (blockPlacementsBefore.Any())
            {
                blockPlacementBefore = blockPlacementsBefore
                    .FirstOrDefault(b =>
                        b.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis())
                        .EqualTo(blockPlacementsBefore.Max(c => c.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis()))));
            }

            // Block placement after.
            PlanogramMerchandisingBlockPlacement blockPlacementAfter = null;
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacementsAfter =
                blockPlacementsWithPositions.Where(c =>
                    c.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis())
                    .GreaterThan(this.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis()))).ToList();
            if (blockPlacementsAfter.Any())
            {
                blockPlacementAfter = blockPlacementsAfter
                    .FirstOrDefault(b =>
                        b.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis())
                        .EqualTo(blockPlacementsAfter.Min(c => c.OriginalBounds.GetCenterPoint().GetCoordinate(sequenceDirection.GetAxis()))));
            }

            // Now get the block placement that is most appropriate.
            // We need the block placement that is before or after this one. If we're reversed, prefereably the one after.
            // If not, preferable the one before.
            blockPlacementToUse =
                sequenceDirection.IsPositive() ?
                blockPlacementBefore ?? blockPlacementAfter :
                blockPlacementAfter ?? blockPlacementBefore;
            isBlockPlacementAfter =
                blockPlacementToUse == null ?
                false :
                blockPlacementToUse == blockPlacementAfter;
        }

        /// <summary>
        /// Creates and adds a new position for the given product in the merchandising group, relative to the other position
        /// placements in this block placement.
        /// </summary>
        /// <param name="product">The product to place.</param>
        /// <param name="sequenceDirection">The direction to place in.</param>
        /// <param name="otherPlacements">
        /// The position placements to place relative to. These might be the position placements in this block placement 
        /// or those in the parent block.
        /// </param>
        /// <param name="sequenceNumber"></param>
        /// <param name="position"></param>
        /// <returns>True if the placement was a success.</returns>
        private Boolean TryAddPositionRelativeToPositionPlacements(
            PlanogramProduct product,
            PlanogramBlockingGroupPlacementType sequenceDirection,
            ICollection<PlanogramPositionPlacement> otherPlacements,
            Int32 sequenceNumber,
            out PlanogramPositionPlacement position)
        {
            position = null;
            if (!PositionPlacements.Any()) return false;

            // If the merch group does have placements, and so does this block placement, sequence the new position
            // relative to it's neighbour within this block placement.
            // Find the position that we're going to think about placing relative to. This may not actually be our actual
            // anchor, but its our reference point for this next placement. The position we're after is the first one
            // in this block placement before the sequence value of this product.
            Int16 precedingSequenceNumber = Convert.ToInt16(
                otherPlacements.Where(p =>
                                      p.Position.SequenceNumber.HasValue &&
                                      p.Position.SequenceNumber < sequenceNumber)
                               .Max(p => p.Position.SequenceNumber));
            PlanogramPositionPlacement precedingPosition = otherPlacements
                .FirstOrDefault(p => p.Position.SequenceNumber == precedingSequenceNumber);

            if (precedingPosition == null) return false;

            // Check that the given sequence direction is valid of the preceding position's subcomponent.
            if (IsSequenceDirectionInvalid(sequenceDirection, precedingPosition.SubComponentPlacement.SubComponent))
                return false;

            // In order to find the appropriate anchor for the new placement, we need to know about all the positions
            // and their block placements on this merchandising group. So, we build a dictionary of the block placements
            // that share this block placement's merchandising group, keyed by position placements.
            var blockPlacementsByPosition = new Dictionary<PlanogramPositionPlacement, PlanogramMerchandisingBlockPlacement>();
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacements =
                Parent.Parent.GetBlockPlacementsWithSameMerchandisingGroupAs(this).Union(new[] {this});
            foreach (PlanogramMerchandisingBlockPlacement blockPlacement in
                blockPlacements)
            {
                IEnumerable<PlanogramPositionPlacement> positionPlacements =
                    blockPlacement.PositionPlacements.Where(positionPlacement => !blockPlacementsByPosition.ContainsKey(positionPlacement));
                foreach (PlanogramPositionPlacement positionPlacement in positionPlacements)
                {
                    blockPlacementsByPosition.Add(positionPlacement, blockPlacement);
                }
            }

            // Try to get the anchor and placement direction relative to the preceding position in sequence
            // and the existing positions on this merchandising group.
            PlanogramPositionPlacement anchor;
            PlanogramPositionAnchorDirection anchorDirection;
            Debug.Write($"Position {product.Gtin} ");
            if (!TryGetAnchorAndDirection(out anchor, out anchorDirection, precedingPosition, blockPlacementsByPosition, sequenceDirection))
                return false;

            //  Check that if the anchor is not from the same block placement...
            //  the new position will not separate the anchor from another position of its own block placement.
            PlanogramPositionPlacement replacedPosition;
            if (!blockPlacementsByPosition[anchor].Parent.Equals(blockPlacementsByPosition[precedingPosition].Parent) &&
                TryFindReplacedPosition(anchor, anchorDirection, ((IDictionary<PlanogramPositionPlacement, PlanogramMerchandisingBlockPlacement>) blockPlacementsByPosition)[anchor].PositionPlacements.ToList(), out replacedPosition) &&
                blockPlacementsByPosition[anchor].Parent.Equals(blockPlacementsByPosition[replacedPosition].Parent))
                return false;

            // If we managed to find an anchor, add a position placement to the merchandising group.
            PlanogramPosition newPosition =
                PlanogramPosition.NewPlanogramPosition(0, product, precedingPosition.SubComponentPlacement, sequenceNumber, Parent.Colour);
            position = MerchandisingGroup.InsertPositionPlacement(newPosition, product, anchor, anchorDirection);
            _positionPlacements.Add(position);

            return true;
        }

        #endregion

        #region Get Anchor Direction
        private PlanogramPositionAnchorDirection GetAnchorDirection(PlanogramBlockingGroupPlacementType sequenceDirection, Boolean reverse)
        {
            switch (sequenceDirection)
            {
                case PlanogramBlockingGroupPlacementType.LeftToRight:
                    return reverse ? PlanogramPositionAnchorDirection.ToLeft : PlanogramPositionAnchorDirection.ToRight;

                case PlanogramBlockingGroupPlacementType.RightToLeft:
                    return reverse ? PlanogramPositionAnchorDirection.ToRight : PlanogramPositionAnchorDirection.ToLeft;

                case PlanogramBlockingGroupPlacementType.BottomToTop:
                    return reverse ? PlanogramPositionAnchorDirection.Below : PlanogramPositionAnchorDirection.Above;

                case PlanogramBlockingGroupPlacementType.TopToBottom:
                    return reverse ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below;

                case PlanogramBlockingGroupPlacementType.FrontToBack:
                    return reverse ? PlanogramPositionAnchorDirection.InFront : PlanogramPositionAnchorDirection.Behind;

                case PlanogramBlockingGroupPlacementType.BackToFront:
                    return reverse ? PlanogramPositionAnchorDirection.Behind : PlanogramPositionAnchorDirection.InFront;

                default:
                    throw new NotSupportedException("sequenceDirection not recognised");
            }
        }

        private PlanogramPositionAnchorDirection GetAnchorDirection(
            AxisType axis, PlanogramMerchandisingBlockPlacement anchorBlockPlacement, PlanogramBlockingGroupPlacementType sequenceDirection)
        {
            Boolean sequenceDirectionIsPositive =
                sequenceDirection.GetAxis() == axis ?
                sequenceDirection.IsPositive() :
                GetSequenceDirection(axis).IsPositive();

            if (sequenceDirectionIsPositive)
            {
                if (anchorBlockPlacement.Coordinates.GetCoordinate(axis).LessOrEqualThan(this.Coordinates.GetCoordinate(axis)))
                {
                    return PlanogramPositionAnchorDirectionHelper.GetDirection(axis, positiveDirection: true);
                }
                else
                {
                    return PlanogramPositionAnchorDirectionHelper.GetDirection(axis, positiveDirection: false);
                }
            }
            else
            {
                if (anchorBlockPlacement.Coordinates.GetCoordinate(axis).GreaterOrEqualThan(this.Coordinates.GetCoordinate(axis)))
                {
                    return PlanogramPositionAnchorDirectionHelper.GetDirection(axis, positiveDirection: false);
                }
                else
                {
                    return PlanogramPositionAnchorDirectionHelper.GetDirection(axis, positiveDirection: true);
                }
            }
        } 
        #endregion
        
        #region Get Sequence Direction Methods
        private PlanogramBlockingGroupPlacementType GetSequenceDirection(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return GetXSequenceDirection();
                case AxisType.Y:
                    return GetYSequenceDirection();
                case AxisType.Z:
                    return GetZSequenceDirection();
                default:
                    throw new NotSupportedException();
            }
        }

        private PlanogramBlockingGroupPlacementType GetXSequenceDirection()
        {
            if (PrimarySequenceDirection.IsX()) return PrimarySequenceDirection;
            if (SecondarySequenceDirection.IsX()) return SecondarySequenceDirection;
            if (TertiarySequenceDirection.IsX()) return TertiarySequenceDirection;
            throw new InvalidOperationException("No X Sequence direction was present");
        }

        private PlanogramBlockingGroupPlacementType GetYSequenceDirection()
        {
            if (PrimarySequenceDirection.IsY()) return PrimarySequenceDirection;
            if (SecondarySequenceDirection.IsY()) return SecondarySequenceDirection;
            if (TertiarySequenceDirection.IsY()) return TertiarySequenceDirection;
            throw new InvalidOperationException("No Y Sequence direction was present");
        }

        private PlanogramBlockingGroupPlacementType GetZSequenceDirection()
        {
            if (PrimarySequenceDirection.IsZ()) return PrimarySequenceDirection;
            if (SecondarySequenceDirection.IsZ()) return SecondarySequenceDirection;
            if (TertiarySequenceDirection.IsZ()) return TertiarySequenceDirection;
            throw new InvalidOperationException("No Z Sequence direction was present");
        } 
        #endregion
        
        private Boolean TryGetAnchorAndDirection(
            out PlanogramPositionPlacement anchor, 
            out PlanogramPositionAnchorDirection anchorDirection, 
            PlanogramPositionPlacement precedingPosition,
            Dictionary<PlanogramPositionPlacement, PlanogramMerchandisingBlockPlacement> blockPlacementsByPosition,
            PlanogramBlockingGroupPlacementType sequenceDirection)
        {
            switch (sequenceDirection.GetAxis())
            {
                // Y is easy - we just insert a position above or below the preceding position, 
                // because this doesn't affect other positions.
                case AxisType.Y:
                    if (precedingPosition.SubComponentPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                    {
                        anchor = precedingPosition;
                    }
                    else
                    {
                        // The y sequence of the positions we'll be placing in, dependant on the direction of the sequence.
                        Int16 targetYSequence =
                            sequenceDirection.IsPositive() ?
                            Convert.ToInt16(precedingPosition.Position.SequenceY + 1) :
                            Convert.ToInt16(precedingPosition.Position.SequenceY - 1);

                        // The y stack of positions at the target y sequence.
                        ICollection<PlanogramPositionPlacement> yGroup =
                            blockPlacementsByPosition
                            .Keys
                            .Where(p => p.Position.SequenceY == targetYSequence).ToList();

                        if (yGroup.Any())
                        {
                            // There are positions that exist on this sequence group.
                            // we need to calculate the correct anchor position based upon the blocking
                            List<PlanogramMerchandisingBlockPlacement> blockPlacementsWithPositions = new List<PlanogramMerchandisingBlockPlacement>();

                            // firstly get the block placements that exist on the y sequence group
                            foreach (var ySequencePlaements in yGroup)
                            {
                                if (!blockPlacementsWithPositions.Contains(blockPlacementsByPosition[ySequencePlaements]))
                                {
                                    blockPlacementsWithPositions.Add(blockPlacementsByPosition[ySequencePlaements]);
                                }
                            }

                            PlanogramMerchandisingBlockPlacement blockPlacementToUse;
                            Boolean isBlockPlacementAfter;

                            GetBlockPlacementBeforeOrAfter(out blockPlacementToUse, out isBlockPlacementAfter, GetXSequenceDirection(), blockPlacementsWithPositions);
                            anchorDirection = PlanogramPositionAnchorDirectionHelper.GetDirection(AxisType.X, !isBlockPlacementAfter);

                            if (blockPlacementToUse == null) // There are no block placements in the x direction
                            {
                                anchor = blockPlacementsByPosition.Keys.Where(p => p.Position.SequenceY == precedingPosition.Position.SequenceY).OrderBy(p => p.Position.SequenceX).First();
                                anchorDirection = GetAnchorDirection(AxisType.Y, blockPlacementsByPosition[anchor], sequenceDirection);
                            }
                            else
                            {
                                // Find the anchor position from the block placement to use \ placement after
                                IEnumerable<PlanogramPositionPlacement> placements =
                                    blockPlacementToUse.PositionPlacements
                                                       .Where(p => p.Position.SequenceY == targetYSequence).OrderBy(p => p.Position.SequenceX);
                                anchor = isBlockPlacementAfter ? placements.FirstOrDefault() : placements.LastOrDefault();
                            }
                            Debug.WriteLine($"will try {anchorDirection} of {anchor?.Product.Gtin ?? "[no anchor found!]"}...");
                            return true;
                        }
                        IEnumerable<PlanogramPositionPlacement> precedingYGroup =
                            blockPlacementsByPosition.Keys.Where(p => p.Position.SequenceY == precedingPosition.Position.SequenceY);
                        anchor = precedingYGroup.OrderBy(p => p.Position.SequenceX).FirstOrDefault() ?? precedingPosition;
                    }
                    anchorDirection = GetAnchorDirection(AxisType.Y, blockPlacementsByPosition[anchor], sequenceDirection);
                    Debug.WriteLine($"will try {anchorDirection} of {anchor.Product.Gtin}...");
                    return true;

                // Z is a bit trickier. We need to see if there are any positions in front of or behind the preceding
                // position and, if so, take one of these positions as an anchor to place above.
                case AxisType.Z:
                    {
                        // The positions in the same x group as the preceding position.
                        IEnumerable<PlanogramPositionPlacement> xGroup = blockPlacementsByPosition
                            .Keys
                            .Where(p => p.Position.SequenceX == precedingPosition.Position.SequenceX);

                        // The z sequence of the positions we'll be placing in, dependant on the direction of the sequence.
                        Int16 targetZSequence =
                            sequenceDirection.IsPositive() ?
                            Convert.ToInt16(precedingPosition.Position.SequenceZ + 1) :
                            Convert.ToInt16(precedingPosition.Position.SequenceZ - 1);

                        // The y stack of positions at the target z sequence within the x group.
                        IEnumerable<PlanogramPositionPlacement> zGroup =
                            xGroup.Where(p => p.Position.SequenceZ == targetZSequence);

                        // If any existed at the target sequence, find an anchor.
                        if (zGroup.Any())
                        {
                            anchor =
                                GetYSequenceDirection().IsPositive() ?
                                zGroup.OrderByDescending(p => p.Position.SequenceY).First() :
                                zGroup.OrderBy(p => p.Position.SequenceY).First();
                            anchorDirection = GetAnchorDirection(AxisType.Z, blockPlacementsByPosition[anchor], sequenceDirection);
                        }
                        else // If not, just use the preceding position.
                        {
                            anchor = precedingPosition;
                            anchorDirection = GetAnchorDirection(AxisType.Z, blockPlacementsByPosition[anchor], sequenceDirection);
                        }
                        Debug.WriteLine($"will try {anchorDirection} of {anchor.Product.Gtin}...");
                        return true;
                    }

                // X is even trickier! We need to find out if we're placing over any existing x groups on either side and
                // then, within that group determine if we're placing over any existing z groups.
                case AxisType.X:
                    {
                        // The sequence to the left or right.
                        Int16 targetXSequence =
                                        sequenceDirection.IsPositive() ?
                                        Convert.ToInt16(precedingPosition.Position.SequenceX + 1) :
                                        Convert.ToInt16(precedingPosition.Position.SequenceX - 1);

                        // The x group of positions to the left or right.
                        IEnumerable<PlanogramPositionPlacement> targetXGroup =
                            blockPlacementsByPosition.Keys.Where(p => p.Position.SequenceX == targetXSequence);

                        // Work out if we need to place this product in z, rather than just inserting it.
                        IEnumerable<Int16> xGroupZSequences = blockPlacementsByPosition.Keys
                            .Where(p => p.Position.SequenceX == precedingPosition.Position.SequenceX)
                            .Select(p => p.Position.SequenceZ).Distinct();
                        Boolean targetGroupHasPositions = targetXGroup.Any();
                        Boolean xGroupHasOneZSequence = xGroupZSequences.Count() == 1;
                        Boolean targetGroupHasOneZSequence = targetXGroup.Select(p => p.Position.SequenceZ).Distinct().Count() == 1;

                        if (targetGroupHasPositions && !(xGroupHasOneZSequence && targetGroupHasOneZSequence))
                        {
                            // The same z group as the preceding position.
                            IEnumerable<PlanogramPositionPlacement> zGroup =
                                targetXGroup.Where(p => p.Position.SequenceZ == precedingPosition.Position.SequenceZ);

                            if (zGroup.Any())
                            {
                                anchor =
                                    GetZSequenceDirection().IsPositive() ?
                                    zGroup.OrderByDescending(p => p.Position.SequenceZ).First() :
                                    zGroup.OrderBy(p => p.Position.SequenceZ).First();
                                anchorDirection = GetAnchorDirection(AxisType.Z, blockPlacementsByPosition[anchor], sequenceDirection);
                                Debug.WriteLine($"will try {anchorDirection} of {anchor.Product.Gtin}...");
                                return true;
                            }

                            // The front or back sequence value.
                            Int16 targetZSequence =
                                GetZSequenceDirection().IsPositive() ?
                                    targetXGroup.Max(p => p.Position.SequenceZ) :
                                    targetXGroup.Min(p => p.Position.SequenceZ);

                            zGroup = targetXGroup.Where(p => p.Position.SequenceZ == targetZSequence);

                            if (zGroup.Any())
                            {
                                anchor =
                                    GetZSequenceDirection().IsPositive() ?
                                        zGroup.OrderByDescending(p => p.Position.SequenceZ).First() :
                                        zGroup.OrderBy(p => p.Position.SequenceZ).First();
                                anchorDirection = GetAnchorDirection(AxisType.Z, blockPlacementsByPosition[anchor], sequenceDirection);
                                Debug.WriteLine($"will try {anchorDirection} of {anchor.Product.Gtin}...");
                                return true;
                            }
                        }

                        if (xGroupZSequences.Count() > 1)
                        {
                            // The preceding position's x group contained multiple z sequences, but we weren't
                            // able to place behind another position. Because we've already got multiple z sequences,
                            // we don't want to insert a position, so assign default values and return false;
                            anchor = null;
                            anchorDirection = PlanogramPositionAnchorDirection.Above;
                            Debug.WriteLine($"did not get any anchor!");
                            return false;
                        }

                        // If we get here, we weren't able (or didn't want) to place the position behind another position,
                        // so we just want to insert it to the left or right of the preceding position.
                        anchor = precedingPosition;
                        anchorDirection = GetAnchorDirection(AxisType.X, blockPlacementsByPosition[anchor], sequenceDirection);
                        Debug.WriteLine($"will try {anchorDirection} of {anchor.Product.Gtin}...");
                        return true;
                    }

                default:
                    throw new NotSupportedException("axis not supported");
            }
        }

        /// <summary>
        /// Calculates the sequence values of the <see cref="PlanogramPosition"/> to use as an anchor from <paramref name="blockPlacementBeforeOrAfter"/>.
        /// </summary>
        /// <param name="precedingSequenceX">The Sequence X variable to be assigned.</param>
        /// <param name="precedingSequenceY">The Sequence Y variable to be assigned.</param>
        /// <param name="precedingSequenceZ">The Sequence Z variable to be assigned.</param>
        /// <param name="sequenceDirection">The direction that products are being placed in.</param>
        /// <param name="blockPlacementBeforeOrAfter">
        /// The <see cref="PlanogramMerchandisingBlockPlacement"/> that has been selected to place relative to, which might appear before or after this one in the <paramref name="sequenceDirection"/>.
        /// </param>
        /// <param name="isBlockPlacementAfter">
        /// Indicates whether or not <paramref name="blockPlacementBeforeOrAfter"/> is after this <see cref="PlanogramMerchandisingBlockPlacement"/>.
        /// </param>
        private void CalculatePrecedingSequences(
            out Int16 precedingSequenceX, 
            out Int16 precedingSequenceY, 
            out Int16 precedingSequenceZ,
            PlanogramBlockingGroupPlacementType sequenceDirection, 
            PlanogramMerchandisingBlockPlacement blockPlacementBeforeOrAfter,
            Boolean isBlockPlacementAfter)
        {
            Int16 minOrMaxSequence =
                isBlockPlacementAfter ?
                blockPlacementBeforeOrAfter.PositionPlacements.Min(p => p.Position.GetSequence(sequenceDirection.GetAxis())) :
                blockPlacementBeforeOrAfter.PositionPlacements.Max(p => p.Position.GetSequence(sequenceDirection.GetAxis()));

            if (sequenceDirection.IsX())
            {
                precedingSequenceX = minOrMaxSequence;
                Int16 precedingSequenceXToMatch = precedingSequenceX;

                IEnumerable<PlanogramPositionPlacement> positionsMatchingInX =
                    blockPlacementBeforeOrAfter.PositionPlacements.Where(p => p.Position.SequenceX == precedingSequenceXToMatch).ToList();

                precedingSequenceZ =
                    GetZSequenceDirection().IsPositive() ?
                    positionsMatchingInX.Min(p => p.Position.SequenceZ) :
                    positionsMatchingInX.Max(p => p.Position.SequenceZ);
                Int16 precedingSequenceZToMatch = precedingSequenceZ;

                IEnumerable<PlanogramPositionPlacement> positionsMatchingInXZ =
                    positionsMatchingInX.Where(p => p.Position.SequenceZ == precedingSequenceZToMatch);

                precedingSequenceY =
                    GetYSequenceDirection().IsPositive() ?
                    positionsMatchingInXZ.Min(p => p.Position.SequenceY) :
                    positionsMatchingInXZ.Max(p => p.Position.SequenceY);
            }
            else if (sequenceDirection.IsY())
            {
                precedingSequenceY = minOrMaxSequence;
                Int16 precedingSequenceYToMatch = precedingSequenceY;

                IEnumerable<PlanogramPositionPlacement> positionsMatchingInY =
                    blockPlacementBeforeOrAfter.PositionPlacements.Where(p => p.Position.SequenceY == precedingSequenceYToMatch).ToList();

                precedingSequenceX =
                    GetXSequenceDirection().IsPositive() ?
                    positionsMatchingInY.Min(p => p.Position.SequenceX) :
                    positionsMatchingInY.Max(p => p.Position.SequenceX);
                Int16 precedingSequenceXToMatch = precedingSequenceX;

                IEnumerable<PlanogramPositionPlacement> positionsMatchingInXY =
                    positionsMatchingInY.Where(p => p.Position.SequenceX == precedingSequenceXToMatch);

                precedingSequenceZ =
                    GetZSequenceDirection().IsPositive() ?
                    positionsMatchingInXY.Min(p => p.Position.SequenceZ) :
                    positionsMatchingInXY.Max(p => p.Position.SequenceZ);
            }
            else if (sequenceDirection.IsZ())
            {
                precedingSequenceZ = minOrMaxSequence;
                Int16 precedingSequenceZToMatch = precedingSequenceZ;

                IEnumerable<PlanogramPositionPlacement> positionsMatchingInZ =
                    blockPlacementBeforeOrAfter.PositionPlacements.Where(p => p.Position.SequenceZ == precedingSequenceZToMatch).ToList();

                precedingSequenceX =
                    GetXSequenceDirection().IsPositive() ?
                    positionsMatchingInZ.Min(p => p.Position.SequenceX) :
                    positionsMatchingInZ.Max(p => p.Position.SequenceX);
                Int16 precedingSequenceXToMatch = precedingSequenceX;

                IEnumerable<PlanogramPositionPlacement> positionsMatchingInXZ =
                    positionsMatchingInZ.Where(p => p.Position.SequenceX == precedingSequenceXToMatch);

                precedingSequenceY =
                    GetYSequenceDirection().IsPositive() ?
                    positionsMatchingInXZ.Min(p => p.Position.SequenceY) :
                    positionsMatchingInXZ.Max(p => p.Position.SequenceY);
            }
            else
            {
                throw new NotSupportedException("sequenceDirection was of an unknown axis");
            }
        }

        private static Boolean IsSequenceDirectionInvalid(PlanogramBlockingGroupPlacementType sequenceDirection, PlanogramSubComponent subComponent)
        {
            if (sequenceDirection.IsX())
            {
                return
                    subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom ||
                    subComponent.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Left ||
                    subComponent.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Right;
            }
            else if (sequenceDirection.IsY())
            {
                return
                    subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom ||
                    subComponent.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Top ||
                    subComponent.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Bottom;
            }
            else if (sequenceDirection.IsZ())
            {
                return
                    subComponent.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Front ||
                    subComponent.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Back;
            }
            else
            {
                throw new NotSupportedException("sequenceDirection not supported");
            }
        }

        /// <summary>
        /// Gets the block placements that have the same Merchandising Group as this one.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PlanogramMerchandisingBlockPlacement> GetMerchandisingGroupBlockPlacementsInParentBlock()
        {
            if (_merchGroupBlockPlacementsInParentBlock == null)
            {
                _merchGroupBlockPlacementsInParentBlock =
                    Parent == null ?
                    new List<PlanogramMerchandisingBlockPlacement>() :
                    Parent.GetBlockPlacementsWithSameMerchandisingGroupAs(this).ToList();
            }
            return _merchGroupBlockPlacementsInParentBlock;
        }

        private IEnumerable<PlanogramMerchandisingBlockPlacement> GetMerchandisingGroupBlockPlacementsInRootBlocking()
        {
            if (_merchGroupBlockPlacementsInRootBlocking == null)
            {
                _merchGroupBlockPlacementsInRootBlocking =
                    Parent == null || Parent.Parent == null ?
                    new List<PlanogramMerchandisingBlockPlacement>() :
                    Parent.Parent.GetBlockPlacementsWithSameMerchandisingGroupAs(this).ToList();
            }
            return _merchGroupBlockPlacementsInRootBlocking;
        }

        /// <summary>
        /// Used to adjust this instance's <see cref="Bounds"/> after it has been initially constructed.
        /// </summary>
        /// <param name="bounds"></param>
        internal void UpdatePlacementBounds(RectValue bounds)
        {
            Bounds = bounds;
        }

        /// <summary>
        /// Used to adjust this instance's <see cref="SequenceNumber"/> after it has been initially constructed.
        /// </summary>
        /// <param name="sequence"></param>
        internal void ReSequence(Int32 sequence)
        {
            SequenceNumber = sequence;
        }

        public IEnumerable<PlanogramSequenceGroupProduct> EnumerateSequenceProducts()
        {
            foreach (PlanogramPositionPlacement positionPlacement in PositionPlacements)
            {
                yield return Parent.SequenceGroup.Products.FirstOrDefault(p => p.Gtin.Equals(positionPlacement.Product.Gtin));
            }

            yield break;
        }

        /// <summary>
        /// Merges the given <paramref name="blockPlacement"/> with this one, combining <see cref="Bounds"/>
        /// and <see cref="PositionPlacements"/>.
        /// </summary>
        /// <param name="blockPlacement"></param>
        internal void MergeWith(PlanogramMerchandisingBlockPlacement blockPlacement)
        {
            Bounds = Bounds.Union(blockPlacement.Bounds);
            OriginalBounds = OriginalBounds.Union(blockPlacement.OriginalBounds);
            _positionPlacements.AddRange(blockPlacement.PositionPlacements);
            Obstructions = Obstructions.Union(blockPlacement.Obstructions).ToList();
        }

        /// <summary>
        /// Determines if the given <paramref name="blockPlacement"/> can be merged with this one.
        /// </summary>
        /// <param name="blockPlacement"></param>
        /// <returns></returns>
        internal Boolean CanMergeWith(PlanogramMerchandisingBlockPlacement blockPlacement)
        {
            return
                MerchandisingGroup == blockPlacement.MerchandisingGroup &&
                Bounds.IsContinuousWith(blockPlacement.Bounds) &&
                PositionsAreConsecutive(PositionPlacements.Union(blockPlacement.PositionPlacements));
        }

        /// <summary>
        /// Returns true if the given <paramref name="positionPlacements"/> are consecutive in sequence.
        /// </summary>
        /// <param name="positionPlacements"></param>
        /// <returns></returns>
        private static Boolean PositionsAreConsecutive(IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            if (!positionPlacements.Any()) return true;
            List<PlanogramPositionPlacement> consecutivePositions = 
                new List<PlanogramPositionPlacement>(positionPlacements.Take(1));
            List<PlanogramPositionPlacement> remainingPositions = 
                new List<PlanogramPositionPlacement>(positionPlacements.Skip(1));
            if (!remainingPositions.Any()) return true;

            while(true)
            {
                Boolean consectivePositionFound = false;
                foreach(PlanogramPositionPlacement positionPlacement in consecutivePositions.ToList())
                {
                    PlanogramPositionPlacement consecutivePosition = remainingPositions
                        .FirstOrDefault(p => p.IsConsecutiveInSequenceWith(positionPlacement));
                    if(consecutivePosition != null)
                    {
                        consecutivePositions.Add(consecutivePosition);
                        remainingPositions.Remove(consecutivePosition);
                        consectivePositionFound = true;
                        break;
                    }
                }

                if (!consectivePositionFound) break;
            }

            return consecutivePositions.Count() == positionPlacements.Count();
        }

        #endregion
        
        #region Helper static methods

        /// <summary>
        ///     Tries to determine which position would inserting another using the provided <paramref name="anchor"/> 
        ///     and the given <paramref name="anchorDirection"/> would be replaced.
        /// </summary>
        /// <param name="anchor">The <see cref="PlanogramPositionPlacement"/> that would be used for the insert operation.</param>
        /// <param name="anchorDirection">The <see cref="PlanogramPositionAnchorDirection"/> of the insert around the <paramref name="anchor"/>.</param>
        /// <param name="placementsInAnchorBlock">Enumeration of <see cref="PlanogramPositionPlacement"/> that contain all the placements for the <see cref="PlanogramMerchandisingBlockPlacement"/> that contains the anchor.</param>
        /// <param name="replacedPosition">[<c>Out</c>] The position that would be replaced if any. If there is no replaced position, this will be null.</param>
        /// <returns><c>True</c> if there was a position that would be replaced in the anchor's block. <c>False</c> otherwise.</returns>
        private static Boolean TryFindReplacedPosition(
            PlanogramPositionPlacement anchor,
            PlanogramPositionAnchorDirection anchorDirection,
            IEnumerable<PlanogramPositionPlacement> placementsInAnchorBlock,
            out PlanogramPositionPlacement replacedPosition)
        {
            //  The replaced position would be the next position in placement sequence 
            //  for the axis and direction determined by the anchor direction.
            //  Only those positions that also belong to the anchor's block matter
            //  as this method is looking for inserts inside a block
            //  (as opposed to placing positions adjacent to a block).
            Int16 replaceSequence = anchorDirection.GetNextSequence(anchor.Position);
            replacedPosition = PlacementsOnSameSequenceAxis(anchor, anchorDirection, placementsInAnchorBlock)
                .FirstOrDefault(placement =>
                                placement.Position.GetSequence(anchorDirection.GetAxis()) == replaceSequence);

            return replacedPosition != null;
        }

        /// <summary>
        ///     Enumerates the <paramref name="placementsToAlign"/> that are on the same axis sequence that the <paramref name="anchor"/>.
        /// </summary>
        /// <param name="anchor">The position around which to obtain other placements.</param>
        /// <param name="anchorDirection">The direction used to determine what axis the other placements must be in.</param>
        /// <param name="placementsToAlign">All the placements that we want to find aligned ones from.</param>
        /// <returns></returns>
        private static IEnumerable<PlanogramPositionPlacement> PlacementsOnSameSequenceAxis(
            PlanogramPositionPlacement anchor,
            PlanogramPositionAnchorDirection anchorDirection,
            IEnumerable<PlanogramPositionPlacement> placementsToAlign)
        {
            IEnumerable<AxisType> normalAxes = anchorDirection.GetAxis().GetNormals().ToList();
            AxisType firstNormal = normalAxes.First();
            AxisType secondNormal = normalAxes.Last();
            Int16 firstNormalSequence = anchor.Position.GetSequence(firstNormal);
            Int16 secondNormalSequence = anchor.Position.GetSequence(secondNormal);
            IEnumerable<PlanogramPositionPlacement> placementsOnSameSequenceAxis = placementsToAlign
                .Where(placement =>
                       placement.Position.GetSequence(firstNormal) == firstNormalSequence &&
                       placement.Position.GetSequence(secondNormal) == secondNormalSequence);
            return placementsOnSameSequenceAxis;
        }

        #endregion
    }
}
