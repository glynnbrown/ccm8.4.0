﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27832 : A.Kuszyk
//  Created.
// V8-27852 : N.Foster
//  Changes lists of positions to PlanogramPositionPlacementList types
#endregion

#region Version History : CCM 801
// V8-28837 : A.Kuszyk
//  Added GetAdjacentsInPositive/Negative methods.
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Framework.Enums;
using System;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// This class represents the positions placements that may lie adjacent to
    /// another position in all three dimensions.
    /// </summary>
    public sealed class PlanogramPositionPlacementAdjacency
    {
        #region Properties
        /// <summary>
        /// The positions that are in the positive Y direction.
        /// </summary>
        public PlanogramPositionPlacementList Above { get; private set; }

        /// <summary>
        /// The positions that are in the negative Y direction.
        /// </summary>
        public PlanogramPositionPlacementList Below { get; private set; }

        /// <summary>
        /// The positions that are in the positive Z direction.
        /// </summary>
        public PlanogramPositionPlacementList Front { get; private set; }

        /// <summary>
        /// The positions that are in the negative Z direction.
        /// </summary>
        public PlanogramPositionPlacementList Back { get; private set; }

        /// <summary>
        /// The positions that are in the negative X direction.
        /// </summary>
        public PlanogramPositionPlacementList Left { get; private set; }

        /// <summary>
        /// The positions that are in the positive X direction.
        /// </summary>
        public PlanogramPositionPlacementList Right { get; private set; }

        /// <summary>
        /// The position placement to whom the other positions in this object are adjacent.
        /// </summary>
        public PlanogramPositionPlacement PositionPlacement { get; private set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new object from the given collections.
        /// </summary>
        public PlanogramPositionPlacementAdjacency(
            PlanogramPositionPlacement positionPlacement,
            IEnumerable<PlanogramPositionPlacement> above,
            IEnumerable<PlanogramPositionPlacement> below,
            IEnumerable<PlanogramPositionPlacement> front,
            IEnumerable<PlanogramPositionPlacement> back,
            IEnumerable<PlanogramPositionPlacement> left,
            IEnumerable<PlanogramPositionPlacement> right)
        {
            PositionPlacement = positionPlacement;
            Above = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(above);
            Below = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(below);
            Front = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(front);
            Back = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(back);
            Left = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(left);
            Right = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(right);
        } 
        #endregion

        #region Methods

        /// <summary>
        /// Returns the list of position placements corresponding to the positive direction in the 
        /// given axis.
        /// </summary>
        /// <param name="axis">The axis in which to return positions.</param>
        /// <returns>A collection of position placements.</returns>
        public IEnumerable<PlanogramPositionPlacement> GetAdjacentsInPositive(AxisType axis)
        {
            switch(axis)
            {
                case AxisType.X: return Right;
                case AxisType.Y: return Above;
                case AxisType.Z: return Front;
                default: throw new ArgumentOutOfRangeException("axis");
            }
        }

        /// <summary>
        /// Returns the list of position placements corresponding to the negative direction in the 
        /// given axis.
        /// </summary>
        /// <param name="axis">The axis in which to return positions.</param>
        /// <returns>A collection of position placements.</returns>
        public IEnumerable<PlanogramPositionPlacement> GetAdjacentsInNegative(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X: return Left;
                case AxisType.Y: return Below;
                case AxisType.Z: return Back;
                default: throw new ArgumentOutOfRangeException("axis");
            }
        }

        #endregion
    }
}
