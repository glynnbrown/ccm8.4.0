﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27738 : N.Foster
//  Created
// V8-27477 : L.Ineson
//  Added IDisposable implementation.
// V8-28620 : N.Foster
//  Fixed issue regarding edit levels
#endregion
#region Version History: CCM803
// V8-29280 : A.Silva
//  Added GroupByAxis to return the placements grouped by a given axis.
// V8-29137 : A.Kuszyk
//  Added edit level check to ApplyEdit.
#endregion
#region Version History: CCM810
// V8-29826 : N.Foster
//  Enhance behavior of merchandising group when dealing with Begin/Cancel/Apply of edits
// V8-29903 : L.Ineson
//  Added RemoveWithoutDelete()
// V8-30173 : L.Ineson
//  Amended GroupByAxis so that it can be passed an enumerable of placements.
#endregion
#region Version History : CCM830
// V8-31807 : A.Kuszyk
//  Added GetReservedSpace method.
// V8-32327 : A.Silva
//  Added ShiftSequenceXFrom, ShiftSequenceYFrom and ShiftSequenceZFrom helper methods.
// CCM-13871 : A.Silva
//  Amended GetMaxSize so that white space is considered when it would not be merchandisable (as in pegboards on the Y axis and Chests on the X axis.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Enums;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents a list of planogram position placements
    /// with method to intertact with those placements
    /// simultaneously
    /// </summary>
    public class PlanogramPositionPlacementList : Collection<PlanogramPositionPlacement>, IDisposable
    {
        #region Nested Classes
        /// <summary>
        /// Class used to capture the state of this placement
        /// during edit operations
        /// </summary>
        private class State
        {
            #region Properties
            /// <summary>
            /// The items held within the list
            /// </summary>
            public IEnumerable<PlanogramPositionPlacement> ListItems { get; private set; }

            /// <summary>
            /// The items deleted from this list
            /// </summary>
            public IEnumerable<PlanogramPositionPlacement> DeletedItems { get; private set; }

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public State(PlanogramPositionPlacementList list)
            {
                this.ListItems = list.ToList();
                this.DeletedItems = list.DeletedItems.ToList();
            }

            #endregion
        }

        #endregion

        #region Fields
        private Stack<State> _stateStack = new Stack<State>();
        private List<PlanogramPositionPlacement> _deletedItems = new List<PlanogramPositionPlacement>();
        #endregion

        #region Properties

        #region EditLevel
        /// <summary>
        /// Returns the edit level for this list of positions
        /// </summary>
        public Int32 EditLevel
        {
            get { return _stateStack.Count; }
        }
        #endregion

        #region DeletedItems
        /// <summary>
        /// Returns all deleted items that are part of this list
        /// </summary>
        public IEnumerable<PlanogramPositionPlacement> DeletedItems
        {
            get { return _deletedItems; }
        }
        #endregion

        #endregion

        #region Constructors
        private PlanogramPositionPlacementList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionPlacementList NewPlanogramPositionPlacementList()
        {
            PlanogramPositionPlacementList item = new PlanogramPositionPlacementList();
            item.Create(null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionPlacementList NewPlanogramPositionPlacementList(
            IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            PlanogramPositionPlacementList item = new PlanogramPositionPlacementList();
            item.Create(positionPlacements);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            if (positionPlacements != null)
            {
                foreach (PlanogramPositionPlacement placement in positionPlacements)
                    this.Add(placement);
            }
        }
        #endregion

        #endregion

        #region Methods

        #region Begin/Cancel/Apply

        /// <summary>
        /// Begins an edit operation on all positions
        /// </summary>
        public void BeginEdit()
        {
            // push our current list state onto the stack 
            _stateStack.Push(new State(this));

            // begin an edit on each placement
            foreach (PlanogramPositionPlacement placement in this)
            {
                placement.BeginEdit();
            }
        }

        /// <summary>
        /// Cancels the edit operation on all positions
        /// </summary>
        public void CancelEdit()
        {
            // ensure we have some state to roll back to
            if (_stateStack.Count == 0) return;

            // pop the last list state of the stack
            State state = _stateStack.Pop();

            // apply the state to this list
            this.Clear();
            foreach (PlanogramPositionPlacement item in state.ListItems) this.Add(item);

            // apply the deleted state to the list
            _deletedItems.Clear();
            foreach (PlanogramPositionPlacement item in state.DeletedItems) _deletedItems.Add(item);

            // cancel an edit on each placement
            foreach (PlanogramPositionPlacement placement in this)
            {
                placement.CancelEdit();
            }
        }

        /// <summary>
        /// Applies all current edit operations to this group
        /// </summary>
        public void ApplyEdit()
        {
            // clear the state stack
            _stateStack.Clear();

            // apply all edits on each placement in this list
            foreach (PlanogramPositionPlacement placement in this)
            {
                placement.ApplyEdit();
            }
        }

        /// <summary>
        /// Resets all pending edits on this list
        /// </summary>
        public void Reset()
        {
            // get the initial state from the stack
            State state = null;
            while (_stateStack.Count > 0)
            {
                state = _stateStack.Pop();
            }

            // if we have no state, then there is nothing to do
            if (state == null) return;

            // apply the state to this list
            this.Clear();
            foreach (PlanogramPositionPlacement item in state.ListItems) this.Add(item);

            // apply the deleted state to the list
            _deletedItems.Clear();
            foreach (PlanogramPositionPlacement item in state.DeletedItems) _deletedItems.Add(item);

            // reset the state of each position in the list
            foreach (PlanogramPositionPlacement placement in this)
            {
                placement.Reset();
            }
        }

        #endregion

        #region Overrides
        /// <summary>
        /// Called when an item is removed from this list
        /// </summary>
        protected override void RemoveItem(int index)
        {
            _deletedItems.Add(this[index]);
            base.RemoveItem(index);
        }

        /// <summary>
        /// Called when all items are being cleared from this list
        /// </summary>
        protected override void ClearItems()
        {
            _deletedItems.AddRange(this);
            base.ClearItems();
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Gets a <see cref="RectValue"/> that represents the reserved spaces of the positions in this list.
        /// This space is an aggregation of the spaces occupied by each of the positions, centred on their total space.
        /// It does not contain any white space that might exist between the positions.
        /// </summary>
        /// <returns></returns>
        public RectValue GetReservedSpace()
        {
            Dictionary<PlanogramPositionPlacement, RectValue> reservedSpacesByPositionPlacement =
                this.ToDictionary(p => p, p => p.GetReservedSpace());

            PointValue minCoords = new PointValue(
                reservedSpacesByPositionPlacement.Values.Min(r => r.X),
                reservedSpacesByPositionPlacement.Values.Min(r => r.Y),
                reservedSpacesByPositionPlacement.Values.Min(r => r.Z));
            PointValue maxCoords = new PointValue(
                reservedSpacesByPositionPlacement.Values.Max(r => r.X + r.Width),
                reservedSpacesByPositionPlacement.Values.Max(r => r.Y + r.Height),
                reservedSpacesByPositionPlacement.Values.Max(r => r.Z + r.Depth));

            WidthHeightDepthValue space = new WidthHeightDepthValue(
                GetMaxSize(AxisType.X, reservedSpacesByPositionPlacement),
                GetMaxSize(AxisType.Y, reservedSpacesByPositionPlacement),
                GetMaxSize(AxisType.Z, reservedSpacesByPositionPlacement));

            PointValue coords = new PointValue(
                minCoords.X + ((maxCoords.X - minCoords.X) / 2) - (space.Width / 2),
                minCoords.Y + ((maxCoords.Y - minCoords.Y) / 2) - (space.Height / 2),
                minCoords.Z + ((maxCoords.Z - minCoords.Z) / 2) - (space.Depth / 2));

            return new RectValue(coords, space);
        }

        /// <summary>
        /// Gets the maximum size of the position placements in <paramref name="reservedSpacesByPositionPlacement"/> at any unique 
        /// combination of sequence values in the directions normal to <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="reservedSpacesByPositionPlacement"></param>
        /// <returns></returns>
        private static Single GetMaxSize(AxisType axis, Dictionary<PlanogramPositionPlacement, RectValue> reservedSpacesByPositionPlacement)
        {
            PlanogramMerchandisingGroup merchGroup = reservedSpacesByPositionPlacement.Keys.FirstOrDefault()?.MerchandisingGroup;
            PlanogramSubComponentMerchandisingType? merchType = merchGroup?.MerchandisingType;
            if (axis == AxisType.Y &&
                (merchType == PlanogramSubComponentMerchandisingType.Hang ||
                 merchType == PlanogramSubComponentMerchandisingType.HangFromBottom) &&
                (merchGroup?.StrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked ||
                 merchGroup?.StrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked ||
                 merchGroup?.StrategyY == PlanogramSubComponentYMerchStrategyType.Even))
            {
                return reservedSpacesByPositionPlacement.Values.Max(value => value.GetCoordinate(axis) + value.GetSize(axis)) - reservedSpacesByPositionPlacement.Values.Min(value => value.GetCoordinate(axis));
            }

            if (axis == AxisType.X &&
                merchType == PlanogramSubComponentMerchandisingType.Stack &&
                (merchGroup?.StrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked ||
                 merchGroup?.StrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked ||
                 merchGroup?.StrategyY == PlanogramSubComponentYMerchStrategyType.Even ||
                 merchGroup?.StrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked ||
                 merchGroup?.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked ||
                 merchGroup?.StrategyZ == PlanogramSubComponentZMerchStrategyType.Even))
            {
                return reservedSpacesByPositionPlacement.Values.Max(value => value.GetCoordinate(axis) + value.GetSize(axis)) - reservedSpacesByPositionPlacement.Values.Min(value => value.GetCoordinate(axis));
            }

            Single maxSize = 0;
            AxisType firstNormal = axis.GetNormals().ElementAt(0);
            AxisType secondNormal = axis.GetNormals().ElementAt(1);
            
            IEnumerable<Int16> firstNormalSequences = reservedSpacesByPositionPlacement.Keys
                .Select(p => p.Position.GetSequence(firstNormal))
                .Distinct();
            foreach(Int16 firstNormalSequence in firstNormalSequences)
            {
                IEnumerable<Int16> secondNormalSequences = reservedSpacesByPositionPlacement.Keys
                    .Where(p => p.Position.GetSequence(firstNormal) == firstNormalSequence)
                    .Select(p => p.Position.GetSequence(secondNormal))
                    .Distinct();
                foreach (Int16 secondNormalSequence in secondNormalSequences)
                {
                    IEnumerable<PlanogramPositionPlacement> matchingPositions = reservedSpacesByPositionPlacement.Keys
                        .Where(p => 
                            p.Position.GetSequence(firstNormal) == firstNormalSequence && 
                            p.Position.GetSequence(secondNormal) == secondNormalSequence);
                    maxSize = Math.Max(maxSize, matchingPositions.Sum(p => reservedSpacesByPositionPlacement[p].GetSize(axis)));
                }
            }

            return maxSize;
        }

        /// <summary>
        /// Returns aggregrated position details
        /// for all positions within this list
        /// </summary>
        public PlanogramPositionsDetails GetPositionDetails()
        {
            return PlanogramPositionsDetails.NewPlanogramPositionsDetails(this);
        }

        /// <summary>
        ///     Group the <see cref="PlanogramPositionPlacement"/> in this instance by the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <remarks>
        ///     Grouping by an axis refers to all the <see cref="PlanogramPositionPlacement"/> 
        ///     that are on the same intersection of the normal planes to the given <paramref name="axis"/>.
        ///     For example, group by the X axis means groups of placements with the same Y and Z values.
        /// </remarks>
        public Dictionary<Tuple<Int16, Int16>, List<PlanogramPositionPlacement>> GroupByAxis(AxisType axis)
        {
            return GroupByAxis(axis, this);
        }

        /// <summary>
        ///     Group the <see cref="PlanogramPositionPlacement"/> in this instance by the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <remarks>
        ///     Grouping by an axis refers to all the <see cref="PlanogramPositionPlacement"/> 
        ///     that are on the same intersection of the normal planes to the given <paramref name="axis"/>.
        ///     For example, group by the X axis means groups of placements with the same Y and Z values.
        /// </remarks>
        public static Dictionary<Tuple<Int16, Int16>, List<PlanogramPositionPlacement>> GroupByAxis(AxisType axis,
            IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            switch (axis)
            {
                case AxisType.X:
                    return
                        positionPlacements.GroupBy(
                            o => new Tuple<Int16, Int16>(o.Position.SequenceY, o.Position.SequenceZ))
                            .ToDictionary(o => o.Key, o => o.ToList());
                case AxisType.Y:
                    return
                        positionPlacements.GroupBy(
                            o => new Tuple<Int16, Int16>(o.Position.SequenceX, o.Position.SequenceZ))
                            .ToDictionary(o => o.Key, o => o.ToList());
                case AxisType.Z:
                    return
                        positionPlacements.GroupBy(
                            o => new Tuple<Int16, Int16>(o.Position.SequenceX, o.Position.SequenceY))
                            .ToDictionary(o => o.Key, o => o.ToList());
                default:
                    Debug.Fail("Unknown axis type");
                    return new Dictionary<Tuple<Int16, Int16>, List<PlanogramPositionPlacement>>();
            }
        }

        /// <summary>
        /// Removes the given placement from this list without
        /// adding it to the deleted list.
        /// </summary>
        internal void RemoveWithoutDelete(PlanogramPositionPlacement placement)
        {
            Int32 idx = this.IndexOf(placement);
            if (idx >= 0)
            {
                base.RemoveItem(idx);
            }
        }

        /// <summary>
        ///     Shifts by one all sequence X values of placements after the given one.
        /// </summary>
        /// <param name="placement">The placement from which to shift other placements.</param>
        /// <remarks>Any placement that has a higher Sequence X than the given one will be shifted, regardless of Sequence Y or Z.</remarks>
        internal void ShiftSequenceXFrom(PlanogramPositionPlacement placement)
        {
            // move all other positions on the same axis along
            // we move all here not just those in same yz group.
            foreach (PlanogramPositionPlacement p in
                this.Where(p => !Equals(p, placement) &&
                                p.Position.SequenceX >= placement.Position.SequenceX))
                p.Position.SequenceX++;
        }

        /// <summary>
        ///     Shifts by one all sequence Y values of placements after the given one.
        /// </summary>
        /// <param name="placement">The placement from which to shift other placements.</param>
        /// <param name="allPositionGroups">Whether to (<c>true</c>) affect all placements with a higher Y or (<c>false</c>) only those on the same sequence X and Z</param>
        /// <remarks>Any placement that has a higher Sequence Y than the given one will be shifted, optionally if it also belongs to the same Sequence X or Z.</remarks>
        internal void ShiftSequenceYFrom(PlanogramPositionPlacement placement, Boolean allPositionGroups)
        {
            foreach (PlanogramPositionPlacement p in
                this.Where(p => !Equals(p, placement) &&
                                (allPositionGroups ||
                                 (p.Position.SequenceX == placement.Position.SequenceX &&
                                  p.Position.SequenceZ == placement.Position.SequenceZ)) &&
                                p.Position.SequenceY >= placement.Position.SequenceY))
                p.Position.SequenceY++;
        }

        /// <summary>
        ///     Shifts by one all sequence Z values of placements after the given one and that are on the same sequence X and Y as it.
        /// </summary>
        /// <param name="placement">The placement from which to shift other placements.</param>
        /// <remarks>Any placement that has a higher Sequence Z than the given one will be shifted, as long as it belongs to the same Sequence X and Y.</remarks>
        internal void ShiftSequenceZFrom(PlanogramPositionPlacement placement)
        {
            // move all other positions on the same axis along
            foreach (PlanogramPositionPlacement p in
                this.Where(p => !Equals(p, placement) &&
                                p.Position.SequenceX == placement.Position.SequenceX &&
                                p.Position.SequenceY == placement.Position.SequenceY &&
                                p.Position.SequenceZ >= placement.Position.SequenceZ))
                p.Position.SequenceZ++;
        }

        #endregion

        #endregion

        #region IDisposable

        public void Dispose()
        {
            //dispose of all placements.
            var disposables = this.ToArray();
            this.Clear();
            foreach (PlanogramPositionPlacement item in disposables)
            {
                item.Dispose();
            }
        }

        #endregion
    }
}
